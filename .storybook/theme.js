import { create } from '@storybook/theming';
import logo from '../src/img/ujin_small.svg';

export default create({
  base: 'light',
  brandTitle: 'UJIN',
  brandUrl: 'https://ujin.tech',
  brandImage: logo,
  brandTarget: '_self',
});
