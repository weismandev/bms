const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const paths = require('./paths');
const { version } = require('../package.json');

module.exports = {
  entry: [`${paths.src}/index.tsx`],
  output: {
    path: paths.build,
    filename: '[name].bundle.js',
    publicPath: '/',
    clean: true,
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.public,
          to: paths.build,
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),
    new HtmlWebpackPlugin({
      template: `${paths.src}/index.html`,
      filename: 'index.html',
      favicon: `${paths.public}/favicon.ico`,
    }),
    new Dotenv(),
    new webpack.DefinePlugin({
      'process.env.BUILD_VERSION': JSON.stringify(version),
      'process.env.BUILD_TYPE': JSON.stringify(process.env.BUILD_TYPE),
      'process.env.API_URL': JSON.stringify(process.env.API_URL),
      'process.env.IM_URL': JSON.stringify(process.env.IM_URL),
      'process.env.NOTIFICATION_URL': JSON.stringify(process.env.NOTIFICATION_URL),
      'process.env.CLIENT_TOKEN': JSON.stringify(process.env.CLIENT_TOKEN),
      'process.env.LANG': JSON.stringify(process.env.LANG),
      // FEATURE FLAGS
      'process.env.OLD_OBJECTS_CRUD': JSON.stringify(process.env.OLD_OBJECTS_CRUD),
      'process.env.OLD_RENTS_CRUD': JSON.stringify(process.env.OLD_RENTS_CRUD),
      'process.env.OLD_PARKINGS_CRUD': JSON.stringify(process.env.OLD_PARKINGS_CRUD),
    }),
    new webpack.ProgressPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: 'swc-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[contenthash].[ext]',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.mp3$/,
        loader: 'file-loader',
        options: {
          name: 'sound/[name].[contenthash].[ext]',
        },
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.mjs'],
    alias: {
      ...paths,
    },
  },
};
