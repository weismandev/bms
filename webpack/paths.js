const path = require('path');

module.exports = {
  src: path.resolve(__dirname, '../src'),
  build: path.resolve(__dirname, '../build'),
  public: path.resolve(__dirname, '../public'),
  '@api': path.resolve(__dirname, '../src/api'),
  '@components': path.resolve(__dirname, '../src/components'),
  '@configs': path.resolve(__dirname, '../src/configs'),
  '@consts': path.resolve(__dirname, '../src/consts'),
  '@entities': path.resolve(__dirname, '../src/entities'),
  '@features': path.resolve(__dirname, '../src/features'),
  '@img': path.resolve(__dirname, '../src/img'),
  '@redux': path.resolve(__dirname, '../src/redux'),
  '@shared': path.resolve(__dirname, '../src/shared'),
  '@tools': path.resolve(__dirname, '../src/tools'),
  '@ui': path.resolve(__dirname, '../src/ui'),
  '@effector-form': path.resolve(__dirname, '../src/features/effector-form'),
  '@sounds': path.resolve(__dirname, '../src/sounds'),
  '@widgets': path.resolve(__dirname, '../src/widgets'),
  '@pages': path.resolve(__dirname, '../src/pages'),
};
