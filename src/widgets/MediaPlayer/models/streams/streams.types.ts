import { CodecFormat } from '../codec/codec.types';

export type Stream = {
  codec: CodecFormat;
  line?: 360 | 480 | 720 | 1080;
  sound?: number;
  url: string;
};
