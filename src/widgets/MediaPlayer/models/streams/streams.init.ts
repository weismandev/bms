import { sample } from 'effector';

import { $codecPriority } from '../codec';
import { $streams, $stream } from './streams.model';

// sample({
//   source: [$streams, $codecPriority],
//   fn: ([streams, codecPriority]) =>
//     streams.find(({ codec }) => codec === codecPriority) || null,
//   target: $stream,
// });
