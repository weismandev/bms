import { createStore } from 'effector';
import { Stream } from './streams.types';

export const $streams = createStore<Stream[] | []>([]);
export const $stream = createStore<Stream | null>(null);
