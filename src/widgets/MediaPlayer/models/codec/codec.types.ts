export type CodecFormat = 'hls' | 'mjpeg' | 'frame';

export type CodecPriority = {
  [id: string]: CodecFormat;
};

export type CodecPriorities = {
  [id: string]: ['hls', 'mjpeg', 'frame'];
};
