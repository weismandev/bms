import { sample } from 'effector';

import {
  $codecPriority,
  $codecListPriorities,
  setCodecPriorities,
  setCodecPriority,
  replaceCodec,
} from './codec.model';

import { CodecFormat } from './codec.types';

$codecPriority.on(setCodecPriority, (state, data) => {
  // if (state[data.id]) return state;
  return {
    ...state,
    [data.id]: data.codec,
  };
});

$codecListPriorities
  .on(setCodecPriorities, (state, data) => ({
    ...state,
    ...data,
  }))
  .watch((raw) => {
    Object.keys(raw).forEach((id) => {
      const codec = raw[id][0] || 'hls';
      setCodecPriority({ id, codec });
    });
  });

sample({
  source: [$codecPriority, $codecListPriorities],
  clock: replaceCodec,
  fn: ([codecPriority, codecListPriorities], id) => {
    const currentCodec = codecPriority[id];
    const codecList = codecListPriorities[id];
    const indexCurrentCodec = codecList.findIndex((codec: CodecFormat) => codec === currentCodec);
    const codec = codecList[indexCurrentCodec + 1] || codecList[0];
    return { id, codec };
  },
  target: setCodecPriority,
});
