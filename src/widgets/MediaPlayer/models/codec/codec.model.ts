import { createStore, createEvent } from 'effector';
import { CodecFormat, CodecPriority, CodecPriorities } from './codec.types';

export const setCodecPriorities = createEvent<CodecPriorities>();
export const setCodecPriority =
  createEvent<{ id: string; codec: CodecFormat }>();
export const replaceCodec = createEvent<number>();

export const $codecListPriorities = createStore<CodecPriorities>({});
export const $codecPriority = createStore<CodecPriority>({});
