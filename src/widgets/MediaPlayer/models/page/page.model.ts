import { createEffect, createStore, createEvent, combine, Store } from 'effector';
import { createGate } from 'effector-react';

import { getCamerasList } from '../../api';
import { PayloadStreams, ResponseStreams, StreamsInfo } from '../../types';
import { $codecPriority } from '../codec/codec.model';
import { parseGMT } from '../../lib/parseGMT';

export const PageGate = createGate<{ id: number }>();
export const { open: pageMounted, close: pageUnmoumted } = PageGate;
export const fxGetCameras = createEffect<
  PayloadStreams,
  ResponseStreams,
  Error
>(getCamerasList);

export const updateStreamById = createEvent<number>();
export const getStreamById = createEvent<number>();
export const setDisplayMode = createEvent<'stream' | 'archive'>();

export const $raw = createStore<StreamsInfo[]>([]);
export const $stream = createStore({});
export const $isLoading = createStore<boolean>(false);

export const $displayMode = createStore<'stream' | 'archive'>('archive');

export const userGMT = new Date().getTimezoneOffset() / 60;
export const $GMT: Store<{ offset: number, timezone: string }> = $raw.map((raw, lastState = { offset: 0, timezone: '+00:00' }) =>
  raw?.length === 0 ? lastState : raw.map(({ timezone }) => parseGMT(timezone))[0]);

export const $streams = combine(
  $raw,
  $codecPriority,
  (cameras, codecPriority) =>
    cameras.map((camera) => ({
      id: camera.id,
      title: camera.title,
      codec: camera.codec,
      isAvailable: camera.available,
      stream: camera.streams.find(
        (stream) => stream.codec === codecPriority[camera.id]
      ),
    }))
);
