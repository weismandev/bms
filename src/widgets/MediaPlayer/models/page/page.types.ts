import { Streams } from '../../types';
import { CodecFormat } from '../codec/codec.types';

export type AnyStreamPayload = {
  id: number;
  title: string;
  preview: string;
};

export type MappingCameraProperty = {
  codec_priority: CodecFormat[];
  streams: Streams[];
  id: number;
  title: string;
  preview: string;
};
