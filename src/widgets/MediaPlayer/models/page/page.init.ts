import { guard, sample } from 'effector';

import { getNameUserPlatform } from '@tools/getNameUserPlatform';
import {
  fxGetCameras,
  PageGate,
  pageUnmoumted,
  $raw,
  $displayMode,
  $isLoading,
  setDisplayMode,
} from './page.model';
import { setCodecPriorities } from '../codec';

$isLoading
  .on(fxGetCameras.pending, (isLoading: boolean) => isLoading)
  .reset(pageUnmoumted);

$raw
  .on(fxGetCameras.done, (_, { result }) => result.camera)
  .reset(pageUnmoumted);

$displayMode.on(setDisplayMode, (_, mode) => mode).reset(pageUnmoumted);

guard({
  clock: PageGate.state,
  filter: ({ id }) => !isNaN(id),
  target: fxGetCameras.prepend((payload: { id: number }) => ({
    ...payload,
    hls: 1,
    platform: getNameUserPlatform(),
  })),
});

sample({
  source: $raw,
  fn: (raw) =>
    raw.reduce((acc, curr) => ({ ...acc, [curr.id]: curr.codec_priority }), {}),
  target: setCodecPriorities,
});
