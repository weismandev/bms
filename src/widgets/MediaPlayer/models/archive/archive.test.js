import { createEvent } from 'effector';
import { $dates } from './archive-dates.model';

test('event dates', () => {
  const setDates = createEvent();
  $dates.on(setDates, (_, dates) => dates);
  const date = new Date();
  setDates([date.getTime()]);
  expect($dates.getState()).toEqual([new Date(date) instanceof Date]);
});
