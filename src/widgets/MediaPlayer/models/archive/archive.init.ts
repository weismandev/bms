import { forward, sample } from 'effector';
import i18n from '@shared/config/i18n';

import { streamApi } from '../../api';
import { fxGetCameras } from '../page/page.model';
import { $codecListPriorities } from '../codec';
import { $streams } from '../streams';

import {
  ArchiveGate,
  archiveMounted,
  archiveUnmoumted,
  fxGetArchiveDateEvents,
  fxGetArchiveStream,
  $environment,
  $events,
  $rawArchiveStream,
  $loading,
  getArchiveDateEvents,
  getArchiveStreamByEvent,
  nextArchiveEvent,
} from './archive.model';
import { $debugInfo, setUnavailable, setDebugInfo } from '../resource-unavailable/resource-unavailable.model';

import { addUserPlatform, addUserTimezone } from '../../lib';
import { prepandPayload } from './archive.formatter';

import { Event } from '../../types';

type DebugInfo = {
  id: number;
  dt: number;
  message: string;
};

const { t } = i18n;

fxGetArchiveDateEvents.use(streamApi.getArchiveDateEvents);
fxGetArchiveStream.use(streamApi.getArchiveStream);

$environment.on(fxGetCameras.done, (_, { result }) => {
  const [camera] = result.camera || [];
  return camera?.url_archive || 'https://api-product.mysmartflat.ru/api/';
});

$events
  .on(fxGetArchiveDateEvents.done, (_, { result }) =>
    result.data.events.sort((x, y) => x.timestamp - y.timestamp)
  )
  .on(getArchiveDateEvents, () => [])
  .reset(archiveUnmoumted);

$loading
  .on(fxGetArchiveDateEvents.pending, (state, isLoading) => ({
    ...state,
    events: isLoading,
  }))
  .on(fxGetArchiveStream.pending, (state, isLoading) => ({
    ...state,
    videoarchive: isLoading,
  }))
  .reset(archiveUnmoumted);

$rawArchiveStream
  .on(fxGetArchiveStream.done, (_, { params: { id }, result: { data } }) => ({
    ...data,
    id,
  }))
  .reset(archiveUnmoumted);

$codecListPriorities
  .on(
    fxGetArchiveStream.done,
    (state, { params: { id }, result: { data } }) => ({
      ...state,
      ...(data ? { [id]: data.codec_priority } : {}),
    })
  )
  .reset(archiveUnmoumted);

/* проверка на существование ресурса */
sample({
  source: fxGetArchiveStream.done,
  filter: (payload) => Boolean(payload.result.data === null || payload.result.error > 0),
  fn: (payload) => ({
    id: payload.params.id,
    dt: payload.params.dt,
    message: payload.result.message || t('ResourceIsNotAvailable'),
  }),
  target: [setDebugInfo, setUnavailable.prepend((): boolean => true)]
});

sample({
  clock: setUnavailable,
  source: [$events, $debugInfo, $environment],
  filter: (_, isUnavailableResource) => Boolean(isUnavailableResource),
  fn: ([events, debugInfo, url]: [events: Event[], debugInfo: DebugInfo, url: string]) => {
    const dt = debugInfo?.dt || 0;
    const newEvent = events?.find((event) => event.timestamp > dt);
    if (newEvent && debugInfo) {
      return { id: debugInfo.id, dt: newEvent.timestamp, url };
    }
    return false;
  },
  target: nextArchiveEvent,
});

sample({
  clock: nextArchiveEvent,
  filter: (payload) => Boolean(payload),
  target: fxGetArchiveStream.prepend(
    prepandPayload({
      ...addUserPlatform(),
      ...addUserTimezone()
    })
  ),
});

sample({
  clock: fxGetArchiveStream.doneData,
  fn: ({ data }) => data?.streams || [],
  target: $streams,
});

forward({
  from: archiveMounted,
  to: fxGetCameras.prepend(prepandPayload({ ...addUserPlatform(), hls: 1 })),
});

sample({
  source: { state: ArchiveGate.state, url: $environment },
  clock: getArchiveDateEvents,
  fn: ({ state, url }, dt) => ({ id: state.id, dt, url }),
  target: fxGetArchiveDateEvents.prepend(prepandPayload({ ...addUserPlatform() })),
});

sample({
  source: { state: ArchiveGate.state, url: $environment },
  clock: getArchiveStreamByEvent,
  fn: ({ state, url }, dt) => ({ id: state.id, dt, url }),
  target: fxGetArchiveStream.prepend(
    prepandPayload({
      ...addUserPlatform(),
      ...addUserTimezone()
    })
  ),
});
