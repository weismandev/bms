import { forward, sample } from 'effector';
import { isYesterday, fromUnixTime } from 'date-fns';
import { streamApi } from '../../api';
import userPlatformToPayload from '../../api/userPlatformToPayload';

import {
  ArchiveDateMounted,
  ArchiveDateUnmoumted,
  fxGetArchiveDates,
  $dates,
  $isLoading,
  $selectedDate,
  selectDate,
} from './archive-dates.model';

fxGetArchiveDates.use(streamApi.getArchiveDates);

$dates
  .on(fxGetArchiveDates.doneData, (_, { data }) => data.dates)
  .reset(ArchiveDateUnmoumted);

$selectedDate
  .on(selectDate, (_, timestamp) => timestamp)
  .reset(ArchiveDateUnmoumted);

$isLoading
  .on(fxGetArchiveDates.pending, (_, pending) => pending)
  .reset(ArchiveDateUnmoumted);

forward({
  from: ArchiveDateMounted,
  to: fxGetArchiveDates.prepend(userPlatformToPayload),
});

/* Устанавливаю по умолчанию вчерашний день */
sample({
  clock: $dates,
  fn: (dates) => dates.find((item) => isYesterday(fromUnixTime(item))) || dates[0],
  target: $selectedDate,
});
