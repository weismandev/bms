import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { PayloadArchive, ResponseArchiveDates } from '../../types';

export const ArchiveDatesGate = createGate<{ id: number }>();
export const { open: ArchiveDateMounted, close: ArchiveDateUnmoumted } =
  ArchiveDatesGate;

export const fxGetArchiveDates = createEffect<
  PayloadArchive,
  ResponseArchiveDates,
  Error
>();

export const $dates = createStore<number[]>([]);
export const $isLoading = createStore<boolean>(false);
export const $selectedDate = createStore<number>(
  Math.floor(new Date().getTime() / 1000)
);

export const selectDate = createEvent<number>();
