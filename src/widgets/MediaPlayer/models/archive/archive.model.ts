import { createStore, createEvent, createEffect, combine } from 'effector';
import { createGate } from 'effector-react';
import { format, fromUnixTime, isSameDay } from 'date-fns';
import { $selectedDate } from './archive-dates.model';

import { toISO } from '../../lib';

import {
  PayloadArchive,
  ResponseArchiveStream,
  ResponseArchiveDateEvents,
  RawArchiveStream,
  Event,
  EventGridStep,
  EventSlots,
  EventGrid,
} from '../../types';

export const ArchiveGate = createGate<{ id: number }>();
export const { open: archiveMounted, close: archiveUnmoumted } = ArchiveGate;

export const fxGetArchiveStream = createEffect<
  PayloadArchive,
  ResponseArchiveStream,
  Error
>();
export const fxGetArchiveDateEvents = createEffect<
  PayloadArchive,
  ResponseArchiveDateEvents,
  Error
>();

export const getArchiveDateEvents = createEvent<number>();
export const getArchiveStreamByEvent = createEvent<number>();
export const getArchiveStream = createEvent<PayloadArchive>();
export const nextArchiveEvent = createEvent<{ id: number; dt: number } | false>();

export const $rawArchiveStream = createStore<RawArchiveStream | null>(null);
export const $environment = createStore<string>('');
export const $events = createStore<Event[]>([]);
export const $archive = createStore([]);

export const $loading = createStore<{
  events: boolean;
  videoarchive: boolean;
}>({ events: false, videoarchive: false });

export const $eventSlots = combine(
  $events,
  $selectedDate,
  (events: Event[], selectedDate) => {
    const filteredCurrentDay = events.filter(({ timestamp }) =>
      isSameDay(new Date(selectedDate * 1000), new Date(timestamp * 1000))
    );

    const data = filteredCurrentDay.reduce(
      (acc: any, { id, timestamp, duration }: any) => {
        const endEventTime = timestamp + duration;
        const timeSlotStart = formatDateToStepSlot(timestamp);
        const timeSlotEnd = formatDateToStepSlot(endEventTime);
        const prevSlot = acc.at(-1);

        if (
          prevSlot?.timestamp &&
          prevSlot?.timestamp + prevSlot.duration + 60 >= timestamp
        ) {
          const modifySlot = acc.pop();
          return [
            ...acc,
            {
              ...modifySlot,
              timeSlotEnd,
            },
          ];
        }
        return [
          ...acc,
          {
            id,
            timestamp,
            duration,
            timeSlotStart,
            timeSlotEnd,
          },
        ];
      }, []);
      // .map(({ id, timestamp, duration }) => {
      //   const timeSlotStart = formatDateToStepSlot(timestamp);
      //   const timeSlotEnd = formatDateToStepSlot(timestamp + duration);
      //   return {
      //     id,
      //     timestamp,
      //     duration,
      //     timeSlotStart,
      //     timeSlotEnd,
      //   };
      // });
    return fillGridSlots(data);
  }
);

export const $stream = combine(
  $rawArchiveStream,
  (raw = { id: 0, codec_priority: [], streams: [] }): any => {
    const codec = Array.isArray(raw?.codec_priority) ? (raw?.codec_priority[0] || 'hls') : 'hls';
    const streams = raw?.streams || [];
    return {
      id: raw?.id || 0,
      stream: streams.find((stream) => stream?.codec === codec) || {
        url: '',
        codec: 'hls',
      },
    };
  }
);

function getCountSecondsOfDay({ hours, minutes, seconds }: EventGridStep) {
  return hours * 3600 + minutes * 60 + seconds;
}

function fillGridSlots(slots: EventSlots[]): EventGrid[] {
  return slots.map(({ timeSlotStart, timeSlotEnd, ...slot }) => {
    const gridColumnStart = getCountSecondsOfDay(timeSlotStart);
    const gridColumnEnd = getCountSecondsOfDay(timeSlotEnd);
    return { ...slot, gridColumnStart, gridColumnEnd };
  });
}

function getTimeStart(timestamp: number) {
  const ISODate = toISO(fromUnixTime(timestamp));
  return format(new Date(ISODate), 'HH:mm:ss');
}

function formatDateToStepSlot(unixTime: number): EventGridStep {
  const [hours, minutes, seconds] = getTimeStart(unixTime)
    .split(':')
    .map(Number);
  return { hours, minutes, seconds };
}
