/**
 * Функция преобразования данных перед отправкой на бэкенд
 *  @param {args} аргументы
 *  @returns {Object} возвращает payload
 * * */

import { PayloadArchive, PayloadStreams } from '../../types';

type Payload = {
  hls?: number;
  platform?: string;
  timezone?: string;
};

export const prepandPayload = (args: Payload) =>
  (payload: PayloadArchive | PayloadStreams) => ({
  ...args,
  ...payload,
});
