import { sample } from 'effector';

import {
  $isUnavailable,
  $debugInfo,
  setUnavailable,
  setDebugInfo
} from './resource-unavailable.model';

import { pageUnmoumted } from '../page';
import { archiveUnmoumted } from '../archive';

$isUnavailable
  .on(setUnavailable, (_, isUnavailable) => isUnavailable)
  .reset([pageUnmoumted, archiveUnmoumted]);

$debugInfo
  .on(setDebugInfo, (_, debugInfo) => debugInfo)
  .reset([pageUnmoumted, archiveUnmoumted]);

/* После скрытия сообщения, удаляю информацию об отладке */
sample({
  source: setUnavailable,
  filter: (isUnavailable) => Boolean(!isUnavailable),
  fn: () => null,
  target: $debugInfo
});
