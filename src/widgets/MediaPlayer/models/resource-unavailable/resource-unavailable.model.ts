import { createStore, createEvent } from 'effector';

type DebugInfo = {
  id: number;
  dt: number;
  message: string;
};

export const $isUnavailable = createStore<boolean>(false);
export const $debugInfo = createStore<DebugInfo | null>(null);

export const setUnavailable = createEvent<boolean>();
export const setDebugInfo = createEvent<DebugInfo>();
