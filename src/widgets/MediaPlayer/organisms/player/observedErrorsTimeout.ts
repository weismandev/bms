import { ErrorTypes } from 'hls.js';

type TimeoutType = ReturnType<typeof setTimeout>;
type CB = () => void;
interface Collection {
  [key: number]: {
    [key: string]: {
      timeout: TimeoutType;
      forgetMistakeTimeout: TimeoutType | null;
      ErrorTypes: Partial<Record<ErrorTypes, number>>;
    };
  };
}

const observedErrorsTimeout = (() => {
  const instanceErrors: Collection = {};

  const initTimer = (cb: CB): TimeoutType => setTimeout(cb, 7000);
  const forgetMistake = (uid: number) => (): void => {
    delete instanceErrors[uid];
  };

  return {
    initInstance: (id: number) => {
      if (!instanceErrors[id]) {
        instanceErrors[id] = {};
      }
    },
    localInstance: (uid: number, cb: CB) => ({
      setError: (type: ErrorTypes, src: string): void => {
        // console.log('setError', type);
        if (!instanceErrors[uid][src]) {
          instanceErrors[uid][src] = {
            timeout: initTimer(cb),
            ErrorTypes: { [type]: 1 },
            forgetMistakeTimeout: null,
          };
        } else {
          instanceErrors[uid][src].ErrorTypes = {
            ...instanceErrors[uid][src].ErrorTypes,
            [type]: (instanceErrors[uid][src].ErrorTypes[type] || 0) + 1,
          };
        }
        const count = instanceErrors[uid][src].ErrorTypes[type] || 0;
        if (count > 3) {
          cb();
        }
      },
      stopWatching: (src: string): void => {
        const errorData = instanceErrors[uid][src];
        clearTimeout(errorData.timeout);
        if (errorData.forgetMistakeTimeout) {
          clearTimeout(errorData.forgetMistakeTimeout);
        }
        instanceErrors[uid][src].forgetMistakeTimeout = setTimeout(forgetMistake(uid), 20000);
      },
    }),
  };
})();

export default observedErrorsTimeout;
