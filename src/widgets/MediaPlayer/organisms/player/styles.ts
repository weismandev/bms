import styled from '@emotion/styled';
import { StyledProps } from '../../types';

// video::-webkit-media-controls-panel
// display: ${(props) => (props?.controls?.fullscreen ? 'block' : 'none')};

export const Wrapper = styled.div<StyledProps>`
  -webkit-full-screen {   min-width: 100%;   min-height: 100%; }

  iframe {
    border: none;
    min-height: 370px;
  }

  // video {
  //   background: linear-gradient(315deg, #f0f0f0, #cacaca);
  // }

  audio::-webkit-media-controls-timeline,
  video::-webkit-media-controls-timeline,
  video::-webkit-media-controls-timeline-container {
    display: none;
  }

  video::-webkit-media-controls-play-button {
    display: none;
  }

  video::-webkit-media-controls-volume-slider-container,
  video::-webkit-media-controls-volume-slider,
  video::-webkit-media-controls-mute-button {
    display: none;
  }

  video::-webkit-media-controls-timeline,
  video::-webkit-media-controls-current-time-display,
  video::-webkit-media-controls-time-remaining-display {
    display: none;
  }

  video::-webkit-media-controls-seek-back-button,
  video::-webkit-media-controls-seek-forward-button,
  video::-webkit-media-controls-toggle-closed-captions-button,
  video::-webkit-media-controls-mute-button,
  video::-webkit-media-controls-rewind-button,
  video::-webkit-media-controls-return-to-realtime-button {
    display: none;
  }

  video::-webkit-full-page-media::-webkit-media-controls-panel {
    display: none;
  }

  video::-webkit-media-controls-fullscreen-button {
    display: none;
  }
`;
