export const streamWatcher = (src: string, cb: () => void): {
  start: () => XMLHttpRequest,
  repeat: () => void;
  unmount: () => void;
} => {
  let xhr: XMLHttpRequest | null = null;

  const start = () => {
    xhr = new XMLHttpRequest();
    xhr.open('GET', src);
    xhr.onload = cb.bind(xhr);
    xhr.send();
    return xhr;
  };

  const unmount = () => {
    if (xhr instanceof XMLHttpRequest) {
      xhr.abort();
      xhr = null;
    }
  };

  const repeat = () => {
    unmount();
    start();
  };

  return {
    start,
    repeat,
    unmount
  };
};
