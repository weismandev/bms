import './styles.css';

export const mjpegStream = (src: string, imageEl: HTMLImageElement) => {
  const init = () => {
    imageEl.classList.add('loader');
    imageEl.setAttribute('src', '');
    imageEl.setAttribute('src', src);
  };
  init();
  const onLoad = () => {
    imageEl.classList.remove('loader');
  };

  imageEl.addEventListener('load', onLoad);

  return {
    unmount: () => {
      if (imageEl instanceof HTMLImageElement) {
        imageEl.setAttribute('src', '');
      }
      imageEl.removeEventListener('load', onLoad);
    }
  };
};
