import { ErrorTypes } from 'hls.js';

type TimeoutType = ReturnType<typeof setTimeout>;

interface InstanceHls {
  numberAttempts: number;
  ErrorTypes: Partial<Record<ErrorTypes, TimeoutType>>;
  timeOut: any;
}

interface Collection {
  [key: number]: InstanceHls;
}

const observedInstance = (() => {
  const defaultInstanceValues = {
    timeOut: null,
    numberAttempts: 2,
    ErrorTypes: {},
  };
  const instanceHls: Collection = {};

  const initTimer = (): TimeoutType =>
    setTimeout(() => {
      // console.log('setTimeout!!!', instanceHls);
    }, 5000);

  return {
    initInstance: (id: number) => {
      if (!instanceHls[id]) {
        instanceHls[id] = { ...defaultInstanceValues };
      }
    },
    localInstance: (uid: number) => {
      return {
        setError: (type: ErrorTypes): void => {
          clearTimeout(
            instanceHls[uid].ErrorTypes[type] || setTimeout(Function)
          );
          instanceHls[uid].ErrorTypes[type] = initTimer();
        },
        isSetErrorType: (type: ErrorTypes): boolean =>
          !!instanceHls[uid].ErrorTypes[type],
        getValue: (): InstanceHls => instanceHls[uid],
      };
    },
  };
})();

export default observedInstance;
