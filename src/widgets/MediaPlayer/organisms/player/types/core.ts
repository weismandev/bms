export type Core = {
  id: number;
  videoSrc: string;
  codec?: 'hls' | 'frame' | 'mjpeg';
  htmlElement: HTMLVideoElement;
  event: {
    onLoad: () => void;
    onReplaceCodec: () => void;
    onStreamFinished: () => void;
    resourceUnavailable: () => void;
    onDetached: () => void;
  };
};
