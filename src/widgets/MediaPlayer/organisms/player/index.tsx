import { FC, useRef, useEffect, useState, useLayoutEffect } from 'react';

import CorePlayer from './core';
import observedInstance from './observedInstance';
import observedErrorsTimeout from './observedErrorsTimeout';
import { mjpegStream } from './mjpeg-stream';
import { iframeStream } from './iframe-stream';

import { ControlBarFullscreen } from '../../features/control-bar-fullscreen';
import { CameraUnavailable } from '../../features/camera-unavailable';
import { ResourceUnavailable } from '../../features/resource-unavailable';

import { setUnavailable } from '../../models/resource-unavailable';

import { createElement } from '../../lib';

import { Wrapper } from './styles';

import { PlayerProps, PlayerElement, TagName, Controls } from '../../types';

const codecMatch: { [key: string]: TagName } = {
  hls: 'video',
  mjpeg: 'img',
  frame: 'iframe',
};

let mjpeg = {};

const Player: FC<PlayerProps> = ({
  id,
  videoSrc,
  codec,
  isAvailable = true,
  isArchive = false,
  controls = false,
  defaultControls = false,
  preview = '',
  isPlay = 'auto',
  ...event
}) => {
  const controlsPlayer = useRef<{
    htmlElement: PlayerElement;
    destroy: () => void;
    loadSource: (src: string) => void;
    stopLoad: () => void;
    startLoad: () => void;
  } | null>();
  const divEl = useRef<HTMLDivElement>(null);

  const [width, setWidth] = useState<number>(500);

  const appendElement = (element: PlayerElement) => {
    if (divEl.current !== null) {
      divEl.current.appendChild(element);
    }
  };

  const onDetached = () => {
    if (divEl.current !== null) {
      divEl.current.childNodes.forEach((element: ChildNode) => {
        if (divEl.current !== null) {
          divEl.current?.removeChild(element);
        }
      });
    }
  };

  const onReplaceCodec = () => {
    controlsPlayer.current = null;
    setUnavailable(true);
    if (event.onReplaceCodec instanceof Function) {
      event.onReplaceCodec();
    }
  };

  const handleLoad = () => {
    if (event.onLoad instanceof Function) {
      // console.log('onLoad');
      event.onLoad(true);
    }
  };

  const getHlsUrl = async (url: string) => {
    const data = await fetch(url);
    const result = await data.json();
    return result;
  };

  const resourceUnavailable = () => {
    setUnavailable(true);
  };

  const handleInitStream = (htmlElement?: PlayerElement) => {
    if (codec === 'hls') {
      const ev = {
        onDetached,
        onStreamFinished,
        onReplaceCodec,
        onLoad: handleLoad,
        resourceUnavailable,
      };
      if (!videoSrc.includes('.m3u8')) {
        getHlsUrl(videoSrc).then(({ url }) => {
          controlsPlayer.current = CorePlayer({
            id,
            videoSrc: url,
            htmlElement,
            event: ev,
          });
        });
      } else {
        controlsPlayer.current = CorePlayer({
          id,
          videoSrc,
          htmlElement,
          event: ev,
        });
      }
    } else if (htmlElement instanceof HTMLImageElement) {
      mjpeg = mjpegStream(videoSrc, htmlElement);
    } else if (htmlElement instanceof HTMLIFrameElement) {
      iframeStream(videoSrc, htmlElement);
    } else {
      htmlElement.src = videoSrc;
    }
  };

  const onStreamFinished = () => {
    const htmlElement = controlsPlayer.current?.htmlElement;
    controlsPlayer.current = null;
    handleInitStream(htmlElement);
  };

  useEffect(() => {
    if (isPlay) {
      controlsPlayer.current?.startLoad();
    } else {
      controlsPlayer.current?.stopLoad();
    }
  }, [isPlay]);

  useEffect(() => {
    setUnavailable(false);
    if (isAvailable && videoSrc) {
      if (controlsPlayer.current) {
        controlsPlayer.current?.loadSource(videoSrc);
      } else {
        const tag = codecMatch[codec] || 'video';
        const htmlElement = createElement(tag, !!controls, preview, width);
        observedInstance.initInstance(id);
        observedErrorsTimeout.initInstance(id);
        appendElement(htmlElement);
        handleInitStream(htmlElement);
      }
    }
    return () => {
      setUnavailable(false);
      event.onLoad(false);
      if (mjpeg && mjpeg?.unmount) {
        mjpeg?.unmount();
      }
      controlsPlayer.current?.destroy();
      controlsPlayer.current = null;
      if (divEl.current !== null) {
        const htmlCollection = divEl.current?.getElementsByTagName(codecMatch[codec] || 'video');
        Array.from(htmlCollection).forEach((element: ChildNode) => {
          divEl.current?.removeChild(element);
        });
      }
    };
  }, [videoSrc]);

  useLayoutEffect(() => {
    if (divEl.current !== null) {
      setWidth(divEl.current.clientWidth);
    }
  }, []);

  const styledProps: Controls = {
    ...(controls instanceof Object ? controls : {})
  };

  if (!isAvailable) {
    return (
      <CameraUnavailable />
    );
  }

  return (
    <Wrapper isArchive controls={defaultControls ? styledProps : {}}>
      <div
        style={{ backgroundColor: isArchive ? 'black' : 'transparent', textAlign: 'center', position: 'relative' }}
        ref={divEl}
      >
        <ControlBarFullscreen videoNode={divEl.current} />
        <ResourceUnavailable />
      </div>
    </Wrapper>
  );
};

export { Player };
