import Hls, { Events, ErrorTypes, ErrorData } from 'hls.js';
import { PlayerElement } from '../../types';

import { Core } from './types';

import observedInstance from './observedInstance';
import observedErrorsTimeout from './observedErrorsTimeout';

type CoreVoid = {
  htmlElement: PlayerElement;
  destroy: () => void;
  loadSource: (src: string) => void;
  stopLoad: () => void;
  startLoad: () => void;
};

// TODO useEventsError
const CorePlayer = ({ id, videoSrc, htmlElement, event }: Core): CoreVoid => {
  const hls = new Hls({
    enableWorker: false
  });

  const errorByTimeout = () => {
    hls.stopLoad();
    hls.destroy();
    event.onReplaceCodec();
  };

  const instanceHls = observedInstance.localInstance(id);
  const errorsHls = observedErrorsTimeout.localInstance(id, errorByTimeout);

  if (htmlElement !== null) {
    hls.attachMedia(htmlElement);
    hls.loadSource(videoSrc);

    hls.on(Hls.Events.MANIFEST_PARSED, () => {
      htmlElement.muted = true;
      htmlElement.play();
    });

    hls.on(Hls.Events.MEDIA_ATTACHED, () => {
      // console.log('MEDIA_ATTACHED');
      event.onLoad();
    });

    hls.on(Hls.Events.LEVEL_LOADED, () => {
      // console.log('LEVEL_LOADED');
      event.onLoad();
      errorsHls.stopWatching(videoSrc);
    });
    hls.on(Hls.Events.MANIFEST_LOADED, () => {
      // console.log('MANIFEST_LOADED');
      event.onLoad();
    });

    hls.on(Hls.Events.DESTROYING, () => {
      event.onDetached();
    });
    hls.on(Hls.Events.MEDIA_DETACHED, () => {
      event.onDetached();
    });

    // hls.on(Hls.Events.BUFFER_EOS, () => {
    //   hls.destroy();
    //   event.onStreamFinished();
    // });

    hls.on(Hls.Events.ERROR, (_: Events.ERROR, data: ErrorData) => {
      if (data.fatal) {
        errorsHls.setError(data.type, videoSrc);
        switch (data.type) {
          case Hls.ErrorTypes.NETWORK_ERROR:
            /* detail: manifestLoadTimeOut */
            hls.loadSource(videoSrc);
            break;
          case Hls.ErrorTypes.MEDIA_ERROR:
            console.log('fatal media error encountered, try to recover');
            hls.recoverMediaError();
            event.resourceUnavailable();
            break;
          default:
            // console.log('default error');
            hls.destroy();
            event.onStreamFinished();
            break;
      }
      }
      return;
      if (data.fatal) {
        const { type } = data;
        // console.log('getValue ', instanceHls.getValue());
        if (instanceHls.isSetErrorType(type)) {
          // console.log('if', instanceHls.getValue());
          event.onReplaceCodec();
          return;
        } else {
          instanceHls.setError(type);
        }

        switch (data.type) {
          case Hls.ErrorTypes.NETWORK_ERROR:
            // try to recover network error
            console.log('fatal network error encountered, try to recover');
            hls.destroy();
            event.onStreamFinished();
            break;
          case Hls.ErrorTypes.MEDIA_ERROR:
            console.log('fatal media error encountered, try to recover');
            hls.recoverMediaError();
            break;
          default:
            console.log('default error');
            hls.destroy();
            event.onStreamFinished();
            break;
        }
      } else {
        console.log('ERROR', data);
        errorsHls.setError(data.type, videoSrc);
        // setTimeout(hls.startLoad, 0);
        return;
        hls.startLoad();
      }
    });
    // const events = Object.keys(Hls.Events);

    // for (let i = 0; i <= events.length; i++) {
    //   const event = events[i];
    //   hls.on(Hls.Events[event], console.info.bind(console));
    // }
    // console.log('Hls.Events', Hls.Events);
    // Object.keys(Hls.Events).map((e: any) => {
    //   type key = typeof e;
    //   const ev: typeof Events = e;
    //   const Event: any = Hls.Events[e] // as keyof HlsListeners // typeof Events;
    //   hls.on(Event, console.info.bind(console));
    // });
  }

  return {
    htmlElement,
    destroy: hls.destroy.bind(hls),
    loadSource: hls.loadSource.bind(hls),
    stopLoad: hls.stopLoad.bind(hls),
    startLoad: hls.startLoad.bind(hls),
  };
};

export default CorePlayer;
