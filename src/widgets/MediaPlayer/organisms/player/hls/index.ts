import Hls, { HlsConfig } from 'hls.js';

const hls = () => new Hls({
  pLoader: function (config: HlsConfig) {
    let loader = new Hls.DefaultConfig.loader(config);

    this.abort = () => {
      // console.log('abort');
      loader.abort();
    }
    this.destroy = () => {
      // console.log('destroy');
      loader.destroy();
    }
    this.load = (context: any, config: any, callbacks: any) => {
      let { type, url } = context;
      if (type === 'manifest') {
        // console.log(`Manifest ${url} will be loaded.`);
      }

      loader.load(context, config, callbacks);
    };
  },
} as any);

export default hls;
