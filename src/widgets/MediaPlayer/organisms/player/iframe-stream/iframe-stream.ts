export const iframeStream = (src: string, iframeEl: HTMLIFrameElement) => {
  iframeEl.setAttribute('src', src);

  return {
    unmount: () => {
      if (iframeEl instanceof HTMLIFrameElement) {
        iframeEl.setAttribute('src', '');
      }
    }
  };
};
