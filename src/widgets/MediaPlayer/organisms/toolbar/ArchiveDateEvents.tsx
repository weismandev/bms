import { FC, SyntheticEvent } from 'react';
import { fromUnixTime, format, isToday, isYesterday } from 'date-fns';
import { ru, enUS } from 'date-fns/locale';
import { useTranslation } from 'react-i18next';
import { Box, Tabs, Tab, Skeleton } from '@mui/material';

import { getArchiveDateEvents } from '../../models/archive';
import { useArchiveDateEvents } from '../../hooks/useArchiveDateEvents';

import { ArchiveDatesGate } from '../../models/archive/archive-dates.model';

interface Props {
  id: number;
  onClick: (dt: number) => void;
}

const ArchiveDateEvents: FC<Props> = ({ id, onClick }) => {
  const { t } = useTranslation();
  const { isLoading, dates, selectedDate, setActiveDate } = useArchiveDateEvents();

  const locale = localStorage.getItem('lang') === 'ru' ? ru : enUS;

  const selectDay = (_: SyntheticEvent<Element, Event>, unixTime: number) => {
    getArchiveDateEvents(unixTime);
    onClick(unixTime);
    setActiveDate(unixTime);
  };

  const formattedDay = (unixTime: number) => {
    const day = fromUnixTime(unixTime);
    if (isToday(day)) return t('Today');
    if (isYesterday(day)) return t('Yesterday');
    return format(day, 'dd, EEEEEE', { locale });
  };

  return (
    <Box>
      <ArchiveDatesGate id={id} />
      {isLoading && <Skeleton height={48} width={300} />}
      {!isLoading && (
        <Tabs
          value={selectedDate}
          onChange={selectDay}
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          {dates?.map((item) => (
            <Tab key={item} value={item} label={formattedDay(item)} />
          ))}
        </Tabs>
      )}
    </Box>
  );
};

export { ArchiveDateEvents };
