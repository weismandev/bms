import { FC, useRef, useState, useEffect } from 'react';
import { isToday, isFuture, isSameDay } from 'date-fns';
import { format, utcToZonedTime } from 'date-fns-tz';

import Skeleton from '@mui/material/Skeleton';
import IconButton from '@mui/material/IconButton';
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';
import PauseCircleOutlineIcon from '@mui/icons-material/PauseCircleOutline';

import { ControlTimeline } from '../../features/control-timeline';

import { useArchiveDateEvents } from '../../hooks/useArchiveDateEvents';

type Props = {
  isPlay: boolean;
  isLoading: boolean;
  GMT: {
    offset: number;
    timezone: string;
  };
  events: {
    laneId: 'lane-1',
    eventId: string;
    startTimeMillis: number;
    endTimeMillis: number;
  }[];
  selectedDt: number;
  displayMode: 'stream' | 'archive';
  playbackControl: () => void;
  setDisplayMode: (mode: 'stream' | 'archive') => void;
  getArchiveDateEvents: (dt: number) => void;
  getArchiveStreamByEvent: (dt: number) => void;
};

const ControlArchiveBar: FC<Props> = ({
  isPlay,
  isLoading,
  GMT,
  events,
  selectedDt,
  displayMode,
  playbackControl,
  setDisplayMode,
  getArchiveDateEvents,
  getArchiveStreamByEvent
}) => {
  const wrapperEl = useRef<HTMLDivElement>(null);

  const userGMT = new Date().getTimezoneOffset() / 60;

  const [width, setWidth] = useState<number>(1000);
  const [startTimeMillis, setStartTimeMillis] = useState<number>(0);
  const [lastClickedMS, setMs] = useState<number | null>(null);

  const { dates, setActiveDate } = useArchiveDateEvents();

  const selectedDate = utcToZonedTime(new Date(selectedDt * 1000), GMT.timezone);

  const onClick = (ms: number) => {
    if (isFuture(ms)) {
      setStartTimeMillis(ms);
      setMs(ms);
      setDisplayMode('stream');
    }

    const timestamp = Math.floor(ms / 1000);
    const clickedDate = utcToZonedTime(new Date(ms), GMT.timezone);

    if (isSameDay(selectedDate, clickedDate)) {
      setStartTimeMillis(ms);
      getArchiveStreamByEvent(timestamp);
      playbackControl();
    } else {
      const foundedDay = dates.find((dt) => isSameDay(new Date(dt * 1000), clickedDate));
      if (foundedDay) {
        setStartTimeMillis(ms);
        setMs(ms);
        setActiveDate(foundedDay);
        getArchiveDateEvents(foundedDay);
        getArchiveStreamByEvent(timestamp);
        playbackControl();
      }
    }
    if (displayMode !== 'archive') {
      setDisplayMode('archive');
    }
  };

  const onEventClick = (eventId: string) => {
    const { startTimeMillis: ms } = events.find((event) => event.eventId === eventId) || {};
    if (ms) {
      const timestamp = ms / 1000;
      getArchiveStreamByEvent(timestamp);
      setStartTimeMillis(ms);
      playbackControl();
    }
  };

  useEffect(() => {
    if (!isLoading && events.length > 0) {
      if (displayMode === 'stream') {
        const date: Date = utcToZonedTime(Date.now(), GMT.timezone);
        const dt: number = date.getTime();
        setStartTimeMillis(dt);
      } else {
        const [firstEvent] = events;
        setStartTimeMillis(lastClickedMS || firstEvent.startTimeMillis);
        if (lastClickedMS === null) {
          getArchiveStreamByEvent(firstEvent.startTimeMillis / 1000);
        }
      }
    }
  }, [isLoading]);

  useEffect(() => {
    if (wrapperEl.current) {
      const parentNode = wrapperEl.current.parentNode as HTMLElement;
      setWidth(Number(parentNode.clientWidth));
    }
  }, []);

  return (
    <div style={{ display: 'flex', flexDirection: 'row' }} ref={wrapperEl}>
      <div style={{ marginTop: 22, height: 50 }}>
        <IconButton aria-label="play" onClick={playbackControl}>
          {isPlay ? <PauseCircleOutlineIcon /> : <PlayCircleOutlineIcon />}
        </IconButton>
      </div>
      <div style={{ position: 'relative', width: '100%' }}>
        <div style={{ position: 'absolute', width: '100%', top: '-30px' }}>
          {isLoading && <Skeleton height={150} />}
        </div>
        {!isLoading && (
          <ControlTimeline
            startTimeMillis={startTimeMillis}
            selectedDt={selectedDt}
            events={events}
            isLoad={isPlay}
            width={width}
            userGMT={userGMT}
            GMT={GMT}
            onClick={onClick}
            onEventClick={onEventClick}
          />
        )}
      </div>
    </div>
  );
};

export { ControlArchiveBar };
