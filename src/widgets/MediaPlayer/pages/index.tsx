import { FC } from 'react';
import { FilterMainDetailLayout, Wrapper } from '@ui/index';

const PageRoot: FC = () => {
  return (
    <>
      <FilterMainDetailLayout
        main={
          <Wrapper style={{ height: '100%', padding: 24 }}>Wrapper</Wrapper>
        }
        filter={false}
        detail={false}
      />
    </>
  );
};

export { PageRoot };
