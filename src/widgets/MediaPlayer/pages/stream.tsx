import { FC, useState } from 'react';
import { useStore, useGate } from 'effector-react';
import { Loader } from '@ui/index';
import { Player } from '../organisms';
import { CameraUnavailable } from '../features/camera-unavailable';
import {
  PageGate,
  $streams,
  $isLoading,
  updateStreamById,
} from '../models/page';
import { $codecPriority, replaceCodec } from '../models/codec';

import { Stream as IStream } from '../types';

type Props = {
  id: number;
  isArchive?: boolean;
};

const Stream: FC<Props> = ({ id, isArchive }: Props) => {
  useGate(PageGate, { id });
  const isLoading = useStore($isLoading);
  const streams: IStream[] = useStore($streams);
  const codecPriority = useStore($codecPriority);
  const [isAttemptUsed, toggleAttemptUsed] = useState(false);

  const handleStreamFinished = (streamId: number) => (loadSource: any) => {
    if (!isAttemptUsed) {
      updateStreamById(streamId);
      toggleAttemptUsed(true);

      const { stream } = streams.find((item: IStream) => item.id === streamId) || {};
      if (stream) {
        loadSource(stream.url);
      }
    }
  };

  const handleReplaceCodec = (streamId: number) => () => {
    replaceCodec(streamId);
  };

  const isLoadingStream = !Object.keys(streams).length;

  return (
    <>
      <Loader isLoading={isLoading} />
      {!isLoading && isLoadingStream && (
        <CameraUnavailable />
      )}
      {!isLoadingStream &&
        streams.map((stream: IStream) => (
          <Player
            key={stream.id}
            id={stream.id}
            isAvailable={stream.isAvailable}
            isArchive={isArchive}
            defaultControls
            controls={{ fullscreen: true }}
            videoSrc={stream?.stream?.url}
            codec={codecPriority[stream.id]}
            preview={stream?.stream?.preview}
            onLoad={() => {}}
            onStreamFinished={handleStreamFinished(stream.id)}
            onReplaceCodec={handleReplaceCodec(stream.id)}
          />
        ))}
    </>
  );
};

export { Stream };
