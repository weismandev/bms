import { FC, useEffect, useState, useCallback } from 'react';
import { useStore } from 'effector-react';
import { Loader } from '@ui/index';
import { Player } from '../organisms';
import { ControlArchiveBar } from '../organisms/control-archive-bar';
import {
  ArchiveGate,
  $loading,
  $stream,
  $eventSlots,
  $selectedDate,
  getArchiveDateEvents,
  getArchiveStreamByEvent,
} from '../models/archive';
import { $GMT, $displayMode, setDisplayMode } from '../models/page';

import { Stream } from './stream';

type Props = {
  id: number;
  dt: number;
};

const ArchivalStream: FC<Props> = ({ id, dt }) => {
  const { stream = { url: '' } } = useStore($stream);
  const {
    videoarchive: isLoading,
    events: isLoadingEvents,
  } = useStore($loading);
  const eventSlots = useStore($eventSlots);
  const displayMode = useStore($displayMode);
  const selectedDate = useStore($selectedDate);
  const GMT = useStore($GMT);

  const [isPlay, setIsPlay] = useState<boolean>(false);

  const events = eventSlots.map((event) => ({
    laneId: 'lane-1',
    eventId: `event-${event.id}`,
    startTimeMillis: event.timestamp * 1000,
    endTimeMillis: (event.timestamp + event.duration) * 1000,
  }));

  const playbackControl = useCallback(() => {
    setIsPlay(!isPlay);
  }, [isPlay]);

  const handleLoad = (isLoad = true) => {
    setIsPlay(isLoad);
  };

  useEffect(() => {
    if (typeof id === 'number' && typeof dt === 'number') {
      if (dt > 0) {
        getArchiveDateEvents(dt);
      }
    }
  }, [id, dt]);

  useEffect(() => {
    setDisplayMode('archive');
  }, []);

  return (
    <>
      <ArchiveGate id={id} />
      <Loader isLoading={isLoading} />
      {displayMode === 'stream' ? (
        <Stream id={id} isArchive />
      ) : (
        <Player
          id={id}
          videoSrc={stream?.url || ''}
          codec={stream?.codec || 'hls'}
          isPlay={isPlay}
          onLoad={handleLoad}
          isArchive
        />
      )}
      <ControlArchiveBar
        isPlay={isPlay}
        isLoading={isLoadingEvents}
        GMT={GMT}
        events={events}
        selectedDt={selectedDate}
        displayMode={displayMode}
        playbackControl={playbackControl}
        setDisplayMode={setDisplayMode}
        getArchiveDateEvents={getArchiveDateEvents}
        getArchiveStreamByEvent={getArchiveStreamByEvent}
      />
    </>
  );
};

export { ArchivalStream };
