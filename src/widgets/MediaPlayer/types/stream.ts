import { CodecFormat } from '../models/codec/codec.types';

export type Line = 240 | 360 | 720 | 1080;

export type Stream = {
  id: number;
  title: string;
  isAvailable: boolean;
  stream: {
    codec: CodecFormat[];
    line: Line;
    url: string;
    preview?: string;
  };
};

export type Streams = {
  url: string;
  line: Line;
  codec: CodecFormat;
  sound: number;
};

export type PayloadStreams = {
  id: number;
  hls?: number;
  limit?: number;
};

export type StreamsInfo = {
  codec: CodecFormat;
  mjpeg_new: string;
  videourl: string;
  preview: string;
  description: null | string;
  ratio: number;
  main: number;
  id: number;
  ext_id?: null | number;
  title: string;
  available: boolean;
  has_archive: boolean;
  has_ptz: boolean;
  fullscreen: boolean;
  timezone: 'GMT-';
  type: 'rts';
  codec_priority: CodecFormat[];
  streams: Streams[];
  url_archive: string;
};

export type ResponseStreams = {
  camera: StreamsInfo[];
  meta: {
    count: number;
    limit: number;
    offset: number;
    total: number;
  };
};
