import { CodecPriorities, CodecFormat } from '../models/codec/codec.types';
import { Stream } from '../models/streams/streams.types';

export type SliceStream = {
  id: number;
  stream: Stream;
};

export type PayloadArchive = {
  id: number;
  dt?: any;
  url?: string;
  platform?: string;
};

export type OriginalStreams = {
  codec: string;
  container: string;
  line: number;
  sound: number;
  url: string;
};

export type RawArchiveStream = {
  id: number;
  codec_priority: CodecFormat[];
  streams?: Stream[];
};

export type ResponseArchiveStream = {
  data: RawArchiveStream;
  error: number;
  message: string;
};

export type Dates = {
  dates: number[];
};

export type ResponseArchiveDates = {
  data: Dates;
};

export type Event = {
  duration: number;
  id: number;
  timestamp: number;
};

export type EventGridStep = {
  hours: number;
  minutes: number;
  seconds: number;
};

export type EventSlots = Event & {
  timeSlotStart: EventGridStep;
  timeSlotEnd: EventGridStep;
};

export type EventGrid = Event & {
  gridColumnStart: number;
  gridColumnEnd: number;
};

export type ResponseArchiveDateEvents = {
  data: {
    events: Event[];
  };
};
