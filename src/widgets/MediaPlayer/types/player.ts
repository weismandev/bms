import { CodecFormat } from '../models/codec/codec.types';

export type PlayerProps = {
  id: number;
  codec: CodecFormat;
  videoSrc: string;
  isAvailable: boolean;
  isArchive?: boolean;
  autoPlay?: boolean;
  controls?: Controls | boolean;
  defaultControls?: boolean;
  preview?: string;
  isPlay?: boolean | 'auto';
  onLoad: (isLoading: boolean) => void;
  onDetached?: (streamId: number) => void;
  onStreamFinished?: (streamId: number) => void;
  onReplaceCodec?: () => void;
  onError?: void;
  onStop?: void;
};

export type StyledProps = {
  controls: Controls;
  isArchive: boolean;
};

export type Controls = {
  fullscreen?: boolean;
};

export type PlayerElement =
  | HTMLVideoElement
  | HTMLImageElement
  | HTMLIFrameElement;

export type TagName = 'video' | 'img' | 'iframe';
