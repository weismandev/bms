import { useStore } from 'effector-react';
import {
  $isLoading,
  $dates,
  $selectedDate,
  selectDate,
} from '../models/archive/archive-dates.model';

const useArchiveDateEvents = (): {
  isLoading: boolean;
  dates: number[];
  selectedDate: number;
  setActiveDate: (unixTime: number) => void;
} => {
  const dates = useStore($dates);
  const isLoading = useStore($isLoading);
  const selectedDate = useStore($selectedDate);

  return {
    isLoading,
    dates,
    selectedDate,
    setActiveDate: selectDate,
  };
};

export { useArchiveDateEvents };
