import { useRef, useEffect } from 'react';

function useSubscriber(src: string) {
  const previousSrcRef = useRef<string>();
  const previous = previousSrcRef.current;

  useEffect(() => {
    previousSrcRef.current = src;
  }, [src]);

  return { previous, src };
}

export { useSubscriber };
