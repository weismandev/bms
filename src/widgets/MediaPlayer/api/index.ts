import { api } from '@api/api2';
import { API_URL } from '@api/api';
import { getCookie } from '@tools/cookie';
import {
  PayloadStreams,
  ResponseStreams,
  PayloadArchive,
  ResponseArchiveStream,
  ResponseArchiveDates,
  ResponseArchiveDateEvents,
} from '../types';

export const getCamerasList = (payload: PayloadStreams): ResponseStreams =>
  api.no_vers('get', 'akado/get-stream-v2', payload);

export const getArchiveDates = async ({
  url = API_URL,
  ...payload
}: PayloadArchive): Promise<ResponseArchiveDates> => {
  const token = getCookie('bms_token');
  const params = `?id=${payload.id}&token=${token}&platform=${payload.platform}`;
  const response = await fetch(
    `${url}/videoarchive/get-archive-dates/${params}`
  );
  return response.json();
};

export const getArchiveDateEvents = async ({
  url = API_URL,
  ...payload
}: PayloadArchive): Promise<ResponseArchiveDateEvents> => {
  const token = getCookie('bms_token');
  const params = `?id=${payload.id}&dt=${payload.dt}&token=${token}`;
  const response = await fetch(
    `${url}videoarchive/get-archive-date-events/${params}`
  );
  return response.json();
};

export const getArchiveStream = async ({
  url = API_URL,
  ...payload
}: PayloadArchive): Promise<ResponseArchiveStream> => {
  const token = getCookie('bms_token');
  const params = `?id=${payload.id}&dt=${payload.dt}&token=${token}&hls=1&timezone=${payload.timezone}`;
  const response = await fetch(
    `${url}videoarchive/get-archive-stream/${params}`
  );
  return response.json();
};

export const streamApi = {
  getArchiveDates,
  getArchiveDateEvents,
  getArchiveStream,
};
