import { getNameUserPlatform } from '@tools/getNameUserPlatform';
import { PayloadArchive, PayloadStreams } from '../types';

export default function userPlatformToPayload(
  payload: PayloadArchive | PayloadStreams
) {
  return {
    ...payload,
    platform: getNameUserPlatform(),
  };
}
