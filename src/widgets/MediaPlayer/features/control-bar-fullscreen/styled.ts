import styled from '@emotion/styled';

export const StyledTooltip = styled.div`
  display: none;
  background-color: rgba(97, 97, 97, 0.92);
  border-radius: 4px;
  color: rgb(255, 255, 255);
  padding: 4px 8px;
  position: absolute;
  white-space: nowrap;
  bottom: 35px;
  right: 12px;
`;
