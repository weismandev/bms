import { FC, useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import IconButton from '@mui/material/IconButton';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit';

import { StyledTooltip } from './styled';

type Props = {
  videoNode: HTMLDivElement | null;
};

let tooltipTimeout: null | ReturnType<typeof setTimeout> = null;

export const ControlBarFullscreen: FC<Props> = ({ videoNode }) => {
  const { t } = useTranslation();
  const tooltipRef = useRef<HTMLDivElement>(null);
  const [isFullscreen, toggleFullscreen] = useState<boolean>(false);

  const hideTooltip = () => {
    if (tooltipRef.current !== null) {
      tooltipRef.current.style.display = 'none';
    }
  };

  const showTooltip = () => {
    if (tooltipTimeout !== null) {
      clearTimeout(tooltipTimeout);
    }
    if (tooltipRef.current !== null) {
      const label = isFullscreen ? t('ExitFullscreen') : t('Fullscreen');
      tooltipRef.current.textContent = label;
      tooltipRef.current.style.display = 'block';
      tooltipTimeout = setTimeout(hideTooltip, 4000);
    }
  };

  const handleNotSupported = () => {
    // eslint-disable-next-line no-alert
    alert(t('WeSorryYourBrowserDoesNotSupportFullscreen'));
  };

  const openFullscreen = () => {
    if (document.fullscreenElement) {
      document.exitFullscreen()
        .catch(handleNotSupported)
        .finally(hideTooltip);
    } else if (videoNode instanceof HTMLElement) {
      videoNode.requestFullscreen()
        .catch(handleNotSupported)
        .finally(hideTooltip);
    }
  };

  const handleFullscreenchanged = () => {
    toggleFullscreen(!!document.fullscreenElement);
  };

  useEffect(() => {
    document.addEventListener('fullscreenchange', handleFullscreenchanged);
    return () => {
      document.removeEventListener('fullscreenchange', handleFullscreenchanged);
    };
  }, []);

  const renderIcon = isFullscreen
    ? <FullscreenExitIcon />
    : <FullscreenIcon />;

  return (
    <>
      <IconButton
        aria-label="fullscreen"
        style={{ color: 'white', position: 'absolute', right: 0, bottom: 0, zIndex: 2 }}
        onClick={openFullscreen}
        onMouseOver={showTooltip}
        onMouseLeave={hideTooltip}
      >
        {renderIcon}
      </IconButton>
      <StyledTooltip ref={tooltipRef} />
    </>
  );
};