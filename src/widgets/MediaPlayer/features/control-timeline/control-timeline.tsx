import { useState, useEffect, lazy } from 'react';
import { isToday } from 'date-fns';
import { format, utcToZonedTime } from 'date-fns-tz';

import IconButton from '@mui/material/IconButton';
import ZoomInIcon from '@mui/icons-material/ZoomIn';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';

import { Timeline, ZoomLevels } from 'react-player-timeline';

type Props = {
  startTimeMillis: number;
  selectedDt: number;
  events: {
    laneId: string;
    eventId: string;
    startTimeMillis: number;
    endTimeMillis: number;
  }[];
  isLoad: boolean;
  width: number;
  userGMT: number;
  GMT: {
    offset: number;
    timezone: string;
  };
  onClick: (ms: number) => void;
  onEventClick: (eventId: string) => void;
};

const laneId = 'lane-1';
const lanes = [
  {
    laneId,
    label: '',
  },
];

const dateFormat = (offset: string) => (ms: number) => {
  const date = new Date(ms);
  return date instanceof Date && !Number.isNaN(date.getTime())
    ? format(utcToZonedTime(date, offset), 'HH:mm')
    : '';
};

const controls = {
  zoomIn: (props: any) => (
    <IconButton aria-label="zoomOut" {...props}>
      <ZoomInIcon />
    </IconButton>
  ),
  zoomOut: (props: any) => (
    <IconButton aria-label="zoomOut" {...props}>
      <ZoomOutIcon />
    </IconButton>
  ),
};

export const ControlTimeline = ({
  startTimeMillis,
  selectedDt,
  events,
  isLoad,
  width,
  userGMT,
  GMT,
  onClick,
  onEventClick
}: Props) => {
  const [isPlay, setIsPlay] = useState<boolean>(false);

  const startDate: Date = utcToZonedTime(new Date(selectedDt * 1000), GMT.timezone);
  startDate.setHours(0, 0, 0, 0);
  const endDate: Date = utcToZonedTime(new Date(selectedDt * 1000), GMT.timezone);

  if (isToday(selectedDt * 1000) && events.length > 0) {
    const lastEvent = events.at(-1) || { endTimeMillis: new Date() };
    const date: Date = utcToZonedTime(new Date(lastEvent.endTimeMillis), GMT.timezone);
    endDate.setHours(date.getHours(), date.getMinutes(), date.getSeconds());
  } else {
    endDate.setHours(23, 59, 59);
  }

  const diffTimezone = GMT.offset + userGMT;
  const diffTimeStamp = diffTimezone * 3600 * 1000;
  const startCustomDomain = startDate.getTime() - diffTimeStamp;
  const endCustomDomain = endDate.getTime() - diffTimeStamp;

  useEffect(() => {
    setIsPlay(isLoad);
  }, [isLoad]);

  return (
    <Timeline
      offset={GMT.timezone}
      isPlay={isPlay}
      width={width - 100}
      height={80}
      events={events}
      customRange={[startCustomDomain, endCustomDomain]}
      lanes={lanes}
      startTimeMillis={startTimeMillis}
      dateFormat={dateFormat(GMT.timezone)}
      zoomLevels={[ZoomLevels.SIX_HOURS, ZoomLevels.THREE_HOURS]}
      controls={controls}
      onClick={onClick}
      onEventClick={onEventClick}
    />
  );
};
