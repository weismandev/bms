import { forwardRef } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';

import { $isUnavailable, $debugInfo } from '../../models/resource-unavailable';

const Alert = forwardRef<HTMLDivElement, AlertProps>((
  props,
  ref,
) => (
  <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />
  ));

export const ResourceUnavailable = () => {
  const { t } = useTranslation();
  const isUnavailable = useStore($isUnavailable);
  const debugInfo = useStore($debugInfo);

  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Snackbar
        open={isUnavailable}
        autoHideDuration={6000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        style={{ top: 72 }}
      >
        <Alert severity="error">{debugInfo?.message ?? t('ResourceIsNotAvailable')}</Alert>
      </Snackbar>
    </Stack>
  )
};
