import { useTranslation } from 'react-i18next';
import { Tooltip } from '@mui/material';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import {
  StyledCard,
  StyledCardContent,
  StyledVisibilityOffIcon,
  StyledIconButton
} from './styled';

export const CameraUnavailable = () => {
  const { t } = useTranslation();
  return (
    <StyledCard>
      <Tooltip title={t('CameraNotAvailable')} followCursor>
        <StyledCardContent>
          <StyledVisibilityOffIcon />
          <StyledIconButton disabled><FullscreenIcon /></StyledIconButton>
        </StyledCardContent>
      </Tooltip>
    </StyledCard>
  );
};
