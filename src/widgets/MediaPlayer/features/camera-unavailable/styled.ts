import styled from '@emotion/styled';
import { Card, CardContent, IconButton } from '@mui/material';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

export const StyledCard = styled(Card)`
  margin: 0px 12px;
  background: rgb(255,255,255);
  background: linear-gradient(0deg, rgba(247,247,247) 0%, rgba(255,255,255) 40%);
`;

export const StyledCardContent = styled(CardContent)`
  text-align: center;
  position: relative;
  padding: 48px;

  &:last-child {
    padding-bottom: 48px;
  }
`;

export const StyledVisibilityOffIcon = styled(VisibilityOffIcon)`
  font-size: 6rem;
  color: #bccbde;
`;

export const StyledIconButton = styled(IconButton)`
  position: absolute;
  bottom: 0;
  right: 0;
`;
