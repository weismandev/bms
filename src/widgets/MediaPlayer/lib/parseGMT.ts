/**
 * Парсинг таймзоны с бэка
 * @param {string} timezone может быть следующего вида GMT(+)(-)Смещение,
 * если смещения нет, то возвращаю ноль
 * @return {Object}
*/

export const parseGMT = (rawTimezone: string): {
  offset: number;
  timezone: string;
} => {
  const [, rawOffset] = rawTimezone.split('GMT');
  const offsetToNumber = Number(rawOffset);
  const offset: number = Number.isNaN(offsetToNumber) ? 0 : offsetToNumber;

  /* Узнаю количество цифр */
  const lenOffset: number = String(offset).match(/\d/g)?.length || 0;
  /* Вычитаю цифры, что бы при конкатенации стало два числа */
  const addMissingNumber = 2 - lenOffset;
  const formatTwoDigitHour = Array(addMissingNumber).fill(0).join('');
  const offsetSign = offsetToNumber < 0 ? '-' : '+';
  const timezone = `${offsetSign}${formatTwoDigitHour}${offset}:00`;
  return {
    offset,
    timezone,
  };
};
