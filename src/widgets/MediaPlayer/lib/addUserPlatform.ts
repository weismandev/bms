/**
 * Добавление свойства платформы пользователя
 *  @returns {Object} platform The connection state.
 *  @returns {string} platform.platform - The X Coordinate
 * * */

import { getNameUserPlatform } from '@tools/getNameUserPlatform';

export const addUserPlatform = (): {
  platform: string;
} => ({
  platform: getNameUserPlatform(),
});
