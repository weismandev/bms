import { Event } from '../types';

class EventsIterator implements IterableIterator<Event> {
  pointer = 0;

  events: Event[] = [];

  setEvents(events: Event[]) {
    this.pointer = 0;
    this.events = events;
  }

  setPointerById(id: number) {
    const index = this.events.findIndex((event) => event.id === id) || 0;
    this.pointer = index;
  }

  next(): IteratorResult<Event> {
    if (this.pointer < this.events.length) {
      return {
        done: false,
        value: this.events[this.pointer++],
      };
    }
    return {
      done: true,
      value: null,
    };
  }

  [Symbol.iterator](): IterableIterator<Event> {
    return this;
  }
}

export default new EventsIterator();
