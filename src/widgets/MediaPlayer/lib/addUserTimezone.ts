/**
 * Добавление свойства таймозоны в формате GMT+0
 * @return {object}
 * * */

export const addUserTimezone = (): {
  timezone: string;
} => ({
  timezone: new Date().toTimeString().slice(9).split(' ')[0].split('+').join('%2B')
});
