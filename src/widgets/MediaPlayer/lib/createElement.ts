import { PlayerElement, TagName } from '../types';

const createElement = (
  type: TagName,
  isControls: boolean,
  preview: string,
  width: number
): PlayerElement => {
  const htmlElement = document.createElement(type);
  htmlElement.style.height = '100%';
  htmlElement.style.width = '100%';
  htmlElement.style.objectFit = 'contain';
  htmlElement.setAttribute('poster', preview);
  htmlElement.setAttribute('disablePictureInPicture', 'true');
  htmlElement.setAttribute('controlsList', 'nodownload noplaybackrate');

  if (isControls) {
    htmlElement.setAttribute('controls', '');
  }
  return htmlElement;
};

export { createElement };
