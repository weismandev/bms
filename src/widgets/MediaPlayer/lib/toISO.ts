// Избавляемся от локальной таймзоны
export const toISO = (date: Date) => date.toISOString().slice(0, -1);
