import PropTypes from 'prop-types';
import styles from './style.module.css';

function TwoColumnTemplate({ sectionTitle, children }) {
  return <div className={styles.container}>{children} </div>;
}

TwoColumnTemplate.propTypes = {
  children: PropTypes.node.isRequired,
};

export default TwoColumnTemplate;
