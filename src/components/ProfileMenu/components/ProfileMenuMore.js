import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useStore } from 'effector-react';
import {
  ExitToAppOutlined,
  MoreHoriz,
  ErrorOutline,
  Person,
  HelpOutline,
} from '@mui/icons-material';
import { $sectionsData } from '@features/common';
import i18n from '@shared/config/i18n';
import { Popover, Menu, IconButton } from '@ui/';
import ProfileMenuInfo from './ProfileMenuInfo';
import ProfileMenuLogout from './ProfileMenuLogout';

const { t } = i18n;

const ProfileMenuMore = () => {
  const [modalType, setModalType] = useState('');
  const history = useHistory();
  const sectionsData = useStore($sectionsData);
  const manual = sectionsData?.manual;

  const closeModal = () => {
    setModalType('');
  };

  const moreItems = [
    {
      title: t('profile'),
      icon: <Person />,
      onClick: () => history.push('/profile'),
    },
    {
      title: t('Instruction'),
      icon: <HelpOutline />,
      onClick: () => window.open(manual),
    },
    {
      title: t('AboutTheSystem'),
      icon: <ErrorOutline />,
      onClick: () => setModalType('info'),
    },
    {
      title: t('Logout'),
      icon: <ExitToAppOutlined />,
      onClick: () => setModalType('logout'),
    },
  ];

  return (
    <>
      <Popover
        trigger={
          <IconButton title={t('Additionally')} size="large">
            <MoreHoriz />
          </IconButton>
        }
      >
        <Menu items={moreItems} />
      </Popover>

      <ProfileMenuInfo isOpen={modalType === 'info'} onClose={closeModal} />
      <ProfileMenuLogout isOpen={modalType === 'logout'} onClose={closeModal} />
    </>
  );
};

export default ProfileMenuMore;
