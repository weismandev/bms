import { useStore, useList } from 'effector-react';
import { Badge, MenuList, Button } from '@mui/material';
import { Notifications } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import {
  $notifications,
  $unreadCount,
  fxSetAllAsReadNotifications,
  $isUserLoading,
} from '@features/common';
import i18n from '@shared/config/i18n';
import { Popover, IconButton, CustomScrollbar, Loader, Empty } from '@ui/index';
import NotificationItem from './NotificationItem';

const { t } = i18n;

const useStyles = makeStyles((theme) => {
  const { palette, status } = theme;
  const badgeColor = status.button.negative.default;

  return {
    badge: {
      background: '#1C90FB',
      color: palette.getContrastText(badgeColor),
      right: -7,
      top: -2,
      width: 27,
      height: 20,
      minWidth: 13,
      minHeight: 13,
      fontSize: 12,
    },
    menu: {
      zIndex: 25,
      maxHeight: 340,
      padding: 0,
    },
    button: {
      borderRadius: 11.25,
    },
    notificationsIcon: {
      color: '#F50057',
    },
  };
});

const ProfileMenuNotifications = () => {
  const unreadCount = useStore($unreadCount);
  const isLoading = useStore($isUserLoading);
  const classes = useStyles();

  const menuItems = useList($notifications, (i) => (
    <NotificationItem
      datetime={i.sent_at}
      description={i.body}
      event={i.event}
      title={i.title}
      data={i.data}
      isRead={Boolean(i.read_at)}
    />
  ));

  const renderContent = () => {
    if (isLoading) return <Loader isLoading fixed />;
    if ($notifications.length === 0 && !isLoading) return <Empty />;
    return (
      <>
        <MenuList classes={{ root: classes.menu }}>
          <CustomScrollbar
            style={{ height: '315px', maxHeight: '400px', width: '500px' }}
          >
            {menuItems}{' '}
          </CustomScrollbar>
        </MenuList>

        <Button
          onClick={fxSetAllAsReadNotifications}
          color="primary"
          classes={{ root: classes.button }}
          fullWidth
        >
          {t('MarkAllRead')}
        </Button>
      </>
    );
  };

  return (
    <Popover
      trigger={
        <IconButton title={t('Notifications')} size="large">
          <Badge
            classes={{ badge: classes.badge }}
            badgeContent={unreadCount}
            max={99}
            style={{ zIndex: 0 }}
          >
            <Notifications className={classes.notificationsIcon} />
          </Badge>
        </IconButton>
      }
    >
      {renderContent()}
    </Popover>
  );
};

export default ProfileMenuNotifications;
