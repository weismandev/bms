import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Paper } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import ProfileMenuMore from './components/ProfileMenuMore';
import ProfileMenuNotifications from './components/ProfileMenuNotifications';

const useStyles = makeStyles((theme) => {
  return {
    paper: {
      background: 'none',
      boxShadow: 'none',
      color: '#65657B',
      display: 'grid',
      gridTemplateColumns: 'auto auto',
      gap: 8,
      alignItems: 'center',
      justifyContent: 'end',
      justifyItems: 'end',
      justifySelf: 'end',
    },
    navLink: {
      color: 'inherit !important',
      padding: '0 5px',
      fontWeight: 700,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    buttons: {
      display: 'flex',
      alignItems: 'center',
      paddingLeft: 24,
    },
  };
});

function ProfileMenu() {
  const classes = useStyles();

  return (
    <Paper classes={{ root: classes.paper }}>
      <ProfileMenuNotifications />
      <ProfileMenuMore />
    </Paper>
  );
}

export default connect(null, null)(withRouter(ProfileMenu));
