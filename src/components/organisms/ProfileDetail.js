import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { ActionButton, CustomScrollbar, AvatarControl } from '../../ui';
import FormSectionHeader from '../atoms/FormSectionHeader';
import Toolbar from '../atoms/Toolbar';
import EmailInput from '../molecules/EmailInput';
import FullnameInput from '../molecules/FullnameInput';
import PasswordInput from '../molecules/PasswordInput';
import PhoneInput from '../molecules/PhoneInput';

function UserForm(props) {
  const { t } = useTranslation();
  const { handleSubmit, handleReset, setFieldValue } = props;

  return (
    <form className="profile-form" autoComplete="off">
      <div className="profile-form__face">
        <FormSectionHeader
          header={t('ProfilePage.Photo')}
          addStyles={{ marginBottom: '50px', marginTop: '15px' }}
        />
        <Field
          name="user_avatar"
          render={({ field }) => (
            <AvatarControl
              encoding="base64"
              mode="edit"
              onChange={(value) => setFieldValue(field.name, value)}
              isModifyResult={false}
              value={field.value}
              actionTitle={t('ProfilePage.Photo')}
            />
          )}
        />
      </div>
      <CustomScrollbar
        style={{ height: '100%' }}
        trackVerticalStyle={{ right: '0px' }}
        autoHide
      >
        <div className="profile-form__detail">
          <div style={{ display: 'flex', marginTop: '15px' }}>
            <FormSectionHeader
              header={t('ProfilePage.PersonalData')}
              addStyles={{ marginBottom: '50px' }}
            />
          </div>

          <Field component={FullnameInput} name="user_fullname" />

          <FormSectionHeader header={t('ProfilePage.ChangePassword')} />
          <Field
            component={PasswordInput}
            name="oldpass"
            label={t('ProfilePage.EnterOldPassword')}
          />
          <Field
            component={PasswordInput}
            name="newpass"
            label={t('ProfilePage.EnterNewPassword')}
          />
          <Field
            component={PasswordInput}
            name="newPasswordConfirm"
            label={t('ProfilePage.RepeatNewPassword')}
          />

          <FormSectionHeader header={t('ProfilePage.ContactInformation')} />
          <Field component={EmailInput} name="user_mail" />
          <Field component={PhoneInput} name="user_phone" />

          <Toolbar style={{ height: 82, justifyContent: 'flex-end' }}>
            <ActionButton
              kind="positive"
              style={{ marginRight: '10px' }}
              onClick={handleSubmit}
            >
              {t('Save')}
            </ActionButton>
            <ActionButton
              kind="negative"
              onClick={(e) => {
                e.preventDefault();
                handleReset();
              }}
            >
              {t('Reset')}
            </ActionButton>
          </Toolbar>
        </div>
      </CustomScrollbar>
    </form>
  );
}

export default UserForm;
