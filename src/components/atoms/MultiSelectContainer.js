import { components } from 'react-select';
import MultiValue from './MultiSelectValue';

const SelectContainer = (props) => {
  const { getValue } = props;
  return (
    <>
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          margin: '0',
          overflowX: 'hidden',
          width: '100%',
          paddingRight: '6px',
        }}
      >
        {getValue().map((options) => (
          <MultiValue
            {...props}
            components={{
              Container: components.MultiValueContainer,
              Label: components.MultiValueLabel,
              Remove: components.MultiValueRemove,
            }}
            key={props.selectProps.getOptionValue(options)}
            removeProps={{
              onClick: () => props.selectOption(options),
              onTouchEnd: () => props.selectOption(options),
              onMouseDown: (e) => {
                e.preventDefault();
                e.stopPropagation();
              },
            }}
            isFocused={false}
          >
            {props.selectProps.getOptionLabel(options)}
          </MultiValue>
        ))}
      </div>
      <components.SelectContainer {...props} />
    </>
  );
};

export default SelectContainer;
