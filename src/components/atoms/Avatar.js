import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';
import withStyles from '@mui/styles/withStyles';

const styles = {
  avatar: {
    width: '17vw',
    height: '17vw',
    minWidth: '190px',
    minHeight: '190px',
    background: '#f7f7fc',
    marginTop: '22px',
  },
};

function ImageAvatars({ classes, alt = 'Аватар', src }) {
  return (
    <Grid container justifyContent="center" alignItems="center">
      <Avatar alt={alt} src={src} className={classes.avatar} />
    </Grid>
  );
}

export default withStyles(styles)(ImageAvatars);
