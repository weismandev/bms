import { useRef } from 'react';
import classNames from 'classnames';
import Input from '@mui/material/Input';
import withStyles from '@mui/styles/withStyles';
import i18n from '@shared/config/i18n';
import { ActionButton, CloseButton } from '../../ui';

const { t } = i18n;

const styles = {
  root: {
    display: 'none',
  },
};

function HideFileInput(props) {
  const {
    classes,
    onChange,
    labelOnly,
    label,
    field,
    name = 'file',
    showStatus = false,
    encode = 'base64',
    Component = ActionButton,
    form,
    clearInEnd,
  } = props;

  const inputElement = useRef(null);

  const handleChange = async (e, callback) => {
    const file = e.target.files[0];
    const name = e.target.name;

    const reader = new FileReader();
    if (!(file instanceof Blob)) return;

    if (encode === 'base64') reader.readAsDataURL(file);
    else callback(name, file);

    reader.addEventListener('load', (event) => {
      const result = event.target.result;
      callback(name, encode !== 'base64' ? new DataView(result) : result);
    });
  };

  const inputId = `input${Number(new Date())}`;

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Input
        id={inputId}
        inputRef={inputElement}
        classes={{
          root: classNames(classes.root),
        }}
        name={(field && field.name) || name}
        inputProps={{
          type: 'file',
        }}
        onChange={(e) => {
          handleChange(e, onChange);

          if (clearInEnd) {
            inputElement.current.value = '';
          }
        }}
      />
      {!labelOnly ? (
        <label htmlFor={inputId}>
          <Component component="span"> {label} </Component>
        </label>
      ) : null}
      {showStatus && field.value ? (
        <span style={{ marginTop: '10px' }}>
          {t('FileUploaded')}
          <span className="text-blue">{` ${field.value.name}`}</span>
          <CloseButton
            className="text-red"
            style={{ marginLeft: '20px' }}
            onClick={() => props.form.setFieldValue(field.name, null)}
          />
        </span>
      ) : null}
      {form && form.errors[field.name] ? (
        <div style={{ color: '#EB5757', alignSelf: 'flex-start' }}>
          {form && form.errors[field.name]}
        </div>
      ) : null}
    </div>
  );
}

export default withStyles(styles)(HideFileInput);
