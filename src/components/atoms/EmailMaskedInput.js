import { forwardRef } from 'react';
import MaskedInput from 'react-text-mask';
import emailMask from 'text-mask-addons/dist/emailMask';

const EmailMaskedInput = forwardRef((props, ref) => (
  <MaskedInput {...props} mask={emailMask} showMask />
));

export default EmailMaskedInput;
