import { forwardRef } from 'react';
import MaskedInput from 'react-text-mask';

const PhoneMaskedInput = forwardRef((props, ref) => (
  <MaskedInput
    {...props}
    keepCharPositions={true}
    mask={[
      '+',
      '7',
      ' ',
      '(',
      /\d/,
      /\d/,
      /\d/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
    ]}
  />
));

export default PhoneMaskedInput;
