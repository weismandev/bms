function GreedyElement(props) {
  return <div {...props} style={{ flexGrow: '13' }} />;
}

export default GreedyElement;
