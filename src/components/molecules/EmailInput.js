import { useTranslation } from 'react-i18next';
import EmailMaskedInput from '../atoms/EmailMaskedInput';
import FormSection from './FormSection';

function EmailInput(props) {
  const { t } = useTranslation();
  return (
    <FormSection
      label="E-mail"
      divider
      placeholder={t('ProfilePage.EnterEmail')}
      inputComponent={EmailMaskedInput}
      {...props}
    />
  );
}

export default EmailInput;
