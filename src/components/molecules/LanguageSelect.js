import { useTranslation } from 'react-i18next';
import i18n from '../../shared/config/i18n';
import { SelectControl, SelectField } from '../../ui';

const optionsLanguage = [
  {
    id: 'en',
    title: 'EN',
  },
  {
    id: 'ru',
    title: 'RU',
  },
];

function LanguageSelect(props) {
  const { t } = useTranslation();
  const onChange = (value, field) => {
    i18n.changeLanguage(value.id);
    props.form.setFieldValue(field.name, value);
  };
  return (
    <SelectField
      label={t('ProfilePage.ChangeLanguage')}
      placeholder={t('ProfilePage.ChooseYourPreferredLang')}
      component={<SelectControl options={optionsLanguage} />}
      onChange={onChange}
      {...props}
    />
  );
}

export default LanguageSelect;
