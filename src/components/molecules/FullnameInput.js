import { useTranslation } from 'react-i18next';
import FormSection from './FormSection';

function FullnameInput(props) {
  const { t } = useTranslation();
  return (
    <FormSection
      label={t('Label.FullName')}
      placeholder={t('Placeholder.EnterFullName')}
      divider
      {...props}
    />
  );
}

export default FullnameInput;
