import { useTranslation } from 'react-i18next';
import FormSection from './FormSection';

function PasswordInput(props) {
  const { t } = useTranslation();
  return (
    <FormSection placeholder={t('Label.Password')} type="password" divider {...props} />
  );
}

export default PasswordInput;
