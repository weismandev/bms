import i18n from '@shared/config/i18n';
import PhoneMaskedInput from '../atoms/PhoneMaskedInput';
import FormSection from './FormSection';

const { t } = i18n;

function PhoneInput(props) {
  return (
    <FormSection
      label={t('Label.phone')}
      placeholder="+7(999)888-77-66"
      inputComponent={PhoneMaskedInput}
      divider
      {...props}
    />
  );
}

export default PhoneInput;
