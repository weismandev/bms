import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl as Select, SelectField } from '@ui/index';
import { useOptions } from './hooks/useOptions';
import { fxGetList, $list, $isLoading, $error } from './model';

const ComboListControl = ({ name, url, ...rest }) => {
  const list = useStore($list);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const { data, fetchOptionsBySearch } = useOptions({
    baseUrl: url,
    params: rest.params,
  });

  const options = data.list ? data.list : list[name];

  useEffect(() => {
    fxGetList({ url, name });
  }, []);

  return (
    <Select
      isLoading={isLoading}
      error={error}
      options={options || []}
      defaultOptions={list[name] || []}
      kind="async"
      cacheOptions
      loadOptions={fetchOptionsBySearch}
      {...rest}
    />
  );
};

const ComboListControlField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);
  return <SelectField component={<ComboListControl />} onChange={onChange} {...props} />;
};

export { ComboListControl, ComboListControlField };
