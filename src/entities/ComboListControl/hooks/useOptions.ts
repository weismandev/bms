import { useState, useEffect } from 'react';

interface IBuildUrl {
  baseUrl: string;
  search?: string;
  value?: string;
  offset?: number;
  limit?: number;
  params?: any;
}

interface IUseOptions {
  baseUrl: string;
  params?: any;
}

export const buildUrl = ({
  baseUrl,
  search,
  value,
  offset = 0,
  limit = 100,
  params = {},
}: IBuildUrl) => {
  const urlObject = new URL(baseUrl);

  urlObject.searchParams.set('limit', limit + '');
  urlObject.searchParams.set('offset', offset + '');

  if (value) {
    urlObject.searchParams.set('id', value);
  }

  if (search) {
    urlObject.searchParams.set('search', search);
    urlObject.searchParams.set('offset', 0 + '');
  }

  Object.keys(params).forEach((name) => {
    urlObject.searchParams.set(name, params[name] || '');
  });

  return urlObject.toString();
};

export const useOptions = ({ baseUrl, params }: IUseOptions) => {
  const [status, setStatus] = useState({
    isLoading: false,
    data: {
      list: [],
    },
  });

  const fetchOptions = async (url: string) => {
    const response = await fetch(url);
    const { data } = await response.json();
    return data;
  };

  const fetchOptionsBySearch = async (search: string) => {
    const url = buildUrl({ baseUrl, search, params });
    const { list } = await fetchOptions(url);
    return list;
  };

  useEffect(() => {
    const getData = async () => {
      const url = buildUrl({ baseUrl, params });
      const res = await fetchOptions(url);

      return res;
    };

    const data = getData();

    setStatus({ ...status, isLoading: false, data });
  }, [baseUrl, params]);

  return { ...status, fetchOptionsBySearch };
};
