export const getList = ({ url }) =>
  fetch(url)
    .then((response) => (response.ok ? response.json() : Promise.reject(response)))
    .catch((error) => error.message);
