import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl as Select, SelectField } from '@ui/index';
import { fxGetList, $list, $isLoading, $error } from './model';

const SelectControl = ({ name, url, path = '', ...rest }) => {
  const options = useStore($list);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (!options[name]) {
      const paths = path.length > 0 ? path.split('.') : [];
      fxGetList({ url, name, path: paths });
    }
  }, []);

  return (
    <Select
      isLoading={isLoading}
      error={error}
      options={options[name] || []}
      defaultOptions={options[name] || []}
      {...rest}
    />
  );
};

const SelectControlField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);
  return <SelectField component={<SelectControl />} onChange={onChange} {...props} />;
};

export { SelectControl, SelectControlField };
