import { api } from '@api/api2';
import {
  GetTypesGuestsPayload,
  GetTypeGuestPayload,
  CreateUpdateTypeGuestPayload,
  ArchiveUnarchiveTypePayload,
} from '../types';

const getTypesGuests = (payload: GetTypesGuestsPayload) =>
  api.v4('get', 'scud-pass/guest-type/list', payload);
const getTypeGuest = (payload: GetTypeGuestPayload) =>
  api.v4('get', 'scud-pass/guest-type/get', payload);
const createTypeGuest = (payload: CreateUpdateTypeGuestPayload) =>
  api.v4('post', 'scud-pass/guest-type/create', payload);
const updateTypeGuest = (payload: CreateUpdateTypeGuestPayload) =>
  api.v4('post', 'scud-pass/guest-type/update', payload);
const archiveType = (payload: ArchiveUnarchiveTypePayload) =>
  api.v4('post', 'scud-pass/guest-type/deactivate', payload);
const unArchiveType = (payload: ArchiveUnarchiveTypePayload) =>
  api.v4('post', 'scud-pass/guest-type/activate', payload);

export const typesGuestsApi = {
  getTypesGuests,
  getTypeGuest,
  createTypeGuest,
  updateTypeGuest,
  archiveType,
  unArchiveType,
};
