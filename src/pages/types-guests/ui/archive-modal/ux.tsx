import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Dialog, DialogActions, DialogContent, Button } from '@mui/material';
import {
  $isOpenArchiveModal,
  changedVisibilityArchiveModal,
  archiveType,
} from '../../models';
import * as Styled from './styled';

export const ArchiveModal: FC = () => {
  const { t } = useTranslation();
  const isOpenArchiveModal = useUnit($isOpenArchiveModal);

  if (!isOpenArchiveModal) {
    return null;
  }

  const handleClose = () => changedVisibilityArchiveModal(false);
  const handleArchive = () => archiveType();

  return (
    <Dialog open={isOpenArchiveModal} onClose={handleClose}>
      <Styled.DialogTitle>{t('ArchivingGuestType')}</Styled.DialogTitle>
      <DialogContent>
        <Styled.DialogContentText>
          {t('AfterArchivingTheGuestType')}
        </Styled.DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="primary" size="medium" onClick={handleArchive}>
          {t('Archive')}
        </Button>
        <Button variant="outlined" color="primary" size="medium" onClick={handleClose}>
          {t('Cancellation')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
