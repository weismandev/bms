import {
  styled,
  DialogTitle as DialogTitleMui,
  DialogContentText as DialogContentTextMui,
} from '@mui/material';

export const DialogTitle = styled(DialogTitleMui)(({ theme }) => ({
  fontSize: 20,
  fontWeight: 500,
  color: theme.palette.text.primary,
}));

export const DialogContentText = styled(DialogContentTextMui)(({ theme }) => ({
  fontSize: 16,
  fontWeight: 400,
  color: theme.palette.text.primary,
}));
