import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { ThemeAdapter } from '@shared/theme/adapter';
import { ErrorMessage, FilterMainDetailLayout } from '@ui/index';
import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isDetailOpen,
} from '../../models';
import { ArchiveModal } from '../archive-modal';
import { Detail } from '../detail';
import { Loader } from '../loader';
import { StatusSnackbar } from '../status-snackbar';
import { Table } from '../table';

const TypesGuestsPage: FC = () => {
  useGate(PageGate);

  const [error, isErrorDialogOpen, isDetailOpen] = useUnit([
    $error,
    $isErrorDialogOpen,
    $isDetailOpen,
  ]);

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <Loader />
      <StatusSnackbar />
      <ArchiveModal />

      <FilterMainDetailLayout
        filter={null}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedTypesGuestsPage: FC = () => (
  <ThemeAdapter>
    <HaveSectionAccess>
      <TypesGuestsPage />
    </HaveSectionAccess>
  </ThemeAdapter>
);

export { RestrictedTypesGuestsPage as TypesGuestsPage };
