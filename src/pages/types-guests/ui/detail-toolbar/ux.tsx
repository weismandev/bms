import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Tooltip, Button, IconButton } from '@mui/material';
import {
  Close,
  Check,
  Edit,
  ArchiveOutlined,
  UnarchiveOutlined,
} from '@mui/icons-material';
import {
  $opened,
  changedDetailVisibility,
  detailForm,
  changedVisibilityArchiveModal,
  unArchiveType,
} from '../../models';
import * as Styled from './styled';

export const DetailToolbar: FC = () => {
  const { t } = useTranslation();
  const { editable, setEditable, submit, submittable, reset } = useUnit(detailForm);
  const opened = useUnit($opened);

  const title = opened?.id ? opened?.title : t('NewTypeOfGuest');

  const handleChangeMode = () => setEditable(true);

  const handleClose = () => {
    changedDetailVisibility(false);

    if (!opened.id) {
      reset();
    }
  };

  const handleCancel = () => {
    reset();

    if (!opened.id) {
      changedDetailVisibility(false);
    }
  };

  const handleArchive = () => changedVisibilityArchiveModal(true);
  const handleUnArchive = () => unArchiveType();

  const secondLevelActiveType = editable ? (
    <>
      <Button
        color="primary"
        variant="contained"
        onClick={submit}
        disabled={!submittable}
        startIcon={<Check />}
      >
        {t('Save')}
      </Button>
      <Button
        color="primary"
        variant="outlined"
        onClick={handleCancel}
        startIcon={<Close />}
      >
        {t('cancel')}
      </Button>
    </>
  ) : (
    <>
      <Button
        color="primary"
        variant="outlined"
        onClick={handleChangeMode}
        startIcon={<Edit />}
      >
        {t('edit')}
      </Button>
      <Button
        color="primary"
        variant="outlined"
        startIcon={<ArchiveOutlined />}
        onClick={handleArchive}
      >
        {t('Archive')}
      </Button>
    </>
  );

  const secondLevelArchiveType = (
    <Button
      color="primary"
      variant="outlined"
      startIcon={<UnarchiveOutlined />}
      onClick={handleUnArchive}
    >
      {t('Restore')}
    </Button>
  );

  return (
    <Styled.Toolbar>
      <Styled.FirstLevel>
        <Styled.ToolbarTitle variant="h6" noWrap>
          {title}
        </Styled.ToolbarTitle>
        <Tooltip title={t('close')}>
          <IconButton onClick={handleClose} size="medium">
            <Close />
          </IconButton>
        </Tooltip>
      </Styled.FirstLevel>
      <Styled.SecondLevel>
        {opened.archived ? secondLevelArchiveType : secondLevelActiveType}
      </Styled.SecondLevel>
    </Styled.Toolbar>
  );
};
