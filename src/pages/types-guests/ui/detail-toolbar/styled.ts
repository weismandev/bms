import { styled, Typography } from '@mui/material';

export const Toolbar = styled('div')(() => ({
  padding: 24,
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  gap: 24,
}));

export const FirstLevel = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  width: '100%',
}));

export const SecondLevel = styled('div')(() => ({
  display: 'flex',
  gap: 12,
}));

export const ToolbarTitle = styled(Typography)(({ theme }) => ({
  fontWeight: 500,
  fontSize: 20,
  color: theme.palette.text.primary,
  paddingTop: 5,
}));
