import { FC } from 'react';
import { useUnit } from 'effector-react';
import type { GridRowParams, GridRowClassNameParams } from '@mui/x-data-grid-pro';
import {
  $tableData,
  $isLoadingTable,
  columnsWidthChanged,
  $columns,
  sortChanged,
  openRowChanged,
  $count,
  $pagination,
  $sorting,
  paginationModelChanged,
  openViaUrl,
  $openRow,
  $selectRow,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import * as Styled from './styled';

export const Table: FC = () => {
  const [isLoading, tableData, pagination, columns, count, sorting, openRow, selectRow] = useUnit([
    $isLoadingTable,
    $tableData,
    $pagination,
    $columns,
    $count,
    $sorting,
    $openRow,
    $selectRow
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openRowChanged(row.id);
    openViaUrl(row.id);
  };

  const getRowClassName = ({ id }: GridRowClassNameParams<any>): string => {
    const baseStyle =
      id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

    return baseStyle;
  };

  return (
    <Styled.Wrapper elevation={0}>
      <TableToolbar />
      <Styled.DataGrid
        rows={tableData}
        columns={columns}
        rowCount={count}
        loading={isLoading}
        density="compact"
        rowSelectionModel={selectRow}
        onRowClick={handleClickRow}
        paginationModel={pagination}
        onPaginationModelChange={paginationModelChanged}
        sortModel={sorting}
        onSortModelChange={sortChanged}
        sortingMode="server"
        onColumnWidthChange={columnsWidthChanged}
        pageSizeOptions={[25, 50, 100]}
        disableColumnMenu
        getRowClassName={getRowClassName}
      />
    </Styled.Wrapper>
  );
};
