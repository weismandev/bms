import { styled, Paper } from '@mui/material';
import { DataGridTable } from '@shared/ui/data-grid-table';

export const Wrapper = styled(Paper)(() => ({
  padding: '24px 24px 0px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
}));

export const DataGrid = styled(DataGridTable)(() => ({
  '&:last-of-type': {
    '& .MuiDataGrid-columnSeparator': {
      display: 'none',
    },
  },
}));
