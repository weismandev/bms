import { FC } from 'react';
import { useUnit } from 'effector-react';
import { CircularProgress } from '@mui/material';
import { $isLoading } from '../../models';
import * as Styled from './styled';

export const Loader: FC = () => {
  const isLoading = useUnit($isLoading);

  if (!isLoading) {
    return null;
  }

  return (
    <Styled.Backdrop open={isLoading}>
      <CircularProgress color="inherit" />
    </Styled.Backdrop>
  );
};
