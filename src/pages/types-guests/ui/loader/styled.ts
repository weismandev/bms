import { styled, Backdrop as BackdropMui } from '@mui/material';

export const Backdrop = styled(BackdropMui)(() => ({
  zIndex: 10,
}));
