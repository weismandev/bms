import { styled, Paper } from '@mui/material';

export const Wrapper = styled(Paper)(() => ({
  height: '100%',
}));

export const Form = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  padding: '0px 24px 24px',
}));
