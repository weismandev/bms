import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Control } from '@effector-form';
import { detailForm } from '../../models';
import { DetailToolbar } from '../detail-toolbar';
import * as Styled from './styled';

export const Detail: FC = () => {
  const { t } = useTranslation();

  return (
    <Styled.Wrapper>
      <DetailToolbar />
      <Styled.Form>
        <Control.Text
          name="title"
          label={t('NameTitle') ?? ''}
          required
          form={detailForm}
        />
      </Styled.Form>
    </Styled.Wrapper>
  );
};
