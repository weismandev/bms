import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { ToggleButtonGroup, ToggleButton, Button } from '@mui/material';
import { Add } from '@mui/icons-material';
import {
  $typeStatus,
  changedTypeStatus,
  detailForm,
  addClicked,
  $opened,
} from '../../models';
import { TypeStatus } from '../../types';
import * as Styled from './styled';

export const TableToolbar: FC = () => {
  const { t } = useTranslation();
  const [typeStatus, opened] = useUnit([$typeStatus, $opened]);
  const { editable } = useUnit(detailForm);

  const isDisabledAddButton = editable && !opened.id;

  const handleStatus = (_: unknown, value: TypeStatus) =>
    value ? changedTypeStatus(value) : null;

  const handleAdd = () => addClicked();

  return (
    <Styled.Toolbar>
      <ToggleButtonGroup
        value={typeStatus}
        exclusive
        size="small"
        onChange={handleStatus}
      >
        <ToggleButton value="active">{t('ActiveThey')}</ToggleButton>
        <ToggleButton value="archive">{t('Archival')}</ToggleButton>
      </ToggleButtonGroup>
      <Styled.Greedy />
      <Button
        variant="contained"
        color="primary"
        startIcon={<Add />}
        onClick={handleAdd}
        disabled={isDisabledAddButton}
      >
        {t('AddGuestType')}
      </Button>
    </Styled.Toolbar>
  );
};
