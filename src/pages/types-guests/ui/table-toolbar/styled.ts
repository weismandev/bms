import { styled } from '@mui/material';

export const Toolbar = styled('div')(() => ({
  display: 'flex',
  width: '100%',
  paddingBottom: 24,
}));

export const Greedy = styled('div')(() => ({
  flexGrow: 13,
}));
