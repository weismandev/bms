import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Snackbar } from '@mui/material';
import { CheckCircleOutline } from '@mui/icons-material';
import { $snackbarData, hideSnackbar } from '../../models';
import * as Styled from './styled';

export const StatusSnackbar: FC = () => {
  const snackbarData = useUnit($snackbarData);

  if (!snackbarData.isShow) {
    return null;
  }

  const handleClose = () => hideSnackbar();

  return (
    <Snackbar
      open={snackbarData.isShow}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      autoHideDuration={2000}
      onClose={handleClose}
      style={{ top: 10 }}
    >
      <Styled.Alert
        icon={<CheckCircleOutline fontSize="inherit" />}
        severity="success"
        variant="filled"
        onClose={handleClose}
      >
        {snackbarData.text}
      </Styled.Alert>
    </Snackbar>
  );
};
