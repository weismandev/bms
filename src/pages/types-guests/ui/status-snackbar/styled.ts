import { styled, Alert as AlertMui } from '@mui/material';

export const Alert = styled(AlertMui)(() => ({
  '.MuiAlert-action': {
    padding: '3px 0px 0px 16px',
  },
}));
