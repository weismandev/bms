import { signout } from '@features/common';
import { archiveType } from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import {
  $isOpenArchiveModal,
  changedVisibilityArchiveModal,
} from './archive-modal.model';

$isOpenArchiveModal
  .on(changedVisibilityArchiveModal, (_, visibility) => visibility)
  .reset([signout, pageUnmounted, archiveType]);
