import { createEvent, createStore } from 'effector';

export const changedVisibilityArchiveModal = createEvent<boolean>();

export const $isOpenArchiveModal = createStore<boolean>(false);
