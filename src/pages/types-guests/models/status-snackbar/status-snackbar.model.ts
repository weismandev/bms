import { createStore, createEvent } from 'effector';
import i18n from '@shared/config/i18n';
import { SnackbarData } from '../../types';

const { t } = i18n;

export const textSuccessfulCreation = t('NewTypeAdded');
export const textSuccessfulMoveToActiveSection = t('TypeMovedToActiveSection');
export const textSuccessfulMovedToArchiveSection = t('TypeMovedToArchiveSection');

export const hideSnackbar = createEvent<void>();

export const $snackbarData = createStore<SnackbarData>({ isShow: false, text: '' });
