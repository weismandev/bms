import { sample } from 'effector';
import { signout } from '@features/common';
import {
  fxCreateGuestType,
  fxArchiveType,
  fxUnArchiveType,
} from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import {
  $snackbarData,
  textSuccessfulCreation,
  hideSnackbar,
  textSuccessfulMovedToArchiveSection,
  textSuccessfulMoveToActiveSection,
} from './status-snackbar.model';

$snackbarData.reset([signout, pageUnmounted, hideSnackbar]);

/** Показать уведомление с текстом успешного создания типа */
sample({
  clock: fxCreateGuestType.done,
  fn: () => ({ isShow: true, text: textSuccessfulCreation }),
  target: $snackbarData,
});

/** Показать уведомление с текстом успешной архивации типа */
sample({
  clock: fxArchiveType.done,
  fn: () => ({ isShow: true, text: textSuccessfulMovedToArchiveSection }),
  target: $snackbarData,
});

/** Показать уведомление с текстом успешной разархивации типа */
sample({
  clock: fxUnArchiveType.done,
  fn: () => ({ isShow: true, text: textSuccessfulMoveToActiveSection }),
  target: $snackbarData,
});
