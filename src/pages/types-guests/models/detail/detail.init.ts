import { sample } from 'effector';
import { delay } from 'patronum';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  detailForm,
  addClicked,
  detailClosed,
  openViaUrl,
  fxGetGuestType,
  $isDetailOpen,
  $path,
  $entityId,
  changedDetailVisibility,
  $opened,
  fxCreateGuestType,
  fxUpdateGuestType,
  archiveType,
  fxArchiveType,
  unArchiveType,
  fxUnArchiveType,
} from './detail.model';

detailForm.editable
  .on(addClicked, () => true)
  .reset([
    signout,
    pageUnmounted,
    detailClosed,
    openViaUrl,
    fxCreateGuestType.done,
    fxUpdateGuestType.done,
  ]);

/** При клике на строку в таблице отправляется запрос getById */
sample({
  clock: openViaUrl,
  fn: (id) => ({ id: id as number }),
  target: fxGetGuestType,
});

/** Если при маунте в url есть id, то отправляется запрос getById */
sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: Number.parseInt(entityId, 10),
  }),
  target: fxGetGuestType,
});

$isDetailOpen
  .on(fxGetGuestType.done, () => true)
  .reset([signout, pageUnmounted, fxGetGuestType.fail]);

/** Если getById падает, то из url убирается id */
sample({
  clock: delay({ source: fxGetGuestType.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

/** При закрытии из url убирается id */
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

$opened
  .on(
    [
      fxGetGuestType.done,
      fxCreateGuestType.done,
      fxUpdateGuestType.done,
      fxArchiveType.done,
      fxUnArchiveType.done,
    ],
    (_, { result: { guestType } }) => ({
      id: guestType.id,
      title: guestType?.title ?? '',
      archived: guestType?.archived ?? false,
    })
  )
  .reset([signout, pageUnmounted, fxGetGuestType]);

/** Создание типа гостя */
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => !id,
  fn: ({ title }) => ({ title }),
  target: fxCreateGuestType,
});

/** После создания типа в url добавляется id */
sample({
  clock: fxCreateGuestType.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        guestType: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

/** Обновление типа */
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => Boolean(id),
  fn: ({ id, title }) => ({ id, title }),
  target: fxUpdateGuestType,
});

/** Архивация типа */
sample({
  clock: archiveType,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxArchiveType,
});

/** Разархивация типа */
sample({
  clock: unArchiveType,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxUnArchiveType,
});
