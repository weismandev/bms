import * as Zod from 'zod';

export const validationSchema = Zod.object({
  title: Zod.string().min(1),
});
