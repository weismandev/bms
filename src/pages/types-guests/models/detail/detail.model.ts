import { createEffect, createEvent } from 'effector';
import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';
import { createForm } from '@unicorn/effector-form';
import {
  DetailForm,
  GetTypeGuestPayload,
  GetTypeGuestResponse,
  CreateUpdateTypeGuestPayload,
  ArchiveUnarchiveTypePayload,
} from '../../types';
import { validationSchema } from './validation';

const newEntity = {
  id: null,
  title: '',
  archived: false,
};

export const {
  changedDetailVisibility,
  addClicked,
  openViaUrl,
  $isDetailOpen,
  $opened,
  detailClosed,
} = createDetailBag(newEntity);

export const detailForm = createForm<DetailForm>({
  initialValues: $opened,
  editable: false,
  validationSchema,
});

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2]);

export const archiveType = createEvent<void>();
export const unArchiveType = createEvent<void>();

export const fxGetGuestType = createEffect<
  GetTypeGuestPayload,
  GetTypeGuestResponse,
  Error
>();
export const fxCreateGuestType = createEffect<
  CreateUpdateTypeGuestPayload,
  GetTypeGuestResponse,
  Error
>();
export const fxUpdateGuestType = createEffect<
  CreateUpdateTypeGuestPayload,
  GetTypeGuestResponse,
  Error
>();
export const fxArchiveType = createEffect<
  ArchiveUnarchiveTypePayload,
  GetTypeGuestResponse,
  Error
>();
export const fxUnArchiveType = createEffect<
  ArchiveUnarchiveTypePayload,
  GetTypeGuestResponse,
  Error
>();
