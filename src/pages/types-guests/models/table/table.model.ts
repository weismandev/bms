import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('NameTitle') ?? '',
    width: 500,
    sortable: true,
    hideable: false,
  },
];

export const {
  $tableParams,
  $pagination,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  sortChanged,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
  $selectRow
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

export const $count = $rawData.map(({ meta }) => meta.total ?? 0);

export const $tableData = $rawData.map(({ guestTypeList }) => {
  if (Array.isArray(guestTypeList) && guestTypeList.length > 0) {
    return guestTypeList.map((type) => ({
      id: type.id,
      title: type?.title ?? '-',
    }));
  }

  return [];
});
