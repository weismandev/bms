import { sample } from 'effector';
import { signout } from '@features/common';
import {
  addClicked,
  detailClosed,
  $entityId,
  fxCreateGuestType,
} from '../detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

$openRow
  .on(
    fxCreateGuestType.done,
    (
      _,
      {
        result: {
          guestType: { id },
        },
      }
    ) => id
  )
  .reset([signout, pageUnmounted, addClicked, detailClosed]);

/** Если при маунте есть id в url, то выбирается строка с нужной записью */
sample({
  clock: pageMounted,
  source: $entityId,
  filter: (id) => Boolean(id),
  fn: (id) => id,
  target: $openRow,
});
