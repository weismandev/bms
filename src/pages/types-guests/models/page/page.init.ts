import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { typesGuestsApi } from '../../api';
import { GetTypesGuestsPayload } from '../../types';
import {
  fxGetGuestType,
  fxCreateGuestType,
  fxUpdateGuestType,
  fxArchiveType,
  fxUnArchiveType,
} from '../detail/detail.model';
import { $tableParams } from '../table/table.model';
import {
  $isLoading,
  pageUnmounted,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetTypesGuests,
  pageMounted,
  $rawData,
  $isLoadingTable,
  $typeStatus,
  changedTypeStatus,
} from './page.model';

fxGetTypesGuests.use(typesGuestsApi.getTypesGuests);
fxGetGuestType.use(typesGuestsApi.getTypeGuest);
fxCreateGuestType.use(typesGuestsApi.createTypeGuest);
fxUpdateGuestType.use(typesGuestsApi.updateTypeGuest);
fxArchiveType.use(typesGuestsApi.archiveType);
fxUnArchiveType.use(typesGuestsApi.unArchiveType);

const errorOccured = merge([
  fxGetTypesGuests.fail,
  fxGetGuestType.fail,
  fxCreateGuestType.fail,
  fxUpdateGuestType.fail,
  fxArchiveType.fail,
  fxUnArchiveType.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetGuestType,
        fxCreateGuestType,
        fxUpdateGuestType,
        fxArchiveType,
        fxUnArchiveType,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetTypesGuests.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);

/** Запрос списка типов */
sample({
  clock: [
    pageMounted,
    $typeStatus,
    $tableParams,
    fxCreateGuestType.done,
    fxUpdateGuestType.done,
    fxArchiveType.done,
    fxUnArchiveType.done,
  ],
  source: { typeStatus: $typeStatus, tableParams: $tableParams },
  fn: ({ typeStatus, tableParams }) => {
    const payload: GetTypesGuestsPayload = {
      page: tableParams.page,
      per_page: tableParams.per_page,
      archived: typeStatus === 'active' ? 0 : 1,
    };

    if (tableParams.sorting.length > 0) {
      const sorting = tableParams.sorting[0];

      payload.sort = sorting.field;
      payload.order = sorting.sort as string;
    }

    return payload;
  },
  target: fxGetTypesGuests,
});

$rawData
  .on(fxGetTypesGuests.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

$typeStatus.on(changedTypeStatus, (_, status) => status).reset([pageUnmounted, signout]);
