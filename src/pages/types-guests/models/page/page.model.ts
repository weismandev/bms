import { createEvent, createStore, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { GetTypesGuestsPayload, GetTypesGuestsResponse, TypeStatus } from '../../types';

export const PageGate = createGate();
export const { open: pageMounted, close: pageUnmounted } = PageGate;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const changedTypeStatus = createEvent<TypeStatus>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isLoadingTable = createStore<boolean>(false);
export const $rawData = createStore<GetTypesGuestsResponse>({
  guestTypeList: [],
  meta: { total: 0 },
});
export const $typeStatus = createStore<TypeStatus>('active');

export const fxGetTypesGuests = createEffect<
  GetTypesGuestsPayload,
  GetTypesGuestsResponse,
  Error
>();
