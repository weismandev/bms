import { GuestType } from '../guest-type';

export type GetTypesGuestsPayload = {
  page: number;
  per_page: number;
  archived: number;
  sort?: string;
  order?: string;
};

export type GetTypesGuestsResponse = {
  guestTypeList: GuestType[];
  meta: {
    total: number;
  };
};

export type TypeStatus = 'active' | 'archive';
