export type SnackbarData = {
  isShow: boolean;
  text: string;
};
