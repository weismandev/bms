export type GuestType = {
  id: number;
  title: string;
  archived: boolean;
};

export type DetailForm = {
  id: null | number;
  title: string;
};

export type GetTypeGuestPayload = {
  id: number;
};

export type GetTypeGuestResponse = {
  guestType: GuestType;
};

export type CreateUpdateTypeGuestPayload = {
  id?: number | null;
  title: string;
};

export type ArchiveUnarchiveTypePayload = {
  id: number;
};
