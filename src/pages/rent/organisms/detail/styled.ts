import { Paper, Chip as ChipMui } from '@mui/material';
import { styled, Theme } from '@mui/material/styles';
import { getStatusColors } from '../../libs';

export const Wrapper = styled(Paper)(() => ({
  height: '100%',
}));

export const FormWrapper = styled('div')(() => ({
  height: 'calc(100% - 112px)',
  padding: '0px 0px 24px 24px',
}));

export const Form = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 20,
  paddingRight: 24,
  marginBottom: 10,
}));

export const Controls = styled('div')(() => ({
  display: 'grid',
  gap: 10,
  gridTemplateColumns: 'repeat(2, 1fr)',
  '@media (max-width: 1350px)': {
    gridTemplateColumns: 'repeat(1, 1fr)',
    gap: '20px 10px',
  },
}));

export const StatusField = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 10,
  marginBottom: 15,
}));

export const Chip = styled(ChipMui)(
  ({ value, theme }: { value: number; theme?: Theme }) => ({
    ...getStatusColors(value, theme),
    width: 'fit-content',
  })
);
