import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { FormLabel, Divider } from '@mui/material';
import { CustomScrollbar } from '@ui/index';
import { detailForm } from '../../models';
import { DetailToolbar } from '../detail-toolbar';
import * as Styled from './styled';

export const Detail: FC = () => {
  const { t } = useTranslation();
  const { values, editable } = useUnit(detailForm);

  const isReadOnly = Boolean(values.id);

  return (
    <Styled.Wrapper>
      <DetailToolbar />
      <Styled.FormWrapper>
        <CustomScrollbar>
          <Styled.Form>
            <Styled.Controls>
              <Control.City
                name="city"
                label={t('Label.city') ?? ''}
                required
                readOnly={isReadOnly}
                form={detailForm}
              />
              <Control.RentComplex
                name="complex"
                label={t('complex') ?? ''}
                city={values.city as number}
                required
                readOnly={isReadOnly}
                form={detailForm}
              />
            </Styled.Controls>
            <Styled.Controls>
              <Control.RentBuilding
                name="building"
                label={t('Label.address') ?? ''}
                complex={values.complex as string}
                required
                readOnly={isReadOnly}
                form={detailForm}
              />
              <Control.RentFloor
                name="floor"
                label={t('Floor') ?? ''}
                building={values.building as string}
                readOnly={isReadOnly}
                form={detailForm}
              />
            </Styled.Controls>
            <Styled.Controls>
              <Control.PropertyType
                name="propertyType"
                label={t('RoomType') ?? ''}
                required
                readOnly={isReadOnly}
                form={detailForm}
              />
              <Control.RentObjects
                name="rentObject"
                label={t('ReservationObject') ?? ''}
                propertyType={values.propertyType}
                object={values?.floor ? values.floor : values.building}
                required
                readOnly={isReadOnly}
                form={detailForm}
              />
            </Styled.Controls>
            <Divider />
            <Styled.Controls>
              {editable ? (
                <Control.RentStatus
                  name="status"
                  label={t('Label.status') ?? ''}
                  required
                  form={detailForm}
                  mode={values?.id ? 'update' : 'create'}
                />
              ) : (
                <Styled.StatusField>
                  <FormLabel required>{t('Label.status')}</FormLabel>
                  {values.statusTitle && values.status && (
                    <Styled.Chip label={values.statusTitle} value={values.status} />
                  )}
                </Styled.StatusField>
              )}
            </Styled.Controls>
            <Divider />
            <Styled.Controls>
              <Control.Date
                name="rentalDate"
                label={t('RentalDate') ?? ''}
                required
                form={detailForm}
              />
              <Control.RentTime
                name="rentalTime"
                label={t('TimeSlot') ?? ''}
                rentalDate={values.rentalDate}
                rentObject={values.rentObject}
                required
                form={detailForm}
              />
            </Styled.Controls>
            <Divider />
            <Control.Textarea
              name="comment"
              label={t('Label.comment') ?? ''}
              form={detailForm}
            />
          </Styled.Form>
        </CustomScrollbar>
      </Styled.FormWrapper>
    </Styled.Wrapper>
  );
};
