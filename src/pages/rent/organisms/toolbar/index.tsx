import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Tabs, Tab, Divider } from '@mui/material';
import { $tab, changedTab } from '../../models';

export const Toolbar: FC = () => {
  const { t } = useTranslation();
  const tab = useUnit($tab);

  const onChangeTab = (_: unknown, value: number) => changedTab(value);

  return (
    <>
      <Tabs value={tab} onChange={onChangeTab}>
        <Tab label={t('Registry')} />
        <Tab label={t('Monitoring')} />
      </Tabs>
      <Divider />
    </>
  );
};
