import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Button } from '@mui/material';
import { Check, Clear } from '@mui/icons-material';
import { FormInstance } from '@unicorn/effector-form';
import * as Styled from './styled';

type Props = {
  form: FormInstance<any>;
  filtersEntity: object;
};

export const FilterFooter: FC<Props> = ({ form, filtersEntity }) => {
  const { t } = useTranslation();
  const { submit, reset, submittable, initialValues } = useUnit(form);

  const isDefaultFilterValues =
    JSON.stringify({ ...filtersEntity }) === JSON.stringify(initialValues);

  return (
    <Styled.Container>
      <Button
        variant="contained"
        startIcon={<Check />}
        color="primary"
        onClick={submit}
        disabled={!submittable}
      >
        {t('Apply')}
      </Button>
      <Button
        variant="outlined"
        startIcon={<Clear />}
        color="primary"
        onClick={reset}
        disabled={isDefaultFilterValues}
      >
        {t('Reset')}
      </Button>
    </Styled.Container>
  );
};
