import { styled } from '@mui/material/styles';

export const Container = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  gap: 10,
}));
