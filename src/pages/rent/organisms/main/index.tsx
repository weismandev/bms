import { FC } from 'react';
import { useUnit } from 'effector-react';
import { $tab } from '../../models';
import { Graphics } from '../../monitoring/organisms';
import { Table } from '../../registry/organisms';
import { Toolbar } from '../toolbar';
import * as Styled from './styled';

export const Main: FC = () => {
  const tab = useUnit($tab);

  return (
    <Styled.Wrapper>
      <Toolbar />
      {tab === 0 && <Table />}
      {tab === 1 && <Graphics />}
    </Styled.Wrapper>
  );
};
