import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Wrapper = styled(Paper)(() => ({
  height: '100%',
}));
