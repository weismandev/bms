import { Backdrop as BackdropMui } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Backdrop = styled(BackdropMui)(() => ({
  zIndex: 10,
}));
