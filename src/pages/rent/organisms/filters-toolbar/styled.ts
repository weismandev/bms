import { Typography as TypographyMui, IconButton } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Container = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
}));

export const Typography = styled(TypographyMui)(({ theme }) => ({
  fontWeight: 500,
  fontSize: 20,
  color: theme.palette.text.primary,
}));

export const CloseButton = styled(IconButton)(() => ({
  marginTop: -7,
}));
