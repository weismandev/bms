import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Tooltip } from '@mui/material';
import { Close } from '@mui/icons-material';
import * as Styled from './styled';

type Props = {
  onClose: () => boolean;
};

export const FilterToolbar: FC<Props> = ({ onClose }) => {
  const { t } = useTranslation();

  return (
    <Styled.Container>
      <Styled.Typography variant="h3">{t('Filter')}</Styled.Typography>
      <Styled.CloseButton onClick={onClose}>
        <Tooltip title={t('close')}>
          <Close />
        </Tooltip>
      </Styled.CloseButton>
    </Styled.Container>
  );
};
