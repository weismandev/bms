import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Button, Tooltip } from '@mui/material';
import { Close, Edit, Check, FileCopyOutlined } from '@mui/icons-material';
import {
  detailForm,
  changedDetailVisibility,
  $opened,
  dublicateRent,
} from '../../models';
import * as Styled from './styled';

export const DetailToolbar: FC = () => {
  const { t } = useTranslation();
  const { editable, setEditable, submit, submittable, reset } = useUnit(detailForm);
  const opened = useUnit($opened);

  const title = opened?.id ? opened?.title : t('NewEntry');
  const isHideEditButton = opened?.id && opened.status === 8;

  const onChangeMode = () => setEditable(true);

  const onClose = () => {
    changedDetailVisibility(false);

    if (!opened.id) {
      reset();
    }
  };

  const onCancel = () => {
    reset();

    if (!opened.id) {
      changedDetailVisibility(false);
    }
  };

  const onDublicate = () => dublicateRent();

  return (
    <Styled.Toolbar>
      <Styled.FirstLevelToolbar>
        <Styled.ToolbarTitle variant="h3" noWrap>
          {title}
        </Styled.ToolbarTitle>
        <Styled.CloseButton onClick={onClose}>
          <Tooltip title={t('close')}>
            <Close />
          </Tooltip>
        </Styled.CloseButton>
      </Styled.FirstLevelToolbar>
      <Styled.SecondLevelToolbar>
        {editable ? (
          <>
            <Button
              color="primary"
              variant="contained"
              onClick={submit}
              disabled={!submittable}
              startIcon={<Check />}
            >
              {t('Save')}
            </Button>
            <Button
              color="primary"
              variant="outlined"
              onClick={onCancel}
              startIcon={<Close />}
            >
              {t('cancel')}
            </Button>
          </>
        ) : (
          <>
            {!isHideEditButton ? (
              <Button
                color="primary"
                variant="outlined"
                onClick={onChangeMode}
                startIcon={<Edit />}
              >
                {t('edit')}
              </Button>
            ) : null}
            <Button
              color="primary"
              variant="outlined"
              onClick={onDublicate}
              startIcon={<FileCopyOutlined />}
            >
              {t('Dublicate')}
            </Button>
          </>
        )}
      </Styled.SecondLevelToolbar>
    </Styled.Toolbar>
  );
};
