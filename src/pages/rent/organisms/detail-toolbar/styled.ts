import { Typography, IconButton } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Toolbar = styled('div')(() => ({
  padding: 24,
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  gap: 12,
}));

export const FirstLevelToolbar = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  width: '100%',
}));

export const SecondLevelToolbar = styled('div')(() => ({
  display: 'flex',
  gap: 12,
}));

export const CloseButton = styled(IconButton)(() => ({
  marginTop: -8,
}));

export const ToolbarTitle = styled(Typography)(({ theme }) => ({
  fontWeight: 500,
  fontSize: 20,
  color: theme.palette.text.primary,
}));
