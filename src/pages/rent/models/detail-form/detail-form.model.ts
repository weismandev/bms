import { createForm } from '@unicorn/effector-form';
import { DetailForm } from '../../types';
import { $opened } from '../detail/detail.model';
import { validationSchema } from './validation';

export const detailForm = createForm<DetailForm>({
  initialValues: $opened,
  validationSchema,
  editable: false,
});
