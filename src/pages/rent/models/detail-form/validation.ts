import * as Zod from 'zod';
import i18n from '@shared/config/i18n';

const { t } = i18n;
const zeroValue = 0;
const coefficientForConvertMinutesToSeconds = 60;
const coefficientForConvertSecondsToMilliseconds = 1000;

export const validationSchema = Zod.object({
  city: Zod.union([Zod.number(), Zod.null()]),
  timezone: Zod.union([Zod.number(), Zod.null()]),
  complex: Zod.union([Zod.string(), Zod.null()]),
  building: Zod.union([Zod.string(), Zod.null()]),
  propertyType: Zod.union([Zod.string(), Zod.null()]),
  status: Zod.union([Zod.number(), Zod.null()]),
  rentObject: Zod.union([Zod.string(), Zod.null()]),
  rentalDate: Zod.union([Zod.date(), Zod.null()]),
  rentalTime: Zod.object({
    id: Zod.string(),
    label: Zod.string(),
    start: Zod.number(),
    finish: Zod.number(),
  })
    .array()
    .nonempty(),
})
  .refine(({ city }) => city, {
    message: t('thisIsRequiredField') ?? '',
    path: ['city'],
  })
  .refine(({ complex }) => complex, {
    message: t('thisIsRequiredField') ?? '',
    path: ['complex'],
  })
  .refine(({ building }) => building, {
    message: t('thisIsRequiredField') ?? '',
    path: ['building'],
  })
  .refine(({ propertyType }) => propertyType, {
    message: t('thisIsRequiredField') ?? '',
    path: ['propertyType'],
  })
  .refine(({ status }) => status, {
    message: t('thisIsRequiredField') ?? '',
    path: ['status'],
  })
  .refine(({ rentObject }) => rentObject, {
    message: t('thisIsRequiredField') ?? '',
    path: ['rentObject'],
  })
  .refine(({ rentalDate }) => rentalDate, {
    message: t('thisIsRequiredField') ?? '',
    path: ['rentalDate'],
  })
  .refine(
    ({ rentalDate, timezone }) => {
      if (rentalDate && timezone) {
        const currentDate = new Date();

        const currentDateOffsetTimezone =
          currentDate.getTimezoneOffset() * coefficientForConvertMinutesToSeconds;

        if (currentDateOffsetTimezone * -1 === timezone) {
          // умножается на -1 для смены знака
          currentDate.setHours(zeroValue, zeroValue, zeroValue, zeroValue);
          rentalDate.setHours(zeroValue, zeroValue, zeroValue, zeroValue);

          return Number(rentalDate) >= Number(currentDate);
        }

        currentDate.setTime(
          currentDate.getTime() +
            currentDateOffsetTimezone * coefficientForConvertSecondsToMilliseconds +
            timezone * coefficientForConvertSecondsToMilliseconds
        );

        currentDate.setHours(zeroValue, zeroValue, zeroValue, zeroValue);
        rentalDate.setHours(zeroValue, zeroValue, zeroValue, zeroValue);

        return Number(rentalDate) >= Number(currentDate);
      }

      return true;
    },
    {
      message: t('TheRentalDateMustBeLaterThanTheCurrentOne') ?? '',
      path: ['rentalDate'],
    }
  )
  .refine(
    ({ rentalTime }) => {
      if (rentalTime.length > 0) {
        return rentalTime.every((time, index) => {
          if (!rentalTime[index + 1]) {
            return true;
          }

          return time.finish === rentalTime[index + 1].start;
        });
      }

      return true;
    },
    {
      message: t('ThereShouldBeNoGapBetweenSlots') ?? '',
      path: ['rentalTime'],
    }
  );
