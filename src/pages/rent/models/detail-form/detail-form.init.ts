import { sample } from 'effector';
import { signout } from '@features/common';
import { fxGetRentObjectData } from '../../monitoring/models/graphics/graphics.model';
import { pageUnmounted } from '../../registry/models/page/page.model';
import {
  addClicked,
  detailClosed,
  openViaUrl,
  fxCreateRent,
  fxUpdateRent,
  dublicateRent,
  open,
} from '../detail/detail.model';
import { changedTab } from '../main/main.model';
import { detailForm } from './detail-form.model';

detailForm.editable
  .on([addClicked, dublicateRent, fxGetRentObjectData.done], () => true)
  .reset([
    signout,
    pageUnmounted,
    detailClosed,
    openViaUrl,
    fxCreateRent.done,
    fxUpdateRent.done,
    open,
  ]);

/** При изменении города сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'city',
  fn: ({ value }) => ({
    city: value?.id,
    timezone: value?.offset,
    complex: null,
    building: null,
    floor: null,
    rentObject: null,
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении комплекса сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'complex',
  fn: () => ({
    building: null,
    floor: null,
    rentObject: null,
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении здания сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'building',
  fn: () => ({
    floor: null,
    rentObject: null,
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении этажа сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'floor',
  fn: () => ({
    rentObject: null,
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении типа помещения сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'propertyType',
  fn: () => ({
    rentObject: null,
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении объекта бронирования сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'rentObject',
  fn: () => ({
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** При изменении типа помещения сбрасываются зависимые поля */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'rentalDate',
  fn: () => ({
    rentalTime: [],
  }),
  target: detailForm.updateValues,
});

/** Сортировка выбранного времени */
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'rentalTime',
  fn: ({ value }) => {
    const newValue = [...value];

    return { rentalTime: newValue.sort((a, b) => a.start - b.start) };
  },
  target: detailForm.updateValues,
});

/** При смене вкладки сбрасывается форма */
sample({
  clock: changedTab,
  target: detailForm.reset,
});
