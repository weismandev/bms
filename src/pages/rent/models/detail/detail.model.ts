import { createEffect, createStore, merge, createEvent } from 'effector';
import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';
import {
  GetRentItemPayload,
  CreateRentPayload,
  GetRentItemResponse,
  CreateUpdateRentResponse,
  UpdateRentPayload,
} from '../../types';

const newEntity = {
  id: null,
  city: null,
  timezone: null,
  complex: null,
  building: null,
  floor: null,
  propertyType: null,
  status: null,
  statusTitle: '',
  rentObject: null,
  rentalDate: null,
  rentalTime: [],
  comment: '',
};

export const {
  changedDetailVisibility,
  addClicked,
  openViaUrl,
  $isDetailOpen,
  $opened,
  detailClosed,
  open,
} = createDetailBag(newEntity);

export const dublicateRent = createEvent<void>();

export const fxGetRentItem = createEffect<
  GetRentItemPayload,
  GetRentItemResponse,
  Error
>();
export const fxCreateRent = createEffect<
  CreateRentPayload,
  CreateUpdateRentResponse,
  Error
>();
export const fxUpdateRent = createEffect<
  UpdateRentPayload,
  CreateUpdateRentResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);
export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);

export const errorOccured = merge([
  fxGetRentItem.fail,
  fxCreateRent.fail,
  fxUpdateRent.fail,
]);
