import { sample } from 'effector';
import { delay, pending } from 'patronum';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { rentApi } from '../../api';
import { getIdInUrl } from '../../libs';
import { dateForm } from '../../monitoring/models/date-form/date-form.model';
import {
  graphicsUnmounted,
  fxGetRentObjectData,
} from '../../monitoring/models/graphics/graphics.model';
import {
  pageMounted,
  pageUnmounted,
  coefficientForSeconds,
} from '../../registry/models/page/page.model';
import { CreateRentPayload, UpdateRentPayload } from '../../types';
import { detailForm } from '../detail-form/detail-form.model';
import { changedTab, $tab } from '../main/main.model';
import {
  $opened,
  fxGetRentItem,
  openViaUrl,
  $isDetailOpen,
  $path,
  changedDetailVisibility,
  fxCreateRent,
  fxUpdateRent,
  $isLoading,
  open,
  dublicateRent,
} from './detail.model';

fxGetRentItem.use(rentApi.getRentItem);
fxCreateRent.use(rentApi.createRent);
fxUpdateRent.use(rentApi.updateRent);

$isLoading
  .on(
    pending({
      effects: [fxGetRentItem, fxCreateRent, fxUpdateRent],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout, graphicsUnmounted]);

/** Если не реестр, то из url убирается id */
sample({
  clock: changedTab,
  source: { tab: $tab, path: $path },
  filter: ({ tab }) => tab !== 0,
  fn: ({ path }) => `../${path}`,
  target: historyPush,
});

/** При клике на строку в таблице отправляется запрос getById */
sample({
  clock: [openViaUrl, open],
  fn: (id) => ({ rent_id: id.toString() }),
  target: fxGetRentItem,
});

/** Если при маунте в url есть id, то отправляется запрос getById */
sample({
  clock: pageMounted,
  source: $pathname,
  filter: (pathname) => IsExistIdInUrl(pathname),
  fn: (pathname) => ({
    rent_id: getIdInUrl(pathname as string),
  }),
  target: fxGetRentItem,
});

$isDetailOpen
  .on([fxGetRentItem.done, fxGetRentObjectData.done], () => true)
  .reset([signout, pageUnmounted, fxGetRentItem.fail, graphicsUnmounted]);

/** Если getById падает, то из url убирается id */
sample({
  clock: delay({ source: fxGetRentItem.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

/** При закрытии из url убирается id */
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

/** Создание аренды */
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => !id,
  fn: (data) => {
    const payload: CreateRentPayload = {
      area_id: data.rentObject as string,
      status_id: data.status as number,
      tenant_id: 700695, // пока нет селектора с арендодателями
      comment: data.comment,
    };

    if (
      data.rentalTime.length > 1 &&
      data.rentalTime[0] &&
      data.rentalTime[data.rentalTime.length - 1]
    ) {
      payload.rent_start = data.rentalTime[0].start;
      payload.rent_finish = data.rentalTime[data.rentalTime.length - 1].finish;
    }

    if (data.rentalTime.length === 1 && data.rentalTime[0]) {
      payload.rent_start = data.rentalTime[0].start;
      payload.rent_finish = data.rentalTime[0].finish;
    }

    return payload;
  },
  target: fxCreateRent,
});

$opened
  .on(fxGetRentItem.done, (_, { result }) => ({
    id: result?.rent?.id,
    title: result?.area?.title ?? '',
    city: result?.area?.city?.id ?? null,
    timezone: result?.area?.city?.offset ?? null,
    complex: result?.area?.complex?.id ?? null,
    building: result?.area?.building?.id ?? null,
    floor: result?.area?.floor?.id ?? null,
    propertyType: result?.area?.type?.id ?? null,
    status: result?.rent?.status?.id ?? null,
    statusTitle: result?.rent?.status?.title ?? '',
    rentObject: result?.area?.id ?? null,
    rentalDate: result?.rent?.rent_start
      ? new Date(result.rent.rent_start * coefficientForSeconds)
      : null,
    rentalTime: Array.isArray(result?.rent?.slots)
      ? result.rent.slots.map((slot) => ({
          id: `${slot.rent_start}-${slot.rent_finish}`,
          label: slot.slot_title,
          start: slot.rent_start,
          finish: slot.rent_finish,
        }))
      : [],
    comment: result?.rent?.comment ?? '',
  }))
  .on(dublicateRent, (state) => {
    const newState = { ...state };

    newState.id = null;
    newState.rentalDate = null;
    newState.rentalTime = [];

    return newState;
  })
  .reset([signout, pageUnmounted, fxGetRentItem, fxGetRentObjectData.done]);

/** Запрос данных после создания или обновления аренды */
sample({
  clock: [fxCreateRent.done, fxUpdateRent.done],
  fn: ({ result: { id } }) => ({ rent_id: id }),
  target: fxGetRentItem,
});

/** После создания в реестре в url добавляется id */
sample({
  clock: fxCreateRent.done,
  source: { path: $path, tab: $tab },
  filter: ({ tab }) => tab === 0,
  fn: ({ path }, { result: { id } }) => `${path}/${id}`,
  target: historyPush,
});

/** Обновление аренды */
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => Boolean(id),
  fn: (data) => {
    const payload: UpdateRentPayload = {
      rent_id: data.id as string,
      status_id: data.status as number,
      comment: data.comment,
    };

    if (
      data.rentalTime.length > 1 &&
      data.rentalTime[0] &&
      data.rentalTime[data.rentalTime.length - 1]
    ) {
      payload.rent_start = data.rentalTime[0].start;
      payload.rent_finish = data.rentalTime[data.rentalTime.length - 1].finish;
    }

    if (data.rentalTime.length === 1 && data.rentalTime[0]) {
      payload.rent_start = data.rentalTime[0].start;
      payload.rent_finish = data.rentalTime[0].finish;
    }

    return payload;
  },
  target: fxUpdateRent,
});

/** При дублировании из url убирается id */
sample({
  clock: dublicateRent,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

/** Формирование $opened при создании аренды после клика на график в мониторинге */
sample({
  clock: fxGetRentObjectData.done,
  source: { opened: $opened, values: dateForm.values },
  fn: ({ opened, values: { date } }, { params, result }) => {
    const newOpened = { ...opened };

    newOpened.rentalDate = date ?? null;

    if (params?.title && params?.start && params?.finish) {
      const [start, finish] = params.title.split(' - ');

      newOpened.rentalTime = [
        {
          id: params.id,
          label: `${start} - ${finish === '24:00' ? '00:00' : finish}`,
          start: params.start,
          finish: params.finish,
        },
      ];
    }

    newOpened.city = result?.city?.id ?? null;
    newOpened.timezone = result?.city?.offset ?? null;
    newOpened.complex = result?.complex?.id ?? null;
    newOpened.building = result?.building?.id ?? null;
    newOpened.floor = result?.floor?.id ?? null;
    newOpened.propertyType = result?.type?.id ?? null;
    newOpened.rentObject = result?.area?.id ?? null;

    return newOpened;
  },
  target: $opened,
});
