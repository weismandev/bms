export {
  $tab,
  changedTab,
  MainGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
} from './main';

export {
  changedDetailVisibility,
  addClicked,
  openViaUrl,
  $isDetailOpen,
  $opened,
  open,
  dublicateRent,
} from './detail';

export { detailForm } from './detail-form';
