import { signout } from '@features/common';
import { errorOccured as errorOccuredMonitoring } from '../../monitoring/models';
import { errorOccured as errorOccuredRegistry } from '../../registry/models';
import { errorOccured as errorOccuredDetail } from '../detail/detail.model';
import {
  $tab,
  changedTab,
  mainUnmounted,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
} from './main.model';

$tab.on(changedTab, (_, tab) => tab).reset([signout, mainUnmounted]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on([errorOccuredRegistry, errorOccuredMonitoring, errorOccuredDetail], () => true)
  .reset([signout, mainUnmounted]);

$error
  .on(
    [errorOccuredRegistry, errorOccuredMonitoring, errorOccuredDetail],
    (_, { error }) => error
  )
  .reset([signout, mainUnmounted]);
