import { createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { some } from 'patronum';
import { $isLoading as $isLoadingMonitoring } from '../../monitoring/models';
import { $isLoading as $isLoadingRegistry } from '../../registry/models';
import { $isLoading as $isLoadingDetail } from '../detail/detail.model';

export const MainGate = createGate();

export const mainUnmounted = MainGate.close;

export const changedTab = createEvent<number>();
export const changedErrorDialogVisibility = createEvent<boolean>();

export const $tab = createStore<number>(0);
export const $isLoading = some({
  stores: [$isLoadingRegistry, $isLoadingMonitoring, $isLoadingDetail],
  predicate: (value) => Boolean(value),
});
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
