export type FiltersForm = {
  cities: number[];
  complexes: string[];
  buildings: string[];
  floors: string[];
  propertyTypes: string[];
};
