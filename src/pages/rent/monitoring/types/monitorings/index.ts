import { ObjectItem } from '../../../types/rent';

export type GetMonitoringPayload = {
  time_date: number;
  cities?: number[];
  complexes?: string[];
  buildings?: string[];
  floors?: string[];
  types?: string[];
};

export type GetMonitoringResponse = {
  rent_monitoring: {
    timezone: { offset: number; title: string };
    areas: MonitoringArea[];
  };
};

export type MonitoringArea = {
  capacity: null;
  floor_number: number;
  id: string;
  payment: string;
  is_payment_required: boolean;
  rent_slots: {
    position: number;
    rent_finish: number;
    rent_info: {
      id: string;
      title: null;
      equipments: [];
      tenant: {
        id: number;
        name: string;
      };
    };
    rent_start: number;
    rent_status: { id: number; title: string };
    slot_title: string;
  }[];
  title: string;
  type_title: string;
};

export type MonitoringItem = {
  timezoneOffset: number | null;
  currentTime: string | null;
  currentTimePosition: number | null;
  areas: MonitoringItemArea[];
};

export type MonitoringItemArea = {
  id: string;
  title: string;
  isPaymentRequired: boolean;
  rentSlots: MonitoringRentSlot[];
};

export type MonitoringRentSlot = {
  id: string;
  title: string;
  status: number;
  slotTitle: string | null;
  rentStart: number | null;
  rentFinish: number | null;
  gridColumnStart?: number;
  gridColumnEnd?: number;
  rentId?: string;
  areaId: string;
};

export type GetRentObjectDataPayload = {
  id: string;
  title: string | null;
  start: number | null;
  finish: number | null;
  area_id: string;
};

export type GetObjectData = {
  id: string;
  title: string | null;
  start: number | null;
  finish: number | null;
  areaId: string;
};

export type GetRentObjectDataResponse = {
  city: { id: number; title: string; offset: number };
  complex: ObjectItem;
  building: ObjectItem;
  floor: ObjectItem;
  area: ObjectItem;
  type: ObjectItem;
};
