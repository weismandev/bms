import { FC } from 'react';
import { Tooltip } from '@mui/material';
import { MonitoringItemArea } from '../../types';
import * as Styled from './styled';

type Props = {
  areas: MonitoringItemArea[];
};

export const ContentTitleGraphic: FC<Props> = ({ areas = [] }) => (
  <Styled.Wrapper>
    {areas.map((item) => (
      <Tooltip title={item.title} key={item.id}>
        <Styled.Title variant="body1" noWrap>
          {item.title}
        </Styled.Title>
      </Tooltip>
    ))}
  </Styled.Wrapper>
);
