import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { columnWidth, cellHeight } from '../../models';

export const Wrapper = styled('div')(() => ({
  position: 'sticky',
  left: 0,
  zIndex: 5,
}));

export const Title = styled(Typography)(({ theme }) => ({
  width: columnWidth,
  fontSize: 14,
  fontWeight: 400,
  color: theme.palette.text.primary,
  height: cellHeight,
  padding: 10,
  backgroundColor: theme.palette.common.white,
  borderRight: `1px solid ${theme.palette.divider}`,
  borderLeft: `1px solid ${theme.palette.divider}`,
  borderBottom: `1px solid ${theme.palette.divider}`,
}));
