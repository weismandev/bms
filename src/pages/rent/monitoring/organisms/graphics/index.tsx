import { FC } from 'react';
import { useUnit } from 'effector-react';
import { nanoid } from 'nanoid';
import { CustomScrollbar } from '@ui/index';
import { GraphicsGate, $monitorings } from '../../models';
import { Legend, TimezoneAlert } from '../../molecules';
import { ContentGraphic } from '../content-graphic';
import { ContentTitleGraphic } from '../content-title-graphic';
import { HeaderGraphic } from '../header-graphic';
import { MonitoringToolbar } from '../monitoring-toolbar';
import * as Styled from './styled';

export const Graphics: FC = () => {
  const monitorings = useUnit($monitorings);

  return (
    <>
      <GraphicsGate />

      <Styled.Wrapper>
        <MonitoringToolbar />
        <Styled.Graphics>
          <CustomScrollbar>
            {monitorings.map((item) => (
              <Styled.WrapperGraphic key={nanoid(3)}>
                <Legend />
                <TimezoneAlert timezone={item?.timezoneOffset} />
                <Styled.Graphic>
                  <CustomScrollbar>
                    <HeaderGraphic
                      currentTime={item?.currentTime}
                      currentTimePosition={item?.currentTimePosition}
                    />
                    <Styled.Content>
                      <ContentTitleGraphic areas={item?.areas} />
                      <ContentGraphic
                        areas={item?.areas}
                        currentTimePosition={item?.currentTimePosition}
                      />
                    </Styled.Content>
                  </CustomScrollbar>
                </Styled.Graphic>
              </Styled.WrapperGraphic>
            ))}
          </CustomScrollbar>
        </Styled.Graphics>
      </Styled.Wrapper>
    </>
  );
};
