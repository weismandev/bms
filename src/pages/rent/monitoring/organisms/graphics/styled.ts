import { styled } from '@mui/material/styles';

export const Wrapper = styled('div')(() => ({
  padding: '0px 24px 24px',
  height: '100%',
}));

export const Graphics = styled('div')(() => ({
  position: 'relative',
  height: 'calc(100% - 118px)',
  marginRight: -24,
}));

export const WrapperGraphic = styled('div')(() => ({
  height: 495,
  marginRight: 24,
}));

export const Graphic = styled('div')(() => ({
  height: 'calc(100% - 160px)',
}));

export const Content = styled('div')(() => ({
  display: 'flex',
  border: 0,
  width: 'fit-content',
  position: 'relative',
  zIndex: 3,
}));
