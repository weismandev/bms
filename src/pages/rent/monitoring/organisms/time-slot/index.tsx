import { FC } from 'react';
import format from 'date-fns/format';
import { nanoid } from 'nanoid';
import {
  coefficientMinutes,
  coefficientForConvertSecondsToMilliseconds,
  zeroTimeValue,
  gridStep,
  gridCountElemets,
} from '../../models';
import * as Styled from './styled';

const startElement = 1;
const minutesForAdd = 30;
const date = new Date();
const currentDate = date.setHours(
  zeroTimeValue,
  zeroTimeValue,
  zeroTimeValue,
  zeroTimeValue
);
const coefficientForConvertMinutesToMilliseconds =
  coefficientMinutes * coefficientForConvertSecondsToMilliseconds;
const array = new Array(gridCountElemets).fill('');

type Props = {
  currentTime: string | null;
  currentTimePosition: number | null;
};

export const TimeSlot: FC<Props> = ({ currentTime, currentTimePosition }) => {
  let start;
  let end = startElement;

  return (
    <>
      {currentTime && currentTimePosition && (
        <Styled.CurrentTime currentTimePosition={currentTimePosition}>
          {currentTime}
        </Styled.CurrentTime>
      )}
      {array.map((_: unknown, index: number) => {
        start = end;
        end += gridStep;

        const time =
          index === 0
            ? format(new Date(currentDate), 'HH:mm')
            : format(
                new Date(
                  date.getTime() +
                    minutesForAdd * index * coefficientForConvertMinutesToMilliseconds
                ),
                'HH:mm'
              );

        return (
          <Styled.SlotItem key={nanoid(3)} gridColumn={`${start} / ${end}`}>
            <Styled.SlotTitle>{time}</Styled.SlotTitle>
          </Styled.SlotItem>
        );
      })}
    </>
  );
};
