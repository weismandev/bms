import { Typography } from '@mui/material';
import { styled, Theme } from '@mui/material/styles';
import { columnWidth, cellWidth } from '../../models';

export const SlotItem = styled('div')(
  ({ gridColumn, theme }: { gridColumn: string; theme?: Theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    padding: '12px 0px 10px 5px',
    borderTop: `1px solid ${theme?.palette.divider as string}`,
    borderBottom: `1px solid ${theme?.palette.divider as string}`,
    gridColumn,

    '&:last-child': {
      borderRight: `1px solid ${theme?.palette.divider as string}`,
      marginRight: 20,
    },
  })
);

export const SlotTitle = styled(Typography)(({ theme }) => ({
  fontSize: 12,
  fontWeight: 400,
  color: theme.palette.text.secondary,
}));

export const CurrentTime = styled('div')(
  ({ currentTimePosition, theme }: { currentTimePosition: number; theme?: Theme }) => ({
    position: 'absolute',
    border: `1px solid ${theme?.palette.primary.main as string}`,
    color: theme?.palette.primary.main,
    backgroundColor: theme?.palette.common.white,
    borderRadius: 12,
    left: columnWidth + currentTimePosition * cellWidth - 19,
    marginTop: 7,
    fontWeight: 400,
    fontSize: 12,
    padding: 4,
  })
);
