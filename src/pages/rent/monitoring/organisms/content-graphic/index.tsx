import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Tooltip } from '@mui/material';
import RentPayment from '@img/rent-payment.svg';
import { open, $opened } from '../../../models';
import { lastGridColumnEnd, getObjectData } from '../../models';
import { CurrentTimeLine } from '../../molecules';
import { MonitoringItemArea } from '../../types';
import * as Styled from './styled';

type Props = {
  areas: MonitoringItemArea[];
  currentTimePosition: number | null;
};

export const ContentGraphic: FC<Props> = ({ areas = [], currentTimePosition = 0 }) => {
  const { t } = useTranslation();
  const opened = useUnit($opened);

  return (
    <Styled.Content>
      {areas.map((row) => {
        if (row.rentSlots.length === 0) {
          return (
            <>
              <CurrentTimeLine
                currentTimePosition={currentTimePosition}
                countColumns={areas.length}
              />
              <Styled.FreeCell gridColumnStart={1} gridColumnEnd={lastGridColumnEnd} />
            </>
          );
        }

        const slots = row.rentSlots.map((slot) => {
          if (!slot?.gridColumnStart || !slot?.gridColumnEnd) {
            return null;
          }

          const isOpenSlot = opened?.id === slot.rentId;

          const isOpenFreeSlot =
            !opened?.id &&
            opened?.rentalTime[0]?.id === slot.id &&
            opened?.rentObject === slot.areaId;

          const handleSlotClick = () => (slot?.rentId ? open(slot.rentId) : null);

          const handleAddSlotClick = () =>
            getObjectData({
              id: slot.id,
              title: slot.slotTitle,
              start: slot.rentStart,
              finish: slot.rentFinish,
              areaId: slot.areaId,
            });

          const getCell = (id: number) => {
            switch (id) {
              case 1:
                return (
                  <Styled.FreeSlot
                    statusId={slot.status}
                    onClick={handleAddSlotClick}
                    isOpenFreeSlot={isOpenFreeSlot}
                  />
                );
              case 9:
                return <Styled.NotAvailableSlot statusId={slot.status} />;
              default:
                return (
                  <Styled.Cell
                    status={slot.status}
                    isClickableSlot={Boolean(slot?.rentId)}
                    onClick={handleSlotClick}
                    isOpenSlot={isOpenSlot}
                  >
                    <Styled.CellContent>
                      <Tooltip title={slot.title}>
                        <Styled.Title variant="body2" noWrap>
                          {slot.title}
                        </Styled.Title>
                      </Tooltip>
                      {row.isPaymentRequired && (
                        <Styled.CellIcon>
                          <Tooltip title={t('PaymentRequired')}>
                            <div>
                              <RentPayment />
                            </div>
                          </Tooltip>
                        </Styled.CellIcon>
                      )}
                    </Styled.CellContent>
                  </Styled.Cell>
                );
            }
          };

          return (
            <Styled.WrapperCell
              key={slot.id}
              gridColumnStart={slot.gridColumnStart}
              gridColumnEnd={slot.gridColumnEnd}
            >
              {getCell(slot.status)}
            </Styled.WrapperCell>
          );
        });

        const slotFirstGridColumnStart = row.rentSlots.reduce(
          (acc, slot) =>
            slot.gridColumnStart ? Math.min(acc, slot.gridColumnStart) : acc,
          lastGridColumnEnd
        );

        const slotLastGridColumnEnd = row.rentSlots.reduce(
          (acc, slot) => (slot.gridColumnEnd ? Math.max(acc, slot.gridColumnEnd) : acc),
          0
        );

        return (
          <>
            <CurrentTimeLine
              currentTimePosition={currentTimePosition}
              countColumns={areas.length}
            />
            {slotFirstGridColumnStart > 1 &&
              slotFirstGridColumnStart !== lastGridColumnEnd && (
                <Styled.WrapperCell
                  gridColumnStart={1}
                  gridColumnEnd={slotFirstGridColumnStart}
                />
              )}
            {slots}
            {slotLastGridColumnEnd !== 0 && slotLastGridColumnEnd < lastGridColumnEnd && (
              <Styled.FreeCell
                gridColumnStart={slotLastGridColumnEnd}
                gridColumnEnd={lastGridColumnEnd}
              />
            )}
          </>
        );
      })}
    </Styled.Content>
  );
};
