import { Paper, Typography } from '@mui/material';
import { styled, Theme } from '@mui/material/styles';
import { getStatusColors } from '../../../libs';
import { cellWidth, cellHeight, lastGridColumnEnd, slotHeight } from '../../models';

export const Content = styled('div')(({ theme }) => ({
  display: 'grid',
  gridTemplateColumns: `repeat(${lastGridColumnEnd}, ${cellWidth}px)`,
  gridAutoRows: cellHeight,
  backgroundColor: theme.palette.common.white,
}));

export const WrapperCell = styled('div')(
  ({
    gridColumnStart,
    gridColumnEnd,
    theme,
  }: {
    gridColumnStart: number;
    gridColumnEnd: number;
    theme?: Theme;
  }) => ({
    borderBottom: `1px solid ${theme?.palette.divider as string}`,
    gridColumnStart,
    gridColumnEnd,
  })
);

export const Cell = styled(Paper)(
  ({
    status,
    isClickableSlot,
    isOpenSlot,
    theme,
  }: {
    status: number;
    isClickableSlot: boolean;
    isOpenSlot: boolean;
    theme?: Theme;
    children: JSX.Element;
  }) => {
    const color = getStatusColors(status, theme).background as string;

    return {
      marginTop: 12,
      padding: 5,
      height: slotHeight,
      borderBottom: `4px solid ${color}`,
      cursor: isClickableSlot ? 'pointer' : 'auto',
      background: isOpenSlot ? theme?.palette.primary.light : 'transparent',
    };
  }
);

export const FreeCell = styled('div')(
  ({
    gridColumnStart,
    gridColumnEnd,
    theme,
  }: {
    gridColumnStart: number;
    gridColumnEnd: number;
    theme?: Theme;
  }) => ({
    borderRight: `1px solid ${theme?.palette.divider as string}`,
    borderBottom: `1px solid ${theme?.palette.divider as string}`,
    gridColumnStart,
    gridColumnEnd,
  })
);

export const Title = styled(Typography)(({ theme }) => ({
  fontWeight: 400,
  fontSize: 14,
  color: theme.palette.text.primary,
}));

export const CellIcon = styled('div')(() => ({
  marginTop: -2,
}));

export const FreeSlot = styled('div')(
  ({
    statusId,
    isOpenFreeSlot,
    theme,
  }: {
    statusId: number;
    isOpenFreeSlot: boolean;
    theme?: Theme;
  }) => ({
    marginTop: 12,
    height: slotHeight,
    border: `1px solid ${theme?.palette.success.dark as string}`,
    borderRadius: 8,
    background: isOpenFreeSlot
      ? theme?.palette.primary.light
      : getStatusColors(statusId, theme).background,
    cursor: 'pointer',
  })
);

export const CellContent = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  padding: '4px 8px',
}));

export const NotAvailableSlot = styled('div')(
  ({ statusId, theme }: { statusId: number; theme?: Theme }) => ({
    marginTop: 12,
    height: slotHeight,
    border: `1px solid ${theme?.palette.grey[400] as string}`,
    borderRadius: 8,
    background: getStatusColors(statusId, theme).background,
  })
);
