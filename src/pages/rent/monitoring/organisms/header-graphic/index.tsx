import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { TimeSlot } from '../time-slot';
import * as Styled from './styled';

type Props = {
  currentTime: string | null;
  currentTimePosition: number | null;
};

export const HeaderGraphic: FC<Props> = ({ currentTime, currentTimePosition }) => {
  const { t } = useTranslation();

  return (
    <Styled.Wrapper>
      <Styled.Content>
        <Styled.Title variant="body1">{t('Booking')}</Styled.Title>
        <Styled.Columns>
          <TimeSlot currentTime={currentTime} currentTimePosition={currentTimePosition} />
        </Styled.Columns>
      </Styled.Content>
    </Styled.Wrapper>
  );
};
