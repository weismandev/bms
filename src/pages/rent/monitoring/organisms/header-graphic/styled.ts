import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { columnWidth, cellWidth, lastGridColumnEnd } from '../../models';

export const Wrapper = styled('div')(() => ({
  width: 'fit-content',
  position: 'sticky',
  top: 0,
  zIndex: 6,
}));

export const Content = styled('div')(() => ({
  display: 'flex',
  border: 0,
  position: 'sticky',
  top: 0,
}));

export const Title = styled(Typography)(({ theme }) => ({
  minWidth: columnWidth,
  position: 'sticky',
  left: 0,
  height: 42,
  padding: '10px 0px 10px 10px',
  border: `1px solid ${theme.palette.divider}`,
  fontWeight: 500,
  background: theme.palette.common.white,
  zIndex: 5,
  color: theme.palette.text.primary,
  fontSize: 14,
}));

export const Columns = styled('div')(({ theme }) => ({
  display: 'grid',
  gridTemplateColumns: `repeat(${lastGridColumnEnd}, ${cellWidth}px)`,
  gridAutoRows: 42,
  background: theme.palette.common.white,
}));
