import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Wrapper = styled(Paper)(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 15,
  height: '100%',
  padding: '24px 0px 24px 24px',
}));

export const Form = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 20,
  paddingRight: 24,
  marginBottom: 10,
}));

export const Container = styled('div')(() => ({
  paddingRight: 24,
}));
