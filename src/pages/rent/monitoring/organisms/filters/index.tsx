import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { Divider } from '@mui/material';
import { CustomScrollbar } from '@ui/index';
import { FilterFooter, FilterToolbar } from '../../../organisms';
import { filtersForm, filtersEntity, changedFilterVisibility } from '../../models';
import * as Styled from './styled';

export const Filters: FC = () => {
  const { t } = useTranslation();
  const { values } = useUnit(filtersForm);

  const onClose = () => changedFilterVisibility(false);

  return (
    <Styled.Wrapper>
      <Styled.Container>
        <FilterToolbar onClose={onClose} />
      </Styled.Container>
      <CustomScrollbar>
        <Styled.Form>
          <Control.Cities
            name="cities"
            label={t('Label.city') ?? ''}
            form={filtersForm}
          />
          <Divider />
          <Control.RentComplexes
            name="complexes"
            label={t('complex') ?? ''}
            cities={values.cities}
            form={filtersForm}
          />
          <Control.RentBuildings
            name="buildings"
            label={t('building') ?? ''}
            complexes={values.complexes}
            form={filtersForm}
          />
          <Control.RentFloors
            name="floors"
            label={t('Floor') ?? ''}
            buildings={values.buildings}
            form={filtersForm}
          />
          <Control.PropertyTypes
            name="propertyTypes"
            label={t('RoomType') ?? ''}
            form={filtersForm}
          />
          <Divider />
        </Styled.Form>
      </CustomScrollbar>
      <Styled.Container>
        <FilterFooter form={filtersForm} filtersEntity={filtersEntity} />
      </Styled.Container>
    </Styled.Wrapper>
  );
};
