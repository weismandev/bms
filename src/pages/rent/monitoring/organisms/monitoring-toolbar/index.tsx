import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { IconButton, Button, Tooltip } from '@mui/material';
import {
  FilterAltOutlined,
  CalendarTodayOutlined,
  ArrowBackIosOutlined,
  ArrowForwardIosOutlined,
} from '@mui/icons-material';
import { addClicked, detailForm, $opened } from '../../../models';
import {
  $isFilterOpen,
  changedFilterVisibility,
  dateForm,
  previousDay,
  nextDay,
} from '../../models';
import * as Styled from './styled';

export const MonitoringToolbar: FC = () => {
  const { t } = useTranslation();
  const { editable } = useUnit(detailForm);
  const [isFilterOpen, opened] = useUnit([$isFilterOpen, $opened]);

  const isDisabledAddButton = editable && !opened.id;

  const onClick = () => changedFilterVisibility(true);
  const onClickAdd = () => addClicked();
  const onClickPreviousDay = () => previousDay();
  const onClickNextDay = () => nextDay();

  return (
    <Styled.Wrapper>
      <IconButton disabled={isFilterOpen} onClick={onClick}>
        <Tooltip title={t('Filters')}>
          <FilterAltOutlined />
        </Tooltip>
      </IconButton>
      <Styled.DateControl>
        <IconButton onClick={onClickPreviousDay}>
          <Tooltip title={t('PreviousDay')}>
            <ArrowBackIosOutlined fontSize="medium" />
          </Tooltip>
        </IconButton>
        <Control.Date name="date" form={dateForm} />
        <IconButton onClick={onClickNextDay}>
          <Tooltip title={t('NextDay')}>
            <ArrowForwardIosOutlined fontSize="medium" />
          </Tooltip>
        </IconButton>
      </Styled.DateControl>
      <Styled.Greedy />
      <Button
        variant="contained"
        color="primary"
        onClick={onClickAdd}
        disabled={isDisabledAddButton}
        startIcon={<CalendarTodayOutlined />}
      >
        {t('Book')}
      </Button>
    </Styled.Wrapper>
  );
};
