import { styled } from '@mui/material/styles';

export const Wrapper = styled('div')(() => ({
  display: 'flex',
  width: '100%',
  gap: 10,
  margin: '20px 0px 10px 0px',
}));

export const Greedy = styled('div')(() => ({
  flexGrow: 13,
}));

export const DateControl = styled('div')(() => ({
  display: 'flex',
  gap: 4,
}));
