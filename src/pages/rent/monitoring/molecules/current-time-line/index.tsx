import { FC } from 'react';
import * as Styled from './styled';

type Props = {
  currentTimePosition: number | null;
  countColumns: number;
};

export const CurrentTimeLine: FC<Props> = ({ currentTimePosition, countColumns }) => {
  if (currentTimePosition && countColumns > 0) {
    return (
      <Styled.CurrentTimeLine
        currentTimePosition={currentTimePosition}
        countColumns={countColumns}
      />
    );
  }

  return null;
};
