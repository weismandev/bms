import { styled, Theme } from '@mui/material/styles';
import { cellHeight, columnWidth, cellWidth } from '../../models';

export const CurrentTimeLine = styled('div')(
  ({
    currentTimePosition,
    countColumns,
    theme,
  }: {
    currentTimePosition: number;
    countColumns: number;
    theme?: Theme;
  }) => ({
    borderLeft: `2px solid ${theme?.palette.primary.main as string}`,
    height: countColumns * cellHeight - 1,
    position: 'absolute',
    left: columnWidth + currentTimePosition * cellWidth,
  })
);
