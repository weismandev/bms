import { Typography as TypographyMui } from '@mui/material';
import { styled, Theme } from '@mui/material/styles';
import { Brightness1 as Brightness1Mui } from '@mui/icons-material';
import { getStatusColors } from '../../../libs';

export const LegendWrapper = styled('div')(() => ({
  display: 'flex',
  gap: 10,
  marginBottom: 10,
}));

export const LegendItemWrapper = styled('div')(() => ({
  display: 'flex',
  gap: 5,
  whiteSpace: 'nowrap',
  overflow: 'hidden',
}));

export const Brightness1 = styled(Brightness1Mui)(
  ({ statusId, theme }: { statusId: number; theme?: Theme }) => ({
    width: 10,
    height: 10,
    marginTop: 4,
    color: getStatusColors(statusId, theme).background,
  })
);

export const Typography = styled(TypographyMui)(({ theme }) => ({
  fontWeight: 400,
  fontSize: 12,
  color: theme.palette.text.secondary,
}));
