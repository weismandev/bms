import { FC } from 'react';
import i18n from '@shared/config/i18n';
import * as Styled from './styled';

const { t } = i18n;

const statuses = [
  { id: 1, title: t('Free') },
  { id: 4, title: t('Booked') },
  { id: 5, title: t('RequiresProofOfUse') },
  { id: 2, title: t('BookingConfirmationRequired') },
  { id: 3, title: t('PaymentExpected') },
  { id: 6, title: t('Busy') },
  { id: 8, title: t('CompletedIt') },
  { id: 9, title: t('NotAvailable') },
];

export const Legend: FC = () => (
  <Styled.LegendWrapper>
    {statuses.map((status) => (
      <Styled.LegendItemWrapper key={status.id}>
        <Styled.Brightness1 statusId={status.id} />
        <Styled.Typography noWrap>{status.title}</Styled.Typography>
      </Styled.LegendItemWrapper>
    ))}
  </Styled.LegendWrapper>
);
