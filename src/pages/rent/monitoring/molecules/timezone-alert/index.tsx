import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import * as Styled from './styled';

type Props = {
  timezone: number | null;
};

export const TimezoneAlert: FC<Props> = ({ timezone }) => {
  const { t } = useTranslation();

  if (!Number.isInteger(timezone)) {
    return null;
  }

  return (
    <Styled.Alert severity="info">
      <Styled.AlertTitle>
        {`${t('UTSTime')} (${
          (timezone as number) > 0 ? `+${timezone as number}` : (timezone as number)
        })`}
      </Styled.AlertTitle>
    </Styled.Alert>
  );
};
