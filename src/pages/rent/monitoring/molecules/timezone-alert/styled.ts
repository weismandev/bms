import { Alert as AlertMui, AlertTitle as AlertTitleMui } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Alert = styled(AlertMui)(() => ({
  marginBottom: 20,
}));

export const AlertTitle = styled(AlertTitleMui)(() => ({
  fontWeight: 400,
  fontSize: 14,
  marginTop: 1,
}));
