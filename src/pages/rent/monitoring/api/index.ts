import { api } from '@api/api2';
import { GetMonitoringPayload, GetRentObjectDataPayload } from '../types';

const getMonitoring = (payload: GetMonitoringPayload) =>
  api.v4('get', 'rent/monitoring', payload);
const getRentObjectData = (payload: GetRentObjectDataPayload) =>
  api.v4('get', 'rent/area', payload);

export const monitoringApi = {
  getMonitoring,
  getRentObjectData,
};
