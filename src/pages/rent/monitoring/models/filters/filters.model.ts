import { createFilterBag } from '@tools/factories';

export const filtersEntity = {
  cities: [],
  complexes: [],
  buildings: [],
  floors: [],
  propertyTypes: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(filtersEntity);
