import { signout } from '@features/common';
import { graphicsUnmounted } from '../graphics/graphics.model';
import { $isFilterOpen, $filters } from './filters.model';

$isFilterOpen.reset([signout, graphicsUnmounted]);
$filters.reset([signout, graphicsUnmounted]);
