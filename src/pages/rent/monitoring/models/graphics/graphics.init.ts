import { sample } from 'effector';
import { pending, throttle } from 'patronum';
import format from 'date-fns/format';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { formatDate } from '../../../libs';
import { fxUpdateRent, fxCreateRent } from '../../../models/detail/detail.model';
import { monitoringApi } from '../../api';
import {
  MonitoringArea,
  MonitoringRentSlot,
  GetMonitoringPayload,
  MonitoringItem,
} from '../../types';
import { dateForm } from '../date-form/date-form.model';
import { $filters } from '../filters/filters.model';
import {
  fxGetMonitoring,
  graphicsMounted,
  graphicsUnmounted,
  $isLoading,
  $monitorings,
  coefficientForConvertSecondsToHours,
  coefficientForConvertSecondsToMilliseconds,
  coefficientMinutes,
  countStep,
  coefficientMinutesForGrid,
  GraphicsGate,
  stepMinutesFor15Minutes,
  stepMinutes,
  quarterHour,
  maximumMinutesInHours,
  coefficientMinutesForCurrentTime,
  zeroTimeValue,
  fxGetRentObjectData,
  getObjectData,
  minYearValue,
} from './graphics.model';

const { t } = i18n;

fxGetMonitoring.use(monitoringApi.getMonitoring);
fxGetRentObjectData.use(monitoringApi.getRentObjectData);

$isLoading
  .on(
    pending({
      effects: [fxGetMonitoring, fxGetRentObjectData],
    }),
    (_, loading) => loading
  )
  .reset([graphicsUnmounted, signout]);

const throttledSetValuesDateForm = throttle({
  source: dateForm.setValue,
  timeout: 1000,
});

/** Запрос графиков  */
sample({
  clock: [
    graphicsMounted,
    throttledSetValuesDateForm,
    $filters,
    fxUpdateRent.done,
    fxCreateRent.done,
  ],
  source: { form: dateForm.values, status: GraphicsGate.status, filters: $filters },
  filter: ({ form: { date }, status }) => {
    if (status && Boolean(date && new Date(date).toString() !== 'Invalid Date')) {
      const year = date.getFullYear();

      return year >= minYearValue;
    }

    return false;
  },
  fn: ({ form: { date }, filters }) => {
    const payload: GetMonitoringPayload = {
      time_date: formatDate(date).getTime() / coefficientForConvertSecondsToMilliseconds,
    };

    if (filters.cities.length > 0) {
      payload.cities = filters.cities;
    }

    if (filters.complexes.length > 0) {
      payload.complexes = filters.complexes;
    }

    if (filters.buildings.length > 0) {
      payload.buildings = filters.buildings;
    }

    if (filters.floors.length > 0) {
      payload.floors = filters.floors;
    }

    if (filters.propertyTypes.length > 0) {
      payload.types = filters.propertyTypes;
    }

    return payload;
  },
  target: fxGetMonitoring,
});

const formatTimestamp = (timestamp?: number, offset?: number) => {
  if (timestamp && offset) {
    const currentOffset = new Date().getTimezoneOffset() * coefficientMinutes;

    return format(
      new Date(
        (timestamp + currentOffset + offset) * coefficientForConvertSecondsToMilliseconds
      ),
      'HH:mm'
    );
  }

  return null;
};

/** Шаги с минутами для слотов */
const getMinutesStep = () => {
  const array = [];
  let step = 0;

  for (let i = 1; i < coefficientMinutes; i += quarterHour) {
    const filledArray = new Array(quarterHour)
      .fill(step)
      .map((item: number, index) => item + stepMinutesFor15Minutes[index]);

    array.push(filledArray);
    step += stepMinutes;
  }

  array[array.length - 1].splice(-1, 1, countStep);

  return array
    .flat()
    .reduce((acc: { [key: number]: number }, current, i) => ({ ...acc, [i]: current }), {
      [maximumMinutesInHours]: countStep,
    });
};

/** Форматирование данных после получения с бэка */
sample({
  clock: fxGetMonitoring.done,
  source: dateForm.values,
  fn: ({ date }, { result }) => {
    if (!Array.isArray(result?.rent_monitoring)) {
      return [];
    }

    const currentDate = new Date();
    const currentDateForComparison = new Date(currentDate);

    currentDateForComparison.setHours(
      zeroTimeValue,
      zeroTimeValue,
      zeroTimeValue,
      zeroTimeValue
    );

    return result.rent_monitoring.map((item) => {
      const monitoring: MonitoringItem = {
        timezoneOffset: item.timezone.offset
          ? item.timezone.offset / coefficientForConvertSecondsToHours
          : null,
        currentTime: null,
        currentTimePosition: null,
        areas: Array.isArray(item?.areas)
          ? item.areas.map((area: MonitoringArea) => ({
              id: area.id,
              title: area?.title ?? '',
              isPaymentRequired: Boolean(area?.is_payment_required),
              rentSlots: Array.isArray(area?.rent_slots)
                ? area.rent_slots.map((slot) => {
                    const data: MonitoringRentSlot = {
                      id: `${slot?.rent_start}-${slot?.rent_finish}`,
                      title: slot?.rent_info?.tenant?.name ?? t('Unknown'),
                      rentId: slot?.rent_info?.id,
                      status: slot?.rent_status?.id,
                      slotTitle: slot?.slot_title ?? null,
                      rentStart: slot?.rent_start ?? null,
                      rentFinish: slot?.rent_finish ?? null,
                      areaId: area.id,
                    };

                    const timeInterval = slot?.slot_title
                      ? slot.slot_title.trim().split('-')
                      : null;

                    if (timeInterval) {
                      const minutesStep = getMinutesStep();

                      const start = timeInterval[0].split(':').map(Number);
                      const finish = timeInterval[1].split(':').map(Number);

                      if (start) {
                        data.gridColumnStart =
                          start[0] * countStep +
                          minutesStep[start[1]] +
                          coefficientMinutesForGrid;
                      }

                      if (finish) {
                        data.gridColumnEnd =
                          finish[0] * countStep +
                          minutesStep[finish[1]] +
                          coefficientMinutesForGrid;
                      }
                    }

                    return data;
                  })
                : [],
            }))
          : [],
      };

      if (
        date &&
        new Date(date).toString() !== 'Invalid Date' &&
        date.getTime() === currentDateForComparison.getTime()
      ) {
        const minutesStep = getMinutesStep();

        const timestamp =
          currentDate.getTime() / coefficientForConvertSecondsToMilliseconds;

        const time = formatTimestamp(timestamp, item.timezone.offset as number);

        if (time) {
          monitoring.currentTime = time;

          const splittedTime = time.split(':').map(Number);

          monitoring.currentTimePosition =
            splittedTime[0] * countStep +
            minutesStep[splittedTime[1]] +
            coefficientMinutesForCurrentTime;
        }
      }

      return monitoring;
    });
  },
  target: $monitorings,
});

$monitorings.reset([graphicsUnmounted, signout]);

/** Запрос объектов у свободного слота */
sample({
  clock: getObjectData,
  fn: (data) => ({
    id: data.id,
    title: data.title,
    start: data.start,
    finish: data.finish,
    area_id: data.areaId,
  }),
  target: fxGetRentObjectData,
});
