import { createEffect, createStore, createEvent, merge } from 'effector';
import { createGate } from 'effector-react';
import {
  GetMonitoringResponse,
  MonitoringItem,
  GetMonitoringPayload,
  GetObjectData,
  GetRentObjectDataPayload,
  GetRentObjectDataResponse,
} from '../../types';

export const coefficientForConvertSecondsToHours = 3600;
export const coefficientForConvertSecondsToMilliseconds = 1000;
export const coefficientMinutes = 60;
export const countStep = 8;
export const coefficientMinutesForGrid = 2;
export const stepMinutesFor15Minutes = [
  0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1, 1.15, 1.3, 1.45, 1.6, 1.75, 1.9, 1.95,
];
export const stepMinutes = 2;
export const quarterHour = 15;
export const maximumMinutesInHours = 59;
export const coefficientMinutesForCurrentTime = 0.95;
export const zeroTimeValue = 0;
export const columnWidth = 200;
export const cellWidth = 20.4;
export const cellHeight = 62;
export const gridStep = 4;
export const gridCountElemets = 49;
export const lastGridColumnEnd = gridStep * gridCountElemets - 1;
export const slotHeight = 38;
export const minYearValue = 1970;

export const GraphicsGate = createGate();

export const graphicsUnmounted = GraphicsGate.close;
export const graphicsMounted = GraphicsGate.open;

export const getObjectData = createEvent<GetObjectData>();

export const $isLoading = createStore<boolean>(false);
export const $monitorings = createStore<MonitoringItem[]>([]);

export const fxGetMonitoring = createEffect<
  GetMonitoringPayload,
  GetMonitoringResponse,
  Error
>();
export const fxGetRentObjectData = createEffect<
  GetRentObjectDataPayload,
  GetRentObjectDataResponse,
  Error
>();

export const errorOccured = merge([fxGetMonitoring.fail, fxGetRentObjectData.fail]);
