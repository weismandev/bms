import { sample } from 'effector';
import { signout } from '@features/common';
import { graphicsUnmounted } from '../graphics/graphics.model';
import { dateForm, nextDay, previousDay, stepDay } from './date-form.model';

/** При уходе сбрасывается форма */
sample({
  clock: [signout, graphicsUnmounted],
  target: dateForm.reset,
});

/** Переключение на прошлый день */
sample({
  clock: previousDay,
  source: dateForm.values,
  fn: ({ date }) => {
    const newDate = new Date(date);

    newDate.setDate(date.getDate() - stepDay);

    return { path: 'date', value: newDate };
  },
  target: dateForm.setValue,
});

/** Переключение на следующий день */
sample({
  clock: nextDay,
  source: dateForm.values,
  fn: ({ date }) => {
    const newDate = new Date(date);

    newDate.setDate(date.getDate() + stepDay);

    return { path: 'date', value: newDate };
  },
  target: dateForm.setValue,
});
