import { createEvent } from 'effector';
import { createForm } from '@unicorn/effector-form';
import { formatDate } from '../../../libs';
import { DateForm } from '../../types';

const currentDate = formatDate(new Date());
export const stepDay = 1;

export const dateForm = createForm<DateForm>({
  initialValues: {
    date: currentDate,
  },
  editable: true,
});

export const previousDay = createEvent<void>();
export const nextDay = createEvent<void>();
