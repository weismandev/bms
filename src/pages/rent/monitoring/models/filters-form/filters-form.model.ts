import { createForm } from '@unicorn/effector-form';
import { FiltersForm } from '../../types';
import { $filters } from '../filters/filters.model';

export const filtersForm = createForm<FiltersForm>({
  initialValues: $filters,
  editable: true,
});
