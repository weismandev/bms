import { sample } from 'effector';
import { filtersEntity, filtersSubmitted } from '../filters/filters.model';
import { graphicsUnmounted } from '../graphics/graphics.model';
import { filtersForm } from './filters-form.model';

/** При уходе с мониторинга сбрасывается форма */
sample({
  clock: graphicsUnmounted,
  target: filtersForm.reset,
});

/** После самбита формы, принять текущее состояние формы за исходное */
sample({
  clock: filtersForm.submitted,
  target: [filtersForm.initCurrent, filtersForm.initValues, filtersSubmitted],
});

/** При сбросе данные запрашиваются без фильтров */
sample({
  clock: filtersForm.reset,
  fn: () => ({ ...filtersEntity }),
  target: filtersForm.submitted,
});

/** При изменении города сбрасываются зависимые поля */
sample({
  clock: filtersForm.setValue,
  filter: ({ path }) => path === 'cities',
  fn: () => ({
    complexes: [],
    buildings: [],
    floors: [],
  }),
  target: filtersForm.updateValues,
});

/** При изменении жк сбрасываются зависимые поля */
sample({
  clock: filtersForm.setValue,
  filter: ({ path }) => path === 'complexes',
  fn: () => ({
    buildings: [],
    floors: [],
  }),
  target: filtersForm.updateValues,
});

/** При изменении зданий сбрасываются зависимые поля */
sample({
  clock: filtersForm.setValue,
  filter: ({ path }) => path === 'buildings',
  fn: () => ({
    floors: [],
  }),
  target: filtersForm.updateValues,
});
