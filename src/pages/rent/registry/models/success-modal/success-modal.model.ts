import { createEvent, createStore } from 'effector';

export const closeSuccessModal = createEvent<void>();

export const $isOpenSuccessModal = createStore<boolean>(false);
