import { signout } from '@features/common';
import { pageUnmounted } from '../page/page.model';
import { $isOpenSuccessModal, closeSuccessModal } from './success-modal.model';

$isOpenSuccessModal.reset([signout, pageUnmounted, closeSuccessModal]);
