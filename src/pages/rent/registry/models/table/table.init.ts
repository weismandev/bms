import { sample } from 'effector';
import { signout, $pathname } from '@features/common';
import { getIdInUrl } from '../../../libs';
import {
  fxCreateRent,
  addClicked,
  detailClosed,
  dublicateRent,
} from '../../../models/detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

$openRow
  .on(fxCreateRent.done, (_, { result: { id } }) => id)
  .reset([signout, pageUnmounted, addClicked, detailClosed, dublicateRent]);

/** Если при маунте есть id в url, то выбирается строка с нужной записью */
sample({
  clock: pageMounted,
  source: { pathname: $pathname, openRow: $openRow },
  filter: ({ pathname, openRow }) => {
    const id = getIdInUrl(pathname as string);

    return Boolean(id) && !openRow;
  },
  fn: ({ pathname }) => getIdInUrl(pathname as string),
  target: $openRow,
});
