import { Chip as ChipMui } from '@mui/material';
import { styled, Theme } from '@mui/material/styles';
import { getStatusColors } from '../../../libs';

export const Chip = styled(ChipMui)(
  ({ value, theme }: { value: number; theme?: Theme }) => getStatusColors(value, theme)
);
