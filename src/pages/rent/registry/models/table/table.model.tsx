import format from 'date-fns/format';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { formatPhone } from '@tools/formatPhone';
import { RentItem } from '../../types';
import { $rawData, coefficientForSeconds } from '../page/page.model';
import * as Styled from './styled';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('BookingObject') ?? '',
    width: 250,
    sortable: true,
    hideable: false,
  },
  {
    field: 'propertyType',
    headerName: t('RoomType') ?? '',
    width: 200,
    sortable: true,
    hideable: true,
  },
  {
    field: 'tenantName',
    headerName: t('tenant') ?? '',
    width: 250,
    sortable: true,
    hideable: true,
  },
  {
    field: 'phone',
    headerName: t('PhoneNumber') ?? '',
    width: 150,
    sortable: false,
    hideable: true,
  },
  {
    field: 'capacity',
    headerName: t('Capacity') ?? '',
    width: 120,
    sortable: true,
    hideable: true,
  },
  {
    field: 'numberOfVisitors',
    headerName: t('NumberOfVisitors') ?? '',
    width: 160,
    sortable: true,
    hideable: true,
  },
  {
    field: 'complex',
    headerName: t('complex') ?? '',
    width: 300,
    sortable: true,
    hideable: true,
  },
  {
    field: 'address',
    headerName: t('Label.address') ?? '',
    width: 300,
    sortable: true,
    hideable: true,
  },
  {
    field: 'floor',
    headerName: t('Floor') ?? '',
    width: 100,
    sortable: true,
    hideable: true,
  },
  {
    field: 'status',
    headerName: t('Label.status') ?? '',
    width: 320,
    sortable: true,
    hideable: true,
    renderCell: ({ value }) => {
      if (value) {
        return <Styled.Chip label={value.title} size="small" value={value.id} />;
      }

      return <div />;
    },
  },
  {
    field: 'rentalPeriod',
    headerName: t('RentalPeriod') ?? '',
    width: 280,
    sortable: true,
    hideable: true,
  },
  {
    field: 'paymentAmount',
    headerName: t('AmountPayableRub') ?? '',
    width: 160,
    sortable: true,
    hideable: true,
  },
  {
    field: 'comment',
    headerName: t('Label.comment') ?? '',
    width: 200,
    sortable: false,
    hideable: true,
  },
];

export const {
  $tableParams,
  $pagination,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  sortChanged,
  searchForm,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

export const $count = $rawData.map(({ meta }) => meta.total ?? 0);

const formatDate = (timestamp: number, offset: number) => {
  if (timestamp && offset) {
    const coefficientMinutes = 60;
    const currentOffset = new Date().getTimezoneOffset() * coefficientMinutes;

    return new Date((timestamp + currentOffset + offset) * coefficientForSeconds);
  }

  return null;
};

const formatData = (rents: RentItem[]) =>
  rents.map((rent) => {
    const startDate = formatDate(rent.rent_start, rent.area_timezone_offset);
    const endDate = formatDate(rent.rent_finish, rent.area_timezone_offset);

    return {
      id: rent.rent_id,
      title: rent?.area_title ?? '',
      propertyType: rent?.area_type_title ?? t('Unknown'),
      tenantName: rent?.tenant_name ?? t('Unknown'),
      phone: rent?.tenant_phone ? formatPhone(rent.tenant_phone) : '-',
      capacity: rent?.capacity ?? 0,
      numberOfVisitors: rent?.participant_amount ?? 0,
      complex: rent?.complex_title ?? '-',
      address: rent?.address ?? '-',
      floor: rent?.floor_number ?? '-',
      status: rent?.status ?? null,
      rentalPeriod:
        startDate && endDate
          ? `${format(startDate, 'dd.MM.yyyy с HH:mm')} по ${format(endDate, 'HH:mm')}`
          : '-',
      paymentAmount: rent?.payment_amount ?? '-',
      comment: rent?.comment ?? '-',
    };
  });

export const $tableData = $rawData.map(({ registry }) =>
  Array.isArray(registry) && registry.length > 0 ? formatData(registry) : []
);
