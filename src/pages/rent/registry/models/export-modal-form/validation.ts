import * as Zod from 'zod';

export const validationSchema = Zod.object({
  email: Zod.string().nonempty().email(),
});
