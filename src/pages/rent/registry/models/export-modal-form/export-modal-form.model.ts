import { createEvent } from 'effector';
import { createForm } from '@unicorn/effector-form';
import { ExportModalForm } from '../../types';
import { $exportModalInitialValues } from '../export-modal';
import { validationSchema } from './validation';

export const exportModalForm = createForm<ExportModalForm>({
  initialValues: $exportModalInitialValues,
  validationSchema,
  editable: true,
});

export const submitExportForm = createEvent<void>();
