import { sample } from 'effector';
import { exportModalForm, submitExportForm } from './export-modal-form.model';

/** Необходимо для сабмита, когда значение почты не изменилось */
sample({
  clock: submitExportForm,
  fn: () => true,
  target: [exportModalForm.dirty, exportModalForm.submittable, exportModalForm.validate],
});
