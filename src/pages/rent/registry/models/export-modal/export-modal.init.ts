import { signout } from '@features/common';
import { pageUnmounted, fxExportEmailRent } from '../page/page.model';
import {
  $exportModalData,
  closeModal,
  $modalStep,
  changeModalStep,
} from './export-modal.model';

$exportModalData.reset([pageUnmounted, signout, closeModal, fxExportEmailRent]);

$modalStep
  .on(changeModalStep, (_, step) => step)
  .reset([pageUnmounted, signout, closeModal, fxExportEmailRent]);
