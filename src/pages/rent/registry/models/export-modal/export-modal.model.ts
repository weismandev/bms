import { createStore, createEvent } from 'effector';
import { ExportModalData } from '../../types';

export const closeModal = createEvent<void>();
export const changeModalStep = createEvent<'warning' | 'form'>();

export const $exportModalData = createStore<ExportModalData>({
  isOpen: false,
  count: 0,
  limit: 0,
  email: '',
  type: '',
});
export const $modalStep = createStore<'warning' | 'form'>('warning');
export const $exportModalInitialValues = $exportModalData.map((data) => ({
  email: data.email,
  type: data.type,
}));
