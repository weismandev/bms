import * as Zod from 'zod';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Zod.object({
  rentDateTimeFrom: Zod.union([Zod.date(), Zod.null()]),
  rentDateTimeTo: Zod.union([Zod.date(), Zod.null()]),
  rentPriceFrom: Zod.union([Zod.number(), Zod.null()]),
  rentPriceTo: Zod.union([Zod.number(), Zod.null()]),
})
  .refine(
    ({ rentDateTimeFrom, rentDateTimeTo }) => {
      if (rentDateTimeFrom && rentDateTimeTo) {
        return Number(rentDateTimeFrom) < Number(rentDateTimeTo);
      }

      return true;
    },
    {
      message: t('PeriodFromMustBeLessThanPeriodTo') ?? '',
      path: ['rentDateTimeFrom'],
    }
  )
  .refine(
    ({ rentDateTimeFrom, rentDateTimeTo }) => {
      if (rentDateTimeFrom && rentDateTimeTo) {
        return Number(rentDateTimeFrom) < Number(rentDateTimeTo);
      }

      return true;
    },
    {
      message: t('PeriodToMustBeGreaterThanThePeriodFrom') ?? '',
      path: ['rentDateTimeTo'],
    }
  )
  .refine(
    ({ rentPriceFrom, rentPriceTo }) => {
      if (rentPriceFrom && rentPriceTo) {
        return rentPriceFrom < rentPriceTo;
      }

      return true;
    },
    {
      message: t('AmountDueFromMustBeLessThanAmountDueTo') ?? '',
      path: ['rentPriceFrom'],
    }
  )
  .refine(
    ({ rentPriceFrom, rentPriceTo }) => {
      if (rentPriceFrom && rentPriceTo) {
        return rentPriceFrom < rentPriceTo;
      }

      return true;
    },
    {
      message: t('PaymentAmountToMustBeGreaterThanPaymentAmountFrom') ?? '',
      path: ['rentPriceTo'],
    }
  );
