import { createForm } from '@unicorn/effector-form';
import { FiltersForm } from '../../types';
import { $filters } from '../filters/filters.model';
import { validationSchema } from './validation';

export const filtersForm = createForm<FiltersForm>({
  initialValues: $filters,
  validationSchema,
  editable: true,
});
