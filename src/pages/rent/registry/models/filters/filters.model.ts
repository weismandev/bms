import { createFilterBag } from '@tools/factories';

export const filtersEntity = {
  statuses: [],
  cities: [],
  complexes: [],
  buildings: [],
  floors: [],
  propertyTypes: [],
  rentDateTimeFrom: null,
  rentDateTimeTo: null,
  isPaymentRequired: null,
  rentPriceFrom: null,
  rentPriceTo: null,
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(filtersEntity);
