import { signout } from '@features/common';
import { pageUnmounted } from '../page/page.model';
import { $filters, $isFilterOpen } from './filters.model';

$isFilterOpen.reset([signout, pageUnmounted]);
$filters.reset([signout, pageUnmounted]);
