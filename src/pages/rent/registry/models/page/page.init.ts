import { sample } from 'effector';
import { pending } from 'patronum/pending';
import { format as formatDateFns } from 'date-fns';
import FileSaver from 'file-saver';
import { GridRowSelectionModel } from '@mui/x-data-grid-pro';
import { signout } from '@features/common';
import { fxCreateRent, fxUpdateRent } from '../../../models/detail/detail.model';
import { registryApi } from '../../api';
import { GetExportRentPayload, FiltersForm, RentFilters } from '../../types';
import { exportModalForm } from '../export-modal-form/export-modal-form.model';
import { $exportModalData } from '../export-modal/export-modal.model';
import { $filters } from '../filters/filters.model';
import { $isOpenSuccessModal } from '../success-modal/success-modal.model';
import {
  $tableParams,
  $count,
  $visibilityColumns,
  $selectRow,
} from '../table/table.model';
import {
  $isLoading,
  pageUnmounted,
  fxGetRent,
  pageMounted,
  $rawData,
  $isLoadingTable,
  coefficientForSeconds,
  exportRent,
  fxExportEmailRent,
  limitForExport,
  fxExportRent,
  PageGate,
} from './page.model';

fxGetRent.use(registryApi.getRent);
fxExportEmailRent.use(registryApi.exportEmailRent);
fxExportRent.use(registryApi.exportRent);

$isLoading
  .on(
    pending({
      effects: [fxExportEmailRent, fxExportRent],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

const formatFilters = (filters: FiltersForm) => {
  const formattedFilters: RentFilters = {};

  if (filters.statuses.length > 0) {
    formattedFilters.statuses = filters.statuses;
  }

  if (filters.cities.length > 0) {
    formattedFilters.cities = filters.cities;
  }

  if (filters.complexes.length > 0) {
    formattedFilters.complexes = filters.complexes;
  }

  if (filters.buildings.length > 0) {
    formattedFilters.buildings = filters.buildings;
  }

  if (filters.floors.length > 0) {
    formattedFilters.floors = filters.floors;
  }

  if (filters.propertyTypes.length > 0) {
    formattedFilters.types = filters.propertyTypes;
  }

  if (filters?.rentDateTimeFrom) {
    const date = new Date(filters.rentDateTimeFrom);

    formattedFilters.rent_start = date.getTime() / coefficientForSeconds;
  }

  if (filters?.rentDateTimeTo) {
    const date = new Date(filters.rentDateTimeTo);

    formattedFilters.rent_finish = date.getTime() / coefficientForSeconds;
  }

  if (filters?.isPaymentRequired === 0 || filters?.isPaymentRequired) {
    formattedFilters.is_payment_required = filters.isPaymentRequired;
  }

  if (filters?.isPaymentRequired && filters.rentPriceFrom) {
    formattedFilters.rent_price_from = filters.rentPriceFrom;
  }

  if (filters?.isPaymentRequired && filters.rentPriceTo) {
    formattedFilters.rent_price_to = filters.rentPriceTo;
  }

  return formattedFilters;
};

const getColumnName = (column: string) => {
  switch (column) {
    case 'title':
      return 'area_title';
    case 'propertyType':
      return 'area_type_title';
    case 'tenantName':
      return 'tenant_name';
    case 'phone':
      return 'tenant_phone';
    case 'capacity':
      return 'capacity';
    case 'numberOfVisitors':
      return 'participant_amount';
    case 'complex':
      return 'complex_title';
    case 'address':
      return 'address';
    case 'floor':
      return 'floor_number';
    case 'status':
      return 'status';
    case 'rentalPeriod':
      return ['rent_start', 'rent_finish'];
    case 'paymentAmount':
      return 'payment_amount';
    case 'comment':
      return 'comment';
    default:
      return null;
  }
};

/** Запрос списка аренд */
sample({
  clock: [pageMounted, $tableParams, $filters, fxCreateRent.done, fxUpdateRent.done],
  source: {
    table: $tableParams,
    filters: $filters,
    status: PageGate.status,
  },
  filter: ({ status }) => status,
  fn: ({ table, filters }) => {
    const payload: GetExportRentPayload = {
      page: table.page,
      per_page: table.per_page,
      search: table.search,
    };

    if (table.sorting[0]?.field && table.sorting[0]?.sort) {
      const field = getColumnName(table.sorting[0].field);

      if (field) {
        payload.sort = Array.isArray(field) ? field[0] : field;
        payload.order = table.sorting[0]?.sort;
      }
    }

    const formattedFilters = formatFilters(filters as FiltersForm);

    return { ...payload, ...formattedFilters };
  },
  target: fxGetRent,
});

$rawData.on(fxGetRent.done, (_, { result }) => result).reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetRent.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);

const formatExportPayload = ({
  search,
  filters,
  format,
  email,
  visibilityColumns,
  selectRow = [],
}: {
  search: string;
  filters: FiltersForm;
  format: string;
  email?: string;
  visibilityColumns: { [key: string]: boolean };
  selectRow?: GridRowSelectionModel;
}) => {
  const payload: GetExportRentPayload = {
    export: 1,
    export_format: format,
    search,
  };

  if (selectRow.length > 0) {
    payload.ids = selectRow as string[];
  }

  if (Object.entries(visibilityColumns).length > 0) {
    payload.export_fields_exclude = Object.entries(visibilityColumns).reduce(
      (acc: string[], column) => {
        const columnName = getColumnName(column[0]);

        if (column[1] || !columnName) {
          return acc;
        }

        return [...acc, columnName].flat();
      },
      []
    );
  }

  if (email) {
    payload.export_email = email;
  }

  const formattedFilters = formatFilters(filters);

  return { ...payload, ...formattedFilters };
};

/** Подготовка к экспорту аренды по почте */
sample({
  clock: exportRent,
  source: {
    tableParams: $tableParams,
    filters: $filters,
    count: $count,
    visibilityColumns: $visibilityColumns,
  },
  filter: ({ count }) => count >= limitForExport,
  fn: ({ tableParams, filters, visibilityColumns }, exportFormat) =>
    formatExportPayload({
      search: tableParams.search,
      filters: filters as FiltersForm,
      format: exportFormat,
      visibilityColumns,
    }),
  target: fxExportEmailRent,
});

/** При лимите выше 10000 записей открывается модалка с экспортом */
sample({
  clock: fxExportEmailRent.done,
  filter: ({ params }) => !params.export_email,
  fn: ({ params, result }) => ({
    isOpen: true,
    count: result.entry_amount,
    limit: result.entry_limit,
    email: result.export_email,
    type: params.export_format as string,
  }),
  target: $exportModalData,
});

/** Экспорт на почту */
sample({
  clock: exportModalForm.submitted,
  source: {
    tableParams: $tableParams,
    filters: $filters,
    visibilityColumns: $visibilityColumns,
  },
  fn: ({ tableParams, filters, visibilityColumns }, exportData) =>
    formatExportPayload({
      search: tableParams.search,
      filters: filters as FiltersForm,
      format: exportData.type,
      email: exportData.email,
      visibilityColumns,
    }),
  target: fxExportEmailRent,
});

/** Открытие модалки после успешного экспорта */
sample({
  clock: fxExportEmailRent.done,
  filter: ({ params }) => Boolean(params?.export_email),
  fn: () => true,
  target: $isOpenSuccessModal,
});

/** Экспорт аренды через файл */
sample({
  clock: exportRent,
  source: {
    tableParams: $tableParams,
    filters: $filters,
    count: $count,
    visibilityColumns: $visibilityColumns,
    selectRow: $selectRow,
  },
  filter: ({ count }) => count < limitForExport,
  fn: ({ tableParams, filters, visibilityColumns, selectRow }, exportFormat) =>
    formatExportPayload({
      search: tableParams.search,
      filters: filters as FiltersForm,
      format: exportFormat,
      visibilityColumns,
      selectRow,
    }),
  target: fxExportRent,
});

/** Сохранение файла */
// eslint-disable-next-line effector/no-watch
fxExportRent.done.watch(({ params, result }) => {
  const fileData =
    params.export_format === 'pdf'
      ? {
          format: 'pdf',
          type: 'application/pdf',
        }
      : {
          format: 'xlsx',
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        };

  const blob = new Blob([result], {
    type: fileData.type,
  });

  const dateTime = formatDateFns(new Date(), 'yyyy-MM-dd_HH-mm');

  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `rent_registry_${dateTime}.${fileData.format}`
  );
});
