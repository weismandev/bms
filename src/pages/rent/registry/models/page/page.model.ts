import { createStore, createEffect, createEvent, merge } from 'effector';
import { createGate } from 'effector-react';
import { GetExportRentPayload, GetRentResponse, ExportRentResponse } from '../../types';

export const coefficientForSeconds = 1000;
export const limitForExport = 10000;

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const exportRent = createEvent<string>();

export const fxGetRent = createEffect<GetExportRentPayload, GetRentResponse, Error>();
export const fxExportEmailRent = createEffect<
  GetExportRentPayload,
  ExportRentResponse,
  Error
>();
export const fxExportRent = createEffect<GetExportRentPayload, ArrayBuffer, Error>();

export const $isLoading = createStore<boolean>(false);
export const $isLoadingTable = createStore<boolean>(false);
export const $rawData = createStore<GetRentResponse>({
  meta: { total: 0 },
  registry: [],
});

export const errorOccured = merge([
  fxGetRent.fail,
  fxExportEmailRent.fail,
  fxExportRent.fail,
]);
