import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Wrapper = styled(Paper)(() => ({
  padding: '0px 24px 48px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
}));
