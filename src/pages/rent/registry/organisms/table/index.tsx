import { FC } from 'react';
import { useUnit } from 'effector-react';
import { GridRowParams, GridRowClassNameParams } from '@mui/x-data-grid-pro';
import { DataGridTable } from '@shared/ui/data-grid-table';
import { openViaUrl } from '../../../models';
import {
  $tableData,
  $isLoadingTable,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $sorting,
  sortChanged,
  PageGate,
  $openRow,
  openRowChanged,
  $pinnedColumns,
  changePinnedColumns,
  $count,
  $pagination,
  $selectRow,
  selectionRowChanged,
  paginationModelChanged,
} from '../../models';
import { ExportModal } from '../export-modal';
import { SuccessModal } from '../success-modal';
import { TableToolbar } from '../table-toolbar';
import * as Styled from './styled';

export const Table: FC = () => {
  const [
    isLoading,
    tableData,
    pagination,
    visibilityColumns,
    columns,
    count,
    openRow,
    pinnedColumns,
    sorting,
    selectRow,
  ] = useUnit([
    $isLoadingTable,
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $count,
    $openRow,
    $pinnedColumns,
    $sorting,
    $selectRow,
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openViaUrl(row.id);
    openRowChanged(row.id);
  };

  const getRowClassName = ({ id }: GridRowClassNameParams) =>
    id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

  return (
    <>
      <PageGate />

      <ExportModal />
      <SuccessModal />

      <Styled.Wrapper elevation={0}>
        <TableToolbar />
        <DataGridTable
          rows={tableData}
          columns={columns}
          sortingMode="server"
          rowCount={count}
          loading={isLoading}
          density="compact"
          checkboxSelection
          disableRowSelectionOnClick
          getRowClassName={getRowClassName}
          onRowClick={handleClickRow}
          rowSelectionModel={selectRow}
          onRowSelectionModelChange={selectionRowChanged}
          paginationModel={pagination}
          onPaginationModelChange={paginationModelChanged}
          sortModel={sorting}
          onSortModelChange={sortChanged}
          pinnedColumns={pinnedColumns}
          onPinnedColumnsChange={changePinnedColumns}
          columnVisibilityModel={visibilityColumns}
          onColumnVisibilityModelChange={visibilityColumnsChanged}
          onColumnWidthChange={columnsWidthChanged}
          onColumnOrderChange={orderColumnsChanged}
          pageSizeOptions={[25, 50, 100]}
        />
      </Styled.Wrapper>
    </>
  );
};
