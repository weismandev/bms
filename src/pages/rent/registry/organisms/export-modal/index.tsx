import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { Dialog, DialogActions, Button } from '@mui/material';
import {
  $exportModalData,
  closeModal,
  $modalStep,
  changeModalStep,
  exportModalForm,
  submitExportForm,
} from '../../models';
import * as Styled from './styled';

export const ExportModal: FC = () => {
  const { t } = useTranslation();
  const [exportModalData, modalStep] = useUnit([$exportModalData, $modalStep]);

  if (!exportModalData.isOpen) {
    return null;
  }

  const onClose = () => closeModal();
  const onConfirmExport = () => changeModalStep('form');
  const onSubmit = () => submitExportForm();

  const config =
    modalStep === 'warning'
      ? {
          header: t('LimitIsExceeded'),
          content: `${t('YouRequestedExport')} ${exportModalData.count} ${t('events')}.
        ${t('MaximumAmount')} - ${exportModalData.limit} ${t('eventsPerExport')}. ${t(
            'PleaseLimitTheNumberUploadedPostsUsingFiltersandorDearch'
          )} ${t('OrExportByEmail')}.`,
          confirmButton: (
            <Button variant="contained" color="primary" onClick={onConfirmExport}>
              {t('ExportToEmail')}
            </Button>
          ),
        }
      : {
          header: t('RegistryExport'),
          content: (
            <Styled.FormWrapper>
              <Control.Email
                name="email"
                label={t('ProfilePage.Email') ?? ''}
                required
                form={exportModalForm}
              />
            </Styled.FormWrapper>
          ),
          confirmButton: (
            <Button variant="contained" color="primary" onClick={onSubmit}>
              {t('Send')}
            </Button>
          ),
        };

  return (
    <Dialog open={exportModalData.isOpen} onClose={onClose}>
      <Styled.DialogTitle>{config.header}</Styled.DialogTitle>
      <Styled.DialogContent>
        <Styled.DialogContentText>{config.content}</Styled.DialogContentText>
      </Styled.DialogContent>
      <DialogActions>
        {config.confirmButton}
        <Button variant="outlined" color="primary" onClick={onClose}>
          {t('Cancellation')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
