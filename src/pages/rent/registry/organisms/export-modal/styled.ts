import {
  DialogTitle as DialogTitleMui,
  DialogContentText as DialogContentTextMui,
  DialogContent as DialogContentMui,
} from '@mui/material';
import { styled } from '@mui/material/styles';

export const DialogTitle = styled(DialogTitleMui)(({ theme }) => ({
  color: theme.palette.text.primary,
  fontSize: 20,
  fontWeight: 500,
}));

export const DialogContentText = styled(DialogContentTextMui)(({ theme }) => ({
  whiteSpace: 'pre-line',
  color: theme.palette.text.primary,
  fontSize: 16,
  fontWeight: 400,
}));

export const DialogContent = styled(DialogContentMui)(() => ({
  width: 600,
}));

export const FormWrapper = styled('div')(() => ({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
}));
