import { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import {
  Button,
  Popover,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import ExportIcon from '@mui/icons-material/VerticalAlignTop';
import Xlsx from '@img/xlsx.svg';
import Xml from '@img/xml.svg';
import { $tableData, $isLoading, exportRent } from '../../models';

export const ExportButton: FC = () => {
  const { t } = useTranslation();
  const [isLoading, tableData] = useUnit([$isLoading, $tableData]);
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) =>
    setAnchorEl(event.currentTarget);

  const handleClose = () => setAnchorEl(null);

  const onExportXlsx = () => {
    exportRent('xlsx');
    handleClose();
  };

  const onExportPdf = () => {
    exportRent('pdf');
    handleClose();
  };

  return (
    <>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClick}
        disabled={tableData.length === 0 || isLoading}
        startIcon={<ExportIcon />}
      >
        {t('export')}
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <MenuList>
          <MenuItem onClick={onExportXlsx}>
            <ListItemIcon>
              <Xlsx />
            </ListItemIcon>
            <ListItemText>{t('ExportToXLS')}</ListItemText>
          </MenuItem>
          <MenuItem onClick={onExportPdf}>
            <ListItemIcon>
              <Xml />
            </ListItemIcon>
            <ListItemText>{t('ExportToPDF')}</ListItemText>
          </MenuItem>
        </MenuList>
      </Popover>
    </>
  );
};
