import {
  DialogContentText as DialogContentTextMui,
  DialogContent as DialogContentMui,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { CheckCircleOutline } from '@mui/icons-material';

export const DialogContentText = styled(DialogContentTextMui)(({ theme }) => ({
  fontWeight: 500,
  fontSize: 20,
  color: theme.palette.common.black,
}));

export const DialogContent = styled(DialogContentMui)(() => ({
  width: 360,
  minHeight: 280,
}));

export const WrapperIcon = styled('div')(() => ({
  textAlign: 'center',
}));

export const CheckCircle = styled(CheckCircleOutline)(() => ({
  margin: '30px auto',
  height: 90,
  width: 90,
}));
