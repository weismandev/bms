import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Dialog } from '@mui/material';
import { $isOpenSuccessModal, closeSuccessModal } from '../../models';
import * as Styled from './styled';

export const SuccessModal: FC = () => {
  const { t } = useTranslation();
  const isOpenSuccessModal = useUnit($isOpenSuccessModal);

  if (!isOpenSuccessModal) {
    return null;
  }

  const onClose = () => closeSuccessModal();

  return (
    <Dialog open={isOpenSuccessModal} onClose={onClose}>
      <Styled.DialogContent>
        <Styled.WrapperIcon>
          <Styled.CheckCircle color="success" />
        </Styled.WrapperIcon>
        <Styled.DialogContentText>
          {t('TheRegistryHasBeenSuccessfullyExported')}
        </Styled.DialogContentText>
      </Styled.DialogContent>
    </Dialog>
  );
};
