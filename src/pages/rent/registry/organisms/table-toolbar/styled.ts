import { styled } from '@mui/material/styles';

export const Wrapper = styled('div')(() => ({
  display: 'flex',
  width: '100%',
  gap: 12,
  padding: '24px 0',
}));

export const Greedy = styled('div')(() => ({
  flexGrow: 13,
}));
