import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { IconButton, Button, Tooltip } from '@mui/material';
import { FilterAltOutlined, CalendarTodayOutlined } from '@mui/icons-material';
import { addClicked, detailForm, $opened } from '../../../models';
import { searchForm, $isFilterOpen, changedFilterVisibility } from '../../models';
import { ExportButton } from '../export-button';
import * as Styled from './styled';

export const TableToolbar: FC = () => {
  const { t } = useTranslation();
  const { editable } = useUnit(detailForm);
  const [isFilterOpen, opened] = useUnit([$isFilterOpen, $opened]);

  const isDisabledAddButton = editable && !opened.id;

  const onClickFilter = () => changedFilterVisibility(true);
  const onClickAdd = () => addClicked();

  return (
    <Styled.Wrapper>
      <IconButton disabled={isFilterOpen} onClick={onClickFilter}>
        <Tooltip title={t('Filters')}>
          <FilterAltOutlined />
        </Tooltip>
      </IconButton>
      <div>
        <Control.Search name="search" form={searchForm} />
      </div>
      <Styled.Greedy />
      <ExportButton />
      <Button
        variant="contained"
        color="primary"
        onClick={onClickAdd}
        disabled={isDisabledAddButton}
        startIcon={<CalendarTodayOutlined />}
      >
        {t('Book')}
      </Button>
    </Styled.Wrapper>
  );
};
