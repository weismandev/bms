import { api } from '@api/api2';
import { GetExportRentPayload } from '../types';

const getRent = (payload: GetExportRentPayload) => api.v4('get', 'rent', payload);
const exportEmailRent = (payload: GetExportRentPayload) => api.v4('get', 'rent', payload);
const exportRent = (payload: GetExportRentPayload) =>
  api.v4('get', 'rent', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });

export const registryApi = {
  getRent,
  exportEmailRent,
  exportRent,
};
