export type ExportModalData = {
  isOpen: boolean;
  count: number;
  limit: number;
  email: string;
  type: string;
};
