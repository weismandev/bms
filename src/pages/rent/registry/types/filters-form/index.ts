export type FiltersForm = {
  statuses: number[];
  cities: number[];
  complexes: string[];
  buildings: string[];
  floors: string[];
  propertyTypes: string[];
  rentDateTimeFrom: null | Date;
  rentDateTimeTo: null | Date;
  isPaymentRequired: null | number;
  rentPriceFrom: null | number;
  rentPriceTo: null | number;
};
