export type ExportModalForm = {
  email: string;
  type: string;
};
