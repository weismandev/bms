export type GetExportRentPayload = {
  page?: number;
  per_page?: number;
  search: string;
  statuses?: number[];
  cities?: number[];
  complexes?: string[];
  buildings?: string[];
  floors?: string[];
  types?: string[];
  rent_time_start?: number;
  rent_time_finish?: number;
  is_payment_required?: number;
  rent_price_from?: number;
  rent_price_to?: number;
  sort?: string;
  order?: 'asc' | 'desc';
  export?: number;
  export_format?: string;
  export_email?: string;
  export_fields_exclude?: string[];
  ids?: string[];
};

export type ExportRentResponse = {
  entry_amount: number;
  entry_limit: number;
  export_email: string;
};

export type RentFilters = {
  statuses?: number[];
  cities?: number[];
  complexes?: string[];
  buildings?: string[];
  floors?: string[];
  types?: string[];
  rent_start?: number;
  rent_finish?: number;
  is_payment_required?: number;
  rent_price_from?: number;
  rent_price_to?: number;
};

export type GetRentResponse = {
  meta: {
    total: number;
  };
  registry: RentItem[];
};

export type RentItem = {
  address: string;
  area_title: string;
  area_type_title: string;
  capacity: number;
  comment: string;
  equipment_list_additional: null;
  equipment_list_basic: number;
  participant_amount: null;
  payment_amount: number;
  payment_method: null;
  rent_id: string;
  rent_finish: number;
  rent_start: number;
  status: { id: number; title: string };
  tenant_name: string;
  tenant_phone: number;
  complex_title: string;
  floor_number: string;
  area_timezone_offset: number;
};
