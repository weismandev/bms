export const getIdInUrl = (pathname: string) => (pathname ? pathname.split('/')[2] : '');
