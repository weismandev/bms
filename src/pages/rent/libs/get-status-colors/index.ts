import { Theme } from '@mui/material/styles';

export const getStatusColors = (status: number, theme?: Theme) => {
  switch (status) {
    case 5:
    case 2:
    case 3:
      return {
        background: theme?.palette.warning.main,
        color: theme?.palette.warning.contrastText,
      };
    case 4:
      return {
        background: theme?.palette.primary.main,
        color: theme?.palette.warning.contrastText,
      };
    case 6:
      return {
        background: theme?.palette.error.main,
        color: theme?.palette.error.contrastText,
      };
    case 1:
      return {
        background: theme?.palette.success.main,
        color: theme?.palette.success.contrastText,
      };
    case 10:
    case 8:
    case 7:
    case 9:
      return {
        background: theme?.palette.grey[300],
        color: theme?.palette.common.black,
      };
    default:
      return {
        background: theme?.palette.grey[300],
        color: theme?.palette.common.black,
      };
  }
};
