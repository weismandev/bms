const zeroTimeValue = 0;

export const formatDate = (date: Date) => {
  date.setHours(zeroTimeValue, zeroTimeValue, zeroTimeValue, zeroTimeValue);

  const timezone = date.getTimezoneOffset();

  const formattedDate = new Date(date);
  formattedDate.setMinutes(date.getMinutes() - timezone);

  return formattedDate;
};
