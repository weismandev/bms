export type DetailForm = {
  id: string | null;
  city: null | number;
  timezone: null | number;
  complex: null | string;
  building: null | string;
  floor: null | string;
  propertyType: null | string;
  status: null | number;
  statusTitle: string;
  rentObject: null | string;
  rentalDate: null | Date;
  rentalTime: {
    id: number;
    label: string;
    start: number;
    finish: number;
  }[];
  comment: string;
};
