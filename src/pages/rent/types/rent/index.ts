export type GetRentItemPayload = {
  rent_id: string;
};

export type CreateRentPayload = {
  area_id: string;
  status_id: number;
  tenant_id: number;
  rent_start?: number;
  rent_finish?: number;
  comment: string;
};

export type GetRentItemResponse = {
  area: {
    building: ObjectItem;
    capacity: null;
    city: ObjectItem;
    floor: ObjectItem;
    complex: ObjectItem;
    geometric_area: number;
    id: string;
    title: string;
    type: ObjectItem;
  };
  rent: {
    comment: string;
    id: string;
    landlord: { id: number; name: string };
    payment_format: null;
    payment_sum: null;
    payment_time: null;
    rent_finish: number;
    rent_start: number;
    slots: {
      position: number;
      rent_finish: number;
      rent_info: number;
      rent_start: number;
      rent_status: ObjectItem;
      slot_title: string;
    }[];
    status: ObjectItem;
    tenant: { id: number; name: string };
    title: null;
  };
};

export type ObjectItem = {
  id: string | number;
  title: string;
  is_selected?: boolean;
  offset?: number;
};

export type CreateUpdateRentResponse = {
  id: string;
};

export type UpdateRentPayload = {
  rent_id: string;
  status_id: number;
  rent_start?: number;
  rent_finish?: number;
  comment: string;
};
