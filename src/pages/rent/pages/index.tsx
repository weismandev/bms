import { FC } from 'react';
import { useUnit } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { ThemeAdapter } from '@shared/theme/adapter';
import { ErrorMessage, FilterMainDetailLayout } from '@ui/index';
import {
  changedErrorDialogVisibility,
  $isErrorDialogOpen,
  $error,
  MainGate,
  $tab,
  $isDetailOpen,
} from '../models';
import { $isFilterOpen as $isFilterOpenMonitoring } from '../monitoring/models';
import { Filters as FiltersMonitoring } from '../monitoring/organisms';
import { Main, Loader, Detail } from '../organisms';
import { $isFilterOpen as $isFilterOpenRegistry } from '../registry/models';
import { Filters as FiltersRegistry } from '../registry/organisms';

const RentPage: FC = () => {
  const [
    isErrorDialogOpen,
    error,
    isDetailOpen,
    isFilterOpenRegistry,
    isFilterOpenMonitoring,
    tab,
  ] = useUnit([
    $isErrorDialogOpen,
    $error,
    $isDetailOpen,
    $isFilterOpenRegistry,
    $isFilterOpenMonitoring,
    $tab,
  ]);

  const filters =
    tab === 0
      ? isFilterOpenRegistry && <FiltersRegistry />
      : isFilterOpenMonitoring && <FiltersMonitoring />;

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);

  return (
    <>
      <MainGate />

      <Loader />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <FilterMainDetailLayout
        filter={filters}
        main={<Main />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedRentPage: FC = () => (
  <HaveSectionAccess>
    <ThemeAdapter>
      <RentPage />
    </ThemeAdapter>
  </HaveSectionAccess>
);

export { RestrictedRentPage as RentPage };
