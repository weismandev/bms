import { api } from '@api/api2';
import { GetRentItemPayload, CreateRentPayload, UpdateRentPayload } from '../types';

const getRentItem = (payload: GetRentItemPayload) => api.v4('get', 'rent/view', payload);
const createRent = (payload: CreateRentPayload) => api.v4('post', 'rent', payload);
const updateRent = (payload: UpdateRentPayload) => api.v4('post', 'rent/update', payload);

export const rentApi = {
  getRentItem,
  createRent,
  updateRent,
};
