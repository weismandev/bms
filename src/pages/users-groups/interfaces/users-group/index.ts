import { Value } from '../general-interfaces';

export interface CreateUpdateUsersGroupPayload {
  id?: number;
  title: string;
  levels: LevelPayload[];
}

export interface CreateUpdateUsersGroupResponse {
  groups: {
    id: number;
  };
}

export interface DeleteUsersGroupPayload {
  id: number;
}

export interface Level {
  level_type: Value;
  user_type: Value;
  complex: Value[] | Value;
  user: Value[];
  building: Value[] | Value;
  rizer: Value[] | Value;
  floor: Value[];
}

export interface LevelPayload {
  level_type: number;
  user_type: number;
  complex_id: number[];
  userdata_id: number[];
  buildings: number[];
  rizer: number[];
}

export interface CreateUpdateLevelPayload {
  group_id?: number;
  use_compls: number;
  compls: LevelPayload[];
}

export interface GetPeopleInfoPayload {
  id: number;
}

export interface GetPeopleInfoResponse {
  userdata: Value[];
}

export interface Opened {
  id?: number;
  title: string;
  levels: Level[];
  access_groups: Value[];
}
