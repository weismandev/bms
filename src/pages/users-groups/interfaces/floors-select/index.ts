export interface GetFloorsPayload {
  id: number;
}

export interface GetFloorsResponse {
  building: {
    entrances: {
      number: number;
      apartments: {
        id: number;
        number: number;
        floor: number;
        title: string;
      }[];
    }[];
  };
}
