import { Value } from '../general-interfaces';

export interface GetUsersGroupsListPayload {
  compl?: number;
  limit?: number;
  offset?: number;
  search?: string;
  sort_by?: string;
  sort_order?: string;
  show_ag?: number;
  group_compl?: number;
  show_objects_compl?: number;
  user_compl_type_ids?: string;
  complex_id?: string;
  buildings?: string;
  access_group_ids?: string;
  id?: number;
  show_users_count?: number;
}

export interface GetUsersGroupsListResponse {
  meta: {
    total: number;
  };
  groups: Group[];
}

export interface Group {
  id: number;
  title: string;
  compl: Compl[];
  access_group: Value[];
  created_at: number;
  updated_at: number;
  complex: Value[];
  buildings: Value[];
  users_count: number;
}

export interface Compl {
  id: number;
  title: string;
  type_id: number;
  type_title: string;
  values: {
    value: number;
    param: string;
    title: string;
  }[];
}
