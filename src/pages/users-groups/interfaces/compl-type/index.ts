export interface GetComplTypePayload {
  object: string;
}

export interface GetComplTypeResponse {
  types: ComplType[];
}

export interface ComplType {
  id: number;
  title: string;
  type: string;
}
