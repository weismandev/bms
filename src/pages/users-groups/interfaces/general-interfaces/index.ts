export interface Value {
  id: number;
  title: string;
  number?: number;
  type?: string;
}

export interface Filters {
  user_type: Value[];
  complexes: Value[];
  houses: Value[];
  accessGroups: Value[];
}
