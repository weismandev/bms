import { api } from '@api/api2';

import {
  GetUsersGroupsListPayload,
  CreateUpdateUsersGroupPayload,
  DeleteUsersGroupPayload,
  GetComplTypePayload,
  CreateUpdateLevelPayload,
  GetPeopleInfoPayload,
  GetFloorsPayload,
} from '../interfaces';

const getUsersGroupsList = (payload: GetUsersGroupsListPayload) =>
  api.no_vers('get', 'scud/get-users-group', payload);
const createUpdateUsersGroup = (payload: CreateUpdateUsersGroupPayload) =>
  api.no_vers('post', 'scud/update-users-group', payload);
const deleteUsersGroup = (payload: DeleteUsersGroupPayload) =>
  api.no_vers('post', 'scud/remove-users-group', payload);
const getComplType = (payload: GetComplTypePayload) =>
  api.no_vers('get', 'scud/get-compl-type', payload);
const createUpdateLevel = (payload: CreateUpdateLevelPayload) =>
  api.no_vers('post', 'scud/new-compl-user-group', payload);
const getPeopleInfo = (payload: GetPeopleInfoPayload) =>
  api.no_vers('get', 'admin/get-userdata', payload);
const getFloors = (payload: GetFloorsPayload) =>
  api.v1('get', 'buildings/crm/get-details', payload);

export const usersGroupsApi = {
  getUsersGroupsList,
  createUpdateUsersGroup,
  deleteUsersGroup,
  getComplType,
  createUpdateLevel,
  getPeopleInfo,
  getFloors,
};
