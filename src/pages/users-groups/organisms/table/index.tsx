import { FC } from 'react';
import { useUnit } from 'effector-react';
import type { GridRowParams, GridRowClassNameParams } from '@mui/x-data-grid-pro';
import { DataGridTable } from '@shared/ui/data-grid-table';
import {
  $tableData,
  $pagination,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  sortChanged,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $count,
  $sorting,
  paginationModelChanged,
  $isLoadingList,
  openViaUrl,
  $selectRow
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import * as Styled from './styled';

export const Table: FC = () => {
  const [
    isLoading,
    tableData,
    pagination,
    visibilityColumns,
    columns,
    count,
    pinnedColumns,
    openRow,
    sorting,
    selectRow
  ] = useUnit([
    $isLoadingList,
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $count,
    $pinnedColumns,
    $openRow,
    $sorting,
    $selectRow
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openViaUrl(row.id);
    openRowChanged(row.id);
  };

  const getRowClassName = ({ id }: GridRowClassNameParams<any>): string => {
    const baseStyle =
      id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

    return baseStyle;
  };

  return (
    <Styled.Wrapper elevation={0}>
      <TableToolbar />
      <DataGridTable
        rows={tableData}
        columns={columns}
        rowCount={count}
        loading={isLoading}
        density="compact"
        rowSelectionModel={selectRow}
        onRowClick={handleClickRow}
        paginationModel={pagination}
        onPaginationModelChange={paginationModelChanged}
        pinnedColumns={pinnedColumns}
        onPinnedColumnsChange={changePinnedColumns}
        columnVisibilityModel={visibilityColumns}
        onColumnVisibilityModelChange={visibilityColumnsChanged}
        onColumnWidthChange={columnsWidthChanged}
        onColumnOrderChange={orderColumnsChanged}
        sortModel={sorting}
        onSortModelChange={sortChanged}
        pageSizeOptions={[25, 50, 100]}
        getRowClassName={getRowClassName}
      />
    </Styled.Wrapper>
  );
};
