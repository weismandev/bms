import * as Yup from 'yup';
import { CreateErrorOptions, ValidationError } from 'yup';

import i18n from '@shared/config/i18n';

import { Value, Level } from '../../interfaces';

const { t } = i18n;

function complexRequired(this: {
  parent: {
    level_type: Value | string;
    complex: Value[];
  };
}) {
  if (!this.parent.level_type && this.parent.complex.length === 0) {
    return true;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    (this.parent.level_type.type === 'complex' ||
      this.parent.level_type.type === 'building' ||
      this.parent.level_type.type === 'rizer' ||
      this.parent.level_type.type === 'floor') &&
    !this.parent.complex
  ) {
    return false;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    (this.parent.level_type.type === 'complex' ||
      this.parent.level_type.type === 'building' ||
      this.parent.level_type.type === 'rizer' ||
      this.parent.level_type.type === 'floor') &&
    this.parent.complex.length === 0
  ) {
    return false;
  }

  return true;
}

function userRequired(this: {
  parent: {
    user_type: Value | string;
    user: Value[];
  };
}) {
  if (!this.parent.user_type && this.parent.user.length === 0) {
    return true;
  }

  if (
    typeof this.parent.user_type === 'object' &&
    this.parent.user_type.id &&
    this.parent.user.length === 0
  ) {
    return false;
  }

  return true;
}

function buildingRequired(this: {
  parent: {
    level_type: Value | string;
    building: Value[];
  };
}) {
  if (!this.parent.level_type && this.parent.building.length === 0) {
    return true;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    (this.parent.level_type.type === 'building' ||
      this.parent.level_type.type === 'rizer' ||
      this.parent.level_type.type === 'floor') &&
    !this.parent.building
  ) {
    return false;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    (this.parent.level_type.type === 'building' ||
      this.parent.level_type.type === 'rizer' ||
      this.parent.level_type.type === 'floor') &&
    this.parent.building.length === 0
  ) {
    return false;
  }

  return true;
}

function rizerRequired(this: {
  parent: {
    level_type: Value | string;
    rizer: Value[];
  };
}) {
  if (!this.parent.level_type && this.parent.rizer.length === 0) {
    return true;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    (this.parent.level_type.type === 'rizer' ||
      this.parent.level_type.type === 'floor') &&
    this.parent.rizer.length === 0
  ) {
    return false;
  }

  return true;
}

function uniqueLevelType(
  this: {
    parent: Level[];
    createError: (params?: CreateErrorOptions) => ValidationError;
    path: string;
  },
  curentValue: any
) {
  const otherValue = this.parent.filter((item) => item !== curentValue);

  const isDuplicate = otherValue.some((other) => {
    if (
      !other.level_type ||
      !curentValue.level_type ||
      !other.user_type ||
      !curentValue.user_type
    ) {
      return false;
    }

    return (
      other.level_type.id === curentValue.level_type.id &&
      other.user_type.id === curentValue.user_type.id
    );
  });

  return isDuplicate ? this.createError({ path: `${this.path}.level_type` }) : true;
}

function uniqueUserType(
  this: {
    parent: Level[];
    createError: (params?: CreateErrorOptions) => ValidationError;
    path: string;
  },
  curentValue: any
) {
  const otherValue = this.parent.filter((item) => item !== curentValue);

  const isDuplicate = otherValue.some((other) => {
    if (
      !other.level_type ||
      !curentValue.level_type ||
      !other.user_type ||
      !curentValue.user_type
    ) {
      return false;
    }

    return (
      other.level_type.id === curentValue.level_type.id &&
      other.user_type.id === curentValue.user_type.id
    );
  });

  return isDuplicate ? this.createError({ path: `${this.path}.user_type` }) : true;
}

function floorRequired(this: {
  parent: {
    level_type: Value | string;
    floor: Value[];
  };
}) {
  if (!this.parent.level_type && this.parent.floor.length === 0) {
    return true;
  }

  if (
    typeof this.parent.level_type === 'object' &&
    this.parent.level_type.type === 'floor' &&
    this.parent.floor.length === 0
  ) {
    return false;
  }

  return true;
}

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  levels: Yup.array().of(
    Yup.object()
      .shape({
        level_type: Yup.object().required(t('thisIsRequiredField')).nullable(),
        user_type: Yup.object().required(t('thisIsRequiredField')).nullable(),
        complex: Yup.mixed().test(
          'complex-required',
          t('thisIsRequiredField'),
          complexRequired
        ),
        user: Yup.array().test('user-required', t('thisIsRequiredField'), userRequired),
        building: Yup.mixed().test(
          'building-required',
          t('thisIsRequiredField'),
          buildingRequired
        ),
        rizer: Yup.mixed().test(
          'rizer-required',
          t('thisIsRequiredField'),
          rizerRequired
        ),
        floor: Yup.array().test(
          'floor-required',
          t('thisIsRequiredField'),
          floorRequired
        ),
      })
      .test('unique-level-type', t('ObjectLevelUserTypeSelected'), uniqueLevelType)
      .test('unique-user-type', t('ObjectLevelUserTypeSelected'), uniqueUserType)
  ),
});
