import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray, FieldProps } from 'formik';
import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { Wrapper, DetailToolbar, CustomScrollbar, InputField } from '@ui/index';
import { AccessGroupSelectField } from '@features/access-group-select';

import {
  detailSubmitted,
  changeMode,
  changedDetailVisibility,
  $mode,
  changedVisibilityDeleteModal,
  levelEntity,
  $formattedOpened,
} from '../../models';
import { LevelCard } from '../level-card';
import { validationSchema } from './validation';
import { useStyles } from './styles';
import { Opened } from '../../interfaces';

export const Detail: FC = memo(() => {
  const { t } = useTranslation();
  const opened = useStore($formattedOpened) as Opened;
  const mode = useStore($mode);

  const classes = useStyles();

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm }) => {
          const onEdit = () => changeMode('edit');
          const onClose = () => changedDetailVisibility(false);
          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };
          const onDelete = () => changedVisibilityDeleteModal(true);

          return (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.toolbar}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <div className={classes.content}>
                <CustomScrollbar>
                  <div className={classes.contentForm}>
                    <Field
                      name="title"
                      mode={mode}
                      label={t('NameTitle')}
                      placeholder={t('EnterTheNameOfTheUserGroup')}
                      component={InputField}
                      required
                    />
                    {!isNew && mode === 'view' && values.access_groups.length > 0 && (
                      <Field
                        name="access_groups"
                        label={t('InclusionInAccessGroups')}
                        mode="view"
                        component={AccessGroupSelectField}
                        isMulti
                      />
                    )}
                    <div className={classes.levelTitle}>{t('ObjectLevels')}</div>
                    <FieldArray
                      name="levels"
                      render={({ push, remove }) => {
                        const levelCard = values.levels.map(
                          (_: unknown, index: number) => (
                            <Field
                              key={index}
                              name={`levels[${index}]`}
                              render={({ field, form }: FieldProps) => {
                                const removeLevel = () => remove(index);

                                return (
                                  <LevelCard
                                    index={index}
                                    parentName={field.name}
                                    remove={removeLevel}
                                    value={field.value}
                                    setFieldValue={form.setFieldValue}
                                  />
                                );
                              }}
                            />
                          )
                        );

                        const onClick = () => push(levelEntity);

                        const addButton =
                          mode === 'edit' ? (
                            <Button color="primary" onClick={onClick}>
                              {`+ ${t('AddLevel')}`}
                            </Button>
                          ) : null;

                        return (
                          <>
                            {levelCard}
                            {addButton}
                          </>
                        );
                      }}
                    />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});
