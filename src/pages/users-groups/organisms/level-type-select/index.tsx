import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { Value } from '../../interfaces';
import { $data, $isLoading, fxGetList } from '../../models/level-type-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const LevelTypeSelect: FC<object> = (props) => {
  useEffect(() => {
    fxGetList({ object: 'user-level-type' });
  }, []);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const LevelTypeSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return <SelectField component={<LevelTypeSelect />} onChange={onChange} {...props} />;
};
