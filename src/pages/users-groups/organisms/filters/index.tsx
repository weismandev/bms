import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { AccessGroupSelectField } from '@features/access-group-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';

import { UserTypeSelectField } from '../user-type-select';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { Value, Filters as FiltersInterface } from '../../interfaces';
import { useStyles } from './styles';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters) as FiltersInterface;

  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            const onChangeComplex = (value: Value) => {
              setFieldValue('complexes', value);
              setFieldValue('houses', []);
            };

            return (
              <Form className={classes.form}>
                <Field
                  name="user_type"
                  label={t('UsersTypes')}
                  placeholder={t('SelectUsersTypes')}
                  component={UserTypeSelectField}
                  isMulti
                />
                <Field
                  name="complexes"
                  label={t('objects')}
                  placeholder={t('selectObjects')}
                  component={ComplexSelectField}
                  isMulti
                  onChange={onChangeComplex}
                />
                <Field
                  name="houses"
                  label={t('Buildings')}
                  placeholder={t('selectBbuildings')}
                  component={HouseSelectField}
                  complex={values.complexes.length > 0 && values.complexes}
                  isMulti
                />
                <Field
                  name="accessGroups"
                  label={t('OneInclusionInAccessGroups')}
                  placeholder={t('SelectAccessGroup')}
                  component={AccessGroupSelectField}
                  isMulti
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
