import { FC, useState } from 'react';
import { useStore } from 'effector-react';
import { Collapse } from '@mui/material';
import { ExpandMore, ExpandLess, HighlightOff } from '@mui/icons-material';
import { Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { IconButton } from '@ui/index';

import { ComplexLevel, BuildingLevel, RizerLevel, FloorLevel } from '../level';
import { LevelTypeSelectField } from '../level-type-select';
import { UserTypeSelectField } from '../user-type-select';
import { Level, Value } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { useStyles, styles } from './styles';

interface Props {
  index: number;
  parentName: string;
  remove: () => void;
  value: Level;
  setFieldValue: (field: string, value: any) => void;
}

export const LevelCard: FC<Props> = ({
  index,
  parentName,
  remove,
  value,
  setFieldValue,
}) => {
  const { t } = useTranslation();
  const mode = useStore($mode);
  const isFilterOpen = useStore($isFilterOpen);

  const classes = useStyles();

  const [expanded, setExpanded] = useState(true);

  const handleExpanded = () => setExpanded(!expanded);

  const handleChangeLevelType = (levelType: Value) => {
    setFieldValue(`${parentName}.level_type`, levelType);
    setFieldValue(`${parentName}.complex`, []);
    setFieldValue(`${parentName}.building`, []);
    setFieldValue(`${parentName}.rizer`, []);
    setFieldValue(`${parentName}.floor`, []);
    setFieldValue(`${parentName}.user`, []);
  };

  const handleChangeUserType = (userType: Value) => {
    setFieldValue(`${parentName}.user_type`, userType);
    setFieldValue(`${parentName}.user`, []);
  };

  const twoFields = isFilterOpen ? classes.twoFieldsWithFilters : classes.twoFields;
  const field = isFilterOpen ? classes.fieldWithFilters : classes.field;

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <span className={classes.headerTitle}>
          {t('Level')} {index + 1}
        </span>
        <div className={classes.headerButtons}>
          {index > 0 && mode === 'edit' && (
            <IconButton style={styles.button} onClick={remove}>
              <HighlightOff titleAccess={t('remove')} style={styles.deleteIcon} />
            </IconButton>
          )}
          <IconButton style={styles.button} onClick={handleExpanded}>
            {expanded ? (
              <ExpandMore titleAccess={t('Collapse')} style={styles.expandIcon} />
            ) : (
              <ExpandLess titleAccess={t('Expand')} style={styles.expandIcon} />
            )}
          </IconButton>
        </div>
      </div>
      <Collapse in={expanded} timeout="auto">
        <div className={classes.content}>
          <div className={twoFields}>
            <div className={field}>
              <Field
                name={`${parentName}.level_type`}
                label={t('ObjectLevel')}
                placeholder={t('ChooseALevel')}
                component={LevelTypeSelectField}
                mode={mode}
                divider={false}
                required
                onChange={handleChangeLevelType}
              />
            </div>
            <div className={field}>
              <Field
                name={`${parentName}.user_type`}
                label={t('UserType')}
                placeholder={t('chooseType')}
                component={UserTypeSelectField}
                mode={mode}
                divider={false}
                required
                onChange={handleChangeUserType}
              />
            </div>
          </div>
          <ComplexLevel
            type={value.level_type}
            parentName={parentName}
            userType={value.user_type}
            complex={value.complex}
            setFieldValue={setFieldValue}
          />
          <BuildingLevel
            type={value.level_type}
            parentName={parentName}
            userType={value.user_type}
            complex={value.complex}
            building={value.building}
            setFieldValue={setFieldValue}
          />
          <RizerLevel
            type={value.level_type}
            parentName={parentName}
            userType={value.user_type}
            complex={value.complex}
            setFieldValue={setFieldValue}
            building={value.building}
          />
          <FloorLevel
            type={value.level_type}
            parentName={parentName}
            userType={value.user_type}
            complex={value.complex}
            setFieldValue={setFieldValue}
            building={value.building}
            rizers={value.rizer}
          />
        </div>
      </Collapse>
    </div>
  );
};
