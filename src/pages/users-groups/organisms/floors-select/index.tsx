import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';

import { SelectControl, SelectField } from '@ui/index';

import { Value } from '../../interfaces';
import { $data, $isLoading, fxGetList } from '../../models/floors-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  building?: Value;
  rizers?: Value[];
}

export const FloorsSelect: FC<Props> = ({ building = null, rizers = [], ...props }) => {
  useEffect(() => {
    if (building) {
      fxGetList({ id: building.id });
    }
  }, [building]);

  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const { t } = useTranslation();

  const options =
    rizers.length > 0
      ? rizers.reduce((acc: { id: string; title: string }[], rizer) => {
          const floors = data[rizer.id] || [];

          const floorsData = floors.map((floor: number) => ({
            id: `${floor}${rizer.id}`,
            title: `${rizer.title} ${t('Floor').toLowerCase()} ${floor}`,
            number: floor,
          }));

          return [...acc, ...floorsData];
        }, [])
      : [];

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const FloorsSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return <SelectField component={<FloorsSelect />} onChange={onChange} {...props} />;
};
