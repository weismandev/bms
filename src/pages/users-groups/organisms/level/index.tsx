import { FC } from 'react';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { RizerSelectField } from '@features/rizer-select';
import { EmployeeSelectField } from '@features/employee-select';
import { PeopleSelectField } from '@features/people-select';

import { Value } from '../../interfaces';
import { $mode } from '../../models';
import { FloorsSelectField } from '../floors-select';
import { useStyles, styles } from '../level-card/styles';

interface UsersProps {
  userType: Value;
  parentName: string;
  object: any;
}

const formatObjects = ({
  complex,
  building,
}: {
  complex: Value[] | Value;
  building: Value[] | Value;
}) => {
  const objects = [];

  if (Array.isArray(complex) && complex.length > 0) {
    const formattedComplex = complex.map((item) => ({ id: item.id, type: 'complex' }));

    objects.push(formattedComplex);
  }

  if (typeof complex === 'object' && (complex as Value)?.id) {
    objects.push({ id: (complex as Value).id, type: 'complex' });
  }

  if (Array.isArray(building) && building.length > 0) {
    const formattedBuilding = building.map((item) => ({ id: item.id, type: 'building' }));

    objects.push(formattedBuilding);
  }

  if (typeof building === 'object' && (building as Value)?.id) {
    objects.push({ id: (building as Value).id, type: 'building' });
  }

  return objects.reverse()[0];
};

const Users: FC<UsersProps> = ({ userType, parentName, object }) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  if (userType?.type === 'resident') {
    return (
      <div className={classes.field} style={styles.fullWidth}>
        <Field
          label={t('People')}
          placeholder={t('SelectPeople')}
          component={PeopleSelectField}
          name={`${parentName}.user`}
          mode={mode}
          isMulti
          required
          divider={false}
          object={object}
        />
      </div>
    );
  }

  if (userType?.type === 'employee') {
    return (
      <div className={classes.field} style={styles.fullWidth}>
        <Field
          label={t('MCEmployees')}
          placeholder={t('SelectEmployees')}
          component={EmployeeSelectField}
          name={`${parentName}.user`}
          mode={mode}
          isMulti
          required
          divider={false}
        />
      </div>
    );
  }
  return null;
};

interface LevelProps {
  type: Value;
  parentName: string;
  userType: Value;
  complex: Value[] | Value;
  setFieldValue: (field: string, value: Value | []) => void;
}

export const ComplexLevel: FC<LevelProps> = ({
  parentName,
  type,
  userType,
  complex,
  setFieldValue,
}) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  if (typeof type === 'object' && type?.type === 'complex') {
    const handleChangeComplex = (value: Value) => {
      setFieldValue(`${parentName}.complex`, value);
      setFieldValue(`${parentName}.user`, []);
    };

    const object = userType?.type === 'resident' && formatObjects({ complex });

    return (
      <>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.complex`}
            label={t('AnObject')}
            placeholder={t('selectObject')}
            component={ComplexSelectField}
            mode={mode}
            divider={false}
            required
            isMulti
            onChange={handleChangeComplex}
          />
        </div>
        {typeof userType === 'object' && (
          <Users userType={userType} parentName={parentName} object={object} />
        )}
      </>
    );
  }

  return null;
};

interface BuildingLevelProps {
  type: Value;
  parentName: string;
  userType: Value;
  complex: Value[] | Value;
  building: Value[] | Value;
  setFieldValue: (field: string, value: Value | []) => void;
}

export const BuildingLevel: FC<BuildingLevelProps> = ({
  type,
  parentName,
  userType,
  complex,
  building,
  setFieldValue,
}) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  if (typeof type === 'object' && type?.type === 'building') {
    const handleChangeComplex = (value: Value) => {
      setFieldValue(`${parentName}.complex`, value);
      setFieldValue(`${parentName}.building`, []);
      setFieldValue(`${parentName}.user`, []);
    };

    const handleChangeBuilding = (value: Value) => {
      setFieldValue(`${parentName}.building`, value);
      setFieldValue(`${parentName}.user`, []);
    };

    const object = userType?.type === 'resident' && formatObjects({ complex, building });

    return (
      <>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.complex`}
            label={t('AnObject')}
            placeholder={t('selectObject')}
            component={ComplexSelectField}
            mode={mode}
            divider={false}
            required
            isMulti
            onChange={handleChangeComplex}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.building`}
            label={t('building')}
            placeholder={t('selectBbuilding')}
            component={HouseSelectField}
            complex={complex}
            mode={mode}
            divider={false}
            required
            isMulti
            onChange={handleChangeBuilding}
          />
        </div>
        {typeof userType === 'object' && (
          <Users userType={userType} parentName={parentName} object={object} />
        )}
      </>
    );
  }

  return null;
};

interface RizersLevelProps {
  type: Value;
  parentName: string;
  userType: Value;
  complex: Value[] | Value;
  setFieldValue: (field: string, value: Value | []) => void;
  building: Value[] | Value;
}

export const RizerLevel: FC<RizersLevelProps> = ({
  type,
  parentName,
  userType,
  complex,
  setFieldValue,
  building,
}) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  if (typeof type === 'object' && type?.type === 'rizer') {
    const handleChangeComplex = (value: Value) => {
      setFieldValue(`${parentName}.complex`, value);
      setFieldValue(`${parentName}.building`, []);
      setFieldValue(`${parentName}.rizer`, []);
      setFieldValue(`${parentName}.user`, []);
    };

    const handleChangeBuilding = (value: Value) => {
      setFieldValue(`${parentName}.building`, value);
      setFieldValue(`${parentName}.rizer`, []);
      setFieldValue(`${parentName}.user`, []);
    };

    const object = userType?.type === 'resident' && formatObjects({ complex, building });

    return (
      <>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.complex`}
            label={t('AnObject')}
            placeholder={t('selectObject')}
            component={ComplexSelectField}
            mode={mode}
            divider={false}
            required
            onChange={handleChangeComplex}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.building`}
            label={t('building')}
            placeholder={t('selectBbuilding')}
            component={HouseSelectField}
            complex={complex ? [complex] : []}
            mode={mode}
            divider={false}
            required
            onChange={handleChangeBuilding}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.rizer`}
            label={t('rizer')}
            placeholder={t('ChooseEntrance')}
            component={RizerSelectField}
            buildings={building ? [building] : []}
            mode={mode}
            divider={false}
            required
            isMulti
          />
        </div>
        {typeof userType === 'object' && (
          <Users
            userType={userType}
            parentName={parentName}
            object={object ? [object] : null}
          />
        )}
      </>
    );
  }

  return null;
};

interface FloorLevelProps {
  type: Value;
  parentName: string;
  userType: Value;
  complex: Value[] | Value;
  setFieldValue: (field: string, value: Value | []) => void;
  building: Value[] | Value;
  rizers: Value[] | Value;
}

export const FloorLevel: FC<FloorLevelProps> = ({
  type,
  parentName,
  userType,
  complex,
  setFieldValue,
  building,
  rizers,
}) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  if (typeof type === 'object' && type?.type === 'floor') {
    const handleChangeComplex = (value: Value) => {
      setFieldValue(`${parentName}.complex`, value);
      setFieldValue(`${parentName}.building`, []);
      setFieldValue(`${parentName}.rizer`, []);
      setFieldValue(`${parentName}.floor`, []);
      setFieldValue(`${parentName}.user`, []);
    };

    const handleChangeBuilding = (value: Value) => {
      setFieldValue(`${parentName}.building`, value);
      setFieldValue(`${parentName}.rizer`, []);
      setFieldValue(`${parentName}.floor`, []);
      setFieldValue(`${parentName}.user`, []);
    };

    const handleChangeRizer = (value: Value) => {
      setFieldValue(`${parentName}.rizer`, value);
      setFieldValue(`${parentName}.floor`, []);
    };

    const object = userType?.type === 'resident' && formatObjects({ complex, building });

    return (
      <>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.complex`}
            label={t('AnObject')}
            placeholder={t('selectObject')}
            component={ComplexSelectField}
            mode={mode}
            divider={false}
            required
            onChange={handleChangeComplex}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.building`}
            label={t('building')}
            placeholder={t('selectBbuilding')}
            component={HouseSelectField}
            complex={complex ? [complex] : []}
            mode={mode}
            divider={false}
            required
            onChange={handleChangeBuilding}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.rizer`}
            label={t('rizer')}
            placeholder={t('ChooseEntrance')}
            component={RizerSelectField}
            buildings={building ? [building] : []}
            mode={mode}
            divider={false}
            required
            onChange={handleChangeRizer}
          />
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            name={`${parentName}.floor`}
            label={t('Floor')}
            placeholder={t('SelectFloor')}
            component={FloorsSelectField}
            building={building}
            rizers={rizers ? [rizers] : []}
            mode={mode}
            divider={false}
            required
            isMulti
          />
        </div>
        {typeof userType === 'object' && (
          <Users
            userType={userType}
            parentName={parentName}
            object={object ? [object] : null}
          />
        )}
      </>
    );
  }

  return null;
};
