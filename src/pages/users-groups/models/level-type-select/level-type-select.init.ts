import { signout } from '@features/common';

import { usersGroupsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './level-type-select.model';

fxGetList.use(usersGroupsApi.getComplType);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.types)) {
      return [];
    }

    return result.types.map((item) => ({
      id: item.id,
      title: item.title,
      type: item.type,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
