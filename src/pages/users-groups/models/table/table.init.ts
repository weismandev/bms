import { sample } from 'effector';
import { signout } from '@features/common';
import {
  detailClosed,
  addClicked,
  fxCreateUpdateUsersGroup,
  fxDeleteUsersGroup,
  $entityId,
} from '../detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

$openRow
  .on(fxCreateUpdateUsersGroup.done, (state, { params, result }) =>
    !params.id && result.groups.id ? result.groups.id : state
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked, fxDeleteUsersGroup.done]);

sample({
  clock: pageMounted,
  source: { id: $entityId, openRow: $openRow },
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => Number.parseInt(id as string, 10),
  target: $openRow,
});
