import { format } from 'date-fns';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { Group } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('NameTitle') ?? '',
    width: 350,
    sortable: true,
    hideable: false,
  },
  {
    field: 'objects',
    headerName: t('objects') ?? '',
    width: 350,
    sortable: false,
    hideable: true,
  },
  {
    field: 'houses',
    headerName: t('Buildings') ?? '',
    width: 350,
    sortable: false,
    hideable: true,
  },
  {
    field: 'users_count',
    headerName: t('CountOfUsers') ?? '',
    width: 200,
    sortable: true,
    hideable: true,
  },
  {
    field: 'access_groups',
    headerName: t('navMenu.security.accessGroups') ?? '',
    width: 230,
    sortable: false,
    hideable: true,
  },
  {
    field: 'created_at',
    headerName: t('DateOfCreation') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
  {
    field: 'updated_at',
    headerName: t('DateOfChange') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
];

export const {
  $tableParams,
  $pagination,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  sortChanged,
  searchChanged,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
  searchForm,
  $selectRow
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

const formatData = (groups: Group[]) =>
  groups.map((group) => {
    const objects =
      Array.isArray(group?.complex) && group.complex.length > 0
        ? group.complex.length === 1
          ? group.complex[0]?.title || '-'
          : group.complex.length
        : '-';

    const houses =
      Array.isArray(group?.buildings) && group.buildings.length > 0
        ? group.buildings.length === 1
          ? group.buildings[0]?.title || '-'
          : group.buildings.length
        : '-';

    const accessGroups = Array.isArray(group?.access_group)
      ? group.access_group.length === 1
        ? group.access_group[0]?.title || '-'
        : group.access_group.length
      : '-';

    return {
      id: group.id,
      title: group?.title || '-',
      objects,
      houses,
      users_count: group?.users_count || 0,
      access_groups: accessGroups,
      created_at: group?.created_at
        ? format(new Date(group.created_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
      updated_at: group?.updated_at
        ? format(new Date(group.updated_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
    };
  });

export const $count = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ groups }) =>
  groups && groups.length > 0 ? formatData(groups) : []
);
