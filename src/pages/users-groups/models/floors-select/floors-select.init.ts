import { signout } from '@features/common';

import { usersGroupsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './floors-select.model';

fxGetList.use(usersGroupsApi.getFloors);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.building?.entrances)) {
      return {};
    }

    return result.building.entrances.reduce((acc, entrance) => {
      const floors = entrance.apartments.map((apartment) => apartment.floor);
      const sortedFloors = [...new Set(floors)].sort((a, b) => a - b);

      return { ...acc, ...{ [entrance.number]: sortedFloors } };
    }, {});
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
