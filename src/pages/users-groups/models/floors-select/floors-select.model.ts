import { createEffect, createStore } from 'effector';

import { GetFloorsPayload, GetFloorsResponse } from '../../interfaces';

export const fxGetList = createEffect<GetFloorsPayload, GetFloorsResponse, Error>();

export const $data = createStore<{ [key: number]: number[] }>({});
export const $isLoading = createStore<boolean>(false);
