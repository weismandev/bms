import { merge, sample } from 'effector';
import { pending } from 'patronum/pending';
import { GridSortModel } from '@mui/x-data-grid-pro';
import { signout } from '@features/common';
import { usersGroupsApi } from '../../api';
import { GetUsersGroupsListPayload, Value, Filters } from '../../interfaces';
import {
  fxGetUsersGroup,
  fxCreateUpdateUsersGroup,
  fxDeleteUsersGroup,
  fxCreateUpdateLevel,
  fxGetPeopleInfo,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  fxGetUsersGroupsList,
  $error,
  $isLoading,
  $isErrorDialogOpen,
  $rawData,
  $isLoadingList,
} from './page.model';

interface Table {
  page: number;
  per_page: number;
  search: string;
  sorting: GridSortModel;
}

fxGetUsersGroupsList.use(usersGroupsApi.getUsersGroupsList);
fxGetUsersGroup.use(usersGroupsApi.getUsersGroupsList);
fxCreateUpdateUsersGroup.use(usersGroupsApi.createUpdateUsersGroup);
fxDeleteUsersGroup.use(usersGroupsApi.deleteUsersGroup);
fxCreateUpdateLevel.use(usersGroupsApi.createUpdateLevel);
fxGetPeopleInfo.use(usersGroupsApi.getPeopleInfo);

const errorOccured = merge([
  fxGetUsersGroupsList.fail,
  fxGetUsersGroup.fail,
  fxCreateUpdateUsersGroup.fail,
  fxDeleteUsersGroup.fail,
  fxCreateUpdateLevel.fail,
  fxGetPeopleInfo.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetUsersGroup,
        fxCreateUpdateUsersGroup,
        fxDeleteUsersGroup,
        fxCreateUpdateLevel,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingList
  .on(fxGetUsersGroupsList.pending, (_, loading) => loading)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: [
    pageMounted,
    $tableParams,
    fxCreateUpdateLevel.done,
    fxDeleteUsersGroup.done,
    $filters,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
  },
  fn: ({ table, filters }: { table: Table; filters: Filters }) => {
    const payload: GetUsersGroupsListPayload = {
      compl: 1,
      limit: table.per_page,
      offset: (table.page - 1) * table.per_page,
      search: table.search,
      show_ag: 1,
      show_objects_compl: 1,
      show_users_count: 1,
    };

    if (table.sorting.length > 0) {
      payload.sort_by = table.sorting[0].field || 'id';
      payload.sort_order = table.sorting[0].sort || 'desc';
    }

    if (filters.user_type.length > 0) {
      payload.user_compl_type_ids = filters.user_type
        .map((item: Value) => item.id)
        .join(',');
    }

    if (filters.complexes.length > 0) {
      payload.complex_id = filters.complexes.map((item: Value) => item.id).join(',');
    }

    if (filters.houses.length > 0) {
      payload.buildings = filters.houses.map((item: Value) => item.id).join(',');
    }

    if (filters.accessGroups.length > 0) {
      payload.access_group_ids = filters.accessGroups
        .map((item: Value) => item.id)
        .join(',');
    }

    return payload;
  },
  target: fxGetUsersGroupsList,
});

$rawData
  .on(fxGetUsersGroupsList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);
