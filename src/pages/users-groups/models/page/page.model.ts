import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import {
  GetUsersGroupsListPayload,
  GetUsersGroupsListResponse,
} from '../../interfaces/users-groups-list';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetUsersGroupsList = createEffect<
  GetUsersGroupsListPayload,
  GetUsersGroupsListResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetUsersGroupsListResponse>({
  meta: { total: 0 },
  groups: [],
});
export const $isLoadingList = createStore<boolean>(false);
