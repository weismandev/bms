import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  user_type: [],
  complexes: [],
  houses: [],
  accessGroups: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
