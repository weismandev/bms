import { createEffect, createEvent, createStore, combine, attach } from 'effector';

import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';
import { $data as $complexes } from '@features/complex-select';
import { $data as $buildings } from '@features/house-select';
import { $employees } from '@features/employee-select';
import i18n from '@shared/config/i18n';

import { $data as $levelType } from '../level-type-select/level-type-select.model';
import { $data as $userType } from '../user-type-select/user-type-select.model';
import {
  GetUsersGroupsListPayload,
  GetUsersGroupsListResponse,
  CreateUpdateUsersGroupPayload,
  CreateUpdateUsersGroupResponse,
  DeleteUsersGroupPayload,
  CreateUpdateLevelPayload,
  Value,
  GetPeopleInfoPayload,
  GetPeopleInfoResponse,
} from '../../interfaces';

const { t } = i18n;

interface Level {
  level_type: number;
  user_type: number;
  complex: number[];
  building: number[];
  rizer: number[];
  user: number[];
  floor: number[];
}

export const levelEntity = {
  level_type: '',
  user_type: '',
  complex: [],
  user: [],
  building: [],
  rizer: [],
  floor: [],
};

export const newEntity = {
  title: '',
  access_groups: [],
  levels: [levelEntity],
};

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map(
  (pathname: string) => pathname.split('/')[2] || null
);

export const deleteUserGroup = createEvent<void>();

export const fxGetUsersGroup = createEffect<
  GetUsersGroupsListPayload,
  GetUsersGroupsListResponse,
  Error
>();
export const fxCreateUpdateUsersGroup = createEffect<
  CreateUpdateUsersGroupPayload,
  CreateUpdateUsersGroupResponse,
  Error
>();
export const fxDeleteUsersGroup = createEffect<DeleteUsersGroupPayload, object, Error>();
export const fxCreateUpdateLevel = createEffect<
  CreateUpdateLevelPayload,
  object,
  Error
>();
export const fxGetPeopleInfo = createEffect<
  GetPeopleInfoPayload,
  GetPeopleInfoResponse,
  Error
>();
export const fxGetPeopleInfoEach = attach({
  async effect(_: unknown, params: number[]) {
    await Promise.all(
      params.forEach((id) => {
        fxGetPeopleInfo({ id });
      })
    );
  },
});

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

const formatArray = (array: Value[]) =>
  array.reduce(
    (acc: { [key: number]: Value }, value) => ({
      ...acc,
      ...{ [value.id]: value },
    }),
    {}
  );

const getUsersName = (
  users: number[],
  userType: Value,
  formattedPeople: { [key: number]: Value },
  formattedEmployees: { [key: number]: Value }
) => {
  if (typeof userType !== 'object') {
    return [];
  }

  if (userType.type === 'resident') {
    return users.map((user) =>
      formattedPeople[user]
        ? {
            id: Number.parseInt(formattedPeople[user].id, 10),
            title: formattedPeople[user].title,
          }
        : ''
    );
  }

  if (userType.type === 'employee') {
    return users.map((user) => formattedEmployees[user] || '');
  }

  return [];
};

export const $people = createStore<Value[]>([]);
export const $formattedOpened = combine(
  $opened,
  $levelType,
  $userType,
  $complexes,
  $buildings,
  $people,
  $employees,
  (opened, levelType, userType, complexes, buildings, people, employees) => {
    const newOpened = { ...opened };

    const formattedLevelType = formatArray(levelType);
    const formattedUserType = formatArray(userType);
    const formattedComplexes = formatArray(complexes);
    const formattedBuildings = formatArray(buildings);
    const formattedPeople = formatArray(people);
    const formattedEmployees = formatArray(employees);

    const formattedLevels = newOpened.levels.map((level: Level) => {
      const levelType = formattedLevelType[level.level_type] || '';
      const userType = formattedUserType[level.user_type] || '';

      const isSingleValue =
        typeof levelType === 'object' &&
        (levelType.type === 'rizer' || levelType.type === 'floor');

      const rizer =
        typeof levelType === 'object' && levelType.type === 'floor'
          ? {
              id: level.rizer[0],
              title:
                level.rizer[0] === 0 ? t('gate') : `${t('rizer')} №${level.rizer[0]}`,
            }
          : level.rizer;

      return {
        level_type: levelType,
        user_type: userType,
        complex: isSingleValue
          ? formattedComplexes[level.complex[0]] || ''
          : level.complex.map((complex) => formattedComplexes[complex] || ''),
        building: isSingleValue
          ? formattedBuildings[level.building[0]] || ''
          : level.building.map((building) => formattedBuildings[building] || ''),
        rizer,
        floor: level.floor.map((f) =>
          !Array.isArray(rizer)
            ? {
                id: `${f}${rizer.id}`,
                title: `${rizer.title} ${t('Floor').toLowerCase()} ${f}`,
                number: f,
              }
            : f
        ),
        user: getUsersName(level.user, userType, formattedPeople, formattedEmployees),
      };
    });

    newOpened.levels = formattedLevels;

    return newOpened;
  }
);
