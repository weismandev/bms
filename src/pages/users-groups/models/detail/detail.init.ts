import { sample } from 'effector';
import { delay } from 'patronum';

import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';

import { Level, CreateUpdateLevelPayload, Opened } from '../../interfaces';
import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  openViaUrl,
  $isDetailOpen,
  fxGetUsersGroup,
  $opened,
  newEntity,
  entityApi,
  $mode,
  fxCreateUpdateUsersGroup,
  deleteUserGroup,
  fxDeleteUsersGroup,
  fxCreateUpdateLevel,
  fxGetPeopleInfo,
  $people,
  $entityId,
  changedDetailVisibility,
  $path,
  fxGetPeopleInfoEach,
} from './detail.model';

$isDetailOpen
  .on(fxGetUsersGroup.done, () => true)
  .off(openViaUrl)
  .reset([signout, pageUnmounted, fxDeleteUsersGroup.done]);

sample({
  clock: openViaUrl,
  fn: (id) => ({ id, group_compl: 1, show_ag: 1 }),
  target: fxGetUsersGroup,
});

$opened
  .on(fxGetUsersGroup.done, (_, { result }) => {
    if (result?.groups && result?.groups[0]) {
      const data = result.groups[0];

      const levels = Array.isArray(data?.compl)
        ? data.compl.map((item: any) => ({
            level_type: item.level_type,
            user_type: item.user_type,
            complex: Array.isArray(item?.complex_id) ? item.complex_id : [],
            user: Array.isArray(item?.userdata_id) ? item.userdata_id : [],
            building: Array.isArray(item?.buildings) ? item.buildings : [],
            rizer: Array.isArray(item?.rizer) ? item.rizer : [],
            floor: Array.isArray(item?.floor) ? item.floor : [],
          }))
        : [];

      return {
        id: data.id,
        title: data?.title || '',
        access_groups: Array.isArray(data?.access_group)
          ? data.access_group.map((item) => ({
              id: item.id,
              title: item.title,
            }))
          : [],
        levels,
      };
    }

    return newEntity;
  })
  .reset([signout, pageUnmounted, fxGetUsersGroup]);

const formatLevels = (levels: Level[]) =>
  levels.map((level) => ({
    level_type: level.level_type.id,
    user_type: level.user_type.id,
    complex_id: Array.isArray(level.complex)
      ? level.complex.map((item) => item.id)
      : [level.complex.id],
    userdata_id: level.user.map((item) => item.id),
    buildings: Array.isArray(level.building)
      ? level.building.map((item) => item.id)
      : [level.building.id],
    rizer: Array.isArray(level.rizer)
      ? level.rizer.map((item) => (typeof item === 'object' ? item.id : item))
      : [level.rizer.id],
    floor: Array.isArray(level.floor) ? level.floor.map((item) => item.number) : [],
  }));

const formatData = (data: Opened) => {
  const levels =
    Array.isArray(data.levels) && data.levels.length > 0 ? formatLevels(data.levels) : [];

  return {
    title: data.title,
    levels,
  };
};

sample({
  clock: entityApi.create,
  fn: (data: Opened) => formatData(data),
  target: fxCreateUpdateUsersGroup,
});

sample({
  clock: entityApi.update,
  fn: (data: Opened) => ({ id: data.id, ...formatData(data) }),
  target: fxCreateUpdateUsersGroup,
});

$mode.reset([signout, pageUnmounted, fxCreateUpdateLevel.done]);

sample({
  clock: fxCreateUpdateLevel.done,
  filter: ({ params: { group_id } }) => Boolean(group_id),
  fn: ({ params: { group_id } }) => ({
    id: group_id,
    group_compl: 1,
  }),
  target: fxGetUsersGroup,
});

sample({
  clock: deleteUserGroup,
  source: $opened,
  fn: ({ id }: { id: number }) => ({ id }),
  target: fxDeleteUsersGroup,
});

sample({
  clock: fxCreateUpdateUsersGroup.done,
  fn: ({ params, result }) => {
    const payload: CreateUpdateLevelPayload = {
      compls: params.levels,
      use_compls: 1,
    };

    if (result.groups.id) {
      payload.group_id = result.groups.id;
    } else {
      payload.group_id = params.id;
    }

    return payload;
  },
  target: fxCreateUpdateLevel,
});

const formatCompl = (compl: { userdata_id: number[]; user_type: number }[]) =>
  compl.filter(
    (item) =>
      Array.isArray(item?.userdata_id) &&
      item.userdata_id.length > 0 &&
      item.user_type === 422
  );

sample({
  clock: fxGetUsersGroup.done,
  filter: ({ result }) => {
    if (!Array.isArray(result?.groups)) {
      return false;
    }

    if (!Array.isArray(result.groups[0]?.compl)) {
      return false;
    }

    const compl = formatCompl(result.groups[0]?.compl);

    return compl.length > 0;
  },
  fn: ({ result }) => {
    const compl = formatCompl(result.groups[0]?.compl);

    return compl.map((i) => i.userdata_id).flat();
  },
  target: fxGetPeopleInfoEach,
});

$people
  .on(fxGetPeopleInfo.done, (state, { result }) => {
    if (!Array.isArray(result?.userdata)) {
      return state;
    }

    const people = result.userdata.map((user) => ({
      id: user.id,
      title: user.title,
    }));

    return [...state, ...people];
  })
  .reset([signout, pageUnmounted]);

sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: entityId,
    group_compl: 1,
    show_ag: 1,
  }),
  target: fxGetUsersGroup,
});

const delayedRedirect = delay({ source: fxGetUsersGroup.fail, timeout: 1000 });

sample({
  clock: delayedRedirect,
  fn: () => '.',
  target: historyPush,
});

sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

sample({
  clock: fxCreateUpdateUsersGroup.done,
  source: $path,
  filter: (
    _,
    {
      params,
      result: {
        groups: { id },
      },
    }
  ) => {
    if (!params?.id && id) {
      return true;
    }

    return false;
  },
  fn: (
    path,
    {
      result: {
        groups: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

sample({
  clock: fxDeleteUsersGroup.done,
  source: $path,
  filter: (_, params) => Boolean(params),
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);
