import { sample } from 'effector';
import { Error } from '@mui/icons-material';

import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';

import { pageUnmounted } from '../page/page.model';
import { deleteUserGroup, $opened } from '../detail/detail.model';
import { $isOpenDeleteModal, changedVisibilityDeleteModal } from './delete-modal.model';
import { Value } from '../../interfaces';

const { t } = i18n;

sample({
  clock: changedVisibilityDeleteModal,
  source: $opened,
  filter: ({ access_groups }) => (access_groups as Value[]).length > 0,
  fn: () => ({
    isOpen: true,
    text: t('attention'),
    color: '#FFA500',
    Icon: Error,
    message: t('UserGroupUse'),
  }),
  target: changeNotification,
});

sample({
  clock: changedVisibilityDeleteModal,
  source: $opened,
  fn: ({ access_groups }, visibility) => {
    if ((access_groups as Value[]).length === 0) {
      return visibility;
    }

    return false;
  },
  target: $isOpenDeleteModal,
});

$isOpenDeleteModal
  .on(changedVisibilityDeleteModal, (_, visibility) => visibility)
  .reset([signout, pageUnmounted, deleteUserGroup]);
