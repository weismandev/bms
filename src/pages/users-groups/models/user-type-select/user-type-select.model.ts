import { createEffect, createStore } from 'effector';

import { GetComplTypePayload, GetComplTypeResponse, ComplType } from '../../interfaces';

export const fxGetList = createEffect<GetComplTypePayload, GetComplTypeResponse, Error>();

export const $data = createStore<ComplType[]>([]);
export const $isLoading = createStore<boolean>(false);
