import type { GridColDef } from '@mui/x-data-grid-pro';
import { createTableBag } from '@shared/tools/table';
import i18n from '@shared/config/i18n';
import { TypeItem } from '../../interfaces';
import { formatDuration } from '../../libs';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'type',
    headerName: t('type'),
    width: 300,
    sortable: false,
  },
  {
    field: 'duration',
    headerName: t('RequestTypeTerm'),
    width: 150,
    sortable: false,
  },
  {
    field: 'estimatedDuration',
    headerName: t('EstimatedDurationWork'),
    width: 275,
    sortable: false,
  },
  {
    field: 'paidType',
    headerName: t('PaidType'),
    width: 120,
    sortable: false,
  },
  {
    field: 'showInMobile',
    headerName: t('TypeDisplayInMobileApp'),
    width: 330,
    sortable: false,
  },
];

export const {
  $tableParams,
  $pagination,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  sortChanged,
  searchForm,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 100,
  },
});

const formatEstimatedDurationWork = (duration: number) => {
  if (!duration) {
    return '-';
  }

  const hours =
    Math.floor(duration / 60).toString().length === 1
      ? `0${Math.floor(duration / 60)}`
      : Math.floor(duration / 60);

  const minutes =
    (duration % 60).toString().length === 1 ? `0${duration % 60}` : duration % 60;

  return `${hours} ${t('ShortHour')} ${minutes} ${t('Min')}`;
};

const formatData = (types: TypeItem[]) =>
  types.map((type) => {
    const formattedType = {
      id: type.id,
      type: type?.title || '-',
      duration: type?.planned_duration_minutes
        ? formatDuration(type.planned_duration_minutes)
        : '-',
      estimatedDuration: formatEstimatedDurationWork(type.estimated_duration_work),
      paidType: Boolean(type?.paid),
      showInMobile: Boolean(type?.is_show_in_mobile),
      hierarchy: [type.id],
    };

    if (type?.level === 1 && type?.parent_id) {
      formattedType.hierarchy = [type.parent_id, type.id];
    }

    if (type?.level === 2 && type?.parent_id) {
      const firstParentId = types.find((item) => item.id === type.parent_id)?.parent_id;

      if (firstParentId) {
        formattedType.hierarchy = [firstParentId, type.parent_id, type.id];
      }
    }

    return formattedType;
  });

export const $count = $rawData.map(({ types }) =>
  Array.isArray(types) ? types.length : 0
);
export const $tableData = $rawData.map(({ types }) =>
  Array.isArray(types) && types.length > 0 ? formatData(types) : []
);
