import { combine } from 'effector';
import { connectStorage } from '@shared/tools/storage';
import { PageGate } from '../page/page.model';
import {
  $pagination,
  $pinnedColumns,
  changePinnedColumns,
  $visibilityColumns,
  visibilityColumnsChanged,
  $columns,
  mergeColumnsByPattern,
  paginationModelChanged,
} from './table.model';

const $pageSize = combine($pagination, ({ pageSize }) => pageSize);
const prependPageSizeChanged = (pageSize: number) => ({
  page: 0,
  pageSize,
});

export default connectStorage({
  storageKey: 'tickets-types',
  storageGate: PageGate,
  storageScheme: {
    columns: [$columns, mergeColumnsByPattern],
    pageSize: [$pageSize, paginationModelChanged.prepend(prependPageSizeChanged)],
    pinnedColumns: [$pinnedColumns, changePinnedColumns],
    visibilityColumns: [$visibilityColumns, visibilityColumnsChanged],
  },
});
