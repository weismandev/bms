import { sample } from 'effector';
import { signout } from '@features/common';
import {
  $entityId,
  addClicked,
  detailClosed,
  fxCreateTicketsType,
} from '../detail/detail.model';
import { filtersForm } from '../filters-form/filters-form.model';
import { pageMounted, pageUnmounted } from '../page/page.model';
import { $selectRow } from './table.model';

$selectRow
  .on(fxCreateTicketsType.done, (_, { result: { types } }) => [types[0]?.id])
  .reset([signout, pageUnmounted, addClicked, detailClosed, filtersForm.submitted]);

// если при маунте есть id в url, то выбирается строка с нужной записью
sample({
  clock: pageMounted,
  source: { id: $entityId, selectRow: $selectRow },
  filter: ({ selectRow }) => Array.isArray(selectRow) && selectRow.length === 0,
  fn: ({ id }) => [Number.parseInt(id, 10)],
  target: $selectRow,
});
