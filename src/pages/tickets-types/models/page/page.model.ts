import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { GetTicketsTypesPayload, GetTicketsTypesResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetTicketsTypes = createEffect<
  GetTicketsTypesPayload,
  GetTicketsTypesResponse,
  Error
>();
export const fxArchiveAllTicketsTypes = createEffect<void, object, Error>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isLoadingTable = createStore<boolean>(false);
export const $rawData = createStore<GetTicketsTypesResponse>({
  types: [],
});
