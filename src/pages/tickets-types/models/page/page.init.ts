import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { ticketsTypesApi } from '../../api';
import { GetTicketsTypesPayload } from '../../interfaces';
import { $archiveModalData } from '../archive-modal/archive-modal.model';
import {
  fxGetTicketsType,
  fxArchiveTicketsType,
  archive,
  fxCreateTicketsType,
  fxUpdateTicketsType,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  $isErrorDialogOpen,
  $isLoading,
  $isLoadingTable,
  $error,
  pageUnmounted,
  changedErrorDialogVisibility,
  fxGetTicketsTypes,
  pageMounted,
  $rawData,
  fxArchiveAllTicketsTypes,
} from './page.model';

fxGetTicketsTypes.use(ticketsTypesApi.getTicketsTypes);
fxGetTicketsType.use(ticketsTypesApi.getTicketsType);
fxArchiveTicketsType.use(ticketsTypesApi.archiveTicketsType);
fxArchiveAllTicketsTypes.use(ticketsTypesApi.archiveAllTicketsTypes);
fxCreateTicketsType.use(ticketsTypesApi.createTicketsType);
fxUpdateTicketsType.use(ticketsTypesApi.updateTicketsType);

const errorOccured = merge([
  fxGetTicketsTypes.fail,
  fxGetTicketsType.fail,
  fxArchiveTicketsType.fail,
  fxArchiveAllTicketsTypes.fail,
  fxCreateTicketsType.fail,
  fxUpdateTicketsType.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetTicketsType,
        fxArchiveTicketsType,
        fxArchiveAllTicketsTypes,
        fxCreateTicketsType,
        fxUpdateTicketsType,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetTicketsTypes.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);

// запрос списка типов
sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxArchiveTicketsType.done,
    fxArchiveAllTicketsTypes.done,
    fxCreateTicketsType.done,
    fxUpdateTicketsType.done,
  ],
  source: { table: $tableParams, filters: $filters },
  fn: ({ table, filters }) => {
    const payload: GetTicketsTypesPayload = {
      search: table.search,
      filters: {
        level: {
          id: '',
        },
      },
    };

    if (filters.isArchive) {
      payload.filters.archived = Number(filters.isArchive);
    } else {
      payload.filters.archived = 0;
    }

    if (filters.level === 0 || filters.level) {
      payload.filters.level.id = filters.level as number;
    }

    return payload;
  },
  target: fxGetTicketsTypes,
});

$rawData
  .on(fxGetTicketsTypes.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

// архивация всех типов
sample({
  clock: archive,
  source: $archiveModalData,
  filter: (archiveModalData) => archiveModalData.isAllArchivedTypes,
  target: fxArchiveAllTicketsTypes,
});
