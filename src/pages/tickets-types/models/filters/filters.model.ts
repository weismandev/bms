import { createFilterBag } from '@tools/factories';

export const filtersEntity = {
  isArchive: false,
  level: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(filtersEntity);
