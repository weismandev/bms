import { createEffect, createEvent, createStore } from 'effector';
import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';
import {
  TypeForm,
  TicketsTypePayload,
  GetTicketsTypeResponse,
  CreateUpdateTypePayload,
  CreateUpdateTypeResponse,
} from '../../interfaces';

export const newEntity: TypeForm = {
  id: null,
  title: '',
  parentId: null,
  parentTitle: '',
  parentLevel: 0,
  duration: '',
  isPaid: false,
  prepaymentType: null,
  costType: null,
  costFrom: '',
  costTo: '',
  isShowMobile: false,
  children: [],
  level: 0,
  isArchived: false,
  estimatedDuration: null,
  isAutomaticRouting: false,
  isReadonly: false,
};

export const {
  changedDetailVisibility,
  addClicked,
  openViaUrl,
  $isDetailOpen,
  $opened,
  detailClosed,
} = createDetailBag(newEntity);

export const archive = createEvent<void>();

export const fxGetTicketsType = createEffect<
  TicketsTypePayload,
  GetTicketsTypeResponse,
  Error
>();
export const fxArchiveTicketsType = createEffect<TicketsTypePayload, object, Error>();
export const fxCreateTicketsType = createEffect<
  CreateUpdateTypePayload,
  CreateUpdateTypeResponse,
  Error
>();
export const fxUpdateTicketsType = createEffect<
  CreateUpdateTypePayload,
  CreateUpdateTypeResponse,
  Error
>();

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2]);
export const $typeData = createStore<{
  id: number;
  level: number;
  title: string;
} | null>(null);
