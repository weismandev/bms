import { sample } from 'effector';
import { delay } from 'patronum';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { TypeForm, TypePayload } from '../../interfaces';
import { formatDuration } from '../../libs';
import { $archiveModalData } from '../archive-modal/archive-modal.model';
import { detailForm } from '../detail-form/detail-form.model';
import { filtersForm } from '../filters-form/filters-form.model';
import { pageUnmounted, pageMounted, fxArchiveAllTicketsTypes } from '../page/page.model';
import {
  detailClosed,
  openViaUrl,
  fxGetTicketsType,
  $opened,
  $path,
  $entityId,
  $isDetailOpen,
  changedDetailVisibility,
  fxArchiveTicketsType,
  archive,
  fxCreateTicketsType,
  fxUpdateTicketsType,
  $typeData,
} from './detail.model';

// при клике на строку в таблице отправляется запрос getById
sample({
  clock: openViaUrl,
  fn: (id) => ({ id }),
  target: fxGetTicketsType,
});

const convertMinutesToDateTime = (duration: number) => {
  if (!duration) {
    return null;
  }

  const date = new Date();

  const hours =
    Math.floor(duration / 60).toString().length === 1
      ? `0${Math.floor(duration / 60)}`
      : Math.floor(duration / 60);

  const minutes =
    (duration % 60).toString().length === 1 ? `0${duration % 60}` : duration % 60;

  date.setHours(hours as number, minutes as number);

  return date;
};

$opened
  .on(fxGetTicketsType.done, (_, { result: { type } }) => ({
    id: type.id,
    title: type?.title || '',
    parentId: type?.parent_id || null,
    parentTitle: '',
    parentLevel: 0,
    duration: formatDuration(type?.planned_duration_minutes),
    isPaid: Boolean(type?.paid),
    prepaymentType: type?.paid?.prepayment_type || null,
    costType: type?.paid?.price_type || null,
    costFrom: type?.paid?.price_lower || '',
    costTo: type?.paid?.price_upper || '',
    isShowMobile: Boolean(type?.is_show_in_mobile),
    children: type?.children || [],
    level: type?.level || 0,
    isArchived: type?.archived || false,
    estimatedDuration: convertMinutesToDateTime(type?.estimated_duration_work),
    isAutomaticRouting: Boolean(type?.is_available_automatic_routing),
    isReadonly: Boolean(type?.is_readonly),
  }))
  .reset([
    signout,
    pageUnmounted,
    fxGetTicketsType,
    fxArchiveTicketsType.done,
    fxArchiveAllTicketsTypes.done,
    filtersForm.submitted,
  ]);

// если при маунте в url есть id, то отправляется запрос getById
sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: Number.parseInt(entityId, 10),
  }),
  target: fxGetTicketsType,
});

$isDetailOpen
  .on(fxGetTicketsType.done, () => true)
  .reset([
    signout,
    pageUnmounted,
    fxGetTicketsType.fail,
    fxArchiveTicketsType.done,
    fxArchiveAllTicketsTypes.done,
    filtersForm.submitted,
  ]);

// если fxGetTicketsType падает, то из url убирается id
sample({
  clock: delay({ source: fxGetTicketsType.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// при закрытии из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// архивация типа
sample({
  clock: archive,
  source: { opened: $opened, archiveModalData: $archiveModalData },
  filter: ({ archiveModalData }) => !archiveModalData.isAllArchivedTypes,
  fn: ({ opened: { id } }) => ({ id }),
  target: fxArchiveTicketsType,
});

// после архивации типа из url убирается id
sample({
  clock: fxArchiveTicketsType.done,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

const convertDurationToMinutes = (data: string) => {
  const duration = data.split(' ');

  const day = Number.parseInt(duration[0], 10);
  const hour = Number.parseInt(duration[2], 10);
  const minutes = Number.parseInt(duration[4], 10);

  const durationInMinutes = day * 60 * 24 + hour * 60 + minutes;

  if (Number.isNaN(durationInMinutes)) {
    return '';
  }

  return durationInMinutes;
};

const formatPayload = (data: TypeForm) => {
  const payload: TypePayload = {
    title: data.title,
    is_show_in_mobile: Number(data.isShowMobile),
    is_paid: Number(data.isPaid),
    is_available_automatic_routing: Number(data.isAutomaticRouting),
  };

  if (data?.parentId) {
    payload.parent_id = data.parentId;
  }

  const duration = convertDurationToMinutes(data?.duration);

  if (duration) {
    payload.planned_duration_minutes = duration;
  }

  if (data?.prepaymentType) {
    payload.prepayment_type = data.prepaymentType;
  }

  if (data?.costType) {
    payload.price_type = data.costType;
  }

  if (data?.costFrom) {
    payload.price_lower = data.costFrom.toString();
  }

  if (data?.costTo) {
    payload.price_upper = data.costTo.toString();
  }

  if (data?.estimatedDuration) {
    const hours = data.estimatedDuration.getHours();
    const minutes = data.estimatedDuration.getMinutes();

    payload.estimated_duration_work = hours * 60 + minutes;
  }

  return { types: [payload] };
};

// отправка запроса на создание типа
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => !id,
  fn: (data) => formatPayload(data),
  target: fxCreateTicketsType,
});

// запрос типа после создания
sample({
  clock: fxCreateTicketsType.done,
  fn: ({ result: { types } }) => ({ id: types[0]?.id }),
  target: fxGetTicketsType,
});

// после создания в url добавляется id
sample({
  clock: fxCreateTicketsType.done,
  source: $path,
  filter: (_, { result: { types } }) => Boolean(types[0]?.id),
  fn: (path, { result: { types } }) => `${path}/${types[0].id}`,
  target: historyPush,
});

// отправка запроса на обновление типа
sample({
  clock: detailForm.submitted,
  filter: ({ id }) => Boolean(id),
  fn: (data) => {
    const payload = formatPayload(data);

    payload.types[0].id = data.id as number;

    return payload;
  },
  target: fxUpdateTicketsType,
});

// запрос типа после обновления
sample({
  clock: fxUpdateTicketsType.done,
  fn: ({ result: { types } }) => ({ id: types[0]?.id }),
  target: fxGetTicketsType,
});

$typeData.reset([
  signout,
  pageUnmounted,
  detailClosed,
  fxGetTicketsType,
  fxArchiveTicketsType.done,
  fxArchiveAllTicketsTypes.done,
  filtersForm.submitted,
]);

// нужно для сохранения id и уровня типа при срабатывании addClicked
sample({
  source: $opened,
  filter: ({ id }) => id,
  fn: ({ id, level, title }) => ({
    id,
    level,
    title,
  }),
  target: $typeData,
});

// при фильтрации из url убирается id
sample({
  clock: filtersForm.submitted,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});
