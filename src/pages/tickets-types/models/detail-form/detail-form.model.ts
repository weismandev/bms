import { createForm } from '@unicorn/effector-form';
import { TypeForm } from '../../interfaces';
import { $opened } from '../detail/detail.model';
import { validationSchema } from './validation';

export const detailForm = createForm<TypeForm>({
  initialValues: $opened,
  validationSchema,
  editable: false,
});
