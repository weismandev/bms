import * as Zod from 'zod';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const maxCostValue = 1000000;
const maxHours = 23;
const maxMinutes = 59;

const theLowerLimitMustBeLessThanTheUpper = t('TheLowerLimitMustBeLessThanTheUpper');
const theUpperLimitOfTheValueMustBeHigherThanTheLower = t(
  'TheUpperLimitOfTheValueMustBeHigherThanTheLower'
);
const maximumValueExceeded = t('MaximumValueExceeded');
const thisIsRequiredField = t('thisIsRequiredField');
const maximumValueForHours = `${t('MaximumValueForHours')} ${maxHours}!`;
const maximumValueForMinutes = `${t('MaximumValueForMinutes')} ${maxMinutes}!`;
const wrongFormat = t('WrongFormat');

export const validationSchema = Zod.object({
  title: Zod.string().nonempty(),
  duration: Zod.string(),
  isPaid: Zod.boolean(),
  prepaymentType: Zod.string().nullable(),
  costType: Zod.string().nullable(),
  costFrom: Zod.any(),
  costTo: Zod.any(),
})
  .refine(
    ({ duration }) => {
      if (!duration) {
        return true;
      }

      const splittedDuration = duration.split(' ');
      const hours = Number.parseInt(splittedDuration[2], 10);

      if (hours || hours === 0) {
        return hours <= maxHours;
      }

      return true;
    },
    {
      message: maximumValueForHours,
      path: ['duration'],
    }
  )
  .refine(
    ({ duration }) => {
      if (!duration) {
        return true;
      }

      const splittedDuration = duration.split(' ');
      const minutes = Number.parseInt(splittedDuration[4], 10);

      if (minutes || minutes === 0) {
        return minutes <= maxMinutes;
      }

      return true;
    },
    {
      message: maximumValueForMinutes,
      path: ['duration'],
    }
  )
  .refine(
    ({ duration }) => {
      if (!duration) {
        return true;
      }

      const splittedDuration = duration.split(' ');
      const days = splittedDuration[0];
      const hours = splittedDuration[2];
      const minutes = splittedDuration[4];

      return !(days.includes('_') || hours.includes('_') || minutes.includes('_'));
    },
    {
      message: wrongFormat,
      path: ['duration'],
    }
  )
  .refine(({ isPaid, prepaymentType }) => !(isPaid && !prepaymentType), {
    message: thisIsRequiredField,
    path: ['prepaymentType'],
  })
  .refine(({ isPaid, costType }) => !(isPaid && !costType), {
    message: thisIsRequiredField,
    path: ['costType'],
  })
  .refine(
    ({ isPaid, costType, costFrom }) => {
      if (isPaid && costType && costType !== 'not-specified') {
        return Boolean(costFrom);
      }

      return true;
    },
    {
      message: thisIsRequiredField,
      path: ['costFrom'],
    }
  )
  .refine(
    ({ costType, costFrom }) => {
      if (costType && costType !== 'not-specified' && costFrom) {
        return BigInt(costFrom as number) < maxCostValue;
      }

      return true;
    },
    {
      message: maximumValueExceeded,
      path: ['costFrom'],
    }
  )
  .refine(
    ({ costType, costFrom, costTo }) => {
      if (costType && costType === 'range' && costFrom && costTo) {
        return BigInt(costFrom as number) < BigInt(costTo as number);
      }

      return true;
    },
    {
      message: theLowerLimitMustBeLessThanTheUpper,
      path: ['costFrom'],
    }
  )
  .refine(
    ({ isPaid, costType, costTo }) => {
      if (isPaid && costType && costType === 'range') {
        return Boolean(costTo);
      }

      return true;
    },
    {
      message: thisIsRequiredField,
      path: ['costTo'],
    }
  )
  .refine(
    ({ costType, costFrom, costTo }) => {
      if (costType && costType === 'range' && costFrom && costTo) {
        return BigInt(costFrom as number) < BigInt(costTo as number);
      }

      return true;
    },
    {
      message: theUpperLimitOfTheValueMustBeHigherThanTheLower,
      path: ['costTo'],
    }
  )
  .refine(
    ({ costType, costTo }) => {
      if (costType && costType !== 'not-specified' && costTo) {
        return BigInt(costTo as number) < maxCostValue;
      }

      return true;
    },
    {
      message: maximumValueExceeded,
      path: ['costTo'],
    }
  );
