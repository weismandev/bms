import { sample } from 'effector';
import { signout } from '@features/common';
import {
  addClicked,
  detailClosed,
  $opened,
  $typeData,
  newEntity,
  openViaUrl,
  fxCreateTicketsType,
  fxUpdateTicketsType,
} from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import { detailForm } from './detail-form.model';

/* если открыта карточка с типом и уровень не равен 2, то при срабатывании
 addClicked подставляются parentId, parentTitle, parentLevel. */
sample({
  clock: addClicked,
  source: $typeData,
  filter: (typeData) => Boolean(typeData?.id) && typeData?.level !== 2,
  fn: (typeData) => {
    const entity = { ...newEntity };

    entity.parentId = typeData?.id || null;
    entity.parentTitle = typeData?.title || '';
    entity.parentLevel = typeData?.level || 0;

    return entity;
  },
  target: $opened,
});

// при изменении isPaid, сбрасываются зависимые поля
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'isPaid',
  fn: () => ({
    prepaymentType: null,
    costType: null,
    costFrom: '',
    costTo: '',
  }),
  target: detailForm.updateValues,
});

// при измнении costType, сбрасываются зависимые поля
sample({
  clock: detailForm.setValue,
  filter: ({ path }) => path === 'costType',
  fn: () => ({
    costFrom: '',
    costTo: '',
  }),
  target: detailForm.updateValues,
});

detailForm.editable
  .on(addClicked, () => true)
  .reset([
    signout,
    pageUnmounted,
    detailClosed,
    openViaUrl,
    fxCreateTicketsType.done,
    fxUpdateTicketsType.done,
  ]);
