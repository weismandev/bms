import { sample } from 'effector';
import { filtersEntity, filtersSubmitted } from '../filters/filters.model';
import { filtersForm } from './filters-form.model';

// после самбита формы, принять текущее состояние формы за исходное
sample({
  clock: filtersForm.submitted,
  target: [filtersForm.initCurrent, filtersForm.initValues, filtersSubmitted],
});

// при сбросе данные запрашиваются без фильтров
sample({
  clock: filtersForm.reset,
  fn: () => ({ ...filtersEntity }),
  target: filtersForm.submitted,
});
