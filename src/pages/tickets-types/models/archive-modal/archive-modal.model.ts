import { createStore, createEvent } from 'effector';
import { ArchiveModal } from '../../interfaces';

export const changeArchiveDataModal = createEvent<ArchiveModal>();

export const $archiveModalData = createStore<ArchiveModal>({
  isOpen: false,
  isAllArchivedTypes: false,
});
