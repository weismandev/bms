import { signout } from '@features/common';
import { fxArchiveTicketsType } from '../detail/detail.model';
import { pageUnmounted, fxArchiveAllTicketsTypes } from '../page/page.model';
import { $archiveModalData, changeArchiveDataModal } from './archive-modal.model';

$archiveModalData
  .on(changeArchiveDataModal, (_, data) => data)
  .reset([signout, pageUnmounted, fxArchiveTicketsType, fxArchiveAllTicketsTypes]);
