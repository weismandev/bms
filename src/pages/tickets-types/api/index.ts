import { api } from '@api/api2';
import {
  GetTicketsTypesPayload,
  TicketsTypePayload,
  CreateUpdateTypePayload,
} from '../interfaces';

const getTicketsTypes = (payload: GetTicketsTypesPayload) =>
  api.v4('get', 'ticket/types/list', payload);
const getTicketsType = (payload: TicketsTypePayload) =>
  api.v4('get', 'ticket/types/view', payload);
const archiveTicketsType = (payload: TicketsTypePayload) =>
  api.v4('post', 'ticket/types/archive', payload);
const archiveAllTicketsTypes = () => api.v4('post', 'ticket/types/archive-all');
const createTicketsType = (payload: CreateUpdateTypePayload) =>
  api.v4('post', 'ticket/types/create-batch', payload);
const updateTicketsType = (payload: CreateUpdateTypePayload) =>
  api.v4('post', 'ticket/types/update-batch', payload);

export const ticketsTypesApi = {
  getTicketsTypes,
  getTicketsType,
  archiveTicketsType,
  archiveAllTicketsTypes,
  createTicketsType,
  updateTicketsType,
};
