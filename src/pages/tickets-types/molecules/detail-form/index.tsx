import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Form, Control } from '@effector-form';
import { CustomScrollbar } from '@ui/index';
import { detailForm, $isFilterOpen } from '../../models';
import { StyledForm, StyledFormWrapper, StyledControls } from './styled';

export const DetailForm: FC = () => {
  const { t } = useTranslation();
  const { values } = useUnit(detailForm);
  const isFilterOpen = useUnit($isFilterOpen);

  const isShowCostFrom = values.costType && values.costType !== 'not-specified';
  const costFromName = values.costType === 'fixed' ? t('PriceCost') : t('PriceFrom');
  const isShowCostTo = values.costType && values.costType === 'range';
  const isShowParentText = !values.id && values?.parentLevel !== 2 && values?.parentTitle;

  return (
    <StyledFormWrapper>
      <CustomScrollbar>
        <StyledForm>
            {isShowParentText && (
              <StyledControls isFilterOpen={isFilterOpen}>
                <Control.Text form={detailForm} name="parentTitle" label={t('ParentType')} readOnly />
                <Control.Text  form={detailForm} name="parentId" label={t('ParentTypeID')} readOnly />
              </StyledControls>
            )}
            <Control.Text form={detailForm}  name="title" label={t('Name')} required />
            <StyledControls isFilterOpen={isFilterOpen}>
              <Control.TicketTypeDuration form={detailForm}  name="duration" label={t('RequestTypeTerm')} />
              <Control.Time  form={detailForm} name="estimatedDuration" label={t('EstimatedDurationWork')} />
            </StyledControls>
            <Control.Checkbox
              form={detailForm}
              name="isAutomaticRouting"
              label={t('DetectPerformerAutomatically')}
            />
            <Control.Checkbox  form={detailForm}  name="isPaid" label={t('Paid')} />
            {values.isPaid && (
              <>
                <StyledControls isFilterOpen={isFilterOpen}>
                  <Control.TicketPrepaymentTypes
                    form={detailForm}
                    name="prepaymentType"
                    label={t('PrepaymentType')}
                    required
                  />
                  <Control.TicketCostTypes
                    form={detailForm}
                    name="costType"
                    label={t('CostType')}
                    required
                  />
                </StyledControls>
                {(isShowCostFrom || isShowCostTo) && (
                  <StyledControls isFilterOpen={isFilterOpen}>
                    {isShowCostFrom && (
                      <Control.Number  form={detailForm}  name="costFrom" label={costFromName} required />
                    )}
                    {isShowCostTo && (
                      <Control.Number   form={detailForm} name="costTo" label={t('PriceTo')} required />
                    )}
                  </StyledControls>
                )}
              </>
            )}
            <Control.Checkbox  form={detailForm}  name="isShowMobile" label={t('DisplayInResidentApp')} />
        </StyledForm>
      </CustomScrollbar>
    </StyledFormWrapper>
  );
};
