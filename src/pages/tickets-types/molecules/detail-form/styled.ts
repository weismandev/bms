import { styled } from '@mui/material/styles';

export const StyledForm = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 20,
  paddingRight: 24,
}));

export const StyledFormWrapper = styled('div')(() => ({
  height: 'calc(100% - 88px)',
  padding: '0px 0px 24px 24px',
}));

export const StyledControls = styled('div')(
  ({ isFilterOpen }: { isFilterOpen: boolean }) => ({
    display: 'grid',
    gap: 10,
    gridTemplateColumns: 'repeat(2, 1fr)',
    [`@media (max-width: ${isFilterOpen ? 1590 : 1250}px)`]: {
      gridTemplateColumns: 'repeat(1, 1fr)',
      gap: '20px 10px',
    },
  })
);
