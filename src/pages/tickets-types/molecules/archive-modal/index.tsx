import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@mui/material';
import { ArchiveOutlined, Close } from '@mui/icons-material';
import { $archiveModalData, changeArchiveDataModal, archive } from '../../models';

export const ArchiveModal: FC = () => {
  const { t } = useTranslation();
  const archiveModalData = useUnit($archiveModalData);

  const onClose = () =>
    changeArchiveDataModal({ isOpen: false, isAllArchivedTypes: false });
  const onArchive = () => archive();

  if (!archiveModalData.isOpen) {
    return null;
  }

  const header = archiveModalData.isAllArchivedTypes
    ? t('ArchivingOfAllTypes')
    : t('ArchivingType');

  const content = archiveModalData.isAllArchivedTypes
    ? t('ConfirmArchivingOfAllTypes')
    : t('ConfirmArchivingType');

  return (
    <Dialog open={archiveModalData.isOpen} onClose={onClose}>
      <DialogTitle>{header}</DialogTitle>
      <DialogContent>
        <DialogContentText>{content}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          startIcon={<ArchiveOutlined />}
          variant="contained"
          color="warning"
          onClick={onArchive}
        >
          {t('Archive')}
        </Button>
        <Button
          startIcon={<Close />}
          variant="outlined"
          color="primary"
          onClick={onClose}
        >
          {t('Cancellation')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
