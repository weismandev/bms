import { FC } from 'react';
import { useUnit } from 'effector-react';
import { IconButton } from '@mui/material';
import { Edit, ArchiveOutlined, Save, Close } from '@mui/icons-material';
import { Greedy } from '@ui/index';
import {
  changedDetailVisibility,
  $opened,
  changeArchiveDataModal,
  detailForm,
} from '../../models';
import { StyledToolbar } from './styled';

export const DetailToolbar: FC = () => {
  const opened = useUnit($opened);
  const { editable, setEditable, reset, submit, submittable } = useUnit(detailForm);

  const onChangeMode = () => setEditable(true);
  const handleArchiveModal = () =>
    changeArchiveDataModal({ isOpen: true, isAllArchivedTypes: false });
  const onClose = () => changedDetailVisibility(false);
  const onCancel = () => {
    reset();

    if (!opened.id) {
      changedDetailVisibility(false);
    }
  };

  return (
    <StyledToolbar disableGutters>
      {!(opened.isArchived || opened.isReadonly) && (
        <>
          <IconButton disabled={editable} onClick={onChangeMode} color="primary">
            <Edit />
          </IconButton>
          <IconButton disabled={editable} color="warning" onClick={handleArchiveModal}>
            <ArchiveOutlined />
          </IconButton>
        </>
      )}
      <Greedy />
      {editable ? (
        <>
          <IconButton color="success" onClick={submit} disabled={!submittable}>
            <Save />
          </IconButton>
          <IconButton color="error" onClick={onCancel}>
            <Close />
          </IconButton>
        </>
      ) : (
        <IconButton onClick={onClose}>
          <Close />
        </IconButton>
      )}
    </StyledToolbar>
  );
};
