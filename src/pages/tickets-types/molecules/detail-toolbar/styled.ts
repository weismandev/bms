import { Toolbar } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledToolbar = styled(Toolbar)(() => ({
  gap: 5,
  padding: 24,
}));
