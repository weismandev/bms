import { styled } from '@mui/material/styles';

export const StyledContainer = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  gap: 10,
}));
