import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Button } from '@mui/material';
import { Check, Clear } from '@mui/icons-material';
import { filtersForm, filtersEntity } from '../../models';
import { StyledContainer } from './styled';

export const FilterFooter: FC = () => {
  const { t } = useTranslation();
  const { submit, reset, submittable, initialValues } = useUnit(filtersForm);

  const isDefaultFilterValues =
    JSON.stringify({ ...filtersEntity }) === JSON.stringify(initialValues);

  return (
    <StyledContainer>
      <Button
        variant="contained"
        startIcon={<Clear />}
        color="error"
        onClick={reset}
        disabled={isDefaultFilterValues}
      >
        {t('Reset')}
      </Button>
      <Button
        variant="contained"
        startIcon={<Check />}
        color="success"
        onClick={submit}
        disabled={!submittable}
      >
        {t('Apply')}
      </Button>
    </StyledContainer>
  );
};
