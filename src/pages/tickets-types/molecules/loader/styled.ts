import { Backdrop } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledBackdrop = styled(Backdrop)(() => ({
  zIndex: 10,
}));
