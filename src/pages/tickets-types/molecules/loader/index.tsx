import { FC } from 'react';
import { useUnit } from 'effector-react';
import { CircularProgress } from '@mui/material';
import { $isLoading } from '../../models';
import { StyledBackdrop } from './styled';

export const Loader: FC = () => {
  const isLoading = useUnit($isLoading);

  if (!isLoading) {
    return null;
  }

  return (
    <StyledBackdrop open={isLoading}>
      <CircularProgress color="inherit" />
    </StyledBackdrop>
  );
};
