import { Typography, Button } from '@mui/material';
import { Greedy } from '@ui/index';
import { styled } from '@mui/material/styles';

export const StyledToolbarWrapper = styled('div')(() => ({
  display: 'flex',
  width: '100%',
  gap: 12,
  padding: '24px 0px',
}));

export const StyledTypography = styled(Typography)(({ theme }) => ({
  fontSize: 18,
  fontWeight: 400,
  whiteSpace: 'nowrap',
  marginTop: 10,
  color: theme.palette.grey[500],
}));

export const StyledGreedy = styled(Greedy)(() => ({
  flexGrow: 13,
}));
