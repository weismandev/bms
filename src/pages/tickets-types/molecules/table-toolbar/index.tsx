import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Form, Control } from '@effector-form';
import { IconButton, Button } from '@mui/material';
import { FilterAltOutlined, Add, ArchiveOutlined } from '@mui/icons-material';

import { getAddLabelName } from '../../libs';
import {
  $count,
  $isFilterOpen,
  changedFilterVisibility,
  searchForm,
  addClicked,
  detailForm,
  $opened,
  changeArchiveDataModal,
  $filters,
} from '../../models';
import { StyledToolbarWrapper, StyledGreedy } from './styled';

export const TableToolbar: FC = () => {
  const { t } = useTranslation();
  const { editable } = useUnit(detailForm);
  const [totalCount, isFilterOpen, opened, filters] = useUnit([
    $count,
    $isFilterOpen,
    $opened,
    $filters,
  ]);

  const isDisabledAddButton =
    (editable && !opened.id) || opened.level === 2 || filters.isArchive;
  const addLabelName = getAddLabelName(opened.id as number, opened.level as number);

  const onClickFilter = () => changedFilterVisibility(true);
  const onClickAdd = () => addClicked();
  const onArchive = () =>
    changeArchiveDataModal({ isOpen: true, isAllArchivedTypes: true });

  return (
    <StyledToolbarWrapper>
      <IconButton onClick={onClickFilter} disabled={isFilterOpen}>
        <FilterAltOutlined titleAccess={t('Filters') ?? ''} />
      </IconButton>
      <div>
        <Control.Search form={searchForm} name="search" />
      </div>

      <StyledGreedy />

      <Button
        color="primary"
        variant="outlined"
        onClick={onArchive}
        startIcon={<ArchiveOutlined />}
        disabled={filters.isArchive}
      >
        {t('Archive')}
      </Button>
    
      <Button
        startIcon={<Add />}
        onClick={onClickAdd}
        disabled={isDisabledAddButton}
        color="primary"
        variant="contained"
      >
        {addLabelName}
      </Button>
    </StyledToolbarWrapper>
  );
};
