import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Form, Control } from '@effector-form';
import { filtersForm } from '../../models';

export const FiltersForm: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Control.Checkbox form={filtersForm} name="isArchive" label={t('SignOfArchiving')} />
      <Control.TicketTypesLevel form={filtersForm} name="level" label={t('LevelDisplay')} />
    </>
  );
};
