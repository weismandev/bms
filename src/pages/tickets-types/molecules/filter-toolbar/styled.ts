import { Typography, IconButton } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledContainer = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
}));

export const StyledTypography = styled(Typography)(({ theme }) => ({
  fontWeight: 700,
  color: theme.palette.text.primary,
}));

export const StyledCloseButton = styled(IconButton)(() => ({
  marginTop: -3,
}));
