import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Close } from '@mui/icons-material';
import { changedFilterVisibility } from '../../models';
import { StyledContainer, StyledTypography, StyledCloseButton } from './styled';

export const FilterToolbar: FC = () => {
  const { t } = useTranslation();

  const onClose = () => changedFilterVisibility(false);

  return (
    <StyledContainer>
      <StyledTypography variant="h5">{t('Filter')}</StyledTypography>
      <StyledCloseButton onClick={onClose}>
        <Close />
      </StyledCloseButton>
    </StyledContainer>
  );
};
