import { TypeItem } from '../type';

export interface GetTicketsTypesPayload {
  search: string;
  filters: {
    level: {
      id: number | string;
    };
    archived?: number;
  };
}

export interface GetTicketsTypesResponse {
  types: TypeItem[];
}
