export interface TypeItem {
  archived: boolean;
  children: number[];
  id: number;
  is_readonly: boolean;
  is_show_in_mobile: boolean;
  level: number;
  paid: null | {
    prepayment_type: string;
    price_type: string;
    price_lower: null | number;
    price_upper: null | number;
  };
  parent_id: null | number;
  planned_duration_minutes: null | number;
  title: string;
  estimated_duration_work: number;
  is_available_automatic_routing: boolean;
}

export interface TicketsTypePayload {
  id: number;
}

export interface GetTicketsTypeResponse {
  type: TypeItem;
}

export interface TypePayload {
  id?: number;
  title: string;
  parent_id?: number;
  is_show_in_mobile: number;
  planned_duration_minutes?: number;
  is_paid: number;
  prepayment_type?: string;
  price_type?: string;
  price_lower?: string;
  price_upper?: string;
  estimated_duration_work?: number;
  is_available_automatic_routing: number;
}

export interface CreateUpdateTypePayload {
  types: TypePayload[];
}

export interface CreateUpdateTypeResponse {
  types: TypeItem[];
}
