export interface FiltersForm {
  isArchive: boolean;
  level: number | string;
}
