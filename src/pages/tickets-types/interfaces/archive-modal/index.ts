export interface ArchiveModal {
  isOpen: boolean;
  isAllArchivedTypes: boolean;
}
