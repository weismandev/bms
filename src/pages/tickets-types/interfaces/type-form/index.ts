export interface TypeForm {
  id: null | number;
  title: string;
  parentId: null | number;
  parentTitle: string;
  parentLevel: number;
  duration: string;
  isPaid: boolean;
  prepaymentType: null | string;
  costType: null | string;
  costFrom: string | number;
  costTo: string | number;
  isShowMobile: boolean;
  children: number[];
  level: number;
  isArchived: boolean;
  estimatedDuration: null | Date;
  isAutomaticRouting: boolean;
  isReadonly: boolean;
}
