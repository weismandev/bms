import i18n from '@shared/config/i18n';

const { t } = i18n;

export const getAddLabelName = (id: number, level: number) => {
  if (!id) {
    return t('Level1type');
  }

  switch (level) {
    case 1:
      return t('Level3type');
    case 2:
      return t('Level3type');
    default:
      return t('Level2type');
  }
};
