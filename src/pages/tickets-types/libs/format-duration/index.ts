import i18n from '@shared/config/i18n';

const { t } = i18n;

export const formatDuration = (duration: number | null) => {
  const day = Math.floor(Number(duration) / 60 / 24);
  const hour = Math.floor(Number(duration) / 60) - day * 24;
  const minutes = Number(duration) - day * 24 * 60 - hour * 60;

  const formattedDay = day.toString().length === 1 ? `0${day}` : day;
  const formattedHour = hour.toString().length === 1 ? `0${hour}` : hour;
  const formattedMinutes = minutes.toString().length === 1 ? `0${minutes}` : minutes;

  return `${formattedDay} ${t('ShortDay')} ${formattedHour} ${t(
    'ShortHour'
  )} ${formattedMinutes} ${t('Min')}`;
};
