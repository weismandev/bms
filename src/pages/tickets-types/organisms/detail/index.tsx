import { FC, memo } from 'react';
import { DetailToolbar, DetailForm } from '../../molecules';
import { StyledWrapper } from './styled';

export const Detail: FC = memo(() => (
  <StyledWrapper>
    <DetailToolbar />
    <DetailForm />
  </StyledWrapper>
));
