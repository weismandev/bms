import { memo, FC } from 'react';
import { useUnit } from 'effector-react';
import type { GridRowParams } from '@mui/x-data-grid-pro';
import { StayPrimaryPortrait } from '@mui/icons-material';
import { DataGridTable } from '@shared/ui/data-grid-table';
import Money from '@img/money.svg';
import {
  $count,
  $sorting,
  $tableData,
  $pinnedColumns,
  $isLoadingTable,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  openViaUrl,
  sortChanged,
  openRowChanged,
  changePinnedColumns
} from '../../models';
import { TableToolbar } from '../../molecules';
import { StyledWrapper } from './styled';

export const Table: FC = memo(() => {
  const [
    count,
    sorting,
    tableData,
    pinnedColumns,
    isLoading,
    selectRow,
    visibilityColumns,
    columns
  ] = useUnit([
    $count,
    $sorting,
    $tableData,
    $pinnedColumns,
    $isLoadingTable,
    $selectRow,
    $visibilityColumns,
    $columns,
  ]);

  const paidTypeIndex = columns.findIndex((column) => column.field === 'paidType');
  const showInMobileIndex = columns.findIndex(
    (column) => column.field === 'showInMobile'
  );

  columns[paidTypeIndex].renderCell = ({ value }: { value: boolean }) => {
    if (value) {
      return <Money />;
    }

    return <div />;
  };

  columns[showInMobileIndex].renderCell = ({ value }: { value: boolean }) => {
    if (value) {
      return <StayPrimaryPortrait />;
    }

    return <div />;
  };

  const getTreeDataPath = ({ hierarchy }: { hierarchy: number }) => hierarchy;

  const groupingColDef = {
    headerName: 'ID',
    width: 140,
  };

  const handleClickRow = (row: GridRowParams) => {
    openViaUrl(row.id);
    openRowChanged(row.id);
  };

  return (
    <StyledWrapper elevation={0}>
      <TableToolbar />
      <DataGridTable
        rows={tableData}
        columns={columns}
        rowCount={count}
        loading={isLoading}
        treeData
        density="compact"
        getTreeDataPath={getTreeDataPath}
        hideFooter
        groupingColDef={groupingColDef}
        onRowClick={handleClickRow}
        rowSelectionModel={selectRow}
        onRowSelectionModelChange={selectionRowChanged}
        sortModel={sorting}
        onSortModelChange={sortChanged}
        pinnedColumns={pinnedColumns}
        onPinnedColumnsChange={changePinnedColumns}
        columnVisibilityModel={visibilityColumns}
        onColumnVisibilityModelChange={visibilityColumnsChanged}
        onColumnWidthChange={columnsWidthChanged}
        onColumnOrderChange={orderColumnsChanged}
      />
    </StyledWrapper>
  );
});
