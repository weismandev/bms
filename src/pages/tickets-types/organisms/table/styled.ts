import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledWrapper = styled(Paper)(() => ({
  height: '100%',
  padding: '0px 24px',
  display: 'flex',
  flexDirection: 'column',
}));
