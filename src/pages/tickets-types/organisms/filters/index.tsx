import { FC, memo } from 'react';
import { FilterToolbar, FilterFooter, FiltersForm } from '../../molecules';
import { StyledWrapper } from './styled';

export const Filters: FC = memo(() => (
  <StyledWrapper>
    <FilterToolbar />
    <FiltersForm />
    <FilterFooter />
  </StyledWrapper>
));
