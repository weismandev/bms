import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledWrapper = styled(Paper)(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 20,
  height: '100%',
  padding: 24,
}));
