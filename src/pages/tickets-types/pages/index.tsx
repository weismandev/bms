import { FC } from 'react';
import { useUnit } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { ThemeAdapter } from '@shared/theme/adapter';
import { ErrorMessage, FilterMainDetailLayout } from '@ui/index';
import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isFilterOpen,
  $isDetailOpen,
} from '../models';
import { ArchiveModal, Loader } from '../molecules';
import { Table, Filters, Detail } from '../organisms';

const TicketsTypesPage: FC = () => {
  const [error, isErrorDialogOpen, isFilterOpen, isDetailOpen] = useUnit([
    $error,
    $isErrorDialogOpen,
    $isFilterOpen,
    $isDetailOpen,
  ]);

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);

  return (
    <>
      <PageGate />

      <ArchiveModal />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <Loader />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedTicketsTypesPage: FC = () => (
  <ThemeAdapter>
    <HaveSectionAccess>
      <TicketsTypesPage />
    </HaveSectionAccess>
  </ThemeAdapter>
);

export { RestrictedTicketsTypesPage as TicketsTypesPage };
