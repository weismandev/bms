import { useEffect } from 'react';
import { useLocation } from 'react-router';
import { useUnit } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { ThemeAdapter } from '@shared/theme/adapter';
import { ErrorMessage, FilterMainDetailLayout } from '../../../ui';
import {
  PageGate,
  $error,
  clearError,
  $isExportError,
  clearExportError,
  searchForm,
} from '../models';
import { $isFilterOpen } from '../models/filter/model';
import { PopupExport, Table } from '../organisms';
import { Filter } from '../organisms';

type LocationState = {
  search?: string;
};

const DebtorsNotifications = () => {
  const [error, isFilterOpen, isExportError] = useUnit([
    $error,
    $isFilterOpen,
    $isExportError,
  ]);

  const { setValue } = useUnit(searchForm);
  const data = useLocation();

  useEffect(() => {
    const search = (data?.state as LocationState)?.search;
    setValue({ path: 'search', value: search ?? '' });
  }, []);

  return (
    <ThemeAdapter>
      <PageGate />

      <ErrorMessage isOpen={error} onClose={clearError} error={error} />

      <ErrorMessage
        isOpen={isExportError}
        onClose={clearExportError}
        error={isExportError}
      />

      <PopupExport />

      <FilterMainDetailLayout
        params={{ filterWidth: '400px' }}
        filter={isFilterOpen && <Filter />}
        main={<Table />}
      />
    </ThemeAdapter>
  );
};

const RestrictedDebtorsNotifications = (props: any) => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <DebtorsNotifications {...props} />
    // </HaveSectionAccess>
  );
};

export { RestrictedDebtorsNotifications };
