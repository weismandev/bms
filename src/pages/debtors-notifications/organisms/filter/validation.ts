import i18n from '@shared/config/i18n';
import * as Yup from 'yup';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  sent_at_from: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .nullable(),
  sent_at_to: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    )
    .nullable(),
});

function startDateAfterFinishDate(this: {
  createError: any;
  parent: {
    sent_at_from: string;
    sent_at_to: string;
  };
}) {
  const isDatesDefined =
    Boolean(this.parent.sent_at_from) && Boolean(this.parent.sent_at_to);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.sent_at_from)) > Number(new Date(this.parent.sent_at_to))
  ) {
    return this.createError();
  }

  return true;
}
