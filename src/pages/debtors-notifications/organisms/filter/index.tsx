import { memo } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectField,
  InputField,
} from '@ui/index';
import { ChannelsTypes, SentModeTypes } from '../../interfaces';
import { CHANNELS } from '../../models';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
  SENT_MODE,
} from '../../models';
import {
  StyledDateItem,
  StyledDates,
  StyledDatesLabel,
  StyledDatesOwnLabel,
  StyledForm,
  StyledWrapper,
} from './styled';
import { validationSchema } from './validation';

interface SelectOptions {
  id: string;
  title: string;
}

const { t } = i18n;

const ChannelsSelectField = (props: Record<string, any>) => {
  const options: SelectOptions[] = [
    {
      id: ChannelsTypes.all,
      title: CHANNELS[ChannelsTypes.all],
    },
    {
      id: ChannelsTypes.management_company,
      title: CHANNELS[ChannelsTypes.management_company],
    },
    { id: ChannelsTypes.sms, title: CHANNELS[ChannelsTypes.sms] },
    { id: ChannelsTypes.email, title: CHANNELS[ChannelsTypes.email] },
  ];

  return (
    <div>
      <SelectField options={options} {...props} />
    </div>
  );
};

const SentModeSelectField = (props: Record<string, any>) => {
  const options: SelectOptions[] = [
    {
      id: SentModeTypes.all,
      title: SENT_MODE[SentModeTypes.all],
    },
    { id: SentModeTypes.automatic, title: SENT_MODE[SentModeTypes.automatic] },
    { id: SentModeTypes.manual, title: SENT_MODE[SentModeTypes.manual] },
  ];

  return (
    <div>
      <SelectField options={options} {...props} />
    </div>
  );
};

const Filter = memo(() => {
  const filters = useUnit($filters);

  return (
    <StyledWrapper>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          validationSchema={validationSchema}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <StyledForm placeholder="">
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('RC')}
                placeholder={t('selectRC')}
              />

              <Field
                name="houses"
                complex={
                  Array.isArray(values?.objects) &&
                  values.objects.length > 0 &&
                  values.objects
                }
                component={HouseSelectField}
                onChange={(value: any) => {
                  setFieldValue('houses', value);
                }}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />

              <Field
                name="channels"
                component={ChannelsSelectField}
                label={t('DebtorsNotifications.NewsletterService')}
                placeholder={t('DebtorsNotifications.ChooseTypeOfMailing')}
                onChange={(value: SelectOptions) => {
                  if (!value || value.id === SentModeTypes.all) {
                    setFieldValue('channels', []);
                    return;
                  }

                  setFieldValue('channels', [value]);
                }}
              />

              <Field
                name="sent_mode"
                component={SentModeSelectField}
                label={t('DebtorsNotifications.DistributionMethod')}
                placeholder={t('DebtorsNotifications.ChooseDistributionMethod')}
                onChange={(value: SelectOptions) => {
                  if (!value) {
                    setFieldValue('sent_mode', SentModeTypes.all);
                    return;
                  }

                  setFieldValue('sent_mode', value.id);
                }}
              />

              <StyledDatesLabel>
                {t('DebtorsNotifications.DepartureDate') as string}
              </StyledDatesLabel>
              <StyledDates>
                <StyledDateItem>
                  <StyledDatesOwnLabel>
                    {t('DebtorsNotifications.from') as string}
                  </StyledDatesOwnLabel>
                  <Field
                    name="sent_at_from"
                    label={null}
                    divider={false}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                </StyledDateItem>

                <StyledDateItem>
                  <StyledDatesOwnLabel>
                    {t('DebtorsNotifications.to') as string}
                  </StyledDatesOwnLabel>
                  <Field
                    name="sent_at_to"
                    label={null}
                    divider={false}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                </StyledDateItem>
              </StyledDates>

              <FilterFooter isResettable />
            </StyledForm>
          )}
        />
      </CustomScrollbar>
    </StyledWrapper>
  );
});

export { Filter };
