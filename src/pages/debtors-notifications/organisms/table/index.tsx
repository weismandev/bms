import { FC, memo } from 'react';
import { useUnit } from 'effector-react';
import { Chip } from '@mui/material';
import {
  GridColumnVisibilityModel,
  GridRenderCellParams,
  GridTreeNodeWithRender,
} from '@mui/x-data-grid-pro';
import { DataGridTable } from '@shared/ui/data-grid-table';
import {
  $columns,
  $pagination,
  $pinnedColumns,
  $tableData,
  $visibilityColumns,
  changePinnedColumns,
  columnsWidthChanged,
  orderColumnsChanged,
  sortChanged,
  visibilityColumnsChanged,
  $isLoading,
  openRowChanged,
  $selectRow,
  selectionRowChanged,
  $count,
  paginationModelChanged,
} from '../../models';
import { BasicPopover } from '../../molecules/basicPopover';
import { TableToolbar } from '../table-toolbar';
import { StylePopover, StyledWrapper } from './styled';

export const Table: FC = memo(() => {
  const [
    tableData,
    pagination,
    visibilityColumns,
    columns,
    pinnedColumns,
    isLoading,
    selectRow,
    count,
  ] = useUnit([
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $pinnedColumns,
    $isLoading,
    $selectRow,
    $count,
  ]);

  const residentsFullNameIndex = columns.findIndex(
    (column) => column.field === 'residents_full_name'
  );

  columns[residentsFullNameIndex].renderCell = (
    params: GridRenderCellParams<any, any, any, GridTreeNodeWithRender>
  ) => {
    return (
      <StylePopover>
        <Chip label={params.value[0]} size="small" />

        <BasicPopover values={params.value.slice(1)} id={params.id} />
      </StylePopover>
    );
  };

  const onColumnVisibilityModelChange = (visibilityColumn: GridColumnVisibilityModel) => {
    visibilityColumnsChanged({
      ...visibilityColumns,
      ...visibilityColumn,
    });
  };

  return (
    <StyledWrapper elevation={0}>
      <TableToolbar />

      <DataGridTable
        checkboxSelection
        rows={tableData}
        density="compact"
        columns={columns}
        rowCount={count}
        loading={isLoading}
        paginationModel={pagination}
        disableRowSelectionOnClick
        rowSelectionModel={selectRow}
        onRowSelectionModelChange={(ids) => selectionRowChanged(ids as number[])}
        onRowClick={(row) => openRowChanged(row.id)}
        pinnedColumns={pinnedColumns}
        onSortModelChange={sortChanged}
        onPaginationModelChange={paginationModelChanged}
        onColumnWidthChange={columnsWidthChanged}
        columnVisibilityModel={visibilityColumns}
        onPinnedColumnsChange={changePinnedColumns}
        onColumnVisibilityModelChange={onColumnVisibilityModelChange}
        onColumnOrderChange={orderColumnsChanged}
      />
    </StyledWrapper>
  );
});
