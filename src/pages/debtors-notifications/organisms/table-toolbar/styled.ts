import styled from '@emotion/styled';

export const StyledTableToolbar = styled.div`
  padding: 24px 0;
  display: flex;
  gap: 12px;
  align-items: center;
  justify-content: space-between;
`;

export const StyledTableToolbarLeft = styled.div`
  display: flex;
  gap: 12px;
  align-items: center;
`;

export const StyledTableToolbarRight = styled.div`
  display: flex;
  justify-content: flex-end;
  gap: 12px;
  align-items: center;
`;
