import { useEffect } from 'react';
import { useLocation } from 'react-router';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { Button, IconButton } from '@mui/material';
import FilterAltIconOutlined from '@mui/icons-material/FilterAltOutlined';
import ExportIcon from '@mui/icons-material/VerticalAlignTop';
import i18n from '@shared/config/i18n';
import {
  $isFilterOpen,
  changedFilterVisibility,
  showExportPopup,
  searchForm,
} from '../../models';
import {
  StyledTableToolbar,
  StyledTableToolbarLeft,
  StyledTableToolbarRight,
} from './styled';

const { t } = i18n;

export const TableToolbarLeft = () => {
  const [isFilterOpen] = useUnit([$isFilterOpen]);

  return (
    <StyledTableToolbarLeft>
      <IconButton onClick={() => changedFilterVisibility(true)} disabled={isFilterOpen}>
        <FilterAltIconOutlined />
      </IconButton>

      <Control.Search form={searchForm} name="search" />
    </StyledTableToolbarLeft>
  );
};

export const TableToolbarRight = () => {
  return (
    <StyledTableToolbarRight>
      <Button
        variant="outlined"
        onClick={() => showExportPopup()}
        startIcon={<ExportIcon />}
      >
        {t('DataGrid.toolbarExport')}
      </Button>
    </StyledTableToolbarRight>
  );
};

const TableToolbar = () => {
  return (
    <StyledTableToolbar>
      <TableToolbarLeft />
      <TableToolbarRight />
    </StyledTableToolbar>
  );
};

export { TableToolbar };
