export enum ChannelsTypes {
  all = 'all',
  management_company = 'management_company',
  email = 'email',
  sms = 'sms',
}

export enum SentModeTypes {
  all = 'all',
  automatic = 'automatic',
  manual = 'manual',
}

export interface Filters {
  channels?: ChannelsTypes[];
  sent_mode?: SentModeTypes[];
  sent_at_from?: string;
  sent_at_to?: string;
  complexes: (number | string)[];
  buildings: (number | string)[];
}

export interface Sort {
  sort?: string;
  column?: string;
}

export interface Pag {
  page?: number | string;
  per_page?: number | string;
}

export interface Search {
  search?: string;
}

export type DebitorsNotifications = Filters & Sort & Pag & Search;

export type DebitorsNotificationsExport = DebitorsNotifications & { email: string };
