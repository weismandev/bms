export interface NotificationTableData {
  id: string | number;
  apartment_title: string;
  channels: string;
  sent_at: string;
  template: string;
  is_automatic: string;
  residents_full_name: string[];
}
