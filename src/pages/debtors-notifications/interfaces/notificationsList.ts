import { ChannelsTypes } from './payload';

export interface Meta {
  page: number;
  per_page: number;
  total: number;
  pages: number;
}

export interface Apartment {
  id: number;
  title: string;
}

export interface Building {
  id: number;
  title: string;
}

export interface Account {
  number: number;
}

export interface ResourceCompany {
  id: number;
  title: string;
}

export interface Resident {
  id: number | string;
  full_name: string;
}

export interface Notification {
  id: number;
  template: string;
  apartment: Apartment;
  building: Building;
  account: Account;
  resource_company: ResourceCompany;
  channels: ChannelsTypes[];
  sent_at: string;
  is_automatic: boolean;
  residents: Resident[];
}

export interface NotificationData {
  meta: Meta;
  notifications: Notification[];
}

export type NotificationTable = Partial<Notification>;
