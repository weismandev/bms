import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  complexes: [],
  houses: [],
  channels: [],
  sent_mode: '',
  sent_at_from: '',
  sent_at_to: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
