import { forward, attach, merge, sample } from 'effector';
import { signout } from '@features/common';
import { debitorsNotificationsApi } from '../../api';
import { $filters } from '../filter/model';
import { $tableParams, selectionRowChanged } from '../table/model';
import { collectPayload } from './collectPayload';
import {
  pageMounted,
  $debtorsNotificationsData,
  $isLoading,
  $error,
  fxGetDebtorsNotificationslist,
  CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
} from './model';

let requestController: AbortController;

/** Прооверка наличия запроса, запись в $isLoading */
const isRequest = merge([fxGetDebtorsNotificationslist.pending]);
$isLoading.on(isRequest, (_state, isLoading) => isLoading).reset(signout);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([fxGetDebtorsNotificationslist.failData]);

/** Записываю ошибку, только если запрос не бы отменен */
sample({
  clock: isError,
  filter: (error) => error.message !== CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
  fn: (error) => error.message ?? 'Request error',
  target: $error,
});

$error
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(signout);

/** Запуск запроса уведомлений */
fxGetDebtorsNotificationslist.use(debitorsNotificationsApi.getNotificationsList);

/** Запись результатов запроса уведомлений */
$debtorsNotificationsData.on(
  fxGetDebtorsNotificationslist.doneData,
  (_, result) => result
);

/** При загрузке, изменении фильтров, табов, пагинации, сортировки
 * вызываем запрос уведомлений должников */
sample({
  clock: [pageMounted, $tableParams, $filters],
  source: [$tableParams, $filters],
  fn: ([tableParams, filters]) => {
    if (requestController) {
      requestController.abort();
    }

    requestController = new AbortController();

    return {
      payload: collectPayload([tableParams, filters]),
      signal: requestController.signal,
    };
  },
  target: fxGetDebtorsNotificationslist,
});

/** Очистка выделенных записей при новой загрузке должников */
sample({
  clock: $debtorsNotificationsData,
  fn: () => [],
  target: selectionRowChanged,
});
