import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';
import { createPageBag } from '../../../../tools/factories';
import { debitorsNotificationsApi } from '../../api';
import {
  ChannelsTypes,
  DebitorsNotifications,
  NotificationData,
  SentModeTypes,
} from '../../interfaces';

const { t } = i18n;

export const LOCAL_STORAGE_KEY = 'debtors-notifications';
export const CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS = 'canceled';

export const DEFAULT_PAGE_SIZE = 10;

export const CHANNELS = {
  [ChannelsTypes.all]: t('all'),
  [ChannelsTypes.email]: 'Email',
  [ChannelsTypes.sms]: 'SMS',
  [ChannelsTypes.management_company]: 'APP',
};

export const SENT_MODE = {
  [SentModeTypes.all]: t('all'),
  [SentModeTypes.automatic]: t('DebtorsNotifications.Automatically'),
  [SentModeTypes.manual]: t('DebtorsNotifications.Manually'),
};

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const clearError = createEvent();

const $debtorsNotificationsData = createStore<Partial<NotificationData>>({});
const $isLoading = createStore(false);
const $error = createStore(null).on(clearError, () => null);

const fxGetDebtorsNotificationslist = createEffect<
  { payload: DebitorsNotifications; signal: AbortController['signal'] },
  NotificationData,
  Error
>();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  $debtorsNotificationsData,
  $isLoading,
  $error,
  fxGetDebtorsNotificationslist,
  clearError,
};

export const {
  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(debitorsNotificationsApi);
