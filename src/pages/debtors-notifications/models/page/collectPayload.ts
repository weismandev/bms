import { DebitorsNotifications } from '../../interfaces';

export function collectPayload([tableParams, filters]: [any, any]) {
  const payload = {} as DebitorsNotifications;

  if (tableParams.search) {
    payload.search = tableParams.search;
  }

  if (tableParams.sorting.length > 0) {
    payload.sort = tableParams.sorting[0].sort;
    payload.column = tableParams.sorting[0].field;
  }

  if (tableParams.per_page) {
    payload.per_page = tableParams.per_page;
  }

  if (tableParams.page) {
    payload.page = tableParams.page;
  }

  if (typeof filters === 'object') {
    if (filters.complexes && filters.complexes.id) {
      payload.complexes = [filters.complexes.id];
    }

    if (Array.isArray(filters.houses) && filters.houses.length > 0) {
      payload.buildings = filters.houses.map((house: any) => house.id);
    }

    if (Array.isArray(filters.channels) && filters.channels.length > 0) {
      payload.channels = filters.channels.map((channel: any) => channel.id);
    }

    if (!!filters.sent_mode) {
      payload.sent_mode = filters.sent_mode;
    }

    if (!!filters.sent_at_from) {
      payload.sent_at_from = `${filters.sent_at_from} 00:00:00`;
    }

    if (!!filters.sent_at_to) {
      payload.sent_at_to = `${filters.sent_at_to} 23:59:59`;
    }

    if (payload.sent_at_from && !payload.sent_at_to) {
      payload.sent_at_to = '3000-01-01 00:00:00';
    }

    if (!payload.sent_at_from && payload.sent_at_to) {
      payload.sent_at_from = '1950-01-01 00:00:00';
    }
  }

  return payload;
}
