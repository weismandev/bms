import { combine } from 'effector';
import { connectStorage } from '@shared/tools/storage';
import { LOCAL_STORAGE_KEY, PageGate } from '../page/model';
import {
  $pagination,
  $pinnedColumns,
  changePinnedColumns,
  $visibilityColumns,
  visibilityColumnsChanged,
  $columns,
  mergeColumnsByPattern,
  paginationModelChanged,
} from './model';

const $pageSize = combine($pagination, ({ pageSize }) => pageSize);
const prependPageSizeChanged = (pageSize: number) => ({
  page: 0,
  pageSize,
});

const storage = connectStorage({
  storageKey: LOCAL_STORAGE_KEY,
  storageGate: PageGate,
  storageScheme: {
    columns: [$columns, mergeColumnsByPattern],
    pageSize: [$pageSize, paginationModelChanged.prepend(prependPageSizeChanged)],
    pinnedColumns: [$pinnedColumns, changePinnedColumns],
    visibilityColumns: [$visibilityColumns, visibilityColumnsChanged],
  },
});

export default storage;
