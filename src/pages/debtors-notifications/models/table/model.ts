import { createEvent, createStore } from 'effector';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { createForm } from '@unicorn/effector-form';
import { NotificationTableData } from '../../interfaces';
import { $debtorsNotificationsData } from '../page/model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'account_number',
    headerName: t('Label.personalAccount') as string,
    width: 250,
    sortable: true,
  },
  {
    field: 'apartment_title',
    headerName: t('AddressOfTheObject') as string,
    width: 250,
    sortable: false,
  },
  {
    field: 'residents_full_name',
    headerName: t('Label.FullName') as string,
    width: 250,
    sortable: false,
  },
  {
    field: 'channels',
    headerName: t('DebtorsNotifications.NewsletterService') as string,
    width: 160,
    sortable: false,
  },
  {
    field: 'sent_at',
    headerName: t('DebtorsNotifications.Sent') as string,
    width: 160,
    sortable: true,
  },
  {
    field: 'template',
    headerName: t('DebtorsNotifications.NotificationText') as string,
    width: 500,
    sortable: false,
  },
  {
    field: 'is_automatic',
    headerName: t('DebtorsNotifications.DistributionMethod') as string,
    width: 250,
    sortable: false,
  },
];

const changeTabledata = createEvent<NotificationTableData[]>();
const $tableData = createStore<NotificationTableData[]>([]);

export const {
  $tableParams,
  $pagination,
  $search,
  searchForm,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  openRowChanged,
  changePinnedColumns,
  $pinnedColumns,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({ columns });

export const $columnsWidthAndOrder = $columns.map((columns) =>
  columns.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);

export const $count = $debtorsNotificationsData.map(({ meta }) => meta?.total ?? 0);

export { $tableData, columns, changeTabledata };
