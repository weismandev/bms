import { sample } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { ChannelsTypes, Notification, NotificationTableData } from '../../interfaces';
import { $debtorsNotificationsData, CHANNELS, PageGate } from '../page/model';
import { $search, $tableData, changeTabledata } from './model';

const { t } = i18n;

$tableData.on(changeTabledata, (_, data) => data);

const getFormatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(localDate, 'dd.MM.yyyy HH:mm');
};

const getChannels = (channels: ChannelsTypes[]): string => {
  let result: string[] = [];
  channels.forEach((channel) => result.push(CHANNELS[channel]));

  return result.join(', ');
};

const formatNotificationsToTable = (
  notifications: Notification[]
): NotificationTableData[] =>
  notifications.map(
    ({
      id,
      apartment,
      channels,
      sent_at,
      template,
      is_automatic,
      account,
      residents,
    }) => {
      return {
        id,
        account_number: account.number,
        apartment_title: apartment.title,
        channels: getChannels(channels),
        sent_at: getFormatDate(sent_at),
        residents_full_name: residents.map((resident) => resident.full_name),
        template,
        is_automatic: is_automatic
          ? t('DebtorsNotifications.Automatically')
          : t('DebtorsNotifications.Manually'),
      };
    }
  );

/** Формирую данные таблицы при загрузке */
sample({
  clock: $debtorsNotificationsData,
  fn: ({ notifications }) => {
    if (!notifications) return [];

    return formatNotificationsToTable(notifications);
  },
  target: changeTabledata,
});
