import { DebitorsNotificationsExport } from '@features/debtors-notifications/interfaces';
import { createEffect, createEvent, createStore } from 'effector';

export const $exportPopup = createStore<boolean>(false);
export const showExportPopup = createEvent();
export const hideExportPopup = createEvent();

export const $isSending = createStore<boolean>(false);
export const clearExportError = createEvent();
export const $isExportError = createStore<Error | null>(null).on(
  clearExportError,
  () => null
);

export const $sendResult = createStore<boolean | null>(null);
export const sendlistExport = createEvent<{ email: string }>();
export const fxSendlistExport = createEffect<DebitorsNotificationsExport, void, Error>();
