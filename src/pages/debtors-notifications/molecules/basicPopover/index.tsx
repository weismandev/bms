import { useState } from 'react';
import { Button, Chip, Popover } from '@mui/material';
import { GridRenderCellParams, GridTreeNodeWithRender } from '@mui/x-data-grid-pro';
import * as Styled from './styled';

export const BasicPopover = ({
  values,
  id,
}: {
  values: string[];
  id: GridRenderCellParams<any, any, any, GridTreeNodeWithRender>['id'];
}) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  if (!Array.isArray(values)) return null;

  if (!values.length) return null;

  return (
    <Styled.Popover>
      <Button
        sx={{ minWidth: 36 }}
        aria-describedby={String(id)}
        variant="text"
        onClick={handleClick}
      >
        +{values.length}
      </Button>

      <Popover
        id={String(id)}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        onClose={handleClose}
      >
        <Styled.PopoverData>
          {values.map((data: string, index: number) => (
            <Chip key={index} label={data} />
          ))}
        </Styled.PopoverData>
      </Popover>
    </Styled.Popover>
  );
};
