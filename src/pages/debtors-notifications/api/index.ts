import { api } from '../../../api/api2';
import { DebitorsNotifications, DebitorsNotificationsExport } from '../interfaces';

const getNotificationsList = ({
  payload,
  signal,
}: {
  payload: DebitorsNotifications;
  signal: AbortController['signal'];
}) => api.v1('get', 'debtors/notifications/list', payload, signal);

const sendlistExport = (payload: DebitorsNotificationsExport) =>
  api.v1('get', 'debtors/notifications/list/export', payload);

export const debitorsNotificationsApi = {
  getNotificationsList,
  sendlistExport,
};
