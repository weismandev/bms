import styled from '@emotion/styled';
import { Button } from '@mui/material';

export const StyledActions = styled.div(() => ({
  marginTop: 20,
}));

export const StyledDownloadButton = styled(Button)`
  border-radius: 20px;
`;

export const StyledCancelButton = styled(Button)`
  border-radius: 20px;
  margin-left: 15px;
`;
