import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';

import { Modal } from '@ui/index';
import { HouseSelect } from '@features/house-select';

import { TextWrapper, HeaderModal } from '../../atoms';
import {
  $isOpenExportModal,
  changeVisibilityExportModal,
  $exportBuildingTemplate,
  changeExportBuildingTemplate,
  exportTemplate,
} from '../../models';
import { StyledCancelButton, StyledDownloadButton, StyledActions } from './styled';
import { Value } from '../../interfaces';

export const ExportModal: FC = () => {
  const { t } = useTranslation();
  const [isOpenExportModal, exportBuildingTemplate] = useUnit([
    $isOpenExportModal,
    $exportBuildingTemplate,
  ]);

  const onClose = () => changeVisibilityExportModal(false);
  const onChangeBuilding = (value: Value) => changeExportBuildingTemplate(value);
  const onExport = () => exportTemplate();

  if (!isOpenExportModal) {
    return null;
  }

  const header = <HeaderModal title={t('DownloadImportTemplate')} onClose={onClose} />;

  const content = (
    <>
      <TextWrapper text={t('HomeExportTemplate')} />
      <HouseSelect
        value={exportBuildingTemplate}
        onChange={onChangeBuilding}
        divider={false}
        label={t('Label.building')}
        placeholder={t('selectHouse')}
        notAbsolute
      />
    </>
  );

  const actions = (
    <StyledActions>
      <StyledDownloadButton
        variant="contained"
        disabled={!exportBuildingTemplate}
        onClick={onExport}
      >
        {t('DownloadTemplate')}
      </StyledDownloadButton>
      <StyledCancelButton onClick={onClose}>{t('cancel')}</StyledCancelButton>
    </StyledActions>
  );

  return (
    <Modal
      isOpen={isOpenExportModal}
      onClose={onClose}
      header={header}
      content={content}
      actions={actions}
    />
  );
};
