import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  tableRoot: {
    cursor: 'pointer',
  },
  modal: {
    width: ({ isFileUploaded }: { isFileUploaded: boolean }) =>
      isFileUploaded ? '100%' : 'auto',
    minWidth: ({ isFileUploaded }: { isFileUploaded: boolean }) =>
      isFileUploaded ? '80%' : 'auto',
  },
});

export const styles = {
  error: {
    color: 'rgba(225, 63, 61, 1)',
  },
  warning: {
    color: 'rgba(251, 137, 1, 1)',
  },
};
