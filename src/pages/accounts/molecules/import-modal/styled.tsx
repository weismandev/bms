import styled from '@emotion/styled';
import { Button } from '@mui/material';

import { DataGrid } from '@features/data-grid';

export const StyledHouseSelect = styled.div(() => ({
  marginTop: 20,
}));

export const StyledTableContainer = styled.div(() => ({
  width: '100%',
  height: 'calc(50vh - 150px)',
}));

export const StyledDataGrid = styled(DataGrid.Mini)(() => ({
  '& .super-app-theme--New': {
    backgroundColor: 'rgba(229, 245, 236, 1)',
    '&:hover': {
      backgroundColor: 'rgba(229, 245, 236, 1)',
      '&.Mui-selected': {
        backgroundColor: 'rgba(229, 245, 236, 1)',
        '&:hover': {
          backgroundColor: 'rgba(229, 245, 236, 1)',
        },
      },
    },
  },
  '& .super-app-theme--Error': {
    backgroundColor: 'rgba(254, 235, 239, 1)',
    '&:hover': {
      backgroundColor: 'rgba(254, 235, 239, 1)',
      '&.Mui-selected': {
        backgroundColor: 'rgba(254, 235, 239, 1)',
        '&:hover': {
          backgroundColor: 'rgba(254, 235, 239, 1)',
        },
      },
    },
  },
  '& .super-app-theme--Warning': {
    backgroundColor: 'rgba(255, 243, 224, 1)',
    '&:hover': {
      backgroundColor: 'rgba(255, 243, 224, 1)',
      '&.Mui-selected': {
        backgroundColor: 'rgba(255, 243, 224, 1)',
        '&:hover': {
          backgroundColor: 'rgba(255, 243, 224, 1)',
        },
      },
    },
  },
}));

export const StyledActions = styled.div(() => ({
  display: 'flex',
  marginTop: 20,
}));

export const StyledImportButton = styled(Button)`
  border-radius: 20px;
`;

export const StyledCancelButton = styled(Button)`
  border-radius: 20px;
  margin-left: 15px;
`;
