import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import ReactFileReader from 'react-file-reader';
import { Tooltip } from '@mui/material';
import { ErrorOutline, Warning } from '@mui/icons-material';

import { Modal, Divider } from '@ui/index';
import { HouseSelect } from '@features/house-select';

import { HeaderModal, TextWrapper } from '../../atoms';
import {
  $isOpenImportModal,
  changeVisibilityImportModal,
  $importBuilding,
  changeImportBuilding,
  uploadFile,
  $uploadFileResult,
  $uploadResultTableData,
  importColumns,
  importData,
} from '../../models';
import {
  StyledActions,
  StyledCancelButton,
  StyledImportButton,
  StyledHouseSelect,
  StyledTableContainer,
  StyledDataGrid,
} from './styled';
import { useStyles, styles } from './styles';
import { Value } from '../../interfaces';

export const ImportModal: FC = () => {
  const { t } = useTranslation();
  const [isOpenImportModal, importBuilding, uploadFileResult, uploadResultTableData] =
    useUnit([
      $isOpenImportModal,
      $importBuilding,
      $uploadFileResult,
      $uploadResultTableData,
    ]);
  const classes = useStyles({ isFileUploaded: Boolean(uploadFileResult?.id) });

  const statusIndex = importColumns.findIndex((column) => column.field === 'status');

  const onClose = () => changeVisibilityImportModal(false);
  const onChangeBuilding = (value: Value) => changeImportBuilding(value);
  const confirmImport = () => importData();

  importColumns[statusIndex].renderCell = ({
    value,
  }: {
    value: { type?: string; message?: string };
  }) => {
    if (value?.type === 'new') {
      return (
        <Tooltip title={value?.message || ''}>
          <ErrorOutline color="success" />
        </Tooltip>
      );
    }

    if (value?.type === 'error') {
      return (
        <Tooltip title={value?.message || ''}>
          <Warning style={styles.error} />
        </Tooltip>
      );
    }

    if (value?.type === 'warning') {
      return (
        <Tooltip title={value?.message || ''}>
          <Warning style={styles.warning} />
        </Tooltip>
      );
    }

    return '';
  };

  const handleFiles = (files: File[]) => {
    if (files[0]) {
      uploadFile(files[0]);
    }
  };

  if (!isOpenImportModal) {
    return null;
  }

  const header = uploadFileResult?.id ? (
    <>
      <HeaderModal title={t('ViewUploadedFile')} onClose={onClose} />
      <StyledHouseSelect>
        <HouseSelect
          value={importBuilding}
          onChange={onChangeBuilding}
          divider={false}
          label={t('Label.building')}
          placeholder={t('selectHouse')}
          notAbsolute
          mode="view"
        />
        <Divider />
      </StyledHouseSelect>
    </>
  ) : (
    <HeaderModal title={t('ImportPersonalAccounts')} onClose={onClose} />
  );

  const getRowClassName = ({ row }: { row: { status: { type?: string } } }) => {
    if (row.status?.type === 'new') {
      return 'super-app-theme--New';
    }

    if (row.status?.type === 'error') {
      return 'super-app-theme--Error';
    }

    if (row.status?.type === 'warning') {
      return 'super-app-theme--Warning';
    }

    return null;
  };

  const content = uploadFileResult?.id ? (
    <StyledTableContainer>
      <StyledDataGrid
        classes={{ root: classes.tableRoot }}
        rows={uploadResultTableData}
        columns={importColumns}
        disableColumnMenu
        border={false}
        hideFooter
        density="compact"
        disableVirtualization={false}
        getRowClassName={getRowClassName}
        disableSelectionOnClick
      />
    </StyledTableContainer>
  ) : (
    <>
      <TextWrapper text={t('HomeImportTemplate')} />
      <HouseSelect
        value={importBuilding}
        onChange={onChangeBuilding}
        divider={false}
        label={t('Label.building')}
        placeholder={t('selectHouse')}
        notAbsolute
      />
    </>
  );

  const actions = (
    <StyledActions>
      {uploadFileResult?.id ? (
        <StyledImportButton variant="contained" onClick={confirmImport}>
          {t('ImportData')}
        </StyledImportButton>
      ) : (
        <ReactFileReader
          fileTypes="*"
          disabled={!importBuilding}
          handleFiles={handleFiles}
        >
          <StyledImportButton variant="contained" disabled={!importBuilding}>
            {t('SelectFile')}
          </StyledImportButton>
        </ReactFileReader>
      )}
      <StyledCancelButton onClick={onClose}>{t('cancel')}</StyledCancelButton>
    </StyledActions>
  );

  return (
    <Modal
      isOpen={isOpenImportModal}
      onClose={onClose}
      header={header}
      content={content}
      actions={actions}
      classes={{ paper: classes.modal }}
    />
  );
};
