import { MouseEvent } from 'react';
import styled from '@emotion/styled';
import { Button } from '@mui/material';

interface ButtonProps {
  className?: string;
  children: JSX.Element;
  onClick: (event: MouseEvent) => void;
}

export const StyledContent = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'start',
  alignContent: 'center',
}));

export const StyledFirstElement = styled.div(() => ({
  display: 'grid',
  alignItems: 'center',
}));

export const StyledButton = styled(({ className, children, onClick }: ButtonProps) => (
  <Button className={className} variant="text" size="small" onClick={onClick}>
    {children}
  </Button>
))`
  min-width: auto;
  margin: 0px 10px;
`;

export const StyledList = styled.div(() => ({
  display: 'grid',
  gap: 10,
  padding: 10,
  background: '#fcfcfc',
}));
