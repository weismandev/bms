import { FC, useState, MouseEvent } from 'react';
import { Popover } from '@mui/material';

import { StyledContent, StyledFirstElement, StyledButton, StyledList } from './styled';

interface Props {
  children: JSX.Element[];
}

export const HiddenArray: FC<Props> = ({ children }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const open = Boolean(anchorEl);

  const handlePopoverOpen = (event: MouseEvent) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  if (!children.length) return null;

  if (children.length === 1) return <div>{children}</div>;

  return (
    <StyledContent>
      <StyledFirstElement>{children[0]}</StyledFirstElement>
      <StyledButton onClick={handlePopoverOpen}>
        <span>+{children.slice(1).length}</span>
      </StyledButton>
      <Popover
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <StyledList>{children.slice(1).map((child) => child)}</StyledList>
      </Popover>
    </StyledContent>
  );
};
