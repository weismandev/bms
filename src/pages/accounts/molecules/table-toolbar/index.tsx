import { useState, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { IconButton, Tooltip, Button } from '@mui/material';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import {
  FilterAltOutlined,
  Add,
  VerticalAlignBottom,
  VerticalAlignTop,
  DescriptionOutlined,
} from '@mui/icons-material';
import {
  $isFilterOpen,
  $isDetailOpen,
  changedFilterVisibility,
  addClicked,
  $mode,
  $opened,
  changeVisibilityExportModal,
  changeVisibilityImportModal,
  searchForm,
} from '../../models';
import * as Styled from './styled';

export const TableToolbar: FC = () => {
  const { t } = useTranslation();
  const [
    isFilterOpen,
    isDetailOpen,
    mode,
    opened
  ] = useUnit([
    $isFilterOpen,
    $isDetailOpen,
    $mode,
    $opened
  ]);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const isDisabledAddClicked = mode === 'edit' && !opened.id;

  const onClickFilter = () => changedFilterVisibility(true);
  const onChangeVisibilityExportModal = () => changeVisibilityExportModal(true);
  const onChangeVisibilityImportModal = () => changeVisibilityImportModal(true);
  const onClickAdd = () => addClicked();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const renderResponsive = () => {
    if (isDetailOpen) {
      return (
        <>
          <Button
            id="demo-customized-button"
            aria-controls={open ? 'demo-customized-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            variant="outlined"
            disableElevation
            onClick={handleClick}
            endIcon={<KeyboardArrowDownIcon />}
            >
              {t('Label.actions')}
          </Button>
          <Menu
            id="demo-customized-menu"
            MenuListProps={{
              'aria-labelledby': 'demo-customized-button',
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={onChangeVisibilityExportModal} disableRipple>
              <DescriptionOutlined />
              {t('DownloadTemplate')}
            </MenuItem>
            <MenuItem onClick={onChangeVisibilityImportModal} disableRipple>
              <VerticalAlignTop />
              {t('Import')}
            </MenuItem>
            <MenuItem onClick={onClickAdd} disabled={isDisabledAddClicked} disableRipple>
              <Add />
              {t('DebtorsList.Debtor')}
            </MenuItem>
  
          </Menu>
        </>
      )
    }
    return (
      <>
        <Button
          variant="outlined"
          color="primary"
          onClick={onChangeVisibilityExportModal}
          startIcon={<DescriptionOutlined />}
        >
          {t('DownloadTemplate')}
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={onChangeVisibilityImportModal}
          startIcon={<VerticalAlignBottom />}
        >
          {t('Import')}
        </Button>
        <Button
          color="primary"
          variant="contained"
          onClick={onClickAdd}
          startIcon={<Add />}
          disabled={isDisabledAddClicked}
        >
          {t('AddPersonalAccount')}
        </Button>
      </>
    )
  }
  return (
    <Styled.ToolbarWrapper>
      <Tooltip title={t('Filters')}>
        <IconButton disabled={isFilterOpen} onClick={onClickFilter}>
          <FilterAltOutlined />
        </IconButton>
      </Tooltip>
      <div>
        <Control.Search name="search" form={searchForm} />
      </div>
      <Styled.Greedy />
      {renderResponsive()}
    </Styled.ToolbarWrapper>
  );
};
