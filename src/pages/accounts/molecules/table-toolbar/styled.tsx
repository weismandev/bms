import { styled } from '@mui/material/styles';

export const ToolbarWrapper = styled('div')(() => ({
  display: 'flex',
  width: '100%',
  gap: 12,
  padding: '24px 0px'
}));

export const Greedy = styled('div')(() => ({
  flexGrow: 13,
}));
