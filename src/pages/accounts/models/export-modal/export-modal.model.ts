import { createEvent, createStore } from 'effector';

import { Value } from '../../interfaces';

export const changeVisibilityExportModal = createEvent<boolean>();
export const changeExportBuildingTemplate = createEvent<Value>();

export const $isOpenExportModal = createStore<boolean>(false);
export const $exportBuildingTemplate = createStore<Value | string>('');
