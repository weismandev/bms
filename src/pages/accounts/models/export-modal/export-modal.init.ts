import { signout } from '@features/common';

import {
  $isOpenExportModal,
  changeVisibilityExportModal,
  $exportBuildingTemplate,
  changeExportBuildingTemplate,
} from './export-modal.model';
import { pageUnmounted, exportTemplate } from '../page/page.model';

$isOpenExportModal
  .on(changeVisibilityExportModal, (_, visibility) => visibility)
  .reset([pageUnmounted, signout, exportTemplate]);

$exportBuildingTemplate
  .on(changeExportBuildingTemplate, (_, building) => building)
  .reset([pageUnmounted, signout, changeVisibilityExportModal]);
