import { createEffect, createEvent } from 'effector';

import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';

import {
  CreateAccountPayload,
  ApartmentAccountPayload,
  GetApartmentAccountResponse,
  UpdateAccountPayload,
} from '../../interfaces';

const accountEntity = {
  id: null,
  account: '',
  type: '',
  complex: '',
  building: '',
  apartment: '',
  users: [],
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(accountEntity);

export const deleteAccount = createEvent<void>();

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map(
  (pathname: string) => pathname.split('/')[2] || null
);

export const fxCreateAccount = createEffect<
  CreateAccountPayload,
  GetApartmentAccountResponse,
  Error
>();
export const fxGetApartmentAccount = createEffect<
  ApartmentAccountPayload,
  GetApartmentAccountResponse,
  Error
>();
export const fxUpdateAccount = createEffect<
  UpdateAccountPayload,
  GetApartmentAccountResponse,
  Error
>();
export const fxDeleteAccount = createEffect<ApartmentAccountPayload, object, Error>();
