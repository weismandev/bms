import { sample } from 'effector';
import { delay } from 'patronum';

import i18n from '@shared/config/i18n';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';

import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  fxCreateAccount,
  entityApi,
  $mode,
  fxGetApartmentAccount,
  openViaUrl,
  $opened,
  fxUpdateAccount,
  $isDetailOpen,
  $path,
  $entityId,
  changedDetailVisibility,
  deleteAccount,
  fxDeleteAccount,
} from './detail.model';
import { Opened } from '../../interfaces';

const { t } = i18n;

// отправка запроса на создание лицевого счета
sample({
  clock: entityApi.create,
  fn: (data: Opened) => ({
    account: data.account,
    account_type: data.type.id,
    apartment_id: data.apartment.id,
  }),
  target: fxCreateAccount,
});

$mode.reset([pageUnmounted, signout, fxCreateAccount.done, fxUpdateAccount.done]);

// запрос лицеового счета при клике в таблице
sample({
  clock: openViaUrl,
  fn: (id) => ({ id }),
  target: fxGetApartmentAccount,
});

$opened
  .on(fxGetApartmentAccount.done, (_, { result: { item } }) => ({
    id: item.id,
    account: item?.account || '',
    type: item?.apartment_account_type || '',
    apartment: item?.apartment || '',
    users: Array.isArray(item?.users)
      ? item.users.map((user) => ({
          id: user.id,
          title: user?.full_name || t('Unknown'),
        }))
      : [],
  }))
  .reset([pageUnmounted, signout, fxGetApartmentAccount]);

// после создания лицевого счета отправляется запрос, чтобы обновить данные
sample({
  clock: fxCreateAccount.done,
  fn: ({
    result: {
      item: { id },
    },
  }) => ({ id }),
  target: fxGetApartmentAccount,
});

// редактирование лицевого счета
sample({
  clock: entityApi.update,
  fn: (data: Opened) => ({
    id: data.id as number,
    account: data.account,
    account_type: data.type?.id,
  }),
  target: fxUpdateAccount,
});

// после редактирования счета отправляется запрос, чтобы обновить данные
sample({
  clock: fxUpdateAccount.done,
  fn: ({
    result: {
      item: { id },
    },
  }) => ({ id }),
  target: fxGetApartmentAccount,
});

$isDetailOpen
  .on(fxGetApartmentAccount.done, () => true)
  .reset([signout, pageUnmounted, fxGetApartmentAccount.fail, fxDeleteAccount.done]);

sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ entityId, pathname }) => entityId && IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: entityId,
  }),
  target: fxGetApartmentAccount,
});

// если запрос getById упал, то редирект в реестр
sample({
  clock: delay({ source: fxGetApartmentAccount.fail, timeout: 1000 }),
  source: $path,
  fn: (path: string) => `../${path}`,
  target: historyPush,
});

// при закрытии деталий из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// после создания лицевого счета добавляется в url id пропуска
sample({
  clock: fxCreateAccount.done,
  source: $path,
  filter: (
    _,
    {
      result: {
        item: { id },
      },
    }
  ) => Boolean(id),
  fn: (
    path,
    {
      result: {
        item: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

// удаление лицевого счета
sample({
  clock: deleteAccount,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxDeleteAccount,
});

// после удаления лицевого счета из url убирается id
sample({
  clock: fxDeleteAccount.done,
  source: $path,
  filter: (_, params) => Boolean(params),
  fn: (path) => `../${path}`,
  target: historyPush,
});
