import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import {
  GetApartmentAccountsPayload,
  GetApartmentAccountsResponse,
  ExportTemplatePayload,
  UploadFilePayload,
  UploadFileResponse,
  Import,
  ImportPayload,
} from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const exportTemplate = createEvent<void>();
export const uploadFile = createEvent<File>();
export const importData = createEvent<void>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetApartmentAccountsResponse>({});
export const $isLoadingTable = createStore<boolean>(false);
export const $uploadFileResult = createStore<Import>({});

export const fxGetApartmentAccounts = createEffect<
  GetApartmentAccountsPayload,
  GetApartmentAccountsResponse,
  Error
>();
export const fxExportTemplate = createEffect<ExportTemplatePayload, ArrayBuffer, Error>();
export const fxUploadFile = createEffect<UploadFilePayload, UploadFileResponse, Error>();
export const fxImportData = createEffect<ImportPayload, object, Error>();
