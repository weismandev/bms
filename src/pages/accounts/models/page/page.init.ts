import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { accountsApi } from '../../api';
import { Table, Filters, GetApartmentAccountsPayload, Value } from '../../interfaces';
import {
  fxCreateAccount,
  fxGetApartmentAccount,
  fxUpdateAccount,
  fxDeleteAccount,
} from '../detail/detail.model';
import { $exportBuildingTemplate } from '../export-modal/export-modal.model';
import { $filters } from '../filters/filters.model';
import {
  $importBuilding,
  changeVisibilityImportModal,
} from '../import-modal/import-modal.model';
import { $tableParams } from '../table/table.model';
import {
  $isLoading,
  $error,
  $isErrorDialogOpen,
  pageUnmounted,
  changedErrorDialogVisibility,
  fxGetApartmentAccounts,
  pageMounted,
  $rawData,
  $isLoadingTable,
  exportTemplate,
  fxExportTemplate,
  uploadFile,
  fxUploadFile,
  $uploadFileResult,
  fxImportData,
  importData,
} from './page.model';

fxGetApartmentAccounts.use(accountsApi.getApartmentAccounts);
fxCreateAccount.use(accountsApi.createAccount);
fxGetApartmentAccount.use(accountsApi.getApartmentAccount);
fxUpdateAccount.use(accountsApi.updateAccount);
fxExportTemplate.use(accountsApi.exportTemplate);
fxUploadFile.use(accountsApi.uploadFile);
fxImportData.use(accountsApi.importData);
fxDeleteAccount.use(accountsApi.deleteAccount);

const errorOccured = merge([
  fxGetApartmentAccounts.fail,
  fxCreateAccount.fail,
  fxGetApartmentAccount.fail,
  fxUpdateAccount.fail,
  fxExportTemplate.fail,
  fxUploadFile.fail,
  fxImportData.fail,
  fxDeleteAccount.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxCreateAccount,
        fxGetApartmentAccount,
        fxUpdateAccount,
        fxExportTemplate,
        fxUploadFile,
        fxImportData,
        fxDeleteAccount,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetApartmentAccounts.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

// запрос лицевых счетов
sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxCreateAccount.done,
    fxUpdateAccount.done,
    fxImportData.done,
    fxDeleteAccount.done,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
  },
  fn: ({ table, filters }: { table: Table; filters: Filters }) => {
    const payload: GetApartmentAccountsPayload = {
      page: table.page,
      per_page: table.per_page,
      account: table.search,
    };

    if (filters.buildings.length > 0) {
      payload.building_id = filters.buildings.map((building) => building.id);
    }

    if (filters.types.length > 0) {
      payload.type_id = filters.types.map((type) => type.id);
    }

    return payload;
  },
  target: fxGetApartmentAccounts,
});

$rawData
  .on(fxGetApartmentAccounts.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

// отправка запроса на экспорт шаблона
sample({
  clock: exportTemplate,
  source: $exportBuildingTemplate,
  fn: (building) => ({ building_id: (building as Value).id }),
  target: fxExportTemplate,
});

// скачивание шаблона
fxExportTemplate.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');

  FileSaver.saveAs(URL.createObjectURL(blob), `accounts-template-${dateTimeNow}.xlsx`);
});

// загрузка файла
sample({
  clock: uploadFile,
  source: $importBuilding,
  fn: (building, file) => ({
    building_id: (building as Value).id,
    import: file,
  }),
  target: fxUploadFile,
});

$uploadFileResult
  .on(fxUploadFile.done, (_, { result }) => result?.import)
  .reset([pageUnmounted, signout, changeVisibilityImportModal, fxImportData.done]);

// подтверждение импорта
sample({
  clock: importData,
  source: $uploadFileResult,
  fn: (uploadFileResult) => ({ id: uploadFileResult.id as number }),
  target: fxImportData,
});
