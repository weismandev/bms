import { createEffect, createStore } from 'effector';

import { GetAccountTypesResponse, AccountTypes } from '../../interfaces';

export const fxGetList = createEffect<void, GetAccountTypesResponse, Error>();

export const $data = createStore<AccountTypes[]>([]);
export const $isLoading = createStore<boolean>(false);
