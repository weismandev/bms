import { signout } from '@features/common';

import {
  $isOpenImportModal,
  changeVisibilityImportModal,
  $importBuilding,
  changeImportBuilding,
  $uploadResultTableData,
} from './import-modal.model';
import { pageUnmounted, fxImportData } from '../page/page.model';

$isOpenImportModal
  .on(changeVisibilityImportModal, (_, visibility) => visibility)
  .reset([pageUnmounted, signout, fxImportData.done]);

$importBuilding
  .on(changeImportBuilding, (_, building) => building)
  .reset([pageUnmounted, signout, changeVisibilityImportModal, fxImportData.done]);

$uploadResultTableData.reset([
  pageUnmounted,
  signout,
  changeVisibilityImportModal,
  fxImportData.done,
]);
