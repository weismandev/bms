import { createEvent, createStore } from 'effector';

import i18n from '@shared/config/i18n';

import { $uploadFileResult } from '../page/page.model';
import { Value, ImportItem } from '../../interfaces';

const { t } = i18n;

export const importColumns = [
  {
    field: 'status',
    headerName: t('Label.status'),
    width: 100,
    sortable: false,
  },
  {
    field: 'account',
    headerName: t('Label.personalAccount'),
    width: 150,
    sortable: false,
  },
  {
    field: 'type',
    headerName: t('type'),
    width: 180,
    sortable: false,
  },
  {
    field: 'address',
    headerName: t('Label.address'),
    width: 350,
    sortable: false,
  },
];

const formatData = (items: ImportItem[]) =>
  items.map((item) => ({
    id: item.id,
    account: item?.account || '',
    type: item?.account_type || '',
    address: item?.apartment || '',
    status:
      item?.type && item?.message ? { type: item.type, message: item.message } : null,
  }));

export const changeVisibilityImportModal = createEvent<boolean>();
export const changeImportBuilding = createEvent<Value>();

export const $isOpenImportModal = createStore<boolean>(false);
export const $importBuilding = createStore<Value | string>('');
export const $uploadResultTableData = $uploadFileResult.map(({ items }) =>
  Array.isArray(items) && items.length > 0 ? formatData(items) : []
);
