import { GridColDef, GridRenderCellParams } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { ApartmentAccount, Value } from '../../interfaces';
import { HiddenArray } from '../../molecules';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'account',
    headerName: t('Label.personalAccount') ?? '',
    width: 180,
    sortable: false,
    hideable: false,
  },
  {
    field: 'type',
    headerName: t('type') ?? '',
    width: 180,
    sortable: false,
    hideable: true,
  },
  {
    field: 'address',
    headerName: t('Label.address') ?? '',
    width: 350,
    sortable: false,
    hideable: true,
  },
  {
    field: 'users',
    headerName: t('Label.FullName') ?? '',
    width: 290,
    sortable: false,
    hideable: true,
    renderCell: ({ value }: GridRenderCellParams<any, Value[]>) => {
      if ((value as Value[]).length === 0) {
        return <div />;
      }

      return (
        <HiddenArray>
          {(value as Value[]).map((item) => (
            <span key={item.id}>{item.title}</span>
          ))}
        </HiddenArray>
      );
    },
  },
];

export const {
  $tableParams,
  $pagination,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  mergeColumnsByPattern,
  paginationModelChanged,
  searchForm,
  $selectRow
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

const formatData = (items: ApartmentAccount[]) =>
  items.map((item) => {
    const users = Array.isArray(item?.users)
      ? item.users.map((user) => ({
          id: user.id,
          title: user?.full_name || t('Unknown'),
        }))
      : [];

    return {
      id: item.id,
      account: item?.account || t('Unknown'),
      type: item?.apartment_account_type?.title || t('Unknown'),
      address: item?.apartment?.title || t('Unknown'),
      users,
    };
  });

export const $count = $rawData.map(({ meta }) => meta?.total || 0);

export const $tableData = $rawData.map(({ items }) =>
  items && items.length > 0 ? formatData(items) : []
);
