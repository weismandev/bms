import { sample } from 'effector';
import { signout } from '@features/common';
import {
  fxCreateAccount,
  detailClosed,
  addClicked,
  $entityId,
} from '../detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

/* при маунте берется id из url и подставляется в
$selectRow (чтобы была выделена строка с открытой сущностью) */
sample({
  clock: pageMounted,
  source: { id: $entityId, openRow: $openRow },
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => Number.parseInt(id as string, 10),
  target: $openRow,
});

$openRow
  .on(
    fxCreateAccount.done,
    (
      _,
      {
        result: {
          item: { id },
        },
      }
    ) => [id]
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked]);
