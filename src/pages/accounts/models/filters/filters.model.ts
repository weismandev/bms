import { createFilterBag } from '@tools/factories';

const defaultFilters = {
  complexes: [],
  buildings: [],
  types: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
