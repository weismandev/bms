import { api } from '@api/api2';

import {
  GetApartmentAccountsPayload,
  CreateAccountPayload,
  ApartmentAccountPayload,
  UpdateAccountPayload,
  ExportTemplatePayload,
  UploadFilePayload,
  ImportPayload,
} from '../interfaces';

const getApartmentAccounts = (payload: GetApartmentAccountsPayload) =>
  api.v1('get', 'apartment-accounts/crm/crud/list', payload);
const getAccountTypes = () => api.v1('get', 'apartment-accounts/crm/crud/account-types');
const createAccount = (payload: CreateAccountPayload) =>
  api.v1('post', 'apartment-accounts/crm/crud/create', payload);
const getApartmentAccount = (payload: ApartmentAccountPayload) =>
  api.v1('get', 'apartment-accounts/crm/crud/get', payload);
const updateAccount = (payload: UpdateAccountPayload) =>
  api.v1('post', 'apartment-accounts/crm/crud/update', payload);
const exportTemplate = (payload: ExportTemplatePayload) =>
  api.v1('request', 'apartment-accounts/crm/crud/template', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });
const uploadFile = (payload: UploadFilePayload) =>
  api.v1('post', 'apartment-accounts/crm/crud/import', payload);
const importData = (payload: ImportPayload) =>
  api.v1('post', 'apartment-accounts/crm/crud/enqueue-task', payload);
const deleteAccount = (payload: ApartmentAccountPayload) =>
  api.v1('post', 'apartment-accounts/crm/crud/delete', payload);

export const accountsApi = {
  getApartmentAccounts,
  getAccountTypes,
  createAccount,
  getApartmentAccount,
  updateAccount,
  exportTemplate,
  uploadFile,
  importData,
  deleteAccount,
};
