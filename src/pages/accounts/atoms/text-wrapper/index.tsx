import { FC } from 'react';
import { ErrorOutline } from '@mui/icons-material';

import { StyledWrapper, StyledTypography } from './styled';

interface Props {
  text: string;
}

export const TextWrapper: FC<Props> = ({ text }) => (
  <StyledWrapper>
    <ErrorOutline />
    <StyledTypography>{text}</StyledTypography>
  </StyledWrapper>
);
