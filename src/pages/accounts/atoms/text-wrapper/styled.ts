import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const StyledWrapper = styled.div(() => ({
  background:
    'linear-gradient(0deg, rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)), #2196F3;',
  width: '100%',
  color: '#0394E3',
  fontWeight: 500,
  fontSize: 13,
  padding: 10,
  display: 'flex',
  margin: '20px 0px',
}));

export const StyledTypography = styled(Typography)`
  font-size: 16px;
  font-weight: 400;
  color: rgba(25, 28, 41, 0.87);
  margin-left: 10px;
`;
