import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Tooltip } from '@mui/material';
import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import { ActionIconButton } from '@ui/index';

interface Props {
  onClick: () => boolean;
}

export const ExportButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  const downloadTemplate = t('DownloadTemplate');

  return (
    <Tooltip title={downloadTemplate}>
      <ActionIconButton onClick={onClick} {...props}>
        <DescriptionOutlinedIcon />
      </ActionIconButton>
    </Tooltip>
  );
};
