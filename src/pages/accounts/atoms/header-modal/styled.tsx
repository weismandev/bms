import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const StyledHeader = styled.div(() => ({
  display: 'flex',
  justifyContent: 'space-between',
}));

export const StyledTypography = styled(Typography)`
  font-weight: 500;
  font-size: 24px;
  color: rgba(37, 40, 52, 0.87);
  margin-top: -5px;
`;
