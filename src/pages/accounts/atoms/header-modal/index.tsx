import { FC } from 'react';

import { CloseButton } from '@ui/index';

import { StyledHeader, StyledTypography } from './styled';

interface Props {
  title: string;
  onClose: () => boolean;
}

export const HeaderModal: FC<Props> = ({ title, onClose }) => (
  <StyledHeader>
    <StyledTypography>{title}</StyledTypography>
    <CloseButton onClick={onClose} />
  </StyledHeader>
);
