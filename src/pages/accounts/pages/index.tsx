import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { ThemeAdapter } from '@shared/theme/adapter';

import { HaveSectionAccess } from '@features/common';
import {
  ErrorMessage,
  Loader,
  FilterMainDetailLayout,
  DeleteConfirmDialog,
} from '@ui/index';

import { ExportModal, ImportModal } from '../molecules';
import { Table, Filters, Detail } from '../organisms';
import {
  PageGate,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changedErrorDialogVisibility,
  $isFilterOpen,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedVisibilityDeleteModal,
  deleteAccount,
} from '../models';

const AccountsPage: FC = () => {
  const { t } = useTranslation();
  const [
    isLoading,
    isErrorDialogOpen,
    error,
    isFilterOpen,
    isDetailOpen,
    isOpenDeleteModal,
  ] = useUnit([
    $isLoading,
    $isErrorDialogOpen,
    $error,
    $isFilterOpen,
    $isDetailOpen,
    $isOpenDeleteModal,
  ]);

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changedVisibilityDeleteModal(false);

  return (
    <ThemeAdapter>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ImportModal />
      <ExportModal />

      <DeleteConfirmDialog
        header={t('DeletingPersonalAccount')}
        content={t('ConfirmAccountDeletion')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteAccount}
      />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </ThemeAdapter>
  );
};

const RestrictedAccountsPage: FC = () => (
  <HaveSectionAccess>
    <AccountsPage />
  </HaveSectionAccess>
);

export { RestrictedAccountsPage as AccountsPage };
