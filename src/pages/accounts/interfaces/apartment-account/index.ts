export interface ApartmentAccount {
  id: number;
  account: string;
  apartment: Value;
  apartment_account_type: Value;
  bms: Value;
  building: Value;
  complex: Value;
  users: User[];
}

export interface Value {
  id: number;
  title: string;
}

export interface User {
  id: number;
  full_name: string;
}

export interface Opened {
  id?: number;
  account: string;
  type: Value;
  complex: Value;
  building: Value;
  apartment: Value;
}

export interface CreateAccountPayload {
  apartment_id: number;
  account_type: number;
  account: string;
}

export interface ApartmentAccountPayload {
  id: number;
}

export interface GetApartmentAccountResponse {
  item: ApartmentAccount;
}

export interface UpdateAccountPayload {
  id: number;
  account: string;
  account_type: number;
}
