export interface GetAccountTypesResponse {
  types: AccountTypes[];
}

export interface AccountTypes {
  id: number;
  title: string;
}
