import { ApartmentAccount, Value } from '../apartment-account';

export interface GetApartmentAccountsPayload {
  page: number;
  per_page: number;
  account: string;
  building_id?: number[];
  type_id?: number[];
}

export interface GetApartmentAccountsResponse {
  meta?: {
    total?: number;
  };
  items?: ApartmentAccount[];
}

export interface Filters {
  buildings: Value[];
  types: Value[];
}

export interface Table {
  page: number;
  per_page: number;
  search: string;
}

export interface ExportTemplatePayload {
  building_id: number;
}

export interface UploadFilePayload {
  building_id: number;
  import: File;
}

export interface UploadFileResponse {
  import: Import;
}

export interface Import {
  id?: number;
  items?: ImportItem[];
}

export interface ImportItem {
  id: number;
  account: string;
  account_type: Value;
  apartment: Value;
  context: [];
  type: string;
  message: string;
}

export interface ImportPayload {
  id: number;
}
