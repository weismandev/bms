import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { complexRequired, buildingRequired } from '../../libs';

const { t } = i18n;
const thisIsRequiredField = t('thisIsRequiredField');

export const validationSchema = Yup.object().shape({
  account: Yup.string().required(thisIsRequiredField),
  type: Yup.object().nullable().required(thisIsRequiredField),
  complex: Yup.object()
    .nullable()
    .test('complex-required', thisIsRequiredField, complexRequired),
  building: Yup.object()
    .nullable()
    .test('building-required', thisIsRequiredField, buildingRequired),
  apartment: Yup.object().nullable().required(thisIsRequiredField),
});
