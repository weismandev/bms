import styled from '@emotion/styled';
import { Form } from 'formik';
import { Typography } from '@mui/material';

import { Wrapper, DetailToolbar } from '@ui/index';

interface StyledWrapperProps {
  className?: string;
  children: JSX.Element;
}

interface StyledFormProps {
  className?: string;
  children: JSX.Element[];
}

interface DetailToolbarProps {
  className?: string;
  mode: string;
  onEdit: () => string;
  onClose: () => boolean;
  onCancel: () => void;
  onDelete: () => boolean;
}

export const StyledWrapper = styled(({ className, children }: StyledWrapperProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
`;

export const StyledForm = styled(({ className, children }: StyledFormProps) => (
  <Form className={className}>{children}</Form>
))`
  height: calc(100% - 78px);
  position: relative;
`;

export const StyledDetailToolbar = styled(
  ({ className, ...props }: DetailToolbarProps) => (
    <DetailToolbar className={className} {...props} />
  )
)`
  padding: 24px 24px 10px;
`;

export const StyledContent = styled.div(() => ({
  height: 'calc(100% - 32px)',
  padding: '0px 0px 24px 24px',
}));

export const StyledFormContent = styled.div(() => ({
  paddingRight: 24,
}));

export const StyledField = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'repeat(2, 1fr)',
  '@media (max-width: 1150px)': {
    gridTemplateColumns: 'repeat(1, 1fr)',
  },
  gap: 15,
  marginBottom: 20,
}));

export const StyledTypography = styled(Typography)`
  font-weight: 500;
  font-size: 20px;
  color: rgba(25, 28, 41, 0.87);
  margin-bottom: 20px;
`;
