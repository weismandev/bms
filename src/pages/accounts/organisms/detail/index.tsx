import { FC, memo } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { CustomScrollbar, InputField } from '@ui/index';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { ApartmentSelectField } from '@features/apartment-select';
import { PeopleSelectField } from '@features/people-select';

import { AccountTypesSelectField } from '../account-types-select';
import {
  $opened,
  detailSubmitted,
  $mode,
  changedDetailVisibility,
  changeMode,
  changedVisibilityDeleteModal,
} from '../../models';
import {
  StyledWrapper,
  StyledForm,
  StyledDetailToolbar,
  StyledContent,
  StyledFormContent,
  StyledField,
  StyledTypography,
} from './styled';
import { Value, Opened } from '../../interfaces';
import { validationSchema } from './validation';

export const Detail: FC = memo(() => {
  const { t } = useTranslation();
  const [opened, mode] = useUnit([$opened, $mode]);

  const isNew = !(opened as Opened).id;

  return (
    <StyledWrapper>
      <Formik
        initialValues={opened}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={detailSubmitted}
        render={({ values, setFieldValue, resetForm }) => {
          const onClose = () => changedDetailVisibility(false);

          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };

          const onEdit = () => changeMode('edit');

          const onDelete = () => changedVisibilityDeleteModal(true);

          const onChangeComplex = (value: Value) => {
            setFieldValue('complex', value);
            setFieldValue('building', '');
            setFieldValue('apartment', '');
          };

          const onChangeBuilding = (value: Value) => {
            setFieldValue('building', value);
            setFieldValue('apartment', '');
          };

          const titleIfAccountExist =
            mode === 'view'
              ? `${t('Label.personalAccount')} ${(values as Opened).account}`
              : t('EditingPersonalAccount');

          const title = isNew ? t('NewPersonalAccount') : titleIfAccountExist;

          return (
            <StyledForm>
              <StyledDetailToolbar
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <StyledContent>
                <StyledTypography>{title}</StyledTypography>
                <CustomScrollbar>
                  <StyledFormContent>
                    <StyledField>
                      <Field
                        name="account"
                        label={t('Label.personalAccount')}
                        placeholder={t('EnterYourPersonalAccountNumber')}
                        component={InputField}
                        mode={mode}
                        divider={false}
                        required
                      />
                    </StyledField>
                    <StyledField>
                      <Field
                        component={AccountTypesSelectField}
                        name="type"
                        label={t('type')}
                        placeholder={t('chooseType')}
                        mode={mode}
                        divider={false}
                        required
                      />
                    </StyledField>
                    {isNew && (
                      <>
                        <StyledField>
                          <Field
                            component={ComplexSelectField}
                            name="complex"
                            label={t('RC')}
                            placeholder={t('selectRC')}
                            mode={mode}
                            divider={false}
                            onChange={onChangeComplex}
                            required
                          />
                        </StyledField>
                        <StyledField>
                          <Field
                            component={HouseSelectField}
                            name="building"
                            label={t('building')}
                            placeholder={t('selectBbuilding')}
                            mode={mode}
                            complex={
                              (values as Opened)?.complex
                                ? [(values as Opened).complex]
                                : []
                            }
                            isDisabled={!(values as Opened)?.complex}
                            divider={false}
                            required
                            onChange={onChangeBuilding}
                            helpText={t('SelectFirstRC')}
                          />
                        </StyledField>
                      </>
                    )}
                    <StyledField>
                      <Field
                        component={ApartmentSelectField}
                        name="apartment"
                        label={isNew ? t('Label.flat') : t('Label.address')}
                        placeholder={isNew ? t('chooseFlat') : t('ChooseAddress')}
                        mode={isNew ? mode : 'view'}
                        divider={false}
                        building_id={(values as Opened)?.building?.id}
                        isDisabled={!(values as Opened)?.building}
                        helpText={isNew && t('SelectFirstBuilding')}
                        required
                      />
                    </StyledField>
                    {!isNew && (
                      <StyledField>
                        <Field
                          label={t('Label.FullName')}
                          component={PeopleSelectField}
                          name="users"
                          mode="view"
                          isMulti
                          divider={false}
                        />
                      </StyledField>
                    )}
                  </StyledFormContent>
                </CustomScrollbar>
              </StyledContent>
            </StyledForm>
          );
        }}
      />
    </StyledWrapper>
  );
});
