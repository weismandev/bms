import { memo, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { CustomScrollbar, FilterFooter } from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { AccountTypesSelectField } from '../account-types-select';
import { StyledWrapper, StyledFilterToolbar, StyledForm } from './styled';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useUnit($filters);

  const onClose = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <StyledWrapper>
      <CustomScrollbar autoHide>
        <StyledFilterToolbar closeFilter={onClose} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <StyledForm>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('AnObject')}
                placeholder={t('selectObject')}
                onChange={(value: Record<string, unknown>) => {
                  setFieldValue('complexes', value);
                  setFieldValue('buildings', []);
                }}
                isMulti
              />

              <Field
                component={HouseSelectField}
                complex={values.complexes?.length > 0 && values.complexes}
                name="buildings"
                label={t('Buildings')}
                placeholder={t('selectBbuildings')}
                isMulti
              />

              <Field
                component={AccountTypesSelectField}
                name="types"
                label={t('Types')}
                placeholder={t('chooseTypes')}
                isMulti
              />
              <FilterFooter isResettable />
            </StyledForm>
          )}
        />
      </CustomScrollbar>
    </StyledWrapper>
  );
});
