import styled from '@emotion/styled';
import { Form } from 'formik';

import { Wrapper, FilterToolbar } from '@ui/index';

interface StyledProps {
  className?: string;
  children: JSX.Element;
}

interface ToolbarProps {
  className?: string;
  closeFilter: () => boolean;
}

interface StyledFormProps {
  className?: string;
  children: JSX.Element[];
}

export const StyledWrapper = styled(({ className, children }: StyledProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
`;

export const StyledFilterToolbar = styled(({ className, ...props }: ToolbarProps) => (
  <FilterToolbar className={className} {...props} />
))`
  padding: 24px;
`;

export const StyledForm = styled(({ className, children }: StyledFormProps) => (
  <Form className={className}>{children}</Form>
))`
  padding: 0px 24px 24px;
`;
