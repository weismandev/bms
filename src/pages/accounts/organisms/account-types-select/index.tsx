import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, fxGetList, $isLoading } from '../../models/account-types-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const AccountTypesSelect: FC<object> = (props) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const AccountTypesSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<AccountTypesSelect />} onChange={onChange} {...props} />
  );
};
