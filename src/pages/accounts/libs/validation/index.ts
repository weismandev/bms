import { Opened } from '../../interfaces';

export function complexRequired(this: { parent: Opened }) {
  if (!this.parent.id && !this.parent.complex) {
    return false;
  }

  return true;
}

export function buildingRequired(this: { parent: Opened }) {
  if (!this.parent.id && !this.parent.building) {
    return false;
  }

  return true;
}
