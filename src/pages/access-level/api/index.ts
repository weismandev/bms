import { api } from '@api/api2';

import {
  GetAccessLevelListPayload,
  CreateUpdateAccessLevelPayload,
  DeleteAccessLevelPayload,
  GetPointsPayload,
  GetTypePointPayload,
} from '../interfaces';

const getAccessLevelList = (payload: GetAccessLevelListPayload) =>
  api.no_vers('get', 'scud/get-levels', payload);
const createUpdateAccessLevel = (payload: CreateUpdateAccessLevelPayload) =>
  api.no_vers('post', 'scud/update-levels', payload);
const deleteAccessLevel = (payload: DeleteAccessLevelPayload) =>
  api.no_vers('post', 'scud/remove-level', payload);
const getTypePoint = (payload: GetTypePointPayload) =>
  api.no_vers('get', 'scud/get-compl-type', {
    ...payload,
    ...{ object: 'level' },
  });
const getPoints = (payload: GetPointsPayload) =>
  api.no_vers('get', 'scud/get-levels-entity', payload);

export const accessLevelApi = {
  getAccessLevelList,
  createUpdateAccessLevel,
  deleteAccessLevel,
  getTypePoint,
  getPoints,
};
