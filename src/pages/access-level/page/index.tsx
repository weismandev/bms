import { FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { ThemeAdapter } from '@shared/theme/adapter';

import { HaveSectionAccess } from '@features/common';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import {
  FilterMainDetailLayout,
  ErrorMessage,
  Loader,
  DeleteConfirmDialog,
} from '@ui/index';

import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isLoading,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedVisibilityDeleteModal,
  deleteAccessLevel,
  $isFilterOpen,
} from '../models';
import { Table, Detail, Filters } from '../organisms';

const AccessLevelPage: FC = () => {
  const { t } = useTranslation();
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);
  const isFilterOpen = useStore($isFilterOpen);

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changedVisibilityDeleteModal(false);

  return (
    <ThemeAdapter>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <ColoredTextIconNotification />

      <DeleteConfirmDialog
        header={t('DeletingALevel')}
        content={t('ConfirmLevelDeletion')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteAccessLevel}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ filterWidth: '370px', detailWidth: 'minmax(370px, 50%)' }}
      />
    </ThemeAdapter>
  );
};

const RestrictedAccessLevelPage: FC = () => {
  return (
    <HaveSectionAccess>
      <AccessLevelPage />
    </HaveSectionAccess>
  );
};

export { RestrictedAccessLevelPage as AccessLevelPage };
