import { FC } from 'react';
import { useUnit } from 'effector-react';

import { DataGridTable } from '@shared/ui/data-grid-table';
import type { GridRowParams, GridRowClassNameParams } from '@mui/x-data-grid-pro';

import {
  $tableData,
  $columns,
  $isLoadingList,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  sortChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $pagination,
  $count,
  paginationModelChanged,
  changePinnedColumns,
  $pinnedColumns,
  $openRow,
  $selectRow,
  openViaUrl,
  openRowChanged,
} from '../../models';
import { CustomToolbar } from '../custom-toolbar';
import { StyledWrapper } from './styled';

export const Table: FC = () => {
  const [
    tableData,
    columns,
    isLoading,
    sorting,
    visibilityColumns,
    pagination,
    count,
    pinnedColumns,
    openRow,
    selectRow
  ] = useUnit([
    $tableData,
    $columns,
    $isLoadingList,
    $sorting,
    $visibilityColumns,
    $pagination,
    $count,
    $pinnedColumns,
    $openRow,
    $selectRow
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openViaUrl(row.id);
    openRowChanged(row.id);
  };

  const getRowClassName = ({ id }: GridRowClassNameParams<any>): string => {
    const baseStyle =
      id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

    return baseStyle;
  };

  return (
    <StyledWrapper elevation={0}>
      <CustomToolbar />

      <DataGridTable
        rows={tableData}
        columns={columns}
        rowCount={count}
        loading={isLoading}
        density="compact"
        rowSelectionModel={selectRow}
        paginationModel={pagination}
        onPaginationModelChange={paginationModelChanged}
        sortModel={sorting}
        onSortModelChange={sortChanged}
        pinnedColumns={pinnedColumns}
        onPinnedColumnsChange={changePinnedColumns}
        columnVisibilityModel={visibilityColumns}
        onColumnVisibilityModelChange={visibilityColumnsChanged}
        onColumnWidthChange={columnsWidthChanged}
        onColumnOrderChange={orderColumnsChanged}
        pageSizeOptions={[25, 50, 100]}
        onRowClick={handleClickRow}
        getRowClassName={getRowClassName}
      />
    </StyledWrapper>
  );
};
