import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, $isLoading, fxGetList } from '../../models/type-point-select';
import { Value } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  complexes?: Value[];
  houses?: Value[];
}

export const TypePointSelect: FC<Props> = ({ complexes = [], houses = [], ...props }) => {
  const complexIds = complexes.map((item) => item.id).join(',');
  const buildingIds = houses.map((item) => item?.id ? item.id : item).join(',');

  useEffect(() => {
    fxGetList({ complex_ids: complexIds, building_ids: buildingIds });
  }, [complexIds, buildingIds]);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const TypePointSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return <SelectField component={<TypePointSelect />} onChange={onChange} {...props} />;
};
