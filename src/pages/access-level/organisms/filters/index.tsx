import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { AccessGroupSelectField } from '@features/access-group-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';

import { TypePointSelectField } from '../type-point-select';
import { PointSelectField } from '../point-select';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { Value, Filters as FiltersInterface } from '../../interfaces';
import { useStyles } from './styles';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters) as FiltersInterface;

  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            const onChangeComplex = (value: Value) => {
              setFieldValue('complexes', value);
              setFieldValue('houses', []);
              setFieldValue('pointsType', []);
              setFieldValue('points', []);
            };

            const onChangeBuilding = (value: Value) => {
              setFieldValue('houses', value);
              setFieldValue('pointsType', []);
              setFieldValue('points', []);
            };

            const onChangeTypePoint = (value: Value) => {
              setFieldValue('pointsType', value);
              setFieldValue('points', []);
            };

            return (
              <Form className={classes.form}>
                <Field
                  name="complexes"
                  label={t('objects')}
                  placeholder={t('selectObjects')}
                  component={ComplexSelectField}
                  isMulti
                  onChange={onChangeComplex}
                />
                <Field
                  name="houses"
                  label={t('Buildings')}
                  placeholder={t('selectBbuildings')}
                  component={HouseSelectField}
                  complex={values.complexes.length > 0 && values.complexes}
                  isMulti
                  onChange={onChangeBuilding}
                />
                <Field
                  name="pointsType"
                  label={t('EntryPointType')}
                  placeholder={t('SelectTypeOfEntryPoint')}
                  component={TypePointSelectField}
                  isMulti
                  complexes={values.complexes}
                  houses={values.houses}
                  onChange={onChangeTypePoint}
                />
                <Field
                  name="points"
                  label={t('PassPoints')}
                  placeholder={t('SelectEntryPoints')}
                  component={PointSelectField}
                  isMulti
                  complexes={values.complexes}
                  houses={values.houses}
                  pointsType={values.pointsType}
                />
                <Field
                  name="accessGroups"
                  label={t('OneInclusionInAccessGroups')}
                  placeholder={t('SelectAccessGroup')}
                  component={AccessGroupSelectField}
                  isMulti
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
