import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    height: '100%',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
  },
  content: {
    height: 'calc(100% - 82px)',
    padding: '0 0 24px 24px',
  },
  contentForm: {
    paddingRight: 24,
  },
}));
