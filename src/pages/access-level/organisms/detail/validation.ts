import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  complexes: Yup.array().min(1, t('thisIsRequiredField')),
  houses: Yup.array().min(1, t('thisIsRequiredField')),
  pointsType: Yup.array().min(1, t('thisIsRequiredField')),
  points: Yup.array().min(1, t('thisIsRequiredField')),
});
