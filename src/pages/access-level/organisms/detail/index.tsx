import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { Wrapper, DetailToolbar, CustomScrollbar, InputField } from '@ui/index';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { AccessGroupSelectField } from '@features/access-group-select';

import {
  $opened,
  detailSubmitted,
  changeMode,
  changedDetailVisibility,
  $mode,
  changedVisibilityDeleteModal,
} from '../../models';
import { TypePointSelectField } from '../type-point-select';
import { PointSelectField } from '../point-select';
import { Value } from '../../interfaces';
import { validationSchema } from './validation';
import { useStyles } from './styles';

export const Detail: FC = memo(() => {
  const { t } = useTranslation();
  const opened = useStore($opened);
  const mode = useStore($mode);

  const classes = useStyles();

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm, setFieldValue }) => {
          const onEdit = () => changeMode('edit');
          const onClose = () => changedDetailVisibility(false);
          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };
          const onDelete = () => changedVisibilityDeleteModal(true);

          const onChangeComplex = (value: Value) => {
            setFieldValue('complexes', value);
            setFieldValue('houses', []);
            setFieldValue('pointsType', []);
            setFieldValue('points', []);
          };

          const onChangeBuilding = (value: Value) => {
            setFieldValue('houses', value);
            setFieldValue('pointsType', []);
            setFieldValue('points', []);
          };

          const onChangeTypePoint = (value: Value) => {
            setFieldValue('pointsType', value);
            setFieldValue('points', []);
          };

          return (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.toolbar}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <div className={classes.content}>
                <CustomScrollbar>
                  <div className={classes.contentForm}>
                    <Field
                      name="title"
                      mode={mode}
                      label={t('NameTitle')}
                      placeholder={t('EnterTheNameOfTheAccessLevel')}
                      component={InputField}
                      required
                    />
                    <Field
                      name="complexes"
                      mode={mode}
                      label={t('objects')}
                      placeholder={t('selectObjects')}
                      component={ComplexSelectField}
                      required
                      isMulti
                      onChange={onChangeComplex}
                    />
                    <Field
                      name="houses"
                      mode={mode}
                      label={t('Buildings')}
                      placeholder={t('selectBbuildings')}
                      component={HouseSelectField}
                      complex={values.complexes.length > 0 && values.complexes}
                      required
                      isMulti
                      onChange={onChangeBuilding}
                    />
                    <Field
                      name="pointsType"
                      mode={mode}
                      label={t('EntryPointType')}
                      placeholder={t('SelectTypeOfEntryPoint')}
                      component={TypePointSelectField}
                      required
                      isMulti
                      complexes={values.complexes}
                      houses={values.houses}
                      onChange={onChangeTypePoint}
                    />
                    <Field
                      name="points"
                      mode={mode}
                      label={t('PassPoints')}
                      placeholder={t('SelectEntryPoints')}
                      component={PointSelectField}
                      required
                      isMulti
                      complexes={values.complexes}
                      houses={values.houses}
                      pointsType={values.pointsType}
                    />
                    {!isNew && mode === 'view' && values.access_groups.length > 0 && (
                      <Field
                        name="access_groups"
                        label={t('InclusionInAccessGroups')}
                        mode="view"
                        component={AccessGroupSelectField}
                        isMulti
                      />
                    )}
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});
