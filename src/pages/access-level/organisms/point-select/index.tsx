import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, $isLoading, fxGetList } from '../../models/point-select';
import { Value } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  complexes?: Value[];
  houses?: Value[];
  pointsType?: Value[];
}

export const PointSelect: FC<Props> = ({
  complexes = [],
  houses = [],
  pointsType = [],
  ...props
}) => {
  const complexId = complexes.map((item) => item.id).join(',');
  const buildingId = houses.map((item) => item?.id ? item.id : item).join(',');
  const complTypeId = pointsType.map((item) => item.id).join(',');

  useEffect(() => {
    fxGetList({
      complex_id: complexId,
      buildings_id: buildingId,
      compl_type_id: complTypeId,
    });
  }, [complexId, buildingId, complTypeId]);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const PointSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return <SelectField component={<PointSelect />} onChange={onChange} {...props} />;
};
