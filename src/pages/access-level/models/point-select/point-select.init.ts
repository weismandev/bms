import { signout } from '@features/common';

import { accessLevelApi } from '../../api';
import { fxGetList, $data, $isLoading } from './point-select.model';

fxGetList.use(accessLevelApi.getPoints);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result.map((item) => ({
      id: Number.parseInt(item.value, 10),
      title: item.title,
      compl_type_id: item.compl_type_id,
    }));
  })
  .reset([signout, fxGetList.fail]);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
