import { createEffect, createStore } from 'effector';

import { GetPointsPayload, GetPointsResponse, Point } from '../../interfaces';

export const fxGetList = createEffect<GetPointsPayload, GetPointsResponse[], Error>();

export const $data = createStore<Point[]>([]);
export const $isLoading = createStore<boolean>(false);
