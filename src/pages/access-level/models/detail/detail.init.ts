import { sample } from 'effector';
import { delay } from 'patronum';

import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';

import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from '../table';
import { Opened } from '../../interfaces';
import {
  openViaUrl,
  $isDetailOpen,
  fxGetAccessLevel,
  $opened,
  newEntity,
  entityApi,
  $mode,
  fxCreateUpdateAccessLevel,
  deleteAccessLevel,
  fxDeleteAccessLevel,
  $entityId,
  changedDetailVisibility,
  $path,
} from './detail.model';

$isDetailOpen
  .on(fxGetAccessLevel.done, () => true)
  .off(openViaUrl)
  .reset([signout, pageUnmounted, fxDeleteAccessLevel.done]);

sample({
  clock: openViaUrl,
  fn: (id) => ({ level_ids: id.toString(), compl: 1, show_ag: 1 }),
  target: fxGetAccessLevel,
});

/** При закрытии деталей, сбрасываю openRow */
sample({
  clock: $isDetailOpen,
  filter: (isDetailOpen) => !isDetailOpen,
  fn: () => null,
  target: $openRow,
});


$opened
  .on(fxGetAccessLevel.done, (_, { result }) => {
    if (result?.levels && result?.levels[0]) {
      const data = result.levels[0];

      const typesId = Array.isArray(data?.compl)
        ? data.compl.map((item) => item.compl_type_id)
        : [];

      const points = Array.isArray(data?.compl)
        ? data.compl.map((item) => ({
            id: item.id,
            title: item.title,
            compl_type_id: item.compl_type_id,
          }))
        : [];

      return {
        id: data.id,
        title: data?.title || '',
        complexes: Array.isArray(data?.complex) ? data.complex : [],
        houses: Array.isArray(data?.buildings)
          ? data.buildings.map((building) => building.id)
          : [],
        typesId: [...new Set(typesId)],
        points,
        access_groups: Array.isArray(data?.access_group)
          ? data.access_group.map((item) => ({
              id: item.id,
              title: item.title,
            }))
          : [],
      };
    }

    return newEntity;
  })
  .reset([signout, pageUnmounted, fxGetAccessLevel]);

const formatData = (data: Opened) => ({
  title: data.title,
  complex_ids: data.complexes
    .map((item) => (typeof item === 'object' ? item.id : item))
    .join(','),
  building_ids: data.houses
    .map((item) => (typeof item === 'object' ? item.id : item))
    .join(','),
  compls: data.points.map((item) => ({
    value: item.id,
    level_compl_type_id: item.compl_type_id,
  })),
});

sample({
  clock: entityApi.create,
  fn: (data: Opened) => formatData(data),
  target: fxCreateUpdateAccessLevel,
});

sample({
  clock: entityApi.update,
  fn: (data: Opened) => ({ id: data.id, ...formatData(data) }),
  target: fxCreateUpdateAccessLevel,
});

$mode.reset([signout, pageUnmounted, fxCreateUpdateAccessLevel.done]);

sample({
  clock: fxCreateUpdateAccessLevel.done,
  filter: ({
    result: {
      groups: { id },
    },
  }) => Boolean(id),
  fn: ({
    result: {
      groups: { id },
    },
  }: {
    result: { groups: { id: number } };
  }) => ({
    level_ids: id.toString(),
    compl: 1,
    show_ag: 1,
  }),
  target: fxGetAccessLevel,
});

sample({
  clock: deleteAccessLevel,
  source: $opened,
  fn: ({ id }: { id: number }) => ({ id }),
  target: fxDeleteAccessLevel,
});

sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    level_ids: entityId?.toString(),
    compl: 1,
    show_ag: 1,
  }),
  target: fxGetAccessLevel,
});

const delayedRedirect = delay({ source: fxGetAccessLevel.fail, timeout: 1000 });

sample({
  clock: delayedRedirect,
  fn: () => '.',
  target: historyPush,
});

sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

sample({
  clock: fxCreateUpdateAccessLevel.done,
  source: $path,
  filter: (
    _,
    {
      params,
      result: {
        groups: { id },
      },
    }
  ) => {
    if (!params?.id && id) {
      return true;
    }

    return false;
  },
  fn: (
    path,
    {
      result: {
        groups: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

sample({
  clock: fxDeleteAccessLevel.done,
  source: $path,
  filter: (_, params) => Boolean(params),
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);
