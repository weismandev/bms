import { createEvent, createEffect, combine } from 'effector';

import { createDetailBag } from '@tools/factories';
import { $pathname } from '@features/common';

import { $data as $pointsType } from '../type-point-select/type-point-select.model';
import {
  GetAccessLevelListPayload,
  GetAccessLevelListResponse,
  CreateUpdateAccessLevelPayload,
  CreateUpdateAccessLevelResponse,
  DeleteAccessLevelPayload,
  Value,
  Opened,
} from '../../interfaces';

export const newEntity = {
  title: '',
  complexes: [],
  houses: [],
  typesId: [],
  pointsType: [],
  points: [],
  access_groups: [],
};

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2] || null);

export const deleteAccessLevel = createEvent<void>();

export const fxGetAccessLevel = createEffect<
  GetAccessLevelListPayload,
  GetAccessLevelListResponse,
  Error
>();
export const fxCreateUpdateAccessLevel = createEffect<
  CreateUpdateAccessLevelPayload,
  CreateUpdateAccessLevelResponse,
  Error
>();
export const fxDeleteAccessLevel = createEffect<
  DeleteAccessLevelPayload,
  object,
  Error
>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const $formattedOpened = combine($opened, $pointsType, (opened, pointsType) => {
  const data = { ...opened } as Opened;

  if (
    Array.isArray(data?.typesId) &&
    Array.isArray(pointsType) &&
    pointsType.length > 0
  ) {
    const formattedPointsType = pointsType.reduce(
      (acc: { [key: number]: Value }, point) => ({
        ...acc,
        ...{ [point.id]: point },
      }),
      {}
    );

    const types = data.typesId.map((item: number) => formattedPointsType[item]);

    data.pointsType = types;
  }

  return data;
});
