import { attach } from 'effector';

import {
  createSavedUserSettings,
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';

import { pageMounted } from '../page/page.model';
import {
  $visibilityColumns as $hiddenColumnNames,
  $savedColumns as $columns,
} from '../table/table.model';
import { $pageSize } from '../table/table.storage';

export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const settings = {
  section: 'access-level',
  params: {
    table: {
      $pageSize,
      $hiddenColumnNames,
      $columns,
    },
  },
  pageMounted,
  storage: {
    fxSaveInStorage,
    fxGetFromStorage,
  },
};

export const [$settingsInitialized, $isGettingSettingsFromStorage] =
  createSavedUserSettings(settings);
