import { merge, sample } from 'effector';
import { pending } from 'patronum/pending';
import { GridSortModel } from '@mui/x-data-grid-pro';

import { signout } from '@features/common';

import { accessLevelApi } from '../../api';
import { $tableParams } from '../table/table.model';
import {
  fxGetAccessLevel,
  fxCreateUpdateAccessLevel,
  fxDeleteAccessLevel,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { GetAccessLevelListPayload, Value, Filters } from '../../interfaces';
import { fxGetList as fxGetTypesPoint } from '../type-point-select/type-point-select.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  fxGetAccessLevelList,
  $error,
  $isLoading,
  $isErrorDialogOpen,
  $rawData,
  $isLoadingList,
} from './page.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';

interface Table {
  page: number;
  per_page: number;
  search: string;
  sorting: GridSortModel;
}

fxGetAccessLevelList.use(accessLevelApi.getAccessLevelList);
fxGetAccessLevel.use(accessLevelApi.getAccessLevelList);
fxCreateUpdateAccessLevel.use(accessLevelApi.createUpdateAccessLevel);
fxDeleteAccessLevel.use(accessLevelApi.deleteAccessLevel);

const errorOccured = merge([
  fxGetAccessLevelList.fail,
  fxGetAccessLevel.fail,
  fxCreateUpdateAccessLevel.fail,
  fxDeleteAccessLevel.fail,
]);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading
  .on(
    pending({
      effects: [fxGetAccessLevel, fxCreateUpdateAccessLevel, fxDeleteAccessLevel],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingList
  .on(fxGetAccessLevelList.pending, (_, loading) => loading)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: [
    pageMounted,
    $tableParams,
    fxCreateUpdateAccessLevel.done,
    fxDeleteAccessLevel.done,
    $filters,
    $settingsInitialized,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }: { table: Table; filters: Filters }) => {
    const payload: GetAccessLevelListPayload = {
      compl: 1,
      limit: table.per_page,
      offset: (table.page - 1) * table.per_page,
      search: table.search,
      show_ag: 1,
    };

    if (table.sorting.length > 0) {
      payload.sort_by = table.sorting[0].field || 'id';
      payload.sort_order = table.sorting[0].sort || 'desc';
    }

    if (filters.complexes.length > 0) {
      payload.complex_id = filters.complexes.map((item: Value) => item.id).join(',');
    }

    if (filters.houses.length > 0) {
      payload.buildings = filters.houses.map((item: Value) => item.id).join(',');
    }

    if (filters.pointsType.length > 0) {
      payload.level_compl_type_ids = filters.pointsType
        .map((item: Value) => item.id)
        .join(',');
    }

    if (filters.points.length > 0) {
      payload.level_compl_type_values = filters.points
        .map((item: Value) => item.id)
        .join(',');
    }

    if (filters.accessGroups.length > 0) {
      payload.access_group_ids = filters.accessGroups
        .map((item: Value) => item.id)
        .join(',');
    }

    return payload;
  },
  target: fxGetAccessLevelList,
});

$rawData
  .on(fxGetAccessLevelList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

sample({
  clock: pageMounted,
  target: fxGetTypesPoint,
});
