import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetAccessLevelListPayload, GetAccessLevelListResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetAccessLevelList = createEffect<
  GetAccessLevelListPayload,
  GetAccessLevelListResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetAccessLevelListResponse>({
  meta: {
    total: 0,
  },
  levels: [],
});
export const $isLoadingList = createStore<boolean>(false);
