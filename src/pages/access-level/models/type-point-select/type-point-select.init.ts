import { signout } from '@features/common';

import { accessLevelApi } from '../../api';
import { fxGetList, $data, $isLoading } from './type-point-select.model';

fxGetList.use(accessLevelApi.getTypePoint);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.types)) {
      return [];
    }

    return result.types.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
