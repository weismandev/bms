import { createEffect, createStore } from 'effector';

import { Value, GetTypePointPayload, GetTypePointResponse } from '../../interfaces';

export const fxGetList = createEffect<GetTypePointPayload, GetTypePointResponse, Error>();

export const $data = createStore<Value[]>([]);
export const $isLoading = createStore<boolean>(false);
