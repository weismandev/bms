import { combine } from 'effector';
import { format } from 'date-fns';

import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';

import { $rawData } from '../page/page.model';
import { $data as $pointsType } from '../type-point-select/type-point-select.model';
import { Level, Value, TableData } from '../../interfaces';

const { t } = i18n;

const columns = [
  {
    field: 'title',
    headerName: t('NameTitle'),
    width: 350,
    sortable: true,
  },
  {
    field: 'complexes',
    headerName: t('objects'),
    width: 250,
    sortable: false,
  },
  {
    field: 'houses',
    headerName: t('Buildings'),
    width: 250,
    sortable: false,
  },
  {
    field: 'pointsType',
    headerName: t('PointTypes'),
    width: 250,
    sortable: false,
  },
  {
    field: 'points',
    headerName: t('PassPoints'),
    width: 130,
    sortable: false,
  },
  {
    field: 'access_groups',
    headerName: t('navMenu.security.accessGroups'),
    width: 230,
    sortable: false,
  },
  {
    field: 'created_at',
    headerName: t('DateOfCreation'),
    width: 130,
    sortable: true,
  },
  {
    field: 'updated_at',
    headerName: t('DateOfChange'),
    width: 130,
    sortable: true,
  },
];

export const {
  $pagination,
  $tableParams,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  sortChanged,
  searchForm,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  }
});

const formatData = (levels: Level[], pointsType: Value[]) =>
  levels.map((level) => {
    const typesId = Array.isArray(level?.compl)
      ? level.compl.map((item) => item.compl_type_id)
      : [];

    const uniqueTypesId = [...new Set(typesId)];

    const complexes = (() => {
      if (!Array.isArray(level?.complex)) return '-';

      return level.complex.length === 1
        ? level.complex[0]?.title || '-'
        : level.complex.length;
    })();

    const houses = (() => {
      if (!Array.isArray(level?.buildings)) return '-';

      return level.buildings.length === 1
        ? level.buildings[0]?.title || '-'
        : level.buildings.length;
    })();

    const points = Array.isArray(level?.compl)
      ? level.compl.filter((item) => item.title).length || 0
      : '-';

    const accessGroups = (() => {
      if (!Array.isArray(level?.access_group)) return '-';

      return level.access_group.length === 1
        ? level.access_group[0]?.title || '-'
        : level.access_group.length;
    })();

    const tableData: TableData = {
      id: level.id,
      title: level?.title || '-',
      complexes,
      houses,
      points,
      access_groups: accessGroups,
      created_at: level?.created_at
        ? format(new Date(level.created_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
      updated_at: level?.updated_at
        ? format(new Date(level.updated_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
    };

    if (
      Array.isArray(uniqueTypesId) &&
      Array.isArray(pointsType) &&
      pointsType.length > 0
    ) {
      const formattedPointsType = pointsType.reduce(
        (acc: { [key: number]: Value }, point) => ({
          ...acc,
          ...{ [point.id]: point },
        }),
        {}
      );

      const types = uniqueTypesId
        .map((item: number) => formattedPointsType[item].title)
        .join('; ');

      tableData.pointsType = types;
    } else {
      tableData.pointsType = '-';
    }

    return tableData;
  });

export const $savedColumns = $columns.map((cols) =>
  cols.map((col) => ({
    field: col.field,
    width: col.width,
  }))
);

export const $count = $rawData.map(({ meta }) => meta?.total || 0);

export const $tableData = combine($rawData, $pointsType, ({ levels }, pointsType) =>
  levels && levels.length > 0 ? formatData(levels, pointsType) : []
);
