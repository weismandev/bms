import { sample } from 'effector';

import { signout } from '@features/common';
import { Columns } from '@features/data-grid';

import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  detailClosed,
  addClicked,
  fxCreateUpdateAccessLevel,
  fxDeleteAccessLevel,
  $entityId,
} from '../detail/detail.model';
import { $selectRow, $columns, $savedColumns } from './table.model';
import { fxGetFromStorage } from '../user-settings/user-settings.model';

$selectRow
  .on(fxCreateUpdateAccessLevel.done, (state, { params, result }) =>
    !params.id && result.groups.id ? [result.groups.id] : state
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked, fxDeleteAccessLevel.done]);

sample({
  clock: pageMounted,
  source: { id: $entityId, selectRow: $selectRow },
  filter: ({ selectRow }) => Array.isArray(selectRow) && selectRow.length === 0,
  fn: ({ id }) => [Number.parseInt(id, 10)],
  target: $selectRow,
});

sample({
  clock: fxGetFromStorage.done,
  source: { columns: $columns, savedColumns: $savedColumns },
  fn: ({ columns, savedColumns }) => {
    const formattedColumns = savedColumns.reduce((acc: Columns[], column) => {
      const findedColumn = columns.find((item) => item.field === column.field);

      return findedColumn
        ? [
            ...acc,
            {
              field: column.field,
              headerName: findedColumn.headerName,
              width: column.width,
              sortable: findedColumn.sortable,
            },
          ]
        : acc;
    }, []);

    const newColumns = columns.filter(
      (item) => !formattedColumns.some((element: Columns) => element.field === item.field)
    );

    newColumns.forEach((item) => {
      const index = columns.map((column) => column.field).indexOf(item.field);

      formattedColumns.splice(index, 0, item);
    });

    return formattedColumns;
  },
  target: $columns,
});
