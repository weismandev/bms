import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  complexes: [],
  houses: [],
  pointsType: [],
  points: [],
  accessGroups: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
