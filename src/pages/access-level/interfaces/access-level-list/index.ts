import { Value } from '../general-interfaces';

export interface GetAccessLevelListPayload {
  level_ids?: string;
  compl?: number;
  limit?: number;
  offset?: number;
  search?: string;
  sort_by?: string;
  sort_order?: string;
  show_ag?: number;
  complex_id?: string;
  buildings?: string;
  level_compl_type_ids?: string;
  level_compl_type_values?: string;
  access_group_ids?: string;
}

export interface GetAccessLevelListResponse {
  meta: {
    total: number;
  };
  levels: Level[];
}

export interface Level {
  id: number;
  title: string;
  complex: Value[];
  buildings: Value[];
  created_at: number;
  updated_at: number;
  compl: Compl[];
  access_group: Value[];
}

export interface Compl {
  id: number;
  title: string;
  type: string;
  compl_type_id: number;
}
