export interface Value {
  id: number;
  title: string;
}

export interface Filters {
  complexes: Value[];
  houses: Value[];
  pointsType: Value[];
  points: Value[];
  accessGroups: Value[];
}
