export interface GetPointsPayload {
  complex_id: string;
  buildings_id: string;
  compl_type_id: string;
}

export interface GetPointsResponse {
  compl_type_id: number;
  value: string;
  title: string;
}

export interface Point {
  id: number;
  title: string;
  compl_type_id: number;
}
