import { Value } from '../general-interfaces';
import { Point } from '../point-select';

export interface CreateUpdateAccessLevelPayload {
  id?: number;
  title: string;
  complex_ids: string;
  building_ids: string;
}

export interface CreateUpdateAccessLevelResponse {
  groups: {
    id: number;
  };
}

export interface DeleteAccessLevelPayload {
  id: number;
}

export interface Opened {
  id?: number;
  title: string;
  complexes: Value[];
  houses: Value[];
  points: Point[];
  typesId: number[];
  pointsType: Value[];
  access_groups: Value[];
}

export interface TableData {
  id: number;
  title: string;
  complexes: number | string | never[];
  houses: number | string | never[];
  pointsType?: string;
  points: number | string;
  created_at: string;
  updated_at: string;
  access_groups: string | number;
}
