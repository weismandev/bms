import { Value } from '../general-interfaces';

export interface GetTypePointPayload {
  complex_ids?: string;
  building_ids?: string;
}

export interface GetTypePointResponse {
  types: Value[];
}
