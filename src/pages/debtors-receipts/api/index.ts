import { api } from '../../../api/api2';
import { DebitorsReceipts, DebitorsReceiptsExport } from '../interfaces';

const getReceiptsList = ({
  payload,
  signal,
}: {
  payload: DebitorsReceipts;
  signal: AbortController['signal'];
}) => api.v1('get', 'debtors/receipts', payload, signal);

const sendlistExport = (payload: DebitorsReceiptsExport) =>
  api.v1('get', 'debtors/receipts/export', payload);

const dowloadReceipt = (payload: any) =>
  api.v1('request', 'apartment-accounts/crm/download-operation', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });

export const debitorsReceiptsApi = {
  getReceiptsList,
  sendlistExport,
  dowloadReceipt,
};
