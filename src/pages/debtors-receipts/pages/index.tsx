import { useEffect } from 'react';
import { useLocation } from 'react-router';
import { useUnit } from 'effector-react';
import { ThemeAdapter } from '@shared/theme/adapter';
import { Loader, ErrorMessage, FilterMainDetailLayout } from '../../../ui';
import {
  PageGate,
  $isLoading,
  $error,
  clearError,
  $isExportError,
  clearExportError,
  searchForm,
} from '../models';
import { $isFilterOpen } from '../models/filter/model';
import { PopupExport, Table, Filter } from '../organisms';

type LocationState = {
  search?: string;
};

const DebtorsReceipts = () => {
  const [error, isFilterOpen, isExportError] = useUnit([
    $error,
    $isFilterOpen,
    $isExportError,
  ]);

  const { setValue } = useUnit(searchForm);
  const data = useLocation();

  useEffect(() => {
    const search = (data?.state as LocationState)?.search;
    setValue({ path: 'search', value: search ?? '' });
  }, []);

  return (
    <ThemeAdapter>
      <PageGate />

      <ErrorMessage isOpen={error} onClose={clearError} error={error} />

      <ErrorMessage
        isOpen={isExportError}
        onClose={clearExportError}
        error={isExportError}
      />

      <PopupExport />

      <FilterMainDetailLayout
        params={{ filterWidth: '400px' }}
        filter={isFilterOpen && <Filter />}
        main={<Table />}
      />
    </ThemeAdapter>
  );
};

const RestrictedDebtorsReceipts = (props: any) => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <DebtorsReceipts {...props} />
    // </HaveSectionAccess>
  );
};

export { RestrictedDebtorsReceipts };
