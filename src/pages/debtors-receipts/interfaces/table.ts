export interface ReceiptFilename {
  filename: string;
  operation_date?: string;
  operation_id: number;
}

export interface ReceiptTableData {
  id: string | number;
  account_number: string;
  apartment_title: string;
  operation_date: string;
  receipt_filename: ReceiptFilename;
}
