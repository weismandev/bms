export interface Meta {
  total: number;
  pages: number;
  page: number;
  per_page: number;
}

export interface Apartment {
  id: number;
  title: string;
}

export interface Building {
  id: number;
  title: string;
}

export interface Account {
  number: string;
}

export interface ReceiptInfo {
  operation_id: number;
  operation_date: string;
  filename: string;
}

export interface Resident {
  id: number | string;
  full_name: string;
}

export interface Receipt {
  apartment: Apartment;
  building: Building;
  account: Account;
  receipt: ReceiptInfo;
  residents: Resident[];
}

export interface ReceiptsData {
  meta: Meta;
  receipts: Receipt[];
}

export type ReceiptsTable = Partial<Receipt>;
