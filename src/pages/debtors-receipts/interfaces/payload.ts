export interface Filters {
  operation_date_from?: string;
  operation_date_to?: string;
  complexes: (number | string)[];
  buildings: (number | string)[];
}

export interface Sort {
  sort?: string;
  column?: string;
}

export interface Pag {
  page?: number | string;
  per_page?: number | string;
}

export interface Search {
  search?: string;
}

export type DebitorsReceipts = Filters & Sort & Pag & Search;

export type DebitorsReceiptsExport = DebitorsReceipts & { email: string };
