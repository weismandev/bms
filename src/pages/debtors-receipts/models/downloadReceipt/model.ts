import { createEffect, createEvent, createStore } from 'effector';
import FileSaver from 'file-saver';

import { debitorsReceiptsApi } from '../../api';

export const downloadReceipt = createEvent();
export const fxDownloadReceipt = createEffect().use(debitorsReceiptsApi.dowloadReceipt);
export const fxOpenDownloadedReceipt = createEffect(({ params, result }) => {
  const { filename: name = '', id = '' } = params;
  const blob = new Blob([result], { type: 'aplication/pdf' });

  FileSaver.saveAs(URL.createObjectURL(blob), name ?? 'receipt.pdf');
});

export const $isDownloadPending = createStore(false);
