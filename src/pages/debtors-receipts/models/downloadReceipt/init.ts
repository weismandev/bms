import { sample } from 'effector';
import {
  downloadReceipt,
  fxDownloadReceipt,
  fxOpenDownloadedReceipt,
  $isDownloadPending,
} from './model';

/** По клику на ссылку передаем в эффект загрузки параметры */
sample({
  clock: downloadReceipt,
  target: fxDownloadReceipt,
});

/** Если загрузка прошла успешно, передаем результат в эффект сохранения */
sample({
  clock: fxDownloadReceipt.done,
  target: fxOpenDownloadedReceipt,
});

/** Статус всего процесса загрузки */
sample({
  clock: [fxDownloadReceipt.pending, fxOpenDownloadedReceipt.pending],
  target: $isDownloadPending,
});
