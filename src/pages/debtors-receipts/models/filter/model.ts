import { createForm } from '@effector-form';

import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  complexes: [],
  houses: [],
  operation_date_from: '',
  operation_date_to: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

export const filterForm = createForm({
  initialValues: {
    complexes: [],
    houses: [],
    operation_date_from: '',
    operation_date_to: '',
  },

  onSubmit: filtersSubmitted,
  resetOn: filtersSubmitted,
  editable: true,
  sid: 'filter:debtors-receipts',
});
