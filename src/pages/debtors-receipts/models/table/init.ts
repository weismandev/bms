import format from 'date-fns/format';
import { sample } from 'effector';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { Receipt, ReceiptTableData } from '../../interfaces';

import { $debtorsReceiptsData } from '../page/model';
import { $tableData, changeTabledata } from './model';

$tableData.on(changeTabledata, (_, data) => data);

const getFormatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(localDate, 'dd.MM.yyyy HH:mm');
};

const formatNotificationsToTable = (receipts: Receipt[]): ReceiptTableData[] =>
  receipts.map(({ account, apartment, receipt, residents }) => ({
    id: receipt.operation_id,
    account_number: account.number,
    residents_full_name: residents.map((resident) => resident.full_name),
    apartment_title: apartment.title,
    operation_date: getFormatDate(receipt.operation_date),
    receipt_filename: {
      ...receipt,
      filename: receipt.filename.replace(/[\'\"]/g, ''),
    },
  }));

/** Формирую данные таблицы при загрузке */
sample({
  clock: $debtorsReceiptsData,
  fn: ({ receipts }) => {
    if (!receipts) return [];

    return formatNotificationsToTable(receipts);
  },
  target: changeTabledata,
});
