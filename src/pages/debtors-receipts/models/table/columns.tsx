import type { GridColDef, GridRenderCellParams } from '@mui/x-data-grid-pro';
import { IconButton, Popover } from '@ui/index';
import i18n from '@shared/config/i18n';
import { downloadReceipt } from '../downloadReceipt/model';
import {
  StyledResidentsRoot,
  StyledAdditionalResidents,
  StyledPsevdoLink
} from '../../organisms/table/styled';

const { t } = i18n;

const renderCellresidentsFullName = ({ value }: GridRenderCellParams) => (
  <StyledResidentsRoot>
    <span>{value[0]}</span>
    {value[1] && (
      <Popover
        cn={{ content: { boxShadow: 'none' } }}
        trigger={(
          <IconButton size="small">
            <span>+</span>
            <span>{value.slice(1).length}</span>
          </IconButton>
        )}
      >
        <StyledAdditionalResidents>
          {value.map((resident: string, index: number) => (
            <span key={index}>
              {resident}
            </span>
          ))}
        </StyledAdditionalResidents>
      </Popover>
    )}
  </StyledResidentsRoot>
);

const renderCellReceiptFilename = ({
  value: { operation_id, filename = 'not_exist' }
}: GridRenderCellParams) => {
  const handleDownloadReceipt = () => {
    downloadReceipt({ id: operation_id, filename });
  };
  return (
    <StyledPsevdoLink onClick={handleDownloadReceipt}>
      {filename}
    </StyledPsevdoLink>
  );
};

export const columns: GridColDef[] = [
  {
    field: 'account_number',
    headerName: t('Label.personalAccount') ?? '',
    width: 250,
    sortable: true,
  },
  {
    field: 'residents_full_name',
    headerName: t('Label.FullName') ?? '',
    width: 400,
    sortable: false,
    renderCell: renderCellresidentsFullName
  },
  {
    field: 'apartment_title',
    headerName: t('AddressOfTheObject') ?? '',
    width: 250,
    sortable: false,
  },
  {
    field: 'operation_date',
    headerName: t('DebtorsNotifications.Sent') ?? '',
    width: 160,
    sortable: true,
  },
  {
    field: 'receipt_filename',
    headerName: t('DebtorsList.Receipts') ?? '',
    width: 400,
    sortable: false,
    renderCell: renderCellReceiptFilename
  },
];
