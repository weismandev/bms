import { combine } from 'effector';
import { connectStorage } from '@shared/tools/storage';

import { PageGate, LOCAL_STORAGE_KEY } from '../page/model';
import {
  $pagination, paginationModelChanged,
  $pinnedColumns, changePinnedColumns,
  $visibilityColumns, visibilityColumnsChanged,
  $columns, mergeColumnsByPattern,
} from './model';

const $pageSize = combine($pagination, ({ pageSize }) => pageSize);
const prependPageSizeChanged = (pageSize: number) => ({
  page: 0,
  pageSize
});

const storage = connectStorage<unknown>({
  storageKey: LOCAL_STORAGE_KEY,
  storageGate: PageGate,
  storageScheme: {
    columns: [$columns, mergeColumnsByPattern],
    pageSize: [$pageSize, paginationModelChanged.prepend(prependPageSizeChanged)],
    pinnedColumns: [$pinnedColumns, changePinnedColumns],
    visibilityColumns: [$visibilityColumns, visibilityColumnsChanged]
  },
});

export default storage;
