import { attach, combine, createEvent, createStore } from 'effector';

import { createTableBag } from '@shared/tools/table';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { $debtorsReceiptsData } from '../page/model';
import { columns } from './columns';
import { ReceiptTableData } from '../../interfaces';

const changeTabledata = createEvent<ReceiptTableData[]>();
const $tableData = createStore<ReceiptTableData[]>([]);

export const {
  $tableParams,
  $pagination,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  sortChanged,
  searchForm,
  searchChanged,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  mergeColumnsByPattern,
  paginationModelChanged
} = createTableBag({
  columns
});

export const $count = $debtorsReceiptsData.map(({ meta }) => meta?.total || 0);

export const $countPage = combine($count, $pagination, (count, { pageSize }) =>
  count ? Math.ceil(count / pageSize) : 0
);

export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

export { $tableData, columns, changeTabledata };
