import { merge, sample } from 'effector';
import { signout } from '@features/common';
import { debitorsReceiptsApi } from '../../api';
import { fxDownloadReceipt } from '../downloadReceipt/model';
import { $filters } from '../filter/model';
import { $tableParams, selectionRowChanged, searchChanged } from '../table/model';
import { collectPayload } from './collectPayload';
import {
  pageMounted,
  pageUnmounted,
  $debtorsReceiptsData,
  $isLoading,
  $error,
  fxGetDebtorsReceiptslist,
  CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
} from './model';

let requestController: AbortController;

/** Прооверка наличия запроса, запись в $isLoading */
const isRequest = merge([fxGetDebtorsReceiptslist.pending]);
$isLoading.on(isRequest, (_state, isLoading) => isLoading).reset(signout);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([
  fxGetDebtorsReceiptslist.failData,
  fxDownloadReceipt.failData,
]);

/** Записываю ошибку, только если запрос не бы отменен */
sample({
  clock: isError,
  filter: (error) => error.message !== CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
  fn: (error) => error.message ?? 'Request error',
  target: $error,
});

$error
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(signout);

/** Запуск запроса уведомлений */
fxGetDebtorsReceiptslist.use(debitorsReceiptsApi.getReceiptsList);

/** Запись результатов запроса уведомлений */
$debtorsReceiptsData.on(fxGetDebtorsReceiptslist.doneData, (_, result) => result);

/** При загрузке, изменении фильтров, табов, пагинации, сортировки
 * вызываем запрос квитанций должников */
sample({
  clock: [pageMounted, $tableParams, $filters],
  source: [$tableParams, $filters],
  fn: ([tableParams, filters]) => {
    if (requestController) {
      requestController.abort();
    }

    requestController = new AbortController();

    return {
      payload: collectPayload([tableParams, filters]),
      signal: requestController.signal,
    };
  },
  target: fxGetDebtorsReceiptslist,
});

/** Очистка выделенных записей при новой загрузке должников */
sample({
  clock: $debtorsReceiptsData,
  fn: () => [],
  target: selectionRowChanged,
});

/** Удаляю search при pageUnmounted */
sample({
  clock: pageUnmounted,
  fn: () => '',
  target: searchChanged,
});
