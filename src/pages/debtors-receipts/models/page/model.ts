import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { createPageBag } from '../../../../tools/factories';
import { debitorsReceiptsApi } from '../../api';
import { DebitorsReceipts, ReceiptsData } from '../../interfaces';

export const LOCAL_STORAGE_KEY = 'debtors-receipts';
export const CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS = 'canceled';

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const clearError = createEvent();

const $debtorsReceiptsData = createStore<Partial<ReceiptsData>>({});
const $isLoading = createStore(false);
const $error = createStore(null).on(clearError, () => null);

const fxGetDebtorsReceiptslist = createEffect<
  { payload: DebitorsReceipts; signal: AbortController['signal'] },
  ReceiptsData,
  Error
>();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  $debtorsReceiptsData,
  $isLoading,
  $error,
  fxGetDebtorsReceiptslist,
  clearError,
};

export const {
  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(debitorsReceiptsApi);
