import { DebitorsReceipts } from '../../interfaces';

export function collectPayload([tableParams, filters]: [any, any]) {
  const payload = {} as DebitorsReceipts;

  if (tableParams.search) {
    payload.search = tableParams.search;
  }

  if (tableParams.sorting.length > 0) {
    payload.sort = tableParams.sorting[0].sort;
    payload.column = tableParams.sorting[0].field;
  }

  if (tableParams.per_page) {
    payload.per_page = tableParams.per_page;
  }

  if (tableParams.page) {
    payload.page = tableParams.page;
  }

  if (typeof filters === 'object') {
    if (filters.complexes && filters.complexes.id) {
      payload.complexes = [filters.complexes.id];
    }

    if (Array.isArray(filters.houses) && filters.houses.length > 0) {
      payload.buildings = filters.houses.map((house: any) => house.id);
    }

    if (!!filters.operation_date_from) {
      payload.operation_date_from = `${filters.operation_date_from} 00:00:00`;
    }

    if (!!filters.operation_date_to) {
      payload.operation_date_to = `${filters.operation_date_to} 23:59:59`;
    }

    if (payload.operation_date_from && !payload.operation_date_to) {
      payload.operation_date_to = '3000-01-01 00:00:00';
    }

    if (!payload.operation_date_from && payload.operation_date_to) {
      payload.operation_date_from = '1950-01-01 00:00:00';
    }
  }

  return payload;
}
