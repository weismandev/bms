import { createEffect, createEvent, createStore } from 'effector';
import { DebitorsReceiptsExport } from '../../interfaces';

export const $exportPopup = createStore<boolean>(false);
export const showExportPopup = createEvent();
export const hideExportPopup = createEvent();

export const $isSending = createStore<boolean>(false);
export const clearExportError = createEvent();
export const $isExportError = createStore<Error | null>(null).on(
  clearExportError,
  () => null
);

export const $sendResult = createStore<boolean | null>(null);
export const sendlistExport = createEvent<{ email: string }>();
export const fxSendlistExport = createEffect<DebitorsReceiptsExport, void, Error>();
