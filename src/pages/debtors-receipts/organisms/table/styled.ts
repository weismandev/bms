import styled from '@emotion/styled';
import { Paper } from '@mui/material';
import { Grid } from '@devexpress/dx-react-grid-material-ui';
import { Form } from 'formik';

export const StyledPaper = styled(Paper)`
  padding: 0 24px;
  height: 100%;
  display: flex;
  flex-direction: column;
  padding-bottom: 0px;
`;

export const StyledResidentsRoot = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'start',
  alignItems: 'center',
}));

export const StyledAdditionalResidents = styled.div(() => ({
  display: 'grid',
  gap: 10,
}));

export const StyledForm = styled(Form)`
  padding: 0 24px 24px;
`;

export const StyledPsevdoLink = styled.span`
  text-decoration: underline;
  color: #007bff;
  :hover {
    text-decoration: none;
  }
`;
