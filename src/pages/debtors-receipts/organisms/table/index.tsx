import { FC, memo } from 'react';
import { useUnit } from 'effector-react';
import { DataGridTable } from '@shared/ui/data-grid-table';
import type { GridRowParams } from '@mui/x-data-grid-pro';
import {
  $tableData,
  sortChanged,
  $count,
  paginationModelChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $isLoading,
  openRowChanged,
  $pagination,
  $pinnedColumns,
  changePinnedColumns,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { StyledPaper } from './styled';

export const Table: FC = memo(() => {
  const [
    isLoading,
    tableData,
    pagination,
    visibilityColumns,
    columns,
    count,
    pinnedColumns
  ] = useUnit([
    $isLoading,
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $count,
    $pinnedColumns
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openRowChanged(Number(row.id));
  };

  return (
    <StyledPaper elevation={0}>
      <TableToolbar />
      <DataGridTable
        columns={columns}
        rows={tableData}
        rowCount={count}
        density="compact"
        loading={isLoading}
        paginationModel={pagination}
        pinnedColumns={pinnedColumns}
        onRowClick={handleClickRow}
        columnVisibilityModel={visibilityColumns}
        onSortModelChange={sortChanged}
        onPaginationModelChange={paginationModelChanged}
        onColumnWidthChange={columnsWidthChanged}
        onPinnedColumnsChange={changePinnedColumns}
        onColumnVisibilityModelChange={visibilityColumnsChanged}
        onColumnOrderChange={orderColumnsChanged}
      />
    </StyledPaper>
  );
});
