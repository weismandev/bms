import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Button } from '@mui/material';
import { Check, Clear } from '@mui/icons-material';
import { filterForm } from '../../models';
import { StyledContainer } from './styled';

export const FilterFooter: FC = () => {
  const { t } = useTranslation();
  const { submit, reset, submittable, initialValues, values } = useUnit(filterForm);

  const isDefaultFilterValues =
    JSON.stringify({ ...values }) === JSON.stringify(initialValues);

  return (
    <StyledContainer>
      <Button
        variant="contained"
        startIcon={<Check />}
        color="primary"
        onClick={submit}
        disabled={!submittable}
      >
        {t('Apply')}
      </Button>
      <Button
        variant="outlined"
        startIcon={<Clear />}
        color="primary"
        onClick={reset}
        disabled={isDefaultFilterValues}
      >
        {t('Reset')}
      </Button>
    </StyledContainer>
  );
};