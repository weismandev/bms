import styled from '@emotion/styled';
import { Wrapper } from '@ui/atoms';

export const StyledDates = styled.div`
  display: flex;
  margin-bottom: 25px;
  justify-content: space-between;
`;

export const StyledDatesLabel = styled.div(() => ({
  color: '#9494a3',
  marginBottom: '10px',
}));

export const StyledDatesOwnLabel = styled.div`
  color: '#9494a3';
  margin-right: 6px;
`;

export const StyledDateItem = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledWrapper = styled(Wrapper)`
  height: 100%;
`;
