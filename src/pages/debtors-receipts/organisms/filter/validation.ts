import i18n from '@shared/config/i18n';
import * as Yup from 'yup';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  operation_date_from: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .nullable(),
  operation_date_to: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    )
    .nullable(),
});

function startDateAfterFinishDate(this: {
  createError: any;
  parent: {
    operation_date_from: string;
    operation_date_to: string;
  };
}) {
  const isDatesDefined =
    Boolean(this.parent.operation_date_from) && Boolean(this.parent.operation_date_to);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.operation_date_from)) >
      Number(new Date(this.parent.operation_date_to))
  ) {
    return this.createError();
  }

  return true;
}
