import { memo } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';

import i18n from '@shared/config/i18n';

import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';

import {
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectField,
  InputField,
} from '@ui/index';

import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import {
  StyledDateItem,
  StyledDates,
  StyledDatesLabel,
  StyledDatesOwnLabel,
  StyledWrapper,
} from './styled';
import { validationSchema } from './validation';
import { StyledForm } from '../table/styled';

const { t } = i18n;

const Filter = memo(() => {
  const filters = useUnit($filters);

  return (
    <StyledWrapper>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          validationSchema={validationSchema}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <StyledForm>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('RC')}
                placeholder={t('selectRC')}
              />

              <Field
                name="houses"
                complex={
                  Array.isArray(values?.objects) &&
                  values.objects.length > 0 &&
                  values.objects
                }
                component={HouseSelectField}
                onChange={(value: any) => {
                  setFieldValue('houses', value);
                }}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />

              <StyledDatesLabel>
                {t('DebtorsNotifications.DepartureDate') as string}
              </StyledDatesLabel>
              <StyledDates>
                <StyledDateItem>
                  <StyledDatesOwnLabel>
                    {t('DebtorsNotifications.from') as string}
                  </StyledDatesOwnLabel>
                  <Field
                    name="operation_date_from"
                    label={null}
                    divider={false}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                </StyledDateItem>

                <StyledDateItem>
                  <StyledDatesOwnLabel>
                    {t('DebtorsNotifications.to') as string}
                  </StyledDatesOwnLabel>
                  <Field
                    name="operation_date_to"
                    label={null}
                    divider={false}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                </StyledDateItem>
              </StyledDates>

              <FilterFooter isResettable />
            </StyledForm>
          )}
        />
      </CustomScrollbar>
    </StyledWrapper>
  );
});

export { Filter };
