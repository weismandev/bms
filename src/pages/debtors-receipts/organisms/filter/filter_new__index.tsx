import { memo } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Formik, Field } from 'formik';
import { Control } from '@effector-form';
import { Paper } from '@mui/material';

import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';

import {
  FilterToolbar,
  CustomScrollbar,
  SelectField,
  InputField,
} from '@ui/index';

import { changedFilterVisibility, filtersSubmitted, $filters, filterForm } from '../../models';
import { FilterFooter } from '../filters-footer';

const Filter = memo(() => {
  const { t } = useTranslation();
  const { submittable, submit, values } = useUnit(filterForm);
  const filters = useUnit($filters);

  const handleFilterVisibility = () => {
    changedFilterVisibility(false);
  };

  return (
    <Paper style={{ height: '100%', padding: 24 }}>
      <FilterToolbar closeFilter={handleFilterVisibility} />
      <CustomScrollbar autoHide>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Control.Complex
            name="complexes"
            label={t('RC')}
            placeholder={t('selectRC')}
            form={filterForm}
            size="small"
            fullWidth
          />
          <Control.Buildings
            name="houses"
            complex={values.complexes}
            label={t('building')}
            placeholder={t('selectBbuilding')}
            form={filterForm}
            size="small"
            fullWidth
          />
          <Control.DateRange
            name="operation_date_from"
            label={t('DebtorsNotifications.DepartureDate')}
            form={filterForm}
          />
        </div>
        <FilterFooter />
      </CustomScrollbar>
    </Paper>
  );
});

export { Filter };
