import {
  $exportPopup,
  $isSending,
  $sendResult,
  hideExportPopup,
  sendlistExport,
} from '../../models';
import { Check, Clear } from '@mui/icons-material';
import { ActionButton } from '@ui/atoms';
import { InputField } from '@ui/molecules';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import {
  StyledAlertMessage,
  StyledButtonsBlock,
  StyledCheckCircleIcon,
  StyledCloseIcon,
  StyledDataBlock,
  StyledForm,
  StyledHeader,
  StyledHeaderTitle,
  StyledItem,
  StyledPopup,
  StyledSuccessMessage,
} from './styled';
import * as Yup from 'yup';
import { $user } from '@features/common';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const PopupEmail = () => {
  const [isSending, user] = useUnit([$isSending, $user]);
  const closePopup = () => hideExportPopup();
  const initialValues = { email: user?.user_mail ?? '' };
  const validationSchema = Yup.object().shape({
    email: Yup.string().required('Обязательное поле').email('Введите корректный email'),
  });

  return (
    <StyledPopup onClick={closePopup}>
      <StyledDataBlock onClick={(e: React.MouseEvent) => e.stopPropagation()}>
        <StyledHeader>
          <StyledHeaderTitle>
            {t('DebtorsNotifications.EmailToSendDownload') as string}
          </StyledHeaderTitle>
          <StyledCloseIcon onClick={closePopup} />
        </StyledHeader>

        <StyledItem>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={sendlistExport}
            render={() => (
              <StyledForm>
                <Field
                  name="email"
                  component={InputField}
                  label={false}
                  divider={false}
                  placeholder={t('DebtorsNotifications.EmailToSendDownload') as string}
                />

                <StyledButtonsBlock>
                  <ActionButton kind="positive" type="submit" disabled={isSending}>
                    <Check />
                    {t('DebtorsNotifications.GetDownload')}
                  </ActionButton>
                  <ActionButton onClick={closePopup}>
                    <Clear />
                    {t('Cancellation')}
                  </ActionButton>
                </StyledButtonsBlock>
              </StyledForm>
            )}
          />
        </StyledItem>
      </StyledDataBlock>
    </StyledPopup>
  );
};

const PopupSent = () => {
  const closePopup = () => hideExportPopup();

  return (
    <StyledPopup onClick={closePopup}>
      <StyledDataBlock
        narrow
        center
        onClick={(e: React.MouseEvent) => e.stopPropagation()}
      >
        <StyledHeader>
          <StyledHeaderTitle></StyledHeaderTitle>
          <StyledCloseIcon onClick={closePopup} />
        </StyledHeader>

        <StyledAlertMessage>
          <StyledCheckCircleIcon />
          <StyledSuccessMessage>
            {t('DebtorsNotifications.DataSentSuccessfully') as string}
          </StyledSuccessMessage>
        </StyledAlertMessage>
      </StyledDataBlock>
    </StyledPopup>
  );
};

export const PopupExport = () => {
  const [exportPopup, sendResult] = useUnit([$exportPopup, $sendResult]);

  if (!exportPopup) return null;

  if (sendResult) return <PopupSent />;

  return <PopupEmail />;
};
