import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { Button, IconButton, CircularProgress } from '@mui/material';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import ExportIcon from '@mui/icons-material/VerticalAlignTop';
import i18n from '@shared/config/i18n';
import {
  $isFilterOpen,
  changedFilterVisibility,
  searchForm,
  showExportPopup,
  $isDownloadPending,
} from '../../models';
import { StyledWrapperToolbar, StyledTableToolbarLeft } from './styled';

const { t } = i18n;

const TableToolbar = () => {
  const [isDownloadPending, isFilterOpen] = useUnit([$isDownloadPending, $isFilterOpen]);

  const handleChangedFilterVisibility = () => {
    changedFilterVisibility(true);
  };

  const handleDownload = () => {
    showExportPopup();
  };

  return (
    <StyledWrapperToolbar>
      <StyledTableToolbarLeft>
        <IconButton
          aria-label="filter"
          disabled={isFilterOpen}
          onClick={handleChangedFilterVisibility}
        >
          <FilterAltOutlinedIcon />
        </IconButton>

        <Control.Search name="search" form={searchForm} />
      </StyledTableToolbarLeft>
      <div style={{ display: 'flex', marginLeft: '16px' }}>
        {isDownloadPending && <CircularProgress size={34} />}
        <Button
          size="medium"
          variant="outlined"
          startIcon={<ExportIcon />}
          onClick={handleDownload}
        >
          {t('DataGrid.toolbarExport')}
        </Button>
      </div>
    </StyledWrapperToolbar>
  );
};

export { TableToolbar };
