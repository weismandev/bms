import styled from '@emotion/styled';
import { FilterButton } from '../../../../ui';

export const StyledWrapperToolbar = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 24px 0px;
`;

export const CustomFilterButton = styled(FilterButton)(() => ({
  margin: '0 4px 0 8px',
}));

export const MarginLeft = styled.span(() => ({
  marginLeft: 12,
}));

export const StyledTableToolbarLeft = styled.div`
  display: flex;
  gap: 12px;
  align-items: center;
`;
