export const convertTimeToMinutes = (time: string) => {
  const splittedTime = time.split(':');

  return Number.parseInt(splittedTime[0], 10) * 60 + Number.parseInt(splittedTime[1], 10);
};
