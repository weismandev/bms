import { Value } from '../general-interfaces';

export interface GetScheduleListPayload {
  id?: number;
  compl?: number;
  show_ag?: number;
  limit?: number;
  offset?: number;
  search?: string;
  access_group_id?: string;
  sort_by?: string;
  sort_order?: string;
}

export interface GetScheduleListResponse {
  meta: {
    total: number;
  };
  schedule: Schedule[];
}

export interface Schedule {
  created_at: number;
  days: Day[];
  end: number;
  holidays: [];
  holidays_gov_en: boolean;
  id: number;
  start: number;
  title: string;
  updated_at: number;
  access_group: Value[];
}

export interface Day {
  id: number;
  number: number;
  enable: boolean;
  isExistInterval?: boolean;
  intervals: {
    id: number;
    start: number;
    end: number;
  }[];
}
