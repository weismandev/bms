import { GridSortModel } from '@mui/x-data-grid-pro';

import { Value } from '../general-interfaces';

export interface CreateUpdateSchedulePayload {
  id?: number;
  title: string;
  start: number;
  end?: number;
  days?: {
    number: number;
    enable: boolean;
    intervals?: {
      start?: number;
      end?: number;
    }[];
  }[];
  holidays_gov_en: boolean;
  holidays?: { scud_holidays_id: number }[];
}

export interface CreateUpdateScheduleResponse {
  schedule: {
    id: number;
  };
}

export interface DeleteSchedulePayload {
  id: number;
}

export interface OpenedSchedule {
  id?: number;
  title: string;
  start: string;
  end: string;
  countDay: string | Value;
  days: {
    id: number;
    number: number;
    enable: boolean;
    isExistInterval: boolean;
    intervals: {
      id: number;
      start: string;
      end: string;
    }[];
  }[];
  holidays_gov_en: boolean;
  holidays: Value[];
  scud_holidays_id: string;
  access_groups: Value[];
}

export interface Interval {
  start?: number;
  end?: number;
}

export interface FormattedDay {
  number: number;
  enable: boolean;
  intervals?: Interval[];
}

export interface Table {
  page: number;
  per_page: number;
  search: string;
  sorting: GridSortModel;
}
