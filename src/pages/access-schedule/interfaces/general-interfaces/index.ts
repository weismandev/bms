export interface Value {
  id: number;
  title: string;
}

export interface Filters {
  accessGroups: Value[];
}
