import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { AccessGroupSelectField } from '@features/access-group-select';

import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { useStyles } from './styles';
import { Filters as FiltersInterface } from '../../interfaces';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters) as FiltersInterface;

  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={() => (
            <Form className={classes.form}>
              <Field
                name="accessGroups"
                label={t('OneInclusionInAccessGroups')}
                placeholder={t('SelectAccessGroup')}
                component={AccessGroupSelectField}
                isMulti
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
