import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Control } from '@effector-form';
import { IconButton, Button, Tooltip } from '@mui/material';
import { FilterAltOutlined, Add } from '@mui/icons-material';
import {
  $isFilterOpen,
  changedFilterVisibility,
  addClicked,
  $mode,
  $opened,
  searchForm,
} from '../../models';
import * as Styled from './styled';

export const TableToolbar: FC = () => {
  const { t } = useTranslation();
  const [isFilterOpen, mode, opened] = useUnit([$isFilterOpen, $mode, $opened]);

  const isDisabledAddClicked = mode === 'edit' && !opened.id;

  const onClickFilter = () => changedFilterVisibility(true);
  const onClickAdd = () => addClicked();

  return (
    <Styled.ToolbarWrapper>
      <Tooltip title={t('Filters')}>
        <IconButton disabled={isFilterOpen} onClick={onClickFilter}>
          <FilterAltOutlined />
        </IconButton>
      </Tooltip>
      <div>
        <Control.Search name="search" form={searchForm} />
      </div>
      <Styled.Greedy />
      <Button
        color="primary"
        variant="contained"
        onClick={onClickAdd}
        startIcon={<Add />}
        disabled={isDisabledAddClicked}
      >
        {t('AddAccessSchedule')}
      </Button>
    </Styled.ToolbarWrapper>
  );
};
