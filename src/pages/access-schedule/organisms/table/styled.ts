import { Paper } from '@mui/material';
import { styled } from '@mui/material/styles';

export const Wrapper = styled(Paper)(() => ({
  display: 'flex',
  flexDirection: 'column',
  height: '100%',
  padding: '0px 24px',
}));
