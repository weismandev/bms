import makeStyles from '@mui/styles/makeStyles';

interface Props {
  isFilterOpen: boolean;
}

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    height: '100%',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
  },
  content: {
    height: 'calc(100% - 82px)',
    padding: '0 0 24px 24px',
  },
  contentForm: {
    paddingRight: 9,
  },
  title: {
    fontWeight: 500,
    fontSize: 21,
    color: '#3B3B50',
    marginBottom: 20,
  },
  twoFields: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1092px)': {
      flexDirection: 'column',
    },
    '@media (max-width: 1385px)': {
      flexDirection: ({ isFilterOpen }: Props) => isFilterOpen && 'column',
    },
  },
  field: {
    width: '50%',
    paddingRight: 15,
    '@media (max-width: 1092px)': {
      width: '100%',
    },
    '@media (max-width: 1385px)': {
      width: ({ isFilterOpen }: Props) => isFilterOpen && '100%',
    },
  },
}));

export const styles = {
  field: {
    marginBottom: 25,
  },
  fullWidth: {
    width: '100%',
  },
};
