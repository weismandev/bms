import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { OpenedSchedule } from '../../interfaces';
import { convertTimeToMinutes } from '../../libs';

const { t } = i18n;

function startDateAfterFinishDate(this: { parent: OpenedSchedule }) {
  const isDatesDefined = Boolean(this.parent.start) && Boolean(this.parent.end);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.start)) >= Number(new Date(this.parent.end))
  ) {
    return false;
  }

  return true;
}

function startDateAfterCurrentDate(this: { parent: OpenedSchedule }) {
  const isDataExist = Boolean(this.parent.start);
  const date = new Date();

  if (isDataExist) {
    const startDate = new Date(this.parent.start);

    if (
      Number(
        new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate())
      ) < Number(new Date(date.getFullYear(), date.getMonth(), date.getDate()))
    ) {
      return false;
    }

    return true;
  }

  return false;
}

function startTimeAfterFinishTime(this: { parent: OpenedSchedule }) {
  const isExistTime = Boolean(this.parent.start) && Boolean(this.parent.end);

  if (isExistTime) {
    const startTimeToMinutes = convertTimeToMinutes(this.parent.start);
    const endTimeToMinutes = convertTimeToMinutes(this.parent.end);

    if (endTimeToMinutes > startTimeToMinutes) {
      return true;
    }

    return false;
  }

  return true;
}

export const validationSchema = Yup.object().shape({
  start: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .test(
      'start-date-after-current-date',
      t('TheStartDateMustBeNoEarlierThanTheCurrentOne'),
      startDateAfterCurrentDate
    ),
  end: Yup.string().test(
    'start_date-after-finish_date',
    t('EndDateAfterStartDate'),
    startDateAfterFinishDate
  ),
  days: Yup.array().of(
    Yup.object().shape({
      intervals: Yup.array().of(
        Yup.object().shape({
          start: Yup.string().test(
            'start-time-after-finish-time',
            t('TheStartTimeMustBeBeforeTheEndTime'),
            startTimeAfterFinishTime
          ),
          end: Yup.string().test(
            'start-time-after-finish-time',
            t('TheEndTimeMustBeLaterThanTheStartTime'),
            startTimeAfterFinishTime
          ),
        })
      ),
    })
  ),
});
