import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray, FieldProps } from 'formik';
import { useTranslation } from 'react-i18next';

import {
  Wrapper,
  DetailToolbar,
  CustomScrollbar,
  InputField,
  SelectField,
} from '@ui/index';
import { AccessGroupSelectField } from '@features/access-group-select';

import {
  $opened,
  detailSubmitted,
  changeMode,
  changedDetailVisibility,
  $mode,
  changedVisibilityDeleteModal,
  countDayOptions,
  dayEntity,
  $isFilterOpen,
} from '../../models';
import { DayCard } from '../day-card';
import { HolidaysGov } from '../holidays-gov';
import { Day, Value, OpenedSchedule } from '../../interfaces';
import { validationSchema } from './validation';
import { useStyles, styles } from './styles';

export const Detail: FC = memo(() => {
  const { t } = useTranslation();
  const opened = useStore($opened) as OpenedSchedule;
  const mode = useStore($mode);
  const isFilterOpen = useStore($isFilterOpen);

  const classes = useStyles({ isFilterOpen });

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm, setFieldValue }) => {
          const onEdit = () => changeMode('edit');
          const onClose = () => changedDetailVisibility(false);
          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };
          const onDelete = () => changedVisibilityDeleteModal(true);

          const onChangeCountDay = (value: Value) => {
            setFieldValue('countDay', value);

            if (!value) {
              setFieldValue('days', []);
              setFieldValue('holidays_gov_en', false);
              setFieldValue('holidays', []);
            }
          };

          return (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.toolbar}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <div className={classes.content}>
                <CustomScrollbar>
                  <div className={classes.contentForm}>
                    <div className={classes.field} style={styles.fullWidth}>
                      <Field
                        name="title"
                        mode={mode}
                        label={t('NameTitle')}
                        placeholder={t('EnterTheNameOfTheSchedule')}
                        component={InputField}
                      />
                    </div>
                    <div className={classes.title}>{t('AccessPeriod')}</div>
                    <div className={classes.twoFields}>
                      <div className={classes.field}>
                        <Field
                          name="start"
                          mode={mode}
                          label={t('Start')}
                          component={InputField}
                          type="date"
                          required
                        />
                      </div>
                      <div className={classes.field}>
                        <Field
                          name="end"
                          mode={mode}
                          label={t('End')}
                          component={InputField}
                          type="date"
                        />
                      </div>
                    </div>
                    <div className={classes.title}>{t('AccessSettings')}</div>
                    {mode === 'edit' && (
                      <div className={classes.field} style={styles.field}>
                        <Field
                          name="countDay"
                          label={t('NumberOfDaysToSetUp')}
                          placeholder={t('SelectNumberOfDays')}
                          component={SelectField}
                          options={countDayOptions}
                          mode={mode}
                          divider={false}
                          onChange={onChangeCountDay}
                        />
                      </div>
                    )}
                    {values.countDay && typeof values.countDay === 'object' && (
                      <>
                        <FieldArray
                          name="days"
                          render={({ push, remove }) => {
                            if (!Array.isArray(values?.days)) {
                              return null;
                            }

                            const { countDay, days } = values;

                            const countDays = days.length;
                            const selectedDays = (countDay as Value).id;

                            if (selectedDays - countDays > 0) {
                              [...Array(selectedDays - countDays)].forEach(() =>
                                push(dayEntity)
                              );
                            }

                            if (countDays - selectedDays > 0) {
                              const firstIndexRemovedDays = days.slice(
                                0,
                                selectedDays
                              ).length;

                              const lastIndex = days.length - 1;

                              days.forEach((_: unknown, index: number) => {
                                if (
                                  index >= firstIndexRemovedDays &&
                                  index <= lastIndex
                                ) {
                                  remove(index);
                                }
                              });
                            }

                            const lastIndex = days.length - 1;

                            return days.map((day: Day, index: number) => (
                              <Field
                                key={index}
                                name={`days[${index}]`}
                                render={({ field }: FieldProps) => (
                                  <DayCard
                                    day={day}
                                    mode={mode}
                                    index={index}
                                    lastIndex={lastIndex}
                                    parentName={field.name}
                                    values={field.value}
                                    setFieldValue={setFieldValue}
                                  />
                                )}
                              />
                            ));
                          }}
                        />
                        <HolidaysGov
                          setFieldValue={setFieldValue}
                          mode={mode}
                          holidays_gov_en={values.holidays_gov_en}
                          holidays={values.holidays}
                        />
                      </>
                    )}
                    {!isNew && mode === 'view' && values.access_groups.length > 0 && (
                      <Field
                        name="access_groups"
                        label={t('InclusionInAccessGroups')}
                        mode="view"
                        component={AccessGroupSelectField}
                        isMulti
                      />
                    )}
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});
