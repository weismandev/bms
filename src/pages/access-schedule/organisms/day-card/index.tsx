import { FC, useState } from 'react';
import { useStore } from 'effector-react';
import { Chip, Button, Collapse, useMediaQuery } from '@mui/material';
import { Field, FieldArray } from 'formik';
import { ExpandMore, ExpandLess, HighlightOff } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { SwitchField, IconButton, InputField, Divider } from '@ui/index';

import { Day } from '../../interfaces';
import { intervalEntity, $isFilterOpen } from '../../models';
import { useStyles as useStylesDetail, styles as stylesDetail } from '../detail/styles';
import { useStyles, styles } from './styles';

interface Props {
  day: Day;
  mode: string;
  index: number;
  lastIndex: number;
  parentName: string;
  values: Day;
  setFieldValue: (field: string, value: any) => void;
}

export const DayCard: FC<Props> = ({
  day,
  mode,
  index,
  parentName,
  values,
  setFieldValue,
  lastIndex,
}) => {
  const { t } = useTranslation();
  const isFilterOpen = useStore($isFilterOpen);

  const isFirstElement = index === 0;
  const isLastElement = index === lastIndex;

  const classes = useStyles({
    enable: day.enable,
    isFirstElement,
    isLastElement,
  });
  const classesDetail = useStylesDetail({ isFilterOpen });

  const [expanded, setExpanded] = useState(true);
  const handleExpanded = () => setExpanded(!expanded);

  const isScreenLess1092 = useMediaQuery('(max-width:1092px)');
  const isScreenLess1385 = useMediaQuery('(max-width:1385px)');
  const isScreenLess = isFilterOpen ? isScreenLess1385 : isScreenLess1092;

  const dayNumber = index + 1;

  const handleEnable = (e: { target: { checked: boolean } }) => {
    setFieldValue(`${parentName}.enable`, e.target.checked);
    setFieldValue(`${parentName}.isExistInterval`, false);
    setFieldValue(`${parentName}.intervals`, []);
  };

  const handleExistInterval = (e: { target: { checked: boolean } }) => {
    setFieldValue(`${parentName}.isExistInterval`, e.target.checked);
    setFieldValue(`${parentName}.intervals`, []);
  };

  if (mode === 'edit') {
    return (
      <div className={classes.containerWithEdit}>
        <div className={classes.header}>
          <div className={classes.checkboxes}>
            <div className={classes.enableCheckbox}>
              <Field
                component={SwitchField}
                name={`${parentName}.enable`}
                label={`${dayNumber} ${t('day')}`}
                labelPlacement="end"
                onChange={handleEnable}
                divider={false}
              />
            </div>
            <Field
              component={SwitchField}
              name={`${parentName}.isExistInterval`}
              label={t('InTheSpecifiedTimePeriod')}
              labelPlacement="end"
              onChange={handleExistInterval}
              disabled={!values.enable}
              divider={false}
            />
          </div>
          {values.isExistInterval && (
            <div className={classes.collapseButton}>
              <IconButton
                onClick={handleExpanded}
                className={classes.icon}
                style={styles.button}
                size="large"
              >
                {expanded ? (
                  <ExpandMore titleAccess={t('Collapse')} />
                ) : (
                  <ExpandLess titleAccess={t('Expand')} />
                )}
              </IconButton>
              <div />
            </div>
          )}
        </div>
        {values.isExistInterval && (
          <Collapse in={expanded} timeout="auto">
            <div className={classes.hint}>{t('AddTimePeriod')}</div>
            <FieldArray
              name={`${parentName}.intervals`}
              render={({ push, remove }) => {
                const intervals = values.intervals.map((_, idx) => {
                  const onClick = () => remove(idx);

                  if (isScreenLess) {
                    return (
                      <div key={idx}>
                        <div className={classes.deleteButton}>
                          <IconButton style={styles.button} onClick={onClick}>
                            <HighlightOff
                              titleAccess={t('remove')}
                              style={styles.deleteIcon}
                            />
                          </IconButton>
                        </div>
                        <div
                          className={classesDetail.field}
                          style={{
                            ...stylesDetail.fullWidth,
                            ...styles.paddingBottom,
                          }}
                        >
                          <Field
                            name={`${parentName}.intervals[${idx}].start`}
                            mode={mode}
                            label={t('Start')}
                            component={InputField}
                            type="time"
                            divider={false}
                          />
                        </div>
                        <div
                          className={classesDetail.field}
                          style={{
                            ...stylesDetail.fullWidth,
                            ...styles.paddingBottom,
                          }}
                        >
                          <Field
                            name={`${parentName}.intervals[${idx}].end`}
                            mode={mode}
                            label={t('End')}
                            component={InputField}
                            type="time"
                            divider={false}
                          />
                        </div>
                      </div>
                    );
                  }

                  const style =
                    idx === 0 ? { marginTop: 26, ...styles.button } : styles.button;

                  return (
                    <div
                      key={idx}
                      className={classesDetail.twoFields}
                      style={styles.paddingBottom}
                    >
                      <div className={classesDetail.field}>
                        <Field
                          name={`${parentName}.intervals[${idx}].start`}
                          mode={mode}
                          label={idx === 0 ? t('Start') : null}
                          component={InputField}
                          type="time"
                          divider={false}
                        />
                      </div>
                      <div className={classesDetail.field}>
                        <Field
                          name={`${parentName}.intervals[${idx}].end`}
                          mode={mode}
                          label={idx === 0 ? t('End') : null}
                          component={InputField}
                          type="time"
                          divider={false}
                        />
                      </div>
                      <IconButton style={style} onClick={onClick}>
                        <HighlightOff
                          titleAccess={t('remove')}
                          style={styles.deleteIcon}
                        />
                      </IconButton>
                    </div>
                  );
                });

                const onClick = () => push(intervalEntity);

                return (
                  <>
                    {intervals}
                    {mode === 'edit' && (
                      <Button color="primary" onClick={onClick}>
                        {`+ ${t('AddPeriod')}`}
                      </Button>
                    )}
                  </>
                );
              }}
            />
          </Collapse>
        )}
        {!isLastElement && <Divider gap="10px -24px 0" />}
      </div>
    );
  }

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <div className={classes.day}>
          {dayNumber} {t('day')}
        </div>
        {day.enable ? (
          <div>
            {day.intervals.map((interval) => {
              const isExistTime = Boolean(interval?.start) && Boolean(interval?.end);

              if (!isExistTime) {
                return null;
              }

              return (
                <Chip
                  key={interval.id}
                  label={`${interval.start} - ${interval.end}`}
                  classes={{ root: classes.chip }}
                />
              );
            })}
          </div>
        ) : (
          <Chip label={t('NonWorkingDay')} classes={{ root: classes.chip }} />
        )}
      </div>
    </div>
  );
};
