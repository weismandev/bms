import makeStyles from '@mui/styles/makeStyles';

interface Props {
  enable: boolean;
  isFirstElement: boolean;
  isLastElement: boolean;
}

export const useStyles = makeStyles(() => ({
  container: {
    background: ({ enable }: Props) => (enable ? '#EDF6FF' : '#E7E7EC'),
    borderRadius: 16,
    padding: '18px 24px',
    margin: '0 15px 15px 0',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  day: {
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
    marginTop: 7,
    width: '40%',
  },
  chip: {
    background: ({ enable }: Props) => (enable ? '#1BB169' : '#AAAAAA'),
    color: '#FFFFFF',
    margin: '0 4px 4px',
  },
  icon: {
    height: 25,
    width: 25,
  },
  collapseButton: {
    justifyContent: 'flex-end',
    marginTop: 5,
  },
  hint: {
    fontWeight: 400,
    fontStyle: 'italic',
    fontSize: 12,
    margin: '20px 0',
    color: '#65657B',
    opacity: 0.7,
  },
  checkboxes: {
    display: 'flex',
  },
  enableCheckbox: {
    marginRight: 25,
  },
  containerWithEdit: {
    background: ({ enable }: Props) => (enable ? '#EDF6FF' : '#F4F7FA'),
    borderRadius: ({ isFirstElement, isLastElement }: Props) =>
      isFirstElement ? '8px 8px 0 0' : isLastElement ? '0 0 8px 8px' : 0,
    padding: ({ isLastElement }: Props) =>
      isLastElement ? '18px 24px 10px' : '18px 24px 0',
    marginBottom: ({ isLastElement }: Props) => (isLastElement ? 15 : 0),
    marginRight: 15,
  },
  deleteButton: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

export const styles = {
  button: {
    borderRadius: 30,
  },
  deleteIcon: {
    color: '#EB5757',
  },
  paddingBottom: {
    paddingBottom: 10,
  },
};
