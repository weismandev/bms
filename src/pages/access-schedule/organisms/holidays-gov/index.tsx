import { FC } from 'react';
import { Field } from 'formik';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { SwitchField } from '@ui/index';

import { GovHolidaysSelectField } from '../gov-holidays-select';
import { useStyles as useStylesDetail, styles as stylesDetail } from '../detail/styles';
import { $isFilterOpen } from '../../models';
import { Value } from '../../interfaces';
import { useStyles } from './styles';

interface Props {
  setFieldValue: (field: string, value: any) => void;
  mode: string;
  holidays_gov_en: boolean;
  holidays: Value[];
}

export const HolidaysGov: FC<Props> = ({
  setFieldValue,
  mode,
  holidays_gov_en,
  holidays,
}) => {
  const { t } = useTranslation();
  const isFilterOpen = useStore($isFilterOpen);

  const classes = useStyles();
  const classesDetail = useStylesDetail({ isFilterOpen });

  const handleEnable = (e: { target: { checked: boolean } }) => {
    setFieldValue('holidays_gov_en', e.target.checked);
    setFieldValue('holidays', []);
  };

  if (mode === 'edit') {
    return (
      <div className={classes.container}>
        <Field
          component={SwitchField}
          name="holidays_gov_en"
          label={t('ConsiderPublicHolidays')}
          labelPlacement="end"
          onChange={handleEnable}
          divider={false}
        />
        {holidays_gov_en && (
          <>
            <div className={classes.hint}>{t('AccessOnSelectedDaysWillBeDenied')}</div>
            <div className={classesDetail.field} style={stylesDetail.fullWidth}>
              <Field
                component={GovHolidaysSelectField}
                name="holidays"
                label={t('ListOfHolidays')}
                placeholder={t('SelectHolidays')}
                divider={false}
                isMulti
              />
            </div>
          </>
        )}
      </div>
    );
  }

  if (mode === 'view' && holidays.length === 0) {
    return null;
  }

  return (
    <div className={classes.container}>
      <div className={classes.holidaysTitle}>
        {`${t('ConsiderPublicHolidays')} (${t(
          'AccessOnSelectedDaysWillBeDenied'
        ).toLowerCase()})`}
      </div>
      {holidays.map((item) => (
        <Chip key={item.id} label={item.title} classes={{ root: classes.chip }} />
      ))}
    </div>
  );
};
