import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  container: {
    background: '#EDF6FF',
    borderRadius: 16,
    padding: '18px 24px',
    margin: '0 15px 15px 0',
  },
  hint: {
    fontWeight: 400,
    fontStyle: 'italic',
    fontSize: 12,
    margin: '20px 0',
    color: '#65657B',
    opacity: 0.7,
  },
  holidaysTitle: {
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
    marginBottom: 10,
  },
  chip: {
    background: '#1BB169',
    color: '#FFFFFF',
    margin: '0 4px 4px',
  },
}));
