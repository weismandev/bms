import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, $isLoading, fxGetList } from '../../models/gov-holidays-select';
import { Value } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const GovHolidaysSelect: FC<object> = (props) => {
  useEffect(() => {
    fxGetList();
  }, []);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const GovHolidaysSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return <SelectField component={<GovHolidaysSelect />} onChange={onChange} {...props} />;
};
