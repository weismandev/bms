import { format } from 'date-fns';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { Schedule } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('NameTitle') ?? '',
    width: 350,
    sortable: true,
    hideable: false,
  },
  {
    field: 'access_groups',
    headerName: t('navMenu.security.accessGroups') ?? '',
    width: 230,
    sortable: false,
    hideable: true,
  },
  {
    field: 'created_at',
    headerName: t('DateOfCreation') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
  {
    field: 'updated_at',
    headerName: t('DateOfChange') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
];

export const {
  $tableParams,
  $pagination,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  sortChanged,
  searchChanged,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
  searchForm,
  $selectRow
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

const formatData = (schedule: Schedule[]) =>
  schedule.map((item) => {
    const accessGroups = Array.isArray(item?.access_group)
      ? item.access_group.length === 1
        ? item.access_group[0]?.title || '-'
        : item.access_group.length
      : '-';

    return {
      id: item.id,
      title: item?.title || '-',
      access_groups: accessGroups,
      created_at: item?.created_at
        ? format(new Date(item.created_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
      updated_at: item?.updated_at
        ? format(new Date(item.updated_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
    };
  });

export const $count = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ schedule }) =>
  schedule && schedule.length > 0 ? formatData(schedule) : []
);
