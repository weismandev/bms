import { sample } from 'effector';
import { signout } from '@features/common';
import {
  detailClosed,
  addClicked,
  fxCreateUpdateSchedule,
  fxDeleteSchedule,
  $entityId,
} from '../detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

$openRow
  .on(fxCreateUpdateSchedule.done, (state, { params, result }) =>
    !params.id && result.schedule.id ? result.schedule.id : state
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked, fxDeleteSchedule.done]);

sample({
  clock: pageMounted,
  source: { id: $entityId, openRow: $openRow },
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => Number.parseInt(id as string, 10),
  target: $openRow,
});
