import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  accessGroups: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
