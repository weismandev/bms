import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetScheduleListPayload, GetScheduleListResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetScheduleList = createEffect<
  GetScheduleListPayload,
  GetScheduleListResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetScheduleListResponse>({
  meta: {
    total: 0,
  },
  schedule: [],
});
export const $isLoadingList = createStore<boolean>(false);
