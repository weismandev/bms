import { merge, sample } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { scheduleApi } from '../../api';
import { GetScheduleListPayload, Value, Filters, Table } from '../../interfaces';
import {
  fxGetSchedule,
  fxCreateUpdateSchedule,
  fxDeleteSchedule,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  fxGetScheduleList,
  $error,
  $isLoading,
  $isErrorDialogOpen,
  $rawData,
  $isLoadingList,
} from './page.model';

fxGetScheduleList.use(scheduleApi.geScheduleList);
fxGetSchedule.use(scheduleApi.geScheduleList);
fxCreateUpdateSchedule.use(scheduleApi.createUpdateSchedule);
fxDeleteSchedule.use(scheduleApi.deleteSchedule);

const errorOccured = merge([
  fxGetScheduleList.fail,
  fxGetSchedule.fail,
  fxCreateUpdateSchedule.fail,
  fxDeleteSchedule.fail,
]);

$isLoading
  .on(
    pending({
      effects: [fxGetSchedule, fxCreateUpdateSchedule, fxDeleteSchedule],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingList
  .on(fxGetScheduleList.pending, (_, loading) => loading)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: [
    pageMounted,
    $tableParams,
    fxCreateUpdateSchedule.done,
    fxDeleteSchedule.done,
    $filters,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
  },
  fn: ({ table, filters }: { table: Table; filters: Filters }) => {
    const payload: GetScheduleListPayload = {
      compl: 1,
      show_ag: 1,
      limit: table.per_page,
      offset: (table.page - 1) * table.per_page,
      search: table.search,
    };

    if (table.sorting.length > 0) {
      payload.sort_by = table.sorting[0].field || 'id';
      payload.sort_order = table.sorting[0].sort || 'desc';
    }

    if (filters.accessGroups.length > 0) {
      payload.access_group_id = filters.accessGroups
        .map((item: Value) => item.id)
        .join(',');
    }

    return payload;
  },
  target: fxGetScheduleList,
});

$rawData
  .on(fxGetScheduleList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);
