import { sample } from 'effector';
import { format } from 'date-fns';
import { delay } from 'patronum';

import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';

import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from '../table';
import {
  Day,
  OpenedSchedule,
  CreateUpdateSchedulePayload,
  Interval,
  FormattedDay,
} from '../../interfaces';
import { convertTimeToMinutes } from '../../libs';
import {
  openViaUrl,
  $isDetailOpen,
  fxGetSchedule,
  $opened,
  newEntity,
  entityApi,
  $mode,
  fxCreateUpdateSchedule,
  deleteSchedule,
  fxDeleteSchedule,
  $entityId,
  changedDetailVisibility,
  $path,
} from './detail.model';

$isDetailOpen
  .on(fxGetSchedule.done, () => true)
  .off(openViaUrl)
  .reset([signout, pageUnmounted, fxDeleteSchedule.done]);

sample({
  clock: openViaUrl,
  fn: (id) => ({ id, compl: 1, show_ag: 1 }),
  target: fxGetSchedule,
});

/** Призакрытии деталей, сбрасываю openRow */
sample({
  clock: $isDetailOpen,
  filter: (isDetailOpen) => !isDetailOpen,
  fn: () => null,
  target: $openRow,
});

const getTimeFromMinutes = (min: number) => {
  if (!min) {
    return null;
  }

  const hours = Math.trunc(min / 60);
  const minutes = min % 60;

  const h = hours.toString().length === 1 ? `0${hours}` : hours;
  const m = minutes.toString().length === 1 ? `0${minutes}` : minutes;

  return `${h}:${m}`;
};

const formatDays = (days: Day[]) =>
  days.map((day) => ({
    id: day.id,
    number: day?.number || 0,
    enable: Boolean(day?.enable),
    isExistInterval: Boolean(Array.isArray(day?.intervals) && day.intervals.length > 0),
    intervals:
      Array.isArray(day?.intervals) && day.intervals.length > 0
        ? day.intervals.map((interval) => ({
            id: interval.id,
            start: getTimeFromMinutes(interval.start),
            end: getTimeFromMinutes(interval.end),
          }))
        : [],
  }));

$opened
  .on(fxGetSchedule.done, (_, { result }) => {
    if (result?.schedule && result?.schedule[0]) {
      const data = result.schedule[0];

      const days =
        Array.isArray(data?.days) && data.days.length > 0 ? formatDays(data.days) : [];

      return {
        id: data.id,
        title: data?.title || '',
        start: data?.start ? format(new Date(data.start * 1000), 'yyyy-MM-dd') : '',
        end: data?.end ? format(new Date(data.end * 1000), 'yyyy-MM-dd') : '',
        countDay:
          Array.isArray(data?.days) && data.days.length > 0
            ? { id: data.days.length, title: data.days.length }
            : '',
        days,
        holidays_gov_en: Boolean(data?.holidays_gov_en),
        holidays:
          Array.isArray(data?.holidays) && data.holidays.length > 0 ? data.holidays : [],
        access_groups: Array.isArray(data?.access_group)
          ? data.access_group.map((item) => ({
              id: item.id,
              title: item.title,
            }))
          : [],
      };
    }

    return newEntity;
  })
  .reset([signout, pageUnmounted, fxGetSchedule]);

const formatData = (data: OpenedSchedule) => {
  const payload: CreateUpdateSchedulePayload = {
    title: data.title,
    start: Number(new Date(data.start)) / 1000,
    holidays_gov_en: data.holidays_gov_en,
  };

  if (data.end.length > 0) {
    payload.end = Number(new Date(data.end)) / 1000;
  }

  if (data.days.length > 0) {
    const days = data.days.map((day, index) => {
      const formattedDay: FormattedDay = {
        number: index + 1,
        enable: day.enable,
      };

      if (day.intervals.length > 0) {
        const intervals = day.intervals.map((interval) => {
          const formattedInterval: Interval = {};

          if (interval.start.length > 0) {
            formattedInterval.start = convertTimeToMinutes(interval.start);
          }

          if (interval.end.length > 0) {
            formattedInterval.end = convertTimeToMinutes(interval.end);
          }

          return formattedInterval;
        });

        formattedDay.intervals = intervals;
      }

      return formattedDay;
    });

    payload.days = days;
  }

  if (data.holidays.length > 0) {
    const holidays = data.holidays.map((item) => ({
      scud_holidays_id: item.id,
    }));

    payload.holidays = holidays;
  }

  return payload;
};

sample({
  clock: entityApi.create,
  fn: (data: OpenedSchedule) => formatData(data),
  target: fxCreateUpdateSchedule,
});

sample({
  clock: entityApi.update,
  fn: (data: OpenedSchedule) => ({ id: data.id, ...formatData(data) }),
  target: fxCreateUpdateSchedule,
});

$mode.reset([signout, pageUnmounted, fxCreateUpdateSchedule.done]);

sample({
  clock: fxCreateUpdateSchedule.done,
  filter: ({
    result: {
      schedule: { id },
    },
  }) => Boolean(id),
  fn: ({
    result: {
      schedule: { id },
    },
  }: {
    result: { schedule: { id: number } };
  }) => ({
    id,
    compl: 1,
  }),
  target: fxGetSchedule,
});

sample({
  clock: deleteSchedule,
  source: $opened,
  fn: ({ id }: { id: number }) => ({ id }),
  target: fxDeleteSchedule,
});

sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: entityId,
    compl: 1,
    show_ag: 1,
  }),
  target: fxGetSchedule,
});

const delayedRedirect = delay({ source: fxGetSchedule.fail, timeout: 1000 });

sample({
  clock: delayedRedirect,
  fn: () => '.',
  target: historyPush,
});

sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

sample({
  clock: fxCreateUpdateSchedule.done,
  source: $path,
  filter: (
    _,
    {
      params,
      result: {
        schedule: { id },
      },
    }
  ) => {
    if (!params?.id && id) {
      return true;
    }

    return false;
  },
  fn: (
    path,
    {
      result: {
        schedule: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

sample({
  clock: fxDeleteSchedule.done,
  source: $path,
  filter: (_, params) => Boolean(params),
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);
