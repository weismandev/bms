import { createEvent, createEffect } from 'effector';
import { format } from 'date-fns';

import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';

import {
  GetScheduleListPayload,
  GetScheduleListResponse,
  CreateUpdateSchedulePayload,
  CreateUpdateScheduleResponse,
  DeleteSchedulePayload,
} from '../../interfaces';

export const newEntity = {
  title: '',
  start: format(new Date(), 'yyyy-MM-dd'),
  end: '',
  countDay: '',
  days: [],
  holidays_gov_en: false,
  holidays: [],
  access_groups: [],
};

export const dayEntity = {
  id: null,
  number: 0,
  enable: false,
  isExistInterval: false,
  intervals: [],
};

export const intervalEntity = {
  id: null,
  start: '',
  end: '',
};

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2] || null);

export const deleteSchedule = createEvent<void>();

export const fxGetSchedule = createEffect<
  GetScheduleListPayload,
  GetScheduleListResponse,
  Error
>();
export const fxCreateUpdateSchedule = createEffect<
  CreateUpdateSchedulePayload,
  CreateUpdateScheduleResponse,
  Error
>();
export const fxDeleteSchedule = createEffect<DeleteSchedulePayload, object, Error>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
