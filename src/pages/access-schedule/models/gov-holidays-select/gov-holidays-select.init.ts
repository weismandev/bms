import { signout } from '@features/common';

import { scheduleApi } from '../../api';
import { fxGetList, $data, $isLoading } from './gov-holidays-select.model';

fxGetList.use(scheduleApi.getGovHolidays);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.holdiays)) {
      return [];
    }

    return result.holdiays.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
