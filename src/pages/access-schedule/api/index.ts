import { api } from '@api/api2';

import {
  GetScheduleListPayload,
  CreateUpdateSchedulePayload,
  DeleteSchedulePayload,
} from '../interfaces';

const geScheduleList = (payload: GetScheduleListPayload) =>
  api.no_vers('get', 'scud/get-schedule', payload);
const createUpdateSchedule = (payload: CreateUpdateSchedulePayload) =>
  api.no_vers('post', 'scud/update-schedule', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const deleteSchedule = (payload: DeleteSchedulePayload) =>
  api.no_vers('post', 'scud/remove-schedule', payload);
const getGovHolidays = () => api.no_vers('get', 'scud/get-public-holidays');

export const scheduleApi = {
  geScheduleList,
  createUpdateSchedule,
  deleteSchedule,
  getGovHolidays,
};
