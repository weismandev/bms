import { FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { ThemeAdapter } from '@shared/theme/adapter';

import { HaveSectionAccess } from '@features/common';
import {
  FilterMainDetailLayout,
  ErrorMessage,
  Loader,
  DeleteConfirmDialog,
} from '@ui/index';

import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isLoading,
  $isFilterOpen,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedVisibilityDeleteModal,
  deleteGroup,
} from '../models';
import { Table, Filters, Detail } from '../organisms';

const AccessGroupsPage: FC = () => {
  const { t } = useTranslation();
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changedVisibilityDeleteModal(false);

  return (
    <ThemeAdapter>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <DeleteConfirmDialog
        header={t('DeletingAGroup')}
        content={t('ConfirmGroupDeletion')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteGroup}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ filterWidth: '370px', detailWidth: 'minmax(370px, 50%)' }}
      />
    </ThemeAdapter>
  );
};

const RestrictedAccessGroupsPage: FC = () => {
  return (
    <HaveSectionAccess>
      <AccessGroupsPage />
    </HaveSectionAccess>
  );
};

export { RestrictedAccessGroupsPage as AccessGroupsPage };
