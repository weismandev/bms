import { signout } from '@features/common';

import { accessGroupsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './schedule-select.model';

fxGetList.use(accessGroupsApi.getSchedules);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.schedule)) {
      return [];
    }

    return result.schedule.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
