import { format } from 'date-fns';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { Group, Compl } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('NameTitle') ?? '',
    width: 350,
    sortable: true,
    hideable: false,
  },
  {
    field: 'schedule',
    headerName: t('ScheduleTitle') ?? '',
    width: 160,
    sortable: false,
    hideable: true,
  },
  {
    field: 'level',
    headerName: t('AccessLevel') ?? '',
    width: 350,
    sortable: false,
    hideable: true,
  },
  {
    field: 'user_group',
    headerName: t('UserGroup') ?? '',
    width: 350,
    sortable: false,
    hideable: true,
  },
  {
    field: 'users_count',
    headerName: t('UsersNo') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
  {
    field: 'levels_count',
    headerName: t('AccessPoints') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
  {
    field: 'created_at',
    headerName: t('DateOfCreation') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
  {
    field: 'updated_at',
    headerName: t('DateOfChange') ?? '',
    width: 130,
    sortable: true,
    hideable: true,
  },
];

export const {
  $tableParams,
  $pagination,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $search,
  $openRow,
  sortChanged,
  searchForm,
  $pinnedColumns,
  changePinnedColumns,
  openRowChanged,
  $sorting,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({
  columns,
  paginationModel: {
    page: 0,
    pageSize: 25,
  },
});

export const $count = $rawData.map(({ meta }) => meta?.total || 0);

const formatOtherData = (compl: Compl[]) => ({
  schedule:
    compl.length === 1
      ? compl[0]?.schedules
        ? compl[0].schedules.length === 1
          ? compl[0].schedules[0]?.title || '-'
          : compl[0].schedules.filter((item) => Boolean(item?.title)).length
        : '-'
      : compl.reduce((acc: number, item) => {
          const scheduleCount =
            item.schedules.filter((schedule) => Boolean(schedule?.id))?.length || 0;

          return acc + scheduleCount;
        }, 0),
  level:
    compl.length === 1
      ? compl[0]?.levels
        ? compl[0].levels.length === 1
          ? compl[0].levels[0]?.title || '-'
          : compl[0].levels.filter((item) => Boolean(item?.title)).length
        : '-'
      : compl.reduce((acc: number, item) => {
          const levelCount =
            item.levels.filter((level) => Boolean(level?.id))?.length || 0;

          return acc + levelCount;
        }, 0),
  user_group:
    compl.length === 1
      ? compl[0]?.user_groups
        ? compl[0].user_groups.length === 1
          ? compl[0].user_groups[0]?.title || '-'
          : compl[0].user_groups.filter((item) => Boolean(item?.title)).length
        : '-'
      : compl.reduce((acc: number, item) => {
          const usersGroupsCount =
            item.user_groups.filter((user_group) => Boolean(user_group?.id))?.length || 0;

          return acc + usersGroupsCount;
        }, 0),
});

const formatData = (groups: Group[]) =>
  groups.map((group) => {
    const baseData = {
      id: group.id,
      title: group?.title || '-',
      users_count: group?.users_count || 0,
      levels_count: group?.levels_count || 0,
      created_at: group?.created_at
        ? format(new Date(group.created_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
      updated_at: group?.updated_at
        ? format(new Date(group.updated_at * 1000), 'dd.MM.yyyy HH:mm')
        : '',
    };

    const otherData =
      Boolean(group?.compl) && group.compl.length > 0
        ? formatOtherData(group.compl)
        : { schedule: '-', level: '-', user_group: '-' };

    return otherData ? { ...baseData, ...otherData } : baseData;
  });

export const $tableData = $rawData.map(({ groups }) =>
  groups && groups.length > 0 ? formatData(groups) : []
);
