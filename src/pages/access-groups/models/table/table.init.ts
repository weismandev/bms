import { sample } from 'effector';
import { signout } from '@features/common';
import {
  detailClosed,
  addClicked,
  fxCreateUpdateAccessGroup,
  fxDeleteGroup,
  $entityId,
} from '../detail/detail.model';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from './table.model';

$openRow
  .on(fxCreateUpdateAccessGroup.done, (state, { params, result }) =>
    !params.id && result.groups.id ? Number.parseInt(result.groups.id, 10) : state
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked, fxDeleteGroup.done]);

sample({
  clock: pageMounted,
  source: { id: $entityId, openRow: $openRow },
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => Number.parseInt(id as string, 10),
  target: $openRow,
});
