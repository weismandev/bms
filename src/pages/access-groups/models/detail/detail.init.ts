import { sample } from 'effector';
import { delay } from 'patronum';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { Compl, AccessGroup, Opened, SubGroup } from '../../interfaces';
import { pageUnmounted, pageMounted } from '../page/page.model';
import { $openRow } from '../table';
import {
  openViaUrl,
  $isDetailOpen,
  fxGetAccessGroup,
  $opened,
  newEntity,
  fxCreateUpdateAccessGroup,
  entityApi,
  fxUpdateSubGroup,
  $mode,
  fxCreateSubGroup,
  fxDeleteGroup,
  deleteGroup,
  $entityId,
  changedDetailVisibility,
  $path,
  fxGetSubDeleteGroupEach,
  fxUpdateSubGroupEach,
  fxCreateSubGroupEach,
} from './detail.model';

$isDetailOpen
  .on(fxGetAccessGroup.done, () => true)
  .off(openViaUrl)
  .reset([signout, pageUnmounted, fxDeleteGroup.done]);

sample({
  clock: openViaUrl,
  fn: (id) => ({ id, inherit_compl: 1, show_users_count: 1, show_levels_count: 1 }),
  target: fxGetAccessGroup,
});

/** При закрытии деталей, сбрасываю openRow */
sample({
  clock: $isDetailOpen,
  filter: (isDetailOpen) => !isDetailOpen,
  fn: () => null,
  target: $openRow,
});

const formatSubGroup = (group: Compl[]) =>
  group.map((item) => ({
    id: item.id,
    schedule: item.schedules || [],
    level: item.levels || [],
    user_group: item.user_groups || [],
    exclude_user_group: item.exclude_user_groups || [],
  }));

$opened
  .on(fxGetAccessGroup.done, (_, { result }) => {
    if (result?.groups && result?.groups[0]) {
      const data = result.groups[0];
      const subGroups =
        data?.compl && data?.compl?.length > 0 ? formatSubGroup(data.compl) : [];

      return {
        id: data.id,
        title: data?.title || '',
        users: data?.users_count || 0,
        levels: data?.levels_count || 0,
        subGroups,
      };
    }

    return newEntity;
  })
  .reset([signout, pageUnmounted, fxGetAccessGroup]);

const formatData = (data: Opened) => {
  const subGroups = data.subGroups.map((group) => ({
    id: group.id,
    schedule_id: group.schedule.map((item) => item.id).join(','),
    level_id: group.level.map((item) => item.id).join(','),
    user_group_id: group.user_group.map((item) => item.id).join(','),
    exclude_user_group:
      group.exclude_user_group.length > 0
        ? group.exclude_user_group.map((item) => item.id).join(',')
        : '',
  }));

  return {
    title: data.title,
    subGroups,
  };
};

sample({
  clock: entityApi.create,
  fn: (data: Opened) => formatData(data),
  target: fxCreateUpdateAccessGroup,
});

sample({
  clock: entityApi.update,
  fn: (data: Opened) => ({ id: data.id, ...formatData(data) }),
  target: fxCreateUpdateAccessGroup,
});

const getDeletedSubGroups = (opened: Opened, subGroups: SubGroup[]) =>
  opened.subGroups.filter(
    (item: AccessGroup) => !subGroups.some((element) => element.id === item.id)
  );

sample({
  clock: fxCreateUpdateAccessGroup.done,
  source: $opened,
  filter: (opened, { params, result }) => {
    const groupId = result.groups.id;
    const { subGroups } = params;

    const deletedSubGroups = getDeletedSubGroups(opened as Opened, subGroups);

    return Boolean(deletedSubGroups.length > 0 && groupId);
  },
  fn: (opened, { params, result }) => {
    const groupId = result.groups.id;
    const { subGroups } = params;

    const deletedSubGroups = getDeletedSubGroups(opened as Opened, subGroups);

    return deletedSubGroups.reduce((acc, group) => {
      if (!group?.id) {
        return acc;
      }

      return [...acc, { group_id: groupId, id: group.id }];
    }, []);
  },
  target: fxGetSubDeleteGroupEach,
});

sample({
  clock: fxCreateUpdateAccessGroup.done,
  filter: ({ result }) => Boolean(result.groups.id),
  fn: ({ params, result }) => {
    const groupId = result.groups.id;
    const { subGroups } = params;

    const lastIndex = subGroups.length - 1;

    return subGroups.reduce((acc, group, index) => {
      if (!group?.id) {
        return acc;
      }

      return [
        ...acc,
        {
          group_id: groupId,
          ...group,
          isLastGroup: lastIndex === index,
        },
      ];
    }, []);
  },
  target: fxUpdateSubGroupEach,
});

sample({
  clock: fxCreateUpdateAccessGroup.done,
  filter: ({ result }) => Boolean(result.groups.id),
  fn: ({ params, result }) => {
    const groupId = result.groups.id;
    const { subGroups } = params;

    const lastIndex = subGroups.length - 1;

    return subGroups.reduce((acc, group, index) => {
      if (group?.id) {
        return acc;
      }

      return [
        ...acc,
        {
          group_id: groupId,
          ...group,
          isLastGroup: lastIndex === index,
        },
      ];
    }, []);
  },
  target: fxCreateSubGroupEach,
});

$mode
  .on(fxCreateSubGroup.done, (state, { params: { isLastGroup } }) =>
    isLastGroup ? 'view' : state
  )
  .on(fxUpdateSubGroup.done, (state, { params: { isLastGroup } }) =>
    isLastGroup ? 'view' : state
  )
  .reset([signout, pageUnmounted]);

sample({
  clock: fxCreateSubGroup.done,
  filter: ({ params: { isLastGroup } }) => isLastGroup,
  fn: ({ params: { group_id } }: { params: { group_id: number } }) => ({
    id: group_id,
    inherit_compl: 1,
    show_users_count: 1,
    show_levels_count: 1,
  }),
  target: fxGetAccessGroup,
});

sample({
  clock: fxUpdateSubGroup.done,
  filter: ({ params: { isLastGroup } }) => isLastGroup,
  fn: ({ params: { group_id } }: { params: { group_id: number } }) => ({
    id: group_id,
    inherit_compl: 1,
    show_users_count: 1,
    show_levels_count: 1,
  }),
  target: fxGetAccessGroup,
});

sample({
  clock: deleteGroup,
  source: $opened,
  fn: ({ id }: { id: number }) => ({ id }),
  target: fxDeleteGroup,
});

sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({
    id: entityId,
    inherit_compl: 1,
    show_users_count: 1,
    show_levels_count: 1,
  }),
  target: fxGetAccessGroup,
});

const delayedRedirect = delay({ source: fxGetAccessGroup.fail, timeout: 1000 });

sample({
  clock: delayedRedirect,
  fn: () => '.',
  target: historyPush,
});

sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

sample({
  clock: fxCreateUpdateAccessGroup.done,
  source: $path,
  filter: (
    _,
    {
      params,
      result: {
        groups: { id },
      },
    }
  ) => {
    if (!params?.id && id) {
      return true;
    }

    return false;
  },
  fn: (
    path,
    {
      result: {
        groups: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

sample({
  clock: fxDeleteGroup.done,
  source: $path,
  filter: (_, params) => Boolean(params),
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);
