import { createEffect, createEvent, attach } from 'effector';

import { createDetailBag } from '@tools/factories';
import { $pathname } from '@features/common';

import {
  GetAccessGroupListPayload,
  GetAccessGroupListResponse,
  CreateUpdateAccessGroupPayload,
  CreateUpdateSubGroupPayload,
  CreateUpdateAccessGroupResponse,
  DeleteGroupPayload,
  DeleteSubGroupPayload,
} from '../../interfaces';

export const subGroupEntity = {
  schedule: [],
  level: [],
  user_group: [],
  exclude_user_group: [],
};

export const newEntity = {
  title: '',
  subGroups: [subGroupEntity],
};

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map(
  (pathname: string) => pathname.split('/')[2] || null
);

export const deleteGroup = createEvent<void>();

export const fxGetAccessGroup = createEffect<
  GetAccessGroupListPayload,
  GetAccessGroupListResponse,
  Error
>();
export const fxCreateUpdateAccessGroup = createEffect<
  CreateUpdateAccessGroupPayload,
  CreateUpdateAccessGroupResponse,
  Error
>();
export const fxCreateSubGroup = createEffect<
  CreateUpdateSubGroupPayload,
  object,
  Error
>();
export const fxCreateSubGroupEach = attach({
  async effect(_: unknown, params: CreateUpdateSubGroupPayload[]) {
    await Promise.all(
      params.forEach((item) => {
        fxCreateSubGroup(item);
      })
    );
  },
});
export const fxUpdateSubGroup = createEffect<
  CreateUpdateSubGroupPayload,
  object,
  Error
>();
export const fxUpdateSubGroupEach = attach({
  async effect(_: unknown, params: CreateUpdateSubGroupPayload[]) {
    await Promise.all(
      params.forEach((item) => {
        fxUpdateSubGroup(item);
      })
    );
  },
});
export const fxDeleteGroup = createEffect<DeleteGroupPayload, object, Error>();
export const fxDeleteSubGroup = createEffect<DeleteSubGroupPayload, object, Error>();
export const fxGetSubDeleteGroupEach = attach({
  async effect(_: unknown, params: DeleteSubGroupPayload[]) {
    await Promise.all(
      params.forEach((item) => {
        fxDeleteSubGroup(item);
      })
    );
  },
});

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
