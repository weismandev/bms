import { createEffect, createStore } from 'effector';

export interface Data {
  id: number;
  title: string;
}

export const fxGetList = createEffect<void, { groups: Data }, Error>();

export const $data = createStore<Data[]>([]);
export const $isLoading = createStore<boolean>(false);
