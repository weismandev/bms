import { signout } from '@features/common';

import { accessGroupsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './level-select.model';

fxGetList.use(accessGroupsApi.getLevels);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.levels)) {
      return [];
    }

    return result.levels.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
