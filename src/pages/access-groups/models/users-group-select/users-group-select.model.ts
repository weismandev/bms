import { createEffect, createStore } from 'effector';

import { Value } from '../../interfaces';

export interface Data {
  id: number;
  title: string;
  exclude_user_group: Value[];
}

export const fxGetList = createEffect<void, { groups: Data }, Error>();

export const $data = createStore<Data[]>([]);
export const $isLoading = createStore<boolean>(false);
