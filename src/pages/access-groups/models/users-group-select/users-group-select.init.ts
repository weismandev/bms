import { signout } from '@features/common';

import { accessGroupsApi } from '../../api';
import { fxGetList, $data, $isLoading, Data } from './users-group-select.model';

fxGetList.use(accessGroupsApi.getUsersGroup);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.groups)) {
      return [];
    }

    return result.groups.map((item: Data) => ({
      id: item.id,
      title: item.title,
      exclude_user_group: item.exclude_user_group,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
