import { merge, sample } from 'effector';
import { pending } from 'patronum/pending';
import { GridSortModel } from '@mui/x-data-grid-pro';
import { signout } from '@features/common';
import { accessGroupsApi } from '../../api';
import { GetAccessGroupListPayload, Value, Filters } from '../../interfaces';
import {
  fxGetAccessGroup,
  fxCreateUpdateAccessGroup,
  fxCreateSubGroup,
  fxUpdateSubGroup,
  fxDeleteGroup,
  fxDeleteSubGroup,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  fxGetAccessGroupList,
  $error,
  $isLoading,
  $isErrorDialogOpen,
  $rawData,
  $isLoadingList,
} from './page.model';

interface Table {
  page: number;
  per_page: number;
  search: string;
  sorting: GridSortModel;
}

fxGetAccessGroupList.use(accessGroupsApi.getAccessGroupList);
fxGetAccessGroup.use(accessGroupsApi.getAccessGroupList);
fxCreateUpdateAccessGroup.use(accessGroupsApi.createUpdateAccessGroup);
fxCreateSubGroup.use(accessGroupsApi.createSubGroup);
fxUpdateSubGroup.use(accessGroupsApi.updateSubGroup);
fxDeleteGroup.use(accessGroupsApi.deleteGroup);
fxDeleteSubGroup.use(accessGroupsApi.deleteSubGroup);

const errorOccured = merge([
  fxGetAccessGroupList.fail,
  fxGetAccessGroup.fail,
  fxCreateUpdateAccessGroup.fail,
  fxCreateSubGroup.fail,
  fxUpdateSubGroup.fail,
  fxDeleteGroup.fail,
  fxDeleteSubGroup.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetAccessGroup,
        fxCreateUpdateAccessGroup,
        fxCreateSubGroup,
        fxUpdateSubGroup,
        fxDeleteGroup,
        fxDeleteSubGroup,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingList
  .on(fxGetAccessGroupList.pending, (_, loading) => loading)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

const formatPayload = (table: Table, filters: Filters) => {
  const payload: GetAccessGroupListPayload = {
    inherit_compl: 1,
    limit: table.per_page,
    offset: (table.page - 1) * table.per_page,
    search: table.search,
    show_users_count: 1,
    show_levels_count: 1,
  };

  if (table.sorting.length > 0) {
    payload.sort_by = table.sorting[0].field || 'id';
    payload.sort_order = table.sorting[0].sort || 'desc';
  }

  if (filters.schedules.length > 0) {
    payload.schedule_id = filters.schedules.map((item: Value) => item.id).join(',');
  }

  if (filters.levels.length > 0) {
    payload.level_id = filters.levels.map((item: Value) => item.id).join(',');
  }

  if (filters.users_group.length > 0) {
    payload.usergroup_id = filters.users_group.map((item: Value) => item.id).join(',');
  }

  return payload;
};

sample({
  clock: [pageMounted, $tableParams, $filters, fxDeleteGroup.done],
  source: {
    table: $tableParams,
    filters: $filters,
  },
  fn: ({ table, filters }: { table: Table; filters: Filters }) =>
    formatPayload(table, filters),
  target: fxGetAccessGroupList,
});

$rawData
  .on(fxGetAccessGroupList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

sample({
  clock: fxCreateSubGroup.done,
  source: { table: $tableParams, filters: $filters },
  filter: (_, { params: { isLastGroup } }) => isLastGroup,
  fn: ({ table, filters }: { table: Table; filters: Filters }) =>
    formatPayload(table, filters),
  target: fxGetAccessGroupList,
});

sample({
  clock: fxUpdateSubGroup.done,
  source: { table: $tableParams, filters: $filters },
  filter: (_, { params: { isLastGroup } }) => isLastGroup,
  fn: ({ table, filters }: { table: Table; filters: Filters }) =>
    formatPayload(table, filters),
  target: fxGetAccessGroupList,
});
