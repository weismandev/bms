import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetAccessGroupListPayload, GetAccessGroupListResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetAccessGroupList = createEffect<
  GetAccessGroupListPayload,
  GetAccessGroupListResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetAccessGroupListResponse>({
  meta: {
    total: 0,
  },
  groups: [],
});
export const $isLoadingList = createStore<boolean>(false);
