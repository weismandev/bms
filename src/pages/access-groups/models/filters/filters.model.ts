import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  schedules: [],
  levels: [],
  users_group: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
