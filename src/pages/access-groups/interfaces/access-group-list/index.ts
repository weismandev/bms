import { Value } from '../general-interfaces';

export interface GetAccessGroupListPayload {
  id?: number;
  inherit_compl?: number;
  limit?: number;
  offset?: number;
  search?: string;
  schedule_id?: string;
  level_id?: string;
  usergroup_id?: string;
  sort_by?: string;
  sort_order?: string;
  show_users_count?: number;
  show_levels_count?: number;
}

export interface GetAccessGroupListResponse {
  meta: {
    total: number;
  };
  groups: Group[];
}

export interface Group {
  id: number;
  title: string;
  users_count: number;
  levels_count: number;
  created_at: number;
  updated_at: number;
  compl: Compl[];
}

export interface Compl {
  id: number;
  levels: Value[];
  schedules: Value[];
  user_groups: Value[];
  exclude_user_groups: Value[];
}
