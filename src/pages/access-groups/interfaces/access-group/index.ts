import { Value } from '../general-interfaces';

export interface AccessGroup {
  id?: number;
  schedule: Value[];
  level: Value[];
  user_group: Value[];
  exclude_user_group: Value[];
}

export interface SubGroup {
  id?: number;
  schedule_id: string;
  level_id: string;
  user_group_id: string;
  exclude_user_group: string;
}

export interface CreateUpdateAccessGroupPayload {
  id?: number;
  title: string;
  subGroups: SubGroup[];
}

export interface CreateUpdateSubGroupPayload {
  group_id: number;
  id?: number;
  schedule_id: string;
  level_id: string;
  user_group_id: string;
  exclude_user_group: string;
  isLastGroup: boolean;
}

export interface CreateUpdateAccessGroupResponse {
  groups: {
    id: string;
  };
}

export interface DeleteGroupPayload {
  id: number;
}

export interface DeleteSubGroupPayload {
  id: number;
  group_id: number;
}

export interface Opened {
  id?: number;
  title: string;
  users: number;
  levels: number;
  subGroups: AccessGroup[];
}
