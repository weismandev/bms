export interface Value {
  id: number;
  title: string;
}

export interface Filters {
  schedules: Value[];
  levels: Value[];
  users_group: Value[];
}
