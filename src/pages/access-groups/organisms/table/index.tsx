import { FC } from 'react';
import { useUnit } from 'effector-react';
import type { GridRowParams, GridRowClassNameParams } from '@mui/x-data-grid-pro';
import { DataGridTable } from '@shared/ui/data-grid-table';
import {
  $tableData,
  $isLoadingList,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  openViaUrl,
  sortChanged,
  openRowChanged,
  $pinnedColumns,
  changePinnedColumns,
  $count,
  $pagination,
  $sorting,
  paginationModelChanged,
  $openRow,
  $selectRow
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { StyledWrapper } from './styled';

export const Table: FC = () => {
  const [
    isLoading,
    tableData,
    pagination,
    visibilityColumns,
    columns,
    count,
    pinnedColumns,
    sorting,
    openRow,
    selectRow,
  ] = useUnit([
    $isLoadingList,
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $count,
    $pinnedColumns,
    $sorting,
    $openRow,
    $selectRow
  ]);

  const handleClickRow = (row: GridRowParams) => {
    openViaUrl(row.id);
    openRowChanged(row.id);
  };

  const getRowClassName = ({ id }: GridRowClassNameParams<any>): string => {
    const baseStyle =
      id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

    return baseStyle;
  };

  return (
    <StyledWrapper elevation={0}>
      <TableToolbar />

      <DataGridTable
        rows={tableData}
        columns={columns}
        rowCount={count}
        loading={isLoading}
        density="compact"
        rowSelectionModel={selectRow}
        onRowClick={handleClickRow}
        paginationModel={pagination}
        onPaginationModelChange={paginationModelChanged}
        sortModel={sorting}
        onSortModelChange={sortChanged}
        pinnedColumns={pinnedColumns}
        onPinnedColumnsChange={changePinnedColumns}
        columnVisibilityModel={visibilityColumns}
        onColumnVisibilityModelChange={visibilityColumnsChanged}
        onColumnWidthChange={columnsWidthChanged}
        onColumnOrderChange={orderColumnsChanged}
        getRowClassName={getRowClassName}
        pageSizeOptions={[25, 50, 100]}
      />
    </StyledWrapper>
  );
};
