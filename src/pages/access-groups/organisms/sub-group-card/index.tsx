import { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { ExpandMore, ExpandLess, HighlightOff } from '@mui/icons-material';
import { IconButton } from '@ui/index';
import { AccessGroup } from '../../interfaces';
import { $mode } from '../../models';
import { ExcludeUsersGroupSelectField } from '../exclude-users-group-select';
import { LevelSelectField } from '../level-select';
import { ScheduleSelectField } from '../schedule-select';
import { UsersGroupSelectField } from '../users-group-select';
import { useStyles, styles } from './styles';

interface Props {
  index: number;
  parentName: string;
  remove: () => void;
  value: AccessGroup;
}

export const SubGroupCard: FC<Props> = ({ index, parentName, remove, value }) => {
  const { t } = useTranslation();
  const mode = useStore($mode);

  const classes = useStyles();

  const [expanded, setExpanded] = useState(true);

  const handleExpanded = () => setExpanded(!expanded);

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <span className={classes.headerTitle}>
          {t('Subgroup')} {index + 1}
        </span>
        <div className={classes.headerButtons}>
          {index > 0 && mode === 'edit' && (
            <IconButton style={styles.button} onClick={remove}>
              <HighlightOff titleAccess={t('remove')} style={styles.deleteIcon} />
            </IconButton>
          )}
          <IconButton style={styles.button} onClick={handleExpanded}>
            {expanded ? (
              <ExpandMore titleAccess={t('Collapse')} style={styles.expandIcon} />
            ) : (
              <ExpandLess titleAccess={t('Expand')} style={styles.expandIcon} />
            )}
          </IconButton>
        </div>
      </div>
      <Collapse in={expanded} timeout="auto">
        <div className={classes.content}>
          <Field
            name={`${parentName}.schedule`}
            label={t('ScheduleTitle')}
            placeholder={t('ChooseAccessSchedule')}
            component={ScheduleSelectField}
            mode={mode}
            divider={false}
            required
            isMulti
          />
          <div className={classes.content}>
            <Field
              name={`${parentName}.level`}
              label={t('AccessLevel')}
              placeholder={t('ChooseAccessLevel')}
              component={LevelSelectField}
              mode={mode}
              divider={false}
              required
              isMulti
            />
          </div>
          <div className={classes.content}>
            <Field
              name={`${parentName}.user_group`}
              label={t('GroupsOfUsers')}
              placeholder={t('ChooseUsersGroups')}
              component={UsersGroupSelectField}
              mode={mode}
              divider={false}
              required
              isMulti
              excludeIds={value.exclude_user_group}
            />
          </div>
          <div className={classes.content}>
            <Field
              name={`${parentName}.exclude_user_group`}
              label={t('ExclusionOfUserGroups')}
              placeholder={t('ExcludeUserGroup')}
              component={ExcludeUsersGroupSelectField}
              mode={mode}
              divider={false}
              isMulti
              excludeIds={value.user_group}
            />
          </div>
        </div>
      </Collapse>
    </div>
  );
};
