import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  container: {
    background: '#EDF6FF',
    borderRadius: 16,
    padding: '18px 24px',
    marginBottom: 15,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  headerTitle: {
    fontWeight: 500,
    fontSize: 18,
    color: '#3B3B50',
  },
  headerButtons: {
    display: 'flex',
  },
  content: {
    marginTop: 20,
  },
}));

export const styles = {
  button: {
    borderRadius: 25,
  },
  expandIcon: {
    color: '#65657B',
  },
  deleteIcon: {
    color: '#EB5757',
  },
};
