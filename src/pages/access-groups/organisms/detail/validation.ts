import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  subGroups: Yup.array().of(
    Yup.object().shape({
      schedule: Yup.array().min(1, t('thisIsRequiredField')),
      level: Yup.array().min(1, t('thisIsRequiredField')),
      user_group: Yup.array().min(1, t('thisIsRequiredField')),
    })
  ),
});
