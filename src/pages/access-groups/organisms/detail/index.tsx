import { FC, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray, FieldProps } from 'formik';
import { Button } from '@mui/material';
import { Wrapper, DetailToolbar, CustomScrollbar, InputField } from '@ui/index';
import { Opened } from '../../interfaces';
import {
  $opened,
  detailSubmitted,
  changeMode,
  changedDetailVisibility,
  $mode,
  subGroupEntity,
  changedVisibilityDeleteModal,
} from '../../models';
import { SubGroupCard } from '../sub-group-card';
import { useStyles } from './styles';
import { validationSchema } from './validation';

export const Detail: FC = memo(() => {
  const { t } = useTranslation();
  const opened = useStore($opened) as Opened;
  const mode = useStore($mode);

  const classes = useStyles();

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm }) => {
          const onEdit = () => changeMode('edit');
          const onClose = () => changedDetailVisibility(false);
          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };
          const onDelete = () => changedVisibilityDeleteModal(true);

          return (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.toolbar}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <div className={classes.content}>
                <CustomScrollbar>
                  <div className={classes.contentForm}>
                    <Field
                      name="title"
                      mode={mode}
                      label={t('NameTitle')}
                      placeholder={t('EnterTheNameOfTheAccessGroup')}
                      component={InputField}
                      required
                    />
                    {!isNew && (
                      <div className={classes.twoFields}>
                        <div className={classes.firstField}>
                          <Field
                            name="users"
                            mode="view"
                            label={t('UsersNo')}
                            component={InputField}
                          />
                        </div>
                        <div className={classes.secondfield}>
                          <Field
                            name="levels"
                            mode="view"
                            label={t('AccessPoints')}
                            component={InputField}
                          />
                        </div>
                      </div>
                    )}
                    <FieldArray
                      name="subGroups"
                      render={({ push, remove }) => {
                        const groupCard = values.subGroups.map(
                          (_: unknown, index: number) => (
                            <Field
                              key={index}
                              name={`subGroups[${index}]`}
                              render={({ field }: FieldProps) => {
                                const removeGroup = () => remove(index);

                                return (
                                  <SubGroupCard
                                    index={index}
                                    parentName={field.name}
                                    remove={removeGroup}
                                    value={field.value}
                                  />
                                );
                              }}
                            />
                          )
                        );

                        const onClick = () => push(subGroupEntity);

                        const addButton =
                          mode === 'edit' ? (
                            <Button color="primary" onClick={onClick}>
                              {`+ ${t('AddSubgroup')}`}
                            </Button>
                          ) : null;

                        return (
                          <>
                            {groupCard}
                            {addButton}
                          </>
                        );
                      }}
                    />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});
