import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { $data, $isLoading, fxGetList } from '../../models/exclude-users-group-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  excludeIds?: Value[];
}

export const ExcludeUsersGroupSelect: FC<Props> = ({ excludeIds = [], ...props }) => {
  useEffect(() => {
    fxGetList();
  }, []);

  const data = useStore($data);
  const isLoading = useStore($isLoading);

  const ids = excludeIds.map((item) => item.id);

  const options =
    excludeIds.length > 0 ? data.filter((option) => !ids.includes(option.id)) : data;

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const ExcludeUsersGroupSelectField: FC<SelectFieldProps> = (props) => {
  const { form, field } = props;

  const onChange = (value: Value) => form.setFieldValue(field.name, value);

  return (
    <SelectField component={<ExcludeUsersGroupSelect />} onChange={onChange} {...props} />
  );
};
