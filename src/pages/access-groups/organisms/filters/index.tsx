import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';

import { UsersGroupSelectField } from '../users-group-select';
import { ScheduleSelectField } from '../schedule-select';
import { LevelSelectField } from '../level-select';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { useStyles } from './styles';
import { Filters as FiltersInterface } from '../../interfaces';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters) as FiltersInterface;

  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={() => (
            <Form className={classes.form}>
              <Field
                name="schedules"
                label={t('ScheduleTitle')}
                placeholder={t('ChooseAccessSchedule')}
                component={ScheduleSelectField}
                isMulti
              />
              <Field
                name="levels"
                label={t('AccessLevel')}
                placeholder={t('ChooseAccessLevel')}
                component={LevelSelectField}
                isMulti
              />
              <Field
                name="users_group"
                label={t('GroupsOfUsers')}
                placeholder={t('ChooseUsersGroups')}
                component={UsersGroupSelectField}
                isMulti
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
