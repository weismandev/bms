import { api } from '@api/api2';

import {
  GetAccessGroupListPayload,
  CreateUpdateAccessGroupPayload,
  CreateUpdateSubGroupPayload,
  DeleteGroupPayload,
  DeleteSubGroupPayload,
} from '../interfaces';

const getAccessGroupList = (payload: GetAccessGroupListPayload) =>
  api.no_vers('get', 'scud/get-access-groups', payload);
const getSchedules = () => api.no_vers('get', 'scud/get-schedule', { limit: 999999 });
const getLevels = () => api.no_vers('get', 'scud/get-levels', { limit: 999999 });
const getUsersGroup = () =>
  api.no_vers('get', 'scud/get-users-group', {
    exclude_user_group: 1,
    limit: 999999,
  });
const createUpdateAccessGroup = (payload: CreateUpdateAccessGroupPayload) =>
  api.no_vers('post', 'scud/update-access-groups', payload);
const createSubGroup = (payload: CreateUpdateSubGroupPayload) =>
  api.no_vers('post', 'scud/new-compl-access-group', payload);
const updateSubGroup = (payload: CreateUpdateSubGroupPayload) =>
  api.no_vers('post', 'scud/update-compl-access-group', payload);
const deleteGroup = (payload: DeleteGroupPayload) =>
  api.no_vers('post', 'scud/remove-access-groups', payload);
const deleteSubGroup = (payload: DeleteSubGroupPayload) =>
  api.no_vers('post', 'scud/remove-compl-access-group', payload);

export const accessGroupsApi = {
  getAccessGroupList,
  getSchedules,
  getLevels,
  getUsersGroup,
  createUpdateAccessGroup,
  createSubGroup,
  updateSubGroup,
  deleteGroup,
  deleteSubGroup,
};
