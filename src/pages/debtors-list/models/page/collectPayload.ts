import { DebitorsPayload } from '../../interfaces';

export function collectPayload([tableParams, filters]: [any, any]) {
  const payload = {} as DebitorsPayload;

  if (tableParams.search) {
    payload.search = tableParams.search;
  }

  if (tableParams.sorting.length > 0) {
    payload.column = tableParams.sorting[0].field;
    payload.sort = tableParams.sorting[0].sort;
  }

  if (tableParams.per_page) {
    payload.per_page = tableParams.per_page;
  }

  if (tableParams.page) {
    payload.page = tableParams.page;
  }

  if (typeof filters === 'object') {
    if (filters.complexes && filters.complexes.id) {
      payload.complexes = [filters.complexes.id];
    }

    if (Array.isArray(filters.houses) && filters.houses.length > 0) {
      payload.buildings = filters.houses.map((house: any) => house.id);
    }

    if (Array.isArray(filters.apartments) && filters.apartments.length > 0) {
      payload.apartments = filters.apartments.map((apartment: any) => apartment.id);
    }

    if (filters.ignore_mode) {
      payload.ignore_mode = filters.ignore_mode;
    }
  }

  return payload;
}
