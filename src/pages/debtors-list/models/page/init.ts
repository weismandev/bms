import { merge, sample } from 'effector';
import { signout } from '@features/common';
import { debitorsApi } from '../../api';
import { fxGetOpened } from '../detail/model';
import { $filters } from '../filter/model';
import { $tableParams, selectionRowChanged, $openRow } from '../table/model';
import storage from '../table/storage';
import { collectPayload } from './collectPayload';
import {
  pageMounted,
  $debtorsData,
  $isLoading,
  $error,
  fxGetDebtorslist,
  fxSetIgnore,
  fxSetRestore,
  CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
} from './model';

let requestController: AbortController;

/** Прооверка наличия запроса, запись в $isLoading */
const isRequest = merge([
  fxGetDebtorslist.pending,
  fxSetIgnore.pending,
  fxSetRestore.pending,
]);
$isLoading.on(isRequest, (_state, isLoading) => isLoading).reset(signout);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([
  fxGetDebtorslist.failData,
  fxSetIgnore.failData,
  fxSetRestore.failData,
]);

/** Записываю ошибку, только если запрос не бы отменен */
sample({
  clock: isError,
  filter: (error) => error.message !== CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS,
  fn: (error) => error.message ?? 'Request error',
  target: $error,
});

$error
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(signout);

const isDoneIgnoreOrRestore = merge([fxSetIgnore.done, fxSetRestore.done]);

/** При загрузке, изменении фильтров, табов, пагинации, сортировки
 * вызываем запрос реестра должников */
sample({
  clock: [
    pageMounted,
    storage.$isGettingSettingsFromStorage,
    $tableParams,
    $filters,
    isDoneIgnoreOrRestore,
  ],
  source: [$tableParams, $filters],
  fn: ([tableParams, filters]) => {
    if (requestController) {
      requestController.abort();
    }

    requestController = new AbortController();

    return {
      payload: collectPayload([tableParams, filters]),
      signal: requestController.signal,
    };
  },
  target: fxGetDebtorslist,
});

/** После изменения статуса, если открыты детали, то обновляю их */
sample({
  clock: isDoneIgnoreOrRestore,
  source: $openRow,
  filter: (openRow) => openRow !== null,
  fn: (openRow) => ({ id: Number(openRow) ?? 0 }),
  target: fxGetOpened,
});

/** Запуск запроса должников */
fxGetDebtorslist.use(debitorsApi.getDebtorsList);

/** Запись результатов запроса должников */
$debtorsData.on(fxGetDebtorslist.doneData, (_, result) => result);

/** Очистка выделенных записей при новой загрузке */
sample({
  clock: $debtorsData,
  fn: () => [],
  target: selectionRowChanged,
});

/** Запуск запроса о переводе в игнорируемые */
fxSetIgnore.use(debitorsApi.setIgnore);

/** Запуск запроса о восстановлении из игнорируемых */
fxSetRestore.use(debitorsApi.setRestore);
