import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { createPageBag } from '../../../../tools/factories';
import { debitorsApi } from '../../api';
import { DebitorsPayload, DebtorsData, IgnoreRestoreDebtors } from '../../interfaces';

export const LOCAL_STORAGE_KEY = 'debtors-list';
export const CANCELED_REQUEST_BY_NEW_REQUEST_ERROR_STATUS = 'canceled';

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const clearError = createEvent();

const $debtorsData = createStore<Partial<DebtorsData>>({});
const $isLoading = createStore(false);
const $error = createStore(null).on(clearError, () => null);

const fxGetDebtorslist = createEffect<
  { payload: DebitorsPayload; signal: AbortController['signal'] },
  DebtorsData,
  Error
>();
const fxSetIgnore = createEffect<IgnoreRestoreDebtors, any, Error>();
const fxSetRestore = createEffect<IgnoreRestoreDebtors, any, Error>();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  $debtorsData,
  $isLoading,
  $error,
  fxGetDebtorslist,
  clearError,
  fxSetIgnore,
  fxSetRestore,
};

export const {
  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(debitorsApi);
