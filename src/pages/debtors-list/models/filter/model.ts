import { createFilterBag } from '@tools/factories';
import { IgnoredStatus } from '../../interfaces';

export const defaultFilters = {
  ignore_mode: IgnoredStatus.all,
  complexes: [],
  houses: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
