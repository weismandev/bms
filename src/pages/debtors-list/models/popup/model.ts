import {
  PreferencesNotifications,
  PreferencesSmsProvider,
} from '@features/debtors-settings/interfaces';
import { createEffect, createEvent, createStore } from 'effector';
import { NotesFormFields, PopupTypes, SendNotificationBatch } from '../../interfaces';

const initialValues: NotesFormFields = {
  message: '',
  management_company: false,
  email: false,
  sms: false,
};

const changePopupNotesStatus = createEvent<PopupTypes | null>();
const $popupNotesStatus = createStore<PopupTypes | null>(null);

const resetInitialValuesNotes = createEvent();
const $initialValuesNotes = createStore<NotesFormFields>(initialValues);

const $sendNotesLoading = createStore<boolean>(false);
const $sendNotesError = createStore<string | null>(null);
const fxSendNotes = createEffect<SendNotificationBatch, void, Error>();
const resetNotesError = createEvent();
const sendNotes = createEvent<NotesFormFields>();

const $preferencesNotifications = createStore<PreferencesNotifications>(
  {} as PreferencesNotifications
);
const fxGetPreferencesNotifications = createEffect<
  void,
  PreferencesNotifications,
  Error
>();

const $preferencesSmsProvider = createStore<PreferencesSmsProvider>(
  {} as PreferencesSmsProvider
);
const fxGetPreferencesSmsProvider = createEffect<void, PreferencesSmsProvider, Error>();

export {
  $popupNotesStatus,
  changePopupNotesStatus,
  resetInitialValuesNotes,
  $initialValuesNotes,
  fxSendNotes,
  $sendNotesLoading,
  $sendNotesError,
  resetNotesError,
  sendNotes,
  $preferencesNotifications,
  fxGetPreferencesNotifications,
  $preferencesSmsProvider,
  fxGetPreferencesSmsProvider,
};
