import { sample } from 'effector';
import { signout } from '@features/common';
import { debitorsSettingsApi } from '@features/debtors-settings/api';
import { debitorsApi } from '../../api';
import { Channels, PopupTypes, SendNotificationBatch } from '../../interfaces';
import { pageUnmounted } from '../page/model';
import { $selectRow } from '../table/model';
import {
  $initialValuesNotes,
  $popupNotesStatus,
  changePopupNotesStatus,
  resetInitialValuesNotes,
  fxSendNotes,
  $sendNotesLoading,
  $sendNotesError,
  resetNotesError,
  sendNotes,
  fxGetPreferencesNotifications,
  $preferencesNotifications,
  fxGetPreferencesSmsProvider,
  $preferencesSmsProvider,
} from './model';

$initialValuesNotes.reset(resetInitialValuesNotes);

$popupNotesStatus
  .on(changePopupNotesStatus, (_, status) => status)
  .reset(pageUnmounted, signout);

fxSendNotes.use(debitorsApi.sendNotificationBatch);

$sendNotesLoading
  .on(fxSendNotes.pending, (_, status) => status)
  .reset(pageUnmounted, signout);

$sendNotesError
  .on(fxSendNotes.failData, (_, error) => error.message ?? 'Request failed')
  .reset(pageUnmounted, resetNotesError, signout);

fxGetPreferencesNotifications.use(debitorsSettingsApi.getPreferencesNotifications);
fxGetPreferencesSmsProvider.use(debitorsSettingsApi.getPreferencesSmsProvider);

$preferencesNotifications
  .on(fxGetPreferencesNotifications.doneData, (_, data) => data)
  .reset(changePopupNotesStatus);
$preferencesSmsProvider
  .on(fxGetPreferencesSmsProvider.doneData, (_, data) => data)
  .reset(changePopupNotesStatus);

/** Запуск загрузки данных настроек уведомлений и возможности отправки СМС */
sample({
  source: $popupNotesStatus,
  filter: (popupNotesStatus) => popupNotesStatus === PopupTypes.info,
  target: [fxGetPreferencesNotifications, fxGetPreferencesSmsProvider],
});

/** При успешной отправке перенаправляю на экран упешной отправки */
sample({
  clock: fxSendNotes.done,
  fn: () => PopupTypes.sentResult,
  target: changePopupNotesStatus,
});

/** При нажатии кнопки отправки уведомелий, собираю данные формы
 * и выделенных пользователей и отпраляю на сервер */
sample({
  clock: sendNotes,
  source: $selectRow,
  fn: (debtors, sendNotes) => {
    const channels: Channels[] = [];
    if (sendNotes.management_company) channels.push(Channels.management_company);
    if (sendNotes.email) channels.push(Channels.email);
    if (sendNotes.sms) channels.push(Channels.sms);

    const payload: SendNotificationBatch = {
      template: sendNotes.message,
      debtors,
      channels,
    };

    return payload;
  },
  target: fxSendNotes,
});
