import { sample } from 'effector';
import { signout } from '@features/common';
import { debitorsApi } from '../../api';
import { $filters } from '../filter/model';
import { collectPayload } from '../page/collectPayload';
import { pageMounted } from '../page/model';
import { $tableParams } from '../table/model';
import {
  $exportPopup,
  showExportPopup,
  hideExportPopup,
  $isSending,
  fxSendlistExport,
  $isExportError,
  $sendResult,
  sendlistExport,
} from './model';

fxSendlistExport.use(debitorsApi.sendlistExport);

$isSending
  .on(fxSendlistExport.pending, (_state, result) => result)
  .reset(signout, pageMounted);

$sendResult
  .on(fxSendlistExport.done, () => true)
  .on(showExportPopup, () => null)
  .reset(signout, pageMounted);

$isExportError
  .on(fxSendlistExport.failData, (_state, result) => result)
  .on(fxSendlistExport.pending, (_, status) => {
    if (status) return null;
  })
  .reset(signout, pageMounted);

$exportPopup
  .on(showExportPopup, () => true)
  .on(hideExportPopup, () => false)
  .reset(signout, pageMounted);

sample({
  clock: sendlistExport,
  source: [$tableParams, $filters],
  fn: ([tableParams, filters], { email }) => {
    const mapParams = collectPayload(null, [tableParams, filters]);

    return { ...mapParams, email };
  },
  target: fxSendlistExport,
});
