import { createEvent, createStore } from 'effector';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { TableDataItem } from '../../interfaces';
import { $debtorsData } from '../page/model';

export const DEFAULT_PAGE_SIZE = 10;

const { t } = i18n;

const columns: GridColDef[] = [
  {
    field: 'account_number',
    headerName: t('Label.personalAccount') as string,
    width: 200,
    sortable: true,
  },
  {
    field: 'debtor_is_ignored',
    headerName: t('DebtorsList.status') as string,
    width: 160,
    sortable: false,
  },
  {
    field: 'building_title',
    headerName: t('Label.address') as string,
    width: 300,
    sortable: false,
  },
  {
    field: 'residents_full_name',
    headerName: t('Label.FullName') as string,
    width: 300,
    sortable: false,
  },
  {
    field: 'account_balance',
    headerName: t('DebtorsList.Balance') as string,
    width: 100,
    sortable: true,
  },
  {
    field: 'account_last_receipt_at',
    headerName: t('DebtorsList.LastReceipt') as string,
    width: 150,
    sortable: true,
  },
  {
    field: 'account_last_payment_at',
    headerName: t('DebtorsList.LastPayment') as string,
    width: 150,
    sortable: true,
  },
  {
    field: 'debtor_last_notification_at',
    headerName: t('DebtorsList.LastNotice') as string,
    width: 150,
    sortable: true,
  },
];

const changeTabledata = createEvent<TableDataItem[]>();
const $tableData = createStore<TableDataItem[]>([]);

export const {
  $tableParams,
  $pagination,
  $search,
  searchForm,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $openRow,
  openRowChanged,
  changePinnedColumns,
  $pinnedColumns,
  mergeColumnsByPattern,
  paginationModelChanged,
} = createTableBag({ columns });

export const $columnsWidthAndOrder = $columns.map((columns) =>
  columns.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);

export const $count = $debtorsData.map(({ meta }) => meta?.total ?? 0);

export { $tableData, columns, changeTabledata };
