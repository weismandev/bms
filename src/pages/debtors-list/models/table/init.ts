import { sample } from 'effector';
import format from 'date-fns/format';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { Debtor } from '../../interfaces';
import { $debtorsData } from '../page/model';
import { $tableData, changeTabledata } from './model';

$tableData.on(changeTabledata, (_, data) => data);

export const getFormatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(localDate, 'dd.MM.yyyy HH:mm');
};

const formatDebtorsToTable = (debtors: Debtor[]) => {
  return debtors.map(({ account, building, apartment, residents, debtor }) => {
    return {
      id: debtor.id,
      debtor_id: debtor.id,
      debtor_is_ignored: debtor.is_ignored,
      account_number: account.number,
      building_title: `${building.title}, ${apartment.number}`,
      residents_full_name: residents.map((resident) => resident.full_name),
      account_balance: account.balance,
      account_last_receipt_at: account.last_receipt_at
        ? getFormatDate(account.last_receipt_at)
        : '-',
      account_last_payment_at: account.last_payment_at
        ? getFormatDate(account.last_payment_at)
        : '-',
      debtor_last_notification_at: debtor.last_notification_at
        ? getFormatDate(debtor.last_notification_at)
        : '-',
    };
  });
};

/** Формирую данные таблицы при загрузке */
sample({
  clock: $debtorsData,
  fn: ({ debtors }) => {
    if (!debtors) return [];

    return formatDebtorsToTable(debtors);
  },
  target: changeTabledata,
});
