import { createEffect, createEvent, createStore } from 'effector';
import { Debtor } from '../../interfaces';

export const clearOpenErrorEvent = createEvent<null>();
export const openViaUrl = createEvent<string | number>();
export const showDetails = createEvent();
export const hideDetails = createEvent();

export const fxGetOpened = createEffect<{ id: string | number }, Debtor, Error>();

export const $openedLoading = createStore(false);
export const $openedError = createStore<Error | null>(null);
export const $openedData = createStore<Debtor | null>(null);
export const $isDetailOpen = createStore<boolean>(false);
export const open = createEvent<number>();
