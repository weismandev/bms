import { sample } from 'effector';
import { signout, $pathname, history, historyPush } from '@features/common';
import { debitorsApi } from '../../api';
import { pageMounted } from '../page/model';
import { $openRow, openRowChanged } from '../table/model';
import {
  openViaUrl,
  fxGetOpened,
  $openedLoading,
  $openedError,
  clearOpenErrorEvent,
  $openedData,
  $isDetailOpen,
  showDetails,
  hideDetails,
} from './model';

$openRow.reset(pageMounted, signout);

$isDetailOpen
  .on(showDetails, () => true)
  .on(hideDetails, () => false)
  .reset(pageMounted, signout);

$openedError
  .on(fxGetOpened.failData, (_, error) => error)
  .on(clearOpenErrorEvent, () => null)
  .reset(pageMounted, signout);

$openedLoading.on(fxGetOpened.pending, (_, status) => status).reset(pageMounted, signout);

$openedData
  .on(fxGetOpened.doneData, (_, opened) => opened)
  .reset(pageMounted, signout, hideDetails);

/** Открываю кладку с деталями в начале их загрузки */
sample({
  clock: $openedLoading,
  filter: (openedLoading) => openedLoading,
  target: showDetails,
});

/** Запуск запроса должника по id */
fxGetOpened.use(debitorsApi.getDebtorsItem);

/** Закрываю детали после перезагрузки или при ошибке их запроса */
sample({
  clock: clearOpenErrorEvent,
  fn: () => false,
  target: hideDetails,
});

/** По нажатию пункт таблицы через изменение url запускаю загрузку деталей */
sample({
  source: $openRow,
  filter: (openRow) => openRow !== null,
  fn: (openRow) => ({ id: Number(openRow) ?? 0 }),
  target: fxGetOpened,
});

/** Призакрытии деталей, сбрасываю openRow */
sample({
  clock: $isDetailOpen,
  filter: (isDetailOpen) => !isDetailOpen,
  fn: () => null,
  target: $openRow,
});

const idNumberInPath = 2;

/** После загрузки страницы проверяю есть ли в $pathname в
 * конце id открытого пункта таблицы и запускаю загрузку деталей через добавление выделенной строки в таблице */
sample({
  clock: pageMounted,
  source: $pathname,
  filter: (pathname) => {
    const getopenedId = pathname.split('/');

    if (!getopenedId[idNumberInPath]) return false;

    return true;
  },
  fn: (pathname) => {
    const id = pathname.split('/')[idNumberInPath];

    return Number(id);
  },
  target: openRowChanged,
});

/** Перехожу на url c id должника по клику */
sample({
  clock: $openRow,
  source: $pathname,
  fn: (pathname, openRow) => {
    if (!openRow) {
      return `../${pathname.split('/')[1]}`;
    }

    const path = pathname.split('/')[1] ? `../${pathname.split('/')[1]}/${openRow}` : '.';

    return path;
  },
  target: historyPush,
});

/** При закрытии деталей, перехожу на корневой url */
sample({
  clock: hideDetails,
  source: $pathname,
  fn: (pathname, _) => {
    const pathArr = pathname.trim('/').split('/');

    const rootUrlArr = pathArr.slice(0, pathArr.length - 1);

    return rootUrlArr.join('/');
  },
  target: historyPush,
});
