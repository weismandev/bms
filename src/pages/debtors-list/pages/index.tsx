import { useUnit } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { ThemeAdapter } from '@shared/theme/adapter';
import { ErrorMessage, FilterMainDetailLayout } from '../../../ui';
import {
  $isExportError,
  $sendNotesError,
  clearExportError,
  resetNotesError,
} from '../models';
import { $isDetailOpen } from '../models/detail/model';
import { $isFilterOpen } from '../models/filter/model';
import { PageGate, $error, clearError } from '../models/page/model';
import { Detail, Table, PopupRoot, PopupExport } from '../organisms';
import { Filter } from '../organisms/filter';

const DebtorsPage = () => {
  const [isExportError, error, sendNotesError, isFilterOpen, isDetailOpen] = useUnit([
    $isExportError,
    $error,
    $sendNotesError,
    $isFilterOpen,
    $isDetailOpen,
  ]);

  return (
    <ThemeAdapter>
      <PageGate />

      <ErrorMessage isOpen={error} onClose={clearError} error={error} />

      <ErrorMessage
        isOpen={isExportError}
        onClose={clearExportError}
        error={isExportError}
      />

      <ErrorMessage
        isOpen={sendNotesError}
        onClose={resetNotesError}
        error={sendNotesError}
      />

      <PopupRoot />

      <PopupExport />

      <FilterMainDetailLayout
        params={{
          detailWidth: 'minmax(370px, 40%)',
          filterWidth: 'minmax(340px, 340px)',
        }}
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
      />
    </ThemeAdapter>
  );
};

const RestrictedDebtorsPage = (props: Record<string, unknown>) => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <DebtorsPage {...props} />
    // </HaveSectionAccess>
  );
};

export { RestrictedDebtorsPage };
