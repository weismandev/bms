import { useState } from 'react';
import { useUnit } from 'effector-react';
import { Button, IconButton, Tooltip } from '@mui/material';
import FilterAltIconOutlined from '@mui/icons-material/FilterAltOutlined';
import ExportIcon from '@mui/icons-material/VerticalAlignTop';

import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { Control } from '@features/effector-form';
import i18n from '@shared/config/i18n';
import { PopupTypes } from '../../interfaces';
import {
  $isLoading,
  $isDetailOpen,
  changePopupNotesStatus,
  fxSetIgnore,
  fxSetRestore,
  showExportPopup,
} from '../../models';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter/model';
import { $selectRow, searchForm } from '../../models/table/model';
import {
  StyledTableToolbarLeft,
  StyledTableToolbarRight,
  StyledTableToolbar,
} from './styled';

const { t } = i18n;

export const TableToolbarLeft = () => {
  const [isFilterOpen] = useUnit([$isFilterOpen]);

  return (
    <StyledTableToolbarLeft>
      <IconButton onClick={() => changedFilterVisibility(true)} disabled={isFilterOpen}>
        <FilterAltIconOutlined />
      </IconButton>

      <Control.Search form={searchForm} name="search" />
    </StyledTableToolbarLeft>
  );
};

export const TableToolbarRight = () => {
  const [
    selection,
    isLoading,
    isFilterOpen,
    isDetailOpen
  ] = useUnit([
    $selectRow,
    $isLoading,
    $isFilterOpen,
    $isDetailOpen
  ]);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const setIngnore = () => {
    const numberSelection = { debtors: selection.map((item) => Number(item)) };
    fxSetIgnore(numberSelection);
  };

  const setRestore = () => {
    const numberSelection = { debtors: selection.map((item) => Number(item)) };
    fxSetRestore(numberSelection);
  };

  const startPopup = () => {
    changePopupNotesStatus(PopupTypes.info);
  };

  const handleDownload = () => {
    showExportPopup();
  };

  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
;
  const getResponsive = () => {
    if (isFilterOpen && isDetailOpen) return (
      <>
      <Button
        id="demo-customized-button"
        aria-controls={open ? 'demo-customized-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        variant="outlined"
        disableElevation
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
      >
        {t('Label.actions')}
      </Button>
      <Menu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={startPopup} disabled={selection.length === 0} disableRipple>
          {t('DebtorsList.sendNotification')}
        </MenuItem>
        <MenuItem onClick={setIngnore} disabled={isLoading || selection.length === 0} disableRipple>
          {t('DebtorsList.Ignored')}
        </MenuItem>
        <MenuItem onClick={setRestore} disabled={isLoading || selection.length === 0} disableRipple>
          {t('DebtorsList.Debtor')}
        </MenuItem>
        <MenuItem onClick={handleDownload} disableRipple>
          <ExportIcon />
          {t('DataGrid.toolbarExport')}
        </MenuItem>
      </Menu>
      </>
    )
    return (
      <>
        <Button size="medium" variant="contained" onClick={startPopup} disabled={selection.length === 0}>
          {t('DebtorsList.sendNotification')}
        </Button>

        <Tooltip title={t('DebtorsList.excludeFromDebtors') as string}>
          <Button
            size="medium"
            variant="contained"
            disabled={isLoading || selection.length === 0}
            onClick={setIngnore}
          >
            {t('DebtorsList.Ignored')}
          </Button>
        </Tooltip>

        <Tooltip title={t('DebtorsList.returnToDebt') as string}>
          <Button
            size="medium"
            variant="contained"
            disabled={isLoading || selection.length === 0}
            onClick={setRestore}
          >
            {t('DebtorsList.Debtor')}
          </Button>
        </Tooltip>

        <Button
          size="medium"
          variant="outlined"
          onClick={handleDownload}
          startIcon={<ExportIcon />}
        >
          {t('DataGrid.toolbarExport')}
        </Button>
      </>
    )
  }
  return (
    <StyledTableToolbarRight>
      {getResponsive()}
    </StyledTableToolbarRight>
  );
};

const TableToolbar = () => {
  return (
    <StyledTableToolbar>
      <TableToolbarLeft />
      <TableToolbarRight />
    </StyledTableToolbar>
  );
};

export { TableToolbar };
