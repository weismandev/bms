import { Form } from 'formik';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CloseIcon from '@mui/icons-material/Close';
import styled from '@emotion/styled';

export const StyledDataBlock = styled.div((props: Record<string, unknown>) => ({
  width: props.narrow ? '360px' : '540px',
  backgroundColor: '#fff',
  boxShadow: '0px 4px 15px #6A6A6E',
  borderRadius: '15px',
  padding: '18px 24px',
  textAlign: props.center ? 'center' : 'inherit',
}));

export const StyledPopup = styled.div(() => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.2);',
  zIndex: 100,
}));

export const StyledHeader = styled.div(() => ({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}));

export const StyledHeaderTitle = styled.div`
  font-weight: 700;
  font-size: 24px;
  line-height: 28px;
  color: #3b3b50;
`;

export const StyledCloseIcon = styled(CloseIcon)({
  cursor: 'pointer',
});

export const StyledItem = styled.div(() => ({
  marginTop: 20,
  display: 'flex',
  alignItems: 'center',
}));

export const StyledButtonsBlock = styled.div`
  margin-top: 31px;
  display: flex;
  align-items: center;
  gap: 12px;
  padding-bottom: 7px;
  > * {
    margin-right: 16px;
  }
`;

export const StyledForm = styled(Form)`
  width: 100%;
`;

export const StyledAlertMessage = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: #767676;
  margin-top: 14px;
`;

export const StyledCheckboxErrorMessage = styled.p`
  color: #d32f2f;
  font-size: 0.75rem;
`;

export const StyledSuccessMessage = styled.span`
  display: block;
  font-weight: 700;
  font-size: 24px;
  line-height: 28px;
  color: #1bb169;
  margin-top: 23px;
`;

export const StyledCheckCircleIcon = styled(CheckCircleIcon)({
  color: '#1BB169',
  fontSize: '105px',
});
