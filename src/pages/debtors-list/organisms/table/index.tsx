import { FC, memo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Button, Chip, Paper, Popover } from '@mui/material';
import {
  GridColumnVisibilityModel,
  GridRenderCellParams,
  GridRowClassNameParams,
  GridTreeNodeWithRender,
} from '@mui/x-data-grid-pro';
import { DataGridTable } from '@shared/ui/data-grid-table';
import {
  $columns,
  $pagination,
  $pinnedColumns,
  $tableData,
  $visibilityColumns,
  changePinnedColumns,
  columnsWidthChanged,
  orderColumnsChanged,
  sortChanged,
  visibilityColumnsChanged,
  $isLoading,
  openRowChanged,
  $selectRow,
  selectionRowChanged,
  $count,
  paginationModelChanged,
  $openRow,
} from '../../models';
import { BasicPopover } from '../../molecules/basicPopover';
import { TableToolbar } from '../table-toolbar';
import { StylePopover, StyledWrapper } from './styled';

export const Table: FC = memo(() => {
  const { t } = useTranslation();
  const [
    tableData,
    pagination,
    visibilityColumns,
    columns,
    pinnedColumns,
    isLoading,
    selectRow,
    openRow,
    count,
  ] = useUnit([
    $tableData,
    $pagination,
    $visibilityColumns,
    $columns,
    $pinnedColumns,
    $isLoading,
    $selectRow,
    $openRow,
    $count,
  ]);

  const ignoredIndex = columns.findIndex(
    (column) => column.field === 'debtor_is_ignored'
  );

  const residentsFullNameIndex = columns.findIndex(
    (column) => column.field === 'residents_full_name'
  );

  columns[ignoredIndex].renderCell = ({ value }: any) => {
    return value ? (
      <Chip label={t('DebtorsList.Ignored')} color="success" size="small" />
    ) : (
      <Chip label={t('DebtorsList.Debtor')} color="error" size="small" />
    );
  };

  columns[residentsFullNameIndex].renderCell = (
    params: GridRenderCellParams<any, any, any, GridTreeNodeWithRender>
  ) => {
    return (
      <StylePopover>
        <Chip label={params.value[0]} size="small" />

        <BasicPopover values={params.value.slice(1)} id={params.id} />
      </StylePopover>
    );
  };

  const onColumnVisibilityModelChange = (visibilityColumn: GridColumnVisibilityModel) => {
    visibilityColumnsChanged({
      ...visibilityColumns,
      ...visibilityColumn,
    });
  };

  const getRowClassName = ({ id }: GridRowClassNameParams<any>): string => {
    const baseStyle =
      id === openRow ? 'row-selection Mui-custom-opened' : 'row-selection';

    return baseStyle;
  };

  return (
    <StyledWrapper elevation={0}>
      <TableToolbar />

      <DataGridTable
        checkboxSelection
        rows={tableData}
        density="compact"
        columns={columns}
        rowCount={count}
        loading={isLoading}
        paginationModel={pagination}
        disableRowSelectionOnClick
        rowSelectionModel={selectRow}
        onRowSelectionModelChange={(ids) => selectionRowChanged(ids as number[])}
        onRowClick={(row) => openRowChanged(row.id)}
        pinnedColumns={pinnedColumns}
        onSortModelChange={sortChanged}
        onPaginationModelChange={paginationModelChanged}
        onColumnWidthChange={columnsWidthChanged}
        columnVisibilityModel={visibilityColumns}
        onPinnedColumnsChange={changePinnedColumns}
        onColumnVisibilityModelChange={onColumnVisibilityModelChange}
        onColumnOrderChange={orderColumnsChanged}
        getRowClassName={getRowClassName}
      />
    </StyledWrapper>
  );
});
