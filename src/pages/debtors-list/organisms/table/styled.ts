import { Paper } from '@mui/material';
import styled from '@emotion/styled';

export const StylePopover = styled('div')`
  display: inline-flex;
  align-items: center;
  gap: 8px;
`;

export const StyledWrapper = styled(Paper)`
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 0 24px 0;
`;
