import { Chip, Paper } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import PersonIcon from '@mui/icons-material/Person';
import styled from '@emotion/styled';

export const StyledWrapper = styled(Paper)`
  height: 100%;
  position: relative;
`;

export const StyledCloseIcon = styled(CloseIcon)({
  cursor: 'pointer',
  background: '#fff',
});

export const StyledPersonIcon = styled(PersonIcon)({
  marginRight: '16px',
});

export const StyledDetailToolbar = styled.div`
  padding: 10px 10px 0;
  display: flex;
  justify-content: flex-end;
  position: sticky;
  top: 0;
  z-index: 5;
`;

export const StyledDetailBodyItem = styled.div((props: Record<string, unknown>) => ({
  borderBottom: props.noBorder ? '0' : '1px solid #e7e7ec',
  flex: props.fullWidth ? '0 0 calc(100% - 24px)' : '0 0 calc(50% - 24px)',
  margin: '0 12px',
  padding: '18px 0 22px',
  display: props.noFlex ? 'block' : 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
}));

export const StyledDetailBody = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  padding: 0 12px;
`;

export const StyledTitle = styled.h3`
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  color: #7d7d8e;
  padding: 0;
  margin: 0;
`;

export const StyledData = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  color: #3b3b50;
  padding: 0;
  margin: 8px 0 0;
  display: flex;
  align-items: center;
`;

export const StyledChip = styled(Chip)<{ ignored?: boolean }>`
  color: #fff;
  background: ${({ ignored }) => (ignored ? '#1bb169' : '#EB5757')};
`;
