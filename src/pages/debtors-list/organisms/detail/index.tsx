import { Link } from 'react-router-dom';
import { useUnit } from 'effector-react';
import i18n from '@shared/config/i18n';
import { ActionButton, Loader } from '@ui/atoms';
import { CustomScrollbar, ErrorMessage } from '@ui/index';
import { Debtor } from '../../interfaces';
import {
  $openedData,
  $openedError,
  $openedLoading,
  hideDetails,
  clearOpenErrorEvent,
} from '../../models';
import { getFormatDate } from '../../models/table/init';
import {
  StyledChip,
  StyledCloseIcon,
  StyledData,
  StyledDetailBody,
  StyledDetailBodyItem,
  StyledDetailToolbar,
  StyledPersonIcon,
  StyledTitle,
  StyledWrapper,
} from './styled';

const { t } = i18n;
const numberResidentsNeedScroll = 4;

const ResidentsData = ({ opened }: { opened: Debtor }) => {
  const { residents } = opened;

  if (residents.length > numberResidentsNeedScroll) {
    return (
      <StyledDetailBodyItem fullWidth style={{ height: '195px' }}>
        <StyledTitle>{t('Label.FullName') as string}</StyledTitle>
        <CustomScrollbar autoHide={false}>
          {residents.map((resident) => (
            <StyledData key={resident.id}>
              <StyledPersonIcon />
              <Link
                to={{
                  pathname: `/people/${resident.id}`,
                }}
                target="_blank"
              >
                {resident.full_name}
              </Link>
            </StyledData>
          ))}
        </CustomScrollbar>
      </StyledDetailBodyItem>
    );
  }

  return (
    <StyledDetailBodyItem fullWidth>
      <StyledTitle>{t('Label.FullName') as string}</StyledTitle>
      <>
        {residents.map((resident) => (
          <StyledData key={resident.id}>
            <StyledPersonIcon />
            <Link
              to={{
                pathname: `/people/${resident.id}`,
              }}
              target="_blank"
            >
              {resident.full_name}
            </Link>
          </StyledData>
        ))}
      </>
    </StyledDetailBodyItem>
  );
};

const DetailComponent = () => {
  const opened = useUnit($openedData);

  if (!opened) return null;

  const lastPayments = () => {
    if (!Array.isArray(opened.account.last_payment_at))
      return opened.account.last_payment_at
        ? getFormatDate(opened.account.last_payment_at)
        : '-';

    return opened.account.last_payment_at.map((item) => getFormatDate(item)).join(', ');
  };

  return (
    <>
      <StyledDetailToolbar>
        <StyledCloseIcon onClick={() => hideDetails()} />
      </StyledDetailToolbar>

      <StyledDetailBody>
        <StyledDetailBodyItem fullWidth>
          <StyledTitle>{t('DebtorsList.status') as string}</StyledTitle>
          <StyledData>
            {opened.debtor.is_ignored ? (
              <StyledChip label={t('DebtorsList.Ignored') as string} ignored />
            ) : (
              <StyledChip label={t('DebtorsList.Debtor') as string} />
            )}
          </StyledData>
        </StyledDetailBodyItem>

        <StyledDetailBodyItem>
          <StyledTitle>{t('Label.personalAccount') as any}</StyledTitle>
          <StyledData>{opened.account.number}</StyledData>
        </StyledDetailBodyItem>

        <StyledDetailBodyItem>
          <StyledTitle>{t('balance') as string}</StyledTitle>
          <StyledData>{opened.account.balance}</StyledData>
        </StyledDetailBodyItem>

        <StyledDetailBodyItem fullWidth>
          <StyledTitle>{t('Label.address') as string}</StyledTitle>
          <StyledData>{`${opened.building.title}, ${opened.apartment.number}`}</StyledData>
        </StyledDetailBodyItem>

        <ResidentsData opened={opened} />

        <StyledDetailBodyItem>
          <StyledTitle>{t('Label.LatestReceipt') as string}</StyledTitle>
          <StyledData>
            {opened.account.last_receipt_at
              ? getFormatDate(opened.account.last_receipt_at)
              : '-'}
          </StyledData>
        </StyledDetailBodyItem>

        <StyledDetailBodyItem>
          <StyledTitle>{t('DebtorsList.SendingLastNotification') as string}</StyledTitle>
          <StyledData>
            {opened.debtor.last_notification_at
              ? getFormatDate(opened.debtor.last_notification_at)
              : '-'}
          </StyledData>
        </StyledDetailBodyItem>

        <StyledDetailBodyItem noBorder noFlex>
          {opened.account.last_receipt_at && (
            <Link
              to={{
                pathname: '/debtors-receipts',
                state: {
                  search: opened.account.number,
                },
              }}
              style={{ textDecoration: 'none' }}
            >
              <ActionButton>{t('DebtorsList.Receipts') as string}</ActionButton>
            </Link>
          )}
        </StyledDetailBodyItem>

        <StyledDetailBodyItem noBorder noFlex>
          {opened.debtor.last_notification_at && (
            <Link
              to={{
                pathname: '/debtors-notifications',
                state: {
                  search: opened.account.number,
                },
              }}
              style={{ textDecoration: 'none' }}
            >
              <ActionButton>{t('DebtorsList.Notifications') as string}</ActionButton>
            </Link>
          )}
        </StyledDetailBodyItem>

        <StyledDetailBodyItem>
          <StyledTitle>{t('DebtorsList.LastPaymentDate') as string}</StyledTitle>
          <StyledData>{lastPayments()}</StyledData>
        </StyledDetailBodyItem>
      </StyledDetailBody>
    </>
  );
};

const Detail = () => {
  const [openedLoading, openedError] = useUnit([$openedLoading, $openedError]);

  return (
    <StyledWrapper elevation={0}>
      <Loader isLoading={openedLoading} />

      {openedError && (
        <ErrorMessage
          isOpen={openedError}
          onClose={clearOpenErrorEvent}
          error={openedError.message}
        />
      )}

      <CustomScrollbar autoHide>
        <DetailComponent />
      </CustomScrollbar>
    </StyledWrapper>
  );
};

export { Detail };
