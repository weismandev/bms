import { memo } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ApartmentSelectField } from '@features/apartment-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectField,
} from '@ui/index';
import { IgnoredStatus } from '../../interfaces';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter/model';
import { StyledWrapper, useStyles } from './style';

interface SelectOptions {
  id: string;
  title: string;
}

const { t } = i18n;

const ignoreModeSelectField = (props: Record<string, unknown>) => {
  const options: SelectOptions[] = [
    { id: IgnoredStatus.all, title: t('DebtorsList.FilterAll') },
    { id: IgnoredStatus.ignored, title: t('DebtorsList.FilterIgnored') },
    { id: IgnoredStatus.notIgnored, title: t('DebtorsList.FilterNotIgnored') },
  ];

  return (
    <div>
      <SelectField options={options} {...props} />
    </div>
  );
};

const Filter = memo(() => {
  const filters = useUnit($filters);

  const classes = useStyles();

  return (
    <StyledWrapper elevation={0}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          validationSchema={{}}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <Form className={classes.form}>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('RC')}
                placeholder={t('selectRC')}
              />

              <Field
                name="houses"
                complex={
                  Array.isArray(values?.objects) &&
                  values.objects.length > 0 &&
                  values.objects
                }
                component={HouseSelectField}
                onChange={(value: any) => {
                  setFieldValue('houses', value);
                }}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />

              <Field
                name="apartments"
                building_ids={
                  Array.isArray(values?.houses) &&
                  values.houses.map((house: { id: number }) => house.id)
                }
                component={ApartmentSelectField}
                label={t('Label.flat')}
                placeholder={t('selectThatApartment')}
                isMulti
              />

              <Field
                name="ignore_mode"
                component={ignoreModeSelectField}
                label={t('DebtorsList.status')}
                placeholder={t('DebtorsList.SelectDebtorType')}
                onChange={(value: SelectOptions) => {
                  if (!value) {
                    setFieldValue('ignore_mode', IgnoredStatus.all);
                    return;
                  }

                  setFieldValue('ignore_mode', value.id);
                }}
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </StyledWrapper>
  );
});

export { Filter };
