import { Paper } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import styled from '@emotion/styled';

export const StyledWrapper = styled(Paper)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const useStyles = makeStyles(() => ({
  form: {
    padding: 24,
    paddingTop: 0,
  },
  title: {
    color: '#8F91A3',
    marginBottom: 12,
    fontSize: 14,
  },
  dateField: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  dateDivider: {
    margin: '7px 10px 0',
    color: '#8F91A3',
  },
}));

export { useStyles };
