import i18n from '@shared/config/i18n';
import * as Yup from 'yup';

const { t } = i18n;

function testManagementCompanyOrEmailOrSms(this: any): boolean {
  if (!this.parent.management_company && !this.parent.email && !this.parent.sms) {
    return this.createError();
  }

  return true;
}

function oneChannelIsMandatory(this: any) {
  return Yup.boolean().test(
    'oneChannelIsMandatory',
    'Выберите канал рассылки сообщения!',
    testManagementCompanyOrEmailOrSms
  );
}

export const validationSchema = Yup.object().shape({
  message: Yup.string().required(t('thisIsRequiredField')),
  management_company: oneChannelIsMandatory(),
});
