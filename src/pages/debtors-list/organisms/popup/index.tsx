import { Link } from 'react-router-dom';
import { useUnit } from 'effector-react';
import { ErrorMessage, Field, FieldProps, Form, Formik } from 'formik';
import { FormControlLabel } from '@mui/material';
import { Check, Clear } from '@mui/icons-material';
import { NotificationText } from '@features/debtors-settings/organisms/viewEditData/NotofocationText';
import i18n from '@shared/config/i18n';
import { ActionButton, Checkbox, Loader, Wrapper } from '@ui/atoms';
import { Channels, NotesFormFields, PopupTypes } from '../../interfaces';
import {
  $initialValuesNotes,
  $popupNotesStatus,
  $preferencesNotifications,
  $preferencesSmsProvider,
  $sendNotesLoading,
  changePopupNotesStatus,
  resetInitialValuesNotes,
  sendNotes,
} from '../../models';
import {
  StyledAlertMessage,
  StyledButtonsBlock,
  StyledCheckboxContainer,
  StyledCheckboxErrorMessage,
  StyledCheckboxItem,
  StyledCheckboxTitle,
  StyledCheckCircleIcon,
  StyledCloseIcon,
  StyledDataBlock,
  StyledHeader,
  StyledHeaderTitle,
  StyledNoteData,
  StyledPopup,
  StyledSettingsIcon,
  StyledSettingsItem,
  StyledSuccessMessage,
  StyledToSettings,
} from './styled';
import { validationSchema } from './validation';

const { t } = i18n;

const CustomCheckbox = (props: Record<string, any>) => {
  const { name, label, disabled } = props;

  return (
    <Field
      name={name}
      render={(props: Record<string, any>) => {
        const { form, field } = props;
        return (
          <FormControlLabel
            label={label}
            control={
              <Checkbox
                size="small"
                onChange={() => form.setFieldValue(field.name, !field.value)}
                checked={field.value}
                disabled={disabled}
              />
            }
          />
        );
      }}
    />
  );
};

const FormNotes = (props: Record<string, any>) => {
  const { closePopup } = props;
  const [sendNotesLoading, preferencesNotifications, preferencesSmsProvider] = useUnit([
    $sendNotesLoading,
    $preferencesNotifications,
    $preferencesSmsProvider,
  ]);

  const isSms = !!preferencesSmsProvider?.credentials?.app_id;

  const initialValues: NotesFormFields = {
    message: preferencesNotifications?.text ?? '',
    [Channels.management_company]: !!preferencesNotifications?.channels?.includes('push'),
    [Channels.email]: !!preferencesNotifications?.channels?.includes('email'),
    [Channels.sms]: isSms && !!preferencesNotifications?.channels?.includes('sms'),
  };

  const idDataLoaded = preferencesNotifications && preferencesSmsProvider;

  return (
    <Wrapper
      style={{ height: '100%', boxShadow: 'none', marginTop: 24, borderRadius: 0 }}
    >
      <Loader isLoading={!idDataLoaded} />

      {idDataLoaded && (
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={sendNotes}
          enableReinitialize
          render={() => (
            <Form>
              <Field
                name="message"
                render={(props: FieldProps) => {
                  const { form, field } = props;
                  return (
                    <NotificationText
                      text={field.value ?? ''}
                      setFieldValue={form.setFieldValue}
                      variables={preferencesNotifications.variables}
                      name="message"
                      label={t('Label.comment') as string}
                      divider={false}
                      placeholder={t('FillField')}
                      multiline
                      rows={4}
                    />
                  );
                }}
              />

              <StyledCheckboxContainer>
                <StyledCheckboxTitle>
                  {t('DebtorsList.SendNotifications') as string}
                </StyledCheckboxTitle>

                <StyledCheckboxItem>
                  <CustomCheckbox
                    name="management_company"
                    label={t('DebtorsList.toApp')}
                    {...props}
                  />
                </StyledCheckboxItem>
                <StyledCheckboxItem>
                  <CustomCheckbox
                    name="sms"
                    label={t('DebtorsList.toSms')}
                    {...props}
                    disabled={!isSms}
                  />
                </StyledCheckboxItem>
                {!isSms && (
                  <StyledNoteData>
                    {`${t('DebtorsList.functionalityOfSendingSms')} `}
                    <Link
                      to={{
                        pathname: '/debtors-settings',
                      }}
                      style={{ textDecoration: 'none' }}
                      target="_blank"
                    >
                      {t('DebtorsList.SmsGateway') as string}
                    </Link>
                  </StyledNoteData>
                )}
                <StyledCheckboxItem>
                  <CustomCheckbox
                    name="email"
                    label={t('DebtorsList.toEmail')}
                    {...props}
                  />
                </StyledCheckboxItem>
                <ErrorMessage
                  name="management_company"
                  component={StyledCheckboxErrorMessage}
                />

                <StyledSettingsItem>
                  <StyledSettingsIcon sx={{ color: '#0394e3' }} />
                  <StyledToSettings
                    onClick={() => changePopupNotesStatus(PopupTypes.transitionAlert)}
                  >
                    {t('DebtorsList.ChangeSettings') as string}
                  </StyledToSettings>
                </StyledSettingsItem>

                <StyledButtonsBlock>
                  <ActionButton
                    kind="positive"
                    type="submit"
                    disabled={sendNotesLoading}
                    style={{ marginRight: 12 }}
                  >
                    <Check />
                    {t('DebtorsList.sendNotification')}
                  </ActionButton>

                  <ActionButton onClick={closePopup}>
                    <Clear />
                    {t('Cancellation')}
                  </ActionButton>
                </StyledButtonsBlock>
              </StyledCheckboxContainer>
            </Form>
          )}
        />
      )}
    </Wrapper>
  );
};

const PopupInfo = (props: Record<string, any>) => {
  const { closePopup } = props;

  return (
    <StyledDataBlock onClick={(e: React.MouseEvent) => e.stopPropagation()}>
      <StyledHeader>
        <StyledHeaderTitle>{t('DebtorsList.DebtNotice') as string}</StyledHeaderTitle>
        <StyledCloseIcon onClick={closePopup} />
      </StyledHeader>

      <FormNotes closePopup={closePopup} />
    </StyledDataBlock>
  );
};

const PopupAlert = () => {
  return (
    <StyledDataBlock onClick={(e: React.MouseEvent) => e.stopPropagation()} narrow>
      <StyledHeader>
        <StyledHeaderTitle>{t('Notification') as string}</StyledHeaderTitle>
        <StyledCloseIcon onClick={() => changePopupNotesStatus(PopupTypes.info)} />
      </StyledHeader>

      <StyledAlertMessage>{t('DebtorsList.goToSettings') as string}</StyledAlertMessage>

      <StyledButtonsBlock>
        <Link
          to={{
            pathname: '/debtors-settings',
          }}
          style={{ textDecoration: 'none' }}
        >
          <ActionButton kind="positive">
            <Check />
            {t('DebtorsList.Confirm')}
          </ActionButton>
        </Link>

        <ActionButton onClick={() => changePopupNotesStatus(PopupTypes.info)}>
          <Clear />
          {t('Cancellation')}
        </ActionButton>
      </StyledButtonsBlock>
    </StyledDataBlock>
  );
};

const PopupSent = () => {
  return (
    <StyledDataBlock narrow center onClick={(e: React.MouseEvent) => e.stopPropagation()}>
      <StyledHeader>
        <StyledHeaderTitle></StyledHeaderTitle>
        <StyledCloseIcon onClick={() => changePopupNotesStatus(null)} />
      </StyledHeader>

      <StyledAlertMessage>
        <StyledCheckCircleIcon />
        <StyledSuccessMessage>
          {t('DebtorsList.NoticeIsSent') as string}
        </StyledSuccessMessage>
      </StyledAlertMessage>
    </StyledDataBlock>
  );
};

const PopupRoot = () => {
  const popupNotesStatus = useUnit($popupNotesStatus);

  if (!popupNotesStatus) return null;

  const closePopup = () => {
    resetInitialValuesNotes;
    changePopupNotesStatus(null);
  };

  return (
    <StyledPopup onClick={closePopup}>
      {popupNotesStatus === PopupTypes.info && <PopupInfo closePopup={closePopup} />}

      {popupNotesStatus === PopupTypes.transitionAlert && <PopupAlert />}

      {popupNotesStatus === PopupTypes.sentResult && <PopupSent />}
    </StyledPopup>
  );
};

export { PopupRoot };
