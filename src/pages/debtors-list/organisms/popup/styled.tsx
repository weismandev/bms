import styled from '@emotion/styled';
import CloseIcon from '@mui/icons-material/Close';
import { InputField } from '@ui/molecules';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import SettingsIcon from '@mui/icons-material/Settings';

export const StyledDataBlock = styled.div((props: Record<string, unknown>) => ({
  width: props.narrow ? '360px' : '700px',
  backgroundColor: '#fff',
  boxShadow: '0px 4px 15px #6A6A6E',
  borderRadius: '15px',
  padding: '18px 24px',
  textAlign: props.center ? 'center' : 'inherit',
}));

export const StyledPopup = styled.div(() => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.2);',
  zIndex: 100,
}));

export const StyledHeader = styled.div(() => ({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}));

export const StyledHeaderTitle = styled.div`
  font-weight: 700;
  font-size: 24px;
  line-height: 28px;
  color: #3b3b50;
`;

export const StyledCloseIcon = styled(CloseIcon)({
  cursor: 'pointer',
});

export const StyledInputField = styled(InputField)({
  padding: '8px 14px',
  fontWeight: 300,
  fontSize: '13px',
  lineHeight: '16px',
  color: '#65657B',
});

export const StyledCheckboxContainer = styled.div`
  margin-top: 24px;
`;

export const StyledCheckboxTitle = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 16px;
  color: #3b3b50;
  margin-bottom: 22px;
`;

export const StyledCheckboxItem = styled.div`
  margin: 8px 0;
  padding-left: 2px;
`;

export const StyledSettingsItem = styled.div(() => ({
  marginTop: 31,
  display: 'flex',
  alignItems: 'center',
}));

export const StyledSettingsIcon = styled(SettingsIcon)`
  color: #0394e3;
`;

export const StyledToSettings = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  color: #0394e3;
  margin-left: 9px;
  cursor: pointer;
`;

export const StyledButtonsBlock = styled.div`
  margin-top: 31px;
  display: flex;
  align-items: center;
  padding-bottom: 7px;
  > * {
    margin-right: 16px;
  }
`;

export const StyledAlertMessage = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: #767676;
  margin-top: 14px;
`;

export const StyledCheckboxErrorMessage = styled.p`
  color: #d32f2f;
  font-size: 0.75rem;
`;

export const StyledSuccessMessage = styled.span`
  display: block;
  font-weight: 700;
  font-size: 24px;
  line-height: 28px;
  color: #1bb169;
  margin-top: 23px;
`;

export const StyledCheckCircleIcon = styled(CheckCircleIcon)({
  color: '#1BB169',
  fontSize: '105px',
});

export const StyledNoteData = styled.p`
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
  color: #aaaaaa;
  a {
    color: #0394e3;
  }
`;
