export enum Tabs {
  'ignored' = 1,
  'notIgnored' = 0,
}

export enum IgnoredStatus {
  all = 'all',
  ignored = 'ignored',
  notIgnored = 'not-ignored',
}
export interface Filter {
  complexes?: (string | number)[];
  buildings?: (string | number)[];
  apartments?: (string | number)[];
}

export interface Pag {
  page?: number | string;
  per_page?: number | string;
}

export interface Sort {
  sort?: string;
  column?: string;
}

export interface IgnoreRestoreDebtors {
  debtors: number[];
}

export interface Search {
  search?: string;
}

export type DebitorsPayload = Filter &
  Pag &
  Sort &
  Search & { ignore_mode: IgnoredStatus };

export type DebitorsExportPayload = DebitorsPayload & { email: string };
