export interface TableDataItem {
  id: number | string;
  debtor_id: number | string;
  debtor_is_ignored: boolean;
  account_number: string;
  building_title: string;
  residents_full_name: string[];
  account_balance: number;
  account_last_receipt_at: string;
  account_last_payment_at: string;
  debtor_last_notification_at: string;
}
