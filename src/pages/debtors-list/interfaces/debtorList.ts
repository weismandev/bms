export interface Meta {
  total: number;
  pages: number;
  page: number;
  perPage: number;
}

export interface DebtorData {
  id: number;
  is_ignored: boolean;
  last_notification_at: string;
}

export interface Complex {
  id: number;
  title: string;
}

export interface Building {
  id: number;
  title: string;
}

export interface Apartment {
  id: number;
  number: string;
}

export interface Account {
  number: string;
  balance: number;
  last_receipt_at: string;
  last_payment_at?: any;
}

export interface Resident {
  id: number;
  full_name: string;
}

export interface Debtor {
  debtor: DebtorData;
  complex: Complex;
  building: Building;
  apartment: Apartment;
  account: Account;
  residents: Resident[];
}

export interface DebtorsData {
  meta: Meta;
  debtors: Debtor[];
}
