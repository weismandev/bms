export enum PopupTypes {
  info = 'info',
  transitionAlert = 'transitionAlert',
  sentResult = 'sentResult',
}

export enum Channels {
  management_company = 'management_company',
  email = 'email',
  sms = 'sms',
}

export interface NotesFormFields {
  message: string;
  [Channels.management_company]: boolean;
  [Channels.email]: boolean;
  [Channels.sms]: boolean;
}

export interface SendNotificationBatch {
  template: string;
  debtors: (number | string)[];
  channels: Channels[];
}
