import { api } from '../../../api/api2';
import {
  DebitorsExportPayload,
  DebitorsPayload,
  IgnoreRestoreDebtors,
  SendNotificationBatch,
} from '../interfaces';

const getDebtorsList = ({
  payload,
  signal,
}: {
  payload: DebitorsPayload;
  signal: AbortController['signal'];
}) => api.v1('get', 'debtors/list', payload, signal);

const getDebtorsItem = (payload: { id: number | string }) =>
  api.v1('get', 'debtors/item', payload);

const setIgnore = (payload: IgnoreRestoreDebtors) =>
  api.v1('post', 'debtors/ignore', payload);

const setRestore = (payload: IgnoreRestoreDebtors) =>
  api.v1('post', 'debtors/restore', payload);

const sendNotificationBatch = (payload: SendNotificationBatch) =>
  api.v1('post', 'debtors/notifications/send-batch', payload);

const sendlistExport = (payload: DebitorsExportPayload) =>
  api.v1('get', 'debtors/list/export', payload);

export const debitorsApi = {
  getDebtorsList,
  getDebtorsItem,
  setIgnore,
  setRestore,
  sendNotificationBatch,
  sendlistExport,
};
