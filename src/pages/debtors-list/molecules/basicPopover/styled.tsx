import { styled } from '@mui/material/styles';

export const Popover = styled('div')``;

export const PopoverData = styled('div')`
  padding: 0;
  display: flex;
  flex-direction: column;
  gap: 8px;
`;
