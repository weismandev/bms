import React from 'react';

export default (props) => (
  <svg
    width="22"
    height="24"
    viewBox="0 0 22 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M6 2C5.44772 2 5 2.44772 5 3V21C5 21.5523 5.44772 22 6 22H16C16.5523 22 17 21.5523 17 21V3C17 2.44772 16.5523 2 16 2H6ZM15.0393 4.03432H6.96143V19.9848H15.0393V4.03432Z"
      fill="#1BB169"
    />
    <circle cx="8.99162" cy="12.016" r="0.999437" fill="#1BB169" />
    <rect x="9.62402" y="4.97754" width="3" height="2" rx="1" fill="#1BB169" />
  </svg>
);
