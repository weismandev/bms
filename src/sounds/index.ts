import alert from './alert.mp3';
import notification from './notification.mp3';
import warning from './warning.mp3';

export { alert, notification, warning };
