import { post } from './wrapper';

export const POST = (url, body) => post(url, body);
