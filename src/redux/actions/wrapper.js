import axios from 'axios';
import { getCookieNameWithBuildType, cookieMap } from '@tools/cookie';

//инстанс axios с настройками
const request = axios.create({
  baseURL: `${process.env.API_URL}/v1`,
  // timeout: 6000,
  withCredentials: false,
});

const cookieToken = getCookieNameWithBuildType('bms_token');

export const post = (url, body) => {
  const TOKEN = cookieMap(cookieToken) ? `token=${cookieMap(cookieToken)}` : '';

  return request
    .post(`${url}?${TOKEN}`, body)
    .then((response) => response)
    .catch((error) => console.log(error));
};
