import { useState } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Check } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { ErrorMessage, ActionButton } from '../../../../ui';
import { RightsSelect } from '../../../rights-select';
import {
  resetAndNotify,
  changeConfirmDialogVisibility,
  changeErrorDialogVisibility,
  changeResultDialogVisibility,
  $error,
  $isConfirmDialogOpen,
  $isErrorDialogOpen,
  $isResultDialogOpen,
  Gate,
  confirm,
} from '../../models';
import { ConfirmDialog, ResultDialog } from '../../molecules';

const { t } = i18n;

const stores = combine({
  error: $error,
  isErrorDialogOpen: $isErrorDialogOpen,
  isConfirmDialogOpen: $isConfirmDialogOpen,
  isResultDialogOpen: $isResultDialogOpen,
});

const useStyles = makeStyles({
  selectRights: {
    minWidth: 240,
    marginBottom: 20,
  },
  applyButton: {
    padding: '10px 0',
  },
});

export const ResetRights = (props) => {
  const { userdata_ids = [], updateUserList } = props;

  const { error, isErrorDialogOpen, isConfirmDialogOpen, isResultDialogOpen } =
    useStore(stores);

  const classes = useStyles();
  const [right, setRight] = useState();

  const isDisabled = !userdata_ids.length;
  return (
    <>
      <Gate
        userdata_ids={userdata_ids}
        right={right && right.id}
        updateUserList={updateUserList}
      />

      <RightsSelect
        divider={false}
        label={t('ChangeRights')}
        placeholder={t('SelectNewValue')}
        labelPlacement="top"
        className={classes.selectRights}
        value={right}
        isDisabled={isDisabled}
        onChange={(value) => setRight(value)}
      />

      <ActionButton
        className={classes.applyButton}
        icon={<Check />}
        onClick={resetAndNotify}
        disabled={isDisabled}
        kind="positive"
        type="submit"
      >
        <div style={{ marginLeft: 5 }}>Применить</div>
      </ActionButton>
      <ErrorMessage
        error={error}
        isOpen={isErrorDialogOpen}
        onClose={() => changeErrorDialogVisibility(false)}
      />
      <ConfirmDialog
        confirm={confirm}
        isOpen={isConfirmDialogOpen}
        right={right && right.title}
        onClose={() => changeConfirmDialogVisibility(false)}
      />
      <ResultDialog
        isOpen={isResultDialogOpen}
        onClose={() => changeResultDialogVisibility(false)}
      />
    </>
  );
};
