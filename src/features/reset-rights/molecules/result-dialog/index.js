import CheckIcon from '@mui/icons-material/Check';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Modal, ActionIconButton } from '../../../../ui';

const { t } = i18n;

const useStyles = makeStyles({
  positive: {
    color: '#00A400',
    fontSize: 24,
    fontWeight: 'bold',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 100,
    padding: 2,
    alignItems: 'center',
  },
});

function ResultDialog(props) {
  const { isOpen, onClose } = props;
  const classes = useStyles();

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      content={
        <div className={classes.container}>
          <ActionIconButton kind="positive" iconSize="40px">
            <CheckIcon />
          </ActionIconButton>
          <div className={classes.positive}>{t('RightsChanged')}</div>
        </div>
      }
    />
  );
}

export { ResultDialog };
