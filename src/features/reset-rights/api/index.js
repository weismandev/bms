import { api } from '../../../api/api2';

const resetRights = (payload) =>
  api.v1('post', 'bulk-operations/set-residents-rights', payload);

export const resetApi = { resetRights };
