import {
  createEffect,
  createStore,
  createEvent,
  restore,
  forward,
  sample,
  guard,
} from 'effector';
import { createGate } from 'effector-react';
import { resetApi } from '../api';

const Gate = createGate();

const confirm = createEvent();
const resetAndNotify = createEvent();
const changeConfirmDialogVisibility = createEvent();
const changeErrorDialogVisibility = createEvent();
const changeResultDialogVisibility = createEvent();

const errorClosed = guard({
  source: changeErrorDialogVisibility,
  filter: (visibility) => !visibility,
});

const fxResetAndNotify = createEffect();

const $error = createStore(null);
const $isConfirmDialogOpen = restore(changeConfirmDialogVisibility, false);
const $isErrorDialogOpen = restore(changeErrorDialogVisibility, false);
const $isResultDialogOpen = restore(changeResultDialogVisibility, false);

fxResetAndNotify.use(({ updateUserList, ...payload }) =>
  resetApi.resetRights(payload).then(() => updateUserList())
);

$error.on(fxResetAndNotify.failData, (_, error) => error).reset(errorClosed);
$isErrorDialogOpen.on(fxResetAndNotify.fail, () => true);
$isResultDialogOpen.on(fxResetAndNotify.done, () => true);
$isConfirmDialogOpen.on(resetAndNotify, () => true).on(confirm, () => false);

forward({
  from: sample(Gate.state, confirm),
  to: fxResetAndNotify,
});

export {
  resetAndNotify,
  changeConfirmDialogVisibility,
  changeErrorDialogVisibility,
  changeResultDialogVisibility,
  $error,
  $isConfirmDialogOpen,
  $isErrorDialogOpen,
  $isResultDialogOpen,
  Gate,
  confirm,
};
