import { forward, sample, guard, merge, createEffect, restore } from 'effector';
import { fxUpdateCameraAccess } from '../../cameras/models/access.model';
import { getComplexes, getBuildings, getApartments, getProperties } from '../api';
import {
  changeComplexSelection,
  changeBuildingSelection,
  changeApartmentSelection,
  changePropertiesSelection,
  fxGetComplexes,
  fxGetBuildings,
  fxGetApartments,
  fxGetProperties,
  $complexes,
  $buildings,
  $apartments,
  $properties,
  $allDataLoaded,
  TreeGate,
  compexChildrenData,
  buildingsChildrenData,
} from './tree.model';

const fxFormatApartments = createEffect().use(formatApartments);

fxGetComplexes.use(getComplexes);
fxGetBuildings.use(getBuildings);
fxGetApartments.use(getApartments);
fxGetProperties.use(getProperties);

const setLoad = () => true;

const complexesLoaded = restore(fxGetComplexes.done.map(setLoad), false).reset(
  TreeGate.close
);
const buildingsLoaded = restore(fxGetBuildings.done.map(setLoad), false).reset(
  TreeGate.close
);
const apartmentsLoaded = restore(fxFormatApartments.done.map(setLoad), false).reset(
  TreeGate.close
);

const propertiesLoaded = restore(fxGetProperties.done.map(setLoad), false).reset(
  TreeGate.close
);

$allDataLoaded
  .on(
    guard({
      source: sample({
        source: [complexesLoaded, buildingsLoaded, apartmentsLoaded, propertiesLoaded],
        fn: (data) => data.every(Boolean),
      }),
      filter: Boolean,
    }),
    (_, res) => res
  )
  .on(fxUpdateCameraAccess.pending, (_, pending) => !pending)
  .reset(TreeGate.close);

/** После загрузки всех данных добавляю признаки наличия children */
sample({
  source: { buildings: $buildings, apartments: $apartments },
  clock: $allDataLoaded,
  target: [compexChildrenData, buildingsChildrenData],
});

const createCollectorFor =
  (name) =>
  ({ tree, data, children, allDataLoaded }) => {
    let collection;

    const knowWhatToUpdate = allDataLoaded && tree.values && Boolean(tree.values[name]);

    if (knowWhatToUpdate) {
      const dataToChange = Object.values(data).reduce((acc, i) => {
        const checked = tree.values[name].includes(i.id);
        const hasCheckedChildren =
          Boolean(children) &&
          Object.values(children).some(
            (j) => j.parentId === i.id && i.hasCheckedChildren
          );

        const hasDiff =
          (checked && !i.checked) ||
          (!checked && i.checked) ||
          (hasCheckedChildren && !i.hasCheckedChildren) ||
          (!hasCheckedChildren && i.hasCheckedChildren);

        const result = hasDiff ? [...acc, { ...i, checked, hasCheckedChildren }] : acc;

        return result;
      }, []);

      collection = { allDataLoaded, dataToChange };
    } else {
      collection = { allDataLoaded, dataToChange: [] };
    }

    return collection;
  };

const hasUpdates = ({ allDataLoaded, dataToChange }) =>
  allDataLoaded && dataToChange.length > 0;

// setting inner complexes values

const collectComplexesToChange = createCollectorFor('complexes');

const innerComplexesValuesChanged = sample({
  source: {
    tree: TreeGate.state,
    data: $complexes,
    children: $buildings,
    allDataLoaded: $allDataLoaded,
  },
  clock: [$allDataLoaded, TreeGate.state],
  fn: collectComplexesToChange,
});

guard({
  source: innerComplexesValuesChanged,
  filter: hasUpdates,
  target: changeComplexSelection.prepend(({ dataToChange }) => dataToChange),
});

// setting inner buildings values

const collectBuildingsToChange = createCollectorFor('buildings');

const innerBuildingsValuesChanged = sample({
  source: {
    tree: TreeGate.state,
    data: $buildings,
    children: $apartments,
    allDataLoaded: $allDataLoaded,
  },
  clock: [$allDataLoaded, TreeGate.state],
  fn: collectBuildingsToChange,
});

guard({
  source: innerBuildingsValuesChanged,
  filter: hasUpdates,
  target: changeBuildingSelection.prepend(({ dataToChange }) => dataToChange),
});

// setting inner apartments values

const collectApartmentsToChange = createCollectorFor('apartments');

const innerApartmentsValuesChanged = sample({
  source: {
    tree: TreeGate.state,
    data: $apartments,
    children: null,
    allDataLoaded: $allDataLoaded,
  },
  clock: [$allDataLoaded, TreeGate.state],
  fn: collectApartmentsToChange,
});

guard({
  source: innerApartmentsValuesChanged,
  filter: hasUpdates,
  target: changeApartmentSelection.prepend(({ dataToChange }) => dataToChange),
});

// setting inner properties values

const collectPropertiesToChange = createCollectorFor('properties');

const innerPropertiesValuesChanged = sample({
  source: {
    tree: TreeGate.state,
    data: $properties,
    children: null,
    allDataLoaded: $allDataLoaded,
  },
  clock: [$allDataLoaded, TreeGate.state],
  fn: collectPropertiesToChange,
});

guard({
  source: innerPropertiesValuesChanged,
  filter: hasUpdates,
  target: changePropertiesSelection.prepend(({ dataToChange }) => dataToChange),
});

// update from child to parent

const apartmentsUpdated = guard({
  source: sample({
    source: { children: $apartments, parents: $buildings },
    clock: changeApartmentSelection,
    fn: preparePayloadToParentSelectionChange,
  }),
  filter: (dataToSelection) => dataToSelection.length > 0,
});

const propertiesUpdated = guard({
  source: sample({
    source: { children: $properties, parents: $buildings },
    clock: changePropertiesSelection,
    fn: preparePayloadToParentSelectionChange,
  }),
  filter: (dataToSelection) => dataToSelection.length > 0,
});

const buildingsUpdated = guard({
  source: sample({
    source: { children: $buildings, parents: $complexes },
    clock: merge([changeBuildingSelection, apartmentsUpdated, propertiesUpdated]),
    fn: preparePayloadToParentSelectionChange,
  }),
  filter: (dataToSelection) => dataToSelection.length > 0,
});

$complexes
  .on(fxGetComplexes.doneData, formatComplexes)
  .on([changeComplexSelection, buildingsUpdated], changeSelection)
  .on(compexChildrenData, addCompexChildrenData)
  .reset(TreeGate.close);

$buildings
  .on(fxGetBuildings.doneData, formatBuildings)
  .on([changeBuildingSelection, apartmentsUpdated, propertiesUpdated], changeSelection)
  .on(buildingsChildrenData, addBuildingsChildrenData)
  .reset(TreeGate.close);

$apartments
  .on(fxFormatApartments.doneData, (state, res) => res)
  .on(changeApartmentSelection, changeSelection)
  .reset(TreeGate.close);

$properties
  .on(fxGetProperties.doneData, formatProperties)
  .on(changePropertiesSelection, changeSelection)
  .reset(TreeGate.close);

forward({
  from: sample($apartments, fxGetApartments.doneData, (apartments, { items }) => ({
    apartments,
    items,
  })),
  to: fxFormatApartments,
});

forward({
  from: TreeGate.open,
  to: [fxGetComplexes, fxGetBuildings, fxGetApartments, fxGetProperties],
});

// update from parent to child

sample({
  source: $apartments,
  clock: changeBuildingSelection,
  fn: collectChildrenToSelectionChange,
});

guard({
  source: sample({
    source: $properties,
    clock: changeBuildingSelection,
    fn: collectChildrenToSelectionChange,
  }),
  filter: (dataToSelection) => dataToSelection.length > 0,
  target: changePropertiesSelection,
});

guard({
  source: sample({
    source: $buildings,
    clock: changeComplexSelection,
    fn: collectChildrenToSelectionChange,
  }),
  filter: (dataToSelection) => dataToSelection.length > 0,
  target: changeBuildingSelection,
});

const $selected = sample({
  source: {
    complexes: $complexes,
    buildings: $buildings,
    apartments: $apartments,
    properties: $properties,
  },

  fn: ({ complexes, buildings, apartments, properties }) => {
    return {
      complexes: Object.values(complexes).filter(isChecked),
      buildings: Object.values(buildings).filter(isChecked),
      apartments: Object.values(apartments).filter(isChecked),
      properties: Object.values(properties).filter(isChecked),
    };
  },
});

const target = guard({
  source: sample({
    source: {
      allDataLoaded: $allDataLoaded,
      tree: TreeGate.state,
    },
    clock: $selected,
    fn: ({ tree, allDataLoaded }, selected) => {
      return {
        allDataLoaded,
        tree,
        complexes: selected.complexes.map((i) => i.id),
        buildings: selected.buildings.map((i) => i.id),
        apartments: selected.apartments.map((i) => i.id),
        properties: selected.properties.map((i) => i.id),
      };
    },
  }),
  filter: ({ allDataLoaded }) => allDataLoaded,
});

target.watch(({ tree, allDataLoaded, ...values }) => {
  if (typeof tree.onChange === 'function') {
    tree.onChange(values);
  }
});

/* ======================== HELPERS ====================== */

function formatComplexes(state, { items }) {
  if (Array.isArray(items)) {
    return items.reduce(
      (acc, { id, title }) => ({
        ...acc,
        [id]: {
          id,
          title,
          checked: false,
          parentId: null,
          hasCheckedChildren: false,
        },
      }),
      state
    );
  }

  return state;
}

function isChecked(i) {
  return Boolean(i.checked);
}

function formatBuildings(state, { buildings }) {
  if (Array.isArray(buildings)) {
    return buildings.reduce(
      (acc, { id, title, complex }) => ({
        ...acc,
        [id]: {
          id,
          title,
          parentId: complex ? complex.id : null,
          checked: false,
          hasCheckedChildren: false,
        },
      }),
      state
    );
  }
  return state;
}

async function formatApartments({ apartments, items }) {
  if (Array.isArray(items)) {
    const promises = [];
    let i = 0;

    while (i <= items.length) {
      const slice = items.slice(i, i + 100);

      const promise = new Promise((res) => {
        res(
          slice.reduce(
            (acc, { apartment, building }) => ({
              ...acc,
              [apartment.id]: {
                type: 'apartment',
                id: apartment.id,
                title: apartment.title,
                parentId: building ? building.id : null,
                checked: false,
                hasCheckedChildren: false,
              },
            }),
            apartments
          )
        );
      });

      promises.push(promise);

      i += 100;
    }

    return Promise.all(promises).then((res) =>
      res.reduce((acc, i) => ({ ...acc, ...i }), {})
    );
  }

  return apartments;
}

function formatProperties(state, { items }) {
  if (Array.isArray(items)) {
    return items.reduce(
      (acc, { property: { id, title }, building }) => ({
        ...acc,
        [id]: {
          type: 'property',
          id,
          title,
          parentId: building?.id,
          checked: false,
          hasCheckedChildren: false,
        },
      }),
      state
    );
  }
  return state;
}

function changeSelection(state, data) {
  const changeItemSelection = (state, id, checked, hasCheckedChildren) => {
    return {
      ...state,
      [id]: {
        ...state[id],
        checked,
        hasCheckedChildren:
          hasCheckedChildren === undefined ? checked : hasCheckedChildren,
      },
    };
  };

  if (Array.isArray(data)) {
    return data.reduce(
      (acc, { id, checked, hasCheckedChildren }) =>
        changeItemSelection(acc, id, checked, hasCheckedChildren),
      state
    );
  }

  const { id, checked, hasCheckedChildren } = data;

  return changeItemSelection(state, id, checked, hasCheckedChildren);
}

function preparePayloadToParentSelectionChange({ children, parents }, data) {
  const selected = Array.isArray(data) ? data : [data];

  const result = Object.values(
    selected.reduce((acc, i) => {
      const childId = i.id;
      const parentId = children[childId].parentId;
      const parentChildren = Object.values(children).filter(
        (i) => i.parentId === parentId
      );

      if (acc[parentId]) {
        return acc;
      }
      const checked = parentChildren.every(isChecked);
      const hasCheckedChildren = parentChildren.some(
        (i) => isChecked(i) || i.hasCheckedChildren
      );

      if (
        parents[parentId].checked === checked &&
        parents[parentId].hasCheckedChildren === hasCheckedChildren
      ) {
        return acc;
      }

      return {
        ...acc,
        [parentId]: { id: parentId, checked, hasCheckedChildren },
      };
    }, {})
  );

  return result;
}

function collectChildrenToSelectionChange(children, data) {
  const selected = Array.isArray(data) ? data : [data];
  const selectedIds = selected.map((s) => s.id);
  const checked = selected[0].checked;

  const changedChildren = [];

  Object.values(children).forEach((child) => {
    if (!selectedIds.includes(child.parentId)) return;

    if (child.type === 'apartment') {
      child.checked = checked;
    }

    changedChildren.push({ id: child.id, checked });
  });

  return changedChildren;
}

/** Фнкция добавления поля наличия children для комплексов */
export function addCompexChildrenData(state, additional) {
  const buildings = additional.buildings;
  const complexParents = new Set();
  Object.keys(buildings).forEach((key) => {
    complexParents.add(buildings[key].parentId);
  });

  Object.keys(state).forEach((key) => {
    state[key].hasAnyChild = complexParents.has(Number(key));
  });
}

/** Фнкция добавления поля наличия children для зданий */
export function addBuildingsChildrenData(state, additional) {
  const apartments = additional.apartments;
  const buildingsParents = new Set();
  Object.keys(apartments).forEach((key) => {
    buildingsParents.add(apartments[key].parentId);
  });

  Object.keys(state).forEach((key) => {
    state[key].hasAnyChild = buildingsParents.has(Number(key));
  });
}
