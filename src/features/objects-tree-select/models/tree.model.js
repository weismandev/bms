import { createEvent, createStore, createEffect } from 'effector';
import { createGate } from 'effector-react';

const TreeGate = createGate();

const changeComplexSelection = createEvent();
const changeBuildingSelection = createEvent();
const changeApartmentSelection = createEvent();
const changePropertiesSelection = createEvent();

const compexChildrenData = createEvent();
const buildingsChildrenData = createEvent();

const fxGetComplexes = createEffect();
const fxGetBuildings = createEffect();
const fxGetApartments = createEffect();
const fxGetProperties = createEffect();

const $complexes = createStore({});
const $buildings = createStore({});
const $apartments = createStore({});
const $properties = createStore({});
const $allDataLoaded = createStore(false);

const $complexesList = $complexes.map((data) => Object.values(data));
const $buildingsList = $buildings.map((data) => Object.values(data));
const $apartmentsList = $apartments.map((data) => Object.values(data));
const $propertiesList = $properties.map((data) => Object.values(data));

export {
  changeComplexSelection,
  changeBuildingSelection,
  changeApartmentSelection,
  changePropertiesSelection,
  fxGetComplexes,
  fxGetBuildings,
  fxGetApartments,
  fxGetProperties,
  $complexes,
  $buildings,
  $apartments,
  $properties,
  $complexesList,
  $buildingsList,
  $apartmentsList,
  $propertiesList,
  $allDataLoaded,
  TreeGate,
  compexChildrenData,
  buildingsChildrenData,
};
