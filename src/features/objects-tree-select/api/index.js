import { api } from '../../../api/api2';

export const getComplexes = (payload = {}) => api.v1('get', 'complex/list', payload);

export const getBuildings = (payload = {}) => {
  return api.v1('get', 'buildings/get-list-available', {
    per_page: 10000,
    ...payload,
  });
};

export const getApartments = (payload = {}) =>
  api.v1('get', 'apartment/simple-list', payload);

export const getProperties = (payload = {}) =>
  api.v1('get', 'property/simple-list', payload);
