import {
  useState,
  forwardRef,
  memo,
  useCallback,
  cloneElement,
  useMemo,
  createContext,
  useContext,
} from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { FixedSizeList as List } from 'react-window';
import { useList, useStore } from 'effector-react';
import classnames from 'classnames';
import { Grid, IconButton, Collapse, Divider } from '@mui/material';
import { ExpandMore, Check, Block } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { SwitchControl, Loader } from '../../../../ui';
import {
  changeComplexSelection,
  changeBuildingSelection,
  changeApartmentSelection,
  changePropertiesSelection,
  $complexesList,
  $buildingsList,
  $apartmentsList,
  TreeGate,
  $allDataLoaded,
  $propertiesList,
} from '../../models/tree.model';

const useStyles = makeStyles({
  expand: {
    transform: 'rotate(-90deg)',
  },
  expandOpen: {
    transform: 'rotate(0deg)',
  },
});

const CustomScrollbars = ({ onScroll, forwardedRef, style, children }) => {
  const refSetter = useCallback((scrollbarsRef) => {
    if (scrollbarsRef) {
      forwardedRef(scrollbarsRef.view);
    } else {
      forwardedRef(null);
    }
  }, []);

  return (
    <Scrollbars
      autoHide
      ref={refSetter}
      style={{ ...style, overflow: 'hidden' }}
      onScroll={onScroll}
      renderView={(props) => (
        <div {...props} style={{ ...props.style, paddingRight: 12 }} />
      )}
    >
      <div style={{ paddingRight: 6, position: 'relative' }}>{children}</div>
    </Scrollbars>
  );
};

const CustomScrollbarsVirtualList = forwardRef((props, ref) => (
  <CustomScrollbars {...props} forwardedRef={ref} />
));

const ComplexesTreeNodes = (props) => {
  const complexes = useList(
    $complexesList,
    ({ id, title, checked, hasAnyChild, hasCheckedChildren }) => {
      return (
        <TreeNode
          id={id}
          title={title}
          checked={checked}
          hasCheckedChildren={hasCheckedChildren}
          onChange={changeComplexSelection}
        >
          {hasAnyChild ? cloneElement(props.children, { parentId: id }) : null}
        </TreeNode>
      );
    }
  );

  return <>{complexes}</>;
};

const BuildingsTreeNodes = (props) => {
  const buildings = useList(
    $buildingsList,
    ({ id, title, parentId, checked, hasAnyChild, hasCheckedChildren }) => {
      if (String(parentId) !== String(props.parentId)) {
        return null;
      }

      return (
        <TreeNode
          id={id}
          title={title}
          checked={checked}
          hasCheckedChildren={hasCheckedChildren}
          onChange={changeBuildingSelection}
        >
          {hasAnyChild ? cloneElement(props.children, { parentId: id }) : null}
        </TreeNode>
      );
    }
  );

  return <div style={{ paddingLeft: 12 }}>{buildings}</div>;
};

const ApartmentAndPropertyRow = (props) => {
  const { data, index, style = {} } = props;
  const { id, title, checked, hasCheckedChildren, type } = data[index];

  const memoStyle = useMemo(() => style, []);

  const changeHandler =
    type === 'property' ? changePropertiesSelection : changeApartmentSelection;

  return (
    <TreeNode
      id={id}
      title={title}
      checked={checked}
      hasCheckedChildren={hasCheckedChildren}
      onChange={changeHandler}
      style={memoStyle}
    />
  );
};

const ApartmentsAndPropertiesTreeNodes = (props) => {
  const apartmentsList = useStore($apartmentsList);
  const propertiesList = useStore($propertiesList);
  const data = apartmentsList.concat(propertiesList);

  const parentChildren = data.filter((i) => i.parentId === props.parentId);
  const nodeHeight = 50;
  const showedCount = 10;

  return (
    <div style={{ paddingLeft: 12 }}>
      <List
        outerElementType={CustomScrollbarsVirtualList}
        itemData={parentChildren}
        itemCount={parentChildren.length}
        width="100%"
        itemSize={nodeHeight}
        height={
          parentChildren.length > showedCount
            ? showedCount * nodeHeight
            : parentChildren.length * nodeHeight
        }
      >
        {ApartmentAndPropertyRow}
      </List>
    </div>
  );
};

const Context = createContext({});

const TreeNode = memo((props) => {
  const { id, title, checked, hasCheckedChildren, onChange, style } = props;

  const [expanded, setExpanded] = useState(false);
  const { expand, expandOpen } = useStyles();
  const isStatesLoaded = useStore($allDataLoaded);

  const { mode } = useContext(Context);

  const handleExpandClick = () => setExpanded((expanded) => !expanded);

  return isStatesLoaded ? (
    <div style={style}>
      <Grid container alignItems="center" wrap="nowrap">
        <Grid item style={{ minWidth: 34, height: 48 }}>
          {!!props.children && (
            <IconButton
              onClick={handleExpandClick}
              className={classnames(expand, { [expandOpen]: expanded })}
              size="large"
            >
              <ExpandMore />
            </IconButton>
          )}
        </Grid>

        <Grid item style={{ flexGrow: 1 }}>
          <SwitchControl
            label={title}
            mode={mode}
            divider={false}
            labelPlacement="start"
            value={checked}
            name={`${id}-${title}`}
            onChange={(e) => onChange({ id, checked: e.target.checked })}
            indeterminate={!checked && hasCheckedChildren}
            viewCheckedIcon={<Check style={{ color: 'green' }} />}
            viewUncheckedIcon={
              hasCheckedChildren ? (
                <Check style={{ color: 'gainsboro' }} />
              ) : (
                <Block style={{ color: 'red' }} />
              )
            }
          />
        </Grid>
      </Grid>
      <Divider />
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {props.children}
      </Collapse>
    </div>
  ) : null;
});

export const ObjectsTreeSelect = ({ mode = 'view', value, onChange }) => {
  const isStatesLoaded = useStore($allDataLoaded);

  return (
    <>
      {!isStatesLoaded && <Loader isLoading={!isStatesLoaded} />}

      <Context.Provider value={{ mode }}>
        <TreeGate
          values={value}
          onChange={(values) => {
            if (typeof onChange === 'function') {
              onChange(values);
            }
          }}
        />

        {isStatesLoaded && (
          <ComplexesTreeNodes>
            <BuildingsTreeNodes>
              <ApartmentsAndPropertiesTreeNodes />
            </BuildingsTreeNodes>
          </ComplexesTreeNodes>
        )}
      </Context.Provider>
    </>
  );
};
