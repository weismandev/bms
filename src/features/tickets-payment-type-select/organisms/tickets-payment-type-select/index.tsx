import React from 'react';

import { SelectControl, SelectField } from '@ui/index';

import { data } from '../../models/tickets-payment-type-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

export const TicketsPaymentTypeSelect: React.FC<Props> = (props) => (
  <SelectControl options={data} {...props} />
);

export const TicketsPaymentTypeSelectField: React.FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<TicketsPaymentTypeSelect />}
      onChange={onChange}
      {...props}
    />
  );
};
