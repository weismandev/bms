import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Toolbar, FoundInfo, Greedy, AddButton, FilterButton } from '@ui/index';
import { addClicked, changedFilterVisibility, $isFilterOpen } from '../../model';

const useStyles = makeStyles({
  toolbarRoot: {
    padding: '0 24px 24px 0',
    alignContent: 'center',
  },
});

export const ListToolbar = ({ count }) => {
  const isFilterOpen = useStore($isFilterOpen);

  const { toolbarRoot } = useStyles();

  return (
    <Toolbar className={toolbarRoot}>
      <FilterButton
        style={{ marginRight: 10 }}
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} />
      <Greedy />
      <AddButton onClick={addClicked} />
    </Toolbar>
  );
};
