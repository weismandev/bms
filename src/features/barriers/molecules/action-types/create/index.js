import { useGate, useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Loader, InputField, CustomScrollbar } from '@ui/index';
import { dynamicValidationSchema } from '../../../lib/dynamic-validation-schema';
import { $loading, createAction, actionEntity, ActionModalGate } from '../../../model';
import { ActionFields } from '../common/action-fields';

const { t } = i18n;

const useStyles = makeStyles({
  content: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 10,
  },
  submitButton: {
    marginTop: 'auto',
    marginRight: 24,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(85vh - 48px - 28px - 22px)',
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      marginBottom: 15,
    },
    paddingRight: 24,
  },
});

export const ActionCreateContent = () => {
  const classes = useStyles();
  useGate(ActionModalGate);

  const loading = useStore($loading);

  if (loading) {
    return <Loader isLoading />;
  }

  return (
    <div className={classes.content}>
      <Formik
        onSubmit={createAction}
        initialValues={actionEntity}
        validationSchema={dynamicValidationSchema}
        enableReinitialize
        render={({ values, setFieldValue }) => {
          return (
            <Form className={classes.form}>
              <CustomScrollbar>
                <div className={classes.wrapper}>
                  <Field
                    name="title"
                    component={InputField}
                    label={t('ActionName')}
                    placeholder={t('EnterNameAction')}
                    divider={null}
                  />
                  <ActionFields setFieldValue={setFieldValue} values={values} />
                </div>
              </CustomScrollbar>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submitButton}
              >
                {t('create')}
              </Button>
            </Form>
          );
        }}
      />
    </div>
  );
};
