import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { EquipmentSelectField } from '@features/equipment-select';
import { RizerSelectField } from '@features/rizer-select';
import i18n from '@shared/config/i18n';
import { InputField, SelectField, SwitchField } from '@ui/index';
import { hasInstruction } from '../../../lib/dynamic-validation-schema';
import {
  $actionTypes,
  $companyList,
  $directions,
  $loading,
  $space,
  $usersGroup,
} from '../../../model';

const { t } = i18n;

const $stores = combine({
  usersGroup: $usersGroup,
  companyList: $companyList,
  directions: $directions,
  loading: $loading,
  actionTypes: $actionTypes,
  space: $space,
});

export const ActionFields = ({ values, setFieldValue }) => {
  const { companyList, usersGroup, directions, space, actionTypes } = useStore($stores);

  const rizer = values.rizer?.id;
  const apartments = values.building?.apartments;

  const apartmentsForCurrentRizer =
    apartments && Array.isArray(apartments)
      ? apartments.filter(({ apartment }) => apartment.entrance === rizer)
      : [];

  return (
    <>
      <Field
        name="verify_parking_slot"
        component={SwitchField}
        label={t('verificationBypm')}
        labelPlacement="start"
        kind="switch"
        divider={null}
      />
      <Field
        name="verify_vehicle"
        component={SwitchField}
        label={t('VehicleVerification')}
        labelPlacement="start"
        kind="switch"
        divider={null}
      />
      <Field
        name="complex"
        component={SelectField}
        label={t('complex')}
        options={space}
        onChange={(value) => {
          setFieldValue('complex', value);
          setFieldValue('building', '');
          setFieldValue('apartment', '');
          setFieldValue('device_serialnumbers', '');
        }}
        placeholder={t('ChooseComplex')}
        menuPlacement="bottom"
        divider={false}
        maxMenuHeight={170}
      />
      {values?.complex?.buildings?.length ? (
        <Field
          name="building"
          component={SelectField}
          label={t('building')}
          options={values.complex.buildings}
          onChange={(value) => {
            setFieldValue('building', value);
            setFieldValue('apartment', '');
            setFieldValue('rizer', '');
            setFieldValue('device_serialnumbers', '');
          }}
          placeholder={t('selectBbuilding')}
          divider={false}
        />
      ) : null}
      {values?.building ? (
        <Field
          name="rizer"
          label={t('entrance')}
          placeholder={t('ChooseEntrance')}
          title={t('GeneralArea')}
          component={RizerSelectField}
          buildings={[values.building.id]}
          divider={false}
        />
      ) : null}
      {values?.rizer?.id ? (
        <Field
          name="apartment"
          component={SelectField}
          label={t('Label.flat')}
          options={apartmentsForCurrentRizer}
          placeholder={t('ChooseApartment')}
          divider={false}
        />
      ) : null}
      <Field
        name="action_type_id"
        component={SelectField}
        label={t('EventType')}
        options={actionTypes}
        onChange={(value) => {
          setFieldValue('action_type_id', value);
          setFieldValue('instruction', {});
        }}
        placeholder={t('chooseType')}
        menuPlacement="top"
        divider={false}
      />
      {hasInstruction(values) &&
        values.action_type_id.instruction.map((i) => {
          const placeholder =
            i.type === 'string' ? t('EnterString') : t('EnterTheNumber');

          const hint = i?.hint ? `Подсказка: ${i.hint}` : '';

          return (
            <Field
              key={i.name}
              name={`instruction.${i.name}`}
              label={i.title}
              placeholder={placeholder}
              component={InputField}
              divider={false}
              required={Boolean(i.required)}
              helpText={hint}
            />
          );
        })}
      <Field
        name="devices"
        component={EquipmentSelectField}
        label={t('equipment')}
        placeholder={t('ChooseEquipment')}
        object={values.complex}
        house={values.building}
        isMulti
        helpText={t('ForListEquipmentAppearStartTypingName')}
        divider={null}
      />
      <Field
        name="direction"
        component={SelectField}
        options={directions}
        label={t('direction')}
        textAlign="end"
        placeholder={t('ChooseDirection')}
        menuPlacement="top"
        divider={null}
      />
      <Field
        name="company"
        component={SelectField}
        options={companyList}
        label={t('Companies')}
        textAlign="end"
        placeholder={t('SelectCompanies')}
        divider={null}
        menuPlacement="top"
        maxMenuHeight={170}
        isMulti
      />
      <Field
        name="concurent_sugid"
        component={SelectField}
        options={usersGroup}
        label={t('ComputingConcurrentNumberUsersWithinObject')}
        placeholder={t('SelectUserGroupList')}
        divider={null}
        menuPlacement="top"
      />
      <Field
        name="control_inside_sugid"
        component={SelectField}
        options={usersGroup}
        label={t('ControllingThePresenceUserInsideObject')}
        placeholder={t('SelectUserGroupList')}
        divider={null}
        menuPlacement="top"
      />
    </>
  );
};
