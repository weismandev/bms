import { combine } from 'effector';
import { useGate, useStore } from 'effector-react';
import { Field, Form, Formik } from 'formik';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Loader, CustomScrollbar, SelectField } from '@ui/index';
import { dynamicValidationSchema } from '../../../lib/dynamic-validation-schema';
import {
  $actions,
  $loading,
  $action,
  setAction,
  updateAction,
  ActionModalGate,
} from '../../../model';
import { ActionFields } from '../common/action-fields';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  content: {
    display: 'flex',
    flexDirection: 'column',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  additionalFields: {
    display: 'flex',
    paddingRight: '24px',
    flexDirection: 'column',
    '& > div': {
      marginBottom: 15,
    },
  },
  actionField: {
    marginRight: 24,
    paddingRight: 0,
  },
  submitButton: {
    marginTop: 'auto',
    marginRight: 24,
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(85vh - 48px - 10px - 30px)',
    '& > div:nth-of-type(1)': {
      marginRight: 24,
    },
    '& > div': {
      marginBottom: 15,
    },
  },
}));

const stores = combine({
  actions: $actions,
  loading: $loading,
  action: $action,
});

export const ActionUpdateContent = () => {
  const { actions, loading, action } = useStore(stores);
  useGate(ActionModalGate);

  const classes = useStyles();

  if (loading) {
    return <Loader isLoading />;
  }

  return (
    <div className={classes.content}>
      <Formik
        onSubmit={updateAction}
        initialValues={action}
        enableReinitialize
        validationSchema={dynamicValidationSchema}
        render={({ values, setFieldValue }) => {
          return (
            <Form className={classes.form}>
              <div className={classes.wrapper}>
                <Field
                  name="action"
                  className={classes.actionField}
                  component={SelectField}
                  options={actions}
                  label={t('Label.action')}
                  labelPlacement="start"
                  textAlign="end"
                  placeholder={t('selectAction')}
                  divider={null}
                  isClearable={false}
                  onChange={(value) => {
                    setAction(value);
                  }}
                />
                {values?.action && (
                  <CustomScrollbar>
                    <div className={classes.additionalFields}>
                      <ActionFields values={values} setFieldValue={setFieldValue} />
                    </div>
                  </CustomScrollbar>
                )}
                {values?.action && (
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className={classes.submitButton}
                  >
                    {t('Apply')}
                  </Button>
                )}
              </div>
            </Form>
          );
        }}
      />
    </div>
  );
};
