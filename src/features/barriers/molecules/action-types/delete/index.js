import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Field, Form, Formik } from 'formik';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { SelectField, Loader } from '@ui/index';
import { $actions, $action, $loading, setAction, deleteAction } from '../../../model';

const useStyles = makeStyles({
  content: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },
  buttonWrapper: {
    height: 670,
    display: 'flex',
    flexDirection: 'column',
  },
  submitButton: {
    marginTop: 'auto',
  },
});

const { t } = i18n;

const stores = combine({
  actions: $actions,
  action: $action,
  loading: $loading,
});

export const ActionDeleteContent = () => {
  const classes = useStyles();

  const { actions, action, loading } = useStore(stores);

  if (loading) {
    return <Loader isLoading />;
  }

  return (
    <div className={classes.content}>
      <Formik
        onSubmit={deleteAction}
        initialValues={action}
        enableReinitialize
        render={({ values }) => {
          return (
            <Form>
              <Field
                name="action"
                component={SelectField}
                options={actions}
                label={t('Label.action')}
                labelPlacement="start"
                textAlign="end"
                placeholder={t('ChooseActionDelete')}
                divider={null}
                isClearable={false}
                onChange={(value) => {
                  setAction(value);
                }}
              />
              {values?.action && (
                <div className={classes.buttonWrapper}>
                  <Button
                    className={classes.submitButton}
                    variant="contained"
                    color="primary"
                    type="submit"
                  >
                    {t('remove')}
                  </Button>
                </div>
              )}
            </Form>
          );
        }}
      />
    </div>
  );
};
