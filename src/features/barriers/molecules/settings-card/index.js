import { Typography } from '@mui/material';
import * as Icons from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  cardRoot: {
    width: '100%',
    display: 'grid',
    borderRadius: 10,
    gridAutoRows: 'max-content',
    gridGap: 12,
    padding: 12,
    alignItems: 'center',
    backgroundColor: '#edf6ff',
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 600,
    padding: '0 10px',
  },
  cardTitleBox: {
    display: 'inline-flex',
    flexDirection: 'row',
    alignSelf: 'start',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'flex-end',
    flexGrow: 10,
  },
});

export const SettingsCard = ({
  mode = 'view',
  title,
  children,
  icon,
  actions = <div />,
}) => {
  const { cardRoot, cardTitle, cardTitleBox, cardActions } = useStyles();
  const SettingCardIcon = Icons[icon || 'Settings'];

  return (
    <li className={cardRoot}>
      <div className={cardTitleBox}>
        <SettingCardIcon />
        <Typography className={cardTitle}>{title}</Typography>
        {mode === 'view' ? null : <div className={cardActions}>{actions}</div>}
      </div>
      {children}
    </li>
  );
};
