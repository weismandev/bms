import { useMemo } from 'react';
import { Typography, IconButton } from '@mui/material';
import { Settings } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { CardPreview, ControlPanel } from '../../atoms';

const useStyles = makeStyles(({ spacing }) => {
  const space = spacing(3);
  return {
    card__root: {
      width: '100%',
      display: 'grid',
      gridTemplateRows: '240px 1fr',
      backgroundColor: ({ active }) => (active ? '#edf6ff' : 'white'),
      overflow: 'hidden',
      borderRadius: 15,
      boxShadow: '0px 1px 3px 0px rgba(0, 0, 0, .2)',
    },
    card__preview: {
      height: 240,
    },
    card__content: {
      display: 'grid',
      alignContent: 'space-between',
      gap: 24,
      height: '100%',
      padding: 24,
    },
    card__title: {
      width: '100%',
      overflow: 'hidden',
      color: '#3B3B50',
      lineHeight: 1,
      fontWeight: 800,
      fontSize: 24,
    },
    card__header: {
      display: 'grid',
      gridTemplateColumns: '1fr auto',
      alignItems: 'top',
      gap: 20,
    },
    card__settings: {
      padding: 0,
      opacity: 0.4,
      '&:hover': {
        opacity: 1,
      },
    },

    card__actions: {},
  };
});

export const BarrierCard = ({
  open,
  action,
  camera,
  enabled,
  id,
  title,
  handler,
  active,
}) => {
  const classes = useStyles({
    active,
    hasActions: Boolean(action.in),
  });

  return useMemo(
    () => (
      <li className={classes.card__root}>
        <CardPreview
          classes={{ root: classes.card__preview }}
          camera={camera}
          description={null}
          barrierId={id}
        />
        <div className={classes.card__content}>
          <div className={classes.card__header}>
            <Typography className={classes.card__title}>{title}</Typography>
            <IconButton
              onClick={() => open(id)}
              classes={{ root: classes.card__settings }}
              color="inherit"
              aria-label="settings"
              component="span"
              size="large"
            >
              <Settings />
            </IconButton>
          </div>

          <ControlPanel
            classes={{ root: classes.card__actions }}
            action={action}
            handler={handler}
          />
        </div>
      </li>
    ),
    [action, camera, title, handler, active]
  );
};
