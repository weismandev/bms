import { useState, useEffect } from 'react';
import { Typography } from '@mui/material';
import { VisibilityOff } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { Loader } from '@ui/index';

const useStyles = makeStyles({
  stubBox: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7E7EC',
    height: '100%',
    width: '100%',
  },
  loaderBox: {
    width: '40vw',
    height: '40vw',
    maxWidth: '720px',
    maxHeight: '720px',
  },
  stubIcon: {
    margin: '10%',
    width: '40%',
    height: '40%',
    color: 'white',
  },
  content: {
    width: '100%',
    height: '100%',
  },
  contentTitle: {
    position: 'absolute',
    top: '8%',
    left: '4%',
    color: 'white',
    fontSize: 32,
    fontWeight: 600,
    padding: '0 10px',
    textShadow: '3px 2px 6px black',
  },
});

const NoContentStub = ({ hasContent }) => {
  const { stubBox, stubIcon } = useStyles();

  return hasContent ? null : (
    <div className={stubBox}>
      <VisibilityOff className={stubIcon} />
    </div>
  );
};

const LoaderStub = ({ isLoading }) => {
  const { stubBox, loaderBox } = useStyles();

  return !isLoading ? null : (
    <div className={stubBox}>
      <div className={loaderBox}>
        <Loader isLoading={isLoading} />
      </div>
    </div>
  );
};

export const ContentLoader = ({ url = '', alt = '' }) => {
  const { content, contentTitle } = useStyles();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => setIsLoading(true), [url]);

  useEffect(() => {
    const image = new Image();

    const handleImageLoad = () => {
      setIsLoading(false);
      setIsError(false);
    };

    const handleImageError = () => {
      setIsLoading(false);
      setIsError(true);
    };

    image.addEventListener('load', handleImageLoad);
    image.addEventListener('error', handleImageError);

    image.src = url;

    return () => {
      image.removeEventListener('load', handleImageLoad);
      image.removeEventListener('error', handleImageError);
    };
  }, [url]);

  return (
    <>
      {alt && (
        <Typography variant="h3" className={contentTitle}>
          {alt}
        </Typography>
      )}
      <LoaderStub isLoading={isLoading} />
      <NoContentStub hasContent={!isError} />
      {!isError && !isLoading && <img className={content} src={url} alt={alt} />}
    </>
  );
};
