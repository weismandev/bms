import { Tooltip, IconButton } from '@mui/material';
import {
  SettingsOutlined,
  AddCircleOutlined,
  RemoveCircleOutlined,
} from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { openModal } from '../../model';

const { t } = i18n;

const useStyles = makeStyles({
  configureButton: {
    padding: 0,
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 80,
  },
});

export const ConfigureAction = () => {
  const classes = useStyles();

  const iconClickHandler = ({ type, title }) => {
    openModal({
      open: true,
      type,
      title,
    });
  };

  return (
    <div className={classes.wrapper}>
      <Tooltip title={t('CustomizeAction')} placement="bottom-end">
        <IconButton
          className={classes.configureButton}
          onClick={() =>
            iconClickHandler({
              type: 'update',
              title: t('CustomizeAction'),
            })
          }
          size="large"
        >
          <SettingsOutlined />
        </IconButton>
      </Tooltip>
      <Tooltip title={t('DeleteAction')} placement="bottom-end">
        <IconButton
          className={classes.configureButton}
          onClick={() =>
            iconClickHandler({
              type: 'delete',
              title: t('DeleteAction'),
            })
          }
          size="large"
        >
          <RemoveCircleOutlined />
        </IconButton>
      </Tooltip>
      <Tooltip title={t('AddAction')} placement="bottom-end">
        <IconButton
          className={classes.configureButton}
          onClick={() =>
            iconClickHandler({
              type: 'create',
              title: t('CreateAction'),
            })
          }
          size="large"
        >
          <AddCircleOutlined />
        </IconButton>
      </Tooltip>
    </div>
  );
};
