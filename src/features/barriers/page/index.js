import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import {
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
  NoDataPage,
  FilterMainDetailLayout,
} from '@ui/index';
import { $path } from '../../../pages/access-schedule/models';
import { HaveSectionAccess } from '../../common';
import {
  BarriersGate,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  $isDetailOpen,
  $currentPage,
  $isDeleteDialogOpen,
  deleteConfirmed,
  addClicked,
  $isFilterOpen,
} from '../model';
import {
  BarriersList,
  BarrierDetail,
  ActionSettingsModal,
  FullFrameView,
  Filter,
} from '../organisms';

const $stores = combine({
  currentPage: $currentPage,
  isLoading: $isLoading,
  isErrorDialogOpen: $isErrorDialogOpen,
  error: $error,
  isDetailOpen: $isDetailOpen,
  isDeleteDialogOpen: $isDeleteDialogOpen,
  isFilterOpen: $isFilterOpen,
});

const BarriersPage = () => {
  const { t } = useTranslation();
  const {
    isLoading,
    isErrorDialogOpen,
    error,
    isDetailOpen,
    currentPage,
    isDeleteDialogOpen,
    isFilterOpen,
  } = useStore($stores);

  function main(page) {
    if (page === 'list') return <BarriersList />;
    if (page === 'full') return <FullFrameView />;
    if (page === 'unavalable') {
      return (
        <NoDataPage text={t('ThereAreNoAccessPointsToDisplay')} onAdd={addClicked} />
      );
    }
    return null;
  }

  return (
    <>
      <BarriersGate />

      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
        content={t('DeleteAccessPoint')}
        header={t('RemovingAnAccessPoint')}
      />

      <FilterMainDetailLayout
        params={{
          mainWidth: !isDetailOpen
            ? 'auto'
            : isFilterOpen
            ? 'calc(24px * 2 + 330px)'
            : 'calc(24px + 330px)',
          detailWidth: '1fr',
          filterWidth: isFilterOpen ? '310px' : '0',
        }}
        filter={isFilterOpen && <Filter />}
        main={main(currentPage)}
        detail={isDetailOpen && <BarrierDetail />}
      />

      <ActionSettingsModal />
      <ColoredTextIconNotification />
    </>
  );
};

const RestrictedSchedulePage = () => {
  return (
    <HaveSectionAccess>
      <BarriersPage />
    </HaveSectionAccess>
  );
};

export { RestrictedSchedulePage as BarriersPage };
