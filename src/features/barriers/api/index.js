import { api } from '@api/api2';

const getBarriersList = (payload) =>
  api.no_vers('get', 'geo/get-points', { ...payload, ...{ ac: 1 } });
const getCamerasList = (payload) => api.no_vers('get', 'akado/get-stream-v2', payload);
const getActionsList = () => api.no_vers('get', 'geo/get-actions');
const getActionTypes = () => api.no_vers('get', 'geo/get-action-types');
const getUsersGroup = () => api.no_vers('get', 'scud/get-users-group');
const getCompanyList = (payload) => api.v1('get', 'enterprises/get-simple-list', payload);

const updateBarrier = (payload) => api.no_vers('post', 'geo/update-points', payload);

const updateAction = (payload) =>
  api.no_vers('post', 'geo/update-action', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const deleteAction = (payload) => api.no_vers('get', 'geo/delete-action', payload);

const sendAction = (payload) => api.no_vers('get', 'geo/send_action', payload);
const deleteBarrier = (id) => api.no_vers('get', 'geo/delete-point', id);

const getComplexes = (payload = {}) => api.v1('get', 'complex/list', payload);
const getBuildings = (payload = {}) => api.v1('get', 'buildings/get-list-available', {
  per_page: 1000000,
  ...payload,
});
const getApartments = (payload = {}) => api.v1('get', 'apartment/simple-list', payload);
const getAvailableCameras = (payload = {}) => api.no_vers('get', 'akado/cameras', payload);

export const barriersApi = {
  getBarriersList,
  getCamerasList,
  getActionsList,
  getActionTypes,
  updateBarrier,
  updateAction,
  deleteAction,
  sendAction,
  getUsersGroup,
  getCompanyList,
  getComplexes,
  getBuildings,
  getApartments,
  deleteBarrier,
  getAvailableCameras
};
