import './action.init';
import './detail.init';
import './full-frame.init';
import './list.init';
import './page.init';

export {
  signout,
  BarriersGate,
  pageMounted,
  pageUnmoumted,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  fxGetBarriers,
  fxGetCameras,
  fxGetActions,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $isDeleteDialogOpen,
  $rawData,
  $barriers,
  $count,
  $normalized,
  $currentPage,
  $isServiceAvaillible,
  $usersGroup,
  $companyList,
  deleteConfirmed,
} from './page.model';

export { launchAction, fxSendAction } from './list.model';

export {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
  fxUpdateBarrier,
  $cameras,
  $actions,
  $availableCameras,
} from './detail.model';

export {
  $modal,
  $loading,
  $directions,
  $action,
  $actionTypes,
  $space,
  setAction,
  openModal,
  closeModal,
  updateAction,
  createAction,
  deleteAction,
  actionEntity,
  ActionModalGate,
} from './action.model';

export { clickFullFrame, $barrierPreview, closeFullFrame } from './full-frame.model';

export {
  changedFilterVisibility,
  filtersSubmitted,
  $isFilterOpen,
  $filters,
} from './filter.model';
