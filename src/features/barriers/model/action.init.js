import { forward, sample, guard } from 'effector';
import { pending } from 'patronum';
import { barriersApi } from '../api';
import {
  openModal,
  closeModal,
  setAction,
  updateAction,
  deleteAction,
  createAction,
  $modal,
  $loading,
  $action,
  $directions,
  $actionTypes,
  $complexes,
  $buildings,
  $apartments,
  $space,
  fxUpdateAction,
  fxCreateAction,
  fxDeleteAction,
  fxGetComplexes,
  fxGetBuildings,
  fxGetApartments,
  ActionModalGate,
} from './action.model';
import { fxGetActionTypes, $companyList, $usersGroup, signout } from './page.model';

fxUpdateAction.use(barriersApi.updateAction);
fxDeleteAction.use(barriersApi.deleteAction);
fxCreateAction.use(barriersApi.updateAction);
fxGetComplexes.use(barriersApi.getComplexes);
fxGetBuildings.use(barriersApi.getBuildings);
fxGetApartments.use(barriersApi.getApartments);

$modal
  .on(openModal, (_, visibility) => visibility)
  .on(closeModal, () => ({ open: false, type: '', title: '' }));

$loading.on(
  pending({
    effects: [
      fxUpdateAction,
      fxCreateAction,
      fxDeleteAction,
      fxGetComplexes,
      fxGetBuildings,
      fxGetApartments,
    ],
  }),
  (_, pending) => pending
);

$action.reset(closeModal);

$actionTypes.on(fxGetActionTypes.doneData, (_, data) => {
  const hasTypes = data?.types && Array.isArray(data.types);

  if (!hasTypes) return [];

  const typesWithReplacedDash = data.types.map((type) => {
    if (type?.instruction) {
      const instructionWithReplacedDash = type.instruction.map((inst) => {
        return {
          ...inst,
          name: inst.name.replace(/-/g, '_'),
        };
      });

      return {
        ...type,
        instruction: instructionWithReplacedDash,
      };
    }
    return type;
  });

  return typesWithReplacedDash;
});

$complexes.on(fxGetComplexes.doneData, (_, { items }) => items || []).reset(signout);
$buildings
  .on(fxGetBuildings.doneData, (_, { buildings }) => buildings || [])
  .reset(signout);
$apartments.on(fxGetApartments.doneData, (_, { items }) => items || []).reset(signout);

guard({
  source: [$complexes, $buildings, $apartments],
  clock: ActionModalGate.open,
  filter: (space) => space.every((store) => !store),
  target: [fxGetComplexes, fxGetBuildings, fxGetApartments],
});

sample({
  source: [$directions, $companyList, $usersGroup, $space, $actionTypes],
  clock: setAction,
  fn: prepareFields,
  target: $action,
});

sample({
  source: updateAction,
  fn: prepareUpdateAction,
  target: fxUpdateAction,
});

sample({
  source: deleteAction,
  fn: ({ action }) => ({
    id: action.id,
  }),
  target: fxDeleteAction,
});

sample({
  source: createAction,
  fn: prepareCreateAction,
  target: fxCreateAction,
});

forward({
  from: [fxUpdateAction.done, fxCreateAction.done, fxDeleteAction.done],
  to: closeModal,
});

function prepareFields(
  [directions, companyList, usersGroup, space, actionTypes],
  action
) {
  const devices =
    action?.devices?.length > 0
      ? action.devices.map((device) => ({
          id: device.device_serialnumber,
          title: device.title,
        }))
      : [];

  const direction = directions.find((dir) => dir.key === action.direction);

  const concurent_sugid = usersGroup.find(({ id }) => id === action.concurent_sugid);
  const control_inside_sugid = usersGroup.find(
    ({ id }) => id === action.control_inside_sugid
  );

  const complex = space.find((currentComplex) => currentComplex.id === action.complex_id);
  const building =
    action?.building_id && Array.isArray(complex?.buildings)
      ? complex.buildings.find(
          (currentBuilding) => currentBuilding.id === action.building_id
        )
      : '';
  const apartment =
    action?.apartment_id && Array.isArray(building?.apartments)
      ? building.apartments.find(
          (currentApartment) => currentApartment.id === action.apartment_id
        )
      : '';

  const company =
    Array.isArray(action.company) && action.company.length
      ? action.company
          .map((currentCompanyId) => {
            const foundedCompany = companyList.find(
              (commonCompany) => commonCompany.id === currentCompanyId
            );

            return foundedCompany;
          })
          .filter(Boolean)
      : [];

  const action_type_id = actionTypes.find(
    (typeAction) => typeAction.id === action.action_type_id
  );

  const instruction = Object.keys(action.instruction).reduce((acc, instructionKey) => {
    const newKey = instructionKey.replace(/-/g, '_');

    const hasKey = action_type_id?.instruction.find((inst) => {
      return inst.name === newKey;
    });

    if (!hasKey) return acc;
    if (!action.instruction[instructionKey]) return acc;

    return {
      ...acc,
      [newKey]: action.instruction[instructionKey],
    };
  }, {});

  return {
    action,
    complex,
    building,
    apartment,
    company,
    action_type_id,
    instruction,
    rizer: action.rizer,
    id: action.id,
    title: action.title,
    verify_parking_slot: Boolean(action.verif_parking_slot),
    verify_vehicle: Boolean(action.verif_vehicle),
    direction: {
      key: action.direction,
      value: direction.value,
    },
    concurent_sugid,
    control_inside_sugid,
    devices,
  };
}

function prepareUpdateAction(action) {
  return {
    id: action.id,
    ...getActionDataWithoutId(action),
  };
}

function prepareCreateAction(action) {
  return getActionDataWithoutId(action);
}

function getActionDataWithoutId(action) {
  const company = getActionCompany(action);
  const instructions = getActionInstructions(action);

  const data = {
    title: action.title,
    complex_id: action.complex.id,
    building_id: action?.building?.id || 0,
    apartment_id: action?.apartment?.id || 0,
    rizer: action?.rizer?.id || 0,
    verif_parking_slot: Number(action.verify_parking_slot),
    verif_vehicle: Number(action.verify_vehicle),
    direction: action?.direction?.key || 0,
    concurent_sugid: action?.concurent_sugid?.id || 0,
    control_inside_sugid: action?.control_inside_sugid?.id || 0,
    action_type_id: action.action_type_id.id,
    company,
    ...instructions,
  };

  if (action?.devices) {
    const devices = action.devices.map((number) => number.id);
    const formattedDevices = devices.join(',');

    data.device_serialnumbers = formattedDevices;
  }

  return data;
}

function getActionCompany(action) {
  return Array.isArray(action.company) ? action.company.map((c) => c.id).join(',') : '';
}

function getActionInstructions(action) {
  return (
    action?.instruction &&
    Object.keys(action.instruction).reduce((acc, name) => {
      const newName = name.replace(/_/g, '-');

      return {
        ...acc,
        [newName]: action.instruction[name],
      };
    }, {})
  );
}
