import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  complexes: [],
  buildings: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
