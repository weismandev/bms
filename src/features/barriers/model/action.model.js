import { createEvent, createStore, createEffect, combine } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const directions = [
  { key: 0, value: t('isNotDefinedit') },
  { key: 1, value: t('wayout') },
  { key: 2, value: t('entrance') },
];

export const actionEntity = {
  title: '',
  verify_parking_slot: false,
  verify_vehicle: false,
  direction: '',
  company: '',
  concurent_sugid: '',
  control_inside_sugid: '',
  complex: '',
  building: '',
  apartment: '',
  action_type_id: '',
};

export const openModal = createEvent();
export const closeModal = createEvent();
export const setAction = createEvent();
export const updateAction = createEvent();
export const deleteAction = createEvent();
export const createAction = createEvent();

export const $modal = createStore({ open: false, type: '', title: '' });
export const $action = createStore(null);
export const $directions = createStore(directions);
export const $loading = createStore(false);
export const $actionTypes = createStore(null);

export const $complexes = createStore(null);
export const $buildings = createStore(null);
export const $apartments = createStore(null);
export const $space = combine(
  $complexes,
  $buildings,
  $apartments,
  (complexes, buildings, apartments) => {
    if (!complexes || !buildings || !apartments) return [];

    const buildingsWithApartments = buildings.map((building) => {
      const currentApartments = apartments
        .filter((apartment) => {
          return apartment?.building?.id === building.id;
        })
        .map((apartment) => ({
          ...apartment,
          title: apartment?.apartment?.title,
        }));

      return {
        ...building,
        apartments: currentApartments,
      };
    });

    const complexesWithBuildings = complexes.map((complex) => {
      const currentBuildings = buildingsWithApartments.filter((building) => {
        return building?.complex?.id === complex.id;
      });

      return {
        ...complex,
        buildings: currentBuildings,
      };
    });

    return complexesWithBuildings;
  }
);

export const fxUpdateAction = createEffect();
export const fxDeleteAction = createEffect();
export const fxCreateAction = createEffect();

export const fxGetComplexes = createEffect();
export const fxGetBuildings = createEffect();
export const fxGetApartments = createEffect();

export const ActionModalGate = createGate();
