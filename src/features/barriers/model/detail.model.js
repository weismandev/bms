import { createEffect, createStore } from 'effector';
import { createDetailBag } from '@tools/factories';
import { barriersApi } from '../api';

const newEntity = {
  id: '',
  title: '',
  camera: '',
  selectedCameras: [],
  action: {},
  beacon: {
    enabled: false,
    mac: '',
    uuid: '',
    major: '',
    minor: '',
  },
  geo: {
    enabled: false,
    latitude: '',
    longitude: '',
  },
  wifi: {
    enabled: false,
    bsid: '',
    level: 0,
  },
  enabled: false,
  hide: true,
  radius: 0,
  order_plan: '',
  control_position: false,
  auto_action: false,
  send_push: false,
};

export const $cameras = createStore([]);
export const $actions = createStore([]);
export const $availableCameras = createStore([]);

export const fxUpdateBarrier = createEffect(barriersApi.updateBarrier);

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
