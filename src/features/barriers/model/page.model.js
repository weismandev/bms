import { createEvent, createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { barriersApi } from '../api';
import { fxUpdateBarrier } from './detail.model';
import { fxSendAction } from './list.model';

const BarriersGate = createGate();

const { open: pageMounted, close: pageUnmoumted } = BarriersGate;

const $currentPage = createStore('list');

const errorDialogVisibilityChanged = createEvent();
const deleteDialogVisibilityChanged = createEvent();
const deleteClicked = createEvent();
const deleteConfirmed = createEvent();

const fxGetBarriers = createEffect(barriersApi.getBarriersList);
const fxGetCameras = createEffect(barriersApi.getCamerasList);
const fxGetAvailableCameras = createEffect(barriersApi.getAvailableCameras);
const fxGetActions = createEffect(barriersApi.getActionsList);
const fxGetActionTypes = createEffect(barriersApi.getActionTypes);
const fxGetCompanyList = createEffect(barriersApi.getCompanyList);
const fxGetUsersGroup = createEffect(barriersApi.getUsersGroup);
const fxDeleteBarrier = createEffect(barriersApi.deleteBarrier);

const $isLoading = pending({
  effects: [
    fxGetBarriers,
    fxGetCameras,
    fxGetActions,
    fxGetActionTypes,
    fxGetUsersGroup,
    fxGetCompanyList,
    fxSendAction,
    fxUpdateBarrier,
    fxDeleteBarrier,
    fxGetAvailableCameras
  ],
});

const $isServiceAvaillible = createStore(true);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);
const $isDeleteDialogOpen = createStore(false);
const $companyList = createStore([]);
const $usersGroup = createStore([]);
const $rawData = createStore([]);
const $barriers = createStore([]);
const $camerasIds = createStore([]);
const $count = $barriers.map((data) => data.length);
const $normalized = $barriers.map((data) =>
  data.reduce((acc, item) => ({ ...acc, [item.id]: item }), {})
);

export { signout } from '../../common';
export {
  BarriersGate,
  pageMounted,
  pageUnmoumted,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  fxGetBarriers,
  fxGetCameras,
  fxGetActions,
  fxGetActionTypes,
  fxGetUsersGroup,
  fxGetCompanyList,
  $isLoading,
  $isServiceAvaillible,
  $error,
  $isErrorDialogOpen,
  $isDeleteDialogOpen,
  $currentPage,
  $rawData,
  $barriers,
  $camerasIds,
  $count,
  $normalized,
  $usersGroup,
  $companyList,
  deleteClicked,
  deleteConfirmed,
  fxDeleteBarrier,
  fxGetAvailableCameras
};
