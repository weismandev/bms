import { createEvent, createStore } from 'effector';

const clickFullFrame = createEvent();
const closeFullFrame = createEvent();
const $barrierPreview = createStore({});

export { clickFullFrame, $barrierPreview, closeFullFrame };
