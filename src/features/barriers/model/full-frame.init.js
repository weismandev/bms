import { sample } from 'effector';
import { clickFullFrame, $barrierPreview, closeFullFrame } from './full-frame.model';
import { signout, $normalized } from './page.model';

sample({
  source: $normalized,
  clock: clickFullFrame,
  fn: (normalized, id) => normalized[id],
  target: $barrierPreview,
});

$barrierPreview.reset([signout, closeFullFrame]);
