import { createEvent, createEffect } from 'effector';
import { barriersApi } from '../api';

const launchAction = createEvent();

const fxSendAction = createEffect(barriersApi.sendAction);

export { launchAction, fxSendAction };
