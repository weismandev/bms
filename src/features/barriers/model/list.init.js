import { forward } from 'effector';
import { fxSendAction, launchAction } from './list.model';

forward({
  from: launchAction,
  to: fxSendAction.prepend((id) => ({ id })),
});
