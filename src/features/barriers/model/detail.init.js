import { forward, sample } from 'effector';
import i18n from '@shared/config/i18n';
import {
  detailSubmitted,
  open,
  $mode,
  $opened,
  fxUpdateBarrier,
  $cameras,
  $actions,
  $isDetailOpen,
  $availableCameras,
} from './detail.model';
import {
  $normalized,
  fxGetCameras,
  fxGetActions,
  fxGetAvailableCameras,
  pageUnmoumted,
  signout,
} from './page.model';

const { t } = i18n;

sample({
  source: $normalized,
  clock: open,
  fn: (normalized, id) => normalized[id],
  target: $opened,
});

sample({
  clock: $isDetailOpen,
  filter: (isDetailOpen) => Boolean(isDetailOpen),
  target: fxGetAvailableCameras
});

$availableCameras
  .on(fxGetAvailableCameras.doneData, (_, data) => data.cameras)
  .reset([pageUnmoumted, signout]);

$cameras
  .on(fxGetCameras.doneData, (_, data) => (Boolean(data) && data.camera) || [])
  .reset([pageUnmoumted, signout]);

$actions
  .on(fxGetActions.doneData, (_, data) => (Boolean(data) && data.action) || [])
  .reset([pageUnmoumted, signout]);

$mode.on(fxUpdateBarrier.done, () => 'view');

forward({
  from: detailSubmitted,
  to: fxUpdateBarrier.prepend(formatPayload),
});

function formatPayload({
  id,
  title,
  selectedCameras,
  action,
  beacon,
  geo,
  wifi,
  enabled,
  hide,
  order_plan,
  radius,
  control_position,
  auto_action,
  send_push,
}) {
  const mapActionsToPayload = Object.entries(action).reduce(
    (acc, [key, value]) =>
      Boolean(value) && Boolean(value.id) ? { ...acc, [`action_${key}`]: value.id } : acc,
    {}
  );

  const payload = {
    id,
    title: title ? String(title) : `-${t('notspecified')}-`,
    enabled: Number(enabled),
    send_push: Number(send_push),
    auto_action: Number(auto_action),
    radius: radius || 0,
    order_plan: order_plan || '',
    control_position: Number(control_position),
    hide: Number(hide),
    beacon_enabled: Number(beacon.enabled),
    beacon_mac: beacon.mac || '',
    beacon_major: beacon.major || '',
    beacon_minor: beacon.minor || '',
    beacon_uuid: beacon.uuid || '',
    wifi_enabled: Number(wifi.enabled),
    wifi_ssid: wifi.bsid || '',
    wifi_level: wifi.level || 0,
    geo_enabled: Number(geo.enabled),
    geo_latitude: geo.latitude || 0,
    geo_longitude: geo.latitude || 0,
    cameras: selectedCameras.length > 0 ? selectedCameras.map((e) => e.id).join() : '',
    ...mapActionsToPayload,
  };

  return payload;
}
