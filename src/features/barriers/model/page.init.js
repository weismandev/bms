import { forward, guard, merge, sample, attach } from 'effector';
import { CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { $isUserLoading } from '@features/common';
import i18n from '@shared/config/i18n';
import { fxUpdateAction, fxDeleteAction, fxCreateAction } from './action.model';
import {
  $cameras,
  $actions,
  $opened,
  fxUpdateBarrier,
  changedDetailVisibility,
} from './detail.model';
import { $filters, defaultFilters } from './filter.model';
import { clickFullFrame, closeFullFrame } from './full-frame.model';
import { fxSendAction } from './list.model';
import {
  pageMounted,
  pageUnmoumted,
  fxGetBarriers,
  fxGetCameras,
  fxGetActions,
  fxGetActionTypes,
  fxGetCompanyList,
  fxGetUsersGroup,
  $rawData,
  $camerasIds,
  $barriers,
  $error,
  $isErrorDialogOpen,
  signout,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  $currentPage,
  $isServiceAvaillible,
  $usersGroup,
  $companyList,
  $isDeleteDialogOpen,
  deleteClicked,
  deleteConfirmed,
  fxDeleteBarrier,
} from './page.model';

const { t } = i18n;

$rawData
  .on(fxGetBarriers.doneData, (_, data) => (Boolean(data) && data.point) || [])
  .reset([signout, pageUnmoumted]);

// Собираем массив id камер, привязанных к точкам доступа
$camerasIds
  .on($rawData, (_, data) => data.reduce((acc, { cameras = [] }) => acc.concat(cameras), []))
  .reset([pageUnmoumted]);

// Запрашиваем только привязанные к точкам камеры
sample({
  clock: $camerasIds,
  fn: (_, ids) => ({ id: ids.join(',') }),
  target: fxGetCameras,
  filter: (ids) => ids.length,
});

$barriers.reset([signout, pageUnmoumted, fxGetBarriers.done]);

$isDeleteDialogOpen.on(deleteDialogVisibilityChanged, (_, bool) => bool);

forward({
  from: deleteClicked,
  to: deleteDialogVisibilityChanged.prepend(() => true),
});

forward({
  from: deleteConfirmed,
  to: deleteDialogVisibilityChanged.prepend(() => false),
});

sample({
  source: $opened,
  clock: deleteConfirmed,
  fn: ({ id }) => ({ id }),
  target: fxDeleteBarrier,
});

forward({
  from: fxDeleteBarrier.done,
  to: changedDetailVisibility.prepend(() => false),
});

$currentPage
  .on(clickFullFrame, () => 'full')
  .on(closeFullFrame, () => 'list')
  .on($isServiceAvaillible, (state, bool) => (bool ? state : 'unavalable'))
  .reset([signout, pageUnmoumted]);

$isErrorDialogOpen.on(errorDialogVisibilityChanged, (_, visibility) => visibility);

$companyList.on(fxGetCompanyList.doneData, (_, data) => data?.enterprises || []);

$usersGroup.on(fxGetUsersGroup.doneData, (_, data) => data?.groups || []);

forward({
  from: [
    pageMounted,
    fxUpdateBarrier.done,
    fxUpdateAction.done,
    fxCreateAction.done,
    fxDeleteAction.done,
    fxDeleteBarrier.done,
  ],
  to: [fxGetActions, fxGetActionTypes, fxGetCompanyList, fxGetUsersGroup],
});

forward({
  from: [
    pageMounted,
    $filters,
    fxUpdateBarrier.done,
    fxUpdateAction.done,
    fxCreateAction.done,
    fxDeleteAction.done,
    fxDeleteBarrier.done,
  ],
  to: attach({
    effect: fxGetBarriers,
    source: { filters: $filters },
    mapParams: (_, { filters }) => {
      const { buildings, complexes } = filters;

      const complex_ids = complexes.map((complex) => complex.id).join(',');
      const buildings_ids = buildings.map((building) => building.id).join(',');

      return {
        complex_ids,
        buildings_ids,
      };
    },
  }),
});

sample({
  clock: $rawData,
  source: [$filters, $rawData, defaultFilters],
  fn: ([filters, rawData, defaultFilters]) => {
    if (JSON.stringify(filters) !== JSON.stringify(defaultFilters)) {
      return true;
    }

    return rawData.length > 0;
  },
  target: $isServiceAvaillible,
});

$isServiceAvaillible
  .on(fxGetBarriers.fail, () => false)
  .reset([$isUserLoading, pageUnmoumted, pageMounted]);

sample({
  source: [$rawData, $cameras, $actions],
  fn: ([barriers, cameras, actions]) => formatData({ barriers, cameras, actions }),
  target: $barriers,
});

const setError = guard({
  source: merge([
    fxGetBarriers.fail,
    fxGetCameras.fail,
    fxGetActions.fail,
    fxGetActionTypes,
    fxSendAction.fail,
    fxUpdateBarrier.fail,
    fxUpdateAction.fail,
    fxDeleteAction.fail,
    fxCreateAction.fail,
    fxGetUsersGroup.fail,
    fxGetCompanyList.fail,
    fxDeleteBarrier.fail,
  ]),
  filter: ({ error }) => Boolean(error),
});

forward({
  from: setError,
  to: errorDialogVisibilityChanged.prepend((data) => Boolean(data)),
});

$error.on(setError, (_, { error }) => error.message).reset([signout, pageUnmoumted]);

forward({
  from: fxCreateAction.done,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#1bb169',
    text: t('EventCreatedSuccessfully'),
    Icon: CheckCircle,
  })),
});

forward({
  from: fxDeleteAction.done,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#1bb169',
    text: t('EventDeletedSuccessfully'),
    Icon: CheckCircle,
  })),
});

forward({
  from: fxUpdateAction.done,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#1bb169',
    text: t('EventChangedSuccessfully'),
    Icon: CheckCircle,
  })),
});

function formatData({ barriers, cameras, actions }) {
  const getCameraOrEmpty = (camera) => {
    const cam_index = cameras.findIndex(({ id }) => id === camera);
    if (cam_index > -1) return cameras[cam_index];
    return '';
  };

  return barriers
    .map((barrier) => ({
      ...barrier,
      camera: barrier.cameras.length ? getCameraOrEmpty(barrier.cameras[0]) : '',
      selectedCameras: barrier.cameras.map(getCameraOrEmpty),
    }))
    .map((barrier) => {
      const getActionsAsFullObject = (action) =>
        Object.entries(action).reduce(
          (acc, [key, value]) =>
            value ? { ...acc, [key]: actions.find(({ id }) => id === value) } : acc,
          {}
        );

      return Boolean(barrier) && typeof barrier.action === 'object'
        ? { ...barrier, action: getActionsAsFullObject(barrier.action) }
        : barrier;
    })
    .map((barrier) => {
      const { control_position, enabled, hide, geo, wifi, beacon, auto_action } = barrier;

      return {
        ...barrier,
        control_position: Boolean(control_position),
        hide: Boolean(hide),
        auto_action: Boolean(auto_action),
        enabled: Boolean(enabled),
        geo: { ...geo, enabled: Boolean(geo.enabled) },
        wifi: { ...wifi, enabled: Boolean(wifi.enabled) },
        beacon: { ...beacon, enabled: Boolean(beacon.enabled) },
      };
    });
}
