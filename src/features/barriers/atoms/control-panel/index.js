import cn from 'classnames';
import makeStyles from '@mui/styles/makeStyles';
import { Controls } from '..';

const useStyles = makeStyles({
  controlRoot: { display: 'flex', gap: 24, width: '100%', height: '100%' },
});

export const ControlPanel = ({ action, handler, classes = {} }) => {
  const { controlRoot } = useStyles();

  return (
    <div className={cn(controlRoot, classes.root)}>
      <Controls action={action} handler={handler} />
    </div>
  );
};
