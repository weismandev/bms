import cn from 'classnames';
import { Fullscreen } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { clickFullFrame } from '../../model';
import { ContentLoader } from '../../molecules';

const useStyles = makeStyles({
  cardRoot: {
    width: '100%',
    display: 'grid',
    gridTemplateRows: '1fr 60px 60px',
    backgroundColor: 'white',
    overflow: 'hidden',
    borderRadius: 10,
    boxShadow: '0px 1px 3px 0px rgba(0, 0, 0, .2)',
  },
  imageBox: {
    position: 'relative',
    backgroundColor: '#E7E7EC',
    overflow: 'hidden',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: '50% 50%',
  },
  fullScreenBox: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    '&:hover > svg': {
      opacity: 1,
    },
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  fullScreenIcon: {
    position: 'absolute',
    width: '40px',
    height: '40px',
    color: 'white',
    opacity: 0.6,
    bottom: 6,
    right: 18,
  },
});

export const CardPreview = (props) => {
  const { imageBox } = useStyles();
  const { camera, description, classes = {} } = props;

  const previewLink = (Boolean(camera) && camera.preview) || '';

  return (
    <div className={cn(imageBox, classes.root)}>
      <FullScreenControl previewLink={previewLink} {...props} />
      <ContentLoader url={previewLink} alt={description} />
    </div>
  );
};

function FullScreenControl({ previewLink, barrierId, camera, description }) {
  const { fullScreenBox, fullScreenIcon } = useStyles();

  return !previewLink ? null : (
    <div className={fullScreenBox} onClick={() => clickFullFrame(barrierId)}>
      <Fullscreen className={fullScreenIcon} />
    </div>
  );
}
