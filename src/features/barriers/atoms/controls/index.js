import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { ActionButton } from '@ui/index';

export const Controls = ({ action, handler }) => {
  const { t } = useTranslation();
  const actions =
    Boolean(action) && typeof action === 'object' ? Object.entries(action) : [];

  const controls = useMemo(
    () =>
      actions.length
        ? actions.map(([key, currentAction]) => {
            const id = currentAction?.id;

            if (!id) return null;

            const getName = (actionKey) => {
              switch (actionKey) {
                case 'in':
                  return { label: t('open'), kind: 'positive' };

                case 'out':
                  return { label: t('close'), kind: 'negative' };

                default:
                  return { label: t('Perform'), kind: 'basic' };
              }
            };

            const buttonParams = getName(key);

            return (
              <ActionButton
                style={{ width: '100%' }}
                kind={buttonParams.kind}
                key={key}
                onClick={() => handler(id)}
              >
                {buttonParams.label}
              </ActionButton>
            );
          })
        : null,
    [action]
  );

  return controls;
};
