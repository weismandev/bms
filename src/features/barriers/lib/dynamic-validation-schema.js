import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;
export const hasInstruction = (values) =>
  values?.action_type_id && Array.isArray(values?.action_type_id?.instruction);

const dynamicFields = (values) => {
  if (!hasInstruction(values)) return {};

  const instruction = values.action_type_id.instruction;

  const matchFieldType = (type) => {
    switch (type) {
      case 'string':
        return Yup.string();
      case 'int':
        return Yup.number();
      case 'float':
        return Yup.number();
      default:
        return Yup.object();
    }
  };

  const fields = instruction.reduce((acc, field) => {
    const type = matchFieldType(field.type);
    const hint = field?.hint || t('thisIsRequiredField');

    const typeWithRequired = field.requred ? type.required(hint) : type;

    return {
      ...acc,
      [`${field.name}`]: typeWithRequired,
    };
  }, {});

  return fields;
};

export const dynamicValidationSchema = () =>
  Yup.lazy((values) => {
    const additionalSchema = dynamicFields(values);

    return Yup.object().shape({
      title: Yup.string().required(t('thisIsRequiredField')),
      action_type_id: Yup.object().nullable().required(''),
      complex: Yup.object().nullable().required(''),
      instruction: Yup.object().shape({
        ...additionalSchema,
      }),
    });
  });
