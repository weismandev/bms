import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SwitchField,
  SelectField,
  CustomScrollbar,
} from '@ui/index';
import {
  $opened,
  $mode,
  changeMode,
  changedDetailVisibility,
  deleteDialogVisibilityChanged,
  detailSubmitted,
  $actions,
  $availableCameras,
} from '../../model';
import { SettingsCard, ConfigureAction } from '../../molecules';

const useStyles = makeStyles({
  root: {
    color: '#3b3b50',
    height: '100%',
  },
  toolbar: {
    paddingRight: 20,
  },
  header: {
    fontSize: 24,
    fontWeight: 800,
    padding: '24px 20px 24px 4px',
  },
  settingsBox: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fit, minmax(360px, 1fr))',
    padding: '4px 24px 106px 4px',
    gridGap: 16,
  },
  formBox: {
    height: '100%',
    overflow: 'hidden',
    padding: '24px 0px 20px 20px',
  },
});

const $stores = combine({
  mode: $mode,
  opened: $opened,
  actions: $actions,
  availableCameras: $availableCameras
});

export const BarrierDetail = () => {
  const { t } = useTranslation();
  const { root, toolbar, header, settingsBox, formBox } = useStyles();

  const { mode, opened, actions, availableCameras } = useStore($stores);

  const isNew = !opened.id;

  return (
    <Wrapper classes={{ wrapper: root }}>
      <Formik
        onSubmit={detailSubmitted}
        initialValues={opened}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={{}}
        enableReinitialize
        render={({ values, resetForm, setFieldValue }) => {
          const { title, enabled } = values;
          return (
            <Form className={formBox}>
              <DetailToolbar
                className={toolbar}
                mode={mode}
                onEdit={() => changeMode('edit')}
                onClose={() => changedDetailVisibility(false)}
                onDelete={() => deleteDialogVisibilityChanged(true)}
                onCancel={() => {
                  if (isNew) changedDetailVisibility(false);
                  else {
                    changeMode('view');
                    resetForm();
                  }
                }}
              />
              <Typography className={header}>
                {title || `${t('CreatingNewObject')}:`}
              </Typography>
              <CustomScrollbar autoHide>
                <ul className={settingsBox}>
                  <SettingsCard title={t('basicSettings')}>
                    <Field
                      name="title"
                      label={t('ObjectName')}
                      labelPlacement="start"
                      placeholder={t('EnterTheTitle')}
                      component={InputField}
                      textAlign="end"
                      mode={mode}
                      divider={null}
                    />
                    <Field
                      name="enabled"
                      component={SwitchField}
                      label={`${t('ObjectStatus')}: ${
                        enabled ? t('activeAt') : t('isTurnedOff')
                      }`}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="send_push"
                      component={SwitchField}
                      label={t('SendPushNotifications')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="auto_action"
                      component={SwitchField}
                      label={t('EnableAutomaticActions')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="radius"
                      component={InputField}
                      label={t('TheRadiusOoperationAutomation')}
                      labelPlacement="start"
                      placeholder={t('EnterRadius')}
                      textAlign="end"
                      type="number"
                      mode={mode}
                      divider={null}
                    />
                    <Field
                      name="order_plan"
                      component={InputField}
                      label={t('Serial_number')}
                      labelPlacement="start"
                      placeholder={t('EnterSerial_number')}
                      textAlign="end"
                      type="number"
                      mode={mode}
                      divider={null}
                    />
                    <Field
                      name="control_position"
                      component={SwitchField}
                      label={t('ShowForNearbyDevices')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="hide"
                      component={SwitchField}
                      label={t('HideObjectForUsers')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                  </SettingsCard>
                  <SettingsCard title={t('BLEDevices')} icon="Bluetooth">
                    <Field
                      name="beacon.enabled"
                      component={SwitchField}
                      label={t('WorkWithBLEDevices')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="beacon.uuid"
                      placeholder={t('EnterDeviceUUID')}
                      component={InputField}
                      mode={mode}
                      textAlign="end"
                      label="UUID"
                      labelPlacement="start"
                      divider={null}
                    />
                    <Field
                      name="beacon.mac"
                      placeholder={t('EnterMacAddress')}
                      component={InputField}
                      mode={mode}
                      textAlign="end"
                      label={t('DeviceMACAddress')}
                      labelPlacement="start"
                      divider={null}
                    />
                    <Field
                      name="beacon.major"
                      placeholder={t('EnterTheMajorDevice')}
                      component={InputField}
                      mode={mode}
                      type="number"
                      textAlign="end"
                      label={t('MajorDevices')}
                      labelPlacement="start"
                      divider={null}
                    />
                    <Field
                      name="beacon.minor"
                      placeholder={t('EnterMinorDevice')}
                      component={InputField}
                      mode={mode}
                      textAlign="end"
                      type="number"
                      label={t('MinorDevices')}
                      labelPlacement="start"
                      divider={null}
                    />
                  </SettingsCard>
                  <SettingsCard title={t('WiFiNetwork')} icon="Wifi">
                    <Field
                      name="wifi.enabled"
                      component={SwitchField}
                      label={t('WirelessNetworkStatus')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="wifi.bsid"
                      placeholder={t('EnterNetworkName')}
                      component={InputField}
                      mode={mode}
                      textAlign="end"
                      label={t('WirelessNetworkName')}
                      labelPlacement="start"
                      divider={null}
                    />
                    <Field
                      name="wifi.level"
                      placeholder={t('EnterSignalLevel')}
                      component={InputField}
                      textAlign="end"
                      mode={mode}
                      label={t('NetworkSignalStrength')}
                      labelPlacement="start"
                      type="number"
                      divider={null}
                    />
                  </SettingsCard>
                  <SettingsCard title={t('GeoCoordinatesOobject')} icon="LocationOn">
                    <Field
                      name="geo.enabled"
                      component={SwitchField}
                      label={t('WorkWithLocation')}
                      labelPlacement="start"
                      kind="switch"
                      disabled={mode === 'view'}
                      divider={null}
                    />
                    <Field
                      name="geo.latitude"
                      placeholder={t('EnterObjectLatitude')}
                      component={InputField}
                      mode={mode}
                      textAlign="end"
                      label={t('ObjectLatitude')}
                      labelPlacement="start"
                      divider={null}
                      type="number"
                    />
                    <Field
                      name="geo.longitude"
                      placeholder={t('EnterLongitudeObject')}
                      component={InputField}
                      mode={mode}
                      type="number"
                      textAlign="end"
                      label={t('ObjectLongitude')}
                      labelPlacement="start"
                      divider={null}
                    />
                  </SettingsCard>
                  <SettingsCard
                    title={t('Label.actions')}
                    icon="SwapHoriz"
                    mode={mode}
                    actions={<ConfigureAction />}
                  >
                    <Field
                      name="action.in"
                      component={SelectField}
                      label={`${t('Label.action')} in`}
                      labelPlacement="start"
                      textAlign="end"
                      placeholder={t('selectAction')}
                      options={actions}
                      mode={mode}
                      divider={null}
                    />
                    <Field
                      name="action.out"
                      component={SelectField}
                      label={`${t('Label.action')} out`}
                      labelPlacement="start"
                      textAlign="end"
                      placeholder={t('selectAction')}
                      options={actions}
                      mode={mode}
                      divider={null}
                    />
                  </SettingsCard>
                  <SettingsCard title={t('CCTV')} icon="Videocam">
                    <Field
                      name="selectedCameras"
                      component={SelectField}
                      isMulti
                      maxOptions={3}
                      label={t('cameras')}
                      labelPlacement="start"
                      placeholder={t('SelectUpToThreeCameras')}
                      options={availableCameras}
                      mode={mode}
                      divider={null}
                      onChange={(value) => {
                        if (value.length <= 3) {
                          setFieldValue('selectedCameras', value);
                        }
                      }}
                    />
                  </SettingsCard>
                </ul>
              </CustomScrollbar>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};
