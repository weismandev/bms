import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../model';

const Filter = memo(() => {
  const filters = useStore($filters);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values }) => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('RC')}
                placeholder={t('selectRC')}
                isMulti
              />
              <Field
                name="buildings"
                component={HouseSelectField}
                complex={Array.isArray(values.complexes) ? values.complexes : []}
                label={t('buildings')}
                placeholder={t('selectHouses')}
                isMulti
              />

              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
