import { combine } from 'effector';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, CustomScrollbar } from '@ui/index';
import { $count, $barriers, launchAction, open, $opened } from '../../model';
import { ListToolbar, BarrierCard } from '../../molecules';

const useStyles = makeStyles(({ spacing }) => ({
  rootContainer: {
    height: '100%',
    padding: '24px 0px 24px 24px',
    overflow: 'hidden',
  },
  barriersList: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(330px, 1fr))',
    gap: spacing(3),
    padding: '0px 24px 0px 0px',
  },
}));

const $stores = combine({
  count: $count,
  barriers: $barriers,
  opened: $opened,
});

export const BarriersList = () => {
  const { barriersList, rootContainer } = useStyles();
  const { count, barriers, opened } = useStore($stores);

  return (
    <Wrapper className={rootContainer}>
      <ListToolbar count={count} />
      <CustomScrollbar style={{ height: 'calc(100% - 50px)' }} autoHide>
        <ul className={barriersList}>
          {barriers.map((barrier) => {
            const isActive = opened.id === (Boolean(barrier) && barrier.id);

            return (
              <BarrierCard
                active={isActive}
                open={open}
                handler={launchAction}
                key={barrier.id}
                {...barrier}
              />
            );
          })}
        </ul>
      </CustomScrollbar>
    </Wrapper>
  );
};
