import { useState } from 'react';
import { useStore } from 'effector-react';
import { IconButton } from '@mui/material';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, Toolbar, CloseButton } from '@ui/index';
import { $barrierPreview, launchAction, closeFullFrame } from '../../model';
import { Controls, ContentLoader } from '../../molecules';

const useStyles = makeStyles({
  rootContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '90vh',
    overflow: 'hidden',
  },
  fullFrameToolbar: {
    display: 'flex',
    padding: 12,
  },
  arrowWrap: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    top: '50%',
    transform: 'translateY(-50%)',

    '&>': {
      fontSize: '8rem',
      cursor: 'pointer',
    },
  },
  arrowIcon: {
    fontSize: '8rem',
    color: 'white',
    textShadow: '3px 2px 6px black',
  },
});

export const FullFrameView = () => {
  const { rootContainer, arrowWrap, arrowIcon } = useStyles();

  const { selectedCameras, action = {}, title } = useStore($barrierPreview);

  const [currentIndexCamera, switchIndexCamera] = useState(0);

  const currentCamera = selectedCameras[currentIndexCamera];
  const videoLink =
    currentCamera && currentCamera.mjpeg_new
      ? currentCamera.mjpeg_new.startsWith('https://')
        ? currentCamera.mjpeg_new
        : `https://${currentCamera.mjpeg_new}`
      : '';

  const handlePrev = () => {
    const prevIndex = currentIndexCamera - 1;
    switchIndexCamera(prevIndex >= 0 ? prevIndex : selectedCameras.length - 1);
  };

  const handleNext = () => {
    const nextIndex = currentIndexCamera + 1;
    const countCameras = selectedCameras.length;
    switchIndexCamera(nextIndex >= countCameras ? 0 : nextIndex);
  };

  return (
    <Wrapper className={rootContainer}>
      <FullFrameToolbar action={action} closeEvent={closeFullFrame} />
      <ContentLoader url={videoLink} alt={title} />
      {selectedCameras.length > 1 && (
        <div className={arrowWrap}>
          <IconButton onClick={handlePrev} size="large">
            <KeyboardArrowLeftIcon className={arrowIcon} />
          </IconButton>
          <IconButton className={arrowIcon} onClick={handleNext} size="large">
            <KeyboardArrowRightIcon className={arrowIcon} />
          </IconButton>
        </div>
      )}
    </Wrapper>
  );
};

function FullFrameToolbar({ action, closeEvent }) {
  const { fullFrameToolbar } = useStyles();

  return (
    <Toolbar className={fullFrameToolbar}>
      <Controls action={action} handler={launchAction} />
      <CloseButton style={{ margin: '0 0 0 12px' }} onClick={closeEvent} />
    </Toolbar>
  );
}
