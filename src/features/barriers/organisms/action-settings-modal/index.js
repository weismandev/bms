import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Modal } from '@ui/index';
import { $modal, closeModal } from '../../model';
import { ActionCreateContent } from '../../molecules/action-types/create';
import { ActionDeleteContent } from '../../molecules/action-types/delete';
import { ActionUpdateContent } from '../../molecules/action-types/update';

const useStyles = makeStyles(() => ({
  modal: {
    borderRadius: '15px',
    boxShadow: '0px 4px 15px #6A6A6E',
    padding: '24px 0 24px 24px',
  },
  title: {
    fontWeight: 800,
    marginBottom: 10,
  },
}));

const actionContent = {
  update: <ActionUpdateContent />,
  create: <ActionCreateContent />,
  delete: <ActionDeleteContent />,
};

const ActionSettingsTitle = ({ classes, title }) => {
  return <div className={classes.title}>{title}</div>;
};

export const ActionSettingsModal = () => {
  const classes = useStyles();

  const modal = useStore($modal);
  const { open, type, title } = modal;

  const content = actionContent[type];

  return (
    <Modal
      classes={{ paper: classes.modal }}
      isOpen={open}
      onClose={closeModal}
      header={<ActionSettingsTitle classes={classes} title={title} />}
      content={content}
    />
  );
};
