import { api } from '../../../api/api2';
import { ContainerListPayload, ContainerListResponse } from '../interfaces';

const getList = (payload: ContainerListPayload, config = {}): ContainerListResponse =>
  api.no_vers('get', 'v4/schedule/container-ticket-list', payload, config);

const getContainerDiagram = (
  payload: ContainerListPayload,
  config = {}
): ContainerListResponse =>
  api.no_vers('get', 'v4/schedule/container-schedule-list', payload, config);

export const containerApi = {
  getList,
  getContainerDiagram,
};
