import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import {
  PageGate,
  $isLoading,
  $view,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../models';
import { $isDetailOpen } from '../models/detail';
import { $isFilterOpen } from '../models/filter';
import { TableContainerDiagram, TableContainerList, Filter, Detail } from '../organisms';

const useStyles = makeStyles(() => ({
  main: {
    overflow: 'hidden',
  },
}));

const PageRoot = () => {
  const { main } = useStyles();
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const view = useStore($view);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);

  const viewDiagram = view === 'diagram';

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        params={{ detailWidth: 'minmax(370px, 50%)' }}
        main={viewDiagram ? <TableContainerDiagram /> : <TableContainerList />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
        classes={{ main }}
      />
    </>
  );
};

const RestrictedSchedulePage = (props: any) => {
  return (
    <HaveSectionAccess>
      <PageRoot {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedSchedulePage as PageRoot };
