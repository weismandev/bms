import { createStore, createEvent } from 'effector';

export const toggleDetailSlotVisibility = createEvent<boolean>();
export const $isOpen = createStore<boolean>(false);
