import { createEffect, createStore, combine } from 'effector';
import { createGate } from 'effector-react';
import { format } from 'date-fns';
import { getStatus } from '@features/job-schedule/lib';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { ContainerListPayload, ContainerListResponse } from '../../interfaces';
import { $raw } from '../page/page.model';

const { t } = i18n;

const convertFormatDate = (date: string | null) => {
  const FORMAT = 'dd.MM.YYY в HH:mm';
  return date && format(convertUTCDateToLocalDate(new Date(date)), FORMAT);
};

export const columns = [
  { name: 'region_name', title: t('Label.city') },
  { name: 'complex_title', title: t('AnObject') },
  { name: 'address', title: t('building') },
  { name: 'name', title: t('NameTitle') },
  { name: 'container_type', title: t('ContainerType') },
  { name: 'planned_start_at', title: t('PlannedPickupTime') },
  { name: 'fact_start_at', title: t('ActualPickupTime') },
  { name: 'status', title: t('Label.status') },
];

export const excludedColumnsFromSort = [
  'region_name',
  'complex_title',
  'address',
  'name',
  'container_type',
  'planned_start_at',
  'fact_start_at',
  'status',
];

export const TableGate = createGate<ContainerListPayload>();
export const tableMounted = TableGate.open;
export const tableUnmounted = TableGate.close;

export const fxGetList = createEffect<
  ContainerListPayload,
  ContainerListResponse,
  Error
>();

export const $isLoading = createStore<boolean>(false);

export const $tableData = combine($raw, ({ data = [] }) => {
  return data.map((item: any) => ({
    ...item,
    planned_start_at: convertFormatDate(item.planned_start_at),
    fact_start_at: convertFormatDate(item.fact_start_at),
    status: getStatus(item.status),
  }));
});

export const $rowCount = $raw.map(({ meta = {} }) => meta?.total || 0);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  order: [],
  widths: [
    { columnName: 'user_name', width: 300 },
    { columnName: 'region_name', width: 150 },
    { columnName: 'complex_title', width: 150 },
  ],
});
