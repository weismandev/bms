import { mergeColumnWidths } from '../../../../tools/merge-column-widths';
import { fxGetFromStorage, setDateRange } from '../page/page.model';
import {
  $tableData,
  $currentPage,
  $pageSize,
  $columnWidths,
  $columnOrder,
  $hiddenColumnNames,
} from './table.model';

$tableData.on(setDateRange, () => []);

$currentPage.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.currentPage : state
);

$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  result ? Array.from(new Set([...result.columnOrder, ...state])) : state
);

$columnWidths.on(fxGetFromStorage.doneData, (state, data) =>
  mergeColumnWidths(state, data && data.columnWidths)
);

$pageSize.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.pageSize : state
);

$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.hiddenColumnNames : state
);
