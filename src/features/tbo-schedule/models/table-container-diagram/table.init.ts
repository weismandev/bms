import { forward, guard, attach } from 'effector';
import { debounce } from 'patronum/debounce';
import { signout } from '@features/common';
import { containerApi } from '../../api';
import { DateRangeType, TableParams, DefaultFilters } from '../../interfaces';
import { dateFormatted } from '../../lib';
import { $filters, filtersSubmitted } from '../filter/filter.model';
import { $view, $dateRange, updateTimeContainerInMilliseconds } from '../page/page.model';
import { $tableParams } from '../table-container-list';
import {
  fxGetDiagram,
  $isLoading,
  $raw,
  updateContainerDiagramByTimeout,
} from './table.model';

fxGetDiagram.use(containerApi.getContainerDiagram);

$isLoading.on(fxGetDiagram.pending, (_, pending) => pending);
$raw
  .on(fxGetDiagram.done, (_, { result }) => result)
  .on($dateRange, (state) => ({ ...state, data: [] }))
  .reset(signout);

forward({
  from: debounce({
    source: fxGetDiagram.done,
    timeout: updateTimeContainerInMilliseconds,
  }),
  to: updateContainerDiagramByTimeout,
});

guard({
  clock: [
    $view,
    $dateRange,
    $tableParams,
    updateContainerDiagramByTimeout,
    filtersSubmitted,
  ],
  source: [$view, $dateRange],
  filter: ([view]) => view === 'diagram',
  target: attach({
    source: [$dateRange, $tableParams, $filters],
    effect: fxGetDiagram.prepend(
      ([{ weekStart, weekEnd }, tableParams, filters]: [
        DateRangeType,
        TableParams,
        DefaultFilters
      ]) => ({
        ...tableParams,
        filter: {
          building_ids: filters.building_ids.map(({ id }) => id),
          ticket_work_status: filters.status.map(({ id }) => id),
        },
        date_from: dateFormatted(weekStart),
        date_to: dateFormatted(weekEnd),
      })
    ),
  }),
});
