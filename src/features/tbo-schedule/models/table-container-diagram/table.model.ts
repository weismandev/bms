import { createEffect, createStore, createEvent, combine } from 'effector';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { ContainerListPayload, ContainerListResponse } from '../../interfaces';
import { weekRange } from '../../lib';
import { $dateRange } from '../page';
import { initialStateRaw } from './initialState';

const { t } = i18n;

const convertFormatDate = (date: string | null) => {
  const FORMAT = 'yyyy-MM-dd HH:mm:ss';
  return date && format(convertUTCDateToLocalDate(new Date(date)), FORMAT);
};

export const columns = [
  { name: 'region_name', title: t('Label.city') },
  { name: 'complex_title', title: t('AnObject') },
  { name: 'address', title: t('building') },
  { name: 'name', title: t('NameTitle') },
  { name: 'container_type', title: t('ContainerType') },
  { name: 'planned_start_at', title: t('PlannedPickupTime') },
  { name: 'fact_start_at', title: t('ActualPickupTime') },
  { name: 'status', title: t('Label.status') },
];

export const excludedColumnsFromSort = [
  'region_name',
  'complex_title',
  'address',
  'name',
  'container_type',
  'planned_start_at',
  'fact_start_at',
  'status',
];

export const fxGetDiagram = createEffect<
  ContainerListPayload,
  ContainerListResponse,
  Error
>();

export const updateContainerDiagramByTimeout = createEvent<void>();

export const $isLoading = createStore<boolean>(false);
export const $raw = createStore<ContainerListResponse>(initialStateRaw);

export const $tableData = combine($raw, $dateRange, ({ data = [] }, dateRange) => {
  const dates = weekRange(dateRange.weekStart, dateRange.weekEnd).map((date: Date) =>
    format(date, 'dd')
  );
  return data.map((item) => ({
    ...item,
    containers: item.containers.map((container) => ({
      ...container,
      tickets: dates.map((date) => {
        const findedContainer = container.tickets.find(
          (ticket) => format(new Date(ticket.planned_start_at), 'dd') === date
        );
        if (findedContainer) {
          return {
            ...findedContainer,
            planned_start_at: convertFormatDate(findedContainer.planned_start_at),
            planned_end_at: convertFormatDate(findedContainer.planned_end_at),
          };
        }
        return {
          ticket_number: '',
          status: 'empty',
          address: '',
          complex_title: '',
          fact_start_at: '',
          planned_end_at: '',
          planned_start_at: '',
          region_name: '',
          title: '',
        };
      }),
    })),
  }));
});

export const $rowCount = $raw.map(({ meta = {} }) => meta?.total || 0);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  order: [],
  widths: [
    { columnName: 'user_name', width: 300 },
    { columnName: 'region_name', width: 150 },
    { columnName: 'complex_title', width: 150 },
  ],
});
