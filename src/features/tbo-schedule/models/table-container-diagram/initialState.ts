import { startOfWeek, endOfWeek } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { DateRangeType, ContainerListResponse } from '../../interfaces';

const date = new Date();

export const initialStateDateRange: DateRangeType = {
  weekStart: startOfWeek(date, { locale: dateFnsLocale }),
  weekEnd: endOfWeek(date, { locale: dateFnsLocale }),
};

export const initialStateRaw: ContainerListResponse = {
  data: [],
  links: {
    first: '',
    last: '',
    next: '',
    prev: '',
  },
  meta: {
    current_page: 1,
    from: 0,
    last_page: 0,
    path: '',
    per_page: '',
    to: 0,
    total: 0,
  },
};
