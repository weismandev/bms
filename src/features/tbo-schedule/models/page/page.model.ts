import { createStore, createEvent, attach } from 'effector';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { createPageBag } from '@tools/factories';
import { containerApi } from '../../api';
import { DateRangeType } from '../../interfaces';
import { initialStateDateRange } from './initialState';

type View = 'list' | 'diagram';

const savedView = localStorage.getItem('tboScheduleView');

export const updateTimeContainerInMilliseconds = 1000 * 60 * 5;

export const $dateRange = createStore<DateRangeType>(initialStateDateRange);
export const $view = createStore<View>(
  savedView === 'list' || savedView === 'diagram' ? savedView : 'list'
);

export const setDateRange = createEvent<DateRangeType>();
export const updateContainerListByTimeout = createEvent<void>();
export const toggleView = createEvent<'list' | 'diagram'>();

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(
  containerApi,
  {},
  {
    idAttribute: 'serial_number',
    itemsAttribute: 'data',
    createdPath: '',
    updatedPath: '',
  }
);

const STORAGE_KEY = 'tbo-schedule';

export const fxSaveInStorage = attach({
  effect: fxSaveInStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
export const fxGetFromStorage = attach({
  effect: fxGetFromStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
