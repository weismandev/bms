import { forward, guard, attach, combine } from 'effector';
import { debounce } from 'patronum/debounce';
import { signout, $isAuthenticated } from '@features/common';
import { DateRangeType, TableParams, DefaultFilters } from '../../interfaces';
import { dateFormatted } from '../../lib';
import { $filters, filtersSubmitted } from '../filter/filter.model';
import {
  $tableParams,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $currentPage,
} from '../table-container-list/table.model';
import {
  pageMounted,
  fxGetList,
  fxSaveInStorage,
  fxGetFromStorage,
  toggleView,
  $view,
  $dateRange,
  setDateRange,
  updateContainerListByTimeout,
  updateTimeContainerInMilliseconds,
} from './page.model';

$view.on(toggleView, (_, view) => view);
$dateRange
  .on(setDateRange, (_, { weekStart, weekEnd }) => ({
    weekStart,
    weekEnd,
  }))
  .reset(signout);

$view.watch((view) => {
  localStorage.setItem('tboScheduleView', view);
});

forward({
  from: debounce({
    source: fxGetList.done,
    timeout: updateTimeContainerInMilliseconds,
  }),
  to: updateContainerListByTimeout,
});

guard({
  clock: [
    toggleView,
    setDateRange,
    $tableParams,
    updateContainerListByTimeout,
    filtersSubmitted,
  ],
  source: $view,
  filter: (view) => view === 'list',
  target: attach({
    source: [$dateRange, $tableParams, $filters],
    effect: fxGetList.prepend(
      ([{ weekStart, weekEnd }, tableParams, filters]: [
        DateRangeType,
        TableParams,
        DefaultFilters
      ]) => ({
        ...tableParams,
        filter: {
          building_ids: filters.building_ids.map(({ id }) => id),
          ticket_work_status: filters.status.map(({ id }) => id),
        },
        date_from: dateFormatted(weekStart),
        date_to: dateFormatted(weekEnd),
      })
    ),
  }),
});

const $savedParams = combine({
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  hiddenColumnNames: $hiddenColumnNames,
});

guard({
  source: $savedParams,
  filter: $isAuthenticated,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (params) => ({ data: params }),
  }),
});

forward({
  from: pageMounted,
  to: fxGetFromStorage,
});
