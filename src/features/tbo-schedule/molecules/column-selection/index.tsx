import { useState, FC, MouseEvent } from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import i18n from '@shared/config/i18n';
import { Props } from './types';

const { t } = i18n;

const selectionBy = [10, 25, 50, 100];

const ColumnSelection: FC<Props> = ({ pageSize, pageChanged }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuItem = (perPage: number) => () => {
    pageChanged(perPage);
    setAnchorEl(null);
  };

  return (
    <div>
      {t('showColumnSelection')}
      <Button
        id="demo-positioned-button"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        {pageSize}
      </Button>
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        {selectionBy.map((item: number) => (
          <MenuItem
            key={item}
            selected={item === pageSize}
            onClick={handleMenuItem(item)}
          >
            {item}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};

export { ColumnSelection };
