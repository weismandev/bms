export type Props = {
  pageSize: number;
  pageChanged: (perPage: number) => void;
};
