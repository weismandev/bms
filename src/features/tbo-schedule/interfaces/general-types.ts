export type Links = {
  first: string;
  last: string;
  next: string;
  prev: string;
};

export type Meta = {
  current_page: number;
  from: number;
  last_page: number;
  path: string;
  per_page: string;
  to: number;
  total: number;
};
