export type DefaultFilters = {
  building_ids: FilterBuildings[];
  user_ids: FilteEmployee[];
  status: FilterStatus[];
};

export type FilterBuildings = {
  id: number;
  title: string;
};

export type FilteEmployee = {
  id: number;
  title: string;
};

export type FilterStatus = {
  id: string;
  title: string;
};
