export * from './container-list';
export * from './filter';

enum typeOfStatus {
  Default = 'Default', // цвет белый
  Request = 'Request',
  Disable = 'Disabled',
}

export type Executor = {
  id: number | string;
  type: 'contractor-employee' | any;
  title: string;
};

export type Activity = {
  id: number | string;
  number: string;
  type: string;
  destination: string;
};

export interface ISlot {
  id: string;
  slot_type: 'view-time-slot' | any;
  from: string;
  to: string;
  composite_slot_id: string | null;
  activity: Activity;
  color: string;
}

export interface IItems {
  employee: Executor;
  slots: ISlot[];
}

export interface IScheduleResponse {
  items: IItems[];
  meta: {
    total: number;
  };
}

export interface ISchedulePayload {
  date: string;
  search: string;
  order_direction: 'asc' | 'desc';
  per_page: number;
  page: number;
  filter: any[];
}

export interface IScheduleUp {
  page: number;
  per_page: number;
  search: string;
  from: Date;
  to: Date;
}

export interface IScheduleUpdate {
  employee: Executor;
  slots: ISlot[];
}

export interface IScheduleDelete {
  employee: Executor;
  slotsId: number;
}

export interface IScheduleDetail {
  nameWork: string;
  building: string;
  room: string;
  request_uuid: string;
  date_start: string;
  date_end: string;
  status: string;
}
