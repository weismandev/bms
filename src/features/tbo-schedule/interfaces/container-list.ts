import { Links, Meta } from './general-types';

export type ContainerListPayload = {
  date_from: string;
  date_to: string;
  page: number;
  per_page: number;
  sorting: Sorting[];
  search: string;
};

export type ContainerListResponse = {
  data?: Containers[];
  links: Links;
  meta: Meta;
};

export type Containers = {
  id: number;
  address: string;
  containers: Container[];
};

export type Container = {
  container_name: string;
  serial_number: number;
  tickets: Tickets[];
};

export type Tickets = {
  address: string;
  complex_title: string;
  fact_start_at: string;
  planned_end_at: string;
  planned_start_at: string;
  region_name: string;
  status: string;
  ticket_number: string;
  title: string;
  ticket_id: number;
  serial_number?: number;
};

export type DateRangeType = {
  weekStart: Date;
  weekEnd: Date;
};

export type TableParams = {
  page: number;
  per_page: number;
  sorting: Sorting[];
  search: string;
};

export type Sorting = {
  name: string;
  order: 'asc' | 'desc';
};
