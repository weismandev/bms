import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Wrapper } from '@ui';
import {
  $mode,
  changedDetailVisibility,
  $opened,
  $isLoading,
  $error,
  $detailType,
} from '../../models/detail';
import { TicketForm, UserForm } from '../forms';

const $stores = combine({
  mode: $mode,
  opened: $opened,
  isLoading: $isLoading,
  error: $error,
  detailType: $detailType,
});

const Detail = () => {
  const { detailType } = useStore($stores);

  const closeHandler = () => {
    changedDetailVisibility(false);
  };

  const renderForms = () => {
    switch (detailType) {
      case 'ticket':
        return <TicketForm closeHandler={closeHandler} />;
      case 'user':
        return <UserForm closeHandler={closeHandler} />;
      default:
        return null;
    }
  };

  return <Wrapper style={{ height: '100%' }}>{renderForms()}</Wrapper>;
};

export { Detail };
