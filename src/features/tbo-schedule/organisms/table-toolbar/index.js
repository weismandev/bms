import { useStore } from 'effector-react';
import { Switch, FormControlLabel } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  FilterButton,
  Greedy,
  Toolbar,
  AdaptiveSearch,
  WeekControl,
} from '../../../../ui';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter';
import { setDateRange } from '../../models/page';
import { toggleView, $view } from '../../models/page/page.model';
import {
  $rowCount as $rowCountList,
  $search as $searchDiagram,
  searchChanged as searchChangedDiagram,
} from '../../models/table-container-diagram';
import {
  $rowCount,
  $search as $searchList,
  searchChanged as searchChangedList,
} from '../../models/table-container-list';
import useStyles from './styles';

const { t } = i18n;

function TableToolbar() {
  const classes = useStyles();
  const view = useStore($view);

  const rowCountSlot = useStore($rowCount);
  const rowCountList = useStore($rowCountList);
  const isFilterOpen = useStore($isFilterOpen);
  const searchValueList = useStore($searchList);
  const searchValueDiagram = useStore($searchDiagram);

  const isAnySideOpen = isFilterOpen;
  const viewDiagram = view === 'diagram';
  const totalCount = viewDiagram ? rowCountSlot : rowCountList;
  const searchValue = viewDiagram ? searchValueDiagram : searchValueList;
  const searchChanged = viewDiagram ? searchChangedDiagram : searchChangedList;

  const handleChangeViewCalendar = () => {
    toggleView(viewDiagram ? 'list' : 'diagram');
  };

  const handleWeekChange = ({ weekStart, weekEnd }) => {
    setDateRange({ weekStart, weekEnd });
  };

  const handleFilterVisibility = () => {
    changedFilterVisibility(true);
  };

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={handleFilterVisibility}
        className={classes.margin}
      />
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <div style={{ width: 300, marginRight: 20 }}>
        <WeekControl onChange={handleWeekChange} />
      </div>
      <Greedy />

      <FormControlLabel
        control={<Switch checked={viewDiagram} />}
        onChange={handleChangeViewCalendar}
        label={t('GraphicView')}
        classes={{
          label: classes.label,
        }}
      />
    </Toolbar>
  );
}

export { TableToolbar };
