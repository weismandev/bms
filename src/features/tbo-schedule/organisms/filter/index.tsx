import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { HouseSelectField } from '@features/house-select';
import { TicketWorkStatusSelectField } from '@features/ticket-work-status-list';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { FilterBuildings } from '../../interfaces';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter/filter.model';

const { t } = i18n;

const Filter = memo(() => {
  const filters = useStore($filters);

  const handleReset = () => filtersSubmitted('');
  const handleCloseFilter = () => changedFilterVisibility(false);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar style={{ padding: 24 }} closeFilter={handleCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={handleReset}
          enableReinitialize
          render={() => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="building_ids"
                component={HouseSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />
              <Field
                name="status"
                component={TicketWorkStatusSelectField}
                label={t('Label.status')}
                placeholder={t('selectStatuse')}
                isMulti
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
