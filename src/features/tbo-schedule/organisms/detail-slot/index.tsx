import React from 'react';
import { format, isValid } from 'date-fns';
import LaunchIcon from '@mui/icons-material/Launch';
import { getStatus } from '@features/job-schedule/lib';
import i18n from '@shared/config/i18n';
import { Modal } from '@ui/index';
import HouseIcon from '../../../../img/house';
import ObjectIcon from '../../../../img/object';
import { Tickets } from '../../interfaces';
import { ticketClicked } from '../../models/detail';
import useStyles from './styles';

const { t } = i18n;

type DetailSlotType = {
  isOpen: boolean;
  handleClose: () => void;
  positon: { left: number; top: number };
  ticket: Tickets;
};

const DetailSlot: React.FC<DetailSlotType> = ({
  isOpen,
  handleClose,
  positon,
  ticket,
}): JSX.Element => {
  const {
    root,
    header,
    header__titleRow,
    header__title,
    header__launch,
    header__desc,
    content,
    status,
    info,
    footer,
  } = useStyles();
  const ticketStartDate = new Date(ticket.planned_start_at);
  const isValidStartDate = isValid(ticketStartDate);
  const formatedStartDate = isValidStartDate
    ? format(ticketStartDate, 'HH:mm')
    : ticket.planned_start_at;

  const ticketEndDate = new Date(ticket.planned_end_at);
  const isValidEndDate = isValid(ticketEndDate);
  const formatedEndDate = isValidEndDate
    ? format(ticketEndDate, 'HH:mm')
    : ticket.planned_end_at;

  const statusInfo = getStatus(ticket.status as any);
  const handleClickLinkTitle = () => {
    handleClose();
    ticketClicked(ticket.ticket_id as any);
  };

  return (
    <Modal
      classes={{ root }}
      isOpen={isOpen}
      onClose={handleClose}
      style={{ position: 'absolute', top: positon.top, left: positon.left }}
      header={
        <div className={header}>
          <div className={header__titleRow}>
            <h1 className={header__title} onClick={handleClickLinkTitle}>
              {ticket.title}
            </h1>
            <LaunchIcon className={header__launch} />
          </div>

          <span className={header__desc}>{ticket.ticket_number}</span>
        </div>
      }
      content={
        <div className={content}>
          <div style={{ backgroundColor: statusInfo.color }} className={status}>
            {statusInfo.name}
          </div>
          <div className={info}>
            <HouseIcon />
            <p>{ticket.address}</p>
          </div>
          <div className={info}>
            <ObjectIcon />
            <p>{ticket.complex_title}</p>
          </div>
        </div>
      }
      actions={
        <div className={footer}>
          <div>
            <p>{t('StartWorkTime')}</p>
            <span>{formatedStartDate}</span>
          </div>
          <div>
            <p>{t('FinishWorkTime')}</p>
            <span>{formatedEndDate}</span>
          </div>
        </div>
      }
    />
  );
};

export { DetailSlot };
