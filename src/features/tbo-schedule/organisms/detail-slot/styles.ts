import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  root: {
    '& .MuiBackdrop-root': {
      backgroundColor: 'transparent !important',
    },

    '& .MuiDialog-scrollPaper': {
      display: 'initial !important',
    },

    '& .MuiDialog-paper': {
      margin: 0,
    },
  },
  header: {},
  header__titleRow: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'start',
    alignItems: 'center',
    gap: 10,
  },
  header__title: {
    cursor: 'pointer',
    color: '#3B3B50',
    fontSize: 18,
    fontWeight: 900,
    marginBlock: 0,

    '&:hover': {
      textDecoration: 'underline',
    },
  },
  header__launch: {
    color: '#65657B',
    fontSize: 18,
  },
  header__desc: {
    fontSize: 12,
    color: '#65657B',
  },
  content: {
    paddingTop: 20,
  },
  status: {
    color: 'white',
    padding: '4px 16px',
    borderRadius: 20,
    fontSize: 13,
    fontWeight: 500,
    display: 'inline-block',
    marginBottom: 20,
  },
  info: {
    display: 'flex',
    color: '#65657B',
    fontWeight: 500,
    fontSize: 13,
    marginBottom: 12,

    '& > p': {
      marginLeft: 10,
    },
  },
  footer: {
    display: 'flex',
    borderTop: '1px solid #E7E7EC',
    fontWeight: 500,
    width: '100%',

    '& > div': {
      width: '50%',
    },

    '& p': {
      color: '#9494A3',
      fontSize: 14,
      fontWeight: 500,
      padding: '16px 0px 8px',
      marginBottom: 0,
    },
    '& span': {
      color: '#65657B',
      fontSize: 13,
    },
  },
}));

export default useStyles;
