import { combine } from 'effector';
import { useStore, useGate } from 'effector-react';
import { Loader } from '@ui';
import { $isLoading } from '@features/users/models';
import { Detail as User } from '@features/users/organisms/detail';

const $stores = combine({
  isLoading: $isLoading,
});

const UserForm = ({ closeHandler }) => {
  const { isLoading } = useStore($stores);

  return (
    <>
      <Loader isLoading={isLoading} />
      <User closeHandler={closeHandler} external />
    </>
  );
};

export { UserForm };
