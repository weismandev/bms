import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Loader, DetailToolbar } from '@ui';
import { $isLoading } from '@features/tickets/models/ticket';
import { Ticket } from '@features/tickets/organisms/ticket';

const $stores = combine({
  isLoading: $isLoading,
});

const TicketForm = ({ closeHandler }) => {
  const { isLoading } = useStore($stores);

  return (
    <>
      <Loader isLoading={isLoading} />
      <DetailToolbar
        style={{ padding: 24 }}
        onClose={closeHandler}
        hidden={{ delete: true, edit: true }}
      />
      <Ticket external />
    </>
  );
};

export { TicketForm };
