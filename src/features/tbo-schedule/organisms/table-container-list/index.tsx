import React, { Fragment } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  SortingState,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  TableColumnVisibility,
  Toolbar,
  PagingPanel,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  SortingLabel,
  TableContainer,
  TableHeaderCell,
  TableRow,
  TableCell,
  Wrapper,
} from '../../../../ui';
import { Tickets } from '../../interfaces';
import { ticketClicked } from '../../models/detail';
import {
  $tableData,
  columns,
  $currentPage,
  $pageSize,
  $rowCount,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $columnOrder,
  $columnWidths,
  columnOrderChanged,
  columnWidthsChanged,
  excludedColumnsFromSort,
  sortChanged,
  hiddenColumnChanged,
  $hiddenColumnNames,
} from '../../models/table-container-list';
import { TableToolbar } from '../table-toolbar';
import useStyles from './styles';

const Root = (props: Object): JSX.Element => (
  <Grid.Root {...props} style={{ height: '100%' }} />
);

const { t } = i18n;

const Row = (props: { row: Tickets }) => {
  const handleClickRow =
    ({ row }: { row: Tickets }) =>
    () => {
      ticketClicked(row.ticket_id as any);
    };

  return <TableRow {...props} onClick={handleClickRow(props)} />;
};

const ToolbarRoot = (props: Object) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StateFormatter = ({
  value: { name, color },
}: {
  value: { name: string; color: string };
}) => <span style={{ color }}>{name}</span>;

const StatusProvider = (props: { for: string[] }) => (
  <DataTypeProvider formatterComponent={StateFormatter} {...props} />
);

const columnsExtensions: {
  columnName: string;
  align: 'left' | 'center' | 'right';
}[] = [];

const pagingPanelMessages = {
  showAll: t('ShowAll'),
  rowsPerPage: t('EntriesPerPage'),
  info: (params: { count: number; from: number; to: number }) =>
    `${params.from} - ${params.to} ${t('from')} ${params.count}`,
};

export const TableContainerList: React.FC = () => {
  const { wrapper } = useStyles();
  const data = useStore($tableData);
  const totalCount = useStore($rowCount);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnOrder = useStore($columnOrder);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  const hasLeastOneVisibleColumn = columns.length !== hiddenColumnNames.length;

  const getRowId = (row: Tickets) => {
    if (row.serial_number) {
      return row.serial_number + row.ticket_id;
    }
    return row.ticket_id;
  };

  return (
    <Wrapper className={wrapper}>
      <Grid rootComponent={Root} rows={data} getRowId={getRowId} columns={columns}>
        <SortingState
          sorting={[{ columnName: '', direction: 'asc' }]}
          onSortingChange={sortChanged}
        />

        <DragDropProvider />

        <StatusProvider for={['status']} />

        <DXTable
          columnExtensions={columnsExtensions}
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />
        <TableColumnVisibility
          data-test-id="hide-columns-btn"
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
          emptyMessageComponent={() => <></>}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        {hasLeastOneVisibleColumn && (
          <PagingPanel pageSizes={[10, 25, 50, 100]} messages={pagingPanelMessages} />
        )}
      </Grid>
    </Wrapper>
  );
};
