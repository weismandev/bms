import React, { Fragment } from 'react';
import i18n from '@shared/config/i18n';
import { DateRangeType } from '../../interfaces';
import { weekRange } from '../../lib';
import useStyles from './styles';

const { t } = i18n;

const daysWeek: string[] = [
  t('WeekDays.SundayShort').toUpperCase(),
  t('WeekDays.MondayShort').toUpperCase(),
  t('WeekDays.TuesdayShort').toUpperCase(),
  t('WeekDays.WednesdayShort').toUpperCase(),
  t('WeekDays.ThursdayShort').toUpperCase(),
  t('WeekDays.FridayShort').toUpperCase(),
  t('WeekDays.SaturdayShort').toUpperCase(),
];

const Header: React.FC<DateRangeType> = ({ weekStart, weekEnd }): JSX.Element => {
  const { header } = useStyles();
  const dates = weekRange(weekStart, weekEnd);
  return (
    <Fragment>
      {dates.map((date: Date) => {
        const indexDayWeek = date.getDay();
        return (
          <div key={indexDayWeek} className={header}>
            <span>{date.getDate()}</span>
            <span>{daysWeek[indexDayWeek]}</span>
          </div>
        );
      })}
    </Fragment>
  );
};

export default Header;
