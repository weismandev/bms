import { Fragment, useState, useEffect } from 'react';
import { useStore } from 'effector-react';
import cn from 'classnames';
import { format, isValid } from 'date-fns';
import { Pagination } from '@mui/material';
import { PaginationItem } from '@mui/lab';
import { Template } from '@devexpress/dx-react-core';
import { Grid, Toolbar } from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { Wrapper, Loader, CustomScrollbar } from '../../../../ui';
import { Tickets } from '../../interfaces';
import { getNotFoundSlot } from '../../lib';
import { $isOpen, toggleDetailSlotVisibility } from '../../models/detail-slot';
import { $dateRange } from '../../models/page';
import { $tableData, $isLoading, $rowCount } from '../../models/table-container-diagram';
import {
  $currentPage,
  $pageSize,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
} from '../../models/table-container-list';
import { ColumnSelection } from '../../molecules';
import { DetailSlot } from '../detail-slot';
import { TableToolbar } from '../table-toolbar';
import Header from './header';
import useStyles from './styles';

const { t } = i18n;

const Root = (props: any) => <Grid.Root {...props} />;

const ToolbarRoot = (props: any) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

export const TableContainerDiagram: React.FC = () => {
  const classes = useStyles();

  const isOpenDetailSlot = useStore($isOpen);
  const dateRange = useStore($dateRange);
  const data = useStore($tableData);
  const isLoading = useStore($isLoading);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);

  const countPage = totalCount ? Math.ceil(totalCount / 10) : 0;

  const [sectionHeight, setSectionHeight] = useState<number>(0);
  const [emptySlots, setEmptySlots] = useState<number[]>([0]);
  const [ticket, setTicket] = useState<Tickets | null>(null);
  const [positon, setPosition] = useState<{ left: number; top: number }>({
    left: 0,
    top: 0,
  });

  const getEmptySlots = (length: number, height: number): number[] => {
    const count = Math.floor((height - 200) / 42);
    const diff = count - length;
    return new Array(diff > 0 ? diff : 0).fill(0);
  };

  const notFoundSlot = getNotFoundSlot(dateRange.weekStart, dateRange.weekEnd);

  const handleClickTicket =
    (row: Tickets | any) => (event: React.MouseEvent<HTMLElement>) => {
      if (!isOpenDetailSlot && row !== null && row.ticket_number) {
        const { clientWidth } = document.body;
        const widthPreviewSlot = 600;
        const isShift = event.pageX + widthPreviewSlot > clientWidth;
        const left = isShift ? clientWidth - widthPreviewSlot : event.pageX;
        const top = event.pageY;
        setPosition({ left, top });
        setTicket(row);
        toggleDetailSlotVisibility(true);
      }
    };

  const handleCloseDetailTicket = () => toggleDetailSlotVisibility(false);

  useEffect(() => {
    const sectionElement = document.querySelectorAll('section');
    setSectionHeight(sectionElement[0].offsetHeight);
  }, []);

  useEffect(() => {
    setEmptySlots(getEmptySlots(data.length, sectionHeight));
  }, [data.length, isLoading, pageSize]);

  return (
    <Wrapper
      style={{
        height: '100%',
        padding: 24,
        display: 'flex',
        flexDirection: 'column-reverse',
        justifyContent: 'flex-end',
        overflow: 'hidden',
        paddingBottom: 12,
      }}
    >
      {ticket !== null && (
        <DetailSlot
          isOpen={isOpenDetailSlot}
          handleClose={handleCloseDetailTicket}
          positon={positon}
          ticket={ticket}
        />
      )}
      <Grid rootComponent={Root} rows={[]} getRowId={(row) => row.id} columns={[]}>
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <Loader isLoading={isLoading} />
        <div className={classes.paginationWrap}>
          <ColumnSelection pageSize={pageSize} pageChanged={pageSizeChanged} />
          <Pagination
            onChange={(_, page) => pageNumberChanged(page - 1)}
            page={currentPage + 1}
            count={countPage}
            classes={{ root: classes.pagination }}
            size="small"
            shape="rounded"
            renderItem={(item) => <PaginationItem {...item} />}
          />
        </div>

        <CustomScrollbar autoHide style={{ zIndex: 3 }}>
          <div className={classes.wrapperPageHeader}>
            <div className={classes.columnEmployee}>
              <div className="columnEmployeeHeader">{t('ContainerName')}</div>
            </div>
            <div className={classes.wrapperContainer}>
              <Header weekStart={dateRange.weekStart} weekEnd={dateRange.weekEnd} />
            </div>
          </div>
          <div className={classes.wrapperPage}>
            <div className={classes.columnEmployee}>
              {data.map((item) => (
                <div key={item.id}>
                  <span>{item.address}</span>
                  {item.containers.map((container) => (
                    <span key={container.serial_number}>{container.container_name}</span>
                  ))}
                </div>
              ))}
            </div>
            <div className={classes.wrapperContainer}>
              {data.length === 0 &&
                !isLoading &&
                notFoundSlot.map((item) =>
                  item.containers.map((container) =>
                    container.tickets.map((ticket, index) => (
                      <div
                        key={ticket.ticket_number || index}
                        className={cn('slot', ticket.status)}
                        style={{
                          gridColumnStart: 1,
                          gridColumnEnd: 8,
                          padding: '12px 16px',
                        }}
                      >
                        <span>{ticket.ticket_number}</span>
                        <span className="resizeble" />
                      </div>
                    ))
                  )
                )}
              {data.map((item) => (
                <Fragment key={item.id}>
                  <div style={{ gridColumnStart: 1, gridColumnEnd: 8 }} />
                  {item.containers.map((container) => (
                    <Fragment key={container.serial_number}>
                      {container.tickets.map((ticket: any, index) => {
                        const ticketDate = new Date(ticket.planned_start_at);
                        const isValidDate = isValid(ticketDate);
                        const formatedDate = isValidDate
                          ? format(ticketDate, 'HH:mm')
                          : ticket.planned_start_at;

                        return (
                          <div
                            key={ticket.ticket_number || index}
                            className={cn('slot', ticket.status)}
                            onClick={handleClickTicket(
                              ticket.status === 'empty' ? null : ticket
                            )}
                          >
                            <span>{ticket.ticket_number}</span>
                            <span>{formatedDate}</span>
                            <span className="resizeble" />
                          </div>
                        );
                      })}
                    </Fragment>
                  ))}
                </Fragment>
              ))}
              {emptySlots.map((slot, index) => (
                <div
                  key={index}
                  style={{ gridColumnStart: 1, gridColumnEnd: 8 }}
                  className={cn('slot', 'empty')}
                />
              ))}
            </div>
          </div>
        </CustomScrollbar>
      </Grid>
    </Wrapper>
  );
};
