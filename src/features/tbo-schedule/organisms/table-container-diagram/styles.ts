import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 500,
    color: '#65657B',
    borderRight: '1px solid #E7E7EC',
    borderBottom: '1px solid #E7E7EC',

    '& span': {
      '&:first-child': {
        fontSize: 18,
      },
      '&:last-child': {
        fontSize: 12,
        opacity: 0.7,
        marginLeft: 2,
      },
    },
  },
  wrapperContainer: {
    display: 'grid',
    gridTemplateColumns: 'repeat(7, 170px)',
    gridAutoRows: '40px',
    backgroundColor: 'white',

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.2)',
      borderRadius: 6,
    },

    /* нет заявки */
    '& > .white': {
      backgroundColor: 'rgb(225 235 255 / 30%)',
      padding: '11px 16px',
      textAlign: 'left',

      '& > .content > p': {
        fontWeight: 300,
        fontSize: 14,
        color: '#65657B',
      },

      '& > .resizeble': {
        backgroundColor: '#35A9E9',
      },
    },
    /* Запланировано */
    '& > .planned': {
      backgroundColor: 'rgb(170 170 170 / 30%)',
      fontWeight: 300,
      fontSize: 14,
      color: '#AAAAAA',
      padding: '4px 0px 0px 16px',
      textAlign: 'left',

      '& > .resizeble': {
        backgroundColor: '#AAAAAA',
      },
    },
    /* Выполнено */
    '& > .completed_on_time': {
      color: '#1BB169',
      backgroundColor: 'rgb(27 177 105 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#1BB169',
      },
    },
    /* Не выполнено */
    '& > .skipped': {
      color: '#EB5757',
      backgroundColor: 'rgb(235 87 87 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#EB5757',
      },
    },
    /* Отставание */
    '& > .late': {
      color: '#EC8A33',
      backgroundColor: 'rgb(236 127 0 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#EC8A33',
      },
    },
    /* Опоздание */
    '& > .completed_with_late': {
      color: '#E4C000',
      backgroundColor: 'rgb(228 192 0 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#E4C000',
      },
    },
    /* Перерыв */
    '& > .break': {
      color: '#7D7D8E',
      backgroundColor: 'rgb(231 231 236 / 70%);',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#7D7D8E',
      },
    },
    /* Отпуск */
    '& > .vacation': {
      color: '#65657B',
      backgroundColor: 'rgb(101 101 123 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#65657B',
      },
    },
    /* Нерабочее время / Отпуск / Обед / перерыв */
    '& > .not_working_time': {
      color: '#65657B',
      backgroundColor: 'rgb(101 101 123 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#65657B',
      },
    },
    '& > .slot': {
      display: 'flex',
      flexDirection: 'column',
      fontWeight: 'bold',
      fontSize: 14,
      position: 'relative',
      padding: '6px 17px',
      lineHeight: '14px',
      borderBottom: '1px solid white',

      '& .resizeble': {
        height: '100%',
        width: 4,
        position: 'absolute',
        left: 0,
        top: 0,
        cursor: 'col-resize',
      },

      '&.empty': {
        backgroundColor: 'rgb(225 235 255 / 30%);',
      },
      '&.completed': {
        color: '#1BB169',
        backgroundColor: 'rgb(27 177 105 / 30%)',

        '& > .resizeble': {
          backgroundColor: '#1BB169',
        },
      },
    },
  },
  columnEmployee: {
    width: 270,
    textAlign: 'left',
    backgroundColor: 'white',
    borderRight: '1px solid #E7E7EC',
    position: 'sticky',
    left: 0,
    zIndex: 2,

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.2)',
      borderRadius: 6,
    },

    '& > div': {
      fontWeight: 500,
      fontSize: 14,
      color: '#767676',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      display: 'flex',
      flexDirection: 'column',
      width: 270,

      '& > span': {
        height: 40,
        marginLeft: 10,
        whiteSpace: 'normal',
        display: 'flex',
        alignItems: 'center',
        paddingRight: 4,
        lineHeight: '14px',

        '&:first-child': {
          fontWeight: 900,
          fontSize: 14,
          marginLeft: 0,
        },
      },
    },

    '& > .columnEmployeeHeader': {
      fontWeight: 900,
      fontSize: 18,
      color: '#7D7D8E',
      borderBottom: '1px solid #E7E7EC',
      height: 40,
      padding: '10px 2px',
    },
  },

  paginationWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    boxShadow: 'inset 0px 5px 15px -10px #DCDCDC',
    position: 'relative',
    width: 'calc(100% + 48px)',
    left: '-24px',
    bottom: '-24px',
    zIndex: 2,
    padding: '12px 24px 0px',
    top: 0,

    '& > div': {
      width: '50%',
    },
  },
  pagination: {
    display: 'flex',
    flexWrap: 'nowrap',
    paddingLeft: '26px',
    minHeight: '10px',

    '& > ul': {
      // position: 'absolute',
      // top: '40px',

      '& button': {
        width: '20px',
        height: '36px',
        fontWeight: 600,
      },
    },
  },
  wrapperPage: {
    display: 'flex',
  },
  wrapperPageHeader: {
    display: 'flex',
    position: 'sticky',
    top: 0,
    zIndex: 3,
  },
}));

export default useStyles;
