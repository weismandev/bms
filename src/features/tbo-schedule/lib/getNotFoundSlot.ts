import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { Containers } from '../interfaces';

const { t } = i18n;

const FORMAT = 'yyyy-MM-dd HH:mm:ss';

const getNotFoundSlot = (dateStart: Date, dateEnd: Date): [Containers] => {
  return [
    {
      id: 1942978,
      address: '',
      containers: [
        {
          container_name: '',
          serial_number: 1,
          tickets: [
            {
              ticket_id: 1,
              address: 'г. Пермь, ул. Несуществующая, 42',
              complex_title: 'ЖК Тестовый',
              fact_start_at: '',
              planned_start_at: format(dateStart, FORMAT),
              planned_end_at: format(dateEnd, FORMAT),
              region_name: 'Воронеж',
              status: 'not_working_time',
              ticket_number: t('NoRequestsForMSWRemovalScheduledToday'),
              title: 'Вывоз отходов',
            },
          ],
        },
      ],
    },
  ];
};

export { getNotFoundSlot };
