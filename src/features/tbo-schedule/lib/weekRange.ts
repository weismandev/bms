import startOfDay from 'date-fns/startOfDay';

const weekRange = (startDate: Date, endDate: Date): Date[] => {
  const dateArray: Date[] = [];
  const currentDate: Date = new Date(startDate);

  while (currentDate <= new Date(endDate)) {
    dateArray.push(startOfDay(new Date(currentDate)));
    currentDate.setUTCDate(currentDate.getUTCDate() + 1);
  }

  return dateArray;
};

export default weekRange;
