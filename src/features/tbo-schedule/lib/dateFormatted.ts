import { format } from 'date-fns';
import { convertNumberToHourse } from './convertNumberToHourse';

const dateFormatted = (sourceDate: Date): string => {
  const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  const date = new Date();
  const timezoneOffset = date.getTimezoneOffset();
  const convertedTimezone = convertNumberToHourse(timezoneOffset);

  return `${format(sourceDate, FORMAT)}${convertedTimezone}`;
};

export { dateFormatted };
