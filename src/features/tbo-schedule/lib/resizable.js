import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react';

/*
EXAMPLE USE

const row2 = (
  <>
    {ResizableBox('approve', { gridColumnStart: 1, gridColumnEnd: 3, zIndex: 5 }, <span>Свободно</span>)}
    {ResizableBox('delete', { gridColumnStart: 3, gridColumnEnd: 10, zIndex: 4 }, <span>Уборка придомовой территории</span>)}
    {ResizableBox('orange', { gridColumnStart: 10, gridColumnEnd: 12, zIndex: 3 }, <span>Обед</span>)}
    {ResizableBox('break', { gridColumnStart: 12, gridColumnEnd: 16, zIndex: 2 }, <span>Перерыв</span>)}
    {ResizableBox('break', { gridColumnStart: 16, gridColumnEnd: 25, zIndex: 1 }, <span>Перерыв2</span>)}
  </>
);

*/
const step = 56;

const ResizableBox = (status, styles, children) => {
  const node = useRef(null);
  const [sizeInfo, setSizeInfo] = useState({
    isResizing: false,
    origin: {},
    initialSize: { minX: '' },
    translation: { x: 'auto' },
    lastTranslation: { x: 'auto' },
  });
  const [gridColumn, setGridColumn] = useState({
    start: styles.gridColumnStart,
    end: styles.gridColumnEnd,
  });

  const onMouseDown = useCallback(
    (e) => {
      if (sizeInfo.isResizing) return;
      setSizeInfo((state) => ({
        ...state,
        isResizing: !state.isResizing,
        origin: { x: e.clientX },
        lastTranslation: sizeInfo.translation,
      }));
    },
    [sizeInfo.isResizing, sizeInfo.translation]
  );

  const onMouseMove = useCallback(
    (e) => {
      const beforeX = sizeInfo.origin.x - sizeInfo.lastTranslation.x;
      setSizeInfo((state) => ({
        ...state,
        translation: {
          x:
            e.clientX > beforeX + sizeInfo.initialSize.minX
              ? e.clientX - beforeX
              : sizeInfo.initialSize.minX,
        },
      }));
    },
    [sizeInfo.origin, sizeInfo.lastTranslation]
  );

  const onMouseUp = useCallback(() => {
    setSizeInfo((state) => ({
      ...state,
      isResizing: false,
    }));
  }, []);

  useEffect(() => {
    if (node && node.current) {
      const { width, x } = node.current.getBoundingClientRect();
      setSizeInfo((state) => ({
        ...state,
        origin: { x },
        initialSize: { minX: step, x, width },
        translation: { x: width },
        lastTranslation: { x: width },
      }));
    }
  }, []);
  useEffect(() => {
    if (sizeInfo.isResizing) {
      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);
    }

    const diff = sizeInfo.translation.x - sizeInfo.lastTranslation.x;

    if (diff > 0) {
      const gridColumnEnd = gridColumn.end + 1;
      const finishWidth = (gridColumnEnd - gridColumn.start) * step;
      setGridColumn({
        ...gridColumn,
        end: gridColumnEnd,
      });
      setSizeInfo((state) => ({
        ...state,
        initialSize: { ...state.initialSize, width: finishWidth },
        translation: { x: finishWidth },
        lastTranslation: { x: finishWidth },
      }));

      const nextNode = node.current.nextSibling;
      if (nextNode !== null) {
        const nextNodeGridStart =
          Number(window.getComputedStyle(nextNode, null).gridColumnStart) + 1;
        const nextNodeGridEnd = Number(
          window.getComputedStyle(nextNode, null).gridColumnEnd
        );
        nextNode.style.gridColumnStart = nextNodeGridStart;
        nextNode.style.width = `${(nextNodeGridEnd - nextNodeGridStart) * step}px`;
      }
    } else if (diff < 0) {
      const gridColumnEnd = gridColumn.end - 1;
      const finishWidth = (gridColumnEnd - gridColumn.start) * step;
      setGridColumn({
        ...gridColumn,
        end: gridColumnEnd,
      });
      setSizeInfo((state) => ({
        ...state,
        initialSize: { ...state.initialSize, width: finishWidth },
        translation: { x: finishWidth },
        lastTranslation: { x: finishWidth },
      }));
      const nextNode = node.current.nextSibling;
      if (nextNode !== null) {
        const nextNodeGridStart =
          Number(window.getComputedStyle(nextNode, null).gridColumnStart) - 1;
        const nextNodeGridEnd = Number(
          window.getComputedStyle(nextNode, null).gridColumnEnd
        );
        nextNode.style.gridColumnStart = nextNodeGridStart;
        nextNode.style.width = `${(nextNodeGridEnd - nextNodeGridStart) * step}px`;
      }
      /* PREV
      const prevNode = node.current.previousSibling;
      if (prevNode !== null) {
        const prevNodeGridStart = Number(window.getComputedStyle(prevNode, null).gridColumnStart);
        const prevNodeGridEnd = Number(window.getComputedStyle(prevNode, null).gridColumnEnd) - 1;
        prevNode.style.gridColumnStart = prevNodeGridEnd;
        prevNode.style.width = `${
          (prevNodeGridEnd - prevNodeGridStart) * step
        }px`;
      }
      */
    } else {
      setSizeInfo((state) => ({
        ...state,
        translation: { x: state.initialSize.width },
        lastTranslation: { x: state.initialSize.width },
      }));
    }

    return () => {
      document.removeEventListener('mousemove', onMouseMove);
      document.removeEventListener('mouseup', onMouseUp);
    };
  }, [sizeInfo.isResizing]);

  const componentStyle = useMemo(
    () => ({
      width: `${sizeInfo.translation.x}px`,
      minWidth: `${sizeInfo.initialSize.minX}px}`,
      userSelect: sizeInfo.isResizing ? 'none' : '',
      position: 'relative',
      gridColumnStart: gridColumn.start,
      gridColumnEnd: gridColumn.end,
    }),
    [sizeInfo.translation, sizeInfo.isResizing, gridColumn]
  );

  return (
    <div
      className={status}
      style={{
        ...componentStyle,
      }}
      ref={node}
    >
      {children}
      <span className="resizeble" onMouseDown={onMouseDown} />
    </div>
  );
};

export default ResizableBox;
