import { api } from '../../../api/api2';

const getFilteredList = (payload) => api.v1('post', 'tickets/filter-list', payload);

const getTicketById = (payload) => api.v1('get', 'tickets/get-by-id', payload);

const createTicket = (payload) =>
  api.v1('post', 'tickets/create', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const updateTicket = (payload) => api.v1('post', 'tickets/update', payload);
const notify = (id) => api.v1('post', 'ticket-notification/notify', { ticket_id: id });

const downloadTicketsList = (payload) =>
  api.v1(
    'request',
    'tickets/download-list',
    { ...payload, per_page: 100000 },
    {
      config: {
        responseType: 'arraybuffer',
      },
    }
  );

const getRelatedBuilding = (apartment_id) =>
  api.v1('get', 'apartment/get-apartment-relations', { apartment_id });

const ticketExport = (id) => api.v1('post', 'tickets/export', { id });

const getRentTypes = () => api.v1('get', 'property/get-types');

const markTicketAsRead = (id) => api.v1('post', 'tickets/mark-read', { id });
const markTicketAsUnread = (id) => api.v1('post', 'tickets/mark-unread', { id });

const createTicketClone = (parent_ticket_id) =>
  api.v1('post', 'tickets/clone', { parent_ticket_id });

export const ticketsApi = {
  getFilteredList,
  getTicketById,
  createTicket,
  updateTicket,
  downloadTicketsList,
  ticketExport,
  getRentTypes,
  getRelatedBuilding,
  notify,
  markTicketAsRead,
  markTicketAsUnread,
  createTicketClone,
};
