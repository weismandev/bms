import { useEffect } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { useRouteMatch } from 'react-router';

import { ErrorMessage, Loader, TwoColumnLayout } from '../../../ui';

import { history } from '../../common';
import { $isSocketsEnabled } from '../../common/model';
import { currentPageChanged } from '@features/navbar/models';
import { HaveSectionAccess } from '@features/common';
import { MessagePanelAdapter } from '../organisms/message-panel-adapter';
import { MessagesPanel } from '../../chats-old'; // удалить после релиза сокетов
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';

import { Detail } from '../organisms';

import { $isDetailLoading, $opened, DetailPageGate } from '../models/detail.model';

import {
  $error,
  $isErrorDialogOpen,
  $sectionPath,
  changedErrorDialogVisibility,
} from '../models/page.model';

const $ticketStores = combine({
  path: $sectionPath,
  opened: $opened,
  error: $error,
  isLoading: $isDetailLoading,
  isErrorDialogOpen: $isErrorDialogOpen,
  sockets: $isSocketsEnabled,
});

const TicketPage = () => {
  const { path, opened, isLoading, sockets, isErrorDialogOpen, error } =
    useStore($ticketStores);
  const match = useRouteMatch(`/${path}/:id`);

  useEffect(() => {
    currentPageChanged({
      title:
        match.url === `/${path}/new`
          ? 'Создание заявки'
          : opened.number
          ? `Заявка №${opened.number}`
          : '',
      back: () => history.push(`/${path}`),
    });
  }, [opened]);

  const socketsChat = opened?.id ? <MessagePanelAdapter chatId={opened.chatId} /> : null;

  const olderChat = opened?.chatId ? (
    <MessagesPanel openedChannel={opened.chatId} />
  ) : null;

  return (
    <>
      <ColoredTextIconNotification />
      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <DetailPageGate />
      {isLoading ? null : (
        <TwoColumnLayout left={<Detail />} right={sockets ? socketsChat : olderChat} />
      )}
    </>
  );
};

const RestrictedTicketPage = (props) => {
  const path = useStore($sectionPath);
  return (
    <HaveSectionAccess>
      <TicketPage {...props} />
    </HaveSectionAccess>
  );
};

export default RestrictedTicketPage;
