import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isFilterOpen,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
} from '../models';
import { Table, Filter } from '../organisms';

const TicketsPage = (props) => {
  const isFilterOpen = useStore($isFilterOpen);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={isFilterOpen ? <Filter /> : null}
        main={<Table />}
        detail={null}
      />
    </>
  );
};

const RestrictedTicketsPage = (props) => (
  <HaveSectionAccess>
    <TicketsPage />
  </HaveSectionAccess>
);

export default RestrictedTicketsPage;
