import { ChatPanel } from '@features/socket-chats-reworked/organisms/chat-panel';

export const MessagePanelAdapter = ({ chatId }) => {
  return <ChatPanel channelId={chatId} standalone />;
};
