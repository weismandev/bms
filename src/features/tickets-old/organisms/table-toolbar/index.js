import { useStore } from 'effector-react';
import AddIcon from '@mui/icons-material/Add';
import {
  Toolbar,
  FoundInfo,
  ExportButton,
  FilterButton,
  Greedy,
  ActionButton,
} from '../../../../ui';
import {
  $rowCount,
  addClicked,
  changedFilterVisibility,
  $isFilterOpen,
  exportClicked,
} from '../../models';
import { $tableData, $hiddenColumnNames, $columns } from '../../models/table.model';

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const tableData = useStore($tableData);
  const columns = useStore($columns);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ margin: '0 10px' }} />
      <Greedy />
      <ActionButton
        kind="positive"
        onClick={addClicked}
        style={{ margin: '0 10px', padding: '0 40px 0 30px' }}
      >
        <AddIcon style={{ marginRight: 10 }} />
        Создать заявку
      </ActionButton>
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        onClick={exportClicked}
        style={{ margin: '0 10px' }}
      />
    </Toolbar>
  );
};

export { TableToolbar };
