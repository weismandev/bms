import { combine } from 'effector';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { Formik, Form, Field, FastField } from 'formik';
import * as Yup from 'yup';
import { Tooltip } from '@mui/material';
import { FileCopyOutlined, MailOutline } from '@mui/icons-material';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SwitchField,
  ActionIconButton,
  ExportButton,
  CustomScrollbar,
} from '../../../../ui';
import { ApartmentSelectField } from '../../../apartment-select';
import { history, $isCompany } from '../../../common';
import { ComplexSelectField } from '../../../complex-select';
import { EmployeeSelectField } from '../../../employee-select';
import { EnterpriseSelectField } from '../../../enterprise-select';
import { EnterprisesAvailablePropertiesSelectField } from '../../../enterprises-available-properties-select';
import { EnterprisesCompaniesSelectField } from '../../../enterprises-companies-select';
import { HouseSelectField } from '../../../house-select';
import { PropertySelectField } from '../../../property-select';
import { StateSelectField } from '../../../state-select';
import { TypeSelectField } from '../../../type-select';
import { UrgencySelectField } from '../../../urgency-select';
import {
  $mode,
  changeMode,
  $opened,
  detailSubmitted,
  notifyClicked,
  exportClicked,
  cloneTicketClicked,
  parentTicketClicked,
} from '../../models/detail.model';
import { $tableData, $hiddenColumnNames, $columns } from '../../models/table.model';
import { ObjectTypeSelectField } from '../object-type-select';

const isRent = (objectType) => {
  const rentTypes = ['property', 'property_type', 'property_service'];
  return objectType && rentTypes.includes(objectType.slug);
};

const getIsRentDateAvailable = (rent) => {
  return rent && rent.start_date && rent.finish_date;
};

const isApartment = (objectType) => {
  return objectType && objectType.slug === 'apartment';
};

const isBuilding = (objectType) => {
  return objectType && objectType.slug === 'building';
};

const isComplex = (objectType) => {
  return objectType && objectType.slug === 'complex';
};

const getRentType = (objectType) => {
  let rentType;

  switch (objectType && objectType.slug) {
    case 'property_type':
      rentType = 'rent_type';
      break;
    case 'property':
    case 'property_service':
      rentType = 'rent_object';
      break;
    default:
      rentType = '';
  }

  return rentType;
};

const $stores = combine({
  mode: $mode,
  opened: $opened,
  isCompany: $isCompany,
  tableData: $tableData,
  hiddenColumnNames: $hiddenColumnNames,
  columns: $columns,
});

const getValidationSchema = (isNew, isCompany) => {
  const errorMessage = 'Поле обязательно для заполнения!';

  const objectTypeValidationSchema = Yup.object().shape({
    objectType: Yup.mixed().required(errorMessage),
  });

  const createdOnlyValidationSchema = Yup.object().shape({
    performer: Yup.mixed().required(errorMessage),
    type: Yup.mixed().required(errorMessage),
  });

  const commonValidationSchema = Yup.object().shape({
    text: Yup.string().required(errorMessage),
    title: Yup.string().required(errorMessage),
  });

  const defaultValidationSchema = isCompany
    ? commonValidationSchema
    : commonValidationSchema.concat(objectTypeValidationSchema);

  return isNew
    ? defaultValidationSchema
    : defaultValidationSchema.concat(createdOnlyValidationSchema);
};

export const Detail = (props) => {
  const { mode, opened, isCompany, tableData, hiddenColumnNames, columns } =
    useStore($stores);
  const isNew = !opened.id;

  const handleSubmit = (values) => {
    detailSubmitted({
      ...values,
      date_start: format(new Date(values.date_start), "yyyy-MM-dd'T'HH:mm"),
    });
  };

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        validationSchema={getValidationSchema(isNew, isCompany)}
        enableReinitialize
        onSubmit={handleSubmit}
        render={({ values, setFieldValue, resetForm, ...rest }) => {
          const { objectType, building, rent } = values;
          const isRentDatesAvailable = getIsRentDateAvailable(rent);
          const isRentTicket = isRent(objectType);
          const rentFor = getRentType(objectType);

          const isApartmentTicket = isApartment(objectType);
          const isBuildingTicket = isBuilding(objectType);
          const isComplexTicket = isComplex(objectType);

          const cancel = () => {
            if (isNew) {
              history.push('/tickets');
            } else {
              resetForm();
              changeMode('view');
            }
          };

          const modeOnlyEditWhenNew = isNew ? 'edit' : 'view';

          return (
            <Form style={{ height: '100%' }}>
              <DetailToolbar
                hidden={{
                  delete: true,
                  close: true,
                  save: isCompany && !isNew,
                  edit: isCompany && !isNew,
                  cancel: isCompany && !isNew,
                }}
                style={{
                  padding: '24px 24px 12px 24px',
                  height: 84,
                  flexWrap: 'wrap',
                }}
                mode={mode}
                onEdit={() => changeMode('edit')}
                onCancel={cancel}
              >
                {!isNew && mode === 'view' && (
                  <>
                    <Tooltip title="Отправить в тех. поддержку">
                      <ActionIconButton
                        style={{ order: 51, marginRight: 10 }}
                        onClick={notifyClicked}
                      >
                        <MailOutline />
                      </ActionIconButton>
                    </Tooltip>
                    <Tooltip title="Клонировать заявку">
                      <ActionIconButton
                        style={{ order: 50, marginRight: 10 }}
                        onClick={cloneTicketClicked}
                      >
                        <FileCopyOutlined />
                      </ActionIconButton>
                    </Tooltip>
                    <ExportButton
                      {...{ columns, tableData, hiddenColumnNames }}
                      onClick={exportClicked}
                      style={{ order: 52, marginRight: 5 }}
                    />
                  </>
                )}
              </DetailToolbar>
              {opened.parent_ticket && opened.parent_ticket.id && (
                <div
                  style={{
                    color: 'red',
                    fontSize: 16,
                    padding: '0 24px',
                    height: 30,
                  }}
                  onClick={() => parentTicketClicked(opened.parent_ticket.id)}
                >
                  Связана с заявкой{' '}
                  <span
                    style={{
                      fontWeight: 'bold',
                      cursor: 'pointer',
                    }}
                  >
                    №{opened.parent_ticket.number}
                  </span>
                </div>
              )}

              <div
                style={{
                  height: 'calc(100% - 84px - 30px)',
                  padding: '0 6px 24px 24px',
                }}
              >
                <CustomScrollbar autoHide trackVerticalStyle={{ right: 0 }}>
                  <div style={{ paddingRight: 12 }}>
                    {isCompany ? (
                      <>
                        <ThemeField mode={mode} />
                        <TextField mode={mode} />
                        <TypeField mode={mode} />
                        <CreationDateTimeField />
                        {isNew && (
                          <>
                            <EnterpriseField mode={mode} />
                            <PropertyField
                              enterprise_id={
                                Boolean(values.enterprise) &&
                                typeof values.enterprise === 'object'
                                  ? values.enterprise.id
                                  : values.enterprise
                              }
                              mode={mode}
                            />
                          </>
                        )}
                      </>
                    ) : (
                      <>
                        <ThemeField mode={mode} />
                        <TextField mode={mode} />
                        <ServiceFlag mode={mode} />
                        {!isNew && <ApplicantFields />}
                        <ObjectTypeField mode={modeOnlyEditWhenNew} />

                        {isApartmentTicket && (
                          <ApartmentFields
                            bldId={building && building.id}
                            setFieldValue={setFieldValue}
                            mode={modeOnlyEditWhenNew}
                          />
                        )}
                        {isBuildingTicket && (
                          <BuildingFields mode={modeOnlyEditWhenNew} />
                        )}
                        {isComplexTicket && <ComplexFields mode={modeOnlyEditWhenNew} />}
                        {isRentTicket && (
                          <RentFields
                            mode={mode}
                            isNew={isNew}
                            rentFor={rentFor}
                            isRentDatesAvailable={isRentDatesAvailable}
                            attributes={rent && rent.attributes}
                          />
                        )}

                        <StateField mode={mode} />
                        <UrgencyField mode={mode} />
                        <PerformerField mode={mode} bldId={building && building.id} />
                        <TypeField mode={mode} />
                        <CreationDateTimeField />
                        <DeadlineField mode={mode} />
                        <RatingField mode={mode} />
                      </>
                    )}
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

const ThemeField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="title"
      mode={mode}
      label="Тема заявки"
      component={InputField}
      placeholder="Введите тему"
      labelPlacement="start"
    />
  );
};

const TextField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="text"
      mode={mode}
      label="Текст заявки"
      multiline
      rowsMax={10}
      component={InputField}
      placeholder="Введите текст"
    />
  );
};

const ServiceFlag = (props) => {
  const { mode } = props;

  return (
    <Field
      component={SwitchField}
      name="isService"
      disabled={mode === 'view'}
      required={false}
      helpText="Сервисная заявка - заявка, инициируемая сотрудником ( без заявителя )"
      labelPlacement="start"
      label="Сервисная заявка"
    />
  );
};

const ApplicantFields = (props) => {
  return (
    <>
      <ApplicantFullnameField />
      <ApplicantPhoneField />
      <ApplicantEmailField />
      <ApplicantCompanyField />
    </>
  );
};

const ApplicantFullnameField = (props) => {
  return (
    <Field
      name="applicantFullname"
      component={InputField}
      label="Заявитель"
      labelPlacement="start"
      readOnly
    />
  );
};

const ApplicantPhoneField = (props) => {
  return (
    <Field
      name="applicantPhone"
      component={InputField}
      label="Телефон заявителя"
      labelPlacement="start"
      readOnly
    />
  );
};

const ApplicantEmailField = (props) => {
  return (
    <Field
      name="applicantEmail"
      component={InputField}
      label="Email заявителя"
      labelPlacement="start"
      readOnly
    />
  );
};

const ApplicantCompanyField = (props) => {
  return (
    <Field
      name="applicantCompany"
      component={EnterpriseSelectField}
      label="Компания заявителя"
      labelPlacement="start"
      placeholder="Введите название"
      readOnly
    />
  );
};

const ObjectTypeField = (props) => {
  const { mode } = props;
  return (
    <Field
      component={ObjectTypeSelectField}
      name="objectType"
      mode={mode}
      label="Объект заявки"
      labelPlacement="start"
      placeholder="Выберите объект заявки"
    />
  );
};

const ApartmentFields = (props) => {
  const { bldId, setFieldValue, mode } = props;

  const aptName = 'apartment';
  const bldName = 'building';

  const changeBld = (value) => {
    setFieldValue(bldName, value);
    setFieldValue(aptName, '');
  };

  return (
    <>
      <HouseField onChange={changeBld} name={bldName} mode={mode} />
      <ApartmentField name={aptName} mode={mode} bldId={bldId} />
    </>
  );
};

const HouseField = (props) => {
  return (
    <Field
      component={HouseSelectField}
      labelPlacement="start"
      label="Дом"
      placeholder="Выберите дом"
      name="building"
      {...props}
    />
  );
};

const ApartmentField = (props) => {
  const { bldId, ...rest } = props;

  return (
    <Field
      component={ApartmentSelectField}
      building_id={bldId}
      isDisabled={!bldId}
      helpText="Сначала необходимо выбрать дом"
      label="Кв."
      labelPlacement="start"
      placeholder="Выберите квартиру"
      {...rest}
    />
  );
};

const BuildingFields = (props) => {
  const { mode } = props;

  return <HouseField mode={mode} />;
};

function ComplexFields(props) {
  const { mode } = props;
  return <ComplexField mode={mode} />;
}

const ComplexField = (props) => {
  const { mode } = props;

  return (
    <Field
      component={ComplexSelectField}
      name="complex"
      mode={mode}
      labelPlacement="start"
      label="Комплекс"
      placeholder="Выберите комплекс"
    />
  );
};

const RentFields = (props) => {
  const { attributes, rentFor, isNew, mode, isRentDatesAvailable } = props;
  const excludeAttrs = ['is_office'];

  const attributesFields =
    Array.isArray(attributes) &&
    attributes
      .filter((attr) => !excludeAttrs.includes(attr.key))
      .map((attr, idx) => {
        return (
          <RentAttributeField
            label={attr.title}
            name={`rent.attributes[${idx}].${attr.key}`}
            key={attr.key}
            value={attr.value}
          />
        );
      });

  return (
    !isNew && (
      <>
        {isRentDatesAvailable && (
          <>
            <StartRentDateField />
            <EndRentDateField />
          </>
        )}
        {rentFor === 'rent_object' && (
          <>
            <PlacementField />
            <CabinetField />
          </>
        )}
        {attributesFields}
      </>
    )
  );
};

const StartRentDateField = (props) => {
  return (
    <FastField
      label="Дата начала аренды"
      labelPlacement="start"
      readOnly
      component={InputField}
      inputProps={{ type: 'date' }}
      name="rent.start_date"
    />
  );
};

const EndRentDateField = (props) => {
  return (
    <FastField
      label="Дата окончания аренды"
      labelPlacement="start"
      component={InputField}
      inputProps={{ type: 'date' }}
      name="rent.finish_date"
      readOnly
    />
  );
};

const PlacementField = (props) => {
  return (
    <FastField
      label="Помещение"
      labelPlacement="start"
      mode="view"
      component={PropertySelectField}
      name="rent.object"
    />
  );
};

const CabinetField = (props) => {
  return (
    <FastField
      mode="view"
      label="Кабинет"
      labelPlacement="start"
      component={InputField}
      name="cabinet"
    />
  );
};

const RentAttributeField = (props) => {
  return <FastField labelPlacement="start" readOnly component={InputField} {...props} />;
};

const StateField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="state"
      mode={mode}
      component={StateSelectField}
      label="Статус заявки"
      labelPlacement="start"
      placeholder="Выберите статус"
      divider
    />
  );
};

const UrgencyField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="urgency"
      mode={mode}
      component={UrgencySelectField}
      label="Срочность выполнения"
      labelPlacement="start"
      placeholder="Выберите срочность"
      divider
    />
  );
};

const PerformerField = (props) => {
  const { mode, bldId } = props;

  return (
    <Field
      name="performer"
      mode={mode}
      buildings={bldId ? [bldId] : ''}
      component={EmployeeSelectField}
      label="Исполнитель"
      labelPlacement="start"
      placeholder="Выберите исполнителя"
      divider
    />
  );
};

const TypeField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="type"
      mode={mode}
      component={TypeSelectField}
      label="Тип заявки"
      labelPlacement="start"
      placeholder="Выберите тип"
      divider
    />
  );
};

const CreationDateTimeField = (props) => {
  return (
    <FastField
      label="Дата и время создания"
      labelPlacement="start"
      readOnly
      component={InputField}
      inputProps={{ type: 'datetime-local' }}
      name="date_start"
    />
  );
};

const DeadlineField = (props) => {
  const { mode } = props;

  return (
    <Field
      label="Срок выполнения"
      labelPlacement="start"
      component={InputField}
      inputProps={{ type: 'date' }}
      name="date_end"
      mode={mode}
    />
  );
};

const RatingField = (props) => {
  const { mode } = props;

  return (
    <Field
      label="Оценка заявки"
      labelPlacement="start"
      placeholder="Появится после выполнения"
      component={InputField}
      name="rating"
      mode={mode}
      readOnly
    />
  );
};

const EnterpriseField = (props) => {
  const { mode } = props;

  return (
    <Field
      name="enterprise"
      component={EnterprisesCompaniesSelectField}
      firstAsDefault
      mode={mode}
      label="Компания"
    />
  );
};

const PropertyField = (props) => {
  const { mode, enterprise_id } = props;

  return (
    <Field
      name="property"
      component={EnterprisesAvailablePropertiesSelectField}
      mode={mode}
      enterprise_id={enterprise_id}
      label="Помещение"
    />
  );
};
