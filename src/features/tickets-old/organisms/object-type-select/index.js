import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import {
  $data,
  $isLoading,
  $error,
  fxGetList,
  disabledOptions,
} from '../../models/select.model';

const ObjectTypeSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl
      options={options}
      isOptionDisabled={(option) => disabledOptions.includes(option.slug)}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const ObjectTypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ObjectTypeSelect />} onChange={onChange} {...props} />;
};

export { ObjectTypeSelect, ObjectTypeSelectField };
