import { useMemo, useCallback, memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  SortingState,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import { TableToolbar } from '..';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  TableToggle,
  SortingLabel,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  $rowCount,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $unreadTickets,
  open,
  $sorting,
  sortChanged,
  excludedColumnsFromSort,
} from '../../models';
import { changeReadStatus, $columns } from '../../models/table.model';

const Row = (props) => {
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} onRowClick={onRowClick} />,
    [props.children]
  );

  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StateFormatter = ({ value }) => {
  const styles = {
    new: {
      background: '#1BB169',
      color: '#fff',
    },
    work: {
      background: '#1c90fb',
      color: '#fff',
    },
    done: {
      background: '#a0a7bd',
      color: '#fff',
    },
    default: {
      color: 'rgba(0,0,0,.8)',
    },
  };

  return (
    <Chip
      component="span"
      label={value.title}
      style={{
        fontWeight: 500,
        width: 200,
        cursor: 'pointer',
        ...(styles[value.slug] || styles.default),
      }}
    />
  );
};

const StateTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StateFormatter} {...props} />
);

const DebtFormatter = ({ value }) => {
  const hasDebt = value;
  return (
    <b style={{ color: hasDebt ? 'red' : 'green' }}>{hasDebt ? 'Имеет' : 'Не имеет'}</b>
  );
};

const DebtTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={DebtFormatter} {...props} />
);

const ReadStatusFormatter = ({ value, row }) => {
  return row.is_read ? value : <b style={{ color: '#3B3B50' }}>{value}</b>;
};

const ReadStatusProvider = (props) => (
  <DataTypeProvider formatterComponent={ReadStatusFormatter} {...props} />
);

const $stores = combine({
  data: $tableData,
  currentPage: $currentPage,
  pageSize: $pageSize,
  rowCount: $rowCount,
  columnWidths: $columnWidths,
  columnOrder: $columnOrder,
  hiddenColumnNames: $hiddenColumnNames,
  unreadTickets: $unreadTickets,
  columns: $columns,
  sorting: $sorting,
});

const Table = memo((props) => {
  const {
    data,
    currentPage,
    pageSize,
    rowCount,
    columnWidths,
    columnOrder,
    hiddenColumnNames,
    unreadTickets,
    columns,
    sorting,
  } = useStore($stores);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <StateTypeProvider for={['state']} />
        <DebtTypeProvider for={['debt']} />
        <ReadStatusProvider
          for={[
            'number',
            'title',
            'date_start',
            'updated_at',
            'date_end',
            'urgency',
            'address',
            'applicant',
            'phone',
            'enterprise',
            'debt',
            'competence',
            'performer',
          ]}
        />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={rowCount} />

        <DragDropProvider />
        <DXTable
          messages={{ noData: 'Нет данных' }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableToggle active={unreadTickets} onToggle={changeReadStatus} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: 'Показать выбор столбцов' }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
