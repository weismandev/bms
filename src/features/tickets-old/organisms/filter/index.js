import { memo } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  SelectField,
  InputField,
  CustomScrollbar,
} from '../../../../ui';
import { $isCompany } from '../../../common';
import { ComplexSelectField } from '../../../complex-select';
import { EmployeeSelectField } from '../../../employee-select';
import { EnterpriseSelectField } from '../../../enterprise-select';
import { HouseSelectField } from '../../../house-select';
import { TypeSelectField as RentTypeSelectField } from '../../../rent-objects';
import { StateSelectField } from '../../../state-select';
import { TypeSelectField } from '../../../type-select';
import { UrgencySelectField } from '../../../urgency-select';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';

const serviceSelectOptions = [
  { id: 'y', title: 'Сервисные' },
  { id: 'n', title: 'Не сервисные' },
  { id: 'all', title: 'Все' },
];

const debtSelectOptions = [
  { id: 'y', title: 'От должников' },
  { id: 'n', title: 'Не от должников' },
  { id: 'all', title: 'Все' },
];

const expiredOptions = [
  { id: 'y', title: 'Истекшие' },
  { id: 'n', title: 'Не истекшие' },
  { id: 'all', title: 'Все' },
];

const Filter = memo((props) => {
  const filters = useStore($filters);
  const isCompany = useStore($isCompany);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
        >
          <Form style={{ padding: 24, paddingTop: 0 }}>
            <Field
              name="applicant_name"
              component={InputField}
              label="ФИО заявителя"
              placeholder="Введите ФИО заявителя"
              divider
            />
            <Field
              name="address"
              component={HouseSelectField}
              label="Адрес"
              placeholder="Выберите адрес"
              divider
            />
            {!isCompany && (
              <Field
                name="complexes"
                component={ComplexSelectField}
                label="По комплексам"
                placeholder="Выберите комплекс"
                isMulti
                divider
              />
            )}
            <Field
              name="ticket_number"
              component={InputField}
              label="По номеру заявки"
              placeholder="Введите номер заявки"
              divider
            />
            <Field
              name="apartment_number"
              component={InputField}
              label="По номеру квартиры"
              placeholder="Введите номер квартиры"
              divider
            />
            <Field
              name="performer"
              component={EmployeeSelectField}
              label="Исполнители"
              placeholder="Выберите исполнителей"
              isMulti
              sorted
              divider
            />

            <Field
              name="state"
              component={StateSelectField}
              label="Статусы заявки"
              placeholder="Выберите статусы"
              isMulti
              divider
            />

            <Field
              name="urgency"
              component={UrgencySelectField}
              label="Срочности"
              placeholder="Выберите срочности"
              isMulti
              divider
            />

            <Field
              name="type"
              component={TypeSelectField}
              label="Типы заявки"
              placeholder="Выберите типы"
              isMulti
              divider
            />
            <Field
              name="property_types"
              component={RentTypeSelectField}
              label="Тип собственности"
              isMulti
            />
            <Field
              name="isService"
              label="По сервисному признаку"
              component={SelectField}
              options={serviceSelectOptions}
            />
            <Field
              name="isDebt"
              label="По задолженности заявителей"
              component={SelectField}
              options={debtSelectOptions}
            />
            <Field
              name="isExpired"
              label="По истечение срока выполнения"
              component={SelectField}
              options={expiredOptions}
            />
            <Field
              name="period_start"
              render={({ field, form }) => {
                return (
                  <DatePicker
                    customInput={
                      <InputField field={field} form={form} label="Начиная с" />
                    }
                    name={field.name}
                    selected={field.value}
                    onChange={(value) => form.setFieldValue(field.name, value)}
                    locale={dateFnsLocale}
                    dateFormat="dd.MM.yyyy"
                    popperPlacement="top"
                  />
                );
              }}
            />
            <Field
              name="period_end"
              render={({ field, form }) => {
                return (
                  <DatePicker
                    customInput={
                      <InputField field={field} form={form} label="Заканчивая" />
                    }
                    name={field.name}
                    selected={field.value}
                    onChange={(value) => form.setFieldValue(field.name, value)}
                    locale={dateFnsLocale}
                    dateFormat="dd.MM.yyyy"
                    popperPlacement="top"
                  />
                );
              }}
            />
            <Field
              name="enterprise"
              component={EnterpriseSelectField}
              label="Компания заявителя"
              placeholder="Выберите компанию"
              isMulti
            />
            <FilterFooter isResettable={true} />
          </Form>
        </Formik>
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
