import { createStore, createEvent, split } from 'effector';
import { createGate } from 'effector-react';
import format from 'date-fns/format';

const API_URL = process.env.API_URL;

const newEntityObject = () => ({
  id: null,
  title: '',
  text: '',
  isService: false,
  applicantFullname: '',
  applicantPhone: '',
  applicantEmail: '',
  applicantCompany: '',
  state: '',
  urgency: '',
  type: '',
  performer: '',
  date_start: format(new Date(), "yyyy-MM-dd'T'HH:mm"),
  date_end: '',
  rating: '',
  objectType: '',
  building: '',
  apartment: '',
  complex: '',
  cabinet: '',
  enterprise: '',
  property: '',
  rent: {
    start_date: '',
    finish_date: '',
    enterprise: '',
    attributes: [],
  },
  parent_ticket: '',
});

const DetailPageGate = createGate();

const detailSubmitted = createEvent();
const addClicked = createEvent();
const open = createEvent();
const changeMode = createEvent();
const visitedDetailPage = createEvent();
const notifyClicked = createEvent();
const exportClicked = createEvent();
const cloneTicketClicked = createEvent();
const parentTicketClicked = createEvent();

const entityApi = split(detailSubmitted, {
  save: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

const visitedApi = split(visitedDetailPage, {
  new: (id) => id === 'new',
  exist: (id) => id !== 'new',
});

const $mode = createStore('view');
const $opened = createStore(newEntityObject());
const $isDetailLoading = createStore(false);

export {
  addClicked,
  open,
  $opened,
  newEntityObject,
  changeMode,
  $mode,
  detailSubmitted,
  entityApi,
  visitedApi,
  visitedDetailPage,
  notifyClicked,
  DetailPageGate,
  exportClicked,
  $isDetailLoading,
  cloneTicketClicked,
  parentTicketClicked,
  API_URL,
};
