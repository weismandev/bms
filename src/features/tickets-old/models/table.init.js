import { attach, forward, guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { mergeColumnWidths } from '../../../tools/merge-column-widths';
import { signout, readNotifications, TICKET_UPDATED } from '../../common';
import { $filters, $filterParams } from './filter.model';
import {
  fxDownloadTicketsList,
  fxMarkTicketAsRead,
  fxMarkTicketAsUnread,
  fxGetFromStorage,
} from './page.model';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  exportClicked,
  changeReadStatus,
  $unreadTickets,
  $sorting,
  sortChanged,
} from './table.model';

$currentPage
  .on(pageNumberChanged, (state, number) => number)
  .on([pageSizeChanged, $filters], (state) => 0)
  .on(fxGetFromStorage.done, (state, { result }) => (result ? result.currentPage : state))
  .reset(signout);

$columnOrder
  .on(columnOrderChanged, (state, order) => order)
  .on(fxGetFromStorage.done, (state, { result }) =>
    result ? Array.from(new Set([...result.columnOrder, ...state])) : state
  )
  .reset(signout);

$columnWidths
  .on(columnWidthsChanged, (state, widths) => widths)
  .on(fxGetFromStorage.doneData, (state, data) =>
    mergeColumnWidths(state, data && data.columnWidths)
  )
  .reset(signout);

$hiddenColumnNames
  .on(hiddenColumnChanged, (state, names) => names)
  .on(fxGetFromStorage.done, (state, { result }) =>
    result ? result.hiddenColumnNames : state
  )
  .reset(signout);

$pageSize
  .on(pageSizeChanged, (state, qty) => qty)
  .on(fxGetFromStorage.done, (state, { result }) => (result ? result.pageSize : state))
  .reset(signout);

$sorting.on(sortChanged, (_, sort) => sort).reset(signout);

forward({
  from: exportClicked,
  to: attach({
    effect: fxDownloadTicketsList,
    source: $filterParams,
    mapParams: (params, filterParams) => ({
      ...params,
      ...filterParams,
    }),
  }),
});

const markedAsRead = guard({
  source: sample($unreadTickets, changeReadStatus, (unreadTickets, id) => {
    return unreadTickets.includes(id) && id;
  }),
  filter: Boolean,
});

forward({
  from: markedAsRead,
  to: fxMarkTicketAsRead,
});

forward({
  from: markedAsRead.map((id) => ({ id, event: TICKET_UPDATED })),
  to: readNotifications,
});

guard({
  source: sample($unreadTickets, changeReadStatus, (unreadTickets, id) => {
    return !unreadTickets.includes(id) && id;
  }),
  filter: Boolean,
  target: fxMarkTicketAsUnread,
});

fxDownloadTicketsList.done.watch(({ result }) => {
  const blob =
    ([result],
    {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `Tickets ${format(new Date(), 'dd-MM-yyyy HH-mm')}.xls`
  );
});
