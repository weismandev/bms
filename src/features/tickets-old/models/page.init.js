import { forward, attach, guard, sample, combine } from 'effector';
import { delay } from 'patronum/delay';
import firebase from 'firebase/app';
import { CheckCircle, Error } from '@mui/icons-material';
import { initializeFirebase } from '../../../configs/firebase';
import { hasPath } from '../../../tools/path';
import { changeNotification } from '../../colored-text-icon-notification/model';
import {
  receivedNotificationAbout,
  TICKET_UPDATED,
  history,
  $isCompany,
  signout,
  $isAuthenticated,
} from '../../common';
import { fxGetList as fxGetComplexes } from '../../complex-select';
import { fxGetList as fxGetBuildings } from '../../house-select';
import { ticketsApi } from '../api';
import { cloneTicketClicked, parentTicketClicked, $opened } from './detail.model';
import { $filterParams } from './filter.model';
import {
  pageMounted,
  $rawData,
  $normalizedData,
  fxGetFilteredList,
  fxGetTicketById,
  fxCreateTicket,
  fxUpdateTicket,
  fxDownloadTicketsList,
  receivedTicketUpdateNotification,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  requestOccured,
  errorOccured,
  changedErrorDialogVisibility,
  fxNotify,
  fxGetTicketData,
  fxMarkTicketAsRead,
  fxMarkTicketAsUnread,
  fxSaveInStorage,
  fxGetFromStorage,
  fxCreateTicketClone,
} from './page.model';
import { fxGetList as fxGetTypesList } from './select.model';
import {
  $tableParams,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $currentPage,
} from './table.model';

const $savedParams = combine({
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  hiddenColumnNames: $hiddenColumnNames,
});

fxGetFilteredList.use(ticketsApi.getFilteredList);
fxGetTicketById.use(ticketsApi.getTicketById);
fxCreateTicket.use(ticketsApi.createTicket);
fxUpdateTicket.use(ticketsApi.updateTicket);
fxDownloadTicketsList.use(ticketsApi.downloadTicketsList);
fxNotify.use(ticketsApi.notify);
fxGetTicketData.use(getTicketData);
fxMarkTicketAsRead.use(ticketsApi.markTicketAsRead);
fxMarkTicketAsUnread.use(ticketsApi.markTicketAsUnread);
fxCreateTicketClone.use(ticketsApi.createTicketClone);

$rawData
  .on(fxGetFilteredList.doneData, (state, tickets) => tickets)
  .on(fxGetTicketById.done, (state, { params: { id: updatedTicketId }, result }) => {
    const tickets = Array.isArray(state.tickets) ? [...state.tickets] : [];
    const updatedTicketIdx = tickets.findIndex(
      (bag) =>
        hasPath('ticket.id', bag) && String(bag.ticket.id) === String(updatedTicketId)
    );

    if (updatedTicketIdx > -1) {
      return {
        ...state,
        tickets: tickets
          .slice(0, updatedTicketIdx)
          .concat(result)
          .concat(tickets.slice(updatedTicketIdx + 1)),
      };
    }
    return {
      ...state,
      meta: { ...state.meta, total: state.meta.total + 1 },
      tickets: [result, ...state.tickets],
    };
  })
  .on(
    [fxMarkTicketAsRead.done, fxMarkTicketAsUnread.done],
    (state, { params: updatedTicketId, result: { read } }) => {
      const tickets = Array.isArray(state.tickets) ? [...state.tickets] : [];

      const updatedTicketIdx = tickets.findIndex(
        (bag) =>
          hasPath('ticket.id', bag) && String(bag.ticket.id) === String(updatedTicketId)
      );

      if (updatedTicketIdx > -1) {
        return {
          ...state,
          tickets: tickets
            .slice(0, updatedTicketIdx)
            .concat({
              ...tickets[updatedTicketIdx],
              ticket: { ...tickets[updatedTicketIdx].ticket, is_read: read },
            })
            .concat(tickets.slice(updatedTicketIdx + 1)),
        };
      }
      return state;
    }
  )
  .reset(signout);

const requestStarted = guard({ source: requestOccured, filter: Boolean });
// const delayedRedirect = delay({ source: fxGetTicketData.fail, timeout: 3000 });

$isLoading.on(requestOccured, (state, pending) => pending).reset(signout);

$error
  .on(errorOccured, (state, { error }) => error)
  .on(requestStarted, () => null)
  .reset(signout);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (state, visibility) => visibility)
  .on(errorOccured, () => true)
  .on(requestStarted, () => false)
  .reset(signout);

sample({
  source: $opened,
  clock: cloneTicketClicked,
  fn: (ticket) => ticket.id,
  target: fxCreateTicketClone,
});

forward({
  from: fxCreateTicketClone.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: 'Заявка клонирована!',
    Icon: CheckCircle,
  })),
});

forward({
  from: fxCreateTicketClone.fail,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#eb5757',
    text: 'Не удалось клонировать заявку',
    Icon: Error,
  })),
});

parentTicketClicked.watch((ticketId) => history.push(`/tickets/${ticketId}`));

forward({
  from: [$filterParams, $tableParams, pageMounted],
  to: attach({
    effect: fxGetFilteredList,
    source: [$tableParams, $filterParams, $isCompany],
    mapParams: (params, [tableParams, filterParams, isCompany]) => {
      const complexes =
        filterParams.complexes?.length > 0
          ? filterParams.complexes.map(({ id }) => id)
          : [];
      const payload = {
        ...params,
        ...tableParams,
        ...filterParams,
        complexes,
      };

      if (isCompany) payload.search_for_business = 1;

      return payload;
    },
  }),
});

forward({
  from: receivedNotificationAbout[TICKET_UPDATED],
  to: fxGetTicketById.prepend(({ data }) => ({ id: data.ticket_id })),
});

guard({
  source: sample(
    $normalizedData,
    receivedTicketUpdateNotification,
    (normalizedData = {}, id) => ({
      normalizedData,
      id,
    })
  ),
  filter: ({ normalizedData, id }) => {
    const entities = (normalizedData && normalizedData.entities) || {};
    return (
      entities.tickets &&
      Object.keys(entities.tickets).find((ticketId) => String(ticketId) === String(id))
    );
  },
  target: attach({
    effect: fxGetTicketById,
    mapParams: ({ id }) => ({ id }),
  }),
});

guard({
  source: $savedParams,
  filter: $isAuthenticated,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (params) => ({ data: params, storage_key: 'tickets' }),
  }),
});

forward({
  from: pageMounted,
  to: fxGetFromStorage.prepend(() => ({ storage_key: 'tickets' })),
});

try {
  initializeFirebase();
  const messaging = firebase.messaging();

  messaging.onMessage((push) => {
    const event =
      push.data && push.data['gcm.notification.event']
        ? push.data['gcm.notification.event']
        : '';

    const regexp = /ticket-update/i;
    const isTicketUpdateEvent = regexp.test(event);

    if (isTicketUpdateEvent) {
      const ticketId = push.data && push.data['gcm.notification.ticket_id'];
      receivedTicketUpdateNotification(ticketId);
    }
  });
} catch (error) {
  console.log('Firebase base error');
}

// delayedRedirect.watch(() => {
//   history.push('/tickets');
// });

async function getTicketData(payload) {
  await Promise.all([fxGetTypesList(), fxGetBuildings(), fxGetComplexes()]);

  let ticket = await ticketsApi.getTicketById(payload);

  if (ticket.apartment && ticket.apartment.id) {
    const response = await ticketsApi.getRelatedBuilding(ticket.apartment.id);

    const apartment =
      Array.isArray(response.apartments) && response.apartments.length > 0
        ? response.apartments[0]
        : null;
    ticket = { ...ticket, building: apartment && apartment.building };
  }

  return ticket;
}
