import { signout } from '../../common';
import {
  $filterParams,
  $filters,
  $isFilterOpen,
  changedFilterVisibility,
  defaultFilters,
  filtersSubmitted,
} from './filter.model';

$isFilterOpen
  .on(changedFilterVisibility, (state, visibility) => visibility)
  .reset(signout);

$filters
  .on(filtersSubmitted, (state, filters) => (filters ? filters : defaultFilters))
  .reset(signout);

$filterParams.reset(signout);
