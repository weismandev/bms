import { createStore, createEvent, combine } from 'effector';
import format from 'date-fns/format';
import isDate from 'date-fns/isDate';
import parseJSON from 'date-fns/parseJSON';
import { $isCompany } from '../../common';
import { $normalizedData, $rawData } from './page.model';

const pageNumberChanged = createEvent();
const pageSizeChanged = createEvent();
const columnWidthsChanged = createEvent();
const columnOrderChanged = createEvent();
const hiddenColumnChanged = createEvent();
const exportClicked = createEvent();
const changeReadStatus = createEvent();
const sortChanged = createEvent();

const $currentPage = createStore(0);
const $pageSize = createStore(10);
const $sorting = createStore([]);

const $columns = combine($isCompany, (isCompany) => {
  const excludeColumnsForCompanyAccount = ['debt', 'urgency', 'competence'];

  let columns = [
    { name: 'number', title: '№' },
    { name: 'title', title: 'Название' },
    { name: 'date_start', title: 'Дата создания' },
    { name: 'updated_at', title: 'Последняя активность' },
    {
      name: 'date_end',
      title: 'Срок выполнения',
    },
    { name: 'state', title: 'Статус' },
    { name: 'urgency', title: 'Срочность' },
    { name: 'address', title: 'Адрес' },
    { name: 'applicant', title: 'Заявитель' },
    { name: 'phone', title: 'Телефон заявителя' },
    { name: 'enterprise', title: 'Компания заявителя' },
    { name: 'debt', title: 'Задолженность' },
    { name: 'competence', title: 'Тип' },
    { name: 'performer', title: 'Исполнитель' },
    { name: 'comment', title: 'Комментарий' },
    { name: 'rating', title: 'Оценка' },
  ];

  if (isCompany) {
    columns = columns.filter((i) => !excludeColumnsForCompanyAccount.includes(i.name));
  }

  return columns;
});

const $columnWidths = createStore([
  { columnName: 'number', width: 100 },
  { columnName: 'title', width: 400 },
  { columnName: 'state', width: 250 },
  { columnName: 'urgency', width: 150 },
  { columnName: 'date_start', width: 200 },
  { columnName: 'updated_at', width: 300 },
  { columnName: 'date_end', width: 200 },
  { columnName: 'address', width: 350 },
  { columnName: 'applicant', width: 300 },
  { columnName: 'phone', width: 200 },
  { columnName: 'enterprise', width: 250 },
  { columnName: 'debt', width: 200 },
  { columnName: 'comment', width: 200 },
  { columnName: 'rating', width: 200 },
  { columnName: 'competence', width: 300 },
  { columnName: 'performer', width: 300 },
]);

const $columnOrder = createStore([
  'number',
  'title',
  'state',
  'urgency',
  'date_start',
  'updated_at',
  'date_end',
  'address',
  'applicant',
  'phone',
  'enterprise',
  'debt',
  'competence',
  'performer',
  'rating',
  'comment',
]);

const excludedColumnsFromSort = [
  'number',
  'title',
  'state',
  'urgency',
  'date_end',
  'address',
  'applicant',
  'phone',
  'enterprise',
  'debt',
  'competence',
  'performer',
  'rating',
  'comment',
];

const $hiddenColumnNames = createStore([]);

const $tableParams = combine(
  $currentPage,
  $pageSize,
  $sorting,
  (page, per_page, sorting) => {
    const [sort] = sorting;
    const sortingParams = {
      sort: {
        order: sort && sort.direction,
        column: sort && sort.columnName,
      },
    };

    return {
      page: page + 1,
      per_page,
      ...(sort && sortingParams),
    };
  }
);

const $tableData = $normalizedData.map(({ entities, result }) => {
  const {
    ticketsBags,
    tickets,
    states,
    urgencies,
    apartments,
    applicants,
    performers,
    competences,
    enterprises,
  } = entities;

  const tableData = result.map((ticket_id) => {
    const bag = ticketsBags[ticket_id];
    const ticket = tickets[ticket_id];

    return {
      id: ticket.id,
      number: ticket.number || ticket.id,
      title: ticket.title,
      date_start: isDate(ticket.date_start)
        ? format(ticket.date_start, 'dd.LL.yyyy HH:mm')
        : '-',
      updated_at:
        ticket.updated_at &&
        ticket.updated_at.date &&
        isDate(parseJSON(ticket.updated_at.date))
          ? format(parseJSON(ticket.updated_at.date), 'dd.LL.yyyy HH:mm')
          : '-',
      date_end: isDate(ticket.date_end)
        ? format(ticket.date_end, 'dd.LL.yyyy HH:mm')
        : '-',
      state: states[ticket.state]
        ? states[ticket.state]
        : { id: -1, slug: 'undefined', title: '-' },
      urgency: urgencies[ticket.urgency] ? urgencies[ticket.urgency].title : '-',
      address:
        typeof apartments === 'object' &&
        apartments !== null &&
        apartments[bag.apartment] &&
        apartments[bag.apartment].address
          ? apartments[bag.apartment].address
          : bag.address || '-',
      applicant:
        applicants[bag.applicant] &&
        (applicants[bag.applicant].fullname || applicants[bag.applicant].email)
          ? applicants[bag.applicant].fullname || applicants[bag.applicant].email
          : '-',
      phone:
        applicants[bag.applicant] && applicants[bag.applicant].phone
          ? applicants[bag.applicant].phone
          : '-',
      debt: bag.has_debts,
      competence:
        competences[ticket.competence] && competences[ticket.competence].title
          ? competences[ticket.competence].title
          : '-',
      comment:
        ticket.rates[0] && ticket.rates[0].comment ? ticket.rates[0].comment : null,
      rating: ticket.rating,
      performer:
        performers[bag.performer] && performers[bag.performer].fullname
          ? performers[bag.performer].fullname
          : '-',
      enterprise:
        applicants[bag.applicant] && enterprises[applicants[bag.applicant].enterprise]
          ? enterprises[applicants[bag.applicant].enterprise].title
          : '-',
      is_read: ticket.is_read || false,
    };
  });

  return tableData;
});

const $unreadTickets = $rawData.map(({ tickets = [] }) => {
  return tickets.filter((i) => !i.ticket.is_read).map((i) => i.ticket.id);
});

const $rowCount = $rawData.map(({ meta }) => (meta && meta.total) || 0);

export {
  $currentPage,
  $pageSize,
  $rowCount,
  $tableData,
  $tableParams,
  pageNumberChanged as pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  exportClicked,
  $unreadTickets,
  changeReadStatus,
  $columns,
  excludedColumnsFromSort,
  sortChanged,
  $sorting,
};
