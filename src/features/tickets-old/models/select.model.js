import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { ticketsApi } from '../api';

const defaultObjectTypes = [
  { id: 's1', title: 'УК', slug: 'bms' },
  { id: 's2', title: 'ЖК', slug: 'complex' },
  { id: 's3', title: 'Дом', slug: 'building' },
  { id: 's4', title: 'Квартира', slug: 'apartment' },
];

const disabledOptions = ['bms', 'property_type', 'property', 'property_service'];

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.data) {
      return defaultObjectTypes;
    }

    let receivedTypes = result.data.map((type) => ({
      id: type.id,
      title: `Аренда типа ${typeof type.title === 'string' && type.title.toLowerCase()}`,
      slug: 'property_type',
    }));

    const isRentAccount = receivedTypes.length > 0;

    if (isRentAccount) {
      receivedTypes = receivedTypes.concat([
        { id: 's5', title: 'Аренда объекта', slug: 'property' },
        {
          id: 's6',
          title: 'Сервисная заявка по объекту аренды',
          slug: 'property_service',
        },
      ]);
    }

    return defaultObjectTypes.concat(receivedTypes);
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);

$isLoading.on(fxGetList.pending, (state, isLoading) => isLoading).reset(signout);

fxGetList.use(ticketsApi.getRentTypes);

export { fxGetList, $data, $error, $isLoading, disabledOptions };
