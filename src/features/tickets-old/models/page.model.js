import { createEffect, createStore, createEvent, merge, attach, sample } from 'effector';
import { createGate } from 'effector-react';
import { schema, normalize } from 'normalizr';
import { formatPhone } from '../../../tools/formatPhone';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
  $isCompany,
} from '../../common';

const object = new schema.Entity('objects');
const type = new schema.Entity('types');
const competence = new schema.Entity('competences');
const urgency = new schema.Entity('urgencies');
const state = new schema.Entity('states');

const ticket = new schema.Entity(
  'tickets',
  {
    competence,
    object,
    state,
    type,
    urgency,
  },
  {
    processStrategy: (
      { date_start, date_end, date_finish, rating, ...rest },
      parent,
      key
    ) => {
      const isValidDate = (date) => Boolean(date) && new Date(date * 1000) > 1000;
      let tempRating = null;
      if (rest.state.slug === 'done' || rest.state.slug === 'closed') {
        tempRating = Boolean(rating) || rating === 0 ? Number(rating) : null;
      }
      return {
        date_start: isValidDate(date_start) ? new Date(date_start * 1000) : '',
        date_end: isValidDate(date_end) ? new Date(date_end * 1000) : '',
        date_finish: isValidDate(date_finish) ? new Date(date_finish * 1000) : '',
        rating: tempRating,
        ...rest,
      };
    },
  }
);

const chat = new schema.Entity('chats');

const enterprise = new schema.Entity(
  'enterprises',
  {},
  {
    idAttribute: (value, parent, key) => (value.id ? value.id : 'unknown'),
    processStrategy: ({ id, title }, parent, key) => {
      return {
        id: id || null,
        title: typeof title === 'string' ? title : '-',
      };
    },
  }
);

const applicant = new schema.Entity(
  'applicants',
  { enterprise },
  {
    processStrategy: ({ phone, ...rest }, parent, key) => ({
      phone: phone ? formatPhone(String(phone)) : '',
      ...rest,
    }),
  }
);

const apartment = new schema.Entity(
  'apartments',
  {},
  {
    idAttribute: (value, parent, key) => (value.id ? value.id : 'unknown'),
    processStrategy: ({ id, title, city, address, entrance, flat }, parent, key) => {
      return {
        id: id || null,
        title: typeof title === 'string' && title.length > 3 ? title : '',
        city: city || '',
        address: address || '',
        entrance: entrance || 0,
        flat: flat || 0,
      };
    },
  }
);

const performer = new schema.Entity(
  'performers',
  {},
  {
    idAttribute: (value, parent, key) => (value.id ? value.id : 'unknown'),
    processStrategy: ({ id, phone, email, fullname }, parent, key) => {
      return {
        id: id || null,
        phone: phone ? formatPhone(String(phone)) : '',
        email: email || '',
        fullname: fullname || '',
      };
    },
  }
);

const ticketBag = new schema.Entity(
  'ticketsBags',
  {
    apartment,
    applicant,
    chat,
    performer,
    ticket,
  },
  {
    idAttribute: (value, parent, key) => value.ticket.id,
    processStrategy: ({ city, ...rest }, parent, key) => ({
      city: city || '',
      ...rest,
    }),
  }
);
const arrayOfTicketBag = new schema.Array(ticketBag);

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const changedErrorDialogVisibility = createEvent();
const receivedTicketUpdateNotification = createEvent();

const fxGetFilteredList = createEffect();
const fxGetTicketById = createEffect();
const fxCreateTicket = createEffect();
const fxUpdateTicket = createEffect();
const fxDownloadTicketsList = createEffect();
const fxNotify = createEffect();
const fxGetTicketData = createEffect();
const fxMarkTicketAsRead = createEffect();
const fxMarkTicketAsUnread = createEffect();
const fxCreateTicketClone = createEffect();

const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const $rawData = createStore({});

const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);
const $sectionPath = sample({
  source: $isCompany,
  fn: (isCompany) => (isCompany ? 'enterprises-tickets' : 'tickets'),
});

const $normalizedData = $rawData.map((data) => {
  const tickets = data.tickets || [];
  return normalize(tickets, arrayOfTicketBag);
});

const requestOccured = merge([
  fxGetFilteredList.pending,
  fxGetTicketById.pending,
  fxCreateTicket.pending,
  fxUpdateTicket.pending,
  fxDownloadTicketsList.pending,
  fxNotify.pending,
  fxGetTicketData.pending,
  fxMarkTicketAsRead.pending,
  fxMarkTicketAsUnread.pending,
  fxCreateTicketClone.pending,
]);

const errorOccured = merge([
  fxGetFilteredList.fail,
  fxGetTicketById.fail,
  fxCreateTicket.fail,
  fxUpdateTicket.fail,
  fxDownloadTicketsList.fail,
  fxNotify.fail,
  fxGetTicketData.fail,
  fxMarkTicketAsRead.fail,
  fxMarkTicketAsUnread.fail,
]);

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  $rawData,
  $normalizedData,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $sectionPath,
  requestOccured,
  errorOccured,
  fxGetFilteredList,
  fxGetTicketById,
  fxCreateTicket,
  fxUpdateTicket,
  fxDownloadTicketsList,
  receivedTicketUpdateNotification,
  fxNotify,
  fxGetTicketData,
  fxMarkTicketAsRead,
  fxMarkTicketAsUnread,
  fxSaveInStorage,
  fxGetFromStorage,
  fxCreateTicketClone,
};
