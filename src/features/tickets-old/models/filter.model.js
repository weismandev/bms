import { createEvent, createStore } from 'effector';
import add from 'date-fns/add';
import format from 'date-fns/format';

const defaultThreePositionSelectValue = { id: 'all', title: 'Все' };

const formatString = 'yyyy-MM-dd HH:mm:ss';

const defaultFilters = {
  applicant_name: '',
  address: '',
  complexes: [],
  ticket_number: '',
  apartment_number: '',
  performer: [],
  urgency: [],
  state: [],
  type: [],
  property_types: [],
  isService: defaultThreePositionSelectValue,
  isDebt: defaultThreePositionSelectValue,
  isExpired: defaultThreePositionSelectValue,
  period_start: null,
  period_end: null,
  enterprise: '',
};

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = createStore(false);
const $filters = createStore(defaultFilters);

const $filterParams = $filters.map(
  ({
    applicant_name,
    address,
    complexes,
    ticket_number,
    apartment_number,
    performer,
    urgency,
    state,
    type,
    property_types,
    isDebt,
    isService,
    isExpired,
    enterprise,
    period_start,
    period_end,
  }) => {
    const filters = {
      applicant_name,
      ticket_number,
      apartment_number,
      complexes,
      address: { building_id: address ? address.id : '' },
      performer: performer.map((user) => user.id),
      urgency: urgency.map((i) => i.slug),
      state: state.map((i) => i.slug),
      competence: type.map((i) => i.section_id),
      property_types: property_types.map((i) => i.id),
    };

    if (isExpired && isExpired.id !== 'all') {
      filters.is_expired = isExpired.id;
    }

    if (isDebt && isDebt.id !== 'all') {
      filters.is_debtor = isDebt.id;
    }

    if (isService && isService.id !== 'all') {
      filters.is_service = isService.id;
    }

    if (Array.isArray(enterprise)) {
      filters.enterprise = enterprise.map((i) => i.id);
    }

    if (period_start) {
      if (!period_end) {
        filters.period_end = format(add(Date.now(), { days: 1 }), formatString);
      }
      filters.period_start = format(new Date(period_start), formatString);
    }

    if (period_end) {
      if (!period_start) {
        filters.period_start = format(new Date(1970, 0, 1), formatString);
      }
      filters.period_end = format(add(new Date(period_end), { days: 1 }), formatString);
    }

    return filters;
  }
);

export {
  changedFilterVisibility,
  $isFilterOpen,
  filtersSubmitted,
  $filters,
  defaultFilters,
  $filterParams,
};
