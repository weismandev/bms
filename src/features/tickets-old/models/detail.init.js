import { merge, forward, sample, attach } from 'effector';
import format from 'date-fns/format';
import set from 'date-fns/set';
import {
  history,
  readNotifications,
  $token,
  signout,
  $isCompany,
  TICKET_UPDATED,
} from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { getPathOr, hasPath } from '@tools/path';
import { $data as $complexes } from '../../complex-select';
import { $data as $buildings } from '../../house-select';
import {
  $isDetailLoading,
  $mode,
  $opened,
  addClicked,
  changeMode,
  entityApi,
  exportClicked,
  newEntityObject,
  notifyClicked,
  open,
  parentTicketClicked,
  visitedApi,
  visitedDetailPage,
  API_URL,
} from './detail.model';
import {
  fxCreateTicket,
  fxUpdateTicket,
  fxGetTicketData,
  fxNotify,
  fxCreateTicketClone,
  $sectionPath,
} from './page.model';
import { $data as $objectTypes } from './select.model';

$opened
  .on(merge([addClicked, visitedApi.new, parentTicketClicked]), () => newEntityObject())
  .on(
    sample({
      source: {
        types: $objectTypes,
        buildings: $buildings,
        complexes: $complexes,
      },
      clock: [fxGetTicketData.done, fxUpdateTicket.doneData],
      fn: ({ types, buildings, complexes }, data) => ({
        types,
        buildings,
        complexes,
        result: data.result || data.updated,
      }),
    }),
    (state, { result, types = [], buildings = [], complexes = [] }) => {
      const { ticket } = result;
      const objectType = types.find((type) =>
        ticket.object.type === 'property_type'
          ? type.id === ticket.object.id
          : type.slug === ticket.object.type
      );
      const formValues = {
        id: ticket.id,
        chatId: result.chat.id,
        title: ticket.title,
        number: ticket.number,
        text: ticket.text,
        isService: ticket.is_service,
        parent_ticket: ticket.parent_ticket || '',
        applicantFullname: getPathOr('applicant.fullname', result, 'Не определено'),
        enterprise: '',
        property: getPathOr('object.id', ticket, ''),
        applicantPhone: formatPhone(String(getPathOr('applicant.phone', result, ''))),
        applicantEmail: getPathOr('applicant.email', result, ''),
        applicantCompany:
          hasPath('applicant.enterprise.id', result) &&
          Boolean(result.applicant.enterprise.id)
            ? result.applicant.enterprise.id
            : { id: 'NOT_DEFINED', title: '-' },
        state: ticket.state,
        urgency: ticket.urgency,
        type: ticket.competence.id
          ? {
              section_id: String(ticket.competence.id),
              section_title: ticket.competence.title,
            }
          : '',
        performer: result.performer.id ? { user: result.performer } : '',
        date_start: format(new Date(ticket.date_start * 1000), "yyyy-MM-dd'T'HH:mm"),
        date_end: format(
          ticket.date_end ? new Date(ticket.date_end * 1000) : new Date(),
          'yyyy-MM-dd'
        ),
        rating: ticket.rating ? ticket.rating : '',
        objectType: objectType || '',
        apartment: result.apartment ? result.apartment : '',
        building: result.building
          ? result.building
          : ticket.object.type === 'building'
          ? buildings.find((build) => String(build.id) === String(ticket.object.id))
          : '',
        complex:
          ticket.object.type === 'complex'
            ? complexes.find((complex) => String(complex.id) === String(ticket.object.id))
            : '',
        rent: {
          object:
            ticket.object.type === 'property_service' || ticket.object.type === 'property'
              ? result.apartment
              : '',
          start_date: result.rent.form.start_date
            ? format(new Date(result.rent.form.start_date * 1000), 'yyyy-MM-dd')
            : '',
          finish_date: result.rent.form.finish_date
            ? format(new Date(result.rent.form.finish_date * 1000), 'yyyy-MM-dd')
            : '',
          attributes: Array.isArray(result.rent.form.desired_attributes)
            ? result.rent.form.desired_attributes.map((i) => ({
                ...i,
                value: i.value === 'true' ? 'Да' : i.value === 'false' ? 'Нет' : i.value,
              }))
            : [],
          enterprise: result.rent.form.enterprise_id || '',
          properties: result.rent.rents,
          cabinet: ticket.cabinet || '',
        },
      };

      return formValues;
    }
  )
  .reset(signout);

$mode
  .on(changeMode, (state, mode) => mode)
  .on(merge([addClicked, visitedApi.new]), () => 'edit')
  .on(merge([visitedApi.exist, fxCreateTicket.done, fxUpdateTicket.done]), () => 'view')
  .reset(signout);

$isDetailLoading.reset(signout);

sample({
  source: [
    fxGetTicketData.pending,
    fxCreateTicket.pending,
    fxUpdateTicket.pending,
    fxNotify.pending,
    fxCreateTicketClone.pending,
  ],
  fn: (states) => states.some(Boolean),
  target: $isDetailLoading,
});

forward({
  from: visitedApi.exist.map((id) => ({ id, mark_read: 1 })),
  to: fxGetTicketData,
});

forward({
  from: entityApi.save,
  to: attach({
    source: $isCompany,
    effect: fxCreateTicket,
    mapParams: preparePayload,
  }),
});

forward({
  from: entityApi.update,
  to: attach({
    source: $isCompany,
    effect: fxUpdateTicket,
    mapParams: preparePayload,
  }),
});

forward({
  from: fxCreateTicket.doneData,
  to: open.prepend(({ created }) => created.ticket.id),
});

forward({
  from: sample($opened, notifyClicked, (opened) => opened.id),
  to: fxNotify,
});

sample({
  source: $sectionPath,
  clock: [addClicked, open],
  fn: (path, id) =>
    typeof id === 'number'
      ? history.push(`/${path}/${id}`)
      : history.push(`/${path}/new`),
});

forward({
  from: visitedApi.exist.map((id) => ({ id, event: TICKET_UPDATED })),
  to: readNotifications,
});

const handleHistoryStep = (location) => {
  const match =
    typeof location.pathname === 'string' &&
    location.pathname.match(/(?<page>tickets)\/(?<id>.*)/);

  if (match) {
    visitedDetailPage(match.groups.id);
  } else {
    visitedDetailPage('new');
  }
};

handleHistoryStep(history.location);
history.listen(handleHistoryStep);

function preparePayload(payload, isCompany) {
  const {
    id,
    title,
    text,
    isService,
    state,
    urgency,
    type,
    performer,
    date_start,
    date_end,
    objectType,
    apartment,
    building,
    complex,
    enterprise,
    property,
  } = payload;

  const object_type = isCompany ? 'property_service' : objectType ? objectType.slug : '';

  const setDateEnd = (date_end) =>
    set(new Date(date_end), { hours: 23, minutes: 59 }) / 1000;

  const getObjectId = (object_type) => {
    let objectId = '';

    if (object_type === 'apartment') {
      objectId = apartment ? apartment.id : '';
    } else if (object_type === 'building') {
      objectId = building ? building.id : '';
    } else if (object_type === 'complex') {
      objectId = complex ? complex.id : '';
    } else if (object_type === 'property_service') {
      objectId =
        Boolean(property) && typeof property === 'object' ? property.id : property;
    }

    return objectId;
  };

  const performer_id =
    typeof performer?.user === 'object' ? performer.user?.id : performer?.id;

  return {
    id,
    title,
    text,
    section_id: type ? type.section_id : '',
    state_id: state ? state.id : '',
    urgency_id: urgency ? urgency.id : '',
    performer_id,
    date_start: date_start ? new Date(date_start) / 1000 : '',
    date_end: date_end ? setDateEnd(date_end) : '',
    is_service: isService,
    object: { type: object_type, id: getObjectId(object_type) },
    enterprise_id: enterprise ? enterprise.id : '',
  };
}

exportClicked.watch(() => {
  const opened = $opened.getState();
  const token = $token.getState();

  window.open(`${API_URL}/v1/tickets/export/?token=${token}&id=${opened.id}`);
});
