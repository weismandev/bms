import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'enterprises/sms-pass-mode-list');

export const smsApi = { getList };
