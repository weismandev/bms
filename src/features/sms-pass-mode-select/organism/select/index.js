import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const SmsPassModeSelect = (props) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={data} isLoading={isLoading} error={error} {...props} />;
};

const SmsPassModeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<SmsPassModeSelect />} onChange={onChange} {...props} />;
};

export { SmsPassModeSelect, SmsPassModeSelectField };
