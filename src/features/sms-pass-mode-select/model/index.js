import { createStore, createEffect } from 'effector';
import { signout } from '../../common';
import { smsApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (Array.isArray(result) && result.length > 0) {
      return result.map((e) => ({ ...e, value: e.slug }));
    }

    return state;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(smsApi.getList);

export { fxGetList, $data, $error, $isLoading };
