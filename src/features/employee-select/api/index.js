import { api } from '../../../api/api2';

const getEmployeesList = (payload = {}) =>
  api.v1('get', 'employee/get-simple-list', {
    ...payload,
    per_page: 999999,
  });

export const employeeApi = {
  getEmployeesList,
};
