import { useEffect } from 'react';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { SelectControl, SelectField } from '../../../../ui';
import { fxGetList, $employees, $employeeError, $isEmployeeLoading } from '../../model';

const { t } = i18n;

const sortByFullname = (data) => {
  return [...data].sort((a, b) => {
    const aName = a.user?.fullname.toLowerCase();
    const bName = b.user?.fullname.toLowerCase();
    return aName < bName ? -1 : aName > bName ? 1 : 0;
  });
};

const getOptionLabel = (option) => {
  const fullname = option.fullname || option.user?.fullname || '';
  const archiveLabel = option.is_archived ? `(${t('movedToArchive')})` : '';
  return `${fullname} ${archiveLabel}`;
};

const EmployeeSelect = ({ sorted, ...props }) => {
  const isLoading = useStore($isEmployeeLoading);
  const error = useStore($employeeError);
  const employees = useStore($employees);

  const { buildings = [] } = props;

  useEffect(() => {
    fxGetList({ buildings });
  }, [buildings[0]]);

  return (
    <SelectControl
      options={sorted ? sortByFullname(employees) : employees}
      isLoading={isLoading}
      getOptionLabel={getOptionLabel}
      getOptionValue={(option) => option.id || option.user?.id}
      isOptionDisabled={(option) => option.is_archived}
      error={error && error.message}
      placeholder={t('ChooseEmployee')}
      label={null}
      {...props}
    />
  );
};

const EmployeeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);
  const { sorted = false } = props;
  return (
    <SelectField
      component={<EmployeeSelect sorted={sorted} />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EmployeeSelect, EmployeeSelectField };
