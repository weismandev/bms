import { createStore, createEffect } from 'effector';
import { signout } from '../../common';
import { employeeApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(employeeApi.getEmployeesList);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result?.list)) {
      return [];
    }

    const archivedEmployees = result.list.filter((item) => item.is_archived);
    const notArchivedEmployees = result.list.filter((item) => !item.is_archived);

    return [...notArchivedEmployees, ...archivedEmployees];
  })
  .reset(signout);
$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, pending) => pending).reset(signout);

export {
  fxGetList,
  $data as $employees,
  $error as $employeeError,
  $isLoading as $isEmployeeLoading,
};
