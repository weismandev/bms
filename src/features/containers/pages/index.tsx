import { FC, useState } from 'react';
import { useStore } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import PageContext, { PageContextType } from '@features/fabrics/app/pageContext';
import { Detail } from '@features/fabrics/devices/detail';
import { $isOpenDetail } from '@features/fabrics/devices/detail/model/detail';
import { Filter } from '@features/fabrics/filter';
import { InstructionGate } from '@features/fabrics/processes/instructions/models/instructions';
import { Table } from '@features/fabrics/table';
import { FilterMainDetailLayout } from '@ui/index';

const ContainersPage: FC = () => {
  const pageName = 'garbage';

  const [isFilterOpen, visibleFilter] = useState(false);

  const isOpenDetail = useStore($isOpenDetail);

  const providerValues: PageContextType = {
    page: pageName,
    filter: {
      isFilterOpen,
      visibleFilter,
    },
  };

  return (
    <PageContext.Provider value={providerValues}>
      <InstructionGate page={pageName} with_data={1} />

      <ColoredTextIconNotification />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isOpenDetail && <Detail />}
        params={{ detailWidth: '600px' }}
      />
    </PageContext.Provider>
  );
};

const RestrictedContainersTboPage: FC = (props) => (
  <HaveSectionAccess>
    <ContainersPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedContainersTboPage as ContainersPage };
