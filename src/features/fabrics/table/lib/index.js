import { format } from 'date-fns';

function filterByValue(fields) {
  return function (name) {
    const field = fields[name];
    if (!field) return false;
    if (Array.isArray(field) && field.length === 0) return false;
    return true;
  };
}

export const formatPayload = ({ tableParams, $filterFields, gate }) => {
  const { page, per_page, sorting, search } = tableParams;

  const filterState = $filterFields.getState() || {};
  const filterValues = Object.keys(filterState).reduce(
    (acc, key) => ({ ...acc, [key]: filterState[key].value }),
    {}
  );

  const filters = Object.keys(filterValues)
    .filter(filterByValue(filterValues))
    .reduce(
      (acc, key) => ({
        ...acc,
        [key]: Array.isArray(filterValues[key])
          ? filterValues[key].map(({ id }) => id).join()
          : typeof Number(filterValues[key].id) === 'number' &&
            !isNaN(filterValues[key].id)
          ? filterValues[key].id
          : filterValues[key],
      }),
      {}
    );

  const sort =
    Array.isArray(sorting) && sorting.length > 0
      ? {
          sort_by: sorting[0]?.name || '',
          sort_value: sorting[0]?.order || '',
        }
      : {};

  const offset = per_page * (page - 1);

  return {
    ...filters,
    ...sort,
    limit: per_page,
    search,
    page: gate.page,
    offset: search?.length > 0 ? 0 : offset,
  };
};

export const formatData = (data = [], meta = []) => {
  if (data.length === 0 || meta.length === []) {
    return [];
  }

  const getDate = (seconds) =>
    seconds === 0 ? '-' : format(new Date(seconds * 1000), 'dd.MM.yyyy HH:mm');

  return data.map(({ line, link }) => {
    const formattedLine = line.reduce((acc, item, index) => {
      if (typeof item === 'object') {
        return {
          ...acc,
          [meta[index].name]: item.title,
          meta: { [meta[index].name]: line[index] },
          link,
        };
      }

      if (meta[index]?.type === 'date') {
        return {
          ...acc,
          [meta[index].name]: getDate(item),
          link,
        };
      }

      return {
        ...acc,
        [meta[index].name]: item,
        link,
      };
    }, {});

    // Хак при отсутствии id у строки
    // В качестве id присваем серийный номер или ссылку
    if (!Object.prototype.hasOwnProperty.call(formattedLine, 'id')) {
      formattedLine.id = formattedLine.serialnumber
        ? formattedLine.serialnumber
        : formattedLine.link;
    }

    return formattedLine;
  });
};

export const formatColumns = (columns = []) => {
  if (columns.length === 0) {
    return [];
  }

  return Object.values(columns).reduce((acc, value) => {
    if (typeof value !== 'object' || value.hide) {
      return acc;
    }

    return [...acc, { ...value }];
  }, []);
};

export const formatColumnsWidth = (columns = []) => {
  if (columns.length === 0) {
    return [];
  }

  return columns.map((item) => ({
    columnName: item.name,
    width: 300,
  }));
};
