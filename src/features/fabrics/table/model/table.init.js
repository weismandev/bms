import { sample, merge } from 'effector';
import { signout } from '@features/common';
import {
  filterForm,
  filtersSubmitted,
} from '@features/fabrics/filter/model/filter.model';
import {
  formatPayload,
  formatData,
  formatColumns,
  formatColumnsWidth,
} from '@features/fabrics/table/lib';
import instructionObserver from '../../processes/instructionObserver';
import {
  InstructionGate,
  instructionUnmounted,
} from '../../processes/instructions/models/instructions';
import { tableApi } from '../api';
import {
  fxGetData,
  $isLoading,
  $error,
  $instructions,
  $tableData,
  $columns,
  $columnWidths,
  $excludedColumnsFromSort,
  $pageSize,
  $tableParams,
  $selection,
  selectionChanged,
  needUpdateTable,
  setInstructionsTable,
  $hiddenColumnNames,
} from './table.model';

fxGetData.use(tableApi.getData);

const requestOccured = [fxGetData.pending];

const errorOccured = merge([fxGetData.failData]);

InstructionGate.open.watch((state) => {
  const config = {
    page: state.page,
    path: '',
  };
  instructionObserver.subscribe(config, setInstructionsTable);
});

$isLoading
  .on(requestOccured, (_, pending) => pending)
  .reset([instructionUnmounted, signout]);

$error
  .on(errorOccured, (_, error) => error)
  .on(requestOccured, () => null)
  .reset([instructionUnmounted, signout]);

$instructions
  .on(setInstructionsTable, (_, result) => result)
  .reset([instructionUnmounted, signout]);

$selection.on(selectionChanged, (_, selection) => selection).reset(signout);
$columns.reset([instructionUnmounted, signout]);
$excludedColumnsFromSort.reset([instructionUnmounted, signout]);
$pageSize.reset([instructionUnmounted, signout]);
$tableData.reset([instructionUnmounted, signout]);

sample({
  clock: fxGetData.done,
  source: $instructions,
  fn: ({ table: { meta } }, { result: { table } }) => ({
    count: table.count,
    data: formatData(table.data, meta),
  }),
  target: $tableData,
});

// При получении инструкций из страницы, пробрасываем в данные таблицы
// Хак для разного начального состояния инструкций с других страниц

sample({
  source: $instructions,
  fn: (data) => ({
    count: data.table?.count,
    data: formatData(data.table?.data, data.table?.meta),
  }),
  target: $tableData,
});

// При необходимости запрашиваем таблицу
sample({
  clock: [filtersSubmitted, $tableParams, needUpdateTable],
  source: {
    tableParams: $tableParams,
    $filterFields: filterForm.$fields,
    gate: InstructionGate.state,
  },
  target: fxGetData.prepend(formatPayload),
});

// Формируем колонки
sample({
  source: $instructions,
  fn: (data) => formatColumns(data?.table?.meta),
  target: $columns,
});

// Вычисляем исключенные колонки
sample({
  source: $instructions,
  fn: (data) => data?.table?.meta?.filter(({ sort }) => !sort).map(({ name }) => name),
  target: $excludedColumnsFromSort,
});

// Из колонок создаем массив с их шириной (300 по дефолту)
sample({
  source: $columns,
  fn: (data) => formatColumnsWidth(data),
  target: $columnWidths,
});

sample({
  source: $columns,
  fn: (columns) => {
    if (!Array.isArray(columns)) {
      return [];
    }

    return columns.reduce(
      (acc, column) => (column?.hide_on_load ? [...acc, column.name] : acc),
      []
    );
  },
  target: $hiddenColumnNames,
});
