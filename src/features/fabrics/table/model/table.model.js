import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { createTableBag } from '@tools/factories';

const TableGate = createGate({});
const mounted = TableGate.close;
const unmounted = TableGate.open;

const fxGetData = createEffect();
const fxDownloadTemplate = createEffect();
const fxImportData = createEffect();

const needUpdateTable = createEvent();
const selectionChanged = createEvent();
const downloadTemplate = createEvent();
const importData = createEvent();
const instructionsReady = createEvent();
const setInstructionsTable = createEvent();

const $isLoading = createStore(false);
const $error = createStore(null);
const $instructions = createStore({});
const $tableData = createStore({});
const $columns = createStore([]);
const $excludedColumnsFromSort = createStore([]);
const $selection = createStore([]);

export {
  TableGate,
  unmounted,
  mounted,
  $tableData,
  $columns,
  $excludedColumnsFromSort,
  $instructions,
  $isLoading,
  $error,
  $selection,
  selectionChanged,
  fxGetData,
  needUpdateTable,
  downloadTemplate,
  importData,
  fxDownloadTemplate,
  fxImportData,
  instructionsReady,
  setInstructionsTable,
};

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,
  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag($columns.getState(), {
  widths: [],
  pageSize: 50,
});
