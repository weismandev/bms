import { useMemo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  SortingState,
  SelectionState,
  PagingState,
  CustomPaging,
  IntegratedSelection,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DxTable,
  TableHeaderRow,
  Toolbar as DxToolbar,
  PagingPanel,
  TableColumnResizing,
  ColumnChooser,
  TableColumnVisibility,
  TableSelection,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import {
  $openedLink,
  openedLinkChanged,
} from '@features/fabrics/devices/detail/model/detail';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  SortingLabel,
  Loader,
} from '@ui/index';
import { TableToolbar } from '../../../table-toolbar';
import {
  $isLoading,
  $instructions,
  $tableData,
  $columns,
  $excludedColumnsFromSort,
  $currentPage,
  $pageSize,
  $sorting,
  $columnWidths,
  $hiddenColumnNames,
  $selection,
  selectionChanged,
  hiddenColumnChanged,
  columnWidthsChanged,
  sortChanged,
  pageNumChanged,
  pageSizeChanged,
} from '../../model';

const { t } = i18n;

const ContainerStatusFormatter = ({ value, row }) => (
  <span style={{ color: row.meta.state.color }}>{value}</span>
);

const ContainerStatusProvider = (props) => (
  <DataTypeProvider formatterComponent={ContainerStatusFormatter} {...props} />
);

const Row = (props) => {
  const openedLink = useStore($openedLink);
  const isSelected = openedLink === props.row.link;

  const handleClickRow = () => openedLinkChanged(props.row.link);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onClick={handleClickRow} />,
    [props.children, isSelected]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const DxToolbarRoot = (props) => (
  <DxToolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const Table = () => {
  const instructions = useStore($instructions);
  const isLoading = useStore($isLoading);
  const tableData = useStore($tableData);
  const rows = tableData.data || [];
  const columns = useStore($columns);
  const excludedColumnsFromSort = useStore($excludedColumnsFromSort);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const sorting = useStore($sorting);
  const columnWidths = useStore($columnWidths);
  const selection = useStore($selection);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  if (!Object.keys(instructions).length) {
    return (
      <Wrapper style={{ height: '100%', padding: 24 }}>
        <Loader isLoading />
      </Wrapper>
    );
  }

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />

      <Grid
        rootComponent={Root}
        rows={rows}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <ContainerStatusProvider for={['state']} />

        <SelectionState selection={selection} onSelectionChange={selectionChanged} />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <CustomPaging totalCount={tableData.count} />

        <DragDropProvider />

        <IntegratedSelection />

        <DxTable
          messages={{ noData: t('noData') }}
          rowComponent={(props) => <Row {...props} />}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <TableSelection showSelectAll />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <DxToolbar rootComponent={DxToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        {instructions.filter?.bottom?.length &&
          instructions.filter.bottom.map((item, key) => {
            if (item.name === 'pagination') {
              return (
                <PagingPanel
                  key={key}
                  pageSizes={item.list.map((value) => value.id)}
                  messages={{ rowsPerPage: item.label.title, info: '' }}
                />
              );
            }
          })}
      </Grid>
    </Wrapper>
  );
};

export { Table };
