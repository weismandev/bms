import { api } from '@api/api2';

const getData = (payload) => {
  return api.no_vers('get', 'interface/get-table', payload);
};

export const tableApi = {
  getData,
};
