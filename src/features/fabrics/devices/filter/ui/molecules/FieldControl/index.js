import { Field } from 'formik';
import { InputField } from '../../../../../../../ui';
import { SelectControlField } from '../SelectControl';

const renderComponent = (type) => {
  switch (type) {
    case 'combolist':
      return SelectControlField;
    case 'state':
      return SelectControlField;
    case 'list':
      return SelectControlField;
    default:
      return InputField;
  }
};

const FieldControl = ({ name, label, control = {}, ...props }) => {
  if (control?.type === 'submit' || control?.type === 'cancel') {
    return null;
  }

  return (
    <Field
      name={name}
      label={label}
      placeholder={control?.placeholder}
      component={renderComponent(control?.type)}
      isMulti={control?.type === 'combolist'}
      {...(control?.type === 'date_start_finish' ? { type: 'date' } : {})}
      {...control}
      {...props}
    />
  );
};

export { FieldControl };
