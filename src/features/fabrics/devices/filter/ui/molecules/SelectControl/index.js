import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl as Select, SelectField } from '../../../../../../../ui';
import { fxGetList, $list, $isLoading, $error } from '../../../model/select.model';

const SelectControl = ({ name, urldata: url, ...rest }) => {
  const options = useStore($list);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList({ url, name });
  }, []);

  return (
    <Select isLoading={isLoading} error={error} options={options[name] || []} {...rest} />
  );
};

const SelectControlField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<SelectControl />} onChange={onChange} {...props} />;
};

export { SelectControl, SelectControlField };
