import { useEffect } from 'react';
import { useStore, useGate } from 'effector-react';
import { Formik, Form } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  CustomScrollbar,
  Loader,
  FilterFooter,
} from '../../../../../../ui';
import {
  $isLoading,
  $filterControls,
  $filters,
  changedFilterVisibility,
  filtersSubmitted,
  ControlsGate,
  setDefaultFilterValues,
} from '../../model/filter.model';
import { FieldControl } from '../molecules/FieldControl';

const Filter = ({ defaultFilter }) => {
  const isLoading = useStore($isLoading);
  const filterControls = useStore($filterControls);
  const filterValues = useStore($filters);

  const handleReset = () =>
    filtersSubmitted(
      Object.keys(filterValues).reduce(
        (acc, current) => ({
          ...acc,
          [current]: '',
        }),
        {}
      )
    );

  useEffect(() => {
    setDefaultFilterValues(defaultFilter);
  }, []);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Loader isLoading={isLoading} />
        <Formik
          initialValues={filterValues}
          onSubmit={filtersSubmitted}
          onReset={handleReset}
          enableReinitialize
          render={(bag) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                {filterControls?.map((control) => (
                  <FieldControl
                    key={control.name}
                    name={control.name}
                    label={control.label.title}
                    control={control.control}
                  />
                ))}
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};

export const FilterWithGate = ({ defaultFilter = {} }) => {
  useGate(ControlsGate);

  return <Filter defaultFilter={{ ...defaultFilter }} />;
};
