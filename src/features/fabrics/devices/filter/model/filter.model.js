import { createEffect, createStore, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';
import { createFilterBag } from '../../../../../tools/factories';
import { signout } from '../../../../common';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const ControlsGate = createGate({});

const fxGetInstructions = createEffect();

const $isLoading = createStore(false);
const $error = createStore(null);
const $filterControls = createStore([]);

function filterObserver() {
  let filterBag = createFilterBag({});
  $filterControls.watch((state) => {
    const defaultFilters = state.reduce(
      (acc, current) => ({
        ...acc,
        [current.name]: current.state.value !== undefined ? current.state.value : '',
      }),
      {}
    );
    filterBag = createFilterBag(defaultFilters);
  });
  return () => filterBag;
}

const watch = filterObserver();

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  watch();

export const setDefaultFilterValues = createEvent();
export const changedGroupVisibility = createEvent();

export const $isGroupOpen = restore(changedGroupVisibility, false).reset([
  signout,
  changedFilterVisibility,
]);

export {
  PageGate,
  pageMounted,
  pageUnmounted,
  fxGetInstructions,
  $isLoading,
  $error,
  $filterControls,
  ControlsGate,
};
