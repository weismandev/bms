import { sample } from 'effector';
import { signout } from '../../../../common';
import { getInstructions } from '../api';
import {
  pageMounted,
  fxGetInstructions,
  $isLoading,
  $filterControls,
  setDefaultFilterValues,
} from './filter.model';

$isLoading.on(fxGetInstructions.pending, (_, isLoading) => isLoading).reset(signout);

$filterControls
  .on(fxGetInstructions.done, (_, { result: { filter } }) => filter.main)
  .on(setDefaultFilterValues, (state, values) => {
    return state.map((item) => {
      if (values[item.name] !== undefined) {
        return {
          ...item,
          state: {
            ...item.state,
            value: values[item.name],
          },
        };
      }
      return item;
    });
  })
  .reset(signout);

fxGetInstructions.use(getInstructions);

sample({
  clock: [pageMounted],
  fn: ({ page }) => ({ page }),
  target: fxGetInstructions,
});
