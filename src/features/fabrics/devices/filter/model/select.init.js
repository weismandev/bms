import { signout } from '../../../../common';
import { getList } from '../api';
import { fxGetList, $isLoading, $error, $list } from './select.model';

fxGetList.use(getList);

$list
  .on(fxGetList.done, (state, { params: { name }, result: { data } }) => ({
    ...state,
    [name]: data?.list || [],
  }))
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);
