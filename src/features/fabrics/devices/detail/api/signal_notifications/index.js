import { api } from '@api/api2';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { getCookieNameWithBuildType } from '@tools/cookie';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const getSignalNotification = async (url) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName);
  const response = await fetch(`${url}&token=${token}`, {
    headers: {
      ...getBaseHeaders()
    }
  });
  return response.json();
};

export const saveBounds = (data) => api.no_vers('post', 'interface/save_bounds', { data });
