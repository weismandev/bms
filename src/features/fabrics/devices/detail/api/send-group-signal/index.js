import { getCookieNameWithBuildType } from '@tools/cookie';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const sendGroupSignal = async ({ url, method = 'GET', ...data }) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName);

  const sendGroupSignalUrl = url.includes('token') ? url : `${url}&token=${token}`;

  if (method.toUpperCase() === 'GET') {
    const response = await fetch(
      `${sendGroupSignalUrl}&${new URLSearchParams(data).toString()}`,
      { headers: { ...getBaseHeaders() } }
    );
    return response.json();
  }
  const response = await fetch(sendGroupSignalUrl, {
    method,
    body: JSON.stringify(data),
    headers: { ...getBaseHeaders() }
  });
  return response.json();
};
