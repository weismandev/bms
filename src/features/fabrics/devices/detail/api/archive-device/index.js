import { getCookieNameWithBuildType } from '@tools/cookie';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const archiveDevice = (url) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName);
  return fetch(`${url}&token=${token}`, {
    headers: {
      ...getBaseHeaders()
    }
  });
};
