import { api } from '@api/api2';

export const getDeviceInfo = (serialnumber) =>
  api.no_vers('get', '/bmsdevice/get-device-info', { serialnumber, mts: 1 });
