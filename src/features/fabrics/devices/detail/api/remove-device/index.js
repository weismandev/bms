import { api } from '@api/api2';

export const removeDevice = ({ url, method }) => {
  const splittedUrl = url.split('?');

  const queryUrl = splittedUrl[0];
  const queryPayload =
    splittedUrl[1] && splittedUrl[1].length > 0
      ? splittedUrl[1].split('&').reduce((acc, value) => {
          if (value.includes('app') || value.includes('token')) {
            return acc;
          }

          const splittedValue = value.split('=');

          const formattedValue = { [splittedValue[0]]: splittedValue[1] };

          return { ...acc, ...formattedValue };
        }, {})
      : {};

  return api.no_vers(method, queryUrl, queryPayload);
};
