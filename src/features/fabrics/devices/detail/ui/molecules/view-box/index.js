import { Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { getSize } from '../../../lib/get-box-size';
import { implementParentParams } from '../../../lib/implementParentParams';
import { implementParentSignals } from '../../../lib/implementParentSignals';
import { Bounds } from '../bounds';

const useStyles = makeStyles(() => ({
  viewBox: {},
  viewBoxTitle: {},
  viewBoxControl: {},
}));

export const ViewBox = ({
  box,
  isViewMode,
  sliceCurrentTabControls,
  serialnumber,
  ...props
}) => {
  const classes = useStyles();

  const size = getSize(box);

  const needTitle = Array.isArray(box?.tags) && !box.tags.includes('no-title');

  const controls = box.signals.map((signal) => {
    const Control = signal.control.Component;
    const config = implementParentSignals(signal, box.signals);
    const params = implementParentParams(signal, box.signals);

    const renderSignal = (
      <Grid xs={size} key={signal.name} item>
        <Control
          isViewMode={isViewMode}
          boxId={box.id}
          info={signal}
          sliceCurrentTabControls={sliceCurrentTabControls}
          serialnumber={serialnumber}
          params={params}
          {...props}
          {...config}
        />
      </Grid>
    );

    return signal.bounds ? (
      <Grid xs={size} key={signal.name} item>
        <Bounds
          title={signal.bounds.title}
          dataBounds={signal.bounds.data}
          signalId={signal.id}
          settingsBlock={signal.settings_block}
          isViewMode={isViewMode}
          url={signal.bounds.url}
        >
          {renderSignal}
        </Bounds>
      </Grid>
    ) : (
      renderSignal
    );
  });

  return (
    <div className={classes.viewBox}>
      {needTitle && <h2 className={classes.viewBoxTitle}>{box.title}</h2>}
      <div className={classes.viewBoxControl}>
        <Grid container direction="row">
          {controls}
        </Grid>
      </div>
    </div>
  );
};
