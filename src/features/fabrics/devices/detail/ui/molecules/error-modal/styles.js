import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  paper: {
    position: 'relative',
    width: 'inherit',
    padding: 18,
    boxShadow: '0px 16px 15px rgba(0, 0, 0, 0.1)',
    borderRadius: 15,
    pointerEvents: 'auto',
    marginTop: 10,
  },
  iconBlock: {
    width: 24,
    height: 24,
    marginTop: -2,
  },
  headerText: {
    lineHeight: '21px',
    fontSize: 18,
    fontWeight: 900,
  },
  contentText: {
    lineHeight: '19px',
    fontSize: 16,
    color: '#767676',
    fontWeight: 'bold',
  },
  closeBlock: {
    marginLeft: 'auto',
  },
  container: {
    width: '370px',
    position: 'fixed',
    pointerEvents: 'none',
    zIndex: 9999,
    right: 10,
    bottom: 40,
  },
});
