import { useEffect } from 'react';
import { Paper, Grid, Typography, IconButton } from '@mui/material';
import { Close, ErrorOutline } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { useStyles } from './styles';

const { t } = i18n;

export const ErrorModal = ({ content = '', onClose = () => null, isOpen = false }) => {
  useEffect(() => {
    if (isOpen) {
      setTimeout(onClose, 5000);
    }
  }, [isOpen]);

  const color = 'red';

  const classes = useStyles();

  if (!isOpen) {
    return null;
  }

  return (
    <div className={classes.container}>
      <Paper style={{ border: `2px solid ${color}` }} classes={{ root: classes.paper }}>
        <Grid
          container
          justifyContent="space-between"
          alignItems="flex-start"
          wrap="nowrap"
          spacing={4}
        >
          <Grid classes={{ root: classes.iconBlock }} item>
            <ErrorOutline style={{ color }} />
          </Grid>
          <Grid item>
            <Typography style={{ color }} classes={{ root: classes.headerText }}>
              {t('Error')}
            </Typography>
            <Typography classes={{ root: classes.contentText }}>{content}</Typography>
          </Grid>
          <Grid classes={{ root: classes.closeBlock }} item>
            <IconButton size="small" onClick={onClose}>
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};
