import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: '#EDF6FF',
    borderRadius: '16px',
    padding: '24px',
    marginBottom: '24px',
    position: 'relative',
  },
  wrapperBound: {
    display: 'flex',
    justifyContent: 'space-between',

    '& > div': {
      minWidth: '33%',
    },
  },
  messageTitle: {
    textAlign: 'left',
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  btnEdit: {
    position: 'absolute',
    right: 9,
    top: 9,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    position: 'absolute',
    right: 9,
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default useStyles;
