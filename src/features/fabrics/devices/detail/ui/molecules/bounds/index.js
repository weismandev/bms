import { useEffect, useState } from 'react';
import { useStore } from 'effector-react';
import cn from 'classnames';
import { Collapse, CardActions, IconButton, LinearProgress } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import i18n from '@shared/config/i18n';
import { SelectControl, MessageText } from '@ui/index';
import {
  fxGetSignalsNotificationByUrl,
  setLoading,
  $bounds,
  sliceBoundsNotification,
} from '../../../model/bounds';
import { cancelClicked } from '../../../model/controls';
import { ListBounds } from '../../elements/controls/ListBounds';
import { TextBounds } from '../../elements/controls/TextBounds';
import useStyles from './style';

const conditionBeth = ['between', 'not between'];

const { t } = i18n;

export const Bounds = ({ title, signalId, isViewMode, children, url }) => {
  const { wrapper, wrapperBound, expand, expandOpen, messageTitle } = useStyles();
  const bounds = useStore($bounds);
  const [lastUpdatedField, updateFields] = useState({});
  const [isExpanded, setExpanded] = useState(false);
  const { data = [], isLoading = false } = bounds[signalId] || {};

  const handleExpandClick = () => {
    setExpanded(!isExpanded);
    if (!isExpanded && !bounds[signalId]) {
      fxGetSignalsNotificationByUrl(url);
      setLoading(signalId);
    }
  };

  const handleWatchLastUpdate = (state) => {
    updateFields({
      [state.sliced.signal_notification_level_id]: state.sliced,
    });
  };

  const handleCancelClick = () => {
    updateFields({});
  };

  useEffect(() => {
    const unsubscribe = sliceBoundsNotification.watch(handleWatchLastUpdate);
    const unsubscribeCancelClicked = cancelClicked.watch(handleCancelClick);
    return () => {
      unsubscribe();
      unsubscribeCancelClicked();
    };
  }, []);

  return (
    <div className={wrapper}>
      <div>{children}</div>
      <CardActions disableSpacing style={{ justifyContent: 'left' }}>
        <h3>{title}</h3>
        <IconButton
          className={cn(expand, {
            [expandOpen]: isExpanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={isExpanded}
          aria-label="show more"
          size="large"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        {data.map((bound) => {
          const options = [
            {
              id: bound.signal_notification_level_id,
              title: bound.signal_notification_level_title,
            },
          ];
          const { condition } = bound.conditions.find(
            (item) => item.id === bound.condition_id
          ) || {
            condition: false,
          };

          const isError =
            Number(bound.signal_value_float) > Number(bound.signal_value_float2);

          const lastFieldsUpdated = lastUpdatedField[bound.signal_notification_level_id];
          return (
            <div key={bound.signal_notification_level_id} className={wrapperBound}>
              <div style={{ padding: 8 }}>
                <SelectControl
                  options={options}
                  label={t('EventType')}
                  value={bound.signal_notification_level_id}
                  readOnly
                />
              </div>
              <ListBounds
                signalId={signalId}
                id={bound.signal_notification_level_id}
                options={bound.conditions}
                value={bound.condition_id}
                isViewMode={isViewMode}
              />
              {conditionBeth.includes(condition) ? (
                <div>
                  <MessageText text={t('Thresholds')} classes={{ item: messageTitle }} />
                  <div style={{ display: 'flex' }}>
                    <TextBounds
                      key={bound.signal_notification_level_id}
                      signalId={signalId}
                      isViewMode={isViewMode}
                      id={bound.signal_notification_level_id}
                      value={bound.signal_value_float}
                      field="signal_value_float"
                      tooltipOpen={lastFieldsUpdated?.signal_value_float}
                      isError={isError}
                    />
                    <TextBounds
                      key={bound.signal_notification_level_id}
                      signalId={signalId}
                      isViewMode={isViewMode}
                      id={bound.signal_notification_level_id}
                      value={bound.signal_value_float2}
                      field="signal_value_float2"
                      tooltipOpen={lastFieldsUpdated?.signal_value_float2}
                      isError={isError}
                    />
                  </div>
                </div>
              ) : (
                <TextBounds
                  key={bound.signal_notification_level_id}
                  signalId={signalId}
                  isViewMode={isViewMode}
                  id={bound.signal_notification_level_id}
                  value={bound.signal_value_float}
                  title={t('Thresholds')}
                  field="signal_value_float"
                />
              )}
            </div>
          );
        })}
      </Collapse>
      {isLoading && <LinearProgress />}
    </div>
  );
};
