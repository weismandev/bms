import PropTypes from 'prop-types';
import { Typography, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  title: (isError) => {
    return {
      textAlign: 'left',
      fontWeight: 500,
      marginBottom: 5,
      color: isError ? '#eb5757' : '#7D7D8E',
    };
  },
  text_required: {
    color: '#eb5757 !important',
  },
});

export const MessageText = (props) => {
  const { text, required, isError } = props;
  const { title, text_required } = useStyles(isError);

  return text.length ? (
    <Grid {...props} classes={{ item: title }} item>
      <Typography sx={{ whiteSpace: 'pre-line' }}>
        {text}
        {required && <span className={text_required}> * </span>}
      </Typography>
    </Grid>
  ) : null;
};

MessageText.propTypes = {
  text: PropTypes.string.isRequired,
};
