import styled from '@emotion/styled';

const colorStatus = {
  success: '#1BB169',
  warning: '#FF8A00',
  alarm: '#EB5757'
};

const StyledTitle = styled.div`
  display: inline-block;
  padding: 4px 16px;
  border-radius: 20px;
  font-size: 13px;
  color: white;
  background-color: ${(props) => props.bgColorStatus};
`;

export const ControlStatus = ({ title = '', status }) => (
  <StyledTitle bgColorStatus={colorStatus[status]}>
    {title}
  </StyledTitle>
);
