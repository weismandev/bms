import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { $isMainAdd } from '@features/fabrics/add/models/add';
import { Tabs } from '@ui/index';
import { $isAddMode } from '../../../model/detail';
import { changeTab } from '../../../model/tabs';

export const ControlsTabs = ({ options, currentTab }) => {
  const isMainAdd = useStore($isMainAdd);
  const isAddMode = useStore($isAddMode);

  if (isAddMode && isMainAdd) return null;

  return (
    <Tabs
      options={options}
      currentTab={currentTab}
      onChange={(e, tab) => changeTab(tab)}
      tabEl={<Tab />}
    />
  );
};
