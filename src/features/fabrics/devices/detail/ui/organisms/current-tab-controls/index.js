import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { CustomScrollbar } from '@ui/index';
import { changeMode, saveClicked } from '../../../model/controls';
import { $slicedCurrentTabControls, sliceCurrentTabControls } from '../../../model/tabs';
import { ViewBox } from '../../molecules/view-box';

const useStyles = makeStyles({
  wrapper: {
    padding: '0 24px',
    height: '100%',
  },
});

export const TabContent = ({ mode, serialnumber }) => {
  const slicedCurrentTabControls = useStore($slicedCurrentTabControls);
  if (!slicedCurrentTabControls) return null;

  const isViewMode = mode === 'view';

  if ('Content' in slicedCurrentTabControls) {
    const { Content, title } = slicedCurrentTabControls;

    return (
      <Content
        isViewMode={isViewMode}
        changeMode={(mode) => changeMode(mode)}
        title={title}
      />
    );
  }

  const viewBoxes =
    slicedCurrentTabControls?.boxes?.map((box) => {
      return (
        <ViewBox
          key={box.id}
          box={box}
          isViewMode={isViewMode}
          sliceCurrentTabControls={sliceCurrentTabControls}
          // sliceCurrentTabComposition={sliceCurrentTabComposition}
          serialnumber={serialnumber}
          //saveClicked={saveClicked}
        />
      );
    }) || null;

  return viewBoxes;
};

export const CurrentTabControls = ({ mode, hideToolbar, serialnumber }) => {
  const classes = useStyles();

  const scrollbarStyles = hideToolbar
    ? { height: 'calc(100vh - 168px)' }
    : { height: 'calc(100vh - 220px)' };

  return (
    <CustomScrollbar style={scrollbarStyles}>
      <div className={classes.wrapper}>
        <TabContent mode={mode} serialnumber={serialnumber} />
      </div>
    </CustomScrollbar>
  );
};
