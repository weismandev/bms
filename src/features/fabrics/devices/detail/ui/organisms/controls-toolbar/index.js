import { useEffect, useMemo } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';
import makeStyles from '@mui/styles/makeStyles';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { $isMainAdd } from '@features/fabrics/add/models/add';
import { DetailToolbar } from '@ui/index';
import {
  changeMode,
  cancelClicked,
  archiveClicked,
  saveClicked,
  saveAddClicked,
  setAction,
} from '../../../model/controls';
import { changeDeleteDialog } from '../../../model/delete-dialog';
import { $isAddMode, changedDetailVisibility } from '../../../model/detail';
import { $isDirty, $labels, $currentTabControls } from '../../../model/tabs';

import { $controlAddButtonName } from '../../../../../table-toolbar/models/controls/control-add-button';
import { $activeStep, setActiveStep } from '../../../../../entities/add-device/model/step';

import { ControlStatus } from '../../atoms/control-status';

const useStyles = makeStyles({
  wrapper: {
    margin: '10px 0 30px 0',
    padding: '0 15px',
  },
  wrapperToolbar: {
    display: 'flex',
    alignItems: 'center'
  }
});

export const ControlsToolbar = ({ closeHandler, mode, configTabsButtons }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const isAddMode = useStore($isAddMode);
  const isMainAdd = useStore($isMainAdd);
  const isDirty = useStore($isDirty);
  const labels = useStore($labels);
  const currentTabControls = useStore($currentTabControls);
  const controlAddButtonName = useStore($controlAddButtonName);
  const activeStep = useStore($activeStep);

  const handleArchive = () => archiveClicked(configTabsButtons?.archive?.url);
  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const isAddHidden = isMainAdd && isAddMode;

  const label = useMemo(() =>
    labels.filter(({ tab_id }) => tab_id === currentTabControls?.id),
  [currentTabControls, labels]
  );

  useEffect(() => {
    setAction(
      Object.keys(configTabsButtons).reduce(
        (acc, current) => ({
          ...acc,
          [current]: {
            url: configTabsButtons[current]?.url,
            method: configTabsButtons[current]?.http_method || 'get',
          },
        }),
        {}
      )
    );
  }, [configTabsButtons]);

  const isShowTitle = Array.isArray(label) && label.length > 0;
  const [status] = label;
  const isAddDevice = controlAddButtonName === 'add_device';

  return (
    <div className={cn(classes.wrapper, { [classes.wrapperToolbar]: isAddDevice })}>
      {isAddDevice && (
        <>
          {activeStep > 0 && (
            <IconButton aria-label="back" onClick={handleBack}>
              <ArrowBackIcon />
            </IconButton>
          )}
          <Typography variant="h6" sx={{ minWidth: 600 }}>
            {t('AddingANewDevice')}
          </Typography>
        </>
      )}
      <DetailToolbar
        mode={mode}
        onEdit={() => changeMode('edit')}
        onClose={closeHandler}
        onCancel={() => {
          if (isAddMode) {
            cancelClicked();
            changedDetailVisibility(false);
          } else {
            cancelClicked();
            changeMode('view');
          }
        }}
        onArchive={handleArchive}
        onSave={isAddMode ? saveAddClicked : saveClicked}
        onDelete={() => changeDeleteDialog(true)}
        hidden={{
          edit: isAddHidden || !configTabsButtons?.edit,
          delete: isAddHidden || !configTabsButtons?.delete,
          archive: isAddHidden || !configTabsButtons?.archive,
        }}
        hasError={!isDirty}
        title={isShowTitle && <ControlStatus title={status.title} status={status.type} /> }
      />
    </div>
  );
};
