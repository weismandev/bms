import { useStore, useGate } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { $isMainAdd, $isAddLoading } from '@features/fabrics/add/models/add';
import { $currentStepAddDevice } from '@features/fabrics/add/models/add-device';
import { AddDeviceForm } from '@features/fabrics/add/ui/organisms/add-device-form';
import { AddDevice } from '@features/fabrics/entities/add-device';
import {
  $isAddMode,
  $openedLink,
  changedDetailVisibility,
  closeDetail,
} from '@features/fabrics/devices/detail/model/detail';
import i18n from '@shared/config/i18n';
import { Wrapper, DeleteConfirmDialog } from '@ui/index';
import { ControlsGate, $mode, $loading, deleteDevice } from '../../model/controls';
import { changeDeleteDialog, $isOpenDeleteDialog } from '../../model/delete-dialog';
import {
  $errorText,
  $isOpenModalAddError,
  changedErrorModalVisibility,
} from '../../model/error-modal';
import { $currentTab, $options, $tabsButtons } from '../../model/tabs';
import { $controlAddButtonName } from '../../../../table-toolbar/models/controls/control-add-button';

import { ControlLoader } from '../atoms/control-loader';
import { ErrorModal } from '../molecules/error-modal';
import { ControlsTabs } from '../organisms/controls-tabs';
import { ControlsToolbar } from '../organisms/controls-toolbar';
import { CurrentTabControls } from '../organisms/current-tab-controls';

const { t } = i18n;

const Detail = ({ serialnumber }) => {
  const mode = useStore($mode);
  const loading = useStore($loading);
  const isAddLoading = useStore($isAddLoading);
  const options = useStore($options);
  const currentTab = useStore($currentTab);
  const isOpenDeleteDialog = useStore($isOpenDeleteDialog);
  const tabsButtons = useStore($tabsButtons);
  const isAddMode = useStore($isAddMode);
  const isMainAdd = useStore($isMainAdd);
  const currentStepAddDevice = useStore($currentStepAddDevice);
  const errorText = useStore($errorText);
  const isOpenModalAddError = useStore($isOpenModalAddError);
  const controlAddButtonName = useStore($controlAddButtonName);

  const currentTabConfig = options[currentTab] || {};
  const hideToolbar = currentTabConfig?.hideToolbar;

  const renderTabsControls = () => {
    switch (controlAddButtonName) {
      case 'add_device':
        return <AddDevice />;
      default:
        return (
          <CurrentTabControls
            mode={mode}
            hideToolbar={hideToolbar}
            serialnumber={serialnumber}
          />
        );
    }
    // if (isAddMode && isMainAdd) return <AddDeviceForm type={currentStepAddDevice} />;
  };

  const configTabsButtons = tabsButtons
    .filter((item) => item.settings_block_id === currentTabConfig.id)
    .reduce(
      (acc, current) => ({
        ...acc,
        [current.action]: current,
      }),
      {}
    );

  const onCloseErrorModal = () => changedErrorModalVisibility(false);

  const onCloseDetail = () => {
    changedDetailVisibility(false);
    closeDetail();
  };

  return (
    <Wrapper
      style={{
        overflow: 'hidden',
        position: 'relative',
        height: '100%',
      }}
    >
      {loading || isAddLoading ? <ControlLoader /> : null}

      <>
        <div>
          <ControlsTabs options={options} currentTab={currentTab} />
          {!hideToolbar && (
            <ControlsToolbar
              closeHandler={onCloseDetail}
              mode={mode}
              configTabsButtons={configTabsButtons}
            />
          )}
        </div>

        {renderTabsControls()}
      </>

      <ColoredTextIconNotification />
      <ErrorModal
        isOpen={isOpenModalAddError}
        content={errorText}
        onClose={onCloseErrorModal}
      />

      <DeleteConfirmDialog
        header={t('RemovingADevice')}
        content={t('AreYouSureYouWantToRemoveTheDevice')}
        isOpen={isOpenDeleteDialog}
        close={() => changeDeleteDialog(false)}
        confirm={deleteDevice}
      />
    </Wrapper>
  );
};

export { Detail };

export const DetailWithGate = () => {
  const url = useStore($openedLink);

  useGate(ControlsGate, {
    url,
    externalContent: [],
  });

  const serialnumber = new URLSearchParams(url).get('serialnumber');

  return <Detail serialnumber={serialnumber} />;
};
