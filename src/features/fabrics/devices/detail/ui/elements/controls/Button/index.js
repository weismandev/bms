import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
  control: { url: '' },
};

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
    },
  },
  title: {
    fontWeight: 500,
    color: '#7D7D8E',
    textAlign: 'center',
    marginBottom: 5,
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
});

export const ButtonControl = ({
  info = defaultValue,
  isViewMode,
}) => {
  const {
    label: { title },
    state: { readonly },
    control: { url },
  } = info;
  const classes = useStyles();

  const clickHandler = async () => {
    await fetch(url, {
      headers: { ...getBaseHeaders() }
    });
  };

  const disabled = readonly || isViewMode;

  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>{title}</div>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          disabled={disabled}
          onClick={clickHandler}
        >
          {t('Button')}
        </Button>
      </div>
    </div>
  );
};
