import { useState, useEffect } from 'react';
import { format } from 'date-fns';
import { makeStyles } from '@mui/styles';
import i18n from '@shared/config/i18n';
import { InputControl } from '@ui/index';
import { useRequiredControls } from '../../../../hooks';
import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: '', measure: '' },
};

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    textAlign: 'left',
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  content: {
    textAlign: 'center',
  },
});

// color: #eb5757!important;
const formatDate = (date) => {
  const splitedDate = date.split('.');

  if (splitedDate.length === 1) {
    return date;
  }

  const day = Number.parseInt(splitedDate[0], 10);
  const month = Number.parseInt(splitedDate[1], 10);
  const year = Number.parseInt(splitedDate[2], 10);

  const newDate = new Date(`${year}-${month}-${day}`);

  return format(newDate, 'yyyy-MM-dd');
};

export const DateControl = ({ info = defaultValue, isViewMode, boxId }) => {
  const {
    label: { title },
    state: { value, readonly },
    required,
    required_title,
  } = info;
  const classes = useStyles();

  const date = value.length !== 0 ? formatDate(value) : '';
  const { errors, resetErrors } = useRequiredControls(date, required, required_title);

  const mode = isViewMode || readonly ? 'view' : 'edit';
  const isError = errors.length > 0;

  const changeHandler = (e) => {
    if (e?.target?.value) {
      const currentDate = new Date(e.target.value);

      const date = format(currentDate, 'yyyy-MM-dd');

      configureData({ info, value: date, boxId }).slice();

      if (errors.length > 0) {
        resetErrors();
      }
    }
  };

  return (
    <div className={classes.wrapper}>
      <MessageText text={title} required={required} isError={isError} />
      <InputControl
        label={null}
        value={date}
        type="date"
        onChange={changeHandler}
        placeholder=""
        mode={mode}
        required={required}
        errors={errors}
        error={isError}
      />
    </div>
  );
};
