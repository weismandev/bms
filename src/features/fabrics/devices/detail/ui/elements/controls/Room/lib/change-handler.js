import { sliceCurrentTabControls } from '../../../../../model/tabs';

export const changeHandler = (info, boxId, type) => {
  const data = {
    ...info,
    boxId,
    modified: type === 'room',
    state: {
      value: type === 'room' && info.room.data.id,
    },
  };

  const slice = () => sliceCurrentTabControls(data);

  return { slice };
};
