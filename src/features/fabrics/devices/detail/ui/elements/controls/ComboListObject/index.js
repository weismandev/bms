import { useState, useEffect } from 'react';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { configureData } from '../../../../lib/common';
import { ComboListControl } from '../ComboList';
import { useStyles } from './styles';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { readonly: null },
  list: [],
};

const formatProps = ({ props, value }) => {
  const currentValues =
    value.find((object) => Number(object.id) === Number(props.id))?.values || [];
  return {
    label: { title: props.title },
    state: { readonly: false, value: currentValues },
    list: props.list,
    styles: props.styles,
  };
};

export const ComboListObjectControl = ({
  info = defaultValue,
  isViewMode,
  sliceCurrentTabControls,
  boxId,
  ...rest
}) => {
  const {
    label: { title },
    state: { readonly, value: initValue },
    list,
    id,
  } = info;

  const classes = useStyles();
  const [value, setValue] = useState(JSON.parse(initValue));

  useEffect(() => {
    setValue(JSON.parse(initValue));
  }, [initValue]);

  const stringifyState = (state) => {
    const arrayStrings = state.map(({ id, values }) => {
      return `{"id":${id},"values":[${values.join()}]}`;
    });
    return `[${arrayStrings.join()}]`;
  };

  const changeHandler = (id, newValue) => {
    const newState = value.map((object) => {
      if (object.id == id) {
        object.values = newValue;
      }
      return object;
    });
    if (newState !== value) {
      setValue(newState);
      configureData({ info, value: stringifyState(newState), boxId }).slice();
    }
  };

  const disabled = readonly || isViewMode;

  if (!list) {
    return null;
  }

  return (
    <div className={classes.wrapper}>
      <Typography className={classes.title}>{title}</Typography>
      <>
        {list.map((props) => (
          <ComboListControl
            info={formatProps({ props, value })}
            isViewMode={disabled}
            disabled={disabled}
            sliceCurrentTabControls={sliceCurrentTabControls}
            key={props.id}
            parentChangeHandler={(value) => changeHandler(props.id, value)}
            {...rest}
          />
        ))}
      </>
    </div>
  );
};
