import format from 'date-fns/format';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { InputControl } from '@ui/index';
import { useRequiredControls } from '../../../../hooks';
import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: '', measure: '' },
};

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  content: {
    textAlign: 'center',
  },
});

export const DateTimeControl = ({ info = defaultValue, isViewMode, boxId }) => {
  const {
    label: { title },
    state: { value, readonly },
    required,
    required_title,
  } = info;
  const classes = useStyles();

  const dateValue = value ? new Date(value * 1000) : new Date();
  const date = format(dateValue, "yyyy-MM-dd'T'HH:mm");
  const { errors, resetErrors } = useRequiredControls(date, required, required_title);

  const mode = isViewMode || readonly ? 'view' : 'edit';
  const isError = errors.length > 0;
  const changeHandler = (e) => {
    const timestamp = new Date(e.target.value) / 1000;

    configureData({ info, value: timestamp, boxId }).slice();
    if (errors.length > 0) {
      resetErrors();
    }
  };

  return (
    <div className={classes.wrapper}>
      <MessageText text={title} required={required} isError={isError} />
      <InputControl
        label={null}
        value={date}
        type="datetime-local"
        onChange={changeHandler}
        placeholder=""
        mode={mode}
        required={required}
        errors={errors}
        error={isError}
      />
    </div>
  );
};
