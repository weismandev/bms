import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { InputControl } from '@ui/index';
import { useRequiredControls } from '../../../../hooks';
import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: '', measure: '' },
};

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  content: {
    textAlign: 'center',
  },
});

export const TextControl = ({
  info = defaultValue,
  isViewMode,
  boxId,
  type = 'control',
  interceptor,
  divider,
}) => {
  const {
    label: { title },
    state: { value, readonly },
  } = info;
  const classes = useStyles();

  const { errors, resetErrors } = useRequiredControls(
    value,
    info.required,
    info.required_title
  );

  const mode = isViewMode || readonly ? 'view' : 'edit';

  const changeHandler = (e) => {
    configureData({ info, value: e.target.value, boxId }).slice(interceptor ?? type);
    if (errors.length > 0) {
      resetErrors();
    }
  };

  return (
    <div className={classes.wrapper}>
      <MessageText text={title} required={info.required} isError={errors.length > 0} />
      <InputControl
        label={null}
        value={value}
        onChange={changeHandler}
        placeholder=""
        mode={mode}
        required={info.required}
        errors={errors}
        error={errors.length > 0}
        divider={info.divider ?? true}
      />
    </div>
  );
};
