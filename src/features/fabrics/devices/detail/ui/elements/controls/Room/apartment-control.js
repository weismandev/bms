import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useOptions, useSelected, changeHandler } from './lib';

const { t } = i18n;

export const ApartmentControl = ({ info, boxId, style, disabled, mode }) => {
  const { id } = info;
  const complexId = info.complex.data?.id;
  const buildingId = info.building.data?.id;

  const isDisabled = disabled ? disabled : !buildingId;
  const title = info.apartment.title;
  const name = `${title}${id}`;
  const url = `${info.apartment.url}&complex_id=${complexId}&building_id=${buildingId}`;

  const options = useOptions({
    url,
    list: [],
    type: 'apartment',
  });

  const selectedValue = useSelected({
    options,
    value: info.apartment.data,
  });

  const value = selectedValue
    ? selectedValue
    : info.apartment.data?.id !== 0
    ? info.apartment.data
    : '';

  const onChange = ({ id, title }) => {
    info.apartment.data = { id: id || 0, title: title || '' };
    info.room.data = { id: 0, title: '' };

    return changeHandler(info, boxId).slice();
  };

  return (
    <SelectControl
      name={name}
      classes={{ item: style }}
      id={title}
      isDisabled={isDisabled}
      options={options}
      value={value}
      onChange={onChange}
      label={title}
      placeholder={t('SelectField')}
      isClearable={false}
      mode={mode}
    />
  );
};
