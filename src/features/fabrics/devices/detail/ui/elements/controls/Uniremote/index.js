import { useState } from 'react';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { api } from '@api/api2';
import i18n from '@shared/config/i18n';
import { Loader } from '@ui/atoms';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    fontWeight: 500,
    color: '#7D7D8E',
  },
  content: {
    padding: '5px 10px',
  },
});

export const UniremoteControl = ({ info, isViewMode, serialnumber }) => {
  const {
    label: { title },
    state: { value, readonly },
    name,
  } = info;
  const classes = useStyles();
  const [loading, changeLoading] = useState(false);

  const isValueValid = typeof value === 'string' && Boolean(value.trim());
  const clickHandler = async () => {
    const data = {
      serialnumber,
      signal: name,
    };

    if (value) {
      data.value = value;
    }

    try {
      changeLoading(true);
      await api.no_vers('get', '/apartment/send-signal', data);
      changeLoading(false);
    } catch {
      changeLoading(false);
    }
  };

  const disabled = readonly || isViewMode;

  if (!isValueValid) return null;

  return (
    <div className={classes.wrapper}>
      <Loader isLoading={loading} />
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>
        <Button
          variant="contained"
          color="primary"
          disabled={disabled || loading}
          onClick={clickHandler}
        >
          {t('Execute')}
        </Button>
      </div>
    </div>
  );
};
