import { useState, useEffect } from 'react';
import { getCookie } from '@tools/cookie';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { possiblyNotEmptyArray } from '../../../../../lib/common';

async function fetchList(url, settled, fieldType) {
  let type = fieldType;
  let response;
  try {
    response = await fetch(`${url}&token=${getCookie('bms_token')}`, {
      headers: {
        ...getBaseHeaders()
      }
    });
  } catch (err) {
    type = 'error';
    console.error(err);
  }

  if (type === 'error') return settled([]);
  const raw = await response.json();

  switch (type) {
    case 'complex':
      return settled(raw?.data?.items || []);
    case 'building':
      return settled(
        raw?.data?.buildings?.map((item) => ({
          id: item.building.id,
          title: item.building.title,
        })) || []
      );
    case 'apartment':
      return settled(
        raw?.data?.items?.map((item) => ({
          id: item.apartment.id,
          title: item.apartment.title,
        })) || []
      );
    case 'room':
      return settled(
        raw?.data?.rooms?.map((room) => ({
          id: room.id,
          title: room.title,
        })) || []
      );
    default:
      return settled([]);
  }
}

export function useOptions({ url, list, type }) {
  const [rawList, updateList] = useState(() => list);

  useEffect(() => {
    if (url) {
      try {
        fetchList(url, updateList, type);
      } catch (e) {
        console.error('fetchList', e);
      }
    }
  }, [url]);

  const options = possiblyNotEmptyArray(rawList)
    ? rawList.map((item) => ({ id: item.id, title: item.title }))
    : [];

  return options;
}
