import format from 'date-fns/format';
import makeStyles from '@mui/styles/makeStyles';
import { MessageText, InputControl } from '@ui/index';
import { configureData } from '../../../../lib/common';

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    textAlign: 'left',
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  content: {
    textAlign: 'center',
  },
});

const getTime = (value) => {
  const currentDate = new Date();

  const hours = Math.trunc(value / 60);
  const minutes = value - hours * 60;
  const seconds = 0;

  return format(new Date(currentDate.setHours(hours, minutes, seconds)), 'HH:mm');
};

const getTimeInMinutes = (value) => {
  const scaleOfNotation = 10;

  const hoursAndMinutes = value.split(':');

  const hours = Number.parseInt(hoursAndMinutes[0], scaleOfNotation);
  const minutes = Number.parseInt(hoursAndMinutes[1], scaleOfNotation);

  return hours * 60 + minutes;
};

export const TimeControl = ({ info, isViewMode, boxId, defaultValues, interceptor }) => {
  const {
    label: { title },
    state: { value, readonly },
  } = info;

  const classes = useStyles();

  const mode = isViewMode || readonly ? 'view' : 'edit';

  const time = getTime(value);

  const changeHandler = (e) => {
    const timeInMinutes = getTimeInMinutes(e.target.value);

    configureData({ info, value: timeInMinutes, boxId }).slice(interceptor);
  };

  return (
    <div className={classes.wrapper}>
      <MessageText text={title} classes={{ item: classes.title }} />
      <InputControl
        label={null}
        type="time"
        value={time}
        onChange={changeHandler}
        placeholder=""
        mode={mode}
        divider={info.divider ?? true}
        state={defaultValues}
      />
    </div>
  );
};
