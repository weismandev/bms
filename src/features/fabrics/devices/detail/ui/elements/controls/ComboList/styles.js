import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
      margin: 0,
    },
  },
  select: {
    marginBottom: 10,
    '&:last-of-type': {
      marginBottom: 0,
    },
  },
});
