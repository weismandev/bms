import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { changeNotification } from '@features/colored-text-icon-notification';
import i18n from '@shared/config/i18n';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { actionStatus } from '../../../../lib/action-status';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
  control: { params: {} },
};

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
      textAlign: 'center',
      marginBottom: 5,
    },
  },
  title: {
    fontWeight: 500,
    color: '#7D7D8E',
    marginBottom: 5,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
  },
  buttonWrapper: {
    marginRight: 10,
  },
  buttonImg: {
    width: 30,
    height: 30,
    marginLeft: 10,
  },
});

export const JalousieControl = ({
  info = defaultValue,
  isViewMode,
  sliceCurrentTabControls,
  boxId,
  ...rest
}) => {
  const {
    label: { title },
    state: { value, readonly },
    control: { params },
  } = info;

  const classes = useStyles();
  const disabled = readonly || isViewMode;

  const buttons = Object.keys(params).map((key) => {
    const img = params[key]?.img;
    const title = params[key]?.title;
    const action = params[key]?.action?.url;

    const actionHandler = async () => {
      try {
        await fetch(action, {
          headers: { ...getBaseHeaders() }
        });
        changeNotification(actionStatus.success);
      } catch (error) {
        changeNotification(actionStatus.failure);
      }
    };

    return (
      <div key={title} className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          onClick={actionHandler}
          disabled={disabled}
        >
          {title}
          {img && <img src={img} className={classes.buttonImg} alt={t('Label.action')} />}
        </Button>
      </div>
    );
  });

  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>{title}</div>
      <div className={classes.buttons}>{buttons}</div>
    </div>
  );
};
