import { useState, useEffect } from 'react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { buildUrl } from '@features/fabrics/devices/detail/lib/buildUrl';
import { formatOptions } from './formatOptions';

const fetchList = async ({
  baseUrl,
  offset,
  list,
  value,
  total,
  params,
  signal,
}) => {
  if (Boolean(total) && offset > total) {
    return { total, options: list };
  }
  const url = buildUrl({ baseUrl, offset, value, params });
  const response = await fetch(url, {
    signal,
    headers: {
      ...getBaseHeaders()
    }
  });
  const { data } = await response.json();
  return {
    total: data?.meta?.total,
    options: data?.list ?? []
  };
};

export const useAsyncOptions = async ({ search, baseUrl, params = {}, signal }) => {
  const url = buildUrl({ search, baseUrl, params });
  const response = await fetch(url, {
    signal,
    headers: {
      ...getBaseHeaders()
    }
  });
  const raw = await response.json();
  const result = formatOptions(raw?.data?.list);
  return result || [];
};

export const useOptions = ({
  baseUrl,
  offset,
  list = [],
  value,
  total,
  params,
  setIsLoading,
}) => {
  const [rawList, updateList] = useState(formatOptions(list));
  let control = new AbortController();

  const fetchOptions = async (id = null) => {
    if (!baseUrl) return;
    control.abort();
    control = new AbortController();
    setIsLoading(true);
    const response = await fetchList({
      baseUrl,
      offset,
      list,
      total,
      params,
      value: id,
      signal: control.signal
    });
    setIsLoading(false);
    updateList(response.options);
  };

  const getOptionsBySearch = (search) => {
    control.abort();
    control = new AbortController();
    const data = useAsyncOptions({ search, baseUrl, params, signal: control.signal });
    return data;
  };

  useEffect(() => {
    if (value && list.length > 0) {
      const isId = !list.some((item) => Number(item.id) === Number(value));
      fetchOptions(isId ? Number(value) : null);
    }
  }, [value]);

  useEffect(() => {
    if (baseUrl) {
      fetchOptions();
    }
  }, [offset, JSON.stringify(params)]);

  useEffect(() => {
    updateList(formatOptions(list));
  }, [list]);

  return {
    options: formatOptions(rawList) || [],
    getOptionsBySearch
  };
};
