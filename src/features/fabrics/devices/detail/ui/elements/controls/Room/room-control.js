import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useOptions, useSelected, changeHandler } from './lib';

const { t } = i18n;

const RoomControl = ({ info, boxId, style, disabled, mode }) => {
  const { id } = info;
  const apartmentId = info.apartment.data?.id;

  const isDisabled = disabled ? disabled : !apartmentId;
  const title = info.room.title;
  const name = `${title}${id}`;
  const url = `${info.room.url}&apartment_id=${apartmentId}`;

  const options = useOptions({
    url,
    list: [],
    type: 'room',
  });

  const selectedValue = useSelected({
    options,
    value: info.room.data,
  });

  const value = selectedValue
    ? selectedValue
    : info.room.data?.id !== 0
    ? info.room.data
    : '';

  const onChange = ({ id, title }) => {
    info.room.data = { id: id || 0, title: title || '' };

    return changeHandler(info, boxId, 'room').slice();
  };

  return (
    <SelectControl
      name={name}
      classes={{ item: style }}
      id={title}
      isDisabled={isDisabled}
      options={options}
      value={value}
      onChange={onChange}
      label={title}
      placeholder={t('SelectField')}
      isClearable={false}
      mode={mode}
    />
  );
};

export { RoomControl as RoomCntrl };
