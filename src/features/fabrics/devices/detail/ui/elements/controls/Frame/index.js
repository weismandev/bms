import makeStyles from '@mui/styles/makeStyles';
import { Video } from '@features/video';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
  control: { ratio: 1 },
};

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    fontWeight: 500,
    color: '#7D7D8E',
    marginBottom: 5,
  },
  frame: {},
});

export const FrameControl = ({
  info = defaultValue,
  isViewMode,
  sliceCurrentTabControls,
  boxId,
  ...rest
}) => {
  const {
    label: { title },
    state: { value },
    control: { ratio },
  } = info;
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>{title}</div>
      <div className={classes.frame}>
        <Video
          src={value}
          additionalStyles={{
            width: '100%',
            minHeight: '300px',
          }}
        />
      </div>
    </div>
  );
};
