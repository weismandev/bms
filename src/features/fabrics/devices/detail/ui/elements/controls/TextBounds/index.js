import { Tooltip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { MessageText, InputControl } from '@ui/index';
import { configureNotification } from '../../../../lib/common';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    textAlign: 'left',
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  content: {
    textAlign: 'center',
  },
});

const placement = {
  signal_value_float: 'bottom',
  signal_value_float2: 'bottom-start',
};

const errors = {
  signal_value_float: t('LowerBoundValue'),
  signal_value_float2: t('UpperBoundValue'),
};

export const TextBounds = ({
  isViewMode,
  signalId,
  id,
  title,
  value,
  field,
  isError,
  tooltipOpen,
}) => {
  const classes = useStyles();

  const mode = isViewMode ? 'view' : 'edit';

  const handler = configureNotification({ id, signalId, field });

  return (
    <div className={classes.wrapper}>
      {title && <MessageText text={title} classes={{ item: classes.title }} />}
      <Tooltip
        title={errors[field]}
        placement={placement[field]}
        open={isError && tooltipOpen && !isViewMode}
        disableFocusListener
        disableHoverListener
      >
        <InputControl
          label={null}
          value={value}
          onChange={handler}
          placeholder=""
          mode={mode}
          error={isError}
        />
      </Tooltip>
    </div>
  );
};
