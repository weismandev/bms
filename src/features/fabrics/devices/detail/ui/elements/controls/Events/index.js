import { useGate, useStore } from 'effector-react';
import { Pagination } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Loader } from '@ui/index';
import {
  EventsGate,
  $events,
  $eventLoading,
  $meta,
  pageChanged,
} from '../../../../model/events';

const { t } = i18n;

const useStyles = makeStyles({
  event: {
    display: 'flex',
    flexDirection: 'row',
    padding: '20px 24px',
    marginBottom: '15px',
    background: 'rgb(27,177,105, 0.15)',
    color: '#65657B',
  },
  left: {
    width: '7%',
    '& img': {
      width: '24px',
      height: '24px',
    },
  },
  center: {
    width: '63%',
  },
  right: {
    display: 'flex',
    flexDirection: 'column',
    width: '30%',
    textAlign: 'right',
    '& div > p': {
      margin: 0,
    },
  },
  create: {
    fontWeight: 500,
    opacity: '0.7',
    marginBottom: '5px',
  },
  references: {
    '& > a': {
      textDecoration: 'underline',
    },
    '& > a:first-child': {
      marginRight: '10px',
    },
  },
  title: {
    fontWeight: 500,
  },
  initiator: {
    marginTop: 'auto',
  },
  pagination: {
    margin: '10px 0',
  },
  pagination_ul: {
    justifyContent: 'flex-end',
  },
});

export const Events = ({ info }) => {
  const {
    control: { url },
  } = info;

  const classes = useStyles();
  const events = useStore($events);
  const loading = useStore($eventLoading);
  const meta = useStore($meta);

  useGate(EventsGate, url);

  if (loading) return <Loader isLoading />;

  if (!events.length) return <div>{t('noData')}</div>;

  const list = events.map((event) => {
    return (
      <div key={event.id} className={classes.event}>
        <div className={classes.left}>
          <img src={event.icon} alt="icon" />
        </div>
        <div className={classes.center}>
          <p className={classes.create}>{event.dt.create}</p>
          <p className={classes.title}>{event.title}</p>
          <div className={classes.references}>
            {event.photo && (
              <a href={event.photo} target="_blank">
                {t('Snapshot')}
              </a>
            )}
            {event.videoarchive && (
              <a href={event.videoarchive} target="_blank">
                {t('archive')}
              </a>
            )}
          </div>
        </div>
        <div className={classes.right}>
          <p>{event.type_title}</p>
          <div className={classes.initiator}>
            <p>{`${t('Performer')}:`}</p>
            <p>{event.initiator}</p>
          </div>
        </div>
      </div>
    );
  });

  return (
    <div>
      {list}
      <div className={classes.pagination}>
        <Pagination
          count={meta.count}
          page={meta.page}
          classes={{ ul: classes.pagination_ul }}
          onChange={(e, page) => pageChanged(page)}
        />
      </div>
    </div>
  );
};
