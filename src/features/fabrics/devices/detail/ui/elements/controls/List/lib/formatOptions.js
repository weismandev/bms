export const formatOptions = (options = []) => {
  if (!Array.isArray(options)) return [];
  return options
    .map((option) => ({
      id: option.id || option.key,
      value: option.title || option.value,
    }));
}
