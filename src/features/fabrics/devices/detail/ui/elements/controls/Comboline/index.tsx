import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';
import { StyledComboline, StyledCombolineItem, StyledItem } from './styled';

export const ComboLineControl = (props: any) => {
  const { info, boxId, isViewMode, interceptor } = props;
  const { label, required, list, state } = info;
  const mode = isViewMode || state.readonly ? 'view' : 'edit';

  const changeHandler = (id: number) => () => {
    const strId = String(id);
    const value = state.value?.trim() ?? '';

    const stateArr = !value.length ? [] : value.split(',');
    let result = !value.length ? [] : value.split(',');

    if (stateArr.indexOf(strId) >= 0) {
      result = stateArr.filter((arrId: number | string) => String(arrId) !== strId);
    } else {
      result.push(strId);
    }

    configureData({ info, value: result.join(','), boxId }).slice(interceptor);
  };

  const value = state.value?.trim() ?? '';
  const stateArr = !value.length ? [] : value.split(',');

  return (
    <StyledItem>
      <MessageText text={label.title} required={!!required} isError={false} />

      <StyledComboline>
        {list.map((item: any) => (
          <StyledCombolineItem
            disabled={mode === 'view'}
            selected={stateArr.indexOf(String(item.id)) >= 0}
            type="button"
            onClick={changeHandler(item.id)}
          >
            {item.value}
          </StyledCombolineItem>
        ))}
      </StyledComboline>
    </StyledItem>
  );
};
