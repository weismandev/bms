import Input from '@mui/material/Input';
import Slider from '@mui/material/Slider';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { configureData } from '../../../../lib/common';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
  },
  title: {
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  counter: {
    display: 'flex',
    margin: 'auto',
  },
  input: {
    textAlign: 'center',
  },
  slider: {
    marginRight: 10,
  },
});

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
};

export const CounterControl = ({ info = defaultValue, isViewMode, boxId }) => {
  const {
    label: { title },
    state: { value, measure, readonly },
  } = info;

  const classes = useStyles();

  const changeHandler = (newValue) => {
    const value = `${Number(newValue)}`;

    configureData({ info, value, boxId }).slice();
  };

  const changeHandlerSlider = (event, newValue) => {
    changeHandler(newValue);
  };

  const changeHandlerInput = (event) => {
    const newValue = Number(event.target.value);

    changeHandler(newValue);
  };

  const header = `${title}, ${measure}`;

  const disabled = readonly || isViewMode;

  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>{header}</div>
      <div className={classes.counter}>
        <Slider
          className={classes.slider}
          value={Number(value)}
          onChange={changeHandlerSlider}
          aria-labelledby="input-slider"
          disabled={disabled}
        />
        <Input
          classes={{ input: classes.input }}
          value={Number(value)}
          margin="dense"
          onChange={changeHandlerInput}
          disabled={disabled}
          inputProps={{
            step: 10,
            min: 0,
            max: 100,
            type: 'number',
            'aria-labelledby': 'input-slider',
          }}
        />
      </div>
    </div>
  );
};
