import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { SwitchControl } from '@ui/index';
import { configureData } from '../../../../lib/common';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
};

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
    },
  },
});

export const ToggleControl = ({
  info = defaultValue,
  isViewMode,
  sliceCurrentTabControls,
  boxId,
  ...rest
}) => {
  const {
    label: { title },
    state: { value, readonly },
  } = info;

  const classes = useStyles();

  const changeHandler = () => {
    const checked = Boolean(Number(value));

    configureData({ info, value: `${Number(!checked)}`, boxId }).slice(rest.interceptor);
  };

  const disabled = readonly || isViewMode;

  return (
    <div className={classes.wrapper} style={rest?.styles?.label ?? {}}>
      <SwitchControl
        {...rest}
        label={title}
        labelPlacement="start"
        value={Boolean(Number(value))}
        onChange={changeHandler}
        divider={false}
        disabled={disabled}
        style={rest?.styles?.label ?? {}}
      />
    </div>
  );
};
