import { useState } from 'react';
import Slider from '@mui/material/Slider';
import Typography from '@mui/material/Typography';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';

const useStyles = makeStyles({
  wrapper: {},
});

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
};

export const RangeControl = ({ info = defaultValue, ...rest }) => {
  const {
    label: { title },
    state: { value, measure, readonly },
  } = info;

  const classes = useStyles();
  const [range, setRange] = useState([20, 37]);

  const handleChange = (event, newValue) => {
    setRange(newValue);
  };

  return (
    <div className={classes.wrapper}>
      <Typography id="range-slider" gutterBottom>
        Temperature range
      </Typography>
      <Slider
        value={range}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        getAriaValueText={() => `${range}${measure}`}
      />
    </div>
  );
};
