import makeStyles from '@mui/styles/makeStyles';
import { ApartmentControl } from './apartment-control';
import { BuildingControl } from './building-control';
import { ComplexControl } from './complex-control';
import { RoomCntrl } from './room-control';

const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
      margin: 0,
    },
  },
  select: {
    marginBottom: 10,
  },
  twoField: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  field: {
    width: '50%',
  },
});

export const RoomControl = ({ info, isViewMode, boxId }) => {
  const classes = useStyles();

  const mode = isViewMode ? 'view' : 'edit';
  const isDisabled = info.state.readonly || isViewMode;

  return (
    <div className={classes.wrapper}>
      <div className={classes.twoField}>
        <div className={classes.field} style={{ marginRight: 20 }}>
          {info.complex && (
            <ComplexControl
              info={info}
              boxId={boxId}
              style={classes.select}
              isDisabled={isDisabled}
              mode={mode}
            />
          )}
        </div>
        <div className={classes.field}>
          {info.building && (
            <BuildingControl
              info={info}
              boxId={boxId}
              style={classes.select}
              disabled={isDisabled}
              mode={mode}
            />
          )}
        </div>
      </div>
      <div className={classes.twoField}>
        <div className={classes.field} style={{ marginRight: 20 }}>
          {info.apartment && (
            <ApartmentControl
              info={info}
              boxId={boxId}
              style={classes.select}
              disabled={isDisabled}
              mode={mode}
            />
          )}
        </div>
        <div className={classes.field}>
          {info.room && (
            <RoomCntrl
              info={info}
              boxId={boxId}
              style={classes.select}
              disabled={isDisabled}
              mode={mode}
            />
          )}
        </div>
      </div>
    </div>
  );
};
