import styled from '@emotion/styled';

export const StyledItem = styled.div`
  padding: 5px 10px;
`;

export const StyledComboline = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: stretch;
  width: 100%;

  > button:first-child {
    border-radius: 16px 0 0 16px;
  }

  > button:last-child {
    border-radius: 0 16px 16px 0;
    border-right: 1px solid #191c291f;
  }
`;

export const StyledCombolineItem = styled.button<{ selected: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1 1 auto;
  border: 1px solid #191c291f;
  border-right: 0;
  background: ${({ selected }) => (selected ? 'rgba(31, 135, 229, 0.08)' : '#fff')};
  cursor: pointer;

  font-size: 14px;
  font-weight: 500;
  line-height: 24px;
  color: ${({ selected }) => (selected ? '#1F87E5' : '#191c2999')};
`;
