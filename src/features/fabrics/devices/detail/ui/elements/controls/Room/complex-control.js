import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useOptions, useSelected, changeHandler } from './lib';

const { t } = i18n;

export const ComplexControl = ({ info, boxId, style, isDisabled, mode }) => {
  const { id } = info;

  const title = info.complex.title;
  const name = `${title}${id}`;
  const url = info.complex.url;
  const options = useOptions({
    url,
    list: [],
    type: 'complex',
  });

  const selectedValue = useSelected({
    options,
    value: info.complex.data,
  });

  const value = selectedValue
    ? selectedValue
    : info.complex.data?.id !== 0
    ? info.complex.data
    : '';

  const onChange = ({ id, title }) => {
    info.complex.data = { id: id || 0, title: title || '' };
    info.building.data = { id: 0, title: '' };
    info.apartment.data = { id: 0, title: '' };
    info.room.data = { id: 0, title: '' };

    return changeHandler(info, boxId).slice();
  };

  return (
    <SelectControl
      name={name}
      classes={{ item: style }}
      id={title}
      isDisabled={isDisabled}
      options={options}
      value={value}
      onChange={onChange}
      label={title}
      placeholder={t('SelectField')}
      isClearable={false}
      mode={mode}
    />
  );
};
