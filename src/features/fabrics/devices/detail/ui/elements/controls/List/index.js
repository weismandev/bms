import { useState, useEffect } from 'react';
import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useRequiredControls } from '../../../../hooks';
import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';
import { useOptions, useAsyncOptions, useSelected } from './lib';
import { useStyles } from './styles';

const LIMIT = 100;

const { t } = i18n;

export const ListControl = ({
  info,
  isViewMode,
  boxId,
  required,
  helpText,
  errors,
  params,
  ...rest
}) => {
  const {
    label: { title },
    state: { readonly, value },
    list,
    id,
    control,
  } = info;
  /* TODO Вынести на уровень выше в мапу добавить фильтрацию */
  if (!rest.visible) return null;

  const [offset, setOffset] = useState(0);
  const [total, setTotal] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const classes = useStyles();

  const baseUrl = control?.url;
  const disabled = readonly || isViewMode || rest.disabled || isLoading;
  const name = `${title}${id}`;

  const { options, getOptionsBySearch } = useOptions({
    baseUrl,
    offset,
    list,
    value,
    total,
    setTotal,
    setIsLoading,
    params,
    name: info.name,
  });

  const selected = useSelected({ options, value });
  const { errors: error, resetErrors } = useRequiredControls(
    selected,
    info.required,
    info.required_title
  );
  const handleOffset = () => {
    if (options.length < total) {
      setOffset((offset) => offset + LIMIT);
    }
  };

  const changeHandler = (value) => {
    const id = value?.id || 0;
    configureData({ info, value: id, boxId }).slice(rest.interceptor);
    if (error.length > 0) {
      resetErrors();
    }
  };

  useEffect(() => {
    if (rest.disabled) {
      configureData({ info, value: null, boxId }).slice(rest.interceptor);
    }
  }, [rest.disabled]);

  return (
    <div className={classes.wrapper}>
      <MessageText text={title} required={info.required} isError={error.length > 0} />
      <SelectControl
        {...rest}
        name={name}
        id={title}
        classes={{ item: classes.select }}
        isDisabled={disabled}
        value={selected}
        onChange={changeHandler}
        label=""
        placeholder={t('SelectField')}
        divider={false}
        isClearable={false}
        mode={isViewMode ? 'view' : 'edit'}
        required={required}
        helpText={helpText}
        error={(errors && errors.length > 0) || error.length}
        errors={error}
        kind="async"
        // cacheOptions
        options={options}
        defaultOptions={options}
        isLoading={isLoading}
        loadOptions={getOptionsBySearch}
        onMenuScrollToBottom={handleOffset}
        openMenuOnFocus={false}
      />
    </div>
  );
};
