import { useMemo } from 'react';
import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useRequiredControls } from '../../../../hooks';
import { configureData } from '../../../../lib/common';
import { MessageText } from '../../../atoms/control-label';
import { useOptions } from './hooks';
import { useStyles } from './styles';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { readonly: null },
  list: [],
};

export const ComboListControl = ({
  info = defaultValue,
  isViewMode,
  boxId,
  required,
  helpText,
  parentChangeHandler,
  disabled,
  params,
  ...rest
}) => {
  const {
    label: { title },
    state: { readonly, value = '[]' },
    list = [],
    id,
    styles = {},
  } = info;
  const classes = useStyles();

  const baseUrl = info?.control?.url;
  const { isLoading, data, fetchOptionsBySearch } = useOptions({
    baseUrl,
    params,
  });

  const extraStyles = Object.entries(styles).reduce((acc, [key, value]) => {
    const formattedKey = key.replace(/-([a-z])/, (a, l) => l.toUpperCase());
    acc[formattedKey] = value;
    return acc;
  }, {});

  const options = Array.isArray(data?.list) ? list.concat(data.list) : list;

  /** парсинг разновидностей типов данных */
  const arrayValues = useMemo(() => {
    if (!value) return [];
    if (Array.isArray(value)) return value;
    if (JSON.parse(value) === 'number') return [value];
    return [];
  }, [value]);

  const selectedValues = arrayValues.map((v) => {
    return options.find((option) => {
      return Number(option?.id) === Number(v);
    });
  });

  const { errors, resetErrors } = useRequiredControls(
    selectedValues,
    info.required,
    info.required_title
  );

  const changeHandler = (value) => {
    const ids = value.map((option) => option.id);
    if (parentChangeHandler) {
      parentChangeHandler(ids);
    } else {
      configureData({ info, value: `[${ids.join()}]`, boxId }).slice();
    }
    if (errors.length > 0) {
      resetErrors();
    }
  };

  const isDisabled = disabled || readonly || isViewMode;

  return (
    <div className={classes.wrapper} style={extraStyles}>
      <MessageText text={title} required={info.required} isError={errors.length > 0} />
      <SelectControl
        {...rest}
        name={`${title}${id}`}
        classes={{ item: classes.select }}
        isDisabled={isDisabled}
        options={options}
        value={selectedValues}
        isMulti
        onChange={changeHandler}
        label=""
        placeholder={t('SelectFields')}
        mode={isViewMode ? 'view' : 'edit'}
        required={required}
        helpText={helpText}
        error={errors && errors.length > 0}
        errors={errors}
        divider={false}
        isClearable={false}
        kind="async"
        cacheOptions
        defaultOptions={options}
        isLoading={isLoading}
        loadOptions={fetchOptionsBySearch}
      />
    </div>
  );
};
