import i18n from '@shared/config/i18n';

const { t } = i18n;

export const UnknownControl = ({ info }) => {
  return (
    <div>
      <div>{t('UnknownControl')}</div>
      <span>{info.control.type}</span>
    </div>
  );
};
