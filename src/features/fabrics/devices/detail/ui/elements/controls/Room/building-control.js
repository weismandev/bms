import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { useOptions, useSelected, changeHandler } from './lib';

const { t } = i18n;

export const BuildingControl = ({ info, boxId, style, disabled, mode }) => {
  const { id } = info;
  const complexId = info.complex.data?.id;

  const isDisabled = disabled ? disabled : !complexId;
  const title = info.building.title;
  const name = `${title}${id}`;
  const url = `${info.building.url}&complex_id=${complexId}`;

  const options = useOptions({
    url,
    list: [],
    type: 'building',
  });

  const selectedValue = useSelected({
    options,
    value: info.building.data,
  });

  const value = selectedValue
    ? selectedValue
    : info.building.data?.id !== 0
    ? info.building.data
    : '';

  const onChange = ({ id, title }) => {
    info.building.data = { id: id || 0, title: title || '' };
    info.apartment.data = { id: 0, title: '' };
    info.room.data = { id: 0, title: '' };

    return changeHandler(info, boxId).slice();
  };

  return (
    <SelectControl
      name={name}
      classes={{ item: style }}
      id={title}
      isDisabled={isDisabled}
      options={options}
      value={value}
      onChange={onChange}
      label={title}
      placeholder={t('SelectField')}
      isClearable={false}
      mode={mode}
    />
  );
};
