import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#4e4e5b',
      margin: 0,
    },
  },
  title: {
    marginBottom: 10,
    color: '#7D7D8E',
    fontWeight: 500,
    fontSize: 14,
  },
});
