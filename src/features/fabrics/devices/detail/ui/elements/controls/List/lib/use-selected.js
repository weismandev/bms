import { possiblyNotEmptyArray } from '../../../../../lib/common';

export function useSelected({ options, value }) {
  const selected = possiblyNotEmptyArray(options)
    ? options.find((item) => Number(item.id) === Number(value))
    : null;

  return selected;
}
