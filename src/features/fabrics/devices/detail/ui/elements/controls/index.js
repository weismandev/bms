import { ActionControl } from './Action';
import { ButtonControl } from './Button';
import { ComboListControl } from './ComboList';
import { ComboListObjectControl } from './ComboListObject';
import { ComboLineControl } from './Comboline';
import { CounterControl } from './Counter';
import { DateControl } from './Date';
import { DateTimeControl } from './DateTime';
import { Events } from './Events';
import { FrameControl } from './Frame';
import { JalousieControl } from './Jalousie';
import { ListControl } from './List';
import { RangeControl } from './Range';
import { RoomControl } from './Room';
import { TextControl } from './Text';
import { TextBounds } from './TextBounds';
import { TimeControl } from './Time';
import { ToggleControl } from './Toggle';
import { UniremoteControl } from './Uniremote';
import { UnknownControl } from './Unknown';

export const Controls = {
  counter: (props) => <CounterControl {...props} />,
  text: (props) => <TextControl {...props} />,
  toggle: (props) => <ToggleControl {...props} />,
  comboline: (props) => <ComboLineControl {...props} />,
  combolist: (props) => <ComboListControl {...props} />,
  combolist_object: (props) => <ComboListObjectControl {...props} />,
  list: (props) => <ListControl {...props} />,
  time: (props) => <TimeControl {...props} />,
  range: (props) => <RangeControl {...props} />,
  frame: (props) => <FrameControl {...props} />,
  button: (props) => <ButtonControl {...props} />,
  jalousie_main: (props) => <JalousieControl {...props} />,
  action_time: (props) => <ActionControl {...props} />,
  action_sun: (props) => <ActionControl {...props} />,
  events: (props) => <Events {...props} />,
  uniremote: (props) => <UniremoteControl {...props} />,
  room: (props) => <RoomControl {...props} />,
  datetime: (props) => <DateTimeControl {...props} />,
  date: (props) => <DateControl {...props} />,
  default: (props) => <UnknownControl {...props} />,
};

export const Bounds = {
  text: (props) => <TextBounds {...props} />,
};
