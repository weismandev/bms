import { useState, useEffect } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { SelectControl } from '@ui/index';
import { configureCondition } from '../../../../lib/common';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    padding: '8px 10px',
    borderRadius: 5,
  },
  title: {
    textAlign: 'left',
    fontWeight: 500,
    marginBottom: 5,
    color: '#7D7D8E',
  },
  content: {
    textAlign: 'center',
  },
});

export const ListBounds = ({ isViewMode, signalId, id, options, value }) => {
  const classes = useStyles();

  const mode = isViewMode ? 'view' : 'edit';

  const handler = configureCondition({ id, signalId });

  return (
    <div className={classes.wrapper}>
      <SelectControl
        options={options}
        label={t('ConditionIf')}
        onChange={handler}
        value={value}
        mode={mode}
      />
    </div>
  );
};
