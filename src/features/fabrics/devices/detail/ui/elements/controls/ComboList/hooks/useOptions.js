import { useState, useEffect } from 'react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { buildUrl } from '@features/fabrics/devices/detail/lib/buildUrl';

const defaultState = {
  isLoading: false,
  data: {
    list: [],
  },
};

export const useOptions = ({ baseUrl, params }) => {
  if (!baseUrl) return {
    ...defaultState,
    fetchOptionsBySearch: async () => []
  };

  const [status, setStatus] = useState(defaultState);

  const fetchOptions = async (url) => {
    const response = await fetch(url, {
      headers: { ...getBaseHeaders() }
    });
    const { data } = await response.json();
    return data;
  };

  const fetchOptionsBySearch = async (search) => {
    const url = buildUrl({ baseUrl, search, params });
    const { list } = await fetchOptions(url);
    return list;
  };

  useEffect(() => {
    const getData = async () => {
      const url = buildUrl({ baseUrl, params });
      const res = await fetchOptions(url);

      return res;
    };

    const data = getData();

    setStatus({ ...status, isLoading: false, data });
  }, [baseUrl, params]);

  return { ...status, fetchOptionsBySearch };
};
