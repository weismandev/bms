import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { changeNotification } from '@features/colored-text-icon-notification';
import i18n from '@shared/config/i18n';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { actionStatus } from '../../../../lib/action-status';

const { t } = i18n;

const defaultValue = {
  label: { title: t('isNotDefinedit') },
  state: { value: null, measure: '', readonly: null },
  control: { params: {} },
};

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    padding: '5px 10px',
    borderRadius: 5,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
      textAlign: 'center',
      marginBottom: 5,
    },
  },
  title: {
    fontWeight: 500,
    color: '#7D7D8E',
    textAlign: 'left',
  },
  action: {
    display: 'flex',
    flexDirection: 'column',
  },
  actionDesc: {
    textAlign: 'center',
    margin: 0,
  },
  actionValue: {
    textAlign: 'center',

    marginBottom: 5,
  },
  buttonWrapper: {
    margin: '0 auto',
  },
  buttonImg: {
    width: 30,
    height: 30,
    marginLeft: 10,
  },
});

export const ActionControl = ({
  info = defaultValue,
  isViewMode,
  sliceCurrentTabControls,
  boxId,
  ...rest
}) => {
  const {
    label: { title },
    state: { value, readonly },
    control: { params },
  } = info;
  const classes = useStyles();
  const disabled = readonly || isViewMode;

  const actions = Object.keys(params).map((key, index) => {
    const currentValue = params[key]?.value;
    const desc = params[key]?.desc;
    const action = params[key]?.action?.url;

    const actionHandler = async () => {
      try {
        await fetch(action, {
          headers: { ...getBaseHeaders() }
        });
        changeNotification(actionStatus.success);
      } catch (error) {
        changeNotification(actionStatus.failure);
      }
    };

    return (
      <div key={index} className={classes.action}>
        <p className={classes.actionDesc}>{desc}</p>
        <b className={classes.actionValue}>{currentValue}</b>
        <div className={classes.buttonWrapper}>
          <Button
            variant="contained"
            color="primary"
            onClick={actionHandler}
            disabled={disabled}
          >
            {t('Label.action')}
          </Button>
        </div>
      </div>
    );
  });
  return (
    <div className={classes.wrapper}>
      <div className={classes.title}>{title}</div>
      {actions}
    </div>
  );
};
