import Composition from './Composition';

export const Interface = {
  composition: (props) => <Composition {...props} />,
};
