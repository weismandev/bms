import { Controls } from '../../../controls';

const implementSignals = (info, values = []) => {
  const parentSignals = {};

  const signalTypes = info.template.signals.reduce(
    (acc, current) => ({ ...acc, [current.name]: current.state.value }),
    {}
  );

  const findParentSignals = (parentId) =>
    info.template.signals.find((signal) => signal.id === parentId);

  const findSignalByName = (name) =>
    info.template.signals.find((signal) => signal.name === name);

  return values.map((item) =>
    Object.keys({ ...signalTypes, ...item })
    .filter(findSignalByName)
    .map((name) => {
      const founded = findSignalByName(name);
      const config = { disabled: false, visible: !founded.parent?.value };
      parentSignals[founded.id] = founded;
      if (founded.parent?.value && founded.parent?.value !== null) {
        const parentId = founded.parent.id;
        const parent = findParentSignals(parentId);
        // eslint-disable-next-line eqeqeq
        const isMatch = item[parent.name] == founded.parent.value;

        if (isMatch) {
          config.disabled = false;
          config.visible = true;
        }
      }

      return {
        ...founded,
        state: {
          ...founded.state,
          value: item[name],
          readonly: config.disabled,
        },
        control: {
          ...founded.control,
          visible: config.visible,
          Component: Controls[founded?.control?.type],
        },
      };
    })
  );
};

export default implementSignals;
