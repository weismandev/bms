import { useState, useEffect, memo } from 'react';
import { IconButton, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/AddCircleOutline';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { saveClicked } from '../../../../model/controls';
import implementSignals from './lib/implement-signals';

const { t } = i18n;

const useStyles = makeStyles({
  compositionForm: {
    display: 'flex',
    flexWrap: 'wrap',

    '& > div': {
      width: '48%',
    },
  },
  btnRemove: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  btnAdd: {
    color: '#0394E3',
    fontWeight: 500,
    fontSize: 13,
    marginTop: 20,
    marginBottom: 20,
  },
});

const Composition = ({ info, isViewMode, boxId, ...props }) => {
  const classes = useStyles();

  const [defaultValues, setDefaultValues] = useState([]);
  const [compositions, setCompositions] = useState([]);
  const [errors, setErrors] = useState([]);
  const [actions, setActions] = useState({});

  const filterByRequiredSignals = (signal) => (
    signal.required &&
    !signal.invisible &&
    !signal.state.value &&
    signal.control.visible
  );

  const handleAddComposition = () => {
    const newDefaultValues = info.template.signals
      .filter((signal) => !signal.parent)
      .reduce(
        (acc, signal) => ({
          ...acc,
          [signal.name]: null,
        }),
        {}
      );
    const newComposition = implementSignals(info, [newDefaultValues]);
    const requiredSignals = newComposition.flat().filter(filterByRequiredSignals);

    setDefaultValues(defaultValues.concat(newDefaultValues));
    setCompositions(compositions.concat(newComposition));
    props.sliceCurrentTabControls({
      ...info,
      state: {
        ...info.state,
        value: JSON.stringify(JSON.parse(info.state.value).concat(newDefaultValues)),
      },
      boxId,
      errors: info.errors || requiredSignals.length > 0,
      modified: true,
    });
  };

  const handleInterceptor = (key) => ({ name, state }) => {
    const newValues = defaultValues.map((values, index) =>
      index !== key ? values : { ...values, [name]: state.value }
    );

    const collectionErrors = errors.map((error, index) => {
      if (index !== key) return error;
      const { [name]: property, ...rest } = error;
      return rest;
    });
    const newCompositions = implementSignals(info, newValues);
    const requiredSignals = newCompositions.map((composition) =>
      composition.filter(filterByRequiredSignals)
    );
    setCompositions(newCompositions);
    setErrors(collectionErrors);
    setDefaultValues(newValues);
    props.sliceCurrentTabControls({
      ...info,
      state: {
        ...info.state,
        value: JSON.stringify(newValues),
      },
      boxId,
      errors: requiredSignals.flat().length > 0,
      modified: true,
    });
  };

  const handleDelete = (key) => () => {
    const newValues = defaultValues.filter((_, index) => index !== key);
    const newCompositions = compositions.filter((_, index) => index !== key);
    const newErrors = errors.filter((_, index) => index !== key);

    setCompositions(newCompositions);
    setErrors(newErrors);
    setDefaultValues(newValues);
    setTimeout(() => {
      props.sliceCurrentTabControls({
        ...info,
        state: {
          ...info.state,
          value: JSON.stringify(newValues),
        },
        boxId,
        errors: newErrors.flat().length > 0,
        modified: true,
      });
    }, 10);
  };

  const subscribeSaveClicked = () => {
    const compositionErrors = compositions.map((composition) =>
      composition.filter(filterByRequiredSignals).reduce(
        (acc, signal) => ({
          ...acc,
          [signal.name]: [t('thisIsRequiredField')],
        }),
        {}
      )
    );
    const isError = compositionErrors.filter((error) => Object.keys(error).length);
    setErrors(compositionErrors);
    props.sliceCurrentTabControls({
      ...info,
      state: {
        ...info.state,
        value: JSON.stringify(defaultValues),
      },
      boxId,
      errors: isError,
      modified: isError,
    });
  };

  useEffect(() => {
    setActions(
      info.actions.reduce((acc, action) => ({ ...acc, [action.type]: action }), {})
    );
  }, []);

  useEffect(() => {
    const values = JSON.parse(info.state.value);
    const compositionSignals = implementSignals(info, values);
    setDefaultValues(values);
    setCompositions(compositionSignals);
  }, [info.state.value]);

  useEffect(() => {
    const unsubscribe = saveClicked.watch(subscribeSaveClicked);
    return unsubscribe;
  });

  return (
    <div>
      {compositions.map((composition, index) => (
        <div key={index} style={info.template.styles}>
          {!isViewMode && (
            <IconButton
              onClick={handleDelete(index)}
              className={classes.btnRemove}
              aria-label="delete"
              size="large"
            >
              <DeleteIcon style={{ fill: '#EB5757', transform: 'rotate(45deg)' }} />
            </IconButton>
          )}
          <div className={classes.compositionForm}>
            {composition
              .filter((signal) => !signal.invisible && signal.control.visible)
              .map((signal) => (
                <signal.control.Component
                  key={signal.id}
                  isViewMode={isViewMode}
                  boxId={boxId}
                  info={signal}
                  sliceCurrentTabControls={signal}
                  serialnumber={signal.id}
                  interceptor={handleInterceptor(index)}
                  required={signal.required}
                  errors={(errors[index] && errors[index][signal.name]) || []}
                  defaultValues={defaultValues}
                  visible
                />
              ))}
          </div>
        </div>
      ))}
      {!isViewMode && actions.add_item && (
        <Button
          onClick={handleAddComposition}
          className={classes.btnAdd}
          startIcon={<AddIcon />}
        >
          {t('AddScript')}
        </Button>
      )}
    </div>
  );
};

export default memo(Composition);
