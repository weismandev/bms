import { getControl } from './getControl';

const tab = 'view-tab';
const box = 'view-box';

const determinator = {
  tab: (tags) => tags.includes(tab),
  box: (tags) => tags.includes(box),
};

const utility = {
  possiblyNotArray: (params) => !Array.isArray(params),
  merge: (tabs, externalContent = []) => tabs.concat(externalContent),
};

const assembling = (meta, determinator) => {
  return meta.settings_block.filter((block) => {
    if (utility.possiblyNotArray(block?.tags)) return false;

    return determinator(block.tags);
  });
};

export const getListOfControlsByTabs = ({ signals, meta }, externalContent) => {
  const empty = [];

  if (!Array.isArray(signals) || !signals.length) return empty;

  const tabs = utility.merge(assembling(meta, determinator.tab), externalContent);

  const boxes = assembling(meta, determinator.box);

  const listOfControls = signals
    .filter((signal) => 'settings_block' in signal)
    .map((info) => ({
      ...info,
      control: {
        ...info.control,
        Component: getControl(info?.control?.type),
      },
    }));

  const boxesWithSignals = boxes.map((box) => {
    const currentSignals = listOfControls.filter((signal) => {
      return Number(signal.settings_block) === Number(box.id);
    });

    return {
      ...box,
      signals: currentSignals,
    };
  });

  const tabsWithBoxes = tabs.map((tab) => {
    const currentBoxes = boxesWithSignals.filter((box) => box.parent_id === tab.id);

    return {
      ...tab,
      boxes: currentBoxes,
    };
  });

  return tabsWithBoxes;
};
