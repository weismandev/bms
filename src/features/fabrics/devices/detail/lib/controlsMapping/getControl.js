import { Interface, Controls } from '../../ui/elements';

export const getControl = (type) => {
  if (type in Interface) return Interface[type];
  if (type in Controls) return Controls[type];

  return Controls.default;
};
