import { getCookieNameWithBuildType } from '@tools/cookie';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const buildUrl = ({
  baseUrl,
  search,
  value,
  offset = 0,
  limit = 10000,
  params = {},
}) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName);
  const urlObject = new URL(baseUrl);

  urlObject.searchParams.set('limit', limit);
  urlObject.searchParams.set('offset', offset);
  urlObject.searchParams.set('token', token);

  if (value) {
    urlObject.searchParams.set('id', value);
  }

  if (search) {
    urlObject.searchParams.set('search', search);
    urlObject.searchParams.set('offset', 0);
  }

  Object.keys(params).forEach((name) => {
    urlObject.searchParams.set(name, params[name] || '');
  });

  return urlObject.toString();
};
