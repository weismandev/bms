import { sliceCurrentTabControls } from '../../../model/tabs';

export function configureData({ info, value, boxId }) {
  const data = {
    ...info,
    state: {
      ...info.state,
      value,
    },
    boxId,
    modified: true,
  };

  const slice = (interceptor) => {
    if (typeof interceptor === 'function') {
      interceptor(data);
    } else {
      sliceCurrentTabControls(data);
    }
  };

  return { slice };
}
