export const possiblyNotEmptyArray = (array) => Array.isArray(array) && array.length;
