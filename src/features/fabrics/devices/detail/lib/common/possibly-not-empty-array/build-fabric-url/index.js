import { getDefaultParams } from '@api/common';

const getDefaultStuff = (serialnumber) => ({
  url: 'https://api-product.mysmartflat.ru/api/bmsdevice/get-device-info/?',
  params: {
    mts: 1,
    serialnumber,
    ...getDefaultParams(),
  },
});

export const buildFabricUrl = (
  { link, serialnumber } = { link: '', serialnumber: '' }
) => {
  if (link) return link;
  if (!serialnumber) return '';

  const { url, params } = getDefaultStuff(serialnumber);
  const searchParams = new URLSearchParams(params);

  return `${url}${searchParams.toString()}`;
};
