import { sliceBoundsNotification } from '../../../model/bounds';
import { toggleDirty } from '../../../model/tabs/tabs.model';

export const configureNotification =
  ({ id, signalId, field }) =>
  (e) => {
    toggleDirty(true);
    sliceBoundsNotification({
      signalId,
      sliced: {
        signal_notification_level_id: id,
        [field]: e.target.value,
        isDirty: true,
      },
    });
  };

export const configureCondition =
  ({ id, signalId }) =>
  (condition) => {
    toggleDirty(true);
    sliceBoundsNotification({
      signalId,
      sliced: {
        signal_notification_level_id: id,
        condition_id: condition?.id,
        isDirty: true,
      },
    });
  };
