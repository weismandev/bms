export const controlsTabMapping = (tabs) => {
  if (!tabs) return [];

  return tabs?.map((tab, index) => {
    const hideToolbar = Boolean(tab?.Content);

    return {
      id: tab.id,
      value: index,
      label: tab.title,
      disabled: false,
      hideToolbar,
    };
  });
};
