import { CheckCircle, Error } from '@mui/icons-material';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const actionStatus = {
  success: {
    isOpen: true,
    color: '#1BB169',
    text: t('ActionCompleted'),
    Icon: CheckCircle,
  },
  failure: {
    isOpen: true,
    color: '#eb5757',
    text: t('ActionFailed'),
    Icon: Error,
  },
};
