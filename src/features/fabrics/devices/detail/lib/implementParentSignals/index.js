const matchCondition = (parentState, childValue) => {
  if (!parentState) return false;
  if (!childValue) return true;
  if (parentState == childValue) return true;

  return childValue.split(',').includes(parentState);
};

const implementParentSignals = (signal, signals) => {
  if (!signal.parent) return { disabled: false, visible: true };

  const visible = signal.parent?.visible;
  const config = { disabled: visible, visible: visible };
  const founded = signals.find(
    (item) => item.id === signal.parent.id || item.name === signal.parent.name
  );

  const isMatch = matchCondition(founded?.state?.value, signal.parent?.value);
  if (isMatch) {
    config.disabled = false;
    config.visible = true;
  }

  return config;
};

export { implementParentSignals };
