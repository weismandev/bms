const implementParentParams = (signal, signals) => {
  if (!signal.control.dop_params) return {};

  return signals
    .filter((control) => signal.control.dop_params.includes(control.name))
    .reduce((acc, curr) => ({ ...acc, [curr.name]: curr.state.value }), {});
};

export { implementParentParams };
