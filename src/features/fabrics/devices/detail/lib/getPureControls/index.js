export const getPureControls = (controls) => {
  return controls.map((tab) => tab.boxes.map((box) => box.signals).flat());
};
