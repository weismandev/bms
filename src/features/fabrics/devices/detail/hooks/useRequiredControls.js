import { useState, useEffect } from 'react';
import i18n from '@shared/config/i18n';
import { saveClicked, saveAddClicked } from '../model/controls/controls.model';

const { t } = i18n;

const useRequiredControls = (value, required, required_title) => {
  const [errors, setErrors] = useState([]);

  const resetErrors = () => setErrors([]);

  const subscribeSaveClicked = () => {
    if (required && !value) {
      setErrors([required_title || t('thisIsRequiredField')]);
    }
  };
  const subscribeSaveAddClicked = subscribeSaveClicked;

  useEffect(() => {
    const unsubscribeSave = saveClicked.watch(subscribeSaveClicked);
    return unsubscribeSave;
  }, [value]);

  useEffect(() => {
    const unsubscribeSaveAdd = saveAddClicked.watch(subscribeSaveAddClicked);
    return unsubscribeSaveAdd;
  });

  return { errors, resetErrors };
};

export { useRequiredControls };
