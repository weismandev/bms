import { deleteDevice } from '../controls';
import { changeDeleteDialog, $isOpenDeleteDialog } from './delete-dialog.model';

$isOpenDeleteDialog
  .on(changeDeleteDialog, (_, visibility) => visibility)
  .reset(deleteDevice);
