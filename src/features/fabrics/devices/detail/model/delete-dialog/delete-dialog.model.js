import { createEvent, createStore } from 'effector';

const changeDeleteDialog = createEvent();

const $isOpenDeleteDialog = createStore(false);

export { changeDeleteDialog, $isOpenDeleteDialog };
