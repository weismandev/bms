import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

export const $data = createStore({ data: null, externalContent: null });
export const $controls = createStore(null);
export const $mode = createStore('view');
export const $loading = createStore(false);
export const $action = createStore({
  save: { url: '', method: 'get' },
  dalete: { url: '', method: 'get' },
});

export const fxGetDeviceInfo = createEffect(async (url) => {
  const data = await fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

export const fxSendGroupSignal = createEffect();
export const fxRemoveDevice = createEffect();
export const fxArchiveDevice = createEffect();
export const fxGetDeviceInfoBySerialNumber = createEffect();

export const changeMode = createEvent();
export const cancelClicked = createEvent();
export const saveClicked = createEvent();
export const archiveClicked = createEvent();
export const deleteDevice = createEvent();
export const setAction = createEvent();
export const controlsProcessed = createEvent();
export const saveAddClicked = createEvent();

export const ControlsGate = createGate();
