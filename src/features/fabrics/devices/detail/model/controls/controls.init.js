import { forward, guard, sample } from 'effector';
import { pending } from 'patronum';
import { push } from '@features/common';
import { fxGetDeviceInfoByProxy } from '@features/fabrics/add/models/add-device';
import {
  openAddDetail,
  openedLinkChanged,
  changedDetailVisibility,
  setURLSearchParamsSerialNumber,
} from '@features/fabrics/devices/detail/model/detail';
import { needUpdateTable } from '@features/fabrics/table/model';
import i18n from '@shared/config/i18n';
import { sendGroupSignal, removeDevice, archiveDevice, getDeviceInfo } from '../../api';
import { controlsMapping } from '../../lib/controlsMapping';
import { fxSaveBounds } from '../bounds';
import {
  fxGetDeviceInfo,
  fxSendGroupSignal,
  fxRemoveDevice,
  fxArchiveDevice,
  fxGetDeviceInfoBySerialNumber,
  ControlsGate,
  $data,
  $mode,
  $action,
  $loading,
  changeMode,
  archiveClicked,
  setAction,
  deleteDevice,
  cancelClicked,
  $controls,
} from './controls.model';
import { fxGetSelectUrl, $nestedParents } from './nested-controls/nested-controls.model';

const { t } = i18n;

const prependCloseDetail = () => false;

fxSendGroupSignal.use(sendGroupSignal);
fxRemoveDevice.use(removeDevice);
fxArchiveDevice.use(archiveDevice);
fxGetDeviceInfoBySerialNumber.use(getDeviceInfo);

$mode
  .on(changeMode, (_, mode) => mode)
  .on(openedLinkChanged, () => 'view')
  .on(openAddDetail, () => 'edit')
  .reset(ControlsGate.close);

$loading.on(
  pending({
    effects: [
      fxSendGroupSignal,
      fxGetDeviceInfo,
      fxGetSelectUrl,
      fxRemoveDevice,
      fxGetDeviceInfoByProxy,
      fxGetDeviceInfoBySerialNumber
    ],
  }),
  (_, isLoading) => isLoading
);

$data.reset(ControlsGate.close);

$action.on(setAction, (_, action) => action).reset(ControlsGate.close);

// При маунте запрашиваем девайс (сигналы)
sample({
  source: ControlsGate.state,
  clock: guard({
    source: ControlsGate.state,
    filter: ({ url }) => url,
  }),
  fn: ({ url }) => url,
  target: fxGetDeviceInfo,
});

/* запрашиваем детали по серийнику в адресной строке */
sample({
  clock: setURLSearchParamsSerialNumber,
  target: fxGetDeviceInfoBySerialNumber
});

sample({
  clock: fxGetDeviceInfoBySerialNumber.doneData,
  fn: (data) => ({ data, externalContent: [] }),
  target: $data,
});

// Принимаем девайс и отправляем в дату
sample({
  source: ControlsGate.state,
  clock: fxGetDeviceInfo.doneData,
  fn: ({ externalContent }, { data }) => ({ data, externalContent }),
  target: $data,
});

sample({
  source: ControlsGate.state,
  clock: fxGetDeviceInfoByProxy.doneData,
  fn: ({ externalContent }, data) => ({ data, externalContent }),
  target: $data,
});

sample({
  source: $data,
  fn: (data) => controlsMapping(data),
  target: $controls,
});

// При отмене редактирования запрашиваем инфу по девайсу
guard({
  source: sample({
    source: { gate: ControlsGate.state, nested: $nestedParents },
    clock: cancelClicked,
    // Не удалять!
    // fn: ({ gate, nested }, _) => (nested?.length > 0 ? gate.url : null),
    fn: ({ gate, nested }, _) => gate.url,
  }),
  filter: (payload) => Boolean(payload),
  target: fxGetDeviceInfo,
});

// Удаление
sample({
  clock: deleteDevice,
  source: $action,
  fn: (source, clock) => source.delete,
  target: fxRemoveDevice,
});

forward({
  from: fxRemoveDevice.done,
  to: [needUpdateTable, changedDetailVisibility.prepend(prependCloseDetail)],
});

/* Сохранение */

// После сохранения запрашиваем девайс
sample({
  source: ControlsGate.state,
  clock: fxSendGroupSignal.done,
  fn: ({ url }) => url,
  target: fxGetDeviceInfo,
});

// Меняем мод на вью после сохранения
forward({
  from: [fxSendGroupSignal, fxSaveBounds],
  to: changeMode.prepend(() => 'view'),
});

forward({
  from: fxSendGroupSignal.done,
  to: needUpdateTable,
});

/* Арихивация */

// После клика архивируем
forward({
  from: archiveClicked,
  to: fxArchiveDevice,
});

// Обработка успешной архивации
forward({
  from: fxArchiveDevice.done,
  to: [
    needUpdateTable,
    changedDetailVisibility.prepend(prependCloseDetail),
    push.prepend(() => ({
      header: t('TheDeviceHasBeenSuccessfullyAddedToTheArchive'),
    })),
  ],
});
