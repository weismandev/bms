import { createEffect, createEvent, createStore } from 'effector';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { getControl } from '../../../lib/controlsMapping/getControl';

export const updateNested = createEvent();
export const parentUpdated = createEvent();
export const $nestedParents = createStore([]);
export const $unsavedChanges = createStore({});
export const addToUnsaved = createEvent();
export const applyUnsavedChanges = createEvent();
export const clearUnsavedChanges = createEvent();

export const $deviceInfo = createStore(null);

export const fxGetSelectUrl = createEffect(async (payload) => {
  if (!payload) {
    return [];
  }
  const urlPromises = payload.map(async ({ urlSelect, parentId }) => {
    const response = await fetch(urlSelect, {
      headers: {
        ...getBaseHeaders()
      }
    });
    const { data } = await response.json();
    return data.signals.map((signal) => ({ ...signal, parentId }));
  });
  const result = await Promise.all(urlPromises);
  return result[0];
});

const enrichControl = ({ parent, newSignal }) => ({
  ...parent,
  ...newSignal,
  control: {
    ...newSignal.control,
    Component: getControl(newSignal.control?.type),
  },
});

const insertNewSignal = (tab, newSignal) => {
  const { boxes } = tab;
  for (const box of boxes) {
    const parentIndex = box?.signals?.findIndex(
      (signal) => signal.id === newSignal.parentId
    );
    if (parentIndex === -1) {
      continue;
    }
    const parent = box.signals[parentIndex];
    const enrichedControl = enrichControl({ parent, newSignal });

    const dublicateIndex = box?.signals?.findIndex(
      (signal) => signal.id === newSignal.id
    );

    if (dublicateIndex !== -1) {
      box.signals[dublicateIndex] = enrichedControl;
    } else {
      box.signals.splice(parentIndex + 1, 0, enrichedControl);
    }

    return true;
  }
  return null;
};

const buildUrlWithParam = ({ url, paramName, paramValue }) => {
  if (!url || !paramValue) {
    return null;
  }
  const urlObject = new URL(url);
  const urlParams = new URLSearchParams(urlObject.search);
  if (urlParams.get(paramName)) {
    return url;
  }
  urlParams.set(paramName, paramValue);
  urlObject.search = urlParams.toString();
  return urlObject.href;
};

export const processNestedSignals = (sourceData, newSignals) => {
  if (!Array.isArray(newSignals) || newSignals.length === 0) {
    return sourceData;
  }
  newSignals.forEach((newSignal) => {
    for (const tab of sourceData) {
      const done = insertNewSignal(tab, newSignal);
      if (done) {
        break;
      }
    }
  });
  return sourceData;
};

export const getNestedSignals = ({ signals }) =>
  signals?.reduce((acc, signal) => {
    if (!signal?.control?.url_select) {
      return acc;
    }
    const urlSelect = buildUrlWithParam({
      url: signal.control?.url_select,
      paramName: signal.name || 'userdata_id',
      paramValue: signal.state?.value || '1000000',
    });

    // if (!urlSelect) {
    //   return acc;
    // }

    return [
      ...acc,
      {
        urlSelect: urlSelect || '',
        parentId: signal.id,
      },
    ];
  }, []);
