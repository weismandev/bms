import { sample, guard, createEvent } from 'effector';
import { fxGetDeviceInfoByProxy } from '@features/fabrics/add/models/add-device';
import {
  sliceCurrentTabControls,
  $slicedCurrentTabControls,
} from '../../tabs/tabs.model';
import {
  fxGetDeviceInfo,
  fxGetDeviceInfoBySerialNumber,
  $data,
  $controls,
  ControlsGate,
  controlsProcessed,
} from '../controls.model';
import {
  fxGetSelectUrl,
  getNestedSignals,
  processNestedSignals,
  $nestedParents,
  updateNested,
  $unsavedChanges,
  clearUnsavedChanges,
  addToUnsaved,
  applyUnsavedChanges,
  parentUpdated,
  $deviceInfo,
} from './nested-controls.model';

// Записываем полученный девайс
$deviceInfo
  .on(fxGetDeviceInfo.doneData, (_, { data }) => data)
  .on(fxGetDeviceInfoBySerialNumber.doneData, (_, data) => data)
  .on(fxGetDeviceInfoByProxy.doneData, (_, result) => result);

// Прокидываем девайс в парсер нестед контролов для получения url_select
sample({
  source: $deviceInfo,
  fn: (data) => getNestedSignals(data),
  target: fxGetSelectUrl,
});

// С парсера прпокидываем в преобразованные контролы
// $controls это контролы которые пришли с бека, они никак не модифицируются
// $slicedCurrentTabControls это измененные контролы
// Если при попытке обновить нестед контролы измененных нет
// то берем базовые $controls, если есть то уже измененные
sample({
  source: sample({
    source: [$controls, $slicedCurrentTabControls],
    fn: (source) => {
      if (source[1] == null) return source[0];
      else return [source[1]];
    },
  }),
  clock: fxGetSelectUrl.doneData,
  fn: processNestedSignals,
  target: controlsProcessed,
});

// Сохраняем в стор все нестед контролы
sample({
  clock: fxGetSelectUrl.doneData,
  fn: (data) => data?.map(({ parentId }) => parentId) || [],
  target: $nestedParents,
});

// При изменении контролов вызываем ивент
guard({
  source: sample({
    source: $nestedParents,
    clock: sliceCurrentTabControls,
    fn: (nestedParents, event) => (nestedParents.includes(event.id) ? event : null),
  }),
  filter: (event) => Boolean(event),
  target: parentUpdated,
});

// Прокидываем изменения в несохраненные
guard({
  source: sample({
    clock: parentUpdated,
    source: $unsavedChanges,
    fn: (unsavedChanges, event) => {
      const { id, state } = event;
      return !Object.keys(unsavedChanges).length ||
        unsavedChanges[id]?.state?.value !== state?.value
        ? event
        : null;
    },
  }),
  filter: (data) => Boolean(data),
  target: [addToUnsaved, updateNested],
});

// Форматируем несохраненные контролы
sample({
  source: $unsavedChanges,
  clock: addToUnsaved,
  fn: (sourceData, clockData) => ({ ...sourceData, [clockData.id]: clockData }),
  target: $unsavedChanges,
});

// При изменении контролов прокидываем опять в парсер контролов
sample({
  source: $slicedCurrentTabControls,
  clock: updateNested,
  fn: (slicedControls, sliceEvent) => {
    const pureSlicedControls = slicedControls.boxes.map((box) => box.signals).flat();

    const changedSignalIndex = pureSlicedControls.findIndex(
      (signal) => signal.id === sliceEvent.id
    );

    const newData = pureSlicedControls.map((control, key) => {
      if (key == changedSignalIndex) {
        return {
          ...control,
          state: { ...control.state, value: sliceEvent.state.value },
        };
      } else return control;
    });

    return getNestedSignals({ signals: newData });
  },
  target: fxGetSelectUrl,
});

// Сохраняем временно изменения
guard({
  source: sample({ source: $unsavedChanges, clock: controlsProcessed }),
  filter: (changes) => Object.keys(changes).length !== 0,
  target: applyUnsavedChanges.prepend((changes) => Object.values(changes)),
});

applyUnsavedChanges.watch((data) => {
  data?.forEach((eventPayload) => sliceCurrentTabControls(eventPayload));
  clearUnsavedChanges();
});

$unsavedChanges.reset([ControlsGate.close, clearUnsavedChanges]);
$nestedParents.reset(ControlsGate.close);
