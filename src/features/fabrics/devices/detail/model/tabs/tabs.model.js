import { createEvent, createStore } from 'effector';
import { controlsTabMapping } from '../../lib/controlsTabMapping';
import { $controls } from '../controls/controls.model';

export const $options = $controls?.map(controlsTabMapping);
export const $tabsButtons = createStore([]);
export const $currentTab = createStore(0);
export const $currentTabControls = createStore(null);
export const $slicedCurrentTabControls = createStore(null);
export const $isDirty = createStore(false);
export const $labels = createStore([]);

export const sliceCurrentTabControls = createEvent();
export const changeTab = createEvent();
export const toggleDirty = createEvent();
