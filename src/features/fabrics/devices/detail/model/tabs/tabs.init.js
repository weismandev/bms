import { sample, guard } from 'effector';
import { addSubmitted } from '@features/fabrics/add/models/add';
import { fxSaveBounds, cleanBoundsNotification, $bounds } from '../bounds';
import {
  $controls,
  $data,
  $action,
  cancelClicked,
  changeMode,
  saveClicked,
  saveAddClicked,
  fxSendGroupSignal,
  ControlsGate,
  controlsProcessed,
} from '../controls/controls.model';
import {
  $currentTab,
  $currentTabControls,
  $slicedCurrentTabControls,
  $options,
  $tabsButtons,
  $labels,
  $isDirty,
  changeTab,
  sliceCurrentTabControls,
  toggleDirty,
} from './tabs.model';

$currentTab.on(changeTab, (_, tab) => tab).reset(ControlsGate.close);
$bounds.reset(ControlsGate.close);

$slicedCurrentTabControls
  .on(sliceCurrentTabControls, formatNewSignal)
  .reset(ControlsGate.close, $controls);

$tabsButtons
  .on($data, (_, { data = {} }) => data?.meta?.tabs_buttons || [])
  .reset(ControlsGate.close);

$labels
  .on($data, (_, { data = {} }) => data?.meta?.labels || [])
  .reset(ControlsGate.close);

$slicedCurrentTabControls.watch(slicedWatcher(false));

sample({
  clock: toggleDirty,
  target: $isDirty,
});

sample({
  source: $options,
  clock: changeMode,
  fn: (options, mode) => {
    const disabled = mode === 'edit';

    return options.map((option) => ({ ...option, disabled }));
  },
  target: $options,
});

sample({
  source: $currentTabControls,
  clock: cancelClicked,
  target: [$slicedCurrentTabControls, toggleDirty.prepend(() => false)],
});

sample({
  source: $currentTab,
  clock: controlsProcessed,
  fn: (currentTab, tabs) => (tabs && currentTab in tabs ? tabs[currentTab] : {}),
  target: [$currentTabControls, $slicedCurrentTabControls],
});

sample({
  source: $controls,
  clock: changeTab,
  fn: (tabs, tab) => (tab in tabs ? tabs[tab] : {}),
  target: [$currentTabControls, $slicedCurrentTabControls],
});

sample({
  source: [$slicedCurrentTabControls, $data, $action, $tabsButtons],
  clock: guard({
    clock: saveAddClicked,
    source: $slicedCurrentTabControls,
    filter: hasPureControls,
  }),
  fn: (source) => ({
    boxes: source[0].boxes,
    url: source[2].save,
  }),
  target: addSubmitted,
});

sample({
  source: [$slicedCurrentTabControls, $data, $action],
  clock: guard({
    clock: saveClicked,
    source: $slicedCurrentTabControls,
    filter: hasPureControls,
  }),
  fn: processNewSignalsToCommand,
  target: fxSendGroupSignal,
});

guard({
  source: sample({
    source: [$bounds, $slicedCurrentTabControls],
    clock: saveClicked,
    fn: processSignalsNotification,
  }),
  filter: (bounds) => Object.keys(bounds).length > 0,
  target: [fxSaveBounds.prepend(payloadNotification), cleanBoundsNotification],
});

function processSignalsNotification([bounds, tab]) {
  const boundIds = getModifiedControls(tab, getBoundsInBox);
  const payload = Object.keys(bounds)
    .filter((signal_id) => boundIds.includes(Number(signal_id)))
    .reduce((acc, signal_id) => {
      const editedBounds = bounds[signal_id].data
        .filter(({ isDirty }) => isDirty)
        .map(({ isDirty, ...bound }) => bound);

      return editedBounds.length !== 0
        ? {
            ...acc,
            [signal_id]: acc[signal_id]
              ? acc[signal_id].concat(editedBounds)
              : [].concat(editedBounds),
          }
        : acc;
    }, {});
  return payload;
}

function payloadNotification(bounds) {
  const payload = Object.keys(bounds).reduce(
    (acc, signal_id) =>
      acc.concat(
        bounds[signal_id].map(({ conditions, ...bound }) => ({
          ...bound,
          signal_id,
        }))
      ),
    []
  );
  return JSON.stringify(payload);
}

function resetDopParams(name) {
  const dopNames = [name];
  return function bypass(signals) {
    return signals.map((signal) => {
      const dop_params = signal?.control?.dop_params?.split(',') || [];
      if (dop_params.length > 0 && dopNames.some((item) => dop_params.includes(item))) {
        dopNames.concat(dop_params);
        return {
          ...signal,
          state: {
            ...signal.state,
            value: '',
          },
        };
      }
      return signal;
    });
  };
}

function formatNewSignal(tab, new_signal) {
  const currentViewBox = tab?.boxes?.find((box) => box.id === new_signal.boxId);
  if (!currentViewBox) return tab;
  const currentViewBoxNewSignals = currentViewBox.signals.map((signal) => {
    if (signal.name === new_signal.name) {
      return new_signal;
    }
    return signal;
  });

  const bypass = resetDopParams(new_signal.name);
  const newCurrentViewBox = {
    ...currentViewBox,
    signals: bypass(currentViewBoxNewSignals),
  };

  const newBoxes = tab.boxes.map((box) => {
    if (box.id === newCurrentViewBox.id) {
      return newCurrentViewBox;
    }

    return box;
  });

  const newTab = {
    ...tab,
    boxes: newBoxes,
  };

  return newTab;
}

function processNewSignalsToCommand([tab, { data }, action]) {
  if (!Array.isArray(tab?.boxes)) return [];

  const serialnumber = data?.meta?.serialnumber;

  const modifiedControls = getModifiedControls(tab, getModifiedControlsInBox);
  const command = buildCommand(modifiedControls, serialnumber);

  return {
    command,
    url: action?.save?.url,
    method: action?.save?.method || 'GET',
  };
}

function hasPureControls(tab) {
  const modifiedControls = getModifiedControls(tab, getModifiedControlsInBox);
  const erroredControls = getModifiedControls(tab, getErrorsControlsInBox);
  const requiredControls = getModifiedControls(tab, getRequiredControlsInBox);

  return modifiedControls.length && !erroredControls.length && !requiredControls.length;
}

function getRequiredControlsInBox(signals) {
  return signals.filter((signal) => signal?.required && !signal?.state.value);
}

function getModifiedControlsInBox(signals) {
  // !!! Dirty hack. Обход логики по обработке только измененных контролов !!!
  // return signals.filter((signal) => Boolean(signal));
  return signals.filter((signal) => signal?.modified);
}

function getErrorsControlsInBox(signals) {
  return signals.filter((signal) => signal?.errors);
}

function getBoundsInBox(signals) {
  return signals.filter((signal) => signal?.bounds).map((bound) => bound.id);
}

function getModifiedControls(tab, callback) {
  return tab?.boxes.reduce((acc, box) => {
    if (!Array.isArray(box?.signals)) return acc;

    const modifiedControlsInBox = callback(box.signals);

    return [...acc, ...modifiedControlsInBox];
  }, []);
}

function buildCommand(controls, serialnumber) {
  const command = controls.reduce((acc, control) => {
    const controlCommand = `${serialnumber}:${control.name}:${control.state.value}`;

    return acc + controlCommand + ';';
  }, '');

  return command.slice(0, -1);
}

function slicedWatcher(isDirty = false) {
  return function (tab) {
    if (tab) {
      const modifiedControls = getModifiedControls(tab, getModifiedControlsInBox);
      const isModified = modifiedControls.length > 0;
      if (isDirty !== isModified) {
        isDirty = isModified;
        toggleDirty(isDirty);
      }
    }
  };
}
