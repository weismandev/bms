import { getSignalNotification, saveBounds } from '../../api/signal_notifications';
import { cancelClicked } from '../controls/controls.model';
import {
  fxSaveBounds,
  fxGetSignalsNotificationByUrl,
  $bounds,
  sliceBoundsNotification,
  cleanBoundsNotification,
  setLoading,
} from './bounds.model';

fxGetSignalsNotificationByUrl.use(getSignalNotification);
fxSaveBounds.use(saveBounds);

$bounds
  .on(setLoading, (state, signalId) => ({
    ...state,
    [signalId]: {
      ...(state[signalId] ? state[signalId] : {}),
      isLoading: true,
    },
  }))
  .on(fxGetSignalsNotificationByUrl.done, (state, { params, result }) => {
    const signalId = params.split('signal_id=')[1];
    return {
      ...state,
      [signalId]: {
        data: result.data,
        isLoading: false,
      },
    };
  })
  .on(sliceBoundsNotification, (state, { signalId, sliced }) => ({
    ...state,
    [signalId]: {
      ...state[signalId],
      data: state[signalId].data.map((item) =>
        item.signal_notification_level_id === sliced.signal_notification_level_id
          ? { ...item, ...sliced }
          : item
      ),
    },
  }))
  .on(cleanBoundsNotification, (state, bounds) => ({
    ...state,
    ...Object.keys(bounds).reduce(
      (acc, signalId) => ({
        ...acc,
        [signalId]: {
          ...state[signalId],
          isLoading: true,
          data: state[signalId].data.map(({ isDirty, ...bound }) => bound),
        },
      }),
      {}
    ),
  }))
  .on(fxSaveBounds.done, (state, { result }) => {
    const signals = JSON.parse(result.signals) || [];
    return {
      ...state,
      ...signals.reduce(
        (acc, signalId) => ({
          ...acc,
          [signalId]: { ...state[signalId], isLoading: false },
        }),
        {}
      ),
    };
  })
  .reset(cancelClicked);
