import { createStore, createEffect, createEvent } from 'effector';

export const $bounds = createStore({});

export const sliceBoundsNotification = createEvent();
export const cleanBoundsNotification = createEvent();
export const setLoading = createEvent();

export const fxSaveBounds = createEffect();
export const fxGetSignalsNotificationByUrl = createEffect();
