import { signout } from '@features/common';
import { instructionUnmounted } from '@features/fabrics/processes/instructions/models/instructions';
import { fxAddDevice } from '../../../../add/models/add';
import { fxRemoveDevice } from '../controls/controls.model';
import {
  changedErrorModalVisibility,
  $isOpenModalAddError,
  $errorText,
} from './error-modal.model';

$isOpenModalAddError
  .on([fxAddDevice.failData, fxRemoveDevice.failData], () => true)
  .on(changedErrorModalVisibility, (_, visibility) => visibility)
  .reset([instructionUnmounted, signout, fxAddDevice, fxRemoveDevice]);

$errorText
  .on(fxAddDevice.fail, (_, { error }) => error.message)
  .on(fxRemoveDevice.fail, (_, { error }) => error.message)
  .reset([instructionUnmounted, signout, fxAddDevice, fxRemoveDevice]);
