import { createStore, createEvent } from 'effector';

const changedErrorModalVisibility = createEvent();

const $isOpenModalAddError = createStore(false);
const $errorText = createStore('');

export { changedErrorModalVisibility, $isOpenModalAddError, $errorText };
