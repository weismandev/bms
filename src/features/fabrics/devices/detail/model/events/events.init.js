import { sample } from 'effector';
import { $timezone } from '@features/common/model/timezone';
import { EventsGate, fxGetEvents, $raw, $eventLoading, pageChanged } from './event.model';

const timezone = $timezone.stateRef.current;

$raw.on(fxGetEvents.doneData, (_, { data }) => data);

$eventLoading.on(fxGetEvents.pending, (_, pending) => pending);

sample({
  source: EventsGate.state,
  fn: (url, page) => url + `&timezone=${timezone}`,
  target: fxGetEvents,
});

sample({
  source: EventsGate.state,
  clock: pageChanged,
  fn: (url, page) => {
    const offset = 20 * page - 20;
    return url + `&timezone=${timezone}&offset=${offset}`;
  },
  target: fxGetEvents,
});
