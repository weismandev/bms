import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const $raw = createStore({ meta: {}, events: [] });
export const $events = $raw.map(({ meta, events }) => {
  if (!events?.length) return { meta: {}, events: [] };

  return events.map((event) => ({
    ...event,
    icon: meta?.img_url ? `${meta.img_url}${event.icon}` : '',
    dt: {
      create: `${event?.dt?.date.split('-').reverse().join('.')} ${t('in')} ${event?.dt?.time.slice(0, -3)}}`
    },
    title: event?.title || '',
    type_title: event?.type_title || '',
    videoarchive: event?.videoarchive || '',
    photo: event?.photo || '',
    initiator: event?.initiator?.title || '',
  }));
});
export const $eventLoading = createStore(false);
export const $meta = $raw.map(({ meta }) => {
  const count = Math.ceil(meta.total / meta.limit) || 0;
  const page = meta.offset ? meta.offset / 20 + 1 : 1;

  return {
    count,
    page,
  };
});

export const fxGetEvents = createEffect(async (url) => {
  const data = await fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

export const pageChanged = createEvent();

export const EventsGate = createGate();
