import { guard, sample } from 'effector';
import { signout } from '@features/common';
import { $pathname, historyPush } from '@features/common';
import { fxRemoveDevice } from '@features/fabrics/devices/detail/model/controls/controls.model';
import instructionObserver from '../../../../processes/instructionObserver';
import {
  InstructionGate,
  instructionUnmounted,
} from '../../../../processes/instructions/models/instructions';
import {
  addClicked,
  changedDetailVisibility,
  detailSubmitted,
  $isOpenDetail,
  $urlAddDevice,
  fxGetDeviceAddNextStep,
  $urlForAddDevice,
  fxAddDevice,
  openedLinkChanged,
  $openedLink,
  $instructions,
  setInstructionsDetail,
  addModeChanged,
  $isAddMode,
  openAddDetail,
  setURLSearchParamsSerialNumber,
} from './detail.model';

InstructionGate.open.watch((state) => {
  const config = {
    page: state.page,
    path: 'filter.top',
  };
  instructionObserver.subscribe(config, setInstructionsDetail);
});

$instructions
  .on(setInstructionsDetail, (_, result) => result)
  .reset([instructionUnmounted, signout]);

$isAddMode
  .on(addModeChanged, (_, mode) => mode)
  .on([addClicked, openAddDetail], () => true)
  .on(openedLinkChanged, () => false)
  .reset([instructionUnmounted, signout]);

$openedLink
  .on(
    [openedLinkChanged, openAddDetail, setURLSearchParamsSerialNumber],
    (_, serialNumber) => serialNumber)
  .reset([signout, addClicked, fxRemoveDevice.done, instructionUnmounted]);

$isOpenDetail
  .on([openedLinkChanged, openAddDetail, addClicked, setURLSearchParamsSerialNumber], () => true)
  .on(fxAddDevice, () => false)
  .on(changedDetailVisibility, (_, visibility) => visibility)
  .reset([instructionUnmounted, signout]);

$urlForAddDevice
  .on(fxGetDeviceAddNextStep.done, (_, { result }) => result?.data?.next_step)
  .reset([instructionUnmounted, signout]);
guard({
  source: $urlAddDevice,
  filter: (url) => url,
  target: fxGetDeviceAddNextStep,
});

sample({
  clock: detailSubmitted,
  source: $urlForAddDevice,
  fn: (url, { name, type }) => ({
    url,
    payload: { title: name, serialnumber: type.id },
  }),
  target: fxAddDevice,
});

/* при закрытии деталей, удаляются параметры из адресной строки */
sample({
  clock: changedDetailVisibility,
  source: $pathname,
  filter: (_, visibility) => !visibility,
  fn: (_, pathname) => pathname,
  target: historyPush
});
