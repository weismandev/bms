import { createEvent, createStore, createEffect, restore } from 'effector';
import { createGate } from 'effector-react';
import { getCookie } from '@tools/cookie';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

const DetailGate = createGate();

const fxGetDeviceAddNextStep = createEffect(async (url) => {
  const data = await fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

const fxAddDevice = createEffect(async ({ url, payload }) => {
  const { title, serialnumber } = payload;
  const token = getCookie('bms_token');

  const request = `${url}&title=${title}&serialnumber=${serialnumber}&token=${token}`;

  const data = await fetch(request, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

const addClicked = createEvent();
const changedDetailVisibility = createEvent();
const detailSubmitted = createEvent();
const openedLinkChanged = createEvent();
const instructionsReady = createEvent();
const setInstructionsDetail = createEvent();
const addModeChanged = createEvent();
const openAddDetail = createEvent();
const setURLSearchParamsSerialNumber = createEvent();
const closeDetail = createEvent();

const $instructions = createStore({});
const $isAddMode = createStore(false);
const $isOpenDetail = createStore(false);
const $openedLink = createStore(null);
const $URLSearchParamsSerialNumber = restore(setURLSearchParamsSerialNumber, null);

// const $urlAddDevice = $accessKeyInstructions.map((instuctions) =>
//   instuctions ? getAddDeviceUrl(instuctions) : {}
// );
const $urlAddDevice = {};
const $urlForAddDevice = createStore(null);

export {
  DetailGate,
  openedLinkChanged,
  $openedLink,
  addClicked,
  changedDetailVisibility,
  detailSubmitted,
  $isOpenDetail,
  $urlAddDevice,
  fxGetDeviceAddNextStep,
  $urlForAddDevice,
  fxAddDevice,
  $instructions,
  instructionsReady,
  setInstructionsDetail,
  addModeChanged,
  $isAddMode,
  $URLSearchParamsSerialNumber,
  setURLSearchParamsSerialNumber,
  openAddDetail,
  closeDetail
};
