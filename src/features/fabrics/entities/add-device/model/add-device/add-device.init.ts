import { sample } from 'effector';

import { addDeviceMounted } from './add-device.model';
import { fxGetDeviceTypes } from '../device-types';

/* при открытии карточки добавления устройства, запрашиваются типы устройств */
sample({
  clock: addDeviceMounted,
  target: fxGetDeviceTypes
});
