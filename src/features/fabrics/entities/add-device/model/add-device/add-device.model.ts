import { createGate } from 'effector-react';

export const AddDeviceGate = createGate();

export const addDeviceUnmounted = AddDeviceGate.close;
export const addDeviceMounted = AddDeviceGate.open;
