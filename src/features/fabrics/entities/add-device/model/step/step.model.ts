import { createStore, createEvent } from 'effector';

import type { Step } from './step.types';

export const setActiveStep = createEvent<Step>();
export const $activeStep = createStore<Step>(0);
