import { Event } from 'effector';
import { openedLinkChanged, closeDetail } from '@features/fabrics/devices/detail/model/detail';
import { setActiveStep, $activeStep } from './step.model';

$activeStep
  .on(setActiveStep, (_, step) => step)
  .reset([openedLinkChanged, closeDetail as Event<void>]);
