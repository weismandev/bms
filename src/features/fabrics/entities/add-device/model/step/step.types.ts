export enum Step {
  StepSelectionDeviceType = 0,
  StepSelectionDevice = 1,
  StepFillingForm = 2,
  StepDone = 3
}
