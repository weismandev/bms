import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { getCookieNameWithBuildType } from '@tools/cookie';

import type { DeviceTypeInstruction } from '../device-types';
import type { GetDeviceListResponse, Device } from './device-list.types';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const DeviceListGate = createGate<DeviceTypeInstruction | null>();

export const fxGetDeviceList = createEffect<
  string,
  GetDeviceListResponse,
  Error
>(async (url: string) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName) as string;
  const res = await fetch(`${url}&token=${token}`);
  const data = res.json();
  return data;
});

export const getDeviceList = createEvent<DeviceTypeInstruction>();
export const setSelectedDevice = createEvent<Device>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<Error | null>(null);
export const $nextStepUrl = createStore<string | null>(null);
export const $deviceList = createStore<Device[]>([]);
export const $selectedDevice = createStore<Device | null>(null);
