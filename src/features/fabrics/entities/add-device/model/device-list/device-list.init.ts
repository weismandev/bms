import { sample } from 'effector';
import type { DeviceTypeInstruction } from '../device-types';
import { DeviceListGate, fxGetDeviceList, getDeviceList } from './device-list.model';

sample({
  clock: DeviceListGate.state,
  filter: (instruction): boolean => instruction?.type === 'default' || instruction?.type === 'dynamic',
  fn: (instruction) => instruction as DeviceTypeInstruction,
  target: getDeviceList
});

sample({
  clock: getDeviceList,
  fn: (instruction) => instruction.next_step,
  target: fxGetDeviceList
});
