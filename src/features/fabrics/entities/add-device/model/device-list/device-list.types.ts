import type { Signal } from '../dynamic';

export type Device = {
  model_id: string;
  serial: string;
  signals: Signal[] | null;
  title: string;
};

export type GetDeviceListResponse = {
  data: {
    devices: Device[];
    next_step: string;
  };
  error: number;
  message: string;
};
