import { Event } from 'effector';
import { openedLinkChanged, closeDetail } from '@features/fabrics/devices/detail/model/detail';
import {
  DeviceListGate,
  fxGetDeviceList,
  setSelectedDevice,
  $error,
  $isLoading,
  $deviceList,
  $nextStepUrl,
  $selectedDevice
} from './device-list.model';

const reset = [openedLinkChanged, DeviceListGate.state, closeDetail];

$isLoading
  .on(fxGetDeviceList.pending, (_, pending) => pending)
  .reset(reset);

$error
  .on(fxGetDeviceList.done, (_, { result }) => (
    result.error > 0 ? Error(result.message) : null
  ))
  .reset(reset);

$nextStepUrl
  .on(fxGetDeviceList.done, (_, { result }) => result.data.next_step)
  .reset(reset);

$deviceList
  .on(fxGetDeviceList.done, (_, { result }) => result.data.devices)
  .reset(reset);

$selectedDevice
  .on(setSelectedDevice, (_, device) => device)
  .reset(reset);
