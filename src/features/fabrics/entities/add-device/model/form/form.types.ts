export type FormType = 'qr' | 'logopass' | 'oauth' | 'dynamic' | 'default';

export type SubmitFormResponse = {
  error: number;
  message: string;
};
