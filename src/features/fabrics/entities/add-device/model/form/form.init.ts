import { sample } from 'effector';

import { setSelectedDeviceType } from '../device-types';
import { fxSubmitForm, setFormType } from './form.model';
import { setActiveStep } from '../step';

sample({
  clock: setSelectedDeviceType,
  fn: (deviceType) => deviceType.instruction.type,
  target: setFormType
});

sample({
  clock: fxSubmitForm.done,
  filter: ({ result }) => result.error === 0 && result.message.length === 0,
  fn: () => 3,
  target: setActiveStep
});
