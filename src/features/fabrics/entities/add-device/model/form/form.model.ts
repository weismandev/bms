import { createStore, createEvent, createEffect } from 'effector';
import { getCookieNameWithBuildType } from '@tools/cookie';

import type { SubmitFormResponse, FormType } from './form.types';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

export const fxSubmitForm = createEffect<
  string,
  SubmitFormResponse,
  Error
>(async (url: string) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName) as string;
  const res = await fetch(`${url}&token=${token}`);
  const data = res.json();
  return data;
});

export const setFormType = createEvent<FormType>();

export const $formType = createStore<FormType>('default');
export const $isLoading = createStore<boolean>(false);
export const $error = createStore<Error | null>(null);
