import { openedLinkChanged, closeDetail } from '@features/fabrics/devices/detail/model/detail';

import { fxSubmitForm, $formType, $isLoading, $error, setFormType } from './form.model';

const reset = [openedLinkChanged, closeDetail];

$formType
  .on(setFormType, (_, formType) => formType)
  .reset(reset);

$isLoading
  .on(fxSubmitForm.pending, (_, pending) => pending)
  .reset(reset);

$error
  .on(fxSubmitForm.done, (_, { result }) => (
    (result.error > 0 || result.message.length > 0) ? Error(result.message ?? '') : null
  ))
  .reset(reset);
