import { sample } from 'effector';
import { $nextStepUrl } from '../device-list';
import { fxSubmitForm } from '../form';
import { submitDefaultForm } from './default-form.model';

sample({
  clock: submitDefaultForm,
  source: $nextStepUrl,
  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
  fn: (url, payload): string => `${url}&${new URLSearchParams(payload)}`,
  target: fxSubmitForm
});
