import { createEvent } from 'effector';

import type { Values } from './default-form.types';

export const submitDefaultForm = createEvent<Values>();
