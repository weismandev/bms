export type Values = {
  title: string;
  building_id: number | null;
  room_id: number | null;
};
