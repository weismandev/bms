import { Event } from 'effector';
import { openedLinkChanged, closeDetail } from '@features/fabrics/devices/detail/model/detail';

import {
  fxGetDeviceTypes,
  $deviceTypes,
  $isLoadingDeviceTypes,
  $error,
  $selectedDeviceType,
  setSelectedDeviceType,
} from './device-types.model';

const reset = [openedLinkChanged, closeDetail];

$deviceTypes
  .on(fxGetDeviceTypes.done, (_, { result }) => result.types)
  .reset(reset);

$isLoadingDeviceTypes
  .on(fxGetDeviceTypes.pending, (_, pending) => pending)
  .reset(reset);

$error
  .on(fxGetDeviceTypes.fail, (_, error) => error.params)
  .reset(reset);

$selectedDeviceType
  .on(setSelectedDeviceType, (_, deviceType) => deviceType)
  .reset(reset);
