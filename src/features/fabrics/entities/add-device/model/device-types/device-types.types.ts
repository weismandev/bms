export type DeviceTypeInstructionType = 'qr' | 'logopass' | 'oauth' | 'dynamic' | 'default';

export type DeviceTypeInstruction = {
  next_step: string;
  skip_available: boolean;
  title: string;
  type: DeviceTypeInstructionType;
};

export type DeviceType = {
  name: string;
  title: string;
  instruction: DeviceTypeInstruction;
};

export type GetDeviceTypesResponse = {
  types: DeviceType[];
};
