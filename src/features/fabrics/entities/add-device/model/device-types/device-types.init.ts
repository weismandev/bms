import { sample } from 'effector';
import { getDeviceTypes } from '../../api';

import {
  fxGetDeviceTypes,
} from './device-types.model';

fxGetDeviceTypes.use(getDeviceTypes);
