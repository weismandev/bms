import { createEffect, createStore, createEvent } from 'effector';

import { DeviceType, GetDeviceTypesResponse } from './device-types.types';

export const fxGetDeviceTypes = createEffect<void, GetDeviceTypesResponse, Error>();

export const $isLoadingDeviceTypes = createStore<boolean>(false);
export const $error = createStore<Error | null>(null);
export const $deviceTypes = createStore<DeviceType[]>([]);
export const $selectedDeviceType = createStore<DeviceType | null>(null);

export const setSelectedDeviceType = createEvent<DeviceType>();
