import { sample } from 'effector';
import { getControl } from '../../../../devices/detail/lib/controlsMapping/getControl';
import { $nextStepUrl } from '../device-list';
import { fxSubmitForm } from '../form';
import { $signals, setSignal, updateSignal, saveDynamicForm } from './dynamic.model';

sample({
  clock: setSignal,
  fn: (signals) => signals.map((signal) => ({
    ...signal,
    control: {
      ...signal.control,
      Component: getControl(signal.control.type)
    }
  })),
  target: $signals
});

sample({
  clock: updateSignal,
  source: $signals,
  fn: (signals, { name, value }) => signals.map((signal) => {
    if (signal.name === name) {
      return {
        ...signal,
        state: {
          ...signal.state,
          value
        }
      };
    }
    return signal;
  }),
  target: $signals
});

sample({
  clock: saveDynamicForm,
  source: [$nextStepUrl, $signals],
  fn: ([url, signals], serialnumber) => {
    const params = {
      serialnumber,
      ...signals?.reduce((acc, curr) => ({
        ...acc,
        [curr.name]: curr.state.value
      }), {})
    };
    return `${url}&${new URLSearchParams(params)}`;
  },
  target: fxSubmitForm
});
