import { api } from '@api/api2';

import type { GetDeviceTypesResponse } from '../model/device-types';

const getDeviceTypes = (): Promise<GetDeviceTypesResponse> =>
  api.no_vers('get', 'devices/get-device-type');

export {
  getDeviceTypes
};
