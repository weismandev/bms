import { InputField, ActionButton } from '@ui/index';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { HouseSelectField } from '@features/house-select';
import { RoomsSelectField, changeBuilding } from '@features/rooms-select';
import i18n from '@shared/config/i18n';

import type { Values } from '../../model/default-form';

const { t } = i18n;

const YupObject = Yup.object().nullable().required(t('thisIsRequiredField') as string);
const YupString = Yup.string().required(t('thisIsRequiredField') as string);

const validationSchema = Yup.object().shape({
  building_id: YupObject,
  title: YupString,
});

type Props = {
  initialValues: {
    title: string;
    serialnumber: string;
  };
  onSubmit: (values: Values) => void;
};

type FormikValue = {
  id: number;
  title: string;
};

type FormikValues = {
  title: string;

  building_id: FormikValue | null;
  room_id: FormikValue | null
};

type SetFieldValue = (field: keyof Values, value: any) => void;

export const DefaultForm = (props: Props) => {
  const { initialValues, onSubmit } = props;

  const handleSubmit = (values: FormikValues) => {
    onSubmit({
      ...values,
      building_id: values?.building_id?.id ?? null,
      room_id: values?.room_id?.id ?? null,
    });
  };

  const handleChangeBuilding = (setFieldValue: SetFieldValue) => (value: FormikValue) => {
    changeBuilding(value ? value.id : '');
    setFieldValue('building_id', value);
  };

  return (
    <div>
      <Formik
        initialValues={{
          ...initialValues,
          building_id: null,
          room_id: null,
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, setFieldValue }) => (
          <Form style={{ paddingTop: 24 }}>
            <Field
              name="title"
              label={t('NameTitle')}
              placeholder={t('EnterDeviceName')}
              component={InputField}
            />
            <Field
              name="building_id"
              label={t('Label.address')}
              placeholder={t('ChooseAddress')}
              component={HouseSelectField}
              onChange={handleChangeBuilding(setFieldValue)}
              required
            />
            <Field
              name="room_id"
              label={t('DevicePremises')}
              placeholder={t('ChooseRoom')}
              component={RoomsSelectField}
              isDisabled={!values.building_id}
            />
            <ActionButton kind="basic" type="submit">
              {t('AddDevice')}
            </ActionButton>
          </Form>
        )}
      />
    </div>
  );
};
