import { useMemo } from 'react';
import { useUnit } from 'effector-react';
import { Loader } from '@ui/index';

import { $formType, $isLoading } from '../../model/form';
import { $selectedDevice } from '../../model/device-list';
import { submitDefaultForm } from '../../model/default-form';

import { DefaultForm } from './default-form';
import { SignalsForm } from './signals-form';

import { StyledForm } from './styled';

export const Form = () => {
  const [formType, isLoading] = useUnit([$formType, $isLoading]);
  const selectedDevice = useUnit($selectedDevice);

  const renderDeviceForm = useMemo(
    () => (
      <DefaultForm
        initialValues={{
          title: selectedDevice?.title ?? '',
          serialnumber: selectedDevice?.serial ?? ''
        }}
        onSubmit={submitDefaultForm}
      />
    ),
    [selectedDevice]
  );

  const renderSignalsForm = useMemo(
    () => (
      <SignalsForm
        serialnumber={String(selectedDevice?.serial)}
      />
    ),
    [selectedDevice]
  );

  const forms = {
    default: renderDeviceForm,
    dynamic: renderSignalsForm
  };

  return (
    <StyledForm>
      <Loader isLoading={isLoading} />
      {forms[formType]}
    </StyledForm>
  );
};
