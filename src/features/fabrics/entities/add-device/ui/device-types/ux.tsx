import { useUnit } from 'effector-react';
import { List, ListItemButton, ListItemText } from '@mui/material';

import {
  $deviceTypes,
  $selectedDeviceType,
  setSelectedDeviceType,
  DeviceType
} from '../../model/device-types';

type Props = {
  handleNext: () => void;
  setDeviceTypeTitle: (title: string) => void;
};

export const DeviceTypes = ({ handleNext, setDeviceTypeTitle }: Props) => {
  const [deviceTypes, selectedDeviceType] = useUnit([$deviceTypes, $selectedDeviceType]);

  const onClickDeviceType = (deviceType: DeviceType) => () => {
    setDeviceTypeTitle(deviceType.title);
    setSelectedDeviceType(deviceType);

    switch (deviceType.instruction.type) {
      case 'default':
      case 'dynamic':
        handleNext();
        break;
      default:
    }
  };

  return (
    <List>
      {deviceTypes.map((deviceType) => (
        <ListItemButton
          key={deviceType.name}
          onClick={onClickDeviceType(deviceType)}
          selected={selectedDeviceType?.name === deviceType.name}
        >
          <ListItemText primary={deviceType.title} />
        </ListItemButton>
      ))}
    </List>
  );
};
