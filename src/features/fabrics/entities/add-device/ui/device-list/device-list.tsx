import { useUnit, useGate } from 'effector-react';
import { List, ListItemButton, ListItemText } from '@mui/material';
import { Loader } from '@ui/index';
import { DeviceListGate, $isLoading, $deviceList, $selectedDevice, setSelectedDevice } from '../../model/device-list';
import { setSignal } from '../../model/dynamic';
import { setActiveStep } from '../../model/step';

import type { DeviceTypeInstruction } from '../../model/device-types';
import type { Device } from '../../model/device-list';

type Props = {
  instruction: DeviceTypeInstruction | undefined
};

export const DeviceList = ({ instruction }: Props) => {
  const [
    isLoading,
    deviceList,
    selectedDevice
  ] = useUnit([$isLoading, $deviceList, $selectedDevice]);

  useGate(DeviceListGate, instruction);

  const onClickDevice = (device: Device) => () => {
    setSelectedDevice(device);
    setActiveStep(2);

    if (instruction?.type === 'dynamic') {
      if (device.signals !== null) {
        setSignal(device.signals);
      }
    }
  };

  return (
    <>
      <Loader isLoading={isLoading} />
      <List>
        {deviceList.map((device) => (
          <ListItemButton
            key={device.serial}
            selected={selectedDevice?.serial === device.serial}
            onClick={onClickDevice(device)}
          >
            <ListItemText primary={device.title} />
          </ListItemButton>
        ))}
      </List>
    </>
  );
};
