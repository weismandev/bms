import styled from '@emotion/styled';
import List from '@mui/material/List';

export const StyledList = styled(List)(() => ({
  width: '100%'
}));

export const StyledSuccess = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '24px'
}));
