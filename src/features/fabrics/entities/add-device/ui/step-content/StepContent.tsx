import { Wrapper, CustomScrollbar } from '@ui/index';

type Props = {
  children: React.ReactNode;
  index: number;
  value: number;
};

export const StepContent = ({ children, index, value }: Props) => (
  <div
    hidden={value !== index}
    id={`step-content-${index}`}
    style={{ height: 'calc(100vh - 220px)' }}
  >
    <CustomScrollbar>
      <div>{children}</div>
    </CustomScrollbar>
  </div>
);
