import { useState, useMemo } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Typography from '@mui/material/Typography';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { Loader } from '@ui/index';

import { AddDeviceGate } from '../model/add-device';
import { $activeStep, setActiveStep } from '../model/step';
import {
  $isLoadingDeviceTypes,
  $selectedDeviceType,
} from '../model/device-types';
import { $selectedDevice, $error } from '../model/device-list';
import { $error as $errorSubmitedForm } from '../model/form';

import { StepContent } from './step-content';
import { DeviceTypes } from './device-types';
import { DeviceList } from './device-list';
import { Form } from './form';

import { StyledSuccess } from './styled';

export const AddDevice = () => {
  const { t } = useTranslation();
  const activeStep = useUnit($activeStep);
  const [
    selectedDeviceType,
    isLoadingDeviceTypes
  ] = useUnit([
    $selectedDeviceType,
    $isLoadingDeviceTypes
  ]);
  const [selectedDevice, error] = useUnit([$selectedDevice, $error]);
  const errorSubmitedForm = useUnit($errorSubmitedForm);

  const [skipped, setSkipped] = useState(new Set<number>());
  const [deviceTypeTitle, setDeviceTypeTitle] = useState<string>('');

  const isStepSkipped = (step: number) => skipped.has(step);

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep(activeStep + 1);
    setSkipped(newSkipped);
  };

  const steps = useMemo(
    () => [t('SelectingDeviceType'), t('DeviceSelection'), t('FillingOutTheForm')], []
  );

  return (
    <>
      <AddDeviceGate />
      <Box sx={{ width: '100%' }}>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps: { completed?: boolean } = {};
            const labelProps: {
              optional?: React.ReactNode;
              error?: boolean;
            } = {};

            if (index === 0) {
              labelProps.optional = (
                <Typography variant="caption">{deviceTypeTitle}</Typography>
              );
            }
            if (index === 1) {
              if (error instanceof Error) {
                labelProps.optional = (
                  <Typography variant="caption" color="error">
                    {error.message}
                  </Typography>
                );
                labelProps.error = true;
              } else {
                labelProps.optional = (
                  <Typography variant="caption">
                    {selectedDevice?.title ?? t('DependingOnCategory')}
                  </Typography>
                );
                labelProps.error = false;
              }
            }
            if (index === 2) {
              if (errorSubmitedForm instanceof Error) {
                labelProps.optional = (
                  <Typography variant="caption" color="error">
                    {errorSubmitedForm.message}
                  </Typography>
                );
                labelProps.error = true;
              } else {
                labelProps.optional = <div />;
                labelProps.error = false;
              }
            }
            if (isStepSkipped(index)) {
              stepProps.completed = false;
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>

        <Loader isLoading={isLoadingDeviceTypes} />

        <StepContent index={0} value={activeStep}>
          <DeviceTypes
            handleNext={handleNext}
            setDeviceTypeTitle={setDeviceTypeTitle}
          />
        </StepContent>

        <StepContent index={1} value={activeStep}>
          <DeviceList instruction={selectedDeviceType?.instruction} />
        </StepContent>

        <StepContent index={2} value={activeStep}>
          <Form />
        </StepContent>

        <StepContent index={3} value={activeStep}>
          <StyledSuccess>
            <CheckCircleOutlineIcon fontSize="large" color="success" />
            <Typography sx={{ mt: 2, mb: 1 }}>
              {t('DeviceAdded')}
            </Typography>
          </StyledSuccess>
        </StepContent>
      </Box>
    </>
  );
};
