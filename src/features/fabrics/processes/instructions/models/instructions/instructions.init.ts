import { guard } from 'effector';
import observer from '../../../instructionObserver';
import { getInstructions } from '../../api';
import { InstructionGate, fxGetInstructions, $raw } from './instructions.model';

fxGetInstructions.use(getInstructions);

$raw.on(fxGetInstructions.done, (state, { params: { page }, result }) => {
  observer.fire(page, result);
  return {
    ...state,
    [page]: result,
  };
});

guard({
  clock: InstructionGate.state,
  filter: (params: any) => params.page,
  target: fxGetInstructions,
});
