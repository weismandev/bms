import { createEffect, createStore, createEvent, merge, split } from 'effector';
import { createGate } from 'effector-react';
import { Page } from '@features/fabrics/shared/types';
import { InstructionsPayload, InstructionsResponse } from '../../types';

const InstructionGate = createGate<InstructionsPayload>();
const instructionUnmounted = InstructionGate.close;
const instructionMounted = InstructionGate.open;

const fxGetInstructions = createEffect<
  InstructionsPayload,
  InstructionsResponse,
  Error
>();

const $raw = createStore<{ [key in Page]: InstructionsResponse | [] } | {}>({});

export {
  InstructionGate,
  instructionUnmounted,
  instructionMounted,
  fxGetInstructions,
  $raw,
};
