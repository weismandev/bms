import { Page } from '@features/fabrics/shared/types';

export type InstructionsResponse = {
  filter: {
    top: Object[];
    main: Object[];
    bottom: Object[];
  };
  meta: {
    title: string;
  };
  table: {
    count: number;
    meta: Column[];
    data: Row[];
  };
};

export type InstructionsPayload = {
  page: Page;
  with_data: 1 | null;
};

type Row = {
  line: any[];
  link: string;
  url_device: string;
  url_devices: string;
};

type Column = {
  name: string;
  sort: boolean;
  title: string;
  type: string;
};
