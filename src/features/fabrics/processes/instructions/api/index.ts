import { api } from '@api/api2';
import { InstructionsPayload, InstructionsResponse } from '../types';

const getInstructions = ({
  page,
  with_data,
}: InstructionsPayload): InstructionsResponse =>
  api.no_vers('get', 'interface/instructions', {
    page,
    with_data,
    offset: 0,
    limit: 100,
  });

export { getInstructions };
