import { createFilterBag } from '@tools/factories';

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag({});
