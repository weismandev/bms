import pageList from '../app/initialPageList';
import { Page } from '../shared/types';
import { InstructionsResponse } from './instructions/types';

type Callback = {
  (raw: any): void;
};

function instructionObserver() {
  const instructions = pageList.reduce(
    (acc, pageName: Page) => ({
      ...acc,
      [pageName]: {},
    }),
    {}
  ) as { [key in Page]: InstructionsResponse };

  const handlers = pageList.reduce(
    (acc, pageName) => ({
      ...acc,
      [pageName]: [],
    }),
    {}
  ) as { [key in Page]: { path: string; cb: Callback }[] };

  return {
    subscribe({ page, path }: { page: Page; path: string }, cb: Callback) {
      if (handlers[page] && cb instanceof Function) {
        handlers[page as Page].push({ path, cb });

        if (path.length !== 0 && Object.keys(instructions[page]).length) {
          cb(
            path
              .split('.')
              .reduce((obj: any, path: string) => obj[path], instructions[page])
          );
        }
      }
    },
    fire(pageName: Page, raw: InstructionsResponse) {
      instructions[pageName] = raw;
      handlers[pageName].forEach((item) => {
        const instructionConfig =
          item.path.length === 0
            ? { filter: raw.filter, table: raw.table }
            : item.path.split('.').reduce((obj: any, path: string) => obj[path], raw);
        item.cb(instructionConfig);
      });
    },
  };
}

const observer = instructionObserver();

export default observer;
