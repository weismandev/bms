import {
  createStore,
  createEvent,
  combine,
  Event,
  Store,
  sample,
  forward,
} from 'effector';
import i18n from '@shared/config/i18n';
import { FormConfig, Form, Fields } from './types';

type Value = string | { id: number }[];
const { t } = i18n;

type StateWatcher = {
  value: any;
  change: Event<Value>;
};

type FieldEvents = {
  watcher: (name: string) => (state: StateWatcher) => void;
  resetter: Event<void>;
};

function createFields(fields: Fields, events: FieldEvents) {
  return Object.keys(fields).reduce((acc, name) => {
    const changeField = createEvent<Value>();
    const $field = createStore({
      ...fields[name],
      change: changeField,
    });

    $field.on(changeField, (state, params: Value) => ({
      ...state,
      value: params,
    }));
    $field.watch(events.watcher(name));
    $field.reset(events.resetter);
    return {
      ...acc,
      [name]: $field,
    };
  }, {});
}

const createForm = (() => {
  let fields = {};
  let config = {};
  const watcherFields: { [key: string]: (Function | null)[] } = {};

  const setConfig = (initConfig: FormConfig) => {
    fields = {
      ...fields,
      ...initConfig.fields,
    };
    config = {
      submitted: initConfig.submitted,
    };
  };

  const watcher = (name: string) => (state: StateWatcher) => {
    if (watcherFields[name] instanceof Array) {
      watcherFields[name].forEach((cb) => {
        if (cb instanceof Function) {
          cb(state.value);
        }
      });
    }
  };

  const watchField = (name: string, cb: Function): number => {
    let index = null;
    if (watcherFields[name] instanceof Array) {
      index = watcherFields[name].push(cb);
    } else {
      watcherFields[name] = [cb];
      index = 1;
    }
    return index;
  };

  const unWatchField = (name: string, index: number) => {
    watcherFields[name].splice(index - 1, 1, null);
  };

  const $form = createStore<any>({ $fields: {}, $errors: {} });
  const $additionFields = createStore<Fields>({});
  const $errors = createStore<any>({});

  const addField = createEvent<Fields>();
  const submitted = createEvent<void>();
  const validate = createEvent<void>();
  const resetter = createEvent<void>();

  $additionFields.on(addField, (state: Fields, fieldsPayload: Fields) => ({
    ...state,
    ...fieldsPayload,
  }));

  const $fields = $additionFields.map((store) =>
    combine(createFields(store, { watcher, resetter }))
  );

  $errors.on(validate, (_, result: any) =>
    Object.entries(result).reduce((acc: any, [key, params]: any) => {
      const { value, required, required_title = t('thisIsRequiredField') } = params;

      if (required) {
        if (!value) {
          return {
            ...acc,
            ...{ [key]: required_title },
          };
        }

        if (value?.length === 0) {
          return {
            ...acc,
            ...{ [key]: required_title },
          };
        }

        return acc;
      }

      return acc;
    }, {})
  );

  return (config: FormConfig): Form => {
    setConfig(config);

    forward({
      from: resetter,
      to: submitted,
    });

    sample({
      clock: submitted,
      source: $fields,
      target: config.submitted.prepend((fields: Store<Fields>) => {
        const state = fields.getState();
        return Object.keys(state).reduce(
          (acc, key) => ({ ...acc, [key]: state[key].value }),
          {}
        );
      }),
    });

    return {
      $form,
      $fields,
      $errors,
      getFields: () => {},
      event: {
        addField,
        resetForm: resetter,
        submitted,
        watchField,
        unWatchField,
        validate,
      },
    };
  };
})();

export { createForm };
