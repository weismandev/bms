import { useStore } from 'effector-react';
import { Form } from '../types';

function useForm({ $form, $fields, $errors, ...store }: Form) {
  const fields = useStore($fields) as any;
  const errors = useStore($errors) as any;

  return {
    ...store,
    fields: Object.keys(fields).length > 0 ? useStore(fields) : fields,
    errors,
  };
}

export { useForm };
