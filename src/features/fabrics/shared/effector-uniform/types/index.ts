import { Store, Event } from 'effector';

export type Action = {
  [key: string]: {
    url: string;
    method: 'post' | 'get';
  };
};

export type Option = { id: number; title: string };

export type Fields = {
  [key: string]: Option | Option[] | string;
};

export type FormConfig = {
  fields: Fields;
  submitted: Event<Fields>;
  errors?: any;
};

export type Events = {
  addField: Event<any>;
  resetForm: Event<any>;
  submitted: Event<any>;
  validate: Event<any>;
  watchField: (name: string, cb: Function) => number;
  unWatchField: (name: string, index: number) => void;
};

export type Form = {
  $form: any;
  $fields: Store<any>;
  $errors: Store<any>;
  onChange?: Event<any>;
  getFields?: any;
  event: Events;
};
