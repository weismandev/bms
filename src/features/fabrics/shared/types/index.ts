export type Page =
  | 'boundsactions'
  | 'catalogdevice'
  | 'doors'
  | 'garbage'
  | 'keys'
  | 'accessgroups'
  | 'devicehints';

export type ControlTypes =
  | 'show_filter'
  | 'search'
  | 'combolist'
  | 'toggle'
  | 'submit'
  | 'clear'
  | 'show_modal';

export type Control = {
  id: number;
  main: number;
  name: string;
  float?: number;
  only_cloud?: number;
  settings_block?: number;
  label: {
    title: string;
    readonly: boolean;
  };
  parent?: {
    name?: string;
  };
  state: {
    change: number;
    value: number[];
    measure: string;
    readonly: boolean;
    go_device: boolean;
  };
  control: {
    type: ControlTypes;
    urldata: string;
    graph: number;
    placeholder: string;
    dop_params?: string;
  };
  settings_block_title: string;
  position: string;
  onChange?: (value: any) => void;
};
