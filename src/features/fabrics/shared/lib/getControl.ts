import { ControlTypes } from '../types';
import { Controls } from '../ui/controls';

export const getControl = (type: ControlTypes) => {
  if (type in Controls) return Controls[type];

  return Controls.default;
};
