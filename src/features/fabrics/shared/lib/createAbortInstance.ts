type State = 'inactive' | 'pending' | 'fulfilled' | 'reject';

type Controllers = {
  [key: string]: {
    controller: AbortController;
    state: State;
  };
};

export function createAbortInstance() {
  const controllers: Controllers = {};

  const isSetByName = (name: string) => controllers[name] && controllers[name].state;

  const isOpenRequest = (name: string) =>
    isSetByName(name) && controllers[name].state === 'pending';

  const createInstance = (name: string) => {
    controllers[name] = {
      controller: new AbortController(),
      state: 'inactive',
    };
  };

  const setStatus = (name: string) => (state: State) => {
    if (isSetByName(name)) {
      controllers[name] = {
        state,
        controller: new AbortController(),
      };
    }
  };

  const getStatus = (name: string) => controllers[name].state;

  const getSignal = (name: string) => controllers[name].controller.signal;

  const abort = (name: string) => {
    if (isSetByName(name) && isOpenRequest(name)) {
      controllers[name].controller.abort();
      controllers[name].controller = new AbortController();
      controllers[name].state = 'reject';
    }
  };

  return function init(name: string) {
    if (!isSetByName(name)) {
      createInstance(name);
    }

    const handleGetSignal = () => getSignal(name);
    const handleAbort = () => abort(name);
    const handleOpenedRequest = () => isOpenRequest(name);
    const handleSetStatus = (state: State) => setStatus(name)(state);
    const handleGetStatus = () => getStatus(name);

    return {
      isOpenRequest: handleOpenedRequest,
      setStatus: handleSetStatus,
      getStatus: handleGetStatus,
      getSignal: handleGetSignal,
      abort: handleAbort,
    };
  };
}
