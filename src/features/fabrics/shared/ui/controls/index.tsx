import { ComboListControlProvider } from './ComboListControl';
import { SubmitControl } from './SubmitControl';
import { CancelControl } from './CancelControl';
import { UnknownControl } from './UnknownControl';
import { TextControl } from './TextControl/ui';
import { DateControl } from './DateControl/ui';
import { ToggleControl } from './ToggleControl/ui';

import { Control, ComboListControlType, CancelControlType } from './types';

type IControls = {
  [key: string]: (props: any) => JSX.Element;
};

export const Controls = ((): IControls => ({
  text: (props: any): JSX.Element => <TextControl {...props} />,
  date: (props: any): JSX.Element => <DateControl {...props} />,
  combolist: (props: ComboListControlType): JSX.Element => (
    <ComboListControlProvider multiple {...props} />
  ),
  list: (props: ComboListControlType): JSX.Element => (
    <ComboListControlProvider {...props} />
  ),
  toggle: (props: Control): JSX.Element => <ToggleControl {...props} />,
  submit: (props: Control): JSX.Element => <SubmitControl {...props} />,
  cancel: (props: CancelControlType): JSX.Element => <CancelControl {...props} />,
  default: (props: Control): JSX.Element => <UnknownControl {...props} />,
}))();
