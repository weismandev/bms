import { createEffect, createStore } from 'effector';

const fxGetList = createEffect();

const $isLoading = createStore(false);
const $error = createStore(null);
const $list = createStore({});

export { fxGetList, $isLoading, $error, $list };
