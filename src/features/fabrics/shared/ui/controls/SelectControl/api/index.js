import { getBaseHeaders } from '@api/tools/getBaseHeaders';

export const getList = ({ url }) =>
  fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  })
    .then((response) => (response.ok ? response.json() : Promise.reject(response)))
    .catch((error) => error.message);
