import { Check } from '@mui/icons-material';
import { ActionButton } from '@ui/index';
import { Control } from '../../../types';

const SubmitControl = (props: Control) => (
  <ActionButton kind="positive" type="submit" style={{ zIndex: 0 }}>
    <Check />
    {props.label.title}
  </ActionButton>
);

export { SubmitControl };
