import { ChangeEvent } from 'react';
import { MessageText, InputControl } from '@ui/index';

export const DateControl = (props: any) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (props.onChange) {
      const {
        target: { value },
      } = event;
      props.onChange(value);
    }
  };

  const inputControlProps = {
    type: 'date',
    label: props.label.title,
    value: props.value,
    onChange: handleChange,
    required: props.required,
    error: props.error,
    helperText: props.error,
    fullWidth: true,
  };

  return (
    <div>
      <InputControl {...(inputControlProps as any)} />
    </div>
  );
};
