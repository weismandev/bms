import { useEffect } from 'react';
import { useUnit } from 'effector-react/scope';
import { getListByParams, $list, $isLoading, $meta } from './model';
import { ComboListComponent } from './ui';

import { ComboListControlType, Option } from './types';

let timeout: ReturnType<typeof setTimeout>;

const ComboListControl = ({ event, parent = {}, ...props }: ComboListControlType) => {
  const {
    name,
    control: { url: baseUrl = '' },
  } = props;
  const parentName = parent.name;

  const isLoading = useUnit($isLoading);
  const list = useUnit($list);
  const meta = useUnit($meta);
  const dispatch = useUnit({ getListByParams });

  const searchChanged =
    (prevSearchValue = '') =>
    (search: string) => {
      if (prevSearchValue !== search) {
        // eslint-disable-next-line no-param-reassign
        prevSearchValue = search;
        dispatch.getListByParams({ name, payload: { baseUrl, search } });
      }
    };

  const nextBatchOptions = ({ limit, offset }: { limit: number; offset: number }) => {
    dispatch.getListByParams({ name, payload: { baseUrl, limit, offset } });
  };

  const prepandPayload = () => {
    const dependParams = Object.keys(props.params || {}).filter(
      (key) => Array.isArray(props.params[key]) && props.params[key]?.length > 0
    );

    if (dependParams.length > 0) {
      return dependParams.reduce(
        (acc, curr) => ({
          ...acc,
          [curr]: props.params[curr]?.join(),
        }),
        {}
      );
    }
    return {};
  };

  useEffect(() => {
    const watcher: { dependentName: string; index: number }[] = [];
    dispatch.getListByParams({ name, payload: { baseUrl, params: prepandPayload() } });
    if (parentName) {
      parentName.split(',').forEach((dependentName: string) => {
        const index = event.watchField(dependentName, (state: { id: number }[]) => {
          if (state.length === 0) {
            props.onChange([]);
          }
          const params = {
            ...(state.length > 0
              ? { [dependentName]: state.map(({ id }) => id).join() }
              : {}),
          };
          dispatch.getListByParams({ name, payload: { baseUrl, params } });
        });
        watcher.push({ dependentName, index });
      });
    }
    return () => {
      if (parentName && watcher.length > 0) {
        watcher.forEach(({ dependentName, index }) => {
          event.unWatchField(dependentName, index);
        });
      }
    };
  }, []);

  useEffect(() => {
    const value = props.fields[props.name].value || [];
    if (parentName && value.length > 0) {
      const values = value.filter((i: Option) => list.some((k: Option) => i.id === k.id));
      if (values.length !== value.length) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
          props.onChange(values);
        }, 0);
      }
    }
  }, [list]);

  return (
    <ComboListComponent
      {...props}
      searchChanged={searchChanged('')}
      nextBatchOptions={nextBatchOptions}
      options={list}
      isLoading={isLoading}
      meta={meta}
    />
  );
};

export { ComboListControl };
