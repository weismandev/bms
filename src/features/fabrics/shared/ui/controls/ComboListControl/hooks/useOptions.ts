import { useState, useEffect } from 'react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { defaultLimit, defaultOffset } from '../model';

interface IBuildUrl {
  baseUrl: string;
  search?: string;
  value?: string;
  offset?: number;
  limit?: number;
  params?: any;
}

interface IUseOptions {
  baseUrl: string;
  params?: any;
}

export const buildUrl = ({
  baseUrl,
  search,
  value,
  offset = defaultOffset,
  limit = defaultLimit,
  params = {},
}: IBuildUrl) => {
  const urlObject = new URL(baseUrl);

  urlObject.searchParams.set('limit', limit + '');
  urlObject.searchParams.set('offset', offset + '');

  if (value) {
    urlObject.searchParams.set('id', value);
  }

  if (search) {
    urlObject.searchParams.set('search', search);
  }

  Object.keys(params).forEach((name) => {
    urlObject.searchParams.set(name, params[name] || '');
  });

  return urlObject.toString();
};

export const useOptions = ({ baseUrl, params }: IUseOptions) => {
  const [status, setStatus] = useState({
    isLoading: false,
    data: {
      list: [],
    },
  });

  const fetchOptions = async (url: string) => {
    const response = await fetch(url, {
      headers: {
        ...getBaseHeaders()
      }
    });
    const { data } = await response.json();
    return data;
  };

  const fetchOptionsBySearch = async (search: string, offset: number) => {
    const offsetOptions = offset || 0;

    const url = buildUrl({ baseUrl, search, params, offset: offsetOptions });
    const data = await fetchOptions(url);
    return data;
  };

  useEffect(() => {
    const getData = async () => {
      const url = buildUrl({ baseUrl, params });
      const res = await fetchOptions(url);

      return res;
    };

    const data = getData();

    setStatus({ ...status, isLoading: false, data });
  }, [baseUrl, params]);

  return { ...status, fetchOptionsBySearch };
};
