import styled from '@emotion/styled';
import { SelectControl } from '@ui/index';

export const StyledSelectControl = styled(SelectControl)`
  margin-bottom: 24px;

  '& label': {
    fontWeight: 500;
    color: '#7D7D8E';
    margin: 0;
  },
`;
