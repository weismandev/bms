import { useState, useEffect } from 'react';
import { SelectControl } from '@ui/index';
import { debounce } from '@tools/debounce';

import { ComboListControlType, Option, Meta } from '../types';
import { useStyles } from './styles';

type Props = Omit<ComboListControlType, 'event'> & {
  isLoading: boolean;
  options: Option[];
  meta: Meta;
  searchChanged: (search: string) => void;
  nextBatchOptions: ({ limit, offset }: { limit: number; offset: number }) => void;
};

const ComboListComponent = ({ name, isLoading, options, meta, ...props }: Props) => {
  const classes = useStyles();

  const [visabilityMenu, visableMenu] = useState(false);

  const handleInputChange = debounce(props.searchChanged, 1000);

  const handleScrollBottom = () => {
    const count = meta.limit + meta.offset;
    if (meta.total > count) {
      props.nextBatchOptions({ offset: 0, limit: count + 10 });
    }
  };

  const handleVisableMenu = (visability: boolean) => () => {
    if (typeof visability === 'boolean') {
      visableMenu(visability);
    }
  };

  useEffect(() => {
    if (props.isDisabled) {
      visableMenu(false);
    }
  }, [props.isDisabled]);

  if (!props.visible) return null;
  return (
    <div className={classes.wrapper}>
      <SelectControl
        {...props}
        name={name}
        isLoading={isLoading}
        value={props.state.value || []}
        onChange={props.onChange}
        label={props.label.title}
        placeholder={props.control.placeholder}
        divider={false}
        isClearable={false}
        options={options}
        isMulti={props.multiple}
        classes={{ item: classes.select }}
        onInputChange={handleInputChange}
        onMenuScrollToBottom={handleScrollBottom}
        menuIsOpen={visabilityMenu}
        onMenuOpen={handleVisableMenu(true)}
        onMenuClose={handleVisableMenu(false)}
      />
    </div>
  );
};

export { ComboListComponent };
