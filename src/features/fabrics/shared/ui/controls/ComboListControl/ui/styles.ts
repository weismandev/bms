import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  wrapper: {
    zIndex: 2,
    '& label': {
      fontWeight: 500,
      color: '#7D7D8E',
      margin: 0,
    },
  },
  select: {
    marginBottom: 10,
  },
});
