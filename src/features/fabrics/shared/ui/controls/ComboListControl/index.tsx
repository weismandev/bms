import { Provider } from 'effector-react/scope';
import { ComboListControl } from './ComboListControl';
import { createScope } from './model';
import { ComboListControlType } from './types';

const ComboListControlProvider = (props: ComboListControlType) => {
  const scope = createScope(props.name);

  return (
    <Provider value={scope}>
      <ComboListControl {...props} />
    </Provider>
  );
};

export { ComboListControlProvider };
