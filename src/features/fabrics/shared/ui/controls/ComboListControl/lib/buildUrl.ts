export interface IBuildUrl {
  baseUrl: string;
  search?: string;
  value?: string;
  offset?: number;
  limit?: number;
  params?: { [key: string]: string };
}

export const buildUrl = ({
  baseUrl,
  search,
  value,
  offset = 0,
  limit = 100,
  params = {},
}: IBuildUrl) => {
  const urlObject = new URL(baseUrl);

  urlObject.searchParams.set('limit', String(limit));
  urlObject.searchParams.set('offset', String(offset));

  if (value) {
    urlObject.searchParams.set('id', value);
  }

  if (search) {
    urlObject.searchParams.set('search', search);
  }

  Object.keys(params).forEach((name) => {
    urlObject.searchParams.set(name, params[name] || '');
  });

  return urlObject.toString();
};
