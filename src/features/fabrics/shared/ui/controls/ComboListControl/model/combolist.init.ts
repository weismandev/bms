import { sample } from 'effector';
import { throttle } from 'patronum/throttle';
import { signout } from '@features/common';
import { getList } from '../api';
import {
  fxGetList,
  $isLoading,
  $error,
  $list,
  $meta,
  $params,
  getListByParams,
  searchChanged,
} from './combolist.model';
import { buildUrl } from '../lib/buildUrl';

const throttledSearch = throttle({
  source: searchChanged,
  timeout: 1000,
});

fxGetList.use(getList);

$list
  .on(fxGetList.done, (state, { result }) => {
    if (typeof result === 'string') return state;
    return result.data.list ?? [];
  })
  .reset(signout);

$meta
  .on(fxGetList.done, (state, { result }) => {
    if (typeof result === 'string') return state;
    return {
      ...result.data.meta,
      total: result.data.meta.total ?? result.data.count,
    };
  })
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);

$params
  .on(getListByParams, (state, params) => ({ ...state, ...params }))
  .on(throttledSearch, (state, search) => ({ ...state, search }))
  .reset(signout);

sample({
  clock: getListByParams,
  fn: ({ name, payload }) => ({ name, url: buildUrl(payload) }),
  target: fxGetList,
});
