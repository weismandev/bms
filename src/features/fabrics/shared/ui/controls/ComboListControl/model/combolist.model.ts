import { fork, Scope, createDomain } from 'effector';
import { createGate } from 'effector-react';
import { IBuildUrl } from '../lib/buildUrl';

import { GetListPayload, GetListResponse, Meta } from '../types';

type Params = Omit<IBuildUrl, 'baseUrl'>;
type Payload = {
  name: string;
  payload: IBuildUrl;
};

export const defaultOffset = 0;
export const defaultLimit = 100;
export const offsetDifference = defaultLimit;

export const scopes: { [key: string]: Scope } = {};

export const combolistDomain = createDomain();
export const ComboListGate = createGate<{ url: string; name: string }>();

export const fxGetList = combolistDomain.createEffect<
  GetListPayload,
  GetListResponse,
  Error
>();

export const getListByParams = combolistDomain.createEvent<Payload>();
export const searchChanged = combolistDomain.createEvent<string>();

export const $isLoading = combolistDomain.createStore<boolean>(false);
export const $error = combolistDomain.createStore<null | Error>(null);
export const $params = combolistDomain.createStore<Params>({});
export const $list = combolistDomain.createStore<{ id: number; title: string }[]>([]);
export const $meta = combolistDomain.createStore<Meta>({
  offset: 0,
  limit: 100,
  search: '',
  total: 100,
});

export const createScope = (name: string) => {
  if (scopes[name]) return scopes[name];
  scopes[name] = fork(combolistDomain);

  return scopes[name];
};
