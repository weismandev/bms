import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { createAbortInstance } from '../../../../lib/createAbortInstance';
import { GetListPayload } from '../types';

const abortController = createAbortInstance();

const getList = ({ name, url }: GetListPayload) => {
  const controller = abortController(name);
  if (controller.isOpenRequest()) {
    controller.abort();
  }
  controller.setStatus('pending');
  return fetch(url, {
    signal: controller.getSignal(),
    headers: {
      ...getBaseHeaders()
    }
  })
    .then((response) => (response.ok ? response.json() : Promise.reject(response)))
    .catch(({ message }) => message)
    .finally(() => {
      controller.setStatus('fulfilled');
    });
};

export { getList };
