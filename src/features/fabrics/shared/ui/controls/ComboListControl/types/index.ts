import { Event } from 'effector';
import { Control } from '../../../../types';
import { Events } from '../../../../effector-uniform/types';

export type Meta = {
  limit: number;
  offset: number;
  search: string;
  total: number;
};

export type Option = {
  id: number;
  title: string;
};

export type GetListPayload = {
  name: string;
  url: string;
};

export type GetListResponse = {
  data: {
    count: number;
    list: Option[];
    meta: Meta;
  };
};

export type ComboListControlType = Control & {
  multiple?: boolean;
  onChange: Event<any>;
  error?: string;
  visible?: boolean;
  isDisabled: boolean;
  control?: {
    url?: string;
  };
  fields: {
    [key: string]: {
      value: string;
      change: Event<any>;
    };
  };
  params: {
    [key: string]: number[] | string | null;
  };
  layoutControls: {
    main: Control[];
  };
  event: Events;
};

export type Value = {
  id: number;
  title: string;
};
