import i18n from '@shared/config/i18n';

const { t } = i18n;

interface Props {
  control: {
    type: string;
  };
}

export const UnknownControl = ({ control }: Props) => {
  return (
    <div>
      <div>{t('UnknownControl')}</div>
      <span>{control.type}</span>
    </div>
  );
};
