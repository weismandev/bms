import { Checkbox, FormControlLabel } from '@mui/material';
import { useStyles } from './styles';
import { Control } from '../types';

export const ToggleControl = (props: Control) => {
  const classes = useStyles();
  const isChecked = Boolean(Number(props.state.value));

  const handleChange = () => {
    if (props.onChange) {
      props.onChange(!isChecked);
    }
  };

  return (
    <div>
      <FormControlLabel
        labelPlacement="start"
        control={<Checkbox checked={isChecked} />}
        onChange={handleChange}
        label={props.label.title}
        classes={{
          root: classes.root,
          label: classes.label,
        }}
      />
    </div>
  );
};
