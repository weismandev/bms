import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  root: {
    marginLeft: 0,
    marginRight: -4,
    marginBottom: 24,
    display: 'flex',
    justifyContent: 'space-between'
  },
  label: {
    color: 'rgb(147, 147, 147)',
  }
});
