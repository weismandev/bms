import { ChangeEvent } from 'react';
import TextField from '@mui/material/TextField';

export const TextControl = (props: any) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (props.onChange) {
      const {
        target: { value },
      } = event;
      props.onChange(value);
    }
  };

  return (
    <div>
      <TextField
        id={props.id}
        label={props.label.title}
        defaultValue={props.state.value}
        required={props.required}
        value={props.value}
        onChange={handleChange}
        error={props.error}
        helperText={props.error}
        fullWidth
        InputProps={{
          readOnly: props.state.readonly,
        }}
      />
    </div>
  );
};
