import { createStore, createEvent } from 'effector';

export const $isSub = createStore(false);
export const submitForm = createEvent();
