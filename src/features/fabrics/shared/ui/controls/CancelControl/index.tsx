import { MouseEvent } from 'react';
import { Clear } from '@mui/icons-material';
import { ActionButton } from '@ui/index';
import { CancelControlType } from './types';

const CancelControl = (props: CancelControlType) => {
  const handleResetForm = (e: MouseEvent) => {
    if (!props?.event?.resetForm) return;

    e.preventDefault();
    props.event.resetForm();
  };
  return (
    <ActionButton
      kind="negative"
      type="reset"
      onClick={handleResetForm}
      style={{ zIndex: 0 }}
    >
      <Clear />
      {props.label.title}
    </ActionButton>
  );
};

export { CancelControl };
