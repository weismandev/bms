import { Event } from 'effector';
import { Control } from '../../../../types';

export type CancelControlType = Control & {
  multiple?: boolean;
  event: {
    resetForm: Event<void>;
  };
};
