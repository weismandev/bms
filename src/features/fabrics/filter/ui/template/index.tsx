import React, { useContext, useEffect, FormEvent } from 'react';
import { useStore, useGate } from 'effector-react';

import { Wrapper, FilterToolbar, CustomScrollbar, Loader } from '@ui/index';
import { useForm } from '../../../shared/effector-uniform';

import {
  $isLoading,
  $filterControls,
  $slicedParams,
  filterForm,
  ControlsGate,
  filtersSubmitted,
  sliceParams,
} from '../../model';

import PageContext from '../../../app/pageContext';
import FormContext from '../../../app/formContext';

import { Control } from '../../../shared/types';
import { implementParentControls } from '../../lib/implementParentControls';
import { implementParentParams } from '../../lib/implementParentParams';

type Layout = {
  main: Control[] & { Component: any };
  footer: Control[];
};

const controlFooterNames = ['submit', 'cancel'];

const Filter: any = ({ page }: any) => {
  const form = useForm(filterForm) as any;
  const isLoading = useStore($isLoading);
  const filterControls = useStore($filterControls) as any;
  const slicedParams = useStore($slicedParams);

  useGate(ControlsGate, { page });

  const layoutControls = filterControls.reduce(
    (acc: Layout, curr: Control) => ({
      ...acc,
      ...(controlFooterNames.includes(curr.control.type)
        ? { footer: acc.footer.concat(curr) }
        : { main: acc.main.concat(curr) }),
    }),
    {
      main: [],
      footer: [],
    }
  );

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    form.event.submitted(form.fields);
  };

  useEffect(() => {
    const watcher = filtersSubmitted.watch(sliceParams);
    return watcher;
  }, []);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <PageContext.Consumer>
          {({ filter: { visibleFilter } }) => (
            <FilterToolbar
              style={{ padding: 24 }}
              closeFilter={() => visibleFilter(false)}
            />
          )}
        </PageContext.Consumer>
        <Loader isLoading={isLoading} />
        <FormContext.Provider value={{ form, controls: filterControls }}>
          <form style={{ padding: 24, paddingTop: 0 }} onSubmit={onSubmit}>
            {layoutControls.main.map(({ Component, ...control }) => {
              const { fields } = form;
              const { name } = control;

              const config = implementParentControls(
                control,
                layoutControls.main,
                fields
              );

              const params = implementParentParams(control, slicedParams);

              return (
                <Component
                  key={name}
                  value={form.fields[name]?.value}
                  onChange={form.fields[name].change}
                  {...control}
                  state={{
                    ...control.state,
                    value: form.fields[name]?.value,
                  }}
                  event={form.event}
                  {...config}
                  params={params}
                  fields={fields}
                  layoutControls={layoutControls}
                />
              );
            })}
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                flexWrap: 'nowrap',
              }}
            >
              {layoutControls.footer.map(({ Component, ...control }: any) => (
                <Component
                  key={control.name}
                  {...control}
                  state={{
                    ...control.state,
                    value: form.fields[control.name]?.value,
                  }}
                  event={form.event}
                />
              ))}
            </div>
          </form>
        </FormContext.Provider>
      </CustomScrollbar>
    </Wrapper>
  );
};

export const FilterWithGate: React.FC<{}> = (): JSX.Element => {
  const { page } = useContext(PageContext);

  return <Filter page={page} />;
};
