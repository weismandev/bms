import { Control } from '../../../shared/types';
import { Fields, Option } from '../../../shared/effector-uniform/types';

export const implementParentParams = (signal: Control, fields: Fields) => {
  if (!signal.control.dop_params) return {};

  const parseValue = (value: Option | Option[] | string) => {
    if (Array.isArray(value)) return value.map(({ id }) => id);
    if (typeof value === 'object') return value?.id;
    return value;
  };

  return signal.control.dop_params.split(',').reduce(
    (acc, key: string) => ({
      ...acc,
      [key]: parseValue(fields[key] || ''),
    }),
    {}
  );
};
