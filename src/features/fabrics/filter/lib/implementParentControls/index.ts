interface Value {
  id: number;
  title: string;
}

interface Control {
  parent: {
    id: string;
    name: string;
    visible: boolean;
    value: string;
  };
}

interface Fields {
  [key: string]: {
    value: string;
  };
}

export const matchCondition = (
  parentState: Value[] | string[],
  childValue: undefined | string
) => {
  if (!parentState || parentState.length === 0) {
    return false;
  }
  if (Array.isArray(parentState) && parentState?.flat()?.length === 0) {
    return false;
  }

  if (!childValue) {
    const isNotSelectedValue = parentState.some((item) => typeof item !== 'object');

    if (isNotSelectedValue) {
      return false;
    }

    return true;
  }

  return parentState.flat().some((item: any) => childValue.includes(item?.id));
};

export const implementParentControls = (
  control: Control,
  controls: Control[],
  fields: Fields
) => {
  if (!control.parent) return { isDisabled: false, visible: true };

  const visible = control.parent?.visible;
  const config = { isDisabled: visible, visible };

  const founded = controls.filter((item: any) => {
    if (control.parent?.id) {
      const parentIds = control.parent?.id.split(',');

      return parentIds.includes(item.id);
    }

    const parentNames = control.parent?.name.split(',');

    return parentNames.includes(item.name);
  });

  if (founded && founded.length > 0) {
    const foundedValues = founded.map((item: any) => fields[item?.name]?.value);
    const isMatch = matchCondition(foundedValues, control.parent?.value);
    if (isMatch) {
      config.isDisabled = false;
      config.visible = true;
    }

    return config;
  }
};
