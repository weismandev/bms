import { api } from '@api/api2';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

export const getInstructions = ({ page }) =>
  api.no_vers('get', 'interface/instructions', {
    page,
  });

export const getTable = (payload) => api.no_vers('get', 'interface/get-table', payload);

export const getList = ({ url }) =>
  fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  })
    .then((response) => (response.ok ? response.json() : Promise.reject(response)))
    .catch((error) => error.message);
