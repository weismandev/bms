import { createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { createForm } from '../../shared/effector-uniform/createForm';
import { Control, Page } from '../../shared/types';
import { Fields } from '../../shared/effector-uniform/types';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const ControlsGate = createGate<{ page: Page }>({});

const $filterForm = createStore({});
const $isLoading = createStore(false);
const $error = createStore(null);
const $filterControls = createStore<Control[] | []>([]);
export const $namespaces = createStore<{ [key: string]: boolean }>({});
export const $slicedParams = createStore<Fields>({});

export const filtersSubmitted = createEvent<Fields>();
export const setDefaultFilterValues = createEvent<Control[] | []>();
export const updateFilterValues = createEvent<void>();
export const changedGroupVisibility = createEvent();
export const setNamespace = createEvent<string>();
export const sliceParams = createEvent<Fields>();

export const filterForm = createForm({
  fields: {},
  submitted: filtersSubmitted,
});

export {
  PageGate,
  pageMounted,
  pageUnmounted,
  $isLoading,
  $error,
  $filterControls,
  ControlsGate,
  $filterForm,
};
