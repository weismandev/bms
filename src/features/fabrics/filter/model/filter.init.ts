import { forward, sample } from 'effector';
import { signout } from '../../../common';

import instructionObserver from '../../processes/instructionObserver';
import {
  ControlsGate,
  filterForm,
  $namespaces,
  $filterControls,
  $slicedParams,
  setDefaultFilterValues,
  updateFilterValues,
  setNamespace,
  sliceParams,
} from './filter.model';
import { getControl } from '../../shared/lib';
import { Control, Page } from '../../shared/types';
import { Fields } from '../../shared/effector-uniform/types';

const enumControlFiledForm = ['combolist', 'date', 'list', 'state', 'toggle'];

type State = {
  page: Page;
};

ControlsGate.open.watch((state: State) => {
  const namespaces = $namespaces.getState();

  if (!namespaces[state.page]) {
    const config = {
      page: state.page,
      path: 'filter.main',
    };
    instructionObserver.subscribe(config, setDefaultFilterValues);
    setNamespace(state.page);
  } else {
    updateFilterValues();
  }
});

$namespaces
  .on(setNamespace, (state, namespace) => ({
    ...state,
    [namespace]: true,
  }))
  .reset(signout);

$slicedParams.on(sliceParams, (_, fields) => fields).reset(signout);

$filterControls
  .on(setDefaultFilterValues, (_, result: Control[]) =>
    result.map((item: Control) => ({
      ...item,
      Component: getControl(item.control.type),
    }))
  )
  .reset(signout);

forward({
  from: setDefaultFilterValues,
  to: filterForm.event.addField.prepend(prepandField),
});

sample({
  source: $slicedParams,
  clock: updateFilterValues,
  target: filterForm.event.addField.prepend(prepandUpdateField),
});

function prepandField(params: Control[]) {
  return params
    .filter((item: any) => enumControlFiledForm.includes(item.control.type))
    .reduce(
      (acc: any, item: any) => ({
        ...acc,
        [item.name]: { value: item.state.value },
      }),
      {}
    );
}

function prepandUpdateField(params: Fields) {
  return Object.keys(params).reduce(
    (acc, key) => ({ ...acc, [key]: { value: params[key] } }),
    {}
  );
}
