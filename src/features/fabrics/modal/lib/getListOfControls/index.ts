import { getControl } from '@features/fabrics/devices/detail/lib/controlsMapping/getControl';
import { getControl as getControlForBottom } from '@features/fabrics/shared/lib';

export const getListOfControls = (settings_block: any, signals: any) => {
  const tabsWithBoxes = [] as any;

  settings_block.forEach((block: any) => {
    if (!block.parent_id) tabsWithBoxes.push({ ...block, boxes: [] });
  });

  settings_block.forEach((block: any) => {
    if (block.parent_id) {
      const blockWidthSignals = { ...block };
      const blockSignals = signals.filter(
        (signal: any) => signal.settings_block === blockWidthSignals.id
      );

      if (!!blockSignals.length) blockWidthSignals.signals = blockSignals;

      tabsWithBoxes
        .find((item: any) => item.id === block.parent_id)
        .boxes.push(blockWidthSignals);
    }
  });

  return tabsWithBoxes;
};

export const getListOfBottomSignals = (signals: any) => {
  return signals.map((info: any) => {
    return {
      ...info,
      control: {
        ...info.control,
        Component: getControlForBottom(info?.control?.type),
      },
    };
  });
};

export const getListOfSignals = (signals: any) =>
  signals.map((info: any) => {
    return {
      info,
      ...info,
      visible: true,
      divider: false,
      control: {
        ...info.control,
        Component: getControl(info?.control?.type),
      },
    };
  });
