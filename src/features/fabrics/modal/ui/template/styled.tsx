import styled from '@emotion/styled';

export const StyledDialogBody = styled.div`
  width: 600px;
  padding: 0 24px 24px;
`;

export const StyledDialogBottom = styled.div<{ align?: string }>`
  width: 600px;
  padding: 0 24px 24px;
  gap: 16px;
  justify-content: ${({ align }) => (align === 'right' ? 'flex-end' : 'inherit')};
  display: flex;
  align-items: center;
`;

export const StyledDialogTop = styled.div`
  width: 600px;
  padding: 24px;
  display: flex;
  align-items: center;
`;

export const StyledDialogHeader = styled.h2`
  font-size: 24px;
  font-weight: 500;
  line-height: 32px;
  color: #252834de;
`;
