import { useUnit } from 'effector-react';
import { Form, Formik } from 'formik';
import { Dialog } from '@mui/material';
import { ViewBox } from '@features/fabrics/devices/detail/ui/molecules/view-box';
import { Loader, Wrapper } from '@ui/atoms';
import { ErrorMessage } from '@ui/index';
import {
  $isModalShown,
  $modalData,
  ModalGate,
  hideModal,
  changeSignal,
  $isLoading,
  $rawModalData,
  $bottomData,
  $activeTab,
  $topData,
  sendModalData,
  clearError,
  $error,
} from '../../model';
import {
  StyledDialogBody,
  StyledDialogBottom,
  StyledDialogHeader,
  StyledDialogTop,
} from './styled';

const ModalData = () => {
  const [modalData, rawModalData, bottomData, activeTab, topData] = useUnit([
    $modalData,
    $rawModalData,
    $bottomData,
    $activeTab,
    $topData,
  ]);

  if (!modalData) return null;

  return (
    <Wrapper style={{ height: '100%', overflow: 'visible', boxShadow: 'none' }}>
      <StyledDialogTop>
        <StyledDialogHeader>{topData.form_title}</StyledDialogHeader>
      </StyledDialogTop>

      <StyledDialogBody>
        {modalData[activeTab].boxes?.map((box: any) => {
          return (
            <ViewBox
              key={box.id}
              box={box}
              isViewMode={false}
              sliceCurrentTabControls={changeSignal}
              interceptor={changeSignal}
              type={changeSignal}
              serialnumber={''}
            />
          );
        }) ?? null}
      </StyledDialogBody>

      <StyledDialogBottom align={rawModalData?.bottom?.position}>
        {bottomData.map((signal: any) => {
          const Component = signal.control.Component;

          return <Component key={signal.name} isViewMode={false} {...signal} />;
        })}
      </StyledDialogBottom>
    </Wrapper>
  );
};

export const Modal = () => {
  const [isModalShown, isLoading, error] = useUnit([$isModalShown, $isLoading, $error]);

  const onSubmit = () => sendModalData();

  const onReset = () => hideModal();

  return (
    <>
      <ModalGate />

      <ErrorMessage isOpen={error} onClose={clearError} error={error} />

      <Loader isLoading={isLoading} />

      <Dialog
        open={isModalShown}
        onClose={() => hideModal()}
        PaperProps={{
          style: { borderRadius: 16, overflow: 'visible' },
        }}
      >
        <Formik
          initialValues={{}}
          onSubmit={onSubmit}
          onReset={onReset}
          render={() => (
            <Form>
              <ModalData />
            </Form>
          )}
        />
      </Dialog>
    </>
  );
};

export const ModalWithGate: React.FC<{}> = (): JSX.Element => {
  return <Modal />;
};
