import { ACTION_BUTTON_NAME } from './model';

export function collectPayload(params: any, [modalSignals, bottomData]: [any, any]) {
  const url = bottomData?.find((item: any) => item.name === ACTION_BUTTON_NAME)?.control
    ?.url;

  const sendData = {
    signals: modalSignals
      .filter((item: any) => item.modified)
      .map(({ name, state }: any) => ({ name, value: state.value })),
  };

  return { url, sendData };
}
