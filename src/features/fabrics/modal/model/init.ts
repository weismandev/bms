import { attach, forward, merge, sample } from 'effector';
import { signout } from '@features/common';
import {
  getListOfBottomSignals,
  getListOfControls,
  getListOfSignals,
} from '../lib/getListOfControls';
import { collectPayload } from './collectPayload';
import {
  $isModalShown,
  $modalData,
  $modalRequestUrl,
  fxGetModal,
  hideModal,
  modalUnmounted,
  setModalRequestUrl,
  showModal,
  changeSignal,
  $modalSignals,
  $rawModalData,
  $isLoading,
  $bottomData,
  $activeTab,
  setActiveTab,
  $topData,
  fxSendModalData,
  sendModalData,
  $error,
  clearError,
} from './model';

const isRequest = merge([fxGetModal.pending, fxSendModalData.pending]);
$isLoading.on(isRequest, (_, status) => status);

const isError = merge<any>([fxSendModalData.failData, fxGetModal.failData]);
$error
  .on(clearError, () => null)
  .on(isError, (_, error) => error.message ?? 'Request error')
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(signout, modalUnmounted);

$isModalShown
  .on(showModal, () => true)
  .on(hideModal, () => false)
  .reset(modalUnmounted);

$modalRequestUrl.on(setModalRequestUrl, (_, url) => url).reset(signout, modalUnmounted);

$modalSignals
  .on(changeSignal, (state, data: any) => {
    const changedData = [...state];
    const changeIndex = changedData.findIndex((item) => item.id === data.id);

    changedData[changeIndex] = data;

    return changedData;
  })
  .reset(signout, modalUnmounted);

$rawModalData
  .on(fxGetModal.doneData, (_, { data }) => data)
  .reset(signout, modalUnmounted);

$modalData.reset(signout, modalUnmounted);

$activeTab.on(setActiveTab, (_, tab) => tab).reset(signout, modalUnmounted);

$topData.on(fxGetModal.doneData, (_, { data }) => data.top);

/** При получении данных, формирую сигналы футера */
sample({
  clock: $rawModalData,
  fn: (rawModalData) => getListOfBottomSignals(rawModalData?.bottom?.signals ?? []),
  target: $bottomData,
});

/** При получении данных, формирую сигналы */
sample({
  clock: $rawModalData,
  fn: (rawModalData) => getListOfSignals(rawModalData?.main?.signals ?? []),
  target: $modalSignals,
});

/** При формировании сигналов, формирую контролы */
sample({
  clock: $modalSignals,
  source: $rawModalData,
  filter: (_, modalSignals) => !!modalSignals.length,
  fn: (rawModalData, modalSignals) =>
    getListOfControls(rawModalData.main.settings_block, modalSignals),
  target: $modalData,
});

/** При получении url делаю запрос данных */
sample({
  clock: $modalRequestUrl,
  filter: (url) => !!url,
  fn: (url) => String(url),
  target: fxGetModal,
});

/** Подготовка запроса отправки данных модалки */
const getModalDataWidthPayload = attach({
  effect: fxSendModalData,
  source: [$modalSignals, $bottomData],
  mapParams: collectPayload,
});

/** При событии sendModalData отправляю данные модалки */
forward({
  from: sendModalData,
  to: getModalDataWidthPayload,
});

/** Закрываю модалку при успешной отправке данных */
sample({
  clock: fxSendModalData.doneData,
  filter: ({ error }) => !error,
  target: hideModal,
});

/** Передаю ошибку */
sample({
  clock: [fxSendModalData.doneData, fxGetModal.doneData],
  filter: ({ error }) => !!error,
  target: isError,
});
