import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

export const ACTION_BUTTON_NAME = 'apply';

export const ModalGate = createGate();
export const modalUnmounted = ModalGate.close;
export const modalMounted = ModalGate.open;

export const $isModalShown = createStore(false);
export const showModal = createEvent();
export const hideModal = createEvent();

export const $isLoading = createStore(false);

export const $modalRequestUrl = createStore<string | null>(null);
export const setModalRequestUrl = createEvent<string>();

export const $activeTab = createStore(0);
export const setActiveTab = createEvent<number>();

export const $bottomData = createStore<any>(null);
export const $topData = createStore<any>(null);

export const $rawModalData = createStore<any>(null);
export const $modalData = createStore<any>(null);
export const $modalSignals = createStore<any>(null);
export const changeSignal = createEvent();

export const clearError = createEvent();
export const $error = createStore(null);

export const fxGetModal = createEffect(async (url: string) => {
  const data = await fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

export const sendModalData = createEvent();
export const fxSendModalData = createEffect(async ({ url, sendData }: any) => {
  const data = await fetch(url, {
    method: 'POST',
    headers: {
      ...getBaseHeaders(),
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(sendData),
  });

  return data.json();
});
