export interface ModalControl {
  id: number;
  name: string;
  label: Label;
  control: Control;
  state: State;
  position: string;
}

export interface Label {
  title: string;
}

export interface Control {
  type: string;
  url: string;
}

export interface State {
  value: string;
}
