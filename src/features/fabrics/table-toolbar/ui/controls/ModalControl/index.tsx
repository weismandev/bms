import { useUnit } from 'effector-react';
import { setModalRequestUrl, showModal } from '@features/fabrics/modal/model';
import { $selection } from '@features/fabrics/table/model';
import MassChanges from '@img/mass-changes.svg';
import { ActionButton } from '@ui/atoms';
import { ModalControl } from './type';

export const ModalButtonControl = (props: ModalControl) => {
  const { label, control } = props;
  const [selection] = useUnit([$selection]);

  const hanldeModalButtonClick = () => {
    const collectedUrl = `${control.url}&ids=${selection.join(',')}`;

    setModalRequestUrl(collectedUrl);
    showModal();
  };

  return (
    <ActionButton
      kind="basic"
      disabled={!selection.length}
      onClick={hanldeModalButtonClick}
    >
      <MassChanges />
      {label.title}
    </ActionButton>
  );
};
