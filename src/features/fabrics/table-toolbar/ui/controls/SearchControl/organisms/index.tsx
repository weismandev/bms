import { createComponent } from 'effector-react';
import { AdaptiveSearch } from '@ui/index';
import { $tableData, searchChanged, $search } from '../../../../../table/model';

const SearchControl = createComponent<unknown, object>(
  { searchValue: $search, tableData: $tableData },
  (props, { searchValue, tableData }: any) => {
    return (
      <AdaptiveSearch
        {...{
          totalCount: tableData?.count || 0,
          searchValue,
          searchChanged,
          isAnySideOpen: false,
        }}
      />
    );
  }
);

export { SearchControl };
