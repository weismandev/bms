import { useStore } from 'effector-react';
import { $isMainAdd } from '@features/fabrics/add/models/add';
import { $instructions } from '@features/fabrics/add/models/add';
import { addClicked, openAddDetail } from '@features/fabrics/devices/detail/model/detail';
import { AddButton } from '@ui/index';
import { setControlAddButtonName, ControlAddButtonName } from '../../../models/controls/control-add-button';
import { useStyles } from './styles';

type Instructions = {
  control: {
    url: string;
  }
};

type Props = {
  name: ControlAddButtonName;
};

export const AddDeviceControl = (props: Props) => {
  const classes = useStyles();
  const instructions: Instructions = useStore($instructions) as Instructions;
  const isMainAdd = useStore($isMainAdd);

  const link = instructions?.control?.url ?? '' as string;

  const handleClick = (name: ControlAddButtonName) => () => {
    setControlAddButtonName(name);
    switch (name) {
      case 'add_device':
        addClicked();
        return;
      case 'add_entity':
        openAddDetail(link);
        return;
      default:
        // eslint-disable-next-line consistent-return
        return isMainAdd ? () => addClicked() : () => openAddDetail(link);
    }
  };

  return (
    <AddButton
      className={classes.margin}
      onClick={handleClick(props.name)}
    />
  );
};
