import { ImportButton, ExportButton } from '@ui/index';
import { useStyles } from './styles';

interface Props {
  name: string;
}

export const ButtonControl = ({ name }: Props) => {
  const classes = useStyles();

  switch (name) {
    case 'import':
      return <ImportButton className={classes.margin} />;
    case 'export':
      return <ExportButton className={classes.margin} />;
    default:
      return null;
  }
};
