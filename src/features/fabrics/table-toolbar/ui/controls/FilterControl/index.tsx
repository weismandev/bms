import React from 'react';
import { createComponent } from 'effector-react';
import { FilterButton } from '@ui/index';
import PageContext from '../../../../app/pageContext';
import { $isFilterOpen } from '../../../../processes/filter-control/model';

const FilterControl = createComponent($isFilterOpen, () => {
  return (
    <PageContext.Consumer>
      {({ filter: { isFilterOpen, visibleFilter } }: any) => (
        <FilterButton disabled={isFilterOpen} onClick={() => visibleFilter(true)} />
      )}
    </PageContext.Consumer>
  );
});

export { FilterControl };
