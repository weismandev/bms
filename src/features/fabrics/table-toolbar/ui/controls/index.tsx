import React from 'react';
import { AddDeviceControl } from './AddDeviceControl';
import { ButtonControl } from './ButtonControl';
import { FilterControl } from './FilterControl';
import { ModalButtonControl } from './ModalControl';
import { SearchControl } from './SearchControl';
import { UnknownControl } from './UnknownControl';

export const Controls: any = {
  show_filter: (props: any): JSX.Element => <FilterControl {...props} />,
  search: (props: any): JSX.Element => <SearchControl {...props} />,
  button: (props: any): JSX.Element => <ButtonControl {...props} />,
  add_device: (props: any): JSX.Element => <AddDeviceControl {...props} />,
  show_modal: (props: any): JSX.Element => <ModalButtonControl {...props} />,
  default: (props: any): JSX.Element => <UnknownControl {...props} />,
};
