import React, { useContext } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, Greedy } from '@ui/index';
import PageContext from '../../../app/pageContext';
import { ToolbarGate, $leftControls, $rightControls } from '../../models';
import { Control } from '../../types';

type MapControl = Control & {
  Component: any;
};

const Controls: React.FC<any> = ({ controls }) =>
  controls.map(({ Component, ...control }: MapControl) => (
    <Component key={control.name} {...control} />
  ));

const TableToolbar: React.FC = () => {
  const { page } = useContext(PageContext);

  const leftControls = useStore($leftControls) as any;
  const rightControls = useStore($rightControls) as any;

  return (
    <>
      <ToolbarGate page={page} />
      <Toolbar>
        <Controls controls={leftControls} />
        <Greedy />
        <Controls controls={rightControls} />
      </Toolbar>
    </>
  );
};

export { TableToolbar };
