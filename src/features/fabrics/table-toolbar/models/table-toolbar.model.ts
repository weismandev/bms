import { createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { Page, Control } from '../../shared/types';

export const ToolbarGate = createGate<Required<{ page: Page }>>({});

export const $controls = createStore<Control[] | []>([]);
export const $leftControls = $controls.map((controls) =>
  controls.filter((control) => control.position === 'left')
);
export const $rightControls = $controls.map((controls) =>
  controls.filter((control) => control.position === 'right')
);

export const setInstructionsToolbar = createEvent<Control[] | []>();
