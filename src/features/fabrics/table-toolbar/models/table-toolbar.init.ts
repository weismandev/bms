import instructionObserver from '../../processes/instructionObserver';
import { getControl } from '../lib';
import { ToolbarGate, setInstructionsToolbar, $controls } from './table-toolbar.model';

ToolbarGate.open.watch((state) => {
  const config = {
    page: state.page,
    path: 'filter.top',
  };
  instructionObserver.subscribe(config, setInstructionsToolbar);
});

$controls.on(setInstructionsToolbar, (_, result) =>
  result.map((item) => ({
    ...item,
    Component: getControl(item.control?.type),
  }))
);
