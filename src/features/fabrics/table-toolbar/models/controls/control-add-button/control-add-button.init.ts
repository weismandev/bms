import { Event } from 'effector';
import { openedLinkChanged, closeDetail } from '@features/fabrics/devices/detail/model/detail';
import { $controlAddButtonName, setControlAddButtonName } from './control-add-button.model';

$controlAddButtonName
  .on(setControlAddButtonName, (_, name) => name)
  .reset([openedLinkChanged, closeDetail as Event<void>]);
