import { createStore, createEvent } from 'effector';

import type { ControlAddButtonName } from './control-add-button.types';

export const $controlAddButtonName = createStore<ControlAddButtonName | null>(null);

export const setControlAddButtonName = createEvent<ControlAddButtonName>();
