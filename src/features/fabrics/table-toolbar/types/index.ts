export type ControlTypes = 'show_filter' | 'search';

export type Control = {
  id: number;
  main: number;
  name: string;
  float?: number;
  only_cloud?: number;
  settings_block?: number;
  label: {
    title: string;
    readonly: boolean;
  };
  state?: {
    change: number;
    value: string;
    measure: string;
    readonly: boolean;
    go_device: boolean;
  };
  control: {
    type: ControlTypes;
    urldata: string;
    graph: number;
  };
  settings_block_title: string;
  position: string;
};
