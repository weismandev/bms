import { ControlTypes } from '../types';
import { Controls } from '../ui/controls';

export const getControl = (type: string) => {
  if (type in Controls) return Controls[type];

  return Controls.default;
};
