import { useEffect } from 'react';
import { useStore, useGate } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { $openedLink } from '@features/fabrics/devices/detail/model/detail';
import { Wrapper, Loader } from '@ui/index';
import { DetailGate, fxGetDetail, $isLoading } from './model';
import { $currentTab, $options, $tabsButtons } from './processes/tabs';
import { TabPane } from './widgets/TabPane/ui';
import { Tabs } from './widgets/Tabs/ui';
import { Toolbar } from './widgets/Toolbar/ui';

const Detail = () => {
  const url = useStore($openedLink) || '';
  const options = useStore($options);
  const currentTab = useStore($currentTab);
  const isLoading = useStore($isLoading);
  const tabsButtons = useStore($tabsButtons);

  const currentTabConfig = options[currentTab] || {};
  const hideToolbar = currentTabConfig?.hideToolbar;

  const configTabsButtons = tabsButtons
    .filter((item: any) => item.settings_block_id === currentTabConfig.id)
    .reduce(
      (acc, current: any) => ({
        ...acc,
        [current.action]: current,
      }),
      {}
    );

  useGate(DetailGate, {
    url,
    externalContent: [],
  });

  useEffect(() => {
    fxGetDetail(url);
  }, [url]);

  return (
    <>
      <Wrapper
        style={{
          overflow: 'hidden',
          position: 'relative',
          height: '100%',
        }}
      >
        <Loader isLoading={isLoading} />
        <Tabs options={options} currentTab={currentTab} />
        {!hideToolbar && (
          <Toolbar
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            closeHandler={() => {}}
            mode={'edit'}
            configTabsButtons={configTabsButtons}
          />
        )}
        <TabPane mode="edit" hideToolbar={hideToolbar} serialnumber="serialnumber" />
      </Wrapper>
    </>
  );
};

export { Detail };
