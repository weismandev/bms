import { Controls } from '../../../shared/ui/controls';

export const getControl = (type) => {
  if (type in Controls) return Controls[type];

  return Controls.default;
};
