import { getListOfControlsByTabs } from './getListOfControlsByTabs';

export const controlsMapping = ({ data, externalContent }) => {
  if (!data) return null;

  return getListOfControlsByTabs(data, externalContent);
};
