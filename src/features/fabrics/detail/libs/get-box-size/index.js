const prefix = 'col-sm';

const tableSizes = [...new Array(12).keys()].reduce((acc, i) => {
  const size = i + 1;
  const key = `${prefix}-${size}`;

  return {
    ...acc,
    [key]: size,
  };
}, {});

export const getSize = (box) => {
  if ('tags' in box) {
    const size = box.tags.find((element) => element.slice(0, 6) === 'col-sm');

    if (size in tableSizes) {
      return tableSizes[size];
    }
  }

  return 12;
};
