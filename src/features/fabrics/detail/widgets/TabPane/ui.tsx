import { useStore } from 'effector-react';
import Grid from '@mui/material/Grid';
import makeStyles from '@mui/styles/makeStyles';
import { CustomScrollbar } from '@ui/index';
import { useForm } from '../../../shared/effector-uniform';
import { getSize } from '../../libs/get-box-size';
import { detailForm } from '../../processes/form';
import { changeMode, saveClicked } from '../../processes/form';
import { $slicedCurrentTabControls, sliceCurrentTabControls } from '../../processes/tabs';

const useStyles = makeStyles({
  wrapper: {
    padding: '6px 24px',
    height: '100%',
  },
});

export const TabPane = ({ hideToolbar, mode, serialnumber }: any) => {
  const classes = useStyles();

  const form = useForm(detailForm);

  const slicedCurrentTabControls: any = useStore($slicedCurrentTabControls);

  if (!slicedCurrentTabControls) return null;

  const isViewMode = mode === 'view';

  if ('Content' in slicedCurrentTabControls) {
    const { Content, title }: any = slicedCurrentTabControls;

    return (
      <Content
        isViewMode={isViewMode}
        changeMode={(mode: 'edit' | 'view') => changeMode(mode)}
        title={title}
      />
    );
  }

  const scrollbarStyles = hideToolbar
    ? { height: 'calc(100vh - 168px)' }
    : { height: 'calc(100vh - 220px)' };

  const viewBoxes = slicedCurrentTabControls?.boxes?.map((box: any) => {
    const size = getSize(box);
    return (
      <Grid container spacing={{ xs: 1, md: 2 }} key={box.id}>
        {box.signals?.map((signal: any) => (
          <Grid item md={size} key={signal.id}>
            <signal.control.Component
              value={form.fields[signal.name]?.value}
              onChange={form.fields[signal.name]?.change}
              error={form.errors[signal.name]}
              {...signal}
            />
          </Grid>
        ))}
      </Grid>
    );
  });

  return (
    <CustomScrollbar style={scrollbarStyles}>
      <div className={classes.wrapper}>{viewBoxes}</div>
    </CustomScrollbar>
  );
};
