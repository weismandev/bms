import { Tab } from '@mui/material';
import { changeTab } from '@features/fabrics/detail/processes/tabs';
import { Tabs as TabsWrap } from '@ui/index';

type Options = {
  disabled: boolean;
  hideToolbar: boolean;
  id: number;
  label: string;
  value: number;
};

type PropTabs = {
  options: Options[];
  currentTab: number;
};

export const Tabs = ({ options, currentTab }: PropTabs) => (
  <TabsWrap
    options={options}
    currentTab={currentTab}
    onChange={(e: any, tab: any) => changeTab(tab)}
    tabEl={<Tab />}
  />
);
