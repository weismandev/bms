import { useEffect } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { DetailToolbar } from '@ui/index';
import { useForm } from '../../../shared/effector-uniform';
import {
  changeMode,
  cancelClicked,
  archiveClicked,
  saveClicked,
  setAction,
  detailForm,
} from '../../processes/form';

const useStyles = makeStyles({
  wrapper: {
    margin: '10px 0 30px 0',
    padding: '0 15px',
  },
});

export const Toolbar = ({ closeHandler, mode, configTabsButtons }: any) => {
  const classes = useStyles();
  const form = useForm(detailForm);

  const handleArchive = () => archiveClicked(configTabsButtons?.archive?.url);

  const onSubmit = () => {
    form.event.validate(form.fields);
    saveClicked(form.fields);
  };

  useEffect(() => {
    setAction(
      Object.keys(configTabsButtons).reduce(
        (acc, current) => ({
          ...acc,
          [current]: {
            url: configTabsButtons[current]?.url,
            method: configTabsButtons[current]?.http_method || 'get',
          },
        }),
        {}
      )
    );
  }, [configTabsButtons]);

  const onDelete = () => {
    // console.log('onDelete');
  };

  return (
    <div className={classes.wrapper}>
      <DetailToolbar
        mode={mode}
        onEdit={() => changeMode('edit')}
        onClose={closeHandler}
        onCancel={() => {
          changeMode('view');
          cancelClicked();
        }}
        onArchive={handleArchive}
        onSave={onSubmit}
        onDelete={onDelete}
        hidden={{
          edit: !configTabsButtons?.edit,
          delete: !configTabsButtons?.delete,
          archive: !configTabsButtons?.archive,
        }}
      />
    </div>
  );
};
