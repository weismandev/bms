import { forward } from 'effector';
import { DetailGate, fxGetDetail, $data, $isLoading } from './detail.model';

$data.on(fxGetDetail.done, (_, { result: { data } }) => ({
  data,
  externalContent: null,
}));

$isLoading.on(fxGetDetail.pending, (_, isLoading) => isLoading);

forward({
  from: DetailGate.open,
  to: fxGetDetail.prepend(({ url }) => url),
});
