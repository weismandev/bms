import { createEvent, createStore, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';
import { controlsMapping } from '../libs/controlsMapping';

export const DetailGate = createGate<{ url: string; externalContent: any[] }>();

export const fxGetDetail = createEffect(async (url: string) => {
  const data = await fetch(url, {
    headers: {
      ...getBaseHeaders()
    }
  });

  return data.json();
});

export const fxGetSelectUrl = createEffect(async (payload: any) => {
  if (!payload) {
    return [];
  }
  const urlPromises = payload.map(async ({ urlSelect, parentId }: any) => {
    const response = await fetch(urlSelect, {
      headers: {
        ...getBaseHeaders()
      }
    });
    const { data } = await response.json();
    return data.signals.map((signal: any) => ({ ...signal, parentId }));
  });
  const result = await Promise.all(urlPromises);
  return result[0];
});

export const controlsProcessed = createEvent();

export const $isLoading = createStore(false);
export const $data = createStore({ data: null, externalContent: null });
export const $controls = $data.map(controlsMapping);
