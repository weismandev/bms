import { sample } from 'effector';
import { fxGetSelectUrl, $controls, controlsProcessed } from '../../model';
import { formatNewSignal, processNestedSignals } from './lib';
import {
  $currentTab,
  $slicedCurrentTabControls,
  $options,
  sliceCurrentTabControls,
  changeTab,
} from './tabs.model';

$currentTab.on(changeTab, (_, tab) => tab);

$slicedCurrentTabControls.on(sliceCurrentTabControls, formatNewSignal);

sample({
  source: $controls,
  clock: changeTab,
  fn: (sourceData: any, clockData: any) => sourceData[clockData],
  target: $slicedCurrentTabControls,
});
