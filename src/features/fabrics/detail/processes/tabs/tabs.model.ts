import { createEvent, createStore } from 'effector';
import { $controls } from '../../model/detail.model';
import { controlsTabMapping } from './lib';

export const $options = $controls.map(controlsTabMapping);
export const $tabsButtons = createStore([]);
export const $currentTab = createStore(0);
export const $currentTabControls = createStore(null);
export const $slicedCurrentTabControls = $controls.map((state) =>
  Array.isArray(state) ? state[0] : null
);

export const sliceCurrentTabControls = createEvent();
export const changeTab = createEvent();
