import { getControl } from '../../libs/controlsMapping/getControl';

export const controlsTabMapping = (tabs: any) => {
  if (!tabs || !Array.isArray(tabs)) return [];

  return tabs?.map((tab: any, index: any) => {
    const hideToolbar = Boolean(tab?.Content);

    return {
      id: tab.id,
      value: index,
      label: tab.title,
      disabled: false,
      hideToolbar,
    };
  });
};

export const formatNewSignal = (tab: any, new_signal: any) => {
  const currentViewBox = tab.boxes.find((box: any) => box.id === new_signal.boxId);
  if (!currentViewBox) return tab;
  const currentViewBoxNewSignals = currentViewBox.signals.map((signal: any) => {
    if (signal.name === new_signal.name) {
      return new_signal;
    }
    return signal;
  });

  const newCurrentViewBox = {
    ...currentViewBox,
    signals: currentViewBoxNewSignals,
  };

  const newBoxes = tab.boxes.map((box: any) => {
    if (box.id === newCurrentViewBox.id) {
      return newCurrentViewBox;
    }

    return box;
  });

  const newTab = {
    ...tab,
    boxes: newBoxes,
  };

  return newTab;
};

const enrichControl = ({ parent, newSignal }: any) => ({
  ...parent,
  ...newSignal,
  control: {
    ...newSignal.control,
    Component: getControl(newSignal.control?.type),
  },
});

const insertNewSignal = (tab: any, newSignal: any) => {
  const { boxes } = tab;
  for (const box of boxes) {
    const parentIndex = box?.signals?.findIndex(
      (signal: any) => signal.id === newSignal.parentId
    );
    if (parentIndex === -1) {
      continue;
    }
    const parent = box.signals[parentIndex];
    const enrichedControl = enrichControl({ parent, newSignal });

    const dublicateIndex = box?.signals?.findIndex(
      (signal: any) => signal.id === newSignal.id
    );

    if (dublicateIndex !== -1) {
      box.signals[dublicateIndex] = enrichedControl;
    } else {
      box.signals.splice(parentIndex + 1, 0, enrichedControl);
    }

    return true;
  }
  return null;
};

export const processNestedSignals = (sourceData: any) => {
  const newSignals: any = [];
  if (!Array.isArray(newSignals) || newSignals.length === 0) {
    return sourceData;
  }
  newSignals.forEach((newSignal) => {
    for (const tab of sourceData) {
      const done = insertNewSignal(tab, newSignal);
      if (done) {
        break;
      }
    }
  });
  return sourceData;
};
