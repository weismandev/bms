import { forward } from 'effector';
import { $slicedCurrentTabControls } from '../tabs';
import { detailForm } from './form.model';

forward({
  from: [$slicedCurrentTabControls],
  to: detailForm.event.addField.prepend((params: any) => {
    const filteredByText = params.boxes
      .map(({ signals }: any) =>
        signals.filter(({ control }: any) => control.type === 'text')
      )
      .flat()
      .reduce(
        (acc: any, item: any) => ({
          ...acc,
          [item.name]: { value: item.state.value, required: true },
        }),
        {}
      );
    return filteredByText;
  }),
});
