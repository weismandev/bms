import { createEvent } from 'effector';
import { createForm } from '../../../shared/effector-uniform';
import { Fields, Action } from '../../../shared/effector-uniform/types';

export const changeMode = createEvent<'view' | 'edit'>();
export const cancelClicked = createEvent();
export const archiveClicked = createEvent();
export const saveClicked = createEvent<Fields>();
export const setAction = createEvent<Action>();

export const detailForm = createForm({
  fields: {},
  submitted: saveClicked,
});
