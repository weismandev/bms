import React, { createContext } from 'react';
import { Page } from '../shared/types';

export type PageContextType = {
  page: Page;
  filter: {
    isFilterOpen: boolean;
    visibleFilter: React.Dispatch<React.SetStateAction<boolean>> | Function;
  };
};

const PageContext = createContext<Required<PageContextType>>({
  page: 'doors',
  filter: {
    isFilterOpen: false,
    visibleFilter: () => Function,
  },
});

export default PageContext;
