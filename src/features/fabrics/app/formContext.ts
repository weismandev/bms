import { createContext } from 'react';
import { Form } from '../shared/effector-uniform/types';
import { Control } from '../shared/types';

const FormContext = createContext<Partial<{ form: Form; controls: Control }>>({});

export default FormContext;
