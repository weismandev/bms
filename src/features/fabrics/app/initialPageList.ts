import { Page } from '../shared/types';

const pageList: Page[] = [
  'boundsactions',
  'catalogdevice',
  'doors',
  'garbage',
  'keys',
  'accessgroups',
  'devicehints',
];

export default pageList;
