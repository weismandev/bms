import { api } from '@api/api2';
import { getCookieNameWithBuildType } from '@tools/cookie';
import { getBaseHeaders } from '@api/tools/getBaseHeaders';

const cookieMap = new Map(
  document.cookie.split(';').map((cookie) => cookie.trim().split('=', 2))
);

const getFromLink = async (url) => {
  const tokenName = getCookieNameWithBuildType('bms_token');
  const token = cookieMap.get(tokenName);
  const data = await fetch(`${url}&token=${token}`, {
    headers: {
      ...getBaseHeaders()
    }
  });
  return data.json();
};

const addDevice = async ({ url, payload }) => {
  const payloadString = `${Object.keys(payload).map(
    (item) => `&${item}=${payload[item]}`
  )}`
    .split(',')
    .join('')
    .split('&')
    .filter((item) => item && item.length > 0)
    .reduce((acc, value) => {
      const splittedValue = value.split('=');

      const formattedValue = { [splittedValue[0]]: splittedValue[1] };

      return { ...acc, ...formattedValue };
    }, {});

  const splittedUrl = url.split('?');

  const queryUrl = splittedUrl[0];
  const paramsInUrl =
    splittedUrl[1] && splittedUrl[1].length > 0
      ? splittedUrl[1]
          .split('&')
          .filter((item) => !item.includes('token'))
          .reduce((acc, value) => {
            const splittedValue = value.split('=');

            const formattedValue = { [splittedValue[0]]: splittedValue[1] };

            return { ...acc, ...formattedValue };
          }, {})
      : null;

  const queryPayload = Boolean(paramsInUrl)
    ? { ...payloadString, ...paramsInUrl }
    : payloadString;

  return api.no_vers('get', queryUrl, queryPayload);
};

const getListDevices = (payload = {}) =>
  api.no_vers('get', 'bmsdevice/get-devices', payload);

const getDevices = (payload = {}, config = {}) =>
  api.no_vers('get', 'bmsdevice/get-devices', payload, config);

const exportList = (payload) =>
  api.no_vers(
    'request',
    'bmsdevice/get-devices',
    { ...payload, limit: 1000000, export: 1 },
    {
      config: {
        responseType: 'arraybuffer',
      },
    }
  );

const getDeviceTypes = (payload = {}, config = {}) =>
  api.no_vers('get', 'devices/get-device-type', payload, config);

const addMainDevice = (payload = {}, config = {}) =>
  api.no_vers('get', 'devices/add-device', payload, config);

const getRooms = (payload = {}, config = {}) =>
  api.no_vers('get', 'bmsdevice/get-rooms/', payload, config);

const nextStep = (url) => api.no_vers('get', url);

const getDiviceInfoByProxy = (proxy) => api.no_vers('get', proxy);

export const addApi = {
  addDevice,
  getFromLink,
  getListDevices,
  getDevices,
  exportList,
  getDeviceTypes,
  addMainDevice,
  getRooms,
  nextStep,
  getDiviceInfoByProxy,
};
