import { useMemo } from 'react';
import { useStore, useUnit } from 'effector-react';
import i18n from '@shared/config/i18n';
import getURLParams from '../../../lib';

import {
  $deviceTypes,
  $devices,
  $deviceForm,
  $deviceError,
  $isLoadingDevice,
  $qrPayload,
  $payloadByNextStep,
  nextStep,
  setQrPayload,
  changedDeviceVisibility,
  updateDeviceFormStore,
  setPayloadByNextStep,
  $currentStepAddDevice,
  $stepsAddDevice,
  setDeviceSignals,
  $deviceSignals,
} from '../../../models/add-device';
import { setSignal, $signals } from '../../../models/dynamic';

import AuthorizationForm from '../forms/authorizationForm';
import DeviceForm from '../forms/deviceForm';
import DeviceListBySelectedType from '../forms/deviceListBySelectedType';
import DeviceListByType from '../forms/deviceListByType';
import DeviceQrForm from '../forms/deviceQrForm';
import { SignalsForm } from '../forms/SignalsForm';

import useStyles from '../styles';

const { t } = i18n;

export const AddDeviceForm = ({ type }) => {
  const classes = useStyles();
  const devices = useStore($deviceTypes);
  const deviceForm = useStore($deviceForm);
  const devicesList = useStore($devices);
  const error = useStore($deviceError);
  const isLoading = useStore($isLoadingDevice);
  const qrPayload = useStore($qrPayload);
  const payloadByNextStep = useStore($payloadByNextStep);
  const currentStepAddDevice = useStore($currentStepAddDevice);
  const stepsAddDevice = useStore($stepsAddDevice);
  const deviceSignals = useStore($deviceSignals);
  const signals = useStore($signals);

  const { title, serialnumber, login, password, room_id } = deviceForm;

  const handleSelectDeviceType = (deviceName) => {
    const { instruction } = devices.find(({ name }) => name === deviceName);

    const urlParams = getURLParams(instruction.next_step);
    setPayloadByNextStep(urlParams);

    const handleUpdateDeviceFormStore = (isSubmittedForm) =>
      updateDeviceFormStore({
        ...urlParams,
        isSubmittedForm,
      });

    switch (instruction.type) {
      case 'qr':
        nextStep('qr');
        setQrPayload(instruction.edit);
        handleUpdateDeviceFormStore(false);
        break;
      case 'logopass':
        nextStep('logopass');
        setQrPayload({ title: instruction.title });
        handleUpdateDeviceFormStore(false);
        break;
      case 'dynamic':
        nextStep('dynamic');
        setQrPayload({ title: 'dynamic' });
        handleUpdateDeviceFormStore(true);
        break;
      case 'default':
        nextStep('deviceList');
        handleUpdateDeviceFormStore(true);
        break;
      default: {
        break;
      }
    }
  };

  const handleSelectDeviceDynamic = (serial) => {
    const foundedDevice = devicesList.find((device) => device.serial === serial);
    if (foundedDevice) {
      setSignal(foundedDevice.signals);
    }
  };

  const handleCancel = () => changedDeviceVisibility(false);
  const handleBack = () => nextStep('typeDevice');

  const handleSubmitNextStep = (step) => (values) => {
    const { signals = [] } =
      devicesList.find(({ serial }) => serial === values.serialnumber) || {};
    updateDeviceFormStore({
      ...values,
      ...payloadByNextStep,
      isSubmittedForm: false,
    });
    setDeviceSignals({
      [values.serialnumber]: signals,
    });
    nextStep(step);
  };

  const handleSubmitByQrType = (values) => {
    const { name, step, type } = deviceForm;
    updateDeviceFormStore({
      ...values,
      ...payloadByNextStep,
      name,
      step,
      type,
      isSubmittedForm: true,
    });
  };

  const handleSubmitSendForm = (values) => {
    updateDeviceFormStore({
      ...values,
      ...payloadByNextStep,
      isSubmittedForm: true,
    });
  };

  const renderDeviceType = useMemo(
    () => (
      <DeviceListByType
        error={error}
        isLoading={isLoading}
        devicesList={devices}
        onSubmit={handleSelectDeviceType}
        onCancel={handleCancel}
      />
    ),
    [isLoading, deviceForm.name, devices]
  );

  const renderDeviceList = useMemo(
    () => (
      <DeviceListBySelectedType
        isLoading={isLoading}
        devicesList={devicesList}
        onSubmit={handleSubmitNextStep('deviceForm')}
        onCancel={handleCancel}
        onBack={handleBack}
      />
    ),
    [devicesList]
  );

  const renderQr = useMemo(
    () => (
      <DeviceQrForm
        isLoading={isLoading}
        initialValues={{ code: serialnumber }}
        qrPayload={qrPayload}
        error={error}
        onSubmit={handleSubmitByQrType}
        onBack={handleBack}
      />
    ),
    [qrPayload, error, isLoading]
  );

  const renderAuthorization = useMemo(
    () => (
      <AuthorizationForm
        initialValues={{
          login,
          password,
        }}
        title={qrPayload.title}
        error={error}
        onSubmit={handleSubmitSendForm}
        onBack={handleBack}
      />
    ),
    [deviceForm.login, error]
  );

  const renderDeviceForm = useMemo(
    () => (
      <DeviceForm
        initialValues={{
          title,
          serialnumber,
          room_id,
        }}
        deviceSignals={deviceSignals}
        isSerialNumber={qrPayload.h2 === t('serialNumber')}
        error={error}
        onSubmit={handleSubmitSendForm}
        onBack={handleBack}
      />
    ),
    [deviceForm.title, deviceForm.serialnumber, error]
  );

  const renderDynamicForm = useMemo(() => (
    <DeviceListByType
      error={error}
      isLoading={isLoading}
      devicesList={devicesList}
      onSubmit={handleSelectDeviceDynamic}
      onCancel={handleCancel}
    />
  ), [devicesList]);

  // const renderSuccess = useMemo(
  //   () => (
  //     <div className={classes.wrap}>
  //       <div className={classes.centerContent}>
  //         <h3>Устройство успешно добавлено</h3>
  //       </div>
  //       <div className={classes.btnGroup}>
  //         {/* <ActionButton
  //           kind="positive"
  //           onClick={handleCancel}
  //           style={{ marginLeft: 10 }}
  //         >
  //           Закрыть
  //         </ActionButton> */}
  //       </div>
  //     </div>
  //   ),
  //   []
  // );

  const collection = {
    typeDevice: renderDeviceType,
    deviceList: renderDeviceList,
    logopass: renderAuthorization,
    qr: renderQr,
    deviceForm: renderDeviceForm,
    dynamic: renderDynamicForm
  };

  return (
    <div>
      <span className={classes.mainTitle}>
        {stepsAddDevice[currentStepAddDevice].title}
      </span>

      {signals.length === 0 && collection[type]}

      <SignalsForm serialnumber={serialnumber} />
    </div>
  );
};
