import { ActionButton, BaseInputErrorText, InputField, Loader } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import useStyles from '../styles';

const { t } = i18n;

const DeviceTitleSchema = Yup.object().shape({
  code: Yup.string().required(t('thisIsRequiredField')),
});

const DeviceQrForm = ({
  isLoading,
  qrPayload,
  initialValues,
  onSubmit,
  error,
  onBack,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.wrap}>
      <p>{qrPayload.h1}</p>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Loader isLoading={isLoading} />
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={DeviceTitleSchema}
        render={({ errors, touched }) => (
          <Form style={{ paddingTop: 24 }}>
            <Field
              name="code"
              label={qrPayload.button_label}
              placeholder={qrPayload.placeholder}
              component={InputField}
            />
            {errors.login && touched.login && <div>{errors.login}</div>}
            <div className={classes.btnGroup}>
              <ActionButton kind="basic" type="submit">
                {t('Proceed')}
              </ActionButton>
              <ActionButton kind="positive" onClick={onBack} style={{ marginLeft: 10 }}>
                {t('Back')}
              </ActionButton>
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default DeviceQrForm;
