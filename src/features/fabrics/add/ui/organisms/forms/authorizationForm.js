import { ActionButton, BaseInputErrorText, InputField } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import useStyles from '../styles';

const { t } = i18n;

const AuthorizationSchema = Yup.object().shape({
  login: Yup.string().required(t('thisIsRequiredField')),
  password: Yup.string().required(t('thisIsRequiredField')),
});

const AuthorizationForm = ({ initialValues, title, onSubmit, error, onBack }) => {
  const classes = useStyles();
  return (
    <div className={classes.wrap}>
      <p>{title}</p>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={AuthorizationSchema}
        render={({ errors, touched }) => (
          <Form style={{ paddingTop: 24 }}>
            <Field
              name="login"
              label={t('login')}
              placeholder={t('Enterlogin')}
              component={InputField}
            />
            {errors.login && touched.login && <div>{errors.login}</div>}
            <Field
              name="password"
              label={t('password')}
              placeholder={t('Enterpassword')}
              type="password"
              component={InputField}
            />
            {errors.password && touched.password && <div>{errors.password}</div>}
            <div className={classes.btnGroup}>
              <ActionButton kind="basic" type="submit">
                {t('ToPlug')}
              </ActionButton>
              <ActionButton kind="positive" onClick={onBack} style={{ marginLeft: 10 }}>
                {t('Back')}
              </ActionButton>
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default AuthorizationForm;
