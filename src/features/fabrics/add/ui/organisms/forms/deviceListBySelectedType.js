import { Loader, ActionButton, CustomScrollbar } from '@ui';
import { List, ListItem, ListItemText } from '@mui/material';
import useStyles from '../styles';

const DeviceListBySelectedType = ({
  isLoading,
  devicesList,
  onSubmit,
  onCancel,
  onBack,
}) => {
  const classes = useStyles();

  const handleSelectDevice = (serialnumber) => () => onSubmit({ serialnumber });

  return (
    <div className={classes.wrap}>
      <Loader isLoading={isLoading} />
      <List component="nav" className={classes.content}>
        <CustomScrollbar style={{ height: 'calc(100vh - 300px)' }}>
          {devicesList.map(({ serial, title }) => (
            <ListItem button onClick={handleSelectDevice(serial)} key={serial}>
              <ListItemText primary={title} />
            </ListItem>
          ))}
        </CustomScrollbar>
      </List>
      <div className={classes.btnGroup}>
        {/* <ActionButton kind="basic" onClick={onCancel}>
          Отмена
        </ActionButton> */}
        {/* <ActionButton
          kind="negative-outlined"
          onClick={onBack}
          style={{ marginLeft: 10 }}
        >
          Назад
        </ActionButton> */}
      </div>
    </div>
  );
};

export default DeviceListBySelectedType;
