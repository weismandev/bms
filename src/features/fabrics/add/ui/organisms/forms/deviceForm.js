import { ActionButton, BaseInputErrorText, InputField } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { ComboListControlField } from '@entities/ComboListControl';
import { SelectControlField } from '@entities/SelectControl';
import { HouseSelectField } from '@features/house-select';
import { RoomsSelectField, changeBuilding } from '@features/rooms-select';
import i18n from '@shared/config/i18n';
import useStyles from '../styles';

const { t } = i18n;

const YupObject = Yup.object().nullable().required(t('thisIsRequiredField'));
const YupArray = Yup.array().nullable().required(t('thisIsRequiredField'));
const YupString = Yup.string().required(t('thisIsRequiredField'));

const schemaByType = {
  list: YupString,
  combolist: YupArray,
  text: YupString,
};

const DeviceFormSchema = (schemaSignals) =>
  Yup.object().shape({
    building_id: YupObject,
    title: YupString,
    ...schemaSignals.reduce(
      (acc, curr) => ({
        ...acc,
        [curr.name]: schemaByType[curr.type],
      }),
      {}
    ),
  });

const getComponent = (signal, setFieldValue) => {
  const handleChange = (type) => (value) => {
    if (type === 'list') {
      setFieldValue(signal.name, value.id);
    } else {
      setFieldValue(
        signal.name,
        value.map(({ id }) => id)
      );
    }
  };

  switch (signal.type) {
    case 'list':
      return {
        onChange: handleChange(signal.type),
        component: SelectControlField,
        path: 'data.list',
      };
    case 'combolist':
      return {
        isMulti: true,
        kind: 'async',
        cacheOptions: true,
        isLoadOptions: true,
        onChange: handleChange(signal.type),
        component: ComboListControlField,
      };
    default:
      return {
        component: InputField,
      };
  }
};

const DeviceForm = (props) => {
  const classes = useStyles();
  const { initialValues, error, onSubmit, deviceSignals } = props;

  const signals = deviceSignals[initialValues.serialnumber] || [];
  const schemaSignals = signals.filter((signal) => Boolean(signal.required));

  const handleSubmit = (values) => {
    onSubmit({
      ...values,
      step: 3,
      building_id: values?.building_id?.id,
      room_id: values?.room_id?.id,
    });
  };

  const handleChangeBuilding = (setFieldValue) => (value) => {
    changeBuilding(value ? value.id : '');
    setFieldValue('building_id', value);
  };

  const validationSchema = DeviceFormSchema(schemaSignals);

  return (
    <div className={classes.wrap}>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Formik
        initialValues={{
          building_id: undefined,
          ...initialValues,
          ...signals.reduce((acc, curr) => ({ ...acc, [curr.name]: undefined }), {}),
          factory_number: 73263509,
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, errors, touched, setFieldValue }) => (
          <Form style={{ paddingTop: 24 }}>
            <Field
              name="title"
              label={t('NameTitle')}
              placeholder={t('EnterDeviceName')}
              component={InputField}
            />
            <Field
              name="building_id"
              label={t('Label.address')}
              placeholder={t('ChooseAddress')}
              component={HouseSelectField}
              onChange={handleChangeBuilding(setFieldValue)}
              required
            />
            <Field
              name="room_id"
              label={t('DevicePremises')}
              placeholder={t('ChooseRoom')}
              component={RoomsSelectField}
              isDisabled={!values.building_id}
            />
            {/* {!isSerialNumber && (
                <>
                  <Field
                    name="serialnumber"
                    label="Серийный номер"
                    placeholder="Введите серийный номер для подключения устройства"
                    component={InputField}
                  />
                  {errors.serialnumber && touched.serialnumber && (
                    <div>{errors.serialnumber}</div>
                  )}
                </>
              )} */}
            {/* <Field
                name="factory_number"
                label={t('FactoryNumber')}
                placeholder={t('EnterFactoryNumber')}
                component={InputField}
              /> */}
            {signals.map((signal) => (
              <div key={signal.name}>
                <Field
                  name={signal.name}
                  label={signal.title}
                  placeholder={signal.placeholder}
                  url={signal.url}
                  required={Boolean(signal.required)}
                  visible
                  {...getComponent(signal, setFieldValue, values)}
                  error={errors[signal.name] && touched[signal.name]}
                  errors={
                    (errors[signal.name] &&
                      touched[signal.name] && [errors[signal.name]]) ||
                    []
                  }
                />
              </div>
            ))}
            <div className={classes.btnGroup}>
              <ActionButton kind="basic" type="submit">
                {t('AddDevice')}
              </ActionButton>
              {/* <ActionButton
                  kind="positive"
                  onClick={onBack}
                  style={{ marginLeft: 10 }}
                >
                  Отмена
                </ActionButton> */}
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default DeviceForm;
