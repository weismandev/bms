import { Loader, ActionButton, BaseInputErrorText, CustomScrollbar } from '@ui';
import { List, ListItem, ListItemText } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import useStyles from '../styles';

const DeviceListByType = ({ isLoading, error, devicesList, onSubmit, onCancel }) => {
  const classes = useStyles();

  const handleSelectDevice = (name) => () => onSubmit(name);

  return (
    <div className={classes.wrap}>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Loader isLoading={isLoading} />
      <List component="nav" className={classes.content}>
        <CustomScrollbar style={{ height: 'calc(100vh - 300px)' }}>
          {devicesList.map(({ name, title, serial }) => (
            <ListItem button onClick={handleSelectDevice(name ?? serial)} key={name ?? serial}>
              <ListItemText primary={title} />
            </ListItem>
          ))}
        </CustomScrollbar>
      </List>
      <div className={classes.btnGroup}>
        {/* <ActionButton onClick={onCancel}>
          <CloseIcon />
          Отмена
        </ActionButton> */}
      </div>
    </div>
  );
};

export default DeviceListByType;
