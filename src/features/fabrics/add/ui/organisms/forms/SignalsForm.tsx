import { useUnit } from 'effector-react';
import { Grid, Button } from '@mui/material';
import { implementParentParams } from '../../../../devices/detail/lib/implementParentParams';
import { implementParentSignals } from '../../../../devices/detail/lib/implementParentSignals';

import { $signals, updateSignal, saveDynamicForm, Control } from '../../../models/dynamic';

type Props = {
  serialnumber: string;
};

export const SignalsForm = ({ serialnumber }: Props) => {
  const signals = useUnit($signals);

  const sliceCurrentTabControls = (datas: any, params: any) => {
    console.log('data, params', datas, params);
  };

  const handleInterceptorControl = ({ name, state }: Control) => {
    updateSignal({
      name,
      value: state.value
    });
  };

  const handleSave = () => {
    saveDynamicForm(serialnumber);
  };

  return (
    <div style={{ overflow: 'scroll' }}>
      {signals.map((signal) => {
        const { Component } = signal.control;
        const config = implementParentSignals(signal, signals);
        const params = implementParentParams(signal, signals);

        return (
          <Grid key={signal.name} item>
            <Component
              isViewMode={false}
              boxId={signal.name}
              info={signal}
              sliceCurrentTabControls={sliceCurrentTabControls}
              serialnumber={serialnumber}
              params={params}
              interceptor={handleInterceptorControl}
              {...config}
              // {...props}
            />
          </Grid>
        );
      })}
      <Button color="primary" onClick={handleSave}>
        Сохранить
      </Button>
    </div>
  );
};
