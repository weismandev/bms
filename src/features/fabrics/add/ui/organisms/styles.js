import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  wrap: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: '0 24px',
  },
  content: {
    height: '100%',
    overflow: 'auto',
  },
  centerContent: {
    height: '90%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainTitle: {
    padding: '0 24px',
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnGroup: {
    bottom: 20,
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  direction: {
    flexDirection: 'column',
  },
});

export default useStyles;
