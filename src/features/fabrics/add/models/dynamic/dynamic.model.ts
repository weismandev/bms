import { createEvent, createStore } from 'effector';

import type { Signal, Control, UpdateSignal } from './dynamic.types';

type DynamicForm = {
  [key: string]: string;
};

export const setSignal = createEvent<any[]>();
export const updateSignal = createEvent<UpdateSignal>();
export const saveDynamicForm = createEvent<string>();

export const $signals = createStore<Control[]>([]);
export const $signalInitialValues = createStore<DynamicForm>({});
