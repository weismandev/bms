export type ControlType = 'list' | 'text';

export type UpdateSignal = {
  name: string;
  value: string;
};

export type Signal = {
  id: number;
  control: {
    type: ControlType;
  };
  label: {
    title: string;
  };
  list?: {
    id: string;
    title: string;
  }[];
  name: string;
  state: {
    readonly: boolean;
    value: string;
  }
};

export type Control = Signal & {
  control: {
    Component: (props: any) => JSX.Element
  }
};
