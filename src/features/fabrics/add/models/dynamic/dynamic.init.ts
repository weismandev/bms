import { sample } from 'effector';
import { getControl } from '../../../devices/detail/lib/controlsMapping/getControl';

import { $signals, setSignal, updateSignal, saveDynamicForm } from './dynamic.model';
import { fxDeviceAdd, $payloadByNextStep } from '../add-device';

sample({
  clock: setSignal,
  fn: (signals) => signals.map((signal) => ({
    ...signal,
    control: {
      ...signal.control,
      Component: getControl(signal.control.type)
    }
  })),
  target: $signals
});

sample({
  clock: updateSignal,
  source: $signals,
  fn: (signals, { name, value }) => signals.map((signal) => {
    if (signal.name === name) {
      return {
        ...signal,
        state: {
          ...signal.state,
          value
        }
      };
    }
    return signal;
  }),
  target: $signals
});

sample({
  clock: saveDynamicForm,
  source: [$payloadByNextStep, $signals],
  fn: ([payload, signals], serialnumber) => ({
    serialnumber,
    ...payload,
    ...signals.reduce((acc, curr) => ({
      ...acc,
      [curr.name]: curr.state.value
    }), {})
  }),
  target: fxDeviceAdd
});
