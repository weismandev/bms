import { createStore, createEvent, createEffect } from 'effector';

const fxAddDevice = createEffect();
const fxGetNextStep = createEffect();

const setInstructions = createEvent();
const valueChanged = createEvent();
const addSubmitted = createEvent();
const submit = createEvent();
const addedSuccess = createEvent();

const $instructions = createStore({});
const $signals = createStore(null);
const $controls = createStore(null);
const $isMainAdd = createStore(false);
const $successData = createStore(null);
const $isAddLoading = createStore(false);
const $addError = createStore(null);

export {
  $instructions,
  $signals,
  $controls,
  setInstructions,
  valueChanged,
  addSubmitted,
  fxAddDevice,
  fxGetNextStep,
  $isMainAdd,
  submit,
  addedSuccess,
  $successData,
  $isAddLoading,
  $addError,
};
