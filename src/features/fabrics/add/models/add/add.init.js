import { sample, merge } from 'effector';
import { signout } from '@features/common';
import {
  addClicked,
  openedLinkChanged,
} from '@features/fabrics/devices/detail/model/detail';
import instructionObserver from '@features/fabrics/processes/instructionObserver';
import {
  InstructionGate,
  instructionUnmounted,
} from '@features/fabrics/processes/instructions/models/instructions';
import { needUpdateTable } from '@features/fabrics/table/model';
import { addApi } from '../../api';
import {
  $instructions,
  setInstructions,
  addSubmitted,
  fxAddDevice,
  fxGetNextStep,
  $isMainAdd,
  $successData,
  $isAddLoading,
  $addError,
} from './add.model';

fxAddDevice.use(addApi.addDevice);
fxGetNextStep.use(addApi.getFromLink);

const requestOccured = [fxAddDevice.pending, fxGetNextStep.pending];

const errorOccured = merge([fxAddDevice.failData, fxGetNextStep.failData]);

$isAddLoading
  .on(requestOccured, (_, pending) => pending)
  .reset([instructionUnmounted, signout]);

$addError
  .on(errorOccured, (_, error) => error)
  .on(requestOccured, () => null)
  .reset([instructionUnmounted, signout]);

// Подписваемся на гейт с инструкциями
InstructionGate.open.watch((state) => {
  const config = {
    page: state.page,
    path: 'filter.top',
  };
  instructionObserver.subscribe(config, setInstructions);
});

// Форматируем инструкции
$instructions
  .on(setInstructions, (_, result) => result.find((item) => item.control.type === 'add_device'))
  .reset([instructionUnmounted, signout]);

// Вычислыем какой функционал добавления будет использован
$isMainAdd
  .on(setInstructions, (_, result) => {
    const addDeviceInstructions = result.find(
      (item) => item.control.type === 'add_device'
    );
    return addDeviceInstructions?.name === 'add_device';
  })
  .reset([instructionUnmounted, signout]);

// Пробрасываем инструкции по контролам в локальный стор
// sample({
//   source: $instructions,
//   fn: (data) => {
//     if (data.length)
//       return data.signals
//         .map((item) => {
//           const type = item.control.type;
//           // Временный фильтр против иных типов контролов
//           if (type == 'text' || type == 'list') {
//             return {
//               ...item,
//               list: item.list?.map((item) => ({
//                 key: item.serial,
//                 value: item.title,
//               })),
//               value: '',
//             };
//           }
//         })
//         .filter((item) => item);
//   },
//   target: $controls,
// });

// Запрашиваем ссылку для добавления устройства
sample({
  clock: addClicked,
  source: $instructions,
  fn: (source) => source.control.urldata,
  target: fxGetNextStep,
});

// Отправляем добавленное устройство
sample({
  source: $instructions,
  clock: addSubmitted,
  fn: (source, clock) => ({ source, clock }),
  target: fxAddDevice.prepend((data) => {
    const signals = data.clock.boxes.map((item) => item.signals).flat();
    const addLink = data.clock.url;

    const payload = signals.reduce(
      (obj, item) =>
        Object.assign(obj, {
          [item.name]: item.state.value,
        }),
      {}
    );

    return {
      url: addLink.url,
      payload,
    };
  }),
});

// Обрабатываем успешное добавление
$successData
  .on(fxAddDevice.done, (_, { result }) => result)
  .reset([instructionUnmounted, addClicked, signout]);

// При успешном добавлении
// вставляем ссылку на новый девайс в detail и обновляем таблицу
sample({
  source: $successData,
  fn: (data) => data.link,
  target: [openedLinkChanged],
});
sample({
  clock: $successData,
  target: [needUpdateTable],
});
