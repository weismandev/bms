import { createStore, createEvent, createEffect } from 'effector';
import i18n from '@shared/config/i18n';

export const fxDeviceTypes = createEffect();
export const fxDeviceAdd = createEffect();
export const fxDeviceNextStep = createEffect();
export const fxDeviceRooms = createEffect();
export const fxGetDeviceInfoByProxy = createEffect();

const { t } = i18n;

export const defaultDeviceForm = {
  isSubmittedForm: true,
  name: '',
  step: 1,
  login: '',
  password: '',
  serialnumber: '',
  code: '',
  title: '',
  room_id: '',
  type: '',
};

export const defaultQrPayload = {
  title: '',
  button_label: '',
  h1: '',
  h2: '',
  placeholder: '',
};

export const $deviceForm = createStore(defaultDeviceForm);
export const $isSubmittedForm = createStore(true);
export const $isDeviceOpen = createStore(false);
export const $deviceTypes = createStore([]);
export const $devices = createStore([]);
export const $deviceError = createStore([]);
export const $isLoadingDevice = createStore(false);
export const $qrPayload = createStore(defaultQrPayload);
export const $payloadByNextStep = createStore({});
export const $deviceSignals = createStore([]);

export const $currentStepAddDevice = createStore('typeDevice');
export const $stepsAddDevice = createStore({
  typeDevice: {
    title: t('NewDevice'),
  },
  deviceList: {
    title: t('AddingANewDevice'),
  },
  logopass: {
    title: t('NewDevice'),
  },
  qr: {
    title: t('NewDevice'),
  },
  dynamic: {
    title: t('NewDevice'),
  },
  deviceForm: {
    title: t('NewDevice'),
  },
  success: {
    title: t('NewDevice'),
  },
});

export const nextStep = createEvent();
export const changedDeviceVisibility = createEvent();
export const setQrPayload = createEvent();
export const updateDeviceFormStore = createEvent();
export const toggleSubmitform = createEvent();
export const setPayloadByNextStep = createEvent();
export const setDeviceSignals = createEvent();
