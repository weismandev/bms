import { guard, sample } from 'effector';
import { signout } from '@features/common';
import {
  addClicked,
  addModeChanged,
} from '@features/fabrics/devices/detail/model/detail';
import { instructionUnmounted } from '@features/fabrics/processes/instructions/models/instructions';
import { needUpdateTable } from '@features/fabrics/table/model';
import { fxGetList } from '@features/house-select';
import { addApi } from '../../api';
import getURLParams from '../../lib';
import {
  $currentStepAddDevice,
  fxDeviceTypes,
  fxDeviceAdd,
  fxDeviceNextStep,
  fxDeviceRooms,
  fxGetDeviceInfoByProxy,
  $deviceTypes,
  $devices,
  $deviceForm,
  $deviceError,
  $isLoadingDevice,
  $isDeviceOpen,
  $payloadByNextStep,
  nextStep,
  updateDeviceFormStore,
  defaultQrPayload,
  $qrPayload,
  setQrPayload,
  defaultDeviceForm,
  $deviceSignals,
  setDeviceSignals,
} from './add-device.model';

fxDeviceTypes.use(addApi.getDeviceTypes);
fxDeviceAdd.use(addApi.addMainDevice);
fxDeviceNextStep.use(addApi.nextStep);
fxGetDeviceInfoByProxy.use(addApi.getDiviceInfoByProxy);

$currentStepAddDevice
  .on(nextStep, (_, step) => (typeof step === 'string' ? step : step.params.step))
  .on(fxDeviceAdd.fail, (step) => (step === 'deviceList' ? 'typeDevice' : step))
  .on(fxDeviceAdd.done, (step) => {
    if (step === 'qr') return 'deviceForm';
    return step === 'deviceForm' ? 'success' : step;
  })
  .on(addClicked, () => 'typeDevice')
  .reset([instructionUnmounted, signout]);

$isDeviceOpen.on(addClicked, () => true).reset([instructionUnmounted, signout]);

$qrPayload
  .on(setQrPayload, (_, payload) => ({ ...defaultQrPayload, ...payload }))
  .on(addClicked, (state, isOpen) => (!isOpen ? defaultQrPayload : state))
  .reset([instructionUnmounted, signout]);

$deviceForm
  .on(updateDeviceFormStore, (_, payload) => payload)
  .on(fxDeviceTypes.done, (state) => ({ ...state, step: 2 }))
  .on(fxDeviceAdd.done, (state, { params, result: { devices = [] } }) => {
    if (params.type === 'dynamic') return devices;
    if (devices[0] && Object.keys(devices[0]).length > 0) {
      return { ...state, ...devices[0], serialnumber: devices[0].serial };
    }
    return state;
  })
  .on(nextStep, (state, step) => (step === 'typeDevice' ? defaultDeviceForm : state))
  .reset([instructionUnmounted, signout]);

$isLoadingDevice
  .on(
    [fxDeviceTypes.pending, fxDeviceAdd.pending, fxDeviceAdd.fail],
    (_, isLoading) => isLoading
  )
  .reset([instructionUnmounted, signout]);

$deviceTypes
  .on(fxDeviceTypes.done, (_, { result }) => result.types)
  .reset([instructionUnmounted, signout]);

$deviceError
  .on(fxDeviceAdd.fail, (_, { error }) => error.message)
  .on(addClicked, () => '')
  .on(nextStep, () => '')
  .reset([instructionUnmounted, signout]);

$devices
  .on(fxDeviceAdd.pending, (state, isLoading) => isLoading ? [] : state)
  .on(fxDeviceAdd.done, (_, { result }) => result.devices)
  .reset([instructionUnmounted, signout]);

$payloadByNextStep
  .on(
    fxDeviceAdd.done,
    (_, { result }) => result.next_step && getURLParams(result.next_step)
  )
  .reset([instructionUnmounted, signout]);

$deviceSignals
  .on(setDeviceSignals, (state, signals) => ({
    ...state,
    ...signals,
  }))
  .reset([instructionUnmounted, signout]);

sample({
  source: $isDeviceOpen,
  clock: addClicked,
  target: [fxDeviceTypes, fxDeviceRooms, fxGetList],
});

guard({
  clock: updateDeviceFormStore,
  source: $deviceForm,
  filter: ({ isSubmittedForm }) => isSubmittedForm,
  target: fxDeviceAdd.prepend(({ isSubmittedForm, ...rest }) => rest),
});

guard({
  clock: fxDeviceAdd.done,
  filter: ({ result }) => result.serialnumber,
  target: [
    addModeChanged.prepend(() => false),
    fxGetDeviceInfoByProxy.prepend(({ result }) => result.proxy),
  ],
});

sample({
  clock: fxGetDeviceInfoByProxy.done,
  target: needUpdateTable,
});
