import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $types, fxGetTypes, $isLoading, $error } from '../../model';

const TypeSelect = (props) => {
  const options = useStore($types);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetTypes();
  }, []);

  return (
    <SelectControl
      getOptionValue={(opt) => opt && opt.section_id}
      getOptionLabel={(opt) => (opt ? opt.section_title : 'Неизвестный тип')}
      options={options}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const TypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<TypeSelect />} onChange={onChange} {...props} />;
};

export { TypeSelect, TypeSelectField };
