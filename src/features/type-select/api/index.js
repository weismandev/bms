import { api } from '../../../api/api2';

const get = () => api.no_vers('get', 'helpdesk/get_section');

export const typesApi = { get };
