import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { typesApi } from '../api';

const fxGetTypes = createEffect();

const $types = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$types
  .on(fxGetTypes.done, (state, { result }) => {
    if (!result) {
      return [];
    }

    return result;
  })
  .reset(signout);

$error.on(fxGetTypes.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetTypes.pending, (state, result) => result).reset(signout);
fxGetTypes.use(typesApi.get);

export { fxGetTypes, $types, $error, $isLoading };
