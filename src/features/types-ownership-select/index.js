export { typesOwnershipApi } from './api';
export { TypesOwnershipSelect, TypesOwnershipSelectField } from './organisms';
export { fxGetList, $data, $error, $isLoading } from './model';
