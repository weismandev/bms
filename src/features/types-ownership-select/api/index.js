import { api } from '../../../api/api2';

const getList = () => api.no_vers('get', 'admin/get-objects-types');

export const typesOwnershipApi = { getList };
