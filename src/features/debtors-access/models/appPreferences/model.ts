import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { AppPreferences, PageStatus } from '../../interfaces';

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const $appPreferences = createStore<AppPreferences>({ rules: [] });
const $isLoading = createStore<boolean>(false);
const $isError = createStore<Error | null>(null);

const clearError = createEvent();

const fxGetAppPreferences = createEffect<void, AppPreferences, Error>();

const fxSetAppPreferences = createEffect<{ disabled: string[] }, void, Error>();
const setAppPreferences = createEvent<{ disabled: string[] }>();

const $statusPage = createStore<PageStatus>(PageStatus.view);
const changeStatusPage = createEvent<PageStatus>();

export {
  $appPreferences,
  $isLoading,
  $isError,
  clearError,
  fxGetAppPreferences,
  fxSetAppPreferences,
  pageUnmounted,
  pageMounted,
  PageGate,
  $statusPage,
  changeStatusPage,
  setAppPreferences,
};
