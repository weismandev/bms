import { signout } from '@features/common';
import { forward, merge, sample } from 'effector';
import {
  $appPreferences,
  $isLoading,
  $isError,
  clearError,
  fxGetAppPreferences,
  fxSetAppPreferences,
  pageUnmounted,
  pageMounted,
  $statusPage,
  changeStatusPage,
  setAppPreferences,
} from './model';
import { debitorsAppPreferencesApi } from '../../api';
import { PageStatus } from '@features/debtors-access/interfaces';

/** Прооверка наличия запроса, запись в $isLoading */
const isRequest = merge([fxGetAppPreferences.pending, fxSetAppPreferences.pending]);
$isLoading.on(isRequest, (_state, isLoading) => isLoading).reset(pageMounted, signout);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([fxGetAppPreferences.failData, fxSetAppPreferences.failData]);
$isError
  .on(isError, (_, error) => error.message ?? 'Request error')
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(pageMounted, signout);

/** Запуск запроса правил ограничения разделов для должников */
fxGetAppPreferences.use(debitorsAppPreferencesApi.getDebtorsAppPreferences);

/** Запись результатов запроса правил ограничения разделов для должников */
$appPreferences
  .on(fxGetAppPreferences.doneData, (_, result) => result)
  .on(fxSetAppPreferences.doneData, (_, result) => result);

/** Делаю запрос при загрузке страницы или переключении просмотр/редактирование */
sample({
  clock: [pageMounted, changeStatusPage],
  target: fxGetAppPreferences,
});

/** Очистка ошибки */
$isError.on(clearError, () => null);

/** Изменение страницы просмотр/редактирование */
$statusPage.on(changeStatusPage, (_, status) => status).reset(pageMounted);

/** Эффект для сохранения настроек  */
fxSetAppPreferences.use(debitorsAppPreferencesApi.saveDebtorsAppPreferences);

/** Запуск сохранения настроек  */
sample({
  clock: setAppPreferences,
  target: fxSetAppPreferences,
});

/** При успешном сохранении настроек перехожу в режим просмотра  */
sample({
  clock: fxSetAppPreferences.done,
  fn: () => PageStatus.view,
  target: changeStatusPage,
});
