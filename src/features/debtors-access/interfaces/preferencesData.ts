export interface Rule {
  name: string;
  title: string;
  is_enabled: boolean;
}

export interface AppPreferences {
  rules: Rule[];
}
