import { HaveSectionAccess } from '@features/common';
import { Loader } from '@ui/atoms';
import { ErrorMessage, FilterMainDetailLayout } from '@ui/index';
import { useUnit } from 'effector-react';
import { $isError, $isLoading, clearError, PageGate } from '../models';
import { AppPreferences } from '../organisms';

const DebtorsAccess = () => {
  const [isLoading, isError] = useUnit([$isLoading, $isError]);

  return (
    <div>
      <PageGate />
      <Loader isLoading={isLoading} />
      <ErrorMessage isOpen={isError} onClose={clearError} error={isError} />
      <FilterMainDetailLayout main={<AppPreferences />} />;
    </div>
  );
};

const RestrictedDebtorsAccess = (props: Record<string, any>) => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <DebtorsAccess {...props} />
    // </HaveSectionAccess>
  );
};

export { RestrictedDebtorsAccess };
