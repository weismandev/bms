import styled from '@emotion/styled';

export const StyledAppPreferences = styled.div`
  background: #fff;
  box-shadow: 0px 4px 15px #e7e7ec;
  border-radius: 16px;
  position: relative;
  padding: 0;
  height: 100%;
  overflow: hidden;
`;

export const StyledHeaderItem = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 24px 14px 24px 24px;
  width: calc(100% - 10px);
  position: sticky;
  top: 0;
  z-index: 100;
  background: #ffffff;
`;

export const StyledHeaderItemLeft = styled.div``;

export const StyledHeaderItemRight = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 10px;
`;

export const CheckboxLabel = styled.span`
  font-weight: 500;
  font-size: 13px;
  line-height: 15px;
  color: #65657b;
`;

export const StyledAccessItems = styled.div`
  margin: 0 24px 24px;
  border: 1px solid #e7e7ec;
  border-radius: 10px;
  overflow: hidden;
`;

export const StyledAccessItem = styled.div`
  padding: 10px 26px;
  transition: 0.1s;
  border-bottom: 1px solid #e7e7ec;
  &:hover {
    background: #e1ebff;
  }
  &:last-child {
    border-bottom: 0;
  }
`;
