import { PageStatus } from '@features/debtors-access/interfaces';
import { useUnit } from 'effector-react';
import { $appPreferences, $statusPage } from '../../models';
import { AppPreferencesItems } from './appPreferencesItems';

export const AppPreferences = () => {
  const [appPreferences, statusPage] = useUnit([$appPreferences, $statusPage]);

  return (
    <>
      {statusPage === PageStatus.view && (
        <AppPreferencesItems appPreferences={appPreferences} disabled />
      )}

      {statusPage === PageStatus.edit && (
        <AppPreferencesItems appPreferences={appPreferences} />
      )}
    </>
  );
};
