import { AppPreferences, PageStatus } from '../../interfaces';
import { Field, FieldProps, Form, Formik } from 'formik';
import { Checkbox, FormControlLabel } from '@mui/material';
import {
  CheckboxLabel,
  StyledAccessItem,
  StyledAccessItems,
  StyledAppPreferences,
  StyledHeaderItem,
  StyledHeaderItemLeft,
  StyledHeaderItemRight,
} from './styled';
import { useUnit } from 'effector-react';
import {
  $isLoading,
  $statusPage,
  changeStatusPage,
  setAppPreferences,
} from '@features/debtors-access/models';
import { CustomScrollbar } from '@ui/molecules';
import { CancelButton, EditButton, SaveButton } from '@ui/atoms';

interface Access {
  [key: string]: boolean;
}

const CustomCheckbox = (props: Record<string, any>) => {
  const { name, label, disabled, checked } = props;

  return (
    <Field
      name={name}
      render={(fieldProps: FieldProps) => {
        const { form, field } = fieldProps;
        return (
          <FormControlLabel
            label={<CheckboxLabel>{label}</CheckboxLabel>}
            control={
              <Checkbox
                size="small"
                onChange={() => form.setFieldValue(field.name, !field.value)}
                checked={field.value ?? checked}
                disabled={disabled}
                {...props}
              />
            }
          />
        );
      }}
    />
  );
};

export const AccessHeader = ({ pageStatus }: { pageStatus: PageStatus }) => {
  const isLoading = useUnit($isLoading);

  if (pageStatus === PageStatus.view) {
    return (
      <StyledHeaderItem>
        <StyledHeaderItemLeft>
          <EditButton onClick={() => changeStatusPage(PageStatus.edit)} />
        </StyledHeaderItemLeft>
      </StyledHeaderItem>
    );
  }

  return (
    <StyledHeaderItem>
      <StyledHeaderItemLeft>
        <EditButton disabled />
      </StyledHeaderItemLeft>
      <StyledHeaderItemRight>
        <SaveButton type={isLoading ? 'button' : 'submit'} />
        <CancelButton onClick={() => changeStatusPage(PageStatus.view)} />
      </StyledHeaderItemRight>
    </StyledHeaderItem>
  );
};

export const AppPreferencesItems = ({
  appPreferences,
  disabled,
}: {
  appPreferences: AppPreferences;
  disabled?: boolean;
}) => {
  const statusPage = useUnit($statusPage);
  const rules = appPreferences?.rules;

  if (!rules.length) return null;

  const access: Access = {};

  rules.forEach((rule) => (access[rule.name] = rule.is_enabled));

  const handleSubmit = (values: Access) => {
    const disabled = Object.keys(values).filter((key) => !values[key]);

    setAppPreferences({ disabled });
  };

  return (
    <StyledAppPreferences>
      <CustomScrollbar autoHide>
        <Formik
          initialValues={access}
          onSubmit={handleSubmit}
          render={() => (
            <Form style={{ width: '100%' }}>
              <AccessHeader pageStatus={statusPage} />
              <StyledAccessItems>
                {rules.map((rule) => (
                  <StyledAccessItem key={rule.name}>
                    <CustomCheckbox
                      name={rule.name}
                      label={rule.title}
                      disabled={disabled}
                    />
                  </StyledAccessItem>
                ))}
              </StyledAccessItems>
            </Form>
          )}
        />
      </CustomScrollbar>
    </StyledAppPreferences>
  );
};
