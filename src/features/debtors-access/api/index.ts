import { api } from '../../../api/api2';

const getDebtorsAppPreferences = () => {
  return api.v1('get', 'debtors/preferences/app');
};

const saveDebtorsAppPreferences = (payload: { disabled: string[] }) => {
  return api.v1('post', 'debtors/preferences/app/save', payload);
};

export const debitorsAppPreferencesApi = {
  getDebtorsAppPreferences,
  saveDebtorsAppPreferences,
};
