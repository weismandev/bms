import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { StatusSelectField } from '../status-select';
import { useStyles } from './styles';

const { t } = i18n;

export const Filters = memo(() => {
  const filters = useStore($filters);

  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={() => (
            <Form className={classes.form}>
              <Field
                name="houses"
                component={HouseSelectField}
                label={t('house')}
                placeholder={t('selectHouse')}
                isMulti
              />
              <Field
                name="status"
                component={StatusSelectField}
                label={t('Label.status')}
                placeholder={t('selectStatuse')}
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
