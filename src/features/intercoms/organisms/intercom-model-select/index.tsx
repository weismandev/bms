import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, fxGetList, $isLoading } from '../../models/intercom-model-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const IntercomModelSelect: FC<object> = (props) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={data} isLoading={isLoading} {...props} />;
};

export const IntercomModelSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<IntercomModelSelect />} onChange={onChange} {...props} />
  );
};
