import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import i18n from '@shared/config/i18n';
import { Toolbar, FilterButton, Greedy, AddButton, SearchInput } from '@ui/index';
import {
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
  $isDetailOpen,
  addClicked,
} from '../../models';
import { styles } from './styles';

const { t } = i18n;

export const TableToolbar: FC = () => {
  const search = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);

  const onSearch = (e: { target: { value: string } }) => searchChanged(e.target.value);
  const handleClear = () => searchChanged('');
  const onClickFilter = () => changedFilterVisibility(true);

  return (
    <Toolbar>
      <Tooltip title={t('Filters') as string}>
        <div>
          <FilterButton
            disabled={isFilterOpen}
            style={styles.margin}
            onClick={onClickFilter}
          />
        </div>
      </Tooltip>
      <SearchInput value={search} onChange={onSearch} handleClear={handleClear} />
      <Greedy />
      <Tooltip title={t('add') as string}>
        <div>
          <AddButton disabled={isDetailOpen} onClick={addClicked} />
        </div>
      </Tooltip>
    </Toolbar>
  );
};
