import { useCallback, useMemo, FC, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  IntegratedFiltering,
  FilteringState,
  IntegratedSorting,
  SearchState,
  SortingState,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  SortingLabel,
  TableHeaderCell,
  TableContainer,
  TableRow,
  TableCell,
  Wrapper,
  TableColumnVisibility,
} from '@ui/index';
import { integratedFilteringColumnExtensions } from '../../libs';
import {
  $tableData,
  columns,
  columnWidthsChanged,
  hiddenColumnChanged,
  columnOrderChanged,
  $search,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $sorting,
  sortChanged,
  $filters,
  openViaUrl,
  $opened,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { useStyles } from './styles';

const { t } = i18n;

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);

  const onRowClick = useCallback((_, id) => openViaUrl(id), []);
  const isSelected = opened?.id === props.row.id;

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: any) => (
  <Toolbar.Root
    style={{
      padding: 0,
      marginBottom: 12,
      minHeight: 36,
      height: 36,
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
    {...props}
  />
);

const StatusFormatter = ({ value }: { value: string }) => {
  const classes = useStyles();

  const style = { color: value === t('Online') ? '#1BB169' : '#EB5757' };

  return (
    <span className={classes.statusSpan} style={style}>
      {value}
    </span>
  );
};

const StatusProvider = (props: any) => (
  <DataTypeProvider formatterComponent={StatusFormatter} {...props} />
);

export const IntercomsTable: FC = () => {
  const tableData = useStore($tableData);
  const search = useStore($search);
  const columnOrder = useStore($columnOrder);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const sorting = useStore($sorting);
  const filters = useStore($filters);

  const filterValue = [
    {
      columnName: 'building_id',
      value: filters.houses,
    },
    {
      columnName: 'name',
      value: search,
    },
    {
      columnName: 'status',
      value: filters.status,
    },
  ];

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={tableData}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <DragDropProvider />

        <SearchState value={search} />
        <SortingState sorting={sorting} onSortingChange={sortChanged} />
        <FilteringState filters={filterValue} />

        <IntegratedFiltering columnExtensions={integratedFilteringColumnExtensions} />
        <IntegratedSorting />

        <StatusProvider for={['status']} />

        <Table
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
          messages={{ noData: t('noData') }}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          sortLabelComponent={SortingLabel}
          showSortingControls
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
      </Grid>
    </Wrapper>
  );
};
