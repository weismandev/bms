import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: { height: '100%', padding: 24 },
  statusSpan: {
    fontWeight: 500,
  },
}));
