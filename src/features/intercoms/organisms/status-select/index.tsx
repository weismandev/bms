import { FC } from 'react';
import { SelectControl, SelectField } from '@ui/index';
import { data } from '../../models/status-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const StatusSelect: FC<object> = (props) => (
  <SelectControl options={data} {...props} />
);

export const StatusSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: { id: string }) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StatusSelect />} onChange={onChange} {...props} />;
};
