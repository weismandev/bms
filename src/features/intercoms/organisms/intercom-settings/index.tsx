import { FC, MouseEvent } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { HouseSelectField } from '@features/house-select';
import { RizerSelectField } from '@features/rizer-select';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  CustomScrollbar,
  InputField,
  SwitchField,
  ActionButton,
} from '@ui/index';
import {
  $mode,
  $opened,
  changedDetailVisibility,
  changeMode,
  changeCollectionKey,
  detailSubmitted,
  changeVisibilityDeleteModal,
} from '../../models';
import { CamerasSelectField } from '../cameras-select';
import { useStyles } from '../detail/styles';
import { IntercomModelSelectField } from '../intercom-model-select';
import { validationSchema } from './validation';

const { t } = i18n;

export const IntercomSettings: FC = () => {
  const mode = useStore($mode);
  const opened = useStore($opened);

  const isNew = !opened.id;
  const isCollect = opened?.collection_key ? opened.collection_key : 0;
  const buttonName = isCollect ? t('OffCollect') : t('OnCollect');
  const kindButton = isCollect ? 'negative' : 'basic';

  const onChangeCollectionKey = (e: MouseEvent) => {
    e.preventDefault();
    changeCollectionKey();
  };

  const classes = useStyles({ isNew });

  return (
    <Formik
      initialValues={opened}
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      enableReinitialize
      render={({ resetForm, setFieldValue, values }) => {
        const onCancel = () => {
          if (isNew) {
            changedDetailVisibility(false);
          } else {
            changeMode('view');
            resetForm();
          }
        };
        const onChangeBuildings = (value: object) => {
          setFieldValue('buildings', value);
          setFieldValue('rizers', '');
        };
        const onClose = () => changedDetailVisibility(false);
        const onEdit = () => changeMode('edit');
        const onDelete = () => changeVisibilityDeleteModal(true);

        const buildingIds =
          values.buildings && values.buildings.length > 0
            ? values.buildings.map((item: { id: number }) =>
                typeof item === 'object' ? item.id : item
              )
            : [];

        return (
          <Form className={classes.form}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={onEdit}
              onCancel={onCancel}
              onClose={onClose}
              onDelete={onDelete}
            />
            <div className={classes.formWrapper}>
              <CustomScrollbar>
                <div className={classes.formContent}>
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('Name')}
                    placeholder={t('EnterTheTitle')}
                    name="name"
                    required
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('serialNumber')}
                    placeholder={t('EnterSerialNumber')}
                    name="serialnumber"
                  />
                  <Field
                    component={HouseSelectField}
                    mode={mode}
                    label={t('objects')}
                    placeholder={t('selectObject')}
                    name="buildings"
                    isMulti
                    required
                    onChange={onChangeBuildings}
                  />
                  <Field
                    component={IntercomModelSelectField}
                    mode={mode}
                    label={t('IntercomModel')}
                    placeholder={t('SelectIntercomModel')}
                    name="model"
                    required
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('Ip')}
                    placeholder={t('EnterIp')}
                    name="ip"
                    required
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('WebPort')}
                    placeholder={t('EnterWebPort')}
                    name="port_web"
                    required
                    type="number"
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('RtspPort')}
                    placeholder={t('EnterRtspPort')}
                    name="port_rtsp"
                    required
                    type="number"
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('login')}
                    placeholder={t('Enterlogin')}
                    name="intercomLog"
                    required
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('password')}
                    placeholder={t('Enterpassword')}
                    name="intercomPass"
                    required
                    type="password"
                  />
                  <Field
                    component={RizerSelectField}
                    mode={mode}
                    label={t('rizer')}
                    placeholder={t('ChooseEntrance')}
                    name="rizers"
                    required
                    buildings={buildingIds}
                  />
                  <Field
                    component={InputField}
                    mode={mode}
                    label={t('IdentifierForpost')}
                    placeholder={t('EnterIdentifierForpost')}
                    name="forpost_id"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="available_for_residents"
                    label={t('AvailabilityForResidents')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="card_en"
                    label={t('CardEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="finger_en"
                    label={t('FingerprintEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="face_en"
                    label={t('FaceIdEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="pin_en"
                    label={t('PinEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="ble_en"
                    label={t('BleEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="qr_en"
                    label={t('QrCodeEntry')}
                    labelPlacement="start"
                  />
                  <Field
                    component={InputField}
                    name="pin_length"
                    label={t('PinCodeLength')}
                    placeholder={t('EnterPinCodeLength')}
                    type="number"
                    mode={mode}
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="pin_en_guest"
                    label={t('PinCodeAvailabilityForGuests')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="pin_en_resident"
                    label={t('PinCodeAvailabilityForResidents')}
                    labelPlacement="start"
                  />
                  <Field
                    component={InputField}
                    name="qr_length"
                    label={t('QrCodeLength')}
                    placeholder={t('EnterQrCodeLength')}
                    type="number"
                    mode={mode}
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="qr_en_guest"
                    label={t('AvailabilityOfQrCodeForGuests')}
                    labelPlacement="start"
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="qr_en_resident"
                    label={t('AvailabilityOfQrCodeForResidents')}
                    labelPlacement="start"
                  />
                  <Field
                    component={InputField}
                    name="button_caption"
                    label={t('OpenButtonLabel')}
                    placeholder={t('EnterOpenButtonLabel')}
                    mode={mode}
                  />
                  <Field
                    component={InputField}
                    name="open_door_text"
                    label={t('DoorOopenMessage')}
                    placeholder={t('EnterDoorOopenMessage')}
                    mode={mode}
                  />
                  <Field
                    component={SwitchField}
                    disabled={mode === 'view'}
                    name="video_exists"
                    label={t('ShowVideo')}
                    labelPlacement="start"
                  />
                  <Field
                    component={CamerasSelectField}
                    mode={mode}
                    label={t('camera')}
                    placeholder={t('SelectCamera')}
                    name="camera"
                  />
                  <Field
                    component={InputField}
                    name="channel_number"
                    label={t('RelayChannelNumber')}
                    placeholder={t('EnterRelayChannelNumber')}
                    mode={mode}
                    type="number"
                  />
                  {values.sipNumber.length > 0 && (
                    <>
                      <Field
                        component={InputField}
                        name="sipNumber"
                        label={t('SIPNumber')}
                        placeholder={t('EnterSIP')}
                        mode="view"
                      />
                      <Field
                        component={InputField}
                        name="sipServer"
                        label={t('SipServerProxyServerRegistrationServer')}
                        placeholder={t('EnterSipServerProxyServerRegistrationServer')}
                        mode="view"
                      />
                      <Field
                        component={InputField}
                        name="sipPort"
                        label={t('SipPort')}
                        placeholder={t('EnterSipPort')}
                        mode="view"
                        type="number"
                      />
                      <Field
                        component={InputField}
                        name="sipState"
                        label={t('ConnectionStatus')}
                        placeholder={t('EnterConnectionStatus')}
                        mode="view"
                        value={values.sipState === 0 ? t('NoActive') : t('active')}
                      />
                      <Field
                        component={InputField}
                        name="sipPassword"
                        label={t('SipPassword')}
                        placeholder={t('EnterSipPassword')}
                        mode="view"
                      />
                    </>
                  )}
                </div>
              </CustomScrollbar>
              {!isNew && (
                <div className={classes.footer}>
                  <ActionButton kind={kindButton} onClick={onChangeCollectionKey}>
                    {buttonName}
                  </ActionButton>
                </div>
              )}
            </div>
          </Form>
        );
      }}
    />
  );
};
