import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  name: Yup.string().required(t('thisIsRequiredField')),
  buildings: Yup.array()
    .min(1, t('thisIsRequiredField'))
    .required(t('thisIsRequiredField')),
  model: Yup.mixed().required(t('thisIsRequiredField')),
  rizers: Yup.mixed().required(t('thisIsRequiredField')),
  ip: Yup.string().required(t('thisIsRequiredField')),
  port_web: Yup.string().required(t('thisIsRequiredField')),
  port_rtsp: Yup.string().required(t('thisIsRequiredField')),
  intercomLog: Yup.string().required(t('thisIsRequiredField')),
  intercomPass: Yup.string().required(t('thisIsRequiredField')),
});
