import React from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldProps } from 'formik';
import { DetailToolbar } from '@ui/index';

import { changedDetailVisibility } from '../../models';

import {
  changeAccessMode,
  $accessMode,
  $access,
  submit,
  AccessFormGate,
} from '../../models';
import Scrollbars from 'react-custom-scrollbars';
import { ObjectsTreeSelect } from '@features/objects-tree-select';
import { ModeTypes, IAccessData } from '../../interfaces';

export const AccessForm = (props: IAccessData) => {
  const { serialnumber } = props;
  const mode = useStore($accessMode);
  const access = useStore($access);

  return (
    <>
      <AccessFormGate serialnumber={serialnumber} />

      <Formik
        initialValues={{ objects: access } as any}
        onSubmit={submit}
        enableReinitialize
        onReset={() => {
          changeAccessMode(ModeTypes.view);
        }}
        render={() => (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ height: 84, padding: 24 }}
              hidden={{ delete: true }}
              mode={mode}
              onClose={() => changedDetailVisibility(false)}
              onEdit={() => changeAccessMode(ModeTypes.edit)}
            />
            <div
              style={{
                height: 'calc(100% - 84px)',
                position: 'relative',
                padding: '0 6px 24px 24px',
              }}
            >
              <Scrollbars autoHide>
                <div
                  style={{
                    height: 'calc(100% - 82px)',
                    paddingRight: '12px',
                  }}
                >
                  <Field
                    name="objects"
                    render={({ field, form }: FieldProps) => (
                      <ObjectsTreeSelect
                        value={field.value}
                        onChange={(value: string) =>
                          form.setFieldValue(field.name, value)
                        }
                        mode={mode as string}
                      />
                    )}
                  />
                </div>
              </Scrollbars>
            </div>
          </Form>
        )}
      />
    </>
  );
};
