import makeStyles from '@mui/styles/makeStyles';

export const styles = {
  wrapper: {
    height: '100%',
  },
  tab: {
    minWidth: '33.3%',
  },
};

export const useStyles = makeStyles(() => ({
  form: {
    height: ({ isNew }: { isNew: boolean }) => `calc(100% - ${isNew ? 51 : 95}px)`,
    position: 'relative',
  },
  formWrapper: {
    height: 'calc(100% - 80px)',
  },
  formContent: {
    padding: '0px 24px 0px 24px',
  },
  footer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 10,
  },
}));
