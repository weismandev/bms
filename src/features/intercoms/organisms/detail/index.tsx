import { memo } from 'react';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Formik, Form, Field, FieldProps } from 'formik';
import { Wrapper, DetailToolbar, Tabs } from '@ui/index';

import Scrollbars from 'react-custom-scrollbars';
import { ObjectsTreeSelect } from '@features/objects-tree-select';
import { ModeTypes, IAccessData } from '@features/intercoms/interfaces';
import { IntercomSettings } from '../intercom-settings';
import { Intercom } from '../intercom';
import {
  $currentTab,
  changeTab,
  tabs,
  $opened,
  tabsWithArchive,
  changedDetailVisibility,
  changeAccessMode,
  $accessMode,
  $access,
  submit,
  AccessFormGate,
} from '../../models';
import { styles } from './styles';

export const Detail = memo(() => {
  const opened = useStore($opened);
  const currentTab = useStore($currentTab);

  const currentTabs = Boolean(opened.camera_id) ? tabsWithArchive : tabs;
  const isNew = !opened.id;

  const onChangeTab = (_: unknown, tab: string) => changeTab(tab);

  return (
    <Wrapper style={styles.wrapper}>
      <Tabs
        options={currentTabs}
        currentTab={currentTab}
        onChange={onChangeTab}
        isNew={isNew}
        tabEl={<Tab style={styles.tab} />}
      />
      {currentTab === 'intercom' && <Intercom />}
      {currentTab === 'access' && <AccessForm serialnumber={opened.id} />}
      {currentTab === 'settings' && <IntercomSettings />}
    </Wrapper>
  );
});

function AccessForm(props: IAccessData) {
  const { serialnumber } = props;
  const mode = useStore($accessMode);
  const access = useStore($access);

  return (
    <>
      <AccessFormGate serialnumber={serialnumber} />

      <Formik
        initialValues={{ objects: access } as any}
        onSubmit={submit}
        enableReinitialize
        onReset={() => {
          changeAccessMode(ModeTypes.view);
        }}
        render={() => (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ height: 84, padding: 24 }}
              hidden={{ delete: true }}
              mode={mode}
              onClose={() => changedDetailVisibility(false)}
              onEdit={() => changeAccessMode(ModeTypes.edit)}
            />
            <div
              style={{
                height: 'calc(100% - 84px)',
                position: 'relative',
                padding: '0 6px 24px 24px',
              }}
            >
              <Scrollbars autoHide>
                <div
                  style={{
                    height: 'calc(100% - 82px)',
                    paddingRight: '12px',
                  }}
                >
                  <Field
                    name="objects"
                    render={({ field, form }: FieldProps) => (
                      <ObjectsTreeSelect
                        value={field.value}
                        onChange={(value: string) =>
                          form.setFieldValue(field.name, value)
                        }
                        mode={mode as string}
                      />
                    )}
                  />
                </div>
              </Scrollbars>
            </div>
          </Form>
        )}
      />
    </>
  );
}
