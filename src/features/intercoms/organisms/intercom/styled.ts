import styled from '@emotion/styled';
import { Form } from 'formik';

export const StyledForm = styled(Form)`
  position: relative;
  height: calc(100% - 95px);
`;

export const StyledWrapperForm = styled.div`
  height: calc(100% - 80px);
`;

export const StyledContentForm = styled.div`
  padding: 0px 24px 0px 24px;
`;

export const StyledStreamContainer = styled.div`
  position: relative;
  &:hover > [data-controller-intercoms-open] {
    opacity: 1;
  }
`;

export const StyledStreamController = styled.div`
  position: absolute;
  bottom: 12px;
  left: 0;
  right: 0;
  text-align: center;
  opacity: 0.7;
`;
