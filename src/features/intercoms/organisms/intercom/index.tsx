import { FC, useState } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Formik, Field } from 'formik';
import { Stream } from '@widgets/MediaPlayer';
import { DetailToolbar, CustomScrollbar, InputField, ActionButton } from '@ui/index';
import { HouseSelectField } from '@features/house-select';
import { ArchiveModal } from '@features/cameras/organisms/detail/archive-modal';

import {
  $opened,
  changedDetailVisibility,
  changeVisibilityDeleteModal,
} from '../../models';

import {
  StyledForm,
  StyledWrapperForm,
  StyledContentForm,
  StyledStreamContainer,
  StyledStreamController
} from './styled';

export const Intercom: FC = () => {
  const { t } = useTranslation();
  const opened = useStore($opened);

  const [isVisibilityArchive, toggleVisibilityArchive] = useState(false);

  const openDoor = () => {
    fetch(opened.openDoor);
  };

  const onSubmit = () => undefined;

  const onClose = () => changedDetailVisibility(false);
  const onDelete = () => changeVisibilityDeleteModal(true);
  const onVideoLibrary = () => toggleVisibilityArchive(true);

  return (
    <>
      <Formik
        initialValues={opened}
        onSubmit={onSubmit}
        enableReinitialize
        render={({ values }) => (
          <StyledForm>
            <DetailToolbar
              style={{ padding: 24 }}
              mode="view"
              onClose={onClose}
              onDelete={onDelete}
              onVideoLibrary={onVideoLibrary}
              hidden={{
                edit: true,
                videoLibrary: !opened?.camera[0]?.has_archive
              }}
            />
            <StyledWrapperForm>
              <CustomScrollbar>
                <StyledContentForm>
                  <Field
                    component={InputField}
                    mode="view"
                    label={t('Name')}
                    placeholder={t('EnterTheTitle')}
                    name="name"
                  />
                  <Field
                    component={HouseSelectField}
                    mode="view"
                    label={t('objects')}
                    placeholder={t('selectObject')}
                    name="buildings"
                    isMulti
                  />
                </StyledContentForm>
                <StyledStreamContainer>
                  <Stream id={opened.camera_id} />
                  <StyledStreamController data-controller-intercoms-open>
                    <ActionButton
                      kind="positive"
                      style={{ padding: '0 30px' }}
                      onClick={openDoor}
                    >
                      {values.button_caption}
                    </ActionButton>
                  </StyledStreamController>
                </StyledStreamContainer>
              </CustomScrollbar>
            </StyledWrapperForm>
          </StyledForm>
        )}
      />
      {opened.id && (
        <ArchiveModal
          id={opened.id}
          isOpen={isVisibilityArchive}
          changeVisibility={toggleVisibilityArchive}
          title={opened.title}
        />
      )}
    </>
  );
};
