import { api } from '../../../api/api2';

const getIntercomAccess = (payload) =>
  api.clear('get', 'intercom/get-intercom-access/', payload);

const updateIntercomAccess = (payload = {}) =>
  api.clear('post', 'intercom/update-intercom-access/', payload);

export const accessApi = { getIntercomAccess, updateIntercomAccess };
