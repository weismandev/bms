import { api } from '@api/api2';
import {
  GetIntercomPayload,
  ChangeCollectionKeyPayload,
  GetArchivePayload,
  CreateUpdateIntercomPayload,
  IntercomDeletePayload,
} from '../interfaces';

const getIntercoms = (payload: void | GetIntercomPayload) =>
  api.no_vers('post', 'intercom/get-intercom-settings-v2', payload);
const getIntercomModel = () => api.no_vers('post', 'intercom/get-intercom-model');
const getCameras = () => api.v1('post', 'cameras/get-by-company');
const changeCollectionKey = (payload: ChangeCollectionKeyPayload) =>
  api.no_vers('post', 'intercom/update-collection-key', payload);
const getArchive = (payload: GetArchivePayload) =>
  api.no_vers('post', 'akado/get-archive-stream', payload);
const createUpdateIntercom = (payload: CreateUpdateIntercomPayload) =>
  api.no_vers('post', 'intercom/update-intercom-settings-v2', payload);
const deleteIntercom = (payload: IntercomDeletePayload) =>
  api.no_vers('post', 'intercom/delete-intercom', payload);

export const intercomsApi = {
  getIntercoms,
  getIntercomModel,
  getCameras,
  changeCollectionKey,
  getArchive,
  createUpdateIntercom,
  deleteIntercom,
};
