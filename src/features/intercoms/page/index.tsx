import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '@ui/index';

import {
  $error,
  $isDetailOpen,
  $isErrorDialogOpen,
  $isFilterOpen,
  $isLoading,
  $isOpenDeleteModal,
  $path,
  changedErrorDialogVisibility,
  changeVisibilityDeleteModal,
  deleteIntercom,
  PageGate,
} from '../models';
import { Detail, Filters, IntercomsTable } from '../organisms';

const IntercomsPage: FC = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changeVisibilityDeleteModal(false);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <DeleteConfirmDialog
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteIntercom}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<IntercomsTable />}
        detail={isDetailOpen && <Detail />}
        params={{
          detailWidth: 'minmax(370px, 35%)',
        }}
      />
    </>
  );
};

const RestrictedIntercomsPage: FC = () => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <IntercomsPage />
    </HaveSectionAccess>
  );
};

export { RestrictedIntercomsPage as IntercomsPage };
