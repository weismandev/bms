import { IntegratedFiltering, Filter } from '@devexpress/dx-react-grid';

const intercomsPredicateBuilding = (
  value: string,
  filter: Filter,
  row: { building_id: string }
) => {
  const isArray = Array.isArray(filter.value);

  if (!isArray) {
    return true;
  }
  const isEmpty = filter.value.length === 0;

  const hasFilterMatches =
    isArray &&
    filter.value.some(
      (item: { id: string }) =>
        Number.parseInt(item.id, 10) === Number.parseInt(row.building_id, 10)
    );

  if (isArray) {
    return isEmpty ? true : hasFilterMatches;
  }

  return IntegratedFiltering.defaultPredicate(value, filter, row);
};

const intercomsPredicateDeviceName = (value: string, filter: Filter, row: object) =>
  IntegratedFiltering.defaultPredicate(value, filter, row);

const intercomsPredicateStatus = (value: string, filter: Filter, row: object) => {
  if (filter.value && typeof filter.value === 'object') {
    const newFilter = { ...filter };

    const statusTitle = newFilter.value.title;

    newFilter.value = statusTitle;

    return IntegratedFiltering.defaultPredicate(value, newFilter, row);
  }

  return true;
};

export const integratedFilteringColumnExtensions = [
  { columnName: 'building_id', predicate: intercomsPredicateBuilding },
  { columnName: 'name', predicate: intercomsPredicateDeviceName },
  { columnName: 'status', predicate: intercomsPredicateStatus },
];
