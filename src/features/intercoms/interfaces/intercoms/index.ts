export interface IntercomsResponse {
  buildings: { [key: number]: string };
  intercom: Intercom[];
}

export interface Intercom {
  building: string;
  building_guid: string;
  building_title: string;
  buildings: string[];
  camera_id: number;
  complex_id: number;
  complex_title: string;
  fias: string;
  forpost_id: string;
  hide_id: string;
  img: string;
  mjpeg: string;
  name: string;
  online: number;
  'open-url': string;
  rizer: number;
  rtsp: string;
  serialnumber: string;
}

export enum ModeTypes {
  edit = 'edit',
  view = 'view',
}

export interface IAccessData {
  serialnumber: string;
}

export interface IGetIntercomAccess {
  apartments: number[];
  [key: string]: any;
}
