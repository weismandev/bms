export interface Building {
  id: number;
  building: {
    address: {
      city: string;
      street: string;
      houseNumber: string;
      fullAddress: string;
    };
  };
}
