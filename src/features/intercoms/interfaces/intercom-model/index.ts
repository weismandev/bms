export interface IntercomModel {
  id: string;
  title: string;
}

export interface IntercomModelResponse {
  model: IntercomModel[];
}
