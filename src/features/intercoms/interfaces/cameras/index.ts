export interface CamerasResponse {
  cameras: Camera[];
}

export interface Camera {
  camera: CameraItem;
}

export interface CameraItem {
  id: number;
  title: string;
}
