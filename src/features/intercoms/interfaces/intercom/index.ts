export interface GetIntercomPayload {
  serialnumber: number;
}

export interface GetIntercomResponse {
  intercom: IntercomResponseItem[];
}

export interface IntercomResponseItem {
  available_for_residents: number;
  action_id: number;
  ble_en: number;
  building: string;
  building_guid: string;
  building_title: string;
  buildings: string[];
  button_caption: string;
  camera_id: number;
  card_en: number;
  channel_number: number;
  collection_key: number;
  complex_id: number;
  complex_title: string;
  concierge_en: number;
  concierge_number: number;
  delete_param: string;
  description: string;
  din_ip: number;
  face_en: number;
  fias: string;
  finger_en: number;
  forpost_id: string;
  hide_id: string;
  ip: string;
  login: string;
  mjpeg: string;
  model: { title: string; id: string };
  name: string;
  online: number;
  'open-url': string;
  open_door_hide: number;
  open_door_text: string;
  password: string;
  pin_en: number;
  pin_en_guest: number;
  pin_en_resident: number;
  pin_length: number;
  port_rtsp: string;
  port_web: string;
  qr_en: number;
  qr_en_guest: number;
  qr_en_resident: number;
  qr_length: number;
  rizers: number | string;
  rtsp: string;
  settings_block_id: number;
  sip_number: string;
  sip_password: string;
  sip_port: string;
  sip_server: string;
  sip_state: number;
  video_exists: number;
  serialnumber: string;
  camera: [];
}

export interface ChangeCollectionKeyPayload {
  serialnumber: number;
  state: number;
}

export interface GetArchivePayload {
  camera_id: number;
  timestamp: number;
}

export interface ArhiveType {
  camera_id: number;
  timestamp: Date;
}

export interface GetArchiveResponse {
  url: string;
}

export interface CreateUpdateIntercomPayload {
  ble_en: number;
  buildings: string;
  button_caption: string;
  camera_id: string | number;
  card_en: number;
  channel_number: number;
  face_en: number;
  finger_en: number;
  forpost_id: string;
  ip: string;
  login: string;
  model: string;
  name: string;
  open_door_text: string;
  password: string;
  pin_en: number;
  pin_en_guest: number;
  pin_en_resident: number;
  pin_length: number;
  port_rtsp: number;
  port_web: number;
  qr_en: number;
  qr_en_guest: number;
  qr_en_resident: number;
  qr_length: number;
  rizers: number;
  video_exists: number;
}

export interface CreateUpdateIntercomResponse {
  serialnumber: number;
}

export interface IntercomDeletePayload {
  param: string;
}
