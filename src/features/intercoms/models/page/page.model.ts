import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { $pathname } from '@features/common';
import { IntercomsResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetIntercoms = createEffect<void, IntercomsResponse, Error>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $raw = createStore<IntercomsResponse>({
  buildings: {},
  intercom: [],
});
export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);
