import { merge, forward, sample } from 'effector';
import { pending } from 'patronum';
import { signout } from '@features/common';
import { fxGetList as fxGetBuildings } from '@features/house-select';
import { intercomsApi } from '../../api';
import {
  fxGetIntercom,
  fxChangeCollectionKey,
  fxGetArchive,
  fxCreateIntercom,
  fxUpdateIntercom,
  fxDeleteIntercom,
} from '../detail/detail.model';
import {
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changedErrorDialogVisibility,
  pageUnmounted,
  fxGetIntercoms,
  pageMounted,
  $raw,
} from './page.model';
import { fxGetIntercomAccess, fxUpdateIntercomAccess } from '../access';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';

fxGetIntercoms.use(intercomsApi.getIntercoms);
fxGetIntercom.use(intercomsApi.getIntercoms);
fxChangeCollectionKey.use(intercomsApi.changeCollectionKey);
fxGetArchive.use(intercomsApi.getArchive);
fxCreateIntercom.use(intercomsApi.createUpdateIntercom);
fxUpdateIntercom.use(intercomsApi.createUpdateIntercom);
fxDeleteIntercom.use(intercomsApi.deleteIntercom);

const errorOccured = merge([
  fxGetIntercoms.fail,
  fxGetIntercom.fail,
  fxChangeCollectionKey.fail,
  fxGetArchive.fail,
  fxCreateIntercom.fail,
  fxUpdateIntercom.fail,
  fxDeleteIntercom.fail,
  fxGetIntercomAccess.fail,
  fxUpdateIntercomAccess.fail,
]);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading
  .on(
    pending({
      effects: [
        fxGetIntercoms,
        fxGetIntercom,
        fxChangeCollectionKey,
        fxGetArchive,
        fxCreateIntercom,
        fxUpdateIntercom,
        fxDeleteIntercom,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

forward({
  from: [
    pageMounted,
    fxCreateIntercom.done,
    fxUpdateIntercom.done,
    fxDeleteIntercom.done,
  ],
  to: fxGetBuildings,
});

sample({
  clock: [pageMounted, $settingsInitialized],
  source: $settingsInitialized,
  filter: (settingsInitialized) => settingsInitialized,
  target: fxGetIntercoms,
});

$raw.on(fxGetIntercoms.done, (_, { result }) => result).reset([pageUnmounted, signout]);
