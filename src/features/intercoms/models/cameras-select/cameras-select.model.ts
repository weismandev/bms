import { createEffect, createStore } from 'effector';
import { CamerasResponse, CameraItem } from '../../interfaces';

export const fxGetList = createEffect<void, CamerasResponse, Error>();

export const $data = createStore<CameraItem[]>([]);
export const $isLoading = createStore<boolean>(false);
