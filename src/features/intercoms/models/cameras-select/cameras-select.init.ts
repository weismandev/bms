import { signout } from '@features/common';
import { intercomsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './cameras-select.model';

fxGetList.use(intercomsApi.getCameras);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.cameras)) {
      return [];
    }

    return result.cameras.map((item) => ({
      id: item.camera.id,
      title: item.camera.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);
