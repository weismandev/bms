import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';
import { IAccessData, IGetIntercomAccess, ModeTypes } from '../../interfaces';

const AccessFormGate = createGate<IAccessData>();

const changeMode = createEvent<ModeTypes>();
const submit = createEvent<void>();

const fxGetIntercomAccess = createEffect<IAccessData, IGetIntercomAccess, Error>();
const fxUpdateIntercomAccess = createEffect<
  IAccessData & IGetIntercomAccess,
  null,
  Error
>();

const $mode = restore<ModeTypes>(changeMode, ModeTypes.view);
const $access = createStore<IGetIntercomAccess>({ apartments: [] });

export {
  AccessFormGate,
  changeMode,
  submit,
  fxGetIntercomAccess,
  fxUpdateIntercomAccess,
  $mode,
  $access,
};
