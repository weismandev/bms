import { sample } from 'effector';
import { accessApi } from '../../api/access';

import {
  AccessFormGate,
  submit,
  fxGetIntercomAccess,
  fxUpdateIntercomAccess,
  $mode,
  $access,
} from './access.model';
import { ModeTypes } from '../../interfaces';

fxGetIntercomAccess.use(accessApi.getIntercomAccess);
fxUpdateIntercomAccess.use(accessApi.updateIntercomAccess);

$access
  .on(fxGetIntercomAccess.doneData, (state, { apartments }) => ({
    ...state,
    apartments,
  }))
  .on(fxUpdateIntercomAccess, (_state, { serialnumber, apartments }) => ({
    serialnumber,
    apartments,
  }))
  .reset([AccessFormGate.state, AccessFormGate.close]);

$mode.on(fxUpdateIntercomAccess.done, () => ModeTypes.view).reset(AccessFormGate.close);

sample({
  clock: submit,
  source: AccessFormGate.state,
  fn: ({ serialnumber }, { objects }: any) => ({
    serialnumber,
    apartments: objects?.apartments ?? [],
  }),
  target: fxUpdateIntercomAccess,
});

sample({
  source: AccessFormGate.open,
  target: fxGetIntercomAccess,
});
