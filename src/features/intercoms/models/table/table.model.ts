import { combine } from 'effector';
import { $data as $buildings } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { Intercom, Building } from '../../interfaces';
import { $raw } from '../page/page.model';

const { t } = i18n;

interface Data {
  [key: number]: Building;
}

export const columns = [
  {
    name: 'city',
    title: t('Label.city'),
  },
  {
    name: 'complex',
    title: t('RC'),
  },
  {
    name: 'full_address',
    title: t('FullAddress'),
  },
  { name: 'name', title: t('Intercom') },
  { name: 'status', title: t('Label.status') },
];

const columnWidths = [
  { columnName: 'city', width: 150 },
  { columnName: 'complex', width: 200 },
  { columnName: 'full_address', width: 320 },
  { columnName: 'name', width: 300 },
  { columnName: 'status', width: 120 },
];

export const $tableData = combine($raw, $buildings, ({ intercom }, buildings) => {
  const formattedBuildings = buildings.reduce(
    (acc: Data, value: Building) => ({
      ...acc,
      ...{ [value.id]: value },
    }),
    {}
  );

  return Array.isArray(intercom) && intercom.length > 0
    ? formatData(intercom, formattedBuildings)
    : [];
});

const formatData = (intercom: Intercom[], formattedBuildings: Data) =>
  intercom.map((item) => {
    const buildingAddress =
      formattedBuildings[Number.parseInt(item?.building, 10)]?.building?.address;

    const city = buildingAddress?.city || '-';
    const address =
      buildingAddress?.street && buildingAddress?.houseNumber
        ? `${buildingAddress.street}, ${buildingAddress.houseNumber}`
        : buildingAddress?.fullAddress;

    return {
      id: item.hide_id,
      city,
      complex: item?.complex_title || '-',
      building_id: item.building,
      full_address: address || '-',
      name: item?.name || '-',
      status: Boolean(item?.online) ? t('Online') : t('Offline'),
    };
  });

export const {
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  columnOrderChanged,
  sortChanged,

  $search,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $sorting,
  $pageSize,
} = createTableBag(columns, {
  widths: columnWidths,
});
