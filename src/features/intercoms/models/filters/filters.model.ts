import { createFilterBag } from '@tools/factories';

const defaultFilters = {
  houses: [],
  status: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
