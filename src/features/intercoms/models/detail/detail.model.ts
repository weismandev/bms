import { createEvent, createStore, createEffect } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';
import {
  GetIntercomPayload,
  GetIntercomResponse,
  ChangeCollectionKeyPayload,
  GetArchivePayload,
  ArhiveType,
  GetArchiveResponse,
  CreateUpdateIntercomPayload,
  CreateUpdateIntercomResponse,
  IntercomDeletePayload,
} from '../../interfaces';

const { t } = i18n;

export const tabs = [
  {
    value: 'intercom',
    onCreateIsDisabled: true,
    label: t('Intercom'),
  },
  {
    value: 'access',
    onCreateIsDisabled: true,
    label: t('Access'),
  },
  {
    value: 'settings',
    onCreateIsDisabled: false,
    label: t('ProfilePage.Settings'),
  },
];

export const tabsWithArchive = [
  {
    value: 'intercom',
    onCreateIsDisabled: true,
    label: t('Intercom'),
  },
  {
    value: 'access',
    onCreateIsDisabled: true,
    label: t('Access'),
  },
  {
    value: 'settings',
    onCreateIsDisabled: false,
    label: t('ProfilePage.Settings'),
  },
];

export const newEntity = {
  available_for_residents: true,
  name: '',
  serialnumber: '',
  buildings: [],
  model: '',
  ip: '',
  port_web: '',
  port_rtsp: '',
  intercomLog: '',
  intercomPass: '',
  rizers: '',
  forpost_id: '',
  card_en: false,
  finger_en: false,
  face_en: false,
  pin_en: false,
  ble_en: false,
  qr_en: false,
  pin_length: 6,
  pin_en_guest: true,
  pin_en_resident: true,
  qr_length: 60,
  qr_en_guest: 1,
  qr_en_resident: 1,
  button_caption: t('open'),
  open_door_text: t('DoorOpened'),
  video_exists: true,
  camera: '',
  channel_number: 1,
  sipNumber: '',
};

export const changeTab = createEvent<string>();
export const changeCollectionKey = createEvent<void>();
export const getArchive = createEvent<ArhiveType>();
export const changeVisibilityDeleteModal = createEvent<boolean>();
export const deleteIntercom = createEvent<void>();

export const $currentTab = createStore<string>('intercom');
export const $archive = createStore<string>('');
export const $isOpenDeleteModal = createStore<boolean>(false);

export const fxGetIntercom = createEffect<
  GetIntercomPayload,
  GetIntercomResponse,
  Error
>();
export const fxChangeCollectionKey = createEffect<
  ChangeCollectionKeyPayload,
  object,
  Error
>();
export const fxGetArchive = createEffect<GetArchivePayload, GetArchiveResponse, Error>();
export const fxCreateIntercom = createEffect<
  CreateUpdateIntercomPayload,
  CreateUpdateIntercomResponse,
  Error
>();
export const fxUpdateIntercom = createEffect<
  CreateUpdateIntercomPayload,
  CreateUpdateIntercomResponse,
  Error
>();
export const fxDeleteIntercom = createEffect<IntercomDeletePayload, object, Error>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
