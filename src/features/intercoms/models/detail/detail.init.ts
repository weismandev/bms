import { sample, guard } from 'effector';
import { signout, $pathname, history } from '@features/common';
import i18n from '@shared/config/i18n';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { pageUnmounted, fxGetIntercoms, $entityId, $path } from '../page/page.model';
import {
  $currentTab,
  changeTab,
  addClicked,
  openViaUrl,
  fxGetIntercom,
  $opened,
  newEntity,
  changeCollectionKey,
  fxChangeCollectionKey,
  changedDetailVisibility,
  $isDetailOpen,
  $archive,
  getArchive,
  fxGetArchive,
  entityApi,
  fxCreateIntercom,
  $mode,
  fxUpdateIntercom,
  $isOpenDeleteModal,
  changeVisibilityDeleteModal,
  deleteIntercom,
  fxDeleteIntercom,
} from './detail.model';

const { t } = i18n;

interface Data {
  available_for_residents: boolean;
  ble_en: boolean;
  buildings: { id: number }[];
  button_caption: string;
  camera: { id: number };
  card_en: boolean;
  channel_number: number;
  face_en: boolean;
  finger_en: boolean;
  forpost_id: string;
  intercomLog: string;
  intercomPass: string;
  ip: string;
  model: { id: string };
  name: string;
  open_door_text: string;
  pin_en: boolean;
  pin_en_guest: boolean;
  pin_en_resident: boolean;
  pin_length: number;
  port_rtsp: number;
  port_web: number;
  qr_en: boolean;
  qr_en_guest: number;
  qr_en_resident: number;
  qr_length: number;
  rizers: any;
  serialnumber: string;
  sipNumber: string;
  video_exists: boolean;
}

guard({
  clock: fxGetIntercoms.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetIntercom.prepend(({ entityId }: { entityId: number }) => ({
    serialnumber: entityId,
  })),
});

sample({
  clock: fxGetIntercom.done,
  fn: ({ result: { intercom } }) => {
    if (!Boolean(intercom[0]?.hide_id)) {
      history.push('.');

      return false;
    }

    return true;
  },
  target: $isDetailOpen,
});

sample({
  clock: fxCreateIntercom.done,
  source: $path,
  fn: (path, { result: { serialnumber } }) => {
    if (serialnumber) {
      history.push(`${path}/${serialnumber}`);
    }
  },
});

sample({
  clock: fxDeleteIntercom.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetIntercom.done, () => true)
  .reset([pageUnmounted, fxDeleteIntercom.done]);

$currentTab
  .on(changeTab, (_, tab) => tab)
  .on(addClicked, () => 'settings')
  .reset([pageUnmounted, signout, changedDetailVisibility, openViaUrl]);

sample({
  clock: openViaUrl,
  fn: (serialnumber) => ({
    serialnumber,
  }),
  target: fxGetIntercom,
});

$opened
  .on(fxGetIntercom.done, (_, { result }) => {
    if (!Array.isArray(result?.intercom)) {
      return newEntity;
    }

    if (result.intercom.length === 0) {
      return newEntity;
    }

    const intercom = result.intercom[0];

    return {
      id: intercom.hide_id,
      name: intercom?.name || '',
      available_for_residents: Number(Boolean(intercom?.available_for_residents)),
      mjpeg: intercom?.mjpeg || '',
      buildings: intercom?.buildings || '',
      model: intercom?.model || '',
      rizers:
        intercom?.rizers === '0' || intercom?.rizers === 0
          ? { value: intercom.rizers, title: t('gate') }
          : {
              value: intercom.rizers,
              title: `${t('rizer')} №${intercom.rizers}`,
            } || '',
      ip: intercom?.ip || '',
      port_web: intercom?.port_web || '',
      port_rtsp: intercom?.port_rtsp || '',
      intercomLog: intercom?.login || '',
      intercomPass: intercom?.password || '',
      serialnumber: intercom?.serialnumber || '',
      sipNumber: intercom?.sip_number || '',
      sipPassword: intercom?.sip_password || '',
      sipPort: intercom?.sip_port || '',
      sipServer: intercom?.sip_server || '',
      sipState: intercom?.sip_state || '',
      timestamp: '',
      forpost_id: intercom?.forpost_id || '',
      card_en: Number(Boolean(intercom?.card_en)),
      finger_en: Number(Boolean(intercom?.finger_en)),
      face_en: Number(Boolean(intercom?.face_en)),
      pin_en: Number(Boolean(intercom?.pin_en)),
      ble_en: Number(Boolean(intercom?.ble_en)),
      qr_en: Number(Boolean(intercom?.qr_en)),
      open_door_text:
        (intercom?.open_door_text !== '-' && intercom?.open_door_text) || t('DoorOpened'),
      video_exists: Boolean(intercom?.video_exists),
      button_caption:
        (intercom?.button_caption !== '-' && intercom?.button_caption) || t('open'),
      camera: intercom?.camera_id || '',
      channel_number: intercom?.channel_number || 1,
      pin_length: intercom?.pin_length || 6,
      pin_en_guest: Boolean(intercom?.pin_en_guest),
      pin_en_resident: Boolean(intercom?.pin_en_resident),
      qr_length: intercom?.qr_length || 60,
      qr_en_guest: Boolean(intercom?.qr_en_guest),
      qr_en_resident: Boolean(intercom?.qr_en_resident),
      collection_key: intercom?.collection_key || 0,
      camera_id: intercom?.camera_id || 0,
      cameraForVideo: intercom?.camera || '',
      openDoor: intercom?.['open-url'] || '',
      delete_param: intercom?.delete_param || '',
    };
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: changeCollectionKey,
  source: $opened,
  fn: ({ id, collection_key }) => ({
    serialnumber: id,
    state: Number(!Boolean(collection_key)),
  }),
  target: fxChangeCollectionKey,
});

sample({
  clock: fxChangeCollectionKey.done,
  fn: ({ params: { serialnumber } }) => ({
    serialnumber,
  }),
  target: fxGetIntercom,
});

sample({
  clock: getArchive,
  fn: ({ camera_id, timestamp }) => ({
    camera_id,
    timestamp: new Date(timestamp).getTime() / 1000,
  }),
  target: fxGetArchive,
});

$archive
  .on(fxGetArchive.done, (_, { result: { url } }) => url)
  .reset(pageUnmounted, signout, changeTab, changedDetailVisibility);

sample({
  clock: entityApi.create,
  fn: (data) => formatData(data),
  target: fxCreateIntercom,
});

const formatData = (data: Data) => ({
  ip: data.ip,
  name: data.name,
  model: data.model.id,
  rizers:
    data?.rizers?.id === 0
      ? 0
      : Number.parseInt(data.rizers.id, 10) || Number.parseInt(data.rizers.value, 10),
  login: data.intercomLog,
  password: data.intercomPass,
  forpost_id: data.forpost_id,
  card_en: Number(data.card_en),
  finger_en: Number(data.finger_en),
  face_en: Number(data.face_en),
  pin_en: Number(data.pin_en),
  ble_en: Number(data.ble_en),
  qr_en: Number(data.qr_en),
  port_web: data.port_web,
  port_rtsp: data.port_rtsp,
  buildings: data.buildings
    .map((build: { id: number }) => (typeof build === 'object' ? build.id : build))
    .join(','),
  button_caption: data.button_caption,
  open_door_text: data.open_door_text,
  video_exists: data.video_exists ? 1 : 0,
  camera_id: typeof data.camera === 'object' ? data.camera.id : data.camera,
  channel_number: Number(data.channel_number),
  pin_length: data.pin_length,
  pin_en_guest: Number(data.pin_en_guest),
  pin_en_resident: Number(data.pin_en_resident),
  qr_length: data.qr_length,
  qr_en_guest: Number(data.qr_en_guest),
  qr_en_resident: Number(data.qr_en_resident),
  available_for_residents: Number(data.available_for_residents),
});

sample({
  clock: fxCreateIntercom.done,
  fn: ({ result: { serialnumber } }) => ({
    serialnumber,
  }),
  target: fxGetIntercom,
});

$mode.reset([fxCreateIntercom.done, fxUpdateIntercom.done]);

sample({
  clock: entityApi.update,
  fn: ({ id, ...restData }) => ({ serialnumber: id, ...formatData(restData) }),
  target: fxUpdateIntercom,
});

sample({
  clock: fxUpdateIntercom.done,
  fn: ({ result: { serialnumber } }) => ({
    serialnumber,
  }),
  target: fxGetIntercom,
});

$isOpenDeleteModal
  .on(changeVisibilityDeleteModal, (_, visibility) => visibility)
  .reset([pageUnmounted, signout, fxDeleteIntercom.done]);

sample({
  clock: deleteIntercom,
  source: $opened,
  fn: ({ delete_param }) => ({ param: delete_param }),
  target: fxDeleteIntercom,
});

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
