import { createEffect, createStore } from 'effector';
import { IntercomModel, IntercomModelResponse } from '../../interfaces';

export const fxGetList = createEffect<void, IntercomModelResponse, Error>();

export const $data = createStore<IntercomModel[]>([]);
export const $isLoading = createStore<boolean>(false);
