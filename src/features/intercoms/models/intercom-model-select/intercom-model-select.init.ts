import { signout } from '@features/common';
import { intercomsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './intercom-model-select.model';

fxGetList.use(intercomsApi.getIntercomModel);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.model)) {
      return [];
    }

    return result.model;
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);
