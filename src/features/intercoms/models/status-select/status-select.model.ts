import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  {
    id: 'online',
    title: t('Online'),
  },
  {
    id: 'offline',
    title: t('Offline'),
  },
];
