import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'apartment/approval/manager/drop-down-list');

export const managersApi = { getList };
