import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { managersApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data.on(fxGetList.doneData, (_, { managers }) => managers).reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

fxGetList.use(managersApi.getList);

export { fxGetList, $data, $error, $isLoading };
