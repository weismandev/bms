import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const ManagersSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      error={error}
      getOptionLabel={(option) => (option ? option.fio : '')}
      getOptionValue={(option) => option && option.id}
      {...props}
    />
  );
};

const ManagersSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ManagersSelect />} onChange={onChange} {...props} />;
};

export { ManagersSelect, ManagersSelectField };
