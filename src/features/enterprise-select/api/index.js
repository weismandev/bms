import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'enterprises/get-simple-list');

export const enterpriseApi = { getList };
