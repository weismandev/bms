import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterpriseSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

/** Удаляю введенный офис при изменении компании */
const setDefaultProperty = (props) => {
  const { parent_name, idx } = props;
  const formValues = props.form.values;

  if (!formValues[parent_name] || !formValues[parent_name][idx]) return;
  formValues[parent_name][idx].property = '';
};

const EnterpriseSelectField = (props) => {
  const onChange = (value) => {
    setDefaultProperty(props);

    return props.form.setFieldValue(props.field.name, value);
  };

  return <SelectField component={<EnterpriseSelect />} onChange={onChange} {...props} />;
};

export { EnterpriseSelect, EnterpriseSelectField };
