import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const MarketCategoriesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const MarketCategoriesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<MarketCategoriesSelect />} onChange={onChange} {...props} />
  );
};

export { MarketCategoriesSelect, MarketCategoriesSelectField };
