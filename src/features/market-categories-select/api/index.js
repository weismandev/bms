import { api } from '../../../api/api2';

const getList = () => api.v2('/client/admin/marketplace/categories/list');

export const categoriesApi = { getList };
