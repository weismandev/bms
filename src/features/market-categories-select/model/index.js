import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { categoriesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

$data.on(fxGetList.doneData, (_, { categories }) => categories || []).reset(signout);

fxGetList.use(categoriesApi.getList);

export { fxGetList, $data, $error, $isLoading };
