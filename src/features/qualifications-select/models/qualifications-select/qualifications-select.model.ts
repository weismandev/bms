import { createEffect, createStore } from 'effector';

import { GetQualificationsResponse, Qualification } from '../../interfaces';

export const fxGetList = createEffect<void, GetQualificationsResponse, Error>();

export const $data = createStore<Qualification[]>([]);
export const $isLoading = createStore<boolean>(false);
