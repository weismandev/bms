import { signout } from '@features/common';

import { qualificationSelectApi } from '../../api';
import { fxGetList, $data, $isLoading } from './qualifications-select.model';

fxGetList.use(qualificationSelectApi.getQualifications);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.qualifications)) {
      return [];
    }

    return result.qualifications.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
