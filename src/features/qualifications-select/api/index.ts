import { api } from '@api/api2';

const getQualifications = () =>
  api.v4('get', 'qualifications/list', { per_page: 99999999999 });

export const qualificationSelectApi = {
  getQualifications,
};
