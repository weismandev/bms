export interface Qualification {
  id: number;
  title: string;
}

export interface GetQualificationsResponse {
  qualifications: Qualification[];
}
