import { createEvent, createStore, restore } from 'effector';

const defaultFilters = {
  building: '',
  apartment: '',
};

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = restore(changedFilterVisibility, false);
const $filters = createStore(defaultFilters);

export {
  changedFilterVisibility,
  $isFilterOpen,
  filtersSubmitted,
  $filters,
  defaultFilters,
};
