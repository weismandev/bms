import { merge } from 'effector';
import { signout } from '../../common';
import { fxGetChatsList, fxMarkAsRead } from './chats.model';
import { fxSendMessage, fxGetMessagesList, fxCreateNewChat } from './messages.model';
import {
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
  $error,
  pageUnmounted,
} from './page.model';

const request = [
  fxSendMessage.pending,
  fxCreateNewChat.pending,
  fxGetChatsList.pending,
  fxMarkAsRead.pending,
];

const errorOccured = merge([
  fxGetMessagesList.failData,
  fxSendMessage.failData,
  fxCreateNewChat.failData,
  fxGetChatsList.failData,
  fxMarkAsRead.failData,
]);

$error
  .on(errorOccured, (state, { error }) => error)
  .on(request, () => null)
  .reset([signout, pageUnmounted]);

$isErrorDialogOpen
  .on(errorOccured, () => true)
  .on(request, () => false)
  .on(errorDialogVisibilityChanged, (state, visibility) => visibility)
  .reset([signout, pageUnmounted]);
const $isNewChatLoading = fxCreateNewChat.pending;
export { errorOccured, $isNewChatLoading };
