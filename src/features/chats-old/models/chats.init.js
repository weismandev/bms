import { forward, guard, sample, attach } from 'effector';
import { debounce } from 'patronum/debounce';
import { throttle } from 'patronum/throttle';
import format from 'date-fns/format';
import { grey } from '@mui/material/colors';
import { getPathOr } from '../../../tools/path';
import { signout, readNotifications, NEW_MESSAGE } from '../../common';
import { chatsApi } from '../api';
import {
  ChatsGate,
  fxGetChatsList,
  $chatsList,
  bottomReached,
  $countToShow,
  $openedChatId,
  fxMarkAsRead,
  searchChanged,
  $search,
} from './chats.model';
import {
  filtersSubmitted,
  $filters,
  $isFilterOpen,
  defaultFilters,
} from './filter.model';
import { $colors } from './messages.model';

fxGetChatsList.use(chatsApi.getChatsList);
fxMarkAsRead.use(chatsApi.markMessageAsRead);

$search.on(searchChanged, (state, value) => value).reset(signout);

$chatsList
  .on(
    sample($colors, fxGetChatsList.doneData, (colors, { chats = [] }) => ({
      colors,
      chats,
    })),
    (state, { chats, colors }) =>
      state.map((i) => i.id).join(',') ===
      chats
        .map((i) =>
          i.message_chat_id > -1
            ? String(i.message_chat_id)
            : `${i.message_chat_id}-${i.userdata_id}`
        )
        .join(',')
        ? state
        : formatChatsList({ chats, colors })
  )
  .on(fxMarkAsRead.done, (state, { params }) =>
    state.map((i) =>
      String(i.id) === String(params.chat_id) ? { ...i, unreadCount: 0 } : i
    )
  )
  .reset(signout);

$countToShow.on(bottomReached, (state) => state + 30).reset(signout);

$openedChatId.on(ChatsGate.state.updates, (state, { chatId }) => chatId).reset(signout);

const triggerToPoll = guard({
  source: sample(ChatsGate.status, fxGetChatsList.done),
  filter: Boolean,
});

const debounceedPoll = debounce({ source: triggerToPoll, timeout: 15000 });

const throttledSearch = throttle({ source: $search.updates, timeout: 500 });

$isFilterOpen.reset(signout);

$filters
  .on(filtersSubmitted, (state, filters) => (filters ? filters : defaultFilters))
  .reset(signout);

$filters.reset(signout);

forward({
  from: [throttledSearch, ChatsGate.open, debounceedPoll, filtersSubmitted],
  to: attach({
    effect: fxGetChatsList,
    source: [$search, $filters],
    mapParams: (params, [search, filters]) => {
      const payload = {};
      if (getPathOr('building.id', filters, '')) {
        payload.building_id = filters.building.id;
        payload.apartment_id = getPathOr('apartment.id', filters, '');
      }

      if (search) {
        payload.filter = search;
      }
      return payload;
    },
  }),
});

guard({
  source: sample({
    source: $openedChatId,
    fn: (chatId) => ({ event: NEW_MESSAGE, id: chatId }),
  }),
  filter: ({ id }) => Boolean(id),
  target: readNotifications,
});

guard({
  source: $openedChatId,
  filter: Boolean,
  target: attach({
    effect: fxMarkAsRead,
    mapParams: (id) => ({ chat_id: id }),
  }),
});

function formatChatsList({ chats = [], colors = {} }) {
  const newYear2000 = new Date('01.01.2000') / 1000;

  return chats.map((i) => {
    const groups = i.apartment_title.match(/\(\s?(?<address>.+)\s?\)/i).groups;
    const author = i.userdata_fullname || i.userdata_email || 'Неизвестный';
    return {
      id:
        i.message_chat_id > -1
          ? String(i.message_chat_id)
          : `${i.message_chat_id}-${i.userdata_id}`,
      author,
      address: (groups && groups.address) || i.message_chat_title || 'Не определен',
      lastMessage: i.message_last_message || 'Никто еще ничего не написал',
      date:
        i.message_last_dt > newYear2000
          ? format(new Date(i.message_last_dt * 1000), 'dd.MM.yyyy')
          : '',
      time:
        i.message_last_dt > newYear2000
          ? format(new Date(i.message_last_dt * 1000), 'HH:mm')
          : '',
      unreadCount: i.not_read || 0,
      color: colors[author] ? colors[author][500] : grey[500],
    };
  });
}
