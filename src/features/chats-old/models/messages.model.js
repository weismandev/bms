import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { grey } from '@mui/material/colors';

const MessagesGate = createGate();

const sendSubmitted = createEvent();

const fxSendMessage = createEffect();

const fxGetMessagesList = createEffect();

const $messagesData = createStore([]);
const $messagesDataLoading = createStore(false);

const $colors = createStore({ hidden: grey });

const $isNewChatModalOpen = createStore(false);
const $selectedUser = createStore(null);

const openNewChatModal = createEvent();
const setUser = createEvent();
const closeNewChatModal = createEvent();
const createNewChat = createEvent();
const fxCreateNewChat = createEffect();
const $isSendMessagePending = createStore(false);

export {
  $messagesData,
  $messagesDataLoading,
  fxSendMessage,
  fxGetMessagesList,
  sendSubmitted,
  $colors,
  MessagesGate,
  $isNewChatModalOpen,
  openNewChatModal,
  closeNewChatModal,
  createNewChat,
  fxCreateNewChat,
  $selectedUser,
  setUser,
  $isSendMessagePending,
};
