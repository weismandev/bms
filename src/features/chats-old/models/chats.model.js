import { createEvent, createEffect, createStore, combine } from 'effector';
import { createGate } from 'effector-react';

const ChatsGate = createGate();

const bottomReached = createEvent();

const fxGetChatsList = createEffect();

const fxMarkAsRead = createEffect();

const $chatsList = createStore([]);
const $countToShow = createStore(30);
const $openedChatId = createStore(null);

const $chatsListToShow = combine($chatsList, $countToShow, (list, count) =>
  list.slice(0, count)
);

const searchChanged = createEvent();

const $search = createStore('');
const $isChatListLoading = fxGetChatsList.pending;

export {
  ChatsGate,
  searchChanged,
  $search,
  bottomReached,
  fxGetChatsList,
  $chatsList,
  $chatsListToShow,
  $countToShow,
  $openedChatId,
  $isChatListLoading,
  fxMarkAsRead,
};
