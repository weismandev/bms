import { forward, attach, sample, guard, merge } from 'effector';
import { debounce } from 'patronum';
import { getRandomColors } from '../../../tools/getRandomColors';
import { signout, history } from '../../common';
import { chatsApi } from '../api';
import { fxGetChatsList } from './chats.model';
import {
  fxSendMessage,
  fxGetMessagesList,
  fxCreateNewChat,
  sendSubmitted,
  openNewChatModal,
  closeNewChatModal,
  $isNewChatModalOpen,
  $isSendMessagePending,
  createNewChat,
  $messagesData,
  $messagesDataLoading,
  $colors,
  setUser,
  $selectedUser,
  MessagesGate,
} from './messages.model';
import { errorOccured } from './page.init';

const delayedSendMessagePending = debounce({
  source: fxSendMessage.done,
  timeout: 4000,
});

$isSendMessagePending
  .on(fxSendMessage, () => true)
  .on([delayedSendMessagePending, fxSendMessage.fail], () => false);

fxGetMessagesList.use(chatsApi.getMessagesList);
fxSendMessage.use(chatsApi.sendMessage);
fxCreateNewChat.use(chatsApi.createChat);

$selectedUser
  .on(setUser, (state, value) => value)
  .reset([signout, errorOccured, fxCreateNewChat.doneData]);

sample({
  clock: createNewChat,
  source: $selectedUser,
  fn: (user) => ({ userdata_id: user.id }),
  target: fxCreateNewChat,
});

fxCreateNewChat.doneData.watch((res) => {
  if (res && res.chat_id) {
    closeNewChatModal();
    history.push(`/messages/${res.chat_id}`);
  }
});

const gotMessagesList = sample({
  source: $colors,
  clock: fxGetMessagesList.done,
  fn: (colors, { result, params }) => {
    return {
      colors,
      data: result && result.messages,
      requestChatId: params && params.id,
    };
  },
});

const gotMessagesListOnDetailPage = guard({
  source: sample(
    MessagesGate.state,
    gotMessagesList,
    ({ chatId }, { colors, data, requestChatId }) => ({
      chatId,
      colors,
      data,
      requestChatId,
    })
  ),
  filter: ({ chatId, requestChatId }) => Boolean(chatId) && chatId === requestChatId,
});

$isNewChatModalOpen.on(openNewChatModal, () => true).on(closeNewChatModal, () => false);

forward({
  from: fxSendMessage.doneData,
  to: fxGetMessagesList,
});

$messagesDataLoading
  .on(MessagesGate.state.updates, () => true)
  .on([fxGetMessagesList.doneData, gotMessagesListOnDetailPage], () => false)
  .reset([MessagesGate.close, signout]);

$messagesData
  .on(gotMessagesListOnDetailPage, (state, { colors, data }) => {
    const lastStateMessage = state[state.length - 1];
    const lastReceivedMessage = data[data.length - 1];

    if (
      (lastStateMessage && String(lastStateMessage.id)) ===
      (lastReceivedMessage && String(lastReceivedMessage.id))
    ) {
      return state;
    }

    return formatMessagesData({ data, colors });
  })
  .reset([MessagesGate.close, MessagesGate.state.updates, signout]);

$colors
  .on(fxGetMessagesList.doneData, (state, data) =>
    getRandomColors(
      data.messages.map((i) => i.autor),
      state
    )
  )
  .on(fxGetChatsList.doneData, (state, { chats }) =>
    getRandomColors(
      chats.map((i) => i.userdata_fullname || i.userdata_email || 'Неизвестный'),
      state
    )
  )
  .reset(signout);

const delayedPoll = debounce({
  source: sample(MessagesGate.state, fxGetMessagesList.done),
  timeout: 3000,
});

guard({
  source: merge([MessagesGate.state.updates, delayedPoll]),
  filter: ({ chatId }) => Boolean(chatId),
  target: attach({
    effect: fxGetMessagesList,
    mapParams: ({ chatId }) => ({ id: chatId }),
  }),
});

forward({
  from: sample({
    source: MessagesGate.state,
    clock: sendSubmitted,
    fn: ({ chatId: id }, payload) => ({ id, ...payload }),
  }),
  to: attach({
    effect: fxSendMessage,
    mapParams: ({ id, is_hidden, text, attachments = [] }) => ({
      id,
      hidden: is_hidden ? 1 : 0,
      text,
      files: Array.isArray(attachments) ? attachments.map((i) => i.id).join(',') : '',
    }),
  }),
});

function formatMessagesData({ data, colors }) {
  return data.map(({ autor, hidden, my, dt, ...rest }) => ({
    ...rest,
    author: autor,
    color: hidden ? colors.hidden : colors[autor] || colors.hidden,
    is_hidden: Boolean(hidden),
    isMine: Boolean(my),
    created_at: dt,
    contact: { name: autor || 'Имя неизвестно' },
  }));
}
