import { createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();
const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;
const errorDialogVisibilityChanged = createEvent();
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);

export {
  PageGate,
  pageMounted,
  pageUnmounted,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
};
