import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '../../common';

import { ErrorMessage, TwoColumnLayout } from '../../../ui';

import { ChatsList, MessagesPanel, NewChatModal, NoSelectedChat } from '../organisms';
import {
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models/page.model';

const ChatsPage = (props) => {
  const { url } = useRouteMatch();
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);

  return (
    <>
      <PageGate />
      <TwoColumnLayout
        left={<ChatsList />}
        right={
          <Switch>
            <Route
              path={`${url}/:chatId`}
              render={({ match }) => (
                <MessagesPanel
                  openedChannel={match && match.params && match.params.chatId}
                />
              )}
            />
            <Route exact path={url} component={NoSelectedChat} />
          </Switch>
        }
      />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <NewChatModal />
    </>
  );
};

const RestrictedChatsPage = (props) => {
  return (
    <HaveSectionAccess>
      <ChatsPage />
    </HaveSectionAccess>
  );
};

export { RestrictedChatsPage as ChatsPage };
