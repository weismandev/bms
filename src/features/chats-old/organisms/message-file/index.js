import { useState } from 'react';
import { ActionFileWrapper, DialogImage } from '../../../../ui';

const MessageFile = (props) => {
  const { url, name } = props;

  const [isMessageImageOpen, changeMessageImageVisibility] = useState(false);

  const openMessageImageDialog = () => changeMessageImageVisibility(true);
  const imgStyles = {
    cursor: 'pointer',
  };

  return (
    <>
      <ActionFileWrapper
        openImageDialog={openMessageImageDialog}
        imgStyles={imgStyles}
        {...props}
      />
      <DialogImage
        isOpen={isMessageImageOpen}
        changeVisibility={changeMessageImageVisibility}
        src={url}
        title={name}
      />
    </>
  );
};

export { MessageFile };
