import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Toolbar, SearchInput, Greedy, AddButton, FilterButton } from '../../../../ui';
import { searchChanged, $search } from '../../models/chats.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import { openNewChatModal } from '../../models/messages.model';
import { Filter } from '../filter';

const useStyles = makeStyles({
  margin: {
    margin: '24px 0 24px 24px;',
  },
  toolbar: {
    position: 'sticky',
    top: 0,
    background: 'white',
    zIndex: 2,
  },
  addButton: {
    margin: '24px',
  },
});

export const ChatsToolbar = (props) => {
  const classes = useStyles();

  const searchValue = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <div className={classes.toolbar}>
      <Toolbar>
        <FilterButton
          className={classes.margin}
          disabled={isFilterOpen}
          onClick={() => changedFilterVisibility(true)}
          titleAccess="Фильтр"
        />
        <SearchInput
          className={classes.margin}
          value={searchValue}
          onChange={(e) => searchChanged(e.target.value)}
          handleClear={() => searchChanged('')}
        />
        <Greedy />
        <AddButton
          onClick={openNewChatModal}
          className={classes.addButton}
          titleAccess="Создать чат"
        />
      </Toolbar>
      {isFilterOpen ? <Filter /> : null}
    </div>
  );
};
