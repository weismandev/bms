import { Typography } from '@mui/material';
import { Wrapper, Dumb } from '../../../../ui';

export const NoSelectedChat = () => (
  <Wrapper style={{ height: '100%' }}>
    <Dumb>
      <Typography
        style={{
          color: '#7d7d8e',
          fontWeight: 900,
          fontSize: 24,
        }}
        variant="body1"
      >
        Чат не выбран. Выберите чат
      </Typography>
    </Dumb>
  </Wrapper>
);
