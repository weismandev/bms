import { useStore } from 'effector-react';
import { Grid, Typography } from '@mui/material';
import { Wrapper, MessagesList, Dumb, Loader } from '../../../../ui';
import {
  $messagesData,
  $messagesDataLoading,
  sendSubmitted,
  $colors,
  MessagesGate,
  $isSendMessagePending,
} from '../../models/messages.model';
import { Message } from '../message';
import { MessageBlock } from '../message-block';

const MessagesPanel = (props) => {
  const messages = useStore($messagesData);
  const messagesDataLoading = useStore($messagesDataLoading);
  const isSendMessagePending = useStore($isSendMessagePending);

  const colors = useStore($colors);

  const { openedChannel } = props;
  const hasMessages = messages.length !== 0;

  let listElement;

  const isLoading = openedChannel ? isSendMessagePending || messagesDataLoading : false;

  if (isLoading) {
    listElement = (
      <Grid style={{ height: '100%' }}>
        <Dumb>
          <Loader isLoading={isLoading} />
        </Dumb>
      </Grid>
    );
  } else if (!hasMessages) {
    listElement = (
      <Grid style={{ height: '100%' }}>
        <Dumb>
          <Typography style={{ color: '#7d7d8e', fontWeight: 900, fontSize: 24 }}>
            Сообщений пока нет, но Вы можете начать беседу))
          </Typography>
        </Dumb>
      </Grid>
    );
  } else {
    listElement = (
      <Grid item style={{ flexGrow: 1, height: '100%', overflow: 'hidden' }}>
        <MessagesList colors={colors} messages={messages}>
          <Message />
        </MessagesList>
      </Grid>
    );
  }

  const chatContent = (
    <Grid
      container
      direction="column"
      wrap="nowrap"
      style={{ height: '100%', overflow: 'hidden' }}
      component={Wrapper}
    >
      {listElement}
    </Grid>
  );

  const chatMessageBox = openedChannel ? (
    <Wrapper style={{ marginTop: 24, overflow: 'unset' }}>
      <MessageBlock send={sendSubmitted} />
    </Wrapper>
  ) : null;

  return (
    <>
      <MessagesGate chatId={openedChannel} />
      <Grid
        container
        direction="column"
        style={{ height: '89vh', position: 'relative' }}
        wrap="nowrap"
      >
        {chatContent}
        {chatMessageBox}
      </Grid>
    </>
  );
};

export { MessagesPanel };
