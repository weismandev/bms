import { useStore } from 'effector-react';
import { Modal, ActionButton } from '../../../../ui';
import { AsyncPeopleSelect } from '../../../people-async-select';
import {
  createNewChat,
  $isNewChatModalOpen,
  closeNewChatModal,
  setUser,
  $selectedUser,
} from '../../models/messages.model';
import { $isNewChatLoading } from '../../models/page.init';

const NewChatModal = (props) => {
  const selectedUser = useStore($selectedUser);
  const isNewChatModalOpen = useStore($isNewChatModalOpen);
  const isNewChatLoading = useStore($isNewChatLoading);

  return (
    <Modal
      isOpen={isNewChatModalOpen}
      onClose={closeNewChatModal}
      header="Создание нового чата с пользователем"
      content={
        <div style={{ minHeight: 400, paddingTop: 20 }}>
          <AsyncPeopleSelect
            onChange={setUser}
            value={selectedUser}
            placeholder="Введите ФИО пользователя"
            helpText="Чтобы появился список пользователей, введите хотя бы одну букву"
            divider={false}
            label={null}
          />
          {selectedUser ? (
            <ActionButton
              disabled={isNewChatLoading}
              onClick={createNewChat}
              style={{ marginTop: 20 }}
            >
              Создать чат с пользователем
            </ActionButton>
          ) : null}
        </div>
      }
    />
  );
};

export { NewChatModal };
