import { useStore } from 'effector-react';
import { Formik, Field } from 'formik';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { AttachFile, Send } from '@mui/icons-material';
import {
  MessageInput,
  AttachedFilesPanel,
  MessageBox,
  FilesList,
  File,
  MessageVisibilityToggle,
  FileControl,
} from '../../../../ui';
import { $isCompany } from '../../../common';

const ChatMessageInput = (props) => {
  return <MessageInput {...props.field} />;
};

const MessageBlock = (props) => {
  const { send } = props;
  const isCompany = useStore($isCompany);

  return (
    <Formik
      initialValues={{
        text: '',
        attachments: [],
        is_hidden: false,
      }}
      onSubmit={(values, { resetForm }) => {
        send(values);
        resetForm();
      }}
      render={({ handleSubmit, values, setFieldValue }) => {
        return (
          <>
            <MessageBox>
              <Field
                name="attachments"
                render={({ field, form }) => {
                  return (
                    <FileControl
                      name={field.name}
                      onChange={(value) => form.setFieldValue(field.name, value)}
                      renderPreview={null}
                      loadable
                      api="no_vers"
                      inputProps={{
                        accept: ['image/jpeg', 'image/png'],
                        multiple: true,
                      }}
                      renderButton={() => (
                        <AttachFile
                          className="txt-gray"
                          style={{
                            marginRight: '16px',
                            transform: 'rotate(45deg)',
                            cursor: 'pointer',
                            color: 'rgb(117, 117, 117)',
                          }}
                        />
                      )}
                    />
                  );
                }}
              />
              {!isCompany ? (
                <Field name="is_hidden" component={MessageVisibilityToggle} />
              ) : null}
              <Field name="text" component={ChatMessageInput} />
              <Send
                style={{ cursor: 'pointer', color: 'rgb(117, 117, 117)' }}
                onClick={handleSubmit}
              />
            </MessageBox>
            <AttachedFilesPanel
              isOpen={values.attachments.length > 0 && Boolean(values.attachments[0])}
            >
              <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                scroll={false}
              >
                <Grid>
                  <FilesList files={values.attachments}>
                    <File />
                  </FilesList>
                </Grid>

                <Grid>
                  <Button
                    color="primary"
                    style={{ margin: '5px', padding: '5px' }}
                    onClick={() => setFieldValue('attachments', [])}
                  >
                    Удалить файлы
                  </Button>
                </Grid>
              </Grid>
            </AttachedFilesPanel>
          </>
        );
      }}
    />
  );
};

export { MessageBlock };
