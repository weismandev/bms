import { Link } from 'react-router-dom';
import cn from 'classnames';
import {
  Avatar,
  Typography,
  Badge,
  Grid,
  Tooltip,
  ListItem,
  Divider,
} from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  item: {
    textDecoration: 'none',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    height: 140,
    padding: '24px 24px 15px 24px',
    '&:hover': {
      textDecoration: 'none',
      background: '#E1EBFF !important',
      "& + *[role='separator']": {
        background: '#E1EBFF !important',
      },
      "& *[class*='headerText']": {
        color: '#0394E3 !important',
      },
    },
  },
  selected: {
    textDecoration: 'none',
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  },
  divider: {
    padding: '0 24px',
    height: 'auto',
    '& > *': {
      width: '100%',
    },
  },
  avatar: {
    width: 71,
    height: 71,
    fontSize: '2em',
  },
  h3: {
    fontWeight: '900',
    width: 'inherit',
  },
  author: {
    color: '#0394E3',
    fontSize: '1.15em',
  },
  address: {
    background: '#e7e7ec',
    color: '#3B3B50',
    fontSize: '0.93em',
    borderRadius: '3px',
    padding: '4px 8px',
    margin: '5px 0',
  },
  selectedAddress: {
    background: '#fff',
  },
  subtitle1: {
    fontWeight: '300',
    fontSize: '1em',
    color: '#7D7D8E',
    height: '30px',
  },
  text: {
    width: 'inherit',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    lineHeight: '1.75',
  },
  date: {
    fontWeight: '300',
    fontSize: '0.85em',
    color: '#7D7D8E',
    padding: '0 10px',
  },
  badge: {
    bottom: '-10px',
    right: '10px',
    top: 'unset',
  },
});

export function ChatItem(props) {
  const {
    author,
    address,
    lastMessage,
    date,
    time,
    unreadCount,
    message_chat_count,
    color,
    selected,
    ...restProps
  } = props;

  const classes = useStyles();

  const authorStyle = cn(classes.h3, classes.text, classes.author);
  const addressStyle = cn(
    classes.h3,
    classes.text,
    classes.address,
    selected && classes.selectedAddress
  );
  const lastMessageStyle = cn(classes.text, classes.subtitle1);

  const avatarLetter = author && author[0].toUpperCase();

  return (
    <li>
      <ListItem
        classes={{ root: classes.item, selected: classes.selected }}
        selected={selected}
        component={Link}
        {...restProps}
      >
        <Grid container>
          <Grid item lg={3} md={3} sm={3} xl={3} xs={3}>
            <Grid container justifyContent="center" alignItems="flex-start">
              <Badge
                badgeContent={unreadCount}
                max={99}
                color="error"
                classes={{ badge: classes.badge }}
              >
                <Avatar style={{ background: color }} className={classes.avatar}>
                  {avatarLetter}
                </Avatar>
              </Badge>
            </Grid>
          </Grid>

          <Grid item lg={6} md={6} sm={6} xl={6} xs={6}>
            <Grid container justifyContent="space-around" direction="column">
              <Typography variant="h3" classes={{ h3: authorStyle }}>
                {author}
              </Typography>

              <Typography variant="h3" classes={{ h3: addressStyle }}>
                <Tooltip title={address}>
                  <span>{address}</span>
                </Tooltip>
              </Typography>

              <Typography variant="subtitle1" classes={{ subtitle1: lastMessageStyle }}>
                {lastMessage}
              </Typography>
            </Grid>
          </Grid>

          {date && time && (
            <Grid item lg={3} md={3} sm={3} xl={3} xs={3}>
              <Grid container justifyContent="flex-end">
                <Typography variant="subtitle1" classes={{ subtitle1: classes.date }}>
                  {date}
                </Typography>
                <Typography variant="subtitle1" classes={{ subtitle1: classes.date }}>
                  в {time}
                </Typography>
              </Grid>
            </Grid>
          )}
        </Grid>
      </ListItem>
      <ListItem component="div" classes={{ root: classes.divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </li>
  );
}
