import {
  MessageText,
  MessageAuthor,
  Message,
  MessageHidden,
  MessageDateTime,
  FilesList,
} from '../../../../ui';
import { MessageFile } from '../message-file';

export const CustomMessage = ({ message, ...props }) => {
  const { contact, color, is_hidden, files, created_at, text } = message;

  const author = contact?.name || 'Имя неизвестно';
  const attaches = Array.isArray(files) ? files : [];

  return (
    <Message {...props} color={color} message={message}>
      <MessageHidden isHidden={is_hidden} />
      <MessageAuthor author={author} color={color[900]} />
      <MessageText text={text} />
      <FilesList files={attaches}>
        <MessageFile />
      </FilesList>
      <MessageDateTime date={created_at} />
    </Message>
  );
};

export { CustomMessage as Message };
