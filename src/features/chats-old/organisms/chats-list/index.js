import { useRef } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useList, useStore } from 'effector-react';
import { List, Typography } from '@mui/material';
import { Wrapper, CustomScrollbar, Loader } from '../../../../ui';
import {
  ChatsGate,
  bottomReached,
  $chatsListToShow,
  $isChatListLoading,
} from '../../models/chats.model';
import { $isFilterOpen } from '../../models/filter.model';
import { ChatItem } from '../chats-item';
import { ChatsToolbar } from '../chats-toolbar';

export const ChatsList = (props) => {
  const { url } = useRouteMatch();

  const match = useRouteMatch(`${url}/:chatId`);
  const isChatListLoading = useStore($isChatListLoading);
  const params = match ? match.params : { chatId: null };
  const isFilterOpen = useStore($isFilterOpen);

  const list = useList($chatsListToShow, {
    keys: [params.chatId],
    fn: (data) => (
      <ChatItem
        selected={params.chatId === data.id}
        to={`${url}${url.endsWith('/') ? '' : '/'}${data.id}`}
        {...data}
      />
    ),
  });

  const scrollbarRef = useRef(null);

  return (
    <>
      <ChatsGate chatId={params ? params.chatId : null} />
      <Wrapper style={{ height: '89vh' }}>
        <ChatsToolbar />
        <CustomScrollbar
          style={{
            height: isFilterOpen ? 'calc(89vh - 308px)' : 'calc(89vh - 82px)',
          }}
          ref={scrollbarRef}
          trackVerticalStyle={{ zIndex: 2 }}
          onScrollStop={() => {
            const { scrollTop, clientHeight, scrollHeight } =
              scrollbarRef.current.getValues();

            if (scrollHeight === scrollTop + clientHeight) {
              bottomReached();
            }
          }}
        >
          <Loader isLoading={isChatListLoading} />
          {!isChatListLoading && !list.length ? (
            <Typography
              style={{
                color: '#7d7d8e',
                fontWeight: 900,
                fontSize: 24,
                margin: 24,
              }}
              variant="body1"
            >
              Не найдено ни одного чата
            </Typography>
          ) : null}
          <List disablePadding>{list}</List>
        </CustomScrollbar>
      </Wrapper>
    </>
  );
};
