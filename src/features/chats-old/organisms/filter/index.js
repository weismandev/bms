import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { FilterFooter } from '../../../../ui';
import { ApartmentSelectField } from '../../../apartment-select';
import { HouseSelectField } from '../../../house-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const Filter = memo((props) => {
  const filters = useStore($filters);

  return (
    <div>
      <Formik
        initialValues={filters}
        onSubmit={filtersSubmitted}
        onReset={() => filtersSubmitted('')}
        enableReinitialize
        render={(props) => {
          return (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="building"
                component={HouseSelectField}
                onChange={(value) => {
                  props.setFieldValue('building', value);
                  props.setFieldValue('apartment', '');
                }}
                label="Дом"
                required
                placeholder="Выберите дом"
              />
              <Field
                name="apartment"
                component={ApartmentSelectField}
                // isDisabled={!props.values.building}
                building_id={
                  props.values.building && typeof props.values.building === 'object'
                    ? props.values.building.id
                    : props.values.building
                }
                // helpText="Сначала выберите дом"
                label="Квартира"
                placeholder="Выберите квартиру"
              />
              <FilterFooter
                isResettable={true}
                hide={() => changedFilterVisibility(false)}
              />
            </Form>
          );
        }}
      />
    </div>
  );
});

export { Filter };
