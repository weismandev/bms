import { api } from '../../../api/api2';

const getChatsList = (payload = {}) =>
  api.no_vers('get', 'helpdesk/get_all_bms_chats', { ...payload, limit: 50 });

const markMessageAsRead = (payload = {}) =>
  api.no_vers('post', 'message/mark-as-read', payload);

const getMessagesList = (payload = {}) =>
  api.no_vers('get', 'message/get-chat-messages', {
    order: 'asc',
    limit: 1000,
    offset: 0,
    ...payload,
  });

const sendMessage = (payload = {}) =>
  api.no_vers('post', 'message/send-message/', payload);

const createChat = (payload = {}) =>
  api.no_vers('post', 'message/create-service-management-chat/', payload);

export const chatsApi = {
  getChatsList,
  markMessageAsRead,
  getMessagesList,
  sendMessage,
  createChat,
};
