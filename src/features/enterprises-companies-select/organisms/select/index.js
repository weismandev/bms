import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesCompaniesSelect = (props) => {
  const { firstAsDefault = false, ...restProps } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  useEffect(() => {
    if (!props.value && firstAsDefault && options.length) {
      props.onChange(options[0]);
    }
  }, [options.length]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...restProps} />
  );
};

const EnterprisesCompaniesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesCompaniesSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesCompaniesSelect, EnterprisesCompaniesSelectField };
