import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'enterprises/crm/resident/enterprises/list', payload);

export const enterprisesCompaniesApi = { getList };
