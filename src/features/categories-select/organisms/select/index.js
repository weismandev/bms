import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const CategoriesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const { companies = [] } = props;

  useEffect(() => {
    fxGetList({ companies });
  }, [companies[0]]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const CategoriesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<CategoriesSelect />} onChange={onChange} {...props} />;
};

export { CategoriesSelect, CategoriesSelectField };
