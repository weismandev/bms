import { api } from '../../../api/api2';

const getList = ({ companies }) => {
  if (Array.isArray(companies) && companies.length > 0) {
    return api
      .v2('client/admin/marketplace/companies/related/categories', {
        companies,
      })
      .then(
        (res) => Array.isArray(res.related) && res.related.length > 0 && res.related[0]
      );
  }

  return api.v2('/client/admin/marketplace/categories/feature/index');
};

export const categoriesApi = { getList };
