import { createStore, createEvent, createEffect } from 'effector';

export const fxSendSection = createEffect();

export const $prevSectionName = createStore('');

export const visitedEvent = createEvent();
export const updateSectionName = createEvent();
