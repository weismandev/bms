import { api } from '../../../api/api2';

const sendUrl = (payload) => api.v1('post', 'app/mark-section-visit', payload);

export { sendUrl };
