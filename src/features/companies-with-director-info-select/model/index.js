import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { enterprisesCompaniesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result.enterprises)) {
      return [];
    }

    return result.enterprises;
  })
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

fxGetList.use(enterprisesCompaniesApi.getList);

function findById(id) {
  const data = $data.getState();

  return data.find((i) => String(i.id) === String(id));
}

export { fxGetList, $data, $error, $isLoading, findById };
