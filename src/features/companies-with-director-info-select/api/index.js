import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'enterprises/admin/index', payload);

export const enterprisesCompaniesApi = { getList };
