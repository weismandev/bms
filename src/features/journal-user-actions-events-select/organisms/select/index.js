import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $options, $isLoading, $error, PageGate } from '../../model/select';

const JournalUserActionsEventsSelect = (props) => {
  const options = useStore($options);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <PageGate />
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
    </>
  );
};

const JournalUserActionsEventsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<JournalUserActionsEventsSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { JournalUserActionsEventsSelect, JournalUserActionsEventsSelectField };
