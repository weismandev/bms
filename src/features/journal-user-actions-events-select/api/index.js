import { api } from '../../../api/api2';

const getEventTypes = () => {
  return api.v1('get', 'journal/filter/get-event-type-list');
};

export const journalUserActionsSelectApi = {
  getEventTypes,
};
