import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { enterprisesRentedPropertiesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.rents) {
      return [];
    }

    return result.rents;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(enterprisesRentedPropertiesApi.getList);

function findById(id) {
  const data = $data.getState();

  return data.find((i) => String(i.id) === String(id));
}

export { fxGetList, $data, $error, $isLoading, findById };
