import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesRentedPropertiesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl
      options={options}
      getOptionLabel={(opt) => (opt && opt.property ? opt.property.title : '')}
      getOptionValue={(opt) =>
        typeof opt === 'number' || typeof opt === 'string'
          ? opt
          : opt && opt.property
          ? opt.property.id
          : ''
      }
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const EnterprisesRentedPropertiesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesRentedPropertiesSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesRentedPropertiesSelect, EnterprisesRentedPropertiesSelectField };
