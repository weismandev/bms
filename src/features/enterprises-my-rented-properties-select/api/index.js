import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'rents/get-my', payload);

export const enterprisesRentedPropertiesApi = { getList };
