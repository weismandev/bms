import { FC } from 'react';
import { VerticalAlignBottom } from '@mui/icons-material';
import { ActionIconButton } from '@ui/index';

interface Props {
  onClick: () => boolean;
  disabled: boolean;
}

export const ExportButton: FC<Props> = ({ onClick, disabled }) => (
  <ActionIconButton onClick={onClick} disabled={disabled}>
    <VerticalAlignBottom />
  </ActionIconButton>
);
