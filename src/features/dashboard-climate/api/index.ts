import { api } from '@api/api2';
import {
  GetApartmentIndicatorsPayload,
  GetIndicatorsPayload,
  GetArchivePayload,
  GetWetherStationPayload,
  GetWetherStationResponse
} from '../interfaces';

const getApartmentIndicators = (payload: GetApartmentIndicatorsPayload) =>
  api.no_vers('get', 'well/get-apartment-indicators', payload);
const getIndicators = (payload: GetIndicatorsPayload) =>
  api.no_vers('get', 'well/get-indicators', payload);
const getArchive = (payload: GetArchivePayload) =>
  api.no_vers('get', 'well/get-archive', payload, {
    config: {
      responseType: 'arraybuffer',
    },
  });
const getWetherStation = (payload: GetWetherStationPayload): Promise<GetWetherStationResponse[]> =>
  api.no_vers('get', 'well/get-meteostation-indicators', payload);

const getRooms = (payload = {}, config = {}) =>
  api.no_vers(
    'get',
    'well/get-rooms',
    payload,
    config
  );

export const dashboardClimateApi = {
  getApartmentIndicators,
  getWetherStation,
  getIndicators,
  getArchive,
  getRooms,
};
