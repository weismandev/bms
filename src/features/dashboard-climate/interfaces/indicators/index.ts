export interface GetIndicatorsPayload {
  complex_id?: string;
  buildings?: string;
}

export interface GetIndicatorsResponse {
  building_id: string;
  building_title: string;
  complex_id: string;
  complex_title: string;
  rooms: Room[];
}

export interface Room {
  rooms_id: string;
  rooms_title: string;
  devices: Device[];
}

export interface Signal {
  name: string;
  list: {
    id: number;
    title: string;
  }[]
}
export interface Device {
  device_id: string;
  device_serialnumber: string;
  title: string;
  indicators: Indicator[];
  signals: Signal[];
}

export interface Indicator {
  title: string;
  value: string;
  measure: string;
  tag: string;
}

export interface Indicators {
  _id: string;
  title: string;
  rooms: Rooms[];
}

export interface Rooms {
  id: string;
  title: string;
  devices: {
    id: string;
    title: string;
    indicators: DeviceIndicator[];
  }[];
}

export interface DeviceIndicator {
  tag: string;
  title: string;
  value: string;
  color: string;
}

export interface GetArchivePayload {
  rooms_id?: string;
  tags?: string;
  from?: number;
  to?: number;
}
