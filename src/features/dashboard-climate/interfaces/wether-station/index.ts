export interface GetWetherStationPayload {
  complex_id: string;
  buildings: string;
}

export interface GetWetherStationResponse {
  measure: string;
  tag: string;
  title: string;
  value: number | string;
}
