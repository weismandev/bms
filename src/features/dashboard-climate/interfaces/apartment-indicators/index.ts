export interface GetApartmentIndicatorsResponse {
  title: string;
  measure: string;
  value: string;
  tag: string;
}

export interface ApartmentIndicators {
  title: string;
  value: string;
  color: string;
  tag: string;
}

export interface GetApartmentIndicatorsPayload {
  complex_id?: string;
  buildings?: string;
}
