export interface Value {
  id: number;
  title: string;
}

export interface Filters {
  complexes: Value[];
  buildings: Value[];
}

export interface Values {
  indicators: Value[];
  rooms: Value[];
  startDate: string;
  endDate: string;
}
