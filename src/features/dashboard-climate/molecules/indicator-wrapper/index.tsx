import { FC } from 'react';
import { Wrapper, FormSectionHeader } from '@ui/index';
import { Indicators } from '../../interfaces';
import { Room } from '../room';
import { useStyles } from './styles';

interface Props {
  indicator: Indicators;
}

export const IndicatorWrapper: FC<Props> = ({ indicator }) => {
  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <FormSectionHeader header={indicator.title} />
      <div className={classes.rooms}>
        {indicator.rooms.map((room) => (
          <Room key={room.id} room={room} />
        ))}
      </div>
    </Wrapper>
  );
};
