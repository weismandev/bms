import { FC } from 'react';
import { Typography } from '@mui/material';
import { DeviceIndicator } from '../../interfaces';
import { getIcon } from '../../libs';
import { useStyles } from './styles';

interface Props {
  data: DeviceIndicator;
}

export const RoomItem: FC<Props> = ({ data }) => {
  const classes = useStyles({ color: data.color });

  const icon = getIcon(data.tag, 24);

  return (
    <div className={classes.content}>
      <div className={classes.titleContainer}>
        <div className={classes.icon}>{icon}</div>
        <Typography classes={{ root: classes.title }}>{data.title}</Typography>
      </div>
      <Typography classes={{ root: classes.value }}>{data.value}</Typography>
    </div>
  );
};
