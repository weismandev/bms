import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  content: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  titleContainer: {
    display: 'flex',
  },
  title: {
    fontWeight: 500,
    fontSize: 14,
    color: '#3B3B50',
    margin: '5px 0px 0px 10px',
  },
  value: {
    fontWeight: 500,
    fontSize: 14,
    color: ({ color }: { color: string }) => color,
    marginTop: 5,
  },
  icon: {
    marginTop: 2,
  },
}));
