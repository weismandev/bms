import { FC, useState } from 'react';
import { Typography, Tab, Tooltip } from '@mui/material';
import { Tabs, Divider } from '@ui/index';
import { Rooms, DeviceIndicator } from '../../interfaces';
import { RoomItem } from '../room-item';
import { useStyles, styles } from './styles';

interface Props {
  room: Rooms;
}

export const Room: FC<Props> = ({ room }) => {
  const [currentTab, changeCurrentTab] = useState(room?.devices[0]?.title);
  const classes = useStyles();

  const tabs = room.devices.map((item) => ({
    value: item.title,
    label:
      item.title.length > 4
        ? `${item.title[0]}...${item.title[item.title.length - 1]}`
        : item.title,
  }));

  const formattedDevices: { [key: string]: DeviceIndicator[] } = room.devices.reduce(
    (acc, value) => ({ ...acc, ...{ [value.title]: value.indicators } }),
    {}
  );

  const onChange = (_: unknown, tab: string) => changeCurrentTab(tab);

  const indicators = formattedDevices[currentTab];

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <Tooltip title={room.title}>
          <div style={tabs.length > 1 ? styles.width : styles.fullWidth}>
            <Typography classes={{ root: classes.roomTitle }}>{room.title}</Typography>
          </div>
        </Tooltip>
        {tabs.length > 1 && (
          <Tabs
            options={tabs}
            currentTab={currentTab}
            onChange={onChange}
            tabEl={<Tab style={styles.tab} />}
            style={styles.tabs}
          />
        )}
      </div>
      <div className={classes.roomContent}>
        {Array.isArray(indicators) &&
          indicators.map((item, index) => (
            <div key={item.tag}>
              <RoomItem data={item} />
              {indicators.length - 1 !== index && <Divider />}
            </div>
          ))}
      </div>
    </div>
  );
};
