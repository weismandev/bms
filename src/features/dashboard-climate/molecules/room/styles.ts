import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    background: '#F4F7FA',
    borderRadius: 16,
    height: 358,
    padding: 10,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10,
    height: 40,
  },
  roomTitle: {
    fontWeight: 700,
    fontSize: 14,
    color: '#3B3B50',
    marginTop: 12,
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  roomContent: {
    background: '#FFFFFF',
    borderRadius: 8,
    marginTop: 5,
    padding: 15,
    height: 290,
  },
}));

export const styles = {
  tab: {
    minWidth: '33.3%',
  },
  tabs: {
    height: 38,
    width: '50%',
  },
  width: {
    width: '50%',
  },
  fullWidth: {
    width: '100%',
  },
};
