import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    background: '#F4F7FA',
    borderRadius: 16,
  },
  firstElementText: {
    fontWeight: 700,
    fontSize: 14,
    color: '#3B3B50',
    fontStyle: 'normal',
    margin: '6px 0px 0px 10px',
  },
  firstElement: {
    display: 'flex',
    padding: 20,
    height: 64,
  },
  secondElement: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
  },
  secondElementText: {
    fontWeight: 900,
    fontSize: 22,
    color: ({ color }: { color: string }) => color,
    fontStyle: 'normal',
  },
}));
