import { FC } from 'react';
import { Typography } from '@mui/material';
import { Divider } from '@ui/index';
import { useStyles } from './styles';

interface Props {
  title: string;
  value: string;
  icon: JSX.Element | null;
  color: string;
}

export const DataWrapper: FC<Props> = ({ title, value, icon, color }) => {
  const classes = useStyles({ color });

  return (
    <div className={classes.wrapper}>
      <div className={classes.firstElement}>
        {icon}
        <Typography classes={{ root: classes.firstElementText }} noWrap>
          {title}
        </Typography>
      </div>
      <Divider gap="0" />
      <div className={classes.secondElement}>
        <Typography classes={{ root: classes.secondElementText }}>{value}</Typography>
      </div>
    </div>
  );
};
