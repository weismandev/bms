import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, ErrorMessage, Loader } from '@ui/index';
import {
  PageGate,
  $isErrorDialogOpen,
  $isLoading,
  $error,
  changedErrorDialogVisibility,
  $isFilterOpen,
} from '../models';
import { Main, Filters, ExportModal } from '../organisms';

const DashboardClimatePage: FC = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isFilterOpen = useStore($isFilterOpen);

  const onClose = () => changedErrorDialogVisibility(false);

  return (
    <>
      <PageGate />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <Loader isLoading={isLoading} />

      <ExportModal />

      <FilterMainDetailLayout filter={isFilterOpen && <Filters />} main={<Main />} />
    </>
  );
};

const RestrictedDashboardClimatePage: FC = () => (
  <HaveSectionAccess>
    <DashboardClimatePage />
  </HaveSectionAccess>
);

export { RestrictedDashboardClimatePage as DashboardClimatePage };
