import { FC } from 'react';
import { useStore } from 'effector-react';
import { $indicators } from '../../models';
import { IndicatorWrapper } from '../../molecules';

export const Indicators: FC = () => {
  const indicators = useStore($indicators);

  if (indicators.length === 0) {
    return null;
  }

  return (
    <>
      {indicators.map((indicator) => (
        <IndicatorWrapper key={indicator._id} indicator={indicator} />
      ))}
    </>
  );
};
