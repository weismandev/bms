import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import classNames from 'classnames';
import { Wrapper, FormSectionHeader } from '@ui/index';
import { getIcon } from '../../libs';
import { $apartmentIndicators, $isFilterOpen } from '../../models';
import { DataWrapper } from '../../molecules';
import { useStyles } from './styles';

export const Apartments: FC = () => {
  const { t } = useTranslation();
  const apartmentIndicators = useStore($apartmentIndicators);
  const isFilterOpen = useStore($isFilterOpen);
  const classes = useStyles();

  const data =
    apartmentIndicators.length > 0
      ? apartmentIndicators.map((indicator) => {
          const icon = getIcon(indicator.tag, 32);

          return (
            <DataWrapper
              key={indicator.title}
              title={indicator.title}
              value={indicator.value}
              icon={icon}
              color={indicator.color}
            />
          );
        })
      : null;

  return (
    <Wrapper className={classes.wrapper}>
      <FormSectionHeader header={t('Apartments')} />
      <div
        className={classNames(
          classes.indicators,
          { [classes.indicatorsWidth1035]: !isFilterOpen },
          { [classes.indicatorsWidth1305]: isFilterOpen }
        )}
      >
        {data}
      </div>
    </Wrapper>
  );
};
