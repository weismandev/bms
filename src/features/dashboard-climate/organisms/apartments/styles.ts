import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    padding: '18px 24px 24px',
    marginBottom: '24px'
  },
  indicators: {
    display: 'grid',
    gridTemplateColumns: 'repeat(4, 1fr)',
    gap: '20px',
  },
  indicatorsWidth1035: {
    '@media (max-width: 1035px)': {
      gridTemplateColumns: 'repeat(2, 1fr)',
    },
  },
  indicatorsWidth1305: {
    '@media (max-width: 1305px)': {
      gridTemplateColumns: 'repeat(2, 1fr)',
    },
  },
}));
