import { FC, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';
import { Filters as FiltersInterface, Value } from '../../interfaces';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { useStyles } from './styles';

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters) as FiltersInterface;
  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            const onChangeComplex = (value: Value) => {
              setFieldValue('complexes', value);
              setFieldValue('buildings', '');
            };

            const onChangeBuilding = (value: Value) => {
              setFieldValue('buildings', value);
            };

            return (
              <Form className={classes.form}>
                <Field
                  name="complexes"
                  component={ComplexSelectField}
                  label={t('AnObject')}
                  placeholder={t('selectObject')}
                  isMulti
                  onChange={onChangeComplex}
                />
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  complex={values?.complexes ? values.complexes : []}
                  isMulti
                  onChange={onChangeBuilding}
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
