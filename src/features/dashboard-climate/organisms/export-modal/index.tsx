import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Typography } from '@mui/material';
import { Done, Close } from '@mui/icons-material';
import { RoomsSelectField } from '../../molecules/rooms-select';
import {
  CustomModal as Modal,
  CloseButton,
  InputField,
  ActionButton,
  CustomScrollbar,
} from '@ui/index';
import { Values } from '../../interfaces';
import { $isOpen, changeOpen, exportInitialData, getArchive } from '../../models';
import { IndicatorsSelectField } from '../indicators-select';
import { useStyles, styles } from './styles';
import { validationSchema } from './validation';

export const ExportModal: FC = () => {
  const { t } = useTranslation();
  const isOpen = useStore($isOpen);
  const classes = useStyles();

  const onClose = () => changeOpen(false);
  const onSubmit = (values: Values) => getArchive(values);

  const header = (
    <div className={classes.header}>
      <Typography classes={{ root: classes.headerTitle }}>{t('export')}</Typography>
      <CloseButton style={styles.closeButton} onClick={onClose} />
    </div>
  );


  const content = (
    <div className={classes.content}>
      <Formik
        initialValues={exportInitialData}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        render={() => (
          <Form className={classes.form}>
            <div className={classes.formContent}>
              <CustomScrollbar>
                <div className={classes.fields}>
                  <div className={classes.field}>
                    <Field
                      name="indicators"
                      component={IndicatorsSelectField}
                      label={t('Indicators')}
                      placeholder={t('SelectIndicators')}
                      isMulti
                      divider={false}
                      required
                    />
                  </div>
                  <div className={classes.field}>
                    <Field
                      name="rooms"
                      component={RoomsSelectField}
                      label={t('Premises')}
                      placeholder={t('SelectPremises')}
                      isMulti
                      divider={false}
                      required
                    />
                  </div>
                  <div className={classes.field}>
                    <Field
                      name="startDate"
                      component={InputField}
                      label={t('PeriodStartDate')}
                      divider={false}
                      type="date"
                      required
                    />
                  </div>
                  <div className={classes.field}>
                    <Field
                      name="endDate"
                      component={InputField}
                      label={t('PeriodEndDate')}
                      divider={false}
                      type="date"
                      required
                    />
                  </div>
                  <div className={classes.buttons}>
                    <ActionButton icon={<Done />} kind="positive" type="submit">
                      {t('export')}
                    </ActionButton>
                    <ActionButton
                      icon={<Close />}
                      classes={{ root: classes.marginLeftButton }}
                      onClick={onClose}
                    >
                      {t('Cancellation')}
                    </ActionButton>
                  </div>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </div>
  );

  return (
    <Modal
      header={header}
      content={content}
      isOpen={isOpen}
      onClose={onClose}
      minWidth={300}
      padding={0}
      disableOverflow
    />
  );
};
