import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

function startDateAfterFinishDate(this: {
  parent: {
    startDate: string;
    endDate: string;
  };
}) {
  const isDatesDefined = Boolean(this.parent.startDate) && Boolean(this.parent.endDate);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.startDate)) >= Number(new Date(this.parent.endDate))
  ) {
    return false;
  }

  return true;
}

export const validationSchema = Yup.object().shape({
  indicators: Yup.array().min(1, t('thisIsRequiredField')),
  rooms: Yup.array().min(1, t('thisIsRequiredField')),
  startDate: Yup.string()
    .required(t('thisIsRequiredField'))
    .test(
      'start-date-after-before-date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    ),
  endDate: Yup.string()
    .required(t('thisIsRequiredField'))
    .test(
      'end-date-before-start-date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    ),
});
