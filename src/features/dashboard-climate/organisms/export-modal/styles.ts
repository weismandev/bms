import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '24px 24px 0px',
  },
  headerTitle: {
    fontWeight: 700,
    fontSize: 24,
    fontStyle: 'normal',
    color: '#3B3B50',
  },
  content: {
    height: 410,
    position: 'relative',
  },
  form: {
    height: '100%',
  },
  formContent: {
    padding: '24px 0px 24px 24px',
    height: '100%',
  },
  fields: {
    paddingRight: 24,
  },
  field: {
    marginBottom: 15,
  },
  buttons: {
    marginBottom: 20,
    '@media (max-width: 1125px)': {
      display: 'flex',
      flexDirection: 'column',
    },
  },
  marginLeftButton: {
    marginLeft: 20,
    '@media (max-width: 1125px)': {
      margin: '20px 0px 0px 0px',
    },
  },
}));

export const styles = {
  closeButton: {
    marginTop: 5,
  },
};
