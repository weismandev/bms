import { FC } from 'react';
import { CustomScrollbar } from '@ui/index';
import { Apartments } from '../apartments';
import { WeatherStation } from '../weather-station';
import { CustomToolbar } from '../custom-toolbar';
import { Indicators } from '../indicators';
import { styles } from './styles';

export const Main: FC = () => (
  <>
    <CustomToolbar />
    <CustomScrollbar style={styles.scrollbar}>
      <Apartments />
      <WeatherStation />
      <Indicators />
    </CustomScrollbar>
  </>
);
