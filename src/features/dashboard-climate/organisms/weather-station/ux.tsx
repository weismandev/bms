import { FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Wrapper, FormSectionHeader } from '@ui/index';
import { $weatherStation } from '../../models';
import { DataWrapper } from '../../molecules';
import { getIcon } from '../../libs';
import { useStyles } from './styles';

export const WeatherStation: FC = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const weatherStations = useStore($weatherStation);

  return (
    <Wrapper className={classes.wrapper}>
      <FormSectionHeader header={t('WeatherStation')} />
      <div className={classes.indicators}>
        {weatherStations.map((indicator) => {
          const icon = getIcon(indicator.tag, 32);
          const value = `${indicator.value} ${indicator.measure}`;
          return (
            <DataWrapper
              key={indicator.title}
              title={indicator.title}
              value={value}
              color="rgb(3, 148, 227)"
              icon={icon}
            />
          );
        })}
      </div>
    </Wrapper>
  );
};
