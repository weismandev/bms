import { FC } from 'react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { options } from '../../models/indicators-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const IndicatorsSelect: FC<object> = (props) => (
  <SelectControl options={options} {...props} />
);

export const IndicatorsSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<IndicatorsSelect />} onChange={onChange} {...props} />;
};
