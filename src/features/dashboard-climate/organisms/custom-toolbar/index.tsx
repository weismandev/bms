import { FC } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, Greedy, FilterButton } from '@ui/index';
import { ExportButton } from '../../atoms';
import {
  $isFilterOpen,
  changedFilterVisibility,
  changeOpen,
  $isOpen,
} from '../../models';
import { useStyles } from './styles';

export const CustomToolbar: FC = () => {
  const isFilterOpen = useStore($isFilterOpen);
  const isOpen = useStore($isOpen);
  const classes = useStyles();

  const openModal = () => changeOpen(true);
  const onClick = () => changedFilterVisibility(true);

  return (
    <div className={classes.toolbar}>
      <Toolbar>
        <FilterButton disabled={isFilterOpen} onClick={onClick} />
        <Greedy />
        <ExportButton onClick={openModal} disabled={isOpen} />
      </Toolbar>
    </div>
  );
};
