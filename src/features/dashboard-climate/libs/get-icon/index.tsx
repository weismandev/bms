import Co2 from '@img/co2.svg';
import Fan from '@img/fan.svg';
import Moisture from '@img/moisture.svg';
import Temperature from '@img/temperature.svg';
import Water from '@img/water.svg';
import Pressure from '@img/pressure.svg';
import Density from '@img/density.svg';
import Window from '@img/window.svg';

export const getIcon = (tag: string, size = 24) => {
  const icons = {
    term: <Temperature width={size} height={size} />,
    hum: <Moisture width={size} height={size} />,
    'air-iaq': <Fan width={size} height={size} />,
    'int-sen': <Water width={size} height={size} />,
    co2: <Co2 width={size} height={size} />,
    pressure: <Pressure width={size} height={size} />,
    pm2_5_density: <Density width={size} height={size} />,
    pm10_density: <Density width={size} height={size} />,
    open_window: <Window width={size} height={size} style={{ padding: 4 }} />
  };

  return icons[tag];
};
