import i18n from '@shared/config/i18n';

const { t } = i18n;

export const options = [
  {
    id: 'term',
    title: t('Temperature'),
  },
  {
    id: 'hum',
    title: t('Humidity'),
  },
  {
    id: 'air-iaq',
    title: t('AirQualityIAQ'),
  },
  {
    id: 'co2',
    title: t('CO2'),
  },
];
