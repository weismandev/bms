import { createEffect, createStore } from 'effector';
import { signout } from '@features/common';
import { dashboardClimateApi } from '../../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data.on(fxGetList.done, (_, { result }) => result.rooms || []).reset(signout);
$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);

fxGetList.use(dashboardClimateApi.getRooms);

export { fxGetList, $data, $error, $isLoading };
