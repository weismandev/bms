import { createEvent, createStore } from 'effector';

export const changeOpen = createEvent<boolean>();

export const $isOpen = createStore<boolean>(false);
