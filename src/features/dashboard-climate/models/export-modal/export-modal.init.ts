import { signout } from '@features/common';
import { pageUnmounted, getArchive } from '../page/page.model';
import { $isOpen, changeOpen } from './export-modal.model';

$isOpen.on(changeOpen, (_, isOpen) => isOpen).reset([pageUnmounted, signout, getArchive]);
