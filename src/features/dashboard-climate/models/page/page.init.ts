import { merge, sample } from 'effector';
import { pending } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { nanoid } from 'nanoid';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { dashboardClimateApi } from '../../api';
import {
  Signal,
  Filters,
  GetApartmentIndicatorsPayload,
  GetIndicatorsPayload,
  GetArchivePayload,
  GetWetherStationPayload,
} from '../../interfaces';
import { $filters } from '../filters/filters.model';
import {
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changedErrorDialogVisibility,
  pageUnmounted,
  pageMounted,
  fxGetApartmentIndicators,
  $apartmentIndicators,
  $indicators,
  fxGetIndicators,
  fxGetArchive,
  getArchive,
  fxGetWeatherStation,
  $weatherStation
} from './page.model';

const { t } = i18n;

fxGetApartmentIndicators.use(dashboardClimateApi.getApartmentIndicators);
fxGetIndicators.use(dashboardClimateApi.getIndicators);
fxGetArchive.use(dashboardClimateApi.getArchive);
fxGetWeatherStation.use(dashboardClimateApi.getWetherStation);

const errorOccured = merge([
  fxGetApartmentIndicators.fail,
  fxGetIndicators.fail,
  fxGetArchive.fail,
]);

$isLoading
  .on(
    pending({
      effects: [fxGetApartmentIndicators, fxGetIndicators, fxGetArchive],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: [pageMounted, $filters],
  source: $filters,
  fn: (filters: Filters) => {
    const payload: GetApartmentIndicatorsPayload = {};

    if (filters.complexes.length > 0) {
      payload.complex_id = filters.complexes.map((complex) => complex.id).join(',');
    }

    if (filters.buildings.length > 0) {
      payload.buildings = filters.buildings.map((building) => building.id).join(',');
    }

    return payload;
  },
  target: fxGetApartmentIndicators,
});

sample({
  clock: [pageMounted, $filters],
  source: $filters,
  fn: (filters: Filters): GetWetherStationPayload => {
    const payload = {
      complex_id: '',
      buildings: ''
    };

    if (filters.complexes.length > 0) {
      payload.complex_id = filters.complexes.map((complex) => complex.id).join(',');
    }

    if (filters.buildings.length > 0) {
      payload.buildings = filters.buildings.map((building) => building.id).join(',');
    }

    return payload;
  },
  target: fxGetWeatherStation,
});

const getColor = (tag: string, value: string) => {
  const valueToNumber = Number.parseInt(value, 10);

  if (tag === 'air-iaq') {
    if (valueToNumber < 10) {
      return '#1BB169';
    }

    if (valueToNumber >= 10 && valueToNumber < 30) {
      return '#FF8A00';
    }

    return '#EB5757';
  }

  if (tag === 'int-sen') {
    if (valueToNumber === 0) {
      return '#1BB169';
    }

    if (valueToNumber > 0 && valueToNumber <= 5) {
      return '#FF8A00';
    }

    return '#EB5757';
  }

  return '#0394E3';
};

$apartmentIndicators
  .on(fxGetApartmentIndicators.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result.map((item) => ({
      title: item.title,
      value: `${item.value || 0} ${item.measure || ''}`,
      color: getColor(item.tag, item.value),
      tag: item.tag,
    }));
  })
  .reset([pageUnmounted, signout]);

$weatherStation
  .on(fxGetWeatherStation.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

sample({
  clock: [pageMounted, $filters],
  source: $filters,
  fn: (filters: Filters) => {
    const payload: GetIndicatorsPayload = {};

    if (filters.complexes.length > 0) {
      payload.complex_id = filters.complexes.map((complex) => complex.id).join(',');
    }

    if (filters.buildings.length > 0) {
      payload.buildings = filters.buildings.map((building) => building.id).join(',');
    }

    return payload;
  },
  target: fxGetIndicators,
});


const getColorIndicatorRoom = (tag: string, value: string, signals: Signal[] = []) => {
  const valueToNumber = Number.parseInt(value, 10);

  if (tag === 'air-iaq') {
    if (valueToNumber >= 0 && valueToNumber <= 50) {
      return '#1BB169';
    }

    if (valueToNumber >= 51 && valueToNumber <= 100) {
      return '#E4C000';
    }

    if (valueToNumber >= 101 && valueToNumber <= 150) {
      return '#FF8A00';
    }

    if (valueToNumber >= 151 && valueToNumber <= 200) {
      return '#EB5757';
    }

    return '#AF1B1B';
  }

  if (tag === 'co2') {
    if (valueToNumber <= 600) {
      return '#1BB169';
    }

    if (valueToNumber > 601 && valueToNumber <= 1000) {
      return '#E4C000';
    }

    if (valueToNumber >= 1001) {
      return '#EB5757';
    }
  }

  if (tag === 'open_window') {
    const signal = signals.find(({ name }) => name === 'open_window');
    if (signal) {
      const item = signal.list.find(({ title }) => title === value);
      return item?.id === 1 ? '#EB5757' : '#0394E3';
    }
    return '#0394E3';
  }

  return '#0394E3';
};

$indicators
  .on(fxGetIndicators.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result.map((item) => {
      const rooms = Array.isArray(item?.rooms)
        ? item.rooms.map((room) => {
            const devices = Array.isArray(room?.devices)
              ? room.devices.map((device) => {
                  const indicators = Array.isArray(device?.indicators)
                    ? device.indicators.map((indicator) => ({
                        tag: indicator.tag,
                        title: indicator.title,
                        value: `${indicator.value || 0} ${indicator.measure || ''}`,
                        color: getColorIndicatorRoom(indicator.tag, indicator.value, device.signals as Signal[]),
                      }))
                    : [];

                  return {
                    id: device.device_id,
                    title: device.title,
                    indicators,
                  };
                })
              : [];

            return {
              id: room.rooms_id,
              title: room.rooms_title,
              devices,
            };
          })
        : [];

      return {
        _id: nanoid(3),
        title: `${item.complex_title} ${
          item.building_title ? `(${item.building_title})` : ''
        }`,
        rooms,
      };
    });
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: getArchive,
  fn: (values) => {
    const payload: GetArchivePayload = {};

    if (values.indicators.length > 0) {
      payload.tags = values.indicators.map((item) => item.id).join(',');
    }

    if (values.rooms.length > 0) {
      payload.rooms_id = values.rooms.map((item) => item.id).join(',');
    }

    if (values.startDate.length > 0) {
      const date = new Date(values.startDate);

      date.setHours(0, 0, 0);

      payload.from = Math.floor(date.getTime() / 1000);
    }

    if (values.endDate.length > 0) {
      const date = new Date(values.endDate);

      date.setHours(0, 0, 0);

      payload.to = Math.floor(date.getTime() / 1000);
    }

    return payload;
  },
  target: fxGetArchive,
});

fxGetArchive.done.watch(({ params, result }) => {
  const startDate = params.from ? format(new Date(params.from * 1000), 'dd.MM.yyyy') : '';
  const endDate = params.to ? format(new Date(params.to * 1000), 'dd.MM.yyyy') : '';

  const fileName =
    startDate && endDate
      ? `${t('ReportWell')} ${startDate}-${endDate}.csv`
      : `${t('ReportWell')}.csv`;

  const blob = new Blob([result], {
    type: 'text/csv',
  });

  FileSaver.saveAs(URL.createObjectURL(blob), fileName);
});
