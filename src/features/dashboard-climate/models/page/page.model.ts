import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import {
  GetApartmentIndicatorsResponse,
  ApartmentIndicators,
  GetApartmentIndicatorsPayload,
  GetIndicatorsResponse,
  Indicators,
  GetIndicatorsPayload,
  GetArchivePayload,
  Values,
  GetWetherStationPayload,
  GetWetherStationResponse
} from '../../interfaces';

export const exportInitialData = {
  indicators: [],
  rooms: [],
  startDate: '',
  endDate: '',
};

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const getArchive = createEvent<Values>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $apartmentIndicators = createStore<ApartmentIndicators[]>([]);
export const $indicators = createStore<Indicators[]>([]);
export const $weatherStation = createStore<GetWetherStationResponse[]>([]);

export const fxGetApartmentIndicators = createEffect<
  GetApartmentIndicatorsPayload,
  GetApartmentIndicatorsResponse[],
  Error
>();
export const fxGetWeatherStation = createEffect<
  GetWetherStationPayload,
  GetWetherStationResponse[],
  Error
>();
export const fxGetIndicators = createEffect<
  GetIndicatorsPayload,
  GetIndicatorsResponse[],
  Error
>();
export const fxGetArchive = createEffect<GetArchivePayload, ArrayBuffer, Error>();
