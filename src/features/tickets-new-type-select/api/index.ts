import { api } from '@api/api2';

const getTypesList = (payload: any) =>
  api.v4('get', 'ticket/types/list', { archived: 0 });

export const typesApi = {
  getTypesList,
};
