export interface TypesResponse {
  types: TypesData[];
}

export interface TypesData {
  id: number;
  title: string;
  parent_id: null | number;
  children: number[] | [];
  planned_duration_minutes: null | number;
  archived: boolean;
  level: number;
  paid: null | {
    prepayment_type: string;
    price_lower: number;
    price_type: string;
    price_upper: number;
  };
}
