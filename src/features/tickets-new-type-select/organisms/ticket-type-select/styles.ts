export const styles = {
  headerGroup: {
    border: '2px dotted #C4C4C4',
    background: '#C4C4C4',
    padding: 5,
    marginTop: -4,
    color: 'black',
  },
  margin: {
    marginLeft: 10,
  },
};
