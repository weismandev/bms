// @ts-nocheck
import { useEffect, FC } from 'react';
import { components } from 'react-select';
import { Provider, useUnit } from 'effector-react/scope';
import { ArchiveOutlined } from '@mui/icons-material';
import Paid from '@img/paid.svg';
import { SelectControl, SelectField } from '@ui/index';
import { TypesData } from '../../interfaces';
import { $data, $isLoading, fxGetList, createScope } from '../../models';
import { styles } from './styles';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface GroupHeadingProps {
  id: string;
  children: string;
  selectProps: {
    addParams: TypesData[];
  };
}

interface Option {
  label: string;
  options: TypesData[];
}

const GroupHeading: FC<GroupHeadingProps> = ({
  id,
  children,
  isFilterByChildrenTypes,
  selectProps: { addParams, options },
}) => {
  const index = Number.parseInt(id.split('-')[4], 10);

  const formattedAddParams = options.map((option) =>
    addParams.find(
      (param) => Number.parseInt(param.id, 10) === Number.parseInt(option.id, 10)
    )
  );

  if (isNaN(index)) {
    return <div style={styles.headerGroup}>{children}</div>;
  }

  const getFullOption = (index: number, addParams: TypesData[]): TypesData => {
    const option = addParams[index];

    if (option.children.length === 0 && index < addParams.length - 1) {
      return getFullOption(index + 1, addParams);
    }

    return option;
  };

  const option = getFullOption(index, formattedAddParams);

  return (
    <div style={styles.headerGroup}>
      {children}
      {option.archived && <ArchiveOutlined style={styles.margin} />}
      {option.paid && <Paid style={styles.margin} />}
    </div>
  );
};

const Group = (props: object) => (
  <div style={{ marginBottom: -4 }}>
    <components.Group {...props} />
  </div>
);

const formatOptions = (options) => {
  if (!Array.isArray(options)) {
    return [];
  }

  const archiveTypes = options.filter((option) => option?.archived);
  const notArchiveTypes = options.filter((option) => !option?.archived);
  return [...notArchiveTypes, ...archiveTypes];
};

const TypeSelect: FC<{
  formattedTypes?: { [key: number]: { [key: string]: number[] } };
  isFirstSelect?: boolean;
  ticketId?: number;
  isFilter?: boolean;
  withoutArchived?: boolean;
  childrenTypes?: number[];
  topLevelValue?: TypesData[];
  name: string;
  isFilterByChildrenTypes?: boolean
}> = ({
  formattedTypes = {},
  isFirstSelect = false,
  isFilter = false,
  withoutArchived = false,
  ticketId,
  childrenTypes = [],
  topLevelValue = [],
  name,
  isFilterByChildrenTypes = false,
  ...restProps
}) => {
  const data = useUnit($data);
  const isLoading = useUnit($isLoading);
  const fnGetList = useUnit(fxGetList);

  useEffect(() => {
    const payload = withoutArchived ? { archived: 0 } : null;

    fnGetList(payload);
  }, [withoutArchived]);

  const getOptionLabel = (option: TypesData) => (
    <div>
      {option?.title}
      {option?.archived && <ArchiveOutlined style={styles.margin} />}
      {option?.paid && <Paid style={styles.margin} />}
    </div>
  );

  const filterOption = (
    option: { label: { props: { children: string[] } } },
    value: string
  ) => {
    const label = option?.label?.props?.children[0];

    if (value.length > 0 && !label) {
      return false;
    }

    if (value.length > 0 && Boolean(label)) {
      const pattern = new RegExp(value.toUpperCase().replace(/[\\]/g, ''));

      if (label.toUpperCase().search(pattern) !== -1) {
        return true;
      }

      return false;
    }

    return true;
  };

  const typeLevel = name.split('.')[1];
  let options = data.filter(({ level }) => level === Number(typeLevel));

  if (isFilterByChildrenTypes) {
    options = options.filter(({ id }) => childrenTypes.includes(id));
  }
  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      components={{ GroupHeading, Group }}
      getOptionLabel={getOptionLabel}
      addParams={topLevelValue}
      formattedTypes={formattedTypes}
      isOptionDisabled={(option) => {
        if (isFilter) {
          return false;
        }

        return option?.archived;
      }}
      filterOption={filterOption}
      {...restProps}
    />
  );
};

const TypeSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: TypesData) =>
    props.form.setFieldValue(props.field.name, value);

  /* При передачи пропса isFilter (необходим для использования в фильтрах) в название
  области видимости дополнительно добавится -filter.
   Это нужно для изоляции юнитов при одновременном использовании в фильтрах и деталях */
  const scopeName = props.isFilter ? `${props.field.name}-filter` : props.field.name;

  const scope = createScope(scopeName);

  return (
    <Provider value={scope}>
      <SelectField
        component={<TypeSelect />}
        onChange={onChange}
        {...props}
      />
    </Provider>
  );
};

export { TypeSelect, TypeSelectField };
