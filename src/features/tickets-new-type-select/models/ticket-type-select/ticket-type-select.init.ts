import { signout } from '@features/common';
import { typesApi } from '../../api';
import { fxGetList, $data, $isLoading } from './ticket-type-select.model';

fxGetList.use(typesApi.getTypesList);

$data.on(fxGetList.done, (_, { result }) => {
  if (!Array.isArray(result?.types)) {
    return [];
  }

  return result.types;
}).reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
