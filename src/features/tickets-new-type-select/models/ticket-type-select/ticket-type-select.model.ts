import { fork, Scope, createDomain } from 'effector';

import { TypesResponse, TypesData } from '../../interfaces';

export const scopes: { [key: string]: Scope } = {};

export const selectDomain = createDomain();

export const fxGetList = selectDomain.createEffect<any, TypesResponse, Error>();

export const $data = selectDomain.createStore<TypesData[] | []>([]);
export const $isLoading = selectDomain.createStore<boolean>(false);

export const createScope = (name: string) => {
  if (scopes[name]) return scopes[name];
  scopes[name] = fork(selectDomain);

  return scopes[name];
};
