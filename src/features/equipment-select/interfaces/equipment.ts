export interface EquipmentResponse {
  devices: EquipmentItemResponse[];
  meta: {
    total: number;
  };
}

export interface EquipmentItemResponse {
  serialnumber: string;
  name: string;
}

export interface Equipment {
  id: number;
  title: string;
}

export interface EquipmentPayload {
  search: string;
  offset: number;
  complex_id: number | null;
  buildings_id: number | null;
}

export interface Value {
  id: number;
}
