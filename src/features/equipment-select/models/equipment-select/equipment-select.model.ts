import { createEffect, createStore, createEvent } from 'effector';
import { EquipmentResponse, EquipmentPayload, Value } from '../../interfaces';

const fxGetList = createEffect<EquipmentPayload, EquipmentResponse, Error>();

const setLimit = createEvent<void>();
const searchChanged = createEvent<string>();
const objectChanged = createEvent<string | Value>();
const houseChanged = createEvent<string | Value>();
const closeMenu = createEvent<void>();

const $data = createStore<{ id: string; title: string }[]>([]);
const $isLoading = createStore<boolean>(false);
const $search = createStore<string>('');
const $offset = createStore<number>(0);
const $total = createStore<number>(0);
const $object = createStore<string | Value>('');
const $house = createStore<string | Value>('');

export {
  fxGetList,
  $data,
  $isLoading,
  $search,
  setLimit,
  $offset,
  searchChanged,
  closeMenu,
  $total,
  $object,
  $house,
  objectChanged,
  houseChanged,
};
