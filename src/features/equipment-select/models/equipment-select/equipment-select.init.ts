import { guard } from 'effector';
import { debounce } from 'patronum/debounce';
import { signout } from '@features/common';
import { equipmentApi } from '../../api';
import { EquipmentItemResponse } from '../../interfaces';
import {
  fxGetList,
  $data,
  $isLoading,
  $search,
  $offset,
  setLimit,
  searchChanged,
  closeMenu,
  $total,
  $object,
  $house,
  objectChanged,
  houseChanged,
} from './equipment-select.model';

fxGetList.use(equipmentApi.getEquipments);

const searchChangedDebounce = debounce({
  source: searchChanged,
  timeout: 2500,
});

guard({
  clock: [$search, $offset],
  source: {
    search: $search,
    offset: $offset,
    total: $total,
    object: $object,
    house: $house,
  },
  filter: ({ search, offset, total }) => {
    if (offset > total) {
      return false;
    }

    return search.length > 1;
  },
  target: fxGetList.prepend(
    ({
      search,
      offset,
      object,
      house,
    }: {
      search: string;
      offset: number;
      object: any;
      house: any;
    }) => ({
      search,
      offset,
      complex_id: typeof object === 'number' ? object : null,
      buildings_id: typeof house === 'number' ? house : null,
    })
  ),
});

$object.on(objectChanged, (_, result) => (result as any)?.id).reset([signout, closeMenu]);
$house.on(houseChanged, (_, result) => (result as any)?.id).reset([signout, closeMenu]);

$search.on(searchChangedDebounce, (_, result) => result).reset([signout, closeMenu]);

$offset.on(setLimit, (offset) => offset + 20).reset([signout, closeMenu]);

$total
  .on(fxGetList.done, (_, { result }) => result?.meta?.total || 0)
  .reset([signout, closeMenu]);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result?.devices)) {
      return state;
    }

    const formatedDevices = result.devices.map((item: EquipmentItemResponse) => ({
      id: item.serialnumber,
      title: item.name,
    }));

    return [...state, ...formatedDevices];
  })
  .reset([signout, closeMenu]);

$isLoading.on(fxGetList.pending, (_, result) => result).reset([signout, closeMenu]);
