import { FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Equipment } from '../../interfaces';
import {
  $data,
  $isLoading,
  setLimit,
  searchChanged,
  $search,
  closeMenu,
  objectChanged,
  houseChanged,
} from '../../models';

interface Props {
  props?: object;
  object?: string | { id: number };
  house?: string | { id: number };
  menuPlacement?: string;
}

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const EquipmentSelect: FC<Props> = ({
  object,
  house,
  menuPlacement = 'top',
  ...props
}) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const search = useStore($search);

  const options = search ? data : isLoading ? [] : data;

  return (
    <SelectControl
      onInputChange={(value) => {
        searchChanged(value);

        if (object) {
          objectChanged(object);
        }

        if (house) {
          houseChanged(house);
        }
      }}
      onMenuScrollToBottom={setLimit}
      onMenuClose={closeMenu}
      menuPlacement={menuPlacement}
      isLoading={isLoading}
      options={options}
      filterOption={() => true}
      {...props}
    />
  );
};

const EquipmentSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Equipment) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<EquipmentSelect />} onChange={onChange} {...props} />;
};

export { EquipmentSelect, EquipmentSelectField };
