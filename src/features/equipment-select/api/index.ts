import { api } from '@api/api2';
import { EquipmentPayload } from '../interfaces';

const getEquipments = (payload: EquipmentPayload) =>
  api.no_vers('get', 'bmsdevice/get-devices', payload);

export const equipmentApi = {
  getEquipments,
};
