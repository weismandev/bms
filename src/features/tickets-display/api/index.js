import { api } from '@api/api2';

export const getTypesList = () => api.v1('get', 'tck/types/list');
