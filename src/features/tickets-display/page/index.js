import makeStyles from '@mui/styles/makeStyles';
import { WidgetAccidentsCritical } from '@features/widgets/widget-accidents-critical';
import { WidgetAccidentsTickets } from '@features/widgets/widget-accidents-tickets';
import { HaveSectionAccess } from '@features/common';
import { CustomScrollbar, Wrapper } from '@ui/index';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { TicketsDisplayGate } from '../model';

const useStyles = makeStyles({
  wrap: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
    width: '100%',
  },
  containerOneColumn: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
  containerTwoColumns: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
  column: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
});

export const TicketsDisplayPage = () => {
  const classes = useStyles();

  return (
    <HaveSectionAccess>
      <TicketsDisplayGate />
      <FilterMainDetailLayout
        main={
          <CustomScrollbar trackHorizontalStyle={null}>
            <div className={classes.wrap}>
              <div className={classes.containerOneColumn}>
                <WidgetAccidentsCritical />
                <WidgetAccidentsTickets />
              </div>
            </div>
          </CustomScrollbar>
        }
      />
    </HaveSectionAccess>
  );
};
