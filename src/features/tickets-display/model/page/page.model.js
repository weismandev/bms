import { createGate } from 'effector-react';
import { createEffect, createStore } from 'effector';

export const TicketsDisplayGate = createGate();
export const unmounted = TicketsDisplayGate.close;
export const mounted = TicketsDisplayGate.open;

export const fxGetTypes = createEffect();

export const $types = createStore([]);
