import { forward } from 'effector';
import { fxGetList as fxGetComplexes } from '@features/complex-select/model';
import { fxGetList as fxGetHouses } from '@features/house-select/models';
import { signout } from '@features/common';

import { getTypesList } from '../../api';
import { mounted, fxGetTypes, $types, unmounted } from './page.model';

fxGetTypes.use(getTypesList);

// Запрашиваем вспомогающие сторы для списков в фильтрах
forward({
  from: mounted,
  to: [fxGetComplexes, fxGetHouses, fxGetTypes],
});

$types.on(fxGetTypes.done, (_, { result }) => {
  if (!Array.isArray(result?.types)) {
    return [];
  }

  return result.types;
}).reset([unmounted, signout]);
