import { createStore } from 'effector';

const $data = createStore([
  {
    id: 1,
    title: 'Просмотр',
  },
  {
    id: 2,
    title: 'Редактирование',
  },
]);

export { $data };
