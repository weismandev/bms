import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data } from '../../model';

const RightsSelect = (props) => {
  const options = useStore($data);

  return <SelectControl options={options} {...props} />;
};

const RightsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RightsSelect />} onChange={onChange} {...props} />;
};

export { RightsSelect, RightsSelectField };
