import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { getList, getListForCompany } from '../api';

const fxGetList = createEffect();
const fxGetListForCompany = createEffect();

fxGetList.use(getList);
fxGetListForCompany.use(getListForCompany);

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on([fxGetList.done, fxGetListForCompany.done], (state, { result }) => {
    if (!result.items) {
      return [];
    }

    return result.items;
  })
  .reset(signout);

$error
  .on([fxGetList.fail, fxGetListForCompany.fail], (state, { error }) => error)
  .reset(signout);

$isLoading
  .on([fxGetList.pending, fxGetListForCompany.pending], (state, result) => result)
  .reset(signout);

export { fxGetList, fxGetListForCompany, $data, $error, $isLoading };
