import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $isCompany } from '../../../common';
import { $data, fxGetList, fxGetListForCompany, $isLoading, $error } from '../../model';

const StuffMyBuildingsSelect = (props) => {
  const { firstAsDefault = false } = props;

  const isCompany = useStore($isCompany);

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (isCompany) {
      fxGetListForCompany();
    } else {
      fxGetList();
    }
  }, []);

  useEffect(() => {
    if (!props.value && firstAsDefault && options.length) {
      props.onChange(options[0]);
    }
  }, [options.length]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const StuffMyBuildingsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<StuffMyBuildingsSelect />} onChange={onChange} {...props} />
  );
};

export { StuffMyBuildingsSelect, StuffMyBuildingsSelectField };
