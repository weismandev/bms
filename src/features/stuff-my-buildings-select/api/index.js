import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'stuff-pass/crm/building/list');

const getListForCompany = () => api.v1('get', 'stuff-pass/management/building/list');

export { getList, getListForCompany };
