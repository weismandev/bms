import { api } from '../../../api/api2';

const getList = (payload) =>
  api.v1('get', 'property/get-without-owners-drop-down', {
    ...payload,
    per_page: 1000,
  });

export const propertyApi = { getList };
