import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const FreeToRentPropertySelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const { property_types = [] } = props;

  useEffect(() => {
    fxGetList({ filter: { property_types } });
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const FreeToRentPropertySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<FreeToRentPropertySelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { FreeToRentPropertySelect, FreeToRentPropertySelectField };
