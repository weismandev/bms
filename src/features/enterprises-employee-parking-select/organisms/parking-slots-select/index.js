import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui';
import { $data, $isLoading, $error, getList } from '../../models';

const ParkingSlotsSelect = (props) => {
  const { userdata_id } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const getLabel = ({ number = '---', zone = '' }) => `[${number}] ${zone}`;

  useEffect(() => {
    getList(userdata_id);
  }, []);

  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      error={error}
      getOptionLabel={getLabel}
      {...props}
    />
  );
};

const ParkingSlotsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<ParkingSlotsSelect />} onChange={onChange} {...props} />
  );
};

export { ParkingSlotsSelect, ParkingSlotsSelectField };
