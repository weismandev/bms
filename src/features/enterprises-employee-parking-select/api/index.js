import { api } from '@api/api2';

const getParkingSlots = (payload) =>
  api.v1('get', 'parking/enterprise_rent/slots-for-employee', payload);

const enterprisesEmployeeParkingSelectApi = {
  getParkingSlots,
};

export { enterprisesEmployeeParkingSelectApi };
