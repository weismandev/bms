import { sample } from 'effector';
import { signout } from '@features/common';
import { enterprisesEmployeeParkingSelectApi } from '../api';
import {
  fxGetList,
  $data,
  $error,
  $isLoading,
  getList,
} from './parking-slots-select.model';

fxGetList.use(enterprisesEmployeeParkingSelectApi.getParkingSlots);

sample({
  clock: getList,
  fn: (clockData) => ({ employee_id: clockData }),
  target: fxGetList,
});

$data
  .on(fxGetList.doneData, (_, { slots }) => {
    if (!Array.isArray(slots)) {
      return [];
    }

    return slots.map((slot) => ({
      id: slot.id,
      number: slot.number,
      zone: slot.zone.title,
    }));
  })
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
