import { createEffect, createStore, createEvent } from 'effector';

const getList = createEvent();

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

export { getList, fxGetList, $data, $error, $isLoading };
