declare type RolesSettings = {
  permissions: any;
  notifications: any;
  users: any;
};

declare interface IRolesPermissionsProps {
  structure: RolesRole['permissions'] | null;
  values: RolesPermissionsValues | null;
}

declare interface IRolesActionsProps {
  onSave: () => void;
  onCancel: () => void;
  dirty: boolean;
}
