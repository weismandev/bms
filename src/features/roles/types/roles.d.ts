declare type RolesRole = {
  id: number;
  title: string;
  is_update_locked: boolean;
  permissions: RolesPermission[];
};

declare type RolesPermission = {
  title: string;
  sections: { code: string; title: string; enabled: boolen }[];
};

declare type RolesPermissionsValues = {
  [key: string]: boolean;
};

declare type RolesEditMode = 'view' | 'edit';

declare type RolesRoles = {
  [key: number]: RolesRole;
};

declare type RolesRawRoles = RolesRole[];

declare type RolesTab = 'permissions' | 'notifications' | 'users';

/** Интерфейсы */

declare interface IRolesGetRoleRequest {
  role_id: RolesRole['id'];
}

declare interface IRolesSaveRole {
  permissions?: RolesPermissionsValues | null;
  notifications?: any;
  user?: any;
}

declare interface IRolesUpdateRoleRequest {
  id: number;
  title: string;
  permissions?: RolesPermissionsValues;
}

declare interface IRolesCreateRoleRequest {
  title: string;
}

declare interface IRolesDeleteRequest {
  role_id: RolesRole['id'];
}

declare interface IRolesCheckDeletableRequest {
  id: RolesRole['id'];
}

declare interface IRolesCheckDeletableResponse {
  employees: any[];
}
