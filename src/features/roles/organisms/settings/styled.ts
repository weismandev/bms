import styled from '@emotion/styled';
import { TabPanel as MuiTabPanel } from '@mui/lab';

export const SettingsContainer = styled.div(() => ({
  position: 'relative',
  height: '100%',
  display: 'grid',
  gridTemplateRows: 'auto 1fr',
  alignContent: 'start',
  gap: 20,
}));

export const TabPanel = styled(MuiTabPanel)(() => ({
  padding: 0,
}));
