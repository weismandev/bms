import { FC } from 'react';
import { TabContext } from '@mui/lab';
import { Loader } from '@ui/index';
import { Permissions } from '@features/roles/organisms/permissions';
import { useStore } from 'effector-react';
import {
  $currentRole,
  $currentRoleId,
  $currentTab,
  $isRoleLoading,
} from '@features/roles/model';
import { StartScreen } from '@features/roles/organisms/start-screen';
import { Header } from '@features/roles/organisms/header';
import { SettingsContainer, TabPanel } from '@features/roles/organisms/settings/styled';

export const RolesContent: FC = () => {
  const currentTab = useStore($currentTab);
  const currentRoleId = useStore($currentRoleId);
  const currentRole = useStore($currentRole);
  const isRoleLoading = useStore($isRoleLoading);

  if (!currentRoleId || !currentRole) {
    return <StartScreen />;
  }

  return (
    <SettingsContainer>
      <Loader isLoading={isRoleLoading} />
      <TabContext value={currentTab}>
        <Header />

        <TabPanel value="permissions">
          <Permissions />
        </TabPanel>
      </TabContext>
    </SettingsContainer>
  );
};
