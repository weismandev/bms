import { InputField, Modal } from '@ui/molecules';
import { FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import {
  $creation,
  $isCreationModalOpened,
  closeCreationModal,
} from '@features/roles/model/creation/creation.model';
import { Field, Formik } from 'formik';
import {
  CreationActions,
  CreationContainer,
} from '@features/roles/organisms/creation/styled';
import { createRole } from '@features/roles/model';
import { ActionButton } from '@ui/atoms';

export const Creation: FC = () => {
  const { t } = useTranslation();
  const creation = useStore($creation);
  const isCreationModalOpened = useStore($isCreationModalOpened);

  const handleCancelClick = () => {
    closeCreationModal();
  };

  return (
    <Modal
      isOpen={isCreationModalOpened}
      header={t('CreatingARole')}
      content={
        <Formik
          initialValues={creation}
          enableReinitialize
          onSubmit={(values) => {
            createRole(values);
          }}
          render={({ handleSubmit }) => (
            <CreationContainer>
              <Field
                component={InputField}
                name="title"
                label={t('RoleName')}
                placeholder=""
                divider={false}
              />

              <CreationActions>
                <ActionButton onClick={handleSubmit}>{t('create')}</ActionButton>
                <ActionButton kind="negative" onClick={handleCancelClick}>
                  {t('Cancellation')}
                </ActionButton>
              </CreationActions>
            </CreationContainer>
          )}
        />
      }
    />
  );
};
