import styled from '@emotion/styled';

export const CreationContainer = styled.div(() => ({
  display: 'grid',
  gap: 15,
  marginTop: 24,
}));

export const CreationActions = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  alignItems: 'center',
  justifySelf: 'end',
  gap: 15,
}));
