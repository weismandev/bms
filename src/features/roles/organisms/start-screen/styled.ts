import styled from '@emotion/styled';

export const StartscreenContainer = styled.div(() => ({
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  width: '100%',
  height: '100%',
}));

export const StartscreenTitle = styled.span(() => ({
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  padding: '12px 16px',
  background: '#ececec',
  borderRadius: 24,
  fontSize: 16,
  fontWeght: 'bold',
  color: 'rgba(112,117,121, 0.7)',
  textAlign: 'center',
}));
