import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import {
  StartscreenContainer,
  StartscreenTitle,
} from '@features/roles/organisms/start-screen/styled';

export const StartScreen: FC = () => {
  const { t } = useTranslation();
  return (
    <StartscreenContainer>
      <StartscreenTitle>
        {t('SelectRoleFromTheListToDisplayDetailedInformation')}
      </StartscreenTitle>
    </StartscreenContainer>
  );
};
