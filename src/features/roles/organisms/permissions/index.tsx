import { FC, SyntheticEvent, useMemo, useState } from 'react';
import { useStore } from 'effector-react';
import { Field, Formik } from 'formik';
import {
  $currentRole,
  $isViewMode,
  $settings,
  savePermissions,
} from '@features/roles/model';
import { Actions } from '@features/roles/organisms/actions';
import { CustomScrollbar, InputField, SwitchField } from '@ui/index';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  PermissionsBlock,
  PermissionsBlockTitle,
  PermissionsContainer,
  PermissionsContent,
  PermissionsFields,
  PermissionsFieldView,
  PermissionsFieldViewTitle,
  PermissionsSectionTitle,
} from './styled';

export const Permissions: FC = () => {
  const isViewMode = useStore($isViewMode);
  const currentRole = useStore($currentRole);
  const permissions = currentRole?.permissions;
  const settings = useStore($settings);
  const [expanded, setExpanded] = useState<string | false>('panel1');

  const handleChange =
    (panel: string) => (event: SyntheticEvent, newExpanded: boolean) => {
      // setExpanded(newExpanded ? panel : false);
    };

  const renderPermissions = () => {
    if (!permissions) return null;

    return permissions.map((item) => {
      if (!item.sections.length) return null;
      return (
        <PermissionsBlock key={item.title}>
          <PermissionsBlockTitle>{item.title}</PermissionsBlockTitle>

          {item.sections.map((section) => (
            <Accordion
              key={section.code}
              expanded={expanded === section.title}
              onChange={() => {
                handleChange(section.title);
              }}
            >
              <AccordionSummary>
                <Field
                  component={SwitchField}
                  name={`permissions.${section.code}`}
                  label=""
                  placeholder=""
                  divider={false}
                  disabled={isViewMode}
                />
                <PermissionsSectionTitle>{section.title}</PermissionsSectionTitle>
              </AccordionSummary>

              <AccordionDetails>В разработке</AccordionDetails>
            </Accordion>
          ))}
        </PermissionsBlock>
      );
    });
  };
  const renderedPermissions = useMemo(
    () => renderPermissions(),
    [permissions, isViewMode]
  );

  return (
    <PermissionsContainer>
      <PermissionsContent>
        <Formik
          initialValues={{ title: currentRole.title, permissions: settings.permissions }}
          enableReinitialize
          onSubmit={(values) => {
            savePermissions(values);
          }}
          render={({ handleSubmit, handleReset, dirty }) => (
            <div>
              <Actions onSave={handleSubmit} onCancel={handleReset} dirty={dirty} />

              <CustomScrollbar autoHide style={{ height: '100%' }}>
                {!isViewMode ? (
                  <PermissionsFields>
                    <PermissionsFieldView>
                      <PermissionsFieldViewTitle>Название роли</PermissionsFieldViewTitle>
                      <Field
                        component={InputField}
                        name="title"
                        label=""
                        placeholder=""
                        divider={false}
                      />
                    </PermissionsFieldView>
                  </PermissionsFields>
                ) : (
                  <div></div>
                )}

                {renderedPermissions}
              </CustomScrollbar>
            </div>
          )}
        />
      </PermissionsContent>
    </PermissionsContainer>
  );
};
