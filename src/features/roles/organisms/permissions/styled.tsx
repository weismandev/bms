import styled from '@emotion/styled';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';

export const PermissionsContainer = styled.div(() => ({
  height: '100%',
}));

export const PermissionsContent = styled.div(() => ({
  height: '100%',
  display: 'grid',
  gap: 24,
}));

export const PermissionsFields = styled.div(() => ({
  display: 'grid',
  gap: 10,
  margin: '20px 0',
}));

export const PermissionsFieldView = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto 400px',
  alignItems: 'center',
  justifyContent: 'start',
  gap: 10,
}));

export const PermissionsFieldViewTitle = styled.span(() => ({
  fontSize: 14,
  fontWeight: 'bold',
}));

export const PermissionsBlock = styled.div(() => ({
  display: 'grid',
  marginBottom: 24,
}));

export const PermissionsBlockTitle = styled.div(() => ({
  padding: '12px 16px',
  border: '1px solid #E7E7EC',
  borderBottom: 'none',
  borderRadius: '8px 8px 0 0',
  fontWeight: 900,
  fontSize: 18,
}));

export const PermissionsSectionTitle = styled.div(() => ({
  fontSize: 14,
  fontWeight: 700,
}));

export const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(() => ({
  border: `1px solid rgba(0, 0, 0, 0.12)`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

export const AccordionSummary = styled((props: AccordionSummaryProps) => (
  <MuiAccordionSummary
    // expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    expandIcon={null}
    {...props}
  />
))(() => ({
  backgroundColor: '#F4F7FA',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    alignItems: 'center',
    margin: '6px 0',
    cursor: 'default',
  },
}));

export const AccordionDetails = styled(MuiAccordionDetails)(() => ({
  padding: 15,
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));
