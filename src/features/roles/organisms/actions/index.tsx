import { FC } from 'react';
import {
  $currentRole,
  $isViewMode,
  changeDeleteDialogVisibility,
  changeMode,
} from '@features/roles/model';
import { useStore } from 'effector-react';
import { CancelButton, DeleteButton, EditButton, SaveButton } from '@ui/index';
import { ActionsContainer } from '@features/roles/organisms/actions/styled';

export const Actions: FC<IRolesActionsProps> = ({ onSave, onCancel, dirty }) => {
  const isViewMode = useStore($isViewMode);
  const currentRole = useStore($currentRole);

  if (!currentRole || currentRole.is_update_locked) return null;

  const handleEditClick = () => {
    changeMode('edit');
  };

  const handleDeleteClick = () => {
    changeDeleteDialogVisibility(true);
  };

  const handleSaveClick = () => {
    onSave();
    changeMode('view');
  };

  const handleCancelClick = () => {
    onCancel();
    changeMode('view');
  };

  if (isViewMode) {
    return (
      <ActionsContainer>
        <EditButton onClick={handleEditClick} />
        <DeleteButton onClick={handleDeleteClick} />
      </ActionsContainer>
    );
  }

  return (
    <ActionsContainer>
      <SaveButton onClick={handleSaveClick} disabled={!dirty} />
      <CancelButton onClick={handleCancelClick} />
    </ActionsContainer>
  );
};
