import styled from '@emotion/styled';

export const ActionsContainer = styled.div(() => ({
  position: 'absolute',
  top: 7,
  right: 0,
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  alignItems: 'center',
  justifyItems: 'end',
  gap: 10,
}));
