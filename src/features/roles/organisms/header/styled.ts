import styled from '@emotion/styled';

export const HeaderContainer = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto 1fr 100px',
  alignItems: 'center',
  justifyItems: 'center',
  gap: 15,
}));

export const HeaderTitle = styled.span(() => ({
  fontSize: 24,
  fontWeight: 700,
}));
