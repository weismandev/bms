import { FC, SyntheticEvent } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { TabList } from '@mui/lab';
import Tab from '@mui/material/Tab';
import { $currentRole, changeCurrentTab } from '@features/roles/model';
import { HeaderContainer, HeaderTitle } from '@features/roles/organisms/header/styled';

export const Header: FC = () => {
  const { t } = useTranslation();
  const currentRole = useStore($currentRole);

  const handleTabChange = (event: SyntheticEvent, newValue: RolesTab) => {
    changeCurrentTab(newValue);
  };

  return (
    <HeaderContainer>
      <HeaderTitle>{currentRole?.title}</HeaderTitle>

      <TabList onChange={handleTabChange}>
        <Tab label={t('SettingUpRights')} value="permissions" />
      </TabList>
    </HeaderContainer>
  );
};
