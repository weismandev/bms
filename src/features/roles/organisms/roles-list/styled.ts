import styled from '@emotion/styled';
import {
  ListItem as MuiListItem,
  ListItemButton as MuiListItemButton,
} from '@mui/material';

export const RolesContainer = styled.div(() => ({
  position: 'relative',
  height: '100%',
}));

export const RolesToolbar = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'space-between',
  alignContent: 'center',
  padding: 24,
  paddingBottom: 20,
}));

export const RolesTitle = styled.span(() => ({
  fontSize: 24,
  fontWeight: 500,
}));

export const ListItem = styled(MuiListItem)(() => ({
  borderRadius: 0,
  boxShadow: 'none',
  padding: 0,

  '& .Mui-selected': {
    backgroundColor: '#E1EBFF !important',
    color: '#0394E3',
  },

  '& .MuiListItem-divider': {
    borderColor: '#E7E7EC',
  },
}));

export const ListItemButton = styled(MuiListItemButton)(() => ({
  paddingLeft: 24,
}));

export const ListItemTitle = styled.span(() => ({
  fontSize: 18,
  fontWeight: 700,
}));
