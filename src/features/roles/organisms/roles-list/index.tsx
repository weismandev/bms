import { FC } from 'react';
import { useGate, useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import {
  $currentRoleId,
  $isRolesLoading,
  $roles,
  openCreationModal,
  openRole,
  RolesGate,
} from '@features/roles/model';
import { AddButton, ContentLoader, CustomScrollbar, FeedList, Loader } from '@ui/index';
import {
  ListItem,
  ListItemButton,
  ListItemTitle,
  RolesContainer,
  RolesTitle,
  RolesToolbar,
} from '@features/roles/organisms/roles-list/styled';

const { t } = i18n;

export const RolesList: FC = () => {
  useGate(RolesGate);

  const roles = useStore($roles);
  const currentRoleId = useStore($currentRoleId);
  const isRolesLoading = useStore($isRolesLoading);

  if (isRolesLoading && !roles) return <ContentLoader isLoading />;

  if (!roles) return null;

  const handleCreationClick = () => {
    openCreationModal();
  };

  const renderRoles = () =>
    Object.values(roles).map((role) => (
      <ListItem key={role.id} divider onClick={() => openRole(role.id)}>
        <ListItemButton selected={currentRoleId === role.id}>
          <ListItemTitle>{role.title}</ListItemTitle>
        </ListItemButton>
      </ListItem>
    ));

  return (
    <RolesContainer>
      <Loader isLoading={isRolesLoading} />
      <RolesToolbar>
        <RolesTitle>{t('Roles')}</RolesTitle>
        <AddButton onClick={handleCreationClick} />
      </RolesToolbar>
      <FeedList style={{ height: 'calc(100% - 82px)', paddingBottom: 24 }}>
        <CustomScrollbar autoHide>{renderRoles()}</CustomScrollbar>
      </FeedList>
    </RolesContainer>
  );
};
