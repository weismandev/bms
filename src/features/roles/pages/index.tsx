import { FC } from 'react';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { HaveSectionAccess } from '@features/common';
import {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  changeDeleteDialogVisibility,
  changedErrorDialogVisibility,
  deleteRole,
} from '@features/roles/model';
import { DeleteConfirmDialog, ErrorMessage, Wrapper } from '@ui/index';
import { RolesList } from '../organisms/roles-list';
import { RolesContent } from '../organisms/settings';
import { useStyles } from './styles';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { Creation } from '@features/roles/organisms/creation';

const { t } = i18n;

const Page: FC = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const classes = useStyles();

  return (
    <div className={classes.rolesPage}>
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        header={t('RemovingARole')}
        content={t('DeleteRole')}
        confirm={() => {
          deleteRole();
          changeDeleteDialogVisibility(false);
        }}
        close={() => {
          changeDeleteDialogVisibility(false);
        }}
      />

      <ColoredTextIconNotification />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <Creation />

      <Wrapper style={{ width: '100%', overflow: 'hidden' }}>
        <RolesList />
      </Wrapper>

      <Wrapper style={{ width: '100%', height: '100%', padding: 24, paddingTop: 18 }}>
        <RolesContent />
      </Wrapper>
    </div>
  );
};

const RolesPage = () => (
  <HaveSectionAccess>
    <Page />
  </HaveSectionAccess>
);

export default RolesPage;
