import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  rolesPage: {
    display: 'grid',
    gridTemplateColumns: '380px 1fr',
    gap: 24,
    padding: '12px 24px 24px 24px',
  },
});
