const removeExtraPropsFromSections = (sections) =>
  Object.fromEntries(
    Object.entries(sections).map(([key, value]) => {
      const { access, payload } = value;

      const onlyIds = Array.isArray(payload)
        ? payload.map(({ section_id }) => section_id)
        : [];

      return [key, { access, payload: onlyIds }];
    })
  );

export { removeExtraPropsFromSections };
