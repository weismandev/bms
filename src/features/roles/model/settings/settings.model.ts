import { createEvent } from 'effector';
import { createGate } from 'effector-react';
import { $currentRole } from '@features/roles/model/roles/roles.model';

export const SettingsGate = createGate();
export const { open: settingsMounted, close: settingsUnmounted } = SettingsGate;

export const $settings = $currentRole.map((currentRole) => {
  const permissions: RolesPermissionsValues = {};
  const notifications: any = {};
  const users: any = {};

  if (currentRole?.permissions) {
    currentRole.permissions.forEach((item) => {
      item.sections.forEach((section) => {
        permissions[section.code] = section.enabled;
      });
    });
  }

  /** В будущем распаковка других настроек будет здесь */

  return { permissions, notifications, users };
});

export const savePermissions = createEvent<RolesPermissionsValues>();
