import { sample } from 'effector';
import { saveRole } from '@features/roles/model/roles/roles.model';
import { savePermissions } from './settings.model';

/** При сохранении настроек, прокидываем их в общий ивент по сохранению роли */
sample({
  clock: savePermissions,
  fn: (values) => ({
    permissions: values.permissions,
    title: values.title,
  }),
  target: saveRole,
});
