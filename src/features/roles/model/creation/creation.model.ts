import { createEffect, createEvent, createStore } from 'effector';

export const fxCreateRole = createEffect<IRolesCreateRoleRequest, RolesRole, Error>();

export const $creation = createStore<IRolesCreateRoleRequest>({
  title: '',
});
export const $isCreationModalOpened = createStore<boolean>(false);

export const openCreationModal = createEvent();
export const closeCreationModal = createEvent();
export const createRole = createEvent<IRolesCreateRoleRequest>();
