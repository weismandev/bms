import { reset } from 'patronum';
import { rolesUnmounted } from '@features/roles/model/roles/roles.model';
import { signout } from '@features/common';
import {
  $isCreationModalOpened,
  closeCreationModal,
  createRole,
  fxCreateRole,
  openCreationModal,
} from './creation.model';
import { rolesApi } from '@features/roles/api';
import { sample } from 'effector';

fxCreateRole.use(rolesApi.createRole);

/** Обработка открытия модалки */
$isCreationModalOpened
  .on(openCreationModal, () => true)
  .on(closeCreationModal, () => false);

/** Создание роли */
sample({
  clock: createRole,
  target: fxCreateRole,
});

/** Сброс */
reset({
  clock: [createRole, signout, rolesUnmounted],
  target: $isCreationModalOpened,
});
