import { merge, sample } from 'effector';
import { reset } from 'patronum';
import { CheckCircle } from '@mui/icons-material';
import { signout } from '@features/common';
import { changeNotification } from '@features/colored-text-icon-notification';
import { rolesApi } from '../../api';
import {
  $currentRole,
  $currentRoleId,
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isRolesLoading,
  $mode,
  $roles,
  changeDeleteDialogVisibility,
  changedErrorDialogVisibility,
  changeMode,
  deleteRole,
  errorOccured,
  fxCheckDeletable,
  fxDeleteRole,
  fxGetRole,
  fxGetRoles,
  fxUpdateRole,
  openRole,
  rolesMounted,
  rolesUnmounted,
  saveRole,
} from './roles.model';
import { fxCreateRole } from '../creation/creation.model';

fxGetRoles.use(rolesApi.getRoles);
fxGetRole.use(rolesApi.getRole);
fxUpdateRole.use(rolesApi.updateRole);
fxDeleteRole.use(rolesApi.deleteRole);
fxCheckDeletable.use(rolesApi.checkDeletable);

/** Запрос списка ролей */
sample({
  clock: rolesMounted,
  fn: () => null,
  target: fxGetRoles,
});

/** Обработка списка ролей */
$roles.on(fxGetRoles.doneData, (_, roles) => {
  const newState: RolesRoles = {};

  roles.map(({ id, is_update_locked, title }) => {
    newState[id] = {
      id,
      is_update_locked,
      title,
    };
  });

  return newState;
});

/** Изменение текущего id роли */
$currentRoleId.on(openRole, (_, roleId) => roleId);

/** Запрос настроек для роли если нет данных */
sample({
  source: $roles,
  clock: openRole,
  fn: (_, roleId) => ({ role_id: roleId }),
  target: fxGetRole,
});

/** Запись полученной информации по роле в общий стор */
$roles.on([fxGetRole.doneData, fxUpdateRole.doneData], (state, roleData) => {
  const newState = state || {};
  newState[roleData.id] = roleData;
  return { ...newState };
});

/** Изменение режима редактирования */
$mode
  .on(changeMode, (state, mode) => mode)
  .on(openRole, () => 'view')
  .on(merge([fxUpdateRole.done]), () => 'view')
  .reset(signout);

/** Обработка сохранения роли */
sample({
  source: $currentRole,
  clock: saveRole,
  fn: (currentRole, saveData) => {
    if (!currentRole) return null;

    const payload: IRolesUpdateRoleRequest = {
      id: currentRole.id,
      title: saveData.title || '',
    };

    if (saveData.permissions) {
      payload.permissions = saveData.permissions;
    }

    return payload;
  },
  target: fxUpdateRole,
});

/** Проверка роли на возможность удаления */
sample({
  source: $currentRole,
  clock: deleteRole,
  fn: (role) => ({ id: role?.id || 0 }),
  target: fxCheckDeletable,
});

/** Если есть возможность удаления, то удаляем */
sample({
  clock: fxCheckDeletable.done,
  filter: (data) => !data.result.employees.length,
  fn: (data) => ({ role_id: data.params.id }),
  target: fxDeleteRole,
});

/** При невозможности удаления роли показываем модалку */
sample({
  clock: fxCheckDeletable.done,
  filter: (data) => Boolean(data.result.employees.length),
  target: changeNotification.prepend(() => ({
    isOpen: true,
    text: 'Роль не может быть удалена пока есть сотрудники с этой ролью. Пожалуйста назначьте сотрудникам с удаляемой ролью другую роль',
    color: 'black',
    Icon: null,
  })),
});

/** Удаление роли из общего стора при удалении */
$roles.on(fxDeleteRole.done, (state, { params, result }) => {
  const newState = state || {};
  delete newState[params.role_id];
  return { ...newState };
});

/** Вывод сообщения об успешном удалении */
sample({
  clock: fxDeleteRole.doneData,
  target: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: 'Роль успешно удалена',
    Icon: CheckCircle,
  })),
});

/** Обработка ошибок */
$error.on(errorOccured, (_, { error }) => error).reset([signout]);
$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true);

/** Сброс текущей роли после удаления */
reset({
  clock: fxDeleteRole.done,
  target: [$currentRole, $currentRoleId],
});

/** Добавление созданной роли в общий стор */
$roles.on(fxCreateRole.doneData, (state, role) => {
  const newState = state || {};
  newState[role.id] = role;
  return { ...newState };
});

/** Открытие созданной роли */
sample({
  clock: fxCreateRole.doneData,
  fn: (role) => role.id,
  target: openRole,
});

/** Обработка подтверждения на удаление роли */
$isDeleteDialogOpen.on(changeDeleteDialogVisibility, (_, value) => value);

/** Общий сброс */
reset({
  clock: [signout, rolesUnmounted],
  target: [$roles, $currentRoleId, $currentRole, $mode, $isRolesLoading, $error],
});
