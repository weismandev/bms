import { combine, createEffect, createEvent, createStore, merge } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { fxCreateRole } from '@features/roles/model/creation/creation.model';

/** Гейт для списка ролей */
export const RolesGate = createGate();
export const { open: rolesMounted, close: rolesUnmounted } = RolesGate;

export const fxGetRoles = createEffect<null, RolesRawRoles, Error>();
export const fxGetRole = createEffect<IRolesGetRoleRequest, RolesRole, Error>();
export const fxUpdateRole = createEffect<IRolesUpdateRoleRequest, RolesRole, Error>();
export const fxDeleteRole = createEffect<IRolesDeleteRequest, any, Error>();
export const fxCheckDeletable = createEffect<
  IRolesCheckDeletableRequest,
  IRolesCheckDeletableResponse,
  Error
>();

export const changeCurrentTab = createEvent<RolesTab>();
export const changeMode = createEvent<RolesEditMode>();
export const openRole = createEvent<RolesRole['id']>();
export const openCreation = createEvent<void>();
export const saveRole = createEvent<IRolesSaveRole>();
export const deleteRole = createEvent<void>();
export const changedErrorDialogVisibility = createEvent<boolean>();
export const updateRolesList = createEvent<void>();
export const changeDeleteDialogVisibility = createEvent<boolean>();

export const $currentTab = createStore<RolesTab>('permissions');
export const $roles = createStore<RolesRoles | null>(null);
export const $currentRoleId = createStore<RolesRole['id'] | null>(null);
export const $currentRole = combine($currentRoleId, $roles, (roleId, roles) => {
  if (roleId && roles) return roles[roleId];
  return null;
});
export const $mode = createStore<RolesEditMode>('view');
export const $error = createStore<Error | null>(null);
export const errorOccured = merge([
  fxGetRoles.fail,
  fxGetRole.fail,
  fxCreateRole.fail,
  fxUpdateRole.fail,
  fxDeleteRole.fail,
  fxCheckDeletable.fail,
]);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isEditMode = $mode.map((mode) => mode === 'edit');
export const $isViewMode = $mode.map((mode) => mode === 'view');
export const $isRolesLoading = pending({
  effects: [fxGetRoles, fxCreateRole],
});
export const $isRoleLoading = pending({
  effects: [fxGetRole, fxUpdateRole, fxCreateRole, fxDeleteRole, fxCheckDeletable],
});
export const $isDeleteDialogOpen = createStore<boolean>(false);
