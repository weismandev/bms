import { api } from '@api/api2';

const getRoles = () => api.v1('post', 'role/list', {});

const getRole = (payload: IRolesGetRoleRequest) =>
  api.v4('get', '/role/form/get', payload);

/** Для получения прав в более простом виде */
const getRoleData = (payload: IRolesGetRoleRequest) =>
  api.v4('get', '/role/get', payload);

const updateRole = (payload: IRolesUpdateRoleRequest) =>
  api.v4('post', '/role/form/store', payload);

const createRole = (payload: IRolesCreateRoleRequest) =>
  api.v4('post', '/role/form/store', {
    ...payload,
  });

const deleteRole = (payload: IRolesDeleteRequest) =>
  api.v4('post', '/role/form/delete', payload);

const checkDeletable = (payload: IRolesCheckDeletableRequest) =>
  api.v1('get', 'employee/get-by-role', payload);

export const rolesApi = {
  getRoles,
  getRole,
  getRoleData,
  updateRole,
  createRole,
  deleteRole,
  checkDeletable,
};

// const getList = () => api.v1('post', 'role/list', {});

// const getEmployeeByRole = (id) => api.v1('get', 'employee/get-by-role', { id });

// const create = (payload) =>
//   api.v1('post', 'role/create', payload, {
//     headers: { 'Content-Type': 'application/json' },
//   });

// const update = (payload) =>
//   api.v1('post', 'role/update', payload, {
//     headers: { 'Content-Type': 'application/json' },
//   });

// const del = (id) => api.v1('post', 'role/delete', { id });

// export const rolesApi = {
//   getList,
//   create,
//   update,
//   delete: del,
//   getEmployeeByRole,
// };
