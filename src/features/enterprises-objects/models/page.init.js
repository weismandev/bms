import { guard, merge, sample, attach, forward } from 'effector';
import { signout } from '../../common';
import { enterprisesObjectsApi } from '../api';
import {
  addEmployeeSubmit,
  addRenterSubmit,
  $opened,
  $willRemove,
  $isDetailOpen,
  open,
} from './detail.model';
import { $currentCompany, changeCompany } from './list.model';
import {
  fxGetList,
  fxGetEmployees,
  fxBindEmployee,
  fxUnbindEmployee,
  fxAddEnterpriseRenter,
  $raw,
  $isErrorDialogOpen,
  deleteConfirmed,
  $isDeleteDialogOpen,
  $error,
  errorDialogVisibilityChanged,
  $isLoading,
  $normalized,
  changePage,
  changePerPage,
  $currentPage,
  $perPage,
} from './page.model';

fxGetList.use(enterprisesObjectsApi.getList);
fxGetEmployees.use(enterprisesObjectsApi.getEmployees);
fxBindEmployee.use(enterprisesObjectsApi.bindEmployee);
fxUnbindEmployee.use(enterprisesObjectsApi.unbindEmployee);
fxAddEnterpriseRenter.use(enterprisesObjectsApi.createEnterpriseRenter);

sample({
  source: $currentCompany,
  clock: open,
  fn: (company, id) => ({ enterprise_id: company.id, property_id: id }),
  target: fxGetEmployees,
});

sample({
  source: [
    fxGetList.pending,
    fxBindEmployee.pending,
    fxUnbindEmployee.pending,
    fxAddEnterpriseRenter.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

const gotError = merge([
  fxGetList.fail,
  fxBindEmployee.fail,
  fxUnbindEmployee.fail,
  fxAddEnterpriseRenter.fail,
]);

const closeError = guard({
  source: errorDialogVisibilityChanged,
  filter: (bool) => !bool,
});

$raw
  .on(
    fxGetList.doneData,
    (state, { properties = [], meta = { total: properties.length } }) => {
      return {
        meta,
        data: properties,
      };
    }
  )
  .reset(signout);

$isDeleteDialogOpen.on(deleteConfirmed, () => false).reset(signout);
$error.on(gotError, (state, { error }) => error).reset([signout, closeError]);
$isErrorDialogOpen.on(gotError, () => true).reset(signout);
$isLoading.reset(signout);

guard({
  clock: [$currentCompany, changePage, changePerPage],
  source: {
    company: $currentCompany,
    page: $currentPage,
    per_page: $perPage,
  },
  filter: ({ company }) => company && Boolean(company.id),
  target: attach({
    effect: fxGetList,
    mapParams: ({ company, page, per_page }) => ({
      enterprise_id: company.id,
      page,
      per_page,
    }),
  }),
});

sample({
  source: $opened,
  clock: addRenterSubmit,
  fn: (property, renter) => {
    const {
      first_name,
      last_name,
      patronymic,
      title,
      phone,
      inn,
      ogrn,
      okved,
      start_date,
      finish_date,
    } = renter;
    const { id } = property;

    return {
      enterprise: {
        first_name,
        last_name,
        patronymic,
        title,
        phone,
        inn,
        ogrn,
        okved,
      },
      property_id: id,
      start_date,
      finish_date,
    };
  },
  target: fxAddEnterpriseRenter,
});

sample({
  source: $currentCompany,
  clock: merge([fxAddEnterpriseRenter.done, fxBindEmployee.done, fxUnbindEmployee.done]),
  fn: (company) => ({ enterprise_id: company.id }),
  target: fxGetList,
});

sample({
  source: { opened: $opened, currentCompany: $currentCompany },
  clock: addEmployeeSubmit,
  fn: ({ opened, currentCompany }, { employee }) => ({
    userdata_id: employee.id,
    property_id: opened.id,
    enterprise_id: currentCompany.id,
  }),
  target: fxBindEmployee,
});

sample({
  source: {
    opened: $opened,
    currentCompany: $currentCompany,
    willRemove: $willRemove,
  },
  clock: deleteConfirmed,
  fn: ({ willRemove: { userdata_id }, opened, currentCompany }) => ({
    userdata_id,
    property_id: opened.id,
    enterprise_id: currentCompany.id,
  }),
  target: fxUnbindEmployee,
});

guard({
  source: sample({
    source: $opened,
    clock: $normalized.updates,
    fn: (opened, _) => opened.id,
  }),
  filter: $isDetailOpen,
  target: open,
});

$currentPage.on(changePage, (_, page) => page).reset([changeCompany, changePerPage]);
$perPage.on(changePerPage, (_, pageSize) => pageSize).reset(changeCompany);
