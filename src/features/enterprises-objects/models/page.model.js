import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;

const fxGetList = createEffect();
const fxGetEmployees = createEffect();
const fxBindEmployee = createEffect();
const fxUnbindEmployee = createEffect();
const fxAddEnterpriseRenter = createEffect();

const errorDialogVisibilityChanged = createEvent();
const deleteDialogVisibilityChanged = createEvent();
const deleteConfirmed = createEvent();
const changePage = createEvent();
const changePerPage = createEvent();

const $isDeleteDialogOpen = restore(deleteDialogVisibilityChanged, false);
const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);
const $isLoading = createStore(false);
const $error = createStore(null);

const $raw = createStore({ data: [], meta: { total: 0 } });
const $normalized = $raw.map(({ data }) =>
  data.reduce((acc, item) => ({ ...acc, [item.id]: item }), {})
);
const $currentPage = createStore(1);
const $perPage = createStore(10);

export {
  fxGetList,
  fxBindEmployee,
  fxGetEmployees,
  fxAddEnterpriseRenter,
  fxUnbindEmployee,
  PageGate,
  pageMounted,
  pageUnmounted,
  $raw,
  $normalized,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteConfirmed,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changePage,
  changePerPage,
  $currentPage,
  $perPage,
};
