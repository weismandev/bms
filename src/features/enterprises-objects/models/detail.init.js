import { sample, guard, forward } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { signout } from '../../common';
import {
  open,
  $isDetailOpen,
  $opened,
  $mode,
  $currentTab,
  $willRemove,
  changeTab,
  addEmployee,
  addRenter,
  removeEmployee,
  resetMode,
  $relatedEmployees,
} from './detail.model';
import { changeCompany, $currentCompany } from './list.model';
import {
  $normalized,
  deleteDialogVisibilityChanged,
  fxAddEnterpriseRenter,
  fxBindEmployee,
  fxGetEmployees,
  fxUnbindEmployee,
} from './page.model';

const { t } = i18n;

const formatOpened = ({ company, opened }) => {
  const { id, title, rents = [], owner_enterprise_id } = opened;
  const isProperty = owner_enterprise_id === company.id;

  const formatDate = (date) =>
    typeof date === 'string' && date !== ''
      ? format(new Date(date), 'dd.MM.yy')
      : t('Unknown');
  const formatParam = (param) => (param ? param : t('Unknown'));

  const formatedRents = rents.map((item) => {
    const { id, start_date, finish_date, enterprise } = item;
    const { title, inn, okved, ogrn } = enterprise;

    return {
      id: id ? id : null,
      start_date: formatDate(start_date),
      finish_date: formatDate(finish_date),
      title: formatParam(title),
      inn: formatParam(inn),
      okved: formatParam(okved),
      ogrn: formatParam(ogrn),
    };
  });

  return {
    id: id ? id : null,
    title: formatParam(title),
    isProperty,
    rents: formatedRents,
  };
};

$isDetailOpen
  .on(open, () => true)
  .on(changeCompany, () => false)
  .reset(signout);

$relatedEmployees
  .on(fxGetEmployees.doneData, (state, { users }) => {
    return Array.isArray(users) ? users : [];
  })
  .reset(signout);
$opened
  .on(
    sample({
      source: { $normalized, $currentCompany },
      clock: open,
      fn: ({ $currentCompany, $normalized }, id) => {
        return {
          company: $currentCompany,
          opened: $normalized[id],
        };
      },
    }),
    (state, opened) => formatOpened(opened)
  )
  .reset(signout);

$currentTab.on(changeTab, (state, currentTab) => currentTab).reset(signout);

$mode
  .on(addRenter, () => 'add-rent')
  .on(addEmployee, () => 'add-employee')
  .on(
    [
      $opened.updates,
      resetMode,
      fxAddEnterpriseRenter.done,
      fxBindEmployee.done,
      fxUnbindEmployee.done,
    ],
    () => 'view'
  )
  .on(
    guard($isDetailOpen.updates, {
      filter: (bool) => !bool,
    }),
    () => 'view'
  )
  .reset(signout);

$willRemove.reset([
  signout,
  fxUnbindEmployee.done,
  guard({ source: deleteDialogVisibilityChanged, filter: (e) => !e }),
]);

forward({
  from: removeEmployee,
  to: $willRemove,
});

forward({
  from: removeEmployee,
  to: deleteDialogVisibilityChanged.prepend((x) => Boolean(x)),
});
