import { signout } from '../../common';
import { $currentCompany } from './list.model';

$currentCompany.reset(signout);
