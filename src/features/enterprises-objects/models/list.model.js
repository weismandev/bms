import { createEvent, restore, combine } from 'effector';
import i18n from '@shared/config/i18n';
import { $raw } from './page.model';

const { t } = i18n;

const formatItem = (item, company) => {
  const { id, title, owner_enterprise_id, type } = item;
  return {
    id,
    title: title || t('Unknown'),
    type: type?.title || t('Unknown'),
    isProperty: company.id === owner_enterprise_id ? t('owned') : t('onLease'),
  };
};

const changeCompany = createEvent();

const $totalCount = $raw.map(({ meta }) => meta.total);
const $currentCompany = restore(changeCompany, null);

const $listData = combine($raw, $currentCompany, ({ data }, currentCompany) => {
  if (data.length && currentCompany) {
    return data.map((item) => formatItem(item, currentCompany));
  }
  return [];
});

export { $totalCount, $currentCompany, $listData, changeCompany };
