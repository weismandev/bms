import { createStore, createEvent, restore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const newEntity = {
  title: '',
  inn: '',
  ogrn: '',
  okved: '',
  start_date: '',
  finish_date: '',
};

const tabsList = [
  {
    value: 'employee',
    label: t('Employees'),
  },
  {
    value: 'subrenters',
    label: t('Subtenants'),
  },
];

const open = createEvent();
const changedDetailVisibility = createEvent();
const addRenter = createEvent();
const addRenterSubmit = createEvent();
const addEmployee = createEvent();
const addEmployeeSubmit = createEvent();
const removeEmployee = createEvent();
const changeTab = createEvent();
const resetMode = createEvent();

const $opened = createStore({});
const $relatedEmployees = createStore([]);
const $willRemove = createStore(null);
const $isDetailOpen = restore(changedDetailVisibility, false);
const $currentTab = createStore('employee');
const $mode = createStore('view');

export {
  newEntity,
  tabsList,
  changedDetailVisibility,
  addRenter,
  addRenterSubmit,
  addEmployee,
  addEmployeeSubmit,
  removeEmployee,
  resetMode,
  open,
  changeTab,
  $currentTab,
  $isDetailOpen,
  $opened,
  $mode,
  $willRemove,
  $relatedEmployees,
};
