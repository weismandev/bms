import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '../../../ui';
import { HaveSectionAccess } from '../../common';
import { $isDetailOpen } from '../models/detail.model';
import {
  PageGate,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $isDeleteDialogOpen,
} from '../models/page.model';
import { List, Detail } from '../organisms';

const { t } = i18n;

const EnterprisesObjectsPage = (props) => {
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <DeleteConfirmDialog
        content={t('AreYouSureYouWantToRemoveThisEmployeeFromThePremises')}
        header={t('RemovingAnEmployeeFromThePremises')}
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={() => deleteConfirmed()}
      />
      <PageGate />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<List />}
        detail={isDetailOpen && <Detail />}
        filter={null}
      />
    </>
  );
};

const RestrictedEnterprisesObjectsPage = (props) => (
  <HaveSectionAccess>
    <EnterprisesObjectsPage />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesObjectsPage as EnterprisesObjectsPage };
