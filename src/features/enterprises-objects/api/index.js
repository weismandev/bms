import { api } from '../../../api/api2';

const getList = (payload) =>
  api.v1('get', 'property/crm/resident/get-owned-and-rents-properties', payload);

const getEmployees = (payload) =>
  api.v1('get', 'enterprises/crm/get-property-related-users', payload);
const unbindEmployee = (payload) =>
  api.v1('post', 'enterprises/detach-user-from-property', payload);
const bindEmployee = (payload) =>
  api.v1('post', 'enterprises/crm/attach-user-to-property', payload);

const createEnterpriseRenter = (payload) =>
  api.v1('post', 'enterprises/crm/resident/create-renter', payload);

export const enterprisesObjectsApi = {
  getList,
  getEmployees,
  unbindEmployee,
  bindEmployee,
  createEnterpriseRenter,
};
