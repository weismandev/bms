import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { FilterFooter } from '../../../../ui';
import { EnterprisesEmployeeSelectField } from '../../../enterprises-employee-select';
import {
  addEmployeeSubmit,
  resetMode,
  $relatedEmployees,
} from '../../models/detail.model';
import { $currentCompany } from '../../models/list.model';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  employee: Yup.mixed().required(t('PleaseSelectFromTheList')),
});

export const EmployeeAddForm = () => {
  const { id } = useStore($currentCompany);
  const relatedEmployees = useStore($relatedEmployees);

  return (
    <Formik
      enableReinitialize
      onSubmit={addEmployeeSubmit}
      onReset={resetMode}
      validationSchema={validationSchema}
      render={() => {
        return (
          <Form style={{ padding: '0 24px' }}>
            <Field
              name="employee"
              component={EnterprisesEmployeeSelectField}
              excludeIds={relatedEmployees.map((e) => e.userdata_id)}
              label={t('Employee')}
              placeholder={t('ChooseEmployee')}
              required
              company_id={id}
            />
            <FilterFooter
              isResettable={true}
              submitLabel={t('Add')}
              cancelLabel={t('cancel')}
            />
          </Form>
        );
      }}
    />
  );
};
