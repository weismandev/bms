import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { Toolbar, FoundInfo, Greedy } from '../../../../ui';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select';
import { $totalCount, $currentCompany, changeCompany } from '../../models/list.model';

const { t } = i18n;

const ListToolbar = (props) => {
  const totalCount = useStore($totalCount);
  const currentCompany = useStore($currentCompany);

  return (
    <Toolbar style={{ padding: '24px', width: '100%' }}>
      <FoundInfo count={totalCount} style={{ margin: '0 10px 0 0' }} />
      <div style={{ width: 300 }}>
        <EnterprisesCompaniesSelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={null}
          placeholder={t('ChooseCompany')}
          firstAsDefault
        />
      </div>
      <Greedy />
    </Toolbar>
  );
};

export { ListToolbar };
