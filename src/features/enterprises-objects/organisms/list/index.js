import { useStore, useList } from 'effector-react';
import { List as MuiList, ListItem, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Wrapper, Divider, CustomScrollbar, Pagination } from '../../../../ui';
import { open, $opened } from '../../models/detail.model';
import { $listData, $totalCount } from '../../models/list.model';
import {
  $currentPage,
  changePage,
  $perPage,
  changePerPage,
} from '../../models/page.model';
import { ListToolbar } from '../list-toolbar';

const { t } = i18n;

const useStyles = makeStyles((theme) => {
  const acentItemStyles = {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  };

  return {
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      height: 70,
      padding: '10px 24px 6px 24px',
      '&:hover': acentItemStyles,
    },
    selected: acentItemStyles,
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },
    headerText: {
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: '100%',
    },
    typeText: {
      fontSize: '12px',
      lineHeight: '14px',
      color: 'rgba(12, 12, 12, .4)',
      whiteSpace: 'nowrap',
      position: 'absolute',
      left: '8px',
      bottom: 0,
    },
    propertyText: {
      fontSize: '18px',
      lineHeight: '21px',
      color: '#65657B',
      whiteSpace: 'nowrap',
    },
    cellContainer: {
      height: '100%',
      minWidth: 400,
      width: '100%',
      display: 'flex',
      direction: 'row',
      alignItems: 'center',
      position: 'relative',
    },
    noObjectText: {
      cursor: 'default',
      fontSize: '18px',
      lineHeight: '21px',
      color: 'rgba(12, 12, 12, .4)',
      whiteSpace: 'nowrap',
    },
    noObjectContainer: {
      width: '100%',
      height: 400,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  };
});

const ItemCell = (props) => {
  const { header, type, property } = props;
  const { headerText, typeText, propertyText, cellContainer } = useStyles();

  return (
    <div className={cellContainer}>
      <Typography className={headerText}>{header}</Typography>
      <Typography className={propertyText}>{property}</Typography>
      <Typography className={typeText}>{`${t('RoomType')}: ${type}`}</Typography>
    </div>
  );
};

const Item = (props) => {
  const { children, ...rest } = props;
  const { item, divider, selected } = useStyles();
  return (
    <>
      <ListItem classes={{ root: item, selected }} button component="li" {...rest}>
        {children}
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const NoObjects = () => {
  const { noObjectContainer, noObjectText } = useStyles();

  return (
    <div className={noObjectContainer}>
      <Typography className={noObjectText}>{t('NoPropertiesFound')}</Typography>
    </div>
  );
};

const List = () => {
  const opened = useStore($opened);
  const currentPage = useStore($currentPage);
  const perPage = useStore($perPage);
  const totalCount = useStore($totalCount);

  const list = useList($listData, {
    keys: [opened],
    fn: ({ id, title, type, isProperty }) => {
      const selectItem = () => open(id);

      return (
        <Item selected={String(opened.id) === String(id)} onClick={selectItem}>
          <ItemCell header={title} type={type} property={isProperty} />
        </Item>
      );
    },
  });

  return (
    <Wrapper style={{ height: '100%' }}>
      <ListToolbar />
      <CustomScrollbar style={{ height: 'calc(100% - 140px)' }} autoHide>
        <MuiList disablePadding>{list.length ? list : <NoObjects />}</MuiList>
      </CustomScrollbar>
      <Pagination
        currentPage={currentPage}
        changePage={changePage}
        rowCount={totalCount}
        perPage={perPage}
        changePerPage={changePerPage}
      />
    </Wrapper>
  );
};

export { List };
