import { useStore } from 'effector-react';
import { Typography, Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Wrapper, CloseButton, CustomScrollbar } from '../../../../ui';
import {
  addEmployee,
  changedDetailVisibility,
  $mode,
  $opened,
  $relatedEmployees,
} from '../../models/detail.model';
import { DetailsList } from '../delails-list';
import { EmployeeAddForm } from '../employee-add-form';
import { RenterAddForm } from '../renter-add-form';

const { t } = i18n;

const useStyles = makeStyles((theme) => {
  return {
    container: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    header: {
      display: 'flex',
      padding: 24,
    },
    headerText: {
      fontSize: '22px',
      lineHeight: '22px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: '100%',
    },
    addContainer: {
      padding: 24,
      margin: '0 auto',
    },
    addButton: {
      height: 40,
    },
    centerBlock: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 20,
    },
    employeesTitle: {
      fontWeight: 500,
      borderBottom: '2px solid #0288d1',
      width: '185px',
      fontSize: 16,
      paddingBottom: 5,
      textAlign: 'center',
    },
  };
});

const Detail = () => {
  const {
    container,
    header,
    headerText,
    addContainer,
    addButton,
    employeesTitle,
    centerBlock,
  } = useStyles();

  const relatedEmployees = useStore($relatedEmployees);
  const { title } = useStore($opened);
  const mode = useStore($mode);

  return (
    <Wrapper style={{ height: '90vh' }}>
      <div className={container}>
        <div className={header}>
          <Typography className={headerText}>{title}</Typography>
          <CloseButton onClick={() => changedDetailVisibility(false)} />
        </div>
        <div className={addContainer}>
          <Button
            className={addButton}
            onClick={addEmployee}
            color="primary"
            disabled={mode === 'add-employee'}
          >
            {t('AddEmployee')}
          </Button>
        </div>
        {/* {isProperty || mode !== 'view' ? null : (
          <Tabs
            tabEl={<Tab style={{ width: '50%', minHeight: 32 }} />}
            currentTab={currentTab}
            options={tabsList}
            onChange={(e, value) => changeTab(value)}
            scrollButtons="off"
          />
        )} */}
        {relatedEmployees.length > 0 ? (
          <div className={centerBlock}>
            <div className={employeesTitle}>{t('Employees')}</div>
          </div>
        ) : null}
        <CustomScrollbar autoHide>
          {mode === 'view' ? <DetailsList /> : null}
          {mode === 'add-rent' ? <RenterAddForm /> : null}
          {mode === 'add-employee' ? <EmployeeAddForm /> : null}
        </CustomScrollbar>
      </div>
    </Wrapper>
  );
};

export { Detail };
