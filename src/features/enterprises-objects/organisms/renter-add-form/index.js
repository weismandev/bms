import { FilterFooter, InputField, LogoControlField, PhoneMaskedInput } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { addRenterSubmit, resetMode } from '../../models/detail.model';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required(t('thisIsRequiredField')),
  last_name: Yup.string().required(t('thisIsRequiredField')),
  title: Yup.string().required(t('thisIsRequiredField')),
  ogrn: Yup.string()
    .required(t('PleaseEnterYourBIN'))
    .matches(/^\d{13}$/, {
      message: t('Contains13Digits'),
    }),
  inn: Yup.string()
    .required(t('PleaseEnterYourTIN'))
    .min(10, t('AtLeast10Characters'))
    .max(12, t('AtLeast12Characters'))
    .matches(/^\d{10,12}$/, {
      message: t('UseNumbersFrom0to9'),
    }),
  okved: Yup.string()
    .required(t('thisIsRequiredField'))
    .matches(/^(\d{1,3}(\.\d{1,3})+)(\s*,\s*(\d{1,3}(\.\d{1,3})+))*$/, {
      message: t('InvalidRCEAPformat'),
    }),
  phone: Yup.string().required(t('thisIsRequiredField')),
  start_date: Yup.string().required(t('thisIsRequiredField')),
  finish_date: Yup.string().required(t('thisIsRequiredField')),
});

export const RenterAddForm = () => (
  <Formik
    validationSchema={validationSchema}
    initialValues={{
      first_name: '',
      last_name: '',
      patronymic: '',
      title: '',
      phone: '',
      inn: '',
      ogrn: '',
      okved: '',
      start_date: '',
      finish_date: '',
    }}
    onSubmit={addRenterSubmit}
    onReset={resetMode}
    render={() => (
      <Form style={{ height: '100%', padding: '0 24px' }}>
        <Field component={LogoControlField} name="logo" label={t('CompanyLogo')} />
        <Field
          name="first_name"
          component={InputField}
          label={t('DirectorNname')}
          placeholder={t('EnterYourName')}
        />
        <Field
          name="last_name"
          component={InputField}
          label={t('SurnameDirector')}
          placeholder={t('EnterLastName')}
        />
        <Field
          name="patronymic"
          component={InputField}
          label={t('MiddleNameDirector')}
          placeholder={t('EnterMiddleName')}
        />
        <Field
          name="title"
          component={InputField}
          label={t('CompanyName')}
          placeholder={t('EnterCompanyName')}
        />
        <Field
          name="phone"
          render={({ field, form }) => (
            <InputField
              field={field}
              form={form}
              inputComponent={PhoneMaskedInput}
              label={t('Label.phone')}
              placeholder={t('EnterPhone')}
            />
          )}
        />
        <Field
          name="inn"
          component={InputField}
          label={t('TIN')}
          placeholder={t('EnterYourTIN')}
        />
        <Field
          name="ogrn"
          component={InputField}
          label={t('BIN')}
          placeholder={t('EnterYourBIN')}
        />
        <Field
          name="okved"
          component={InputField}
          label={t('RCEAP')}
          placeholder={t('EnterYourRCEAP')}
        />
        <Field
          name="start_date"
          component={InputField}
          type="date"
          label={t('RentalStartDate')}
          placeholder={t('EnterStartDate')}
        />
        <Field
          name="finish_date"
          component={InputField}
          type="date"
          label={t('EndDateLease')}
          placeholder={t('EnterEndDate')}
        />
        <div style={{ padding: '0 24px 24px' }}>
          <FilterFooter isResettable submitLabel={t('Add')} cancelLabel={t('cancel')} />
        </div>
      </Form>
    )}
  />
);
