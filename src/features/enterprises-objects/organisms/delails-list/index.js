import { useStore } from 'effector-react';
import { List, ListItem, Typography, Box } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Divider, CloseButton } from '../../../../ui';
import {
  $opened,
  $currentTab,
  removeEmployee,
  $relatedEmployees,
} from '../../models/detail.model';

const { t } = i18n;

const useStyles = makeStyles((theme) => {
  const acentItemStyles = {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  };

  return {
    rentItem: {
      cursor: 'default',
      display: 'grid',
      gridTemplateColumns: '1fr 152px',
      gridTemplateRows: '90px auto',
      width: '100%',
      padding: '10px 24px',
      '&:hover': acentItemStyles,
    },
    employeeItem: {
      cursor: 'default',
      display: 'flex',
      width: '100%',
      padding: '10px 24px',
      '&:hover': acentItemStyles,
    },
    selected: acentItemStyles,
    headerText: {
      gridRow: '1/2',
      gridColumn: '1/2',
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      wordBreak: 'break-word',
      width: '100%',
    },
    employeeText: {
      gridRow: '1/2',
      gridColumn: '1/2',
      fontSize: '14px',
      lineHeight: '20px',
      fontWeight: 500,
      color: '#65657B',
      overflow: 'hidden',
      width: '100%',
    },
    rentDates: {
      gridRow: '2/3',
      gridColumn: '1/3',
      fontSize: '16px',
      lineHeight: '18px',
      fontWeight: 400,
      color: 'rgba(0, 0, 0, 0.54)',
      margin: '7px auto 6px',
    },
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },
    details: {
      gridRow: '1/2',
      gridColumn: '2/3',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'end',
    },
    detailsText: {
      fontSize: '13px',
      lineHeight: '16px',
      fontWeight: 500,
      color: '#65657B',
    },
    detailsTitles: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: '13px',
      lineHeight: '18px',
      fontWeight: 300,
      color: '#65657B',
      textAlign: 'end',
      marginRight: '6px',
    },
    detailsValues: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: '13px',
      lineHeight: '18px',
      fontWeight: 600,
      color: '#65657B',
    },
    deletEmployeeBtn: {
      width: '18px',
      height: '18px',
      opacity: 0.6,
      '&:hover': {
        opacity: 1,
      },
    },
  };
});

const RentersList = (props) => {
  const { rents, ...rest } = props;
  const { rentItem, headerText, rentDates, divider, selected } = useStyles();

  return (
    <List>
      {rents.map((item) => {
        const { id, title, start_date, finish_date, ...etc } = item;
        return (
          <div key={id}>
            <ListItem classes={{ root: rentItem, selected }} component="li" {...rest}>
              <Typography className={headerText} component="h2">
                {title}
              </Typography>
              <Typography className={rentDates} component="p">
                {`${t('RentedFrom')} `}
                <strong>{start_date}</strong>
                {` ${t('on')} `}
                <strong>{finish_date}</strong>
              </Typography>
              <CompanyDetails details={etc} />
            </ListItem>
            <ListItem classes={{ root: divider, selected }} role="separator">
              <Divider gap="0" />
            </ListItem>
          </div>
        );
      })}
    </List>
  );
};

const CompanyDetails = (props) => {
  const { details, detailsTitles, detailsValues } = useStyles();
  const { inn, okved, ogrn } = props.details;

  return (
    <Box className={details}>
      <Box className={detailsTitles}>
        <span>{`${t('BIN')}:`}</span>
        <span>{`${t('TIN')}:`}</span>
        <span>{`${t('RCEAP')}:`}</span>
      </Box>
      <Box className={detailsValues}>
        <span>{ogrn}</span>
        <span>{inn}</span>
        <span>{okved}</span>
      </Box>
    </Box>
  );
};

const EmployeeList = (props) => {
  const { employee, ...rest } = props;
  const { employeeItem, employeeText, divider, selected, deletEmployeeBtn } = useStyles();
  return (
    <List>
      {employee.map((item) => {
        const { userdata_id, fullname } = item;
        return (
          <div key={userdata_id}>
            <ListItem classes={{ root: employeeItem, selected }} component="li" {...rest}>
              <Typography className={employeeText} component="h2">
                {fullname}
              </Typography>
              <CloseButton
                className={deletEmployeeBtn}
                onClick={() => removeEmployee({ userdata_id })}
              />
            </ListItem>
            <ListItem classes={{ root: divider, selected }} role="separator">
              <Divider gap="0" />
            </ListItem>
          </div>
        );
      })}
    </List>
  );
};

export const DetailsList = (props) => {
  const { rents } = useStore($opened);
  const relatedEmployees = useStore($relatedEmployees);
  const tab = useStore($currentTab);

  return tab === 'subrenters' ? (
    <RentersList rents={rents} />
  ) : (
    <EmployeeList employee={relatedEmployees} />
  );
};
