import makeStyles from '@mui/styles/makeStyles';

//     scroll: {
//       maxHeight: `calc( 89vh - 105px )`,
//     },

export const useStyles = makeStyles({
  wrapper: { width: '100%', height: '100%' },
  form: {
    padding: 24,
    paddingTop: 0,
  },
  formInner: {
    paddingRight: 12,
  },
  footerToolbar: {
    justifyContent: 'center',
    width: 'auto',
    height: 40,
    margin: 0,
  },
  detailToolbar: {
    padding: '24px 24px 24px 12px',
  },
  actionButton: {
    padding: '0 30px',
  },
});
