import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Wrapper, InputField, DetailToolbar } from '@ui/index';
import { IModeType } from '../../interfaces';
import {
  changedDetailVisibility,
  deleteDialogVisibilityChanged,
  changeMode,
  detailSubmitted,
  $mode,
  $opened,
  newEntity,
  setReaction,
} from '../../models';
import { AudioPlayer } from '../../molecules/audio-player';
import { DetailSelectEmployee } from '../../molecules/detail-select-employee';
import { DetailSelectReaction } from '../../molecules/detail-select-reaction';
import { DetailSelectStatus } from '../../molecules/detail-select-status';
import { DetailTicketCreate } from '../../molecules/detail-ticket-create';
import { useStyles } from './styles';
import { validationSchema } from './validation';

export const CallsDetail: FC = () => {
  const classes = useStyles();
  const opened = useStore($opened);
  const mode = useStore($mode);

  const { t } = useTranslation();

  const isNew = !opened.id;
  const recordUrl = opened.call_info?.record;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        validateOnChange={false}
        validateOnBlur={false}
        validationSchema={validationSchema}
        enableReinitialize
        //@ts-ignore
        onSubmit={(values) => setReaction(values)}
        render={({ handleSubmit, values, setFieldValue, resetForm }) => (
          <>
            <DetailToolbar
              className={classes.detailToolbar}
              onClose={() => changedDetailVisibility(false)}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onSave={handleSubmit}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm(newEntity);
                }
              }}
              onDelete={() => deleteDialogVisibilityChanged(true)}
              hidden={{ delete: true, send: true }}
            />
            <Form className={classes.form}>
              <div className={classes.formInner}>
                {recordUrl && <AudioPlayer url={recordUrl} />}
                <DetailSelectEmployee
                  mode={mode as IModeType}
                  setFieldValue={setFieldValue}
                />
                <DetailSelectReaction
                  mode={mode as IModeType}
                  setFieldValue={setFieldValue}
                />
                <Field
                  component={InputField}
                  mode={mode}
                  name="date"
                  label={t('Label.date')}
                  disabled
                />
                <Field
                  component={InputField}
                  mode={mode}
                  name="time"
                  label={t('Label.time')}
                  disabled
                />
                <Field
                  component={InputField}
                  mode={mode}
                  name="phone"
                  label={t('Label.phone')}
                  disabled
                />
                <Field
                  component={InputField}
                  mode={mode}
                  name="duration"
                  label={t('Duration')}
                  disabled
                />
                <DetailSelectStatus
                  mode={mode as IModeType}
                  setFieldValue={setFieldValue}
                />
                {opened.is_known_user && (
                  <Field
                    component={InputField}
                    mode={mode}
                    name="address"
                    label={t('Label.address')}
                    disabled
                  />
                )}
              </div>
              <DetailTicketCreate opened={opened} values={values} />
            </Form>
          </>
        )}
      />
    </Wrapper>
  );
};
