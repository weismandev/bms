import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  employee: Yup.object()
    .shape({
      user_id: Yup.string(),
      userdata_id: Yup.string(),
    })
    .required(),
  reaction: Yup.object()
    .shape({
      id: Yup.number(),
      title: Yup.string(),
    })
    .required(),
  status: Yup.object().shape({}),
});
