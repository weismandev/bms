import { ChangeEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FormikValues } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
} from '@ui/index';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
  defaultFilters,
} from '../../models';
import { FilterStatusSelect } from '../../molecules/filter-status';
import { useStyles } from './styles';

const isDisabled = (values: FormikValues) =>
  Object.values(values).filter(
    (value) =>
      (typeof value === 'number' && value > 0) ||
      (typeof value === 'object' && value !== null && Object.values(value).length > 0)
  ).length === 0;

export const CallsFilter = () => {
  const classes = useStyles();
  const filters = useStore($filters);

  const { t } = useTranslation();

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          className={classes.toolbar}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          validateOnChange={false}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted(defaultFilters)}
          enableReinitialize
          render={({ values, setFieldValue, handleSubmit, resetForm }) => (
            <Form className={classes.form}>
              <FilterStatusSelect values={values} setFieldValue={setFieldValue} />
              <Field
                component={InputField}
                name="start_date"
                label={t('startDate')}
                divider
                type="datetime-local"
                mode="edit"
              />
              <Field
                component={InputField}
                name="end_date"
                type="datetime-local"
                label={t('expirationDate')}
                mode="edit"
              />
              <FilterFooter
                onSubmit={handleSubmit}
                onReset={(e: ChangeEvent<HTMLInputElement>) => {
                  e.preventDefault();
                  resetForm();
                  handleSubmit(values);
                }}
                isResettable
                disabled={(values: FormikValues) => isDisabled(values)}
              />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};
