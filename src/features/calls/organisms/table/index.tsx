//@ts-nocheck
import { useMemo, useCallback, ReactNode, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import SuccessIcon from '@mui/icons-material/PhoneInTalk';
import MissedIcon from '@mui/icons-material/PhoneMissed';
import { Template } from '@devexpress/dx-react-core';
import {
  IntegratedFiltering,
  IntegratedSorting,
  SearchState,
  SortingState,
  PagingState,
  CustomPaging,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableColumnResizing,
  Toolbar,
  DragDropProvider,
  TableColumnReordering,
  PagingPanel,
} from '@devexpress/dx-react-grid-material-ui';
import {
  SortingLabel,
  TableHeaderCell,
  TableContainer,
  TableCell,
  TableRow,
  Wrapper,
} from '@ui/index';
import { IStatusFormatter, ITableRow } from '../../interfaces';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $search,
  columns,
  $tableData,
  $totalCount,
  open,
  $opened,
} from '../../models';
import { TableToolbar, ToolbarRoot } from '../../molecules/toolbar';
import { useStyles } from './styles';

const Root = ({ children, ...props }: { children?: ReactNode }) => {
  const { gridRoot } = useStyles({});
  return (
    <Grid.Root {...props} className={gridRoot}>
      {children}
    </Grid.Root>
  );
};

const Row = (props: ITableRow) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => open(id), []);

  const tableRow = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return tableRow;
};

const StatusFormatter: FC<IStatusFormatter> = ({ row, value }) => {
  const status = row?.call_info?.status.type;
  const isSuccess = status === 'success';

  const classes = useStyles({ color: isSuccess ? '#1BB169' : '#EB5757' });
  const Icon = isSuccess ? SuccessIcon : MissedIcon;

  return (
    <Chip
      avatar={
        <Avatar className={classes.chipAvatar}>
          <Icon className={classes.chipIcon} />
        </Avatar>
      }
      label={value}
      className={classes.chip}
    />
  );
};

export const CallsTable: FC = () => {
  const columnWidths = useStore($columnWidths);
  const columnsOrder = useStore($columnOrder);
  const searchValue = useStore($search);
  const sortValue = useStore($sorting);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);

  const data = useStore($tableData);
  const totalCount = useStore($totalCount);

  const { outerWrapper, innerWrapper } = useStyles({ color: '#EB5757' });

  const { t } = useTranslation();
  return (
    <Wrapper className={outerWrapper}>
      <div className={innerWrapper}>
        <Grid
          rootComponent={Root}
          rows={data}
          getRowId={(row) => row?.id}
          columns={columns}
        >
          <DataTypeProvider formatterComponent={StatusFormatter} for={['status']} />

          <SortingState sorting={sortValue} onSortingChange={sortChanged} />
          <IntegratedSorting />

          <SearchState value={searchValue} />
          <IntegratedFiltering />

          <PagingState
            currentPage={currentPage}
            pageSize={pageSize}
            onCurrentPageChange={pageNumberChanged}
            onPageSizeChange={pageSizeChanged}
          />
          <CustomPaging totalCount={totalCount} />

          <DragDropProvider />
          <Table
            messages={{ noData: t('noData') }}
            rowComponent={Row}
            cellComponent={TableCell}
            containerComponent={TableContainer}
          />
          <TableColumnResizing
            columnWidths={columnWidths}
            onColumnWidthsChange={(widths) => columnWidthsChanged(widths)}
          />
          <TableColumnReordering
            order={columnsOrder}
            onOrderChange={(order) => columnOrderChanged(order)}
          />
          <TableHeaderRow
            cellComponent={TableHeaderCell}
            sortLabelComponent={SortingLabel}
            showSortingControls
          />
          <Toolbar rootComponent={ToolbarRoot} />
          <Template name="toolbarContent">
            <TableToolbar />
          </Template>
          <PagingPanel
            messages={{
              info: ({ from, to, count }) => `${from}-${to} из ${count}`,
            }}
          />
        </Grid>
      </div>
    </Wrapper>
  );
};
