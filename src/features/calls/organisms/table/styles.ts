import { DefaultTheme } from '@mui/styles';
import makeStyles from '@mui/styles/makeStyles';

interface Props {
  color?: '#1BB169' | '#EB5757';
}

export const useStyles = makeStyles<DefaultTheme, Props>({
  outerWrapper: {
    width: '100%',
    height: '100%',
    overflow: 'hidden',
  },
  innerWrapper: {
    padding: '24px',
    width: '100%',
    height: '87vh',
  },
  gridRoot: {
    height: '100%',
  },
  chip: {
    background: (props) => props!.color,
    color: 'white',
    fontWeight: 500,
  },
  chipAvatar: {
    background: (props) => props!.color,
    marginRight: '-12px',
  },
  chipIcon: {
    color: '#fff',
  },
});
