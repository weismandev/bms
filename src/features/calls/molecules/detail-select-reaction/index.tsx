import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { SelectField } from '@ui/index';
import { IReactionType, IDetailSelectReaction } from '../../interfaces';
import { $reactions, selectReaction } from '../../models';

export const DetailSelectReaction: FC<IDetailSelectReaction> = ({
  mode,
  setFieldValue,
  ...fieldProps
}) => {
  const reactions = useStore($reactions);
  const { t } = useTranslation();

  return (
    <Field
      component={SelectField}
      name="reaction"
      label={t('Label.action')}
      placeholder={t('SelectAction')}
      options={reactions}
      getOptionValue={(option: IReactionType) => option.id}
      getOptionLabel={(option: IReactionType) => option.title || ''}
      onChange={(e: IReactionType) => {
        setFieldValue('reaction', e);
        selectReaction(e.id);
      }}
      mode={mode}
      {...fieldProps}
    />
  );
};
