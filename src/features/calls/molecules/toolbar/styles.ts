import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  root: {
    zIndex: 2,
    '& > div': {
      flex: 1,
    },
  },
  margin: {
    margin: '0 10px',
  },
  flex: {
    flex: 1,
  },
  controlLabel: {
    margin: 0,
    flexDirection: 'row',
    flex: 1,
  },
  dxToolbarRoot: {
    padding: '0',
    minHeight: '36px',
    height: '60px',
    alignItems: 'flex-start',
    borderBottom: 'none',
    flexWrap: 'nowrap',
  },
});
