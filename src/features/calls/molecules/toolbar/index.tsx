import { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Toolbar as DXToolbar } from '@devexpress/dx-react-grid-material-ui';
import { Toolbar, SelectField, FilterButton } from '@ui/index';
import {
  $isFilterOpen,
  changedFilterVisibility,
  selectConnection,
  $connections,
  $selectedConnectionId,
} from '../../models';
import { useStyles } from './styles';

const ToolbarRoot = ({ children }: { children?: ReactNode }) => {
  const classes = useStyles();
  return <DXToolbar.Root className={classes.dxToolbarRoot}>{children}</DXToolbar.Root>;
};

const TableToolbar = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const isFilterOpen = useStore($isFilterOpen);
  const connections = useStore($connections);
  const selectedConnectionId = useStore($selectedConnectionId);

  return (
    <Toolbar className={classes.root}>
      <FilterButton
        onClick={() => changedFilterVisibility(true)}
        disabled={isFilterOpen}
        className={classes.margin}
      />
      <SelectField
        className={classes.flex}
        options={connections}
        value={connections.find(({ id }) => id === selectedConnectionId)}
        onChange={({ id }: { id: number }) => selectConnection(id)}
        label={null}
        placeholder={t('ChooseProvider')}
        isClearable={false}
        fullWidth={true}
        divider={false}
      />
    </Toolbar>
  );
};

export { ToolbarRoot, TableToolbar };
