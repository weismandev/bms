import { FC } from 'react';
import { Field } from 'formik';
import i18n from '@shared/config/i18n';
import { SelectField } from '@ui/index';
import { IDetailSelectStatus, IStatus } from '../../interfaces';

const { t } = i18n;

const statusOptions: IStatus[] = [
  { type: 'success', title: t('Adopted') },
  { type: 'missed', title: t('missed') },
];

export const DetailSelectStatus: FC<IDetailSelectStatus> = ({
  mode,
  setFieldValue,
  ...fieldProps
}) => (
  <Field
    component={SelectField}
    mode={mode}
    name="status"
    label={t('Label.status')}
    options={statusOptions}
    getOptionValue={(option: IStatus) => option?.type || ''}
    getOptionLabel={(option: IStatus) => option?.title || ''}
    isClearable={false}
    onChange={(e: IStatus) => {
      setFieldValue('status', e);
    }}
    disabled
    {...fieldProps}
  />
);
