import { FC } from 'react';
import { withRouter } from 'react-router-dom';
import i18n from '@shared/config/i18n';
import { ActionButton, Toolbar } from '@ui/index';
import { ITicketRaw, ITicketCreate } from '../../interfaces';
import { useStyles } from './styles';

const { t } = i18n;

const prepareTicket = ({ opened, values }: ITicketRaw) => ({
  ticket: {
    text: `${t('ApplicationBasedCall')}:

    ${t('CallStatus')}: ${values.status?.title.toLowerCase()}
    ${t('Label.phone')}.: ${values.phone},
    ${t('CallDate')}: ${values.date},
    ${t('TimeToCall')}: ${values.time}`,
    date_start: Number(new Date()) / 1000,
    date_finish: Number(new Date()) / 1000,
    link: opened.call_info?.record || '',
  },
  apartment: opened.is_known_user
    ? opened.user?.apartment
    : {
        type: 'apartment',
        id: 0,
      },
});

const TicketCreate: FC<ITicketCreate> = ({ opened, values, history }) => {
  const classes = useStyles();

  const createTicket = () => {
    const ticket = prepareTicket({ opened, values });
    /* ??? */
    history.push('/tickets/new');
  };

  return (
    <Toolbar className={classes.footerToolbar}>
      <ActionButton
        kind="positive"
        onClick={(e) => {
          createTicket();
          e.preventDefault();
        }}
      >
        {t('createApplication')}
      </ActionButton>
    </Toolbar>
  );
};

export const DetailTicketCreate = withRouter(TicketCreate);
