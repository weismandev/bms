import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  footerToolbar: {
    justifyContent: 'center',
    width: 'auto',
    height: '40px',
    margin: '0px',
    '& span': {
      padding: '0 30px',
    },
  },
});
