import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { SelectField } from '@ui/index';
import { IUser, IDetailSelectEmployee } from '../../interfaces';
import { $users, selectUser } from '../../models';

export const DetailSelectEmployee: FC<IDetailSelectEmployee> = ({
  mode,
  setFieldValue,
  ...fieldProps
}) => {
  const users = useStore($users);
  const { t } = useTranslation();

  return (
    <Field
      component={SelectField}
      name="employee"
      label={t('Employee')}
      placeholder={t('SelectEemployee')}
      getOptionValue={(option: IUser) => {
        if ('userdata_id' in option) {
          return option.userdata_id;
        } else if ('user_id' in option) {
          return option.user_id;
        }
      }}
      getOptionLabel={(option: IUser) => {
        if ('userdata_fullname' in option) {
          return option.userdata_fullname;
        } else if ('user_fullname' in option) {
          return option.user_fullname;
        }
      }}
      options={users}
      mode={mode}
      onChange={(e: IUser) => {
        setFieldValue('employee', e);
        if ('userdata_id' in e) {
          selectUser(e!.userdata_id);
        } else if ('user_id' in e) {
          selectUser(e!.user_id);
        }
      }}
      {...fieldProps}
    />
  );
};
