import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  'audio-button': {
    width: '40px',
    height: '40px',
    border: '1px solid #757575',
    background: 'transparent',
    color: '#757575',
    borderRadius: 4,
    cursor: 'pointer',
    marginRight: 5,
    flexShrink: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '&:hover': {
      color: '#0288d1',
      borderColor: '#0288d1',
    },
    '& svg': {
      width: 14,
      height: 14,
    },
  },

  volume: {
    display: 'flex',
    '& div': {
      display: 'none',
      width: '100%',
    },
    '&:hover > div': {
      display: 'inherit',
    },
    '&:hover > .audio-button': {
      color: '#0288d1',
      borderColor: '#0288d1',
    },
  },

  'volume-range': {
    width: '100%',
  },

  timer: {
    fontSize: '.85em',
  },

  progress: {
    background: '#eee',
    height: 10,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 5,
  },

  'progress-inner': {
    background: '#0288d1',
    height: 10,
    zIndex: 2,
  },

  grow: {
    flexGrow: 1,
  },
});

export { useStyles };
