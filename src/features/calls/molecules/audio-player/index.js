import { withCustomAudio } from 'react-soundplayer/addons';
import { PlayButton, VolumeControl, Progress, Timer } from 'react-soundplayer/components';
import { Grid } from '@mui/material';
import { Divider } from '@ui/index';
import { useStyles } from './styles';

export const AudioPlayer = withCustomAudio((props) => {
  const { currentTime, duration, soundCloudAudio } = props;
  const roundedDuration = Math.round(duration);
  const classes = useStyles();

  return (
    <div>
      <Grid container wrap="nowrap" alignItems="center">
        <Grid item className={classes.grow}>
          <Progress
            className={classes.progress}
            value={(currentTime / roundedDuration) * 100 || 0}
            innerClassName={classes['progress-inner']}
            {...props}
          />
        </Grid>
        <Grid item>
          <Timer
            {...props}
            duration={roundedDuration}
            className={`${classes.timer} text-gray`}
          />
        </Grid>
      </Grid>
      <Grid container wrap="nowrap">
        <Grid item>
          <PlayButton {...props} className={classes['audio-button']} />
        </Grid>
        <Grid item className={classes.grow}>
          <VolumeControl
            className={classes.volume}
            buttonClassName={classes['audio-button']}
            rangeClassName={classes['volume-range']}
            onToggleMute={(value, e) => {
              e.preventDefault();
              if (value) {
                soundCloudAudio.setVolume(0);
              } else {
                soundCloudAudio.setVolume(1);
              }
            }}
            {...props}
          />
        </Grid>
      </Grid>
      <Divider />
    </div>
  );
});
