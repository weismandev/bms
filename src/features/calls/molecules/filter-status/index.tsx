import { FC } from 'react';
import { Field } from 'formik';
import i18n from '@shared/config/i18n';
import { SelectField } from '@ui/index';
import {
  IFilterStatusSelect,
  IStatusSelect,
  IStatusSelectOption,
} from '../../interfaces';

const { t } = i18n;

const options: IStatusSelectOption[] = [
  { type: 'success', title: t('adoptedBy') },
  { type: 'missed', title: t('Missed') },
];

const StatusSelect: FC<IStatusSelect> = ({ onChange, field, ...restProps }) => (
  <SelectField
    label={t('Label.status')}
    placeholder={t('selectStatuse')}
    options={options}
    getOptionValue={(option: IStatusSelectOption) => option?.type || ''}
    getOptionLabel={(option: IStatusSelectOption) => option?.title || ''}
    isClearable={false}
    onChange={onChange}
    {...restProps}
  />
);

export const FilterStatusSelect: FC<IFilterStatusSelect> = ({
  values,
  setFieldValue,
  ...restProps
}) => (
  <Field
    component={StatusSelect}
    mode="edit"
    name="status"
    value={values.status}
    isMulti={false}
    onChange={(e: IStatusSelectOption) => {
      setFieldValue('status', e);
    }}
    {...restProps}
  />
);
