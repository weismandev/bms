import { sample } from 'effector';
import { signout } from '@features/common';
import { callsApi } from '../api';
import { $isLoading, $error, $isErrorDialogOpen } from './page.model';
import {
  $reactions,
  $selectedReactionId,
  selectReaction,
  fxFetchReactions,
  fxSetReaction,
} from './reactions.model';

fxFetchReactions.use(callsApi.getReactions);
fxSetReaction.use(callsApi.setReaction);

$reactions.on(fxFetchReactions.doneData, (_, { items }) => items).reset([signout]);

$error
  .on(
    [fxFetchReactions.fail, fxSetReaction.fail],
    (_, { error }: { error: any }) => error
  )
  .reset([signout]);

$isLoading.on([fxFetchReactions.pending, fxSetReaction.pending], (_, result) => result);

$isErrorDialogOpen.on([fxFetchReactions.fail, fxSetReaction.fail], () => true);

$selectedReactionId.on(selectReaction, (_, id) => id).reset([signout]);
