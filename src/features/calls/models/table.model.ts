import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { hasPath } from '@tools/path';
import { ICallFormatted, ICall, IStatus, ICallDirectionType } from '../interfaces';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  {
    name: 'date',
    title: t('Label.date'),
    getCellValue: (cell: ICallFormatted) =>
      `${new Date(cell.call_info.timestamp * 1000).toLocaleDateString()}`,
  },
  {
    name: 'time',
    title: t('Label.time'),
    getCellValue: (cell: ICallFormatted) =>
      `${new Date(cell.call_info.timestamp * 1000).toLocaleTimeString()}`,
  },
  {
    name: 'phone',
    title: t('Label.phone'),
    getCellValue: (cell: ICallFormatted) => cell.call_info.client_phone,
  },
  {
    name: 'duration',
    title: t('Duration'),
    getCellValue: (cell: ICallFormatted) => {
      const mins = Math.floor(cell.call_info.duration / 60);
      const seconds = (cell.call_info.duration - mins * 60).toFixed(0);
      const result = `${mins > 0 ? mins + t('min') + '. ' : ''} ${
        Number(seconds) > 0 ? seconds + t('sec') + '.' : ''
      }`;
      return result;
    },
  },
  {
    name: 'direction',
    title: t('callType'),
    getCellValue: (cell: ICallFormatted) =>
      cell.call_info.direction === 'in' ? t('Incoming') : t('Outgoing'),
  },
  {
    name: 'status',
    title: t('Label.status'),
    getCellValue: (cell: ICallFormatted) => cell.call_info.status.title,
  },
  {
    name: 'fullname',
    title: t('Label.FullName'),
    getCellValue: (cell: ICallFormatted) => {
      if (!cell.is_known_user || !cell.user) return t('isNotDefinedit');
      return 'fullname' in cell.user
        ? cell.user.fullname
        : 'user_fullname' in cell.user
        ? cell.user.user_fullname
        : 'userdata_fullname' in cell.user
        ? cell.user.userdata_fullname
        : t('isNotDefinedit');
    },
  },
  {
    name: 'address',
    title: t('Label.address'),
    getCellValue: (cell: ICallFormatted) => {
      return cell.is_known_user && hasPath('cell.user.apartment.title', cell)
        ? cell.user?.apartment?.title
        : t('isNotDefined');
    },
  },
];

const widths = [
  { columnName: 'date', width: 150 },
  { columnName: 'time', width: 150 },
  { columnName: 'phone', width: 200 },
  { columnName: 'duration', width: 180 },
  { columnName: 'status', width: 200 },
  { columnName: 'fullname', width: 300 },
  { columnName: 'address', width: 350 },
  { columnName: 'direction', width: 200 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths, order: [] });

const formatTableData = (data: ICall[]) => {
  return data.map(({ call_info, ...rest }) => {
    const { duration, timestamp, ...restInfo } = call_info;

    return {
      call_info: {
        duration: Number(duration),
        timestamp: Number(timestamp),
        ...restInfo,
      },
      ...rest,
    };
  });
};

export const $totalCount = $raw.map(({ meta }) => meta.total);
export const $tableData = $raw.map(({ data }) => formatTableData(data));
