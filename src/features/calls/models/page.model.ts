import { createPageBag } from '@tools/factories';
import { callsApi } from '../api';

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(callsApi, {});
