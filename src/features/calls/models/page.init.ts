import { forward, sample } from 'effector';
import { $user } from '@features/common';
import {
  $tableParams,
  $tableData,
  $filters,
  open,
  $opened,
  $selectedCallId,
  fxFetchConnections,
  $selectedConnectionId,
  fxFetchReactions,
  fxSetReaction,
  $reactions,
  setReaction,
  fxFetchUsers,
  $users,
  pageMounted,
  fxGetList,
  $isLoading,
} from '.';
import { ICallFormatted, IReactionType, IUser, ICurrentUser } from '../interfaces';
import './connections.init';
import './reactions.init';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';
import './users.init';

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

forward({
  from: pageMounted,
  to: [fxFetchConnections, fxFetchReactions, fxFetchUsers],
});

sample({
  clock: [$selectedConnectionId, $tableParams, $filters, $settingsInitialized],
  source: {
    connectionId: $selectedConnectionId,
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized, connectionId }) =>
    settingsInitialized && Boolean(connectionId),
  fn: ({ connectionId, table, filters }) => ({
    page: table.page + 1,
    connection: connectionId?.toString(),
    start_date: filters.start_date ? new Date(filters.start_date).getTime() / 1000 : '',
    end_date: filters.end_date ? new Date(filters.end_date).getTime() / 1000 : '',
    type: filters.status?.type || '',
  }),
  target: fxGetList,
});

forward({
  from: open,
  to: $selectedCallId,
});

sample({
  source: {
    calls: $tableData,
    reactions: $reactions,
    users: $users,
    currentUser: $user,
  },
  clock: open,
  fn: ({ calls, reactions, users, currentUser }, id) =>
    formatOpened({
      call: calls.filter((item) => item.id === id.toString())[0],
      reactions,
      users,
      currentUser,
    }),
  target: $opened,
});

sample({
  source: {
    currentUser: $user,
    selectedConnectionId: $selectedConnectionId,
    selectedCallId: $selectedCallId,
  },
  clock: setReaction,
  fn: ({ currentUser, selectedConnectionId, selectedCallId }, payload) => ({
    operator_id:
      'userdata_id' in payload.employee
        ? payload.employee?.userdata_id
        : 'operator_id' in payload.reaction
        ? payload.reaction.operator_id
        : currentUser.user_id,
    reaction_id: payload.reaction.id,
    call_uid: selectedCallId,
    connection_id: selectedConnectionId,
  }),
  target: fxSetReaction as any,
});

function formatOpened({
  call,
  reactions,
  users,
  currentUser,
}: {
  call: ICallFormatted;
  reactions: IReactionType[];
  users: IUser[];
  currentUser: ICurrentUser;
}) {
  const reactionOperatorId = call?.reaction?.operator_id;
  const reactionId = call?.reaction?.reaction_id;
  const selectedUser = reactionOperatorId && users[reactionOperatorId];
  return {
    date: new Date(call.call_info.timestamp * 1000).toLocaleDateString() || '',
    time: new Date(call.call_info.timestamp * 1000).toLocaleTimeString() || '',
    phone:
      call.user && 'phone' in call.user
        ? call.user.phone
        : call.user && 'userdata_phone' in call.user
        ? call.user.userdata_phone
        : call.call_info && 'src' in call.call_info
        ? call.call_info.src
        : '',
    duration: call.call_info.duration || '',
    status: call.call_info.status || '',
    address: call.user?.apartment?.title || '',
    employee: selectedUser || currentUser || '',
    reaction: reactionId ? reactions[reactionId] : '',
  };
}
