import { createStore, createEvent, createEffect } from 'effector';
import { IUsersResponse, IUser } from '../interfaces';

export const $users = createStore<IUser[]>([]);
export const $selectedUserId = createStore<string | null>(null);

export const selectUser = createEvent<string>();
export const fxFetchUsers = createEffect<void, IUsersResponse, Error>();
