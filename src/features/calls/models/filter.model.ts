import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  status: '',
  start_date: '',
  end_date: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
