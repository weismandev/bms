import { createStore } from 'effector';
import { signout } from '@features/common';
import { createDetailBag } from '@tools/factories';

export const newEntity = {
  date: '',
  time: '',
  phone: '',
  duration: '',
  status: '',
  address: '',
  employee: '',
  reaction: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const $selectedCallId = createStore<number | null>(null).reset(signout);
