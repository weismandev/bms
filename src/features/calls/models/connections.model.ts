import { createStore, createEvent, createEffect } from 'effector';
import { IConnection, IConnectionsResponse } from '../interfaces';

export const $connections = createStore<Array<IConnection>>([]);
export const $selectedConnectionId = createStore<number | null>(null);

export const selectConnection = createEvent<number>();
export const fxFetchConnections = createEffect<void, IConnectionsResponse, Error>();
