import { signout } from '@features/common';
import { callsApi } from '../api';
import { $isLoading, $error, $isErrorDialogOpen } from './page.model';
import { $users, $selectedUserId, selectUser, fxFetchUsers } from './users.model';

fxFetchUsers.use(callsApi.getUsers);

$users.on(fxFetchUsers.doneData, (_, data) => data).reset(signout);

$error.on(fxFetchUsers.fail, (_, { error }: { error: any }) => error).reset(signout);
$isLoading.on(fxFetchUsers.pending, (_, result) => result);
$isErrorDialogOpen.on(fxFetchUsers.fail, () => true);

$selectedUserId.on(selectUser, (_, id) => id).reset(signout);
