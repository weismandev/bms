import { signout } from '@features/common';
import { callsApi } from '../api';
import {
  $connections,
  $selectedConnectionId,
  selectConnection,
  fxFetchConnections,
} from './connections.model';
import { $isLoading, $error, $isErrorDialogOpen } from './page.model';

fxFetchConnections.use(callsApi.getConnections);

$connections.on(fxFetchConnections.doneData, (_, { items }) => items).reset([signout]);

$error
  .on(fxFetchConnections.fail, (_, { error }: { error: any }) => error)
  .reset([signout]);
$isLoading.on(fxFetchConnections.pending, (_, result) => result);
$isErrorDialogOpen.on(fxFetchConnections.fail, () => true);

$selectedConnectionId
  .on($connections, (_, connections) => connections.length && connections[0].id)
  .on(selectConnection, (_, id) => id)
  .reset([signout]);
