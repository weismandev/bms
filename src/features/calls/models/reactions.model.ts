import { createStore, createEvent, createEffect } from 'effector';
import {
  IReaction,
  IReactionType,
  IReactionTypesResponse,
  IReactionSetResponse,
  IReactionSetPayload,
  IUser,
} from '../interfaces';

export const $reactions = createStore<IReactionType[]>([]);
export const $selectedReactionId = createStore<number | null>(null);

export const setReaction = createEvent<{
  employee: Partial<IUser>;
  reaction: IReaction;
}>();

export const selectReaction = createEvent<number>();

export const fxFetchReactions = createEffect<void, IReactionTypesResponse, Error>();

export const fxSetReaction = createEffect<
  IReactionSetPayload,
  IReactionSetResponse,
  Error
>();
