import { api } from '@api/api2';
import {
  IReactionSetPayload,
  IReactionSetResponse,
  ICallsGetPayload,
  ICallsGetResponse,
  IReactionTypesResponse,
  IConnectionsResponse,
  IUsersResponse,
} from '../interfaces';

const getList = (payload: ICallsGetPayload): ICallsGetResponse =>
  api.v1('post', 'ats-integration/search-calls', payload);

const getUsers = (): IUsersResponse => api.no_vers('get', 'helpdesk/get_performer', {});

const getConnections = (): IConnectionsResponse =>
  api.v1('get', 'ats-integration/get-available-connections');

const getReactions = (): IReactionTypesResponse =>
  api.v1('get', 'ats-integration/get-reactions');

const setReaction = (payload: IReactionSetPayload): IReactionSetResponse =>
  api.v1('post', 'ats-integration/set-reaction', payload);

export const callsApi = {
  getList,
  getUsers,
  getConnections,
  getReactions,
  setReaction,
};
