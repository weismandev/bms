import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import {
  FilterMainDetailLayout,
  Loader,
  DeleteConfirmDialog,
  ErrorMessage,
} from '@ui/index';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
} from '../models';
import { $isFilterOpen, $isDetailOpen } from '../models';
import '../models/page.init';
import { CallsDetail, CallsFilter, CallsTable } from '../organisms';

const CallsPage: FC = () => {
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isLoading = useStore($isLoading);

  return (
    <>
      <PageGate />
      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <DeleteConfirmDialog
        close={() => deleteDialogVisibilityChanged(false)}
        isOpen={isDeleteDialogOpen}
        confirm={deleteConfirmed}
      />
      <FilterMainDetailLayout
        main={<CallsTable />}
        filter={isFilterOpen && <CallsFilter />}
        detail={isDetailOpen && <CallsDetail />}
      />
    </>
  );
};

const RestrictedCallsPage: FC = () => (
  <HaveSectionAccess>
    <CallsPage />
  </HaveSectionAccess>
);

export { RestrictedCallsPage as CallsPage };
