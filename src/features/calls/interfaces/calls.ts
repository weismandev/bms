import { IReaction } from './reactions';
import { IUser } from './users';

type IStatusType = 'success' | 'missed';

export interface IStatusSelectOption {
  type: IStatusType;
  title: 'Принятые' | 'Пропущенные';
}

export interface IStatus {
  type: IStatusType;
  title: 'Принят' | 'Пропущен';
}

export type ICallDirectionType = 'in' | 'out';

export interface ICallInfo {
  uid: string;
  duration: string;
  timestamp: string;
  record: string;
  client_phone: string;
  src: string;
  dst: string;
  status: IStatus;
  direction: ICallDirectionType;
}

export interface ICallInfoFormatted extends Omit<ICallInfo, 'duration' | 'timestamp'> {
  duration: number;
  timestamp: number;
}

export interface ICall {
  id: string;
  reaction: IReaction | null;
  is_known_user: boolean;
  user: Partial<IUser> | null;
  call_info: ICallInfo;
}

export interface ICallFormatted extends Omit<ICall, 'call_info'> {
  call_info: ICallInfoFormatted;
}

export interface ICallsGetPayload {
  connection: string;
  start_date: string;
  end_date: string;
  min_duration: number;
  max_duration: number;
  src: string;
  page: number;
}

export interface ICallsGetResponse {
  items: ICall[];
}
