export interface IConnection {
  id: number;
  title: string;
  provider: {
    id: number;
    title: string;
  };
}

export interface IConnectionsResponse {
  items: IConnection[];
}
