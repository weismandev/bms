export interface ICompetence {
  id: number;
  title: string;
}

type IUserId = { user_id: string } | { userdata_id: string };

type IUserFullname =
  | { fullname: string }
  | { user_fullname: string }
  | { userdata_fullname: string };

type IUserPhone = { phone: string } | { userdata_phone: string };

export type IUser = IUserId &
  IUserFullname &
  IUserPhone & {
    competences: ICompetence[];
    on_quarantine: number;
    position_title: string;
    rating: string | null;
    role_id: number;
    ticket_notify: boolean;
    ticket_notify_email: boolean;
    userdata_addresses: number[] | [];
    userdata_avatar: string;
    userdata_email: string;
    apartment?: {
      title: string;
      [field: string]: string;
    };
  };

export type IUsersResponse = IUser[];

export interface ICurrentUser {
  token: string;
  user_avatar: string;
  user_fullname: string;
  user_id: number;
  user_mail: string;
  user_name: string;
  user_patronymic: '';
  user_phone: number;
  user_surname: string;
}
