import { ReactNode, ChangeEvent } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { FieldProps, FormikValues } from 'formik';
import { TableRow } from '@devexpress/dx-react-grid';
import { ICall, ICallFormatted, IStatus, IStatusSelectOption } from './calls';
import { IReactionType } from './reactions';
import { IUser } from './users';

type IFieldType = FieldProps['field'];

export type IModeType = 'view' | 'edit';

export interface ITableRow {
  row: ICallFormatted;
  tableRow: TableRow;
  children?: ReactNode;
}

export type ISetFieldValueType<T> = (
  field: string,
  value: T,
  shouldValidate?: boolean
) => void;
export interface IFilterStatusSelect {
  values: FormikValues;
  setFieldValue: ISetFieldValueType<IStatusSelectOption>;
}

export interface IDetailSelectReaction {
  mode: IModeType;
  setFieldValue: ISetFieldValueType<IReactionType>;
}

export interface IDetailSelectEmployee {
  mode: IModeType;
  setFieldValue: ISetFieldValueType<IUser>;
}

export interface IDetailSelectStatus {
  mode: IModeType;
  setFieldValue: ISetFieldValueType<IStatus>;
}

export interface IStatusSelect {
  onChange: (arg0: string, arg1: ChangeEvent<HTMLInputElement>) => void;
  field: IFieldType;
}

export interface IStatusFormatter {
  row?: ICall;
  value: string;
}

export interface ITicketRaw {
  opened: ICall;
  values: FormikValues;
}

export interface ITicketCreate extends RouteComponentProps {
  opened: ICall;
  values: FormikValues;
}
