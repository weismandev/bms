import { ICall } from './calls';

export type operatorIdType = number | null;

export interface IReactionType {
  id: number;
  title: string;
}

export interface IReactionTypesResponse {
  items: IReactionType[];
}

export interface IReaction {
  id: number;
  operator_id: operatorIdType;
  reaction_id: number;
}

export interface IReactionSetPayload {
  connection_id: number;
  call_uid: string;
  reaction_id: number;
  operator_id: operatorIdType;
}

export interface IReactionSetResponse {
  call: ICall;
}
