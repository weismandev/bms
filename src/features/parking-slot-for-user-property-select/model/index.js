import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { changeEnterprise } from '../../stuff/models/detail.model';
import { parkingApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result.items) && !Array.isArray(result.slots)) {
      return [];
    }

    if (result.slots) {
      const sortedSlotsByZoneAndByNumber = [...result.slots].sort(
        (a, b) => b.zone.localeCompare(a.zone) || a.number - b.number
      );

      return sortedSlotsByZoneAndByNumber;
    }

    return result.items.reduce((acc, cur) => [...acc, ...cur.related_slots], []);
  })
  .reset([signout, changeEnterprise]);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use((params) => {
  return parkingApi.getUserSlots(params);
});
export { fxGetList, $data, $error, $isLoading };
