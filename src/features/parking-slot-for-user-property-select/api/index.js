import { api } from '../../../api/api2';

const getUserSlots = (payload = {}) =>
  api.v1('get', 'parking/slots/index', { ...payload, per_page: 1000 });

export const parkingApi = {
  getUserSlots,
};
