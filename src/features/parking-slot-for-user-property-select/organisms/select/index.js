import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const ParkingSlotForUserPropertySelect = (props) => {
  const { buildings = [], userdata_id, сurrentSlot, ...rest } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const [building_id] = buildings;
  useEffect(() => {
    fxGetList({ buildings, userdata_id });
  }, [userdata_id, building_id, buildings.length]);
  const isCurrentSlot =
    сurrentSlot && сurrentSlot.id && !options.some((e) => e.id === сurrentSlot.id);

  const currentOptions = isCurrentSlot ? [сurrentSlot, ...options] : options;

  return (
    <SelectControl
      options={currentOptions}
      isLoading={isLoading}
      error={error}
      getOptionLabel={(opt) => (opt ? `[${opt.number}] ${opt.zone}` : '')}
      {...rest}
    />
  );
};

const ParkingSlotForUserPropertySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ParkingSlotForUserPropertySelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { ParkingSlotForUserPropertySelect, ParkingSlotForUserPropertySelectField };
