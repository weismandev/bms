import { api } from '../../../api/api2';

const getList = () => api.no_vers('get', 'information/get-crash-types/');

export const alarmsTypeApi = { getList };
