import { createEffect, createStore, createEvent, forward, sample, guard } from 'effector';
import { signout } from '../../common';
import { alarmsTypeApi } from '../api';

const load = createEvent();
const forceLoad = createEvent();

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data.on(fxGetList.doneData, (state, { types = [] }) => types).reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(alarmsTypeApi.getList);

forward({
  from: forceLoad,
  to: fxGetList,
});

guard({
  source: sample($data, load),
  filter: (data) => data.length === 0,
  target: fxGetList,
});

export { fxGetList, load, forceLoad, $data, $error, $isLoading };
