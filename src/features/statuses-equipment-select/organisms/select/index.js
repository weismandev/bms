import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data } from '../../model';

const StatusesEquipmentSelect = (props) => {
  const options = useStore($data);

  return <SelectControl options={options} {...props} />;
};

const StatusesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<StatusesEquipmentSelect />} onChange={onChange} {...props} />
  );
};

export { StatusesEquipmentSelect, StatusesSelectField };
