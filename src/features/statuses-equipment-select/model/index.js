import { createStore } from 'effector';

const $data = createStore([
  {
    title: 'Активно',
    state: 'ok',
  },
  {
    title: 'Отключено',
    state: 'disconnect',
  },
  {
    title: 'Статус неизвестен',
    state: 'undefined',
  },
]);

export { $data };
