export {
  $data,
  $isLoading,
  changePage,
  closeMenu,
  searchChange,
  changeObject,
  fxGetList,
  fxGetListWithObject,
  $perPage,
  createScope,
} from './people-select';
