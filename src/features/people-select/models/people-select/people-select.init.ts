import { guard } from 'effector';
import { debounce } from 'patronum/debounce';

import { signout } from '@features/common';

import { peopleApi } from '../../api';
import { PeopleItem, PeopleResponse, PeoplePayload, ObjectItem } from '../../interfaces';
import {
  fxGetList,
  $data,
  $isLoading,
  $perPage,
  $lastPage,
  changePage,
  $page,
  closeMenu,
  $object,
  changeObject,
  selectUnmounted,
  fxGetListWithObject,
  $search,
  searchChange,
} from './people-select.model';

fxGetList.use(peopleApi.getPeople);
fxGetListWithObject.use(peopleApi.getPeople);

const searchChangeDebounce = debounce({
  source: searchChange,
  timeout: 2000,
});

$page
  .on(changePage, (page) => page + 1)
  .reset([signout, closeMenu, searchChangeDebounce]);

$lastPage
  .on(fxGetList.done, (_, { result }) => result?.meta?.last_page || 0)
  .on(fxGetListWithObject.done, (_, { result }) => result?.meta?.last_page || 0)
  .reset([signout, closeMenu, searchChangeDebounce]);

guard({
  clock: [closeMenu, searchChangeDebounce],
  source: { perPage: $perPage, object: $object, q: $search },
  filter: ({ object }) => !object,
  target: fxGetList.prepend(({ perPage, q }: { perPage: number; q: string }) => ({
    per_page: perPage,
    q,
  })),
});

guard({
  clock: $page,
  source: {
    page: $page,
    lastPage: $lastPage,
    perPage: $perPage,
    object: $object,
    q: $search,
  },
  filter: ({ page, lastPage, object }) => !object && page <= lastPage,
  target: fxGetList.prepend(
    ({ page, perPage, q }: { page: number; perPage: number; q: string }) => ({
      page,
      per_page: perPage,
      q,
    })
  ),
});

guard({
  clock: [closeMenu, searchChangeDebounce],
  source: { perPage: $perPage, object: $object, q: $search },
  filter: ({ object }) =>
    Array.isArray(object) && object.every((item: ObjectItem) => Boolean(item?.id)),
  target: fxGetListWithObject.prepend(
    ({
      perPage,
      object,
      q,
    }: {
      perPage: number;
      object: null | ObjectItem;
      q: string;
    }) => ({
      per_page: perPage,
      objects: object,
      q,
    })
  ),
});

guard({
  clock: $page,
  source: {
    page: $page,
    lastPage: $lastPage,
    perPage: $perPage,
    object: $object,
    q: $search,
  },
  filter: ({ page, lastPage, object }) =>
    Array.isArray(object) &&
    object.every((item: ObjectItem) => Boolean(item?.id)) &&
    page <= lastPage,
  target: fxGetListWithObject.prepend(
    ({
      page,
      perPage,
      object,
      q,
    }: {
      page: number;
      perPage: number;
      object: null | ObjectItem;
      q: string;
    }) => ({
      page,
      per_page: perPage,
      objects: object,
      q,
    })
  ),
});

const formatData = (
  state: PeopleItem[],
  params: PeoplePayload,
  result: PeopleResponse
) => {
  if (!Array.isArray(result?.list)) {
    return [];
  }

  const formattedData = result.list.map((item) => ({
    id: item.id,
    title: item.title,
    email: item.email,
    phone: item.phone,
  }));

  return params?.page ? [...state, ...formattedData] : formattedData;
};

$data
  .on(fxGetList.done, (state, { params, result }) => formatData(state, params, result))
  .on(fxGetListWithObject.done, (state, { params, result }) =>
    formatData(state, params, result)
  )
  .reset([signout, closeMenu, changeObject, searchChangeDebounce]);

$isLoading
  .on([fxGetList.pending, fxGetListWithObject.pending], (_, result) => result)
  .reset([signout, closeMenu]);

$object.on(changeObject, (_, object) => object || null).reset([selectUnmounted, signout]);

$search.on(searchChangeDebounce, (_, search) => search).reset([signout, closeMenu]);
