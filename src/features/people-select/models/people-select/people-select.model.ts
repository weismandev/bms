import { fork, Scope, createDomain } from 'effector';
import { createGate } from 'effector-react';

import { PeopleResponse, PeoplePayload, PeopleItem, ObjectItem } from '../../interfaces';

export const scopes: { [key: string]: Scope } = {};

export const selectDomain = createDomain();

const SelectGate = createGate();

const selectUnmounted = SelectGate.close;

const changePage = selectDomain.createEvent<void>();
const closeMenu = selectDomain.createEvent<void>();
const changeObject = selectDomain.createEvent<null | ObjectItem>();
const searchChange = selectDomain.createEvent<string>();

const fxGetList = selectDomain.createEffect<PeoplePayload, PeopleResponse, Error>();
const fxGetListWithObject = selectDomain.createEffect<
  PeoplePayload,
  PeopleResponse,
  Error
>();

const $data = selectDomain.createStore<PeopleItem[]>([]);
const $isLoading = selectDomain.createStore<boolean>(false);
const $perPage = selectDomain.createStore<number>(50);
const $page = selectDomain.createStore<number>(1);
const $lastPage = selectDomain.createStore<number>(0);
const $object = selectDomain.createStore<null | ObjectItem>(null);
const $search = selectDomain.createStore<string>('');

export const createScope = (name: string) => {
  if (scopes[name]) return scopes[name];
  scopes[name] = fork(selectDomain);

  return scopes[name];
};

export {
  fxGetList,
  $data,
  $isLoading,
  SelectGate,
  selectUnmounted,
  $perPage,
  $lastPage,
  changePage,
  $page,
  closeMenu,
  changeObject,
  $object,
  fxGetListWithObject,
  searchChange,
  $search,
};
