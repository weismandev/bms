import { FC, useEffect } from 'react';
import { Provider, useUnit } from 'effector-react/scope';

import { SelectControl, SelectField } from '@ui/index';

import {
  $data,
  $isLoading,
  changePage,
  closeMenu,
  searchChange,
  fxGetList,
  fxGetListWithObject,
  $perPage,
  changeObject,
  createScope,
} from '../../models';
import { PeopleItem, ObjectItem } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  object?: ObjectItem[];
}

const PeopleSelect: FC<Props> = ({ object, ...props }) => {
  const options = useUnit($data);
  const isLoading = useUnit($isLoading);
  const perPage = useUnit($perPage);
  const getList = useUnit(fxGetList);
  const getListWithObject = useUnit(fxGetListWithObject);
  const changeObjectFn = useUnit(changeObject);
  const searchChangeFn = useUnit(searchChange);
  const changePageFn = useUnit(changePage);
  const closeMenuFn = useUnit(closeMenu);

  useEffect(() => {
    if (!object) {
      getList({ per_page: perPage });
    }
  }, [object]);

  useEffect(() => {
    changeObjectFn(object);

    if (Array.isArray(object) && object.every((item) => Boolean(item?.id))) {
      getListWithObject({ per_page: perPage, objects: object });
    }
  }, [JSON.stringify(object)]);

  const handleInput = (value: string, { action }: { action: string }) => {
    if (action === 'input-change') {
      searchChangeFn(value);
    }
  };

  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      onMenuScrollToBottom={changePageFn}
      onMenuClose={closeMenuFn}
      onInputChange={handleInput}
      {...props}
    />
  );
};

const PeopleSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: PeopleItem) =>
    props.form.setFieldValue(props.field.name, value);

  const scope = createScope(props.field.name);

  return (
    <Provider value={scope}>
      <SelectField component={<PeopleSelect />} onChange={onChange} {...props} />
    </Provider>
  );
};

export { PeopleSelect, PeopleSelectField };
