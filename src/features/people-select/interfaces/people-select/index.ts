export interface PeoplePayload {
  page?: number;
  per_page?: number;
  object?: null | ObjectItem;
  q?: string;
}

export interface PeopleResponse {
  list: PeopleItem[];
  meta: {
    last_page: number;
  };
}

export interface PeopleItem {
  id: number;
  title: string;
  email: string;
  phone: string;
}

export interface ObjectItem {
  id: number;
  type: string;
}
