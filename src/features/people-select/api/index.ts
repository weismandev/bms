import { api } from '@api/api2';

import { PeoplePayload } from '../interfaces';

const getPeople = (payload: PeoplePayload) => api.v1('get', 'crm/userdata/list', payload);

export const peopleApi = {
  getPeople,
};
