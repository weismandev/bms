import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';

import { HaveSectionAccess } from '@features/common';
import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '@ui/index';

import {
  $error,
  $isDetailOpen,
  $isErrorDialogOpen,
  $isLoading,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  changedErrorDialogVisibility,
  deleteWork,
  PageGate,
} from '../models';
import { Detail, Table } from '../organisms';

const WorksPage: FC = () => {
  const [error, isErrorDialogOpen, isLoading, isDetailOpen, isOpenDeleteModal] = useUnit([
    $error,
    $isErrorDialogOpen,
    $isLoading,
    $isDetailOpen,
    $isOpenDeleteModal,
  ]);
  const { t } = useTranslation();

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteMessage = () => changedDeleteVisibilityModal(false);

  return (
    <>
      <PageGate />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <DeleteConfirmDialog
        header={t('DeletingAnObject')}
        content={t('AreYouSureYouWantToRemoveThisObjectFromTheDirectory')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteMessage}
        confirm={deleteWork}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={null}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedWorksPage: FC = () => (
  <HaveSectionAccess>
    <WorksPage />
  </HaveSectionAccess>
);

export { RestrictedWorksPage as WorksPage };
