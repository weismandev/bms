export interface WorkType {
  id: number;
  title: string;
}

export interface WorkTypeResponse {
  work_types: WorkType[];
}
