import { WorkType } from '../work-type';

export interface Value {
  id: number;
  title: string;
}

export interface Qualification {
  id: number;
  title: string;
  planned_duration_minutes: number;
  employees_number: number;
}

export interface Work {
  id: number;
  title: string;
  work_type: WorkType;
  assignee_appointment_method_id: number;
  tech_cards: Value[];
  qualificatins: Qualification[];
  total_planned_duration_minutes: number;
}

export interface WorkData {
  meta: {
    total: number;
  };
  works: Work[];
}

export interface WorkPayload {
  id?: number;
  page?: number;
  per_page?: number;
  search?: string;
  order_direction?: string;
  order_column?: string;
}

export interface WorkResponse {
  work: Work;
}

export interface WorkCreateUpdatePayload {
  id?: number;
  title: string;
  work_type_id: number;
  qualifications: {
    id: number;
    employees_number: number;
    planned_duration_minutes: number;
  }[];
}

export interface GetWorkByIdPayload {
  id: number;
}

export interface OpenedQualification {
  _id: string;
  qualification: Value;
  count: number;
  duration: number;
}

export interface Opened {
  id: number | null;
  title: string;
  workType: WorkType;
  assigneesMethod: Value;
  qualifications: OpenedQualification[];
  tech_cards: TechCard[];
}

export interface DeleteWorkPayload {
  id: number;
}

export interface TechCard {
  id: number;
  title: string;
}
