import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { HighlightOff } from '@mui/icons-material';

import { IconButton } from '@ui/index';

interface Props {
  onClick: () => unknown;
  style: object;
}

export const DeleteButton: FC<Props> = ({ onClick, style }) => {
  const { t } = useTranslation();

  const titleAccess = t('remove');

  return (
    <IconButton onClick={onClick} style={style}>
      <HighlightOff color="error" titleAccess={titleAccess} />
    </IconButton>
  );
};
