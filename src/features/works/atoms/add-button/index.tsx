import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Add } from '@mui/icons-material';

import { ActionButton } from '@ui/index';

interface Props {
  onClick: () => void;
  className: string;
}

export const AddButton: FC<Props> = (props) => {
  const { t } = useTranslation();

  return (
    <ActionButton kind="positive" {...props}>
      <Add />
      {t('Add')}
    </ActionButton>
  );
};
