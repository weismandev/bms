import { signout } from '@features/common';
import { worksApi } from '../../api';
import { $data, $isLoading, fxGetList } from './assignees-method-select.model';

fxGetList.use(worksApi.getAssigneesMethodList);

$data
  .on(fxGetList.done, (_, { result }) =>
    !Array.isArray(result?.methods)
      ? []
      : result.methods.map((item) => ({
          id: item.id,
          title: item?.title || '',
        }))
  )
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
