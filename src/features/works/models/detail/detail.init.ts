import { sample } from 'effector';
import { delay } from 'patronum';
import { nanoid } from 'nanoid';
import { signout, $pathname, historyPush } from '@features/common';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { Opened } from '../../interfaces';
import { pageUnmounted, $path, $entityId, pageMounted } from '../page/page.model';
import {
  openViaUrl,
  $opened,
  fxGetWorkById,
  $isDetailOpen,
  fxUpdateWork,
  entityApi,
  $mode,
  fxCreateWork,
  $isOpenDeleteModal,
  fxDeleteWork,
  changedDeleteVisibilityModal,
  deleteWork,
  changedDetailVisibility,
  newEntity,
} from './detail.model';

// Если в url есть id, то запрашивается работа
sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }) => ({ id: Number.parseInt(entityId, 10) }),
  target: fxGetWorkById,
});

// если падает запрос на работу, то убирается id из url
sample({
  clock: delay({ source: fxGetWorkById.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// после удаления работы из url убирается id
sample({
  clock: fxDeleteWork.done,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// при клике на строку в таблице отправляется запрос на получение работы
sample({
  clock: openViaUrl,
  fn: (id) => ({ id }),
  target: fxGetWorkById,
});

const getTime = (durationInMinutes: number) => {
  const hours = Math.floor(durationInMinutes / 60);
  const minutes = durationInMinutes % 60;

  const h = hours.toString().length === 1 ? `0${hours}` : hours;
  const m = minutes.toString().length === 1 ? `0${minutes}` : minutes;

  return `${h}:${m}`;
};

$opened
  .on(fxGetWorkById.done, (_, { result: { work } }) => {
    if (!work) {
      return newEntity;
    }

    return {
      id: work?.id,
      title: work?.title || '-',
      workType: work?.work_type,
      assigneesMethod: work?.assignee_appointment_method_id || '',
      tech_cards: work?.tech_cards,
      qualifications:
        Array.isArray(work?.qualificatins) && work.qualificatins.length > 0
          ? work.qualificatins.map((qualification) => ({
              _id: nanoid(3),
              qualification: {
                id: qualification?.id,
                title: qualification?.title || '',
              },
              count: qualification?.employees_number || '',
              duration: qualification?.planned_duration_minutes
                ? getTime(qualification.planned_duration_minutes)
                : '',
            }))
          : [],
    };
  })
  .reset([pageUnmounted, signout, fxGetWorkById]);

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetWorkById.done, () => true)
  .reset([pageUnmounted, signout, fxDeleteWork.done]);

const formatPayload = (data: Opened) => ({
  title: data.title,
  work_type_id: data.workType.id,
  assignee_appointment_method_id:
    typeof data.assigneesMethod === 'object'
      ? data.assigneesMethod?.id
      : data.assigneesMethod,
  qualifications: Array.isArray(data?.qualifications)
    ? data.qualifications.map((qualification) => {
        const splittedTime = qualification.duration.toString().split(':');

        return {
          id: qualification.qualification.id,
          employees_number: qualification.count,
          planned_duration_minutes:
            Number.parseInt(splittedTime[0], 10) * 60 +
            Number.parseInt(splittedTime[1], 10),
        };
      })
    : [],
});

// отправка запроса на создание работы
sample({
  clock: entityApi.create,
  fn: (data) => formatPayload(data),
  target: fxCreateWork,
});

// отправка запроса на обновление работы
sample({
  clock: entityApi.update,
  fn: ({ id, ...data }) => ({
    id,
    ...formatPayload(data),
  }),
  target: fxUpdateWork,
});

$mode.reset([
  fxUpdateWork.done,
  fxDeleteWork.done,
  signout,
  pageUnmounted,
  fxCreateWork.done,
]);

$isOpenDeleteModal
  .on(changedDeleteVisibilityModal, (_, visibility) => visibility)
  .reset([pageUnmounted, signout, fxDeleteWork.done]);

// отправка запроса на удаление работы
sample({
  clock: deleteWork,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxDeleteWork,
});

// если открыты детали, то при закрытии из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// после создания работы запрашивается работа
sample({
  clock: fxCreateWork.done,
  fn: ({
    result: {
      work: { id },
    },
  }) => ({ id }),
  target: fxGetWorkById,
});

// после обновления работы запрашивается работа
sample({
  clock: fxUpdateWork.done,
  fn: ({
    result: {
      work: { id },
    },
  }) => ({ id }),
  target: fxGetWorkById,
});

// после создания работы в url добавляется id
sample({
  clock: fxCreateWork.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        work: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});
