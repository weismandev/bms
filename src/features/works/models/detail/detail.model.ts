import { createEffect, createEvent, createStore } from 'effector';
import { createDetailBag } from '@tools/factories';
import {
  WorkResponse,
  WorkCreateUpdatePayload,
  GetWorkByIdPayload,
  DeleteWorkPayload,
} from '../../interfaces';

export const newEntity = {
  id: null,
  title: '',
  workType: null,
  assigneesMethod: '',
  qualifications: [],
  tech_cards: [],
};

export const deleteWork = createEvent<void>();
export const changedDeleteVisibilityModal = createEvent<boolean>();

export const $isOpenDeleteModal = createStore<boolean>(false);

export const fxGetWorkById = createEffect<GetWorkByIdPayload, WorkResponse, Error>();
export const fxCreateWork = createEffect<WorkCreateUpdatePayload, WorkResponse, Error>();
export const fxUpdateWork = createEffect<WorkCreateUpdatePayload, WorkResponse, Error>();
export const fxDeleteWork = createEffect<DeleteWorkPayload, WorkResponse, Error>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
