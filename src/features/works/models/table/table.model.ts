import { combine } from 'effector';

import i18n from '@shared/config/i18n';
import { createTableBag, Columns } from '@features/data-grid';

import { Work } from '../../interfaces';
import { $rawDataWorks } from '../page/page.model';

const { t } = i18n;

const columns = [
  {
    field: 'number',
    headerName: t('№pp'),
    width: 80,
    sortable: false,
  },
  { field: 'workTypeTitle', headerName: t('View'), width: 400, sortable: true },
  { field: 'title', headerName: t('NameTitle'), width: 230, sortable: true },
  { field: 'techCards', headerName: t('TechnicalMap'), width: 200, sortable: false },
  {
    field: 'qualifications',
    headerName: t('Qualification'),
    width: 200,
    sortable: false,
  },
  {
    field: 'plannedDuration',
    headerName: t('WorkingHours'),
    width: 140,
    sortable: true,
  },
];

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} = createTableBag({
  columns,
  pageSize: 25,
});

const getTime = (durationInMinutes: number) => {
  const hours = Math.floor(durationInMinutes / 60);
  const minutes = durationInMinutes % 60;

  return `${hours ? `${hours} ч.` : ''} ${minutes ? `${minutes} мин.` : ''}`;
};

const formatData = (works: Work[]) =>
  works.map((work, index) => ({
    id: work.id,
    number: index + 1,
    workTypeTitle: work.work_type.title || t('Unknown'),
    title: work.title || t('Unknown'),
    techCards:
      Array.isArray(work?.tech_cards) && work.tech_cards.length > 0
        ? work.tech_cards.map((card) => ({
            id: card.id,
            title: card?.title || t('Unknown'),
          }))
        : [{ id: 1, title: '-' }],
    qualifications:
      Array.isArray(work?.qualificatins) && work.qualificatins.length > 0
        ? work.qualificatins.map((qualificatin) => ({
            id: qualificatin.id,
            title: qualificatin?.title || t('Unknown'),
          }))
        : [{ id: 1, title: '-' }],
    plannedDuration: work?.total_planned_duration_minutes
      ? getTime(work.total_planned_duration_minutes)
      : '-',
  }));

export const $count = $rawDataWorks.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawDataWorks.map(({ works }) =>
  works && works.length > 0 ? formatData(works) : []
);
export const $countPage = combine($count, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);
export const $savedColumns = $columns.map((currentColumn: Columns[]) =>
  currentColumn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
