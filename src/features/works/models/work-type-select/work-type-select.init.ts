import { signout } from '@features/common';

import { worksApi } from '../../api';
import { $data, $isLoading, fxGetList } from './work-type-select.model';

fxGetList.use(worksApi.getTypesWork);

$data
  .on(fxGetList.done, (_, { result }) =>
    !Array.isArray(result?.work_types) ? [] : result.work_types
  )
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
