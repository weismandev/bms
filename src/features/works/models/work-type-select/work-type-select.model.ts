import { createEffect, createStore } from 'effector';

import { WorkType, WorkTypeResponse } from '../../interfaces';

export const fxGetList = createEffect<void, WorkTypeResponse, Error>();

export const $data = createStore<WorkType[]>([]);
export const $isLoading = createStore<boolean>(false);
