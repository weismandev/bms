import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { $pathname } from '@features/common';

import { WorkData, WorkPayload } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetWorks = createEffect<WorkPayload, WorkData, Error>();

export const $rawDataWorks = createStore<WorkData>({
  meta: {
    total: 0,
  },
  works: [],
});
export const $isLoading = createStore<boolean>(false);
export const $isLoadingList = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2]);
