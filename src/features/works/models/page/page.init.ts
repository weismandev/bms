import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';

import { signout } from '@features/common';

import { worksApi } from '../../api';
import { $tableParams } from '../table/table.model';
import {
  fxGetWorkById,
  fxCreateWork,
  fxUpdateWork,
  fxDeleteWork,
} from '../detail/detail.model';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  $rawDataWorks,
  fxGetWorks,
  pageMounted,
  pageUnmounted,
  $isLoadingList,
} from './page.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import { WorkPayload } from '../../interfaces';

fxGetWorks.use(worksApi.getWorks);
fxGetWorkById.use(worksApi.getWorkById);
fxCreateWork.use(worksApi.createWork);
fxUpdateWork.use(worksApi.updateWork);
fxDeleteWork.use(worksApi.deleteWork);

const errorOccured = merge([
  fxGetWorks.fail,
  fxGetWorkById.fail,
  fxCreateWork.fail,
  fxUpdateWork.fail,
  fxDeleteWork.fail,
]);

$isLoading
  .on(
    pending({
      effects: [fxGetWorkById, fxCreateWork, fxUpdateWork, fxDeleteWork],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$isLoadingList
  .on(fxGetWorks.pending, (_, isLoadingList) => isLoadingList)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$rawDataWorks
  .on(fxGetWorks.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

// получение списка работ
sample({
  clock: [
    pageMounted,
    $tableParams,
    fxCreateWork.done,
    fxUpdateWork.done,
    fxDeleteWork.done,
    $settingsInitialized,
  ],
  source: { table: $tableParams, settingsInitialized: $settingsInitialized },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table: { page, per_page, search, sorting } }) => {
    const payload: WorkPayload = {
      page,
      per_page,
      search,
    };

    if (sorting.length > 0) {
      if (sorting[0].field === 'workTypeTitle') {
        payload.order_column = 'work_type_title';
      }

      if (sorting[0].field === 'title') {
        payload.order_column = 'title';
      }

      if (sorting[0].field === 'plannedDuration') {
        payload.order_column = 'total_planned_duration_minutes';
      }

      payload.order_direction = sorting[0].sort || 'desc';
    }

    return payload;
  },
  target: fxGetWorks,
});

// во время получения пользовательских данных отображается лоадер
sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});
