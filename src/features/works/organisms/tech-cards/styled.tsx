import { styled } from '@mui/material';

export const TechCards = styled('div')`
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
  padding-bottom: 10px;
  margin-bottom: 10px;
`;

export const Header = styled('h3')`
  font-family: Roboto;
  margin-bottom: 10px;
  font-size: 14px;
  font-weight: 400;
  line-height: 16px;
  color: rgba(0, 0, 0, 0.54);
`;

export const TechCard = styled('p')`
  font-family: Roboto;
  font-size: 13px;
  font-weight: 500;
  line-height: 16px;
  color: rgba(101, 101, 123, 1);
  margin: 6px 0;
`;
