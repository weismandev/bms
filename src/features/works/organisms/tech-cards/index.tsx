import { TechCard } from '@features/works/interfaces';
import * as Styled from './styled';

export const TechCards = ({ techcards }: { techcards: TechCard[] }) => {
  if (!techcards?.length) return null;

  return (
    <Styled.TechCards>
      <Styled.Header>Входит в состав техкарт</Styled.Header>
      {techcards.map(({ title }) => (
        <Styled.TechCard>{title}</Styled.TechCard>
      ))}
    </Styled.TechCards>
  );
};
