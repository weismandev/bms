import { memo, FC } from 'react';
import { useUnit } from 'effector-react';

import { Wrapper, HiddenArray } from '@ui/index';
import { DataGrid } from '@features/data-grid';

import {
  $tableData,
  openViaUrl,
  $isLoadingList,
  $selectRow,
  selectionRowChanged,
  sortChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} from '../../models';
import { CustomToolbar } from '../custom-toolbar';
import { Value } from '../../interfaces';
import { useStyles } from './styles';

export const Table: FC = memo(() => {
  const [
    tableData,
    isLoading,
    selectRow,
    sorting,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
  ] = useUnit([
    $tableData,
    $isLoadingList,
    $selectRow,
    $sorting,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
  ]);
  const classes = useStyles();

  const techCardsIndex = columns.findIndex((column) => column.field === 'techCards');
  const qualificationsIndex = columns.findIndex(
    (column) => column.field === 'qualifications'
  );

  columns[techCardsIndex].renderCell = ({ value }: { value: Value[] }) => (
    <HiddenArray>
      {value.map((item) => (
        <span key={item.id}>{item.title}</span>
      ))}
    </HiddenArray>
  );

  columns[qualificationsIndex].renderCell = ({ value }: { value: Value[] }) => (
    <HiddenArray>
      {value.map((item) => (
        <span key={item.id}>{item.title}</span>
      ))}
    </HiddenArray>
  );

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    open: openViaUrl,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <Wrapper className={classes.wrapper}>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </Wrapper>
  );
});
