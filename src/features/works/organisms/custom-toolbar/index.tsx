import { FC, ChangeEvent } from 'react';
import { useUnit } from 'effector-react';

import { Toolbar, FoundInfo, AddButton, SearchInput, Greedy } from '@ui/index';

import { $count, $search, searchChanged, addClicked, $mode, $opened } from '../../models';
import { Opened } from '../../interfaces';
import { useStyles } from './styles';

export const CustomToolbar: FC = () => {
  const [count, search, mode, opened] = useUnit([$count, $search, $mode, $opened]);
  const classes = useStyles();

  const isDisabled = mode === 'edit' && !(opened as Opened).id;

  const onChange = (e: ChangeEvent<HTMLInputElement>) => searchChanged(e.target.value);
  const onClear = () => searchChanged('');

  return (
    <Toolbar>
      <FoundInfo count={count} className={classes.margin} />
      <SearchInput value={search} onChange={onChange} handleClear={onClear} />
      <Greedy />
      <AddButton className={classes.margin} onClick={addClicked} disabled={isDisabled} />
    </Toolbar>
  );
};
