import { FC, ChangeEvent } from 'react';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Toolbar, FoundInfo, AddButton, SearchInput, Greedy } from '@ui/index';
import { $rowCount, $search, searchChanged, addClicked } from '../../models';

const useStyles = makeStyles({
  margin: {
    margin: '0 10px',
  },
});

const TableToolbar: FC = () => {
  const count = useStore($rowCount);
  const search = useStore($search);

  const classes = useStyles();

  return (
    <Toolbar>
      <FoundInfo count={count} className={classes.margin} />
      <SearchInput
        value={search}
        onChange={(e: ChangeEvent<HTMLInputElement>) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <AddButton className={classes.margin} onClick={addClicked} />
    </Toolbar>
  );
};

export { TableToolbar };
