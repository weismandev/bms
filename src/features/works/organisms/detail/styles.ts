import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
  },
  form: {
    height: 'calc(100% - 50px)',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
  },
  content: {
    height: 'calc(100% - 40px)',
    padding: '0 6px 24px 24px',
  },
  contentForm: {
    paddingRight: 24,
  },
});
