import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const requiredText = t('thisIsRequiredField');
const lengthText = t('TheNameMustHaveFrom1To70Characters');

export const validationSchema = Yup.object().shape({
  workType: Yup.object().nullable().required(requiredText),
  title: Yup.string().test(
    'length',
    lengthText,
    (value: any) => (value as string)?.length >= 1 && (value as string)?.length <= 70
  ),
  assigneesMethod: Yup.mixed().required(requiredText),
  qualifications: Yup.array().of(
    Yup.object().shape({
      qualification: Yup.object().required(requiredText).nullable(),
      count: Yup.string().required(requiredText),
      duration: Yup.string().required(requiredText),
    })
  ),
});
