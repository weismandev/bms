import { FC, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Wrapper, DetailToolbar, CustomScrollbar, InputField } from '@ui/index';
import { Opened } from '../../interfaces';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  changedDeleteVisibilityModal,
} from '../../models';
import { AssigneesMethodSelectField } from '../assignees-method-select';
import { Qualifications } from '../qualifications';
import { TechCards } from '../tech-cards';
import { WorkTypeSelectField } from '../work-type-select';
import { useStyles } from './styles';
import { validationSchema } from './validation';

export const Detail: FC = memo(() => {
  const [mode, opened] = useUnit([$mode, $opened]);
  const { t } = useTranslation();
  const classes = useStyles();

  const isNew = !(opened as Opened).id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ resetForm, values }) => {
          const onEdit = () => changeMode('edit');
          const onClose = () => changedDetailVisibility(false);

          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };

          const onDelete = () => changedDeleteVisibilityModal(true);

          return (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.toolbar}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <div className={classes.content}>
                <CustomScrollbar>
                  <div className={classes.contentForm}>
                    <Field
                      name="workType"
                      component={WorkTypeSelectField}
                      label={t('View')}
                      placeholder={t('ChooseTheTypeOfWork')}
                      required
                      mode={mode}
                    />
                    <Field
                      name="title"
                      component={InputField}
                      label={t('NameTitle')}
                      placeholder={t('EnterTheTitleOfTheWork')}
                      required
                      mode={mode}
                    />
                    <TechCards techcards={(values as Opened).tech_cards} />
                    <Field
                      name="assigneesMethod"
                      component={AssigneesMethodSelectField}
                      label={t('AppointmentExecutor')}
                      placeholder={t('ChooseDestination')}
                      mode={mode}
                      required
                    />
                    <Qualifications qualifications={(values as Opened).qualifications} />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});
