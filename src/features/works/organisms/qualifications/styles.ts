import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    background: '#ECF6FF',
    borderRadius: 16,
    padding: 24,
  },
  title: {
    fontWeight: 500,
    fontSize: 18,
    color: '#3b3b50',
  },
  addButton: {
    marginTop: 20,
  },
  fields: {
    display: 'grid',
    gridTemplateColumns: '1fr 100px 100px 30px',
    gap: 30,
    marginTop: 15,
  },
});
