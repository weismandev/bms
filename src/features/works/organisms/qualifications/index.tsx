import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Field, FieldArray } from 'formik';
import { Typography } from '@mui/material';

import { InputField } from '@ui/index';
import { QualificationsSelectField } from '@features/qualifications-select';

import { getQualificationEntity } from '../../libs';
import { AddButton, DeleteButton } from '../../atoms';
import { $mode } from '../../models';
import { OpenedQualification } from '../../interfaces';
import { useStyles } from './styles';

interface Props {
  qualifications: OpenedQualification[];
}

export const Qualifications: FC<Props> = ({ qualifications }) => {
  const mode = useUnit($mode);
  const { t } = useTranslation();
  const classes = useStyles();

  if (qualifications.length === 0 && mode === 'view') {
    return null;
  }

  return (
    <div className={classes.wrapper}>
      <Typography classes={{ root: classes.title }}>
        {t('EmployeeQualification')}
      </Typography>
      <FieldArray
        name="qualifications"
        render={({ push, remove }) => {
          const qualificationsCards = qualifications.map((qualification, index) => {
            const onDelete = () => remove(index);

            return (
              <div key={qualification._id} className={classes.fields}>
                <Field
                  name={`qualifications[${index}].qualification`}
                  label={index === 0 && t('Qualification')}
                  placeholder={t('SelectQualification')}
                  component={QualificationsSelectField}
                  mode={mode}
                  divider={false}
                  required
                />
                <Field
                  name={`qualifications[${index}].count`}
                  label={index === 0 && t('Amount')}
                  placeholder={t('EnterQuantity')}
                  component={InputField}
                  mode={mode}
                  divider={false}
                  type="number"
                  required
                />
                <Field
                  name={`qualifications[${index}].duration`}
                  label={index === 0 && t('WorkingHours')}
                  placeholder={t('chooseTime')}
                  component={InputField}
                  mode={mode}
                  divider={false}
                  type="time"
                  required
                />
                {mode === 'edit' && (
                  <DeleteButton
                    onClick={onDelete}
                    style={{ borderRadius: 25, marginTop: index === 0 ? 26 : 0 }}
                  />
                )}
              </div>
            );
          });

          const onClickAdd = () => push(getQualificationEntity());

          return (
            <>
              {qualificationsCards}
              {mode === 'edit' && (
                <AddButton className={classes.addButton} onClick={onClickAdd} />
              )}
            </>
          );
        }}
      />
    </div>
  );
};
