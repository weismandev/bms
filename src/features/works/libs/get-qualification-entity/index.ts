import { nanoid } from 'nanoid';

export const getQualificationEntity = () => ({
  _id: nanoid(3),
  qualification: null,
  count: '',
  duration: '',
});
