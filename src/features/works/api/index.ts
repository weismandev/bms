import { api } from '@api/api2';
import {
  WorkPayload,
  WorkCreateUpdatePayload,
  GetWorkByIdPayload,
  DeleteWorkPayload,
} from '../interfaces';

const getWorks = (payload: WorkPayload) => api.v4('get', 'works/list', payload);
const getWorkById = (payload: GetWorkByIdPayload) => api.v4('get', 'works/view', payload);
const getTypesWork = () => api.v4('get', 'time-tracker/work-types');
const createWork = (payload: WorkCreateUpdatePayload) =>
  api.v4('post', 'works/create', payload);
const updateWork = (payload: WorkCreateUpdatePayload) =>
  api.v4('post', 'works/update', payload);
const deleteWork = (payload: DeleteWorkPayload) =>
  api.v4('post', 'works/delete', payload);
const getAssigneesMethodList = () =>
  api.v4('get', 'ticket/assignee-appointment-method-list');

export const worksApi = {
  getWorks,
  getWorkById,
  getTypesWork,
  createWork,
  updateWork,
  deleteWork,
  getAssigneesMethodList,
};
