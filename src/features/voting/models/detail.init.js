import { sample, forward, attach, guard } from 'effector';
import { delay } from 'patronum';
import { format } from 'date-fns';
import { Error as ErrorIcon, CheckCircle } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { $notification } from '../../colored-text-icon-notification';
import { signout, history } from '../../common';
import {
  $opened,
  openViaUrl,
  addClicked,
  newEntity,
  $voteResults,
  entityApi,
  $mode,
  $isDetailOpen,
  $currentTab,
  changeTab,
  $tableData,
  $selection,
  selectionChanged,
  $currentPage,
  $pageSize,
  $sorting,
  changeCancelDialogVisibility,
  $isCancelDialogOpen,
  changeVoteDialog,
  $voteDialog,
  changeMessageDialog,
  $messageDialog,
  $wasNotDetailOpen,
  $lastOpened,
  cancelVotingCreate,
  changedDetailVisibility,
} from './detail.model';
import {
  fxGetById,
  fxGetVoteResults,
  fxCreate,
  pageUnmounted,
  deleteConfirmed,
  fxDelete,
  fxPublish,
  fxCancel,
  fxCloseToCounting,
  fxGetVotersDetailList,
  fxClose,
  fxArchive,
  $raw,
  fxVoteForUser,
  fxNotify,
  $entityId,
  $path,
} from './page.model';

const { t } = i18n;

$voteDialog
  .on(changeVoteDialog, (state, payload) => ({
    ...state,
    ...payload,
  }))
  .on(fxVoteForUser.done, () => ({
    isOpen: false,
  }))
  .reset(signout);

$messageDialog
  .on(changeMessageDialog, (state, payload) => ({
    ...state,
    ...payload,
  }))
  .on(fxNotify.done, () => ({ isOpen: false }))
  .reset(signout);

$isCancelDialogOpen
  .on(changeCancelDialogVisibility, (state, visibility) => visibility)
  .on(fxCancel.done, () => false)
  .reset(signout);

$notification
  .on(fxCancel.done, () => ({
    isOpen: true,
    text: t('MeetingCanceled'),
    Icon: ErrorIcon,
    color: '#EB5757',
  }))
  .on(fxCreate.done, () => ({
    isOpen: true,
    text: t('MeetingDraftCreated'),
    Icon: CheckCircle,
    color: '#1BB169',
  }))
  .on(fxVoteForUser.done, () => ({
    isOpen: true,
    text: t('VoiceSaved'),
    Icon: CheckCircle,
    color: '#1BB169',
  }))
  .on(fxNotify.done, () => ({
    isOpen: true,
    text: t('MessageSent'),
    Icon: CheckCircle,
    color: '#1BB169',
  }))
  .on(fxDelete.done, (_, { result }) => {
    if (result.deleted) {
      return {
        isOpen: true,
        text: t('MeetingDraftDeleted'),
        Icon: CheckCircle,
        color: '#1BB169',
      };
    }
    return {
      isOpen: true,
      text: t('DraftDeletionError'),
      Icon: ErrorIcon,
      color: '#EB5757',
    };
  })
  .reset(signout);

$currentTab.on(changeTab, (state, tab) => tab).reset([openViaUrl, signout]);

$selection
  .on(selectionChanged, (state, selection) => selection)
  .reset([openViaUrl, signout]);

$currentPage.reset([openViaUrl, signout]);
$pageSize.reset([openViaUrl, signout]);
$sorting.reset([openViaUrl, signout]);

$isDetailOpen.on($raw.updates, (state, { data }) => data.length > 0).reset(signout);

$tableData
  .on(fxGetVotersDetailList.done, (state, { result }) => result.items || [])
  .reset([openViaUrl, signout]);

$voteResults
  .on(fxGetVoteResults.done, (state, { result }) => result)
  .reset([fxDelete.done, signout]);

$mode
  .on([fxCreate.done, cancelVotingCreate], () => 'view')
  .on(guard(changedDetailVisibility, { filter: (bool) => bool }), () => 'edit')
  .reset(signout);

$opened
  .on(
    [
      fxGetById.done,
      fxCreate.done,
      fxPublish.done,
      fxCancel.done,
      fxCloseToCounting.done,
      fxClose.done,
      fxArchive.done,
    ],
    (state, { result }) => {
      return {
        ...result.vote,
        buildings: Array.isArray(result.vote?.buildings)
          ? result.vote.buildings.map((item) => item.id)
          : [],
        start_date: dateFormat(result.vote.start_date),
        end_date: dateFormat(result.vote.end_date),
        created_at: dateTimeFormat(result.vote.created_at),
        updated_at: dateTimeFormat(result.vote.updated_at),
      };
    }
  )
  .on(addClicked, () => newEntity)
  .reset(signout);

$wasNotDetailOpen.on(openViaUrl, () => false).reset([pageUnmounted, signout]);

$lastOpened.reset([pageUnmounted, signout]);

guard({
  source: guard($raw, { filter: ({ data }) => data.length > 0 }),
  filter: $wasNotDetailOpen,
  target: openViaUrl.prepend(({ data }) => {
    const entityId = $entityId.getState();

    const isExistId = Boolean(entityId) && entityId.length > 0;

    if (isExistId) {
      return entityId;
    }

    return data[0].id;
  }),
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxGetById.fail,
  fn: () => false,
  target: $isDetailOpen,
});

sample({
  source: guard($lastOpened, { filter: (id) => Boolean(id) }),
  clock: cancelVotingCreate,
  target: openViaUrl,
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        vote: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path) => {
    history.push(`../${path}`);

    return false;
  },
  target: $isDetailOpen,
});

sample({
  source: $raw,
  clock: cancelVotingCreate,
  fn: ({ data }) => Boolean(data.length),
  target: $isDetailOpen,
});

function dateFormat(date) {
  return date.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1');
}

function dateTimeFormat(datetime) {
  if (datetime) {
    const dateRegexp = new RegExp(/(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2}):(\d{2})/);

    if (dateRegexp.test(datetime)) {
      const [year, month, day, hour, minute] = datetime
        .replace(dateRegexp, '$3 $2 $1 $4 $5 $6')
        .split(' ')
        .reduce((acc, i) => [...acc, Number(i)], []);

      const localDate = new Date(Date.UTC(year, month - 1, day, hour, minute));
      return format(localDate, 'dd.MM.yyyy HH:mm');
    }
  }
  return '';
}

forward({ from: openViaUrl, to: fxGetById });
forward({
  from: [openViaUrl, fxVoteForUser.done.map(({ params }) => params.vote_id)],
  to: fxGetVoteResults,
});
forward({
  from: [openViaUrl, fxVoteForUser.done.map(({ params }) => params.vote_id)],
  to: fxGetVotersDetailList,
});

forward({
  from: fxCreate.done,
  to: attach({
    effect: fxGetVoteResults,
    mapParams: ({ result }) => result.vote.id,
  }),
});

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: ({ buildings, questions, start_date, end_date, ...rest }) => ({
      end_date: format(new Date(end_date), 'dd.MM.yyyy'),
      start_date: format(new Date(start_date), 'dd.MM.yyyy'),
      buildings: Array.isArray(buildings)
        ? buildings.map((i) => (typeof i === 'object' ? i.id : i))
        : [],
      questions: Array.isArray(questions)
        ? questions.map(({ variants, type, ...rest }) => {
            const oneOfListTypeId = 1;
            const defaultAnswersSet = [
              { value: t('Agree') },
              { value: t('Disagree') },
              { value: t('Abstained') },
            ];

            return {
              variants: defaultAnswersSet,
              type_id: oneOfListTypeId,
              ...rest,
            };
          })
        : [],
      ...rest,
    }),
  }),
});

forward({
  from: sample($opened, deleteConfirmed, (opened) => opened.id),
  to: fxDelete,
});
