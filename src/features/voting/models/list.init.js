import { signout } from '../../common';
import { $isListOpen } from './list.model';
import { $raw } from './page.model';

$isListOpen.on($raw.updates, (state, { data }) => data.length > 0).reset(signout);
