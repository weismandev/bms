import { createEffect, createEvent } from 'effector';
import { $pathname } from '@features/common';
import { createPageBag } from '../../../tools/factories';
import { votingApi } from '../api';

export const getBlankClicked = createEvent();
export const getResultPDFClicked = createEvent();
export const getUserAswerClicked = createEvent();
export const getBlanksClicked = createEvent();

export const fxGetVoteResults = createEffect();
export const fxPublish = createEffect();
export const fxCancel = createEffect();
export const fxCloseToCounting = createEffect();
export const fxClose = createEffect();
export const fxArchive = createEffect();
export const fxGetBlank = createEffect();
export const fxGetResultPDF = createEffect();
export const fxGetVotersDetailList = createEffect();
export const fxGetUserAswerPDF = createEffect();
export const fxVoteForUser = createEffect();
export const fxGetBlankForAllNoVoted = createEffect();
export const fxGetBlankForSelectedNoVoted = createEffect();
export const fxNotify = createEffect();

export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export const {
  PageGate,
  pageUnmounted,
  pageMounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteClicked,
  deleteConfirmed,

  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
} = createPageBag(
  votingApi,
  {},
  {
    idAttribute: 'id',
    itemsAttribute: 'items',
    updatedPath: 'vote',
    createdPath: 'vote',
  }
);

export const $meta = $raw.map(({ meta }) => meta);
