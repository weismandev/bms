import { createStore, createEvent } from 'effector';
import { pick } from '../../../tools/pick';
import { $raw } from './page.model';

const bottomReached = createEvent();

const $isListOpen = createStore(false);

const $listData = $raw.map(({ data }) =>
  data
    .map((item) => ({
      ...pick(['id', 'start_date', 'end_date'], item),
      percent: item.aggregate.voted_percent,
      status: item.status,
      header: item.title,
    }))
    .sort((a, b) => b.id - a.id)
);

export { $listData, bottomReached, $isListOpen };
