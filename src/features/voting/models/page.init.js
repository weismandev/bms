import {
  forward,
  sample,
  guard,
  attach,
  merge,
  split,
  createStoreObject,
} from 'effector';
import FileSaver from 'file-saver';
import { signout } from '../../common';
import { votingApi } from '../api';
import {
  changeStatusTo,
  $opened,
  voteForUserSubmitted,
  $selection,
  sendMessageSubmitted,
} from './detail.model';
import { bottomReached } from './list.model';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  $meta,
  $raw,
  fxArchive,
  fxCancel,
  fxClose,
  fxCloseToCounting,
  fxGetBlank,
  fxGetBlankForAllNoVoted,
  fxGetBlankForSelectedNoVoted,
  fxGetList,
  fxGetResultPDF,
  fxGetUserAswerPDF,
  fxGetVoteResults,
  fxGetVotersDetailList,
  fxNotify,
  fxPublish,
  fxVoteForUser,
  getBlankClicked,
  getBlanksClicked,
  getResultPDFClicked,
  getUserAswerClicked,
  pageMounted,
} from './page.model';

const $selectedPayload = createStoreObject({
  selection: $selection,
  opened: $opened,
});

const getBlanks = split(
  sample($selectedPayload, getBlanksClicked, ({ selection, opened }) => ({
    owners: selection.map((i) => JSON.parse(i)),
    vote_id: opened.id,
  })),
  {
    selected: ({ owners }) => owners.length > 0,
    all: ({ owners }) => owners.length === 0,
  }
);

fxGetVoteResults.use(votingApi.getVoteResults);
fxPublish.use(votingApi.publish);
fxCancel.use(votingApi.cancel);
fxCloseToCounting.use(votingApi.closeToCounting);
fxClose.use(votingApi.close);
fxArchive.use(votingApi.archive);
fxGetBlank.use(votingApi.getBlank);
fxGetResultPDF.use(votingApi.getResultPDF);
fxGetVotersDetailList.use(votingApi.getVotersDetailList);
fxGetUserAswerPDF.use(votingApi.getUserAswerPDF);
fxVoteForUser.use(votingApi.voteForUser);
fxGetBlankForAllNoVoted.use(votingApi.getBlankForAllNoVoted);
fxGetBlankForSelectedNoVoted.use(votingApi.getBlankForSelectedNoVoted);
fxNotify.use(votingApi.notify);

const relatedRequest = merge([
  fxGetVoteResults.pending,
  fxPublish.pending,
  fxCancel.pending,
  fxCloseToCounting.pending,
  fxClose.pending,
  fxArchive.pending,
  fxGetBlank.pending,
  fxGetResultPDF.pending,
  fxGetVotersDetailList.pending,
  fxGetUserAswerPDF.pending,
  fxVoteForUser.pending,
  fxGetBlankForAllNoVoted.pending,
  fxGetBlankForSelectedNoVoted.pending,
  fxNotify.pending,
]);

const relatedErrorOccured = merge([
  fxGetVoteResults.fail,
  fxPublish.fail,
  fxCancel.fail,
  fxCloseToCounting.fail,
  fxClose.fail,
  fxArchive.fail,
  fxGetBlank.fail,
  fxGetResultPDF.fail,
  fxGetVotersDetailList.fail,
  fxGetUserAswerPDF.fail,
  fxVoteForUser.fail,
  fxGetBlankForAllNoVoted.fail,
  fxGetBlankForSelectedNoVoted.fail,
  fxNotify.fail,
]);

const relatedRequestStarted = relatedRequest.filterMap((isLoading) =>
  isLoading ? isLoading : undefined
);

$isLoading.on(relatedRequest, (isLoading) => isLoading).reset(signout);

$error
  .on(relatedErrorOccured, (state, { error }) => error)
  .reset([relatedRequestStarted, signout]);

$isErrorDialogOpen.on(relatedErrorOccured, () => true).reset(signout);

$raw
  .on(
    [
      fxPublish.done,
      fxCancel.done,
      fxCloseToCounting.done,
      fxClose.done,
      fxArchive.done,
      fxVoteForUser.done,
    ],
    (state, { result }) => {
      const { data, meta } = state;
      const updatedIdx = data.findIndex((i) => String(i.id) === String(result.vote.id));

      if (updatedIdx > -1) {
        return {
          meta,
          data: data
            .slice(0, updatedIdx)
            .concat(result.vote)
            .concat(data.slice(updatedIdx + 1)),
        };
      }

      return state;
    }
  )
  .reset(signout);

forward({
  from: pageMounted,
  to: fxGetList,
});

forward({
  from: sample($opened, changeStatusTo.published, (opened) => opened.id),
  to: fxPublish,
});

forward({
  from: voteForUserSubmitted,
  to: attach({
    effect: fxVoteForUser,
    mapParams: ({ answers, ...rest }) => ({
      ...rest,
      answers: answers.map((a) => ({
        variant_id: a.id,
        question_id: a.question_id,
      })),
    }),
  }),
});

forward({
  from: sample($opened, changeStatusTo.cancelled, (opened, { payload } = {}) => ({
    vote_id: opened.id,
    ...payload,
  })),
  to: fxCancel,
});

forward({
  from: sample($opened, changeStatusTo.closed_counting, (opened) => opened.id),
  to: fxCloseToCounting,
});

forward({
  from: sample($opened, changeStatusTo.closed, (opened) => opened.id),
  to: fxClose,
});

forward({
  from: sample($opened, changeStatusTo.archived, (opened) => opened.id),
  to: fxArchive,
});

forward({
  from: sample($opened, getBlankClicked, (opened) => opened.id),
  to: fxGetBlank,
});

forward({
  from: sample($opened, getResultPDFClicked, (opened) => opened.id),
  to: fxGetResultPDF,
});

forward({
  from: sample($opened, getUserAswerClicked, (opened, user_id) => ({
    vote_id: opened.id,
    user_id,
  })),
  to: fxGetUserAswerPDF,
});

forward({
  from: getBlanks.all,
  to: fxGetBlankForAllNoVoted,
});

forward({
  from: getBlanks.selected,
  to: fxGetBlankForSelectedNoVoted,
});

guard({
  source: sample($meta, bottomReached),
  filter: ({ current_page, last_page }) => current_page < last_page,
  target: attach({
    effect: fxGetList,
    mapParams: ({ per_page, current_page }) => ({
      per_page: per_page * (Number(current_page) + 1),
    }),
  }),
});

guard({
  source: sample(
    $selectedPayload,
    sendMessageSubmitted,
    ({ selection, opened }, { message }) => ({
      message,
      vote_id: opened.id,
      owners: selection.map((i) => JSON.parse(i)),
    })
  ),
  filter: ({ owners }) => owners.length > 0,
  target: fxNotify,
});

fxGetBlank.done.watch(({ result }) => {
  const blob = new Blob([result]);
  FileSaver.saveAs(URL.createObjectURL(blob), `blank.pdf`);
});

fxGetResultPDF.done.watch(({ result }) => {
  const blob = new Blob([result]);
  FileSaver.saveAs(URL.createObjectURL(blob), `results.pdf`);
});

fxGetUserAswerPDF.done.watch(({ result }) => {
  const blob = new Blob([result]);
  FileSaver.saveAs(URL.createObjectURL(blob), `user_answer.pdf`);
});

fxGetBlankForAllNoVoted.done.watch(({ result }) => {
  const blob = new Blob([result]);
  FileSaver.saveAs(URL.createObjectURL(blob), `all_no_voted_blanks.zip`);
});

fxGetBlankForSelectedNoVoted.done.watch(({ result }) => {
  const blob = new Blob([result]);
  FileSaver.saveAs(URL.createObjectURL(blob), `selected_no_voted_blanks.zip`);
});
