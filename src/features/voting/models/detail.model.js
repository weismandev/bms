import { createStore, createEvent, split, restore } from 'effector';
import { createGate } from 'effector-react';
import SettingsIcon from '@mui/icons-material/Settings';
import i18n from '@shared/config/i18n';
import { createDetailBag, createTableBag } from '../../../tools/factories';

const { t } = i18n;

export const FormErrorsGate = createGate();

export const changeStatus = createEvent();
export const changeTab = createEvent();
export const selectionChanged = createEvent();
export const changeCancelDialogVisibility = createEvent();
export const changeVoteDialog = createEvent();
export const voteForUserSubmitted = createEvent();
export const changeMessageDialog = createEvent();
export const sendMessageSubmitted = createEvent();
export const cancelVotingCreate = createEvent();

export const changeStatusTo = split(changeStatus, {
  published: ({ status }) => status === 'published',
  cancelled: ({ status }) => status === 'cancelled',
  closed_counting: ({ status }) => status === 'closed_counting',
  closed: ({ status }) => status === 'closed',
  archived: ({ status }) => status === 'archived',
});

export const $voteResults = createStore({});
export const $currentTab = createStore('info');
export const $tableData = createStore([]);
export const $selection = createStore([]);
const $formErrors = FormErrorsGate.state;

export const $isCancelDialogOpen = createStore(false);
export const $voteDialog = createStore({ isOpen: false });
export const $messageDialog = createStore({ isOpen: false });

export const newEntity = {
  title: '',
  description: '',
  detailed_information: '',
  questions: [],
  status: '',
  start_date: '',
  end_date: '',
  buildings: '',
  created_at: '',
  updated_at: '',
  author: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const $wasNotDetailOpen = createStore(true);
export const $lastOpened = restore(openViaUrl, null);

const hasInfoErrors = (errors) =>
  errors && (errors.title || errors.description || errors.detailed_information);
const hasQuestionsErrors = (errors) => errors && errors.questions;

export const $hasErrorsOnInfoTab = $formErrors.map(({ errors }) =>
  Boolean(hasInfoErrors(errors))
);

export const $hasErrorsOnQuestionsTab = $formErrors.map(({ errors }) =>
  Boolean(hasQuestionsErrors(errors))
);

export const columns = [
  { name: 'user_name', title: t('Label.FullName') },
  {
    name: 'voice_weight',
    title: t('VoiceWeight'),
    getCellValue: (cell) => cell.voice_weight + '%',
  },
  { name: 'is_voted', title: t('VoiceStatus') },
  {
    name: 'app_status',
    title: t('ApplicationStatus'),
    getCellValue: ({ is_logged_in, is_online, last_use }) =>
      JSON.stringify({
        is_logged_in,
        is_online,
        last_use,
      }),
  },
  {
    name: 'vote',
    title: <SettingsIcon />,
    getCellValue: (cell) => cell.is_voted,
  },
];

export const excludedColumnsFromSort = ['vote'];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: [
    { columnName: 'user_name', width: 300 },
    { columnName: 'app_status', width: 250 },
    { columnName: 'vote', width: 100 },
  ],
});
