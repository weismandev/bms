import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import i18n from '@shared/config/i18n';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  Loader,
  NoDataPage,
  TwoColumnLayout,
} from '../../../ui';

import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { Detail, List } from '../organisms';
import '../models';

import {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  $path,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models/page.model';

import { $isListOpen } from '../models/list.model';
import { $isDetailOpen, changedDetailVisibility } from '../models/detail.model';

const { t } = i18n;

const VotingPage = () => {
  const isListOpen = useStore($isListOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);

  const list = isListOpen ? <List /> : null;
  const detail = isDetailOpen ? <Detail /> : null;
  const hasContentToShow = list || detail;
  const waitForContent = !hasContentToShow && isLoading;

  const content = hasContentToShow ? (
    <TwoColumnLayout left={list} right={detail} />
  ) : waitForContent ? (
    <Loader isLoading />
  ) : (
    <NoDataPage
      onAdd={() => changedDetailVisibility(true)}
      text={t('ThereIsNoMeeting')}
    />
  );

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />
      <ColoredTextIconNotification />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />

      {content}
    </>
  );
};

const RestrictedVotingPage = () => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <VotingPage />
    </HaveSectionAccess>
  );
};

export { RestrictedVotingPage as VotingPage };
