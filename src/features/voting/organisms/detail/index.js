import { useStore } from 'effector-react';
import { Formik, Form, Field, FastField, FieldArray } from 'formik';
import * as Yup from 'yup';
import {
  Typography,
  IconButton,
  Button,
  Divider as MuiDivider,
  Tab,
  Grid,
  useMediaQuery,
  RadioGroup,
  Radio,
  FormControlLabel,
  Tooltip,
} from '@mui/material';
import { green, blue, red } from '@mui/material/colors';
import {
  Close,
  SaveOutlined as Save,
  InfoOutlined,
  HelpOutlineOutlined,
  HowToRegOutlined,
  CheckCircle,
  DoneAll,
  Error as ErrorIcon,
  ErrorOutlineOutlined,
  DescriptionOutlined,
  MailOutline,
  VerticalAlignBottom,
} from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { getRandomColors } from '../../../../tools/getRandomColors';
import {
  Toolbar,
  Wrapper,
  DeleteButton,
  ActionButton,
  ActionIconButton,
  Greedy,
  InputField,
  SelectField,
  Tabs,
  BaseInputErrorText,
  Modal,
  VoteProgress,
  CustomScrollbar,
} from '../../../../ui';
import { HouseSelectField } from '../../../house-select';
import { VotingStatusesSelectField } from '../../../voting-statuses-select';
import {
  $opened,
  $mode,
  detailSubmitted,
  $voteResults,
  changeStatus,
  changeTab,
  $currentTab,
  changeCancelDialogVisibility,
  $isCancelDialogOpen,
  $voteDialog,
  changeVoteDialog,
  voteForUserSubmitted,
  $selection,
  changeMessageDialog,
  sendMessageSubmitted,
  $messageDialog,
  FormErrorsGate,
  $hasErrorsOnInfoTab,
  $hasErrorsOnQuestionsTab,
  cancelVotingCreate,
} from '../../models/detail.model';
import {
  deleteClicked,
  getResultPDFClicked,
  getBlanksClicked,
} from '../../models/page.model';
import { Table } from '../table';

const { t } = i18n;

const useStyles = makeStyles({
  buttonMargin: {
    marginRight: 10,
  },
  iconMargin: {
    marginRight: 5,
  },
  questionCaption: {
    fontSize: 14,
    lineHeight: '16px',
    color: '#767676',
  },
  scrollbarView: {
    padding: 24,
  },
  nestedScrollbarView: {
    paddingRight: 24,
  },
  header: {
    color: '#3B3B50',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: '28px',
  },
  description: {
    color: '#65657B',
    fontWeight: 'normal',
    fontSize: 18,
    marginTop: '24px',
  },

  viewSection: {
    marginBottom: 20,
  },

  viewSectionLabel: {
    color: '#65657B',
    fontSize: 14,
    fontWeight: 500,
  },

  viewSectionContent: {
    color: '#3B3B50',
    fontSize: 18,
  },
});

const DetailToolbar = (props) => {
  const { mode, status } = props;

  const { buttonMargin, iconMargin, statusBtnContainer } = useStyles();

  const selection = useStore($selection);
  const isEditMode = mode === 'edit';
  const hasSelectedOwners = selection.length > 0;

  const editModeToolbar = (
    <>
      <Greedy />
      <ActionButton className={buttonMargin} kind="positive" type="submit">
        <Save className={iconMargin} />
        {t('Save')}
      </ActionButton>
      <ActionButton
        onClick={() => cancelVotingCreate()}
        className={buttonMargin}
        kind="negative"
        type="reset"
      >
        <Close className={iconMargin} />
        {t('Cancellation')}
      </ActionButton>
    </>
  );

  const screenLess1650 = useMediaQuery('(max-width:1650px)');

  const draftStatusButtons = (
    <>
      <DeleteButton onClick={deleteClicked} className={buttonMargin} />
      <ActionButton onClick={() => changeStatus({ status: 'published' })} kind="positive">
        {t('Publish')}
      </ActionButton>
    </>
  );

  const publishedStatusButtons = (
    <>
      {hasSelectedOwners && (
        <Tooltip title={t('SendMessageToSelected')}>
          <ActionIconButton
            onClick={() => changeMessageDialog({ isOpen: true })}
            className={buttonMargin}
          >
            <MailOutline />
          </ActionIconButton>
        </Tooltip>
      )}
      <Tooltip
        title={
          hasSelectedOwners
            ? t('DownloadFormsForSelected')
            : t('DownloadFormsForNonVoters')
        }
      >
        <ActionIconButton className={buttonMargin} onClick={() => getBlanksClicked()}>
          <DescriptionOutlined />
        </ActionIconButton>
      </Tooltip>

      {screenLess1650 ? (
        <Tooltip title={t('CancelMeeting')}>
          <ActionIconButton
            onClick={() => changeCancelDialogVisibility(true)}
            kind="negative"
            className={buttonMargin}
          >
            <ErrorOutlineOutlined />
          </ActionIconButton>
        </Tooltip>
      ) : (
        <ActionButton
          className={buttonMargin}
          onClick={() => changeCancelDialogVisibility(true)}
          kind="negative"
        >
          <ErrorOutlineOutlined className={iconMargin} />
          {t('CancelMeeting')}
        </ActionButton>
      )}

      <ActionButton
        onClick={() => changeStatus({ status: 'closed_counting' })}
        kind="positive"
      >
        {t('CountVotes')}
      </ActionButton>
    </>
  );

  const closedCountingStatusButtons = (
    <ActionButton onClick={() => changeStatus({ status: 'closed' })}>
      <DoneAll className={iconMargin} />
      {t('FinishCounting')}
    </ActionButton>
  );

  const closedStatusButtons = (
    <ActionButton onClick={() => getResultPDFClicked()}>
      <VerticalAlignBottom className={iconMargin} />
      {t('DownloadMeetingDocuments')}
    </ActionButton>
  );

  const mapStatusToToolbarButtons = {
    draft: draftStatusButtons,
    published: publishedStatusButtons,
    closed_counting: closedCountingStatusButtons,
    closed: closedStatusButtons,
  };

  const viewModeToolbar = (
    <div className={statusBtnContainer}>
      {mapStatusToToolbarButtons[status.slug] || null}
    </div>
  );

  const currentTab = useStore($currentTab);
  const hasErrorsOnInfoTab = useStore($hasErrorsOnInfoTab);
  const hasErrorsOnQuestionsTab = useStore($hasErrorsOnQuestionsTab);

  const screenLess1480 = useMediaQuery('(max-width:1480px)');

  const tabsList = [
    {
      value: 'info',
      disabled: currentTab !== 'info' && hasErrorsOnQuestionsTab,
      label: screenLess1480 ? <InfoOutlined /> : t('information'),
    },
    {
      value: 'questions',
      disabled: currentTab !== 'questions' && hasErrorsOnInfoTab,
      label: screenLess1480 ? <HelpOutlineOutlined /> : t('Agenda'),
    },
    {
      value: 'members',
      onCreateIsDisabled: true,
      disabled: hasErrorsOnQuestionsTab || hasErrorsOnInfoTab,
      label: screenLess1480 ? <HowToRegOutlined /> : t('Members'),
    },
  ];

  return (
    <Toolbar style={{ padding: '24px' }}>
      <Tabs
        tabEl={<Tab style={{ minWidth: 20 }} />}
        currentTab={currentTab}
        options={tabsList}
        onChange={(e, value) => changeTab(value)}
        isNew={isEditMode}
      />
      <Greedy />
      {isEditMode ? editModeToolbar : viewModeToolbar}
    </Toolbar>
  );
};

const CancelDialog = (props) => {
  const { isCancelDialogOpen } = props;
  return (
    <Modal
      isOpen={isCancelDialogOpen}
      header={
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <span>{t('CancellationOfAMeeting')}</span>
          <IconButton onClick={() => changeCancelDialogVisibility(false)} size="large">
            <Close />
          </IconButton>
        </div>
      }
      content={
        <Formik
          initialValues={{ reason: '' }}
          onSubmit={(values) => changeStatus({ status: 'cancelled', payload: values })}
          render={() => (
            <Form>
              <Field
                style={{ height: 120 }}
                name="reason"
                component={InputField}
                divider={false}
                required
                label={t('ReasonForCancellation')}
                placeholder={t('TheMeetingHasAlreadyBeenHeld')}
                rowsMax={5}
                multiline
              />
              <Toolbar
                style={{
                  height: 80,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <ActionButton
                  style={{ padding: '0 20px', marginRight: 10 }}
                  type="submit"
                >
                  {t('cancel')}
                </ActionButton>
              </Toolbar>
            </Form>
          )}
        />
      }
    />
  );
};

const MessageDialog = (props) => {
  const { isMessageDialogOpen } = props;
  return (
    <Modal
      isOpen={isMessageDialogOpen}
      header={
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <span>{t('SendAMessage')}</span>
          <IconButton onClick={() => changeMessageDialog({ isOpen: false })} size="large">
            <Close />
          </IconButton>
        </div>
      }
      content={
        <Formik
          initialValues={{ message: '' }}
          onSubmit={sendMessageSubmitted}
          render={() => (
            <Form>
              <Field
                style={{ height: 120 }}
                name="message"
                component={InputField}
                divider={false}
                required
                label={t('Message')}
                placeholder={t('EnterMessage')}
                rowsMax={5}
                multiline
              />
              <Toolbar
                style={{
                  height: 80,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <ActionButton
                  style={{ padding: '0 20px', marginRight: 10 }}
                  type="submit"
                >
                  {t('Send')}
                </ActionButton>
              </Toolbar>
            </Form>
          )}
        />
      }
    />
  );
};

const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  description: Yup.string().required(t('thisIsRequiredField')),
  detailed_information: Yup.string().required(t('thisIsRequiredField')),
  questions: Yup.array()
    .of(
      Yup.object({
        proposal: Yup.string().required(t('thisIsRequiredField')),
        question: Yup.string().required(t('thisIsRequiredField')),
      })
    )
    .min(1, t('AtLeast1QuestionIsRequired')),
  start_date: Yup.string().required(t('thisIsRequiredField')),
  end_date: Yup.string().required(t('thisIsRequiredField')),
  buildings: Yup.mixed().required(t('thisIsRequiredField')),
});

const Detail = () => {
  const { scrollbarView } = useStyles();
  const opened = useStore($opened);
  const mode = useStore($mode);
  const isNew = !opened.id;
  const isEditMode = mode === 'edit';
  const currentTab = useStore($currentTab);
  const isCancelDialogOpen = useStore($isCancelDialogOpen);
  const isMessageDialogOpen = useStore($messageDialog).isOpen;
  const voteDialog = useStore($voteDialog);

  return (
    <Wrapper style={{ height: '89vh' }}>
      <CancelDialog isCancelDialogOpen={isCancelDialogOpen} />
      <MessageDialog isMessageDialogOpen={isMessageDialogOpen} />
      <VoteDialog opened={opened} {...voteDialog} />
      <Formik
        onSubmit={detailSubmitted}
        initialValues={opened}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, errors }) => (
          <Form
            style={{
              display: 'grid',
              gridTemplateColumns: 'auto 20px 260px',
              gridTemplateRows: '1fr',
            }}
          >
            <FormErrorsGate errors={errors} />
            <div>
              <DetailToolbar mode={mode} status={opened.status} />
              <div style={{ height: 'calc(89vh - 110px)' }}>
                <CustomScrollbar
                  autoHide
                  renderView={(props) => <div {...props} className={scrollbarView} />}
                >
                  {isEditMode ? (
                    <EditQuestionContent
                      currentTab={currentTab}
                      values={values}
                      errors={errors}
                    />
                  ) : (
                    <ViewQuestionContent currentTab={currentTab} opened={opened} />
                  )}
                </CustomScrollbar>
              </div>
            </div>
            <div style={{ paddingTop: 24 }}>
              <MuiDivider orientation="vertical" />
            </div>
            <div>
              <CustomScrollbar
                autoHide
                renderView={(props) => <div {...props} className={scrollbarView} />}
              >
                {!isNew && (
                  <Field
                    component={VotingStatusesSelectField}
                    name="status"
                    label={t('Label.status')}
                    readOnly
                    placeholder={t('selectStatuse')}
                  />
                )}
                <Field
                  required
                  component={InputField}
                  name="start_date"
                  type="date"
                  mode={mode}
                  label={t('VotingStart')}
                />
                <Field
                  required
                  component={InputField}
                  name="end_date"
                  type="date"
                  mode={mode}
                  label={t('EndOfVoting')}
                />
                <Field
                  required
                  component={HouseSelectField}
                  name="buildings"
                  label={t('objects')}
                  mode={mode}
                  isMulti
                  placeholder={t('selectObjects')}
                />

                {!isNew && (
                  <>
                    <Field
                      component={InputField}
                      name="created_at"
                      label={t('dateTimeCreation')}
                      type="datetime"
                      readOnly
                    />
                    <Field
                      component={InputField}
                      name="updated_at"
                      label={t('LastModified')}
                      type="datetime"
                      readOnly
                    />
                    <Field
                      component={SelectField}
                      name="author"
                      label={t('Author')}
                      getOptionLabel={(opt) =>
                        opt && opt.name ? opt.name : t('Unknown')
                      }
                      readOnly
                    />
                  </>
                )}
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

const VoteDialog = (props) => {
  const { isOpen, user_name = '', user_id, opened } = props;
  const { questions } = opened;
  const { nestedScrollbarView } = useStyles();

  return (
    <Modal
      isOpen={isOpen}
      header={
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
          }}
        >
          <span>{`${t('VoteFor')} ${user_name}`}</span>
          <IconButton onClick={() => changeVoteDialog({ isOpen: false })} size="large">
            <Close />
          </IconButton>
        </div>
      }
      content={
        <Formik
          initialValues={{ answers: [] }}
          onSubmit={(values) =>
            voteForUserSubmitted({ ...values, user_id, vote_id: opened.id })
          }
          render={({ values, setFieldValue }) => (
            <Form style={{ height: '80vh' }}>
              <div style={{ height: 'calc(100% - 100px)' }}>
                <CustomScrollbar
                  renderView={(props) => (
                    <div {...props} className={nestedScrollbarView} />
                  )}
                >
                  {questions.map((i, idx, array) => {
                    const answers = i.variants.map((v) => v.title);
                    const isLast = idx === array.length - 1;
                    return (
                      <QuestionBlock
                        question={i.question}
                        key={i.id}
                        answers={answers}
                        proposal={i.proposal}
                        isLast={isLast}
                        num={idx + 1}
                        renderAnswers={(colors) => (
                          <Grid container direction="column">
                            {i.variants.map((variant, index) => {
                              const name = `answers[${idx}]`;
                              const value = values.answers[idx] || '';

                              return (
                                <Grid key={variant.id} item>
                                  <RadioGroup
                                    name={name}
                                    value={value}
                                    onChange={(e, value) => {
                                      setFieldValue(name, {
                                        ...variant,
                                        question_id: i.id,
                                      });
                                    }}
                                  >
                                    <FormControlLabel
                                      value={variant}
                                      checked={
                                        Boolean(value) &&
                                        String(value.id) === String(variant.id)
                                      }
                                      control={<Radio />}
                                      label={variant.title}
                                    />
                                  </RadioGroup>
                                </Grid>
                              );
                            })}
                          </Grid>
                        )}
                      />
                    );
                  })}
                </CustomScrollbar>
              </div>
              <Toolbar
                style={{
                  marginTop: 24,
                  height: 80,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <ActionButton
                  style={{ padding: '0 20px', marginRight: 10 }}
                  type="submit"
                >
                  {t('Send')}
                </ActionButton>
              </Toolbar>
            </Form>
          )}
        />
      }
    />
  );
};

const EditQuestionContent = (props) => {
  const newQuestion = { question: '', type: '', variants: '', proposal: '' };
  const { values, errors, currentTab } = props;
  return currentTab === 'info' ? (
    <>
      <Field
        required
        component={InputField}
        name="title"
        label={t('title')}
        placeholder={t('EnterTitle')}
      />
      <Field
        required
        component={InputField}
        multiline
        rowsMax={10}
        name="description"
        placeholder={t('EnterDescription')}
        label={t('Text')}
      />
      <Field
        required
        component={InputField}
        multiline
        rowsMax={10}
        name="detailed_information"
        placeholder={t('EnterAdditionalExplanations')}
        label={t('AdditionalInformation')}
      />
    </>
  ) : currentTab === 'questions' ? (
    <FieldArray
      name="questions"
      render={({ push, remove }) => (
        <>
          <QuestionsList remove={remove} questions={values.questions} />
          <Button onClick={() => push(newQuestion)} color="primary">
            {`+ ${t('AddAQuestion')}`}
          </Button>
          {typeof errors.questions === 'string' && (
            <BaseInputErrorText style={{ color: 'red' }} errors={[errors.questions]} />
          )}
        </>
      )}
    />
  ) : null;
};

const ViewQuestionContent = (props) => {
  const classes = useStyles();

  const voteResults = useStore($voteResults);
  const questions = voteResults.questions || [];

  const { opened, currentTab } = props;
  const { title, description, detailed_information } = opened;

  const answers = Array.from(
    questions.reduce((acc, i) => {
      i.result.forEach((j) => acc.add(j.answer));
      return acc;
    }, new Set())
  );

  return currentTab === 'info' ? (
    <>
      {opened.status.slug === 'closed_counting' && (
        <StatusBlock
          header={t('TheMeetingIsOverVoteCounting')}
          text={t('FillInTheInformation')}
          Icon={CheckCircle}
          color={blue}
        />
      )}
      {opened.status.slug === 'closed' && (
        <StatusBlock
          header={t('TheMeetingIsOver')}
          text={t('SaveMeetingMinutes')}
          Icon={DoneAll}
          color={blue}
        />
      )}
      {opened.status.slug === 'cancelled' && (
        <StatusBlock
          header={t('MeetingCanceled')}
          text={
            opened.cancel_reason ? (
              <span>
                <b>{`${t('Label.comment')}: `}</b>
                {opened.cancel_reason}
              </span>
            ) : null
          }
          Icon={ErrorIcon}
          color={red}
        />
      )}
      <Typography component="h3" classes={{ root: classes.header }}>
        {title}
      </Typography>
      <Typography component="p" classes={{ root: classes.description }}>
        {description}
      </Typography>
      <Typography component="p" classes={{ root: classes.description }}>
        {detailed_information}
      </Typography>
    </>
  ) : currentTab === 'questions' ? (
    questions.map((i, idx, array) => (
      <QuestionBlock
        key={i.id}
        {...i}
        isLast={idx === array.length - 1}
        answers={answers}
        num={idx + 1}
        renderAnswers={(colors) => <VoteProgressList list={i.result} colors={colors} />}
      />
    ))
  ) : (
    <Table />
  );
};

const QuestionBlock = (props) => {
  const { question, answers, proposal, isLast, num, renderAnswers } = props;
  const { viewSection, viewSectionLabel } = useStyles();

  const predefinedColors = {
    [t('Agree')]: green,
    [t('Disagree')]: red,
    [t('Abstained')]: blue,
  };

  const colors = getRandomColors(answers, predefinedColors);

  return (
    <div
      style={{
        padding: 24,
        paddingBottom: 11,
        background: '#EDF6FF',
        borderRadius: 16,
        marginBottom: isLast ? 0 : 24,
      }}
    >
      <Typography
        component="h6"
        style={{
          color: '#3B3B50',
          marginBottom: 15,
          fontWeight: 500,
          fontSize: 24,
          lineHeight: '28px',
        }}
      >
        {`${t('Question')} ${num}`}
      </Typography>

      <QuestionInfoBlock header={t('Essence of the question')} text={question} />
      {proposal && <QuestionInfoBlock header={t('Sentence')} text={proposal} />}

      <div className={viewSection}>
        <Typography classes={{ root: viewSectionLabel }} component="p">
          {t('AnswerOptions')}
        </Typography>
        {renderAnswers ? renderAnswers(colors) : null}
      </div>
    </div>
  );
};

const QuestionInfoBlock = (props) => {
  const { header, text } = props;
  const { viewSection, viewSectionLabel, viewSectionContent } = useStyles();

  return (
    <div className={viewSection}>
      <Typography classes={{ root: viewSectionLabel }} component="h6">
        {header}
      </Typography>
      <Typography classes={{ root: viewSectionContent }} component="p">
        {text}
      </Typography>
    </div>
  );
};

const StatusBlock = (props) => {
  const { header, text, Icon, color } = props;
  return (
    <div style={{ background: color[50], padding: 24, marginBottom: 24 }}>
      <Grid container spacing={2} wrap="nowrap">
        <Grid item>
          <Icon style={{ color: color[500], width: 48, height: 48 }} />
        </Grid>
        <Grid item>
          <Typography
            component="h6"
            style={{
              color: color[500],
              fontSize: 18,
              lineHeight: '21px',
              fontWeight: 'bold',
            }}
          >
            {header}
          </Typography>
          {text ? (
            <Typography style={{ color: '#65657B', fontSize: 14 }}>{text}</Typography>
          ) : null}
        </Grid>
      </Grid>
    </div>
  );
};

const VoteProgressList = (props) => {
  const { list, colors } = props;

  return (
    <Grid container spacing={2}>
      {list.map((item) => (
        <Grid key={item.answer} item style={{ minWidth: 200, flexGrow: 2 }}>
          <VoteProgress
            title={item.answer}
            progress={item.percent}
            color={colors[item.answer]}
          />
        </Grid>
      ))}
    </Grid>
  );
};

function QuestionsList(props) {
  const { questions = [], remove } = props;

  return questions.map((question, idx) => (
    <Question key={idx} remove={() => remove(idx)} name={`questions[${idx}]`} idx={idx} />
  ));
}

function Question(props) {
  const { name, remove, idx } = props;
  return (
    <Grid
      container
      direction="column"
      style={{
        position: 'relative',
        padding: 24,
        background: '#EDF6FF',
        borderRadius: 16,
        marginBottom: 24,
      }}
    >
      <Grid item>
        <Typography
          component="h6"
          style={{
            color: '#3B3B50',
            marginBottom: 15,
            fontWeight: 500,
            fontSize: 24,
            lineHeight: '28px',
          }}
        >
          {`${t('Question')} ${idx + 1}`}
        </Typography>
      </Grid>
      <Grid item>
        <Grid container spacing={2}>
          <Grid item md={12} lg={6}>
            <FastField
              component={InputField}
              name={`${name}.question`}
              label={t('EssenceOfTheQuestion')}
              multiline
              required
              rowsMax={10}
              placeholder={t('EnterQuestionText')}
              divider={false}
            />
          </Grid>
          <Grid item md={12} lg={6}>
            <FastField
              component={InputField}
              name={`${name}.proposal`}
              multiline
              required
              rowsMax={10}
              label={t('Sentence')}
              placeholder={t('EnterSentence')}
              divider={false}
            />
          </Grid>
          <IconButton
            onClick={remove}
            style={{
              width: 40,
              height: 40,
              position: 'absolute',
              top: 5,
              right: 5,
            }}
            size="small"
          >
            <Close />
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  );
}

export { Detail };
