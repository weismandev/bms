import { useMemo, memo } from 'react';
import { useStore } from 'effector-react';
import { SortingLabel, TableContainer, TableRow, TableCell, PagingPanel } from '@ui';
import { IconButton, Tooltip } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import {
  SortingState,
  SelectionState,
  IntegratedSelection,
  DataTypeProvider,
  PagingState,
  IntegratedPaging,
  IntegratedSorting,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableSelection,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  $tableData,
  columns,
  $columnOrder,
  $columnWidths,
  columnOrderChanged,
  columnWidthsChanged,
  $sorting,
  sortChanged,
  excludedColumnsFromSort,
  $selection,
  selectionChanged,
  $currentPage,
  $pageSize,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $opened,
  changeVoteDialog,
} from '../../models/detail.model';
import { getUserAswerClicked } from '../../models/page.model';

const { t } = i18n;

const Root = (props) => <Grid.Root {...props} style={{ height: '100%' }} />;

const VoiceStatusFormatter = ({ value, row }) => {
  return (
    <span style={{ color: value ? '#38BB7C' : 'inherit' }}>
      {value ? t('Voted') : t('DidNotVote')}
    </span>
  );
};

const VoiceStatusTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={VoiceStatusFormatter} {...props} />
);

const AppStatusFormatter = ({ value, row }) => {
  const { is_logged_in, is_online, last_use } = JSON.parse(value);
  const color = is_logged_in ? (is_online ? '#1D90FB' : 'inherit') : '#EF372B';

  return (
    <span style={{ color }}>
      {is_logged_in ? (is_online ? t('online') : last_use) : t('notInstalled')}
    </span>
  );
};

const AppStatusTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={AppStatusFormatter} {...props} />
);

const VoteFormatter = ({ value, row }) => {
  const opened = useStore($opened);

  if (opened && opened.status.slug === 'closed' && row.is_voted) {
    return (
      <Tooltip title={t('DownloadTheVotingResultForm')} placement="top">
        <IconButton size="small" onClick={(e) => getUserAswerClicked(row.user_id)}>
          <AssignmentOutlinedIcon />
        </IconButton>
      </Tooltip>
    );
  }

  return !value && opened && opened.status.slug === 'closed_counting' ? (
    <Tooltip title={t('VoteForTheOwner')} placement="top">
      <IconButton
        size="small"
        onClick={(e) => {
          e.stopPropagation();
          changeVoteDialog({ ...row, isOpen: true });
        }}
      >
        <AddIcon />
      </IconButton>
    </Tooltip>
  ) : (
    ''
  );
};

const VoteTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={VoteFormatter} {...props} />
);

const comparePercent = (a, b) => b.slice(0, -1) - a.slice(0, -1);

const Row = (props) => {
  const selection = useStore($selection);
  const isSelected = selection.includes(
    JSON.stringify({
      user_id: props.row.user_id,
      apartment_id: props.row.apartment_id,
    })
  );
  const onRowClick = () => null;

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

export const Table = memo(() => {
  const data = useStore($tableData);
  const columnOrder = useStore($columnOrder);
  const columnWidths = useStore($columnWidths);
  const sorting = useStore($sorting);
  const selection = useStore($selection);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);

  return (
    <Grid
      rootComponent={Root}
      rows={data}
      getRowId={(row) =>
        JSON.stringify({ user_id: row.user_id, apartment_id: row.apartment_id })
      }
      columns={columns}
    >
      <SelectionState selection={selection} onSelectionChange={selectionChanged} />

      <SortingState sorting={sorting} onSortingChange={sortChanged} />

      <DragDropProvider />
      <IntegratedSelection />
      <IntegratedSorting
        columnExtensions={[{ columnName: 'voice_weight', compare: comparePercent }]}
      />

      <VoiceStatusTypeProvider for={['is_voted']} />
      <AppStatusTypeProvider for={['app_status']} />
      <VoteTypeProvider for={['vote']} />

      <PagingState
        currentPage={currentPage}
        onCurrentPageChange={pageNumberChanged}
        pageSize={pageSize}
        onPageSizeChange={pageSizeChanged}
      />
      <IntegratedPaging />

      <DXTable
        columnExtensions={[{ columnName: 'vote', align: 'center' }]}
        messages={{ noData: t('noData') }}
        rowComponent={Row}
        cellComponent={TableCell}
        containerComponent={TableContainer}
      />
      <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
      <TableColumnResizing
        columnWidths={columnWidths}
        onColumnWidthsChange={columnWidthsChanged}
      />
      <TableHeaderRow
        showSortingControls
        sortLabelComponent={(props) => (
          <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
        )}
      />
      <TableSelection showSelectAll />
      <PagingPanel />
    </Grid>
  );
});
