import { useRef } from 'react';
import { useStore, useList } from 'effector-react';
import classNames from 'classnames';
import { List as MuiList, ListItem, Typography } from '@mui/material';
import {
  VisibilityOutlined,
  VisibilityOffOutlined,
  Equalizer,
  DoneOutlined,
  DoneAllOutlined,
  ErrorOutlineOutlined,
  Archive,
} from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, Toolbar, Divider, AddButton, CustomScrollbar } from '../../../../ui';
import { openViaUrl, $opened, addClicked, $mode } from '../../models/detail.model';
import { $listData, bottomReached } from '../../models/list.model';

const useStyles = makeStyles((theme) => {
  return {
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'space-between',
      height: 120,
      padding: '24px 24px 15px 24px',
      '&:hover': {
        background: '#E1EBFF !important',
        "& + *[role='separator']": {
          background: '#E1EBFF !important',
        },
        "& *[class*='headerText']": {
          color: '#0394E3 !important',
        },
      },
    },
    selected: {
      background: '#E1EBFF !important',
      "& + *[role='separator']": {
        background: '#E1EBFF !important',
      },
      "& *[class*='headerText']": {
        color: '#0394E3 !important',
      },
    },
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },

    infoContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexWrap: 'nowrap',
      width: '100%',
    },

    statusIcon: {
      width: 14,
      height: 14,
      color: '#65657B',
      marginRight: 5,
    },

    statusText: {
      fontSize: 14,
      lineHeight: '16px',
      color: '#65657B',
    },

    resultText: {
      fontSize: 16,
      lineHeight: '19px',
      fontWeight: 'bold',
      margin: '0 4px',
    },

    resultIcon: {
      width: 18,
      height: 18,
      color: '#65657B',
      margin: '0 4px',
    },

    resultContainer: {
      display: 'flex',
      flexWrap: 'nowrap',
    },

    headerText: {
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: '100%',
    },

    dateText: {
      color: '#7D7D8E',
      fontSize: '16px',
      lineHeight: '19px',
      fontWeight: 300,
    },

    green: {
      color: '#1BB169',
    },
    red: {
      color: '#EB5757',
    },
    grey: {
      color: '#65657B',
    },
    blue: {
      color: '#0394E3',
    },
  };
});

const ItemStatus = (props) => {
  const { status } = props;
  const { statusIcon, statusText, green, red, blue, grey } = useStyles();

  const isPublished = status.slug === 'published';
  const isClosed = status.slug === 'closed';
  const isVoteCounting = status.slug === 'closed_counting';
  const isCancelled = status.slug === 'cancelled';
  const isDraft = status.slug === 'draft';
  const isArchived = status.slug === 'archived';

  const statusColorSchema = {
    [green]: isPublished,
    [red]: isCancelled,
    [blue]: isClosed || isVoteCounting,
    [grey]: isDraft || isArchived,
  };

  const iconClassName = classNames(statusIcon, statusColorSchema);
  const textClassName = classNames(statusText, statusColorSchema);

  const mapStatusToIcon = {
    published: <VisibilityOutlined className={iconClassName} />,
    draft: <VisibilityOffOutlined className={iconClassName} />,
    archived: <Archive className={iconClassName} />,
    cancelled: <ErrorOutlineOutlined className={iconClassName} />,
    closed_counting: <DoneOutlined className={iconClassName} />,
    closed: <DoneAllOutlined className={iconClassName} />,
  };

  const icon = mapStatusToIcon[status.slug] || null;

  return (
    <span>
      {icon} <span className={textClassName}>{status.title}</span>
    </span>
  );
};

const ItemResultInfo = (props) => {
  const { percent } = props;

  const { resultContainer, resultIcon, green, red, grey, resultText } = useStyles();

  const votedMoreHalf = percent >= 50;

  const resultColorSchema = {
    [grey]: percent === 0,
    [green]: votedMoreHalf,
    [red]: !votedMoreHalf && percent !== 0,
  };

  const iconClassName = classNames(resultIcon, resultColorSchema);
  const textClassName = classNames(resultText, resultColorSchema);

  return percent || percent === 0 ? (
    <div className={resultContainer}>
      <Equalizer className={iconClassName} />
      <span className={textClassName}>{percent}%</span>
    </div>
  ) : null;
};

const ItemDateInfo = (props) => {
  const { start_date, end_date } = props;
  const { dateText } = useStyles();
  return (
    <Typography classes={{ root: dateText }}>
      {start_date} - {end_date}
    </Typography>
  );
};

const ItemInfo = (props) => {
  const { status, percent } = props;
  const { infoContainer } = useStyles();

  if (!status && !percent) {
    return null;
  }

  return (
    <div className={infoContainer}>
      <ItemStatus status={status} />
      <ItemResultInfo percent={percent} />
    </div>
  );
};

const ItemHeader = (props) => {
  const { header } = props;
  const { headerText } = useStyles();

  return <Typography className={headerText}>{header}</Typography>;
};

const Item = (props) => {
  const { children, ...rest } = props;
  const { item, divider, selected } = useStyles();
  return (
    <>
      <ListItem classes={{ root: item, selected }} button component="li" {...rest}>
        {children}
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const List = (props) => {
  const opened = useStore($opened);
  const mode = useStore($mode);

  const isEditMode = mode === 'edit';
  const scrollbarRef = useRef(null);

  const list = useList($listData, {
    keys: [opened],
    fn: ({ id, status, percent, header, start_date, end_date }) => {
      const selectItem = () => openViaUrl(id);

      return (
        <Item selected={String(opened.id) === String(id)} onClick={selectItem}>
          <ItemInfo status={status} percent={percent} />
          <ItemHeader header={header} />
          <ItemDateInfo start_date={start_date} end_date={end_date} />
        </Item>
      );
    },
  });

  return (
    <Wrapper style={{ height: '89vh' }}>
      <Toolbar style={{ height: 82, padding: '0 24px' }}>
        <AddButton disabled={isEditMode} onClick={addClicked} />
      </Toolbar>
      <CustomScrollbar
        style={{ height: 'calc(89vh - 82px)' }}
        ref={scrollbarRef}
        onScrollStop={() => {
          const { scrollTop, clientHeight, scrollHeight } =
            scrollbarRef.current.getValues();

          if (scrollHeight === scrollTop + clientHeight) {
            bottomReached();
          }
        }}
        autoHide
      >
        <MuiList disablePadding>{list}</MuiList>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { List };
