import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'house-vote/crm/list', payload);
const getById = (id) => api.v1('get', 'house-vote/crm/get', { vote_id: id });
const create = (payload) => api.v1('post', 'house-vote/crm/vote/create', payload);
const deleteItem = (id) => api.v1('post', 'house-vote/crm/vote/delete', { vote_id: id });

const getVotersDetailList = (id) =>
  api.v1('get', 'house-vote/crm/vote/voters/details-list', { vote_id: id });

const getVoteResults = (id) =>
  api.v1('get', 'house-vote/crm/vote/results', { vote_id: id });
const publish = (id) => api.v1('post', 'house-vote/crm/vote/publish', { vote_id: id });
const cancel = (payload) => api.v1('post', 'house-vote/crm/vote/cancel', payload);
const closeToCounting = (id) =>
  api.v1('post', 'house-vote/crm/vote/close-counting', { vote_id: id });
const close = (id) => api.v1('post', 'house-vote/crm/vote/close', { vote_id: id });
const archive = (id) => api.v1('post', 'house-vote/crm/vote/archive', { vote_id: id });

const getBlank = (id) =>
  api.v1(
    'request',
    'house-vote/crm/vote/blank',
    { vote_id: id },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );
const getResultPDF = (id) =>
  api.v1(
    'request',
    'house-vote/crm/vote/results/pdf',
    { vote_id: id },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );
const getUserAswerPDF = (payload) =>
  api.v1('request', 'house-vote/crm/vote/answers/pdf', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });

const voteForUser = (payload) => api.v1('post', 'house-vote/crm/vote', payload);

const getBlankForAllNoVoted = (payload) =>
  api.v1('request', 'house-vote/crm/vote/named-blanks', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });

const getBlankForSelectedNoVoted = (payload) =>
  api.v1('request', 'house-vote/crm/vote/owners-named-blanks', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'post',
  });

const notify = (payload) => api.v1('post', 'house-vote/crm/vote/notify', payload);

export const votingApi = {
  getList,
  getById,
  create,
  delete: deleteItem,
  getVoteResults,
  publish,
  cancel,
  closeToCounting,
  close,
  archive,
  getBlank,
  getResultPDF,
  getVotersDetailList,
  getUserAswerPDF,
  voteForUser,
  getBlankForAllNoVoted,
  getBlankForSelectedNoVoted,
  notify,
};
