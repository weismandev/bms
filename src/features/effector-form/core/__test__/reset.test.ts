import { createTestForm, initialValues, validationSchema } from './helpers';

test('Базовый', async () => {
  const form = createTestForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });

      form.reset();

      resolve(form);
    });

  await promise().then((values) => {
    expect(form.$values.getState()).toMatchObject(initialValues);
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationErrors.getState()).toMatchObject({});
    expect(form.$externalErrors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
  });
});
