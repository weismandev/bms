import { createTestForm, initialValues } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.arrayDelete({ path: 'books', index: 0 });

  expect(form.$values.getState().books).toEqual([]);
});

test('Удаление второго элемента', () => {
  const form = createTestForm();
  const newArrayItem = { title: 'test', desc: 'test' };

  form.arrayPush({ path: 'books', value: newArrayItem });
  form.arrayDelete({ path: 'books', index: 1 });

  expect(form.$values.getState().books).toEqual([newArrayItem]);
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayDelete({ path: 50, index: 0 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Неправильный тип index', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayDelete({ path: 'books', index: '0' });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Конечный путь не массив', () => {
  const form = createTestForm();

  form.arrayDelete({ path: 'address', index: 0 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});
