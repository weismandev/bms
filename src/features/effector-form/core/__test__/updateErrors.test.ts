import { createTestForm } from './helpers';

test('Default', () => {
  const form = createTestForm();

  form.setErrors({ name: 'testError', address: 'someError' });
  form.updateErrors({ name: 'updatedError' });

  expect(form.$errors.getState()).toMatchObject({
    name: 'updatedError',
    address: 'someError',
  });
});

test('Неправильный тип аргумента', () => {
  const form = createTestForm();

  // @ts-ignore
  form.updateErrors(['error1', 'error2']);

  expect(form.$errors.getState()).toMatchObject({});
});

test('Неправильный тип одной из ошибок', () => {
  const form = createTestForm();

  // @ts-ignore
  form.updateErrors({
    name: 'testError',
    address: 20,
  });

  expect(form.$errors.getState()).toMatchObject({});
});
