import { createTestForm } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.setError({ path: 'name', error: 'someError' });
  form.deleteError({ path: 'name' });

  expect(form.$errors.getState()).toMatchObject({});
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  form.setError({ path: 'name', error: 'someError' });
  // @ts-ignore
  form.deleteError({ path: 50 });

  expect(form.$errors.getState()).toMatchObject({ name: 'someError' });
});
