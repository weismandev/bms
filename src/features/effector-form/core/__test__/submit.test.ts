import { createTestForm, initialValues } from './helpers';

test('Базовый', async () => {
  const form = createTestForm();

  const promise = () =>
    new Promise((resolve) => {
      form.submitted.watch((values: any) => {
        resolve(values);
      });

      form.setValue({ path: 'name', value: 'newName' });

      form.submit();
    });

  await promise().then((values) =>
    expect(values).toMatchObject({ ...initialValues, name: 'newName' })
  );
});
