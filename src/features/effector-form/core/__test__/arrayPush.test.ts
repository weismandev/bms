import { createTestForm, initialValues } from './helpers';

test('Значение число', () => {
  const form = createTestForm();

  form.arrayPush({ path: 'books', value: 50 });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    50,
  ]);
});

test('Значение строка', () => {
  const form = createTestForm();

  form.arrayPush({ path: 'books', value: '50' });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    '50',
  ]);
});

test('Значение boolean', () => {
  const form = createTestForm();

  form.arrayPush({ path: 'books', value: true });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    true,
  ]);
});

test('Значение объект', () => {
  const form = createTestForm();

  form.arrayPush({ path: 'books', value: { title: '', desc: '' } });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    { title: '', desc: '' },
  ]);
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayPush({ path: 50, value: 10 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Конечный путь не массив', () => {
  const form = createTestForm();

  form.arrayPush({ path: 'address', value: true });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
});
