import { createTestForm, initialValues } from './helpers';

test('Базовый', () => {
  const form = createTestForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 1 });

  expect(form.$values.getState().books).toEqual([initialValues.books[0], newArrayItem]);
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayMove({ path: 10, source: 0, target: 1 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Неправильный тип source', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayMove({ path: 'books', source: 'test', target: 2 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Неправильный тип target', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayMove({ path: 'books', source: 1, target: 'test' });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Без аргументов', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayMove({ path: 'books' });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('С равными параметрами source и target', () => {
  const form = createTestForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 0 });

  expect(form.$values.getState().books).toEqual([newArrayItem, initialValues.books[0]]);
});

test('Конечный путь не массив', () => {
  const form = createTestForm();

  form.arrayMove({ path: 'books', source: 0, target: 1 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});
