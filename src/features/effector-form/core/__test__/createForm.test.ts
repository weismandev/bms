import { is } from 'effector';
import { createTestForm } from './helpers';

test('Корректное создание формы', () => {
  const form = createTestForm();
  expect(typeof form).toBe('object');
});

test('Корректность юнитов', () => {
  const form = createTestForm();

  expect(is.store(form.$initialValues)).toBeTruthy();
  expect(is.store(form.$validationSchema)).toBeTruthy();
  expect(is.store(form.$values)).toBeTruthy();
  expect(is.store(form.$errors)).toBeTruthy();
  expect(is.store(form.$externalErrors)).toBeTruthy();
  expect(is.store(form.$editable)).toBeTruthy();
  expect(is.store(form.$valid)).toBeTruthy();
  expect(is.store(form.$dirty)).toBeTruthy();
  expect(is.store(form.$submittable)).toBeTruthy();
  expect(is.event(form.submit)).toBeTruthy();
  expect(is.event(form.submitted)).toBeTruthy();
  expect(is.event(form.reset)).toBeTruthy();
  expect(is.event(form.init)).toBeTruthy();
  expect(is.event(form.setEditable)).toBeTruthy();
  expect(is.event(form.setValue)).toBeTruthy();
  expect(is.event(form.setValues)).toBeTruthy();
  expect(is.event(form.arrayPush)).toBeTruthy();
  expect(is.event(form.arrayDelete)).toBeTruthy();
  expect(is.event(form.arrayMove)).toBeTruthy();
  expect(is.event(form.setValidationSchema)).toBeTruthy();
  expect(is.event(form.setError)).toBeTruthy();
  expect(is.event(form.setErrors)).toBeTruthy();
  expect(is.event(form.deleteError)).toBeTruthy();
  expect(is.event(form.resetErrors)).toBeTruthy();
});
