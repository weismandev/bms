import * as Yup from 'yup';
import { createTestForm, validationSchema } from './helpers';

test('Базовая схема', () => {
  const form = createTestForm();
  const customValidationSchema = Yup.object().shape({
    name: Yup.string().min(5).required(),
  });

  form.setValidationSchema(customValidationSchema);

  expect(form.$validationSchema.getState()).toEqual(customValidationSchema);
});

test('Неправильный тип аргумента (null)', () => {
  const form = createTestForm();

  form.setValidationSchema(null);

  expect(form.$validationSchema.getState()).toBeNull();
});

test('Неправильный тип аргумента (number)', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setValidationSchema(20);

  expect(form.$validationSchema.getState()).toEqual(validationSchema);
});
