import { createTestForm, initialValues } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.updateValues({
    name: 'newName',
  });

  expect(form.$values.getState()).toMatchObject({
    ...initialValues,
    name: 'newName',
  });
});

test('Объект', () => {
  const form = createTestForm();

  const newAddress = {
    city: 'Perm',
    street: 'Pushkin',
    building: 8,
    actual: true,
  };

  form.updateValues({
    address: newAddress,
  });

  expect(form.$values.getState()).toMatchObject({
    ...initialValues,
    address: newAddress,
  });
});

test('Часть объекта', () => {
  const form = createTestForm();

  const newAddress = { city: 'Perm' };

  form.updateValues({
    address: newAddress,
  });

  expect(form.$values.getState().address).toMatchObject(newAddress);
});

test('Часть массива', () => {
  const form = createTestForm();

  const newBooks = [
    {
      title: 'newTitle',
      desc: 'newDesc',
    },
  ];

  form.updateValues({
    books: newBooks,
  });

  expect(form.$values.getState().books).toBe(newBooks);
});

test('Пустой объект', () => {
  const form = createTestForm();

  form.updateValues({});

  expect(form.$values.getState()).toMatchObject(initialValues);
});

test('Массив', () => {
  const form = createTestForm();

  // @ts-ignore
  form.updateValues([{ name: 'newName', surname: 'newSurname' }]);

  expect(form.$values.getState()).toMatchObject(initialValues);
});

test('Неправильный тип аргумента (null)', () => {
  const form = createTestForm();

  // @ts-ignore
  form.updateValues(null);

  expect(form.$values.getState()).toMatchObject(initialValues);
});
