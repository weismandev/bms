import * as Yup from 'yup';
import { createForm } from '../createForm';

export type FormData = {
  name: string;
  surname?: string;
  address: {
    city: string;
    street?: string;
    building?: number;
    actual?: boolean;
  };
  books:
    | {
        title: string;
        desc: string;
      }[]
    | [];
};

export const validationSchema = Yup.object().shape({
  name: Yup.string().strict().required(),
  address: Yup.object().shape({
    city: Yup.string().required(),
    street: Yup.string().required(),
    building: Yup.number().required(),
    actual: Yup.boolean().required(),
  }),
});

export const initialValues = {
  name: 'name',
  surname: 'surname',
  address: {
    city: 'Moscow',
    street: 'Lenin',
    building: 5,
    actual: true,
  },
  books: [
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ],
};

export const createTestForm = () =>
  createForm<FormData>({
    initialValues: { ...initialValues },
    validationSchema,
    editable: true,
  });
