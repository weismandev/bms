import isEqual from 'react-fast-compare';
import { createTestForm, initialValues, validationSchema } from './helpers';

test('С текущими значениями', () => {
  const form = createTestForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  form.init();

  expect(isEqual(form.$initialValues.getState(), form.$values.getState())).toBeTruthy();
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$validationErrors.getState()).toMatchObject({});
  expect(form.$externalErrors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
});

test('С установкой значений', () => {
  const form = createTestForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.init(newValues);

  expect(isEqual(form.$initialValues.getState(), form.$values.getState())).toBeTruthy();
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$validationErrors.getState()).toMatchObject({});
  expect(form.$externalErrors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
});
