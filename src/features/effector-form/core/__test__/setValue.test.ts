import { createTestForm, initialValues } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.setValue({ path: 'name', value: 'newName' });

  expect(form.$values.getState().name).toBe('newName');
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('С типом boolean', () => {
  const form = createTestForm();

  form.setValue({ path: 'address.actual', value: false });

  expect(form.$values.getState().address.actual).toBeFalsy();
});

test('С типом object', () => {
  const form = createTestForm();

  const newAddress = {
    city: 'Perm',
    street: 'Pushkin',
    building: 10,
    actual: true,
  };

  form.setValue({
    path: 'address',
    value: newAddress,
  });

  expect(form.$values.getState().address).toMatchObject(newAddress);
});

test('С типом array', () => {
  const form = createTestForm();

  const newBooks = [
    { title: 'Book-1', desc: 'Book-1-desc' },
    { title: 'Book-2', desc: 'Book-2-desc' },
  ];

  form.setValue({
    path: 'books',
    value: newBooks,
  });

  expect(form.$values.getState().books).toEqual(newBooks);
});

test('Path как путь до объекта', () => {
  const form = createTestForm();

  form.setValue({ path: 'address.city', value: 'Moscow' });

  expect(form.$values.getState().address.city).toBe('Moscow');
});

test('Path как путь до массива', () => {
  const form = createTestForm();

  form.setValue({ path: 'books[0].title', value: 'newBookTitle' });

  expect(form.$values.getState().books[0].title).toBe('newBookTitle');
});

test('Неправильный тип path', () => {
  const form = createTestForm();
  // @ts-ignore
  form.setValue({ path: 50, value: 'newName' });

  expect(form.$values.getState().name).toBe(initialValues.name);
});
