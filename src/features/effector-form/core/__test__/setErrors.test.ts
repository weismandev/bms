import { createTestForm } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.setErrors({ name: 'testError' });

  expect(form.$errors.getState()).toMatchObject({
    name: 'testError',
  });
});

test('Неправильный тип аргумента', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setErrors(['error1', 'error2']);

  expect(form.$errors.getState()).toMatchObject({});
});

test('Неправильный тип одной из ошибок', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setErrors({
    name: 'testError',
    address: 20,
  });

  expect(form.$errors.getState()).toMatchObject({});
});
