import { createTestForm } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  form.setError({ path: 'name', error: 'testError' });

  expect(form.$errors.getState()).toMatchObject({
    name: 'testError',
  });
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setError({ path: 50, error: 'testError' });

  expect(form.$errors.getState()).toMatchObject({});
});

test('Неправильный тип error', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setError({ path: 'name', error: 50 });

  expect(form.$errors.getState()).toMatchObject({});
});
