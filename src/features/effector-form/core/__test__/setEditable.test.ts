import { createTestForm } from './helpers';

test('Отрицательный', () => {
  const form = createTestForm();

  form.setEditable(false);

  expect(form.$editable.getState()).toBeFalsy();
});

test('Положительный', () => {
  const form = createTestForm();

  form.setEditable(true);

  expect(form.$editable.getState()).toBeTruthy();
});

test('Неправильный тип аргумента', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setEditable(50);

  expect(form.$editable.getState()).toBeTruthy();
});
