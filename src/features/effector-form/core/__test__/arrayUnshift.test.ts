import { createTestForm, initialValues } from './helpers';

test('Значение число', () => {
  const form = createTestForm();

  form.arrayUnshift({ path: 'books', value: 50 });

  expect(form.$values.getState().books).toEqual([
    50,
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
});

test('Значение строка', () => {
  const form = createTestForm();

  form.arrayUnshift({ path: 'books', value: '50' });

  expect(form.$values.getState().books).toEqual([
    '50',
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
});

test('Значение boolean ', () => {
  const form = createTestForm();

  form.arrayUnshift({ path: 'books', value: true });

  expect(form.$values.getState().books).toEqual([
    true,
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
});

test('Значение объект', () => {
  const form = createTestForm();

  form.arrayUnshift({ path: 'books', value: { title: '', desc: '' } });

  expect(form.$values.getState().books).toEqual([
    { title: '', desc: '' },
    { desc: 'bookDesc', title: 'bookTitle' },
  ]);
});

test('Неправильный тип path', () => {
  const form = createTestForm();

  // @ts-ignore
  form.arrayUnshift({ path: 50, value: 10 });

  expect(form.$values.getState().books).toEqual(initialValues.books);
});

test('Конечный путь не массив', () => {
  const form = createTestForm();

  form.arrayUnshift({ path: 'address', value: true });

  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
});
