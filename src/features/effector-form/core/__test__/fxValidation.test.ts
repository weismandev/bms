import { FormErrors } from '@effector-form';
import { createTestForm } from './helpers';

test('Обязательное поле пустое', async () => {
  const form = createTestForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: null });

      form.submit();

      form.$errors.watch((errors: FormErrors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then((errors) => expect(errors).toHaveProperty('name'));
});

test('Неправильный тип значения', async () => {
  const form = createTestForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 50 });

      form.submit();

      form.$errors.watch((errors: FormErrors) => {
        resolve(errors);
      });
    });

  await promise().then((errors) => expect(errors).toHaveProperty('name'));
});
