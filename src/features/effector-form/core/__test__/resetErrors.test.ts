import { createTestForm } from './helpers';

test('Default', () => {
  const form = createTestForm();

  form.setError({ path: 'name', error: 'someError' });
  form.resetErrors();

  const result = form.$errors.getState();

  expect(result).toMatchObject({});
});
