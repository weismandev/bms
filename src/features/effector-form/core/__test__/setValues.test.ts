import { createTestForm, initialValues } from './helpers';

test('Базовый', () => {
  const form = createTestForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Пустой объект', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setValues({});

  expect(form.$values.getState()).toMatchObject({});
});

test('Массив', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setValues([{ title: 'title', desc: 'desc' }]);

  expect(form.$values.getState()).toMatchObject(initialValues);
});

test('Неправильный тип данных (null)', () => {
  const form = createTestForm();

  // @ts-ignore
  form.setValues(null);

  expect(form.$values.getState()).toMatchObject(initialValues);
});

test('Неправильный тип path', () => {
  const form = createTestForm();
  // @ts-ignore
  form.setValue({ path: 50, value: 'newName' });

  expect(form.$values.getState().name).toBe(initialValues.name);
});
