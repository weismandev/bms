# Effector form

- [createForm](#createform)
- [Form](#form)
- [Control](#control)
- [Сторы](#сторы)
- [Команды](#команды)
- [События](#события)
- [Рецепты](#рецепты)

## createForm

Фабрика для создания модели формы

### Дженерик

`V` - Тип объекта со значениями

### Аргументы

- `initialValues`: **Store\<V\> | V** - Исходные значения формы. Может быть объектом или внешним стором.
  При каждом обновлении внешнего стора будет происходить реинициализация формы
  При каждом обновлении такого стора форма будет реинициализироваться
- `validationSchema?`: **Schema\<Partial\<V\>\> | null** - Схема валидации Yup
- `onSubmit?`: **Unit\<any\>[]** - Юнит для успешного ввода формы
- `onReject?`: **Unit\<any\>[]** - Юнит для ввода формы с ошибками
- `resetOn?`: **Unit\<any\>[]** - Массив юнитов для сброса формы
- `editable?`: **boolean** - Режим редактирования формы, по умолчанию **false**
- `debug?`: **boolean** - Режим отладки
- `sid?`: **string** - Уникальный идентификатор формы, используется в качестве имени домена

### Возвращает

`FormInstance<V>` [Сторы](#сторы) и [Юниты](#события) формы

## Form

Провайдер контекста формы для контролов. На текущем этапе включает в себя обертку из новой темы и провайдер локализации
для moment (используется в mui date pickers)

### Параметры

- `form`:**FormInstance\<V\>** - Форма созданная через фабрику **createForm**

### Пример

```tsx
import { Form, Control } from '@effector-form';
import { myForm } from './model';

  <Control.Text  form={myForm} name="title" label="Title" />
```

## Control

Консьюмер контекста и преобразователь юнитов формы

### Параметры

- `name`: **string** - Имя контрола. Плоский путь до части объекта со значениями. Пример: **books[0].title**
- `label?`: **string** - Текстовый лейбл контрола
- `required?`: **boolean** - Параметр для рендера красной звездочки
- `disabled?`: **boolean** - Доступность контрола для ввода

```tsx
<Form form={myForm}>
  <Control.Text name="title" label="Title" disabled />
  <Control.Text name="desc" label="Description" required />
</Form>
```

Встроенные параметры можно расширить, достаточно при создании контрола указать тип пропсов в дженерик withControl

```tsx
type BuildingControlProps = {
  complex?: number
}

const BuildingControl = withControl<BuildingControlProps>(
  ({ value, complex }) => {
    ... рендер контрола
  }
);
```

Использование кастомных параметров не отличается от встроенных, указываются как обычные пропсы

```tsx
const { values } = useUnit(myForm);

<Form form={myForm}>
  <Control.Complex name="complex" label="Комплекс" />
  <Control.Building name="building" label="Здание" complex={values.complex} />
</Form>;
```

Для более сложных вычислений кастомных параметров рекомендуется перенести логику в модель и использовать в качестве стора

`model`

```ts
// Создаем стор с кастомными параметрами здания
export const $buildingParams = createStore<{ complex: number }>(null);

sample({
  // Подписываемся на изменение значений формы
  clock: myForm.setValue,
  // Фильтруем только по изменению контрола с именем 'complex'
  filter: ({ path, value }) => path === 'complex' && value,
  // Мутируем перехваченное значение
  fn: (objectId, { value }) => ({ complex: `20-${value}` }),
  // Отправляем в стор с кастомными параметрами здания
  target: $buildingParams,
});
```

`view`

```tsx
const [buildingParams] = useUnit([$buildingParams]);

<Form form={myForm}>
  <Control.Complex name="complex" label="Комплекс" />
  <Control.Building name="building" label="Здание" params={buildingParams} />
</Form>;
```

### Список контролов

> Нижеприведенные контролы являются дочерними от компонента Control и доступны через точку

Базовые:

- `Text`: **string** - Обычный текстовый контрол
<!-- -->
- `Textarea`: **string** - Для ввода многострочного текста
<!-- -->
- `Number`: **number** - Настоящий числовой контрол без возможности ввести **"e"** или **"-"**
<!-- -->
- `Search`: **string** - Контрол для поиска
<!-- -->

Объекты:

- `Complex`: **number** - Комплекс
<!-- -->
- `Complexes`: **number[]** - Комплексы
<!-- -->
- `Building`: **number** - Здание
<!-- -->
- `Buildings`: **number[]** - Здания
<!-- -->
- `Apartment`: **number** - Квартиры
<!-- -->

Дата и время:

- `Date`: **Moment** - Дата
<!-- -->
- `DateTime`: **Moment** - Дата и Время
<!-- -->
- `DateRange`: **[Moment, Moment]** - Период дат
<!-- -->
- `DateTimeRange`: **[Moment, Moment]** - Период дат
<!-- -->
- `Time`: **Moment** - Время
<!-- -->
- `TimeRange`: **[Moment, Moment]** - Период времени
<!-- -->

С масками

- `Email`: **string** - Электронная почта
<!-- -->
- `Phone`: **string** - Мобильный телефон
<!-- -->
- `Password`: **string** - Пароль
<!-- -->

Логические

- `Checkbox`: **boolean** - Чекбокс
<!-- -->
- `Switch`: **boolean** - Переключатель

Типы заявок

- `TicketCostTypes`: **string** - Тип стоимости
<!-- -->
- `TicketPrepaymentTypes`: **string** - Тип предоплаты
<!-- -->
- `TicketTypeDuration`: **string** - Срок типа заявки по маске
<!-- -->
- `TicketTypesLevel`: **number** - Уровень вложенности типа

# Сторы

> У всех сторов есть "дублеры" без знака **$**, для удобства деструктуризация из useUnit.
> В модели рекомендуется использовать сторы только со знаком **$**

- `$values`: **Store\<V\>** - Текущие значения формы
<!-- -->
- `$initialValues`: **Store\<V\>** - Исходные значения формы
<!-- -->
- `$editable`: **Store\<boolean\>** - Возможность редактировать
<!-- -->
- `$valid`: **Store\<boolean\>** - Валидность значений
<!-- -->
- `$dirty`: **Store\<boolean\>** - Менялись ли значения относительно исходных
<!-- -->
- `$submittable`: **Store\<boolean\>** - Возможность сделать ввод формы. Вычисляемое значение на основе предыдущих
<!-- -->
- `$validationSchema`: **Schema\<Partial\<V\>\> | null** - Схема валидации Yup
<!-- -->
- `$errors`: **Store\<Record\<string, string\>\>** - Плоский объект с ошибками валидации, формат **\[путь до значения] : текст ошибки**

# Команды

Установка значений

- `setValue`: **Event\<{ path: string; value: any }\>** - Установить значение по указанному пути
<!-- -->
- `setValues`: **Event\<V\>** - Заменить текущие значения целиком
<!-- -->
- `updateValues`: \*\*Event\<Partial\<V\>\> - Заменить часть значений
<!-- -->
- `arrayPush`: **Event\<{ path: string; structure: Record<string, any> }\>** - Добавить элемент в массив на основе
структуры из вью слоя
<!-- -->
- `arrayDelete`: **Event\<{ path: string; index: number }\>** - Удалить элемент массива
<!-- -->
- `arrayMove`: **Event\<{ path: string; source: number; target: number }\>** - Переместить элемент массива на другое место

Установка ошибок

- `setError`: **Event\<{ path: string; error: string }\>** - Добавить ошибку для контрола
<!-- -->
- `setErrors`: **Event\<FormErrors\>** - Заменить текущие ошибки на новые
<!-- -->
- `updateErrors`: \*\*Event\<Record\<string, string\>\> - Заменить часть ошибок
<!-- -->
- `deleteError`: **Event\<{ path: string }\>** - Удалить ошибку
<!-- -->
- `resetErrors`: **Event\<void\>** - Сбросить объект с ошибками

Мета команды

- `submit`: **Event\<void\>** - Ввод формы
<!-- -->
- `reset`: **Event\<void\>** - Сброс формы до исходного состояния
<!-- -->
- `initCurrent`: **Event\<void\>** - Текущее состояние формы принять за исходное и сбросить все метовые сторы
<!-- -->
- `initValues`: **Event\<V\>** - Новые значения принять за исходные и сбросить все метовые сторы
<!-- -->
- `setEditable`: **Event\<boolean\>** - Изменить режим редактирования
<!-- -->
- `setValidationSchema`: **Event\<Schema\<Partial\<V\>\> | null\>**

# События

- `changed`: **Event\<V\>** - Ивент после изменения формы, возвращает весь объект со значениями
- `submitted`: **Event\<V\>** - Результирующий ивент после валидации, в него передаются значения из $values
- `rejected`: \*\*Event\<FormErrors\> - Ивент после ввода формы с ошибками

# Рецепты

## Базовое использование

`model`

```ts
import { sample } from 'effector';
import { createEffect } from 'effector/compat';
import { createForm } from '@effector-form';
import * as Yup from 'yup';

type FormData = {
  title: string;
  desc: string;
};

const fxSendForm = createEffect<FormData, any, Error>().use(...промис);

const myForm = createForm<FormData>({
  initialValues: {
    title: 'example title',
    desc: 'example desc',
  },
  validationSchema: Yup.object().shape({
    title: Yup.string().min(5).required(),
    desc: YupString.max(100).required(),
  }),
});

sample({
  clock: myForm.submitted,
  target: fxSendForm,
});

sample({
  clock: fxSendForm.doneData,
  target: myForm.initValues,
});
```

`view`

```tsx
import { useUnit } from 'effector-react';
import { Form } from '@effector-form';
import { Button } from '@mui/material';
import { myForm } from './model';

const Page = () => {
  const { dirty, editable, valid, submittable, submit, setEditable, reset } =
    useUnit(myForm);

  return (
    <Form form={myForm}>
      <Button onCLick={submit} disabled={!submittable}>
        Сохранить
      </Button>
      <Button onClick={reset}>Отменить</Button>

      {editable ? (
        <Button onClick={() => setEditable(false)}>Переключить в режим просмотра</Button>
      ) : (
        <Button onClick={() => testForm.setEditable(true)}>
          Переключить в режим редактирования
        </Button>
      )}

      <Control.Text name="title" label="Название" />
      <Control.Text name="вуыс" label="Описание" />
    </Form>
  );
};
```

### Использование с внешним стором

`model`

```ts
import { sample } from 'effector';
import { createEffect } from 'effector/compat';
import { createForm } from '@effector-form';
import * as Yup from 'yup';

type FormData = {
  title: string;
  desc: string;
};

const fxSendForm = createEffect<FormData, any, Error>().use(...промис);

const $myFormData = createStore<FormData>({
  title: 'example title',
  desc: 'example desc',
});

const myForm = createForm<FormData>({
  initialValues: $myFormData,
  validationSchema: Yup.object().shape({
    title: Yup.string().min(5).required(),
    desc: YupString.max(100).required(),
  }),
});

sample({
  clock: myForm.submitted,
  target: fxSendForm,
});

$myFormData.on(fxSendForm.doneData, (state, data) => data);
```

### Использование в чистом виде

Форму можно использовать за рамками котролов и любых инпутов, достаточно напрямую вызывать команды из формы.

```tsx
//  По клику на этот div изменится значение в форме
<div onClick={() => myForm.setValue({path: 'title', value: 'test'})}></div>

//  По клику на эту картинку произойдет ввод формы
<img src={...} onClick={() => myForm.submit()}/>
```

### Отслеживание изменений

```ts
sample({
  clock: testForm.setValue,
  filter: ({ path, value }) => path === 'books[1].title' && value === 'test text',
  fn: () => {
    console.log('Название первой книги изменено на "test text"');
  },
});
```

### Изменение значений на основе других

```ts
sample({
  clock: myForm.setValue,
  filter: ({ path, value }) => path === 'books[1].title',
  fn: ({ value }) => ({ path: 'books[1].desc', value: `${value} copied from title` }),
  target: myForm.setValue,
});
```
