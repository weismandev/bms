import { ApartmentControl } from '@features/effector-form/controls/apartment';
import { AsyncAutocompleteControl } from '@features/effector-form/controls/async-autocomplete';
import { BuildingControl } from '@features/effector-form/controls/building-controls/building';
import { BuildingsControl } from '@features/effector-form/controls/building-controls/buildings';
import { CheckboxControl } from '@features/effector-form/controls/checkbox';
import { CitiesControl } from '@features/effector-form/controls/city-controls/cities';
import { CityControl } from '@features/effector-form/controls/city-controls/city';
import { ComplexControl } from '@features/effector-form/controls/complex';
import { ComplexesControl } from '@features/effector-form/controls/complexes';
import { CountControl } from '@features/effector-form/controls/count';
import { DateControl } from '@features/effector-form/controls/date';
import { DateRangeControl } from '@features/effector-form/controls/dateRange';
import { DateTimeControl } from '@features/effector-form/controls/dateTime';
import { DateTimeRangeControl } from '@features/effector-form/controls/dateTimeRange';
import { EmailControl } from '@features/effector-form/controls/email';
import { EnterpriseControl } from '@features/effector-form/controls/enterprise';
import { EnterprisePositionControl } from '@features/effector-form/controls/enterprisePosition';
import { GroupControl } from '@features/effector-form/controls/group';
import { NumberControl } from '@features/effector-form/controls/number';
import { ObjectControl } from '@features/effector-form/controls/object';
import { ObjectShareControl } from '@features/effector-form/controls/objectShare';
import { ObjectTypeControl } from '@features/effector-form/controls/objectType';
import { PasswordControl } from '@features/effector-form/controls/password';
import { PersonalAccountControl } from '@features/effector-form/controls/personalAccountType';
import { PhoneControl } from '@features/effector-form/controls/phone';
import { PropertyTypeControl } from '@features/effector-form/controls/property-type-control/property-type';
import { PropertyTypesControl } from '@features/effector-form/controls/property-type-control/property-types';
import { RadioGroupControl } from '@features/effector-form/controls/radioGroup';
import { RentBuildingControl } from '@features/effector-form/controls/rent-building-controls/rent-building';
import { RentBuildingsControl } from '@features/effector-form/controls/rent-building-controls/rent-buildings';
import { RentComplexControl } from '@features/effector-form/controls/rent-complex-controls/rent-complex';
import { RentComplexesControl } from '@features/effector-form/controls/rent-complex-controls/rent-complexes';
import { RentFloorControl } from '@features/effector-form/controls/rent-floor-controls/rent-floor';
import { RentFloorsControl } from '@features/effector-form/controls/rent-floor-controls/rent-floors';
import { RentObjectsControl } from '@features/effector-form/controls/rent-objects';
import { RentPaymentControl } from '@features/effector-form/controls/rent-payment';
import { RentStatusControl } from '@features/effector-form/controls/rent-status-controls/rent-status';
import { RentStatusesControl } from '@features/effector-form/controls/rent-status-controls/rent-statuses';
import { RentTimeControl } from '@features/effector-form/controls/rent-time';
import { ResidentStatusControl } from '@features/effector-form/controls/residentStatus';
import { SearchControl } from '@features/effector-form/controls/search';
import { SelectControl } from '@features/effector-form/controls/select';
import { SwitchControl } from '@features/effector-form/controls/switch';
import { TextControl } from '@features/effector-form/controls/text';
import { TextareaControl } from '@features/effector-form/controls/textarea';
import { TicketClassControl } from '@features/effector-form/controls/ticketClass';
import { TicketCostTypesControl } from '@features/effector-form/controls/ticketCostTypes';
import { TicketOriginControl } from '@features/effector-form/controls/ticketOrigin';
import { TicketPrepaymentTypesControl } from '@features/effector-form/controls/ticketPrepaymentTypes';
import { TicketPriorityControl } from '@features/effector-form/controls/ticketPriority';
import { TicketStatusControl } from '@features/effector-form/controls/ticketStatus';
import { TicketTypeControl } from '@features/effector-form/controls/ticketType';
import { TicketTypeDurationControl } from '@features/effector-form/controls/ticketTypeDuration';
import { TicketTypesLevelControl } from '@features/effector-form/controls/ticketTypesLevel';
import { TimeControl } from '@features/effector-form/controls/time';
import { TimeRangeControl } from '@features/effector-form/controls/timeRange';

export const Control = {
  AsyncAutocomplete: AsyncAutocompleteControl,
  Complex: ComplexControl,
  Complexes: ComplexesControl,
  Building: BuildingControl,
  Buildings: BuildingsControl,
  Apartment: ApartmentControl,
  Checkbox: CheckboxControl,
  Email: EmailControl,
  Group: GroupControl,
  Password: PasswordControl,
  Phone: PhoneControl,
  Switch: SwitchControl,
  Text: TextControl,
  Date: DateControl,
  DateRange: DateRangeControl,
  Time: TimeControl,
  TimeRange: TimeRangeControl,
  DateTime: DateTimeControl,
  DateTimeRange: DateTimeRangeControl,
  Number: NumberControl,
  TicketClass: TicketClassControl,
  TicketStatus: TicketStatusControl,
  TicketType: TicketTypeControl,
  TicketPriority: TicketPriorityControl,
  TicketOrigin: TicketOriginControl,
  Textarea: TextareaControl,
  Count: CountControl,
  Select: SelectControl,
  Search: SearchControl,
  RadioGroup: RadioGroupControl,
  ResidentStatus: ResidentStatusControl,
  ObjectType: ObjectTypeControl,
  EnterprisePosition: EnterprisePositionControl,
  Enterprise: EnterpriseControl,
  TicketTypesLevel: TicketTypesLevelControl,
  TicketTypeDuration: TicketTypeDurationControl,
  TicketPrepaymentTypes: TicketPrepaymentTypesControl,
  TicketCostTypes: TicketCostTypesControl,
  Cities: CitiesControl,
  RentStatuses: RentStatusesControl,
  RentComplexes: RentComplexesControl,
  RentFloors: RentFloorsControl,
  PropertyTypes: PropertyTypesControl,
  RentPayment: RentPaymentControl,
  City: CityControl,
  RentComplex: RentComplexControl,
  PropertyType: PropertyTypeControl,
  RentStatus: RentStatusControl,
  RentFloor: RentFloorControl,
  RentBuilding: RentBuildingControl,
  RentBuildings: RentBuildingsControl,
  RentObjects: RentObjectsControl,
  RentTime: RentTimeControl,
  ObjectShare: ObjectShareControl,
  PersonalAccount: PersonalAccountControl,
  Object: ObjectControl,
};
