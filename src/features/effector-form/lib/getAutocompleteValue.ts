export const getAutocompleteValue = (options: any, value: any, multiple: boolean) => {
  if ((!options && multiple) || (!value && multiple)) return [];
  if ((!options && !multiple) || (!value && !multiple)) return null;

  return multiple
    ? options?.filter((item: any) => value?.indexOf(item.id) > -1)
    : options?.find((item: any) => item.id === value);
};
