import { FC } from 'react';
import { FormInstance } from '@unicorn/effector-form';

type CustomControlProps<T> = Omit<
  T,
  | 'form'
  | 'value'
  | 'setValue'
  | 'error'
  | 'isError'
  | 'label'
  | 'required'
  | 'readOnly'
  | 'disabled'
>;

export type ControlComponent<T> = FC<
  {
    value: any;
    setValue: (value: any) => void;
    error: string | null;
    isError: boolean;
    label: string | null;
    required: boolean;
    readOnly: boolean;
    disabled: boolean;
  } & CustomControlProps<T>
>;

export type Control<T> = FC<
  {
    form: FormInstance<any>;
    name: string;
    label?: string;
    required?: boolean;
    readOnly?: boolean;
    disabled?: boolean;
  } & CustomControlProps<T>
>;
