import { FormInstance } from '@unicorn/effector-form';

export type FormContextType = null | { form: any };

export type FormProps = {
  form: FormInstance<any>;
  children: any;
};
