import { Event, Store, Unit } from 'effector';
import * as Yup from 'yup';

export type FormValidationSchema<V> = Yup.Schema<Partial<V>>;

export type FormValidationParams<V> = {
  values: V;
  schema: FormValidationSchema<V>;
};

export type FormPath = string;

export type FormErrors = Record<FormPath, string>;

export type Units = Unit<any> | Array<Unit<any>>;

/** Конфигурация формы, аргументы функции createForm */
export type FormConfig<V> = {
  initialValues: Store<V> | V;
  validationSchema?: FormValidationSchema<V>;
  onSubmit?: Units;
  onReject?: Units;
  resetOn?: Units;
  editable?: boolean;
  debug?: boolean;
  sid?: string;
};

/** Внешние и внутренние юниты формы */
export type FormUnits<V> = {
  $values: Store<V>;
  $initialValues: Store<V>;
  $editable: Store<boolean>;
  $valid: Store<boolean>;
  $dirty: Store<boolean>;
  $submittable: Store<boolean>;
  $validationSchema: Store<FormValidationSchema<V> | null>;
  $validationErrors: Store<FormErrors>;
  $externalErrors: Store<FormErrors>;
  $errors: Store<FormErrors>;

  submit: Event<void>;
  reset: Event<void>;
  initCurrent: Event<void>;
  initValues: Event<V>;
  setEditable: Event<boolean>;
  setValue: Event<{ path: FormPath; value: any }>;
  setValues: Event<V>;
  updateValues: Event<Partial<V>>;
  arrayUnshift: Event<{ path: FormPath; value: any }>;
  arrayPush: Event<{ path: FormPath; value: any }>;
  arrayDelete: Event<{ path: FormPath; index: number }>;
  arrayMove: Event<{ path: FormPath; source: number; target: number }>;
  setValidationSchema: Event<FormValidationSchema<V> | null>;
  setError: Event<{ path: FormPath; error: string }>;
  setErrors: Event<FormErrors>;
  updateErrors: Event<FormErrors>;
  deleteError: Event<{ path: FormPath }>;
  resetErrors: Event<void>;

  changed: Event<V>;
  submitted: Event<V>;
  rejected: Event<FormErrors>;
};

/** Основная сущность создаваемой формы */
export type FormInstance<V> = {
  $values: FormUnits<V>['$values'];
  $initialValues: FormUnits<V>['$initialValues'];
  $editable: FormUnits<V>['$editable'];
  $valid: FormUnits<V>['$valid'];
  $dirty: FormUnits<V>['$dirty'];
  $submittable: FormUnits<V>['$submittable'];
  $validationSchema: FormUnits<V>['$validationSchema'];
  $validationErrors: FormUnits<V>['$validationErrors'];
  $externalErrors: FormUnits<V>['$externalErrors'];
  $errors: FormUnits<V>['$errors'];

  values: FormUnits<V>['$values'];
  initialValues: FormUnits<V>['$initialValues'];
  editable: FormUnits<V>['$editable'];
  valid: FormUnits<V>['$valid'];
  dirty: FormUnits<V>['$dirty'];
  submittable: FormUnits<V>['$submittable'];
  validationSchema: FormUnits<V>['$validationSchema'];
  validationErrors: FormUnits<V>['$validationErrors'];
  externalErrors: FormUnits<V>['$externalErrors'];
  errors: FormUnits<V>['$errors'];

  submit: FormUnits<V>['submit'];
  reset: FormUnits<V>['reset'];
  initCurrent: FormUnits<V>['initCurrent'];
  initValues: FormUnits<V>['initValues'];
  setEditable: FormUnits<V>['setEditable'];
  setValue: FormUnits<V>['setValue'];
  setValues: FormUnits<V>['setValues'];
  updateValues: FormUnits<V>['updateValues'];
  arrayUnshift: FormUnits<V>['arrayUnshift'];
  arrayPush: FormUnits<V>['arrayPush'];
  arrayDelete: FormUnits<V>['arrayDelete'];
  arrayMove: FormUnits<V>['arrayMove'];
  setValidationSchema: FormUnits<V>['setValidationSchema'];
  setError: FormUnits<V>['setError'];
  setErrors: FormUnits<V>['setErrors'];
  updateErrors: FormUnits<V>['updateErrors'];
  deleteError: FormUnits<V>['deleteError'];
  resetErrors: FormUnits<V>['resetErrors'];

  changed: FormUnits<V>['changed'];
  submitted: FormUnits<V>['submitted'];
  rejected: FormUnits<V>['rejected'];
};
