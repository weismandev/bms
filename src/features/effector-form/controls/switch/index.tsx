import { ChangeEvent } from 'react';
import { FormControlLabel, Switch } from '@mui/material';
import { withControl } from '@features/effector-form/hoc/withControl';

export const SwitchControl = withControl(
  ({ value, setValue, label, readOnly, disabled }) => {
    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
      setValue(event.target.checked);
    };

    return (
      <FormControlLabel
        control={<Switch checked={value} onChange={handleChange} />}
        label={label}
        disabled={readOnly || disabled}
      />
    );
  }
);
