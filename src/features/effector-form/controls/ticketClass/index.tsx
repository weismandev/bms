import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import {
  TicketClassControlGate,
  ticketClassesQuery,
} from '@features/effector-form/controls/ticketClass/model';
import { withControl } from '@features/effector-form/hoc/withControl';

export const TicketClassControl = withControl(
  ({ value, setValue, error, label, required, readOnly, disabled }) => {
    useGate(TicketClassControlGate);

    const { data, pending } = useUnit(ticketClassesQuery);

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          loading={pending}
          value={!data ? null : data?.find((item) => item.id === value)}
          options={!data ? [] : data}
          onChange={(event: any, newValue: any) => {
            setValue(newValue?.id || null);
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
      </FormControl>
    );
  }
);
