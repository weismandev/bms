import { api } from '@api/api2';
import { ApartmentRequest } from '@features/effector-form/controls/apartment/types';

export const getApartments = (payload: ApartmentRequest = {}) =>
  api.v1('get', 'apartment/simple-list', { ...payload, per_page: 1000000 });
