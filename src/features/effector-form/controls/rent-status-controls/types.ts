export type Statuses = {
  id: number;
  title: string;
};

export type StatusProps = {
  mode?: 'registry' | 'monitoring' | 'create' | 'update';
};

export type GetStatusPayload = {
  mode: StatusProps;
};
