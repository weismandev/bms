import { api } from '@api/api2';
import { GetStatusPayload } from './types';

const getRentStatuses = (payload: GetStatusPayload) =>
  api.v4('get', 'rent/status/list', payload);

export const rentStatusesApi = { getRentStatuses };
