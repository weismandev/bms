import { api } from '@api/api2';

const getAccountTypes = () => api.v1('get', 'apartment-accounts/crm/crud/account-types');

export const personalAccountApi = {
  getAccountTypes,
};
