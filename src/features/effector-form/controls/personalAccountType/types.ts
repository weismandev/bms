export type PersonalAccount = {
  id: number;
  title: string;
};

export type PersonalAccountResponse = {
  data: {
    items: PersonalAccount[];
  };
};
