import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import { RentBuildingsProps } from '@features/effector-form/controls/rent-building-controls/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { RentBuildingsControlGate, rentBuildingsQuery } from '../model';

export const RentBuildingControl = withControl<RentBuildingsProps>(
  ({ value, setValue, error, label, required, readOnly, disabled, complex = null }) => {
    useGate(RentBuildingsControlGate, { complexes: complex });

    const { data, pending } = useUnit(rentBuildingsQuery);

    const val = value !== '' ? data?.find((item) => item.id === value) : null;

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          loading={pending}
          value={!data ? null : val || null}
          options={data && complex ? data : []}
          onChange={(_: unknown, newValue: any) => {
            setValue(newValue?.id || null);
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
