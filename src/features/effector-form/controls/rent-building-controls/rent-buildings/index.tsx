import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import {
  RentBuilding,
  RentBuildingsProps,
} from '@features/effector-form/controls/rent-building-controls/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { RentBuildingsControlGate, rentBuildingsQuery } from '../model';

export const RentBuildingsControl = withControl<RentBuildingsProps>(
  ({ value, setValue, error, label, required, readOnly, disabled, complexes = [] }) => {
    useGate(RentBuildingsControlGate, { complexes });

    const { data, pending } = useUnit(rentBuildingsQuery);

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          multiple
          loading={pending}
          value={!data ? [] : data.filter((item) => value.indexOf(item.id) > -1)}
          options={data && complexes.length > 0 ? data : []}
          onChange={(_: unknown, newValue: any) => {
            setValue(newValue.map((item: RentBuilding) => item.id));
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
