export type RentBuildingsProps = {
  complexes?: string[];
  complex?: string;
};

export type RentBuildingsPayload = {
  parent_guids: string[];
};

export type RentBuildingsResponse = {
  areaList: {
    guid: string;
    building: {
      title: string;
    };
  }[];
};

export type RentBuilding = {
  id: string;
  label: string;
};
