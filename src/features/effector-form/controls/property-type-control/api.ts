import { api } from '@api/api2';

const getPropertyTypes = () => api.v4('get', 'area/type/list', { mode: 'rent' });

export const propertyTypesApi = { getPropertyTypes };
