export type PropertyType = {
  id: number;
  title: string;
};
