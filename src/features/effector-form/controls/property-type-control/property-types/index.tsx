import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import { PropertyType } from '@features/effector-form/controls/property-type-control/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { PropertyTypesControlGate, propertyTypesQuery } from '../model';

export const PropertyTypesControl = withControl(
  ({ value, setValue, error, label, required, readOnly, disabled }) => {
    useGate(PropertyTypesControlGate);

    const { data, pending } = useUnit(propertyTypesQuery);

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          multiple
          loading={pending}
          value={!data ? [] : data.filter((item) => value.indexOf(item.id) > -1)}
          options={!data ? [] : data}
          onChange={(_: unknown, newValue: any) => {
            setValue(newValue.map((item: PropertyType) => item.id));
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
