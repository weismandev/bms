export type Enterprise = {
  id: number;
  title: string;
};

export type EnterpriseResponse = {
  enterprises: Enterprise[];
};
