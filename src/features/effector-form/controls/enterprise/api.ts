import { api } from '@api/api2';

const getEnterprises = () => api.v1('get', 'enterprises/get-simple-list');

export const enterpriseApi = { getEnterprises };
