import { ObjectShare } from '@features/effector-form/controls/objectShare/types/common';

export const shareToString = (share: ObjectShare): string => {
  return `${share.numerator || 0}/${share.denominator || 0}`;
};
