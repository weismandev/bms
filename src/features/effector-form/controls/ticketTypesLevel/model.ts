import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  {
    id: 0,
    label: t('1StLevel'),
  },
  {
    id: 1,
    label: t('2NdLevel'),
  },
  {
    id: 2,
    label: t('3RdLevel'),
  },
];
