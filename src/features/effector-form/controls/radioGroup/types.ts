type RadioOption = {
  id: any;
  label: string;
};

export type RadioGroupControlType = {
  options: RadioOption[];
};
