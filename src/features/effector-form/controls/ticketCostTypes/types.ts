export type GetPaymentTypesResponse = {
  invoice_statuses: PaymentItem[];
  prepayments: PaymentItem[];
  prices: PaymentItem[];
};

export type PaymentItem = {
  id: number | string;
  name: string;
  title: string;
};
