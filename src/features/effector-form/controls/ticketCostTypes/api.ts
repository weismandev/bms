import { api } from '@api/api2';

export const getCostTypes = () => api.v1('get', '/tck/payment-types/list');
