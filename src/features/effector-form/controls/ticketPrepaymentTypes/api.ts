import { api } from '@api/api2';

export const getPrepaymentTypes = () => api.v1('get', '/tck/payment-types/list');
