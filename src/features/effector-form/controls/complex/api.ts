import { api } from '@api/api2';

const getComplexes = () => api.v1('get', 'complex/list');

export const complexApi = { getComplexes };
