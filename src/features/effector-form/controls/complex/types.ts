export type Complex = {
  id: number;
  title: string;
};

export type ComplexResponse = {
  items: Complex[];
};
