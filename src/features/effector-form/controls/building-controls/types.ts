export type Building = {
  id: number;
  title: string;
};

export type BuildingItem = {
  building: Building;
  id: number;
};

export type BuildingRequest = {
  complex_id?: number;
};

export type BuildingResponse = {
  buildings: BuildingItem[];
};

export type BuildingControlProps = {
  complex?: number;
};
