import { api } from '@api/api2';

const getBuildings = (payload = {}) =>
  api.v1('get', 'buildings/get-list-crm', { ...payload, per_page: 1000000 });

export const buildingApi = { getBuildings };
