import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import { BuildingControlProps } from '@features/effector-form/controls/building-controls/types';
import { Complex } from '@features/effector-form/controls/complexes/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { BuildingControlGate, buildingQuery } from '../model';

export const BuildingsControl = withControl<BuildingControlProps>(
  ({ value, setValue, error, label, required, readOnly, disabled, complex }) => {
    useGate(BuildingControlGate, { complex });

    const { data, pending } = useUnit(buildingQuery);

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          multiple
          loading={pending}
          value={!data ? [] : data.filter((item) => value.indexOf(item.id) > -1)}
          options={!data ? [] : data}
          onChange={(event: any, newValue: any) => {
            setValue(newValue.map((item: Complex) => item.id));
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
