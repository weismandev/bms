export type RentTimeProps = {
  rentalDate?: Date | null;
  rentObject?: string | null;
};

export type RentTimePayload = {
  area_id: string;
  time_date: number;
};

export type RentTimeResponse = {
  position: number;
  rent_start: number;
  rent_finish: number;
  slot_title: string;
};

export type RentTime = {
  id: string;
  label: string;
  start: number;
  finish: number;
};
