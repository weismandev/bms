import { api } from '@api/api2';
import { RentTimePayload } from '@features/effector-form/controls/rent-time/types';

const getRentTime = (payload: RentTimePayload) =>
  api.v4('get', 'rent/timeslot/list', payload);

export const rentTimeApi = { getRentTime };
