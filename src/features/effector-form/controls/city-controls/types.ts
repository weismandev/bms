export type CitiesResponse = {
  items: City[];
};

export type City = {
  id: number;
  title: string;
  offset: number;
};
