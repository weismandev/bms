import { api } from '@api/api2';

const getCities = () => api.v4('get', 'localities/list');

export const citiesApi = { getCities };
