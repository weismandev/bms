export type RentFloorsControlProps = {
  buildings?: string[];
  building?: string;
};

export type RentFloorsPayload = {
  parent_guids: string[];
};

export type RentFloor = {
  id: string;
  title: string;
};

export type RentFloorsResponse = {
  areaList: {
    guid: string;
    title: string;
  }[];
};
