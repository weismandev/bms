export type ObjectInstance = { guid: string; type_title: ObjectType } & Partial<{
  guid: string;
  type_title: ObjectType;
  name: string;
  number: string;
  image: ObjectImage;
  scheme_id: number;
  address: string;
  fias: string;
  alias: string;
  timezone: string;
  area: ObjectArea;
  region_id: number;
  // utc: number;
  building_title: string;
  entrance_count: number;
  entrance_area: ObjectArea;
  entrance_number: number;
  passage_count: number;
  passage_area: ObjectArea;
  passage_number: number;
  room_count: number;
  elevator_count: number;
  floor_count: string;
  floor_number: number;
  apartment_count: number;
  apartment_area: ObjectArea;
  office_count: number;
  office_area: ObjectArea;
  property_place_count: number;
  property_place_area: ObjectArea;
  technical_facility_count: number;
  technical_facility_area: ObjectArea;
  common_facility_count: number;
  common_facility_area: ObjectArea;
  other_facility_count: number;
  other_facility_area: ObjectArea;
  parking_place_count: number;
  property_title: string;
  coordinates: {
    latitude: string;
    longitude: number;
  };
  is_rent_available: boolean;
  is_private_rent_available: boolean;
  building_guids: boolean;
  is_common_zone: boolean;
  is_for_residents_zone: boolean;
  is_for_guests_zone: boolean;
  is_fullness_control_zone: boolean;
  price: number;
  capacity: number;
  is_manufacture: boolean;
  truck_access: boolean;
  property_owner_enterprise: { id: number; title: string };
  area_accounts: {
    id: number;
    account_number: string;
    archived: boolean;
  }[];
  // Хак для обхода ограничений формика
  updated_at: number;
  assignee_appointment_method_id?: number;
}>;

export type ObjectType =
  | 'complex'
  | 'building'
  | 'entrance'
  | 'floor'
  | 'elevator'
  | 'apartment'
  | 'room'
  | 'passage'
  | 'office'
  | 'coworking'
  | 'parking'
  | 'parking_place'
  | 'parking_zone'
  | 'parking_lot'
  | 'pantry'
  | 'property_place'
  | 'property'
  | 'workplace';

export type Objects = Record<ObjectInstance['guid'], ObjectMeta>;

export type ObjectMeta = {
  guid: ObjectInstance['guid'];
  parent: ObjectInstance['guid'] | null;
  title: string;
  type: ObjectInstance['type_title'];
  level: number;
  children?: ObjectMeta[];
};

export type GetObjectsRequest = {
  parent_guid?: string;
  level?: number;
};

export type GetObjectsResponse = {
  areaTree: ObjectMeta[];
};

export type ObjectsTree = ObjectMeta[];

export type ObjectDialogProps = {
  setValue: (value: any) => void;
};
