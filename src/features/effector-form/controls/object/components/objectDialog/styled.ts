import { LinearProgress } from '@mui/material';
import { styled } from '@mui/styles';

export const Loader = styled(LinearProgress)(() => ({
  position: 'absolute',
  width: '100%',
  top: 0,
}));
