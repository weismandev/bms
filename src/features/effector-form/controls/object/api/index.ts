import { api } from '@api/api2';
import { GetObjectsRequest } from '../types';

const getObjects = (payload: GetObjectsRequest) =>
  api.v4('get', 'area/objects/tree', payload);

export const objectsApi = { getObjects };
