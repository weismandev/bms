export type RentComplexResponse = {
  areaList: {
    guid: string;
    complex: {
      title: string;
    };
  }[];
};

export type RentComplex = {
  id: string;
  title: string;
};

export type RentComplexProps = {
  cities?: number[];
  city?: number;
};

export type RentComplexPayload = {
  filters: {
    city_ids: string[];
  };
};
