import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import {
  RentComplex,
  RentComplexProps,
} from '@features/effector-form/controls/rent-complex-controls/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { RentComplexesControlGate, rentComplexesQuery } from '../model';

export const RentComplexesControl = withControl<RentComplexProps>(
  ({ value, setValue, error, label, required, readOnly, disabled, cities = [] }) => {
    useGate(RentComplexesControlGate, { cities });

    const { data, pending } = useUnit(rentComplexesQuery);

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          multiple
          loading={pending}
          value={!data ? [] : data.filter((item) => value.indexOf(item.id) > -1)}
          options={data && cities.length > 0 ? data : []}
          onChange={(_: unknown, newValue: any) => {
            setValue(newValue.map((item: RentComplex) => item.id));
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
