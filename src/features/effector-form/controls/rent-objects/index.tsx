import { useGate, useUnit } from 'effector-react';
import {
  Autocomplete,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
  CircularProgress,
} from '@mui/material';
import { Listbox } from '@features/effector-form/components/listbox';
import { RentObjectsProps } from '@features/effector-form/controls/rent-objects/types';
import { withControl } from '@features/effector-form/hoc/withControl';
import { RentObjectsControlGate, rentObjectsQuery } from './model';

export const RentObjectsControl = withControl<RentObjectsProps>(
  ({
    value,
    setValue,
    error,
    label,
    required,
    readOnly,
    disabled,
    propertyType = null,
    object = null,
  }) => {
    useGate(RentObjectsControlGate, {
      propertyType: propertyType as string,
      object: object as string,
    });

    const { data, pending } = useUnit(rentObjectsQuery);

    const val = value !== '' ? data?.find((item) => item.id === value) : null;

    return (
      <FormControl required={required} disabled={disabled}>
        <FormLabel>{label}</FormLabel>
        <Autocomplete
          loading={pending}
          value={!data ? null : val || null}
          options={data && propertyType && object ? data : []}
          onChange={(_: unknown, newValue: any) => {
            setValue(newValue?.id || null);
          }}
          ListboxComponent={Listbox}
          renderInput={(params): JSX.Element => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                readOnly,
                endAdornment: readOnly ? null : (
                  <>
                    {pending && <CircularProgress size={20} color="inherit" />}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
        />
        <FormHelperText error>{error}</FormHelperText>
      </FormControl>
    );
  }
);
