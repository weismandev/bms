import { api } from '@api/api2';
import { RentObjectsPayload } from '@features/effector-form/controls/rent-objects/types';

const getRentObjects = (payload: RentObjectsPayload) =>
  api.v4('get', 'area/objects/simple/list', payload);

export const rentObjectsApi = { getRentObjects };
