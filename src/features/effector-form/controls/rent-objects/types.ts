export type RentObjectsProps = {
  propertyType?: string | null;
  object?: string | null;
};

export type RentObjectsPayload = {
  parent_guids: string[];
  area_types: string[];
};

export type RentObjectsResponse = {
  areaList: {
    guid: string;
    title: string;
  }[];
};
