import { createEffect, createStore, forward } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { getList } from '../api';

const Gate = createGate();
const { open, close } = Gate;

const fxGetList = createEffect().use(getList);

const $data = createStore([])
  .on(fxGetList.doneData, (_, { sections }) => sections || [])
  .reset([signout, close]);

const $error = createStore(null)
  .on(fxGetList.fail, (_, { error }) => error)
  .reset([signout, close]);

const $isLoading = createStore(false)
  .on(fxGetList.pending, (_, result) => result)
  .reset([signout, close]);

forward({
  from: open,
  to: fxGetList,
});

export { fxGetList, $data, $error, $isLoading, Gate };
