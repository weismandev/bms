import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'tickets/sections/get-list');

export { getList };
