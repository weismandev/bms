import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, Gate } from '../../model';

const TicketsTypesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const TicketsTypesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);
  const getOptionValue = (option) => Boolean(option) && option.section_id;
  const getOptionLabel = (option) =>
    (Boolean(option) && option.section_title) || '- другой тип -';

  return (
    <>
      <Gate />
      <SelectField
        onChange={onChange}
        getOptionValue={getOptionValue}
        getOptionLabel={getOptionLabel}
        component={<TicketsTypesSelect />}
        {...props}
      />
    </>
  );
};

export { TicketsTypesSelect, TicketsTypesSelectField };
