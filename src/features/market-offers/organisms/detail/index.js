import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import { TargetedContent } from '@features/targeted-content';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  BaseInputLabel,
  SwitchField,
  ActionFileWrapper,
  FileControl,
  Divider,
  CustomScrollbar,
} from '../../../../ui';
import { CategoriesSelectField } from '../../../categories-select';
import { MarketCompanySelectField } from '../../../market-company-select';
import { MarketTagSelectField } from '../../../market-tag-select';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  deleteClicked,
} from '../../models';
import { states, types } from '../../models/detail.model';

const { t } = i18n;

const Detail = memo((props) => {
  const mode = useStore($mode);
  const opened = useStore($opened);

  const isNew = !opened.id;
  const validationSchema = Yup.object().shape({});

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values }) => (
          <Form style={{ height: 'inherit', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              hidden={{ edit: !opened.edit_enabled }}
              onDelete={() => deleteClicked()}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  props.resetForm();
                }
              }}
            />
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    name="company"
                    component={MarketCompanySelectField}
                    label={t('company')}
                    mode={mode}
                    placeholder={t('ChooseCompany')}
                  />
                  <Field
                    name="title"
                    label={t('Name')}
                    placeholder={t('EnterTheTitle')}
                    mode={mode}
                    component={InputField}
                  />
                  <Field
                    name="state"
                    component={SelectField}
                    labelPlacement="start"
                    options={states}
                    label={t('Label.status')}
                    placeholder={t('selectStatuse')}
                    mode={mode}
                  />
                  <Field
                    name="type"
                    component={SelectField}
                    options={types}
                    label={t('type')}
                    labelPlacement="start"
                    placeholder={t('chooseType')}
                    mode={mode}
                  />
                  <Field
                    name="ext_id"
                    component={InputField}
                    label={t('ProductId')}
                    labelPlacement="start"
                    placeholder={t('EnterProductId')}
                    mode={mode}
                  />
                  <Field
                    name="summary"
                    label={t('ShortDescription')}
                    helpText={t('WithoutRestrictions')}
                    placeholder={t('EnterDescription')}
                    multiline
                    rowsMax={5}
                    mode={mode}
                    component={InputField}
                  />

                  <Field
                    name="description"
                    label={t('FullDescription')}
                    helpText={t('WithoutRestrictionsFullDescription')}
                    placeholder={t('EnterDescription')}
                    multiline
                    rowsMax={20}
                    mode={mode}
                    component={InputField}
                  />
                  <Field
                    name="tags"
                    component={MarketTagSelectField}
                    label={t('Tags')}
                    mode={mode}
                    placeholder={t('SelectTags')}
                    isMulti
                  />
                  <Field
                    name="categories"
                    component={CategoriesSelectField}
                    label={t('Categories')}
                    companies={
                      values.company
                        ? [
                            typeof values.company === 'object'
                              ? values.company.id
                              : values.company,
                          ]
                        : ''
                    }
                    mode={mode}
                    placeholder={t('SelectCategories')}
                    isMulti
                  />

                  <FieldArray
                    name="images"
                    render={({ push, remove }) => {
                      const images = Array.isArray(values.images)
                        ? values.images.map((i, idx, array) => {
                            const { meta, ...rest } = i;

                            return (
                              <ActionFileWrapper
                                key={idx}
                                deleteFn={mode === 'edit' ? () => remove(idx) : null}
                                url={rest.preview}
                                {...rest}
                                {...meta}
                              />
                            );
                          })
                        : [];

                      return (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('Images')}
                          </BaseInputLabel>
                          <div
                            style={{
                              display: 'flex',
                              flexWrap: 'wrap',
                              justifyContent: 'center',
                            }}
                          >
                            {images}
                            <FileControl
                              inputProps={{ accept: 'image/*' }}
                              name="image"
                              value=""
                              loadable
                              renderButton={() => (
                                <Button
                                  style={{ width: 150, height: 150 }}
                                  component="span"
                                  color="primary"
                                >
                                  {t('Downloads')}
                                </Button>
                              )}
                              renderPreview={() => null}
                              onChange={(value) => value && push(value)}
                              mode={mode}
                            />
                          </div>
                          <Divider />
                        </>
                      );
                    }}
                  />

                  <Field
                    name="units"
                    component={InputField}
                    label={t('UnitsMeasurements')}
                    mode={mode}
                    placeholder={t('EnterUnitsMeasurements')}
                  />
                  <Field
                    name="old_price"
                    component={InputField}
                    label={t('OldPrice')}
                    type="number"
                    helpText={t('IfExistPrice')}
                    mode={mode}
                    placeholder={t('EnterUnitsMeasurements')}
                  />
                  <Field
                    name="price"
                    component={InputField}
                    label={t('Price')}
                    type="number"
                    helpText={t('CurrentPrice')}
                    mode={mode}
                    placeholder={t('EnterUnitsMeasurements')}
                  />
                  <Field
                    name="is_price_range"
                    component={SwitchField}
                    label={t('DisplayPriceAsARange')}
                    helpText={t('DisplayPriceAsARangeHelperText')}
                  />
                  {isNew && <TargetedContent mode={mode} />}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
});

export { Detail };
