import { forward, attach, merge, guard, sample } from 'effector';
import { delay } from 'patronum';
import { $pathname, history } from '@features/common';
import { formatFile } from '@tools/formatFile';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import {
  entityApi,
  $opened,
  $mode,
  openViaUrl,
  $isDetailOpen,
  newEntity,
  changedDetailVisibility,
} from './detail.model';
import {
  fxCreate,
  fxUpdate,
  fxGetById,
  fxDelete,
  fxGetList,
  $entityId,
  $path,
  pageUnmounted,
} from './page.model';

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetById.prepend(({ entityId }) => entityId),
});

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetById.done, () => true)
  .on(fxDelete.done, () => false)
  .reset([pageUnmounted, fxDelete.done, fxGetById.fail]);

sample({
  clock: fxGetById.done,
  fn: ({ result: { offer } }) => {
    if (!offer) {
      history.push('.');

      return false;
    }

    return true;
  },
  target: $isDetailOpen,
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        offer: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

const successCreateUpdate = merge([fxCreate.done, fxUpdate.done]);

$opened
  .on(merge([successCreateUpdate, fxGetById.done]), (state, { result }) =>
    formatOpened(result.offer)
  )
  .on(fxDelete.done, () => newEntity)
  .reset(pageUnmounted);

$mode.on(merge([successCreateUpdate, fxDelete.done]), () => 'view');

forward({
  from: openViaUrl,
  to: fxGetById,
});

sample({
  clock: entityApi.create,
  fn: ({ age, genders, targetingOptions, ...data }) => {
    const payload = formatPayload(data);

    const targeting = {
      age_from: age[0],
      age_to: age[1],
      genders: genders.map((gender) => gender.id),
      options: targetingOptions.map((option) => option.map((i) => i.id)).flat(),
    };

    payload.targeting = targeting;

    return payload;
  },
  target: fxCreate,
});

sample({
  clock: entityApi.update,
  fn: ({ age, genders, targetingOptions, ...data }) => formatPayload(data),
  target: fxUpdate,
});

function formatOpened(data) {
  const {
    images,
    description,
    title,
    summary,
    state,
    type,
    ext_id,
    units,
    old_price,
    price,
    is_price_range,
    id,
    tags,
    categories,
    company_id,
    edit_enabled,
  } = data;

  return {
    id,
    images: Array.isArray(images) ? images.map(formatFile) : '',
    description: description || '',
    title: title || '',
    summary: summary || '',
    state: state || 'draft',
    type: type || 'product',
    ext_id: ext_id || '',
    units: units || '',
    old_price: old_price || '',
    price: price || '',
    is_price_range: Boolean(is_price_range),
    tags: Array.isArray(tags) ? tags : '',
    categories: Array.isArray(categories) ? categories : '',
    company: company_id || company_id === 0 ? company_id : '',
    edit_enabled: Boolean(edit_enabled),
    targetingOptions: [],
    genders: [],
    age: [0, 100],
  };
}

function formatPayload(payload) {
  const {
    id,
    title,
    summary,
    state,
    type,
    ext_id,
    units,
    old_price,
    price,
    is_price_range,
    categories,
    company,
    images,
    tags,
    logo,
    edit_enabled,
    ...rest
  } = payload;

  return {
    id,
    title,
    summary,
    state: typeof state === 'object' ? state.id : state,
    type: typeof type === 'object' ? type.id : type,
    ext_id,
    units,
    old_price,
    price,
    is_price_range,
    tags: Array.isArray(tags) ? tags.map((i) => (typeof i === 'object' ? i.id : i)) : [],
    categories: Array.isArray(categories)
      ? categories.map((i) => (typeof i === 'object' ? i.id : i))
      : [],
    company_id: typeof company === 'object' ? company.id : company,
    images: Array.isArray(images) ? images.map((i) => i.id) : [],
    ...rest,
  };
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
