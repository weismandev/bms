import { attach } from 'effector';
import {
  createSavedUserSettings,
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';

import { PageGate } from './page.model';
import {
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $pageSize,
} from './table.model';

export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const settings = {
  section: 'market-offers',
  params: {
    table: { $columnWidths, $columnOrder, $hiddenColumnNames, $pageSize },
  },
  pageMounted: PageGate.open,
  storage: {
    fxSaveInStorage,
    fxGetFromStorage,
  },
};

export const [$settingsInitialized, $isGettingSettingsFromStorage] =
  createSavedUserSettings(settings);
