import { $pathname } from '@features/common';
import { createPageBag } from '../../../tools/factories';
import { marketCompaniesApi } from '../api';

export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  marketCompaniesApi,
  {},
  {
    idAttribute: 'id',
    itemsAttribute: 'offers',
    createdPath: 'offer',
    updatedPath: 'offer',
  }
);
