import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

export const newEntity = {
  images: '',
  description: '',
  title: '',
  summary: '',
  state: 'draft',
  type: 'product',
  ext_id: '',
  units: '',
  old_price: '',
  price: '',
  is_price_range: false,
  tags: '',
  categories: '',
  company: '',
  targetingOptions: [],
  genders: [],
  age: [0, 100],
};

const { t } = i18n;

export const states = [
  { id: 'draft', title: t('Draft') },
  { id: 'published', title: t('Published') },
  { id: 'hidden', title: t('Hidden') },
];

export const types = [
  { id: 'product', title: t('Product') },
  { id: 'service', title: t('Service') },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
