import { $filters } from './filter.model';
import { $currentPage, $search } from './table.model';

$currentPage.reset([$filters, $search]);
