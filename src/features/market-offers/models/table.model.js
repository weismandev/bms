import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'id', title: 'ID' },
  { name: 'title', title: t('Name') },
  { name: 'state', title: t('Label.status') },
];

const states = {
  draft: t('Draft'),
  hidden: t('Hidden'),
  published: t('Published'),
};

export const $tableData = $raw.map(({ data }) =>
  data.map((item) => ({
    ...item,
    state: states[item.state],
  }))
);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns);
