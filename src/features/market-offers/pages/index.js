import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '../../../ui';

import { Detail, Filters, Table } from '../organisms';

import {
  $error,
  $isDeleteDialogOpen,
  $isDetailOpen,
  $isErrorDialogOpen,
  $isLoading,
  $path,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models';

import { $isFilterOpen } from '../models/filter.model';

const MarketOffersPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const error = useStore($error);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        filter={isFilterOpen && <Filters />}
      />
    </>
  );
};

const RestrictedMarketOffersPsge = (props) => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <MarketOffersPage />
    </HaveSectionAccess>
  );
};

export { RestrictedMarketOffersPsge as MarketOffersPage };
