import { api } from '../../../api/api2';

const getList = ({ params, data }) =>
  api.v2('/client/admin/marketplace/offers/list', data, params);

const getById = (id) => {
  return api
    .v2('/client/admin/marketplace/offers/details', { offers: [id] })
    .then((result) => {
      return { offer: result.offers[0] };
    });
};

const create = (payload) => api.v2('/client/admin/marketplace/offers/create', payload);

const update = (payload) => api.v2('/client/admin/marketplace/offers/update', payload);
const deleteItem = (id) => api.v2('/client/admin/marketplace/offers/delete', { id });

export const marketCompaniesApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteItem,
};
