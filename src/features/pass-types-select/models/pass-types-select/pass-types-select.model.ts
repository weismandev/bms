import { createEffect, createStore } from 'effector';
import { PassTypesResponse, PassType, PassTypesPayload } from '../../types';

export const fxGetList = createEffect<PassTypesPayload, PassTypesResponse, Error>();

export const $data = createStore<PassType[]>([]);
export const $isLoading = createStore<boolean>(false);
