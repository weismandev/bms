import { signout } from '@features/common';
import { passTypesApi } from '../../api';
import { fxGetList, $data, $isLoading } from './pass-types-select.model';

fxGetList.use(passTypesApi.getPassTypes);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.strategyList)) {
      return [];
    }

    return result.strategyList.map((item) => ({
      id: item.id,
      title: item?.title || '',
      slug: item.slug || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
