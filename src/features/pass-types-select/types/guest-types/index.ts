export type PassTypesPayload = {
  building_id: number;
};

export type PassTypesResponse = {
  strategyList: PassType[];
};

export type PassType = {
  id: number;
  title: string;
  slug: string;
};
