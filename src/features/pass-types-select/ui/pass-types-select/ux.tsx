import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, fxGetList, $isLoading } from '../../models/pass-types-select';

type SelectFieldProps = {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
};

type Props = {
  buildingId?: number;
};

export const PassTypesSelect: FC<Props> = ({ buildingId, ...props }) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    if (buildingId) {
      fxGetList({ building_id: buildingId });
    } else {
      fxGetList();
    }
  }, [buildingId]);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const PassTypesSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PassTypesSelect />} onChange={onChange} {...props} />;
};
