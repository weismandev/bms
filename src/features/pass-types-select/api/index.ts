import { api } from '@api/api2';
import { PassTypesPayload } from '../types';

const getPassTypes = (payload: PassTypesPayload) =>
  api.v4('get', 'scud-pass/strategy/list', payload);

export const passTypesApi = {
  getPassTypes,
};
