import { api } from '@api/api2';

const getOriginList = () => api.v1('get', 'tck/origins/list');

export const ticketOriginApi = {
  getOriginList,
};
