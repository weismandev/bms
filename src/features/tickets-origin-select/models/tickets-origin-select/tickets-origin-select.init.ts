import { signout } from '@features/common';
import { ticketOriginApi } from '../../api';
import { fxGetList, $data, $isLoading } from './tickets-origin-select.model';

fxGetList.use(ticketOriginApi.getOriginList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.origins)) {
      return [];
    }

    return result.origins.map((item) => ({
      id: item.name,
      name: item.name,
      title: item.title,
      number: item.id,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
