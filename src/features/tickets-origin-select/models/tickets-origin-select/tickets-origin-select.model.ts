import { createEffect, createStore } from 'effector';
import { OriginResponse, OriginItem } from '../../interfaces';

const fxGetList = createEffect<void, OriginResponse, Error>();

const $data = createStore<OriginItem[] | []>([]);

const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
