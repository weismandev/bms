import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { OriginItem } from '../../interfaces';
import { $data, fxGetList, $isLoading } from '../../models';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  isNew?: boolean;
  exclude?: string[];
}

const OriginSelect: FC<Props> = ({ isNew, exclude, ...restProps }) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  const options = isNew
    ? exclude && exclude.length > 0
      ? data.filter(
          (option: { id: string }) => !exclude?.find((item) => item === option.id)
        )
      : data
    : data;

  return <SelectControl options={options} isLoading={isLoading} {...restProps} />;
};

const OriginSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: OriginItem) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<OriginSelect />} onChange={onChange} {...props} />;
};

export { OriginSelect, OriginSelectField };
