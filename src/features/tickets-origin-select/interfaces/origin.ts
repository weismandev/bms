export interface OriginItem {
  id: string;
  name: string;
  title: string;
}

export interface OriginResponse {
  origins: OriginItem[];
}
