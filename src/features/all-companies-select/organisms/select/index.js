import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $error, $isLoading, SelectGate } from '../../model';

const AllCompanies = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <SelectGate />
      <SelectControl
        options={options}
        isLoading={isLoading}
        getOptionValue={(option) => option && option.director_id}
        error={error}
        {...props}
      />
    </>
  );
};

const AllCompaniesField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<AllCompanies />} onChange={onChange} {...props} />;
};

export { AllCompanies, AllCompaniesField };
