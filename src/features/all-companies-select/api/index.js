import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'enterprises/index', payload);

export const tenantApi = { getList };
