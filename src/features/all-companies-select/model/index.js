import { createStore, createEffect, guard, sample } from 'effector';
import { createGate } from 'effector-react';
import { tenantApi } from '../api';

const SelectGate = createGate();

const fxGetList = createEffect();

const $data = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);

fxGetList.use(tenantApi.getList);

$data
  .on(fxGetList.doneData, (state, data) => (Array.isArray(data) ? data : state))
  .reset(SelectGate.close);

$error.on(fxGetList.fail, (state, { error }) => error).reset(SelectGate.close);

$isLoading.on(fxGetList.pending, (state, pending) => pending).reset(SelectGate.close);

guard({
  source: sample({
    source: { isLoading: $isLoading, data: $data },
    clock: SelectGate.open,
  }),
  filter: ({ isLoading, data = [] }) => !isLoading && !data.length,
  target: fxGetList,
});

export { $data, $error, $isLoading, fxGetList, SelectGate };
