import { useState, useEffect } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { IconButton } from '@mui/material';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import makeStyles from '@mui/styles/makeStyles';
import { ActionButton } from '@ui/index';
import { Divider, Modal, Loader, CloseButton } from '../../../ui';
import {
  VideoGate,
  $videoFullScreen,
  $video,
  changedVideoFullScreen,
} from '../model/video.model';

const useStyles = makeStyles({
  fullwidth: {
    width: '100%',
    minWidth: '80%',
  },
  video: {
    width: '100%',
    height: '300px',
  },
});

const $stores = combine({
  video: $video,
  videoFullScreen: $videoFullScreen,
});

const Video = (props) => {
  const {
    src = '',
    preview,
    additionalStyles = { width: '100%', height: '200px' },
  } = props;

  const { video, videoFullScreen } = useStore($stores);
  const [videoLoaded, changedVideoLoaded] = useState(false);

  const classes = useStyles();

  useEffect(() => {
    changedVideoLoaded(false);
  }, [src]);

  const srcdoc = `
    <html style="height: 100%;">
      <head>
        <meta name="viewport" content="width=device-width, minimum-scale=0.1">
      </head>
      <body style="margin: 0px;width: 100%; height: 100%; background: black">

      <img alt="Камера" style="object-fit: contain; width: 100%; height: 100%; display: block;-webkit-user-select: none; margin: 0 auto" src=${video}>

      </body>
    </html>
  `;

  const content = (
    <>
      <iframe
        onLoad={() => changedVideoLoaded(true)}
        height="300px"
        width="100%"
        frameborder="0"
        align="center"
        srcdoc={srcdoc}
        style={
          videoFullScreen
            ? {
                position: 'fixed',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                width: '100%',
                height: '100%',
                border: 'none',
                margin: 0,
                padding: 0,
                overflow: 'hidden',
                zIndex: 888888,
              }
            : null
        }
      />
      {videoFullScreen && (
        <CloseButton
          style={{
            position: 'fixed',
            top: 0,
            right: 0,
            width: '50px',
            height: 'auto',
            border: 'none',
            margin: '0 auto',
            padding: 0,
            color: 'white',
            overflow: 'hidden',
            zIndex: 999999,
            borderRadius: 5,
            background: 'black',
          }}
          onClick={() => changedVideoFullScreen(false)}
        />
      )}
    </>
  );

  return (
    <>
      <VideoGate state={{ src, preview }} />
      <div style={{ position: 'relative' }}>
        {content}
        {videoLoaded ? (
          <IconButton
            onClick={() => changedVideoFullScreen(true)}
            style={{
              borderRadius: 'unset',
              position: 'absolute',
              right: 0,
              top: 0,
            }}
            size="large"
          >
            <FullscreenIcon style={{ color: 'white' }} />
          </IconButton>
        ) : (
          <div style={additionalStyles}>
            <Loader isLoading />
          </div>
        )}
      </div>
      <Divider />
    </>
  );
};

export { Video };
