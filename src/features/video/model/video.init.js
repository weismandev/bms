import { sample } from 'effector';
import {
  VideoGate,
  $videoFullScreen,
  $video,
  changedVideoFullScreen,
  updatedVideo,
} from './video.model';

$video.on(updatedVideo, (_, video) => video);

$videoFullScreen.on(changedVideoFullScreen, (_, fullscreen) => fullscreen);

sample({
  source: VideoGate.state,
  fn: ({ state }) => state && state.src,
  target: updatedVideo.prepend(processingSrc),
});

function processingSrc(src) {
  if (typeof src === 'string' && !src.startsWith('https')) {
    return `https://${src}`;
  }
  return src;
}
