import { createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';

const VideoGate = createGate();

const updatedVideo = createEvent();
const changedVideoFullScreen = createEvent();

const $video = createStore(null);
const $videoFullScreen = createStore(false);

export { VideoGate, updatedVideo, changedVideoFullScreen, $video, $videoFullScreen };
