import { api } from '@api/api2';
import { ChartPayload, GetErrorsEquipmentPayload } from '../interfaces';

const getPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-periods', payload);
const exportPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-periods', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getOrigins = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-origins', payload);
const exportOrigins = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-origins', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getInitiators = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-initiators', payload);
const exportInitiators = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-initiators', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-assignees', payload);
const exportAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-assignees', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-objects', payload);
const exportObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/ticket-objects', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getTypes = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/type/ticket-levels', payload);
const exportTypes = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/type/ticket-levels', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getLocations = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/type/ticket-locations', payload);
const exportLocations = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/type/ticket-locations', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getRatingsPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-periods', payload);
const exportRatingsPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-periods', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getRatingsObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-objects', payload);
const exportRatingsObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-objects', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getRatingsAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-assignees', payload);
const exportRatingsAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/rating/ticket-assignees', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getStatusesPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-periods', payload);
const exportStatusesPeriods = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-periods', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getStatusesObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-objects', payload);
const exportStatusesObjects = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-objects', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getStatusesAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-assignees', payload);
const exportStatusesAssignees = (payload: ChartPayload) =>
  api.v4('post', 'ticket/report/status/ticket-assignees', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });
const getPositions = () => api.v4('get', 'position/list');
const getErrorsEquipment = (payload: GetErrorsEquipmentPayload) =>
  api.no_vers(
    'post',
    'https://api-o-mon-platform.ujin.tech/api/stat/incident/universal.stat/class_for_object',
    payload
  );
const getCategoriesDevices = () =>
  api.no_vers(
    'post',
    'https://api.ujin.tech/api/device_categories/get-device-catigories',
    {
      tree: 1,
    }
  );
const getTypesList = () => api.v1('get', 'tck/types/list');

export const reportsApi = {
  getPeriods,
  exportPeriods,
  getOrigins,
  exportOrigins,
  getInitiators,
  exportInitiators,
  getAssignees,
  exportAssignees,
  getObjects,
  exportObjects,
  getTypes,
  exportTypes,
  getLocations,
  exportLocations,
  getRatingsPeriods,
  exportRatingsPeriods,
  getRatingsObjects,
  exportRatingsObjects,
  getRatingsAssignees,
  exportRatingsAssignees,
  getStatusesPeriods,
  exportStatusesPeriods,
  getStatusesObjects,
  exportStatusesObjects,
  getStatusesAssignees,
  exportStatusesAssignees,
  getPositions,
  getErrorsEquipment,
  getCategoriesDevices,
  getTypesList,
};
