export interface RatingsPeriodsResponse {
  ticket_periods: RatingPeriods[];
}

export interface RatingPeriods {
  period: string;
  rating_number: Rating;
  rating_percent: Rating;
}

export interface Rating {
  less: number;
  more: number;
  none: number;
}

export interface RatingPeriodItem {
  month: string;
  ratingNone: number;
  ratingLess: number;
  ratingMore: number;
}

export interface RatingsObjectsResponse {
  ticket_objects: RatingObject[];
}

export interface RatingObject {
  object_title: string;
  rating_number: Rating;
  rating_percent: Rating;
}

export interface RatingItem {
  name: string;
  ratingNone: number;
  ratingLess: number;
  ratingMore: number;
}

export interface RatingsAssigneesResponse {
  ticket_assignees: AssigneesObject[];
}

export interface AssigneesObject {
  user_name: string;
  position: string;
  rating_number: Rating;
  rating_percent: Rating;
}
