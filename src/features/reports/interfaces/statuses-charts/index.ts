export interface StatusPeriodsResponse {
  ticket_periods: StatusPeriods[];
}

export interface StatusPeriods {
  period: string;
  status_number: Status;
  status_percent: Status;
}

export interface StatusPeriodItem {
  month: string;
  new: number;
  assigned: number;
  work: number;
  paused: number;
  done: number;
  returned: number;
  canceled: number;
  closed: number;
  confirmation: number;
  denied: number;
}

export interface Status {
  new: number;
  assigned: number;
  work: number;
  paused: number;
  done: number;
  returned: number;
  canceled: number;
  closed: number;
  confirmation: number;
  denied: number;
}

export interface StatusObjects {
  object_title: string;
  status_number: Status;
  status_percent: Status;
}

export interface StatusObjectsResponse {
  ticket_objects: StatusObjects[];
}

export interface StatusItem {
  name: string;
  new: number;
  assigned: number;
  work: number;
  paused: number;
  done: number;
  returned: number;
  canceled: number;
  closed: number;
  confirmation: number;
  denied: number;
}

export interface StatusAssignees {
  user_name: string;
  position: string;
  status_number: Status;
  status_percent: Status;
}

export interface StatusAssigneesResponse {
  ticket_assignees: StatusAssignees[];
}
