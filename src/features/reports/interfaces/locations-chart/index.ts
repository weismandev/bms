export interface LocationsResponse {
  ticket_locations: Location[];
}

export interface Location {
  number: number;
  location: string;
  address?: string;
  fullLocation?: string;
}

export interface LocationValue {
  [key: string]: string;
}
