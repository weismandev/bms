export * from './periods-chart';
export * from './general-charts';
export * from './positions';
export * from './types-charts';
export * from './locations-chart';
export * from './ratings-charts';
export * from './statuses-charts';
export * from './errors-equipment-charts';

export interface ChartPayload {
  period_from?: string;
  period_to?: string;
  complex_ids?: number[];
  building_ids?: number[];
  statuses?: string[];
  origins?: number[];
  is_guaranteed?: boolean;
  ticket_type_level_1?: number[];
  ticket_type_level_2?: number[];
  ticket_type_level_3?: number[];
  priorities?: (string | number)[];
  is_complexes_on?: boolean;
  is_buildings_on?: boolean;
  positions?: (string | number)[];
  export?: boolean;
  level?: number;
}

export interface Value {
  id: number | string;
}

export interface Filters {
  period_from: string;
  period_to: string;
  objects: [];
  houses: [];
  statuses: [];
  origins: [];
  is_guaranteed: { id: boolean };
  types: any[];
  categories: [];
}
