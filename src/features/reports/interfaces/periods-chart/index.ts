export interface PeriodsResponse {
  ticket_periods: Periods[];
}

export interface Periods {
  number: number;
  period: string;
  month?: string;
}
