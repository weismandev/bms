export interface GetOriginsResponse {
  ticket_periods: OriginsItem[];
}

export interface OriginsItem {
  number: number;
  name: string;
  fullName?: string;
}

export interface GetInitiatorsResponse {
  ticket_initiators: InitiatorsItem[];
}

export interface InitiatorsItem {
  number: number;
  user_name?: string;
  object_title?: string;
  name?: string;
  fullName?: string;
}

export interface GetAssigneesResponse {
  ticket_assignees: AssigneesItem[];
}

export interface AssigneesItem {
  number: number;
  user_name?: string;
  position?: string | null;
  name?: string;
  fullName?: string;
}

export interface GetObjectsResponse {
  ticket_objects: ObjectsItem[];
}

export interface ObjectsItem {
  number: number;
  object_title?: string;
  name?: string;
}

export interface GeneralValues {
  [key: string]: string;
}
