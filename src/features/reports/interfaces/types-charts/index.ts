export interface TypesResponse {
  ticket_levels: Type[];
}

export interface Type {
  number: number;
  title: string;
  fullTitle?: string;
  percent?: string;
}

export interface TypeValue {
  [key: string]: {
    value: string;
    fullTitle: string;
  };
}

export interface TicketsTypesResponse {
  types: TypesData[];
}

export interface TypesData {
  id: number;
  title: string;
  parent_id: null | number;
  children: number[] | [];
  planned_duration_minutes: null | number;
  archived: boolean;
  paid: null | {
    prepayment_type: string;
    price_lower: number;
    price_type: string;
    price_upper: number;
  };
}
