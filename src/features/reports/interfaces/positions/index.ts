export interface PositionsResponse {
  positionList: string[];
}

export interface Positions {
  id: string;
  title: string;
}
