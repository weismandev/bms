export interface GetErrorsEquipmentPayload {
  date_start?: string;
  date_end?: string;
  tag_type?: string;
  complex_id?: string;
  building_id?: string;
  category_id?: string;
}

export interface GetErrorsEquipmentResponse {
  category: ErrorsEquipmentCategory[];
  object: ErrorsEquipmentObject;
}

export interface ErrorsEquipmentCategory {
  id?: number;
  title: string;
  count: number;
}

export interface ErrorsEquipmentObject {
  id: number;
  title: string;
  category: ErrorsEquipmentCategory[];
}

export interface ErrorsObjects {
  [key: string]: number;
}

export interface CategoriesDevicesResponse {
  'device-catigories': CategoryDevice[];
}

export interface CategoryDevice {
  id: number;
  title: string;
}
