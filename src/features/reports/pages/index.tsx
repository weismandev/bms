import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, ErrorMessage, Loader } from '@ui/index';
import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isLoading,
  $isFilterOpen,
} from '../models';
import { Main, Filters } from '../organisms';

const ReportsPage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Main />}
        detail={null}
        params={{ filterWidth: '350px' }}
      />
    </>
  );
};

const RestrictedReportsPage: FC = () => (
  <HaveSectionAccess>
    <ReportsPage />
  </HaveSectionAccess>
);

export { RestrictedReportsPage as ReportsPage };
