import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import {
  ChartPayload,
  Location,
  LocationsResponse,
  LocationValue,
} from '../../interfaces';

export const maxLocationsOnChart = 8;

export const LocationsGate = createGate();

export const locationsUnmounted = LocationsGate.close;
export const locationsMounted = LocationsGate.open;

export const exportLocations = createEvent<void>();

export const fxGetLocations = createEffect<ChartPayload, LocationsResponse, Error>();
export const fxExportLocations = createEffect<ChartPayload, ArrayBuffer, Error>();

export const $locations = createStore<Location[]>([]);
export const $locationsValues = createStore<LocationValue>({});
