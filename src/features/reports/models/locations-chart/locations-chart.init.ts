import { guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { ChartPayload, Filters, Location, LocationValue } from '../../interfaces';
import { formatBaseFilters, formatPeriodsFilters, getMaxValue } from '../../libs';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  fxGetLocations,
  $locations,
  locationsMounted,
  locationsUnmounted,
  exportLocations,
  fxExportLocations,
  maxLocationsOnChart,
  $locationsValues,
} from './locations-chart.model';

const formatPayload = (filters: Filters) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodsFilters(filters);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

guard({
  clock: [locationsMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'locations',
  target: fxGetLocations.prepend(({ filters }: { filters: Filters }) =>
    formatPayload(filters)
  ),
});

const formatLocation = (locations: Location[]) => {
  if (locations.length === 0) {
    return [];
  }

  const locationsWithLocationLength = locations
    .slice(0, maxLocationsOnChart)
    .map((item) => ({
      number: item.number,
      location: item.location,
      length: item?.location ? item.location.length : 0,
    }));

  const locationsWithAddressLength = locations
    .slice(0, maxLocationsOnChart)
    .map((item) => ({
      number: item.number,
      address: item.address,
      length: item?.address ? item.address.length : 0,
    }));

  const maxLocationLength = getMaxValue(locationsWithLocationLength).length;
  const maxLocationValue = getMaxValue(locationsWithLocationLength).location;

  const maxAddressLength = getMaxValue(locationsWithAddressLength).length;
  const maxAddressValue = getMaxValue(locationsWithAddressLength).address;

  const maxLength =
    maxLocationLength >= maxAddressLength ? maxLocationLength - 5 : maxAddressLength - 5;

  const maxValue =
    maxLocationLength >= maxAddressLength ? maxLocationValue : maxAddressValue;

  return locations.map((item) => {
    const location = item?.location
      ? item.location.length === maxLength && item.location !== maxValue
        ? `${item.location.slice(0, maxLength - 5)}...`
        : item.location.length > maxLength
        ? `${item.location.slice(0, maxLength - 5)}...`
        : item.location
      : '';

    const address = item?.address
      ? item.address.length === maxLength && item.address !== maxValue
        ? `${item.address.slice(0, maxLength - 5)}...`
        : item.address.length > maxLength
        ? `${item.address.slice(0, maxLength - 5)}...`
        : item.address
      : '';

    return {
      number: item.number,
      location: `${location}\n${address}`,
      fullLocation: `${item.location.trim()}, ${item.address}`,
    };
  });
};

$locations
  .on(fxGetLocations.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_locations)) {
      return [];
    }

    return formatLocation(result.ticket_locations);
  })
  .reset([locationsUnmounted, signout]);

$locationsValues
  .on(fxGetLocations.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_locations)) {
      return {};
    }

    const formattedLocations = formatLocation(result.ticket_locations);

    return formattedLocations.reduce((acc: LocationValue, value: Location) => {
      const id = `${value.number}${value.location}`;

      const newValue = {
        [id]: value.fullLocation || value.location,
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([locationsUnmounted, signout]);

sample({
  clock: exportLocations,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    payload.export = true;

    return payload;
  },
  target: fxExportLocations,
});

fxExportLocations.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-locations-${dateTimeNow}.xls`);
});
