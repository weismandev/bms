import { guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import {
  ChartPayload,
  Value,
  Filters,
  OriginsItem,
  GeneralValues,
  InitiatorsItem,
  AssigneesItem,
} from '../../interfaces';
import { formatBaseFilters, formatPeriodsFilters, getMaxValue } from '../../libs';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  generalMounted,
  generalUnmounted,
  fxGetOrigins,
  $origins,
  fxGetInitiators,
  $initiators,
  $assignees,
  fxGetAssignees,
  $prioritiesAssignees,
  changePrioritiesAssignees,
  $objects,
  fxGetObjects,
  $isCheckedBuilding,
  changeIsCheckedBuilding,
  $positionsAssignees,
  changePositionAssignees,
  exportObjects,
  fxExportObjects,
  exportOrigins,
  fxExportOrigins,
  exportInitiators,
  fxExportInitiators,
  exportAssignees,
  fxExportAssignees,
  maxOriginsOnChart,
  $originsValues,
  maxInitiatorsOnChart,
  $initiatorsValues,
  $assigneesValues,
  maxAssigneesOnChart,
} from './general-charts.model';

const formatPayload = (
  filters: Filters,
  priorities?: Value[],
  isCheckedBuilding?: boolean,
  positions?: Value[]
) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodsFilters(filters);
  const baseFilters = formatBaseFilters(filters);

  if (priorities && priorities.length > 0) {
    const formattedPriorities = priorities.map((item) => item.id);

    payload.priorities = formattedPriorities;
  }

  if (isCheckedBuilding === false) {
    payload.is_complexes_on = true;
  }

  if (isCheckedBuilding === true) {
    payload.is_buildings_on = true;
  }

  if (positions && positions.length > 0) {
    const formattedPositions = positions.map((item) => item.id);

    payload.positions = formattedPositions;
  }

  return { ...payload, ...baseFilters, ...periodsFilters };
};

guard({
  clock: [generalMounted, $filters],
  source: { filters: $filters, currentTab: $currentTab },
  filter: ({ currentTab }) => currentTab === 'general',
  target: fxGetOrigins.prepend(({ filters }: { filters: Filters }) =>
    formatPayload(filters)
  ),
});

const formatOrigins = (origins: OriginsItem[]) => {
  if (origins.length === 0) {
    return [];
  }

  const originsWithNameLength = origins.slice(0, maxOriginsOnChart).map((item) => ({
    number: item.number,
    name: item.name,
    length: item.name.length,
  }));

  const maxLength = getMaxValue(originsWithNameLength).length;
  const maxValue = getMaxValue(originsWithNameLength).name;

  return origins.map((item) => {
    const name =
      item.name.length === maxLength && item.name !== maxValue
        ? `${item.name.slice(0, maxLength - 5)}...`
        : item.name.length > maxLength
        ? `${item.name.slice(0, maxLength - 5)}...`
        : item.name;

    return {
      number: item.number,
      fullName: item.name,
      name,
    };
  });
};

$origins
  .on(fxGetOrigins.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_periods)) {
      return [];
    }

    const formattedOrigins = formatOrigins(result.ticket_periods);

    return [...formattedOrigins].reverse();
  })
  .reset([generalUnmounted, signout]);

$originsValues
  .on(fxGetOrigins.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_periods)) {
      return {};
    }

    const formattedOrigins = formatOrigins(result.ticket_periods);

    return formattedOrigins.reduce((acc: GeneralValues, value: OriginsItem) => {
      const id = `${value.number}${value.name}`;

      const newValue = {
        [id]: value.fullName || value.name,
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([generalUnmounted, signout]);

guard({
  clock: [generalMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'general',
  target: fxGetInitiators.prepend(({ filters }: { filters: Filters }) =>
    formatPayload(filters)
  ),
});

const formatInitiators = (initiators: InitiatorsItem[]) => {
  if (initiators.length === 0) {
    return [];
  }

  const initiatorsWithObjectLength = initiators
    .slice(0, maxInitiatorsOnChart)
    .map((item) => ({
      number: item.number,
      object_title: item.object_title,
      length: item?.object_title ? item.object_title.length : 0,
    }));

  const initiatorsWithUsersLength = initiators
    .slice(0, maxInitiatorsOnChart)
    .map((item) => ({
      number: item.number,
      user_name: item.user_name,
      length: item?.user_name ? item.user_name.length : 0,
    }));

  const maxObjectsLength = getMaxValue(initiatorsWithObjectLength).length;
  const maxObjectsValue = getMaxValue(initiatorsWithObjectLength).object_title;

  const maxUsersLength = getMaxValue(initiatorsWithUsersLength).length;
  const maxUsersValue = getMaxValue(initiatorsWithUsersLength).user_name;

  const maxLength =
    maxObjectsLength >= maxUsersLength ? maxObjectsLength : maxUsersLength;
  const maxValue = maxObjectsLength >= maxUsersLength ? maxObjectsValue : maxUsersValue;

  return initiators.map((item) => {
    const object_title = item?.object_title
      ? item.object_title.length === maxLength && item.object_title !== maxValue
        ? `${item.object_title.slice(0, maxLength - 5)}...`
        : item.object_title.length > maxLength
        ? `${item.object_title.slice(0, maxLength - 5)}...`
        : item.object_title
      : '';

    const user_name = item?.user_name
      ? item.user_name.length === maxLength && item.user_name !== maxValue
        ? `${item.user_name.slice(0, maxLength - 5)}...`
        : item.user_name.length > maxLength
        ? `${item.user_name.slice(0, maxLength - 5)}...`
        : item.user_name
      : '';

    return {
      number: item.number,
      name: `${user_name}\n${object_title}`,
      fullName: `${item.user_name}, ${item.object_title}`,
    };
  });
};

$initiators
  .on(fxGetInitiators.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_initiators)) {
      return [];
    }

    const formattedInitiators = formatInitiators(result.ticket_initiators);

    return [...formattedInitiators].reverse();
  })
  .reset([generalUnmounted, signout]);

$initiatorsValues
  .on(fxGetInitiators.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_initiators)) {
      return {};
    }

    const formattedInitiators = formatInitiators(result.ticket_initiators);

    return formattedInitiators.reduce((acc: GeneralValues, value: InitiatorsItem) => {
      const id = `${value.number}${value.name}`;

      const newValue = {
        [id]: value.fullName ? value.fullName : value.name || '',
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([generalUnmounted, signout]);

guard({
  clock: [generalMounted, $filters, $prioritiesAssignees, $positionsAssignees],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    priorities: $prioritiesAssignees,
    positions: $positionsAssignees,
  },
  filter: ({ currentTab }) => currentTab === 'general',
  target: fxGetAssignees.prepend(
    ({
      filters,
      priorities,
      positions,
    }: {
      filters: Filters;
      priorities: Value[];
      positions: Value[];
    }) => formatPayload(filters, priorities, undefined, positions)
  ),
});

const formatAssignees = (assignees: AssigneesItem[]) => {
  if (assignees.length === 0) {
    return [];
  }

  const assigneesWithUserLength = assignees.slice(0, maxAssigneesOnChart).map((item) => ({
    number: item.number,
    user_name: item.user_name,
    length: item?.user_name ? item.user_name.length : 0,
  }));

  const assigneesWithPositionLength = assignees
    .slice(0, maxInitiatorsOnChart)
    .map((item) => ({
      number: item.number,
      position: item.position,
      length: item?.position ? item.position.length : 0,
    }));

  const maxUserLength = getMaxValue(assigneesWithUserLength).length;
  const maxUserValue = getMaxValue(assigneesWithUserLength).user_name;

  const maxPositionLength = getMaxValue(assigneesWithPositionLength).length;
  const maxPositionValue = getMaxValue(assigneesWithPositionLength).position;

  const maxLength =
    maxUserLength >= maxPositionLength ? maxUserLength : maxPositionLength;

  const maxValue = maxUserLength >= maxPositionLength ? maxUserValue : maxPositionValue;

  return assignees.map((item) => {
    const position = item?.position
      ? item.position.length === maxLength && item.position !== maxValue
        ? `${item.position.slice(0, maxLength - 5)}...`
        : item.position.length > maxLength
        ? `${item.position.slice(0, maxLength - 5)}...`
        : item.position
      : '';

    const user_name = item?.user_name
      ? item.user_name.length === maxLength && item.user_name !== maxValue
        ? `${item.user_name.slice(0, maxLength - 5)}...`
        : item.user_name.length > maxLength
        ? `${item.user_name.slice(0, maxLength - 5)}...`
        : item.user_name
      : '';

    return {
      number: item.number,
      name: `${user_name}\n${position}`,
      fullName: `${item.user_name}, ${item.position}`,
    };
  });
};

$assignees
  .on(fxGetAssignees.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_assignees)) {
      return [];
    }

    const formattedAssignees = formatAssignees(result.ticket_assignees);

    return [...formattedAssignees].reverse();
  })
  .reset([generalUnmounted, signout]);

$assigneesValues
  .on(fxGetAssignees.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_assignees)) {
      return {};
    }

    const formattedAssignees = formatAssignees(result.ticket_assignees);

    return formattedAssignees.reduce((acc: GeneralValues, value: AssigneesItem) => {
      const id = `${value.number}${value.name}`;

      const newValue = {
        [id]: value.fullName ? value.fullName : value.name || '',
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([generalUnmounted, signout]);

$prioritiesAssignees.on(changePrioritiesAssignees, (_, value) => value).reset(signout);

guard({
  clock: [generalMounted, $filters, $isCheckedBuilding],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    isCheckedObject: $isCheckedBuilding,
  },
  filter: ({ currentTab }) => currentTab === 'general',
  target: fxGetObjects.prepend(
    ({ filters, isCheckedObject }: { filters: Filters; isCheckedObject: boolean }) =>
      formatPayload(filters, undefined, isCheckedObject)
  ),
});

$objects
  .on(fxGetObjects.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_objects)) {
      return [];
    }

    return result.ticket_objects.map((item) => ({
      number: item.number,
      name: item.object_title,
    }));
  })
  .reset([generalUnmounted, signout]);

$isCheckedBuilding.on(changeIsCheckedBuilding, (_, value) => value).reset(signout);

$positionsAssignees.on(changePositionAssignees, (_, value) => value).reset(signout);

sample({
  clock: exportObjects,
  source: { filters: $filters, isCheckedObject: $isCheckedBuilding },
  fn: ({ filters, isCheckedObject }) => {
    const payload = formatPayload(filters, undefined, isCheckedObject);

    return { ...payload, ...{ export: true } };
  },
  target: fxExportObjects,
});

fxExportObjects.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-objects-${dateTimeNow}.xls`);
});

sample({
  clock: exportOrigins,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    return { ...payload, ...{ export: true } };
  },
  target: fxExportOrigins,
});

fxExportOrigins.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-origins-${dateTimeNow}.xls`);
});

sample({
  clock: exportInitiators,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    return { ...payload, ...{ export: true } };
  },
  target: fxExportInitiators,
});

fxExportInitiators.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-initiators-${dateTimeNow}.xls`);
});

sample({
  clock: exportAssignees,
  source: {
    filters: $filters,
    priorities: $prioritiesAssignees,
    positions: $positionsAssignees,
  },
  fn: ({ filters, priorities, positions }) => {
    const payload = formatPayload(filters, priorities, undefined, positions);

    return { ...payload, ...{ export: true } };
  },
  target: fxExportAssignees,
});

fxExportAssignees.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-assignees-${dateTimeNow}.xls`);
});
