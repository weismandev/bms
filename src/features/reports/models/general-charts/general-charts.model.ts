import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import {
  GetOriginsResponse,
  OriginsItem,
  ChartPayload,
  GetInitiatorsResponse,
  InitiatorsItem,
  Value,
  GetAssigneesResponse,
  AssigneesItem,
  GetObjectsResponse,
  ObjectsItem,
  GeneralValues,
} from '../../interfaces';

export const maxOriginsOnChart = 8;
export const maxInitiatorsOnChart = 8;
export const maxAssigneesOnChart = 8;

export const GeneralGate = createGate();

export const generalUnmounted = GeneralGate.close;
export const generalMounted = GeneralGate.open;

export const changePrioritiesAssignees = createEvent<Value[]>();
export const changeIsCheckedBuilding = createEvent<boolean>();
export const changePositionAssignees = createEvent<Value[]>();
export const exportObjects = createEvent<void>();
export const exportOrigins = createEvent<void>();
export const exportInitiators = createEvent<void>();
export const exportAssignees = createEvent<void>();

export const fxGetOrigins = createEffect<ChartPayload, GetOriginsResponse, Error>();
export const fxGetInitiators = createEffect<ChartPayload, GetInitiatorsResponse, Error>();
export const fxGetAssignees = createEffect<ChartPayload, GetAssigneesResponse, Error>();
export const fxGetObjects = createEffect<ChartPayload, GetObjectsResponse, Error>();
export const fxExportObjects = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxExportOrigins = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxExportInitiators = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxExportAssignees = createEffect<ChartPayload, ArrayBuffer, Error>();

export const $origins = createStore<OriginsItem[]>([]);
export const $originsValues = createStore<GeneralValues>({});
export const $initiators = createStore<InitiatorsItem[]>([]);
export const $initiatorsValues = createStore<GeneralValues>({});
export const $assignees = createStore<AssigneesItem[]>([]);
export const $prioritiesAssignees = createStore<Value[]>([]);
export const $assigneesValues = createStore<GeneralValues>({});
export const $objects = createStore<ObjectsItem[]>([]);
export const $isCheckedBuilding = createStore<boolean>(false);
export const $positionsAssignees = createStore<Value[]>([]);
