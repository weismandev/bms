import { merge, sample } from 'effector';
import { pending, debounce } from 'patronum';

import { signout } from '@features/common';

import { reportsApi } from '../../api';
import { fxGetPeriods, fxExportPeriods } from '../periods-chart/periods-chart.model';
import {
  fxGetOrigins,
  fxGetInitiators,
  fxGetAssignees,
  fxGetObjects,
  fxExportObjects,
  fxExportOrigins,
  fxExportInitiators,
  fxExportAssignees,
} from '../general-charts/general-charts.model';
import {
  fxGetTypesLevelOne,
  fxGetTypesLevelTwo,
  fxGetTypesLevelThree,
  fxExportTypesLevelOne,
  fxExportTypesLevelTwo,
  fxExportTypesLevelThree,
  fxGetTypes,
} from '../types-charts/types-charts.model';
import {
  fxGetLocations,
  fxExportLocations,
} from '../locations-chart/locations-chart.model';
import {
  fxGetRatingsPeriods,
  fxExportPeriodsRatings,
  fxGetRatingsObjects,
  fxExportObjectsRatings,
  fxGetRatingsBuildings,
  fxExportBuildingsRatings,
  fxGetRatingsAssignees,
  fxExportAssigneesRatings,
} from '../ratings-charts/ratings-charts.model';
import {
  fxGetStatusesPeriods,
  fxExportStatusesPeriods,
  fxGetStatusesObjects,
  fxExportObjectsStatuses,
  fxGetStatusesBuildings,
  fxExportStatusesBuildings,
  fxGetStatusesAssignees,
  fxExportStatusesAssignees,
} from '../statuses-charts/statuses-charts.model';
import { changedFilterVisibility } from '../filters/filters.model';
import {
  fxGetErrorsComplex,
  fxGetErrorsBuildings,
} from '../errors-equipment-charts/errors-equipment-charts.model';
import {
  $currentTab,
  changeTab,
  pageUnmounted,
  $isErrorDialogOpen,
  $isLoading,
  $error,
  changedErrorDialogVisibility,
  $isForceRender,
} from './page.model';

fxGetPeriods.use(reportsApi.getPeriods);
fxGetOrigins.use(reportsApi.getOrigins);
fxGetInitiators.use(reportsApi.getInitiators);
fxGetAssignees.use(reportsApi.getAssignees);
fxGetObjects.use(reportsApi.getObjects);
fxExportPeriods.use(reportsApi.exportPeriods);
fxExportObjects.use(reportsApi.exportObjects);
fxExportOrigins.use(reportsApi.exportOrigins);
fxExportInitiators.use(reportsApi.exportInitiators);
fxExportAssignees.use(reportsApi.exportAssignees);
fxGetTypesLevelOne.use(reportsApi.getTypes);
fxGetTypesLevelTwo.use(reportsApi.getTypes);
fxGetTypesLevelThree.use(reportsApi.getTypes);
fxExportTypesLevelOne.use(reportsApi.exportTypes);
fxExportTypesLevelTwo.use(reportsApi.exportTypes);
fxExportTypesLevelThree.use(reportsApi.exportTypes);
fxGetLocations.use(reportsApi.getLocations);
fxExportLocations.use(reportsApi.exportLocations);
fxGetRatingsPeriods.use(reportsApi.getRatingsPeriods);
fxExportPeriodsRatings.use(reportsApi.exportRatingsPeriods);
fxGetRatingsObjects.use(reportsApi.getRatingsObjects);
fxExportObjectsRatings.use(reportsApi.exportRatingsObjects);
fxGetRatingsBuildings.use(reportsApi.getRatingsObjects);
fxExportBuildingsRatings.use(reportsApi.exportRatingsObjects);
fxGetRatingsAssignees.use(reportsApi.getRatingsAssignees);
fxExportAssigneesRatings.use(reportsApi.exportRatingsAssignees);
fxGetStatusesPeriods.use(reportsApi.getStatusesPeriods);
fxExportStatusesPeriods.use(reportsApi.exportStatusesPeriods);
fxGetStatusesObjects.use(reportsApi.getStatusesObjects);
fxExportObjectsStatuses.use(reportsApi.exportStatusesObjects);
fxGetStatusesBuildings.use(reportsApi.getStatusesObjects);
fxExportStatusesBuildings.use(reportsApi.exportStatusesObjects);
fxGetStatusesAssignees.use(reportsApi.getStatusesAssignees);
fxExportStatusesAssignees.use(reportsApi.exportStatusesAssignees);
fxGetErrorsComplex.use(reportsApi.getErrorsEquipment);
fxGetErrorsBuildings.use(reportsApi.getErrorsEquipment);
fxGetTypes.use(reportsApi.getTypesList);

const errorOccured = merge([
  fxGetPeriods.fail,
  fxGetOrigins.fail,
  fxGetInitiators.fail,
  fxGetAssignees.fail,
  fxGetObjects.fail,
  fxExportPeriods.fail,
  fxExportObjects.fail,
  fxExportOrigins.fail,
  fxExportInitiators.fail,
  fxExportAssignees.fail,
  fxGetTypesLevelOne.fail,
  fxGetTypesLevelTwo.fail,
  fxGetTypesLevelThree.fail,
  fxExportTypesLevelOne.fail,
  fxExportTypesLevelTwo.fail,
  fxExportTypesLevelThree.fail,
  fxGetLocations.fail,
  fxExportLocations.fail,
  fxGetRatingsPeriods.fail,
  fxExportPeriodsRatings.fail,
  fxGetRatingsObjects.fail,
  fxExportObjectsRatings.fail,
  fxGetRatingsBuildings.fail,
  fxExportBuildingsRatings.fail,
  fxGetRatingsAssignees.fail,
  fxExportAssigneesRatings.fail,
  fxGetStatusesPeriods.fail,
  fxExportStatusesPeriods.fail,
  fxGetStatusesObjects.fail,
  fxExportObjectsStatuses.fail,
  fxGetStatusesBuildings.fail,
  fxExportStatusesBuildings.fail,
  fxGetStatusesAssignees.fail,
  fxExportStatusesAssignees.fail,
  fxGetErrorsComplex.fail,
  fxGetErrorsBuildings.fail,
  fxGetTypes.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetPeriods,
        fxGetOrigins,
        fxGetInitiators,
        fxGetAssignees,
        fxGetObjects,
        fxExportPeriods,
        fxExportObjects,
        fxExportOrigins,
        fxExportInitiators,
        fxExportAssignees,
        fxGetTypesLevelOne,
        fxGetTypesLevelTwo,
        fxGetTypesLevelThree,
        fxExportTypesLevelOne,
        fxExportTypesLevelTwo,
        fxExportTypesLevelThree,
        fxGetLocations,
        fxExportLocations,
        fxGetRatingsPeriods,
        fxExportPeriodsRatings,
        fxGetRatingsObjects,
        fxExportObjectsRatings,
        fxGetRatingsBuildings,
        fxExportBuildingsRatings,
        fxGetRatingsAssignees,
        fxExportAssigneesRatings,
        fxGetStatusesPeriods,
        fxExportStatusesPeriods,
        fxGetStatusesObjects,
        fxExportObjectsStatuses,
        fxGetStatusesBuildings,
        fxExportStatusesBuildings,
        fxGetStatusesAssignees,
        fxExportStatusesAssignees,
        fxGetErrorsComplex,
        fxGetErrorsBuildings,
        fxGetTypes,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$isForceRender.on(changedFilterVisibility, () => true).reset([pageUnmounted, signout]);

sample({
  clock: debounce({
    source: changedFilterVisibility,
    timeout: 1,
  }),
  fn: () => false,
  target: $isForceRender,
});

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$currentTab.on(changeTab, (_, tab) => tab).reset([signout, pageUnmounted]);
