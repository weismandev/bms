import { createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const tabs = [
  {
    label: t('Periods'),
    value: 'periods',
  },
  {
    label: t('General'),
    value: 'general',
  },
  {
    label: t('Types'),
    value: 'types',
  },
  {
    label: t('Locations'),
    value: 'locations',
  },
  {
    label: t('Ratings'),
    value: 'ratings',
  },
  {
    label: t('Statuses'),
    value: 'statuses',
  },
  {
    label: t('HardwareErrorRating'),
    value: 'errors-equipment',
  },
];

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const changeTab = createEvent<string>();

export const $currentTab = createStore<string>('periods');
export const $isLoading = createStore<boolean>(false);
export const $isForceRender = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
