import { createEffect, createStore } from 'effector';
import { Positions, PositionsResponse } from '../../interfaces';

const fxGetList = createEffect<void, PositionsResponse, Error>();

const $data = createStore<Positions[]>([]);
const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
