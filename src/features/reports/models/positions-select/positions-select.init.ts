import { signout } from '@features/common';
import { reportsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './positions-select.model';

fxGetList.use(reportsApi.getPositions);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.positionList)) {
      return [];
    }

    return result.positionList.map((item) => ({
      id: item,
      title: item,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
