import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { ChartPayload, TypesResponse, Type, TypeValue, TicketsTypesResponse, TypesData } from '../../interfaces';

export const maxTypesOnChart = 8;

export const TypesGate = createGate();

export const typesUnmounted = TypesGate.close;
export const typesMounted = TypesGate.open;

export const exportTypesLevelOne = createEvent<void>();
export const exportTypesLevelTwo = createEvent<void>();
export const exportTypesLevelThree = createEvent<void>();
export const selectOneLevel = createEvent<string | number | Date>();
export const selectTwoLevel = createEvent<string | number | Date>();
export const selectThreeLevel = createEvent<string | number | Date>();

export const fxGetTypesLevelOne = createEffect<ChartPayload, TypesResponse, Error>();
export const fxGetTypesLevelTwo = createEffect<ChartPayload, TypesResponse, Error>();
export const fxGetTypesLevelThree = createEffect<ChartPayload, TypesResponse, Error>();
export const fxExportTypesLevelOne = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxExportTypesLevelTwo = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxExportTypesLevelThree = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetTypes = createEffect<void, TicketsTypesResponse, Error>();

export const $types = createStore<TypesData[]>([]);
export const $typesLevelOne = createStore<Type[]>([]);
export const $typesLevelOneValues = createStore<TypeValue>({});
export const $typesLevelTwo = createStore<Type[]>([]);
export const $typesLevelTwoValues = createStore<TypeValue>({});
export const $typesLevelThree = createStore<Type[]>([]);
export const $typesLevelThreeValues = createStore<TypeValue>({});
export const $selectedOneLevel = createStore<number[]>([]);
export const $selectedTwoLevel = createStore<number[]>([]);
export const $selectedThreeLevel = createStore<number[]>([]);
