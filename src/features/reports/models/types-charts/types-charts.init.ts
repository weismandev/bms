import { guard, sample, forward } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';

import { signout } from '@features/common';

import { ChartPayload, Type, TypeValue, Filters } from '../../interfaces';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import { formatBaseFilters, formatPeriodsFilters, getMaxValue } from '../../libs';
import {
  fxGetTypesLevelOne,
  typesMounted,
  typesUnmounted,
  $typesLevelOne,
  $typesLevelTwo,
  fxGetTypesLevelTwo,
  $typesLevelThree,
  fxGetTypesLevelThree,
  $typesLevelOneValues,
  $typesLevelTwoValues,
  $typesLevelThreeValues,
  exportTypesLevelOne,
  fxExportTypesLevelOne,
  exportTypesLevelTwo,
  fxExportTypesLevelTwo,
  exportTypesLevelThree,
  fxExportTypesLevelThree,
  maxTypesOnChart,
  $selectedOneLevel,
  selectOneLevel,
  $selectedTwoLevel,
  selectTwoLevel,
  $selectedThreeLevel,
  selectThreeLevel,
  fxGetTypes,
  $types,
} from './types-charts.model';

const formatPayload = (filters: Filters) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodsFilters(filters);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

$types.on(fxGetTypes.done, (_, { result }) => {
  if (!Array.isArray(result?.types)) {
    return [];
  }

  return result.types;
}).reset([typesUnmounted, signout]);

forward({
  from: typesMounted,
  to: fxGetTypes,
});

guard({
  clock: [typesMounted, $filters, $selectedOneLevel],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    selectedOneLevel: $selectedOneLevel,
  },
  filter: ({ currentTab }) => currentTab === 'types',
  target: fxGetTypesLevelOne.prepend(
    ({ filters, selectedOneLevel }: { filters: Filters; selectedOneLevel: number[] }) => {
      const payload = formatPayload(filters);

      if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
        payload.ticket_type_level_1 = selectedOneLevel;
      }

      payload.level = 1;

      return payload;
    }
  ),
});

const formatTypes = (types: Type[]) => {
  if (types.length === 0) {
    return [];
  }

  const typesWithTitleLength = types.slice(0, maxTypesOnChart).map((item) => ({
    number: item.number,
    title: item.title,
    length: item.title.length,
  }));

  const maxLength = getMaxValue(typesWithTitleLength).length;
  const maxValue = getMaxValue(typesWithTitleLength).title;

  return types.map((item) => {
    const title =
      item.title.length === maxLength && item.title !== maxValue
        ? `${item.title.slice(0, maxLength - 5)}...`
        : item.title.length > maxLength
        ? `${item.title.slice(0, maxLength - 5)}...`
        : item.title;

    return {
      number: item.number,
      fullTitle: item.title,
      percent: item.percent,
      title,
    };
  });
};

$typesLevelOne
  .on(fxGetTypesLevelOne.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return [];
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return [...formattedTypes].reverse();
  })
  .reset([typesUnmounted, signout]);

$typesLevelOneValues
  .on(fxGetTypesLevelOne.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return {};
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return formattedTypes.reduce((acc: TypeValue, value: Type) => {
      const id = `${value.number}${value.title}`;

      const newValue = {
        [id]: {
          value: `${value.number} (${value.percent}%)`,
          fullTitle: value.fullTitle || '',
        },
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([typesUnmounted, signout]);

guard({
  clock: [typesMounted, $filters, $selectedOneLevel, $selectedTwoLevel],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    selectedOneLevel: $selectedOneLevel,
    selectedTwoLevel: $selectedTwoLevel,
  },
  filter: ({ currentTab }) => currentTab === 'types',
  target: fxGetTypesLevelTwo.prepend(
    ({
      filters,
      selectedOneLevel,
      selectedTwoLevel,
    }: {
      filters: Filters;
      selectedOneLevel: number[];
      selectedTwoLevel: number[];
    }) => {
      const payload = formatPayload(filters);

      if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
        payload.ticket_type_level_1 = selectedOneLevel;
      }

      if (Array.isArray(selectedTwoLevel) && selectedTwoLevel.length > 0) {
        payload.ticket_type_level_2 = selectedTwoLevel;
      }

      payload.level = 2;

      return payload;
    }
  ),
});

$typesLevelTwo
  .on(fxGetTypesLevelTwo.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return [];
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return [...formattedTypes].reverse();
  })
  .reset([typesUnmounted, signout]);

$typesLevelTwoValues
  .on(fxGetTypesLevelTwo.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return {};
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return formattedTypes.reduce((acc: TypeValue, value: Type) => {
      const id = `${value.number}${value.title}`;

      const newValue = {
        [id]: {
          value: `${value.number} (${value.percent}%)`,
          fullTitle: value.fullTitle || '',
        },
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([typesUnmounted, signout]);

guard({
  clock: [
    typesMounted,
    $filters,
    $selectedOneLevel,
    $selectedTwoLevel,
    $selectedThreeLevel,
  ],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    selectedOneLevel: $selectedOneLevel,
    selectedTwoLevel: $selectedTwoLevel,
    selectedThreeLevel: $selectedThreeLevel,
  },
  filter: ({ currentTab }) => currentTab === 'types',
  target: fxGetTypesLevelThree.prepend(
    ({
      filters,
      selectedOneLevel,
      selectedTwoLevel,
      selectedThreeLevel,
    }: {
      filters: Filters;
      selectedOneLevel: number[];
      selectedTwoLevel: number[];
      selectedThreeLevel: number[];
    }) => {
      const payload = formatPayload(filters);

      if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
        payload.ticket_type_level_1 = selectedOneLevel;
      }

      if (Array.isArray(selectedTwoLevel) && selectedTwoLevel.length > 0) {
        payload.ticket_type_level_2 = selectedTwoLevel;
      }

      if (Array.isArray(selectedThreeLevel) && selectedThreeLevel.length > 0) {
        payload.ticket_type_level_3 = selectedThreeLevel;
      }

      payload.level = 3;

      return payload;
    }
  ),
});

$typesLevelThree
  .on(fxGetTypesLevelThree.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return [];
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return [...formattedTypes].reverse();
  })
  .reset([typesUnmounted, signout]);

$typesLevelThreeValues
  .on(fxGetTypesLevelThree.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_levels)) {
      return {};
    }

    const formattedTypes = formatTypes(result.ticket_levels);

    return formattedTypes.reduce((acc: TypeValue, value: Type) => {
      const id = `${value.number}${value.title}`;

      const newValue = {
        [id]: {
          value: `${value.number} (${value.percent}%)`,
          fullTitle: value.fullTitle || '',
        },
      };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([typesUnmounted, signout]);

sample({
  clock: exportTypesLevelOne,
  source: { filters: $filters, selectedOneLevel: $selectedOneLevel },
  fn: ({ filters, selectedOneLevel }) => {
    const payload = formatPayload(filters);

    payload.level = 1;
    payload.export = true;

    if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
      payload.ticket_type_level_1 = selectedOneLevel;
    }

    return payload;
  },
  target: fxExportTypesLevelOne,
});

fxExportTypesLevelOne.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-types-1-${dateTimeNow}.xls`);
});

sample({
  clock: exportTypesLevelTwo,
  source: {
    filters: $filters,
    selectedOneLevel: $selectedOneLevel,
    selectedTwoLevel: $selectedTwoLevel,
  },
  fn: ({ filters, selectedOneLevel, selectedTwoLevel }) => {
    const payload = formatPayload(filters);

    payload.level = 2;
    payload.export = true;

    if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
      payload.ticket_type_level_1 = selectedOneLevel;
    }

    if (Array.isArray(selectedTwoLevel) && selectedTwoLevel.length > 0) {
      payload.ticket_type_level_2 = selectedTwoLevel;
    }

    return payload;
  },
  target: fxExportTypesLevelTwo,
});

fxExportTypesLevelTwo.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-types-2-${dateTimeNow}.xls`);
});

sample({
  clock: exportTypesLevelThree,
  source: {
    filters: $filters,
    selectedOneLevel: $selectedOneLevel,
    selectedTwoLevel: $selectedTwoLevel,
    selectedThreeLevel: $selectedThreeLevel,
  },
  fn: ({ filters, selectedOneLevel, selectedTwoLevel, selectedThreeLevel }) => {
    const payload = formatPayload(filters);

    payload.level = 3;
    payload.export = true;

    if (Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0) {
      payload.ticket_type_level_1 = selectedOneLevel;
    }

    if (Array.isArray(selectedTwoLevel) && selectedTwoLevel.length > 0) {
      payload.ticket_type_level_2 = selectedTwoLevel;
    }

    if (Array.isArray(selectedThreeLevel) && selectedThreeLevel.length > 0) {
      payload.ticket_type_level_3 = selectedThreeLevel;
    }

    return payload;
  },
  target: fxExportTypesLevelThree,
});

fxExportTypesLevelThree.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-types-3-${dateTimeNow}.xls`);
});

sample({
  clock: selectOneLevel,
  source: $types,
  fn: (types, selectedType) => {
    if (typeof selectedType === 'string' && selectedType.length === 0) {
      return [];
    }

    const findedTypeId = types.find((type) => type.title === selectedType);

    if (findedTypeId) {
      return [findedTypeId.id];
    }

    return [];
  },
  target: $selectedOneLevel,
});

$selectedOneLevel.reset([typesUnmounted, signout]);

sample({
  clock: selectTwoLevel,
  source: $types,
  fn: (types, selectedType) => {
    if (typeof selectedType === 'string' && selectedType.length === 0) {
      return [];
    }

    const findedTypeId = types.find((type) => type.title === selectedType);

    if (findedTypeId) {
      return [findedTypeId.id];
    }

    return [];
  },
  target: $selectedTwoLevel,
});

$selectedTwoLevel.reset([typesUnmounted, signout]);

sample({
  clock: selectThreeLevel,
  source: $types,
  fn: (types, selectedType) => {
    if (typeof selectedType === 'string' && selectedType.length === 0) {
      return [];
    }

    const findedTypeId = types.find((type) => type.title === selectedType);

    if (findedTypeId) {
      return [findedTypeId.id];
    }

    return [];
  },
  target: $selectedThreeLevel,
});

$selectedThreeLevel.reset([typesUnmounted, signout]);
