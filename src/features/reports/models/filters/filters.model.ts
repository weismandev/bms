import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  period_from: '',
  period_to: '',
  objects: [],
  houses: [],
  statuses: [],
  origins: [],
  is_guaranteed: '',
  types: [],
  categories: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
