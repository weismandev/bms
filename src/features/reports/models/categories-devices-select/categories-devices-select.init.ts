import { signout } from '@features/common';
import { reportsApi } from '../../api';
import { CategoryDevice } from '../../interfaces';
import { fxGetList, $data, $isLoading } from './categories-devices-select.model';

fxGetList.use(reportsApi.getCategoriesDevices);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.['device-catigories'])) {
      return [];
    }

    return result['device-catigories'].reduce(
      (acc: CategoryDevice[], item) =>
        item.title === 'Объекты клининга'
          ? acc
          : [...acc, { id: item.id, title: item.title }],

      []
    );
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
