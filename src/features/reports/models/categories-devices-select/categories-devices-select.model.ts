import { createEffect, createStore } from 'effector';
import { CategoriesDevicesResponse, CategoryDevice } from '../../interfaces';

export const fxGetList = createEffect<void, CategoriesDevicesResponse, Error>();

export const $data = createStore<CategoryDevice[]>([]);
export const $isLoading = createStore<boolean>(false);
