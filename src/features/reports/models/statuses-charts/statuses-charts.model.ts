import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import {
  ChartPayload,
  StatusPeriodsResponse,
  StatusPeriodItem,
  StatusObjectsResponse,
  StatusItem,
  StatusAssigneesResponse,
  Value,
} from '../../interfaces';

export const StatusesGate = createGate();

export const statusesUnmounted = StatusesGate.close;
export const statusesMounted = StatusesGate.open;

export const changeEndYearPeriodsStatuses = createEvent<Date>();
export const exportPeriodsStatuses = createEvent<void>();
export const exportObjectsStatuses = createEvent<void>();
export const exportBuildingsStatuses = createEvent<void>();
export const changePositionStatusAssignees = createEvent<Value[]>();
export const exportAssigneesStatuses = createEvent<void>();

export const fxGetStatusesPeriods = createEffect<
  ChartPayload,
  StatusPeriodsResponse,
  Error
>();
export const fxExportStatusesPeriods = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetStatusesObjects = createEffect<
  ChartPayload,
  StatusObjectsResponse,
  Error
>();
export const fxExportObjectsStatuses = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetStatusesBuildings = createEffect<
  ChartPayload,
  StatusObjectsResponse,
  Error
>();
export const fxExportStatusesBuildings = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetStatusesAssignees = createEffect<
  ChartPayload,
  StatusAssigneesResponse,
  Error
>();
export const fxExportStatusesAssignees = createEffect<ChartPayload, ArrayBuffer, Error>();

export const $statusesPeriods = createStore<StatusPeriodItem[]>([]);
export const $endYearPeriodsStatuses = createStore<Date>(new Date());
export const $statusesObjects = createStore<StatusItem[]>([]);
export const $statusesBuildings = createStore<StatusItem[]>([]);
export const $statusesAssignees = createStore<StatusItem[]>([]);
export const $positionsStatusAssignees = createStore<Value[]>([]);
