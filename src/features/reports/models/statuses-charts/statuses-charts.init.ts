import { guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { ChartPayload, Value, Filters } from '../../interfaces';
import {
  formatBaseFilters,
  getMonth,
  formatPeriodFilters,
  formatPeriodsFilters,
} from '../../libs';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  statusesMounted,
  $endYearPeriodsStatuses,
  fxGetStatusesPeriods,
  $statusesPeriods,
  statusesUnmounted,
  changeEndYearPeriodsStatuses,
  exportPeriodsStatuses,
  fxExportStatusesPeriods,
  $statusesObjects,
  fxGetStatusesObjects,
  exportObjectsStatuses,
  fxExportObjectsStatuses,
  $statusesBuildings,
  fxGetStatusesBuildings,
  fxExportStatusesBuildings,
  exportBuildingsStatuses,
  $positionsStatusAssignees,
  changePositionStatusAssignees,
  $statusesAssignees,
  fxGetStatusesAssignees,
  fxExportStatusesAssignees,
  exportAssigneesStatuses,
} from './statuses-charts.model';

const formatPayloadForPeriods = (filters: Filters, endYearPeriodStatuses: Date) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodFilters(endYearPeriodStatuses);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

const formatPayload = (filters: Filters) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodsFilters(filters);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

guard({
  clock: [statusesMounted, $filters, $endYearPeriodsStatuses],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    endYearPeriodStatuses: $endYearPeriodsStatuses,
  },
  filter: ({ currentTab }) => currentTab === 'statuses',
  target: fxGetStatusesPeriods.prepend(
    ({
      filters,
      endYearPeriodStatuses,
    }: {
      filters: Filters;
      endYearPeriodStatuses: Date;
    }) => formatPayloadForPeriods(filters, endYearPeriodStatuses)
  ),
});

$statusesPeriods
  .on(fxGetStatusesPeriods.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_periods)) {
      return [];
    }

    return result.ticket_periods.map((item) => ({
      month: getMonth(item.period),
      new: item.status_percent.new,
      assigned: item.status_percent.assigned,
      work: item.status_percent.work,
      paused: item.status_percent.paused,
      done: item.status_percent.done,
      returned: item.status_percent.returned,
      canceled: item.status_percent.canceled,
      closed: item.status_percent.closed,
      confirmation: item.status_percent.confirmation,
      denied: item.status_percent.denied,
    }));
  })
  .reset([signout, statusesUnmounted]);

$endYearPeriodsStatuses
  .on(changeEndYearPeriodsStatuses, (_, year) => year)
  .reset(signout);

sample({
  clock: exportPeriodsStatuses,
  source: { filters: $filters, endYearPeriodStatuses: $endYearPeriodsStatuses },
  fn: ({ filters, endYearPeriodStatuses }) => {
    const payload = formatPayloadForPeriods(filters, endYearPeriodStatuses);

    payload.export = true;

    return payload;
  },
  target: fxExportStatusesPeriods,
});

fxExportStatusesPeriods.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-statuses-periods-${dateTimeNow}.xls`
  );
});

guard({
  clock: [statusesMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'statuses',
  target: fxGetStatusesObjects.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ is_complexes_on: true },
  })),
});

$statusesObjects
  .on(fxGetStatusesObjects.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_objects)) {
      return [];
    }

    return result.ticket_objects.map((item) => ({
      name: item.object_title,
      new: item.status_percent.new,
      assigned: item.status_percent.assigned,
      work: item.status_percent.work,
      paused: item.status_percent.paused,
      done: item.status_percent.done,
      returned: item.status_percent.returned,
      canceled: item.status_percent.canceled,
      closed: item.status_percent.closed,
      confirmation: item.status_percent.confirmation,
      denied: item.status_percent.denied,
    }));
  })
  .reset([signout, statusesUnmounted]);

sample({
  clock: exportObjectsStatuses,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    payload.is_complexes_on = true;
    payload.export = true;

    return payload;
  },
  target: fxExportObjectsStatuses,
});

fxExportObjectsStatuses.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-statuses-objects-${dateTimeNow}.xls`
  );
});

guard({
  clock: [statusesMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'statuses',
  target: fxGetStatusesBuildings.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ is_buildings_on: true },
  })),
});

$statusesBuildings
  .on(fxGetStatusesBuildings.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_objects)) {
      return [];
    }

    return result.ticket_objects.map((item) => ({
      name: item.object_title,
      new: item.status_percent.new,
      assigned: item.status_percent.assigned,
      work: item.status_percent.work,
      paused: item.status_percent.paused,
      done: item.status_percent.done,
      returned: item.status_percent.returned,
      canceled: item.status_percent.canceled,
      closed: item.status_percent.closed,
      confirmation: item.status_percent.confirmation,
      denied: item.status_percent.denied,
    }));
  })
  .reset([signout, statusesUnmounted]);

sample({
  clock: exportBuildingsStatuses,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    payload.is_buildings_on = true;
    payload.export = true;

    return payload;
  },
  target: fxExportStatusesBuildings,
});

fxExportStatusesBuildings.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-statuses-buildings-${dateTimeNow}.xls`
  );
});

$positionsStatusAssignees
  .on(changePositionStatusAssignees, (_, value) => value)
  .reset(signout);

guard({
  clock: [statusesMounted, $filters, $positionsStatusAssignees],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    positions: $positionsStatusAssignees,
  },
  filter: ({ currentTab }) => currentTab === 'statuses',
  target: fxGetStatusesAssignees.prepend(
    ({ filters, positions }: { filters: Filters; positions: Value[] }) => {
      const payload = formatPayload(filters);

      if (positions && positions.length > 0) {
        const formattedPositions = positions.map((item) => item.id);

        return { ...payload, ...{ positions: formattedPositions } };
      }

      return payload;
    }
  ),
});

$statusesAssignees
  .on(fxGetStatusesAssignees.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_assignees)) {
      return [];
    }

    return result.ticket_assignees.map((item) => ({
      name: `${item.user_name}\n${item.position || ''}`,
      new: item.status_percent.new,
      assigned: item.status_percent.assigned,
      work: item.status_percent.work,
      paused: item.status_percent.paused,
      done: item.status_percent.done,
      returned: item.status_percent.returned,
      canceled: item.status_percent.canceled,
      closed: item.status_percent.closed,
      confirmation: item.status_percent.confirmation,
      denied: item.status_percent.denied,
    }));
  })
  .reset([signout, statusesUnmounted]);

sample({
  clock: exportAssigneesStatuses,
  source: { filters: $filters, positions: $positionsStatusAssignees },
  fn: ({ filters, positions }) => {
    const payload = formatPayload(filters);

    payload.export = true;

    if (positions && positions.length > 0) {
      const formattedPositions = positions.map((item) => item.id);

      return { ...payload, ...{ positions: formattedPositions } };
    }

    return payload;
  },
  target: fxExportStatusesAssignees,
});

fxExportStatusesAssignees.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-statuses-assignees-${dateTimeNow}.xls`
  );
});
