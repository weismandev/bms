import { forward, guard } from 'effector';
import format from 'date-fns/format';
import { signout } from '@features/common';
import {
  ErrorsEquipmentCategory,
  ErrorsObjects,
  Filters,
  GetErrorsEquipmentPayload,
  Value,
} from '../../interfaces';
import { fxGetList as fxGetCategoriesDevices } from '../categories-devices-select/categories-devices-select.model';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  errorsEquipmentMounted,
  errorsEquipmentUnmounted,
  $errorsEquipmentComplex,
  $errorsEquipmentCategories,
  $errorsEquipmentBuildings,
  fxGetErrorsComplex,
  fxGetErrorsBuildings,
} from './errors-equipment-charts.model';

const formatPayload = (filters: Filters) => {
  const payload: GetErrorsEquipmentPayload = {};

  const currentDate = format(new Date(), 'yyyy-MM-dd');

  if (filters.period_from.length > 0) {
    const period_from = format(new Date(filters.period_from), 'yyyy-MM-dd');

    payload.date_start = period_from;
  } else {
    payload.date_start = currentDate;
  }

  if (filters.period_to.length > 0) {
    const period_to = format(new Date(filters.period_to), 'yyyy-MM-dd');

    payload.date_end = period_to;
  } else {
    payload.date_end = currentDate;
  }

  if (filters.objects.length > 0) {
    const formattedComplex = filters.objects.map((item: Value) => item.id).join(',');

    payload.complex_id = formattedComplex;
  }

  if (filters.houses.length > 0) {
    const formattedHouses = filters.houses.map((item: Value) => item.id).join(',');

    payload.building_id = formattedHouses;
  }

  if (filters.categories.length > 0) {
    const formattedCategories = filters.categories
      .map((item: Value) => item.id)
      .join(',');

    payload.category_id = formattedCategories;
  }

  return payload;
};

guard({
  clock: [errorsEquipmentMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'errors-equipment',
  target: fxGetErrorsComplex.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ tag_type: 'complex' },
  })),
});

forward({
  from: errorsEquipmentMounted,
  to: fxGetCategoriesDevices,
});

$errorsEquipmentComplex
  .on(fxGetErrorsComplex.done, (_, { result }) => {
    if (!Array.isArray(result?.object)) {
      return [];
    }

    return result.object.map((item) => {
      const categories: ErrorsEquipmentCategory[] = Object.values(item.category);

      const formattedCategories = categories.reduce(
        (acc: ErrorsObjects, value: ErrorsEquipmentCategory) => ({
          ...acc,
          ...{ [value.title]: value.count },
        }),
        {}
      );

      return {
        title: item.title,
        ...formattedCategories,
      };
    });
  })
  .reset([signout, errorsEquipmentUnmounted]);

$errorsEquipmentCategories
  .on(fxGetErrorsComplex.done, (_, { result }) => {
    if (!Array.isArray(result?.category)) {
      return [];
    }

    return result.category.map((item) => ({
      title: item.title,
      count: item.count,
    }));
  })
  .reset([signout, errorsEquipmentUnmounted]);

guard({
  clock: [errorsEquipmentMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'errors-equipment',
  target: fxGetErrorsBuildings.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ tag_type: 'building' },
  })),
});

$errorsEquipmentBuildings
  .on(fxGetErrorsBuildings.done, (_, { result }) => {
    if (!Array.isArray(result?.object)) {
      return [];
    }

    return result.object.map((item) => {
      const categories: ErrorsEquipmentCategory[] = Object.values(item.category);

      const formattedCategories = categories.reduce(
        (acc: ErrorsObjects, value: ErrorsEquipmentCategory) => ({
          ...acc,
          ...{ [value.title]: value.count },
        }),
        {}
      );

      return {
        title: item.title,
        ...formattedCategories,
      };
    });
  })
  .reset([signout, errorsEquipmentUnmounted]);
