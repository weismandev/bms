import { createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';
import {
  GetErrorsEquipmentResponse,
  ErrorsObjects,
  ErrorsEquipmentCategory,
  GetErrorsEquipmentPayload,
} from '../../interfaces';

export const ErrorsEquipmentGate = createGate();

export const errorsEquipmentUnmounted = ErrorsEquipmentGate.close;
export const errorsEquipmentMounted = ErrorsEquipmentGate.open;

export const fxGetErrorsComplex = createEffect<
  GetErrorsEquipmentPayload,
  GetErrorsEquipmentResponse,
  Error
>();
export const fxGetErrorsBuildings = createEffect<
  GetErrorsEquipmentPayload,
  GetErrorsEquipmentResponse,
  Error
>();

export const $errorsEquipmentComplex = createStore<ErrorsObjects[]>([]);
export const $errorsEquipmentCategories = createStore<ErrorsEquipmentCategory[]>([]);
export const $errorsEquipmentBuildings = createStore<ErrorsObjects[]>([]);
