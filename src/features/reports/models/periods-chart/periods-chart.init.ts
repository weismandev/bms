import { guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { ChartPayload, Filters } from '../../interfaces';
import { formatBaseFilters, formatPeriodFilters, getMonth } from '../../libs';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  fxGetPeriods,
  periodsMounted,
  periodsUnmounted,
  $periods,
  $endYearPeriod,
  changeEndYearPeriod,
  exportPeriods,
  fxExportPeriods,
} from './periods-chart.model';

const formatPayload = (endYearPeriod: Date, filters: Filters) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodFilters(endYearPeriod);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

guard({
  clock: [periodsMounted, changeEndYearPeriod, $filters],
  source: {
    endYearPeriod: $endYearPeriod,
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'periods',
  target: fxGetPeriods.prepend(
    ({ endYearPeriod, filters }: { endYearPeriod: Date; filters: Filters }) =>
      formatPayload(endYearPeriod, filters)
  ),
});

$periods
  .on(fxGetPeriods.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_periods)) {
      return [];
    }

    return result.ticket_periods
      .map((item) => ({
        number: item.number,
        period: item.period,
        month: getMonth(item.period),
      }))
      .sort((a, b) => Number(new Date(a.period)) - Number(new Date(b.period)));
  })
  .reset([periodsUnmounted, signout]);

$endYearPeriod.on(changeEndYearPeriod, (_, year) => year).reset(signout);

sample({
  clock: exportPeriods,
  source: { endYearPeriod: $endYearPeriod, filters: $filters },
  fn: ({ endYearPeriod, filters }) => {
    const payload = formatPayload(endYearPeriod, filters);

    return { ...payload, ...{ export: true } };
  },
  target: fxExportPeriods,
});

fxExportPeriods.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(URL.createObjectURL(blob), `tickets-periods-${dateTimeNow}.xls`);
});
