import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { PeriodsResponse, Periods, ChartPayload } from '../../interfaces';

export const PeriodsGate = createGate();

export const periodsUnmounted = PeriodsGate.close;
export const periodsMounted = PeriodsGate.open;

export const changeEndYearPeriod = createEvent<Date>();
export const exportPeriods = createEvent<void>();

export const fxGetPeriods = createEffect<ChartPayload, PeriodsResponse, Error>();
export const fxExportPeriods = createEffect<ChartPayload, ArrayBuffer, Error>();

export const $periods = createStore<Periods[]>([]);
export const $endYearPeriod = createStore<Date>(new Date());
