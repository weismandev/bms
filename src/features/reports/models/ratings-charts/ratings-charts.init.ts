import { guard, sample } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { ChartPayload, Value, Filters } from '../../interfaces';
import {
  formatBaseFilters,
  getMonth,
  formatPeriodFilters,
  formatPeriodsFilters,
} from '../../libs';
import { $filters } from '../filters/filters.model';
import { $currentTab } from '../page/page.model';
import {
  ratingsMounted,
  fxGetRatingsPeriods,
  $ratingsPeriods,
  ratingsUnmounted,
  $endYearPeriodsRatings,
  changeEndYearPeriodsRatings,
  exportPeriodsRatings,
  fxExportPeriodsRatings,
  $ratingsObjects,
  fxGetRatingsObjects,
  fxExportObjectsRatings,
  exportObjectsRatings,
  fxGetRatingsBuildings,
  $ratingsBuildings,
  exportBuildingsRatings,
  fxExportBuildingsRatings,
  fxGetRatingsAssignees,
  $ratingsAssignees,
  $positionsRatingAssignees,
  changePositionRatingAssignees,
  exportAssigneesRatings,
  fxExportAssigneesRatings,
  $formattedAssignees,
} from './ratings-charts.model';

const formatPayloadForPeriods = (filters: Filters, endYearPeriodRatings: Date) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodFilters(endYearPeriodRatings);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

const formatPayload = (filters: Filters) => {
  const payload: ChartPayload = {};

  const periodsFilters = formatPeriodsFilters(filters);
  const baseFilters = formatBaseFilters(filters);

  return { ...payload, ...periodsFilters, ...baseFilters };
};

guard({
  clock: [ratingsMounted, $filters, $endYearPeriodsRatings],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    endYearPeriodRatings: $endYearPeriodsRatings,
  },
  filter: ({ currentTab }) => currentTab === 'ratings',
  target: fxGetRatingsPeriods.prepend(
    ({
      filters,
      endYearPeriodRatings,
    }: {
      filters: Filters;
      endYearPeriodRatings: Date;
    }) => formatPayloadForPeriods(filters, endYearPeriodRatings)
  ),
});

$ratingsPeriods
  .on(fxGetRatingsPeriods.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_periods)) {
      return [];
    }

    return result.ticket_periods.map((item) => ({
      ratingNone: item.rating_percent.none,
      ratingLess: item.rating_percent.less,
      ratingMore: item.rating_percent.more,
      month: getMonth(item.period),
    }));
  })
  .reset([signout, ratingsUnmounted]);

$endYearPeriodsRatings.on(changeEndYearPeriodsRatings, (_, year) => year).reset(signout);

sample({
  clock: exportPeriodsRatings,
  source: { filters: $filters, endYearPeriodRatings: $endYearPeriodsRatings },
  fn: ({ filters, endYearPeriodRatings }) => {
    const payload = formatPayloadForPeriods(filters, endYearPeriodRatings);

    payload.export = true;

    return payload;
  },
  target: fxExportPeriodsRatings,
});

fxExportPeriodsRatings.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-ratings-periods-${dateTimeNow}.xls`
  );
});

guard({
  clock: [ratingsMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'ratings',
  target: fxGetRatingsObjects.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ is_complexes_on: true },
  })),
});

$ratingsObjects
  .on(fxGetRatingsObjects.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_objects)) {
      return [];
    }

    return result.ticket_objects.map((item) => ({
      name: item.object_title,
      ratingNone: item.rating_percent.none,
      ratingLess: item.rating_percent.less,
      ratingMore: item.rating_percent.more,
    }));
  })
  .reset([signout, ratingsUnmounted]);

sample({
  clock: exportObjectsRatings,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    payload.is_complexes_on = true;
    payload.export = true;

    return payload;
  },
  target: fxExportObjectsRatings,
});

fxExportObjectsRatings.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-ratings-objects-${dateTimeNow}.xls`
  );
});

guard({
  clock: [ratingsMounted, $filters],
  source: {
    filters: $filters,
    currentTab: $currentTab,
  },
  filter: ({ currentTab }) => currentTab === 'ratings',
  target: fxGetRatingsBuildings.prepend(({ filters }: { filters: Filters }) => ({
    ...formatPayload(filters),
    ...{ is_buildings_on: true },
  })),
});

$ratingsBuildings
  .on(fxGetRatingsBuildings.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_objects)) {
      return [];
    }

    return result.ticket_objects.map((item) => ({
      name: item.object_title,
      ratingNone: item.rating_percent.none,
      ratingLess: item.rating_percent.less,
      ratingMore: item.rating_percent.more,
    }));
  })
  .reset([signout, ratingsUnmounted]);

sample({
  clock: exportBuildingsRatings,
  source: { filters: $filters },
  fn: ({ filters }) => {
    const payload = formatPayload(filters);

    payload.is_buildings_on = true;
    payload.export = true;

    return payload;
  },
  target: fxExportBuildingsRatings,
});

fxExportBuildingsRatings.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-ratings-buildings-${dateTimeNow}.xls`
  );
});

$positionsRatingAssignees
  .on(changePositionRatingAssignees, (_, value) => value)
  .reset(signout);

guard({
  clock: [ratingsMounted, $filters, $positionsRatingAssignees],
  source: {
    filters: $filters,
    currentTab: $currentTab,
    positions: $positionsRatingAssignees,
  },
  filter: ({ currentTab }) => currentTab === 'ratings',
  target: fxGetRatingsAssignees.prepend(
    ({ filters, positions }: { filters: Filters; positions: Value[] }) => {
      const payload = formatPayload(filters);

      if (positions && positions.length > 0) {
        const formattedPositions = positions.map((item) => item.id);

        return { ...payload, ...{ positions: formattedPositions } };
      }

      return payload;
    }
  ),
});

$ratingsAssignees
  .on(fxGetRatingsAssignees.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_assignees)) {
      return [];
    }

    return result.ticket_assignees.map((item) => ({
      name: `${item.user_name}\n${item.position || ''}`,
      ratingNone: item.rating_percent.none,
      ratingLess: item.rating_percent.less,
      ratingMore: item.rating_percent.more,
    }));
  })
  .reset([signout, ratingsUnmounted]);

$formattedAssignees
  .on(fxGetRatingsAssignees.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_assignees)) {
      return {};
    }

    return result.ticket_assignees.reduce(
      (acc, item) => ({
        ...acc,
        ...{
          [`${item.user_name}\n${item.position || ''}`]: {
            ['ratingNone']: item.rating_number.none,
            ['ratingLess']: item.rating_number.less,
            ['ratingMore']: item.rating_number.more,
          },
        },
      }),
      {}
    );
  })
  .reset([signout, ratingsUnmounted]);

sample({
  clock: exportAssigneesRatings,
  source: { filters: $filters, positions: $positionsRatingAssignees },
  fn: ({ filters, positions }) => {
    const payload = formatPayload(filters);

    payload.export = true;

    if (positions && positions.length > 0) {
      const formattedPositions = positions.map((item) => item.id);

      return { ...payload, ...{ positions: formattedPositions } };
    }

    return payload;
  },
  target: fxExportAssigneesRatings,
});

fxExportAssigneesRatings.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `tickets-ratings-assignees-${dateTimeNow}.xls`
  );
});
