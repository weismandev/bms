import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import {
  ChartPayload,
  RatingsPeriodsResponse,
  RatingPeriodItem,
  RatingsObjectsResponse,
  RatingItem,
  RatingsAssigneesResponse,
  Value,
} from '../../interfaces';

export const RatingsGate = createGate();

export const ratingsUnmounted = RatingsGate.close;
export const ratingsMounted = RatingsGate.open;

export const changeEndYearPeriodsRatings = createEvent<Date>();
export const exportPeriodsRatings = createEvent<void>();
export const exportObjectsRatings = createEvent<void>();
export const exportBuildingsRatings = createEvent<void>();
export const changePositionRatingAssignees = createEvent<Value[]>();
export const exportAssigneesRatings = createEvent<void>();

export const fxGetRatingsPeriods = createEffect<
  ChartPayload,
  RatingsPeriodsResponse,
  Error
>();
export const fxExportPeriodsRatings = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetRatingsObjects = createEffect<
  ChartPayload,
  RatingsObjectsResponse,
  Error
>();
export const fxExportObjectsRatings = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetRatingsBuildings = createEffect<
  ChartPayload,
  RatingsObjectsResponse,
  Error
>();
export const fxExportBuildingsRatings = createEffect<ChartPayload, ArrayBuffer, Error>();
export const fxGetRatingsAssignees = createEffect<
  ChartPayload,
  RatingsAssigneesResponse,
  Error
>();
export const fxExportAssigneesRatings = createEffect<ChartPayload, ArrayBuffer, Error>();

export const $ratingsPeriods = createStore<RatingPeriodItem[]>([]);
export const $endYearPeriodsRatings = createStore<Date>(new Date());
export const $ratingsObjects = createStore<RatingItem[]>([]);
export const $ratingsBuildings = createStore<RatingItem[]>([]);
export const $ratingsAssignees = createStore<RatingItem[]>([]);
export const $positionsRatingAssignees = createStore<Value[]>([]);
export const $formattedAssignees = createStore<{
  [key: string]: {
    [key: string]: number;
  };
}>({});
