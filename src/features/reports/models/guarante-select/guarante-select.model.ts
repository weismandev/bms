import i18n from '@shared/config/i18n';

const { t } = i18n;

export const options = [
  { id: true, value: t('Guarantee') },
  { id: false, value: t('NotGuaranteed') },
];
