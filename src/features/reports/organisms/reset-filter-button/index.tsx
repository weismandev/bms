import { FC } from 'react';
import { Tooltip } from '@mui/material';
import { CloseButton } from '@ui/index';

interface Props {
  onClick: () => void;
}

export const ResetFilterButton: FC<Props> = ({ onClick }) => (
  <Tooltip title="Сбросить фильтр">
    <div>
      <CloseButton onClick={onClick} style={{ margin: '4px 0px 0px 15px' }} />
    </div>
  </Tooltip>
);
