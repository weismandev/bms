import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
  ScrollBar,
  ZoomAndPan,
  ArgumentAxis,
  Size,
} from 'devextreme-react/chart';
import { Typography, Switch } from '@mui/material';
import {
  $objects,
  $isForceRender,
  $filters,
  $isCheckedBuilding,
  changeIsCheckedBuilding,
  exportObjects,
  $isLoading,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';

const ObjectsChart: FC = () => {
  const { t } = useTranslation();

  const obj = useStore($objects);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to, objects } = useStore($filters);
  const isCheckedBuilding = useStore($isCheckedBuilding);
  const isLoading = useStore($isLoading);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
  }: {
    originalArgument: string;
    originalValue: string;
  }) => ({
    html: `<div>
      <span style="font-weight: bold;">${
        isCheckedBuilding === false ? `${t('AnObject')}: ` : `${t('building')}: `
      }</span>
      <span style="font-weight: 400;">${originalArgument}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Requests')}: </span>
      <span style="font-weight: 400;">${originalValue}</span>
    </div>`,
  });

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const dateFrom =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const dateTo =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${dateFrom} - ${dateTo}`
    : `${currentDate} - ${currentDate}`;

  const classes = useStyles();

  const visualRange = {
    startValue: obj[0]?.name,
    length: 20,
  };

  const onChange = () => changeIsCheckedBuilding(!isCheckedBuilding);
  const isExportDisabled = isLoading || obj.length === 0;

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  return (
    <div className={classes.wrapper} style={style}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>
            {isCheckedBuilding === false ? t('ByObjects') : t('ByBuildings')}
          </Typography>
          <div className={classes.switchContainer}>
            <Typography classes={{ root: classes.titleSwitch }}>
              {t('objects')}
            </Typography>
            <Switch
              checked={isCheckedBuilding}
              onChange={onChange}
              classes={{
                switchBase: classes.switchBase,
                track: classes.switchTrack,
              }}
            />
            <Typography classes={{ root: classes.titleSwitch }}>
              {t('Buildings')}
            </Typography>
          </div>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
          <ExportButton onClick={exportObjects} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={obj}>
          <Size height={620} />
          <CommonSeriesSettings>
            <Label visible={true} />
          </CommonSeriesSettings>

          <Series valueField="number" argumentField="name" type="bar" color="#5E9FDF">
            <Label
              visible={true}
              verticalOffset={8}
              backgroundColor="none"
              font={{
                size: 16,
                color: '#65657B',
                opacity: 0.7,
                family: 'Roboto',
              }}
            />
          </Series>
          <Legend visible={false} />
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label displayMode="rotate" rotationAngle="45" />
          </ArgumentAxis>
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { ObjectsChart };
