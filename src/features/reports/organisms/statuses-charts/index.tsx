import { FC } from 'react';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '@ui/index';
import { StatusesGate, $filters } from '../../models';
import { styles } from '../main/styles';
import { StatusesAssigneesChart } from '../statuses-assignees-chart';
import { StatusesBuildingsChart } from '../statuses-buildings-chart';
import { StatusesObjectsChart } from '../statuses-objects-chart';
import { StatusesPeriodsChart } from '../statuses-periods-chart';

const StatusesCharts: FC = () => {
  const { objects } = useStore($filters);

  const style =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <StatusesGate />

      <CustomScrollbar style={style}>
        <StatusesPeriodsChart />
        <StatusesObjectsChart />
        <StatusesBuildingsChart />
        <StatusesAssigneesChart />
      </CustomScrollbar>
    </>
  );
};

export { StatusesCharts };
