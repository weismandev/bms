import { FC } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Tooltip,
  ValueAxis,
  Legend,
  Size,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import { InputField } from '@ui/index';
import {
  $ratingsPeriods,
  $endYearPeriodsRatings,
  changeEndYearPeriodsRatings,
  $isForceRender,
  exportPeriodsRatings,
  $isLoading,
  $filters,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';

const { t } = i18n;

const RatingsPeriodsChart: FC = () => {
  const ratingsPeriods = useStore($ratingsPeriods);
  const endYearPeriodRatings = useStore($endYearPeriodsRatings);
  const isForceRender = useStore($isForceRender);
  const isLoading = useStore($isLoading);
  const { objects } = useStore($filters);

  const currentDate = new Date();

  const customizeTooltip = ({
    originalArgument,
    originalValue,
    seriesName,
  }: {
    originalArgument: string;
    originalValue: string;
    seriesName: string;
  }) => {
    const year = endYearPeriodRatings
      ? endYearPeriodRatings.getFullYear()
      : currentDate.getFullYear();

    return {
      html: `<div>
        <span style="font-weight: bold;">${t('Label.date')}: </span>
        <span style="font-weight: 400;">${originalArgument} ${year}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Requests')}: </span>
        <span style="font-weight: 400;">${originalValue}%</span>
      </div>
      <div>
        <span style="font-weight: bold;">Оценка: </span>
        <span style="font-weight: 400;">${seriesName}</span>
      </div>`,
    };
  };

  const customizeValueAxisText = ({ value }: { value: number }) => `${value}%`;

  const onChangeDate = (date: Date) => changeEndYearPeriodsRatings(date);
  const isExportDisabled = isLoading || ratingsPeriods.length === 0;

  const year = endYearPeriodRatings
    ? `${endYearPeriodRatings.getFullYear()} г.`
    : `${currentDate.getFullYear()} г.`;

  const classes = useStyles();

  const customizeItems = (data: []) => [...data].reverse();

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  return (
    <div className={classes.wrapper} style={style}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>{t('ByPeriods')}</Typography>
          <div className={classes.rowYear}>
            <p className={classes.yearLabel}>{t('Year')}</p>
            <DatePicker
              customInput={<InputField label={null} divider={false} />}
              placeholderText={t('SelectYear')}
              selected={endYearPeriodRatings}
              dateFormat="yyyy"
              showYearPicker
              onChange={onChangeDate}
              locale={locale}
            />
          </div>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{year}</Typography>
          <ExportButton onClick={exportPeriodsRatings} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={ratingsPeriods}>
          <Size height={620} />
          <CommonSeriesSettings argumentField="month" type="stackedBar" />
          <Legend customizeItems={customizeItems} />
          <ValueAxis>
            <Label customizeText={customizeValueAxisText} />
          </ValueAxis>
          <Series name={t('NoRating')} valueField="ratingNone" color="#5E9FDF">
            <Label visible={false} />
          </Series>
          <Series name={t('3AndBelow')} valueField="ratingLess" color="#2A50B4">
            <Label visible={false} />
          </Series>
          <Series name={t('4AndAbove')} valueField="ratingMore" color="#71E0F9">
            <Label visible={false} />
          </Series>
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
        </Chart>
      )}
    </div>
  );
};

export { RatingsPeriodsChart };
