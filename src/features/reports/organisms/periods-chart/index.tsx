import { FC } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
} from 'devextreme-react/chart';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { InputField, CustomScrollbar } from '@ui/index';
import {
  PeriodsGate,
  $periods,
  $endYearPeriod,
  changeEndYearPeriod,
  $isForceRender,
  exportPeriods,
  $isLoading,
  $filters,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles, styles } from '../main/styles';

const { t } = i18n;

const PeriodsChart: FC = () => {
  const periods = useStore($periods);
  const endYearPeriod = useStore($endYearPeriod);
  const isForceRender = useStore($isForceRender);
  const isLoading = useStore($isLoading);
  const { objects } = useStore($filters);

  const currentDate = new Date();

  const customizeTooltip = ({
    originalArgument,
    originalValue,
  }: {
    originalArgument: string;
    originalValue: string;
  }) => {
    const year = endYearPeriod ? endYearPeriod.getFullYear() : currentDate.getFullYear();

    return {
      html: `<div>
        <span style="font-weight: bold;">${t('Label.date')}: </span>
        <span style="font-weight: 400;">${originalArgument} ${year}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Requests')}: </span>
        <span style="font-weight: 400;">${originalValue}</span>
      </div>`,
    };
  };

  const onChangeDate = (date: Date) => changeEndYearPeriod(date);
  const isExportDisabled = isLoading || periods.length === 0;

  const year = endYearPeriod
    ? `${endYearPeriod.getFullYear()}`
    : `${currentDate.getFullYear()}`;

  const classes = useStyles();

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  const styleScrollbar =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <PeriodsGate />

      <CustomScrollbar style={styleScrollbar}>
        <div className={classes.wrapper} style={style}>
          <div className={classes.header}>
            <div className={classes.row}>
              <Typography classes={{ root: classes.title }}>{t('ByPeriods')}</Typography>
              <div className={classes.rowYear}>
                <p className={classes.yearLabel}>{t('Year')}</p>
                <DatePicker
                  customInput={<InputField label={null} divider={false} />}
                  placeholderText={t('SelectYear')}
                  selected={endYearPeriod}
                  dateFormat="yyyy"
                  showYearPicker
                  onChange={onChangeDate}
                  locale={dateFnsLocale}
                />
              </div>
            </div>
            <div className={classes.dateRow}>
              <Typography classes={{ root: classes.dateInfo }}>
                {year}
                {' '}
                {t('y.') ?? ''}
              </Typography>
              <ExportButton onClick={exportPeriods} disabled={isExportDisabled} />
            </div>
          </div>
          {!isForceRender && (
            <Chart dataSource={periods}>
              <CommonSeriesSettings>
                <Label visible={true} />
              </CommonSeriesSettings>

              <Series
                valueField="number"
                argumentField="month"
                type="bar"
                color="#5E9FDF"
              >
                <Label
                  visible={true}
                  verticalOffset={8}
                  backgroundColor="none"
                  font={{
                    size: 16,
                    color: '#65657B',
                    opacity: 0.7,
                    family: 'Roboto',
                  }}
                />
              </Series>
              <Legend visible={false} />
              <Tooltip
                enabled={true}
                location="edge"
                customizeTooltip={customizeTooltip}
                border={{
                  color: '#0394E3',
                  width: 3,
                }}
                cornerRadius={8}
                zIndex={9999}
                shared
              />
            </Chart>
          )}
        </div>
      </CustomScrollbar>
    </>
  );
};

export { PeriodsChart };
