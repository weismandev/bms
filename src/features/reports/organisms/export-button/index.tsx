import { FC } from 'react';
import { Tooltip } from '@mui/material';
import ExportIcon from '@mui/icons-material/VerticalAlignBottom';
import i18n from '@shared/config/i18n';
import { ActionIconButton } from '@ui/index';

const { t } = i18n;

const ExportButton: FC<any> = ({ onClick = () => null, ...rest }) => (
  <Tooltip title={`${t('export')}`}>
    <div>
      <ActionIconButton onClick={() => onClick()} {...rest}>
        <ExportIcon titleAccess={t('export')} />
      </ActionIconButton>
    </div>
  </Tooltip>
);

export { ExportButton };
