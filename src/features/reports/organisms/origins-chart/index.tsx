import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  $origins,
  $isForceRender,
  $filters,
  $isLoading,
  exportOrigins,
  maxOriginsOnChart,
  $originsValues,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';

const { t } = i18n;

const OriginsChart: FC = () => {
  const origins = useStore($origins);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to } = useStore($filters);
  const isLoading = useStore($isLoading);
  const originsValues = useStore($originsValues);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
  }: {
    originalArgument: string;
    originalValue: string;
  }) => {
    const id = `${originalValue}${originalArgument}`;

    const argument = originsValues[id] ? originsValues[id] : originalArgument;

    return {
      html: `<div>
      <span style="font-weight: bold;">${t('Source')}: </span>
      <span style="font-weight: 400;">${argument}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Requests')}: </span>
      <span style="font-weight: 400;">${originalValue}</span>
    </div>`,
    };
  };

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const classes = useStyles();

  const visualRange = {
    length: maxOriginsOnChart,
  };

  const isExportDisabled = isLoading || origins.length === 0;

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>
            {t('AccordingToSources')}
          </Typography>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
          <ExportButton onClick={exportOrigins} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={origins} rotated>
          <CommonSeriesSettings>
            <Label visible={true} />
          </CommonSeriesSettings>

          <Series valueField="number" argumentField="name" type="bar" color="#5E9FDF">
            <Label
              visible={true}
              backgroundColor="none"
              font={{
                size: 16,
                color: '#65657B',
                opacity: 0.7,
                family: 'Roboto',
              }}
            />
          </Series>
          <Legend visible={false} />
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange} />
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { OriginsChart };
