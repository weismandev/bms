import { FC } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Tooltip,
  ValueAxis,
  Legend,
  Size,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import { InputField } from '@ui/index';
import {
  $statusesPeriods,
  $endYearPeriodsStatuses,
  changeEndYearPeriodsStatuses,
  $isForceRender,
  exportPeriodsStatuses,
  $isLoading,
  $filters,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';

const { t } = i18n;

const StatusesPeriodsChart: FC = () => {
  const statusesPeriods = useStore($statusesPeriods);
  const endYearPeriodStatuses = useStore($endYearPeriodsStatuses);
  const isForceRender = useStore($isForceRender);
  const isLoading = useStore($isLoading);
  const { objects } = useStore($filters);

  const currentDate = new Date();

  const customizeTooltip = ({
    originalArgument,
    originalValue,
    seriesName,
  }: {
    originalArgument: string;
    originalValue: string;
    seriesName: string;
  }) => {
    const year = endYearPeriodStatuses
      ? endYearPeriodStatuses.getFullYear()
      : currentDate.getFullYear();

    return {
      html: `<div>
        <span style="font-weight: bold;">${t('Label.date')}: </span>
        <span style="font-weight: 400;">${originalArgument} ${year}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Requests')}: </span>
        <span style="font-weight: 400;">${originalValue}%</span>
      </div>
      <div>
        <span style="font-weight: bold;">Статус: </span>
        <span style="font-weight: 400;">${seriesName}</span>
      </div`,
    };
  };

  const customizeValueAxisText = ({ value }: { value: number }) => `${value}%`;

  const onChangeDate = (date: Date) => changeEndYearPeriodsStatuses(date);
  const isExportDisabled = isLoading || statusesPeriods.length === 0;

  const year = endYearPeriodStatuses
    ? `${endYearPeriodStatuses.getFullYear()} ${t('y.')}`
    : `${currentDate.getFullYear()} ${t('y.')}`;

  const classes = useStyles();

  const customizeItems = (data: []) => [...data].reverse();

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  return (
    <div className={classes.wrapper} style={style}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>{t('ByPeriods')}</Typography>
          <div className={classes.rowYear}>
            <p className={classes.yearLabel}>{t('Year')}</p>
            <DatePicker
              customInput={<InputField label={null} divider={false} />}
              placeholderText={t('SelectYear')}
              selected={endYearPeriodStatuses}
              dateFormat="yyyy"
              showYearPicker
              onChange={onChangeDate}
              locale={locale}
            />
          </div>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{year}</Typography>
          <ExportButton onClick={exportPeriodsStatuses} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={statusesPeriods}>
          <Size height={620} />
          <CommonSeriesSettings argumentField="month" type="stackedBar" />
          <Legend customizeItems={customizeItems} />
          <ValueAxis>
            <Label customizeText={customizeValueAxisText} />
          </ValueAxis>
          <Series name={t('New')} valueField="new" color="#5E9FDF">
            <Label visible={false} />
          </Series>
          <Series name={t('Appointed')} valueField="assigned" color="#B388FF">
            <Label visible={false} />
          </Series>
          <Series name={t('InWork')} valueField="work" color="#1DE9B6">
            <Label visible={false} />
          </Series>
          <Series name={t('OnPause')} valueField="paused" color="#33691E">
            <Label visible={false} />
          </Series>
          <Series name={t('OnConfirmation')} valueField="confirmation" color="#416BFF">
            <Label visible={false} />
          </Series>
          <Series name={t('rejected')} valueField="denied" color="#930000">
            <Label visible={false} />
          </Series>
          <Series name={t('Returned')} valueField="returned" color="#FF6F00">
            <Label visible={false} />
          </Series>
          <Series name={t('Canceled')} valueField="canceled" color="#FFEA00">
            <Label visible={false} />
          </Series>
          <Series name={t('Сompleted')} valueField="done" color="#F50057">
            <Label visible={false} />
          </Series>
          <Series name={t('Closed')} valueField="closed" color="#0097A7">
            <Label visible={false} />
          </Series>
          <Tooltip
            enabled
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
        </Chart>
      )}
    </div>
  );
};

export { StatusesPeriodsChart };
