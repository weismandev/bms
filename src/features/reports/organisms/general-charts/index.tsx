import { FC } from 'react';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '@ui/index';
import { GeneralGate, $filters } from '../../models';
import { AssigneesChart } from '../assignees-chart';
import { InitiatorsChart } from '../initiators-chart';
import { styles } from '../main/styles';
import { ObjectsChart } from '../objects-chart';
import { OriginsChart } from '../origins-chart';

const GeneralCharts: FC = () => {
  const { objects } = useStore($filters);

  const style =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <GeneralGate />

      <CustomScrollbar style={style}>
        <ObjectsChart />
        <OriginsChart />
        <InitiatorsChart />
        <AssigneesChart />
      </CustomScrollbar>
    </>
  );
};

export { GeneralCharts };
