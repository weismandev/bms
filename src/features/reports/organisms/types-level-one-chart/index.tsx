import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  $typesLevelOne,
  $isForceRender,
  $filters,
  $isLoading,
  exportTypesLevelOne,
  $typesLevelOneValues,
  maxTypesOnChart,
  selectOneLevel,
  $selectedOneLevel,
  selectTwoLevel,
  selectThreeLevel,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';
import { ResetFilterButton } from '../reset-filter-button';

interface PropsText {
  originalArgument: string;
  originalValue: string;
}

const { t } = i18n;

const ArgumentLabel = ({ value }: { value: string }) => (
  <text
    style={{
      fontSize: 12,
      cursor: 'pointer',
      whiteSpace: 'pre',
      fill: 'rgb(118, 118, 118)',
      fontWeight: 400,
      fontFamily: '"Segoe UI", "Helvetica Neue", "Trebuchet MS", Verdana, sans-serif',
    }}
  >
    {value}
  </text>
);

const TypesLevelOneChart: FC = () => {
  const typesLevelOne = useStore($typesLevelOne);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to, objects } = useStore($filters);
  const isLoading = useStore($isLoading);
  const typesLevelOneValues = useStore($typesLevelOneValues);
  const selectedOneLevel = useStore($selectedOneLevel);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({ originalArgument, originalValue }: PropsText) => {
    const id = `${originalValue}${originalArgument}`;

    const argument = typesLevelOneValues[id]?.fullTitle
      ? typesLevelOneValues[id].fullTitle
      : originalArgument;

    const value = typesLevelOneValues[id]?.value
      ? typesLevelOneValues[id].value
      : originalValue;

    return {
      html: `<div>
      <span style="font-weight: bold;">${t('type')}: </span>
      <span style="font-weight: 400;">${argument}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Requests')}: </span>
      <span style="font-weight: 400;">${value}</span>
    </div>`,
    };
  };

  const customizeText = ({ originalArgument, originalValue }: PropsText) => {
    const id = `${originalValue}${originalArgument}`;

    const value = typesLevelOneValues[id]?.value
      ? typesLevelOneValues[id].value
      : originalValue;

    return value;
  };

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const classes = useStyles();

  const visualRange = {
    length: maxTypesOnChart,
  };

  const isExportDisabled = isLoading || typesLevelOne.length === 0;

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  const onArgumentAxisClick = ({ argument }: { argument: number | string | Date }) =>
    selectOneLevel(argument);

  const resetFilter = () => {
    selectOneLevel('');
    selectTwoLevel('');
    selectThreeLevel('');
  };

  return (
    <div className={classes.wrapper} style={style}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>
            {`${t('RequestType')} 1`}
          </Typography>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
          <ExportButton onClick={exportTypesLevelOne} disabled={isExportDisabled} />
          {Array.isArray(selectedOneLevel) && selectedOneLevel.length > 0 && (
            <ResetFilterButton onClick={resetFilter} />
          )}
        </div>
      </div>
      {!isForceRender && (
        <Chart
          dataSource={typesLevelOne}
          rotated
          onArgumentAxisClick={onArgumentAxisClick}
        >
          <CommonSeriesSettings>
            <Label visible={true} />
          </CommonSeriesSettings>

          <Series valueField="number" argumentField="title" type="bar" color="#5E9FDF">
            <Label
              visible={true}
              backgroundColor="none"
              font={{
                size: 16,
                color: '#65657B',
                opacity: 0.7,
                family: 'Roboto',
              }}
              customizeText={customizeText}
            />
          </Series>
          <Legend visible={false} />
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label render={ArgumentLabel} />
          </ArgumentAxis>
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { TypesLevelOneChart };
