import { FC } from 'react';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '@ui/index';
import { ErrorsEquipmentGate, $filters } from '../../models';
import { ErrorsEquipmentBuildingsChart } from '../errors-equipment-buildings-chart';
import { ErrorsEquipmentCategoriesChart } from '../errors-equipment-categories-chart';
import { ErrorsEquipmentComplexChart } from '../errors-equipment-complex-chart';
import { styles } from '../main/styles';

const ErrorsEquipmentCharts: FC = () => {
  const { objects } = useStore($filters);

  const style =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <ErrorsEquipmentGate />

      <CustomScrollbar style={style}>
        <ErrorsEquipmentComplexChart />
        <ErrorsEquipmentBuildingsChart />
        <ErrorsEquipmentCategoriesChart />
      </CustomScrollbar>
    </>
  );
};

export { ErrorsEquipmentCharts };
