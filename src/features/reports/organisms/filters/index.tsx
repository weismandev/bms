import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { TypeSelectField, TypesData } from '@features/tickets-new-type-select';
import { OriginSelectField } from '@features/tickets-origin-select';
import { StatusSelectField } from '@features/tickets-status-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  Divider,
  InputField,
} from '@ui/index';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
  $currentTab,
} from '../../models';
import { CategoriesDevicesSelectField } from '../categories-devices-select';
import { GuaranteSelectField } from '../guarante-select';
import { useStyles } from './styles';

const { t } = i18n;

const Filters = memo(() => {
  const filters = useStore($filters);
  const currentTab = useStore($currentTab);

  const classes = useStyles();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <Form className={classes.form}>
              {currentTab !== 'periods' && (
                <>
                  <p className={classes.title}>{t('period')}</p>
                  <div className={classes.dateField}>
                    <div>
                      <Field
                        name="period_from"
                        label={null}
                        divider={false}
                        component={InputField}
                        placeholder=""
                        type="date"
                      />
                    </div>
                    <p className={classes.dateDivider}>-</p>
                    <div>
                      <Field
                        name="period_to"
                        label={null}
                        divider={false}
                        component={InputField}
                        placeholder=""
                        type="date"
                      />
                    </div>
                  </div>
                  <Divider />
                </>
              )}
              <Field
                name="objects"
                component={ComplexSelectField}
                label={t('AnObject')}
                placeholder={t('selectObject')}
                isMulti
              />
              <Field
                name="houses"
                complex={values?.objects?.length > 0 && values.objects}
                component={HouseSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />
              {currentTab !== 'errors-equipment' && (
                <>
                  <Field
                    name="statuses"
                    component={StatusSelectField}
                    label={t('RequestStatus')}
                    placeholder={t('SelectRequestStatus')}
                    isMulti
                  />
                  <Field
                    label={t('Source')}
                    placeholder={t('SelectTheSourceOfTheRequest')}
                    component={OriginSelectField}
                    name="origins"
                    isMulti
                  />
                  <Field
                    label={t('Warranty')}
                    placeholder={t('SelectWarranty')}
                    component={GuaranteSelectField}
                    name="is_guaranteed"
                  />
                  <FieldArray
                    name="types"
                    render={() => (
                      <>
                        <Field
                          label={`${t('RequestType')} 1`}
                          placeholder={`${t('SelectRequestType')} 1`}
                          component={TypeSelectField}
                          name="types.0"
                          isFirstSelect
                          isFilter
                          isMulti
                          onChange={(value: TypesData[]) => {
                            setFieldValue('types.0', value);
                            setFieldValue('types.1', []);
                            setFieldValue('types.2', []);
                          }}
                        />
                        {Array.isArray(values?.types) &&
                          values.types.map((type: TypesData[], index: number) => {
                            if (!type) {
                              return null;
                            }

                            const formattedTypes = type.reduce(
                              (
                                acc: {
                                  [key: number]: { [key: string]: number[] };
                                },
                                value
                              ) => {
                                if (Boolean(value?.id) && Boolean(value?.title)) {
                                  return {
                                    ...acc,
                                    ...{
                                      [value.id]: {
                                        [value.title]: value?.children || [],
                                      },
                                    },
                                  };
                                }

                                return acc;
                              },
                              {}
                            );

                            const childrenTypes =
                              Object.values(formattedTypes)
                                .map((item) => Object.values(item))
                                .flat(2) || [];

                            if (childrenTypes.length === 0) {
                              return null;
                            }

                            return (
                              <Field
                                key={index}
                                label={`${t('RequestType')} ${index + 2}`}
                                isFilter
                                isMulti
                                placeholder={`${t('SelectRequestType')} ${index + 2}`}
                                component={TypeSelectField}
                                name={`types.${index + 1}`}
                                formattedTypes={formattedTypes}
                                childrenTypes={childrenTypes}
                                onChange={(value: TypesData[]) => {
                                  setFieldValue(`types.${index + 1}`, value);
                                  setFieldValue(`types.${index + 2}`, []);
                                }}
                                topLevelValue={values.types[index]}
                              />
                            );
                          })}
                      </>
                    )}
                  />
                </>
              )}
              {currentTab === 'errors-equipment' && (
                <Field
                  label={t('Category')}
                  placeholder={t('SelectCategory')}
                  component={CategoriesDevicesSelectField}
                  name="categories"
                  isMulti
                />
              )}
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filters };
