import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  form: {
    padding: 24,
    paddingTop: 0,
  },
  title: {
    color: '#8F91A3',
    marginBottom: 12,
    fontSize: 14,
  },
  dateField: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  dateDivider: {
    margin: '7px 10px 0',
    color: '#8F91A3',
  },
}));

export { useStyles };
