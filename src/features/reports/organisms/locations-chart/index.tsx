import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { CustomScrollbar } from '@ui/index';
import {
  LocationsGate,
  $locations,
  $isForceRender,
  exportLocations,
  $isLoading,
  $filters,
  maxLocationsOnChart,
  $locationsValues,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles, styles } from '../main/styles';

const { t } = i18n;

const LocationsChart: FC = () => {
  const locations = useStore($locations);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to, objects } = useStore($filters);
  const isLoading = useStore($isLoading);
  const locationsValues = useStore($locationsValues);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
  }: {
    originalArgument: string;
    originalValue: string;
  }) => {
    const id = `${originalValue}${originalArgument}`;

    const argument = locationsValues[id] ? locationsValues[id] : originalArgument;

    return {
      html: `<div>
        <span style="font-weight: bold;">${t('Location')}: </span>
        <span style="font-weight: 400;">${argument}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Requests')}: </span>
        <span style="font-weight: 400;">${originalValue}</span>
      </div>`,
    };
  };

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const classes = useStyles();

  const visualRange = {
    startValue: locations[0]?.location,
    length: maxLocationsOnChart,
  };

  const isExportDisabled = isLoading || locations.length === 0;

  const style = {
    marginTop: Array.isArray(objects) && objects.length > 0 ? 0 : 20,
  };

  const styleScrollbar =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <LocationsGate />

      <CustomScrollbar style={styleScrollbar}>
        <div className={classes.wrapper} style={style}>
          <div className={classes.header}>
            <div className={classes.row}>
              <Typography classes={{ root: classes.title }}>
                {t('ByProblemLocation')}
              </Typography>
            </div>
            <div className={classes.dateRow}>
              <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
              <ExportButton onClick={exportLocations} disabled={isExportDisabled} />
            </div>
          </div>
          {!isForceRender && (
            <Chart dataSource={locations}>
              <CommonSeriesSettings>
                <Label visible />
              </CommonSeriesSettings>

              <Series
                valueField="number"
                argumentField="location"
                type="bar"
                color="#5E9FDF"
              >
                <Label
                  visible
                  backgroundColor="none"
                  font={{
                    size: 16,
                    color: '#65657B',
                    opacity: 0.7,
                    family: 'Roboto',
                  }}
                />
              </Series>
              <Legend visible={false} />
              <Tooltip
                enabled
                location="edge"
                customizeTooltip={customizeTooltip}
                border={{
                  color: '#0394E3',
                  width: 3,
                }}
                cornerRadius={8}
                zIndex={9999}
                shared
              />
              <ArgumentAxis visualRange={visualRange}>
                <Label displayMode="rotate" rotationAngle="45" />
              </ArgumentAxis>
              <ScrollBar visible />
              <ZoomAndPan argumentAxis="pan" />
            </Chart>
          )}
        </div>
      </CustomScrollbar>
    </>
  );
};

export { LocationsChart };
