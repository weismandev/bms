import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tabs, Chip } from '@mui/material';
import { $isFilterOpen, $filters } from '../../models';
import { useStyles } from './styles';

interface Props {
  width: number;
}

export const ObjectsCarousel: FC<Props> = ({ width = 0 }) => {
  const isFilterOpen = useStore($isFilterOpen);
  const { objects } = useStore($filters);

  const classes = useStyles();

  if (!Array.isArray(objects) || objects.length === 0) {
    return null;
  }

  const containerWidth = width === 0 ? '100%' : isFilterOpen ? width - 419 : width - 47;

  return (
    <Tabs
      classes={{
        root: classes.tabsRoot,
        scrollButtons: classes.scrollButtons,
      }}
      variant="scrollable"
      scrollButtons="auto"
      style={{ width: containerWidth }}
    >
      {objects.map((object) => (
        <Chip
          key={object.id}
          label={object.title}
          variant="outlined"
          classes={{ root: classes.chip }}
        />
      ))}
    </Tabs>
  );
};
