import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  tabsRoot: {
    marginTop: 15,
  },
  scrollButtons: {
    marginTop: 2,
    width: '25px',
    height: '25px',
  },
  chip: {
    marginRight: 10,
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
  },
});
