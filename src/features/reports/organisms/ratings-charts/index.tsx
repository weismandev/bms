import { FC } from 'react';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '@ui/index';
import { RatingsGate, $filters } from '../../models';
import { styles } from '../main/styles';
import { RatingsAssigneesChart } from '../ratings-assignees-chart';
import { RatingsBuildingsChart } from '../ratings-buildings-chart';
import { RatingsObjectsChart } from '../ratings-objects-chart';
import { RatingsPeriodsChart } from '../ratings-periods-chart';

const RatingsCharts: FC = () => {
  const { objects } = useStore($filters);

  const style =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <RatingsGate />

      <CustomScrollbar style={style}>
        <RatingsPeriodsChart />
        <RatingsObjectsChart />
        <RatingsBuildingsChart />
        <RatingsAssigneesChart />
      </CustomScrollbar>
    </>
  );
};

export { RatingsCharts };
