import { FC } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, FilterButton, Tabs, Greedy } from '@ui/index';
import {
  tabs,
  $currentTab,
  changeTab,
  $isFilterOpen,
  changedFilterVisibility,
} from '../../models';

const ReportsToolbar: FC = () => {
  const currentTab = useStore($currentTab);
  const isFilterOpen = useStore($isFilterOpen);

  const onClickFilter = () => changedFilterVisibility(true);

  return (
    <Toolbar>
      <FilterButton
        style={{ marginRight: 20 }}
        onClick={onClickFilter}
        disabled={isFilterOpen}
      />
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(_: unknown, tab: string) => changeTab(tab)}
      />
      <Greedy />
    </Toolbar>
  );
};

export { ReportsToolbar };
