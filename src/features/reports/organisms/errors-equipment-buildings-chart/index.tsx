import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
  Legend,
  Size,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { $errorsEquipmentBuildings, $isForceRender, $filters } from '../../models';
import { $data as $categoriesDevices } from '../../models/categories-devices-select';
import { useStyles } from '../main/styles';

const { t } = i18n;

const ErrorsEquipmentBuildingsChart: FC = () => {
  const errorsEquipmentBuildings = useStore($errorsEquipmentBuildings);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to } = useStore($filters);
  const categoriesDevices = useStore($categoriesDevices);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
    seriesName,
  }: {
    originalArgument: string;
    originalValue: string;
    seriesName: string;
  }) => ({
    html: `<div>
        <span style="font-weight: bold;">${t('building')}: </span>
        <span style="font-weight: 400;">${originalArgument}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Amount')}: </span>
        <span style="font-weight: 400;">${originalValue}</span>
      </div>
      <div>
        <span style="font-weight: bold;">Категория: </span>
        <span style="font-weight: 400;">${seriesName}</span>
      </div>`,
  });

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const classes = useStyles();

  const visualRange = {
    startValue: errorsEquipmentBuildings[0]?.title,
    length: 20,
  };

  const valueFields = () =>
    categoriesDevices.map((item) => (
      <Series key={item.id} name={item.title} valueField={item.title}>
        <Label visible={false} />
      </Series>
    ));

  const customizeItems = (data: []) => [...data].reverse();

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>{t('ByBuildings')}</Typography>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={errorsEquipmentBuildings}>
          <Size height={620} />
          <CommonSeriesSettings argumentField="title" type="stackedBar" />
          <Legend customizeItems={customizeItems} />
          {valueFields()}
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label displayMode="rotate" rotationAngle="45" />
          </ArgumentAxis>
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { ErrorsEquipmentBuildingsChart };
