import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
  ValueAxis,
  Legend,
  Size,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import { Value } from '../../interfaces';
import {
  $ratingsAssignees,
  $isForceRender,
  $filters,
  $positionsRatingAssignees,
  $isLoading,
  exportAssigneesRatings,
  changePositionRatingAssignees,
  $formattedAssignees,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';
import { PositionsSelect } from '../positions-select';
import { useStyles as useStylesChart } from './styles';

const RatingsAssigneesChart: FC = () => {
  const { t } = useTranslation();

  const ratingsAssignees = useStore($ratingsAssignees);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to } = useStore($filters);
  const positionsRatingAssignees = useStore($positionsRatingAssignees);
  const isLoading = useStore($isLoading);
  const formattedAssignees = useStore($formattedAssignees);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const getSeriesId = (seriesName: string) => {
    if (seriesName === t('4AndAbove')) {
      return 'ratingMore';
    }

    if (seriesName === t('3AndBelow')) {
      return 'ratingLess';
    }

    return 'ratingNone';
  };

  const customizeTooltip = ({
    originalArgument,
    originalValue,
    seriesName,
  }: {
    originalArgument: string;
    originalValue: string;
    seriesName: string;
  }) => {
    const series = getSeriesId(seriesName);
    const count = formattedAssignees[originalArgument][series];

    return {
      html: `<div>
      <span style="font-weight: bold;">${t('Performer')}: </span>
      <span style="font-weight: 400;">${originalArgument}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Requests')}: </span>
      <span style="font-weight: 400;">${originalValue}%</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('CountOfTicket')}: </span>
      <span style="font-weight: 400;">${count}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Rating')}: </span>
      <span style="font-weight: 400;">${seriesName}</span>
    </div>`,
    };
  };

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const onChangePosition = (value: Value[]) => changePositionRatingAssignees(value);

  const classes = useStyles();
  const classesChart = useStylesChart();

  const visualRange = {
    startValue: ratingsAssignees[0]?.name,
    length: 20,
  };

  const customizeValueAxisText = ({ value }: { value: number }) => `${value}%`;

  const isExportDisabled = isLoading || ratingsAssignees.length === 0;

  const customizeItems = (data: []) => [...data].reverse();

  return (
    <div className={classes.wrapper}>
      <div className={classesChart.header}>
        <div className={classesChart.row}>
          <div className={classesChart.row}>
            <Typography classes={{ root: classes.title }} noWrap>
              {t('ByPerformers')}
            </Typography>
          </div>
          <div className={classesChart.select}>
            <PositionsSelect
              label={null}
              divider={false}
              placeholder={t('Label.Position')}
              value={positionsRatingAssignees}
              onChange={onChangePosition}
              isMulti
            />
          </div>
        </div>
        <div className={classesChart.dateRow}>
          <Typography classes={{ root: classes.dateInfo }} noWrap>
            {date}
          </Typography>
          <ExportButton onClick={exportAssigneesRatings} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={ratingsAssignees}>
          <Size height={620} />
          <CommonSeriesSettings argumentField="name" type="stackedBar" />
          <Legend customizeItems={customizeItems} />
          <ValueAxis>
            <Label customizeText={customizeValueAxisText} />
          </ValueAxis>
          <Series name={t('NoRating')} valueField="ratingNone" color="#5E9FDF">
            <Label visible={false} />
          </Series>
          <Series name={t('3AndBelow')} valueField="ratingLess" color="#2A50B4">
            <Label visible={false} />
          </Series>
          <Series name={t('4AndAbove')} valueField="ratingMore" color="#71E0F9">
            <Label visible={false} />
          </Series>
          <Tooltip
            enabled
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label displayMode="rotate" rotationAngle="45" />
          </ArgumentAxis>
          <ScrollBar visible />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { RatingsAssigneesChart };
