import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  header: {
    display: 'flex',
    marginBottom: 10,
  },
  row: {
    display: 'flex',
    flexGrow: 1,
  },
  select: {
    paddingTop: 4,
    paddingRight: 25,
    width: '100%',
  },
  dateRow: {
    display: 'flex',
    marginTop: 3,
  },
});
