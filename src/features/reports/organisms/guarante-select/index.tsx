import { FC } from 'react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { options } from '../../models/guarante-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const GuaranteSelect: FC<object> = (props) => (
  <SelectControl options={options} {...props} />
);

const GuaranteSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<GuaranteSelect />} onChange={onChange} {...props} />;
};

export { GuaranteSelect, GuaranteSelectField };
