import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, $isLoading, fxGetList } from '../../models/categories-devices-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const CategoriesDevicesSelect: FC<object> = (props) => {
  useEffect(() => {
    fxGetList();
  }, []);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const CategoriesDevicesSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<CategoriesDevicesSelect />} onChange={onChange} {...props} />
  );
};
