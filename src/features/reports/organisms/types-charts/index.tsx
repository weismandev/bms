import { FC } from 'react';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '@ui/index';
import { TypesGate, $filters } from '../../models';
import { styles } from '../main/styles';
import { TypesLevelOneChart } from '../types-level-one-chart';
import { TypesLevelThreeChart } from '../types-level-three-chart';
import { TypesLevelTwoChart } from '../types-level-two-chart';

const TypesCharts: FC = () => {
  const { objects } = useStore($filters);

  const style =
    Array.isArray(objects) && objects.length > 0
      ? styles.scrollbarWithObject
      : styles.scrollbar;

  return (
    <>
      <TypesGate />

      <CustomScrollbar style={style}>
        <TypesLevelOneChart />
        <TypesLevelTwoChart />
        <TypesLevelThreeChart />
      </CustomScrollbar>
    </>
  );
};

export { TypesCharts };
