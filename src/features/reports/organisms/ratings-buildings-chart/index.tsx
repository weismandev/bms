import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Tooltip,
  ValueAxis,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
  Legend,
  Size,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  $ratingsBuildings,
  $isForceRender,
  exportBuildingsRatings,
  $isLoading,
  $filters,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';

const { t } = i18n;

const RatingsBuildingsChart: FC = () => {
  const ratingsBuildings = useStore($ratingsBuildings);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to } = useStore($filters);
  const isLoading = useStore($isLoading);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
    seriesName,
  }: {
    originalArgument: string;
    originalValue: string;
    seriesName: string;
  }) => ({
    html: `<div>
        <span style="font-weight: bold;">${t('building')}: </span>
        <span style="font-weight: 400;">${originalArgument}</span>
      </div>
      <div>
        <span style="font-weight: bold;">${t('Requests')}: </span>
        <span style="font-weight: 400;">${originalValue}%</span>
      </div>
      <div>
        <span style="font-weight: bold;">Оценка: </span>
        <span style="font-weight: 400;">${seriesName}</span>
      </div>`,
  });

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const customizeValueAxisText = ({ value }: { value: number }) => `${value}%`;

  const isExportDisabled = isLoading || ratingsBuildings.length === 0;

  const classes = useStyles();

  const visualRange = {
    startValue: ratingsBuildings[0]?.name,
    length: 20,
  };

  const customizeItems = (data: []) => [...data].reverse();

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>{t('ByBuildings')}</Typography>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
          <ExportButton onClick={exportBuildingsRatings} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={ratingsBuildings}>
          <Size height={620} />
          <CommonSeriesSettings argumentField="name" type="stackedBar" />
          <Legend customizeItems={customizeItems} />
          <ValueAxis>
            <Label customizeText={customizeValueAxisText} />
          </ValueAxis>
          <Series name={t('NoRating')} valueField="ratingNone" color="#5E9FDF">
            <Label visible={false} />
          </Series>
          <Series name={t('3AndBelow')} valueField="ratingLess" color="#2A50B4">
            <Label visible={false} />
          </Series>
          <Series name={t('4AndAbove')} valueField="ratingMore" color="#71E0F9">
            <Label visible={false} />
          </Series>
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label displayMode="rotate" rotationAngle="45" />
          </ArgumentAxis>
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { RatingsBuildingsChart };
