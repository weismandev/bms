import { FC } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale as locale } from '@tools/dateFnsLocale';
import {
  Chart,
  Series,
  CommonSeriesSettings,
  Label,
  Legend,
  Tooltip,
  ArgumentAxis,
  ScrollBar,
  ZoomAndPan,
} from 'devextreme-react/chart';
import { Typography } from '@mui/material';
import { PrioritySelect } from '@features/tickets-priority-select';
import i18n from '@shared/config/i18n';
import { Value } from '../../interfaces';
import {
  $assignees,
  $isForceRender,
  $filters,
  $prioritiesAssignees,
  changePrioritiesAssignees,
  $positionsAssignees,
  changePositionAssignees,
  $isLoading,
  exportAssignees,
  maxAssigneesOnChart,
  $assigneesValues,
} from '../../models';
import { ExportButton } from '../export-button';
import { useStyles } from '../main/styles';
import { PositionsSelect } from '../positions-select';

const { t } = i18n;

const AssigneesChart: FC = () => {
  const assignees = useStore($assignees);
  const isForceRender = useStore($isForceRender);
  const { period_from, period_to } = useStore($filters);
  const prioritiesAssignees = useStore($prioritiesAssignees);
  const positionsAssignees = useStore($positionsAssignees);
  const isLoading = useStore($isLoading);
  const assigneesValues = useStore($assigneesValues);

  const currentDate = format(new Date(), `dd MMM yyyy ${t('y.')}`, { locale });

  const customizeTooltip = ({
    originalArgument,
    originalValue,
  }: {
    originalArgument: string;
    originalValue: string;
  }) => {
    const id = `${originalValue}${originalArgument}`;

    const argument = assigneesValues[id] ? assigneesValues[id] : originalArgument;

    return {
      html: `<div>
      <span style="font-weight: bold;">${t('Performer')}: </span>
      <span style="font-weight: 400;">${argument}</span>
    </div>
    <div>
      <span style="font-weight: bold;">${t('Requests')}: </span>
      <span style="font-weight: 400;">${originalValue}</span>
    </div>`,
    };
  };

  const isExistPeriodsDate = period_from.length > 0 && period_to.length > 0;
  const date_from =
    isExistPeriodsDate && format(new Date(period_from), `dd MMM yyyy ${t('y.')}`, { locale });
  const date_to =
    isExistPeriodsDate && format(new Date(period_to), `dd MMM yyyy ${t('y.')}`, { locale });
  const date = isExistPeriodsDate
    ? `${date_from} - ${date_to}`
    : `${currentDate} - ${currentDate}`;

  const onChange = (value: Value[]) => changePrioritiesAssignees(value);
  const onChangePosition = (value: Value[]) => changePositionAssignees(value);

  const classes = useStyles();

  const visualRange = {
    length: maxAssigneesOnChart,
  };

  const isExportDisabled = isLoading || assignees.length === 0;

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <div className={classes.row}>
          <Typography classes={{ root: classes.title }}>{t('ByPerformers')}</Typography>
          <div style={{ paddingTop: 5, paddingRight: 25, width: 210 }}>
            <PositionsSelect
              label={null}
              divider={false}
              placeholder={t('Label.Position')}
              value={positionsAssignees}
              onChange={onChangePosition}
              isMulti
            />
          </div>
          <div style={{ paddingTop: 5, width: 200 }}>
            <PrioritySelect
              label={null}
              divider={false}
              placeholder={t('Priority')}
              value={prioritiesAssignees}
              onChange={onChange}
              isMulti
            />
          </div>
        </div>
        <div className={classes.dateRow}>
          <Typography classes={{ root: classes.dateInfo }}>{date}</Typography>
          <ExportButton onClick={exportAssignees} disabled={isExportDisabled} />
        </div>
      </div>
      {!isForceRender && (
        <Chart dataSource={assignees} rotated>
          <CommonSeriesSettings>
            <Label visible={true} />
          </CommonSeriesSettings>

          <Series valueField="number" argumentField="name" type="bar" color="#5E9FDF">
            <Label
              visible={true}
              backgroundColor="none"
              font={{
                size: 16,
                color: '#65657B',
                opacity: 0.7,
                family: 'Roboto',
              }}
            />
          </Series>
          <Legend visible={false} />
          <Tooltip
            enabled={true}
            location="edge"
            customizeTooltip={customizeTooltip}
            border={{
              color: '#0394E3',
              width: 3,
            }}
            cornerRadius={8}
            zIndex={9999}
            shared
          />
          <ArgumentAxis visualRange={visualRange}>
            <Label />
          </ArgumentAxis>
          <ScrollBar visible={true} />
          <ZoomAndPan argumentAxis="pan" />
        </Chart>
      )}
    </div>
  );
};

export { AssigneesChart };
