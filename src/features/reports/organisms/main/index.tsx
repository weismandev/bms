import { FC, useEffect, useRef, useState } from 'react';
import { useStore } from 'effector-react';
import { Wrapper } from '@ui/index';
import { $currentTab } from '../../models';
import { ErrorsEquipmentCharts } from '../errors-equipment-charts';
import { GeneralCharts } from '../general-charts';
import { LocationsChart } from '../locations-chart';
import { ObjectsCarousel } from '../objects-carousel';
import { PeriodsChart } from '../periods-chart';
import { RatingsCharts } from '../ratings-charts';
import { StatusesCharts } from '../statuses-charts';
import { ReportsToolbar } from '../toolbar';
import { TypesCharts } from '../types-charts';
import { useStyles } from './styles';

const Main: FC = () => {
  const currentTab = useStore($currentTab);

  const classes = useStyles();

  const ref = useRef<null | { offsetWidth: number }>(null);
  const [width, changeWidth] = useState<number>(0);

  useEffect(() => {
    if (ref?.current?.offsetWidth) {
      changeWidth(ref.current.offsetWidth);
    }
  }, []);

  return (
    <Wrapper className={classes.mainWrapper} ref={ref}>
      <ReportsToolbar />
      <ObjectsCarousel width={width} />
      {currentTab === 'periods' && <PeriodsChart />}
      {currentTab === 'general' && <GeneralCharts />}
      {currentTab === 'types' && <TypesCharts />}
      {currentTab === 'locations' && <LocationsChart />}
      {currentTab === 'ratings' && <RatingsCharts />}
      {currentTab === 'statuses' && <StatusesCharts />}
      {currentTab === 'errors-equipment' && <ErrorsEquipmentCharts />}
    </Wrapper>
  );
};

export { Main };
