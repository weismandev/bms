import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  mainWrapper: {
    height: '100%',
    padding: '24px 0 0 24px',
    overflow: 'hidden',
    margin: 0,
  },
  wrapper: {
    border: '1px solid #E7E7EC',
    boxSizing: 'border-box',
    borderRadius: 15,
    padding: 20,
    margin: '0px 24px 20px 0px',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  title: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 700,
    fontSize: 24,
    paddingRight: 30,
  },
  yearLabel: {
    color: '#8F91A3',
    fontSize: 14,
    margin: '5px 10px 0 0',
  },
  rowYear: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 4,
  },
  row: {
    display: 'flex',
  },
  titleSwitch: {
    fontFamily: 'Roboto',
    fontWeight: 500,
    fontSize: 14,
    color: '#9393A3',
    marginTop: 8,
  },
  switchContainer: {
    marginTop: 2,
    display: 'flex',
  },
  switchBase: {
    color: '#0288d1',
  },
  switchTrack: {
    backgroundColor: '#0288d1',
  },
  dateRow: {
    display: 'flex',
  },
  dateInfo: {
    marginRight: 20,
    marginTop: 7,
    color: '#3B3B50',
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: 700,
  },
});

const styles = {
  scrollbar: {
    height: 'calc(100% - 60px)',
  },
  scrollbarWithObject: {
    height: 'calc(100% - 120px)',
  },
};

export { useStyles, styles };
