import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { fxGetList, $data, $isLoading } from '../../models/positions-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const PositionsSelect: FC<any> = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

const PositionsSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PositionsSelect />} onChange={onChange} {...props} />;
};

export { PositionsSelect, PositionsSelectField };
