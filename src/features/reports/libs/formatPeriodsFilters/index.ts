import format from 'date-fns/format';
import { ChartPayload, Filters } from '../../interfaces';

export const formatPeriodsFilters = (filters: Filters) => {
  const payload: ChartPayload = {};

  if (filters.period_from.length > 0) {
    const period_from = format(new Date(filters.period_from), 'yyyy-MM-dd');

    payload.period_from = period_from;
  }

  if (filters.period_to.length > 0) {
    const period_to = format(new Date(filters.period_to), 'yyyy-MM-dd');

    payload.period_to = period_to;
  }

  return payload;
};
