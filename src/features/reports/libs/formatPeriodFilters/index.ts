import format from 'date-fns/format';
import { ChartPayload } from '../../interfaces';

export const formatPeriodFilters = (endYearPeriod: Date) => {
  const payload: ChartPayload = {};

  if (endYearPeriod) {
    const endYear = new Date(endYearPeriod);
    endYear.setMonth(11, 31);

    const startYear = new Date(endYearPeriod);
    startYear.setMonth(0, 1);

    const period_from = format(startYear, 'yyyy-MM-dd');
    const period_to = format(endYear, 'yyyy-MM-dd');

    payload.period_from = period_from;
    payload.period_to = period_to;
  }

  return payload;
};
