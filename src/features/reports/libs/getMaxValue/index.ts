export const getMaxValue = (array: { length: number }[]): any =>
  array.reduce((prev, cur) => (prev.length > cur.length ? prev : cur));
