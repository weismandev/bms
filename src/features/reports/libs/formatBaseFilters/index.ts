import { ChartPayload, Value, Filters } from '../../interfaces';

export const formatBaseFilters = (filters: Filters) => {
  const payload: ChartPayload = {};

  if (filters.objects.length > 0) {
    const objects = filters.objects.map((item: { id: number }) => item.id);

    payload.complex_ids = objects;
  }

  if (filters.houses.length > 0) {
    const houses = filters.houses.map((item: { id: number }) => item.id);

    payload.building_ids = houses;
  }

  if (filters.statuses.length > 0) {
    const statuses = filters.statuses.map((item: { id: string }) => item.id);

    payload.statuses = statuses;
  }

  if (filters.origins.length > 0) {
    const origins = filters.origins.map((item: { number: number }) => item.number);

    payload.origins = origins;
  }

  if (filters.is_guaranteed?.id === true) {
    payload.is_guaranteed = filters.is_guaranteed.id;
  }

  if (filters.is_guaranteed?.id === false) {
    payload.is_guaranteed = filters.is_guaranteed.id;
  }

  if (filters.types.length > 0) {
    if (filters.types[0]?.length > 0) {
      const types = filters.types[0].map((type: Value) => type.id);

      payload.ticket_type_level_1 = types;
    }

    if (filters.types[1]?.length > 0) {
      const types = filters.types[1].map((type: Value) => type.id);

      payload.ticket_type_level_2 = types;
    }

    if (filters.types[2]?.length > 0) {
      const types = filters.types[2].map((type: Value) => type.id);

      payload.ticket_type_level_3 = types;
    }
  }

  return payload;
};
