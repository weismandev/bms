import i18n from '@shared/config/i18n';

const { t } = i18n;

export const monthNames = [
  t('January'),
  t('February'),
  t('March'),
  t('April'),
  t('May'),
  t('June'),
  t('July'),
  t('August'),
  t('September'),
  t('October'),
  t('November'),
  t('December'),
];

export const getMonth = (period: string) => {
  const date = new Date(period);

  return monthNames[date.getMonth()];
};
