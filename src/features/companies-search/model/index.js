import { createStore, createEffect, createEvent, forward } from 'effector';
import { signout } from '../../common';
import { daDataApi } from '../api';

const search = createEvent();
const resetDaData = createEvent();
const specify = createEvent();

const fxGetList = createEffect();

const $dadata = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(daDataApi.getExternalCompanies);

$dadata
  .on(fxGetList.done, (state, data) => {
    if (!data.result.suggestions) {
      return [];
    }

    const result = data.result.suggestions.map((item, number) => {
      const { inn = '', okved = '', ogrn = '', address = '', name = '' } = item.data;

      return {
        inn,
        ogrn,
        okved,
        number,
        building_id: '',
        title: name.short_with_opf,
        actual_address: address.value,
        pass_confirmation_required: true,
        guest_pass_confirmation_required: true,
      };
    });

    return result;
  })
  .reset([resetDaData, signout]);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

forward({ from: search, to: fxGetList });

export { search, resetDaData, specify, fxGetList, $dadata, $error, $isLoading };
