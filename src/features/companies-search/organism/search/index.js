import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ActionButton, InputField } from '../../../../ui';
import { search, $error } from '../../model';

const CompanySearch = () => {
  const error = useStore($error);
  const { t } = useTranslation();

  return (
    <Formik
      onSubmit={(values) => search(values.search)}
      enableReinitialize
      initialValues={{ search: '' }}
      render={() => {
        return (
          <Form
            style={{
              width: '100%',
              display: 'grid',
              gridTemplateColumns: 'auto 90px',
              gridColumnGap: '10px',
              gridTemplateRows: '1fr',
            }}
          >
            <Field
              component={InputField}
              placeholder={`${t('Name')} / ${t('TIN')} / ${t('BIN')} / ${t(
                'nameDirector'
              )}`}
              name="search"
              type="text"
              divider={false}
              label={null}
              error={error}
            />
            <ActionButton style={{ width: '100%' }} type="submit">
              {t('search')}
            </ActionButton>
          </Form>
        );
      }}
    />
  );
};

export { CompanySearch };
