const getExternalCompanies = (payload) => {
  return fetch('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party', {
    method: 'post',
    headers: {
      Authorization: 'Token 5fb595c2e7010691d3081259f6a1ba298052fd15',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ query: payload }),
  })
    .then((response) => (response.ok ? response.json() : Promise.reject(response)))
    .catch((error) => error.message);
};

export const daDataApi = { getExternalCompanies };
