import { api } from '../../../api/api2';

const getCompanyInfo = () => api.no_vers('get', 'information/get-management-company-v2');

const getList = (payload) =>
  api.no_vers('get', 'admin/get-user-signal', {
    ...payload,
    no_deleted: 1,
    paging_enable: true,
    as_array: 1,
  });

const exportCounters = (payload) =>
  api.no_vers(
    'request',
    'admin/get-user-signal',
    {
      ...payload,
      no_deleted: 1,
      export: 1,
    },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );

const exportCountersAstraHouse54 = (payload) =>
  api.no_vers('get', 'astra_excel/get_file', payload);

export const countersApi = {
  getCompanyInfo,
  getList,
  exportCounters,
  exportCountersAstraHouse54,
};
