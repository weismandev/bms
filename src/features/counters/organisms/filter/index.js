import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ApartmentSelectField } from '@features/apartment-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  SelectField,
  SwitchField,
  CustomScrollbar,
  InputField,
} from '@ui/index';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
  defaultFilters,
} from '../../models/filter.model';

const Filter = memo(() => {
  const filters = useStore($filters);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted(defaultFilters)}
          enableReinitialize
          render={({ values, setFieldValue }) => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('AnObject')}
                placeholder={t('selectObject')}
                onChange={(value) => {
                  setFieldValue('complexes', value);
                  setFieldValue('buildings', []);
                  setFieldValue('apartments', []);
                }}
                isMulti
              />
              <Field
                name="buildings"
                component={HouseSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                complex={values.complexes?.length > 0 && values.complexes}
                onChange={(value) => {
                  setFieldValue('buildings', value);
                  setFieldValue('apartments', []);
                }}
                isMulti
              />
              <Field
                name="apartments"
                building_ids={
                  Array.isArray(values?.buildings)
                    ? values.buildings.map((building) => building.id)
                    : []
                }
                component={ApartmentSelectField}
                label={t('room')}
                placeholder={t('ChooseRoom')}
                isMulti
              />
              <Field
                name="types"
                component={SelectField}
                label={t('IndicationTypes')}
                isMulti
                placeholder={t('chooseTypes')}
                options={[
                  { id: 'heat', title: t('Heat') },
                  { id: 'water', title: t('Water') },
                  { id: 'energy', title: t('Electricity') },
                  { id: 'gas', title: t('gas') },
                ]}
              />
              <Field
                name="date_start"
                label={t('IndicationsWith')}
                component={InputField}
                type="date"
              />
              <Field
                name="date_end"
                label={t('IndicationsUpTo')}
                component={InputField}
                type="date"
              />
              <Field
                name="manual_only"
                component={SwitchField}
                label={t('FiledByHand')}
                labelPlacement="start"
              />
              <Field
                name="less_previous"
                component={SwitchField}
                label={t('ReadingsLowerThanPrevious')}
                labelPlacement="start"
              />
              <Field
                name="showArchive"
                component={SwitchField}
                label={t('ShowArchive')}
                labelPlacement="start"
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
