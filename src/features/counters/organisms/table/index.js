import { memo } from 'react';
import { useUnit } from 'effector-react';
import { DataGrid } from '@features/data-grid';
import { Wrapper } from '@ui/index';
import { $isLoadingList } from '../../models/page.model';
import {
  $tableData,
  $selectRow,
  selectionRowChanged,
  sortChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} from '../../models/table.model';
import { CustomToolbar } from '../../molecules';
import { useStyles } from './styles';

export const Table = memo(() => {
  const [
    tableData,
    isLoading,
    selectRow,
    sorting,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
  ] = useUnit([
    $tableData,
    $isLoadingList,
    $selectRow,
    $sorting,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
  ]);
  const classes = useStyles();

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <Wrapper className={classes.wrapper}>
      <DataGrid.Full
        params={params}
        toolbar={toolbar}
        checkboxSelection
        disableRowSelectionOnClick
      />
    </Wrapper>
  );
});
