import { useUnit } from 'effector-react';

import { Greedy, FilterButton, ExportButton, AdaptiveSearch } from '@ui/index';

import { $rowCount } from '../../models/page.model';
import { exportBtnClicked } from '../../models/export.model';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import {
  $columns,
  $tableData,
  $visibilityColumns,
  $search,
  searchChanged,
} from '../../models/table.model';
import { styles } from './styles';

export const CustomToolbar = () => {
  const [count, isFilterOpen, columns, tableData, visibilityColumns, search] = useUnit([
    $rowCount,
    $isFilterOpen,
    $columns,
    $tableData,
    $visibilityColumns,
    $search,
  ]);

  const hiddenColumnNames = Object.entries(visibilityColumns).reduce((acc, column) => {
    if (column[0] === '__check__') {
      return acc;
    }

    if (!column[1]) {
      return [...acc, column[0]];
    }

    return acc;
  }, []);

  const onClick = () => changedFilterVisibility(true);

  return (
    <>
      <FilterButton disabled={isFilterOpen} onClick={onClick} style={styles.margin} />
      <AdaptiveSearch
        {...{
          totalCount: count,
          searchValue: search,
          searchChanged,
          isAnySideOpen: false,
        }}
      />
      <Greedy />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        onClick={exportBtnClicked}
        style={styles.margin}
      />
    </>
  );
};
