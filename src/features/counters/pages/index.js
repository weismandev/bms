import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../models/page.model';
import { Table, Filter } from '../organisms';

const CountersPage = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={null}
      />
    </>
  );
};

const RestrictedCountersPage = () => (
  <HaveSectionAccess>
    <CountersPage />
  </HaveSectionAccess>
);

export { RestrictedCountersPage as CountersPage };
