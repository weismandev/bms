import { forward, guard, sample } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '../../common';
import { countersApi } from '../api';
import { fxExport, fxExportAstraHouse54 } from './export.model';
import { $filters } from './filter.model';
import {
  pageMounted,
  fxGetList,
  $isAstraHouse,
  fxGetCompanyInfo,
  $raw,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  formatPayload,
  $isLoadingList,
} from './page.model';
import { $tableParams } from './table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from './user-settings.model';

fxGetList.use(countersApi.getList);
fxGetCompanyInfo.use(countersApi.getCompanyInfo);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isAstraHouse
  .on(fxGetCompanyInfo.done, (_, { result: company }) => company.id === 1)
  .reset(signout);

$isLoadingList.on(fxGetList.pending, (_, loading) => loading).reset(signout);

$isLoading
  .on(
    pending({
      effects: [fxGetCompanyInfo, fxExport, fxExportAstraHouse54],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [
      fxGetList.failData,
      fxGetCompanyInfo.failData,
      fxExport.failData,
      fxExportAstraHouse54.failData,
    ],
    (_, error) => error
  )
  .reset([signout, newRequestStarted]);

$isErrorDialogOpen
  .on(guard({ source: $error, filter: Boolean }), () => true)
  .reset([signout, newRequestStarted]);

$raw
  .on(fxGetList.doneData, (_, { meta, signal }) => {
    if (!Array.isArray(signal)) {
      return [];
    }

    return { data: signal, meta: { total: meta?.total_record || 0 } };
  })
  .reset(signout);

forward({
  from: pageMounted,
  to: fxGetCompanyInfo,
});

sample({
  clock: [pageMounted, $tableParams, $filters, $settingsInitialized],
  source: {
    filters: $filters,
    table: $tableParams,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ filters, table }) => formatPayload(filters, table),
  target: fxGetList,
});
