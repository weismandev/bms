import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  complexes: [],
  buildings: [],
  apartments: [],
  types: '',
  manual_only: false,
  date_start: '',
  date_end: '',
  less_previous: false,
  showArchive: false,
};

export const formatAstraHouse54FilterPayload = (
  { buildings, apartment, types },
  table
) => {
  const formattedPayload = {
    page: table.page,
    per_page: table.per_page,
  };

  if (buildings) {
    formattedPayload.buildings = [buildings.id];
  }

  if (apartment) {
    formattedPayload.apartment = apartment.id;
  }

  if (Array.isArray(types) && types.length > 0) {
    formattedPayload.filter = types.map((i) => i.id).join(',');
  }

  return formattedPayload;
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
