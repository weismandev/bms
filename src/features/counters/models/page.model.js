import { createStore, createEffect, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';

export const PageGate = createGate();
export const pageMounted = PageGate.open;
export const errorDialogVisibilityChanged = createEvent();

export const fxGetList = createEffect();
export const fxGetCompanyInfo = createEffect();

export const $raw = createStore({ data: [], meta: { total: 0 } });
export const $error = createStore(null);
export const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);
export const $isLoading = createStore(false);
export const $isLoadingList = createStore(false);
export const $rowCount = $raw.map(({ meta }) => (meta ? meta.total : 0));

export const specialAstraHouse54Id = 714;
export const $isAstraHouse = createStore(false);
export const isSpecialAstraHouse54 = (isAstraHouse, building) =>
  isAstraHouse && building && String(building.id) === String(specialAstraHouse54Id);

export const formatPayload = (filters, table) => {
  const {
    complexes,
    buildings,
    apartments,
    types,
    date_start,
    date_end,
    manual_only,
    less_previous,
    showArchive,
  } = filters;

  const payload = {
    page: table.page,
    per_page: table.per_page,
    search: table.search,
  };

  if (complexes.length > 0) {
    payload.complex_ids = complexes.map((complex) => complex.id).join(',');
  }

  if (buildings.length > 0) {
    payload.buildings = buildings.map((building) => building.id);
  }

  if (apartments.length > 0) {
    payload.apartment = apartments.map((apartment) => apartment.id).join(',');
  }

  if (Array.isArray(types) && types.length > 0) {
    payload.filter = types.map((i) => i.id).join(',');
  }

  if (date_start) {
    payload.date_start = date_start;
  }

  if (date_end) {
    payload.date_end = date_end;
  }

  if (less_previous) {
    payload.less_previous = Number(less_previous);
  }

  payload.manual_only = Number(manual_only);
  payload.archive = Number(Boolean(showArchive));

  return payload;
};
