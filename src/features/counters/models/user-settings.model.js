import { attach } from 'effector';
import {
  createSavedUserSettings,
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { pageMounted } from './page.model';
import {
  $pageSize,
  $visibilityColumns as $hiddenColumnNames,
  $savedColumns as $columns,
} from './table.model';

export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const settings = {
  section: 'counters',
  params: {
    table: { $pageSize, $hiddenColumnNames, $columns },
  },
  pageMounted,
  storage: {
    fxSaveInStorage,
    fxGetFromStorage,
  },
};

export const [$settingsInitialized, $isGettingSettingsFromStorage] =
  createSavedUserSettings(settings);
