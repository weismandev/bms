import { sample } from 'effector';
import { $columns, $savedColumns } from './table.model';
import { fxGetFromStorage } from './user-settings.model';

sample({
  clock: fxGetFromStorage.done,
  source: { columns: $columns, savedColumns: $savedColumns },
  fn: ({ columns, savedColumns }) => {
    const formattedColumns = savedColumns.reduce((acc, column) => {
      const findedColumn = columns.find((item) => item.field === column.field);

      return findedColumn
        ? [
            ...acc,
            {
              field: column.field,
              headerName: findedColumn.headerName,
              width: column.width,
              sortable: findedColumn.sortable,
            },
          ]
        : acc;
    }, []);

    const newColumns = columns.filter(
      (item) => !formattedColumns.some((element) => element.field === item.field)
    );

    newColumns.forEach((item) => {
      const index = columns.map((column) => column.field).indexOf(item.field);

      formattedColumns.splice(index, 0, item);
    });

    return formattedColumns;
  },
  target: $columns,
});
