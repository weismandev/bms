import { sample, split } from 'effector';
import {
  $filters,
  filtersSubmitted,
  formatAstraHouse54FilterPayload,
} from './filter.model';
import {
  formatPayload,
  isSpecialAstraHouse54,
  $isAstraHouse,
  fxGetList,
} from './page.model';
import { $tableParams, $currentPage } from './table.model';

$currentPage.reset(filtersSubmitted);

const filter = sample({
  source: { filters: $filters, isAstraHouse: $isAstraHouse },
  clock: filtersSubmitted,
});

const filterFor = split(filter, {
  astraHouse54: ({ filters, isAstraHouse }) =>
    isSpecialAstraHouse54(isAstraHouse, filters.buildings),
  other: ({ filters, isAstraHouse }) =>
    !isSpecialAstraHouse54(isAstraHouse, filters.buildings),
});

// отключено в задаче UJIN-13091
// sample({
//   clock: filterFor.astraHouse54,
//   source: $tableParams,
//   fn: (table, { filters }) => formatAstraHouse54FilterPayload(filters, table),
//   target: fxGetList,
// });

// sample({
//   clock: filterFor.other,
//   source: $tableParams,
//   fn: (table, { filters }) => formatPayload(filters, table),
//   target: fxGetList,
// });
// отключено в задаче UJIN-13091
