import { createEffect, createEvent } from 'effector';
import FileSaver from 'file-saver';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';

export const exportBtnClicked = createEvent();

export const fxExport = createEffect();
export const fxExportAstraHouse54 = createEffect();

export const formatAstraHouse54ExportPayload = ({ date_start, buildings }) => {
  const formattedPayload = {
    date: date_start ? new Date(date_start) : new Date(),
  };

  if (buildings) {
    formattedPayload.building_id = buildings.id;
  }

  return formattedPayload;
};

export const saveFile = ({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `Counters-${dateTimeNow}.xlsx`);
};

export const saveAstraHouse54File = ({ result }) => {
  const url = new URL(result.url);
  const params = new URLSearchParams(url.search);
  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');

  const fileName = params.get('name') || `Counters-${dateTimeNow}.xlsx`;
  FileSaver.saveAs(result.url, fileName);
};
