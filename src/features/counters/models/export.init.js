import { sample, split } from 'effector';
import { countersApi } from '../api';
import {
  exportBtnClicked,
  fxExport,
  fxExportAstraHouse54,
  formatAstraHouse54ExportPayload,
  saveFile,
  saveAstraHouse54File,
} from './export.model';
import { $filters } from './filter.model';
import { formatPayload, isSpecialAstraHouse54, $isAstraHouse } from './page.model';
import { $tableParams, $selectRow, $tableData } from './table.model';

fxExport.use(countersApi.exportCounters);
fxExportAstraHouse54.use(countersApi.exportCountersAstraHouse54);

const exportCounters = sample({
  source: { filters: $filters, isAstraHouse: $isAstraHouse },
  clock: exportBtnClicked,
});

// отключено в UJIN-13091
// const exportCountersFor = split(exportCounters, {
//   astraHouse54: ({ filters, isAstraHouse }) =>
//     isSpecialAstraHouse54(isAstraHouse, filters.buildings),
//   other: ({ filters, isAstraHouse }) =>
//     !isSpecialAstraHouse54(isAstraHouse, filters.buildings),
// });

// sample({
//   clock: exportCountersFor.astraHouse54,
//   source: $tableParams,
//   fn: (table, { filters }) => formatAstraHouse54ExportPayload(filters, table),
//   target: fxExportAstraHouse54,
// });
// отключено в UJIN-13091

sample({
  clock: exportBtnClicked,
  source: {
    table: $tableParams,
    filters: $filters,
    selectRow: $selectRow,
    data: $tableData,
  },
  fn: ({ table, filters, selectRow, data }) => {
    const payload = formatPayload(filters, table);

    if (selectRow.length > 0) {
      const ids = selectRow.reduce((acc, row) => {
        const fullRow = data.find((item) => item.id === row);

        if (fullRow?.device_id) {
          return [...acc, fullRow.device_id];
        }

        return acc;
      }, []);

      payload.device_ids = ids.join(',');
    }

    return payload;
  },
  target: fxExport,
});

fxExport.done.watch(saveFile);
fxExportAstraHouse54.done.watch(saveAstraHouse54File);
