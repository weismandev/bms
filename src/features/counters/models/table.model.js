import { combine } from 'effector';
import format from 'date-fns/format';
import { createTableBag } from '@features/data-grid';
import i18n from '@shared/config/i18n';
import { $raw, $rowCount } from './page.model';

const { t } = i18n;

const columns = [
  {
    field: 'complex',
    headerName: t('AnObject'),
    width: 160,
    sortable: false,
  },
  {
    field: 'building',
    headerName: t('building'),
    width: 300,
    sortable: false,
  },
  {
    field: 'apartment_title',
    headerName: t('room'),
    width: 350,
    sortable: false,
  },
  {
    field: 'account',
    headerName: t('Label.personalAccount'),
    width: 200,
    sortable: false,
  },
  {
    field: 'title',
    headerName: t('NamePU'),
    width: 220,
    sortable: false,
  },
  {
    field: 'name',
    headerName: t('ResourceType'),
    width: 230,
    sortable: false,
  },
  {
    field: 'value',
    headerName: t('CurrentReadings'),
    width: 160,
    sortable: false,
  },
  {
    field: 'signal_update_dt_attempt',
    headerName: t('IndicationsReceived'),
    width: 180,
    sortable: false,
  },
  {
    field: 'previous_value',
    headerName: t('PreviousReadings'),
    width: 180,
    sortable: false,
  },
  {
    field: 'diff',
    headerName: t('Difference'),
    width: 100,
    sortable: false,
    renderCell: ({ row }) => {
      const styles = {
        color: row?.diffValue && row.diffValue < 0 ? 'red' : 'black',
      };

      return <span style={styles}>{row.diff}</span>;
    },
  },
  {
    field: 'signal_update_dt',
    headerName: t('IndicationsUpdated'),
    width: 245,
    sortable: false,
  },
  {
    field: 'sn',
    headerName: t('serialNumber'),
    width: 170,
    sortable: false,
  },
  {
    field: 'ext_guid',
    headerName: t('ExternalUID'),
    width: 160,
    sortable: false,
  },
  {
    field: 'previous_value_dt',
    headerName: t('PreviousReadingsUpdated'),
    width: 260,
    sortable: false,
  },
];

export const formatData = (data) =>
  data.map((i) => ({
    id: i.id,
    complex: i?.complex?.title || t('Unknown'),
    building: i?.building?.title || t('Unknown'),
    apartment_title: i?.apartment_title || t('Unknown'),
    account: i?.apartment_account?.account || '',
    title: i?.title || '',
    name: i?.name || t('Unknown'),
    value: i.value && i.measure ? `${i.value} ${i.measure}` : t('isNotDefinedit'),
    signal_update_dt_attempt: format(
      new Date(i.signal_update_dt_attempt * 1000),
      'dd.MM.yyyy, HH:mm'
    ),
    previous_value: i.measure ? `${i.previous_value} ${i.measure}` : t('isNotDefinedit'),
    diff: i.measure ? `${i.diff} ${i.measure}` : t('isNotDefinedit'),
    diffValue: i?.diff,
    signal_update_dt: format(new Date(i.signal_update_dt * 1000), 'dd.MM.yyyy, HH:mm'),
    sn: i?.sn || '',
    ext_guid: i.ext_guid ? i.ext_guid : t('isNotDefined'),
    previous_value_dt: i.previous_value_dt
      ? format(new Date(i.previous_value_dt * 1000), 'dd.MM.yyyy, HH:mm')
      : t('isNotDefined'),
    device_id: i?.device_id || '',
  }));

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $columns,
  orderColumnsChanged,
  columnsWidthChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
} = createTableBag({
  columns,
  pageSize: 25,
});

export const $savedColumns = $columns.map((clmn) =>
  clmn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
export const $tableData = $raw.map(({ data }) =>
  Array.isArray(data) && data.length > 0 ? formatData(data) : []
);
export const $countPage = combine($rowCount, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);
