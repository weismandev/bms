import { DragDropContext } from 'react-beautiful-dnd';
import { useStore } from 'effector-react';
import { $lists, moveColumn, $pointsMode } from '../../models';
import { PointsList } from '../points-list';

const PointsWithoutCamerasLists = () => {
  const lists = useStore($lists);
  const mode = useStore($pointsMode);

  if (!lists) {
    return null;
  }
  const { pointsLists, favoritesList } = lists;

  const listsForRender = (
    <>
      <PointsList data={Object.values(favoritesList)[0]} />
      {Object.values(pointsLists).map((point) => (
        <PointsList key={point.id} data={point} />
      ))}
    </>
  );

  if (mode === 'edit') {
    return <DragDropContext onDragEnd={moveColumn}>{listsForRender}</DragDropContext>;
  }

  return <>{listsForRender}</>;
};

export { PointsWithoutCamerasLists };
