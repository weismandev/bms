import React from 'react';
import { useStore } from 'effector-react';
import Options from '@img/options.svg';
import { ActionButton } from '@ui/index';
import { $camerasMode, changedEditMenuVisibility } from '../../models';

const OptionsButton = ({ id }) => {
  const mode = useStore($camerasMode);

  if (mode !== 'edit') {
    return null;
  }

  return (
    <ActionButton
      icon={<Options />}
      style={{
        position: 'absolute',
        right: 16,
        top: 16,
        background: 'transparent',
        padding: 0,
      }}
      onClick={(event) => {
        event.preventDefault();
        changedEditMenuVisibility({
          open: {
            top: event.pageY - 70,
            left: event.pageX,
          },
          id,
        });
      }}
    />
  );
};

export { OptionsButton };
