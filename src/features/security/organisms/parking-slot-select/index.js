import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui';
import {
  $data,
  $isLoading,
  $error,
  fxGetParkingSlots,
} from '../../models/parking-slot.model';

const ParkingSlotSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetParkingSlots();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const ParkingSlotSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ParkingSlotSelect />} onChange={onChange} {...props} />;
};

export { ParkingSlotSelect, ParkingSlotSelectField };
