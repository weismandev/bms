import { useStore } from 'effector-react';
import { $modalName } from '../../models';
import { OpenBarriersModal } from '../open-barriers-modal';
import { VideoModal } from '../video-modal';

const SecurityModal = () => {
  const modalName = useStore($modalName);

  if (modalName === 'video') {
    return <VideoModal />;
  }

  if (modalName === 'openBarriers') {
    return <OpenBarriersModal />;
  }

  return null;
};

export { SecurityModal };
