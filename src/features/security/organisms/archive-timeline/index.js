import { useStore } from 'effector-react';
import { ActionButton } from '@ui';
import { Slider } from '@mui/material';
import { PlayArrow, Pause } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import {
  $archive,
  getArchive,
  startSeconds,
  endSeconds,
  $timeLine,
  $currentTimeline,
  $isPlaying,
  changePlaying,
  $dataModal,
} from '../../models';

const useStyles = makeStyles({
  playAndPauseContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  playAndPauseButton: {
    background: 'none',
    boxShadow: 'none',
    '&:hover': {
      boxShadow: 'none',
      background: 'none',
    },
  },
  playAndPauseIcon: {
    color: '#94a8b7',
  },
  sliderContainer: {
    paddingLeft: 15,
    paddingRight: 18,
  },
  root: {
    color: '#94a8b7',
  },
  mark: {
    height: 10,
  },
  timeLine: {
    margin: '0 auto',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
});

const ArchiveTimeline = () => {
  const classes = useStyles();

  const timeLine = useStore($timeLine);
  const archive = useStore($archive);
  const dataModal = useStore($dataModal);
  const isPlaying = useStore($isPlaying);
  const currentTimeline = useStore($currentTimeline);

  const { id } = dataModal;

  if (!archive) {
    return null;
  }

  return (
    <div className={classes.timeLine}>
      <div className={classes.playAndPauseContainer}>
        <ActionButton
          classes={{ root: classes.playAndPauseButton }}
          onClick={() => changePlaying(!isPlaying)}
        >
          {isPlaying ? (
            <Pause className={classes.playAndPauseIcon} />
          ) : (
            <PlayArrow className={classes.playAndPauseIcon} />
          )}
        </ActionButton>
      </div>

      <div className={classes.sliderContainer}>
        <Slider
          classes={{ root: classes.root, mark: classes.mark }}
          track={false}
          aria-labelledby="discrete-slider"
          defaultValue={0}
          step={null}
          min={startSeconds}
          max={endSeconds}
          marks={timeLine}
          onChangeCommitted={(_, value) => {
            if (currentTimeline !== value) {
              getArchive({ id, newTime: value });
            }
          }}
        />
      </div>
    </div>
  );
};

export { ArchiveTimeline };
