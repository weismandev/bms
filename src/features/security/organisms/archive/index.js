import { useState } from 'react';
import ReactPlayer from 'react-player';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { InputField, ActionButton, Loader } from '@ui/index';
import { $archive, getArchive, $isLoading, $isPlaying, $dataModal } from '../../models';

const { t } = i18n;

const useStyles = makeStyles({
  archiveContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  archiveFormContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 15,
  },
  archiveForm: {
    display: 'flex',
    flexDirection: 'row',
  },
  archiveFormButton: {
    display: 'flex',
    justifyContent: 'center',
    marginLeft: 15,
  },
  loader: {
    width: '100%',
  },
  wrapperPlayer: {
    position: 'relative',
    paddingTop: '56.25%',
    borderRadius: 5,
  },
  player: {
    display: (isVideoLoaded) => !isVideoLoaded && 'none',
    position: 'absolute',
    top: 0,
    left: 0,
    borderRadius: 15,
    overflow: 'hidden',
  },
});

const validationSchema = Yup.object().shape({
  timestamp: Yup.string().required(t('thisIsRequiredField')),
});

const Archive = () => {
  const classes = useStyles();

  const archive = useStore($archive);
  const isLoading = useStore($isLoading);
  const isPlaying = useStore($isPlaying);
  const dataModal = useStore($dataModal);

  const [isVideoLoaded, setIsVideoLoaded] = useState(false);

  const { id } = dataModal;
  const archiveUrl = archive?.original_streams[0].url;

  return (
    <>
      {isLoading && (
        <div className={classes.loader}>
          <Loader isLoading />
        </div>
      )}
      <div className={classes.archiveContainer}>
        <div className={classes.archiveFormContainer}>
          <Formik
            initialValues={{ timestamp: '' }}
            validationSchema={validationSchema}
            onSubmit={(values) => getArchive({ values, id })}
          >
            <Form>
              <div className={classes.archiveForm}>
                <Field
                  name="timestamp"
                  label={t('Label.date')}
                  labelPlacement="start"
                  type="datetime-local"
                  component={InputField}
                  divider={false}
                  required
                />
                <div className={classes.archiveFormButton}>
                  <ActionButton style={{ padding: '0 40px' }} type="submit">
                    {t('Show')}
                  </ActionButton>
                </div>
              </div>
            </Form>
          </Formik>
        </div>
        {archive && (
          <>
            {!isVideoLoaded && (
              <div style={{ width: '100%' }}>
                <Loader isLoading />
              </div>
            )}
            <div className={classes.wrapperPlayer}>
              <ReactPlayer
                url={archiveUrl}
                playing={isPlaying}
                width="100%"
                height="100%"
                className={classes.player}
                muted={true}
                onPlay={() => setIsVideoLoaded(true)}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export { Archive };
