import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { $complexList } from '../../models';
import { Video } from '../video';

const { t } = i18n;

const useStyles = makeStyles({
  card: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
  },
  cardText: {
    padding: '16px 0',
    height: 50,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: '#3B3B50',
  },
  cardCameraName: {
    lineHeight: 1,
    fontWeight: 500,
    fontSize: 18,
  },
  cardCameraComplex: {
    lineHeight: 1,
    fontWeight: 400,
    fontSize: 14,
  },
});

const CameraCard = (props) => {
  const { card, cardText, cardCameraName, cardCameraComplex } = useStyles();

  const { title, complexId } = props;
  const complexList = useStore($complexList);

  const complexTitle = complexList[complexId]?.title;

  return (
    <li className={card}>
      <Video {...props} />
      <div className={cardText}>
        <Typography className={cardCameraName}>{title}</Typography>
        <Typography className={cardCameraComplex}>
          {complexTitle && `${t('ResidentialComplex')} «${complexTitle}»`}
        </Typography>
      </div>
    </li>
  );
};

export { CameraCard };
