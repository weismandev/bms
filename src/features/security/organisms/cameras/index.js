import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import { ErrorOutlineRounded } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { useTranslation } from 'react-i18next';

import {
  Wrapper,
  CustomScrollbar,
  ActionButton,
  SelectBuildings,
} from '@ui/index';

import { Pagination } from '../pagination';
import { SecurityToolbar } from '../security-toolbar';
import { CameraCard } from '../camera-card';

import {
  $currentCameras,
  $currentPage,
  $camerasMode,
  changeCamerasModeToEdit,
  resetCamerasList,
  $houses,
  $isOpenBuildingsSelect,
  changeVisibilityBuildingsSelect,
  changeBuildings,
  changeComplex,
  setAlarm,
} from '../../models';

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    padding: '24px 0 0 24px',
    overflow: 'hidden',
    margin: 0,
  },
  camerasList: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(400px, 1fr))',
    gap: '24px calc(5px + 2%)',
    padding: '0px 24px 0px 0px',
    margin: 0,
  },
  title: {
    color: '#3B3B50',
    fontSize: 24,
    fontWeight: 400,
    lineHeight: '28px',
    marginBottom: 23,
  },
  container: {
    height: 'calc(100% - 180px)',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingRight: 24,
  },
  headerToolbarButtons: {
    display: 'flex',
    '@media (max-width: 1197px)': {
      flexDirection: 'column',
    },
  },
  button: {
    marginRight: 10,
    '@media (max-width: 1197px)': {
      marginBottom: 10,
      marginRight: 0,
    },
  },
  buttonIcon: {
    marginRight: 10,
  },
});

const Cameras = () => {
  const { t } = useTranslation();

  const {
    wrapper,
    camerasList,
    title,
    container,
    header,
    headerToolbarButtons,
    button,
    buttonIcon,
  } = useStyles();

  const currentPage = useStore($currentPage);
  const camerasMode = useStore($camerasMode);
  const currentCameras = useStore($currentCameras);
  const houses = useStore($houses);
  const isOpenBuildingsSelect = useStore($isOpenBuildingsSelect);

  const openSelect = () => changeVisibilityBuildingsSelect(true);
  const onSave = () => setAlarm(true);
  const cancelAlarm = () => setAlarm(false);

  const headerToolbar = (
    <div className={header}>
      <Typography classes={{ root: title }}>
        {`${t('CamerasSet')} №${currentPage}`}
      </Typography>
      <div className={headerToolbarButtons}>
        <ActionButton onClick={openSelect} className={button}>
          {t('StatementOfEmergencyMode')}
        </ActionButton>
        <ActionButton kind="negative" onClick={cancelAlarm}>
          <ErrorOutlineRounded className={buttonIcon} />
          {t('EndEmergencyMode')}
        </ActionButton>
      </div>
    </div>
  );

  return (
    <Wrapper className={wrapper}>
      <SelectBuildings
        houses={houses}
        buildings={[]}
        mode="edit"
        hideSelect
        isOpenSelect={isOpenBuildingsSelect}
        setBuildings={(data) => changeBuildings(data)}
        changeVisibilityBuildingsSelect={changeVisibilityBuildingsSelect}
        changeComplex={changeComplex}
        onSave={onSave}
        buttonSaveText={t('OpenPoints')}
        hideIcon
        saveKind="negative"
        cancelKind="basic"
      />
      <SecurityToolbar
        header={headerToolbar}
        mode={camerasMode}
        changeModeToEdit={changeCamerasModeToEdit}
        onClose={resetCamerasList}
      />
      <div className={container}>
        <CustomScrollbar style={{ height: '100%' }} autoHide>
          <ul className={camerasList}>
            {currentCameras.map((camera) => (
              <CameraCard key={camera.id} {...camera} />
            ))}
          </ul>
        </CustomScrollbar>
        <Pagination />
      </div>
    </Wrapper>
  );
};

export { Cameras };
