import { useStore } from 'effector-react';
import { ActionButton, Modal } from '@ui';
import { Typography } from '@mui/material';
import Entry from '@img/entry.svg';
import Exit from '@img/exit.svg';
import i18n from '@shared/config/i18n';
import {
  $dataModal,
  $typeOpenBarriersModal,
  changedTypeOpenBarriersModal,
  closeModal,
  $modalName,
} from '../../models';
import { headerForm, ContentForm } from '../open-barriers-form';

const { t } = i18n;

const OpenBarriersModal = () => {
  const dataModal = useStore($dataModal);
  const typeOpenBarriersModal = useStore($typeOpenBarriersModal);
  const modalName = useStore($modalName);

  const { title, action } = dataModal;

  const header = (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Typography
        style={{
          marginTop: -7,
          fontWeight: 700,
          fontSize: 24,
          marginBottom: 17,
        }}
      >
        {t('Opening')}
      </Typography>
    </div>
  );

  const content = (
    <>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Typography
          style={{
            fontWeight: 500,
            fontSize: 18,
          }}
        >
          {title}
        </Typography>
      </div>
      <div
        style={{
          height: 200,
          display: 'flex',
          width: '100%',
          justifyContent: 'space-between',
          padding: 10,
        }}
      >
        <ActionButton
          style={{
            height: '100%',
            width: '50%',
            marginRight: 10,
            background: '#FFFFFF',
            boxShadow: '0px 5px 15px #DCDCDC',
            color: '#65657B',
            fontWeight: 700,
            fontSize: 24,
          }}
          onClick={() => changedTypeOpenBarriersModal('entry')}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Entry />
            {t('Entry')}
          </div>
        </ActionButton>

        <ActionButton
          style={{
            height: '100%',
            width: '50%',
            background: '#FFFFFF',
            boxShadow: '0px 5px 15px #DCDCDC',
            color: '#65657B',
            fontWeight: 700,
            fontSize: 24,
          }}
          onClick={() => changedTypeOpenBarriersModal('exit')}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Exit />
            {t('Exit')}
          </div>
        </ActionButton>
      </div>
    </>
  );

  const modalElements = typeOpenBarriersModal
    ? {
        header: headerForm(closeModal, typeOpenBarriersModal),
        content: null,
        actions: (
          <ContentForm
            action={action}
            close={closeModal}
            title={title}
            type={typeOpenBarriersModal}
          />
        ),
      }
    : {
        header,
        content,
        actions: null,
      };

  return (
    <Modal
      isOpen={modalName === 'openBarriers'}
      onClose={closeModal}
      header={modalElements.header}
      content={modalElements.content}
      actions={modalElements.actions}
    />
  );
};

export { OpenBarriersModal };
