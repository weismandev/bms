import { useStore } from 'effector-react';
import { Wrapper, CustomScrollbar } from '@ui';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { changePointsModeToEdit, $pointsMode, resetPointsList } from '../../models';
import { PointsWithoutCamerasLists } from '../points-without-camera-lists';
import { SecurityToolbar } from '../security-toolbar';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  title: {
    color: '#3B3B50',
    fontSize: 24,
    fontWeight: 700,
    lineHeight: '28px',
    marginBottom: 23,
  },
  toolbar: {
    padding: 24,
  },
  content: {
    height: 'calc(100% - 180px)',
  },
});

const PointsWithoutCameras = () => {
  const { wrapper, title, toolbar, content } = useStyles();

  const pointsMode = useStore($pointsMode);

  const titleToolbar = (
    <Typography classes={{ root: title }}>{t('AccessControl')}</Typography>
  );

  return (
    <Wrapper className={wrapper}>
      <div className={toolbar}>
        <SecurityToolbar
          title={titleToolbar}
          mode={pointsMode}
          changeModeToEdit={changePointsModeToEdit}
          onClose={resetPointsList}
          isPointsList
        />
      </div>
      <div className={content}>
        <CustomScrollbar style={{ height: 'calc(100% - 50px)' }} autoHide>
          <PointsWithoutCamerasLists />
        </CustomScrollbar>
      </div>
    </Wrapper>
  );
};

export { PointsWithoutCameras };
