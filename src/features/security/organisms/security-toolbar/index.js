import { Toolbar, EditButton, CancelButton, SaveButton, Greedy } from '@ui';
import makeStyles from '@mui/styles/makeStyles';
import { saveData } from '../../models';

const useStyles = makeStyles({
  toolbar: {
    marginBottom: 24,
  },
  margin: {
    marginRight: ({ isPointsList }) => (isPointsList ? 0 : 38),
  },
  toolbarRoot: {
    padding: '0 24px 24px 0',
    alignContent: 'center',
  },
  saveButton: {
    marginRight: 15,
  },
});

const SecurityToolbar = ({
  header,
  mode,
  changeModeToEdit,
  onClose,
  isPointsList = false,
}) => {
  const { margin, toolbar, saveButton } = useStyles({ isPointsList });

  return (
    <div className={toolbar}>
      {header}
      <Toolbar>
        <EditButton
          className={margin}
          disabled={mode === 'edit'}
          onClick={changeModeToEdit}
        />
        {mode === 'edit' && (
          <>
            <Greedy />
            <SaveButton className={saveButton} onClick={saveData} />
            <CancelButton className={margin} onClick={onClose} />
          </>
        )}
      </Toolbar>
    </div>
  );
};

export { SecurityToolbar };
