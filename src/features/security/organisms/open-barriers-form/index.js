import { ActionButton, CloseButton, InputField } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Typography, Toolbar } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import i18n from '@shared/config/i18n';
import { sendAction, markDrivenIn, markDrivenOut } from '../../models';
import { ParkingSlotSelectField } from '../parking-slot-select';
import { VehicleInSelectField } from '../vehicle-in-select';

const { t } = i18n;

const headerForm = (close, type) => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Typography
      style={{
        marginTop: -7,
        fontWeight: 700,
        fontSize: 24,
        marginBottom: 30,
      }}
    >
      {type === 'entry' ? t('EntranceToTheTerritory') : t('DepartureFromTheTerritory')}
    </Typography>
    <CloseButton onClick={close} />
  </div>
);

const validationSchema = Yup.object().shape({
  vehicle_number: Yup.mixed().required(t('EnterGovNumber')),
});

const styles = {
  root: {
    display: 'flex',
    alignItems: 'center',
    minHeight: 'auto',
    justifyContent: 'center',
    width: '100%',
  },
};

const ModalContentForm = ({ classes, action, close, title, type }) => (
  <Toolbar disableGutters variant="dense" classes={{ root: classes.root }}>
    <Formik
      initialValues={{
        id: action?.in,
        parking_slot_id: '',
        vehicle_number: '',
        title: `${title} ${t('Open')}`,
      }}
      onSubmit={(values) => {
        if (type === 'entry') {
          markDrivenIn({
            vehicleNumber: values.vehicle_number,
            parkingSlot: values.parking_slot_id,
          });
        } else {
          markDrivenOut(values.vehicle_number);
        }

        sendAction(values);
        close();
      }}
      validationSchema={validationSchema}
      enableReinitialize
      render={() => (
        <Form
          style={{
            minHeight: 500,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <div
            style={{
              padding: '0 6px 24px 24px',
              width: 550,
            }}
          >
            <div style={{ paddingRight: 18 }}>
              {type === 'entry' && (
                <Field
                  name="parking_slot_id"
                  label={t('ParkingSpaceNumber')}
                  placeholder={t('ChooseParkingSpace')}
                  component={ParkingSlotSelectField}
                />
              )}
              <Field
                name="vehicle_number"
                component={type === 'entry' ? InputField : VehicleInSelectField}
                label={t('GovNumber')}
                placeholder={t('EnterGovNumber')}
                required
              />
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginBottom: 25,
            }}
          >
            <ActionButton
              style={{
                width: '60%',
                background: '#1BB169',
              }}
              type="submit"
            >
              {t('open')}
            </ActionButton>
          </div>
        </Form>
      )}
    />
  </Toolbar>
);

const ContentForm = withStyles(styles)(ModalContentForm);

export { headerForm, ContentForm };
