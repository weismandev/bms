import { useState } from 'react';
import ReactPlayer from 'react-player';
import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { ActionButton, Modal, CloseButton, Loader } from '@ui/index';
import {
  $dataModal,
  openModal,
  closeModal,
  sendAction,
  $isModalVideoArchive,
  setIsModalVideoArchive,
  $modalName,
} from '../../models';
import { Archive } from '../archive';
import { ArchiveTimeline } from '../archive-timeline';

const { t } = i18n;

const useStyles = makeStyles({
  fullwidth: {
    minWidth: '55%',
  },
  headerContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  headerTitle: {
    marginTop: -7,
    fontWeight: 700,
    fontSize: 24,
    marginBottom: 17,
  },
  actionsContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 25,
  },
  closeButton: {
    marginLeft: 25,
  },
  wrapperPlayer: {
    position: 'relative',
    paddingTop: '56.25%',
    borderRadius: 5,
  },
  player: {
    display: (isVideoLoaded) => !isVideoLoaded && 'none',
    position: 'absolute',
    top: 0,
    left: 0,
    borderRadius: 15,
    overflow: 'hidden',
  },
});

const VideoModal = () => {
  const [isVideoLoaded, setIsVideoLoaded] = useState(false);
  const {
    fullwidth,
    headerContainer,
    headerTitle,
    actionsContainer,
    closeButton,
    wrapperPlayer,
    player,
  } = useStyles(isVideoLoaded);

  const dataModal = useStore($dataModal);
  const modalName = useStore($modalName);
  const isModalVideoArchive = useStore($isModalVideoArchive);

  const { title, url, action, hasArchive } = dataModal;

  const header = (
    <div className={headerContainer}>
      <Typography classes={{ root: headerTitle }}>{title}</Typography>
      <CloseButton onClick={closeModal} />
    </div>
  );

  const content = (
    <>
      {!isVideoLoaded && (
        <div style={{ width: '100%' }}>
          <Loader isLoading />
        </div>
      )}
      <div className={wrapperPlayer}>
        <ReactPlayer
          className={player}
          width="100%"
          height="100%"
          url={url}
          playing
          muted={true}
          onPlay={() => setIsVideoLoaded(true)}
        />
      </div>
    </>
  );

  const contentArchive = <Archive />;
  const archiveTimeline = <ArchiveTimeline />;

  const actions = (
    <div className={actionsContainer}>
      <div>
        {!action?.in ? null : (
          <ActionButton
            style={{
              background: action.in !== 0 ? '#1BB169' : '#E7E7EC',
              width: 150,
            }}
            disabled={action.in === 0}
            onClick={() => {
              openModal({
                dataModal,
                name: 'openBarriers',
              });
            }}
          >
            {t('open')}
          </ActionButton>
        )}

        {!action?.out ? null : (
          <ActionButton
            classes={{ root: closeButton }}
            style={{ width: 150 }}
            disabled={action.out === 0}
            onClick={() => {
              closeModal();
              sendAction({
                id: action.out,
                title: `${title} ${t('ClosedHe')}`,
              });
            }}
          >
            {t('close')}
          </ActionButton>
        )}
      </div>

      <ActionButton
        style={{ width: 150 }}
        disabled={!hasArchive}
        onClick={() => setIsModalVideoArchive(true)}
      >
        {t('archive')}
      </ActionButton>
    </div>
  );

  return (
    <Modal
      header={header}
      content={isModalVideoArchive ? contentArchive : content}
      actions={isModalVideoArchive ? archiveTimeline : actions}
      minWidth={1000}
      onClose={closeModal}
      isOpen={modalName === 'video'}
      classes={{ paper: fullwidth }}
    />
  );
};

export { VideoModal };
