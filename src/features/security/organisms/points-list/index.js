import { useState } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import { useStore } from 'effector-react';
import { List, ListItem, ListItemText, Typography, Collapse } from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { $pointsMode, $collapseId, changedCollapseId, $complexList } from '../../models';
import { PointsListItem } from '../points-list-item';

const { t } = i18n;

const useStyles = makeStyles({
  listContainer: {
    padding: 0,
  },
  headerList: {
    display: 'flex',
    justifyContent: 'space-between',
    border: '1px solid #E7E7EC',
    background: (isOpen) => (isOpen ? '#1BB169' : '#FFFFFF'),
    padding: 16,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  headerListTitle: {
    fontWeight: 400,
    fontSize: 14,
    color: (isOpen) => (isOpen ? '#FFFFFF' : '#000000'),
  },
  text: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 400,
    fontSize: 14,
    color: '#AAAAAA',
  },
  titlePointsLists: {
    fontWeight: 500,
    fontSize: 14,
    color: '#65657B',
    opacity: 0.7,
    padding: '40px 0px 16px 16px',
  },
  listItem: {
    border: '1px dashed #1BB169',
  },
  expandLess: {
    color: '#FFFFFF',
  },
  expandMore: {
    color: '#AAAAAA',
  },
});

const PointsList = ({ data }) => {
  const { id, list } = data;

  const mode = useStore($pointsMode);
  const complexList = useStore($complexList);
  const collapseId = useStore($collapseId);

  const changeCollapse = () => {
    if (id === collapseId) {
      changedCollapseId('');
    } else {
      changedCollapseId(id);
    }
  };

  const complexTitle = id === 'favorites' ? t('Favorites') : complexList[id]?.title;

  const [open, setOpen] = useState(true);

  const isOpen = mode === 'edit' ? open : collapseId === id;
  const handleClick = mode === 'edit' ? () => setOpen(!open) : () => changeCollapse();

  const classes = useStyles(isOpen);

  const dropablePointsList = (
    <Droppable droppableId={id}>
      {(provided) => (
        <div {...provided.droppableProps} ref={provided.innerRef}>
          <Collapse in={isOpen} timeout="auto">
            <List classes={{ root: classes.listContainer }}>
              {list.map((item, index) => (
                <PointsListItem key={item.id} index={index} item={item} />
              ))}
              {id === 'favorites' && (
                <ListItem className={classes.listItem}>
                  <ListItemText
                    primary={
                      <Typography classes={{ root: classes.text }}>
                        {t('DragDeviceHere')}
                      </Typography>
                    }
                  />
                </ListItem>
              )}
              {provided.placeholder}
            </List>
          </Collapse>
          {id === 'favorites' && (
            <Typography classes={{ root: classes.titlePointsLists }}>
              {t('RC')}
            </Typography>
          )}
        </div>
      )}
    </Droppable>
  );

  const pointslist = (
    <>
      <Collapse in={isOpen} timeout="auto">
        {list.map((item, index) => (
          <PointsListItem key={item.id} index={index} item={item} listId={id} />
        ))}
      </Collapse>
      {id === 'favorites' && (
        <Typography classes={{ root: classes.titlePointsLists }}>{t('RC')}</Typography>
      )}
    </>
  );

  return (
    <div>
      <div className={classes.headerList} onClick={handleClick}>
        <Typography classes={{ root: classes.headerListTitle }}>
          {complexTitle}
        </Typography>
        {isOpen ? (
          <ExpandLess className={classes.expandLess} />
        ) : (
          <ExpandMore className={classes.expandMore} />
        )}
      </div>
      {mode === 'view' && <>{pointslist}</>}
      {mode === 'edit' && <>{dropablePointsList}</>}
    </div>
  );
};

export { PointsList };
