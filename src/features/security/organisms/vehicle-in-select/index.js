import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui';
import { $data, $isLoading, $error, fxGetVehicleIn } from '../../models/vehicle-in.model';

const VehicleInSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetVehicleIn();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const VehicleInSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<VehicleInSelect />} onChange={onChange} {...props} />;
};

export { VehicleInSelect, VehicleInSelectField };
