import { useStore } from 'effector-react';
import { Typography, Menu, List, ListItem, ListItemText, Collapse } from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  $camerasListEditMenu,
  $isCollapseCamerasList,
  changedCamerasListCollapse,
  $currentCameraId,
  moveCamera,
  deleteCamera,
} from '../../models';

const { t } = i18n;

const useStyles = makeStyles({
  menu: {
    width: '250px',
    background: 'rgba(0, 0, 0, 0.7)',
    backdropFilter: 'blur(4px)',
    borderRadius: '8px',
    maxHeight: 400,
  },
  text: {
    color: '#FFFFFF',
    fontWeight: 500,
    fontSize: 14,
  },
  icon: {
    color: '#FFFFFF',
  },
});

const EditCameraMenu = ({ isOpenEditMenu, onClose }) => {
  const { text, menu, icon } = useStyles();

  const camerasListEditMenu = useStore($camerasListEditMenu);
  const isCollapseCamerasList = useStore($isCollapseCamerasList);
  const currentCameraId = useStore($currentCameraId);

  const handleClick = () => {
    changedCamerasListCollapse(!isCollapseCamerasList);
  };

  return (
    <Menu
      anchorEl={isOpenEditMenu}
      open={Boolean(isOpenEditMenu)}
      onClose={onClose}
      anchorReference="anchorPosition"
      anchorPosition={isOpenEditMenu}
      classes={{ paper: menu }}
    >
      <List>
        <ListItem button onClick={handleClick}>
          <ListItemText
            primary={
              <Typography classes={{ root: text }}>{t('ReplaceCamera')}</Typography>
            }
          />
          {!isCollapseCamerasList ? (
            <ExpandLess className={icon} />
          ) : (
            <ExpandMore className={icon} />
          )}
        </ListItem>
        <Collapse in={!isCollapseCamerasList} timeout="auto">
          <List component="div" disablePadding>
            {camerasListEditMenu.map((camera) => (
              <ListItem
                key={camera.id}
                button
                onClick={() => {
                  moveCamera({
                    currentCameraId,
                    replacementCameraId: camera.id,
                  });
                }}
              >
                <Typography classes={{ root: text }} style={{ paddingLeft: 15 }}>
                  {camera.title}
                </Typography>
              </ListItem>
            ))}
          </List>
        </Collapse>
        <ListItem button onClick={() => deleteCamera(currentCameraId)}>
          <ListItemText
            primary={
              <Typography classes={{ root: text }}>{t('RemoveCamera')}</Typography>
            }
          />
        </ListItem>
      </List>
    </Menu>
  );
};

export { EditCameraMenu };
