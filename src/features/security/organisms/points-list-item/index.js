import { Draggable } from 'react-beautiful-dnd';
import { useStore } from 'effector-react';
import { ActionButton } from '@ui';
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Typography,
} from '@mui/material';
import { StarBorder, Star } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { $pointsMode, changedFavorites, sendAction, $complexList } from '../../models';

const { t } = i18n;

const useStyles = makeStyles({
  text: {
    fontWeight: 400,
    fontSize: 13,
    color: '#757575',
  },
  listItemAction: {
    paddingRight: 10,
  },
  containerList: {
    padding: '21px 16px 46px',
  },
  listHeader: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: 17,
  },
  button: {
    width: 100,
  },
});

const PointsListItem = ({ item, index, listId }) => {
  const { id, title, isFavorit, action, complexId } = item;

  const mode = useStore($pointsMode);
  const complexList = useStore($complexList);

  const complexTitle = complexList[complexId]?.title;

  const { text, listItemAction, containerList, listHeader, actions, button } =
    useStyles();

  if (mode === 'edit') {
    return (
      <Draggable draggableId={id} index={index} isDragDisabled={isFavorit}>
        {(provided, snapshot) => (
          <>
            <div
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              ref={provided.innerRef}
            >
              <ListItem button isDragging={snapshot.isDragging}>
                <ListItemText
                  primary={<Typography classes={{ root: text }}>{title}</Typography>}
                />
                <ListItemSecondaryAction classes={{ root: listItemAction }}>
                  <IconButton
                    edge="end"
                    onClick={() => changedFavorites(item)}
                    size="large"
                  >
                    {isFavorit ? <Star /> : <StarBorder />}
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            </div>
            {snapshot.isDragging && (
              <ListItem button>
                <ListItemText primary={`${title}`} />
              </ListItem>
            )}
          </>
        )}
      </Draggable>
    );
  }

  return (
    <div className={containerList}>
      <div className={listHeader}>
        <Typography classes={{ root: text }}>{title}</Typography>
        {listId === 'favorites' && (
          <Typography classes={{ root: text }}>{complexTitle}</Typography>
        )}
      </div>
      <div>
        <div className={actions}>
          <ActionButton
            style={{
              background: action.in !== 0 ? '#1BB169' : '#E7E7EC',
            }}
            classes={{ label: button }}
            disabled={action.in === 0}
            onClick={() => sendAction({ id: action.in, title: `${title} ${t('Open')}` })}
          >
            {t('open')}
          </ActionButton>
          <ActionButton
            classes={{ label: button }}
            disabled={action.out === 0}
            onClick={() =>
              sendAction({ id: action.out, title: `${title} ${t('ClosedHe')}` })
            }
          >
            {t('close')}
          </ActionButton>
        </div>
      </div>
    </div>
  );
};

export { PointsListItem };
