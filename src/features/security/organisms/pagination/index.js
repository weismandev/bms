import { useStore } from 'effector-react';
import {
  Pagination as MuiPagination,
  PaginationItem as MuiPaginationItem,
} from '@mui/material';
import { $currentPage, changePage, $camerasPoint, countPageVideo } from '../../models';

const Pagination = () => {
  const currentPage = useStore($currentPage);
  const camerasPoint = useStore($camerasPoint);

  const countPage = camerasPoint ? Math.ceil(camerasPoint.length / countPageVideo) : 0;

  return (
    <MuiPagination
      style={{
        padding: 24,
        display: 'flex',
        justifyContent: 'flex-end',
        flexWrap: 'nowrap',
      }}
      onChange={(_, page) => changePage(page)}
      page={currentPage}
      count={countPage}
      size="small"
      shape="rounded"
      renderItem={(item) => {
        return (
          <MuiPaginationItem
            {...item}
            style={{
              width: 20,
              height: 36,
              fontWeight: 600,
            }}
          />
        );
      }}
    />
  );
};

export { Pagination };
