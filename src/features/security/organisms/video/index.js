import { useState } from 'react';
import ReactPlayer from 'react-player';
import { useStore } from 'effector-react';
import { IconButton } from '@mui/material';
import { VisibilityOff, Fullscreen } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Loader, ActionButton } from '@ui/index';
import { openModal, $camerasMode, sendAction } from '../../models';
import { OptionsButton } from '../options-button';

const { t } = i18n;

const useStyles = makeStyles({
  fullScreenButton: {
    position: 'absolute',
    width: 34,
    height: 34,
    right: 16,
    top: 16,
    background: 'rgba(0, 0, 0, 0.5)',
    '&:hover': {
      background: 'rgba(0, 0, 0, 0.5)',
    },
  },
  container: {
    position: 'relative',
    overflow: 'hidden',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: '50% 50%',
  },
  playerWrapper: {
    position: 'relative',
    paddingTop: '56.25%',
    borderRadius: 5,
  },
  player: {
    display: (isVideoLoaded) => !isVideoLoaded && 'none',
    position: 'absolute',
    top: 0,
    left: 0,
    borderRadius: 15,
    overflow: 'hidden',
    backgroundColor: '#E7E7EC',
  },
  visibilityOffIconWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    borderRadius: 15,
    backgroundColor: '#E7E7EC',
  },
  visibilityOffIcon: {
    width: '40%',
    height: '40%',
    color: '#FFFFFF',
    margin: '8%',
  },
});

const Video = ({ streamUrl, title, action, id, hasArchive }) => {
  const [isVideoLoaded, setIsVideoLoaded] = useState(false);
  const [isHideAction, setHideAction] = useState(true);
  const [isVideoError, setVideoError] = useState(false);

  const mode = useStore($camerasMode);

  const {
    fullScreenButton,
    container,
    playerWrapper,
    player,
    visibilityOffIconWrapper,
    visibilityOffIcon,
  } = useStyles(isVideoLoaded);

  const modalData = {
    id,
    title,
    url: streamUrl,
    action,
    hasArchive,
  };

  const onPlay = () => setIsVideoLoaded(true);
  const onError = () => {
    if (isVideoLoaded) {
      return null;
    }

    setVideoError(true);
    setIsVideoLoaded(true);

    return null;
  };

  if (isVideoError || !streamUrl) {
    return (
      <div className={container}>
        <div className={visibilityOffIconWrapper}>
          <VisibilityOff className={visibilityOffIcon} />
        </div>
        <OptionsButton id={id} />
      </div>
    );
  }

  return (
    <div className={container}>
      <div
        className={playerWrapper}
        onMouseEnter={() => {
          if (mode === 'view') {
            setHideAction(false);
          }
        }}
        onMouseLeave={() => {
          if (mode === 'view') {
            setHideAction(true);
          }
        }}
      >
        <ReactPlayer
          url={streamUrl}
          width="100%"
          height="100%"
          playing
          muted={true}
          className={player}
          onPlay={onPlay}
          onError={onError}
        />
      </div>
      {isVideoLoaded ? (
        <>
          <OptionsButton id={id} />
          {!isHideAction && (
            <div
              onMouseEnter={() => {
                setHideAction(false);
              }}
              onMouseLeave={() => {
                setHideAction(true);
              }}
            >
              <IconButton
                classes={{ root: fullScreenButton }}
                onClick={() =>
                  openModal({
                    modalData,
                    name: 'video',
                  })
                }
              >
                <Fullscreen style={{ color: '#FFFFFF' }} />
              </IconButton>
              {!action?.in ? null : (
                <ActionButton
                  style={{
                    position: 'absolute',
                    width: '35%',
                    left: 24,
                    bottom: 25,
                    background: action.in !== 0 ? '#1BB169' : '#E7E7EC',
                  }}
                  onClick={() =>
                    openModal({
                      modalData,
                      name: 'openBarriers',
                    })
                  }
                  disabled={action?.in === 0}
                >
                  {t('open')}
                </ActionButton>
              )}
              {!action?.out ? null : (
                <ActionButton
                  style={{
                    position: 'absolute',
                    width: '35%',
                    right: 24,
                    bottom: 25,
                  }}
                  disabled={action?.out === 0}
                  onClick={() =>
                    sendAction({
                      id: action.out,
                      title: `${title} ${t('ClosedHe')}`,
                    })
                  }
                >
                  {t('close')}
                </ActionButton>
              )}
            </div>
          )}
        </>
      ) : (
        <div style={{ width: '100%' }}>
          <Loader isLoading />
        </div>
      )}
    </div>
  );
};

export { Video };
