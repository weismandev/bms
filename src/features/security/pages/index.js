import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isOpenEditMenu,
  changedEditMenuVisibility,
} from '../models';
import {
  Cameras,
  PointsWithoutCameras,
  SecurityModal,
  EditCameraMenu,
} from '../organisms';

const SecurityPage = () => {
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isOpenEditMenu = useStore($isOpenEditMenu);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <SecurityModal />
      <ColoredTextIconNotification />

      <EditCameraMenu
        isOpenEditMenu={isOpenEditMenu}
        onClose={() => changedEditMenuVisibility({ open: null })}
      />

      <FilterMainDetailLayout
        main={<Cameras />}
        detail={null}
        filter={<PointsWithoutCameras />}
      />
    </>
  );
};

const RestrictedSecurityPage = (props) => (
  <HaveSectionAccess>
    <SecurityPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedSecurityPage as SecurityPage };
