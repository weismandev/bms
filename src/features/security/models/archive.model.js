import { createStore, createEvent, createEffect } from 'effector';
import { addSeconds, format } from 'date-fns';

const startSeconds = -1800;
const endSeconds = 1800;
const stepSeconds = 300;

const getArchive = createEvent();
const changePlaying = createEvent();

const fxGetArchive = createEffect();

const $archiveDate = createStore(null);
const $currentTimeline = createStore(null);
const $archive = createStore(null);
const $timeLine = $archiveDate.map((date) => {
  const marks = [];

  const getTime = (time) => {
    if (date) {
      return format(new Date(addSeconds(Number(new Date(date)), time)), 'HH:mm');
    }

    return null;
  };

  for (let i = startSeconds; i <= endSeconds; i += stepSeconds) {
    marks.push({ value: i, label: getTime(i) });
  }

  return marks;
});
const $isPlaying = createStore(true);

export {
  getArchive,
  fxGetArchive,
  $archive,
  $archiveDate,
  $timeLine,
  startSeconds,
  endSeconds,
  stepSeconds,
  $currentTimeline,
  $isPlaying,
  changePlaying,
};
