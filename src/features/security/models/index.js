import './archive.init';
import './cameras.init';
import './modal.init';
import './page.init';
import './parking-slot.init';
import './points-without-camera.init';
import './vehicle-in.init';

export {
  $error,
  $isLoading,
  PageGate,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  sendAction,
  saveData,
  $complexList,
  markDrivenIn,
  markDrivenOut,
} from './page.model';

export {
  $dataModal,
  $typeOpenBarriersModal,
  changedTypeOpenBarriersModal,
  openModal,
  closeModal,
  $isModalVideoArchive,
  setIsModalVideoArchive,
  $modalName,
} from './modal.model';

export {
  $currentPage,
  $currentCameras,
  countPageVideo,
  $points,
  $camerasPoint,
  changePage,
  $camerasMode,
  changeCamerasModeToEdit,
  resetCamerasList,
  changedEditMenuVisibility,
  $isOpenEditMenu,
  changedCamerasListCollapse,
  $isCollapseCamerasList,
  $camerasListEditMenu,
  $currentCameraId,
  moveCamera,
  deleteCamera,
  $houses,
  changeVisibilityBuildingsSelect,
  $isOpenBuildingsSelect,
  $buildings,
  changeBuildings,
  $complex,
  changeComplex,
  setAlarm,
} from './cameras.model';

export {
  $lists,
  moveColumn,
  changedFavorites,
  $collapseId,
  changedCollapseId,
  changePointsModeToEdit,
  resetPointsList,
  $pointsMode,
} from './points-without-camera.model';

export {
  getArchive,
  $archive,
  $timeLine,
  startSeconds,
  endSeconds,
  stepSeconds,
  $currentTimeline,
  $isPlaying,
  changePlaying,
} from './archive.model';
