import { createStore, createEvent } from 'effector';

const changedTypeOpenBarriersModal = createEvent();
const openModal = createEvent();
const closeModal = createEvent();
const setIsModalVideoArchive = createEvent();

const $modalName = createStore(null);
const $dataModal = createStore(null);
const $typeOpenBarriersModal = createStore(null);
const $isModalVideoArchive = createStore(false);

export {
  $dataModal,
  $typeOpenBarriersModal,
  changedTypeOpenBarriersModal,
  openModal,
  closeModal,
  $isModalVideoArchive,
  setIsModalVideoArchive,
  $modalName,
};
