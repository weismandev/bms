import { signout } from '@features/common';
import {
  fxGetPoints,
  pageUnmounted,
  fxSaveInStorage,
  fxGetFromStorage,
} from './page.model';
import {
  $lists,
  moveColumn,
  changedFavorites,
  $collapseId,
  changedCollapseId,
  changePointsModeToEdit,
  $pointsMode,
} from './points-without-camera.model';

$pointsMode
  .on(changePointsModeToEdit, () => 'edit')
  .on([fxSaveInStorage.done, fxGetPoints.done, fxGetFromStorage.done], () => 'view')
  .reset([pageUnmounted, signout]);

$lists
  .on(moveColumn, (state, { source, destination }) => {
    const allLists = { ...state.favoritesList, ...state.pointsLists };

    if (!destination) return state;

    const start = allLists[source.droppableId];
    const end = allLists[destination.droppableId];

    if (
      end.list.find((item) => item.id === start.list[source.index].id) ||
      end.id !== 'favorites'
    ) {
      return state;
    }

    start.list[source.index].isFavorit = true;

    const endList = [...end.list, start.list[source.index]];

    const newEndCol = {
      id: end.id,
      list: endList,
    };

    return { ...state, ...{ favoritesList: { [newEndCol.id]: newEndCol } } };
  })
  .on(changedFavorites, ({ pointsLists, favoritesList }, result) => {
    const { isFavorit, id, complexId } = result;

    const favoriteColumn = 'favorites';
    const column = complexId.toString();

    const fList = favoritesList[favoriteColumn].list;
    const pList = pointsLists[column].list;

    const newPList = pList.map((item) => {
      if (item.id === id) {
        item.isFavorit = !item.isFavorit;
      }
      return item;
    });

    const newPointsLists = {
      ...pointsLists,
      [column]: { id: column, list: newPList },
    };

    if (column === favoriteColumn) {
      const newFList = fList.filter((element) => element.id !== id);

      return {
        favoritesList: {
          ...favoritesList,
          [favoriteColumn]: { id: favoriteColumn, list: newFList },
        },
        pointsLists: newPointsLists,
      };
    }

    if (!isFavorit) {
      const newFlist = [...fList, result];

      return {
        favoritesList: {
          ...favoritesList,
          [favoriteColumn]: { id: favoriteColumn, list: newFlist },
        },
        pointsLists: newPointsLists,
      };
    }

    const newFList = fList.filter((element) => element.id !== id);

    return {
      favoritesList: {
        ...favoritesList,
        [favoriteColumn]: { id: favoriteColumn, list: newFList },
      },
      pointsLists: newPointsLists,
    };
  })
  .reset([signout, pageUnmounted]);

$collapseId.on(changedCollapseId, (_, result) => result).reset([signout, pageUnmounted]);
