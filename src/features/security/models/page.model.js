import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { fxSaveInStorage, fxGetFromStorage } from '@features/common';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const changedErrorDialogVisibility = createEvent();
const sendAction = createEvent();
const saveData = createEvent();
const markDrivenIn = createEvent();
const markDrivenOut = createEvent();

const fxGetCamerasList = createEffect();
const fxGetPoints = createEffect();
const fxSendAction = createEffect();
const fxGetComplexList = createEffect();
const fxMarkDrivenIn = createEffect();
const fxMarkDrivenOut = createEffect();

const $complexList = createStore([]);
const $cameras = createStore([]);
const $points = createStore([]);
const $savedData = createStore(null);
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);

export {
  $error,
  $isLoading,
  $cameras,
  PageGate,
  fxGetCamerasList,
  pageMounted,
  pageUnmounted,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetPoints,
  $points,
  sendAction,
  fxSendAction,
  fxSaveInStorage,
  fxGetFromStorage,
  saveData,
  $savedData,
  $complexList,
  fxGetComplexList,
  markDrivenIn,
  fxMarkDrivenIn,
  markDrivenOut,
  fxMarkDrivenOut,
};
