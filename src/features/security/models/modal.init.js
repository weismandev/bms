import { signout } from '@features/common';
import {
  $dataModal,
  $typeOpenBarriersModal,
  changedTypeOpenBarriersModal,
  openModal,
  closeModal,
  $isModalVideoArchive,
  setIsModalVideoArchive,
  $modalName,
} from './modal.model';
import { fxSendAction, pageUnmounted } from './page.model';

$modalName
  .on(openModal, (_, { name }) => name)
  .reset([closeModal, pageUnmounted, signout]);

$dataModal
  .on(openModal, (_, { modalData }) => modalData)
  .on(fxSendAction.done, (_, { params }) => ({
    title: params.title,
  }))
  .reset([pageUnmounted, signout]);

$typeOpenBarriersModal
  .on(changedTypeOpenBarriersModal, (_, type) => type)
  .reset([closeModal, pageUnmounted, signout]);

$isModalVideoArchive
  .on(setIsModalVideoArchive, (_, archive) => archive)
  .reset([closeModal, pageUnmounted, signout]);
