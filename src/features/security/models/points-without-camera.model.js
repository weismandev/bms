import { createStore, createEvent, combine } from 'effector';
import { $points, $savedData } from './page.model';

const moveColumn = createEvent();
const changedFavorites = createEvent();
const changedCollapseId = createEvent();
const changePointsModeToEdit = createEvent();
const resetPointsList = createEvent();

const $lists = combine($points, $savedData, (points, savedData) => {
  if (savedData && savedData.points.length !== 0) {
    const pointsList = points.reduce((acc, point) => {
      if (point.cameraId !== 0 || point.complexId === 0) {
        return acc;
      }

      return { ...acc, ...{ [point.id]: point } };
    }, {});

    const formattedSavedPoints = savedData.points.reduce((acc, point) => {
      const newPoint = pointsList[point];

      if (!newPoint) {
        return acc;
      }

      newPoint.id = newPoint.id.toString();
      newPoint.isFavorit = true;

      return [...acc, newPoint];
    }, []);

    const favoritesList = {
      favorites: {
        id: 'favorites',
        list: formattedSavedPoints,
      },
    };

    const differencePoints = Object.values(pointsList).filter(
      (point) =>
        !formattedSavedPoints.some(
          (currentPoint) =>
            Number.parseInt(currentPoint.id, 10) === Number.parseInt(point.id, 10)
        )
    );

    const pointsForFormat = [...formattedSavedPoints, ...differencePoints];

    const formattedPoints = pointsForFormat.reduce((acc, point) => {
      point.id = point.id.toString();
      point.isFavorit = Boolean(point.isFavorit);

      const { complexId } = point;

      if (acc[complexId]) {
        const complexArray = acc[complexId];
        const newValue = [...complexArray, point];

        return { ...acc, [complexId]: newValue };
      }

      return {
        ...acc,
        [complexId]: [point],
      };
    }, {});

    const pointsLists = Object.entries(formattedPoints).reduce((acc, value) => {
      const id = value[0];
      const list = value[1];

      return {
        ...acc,
        [id]: {
          id,
          list,
        },
      };
    }, {});

    return { pointsLists, favoritesList };
  }

  const formattedPoints = points.reduce((acc, value) => {
    const pointData = {
      id: value.id.toString(),
      title: value.title,
      cameraId: value.cameraId,
      complexId: value.complexId,
      action: value.action,
      isFavorit: false,
    };

    const { complexId, cameraId } = pointData;

    if (cameraId !== 0 || complexId === 0) {
      return acc;
    }

    if (acc[complexId]) {
      const complexArray = acc[complexId];
      const newValue = [...complexArray, pointData];

      return { ...acc, [complexId]: newValue };
    }

    return {
      ...acc,
      [complexId]: [pointData],
    };
  }, {});

  const pointsLists = Object.entries(formattedPoints).reduce((acc, value) => {
    const id = value[0];
    const list = value[1];

    return {
      ...acc,
      [id]: {
        id,
        list,
      },
    };
  }, {});

  const favoritesList = {
    favorites: {
      id: 'favorites',
      list: [],
    },
  };

  return { pointsLists, favoritesList };
});
const $collapseId = createStore('favorites');
const $pointsMode = createStore('view');

export {
  $lists,
  moveColumn,
  changedFavorites,
  $collapseId,
  changedCollapseId,
  changePointsModeToEdit,
  $pointsMode,
  resetPointsList,
};
