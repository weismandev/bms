import { createEffect, createStore } from 'effector';

const fxGetParkingSlots = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

export { fxGetParkingSlots, $data, $error, $isLoading };
