import { forward, sample } from 'effector';

import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import Done from '@img/done.svg';
import { changeNotification } from '@features/colored-text-icon-notification';
import {
  $camerasMode,
  changeCamerasModeToEdit,
  changedEditMenuVisibility,
  $isOpenEditMenu,
  changedCamerasListCollapse,
  $isCollapseCamerasList,
  $currentCameraId,
  moveCamera,
  $camerasPoint,
  deleteCamera,
  fxGetHls,
  getHls,
  $hlsStreams,
  changePage,
  fxGetHouses,
  $houses,
  changeVisibilityBuildingsSelect,
  $isOpenBuildingsSelect,
  $buildings,
  changeBuildings,
  $complex,
  changeComplex,
  setAlarm,
  fxSetAlarm,
} from './cameras.model';

import {
  pageUnmounted,
  fxGetCamerasList,
  fxSaveInStorage,
  pageMounted,
} from './page.model';

const { t } = i18n;

$camerasMode
  .on(changeCamerasModeToEdit, () => 'edit')
  .on([fxGetCamerasList.done, fxSaveInStorage.done], () => 'view')
  .reset([pageUnmounted, signout]);

$isOpenEditMenu
  .on(changedEditMenuVisibility, (_, { open }) => open)
  .reset([moveCamera, deleteCamera, pageUnmounted, signout]);

$currentCameraId
  .on(changedEditMenuVisibility, (_, { id }) => id)
  .reset([pageUnmounted, signout]);

$isCollapseCamerasList
  .on(changedCamerasListCollapse, (_, collapse) => collapse)
  .reset([changedEditMenuVisibility, pageUnmounted, signout]);

$camerasPoint
  .on(moveCamera, (state, { currentCameraId, replacementCameraId }) => {
    const currentCameraIndex = state.indexOf(
      state.find((item) => item.id === currentCameraId)
    );
    const replacementCameraIndex = state.indexOf(
      state.find((item) => item.id === replacementCameraId)
    );

    const currentCamera = state[currentCameraIndex];

    state[currentCameraIndex] = state[replacementCameraIndex];
    state[replacementCameraIndex] = currentCamera;

    return [...state];
  })
  .on(deleteCamera, (state, currentCameraId) =>
    state.filter((item) => item.id !== currentCameraId)
  )
  .reset([pageUnmounted, signout]);

forward({
  from: getHls,
  to: fxGetHls,
});

$hlsStreams
  .on(fxGetHls.done, (state, { params, result }) => ({
    ...state,
    ...{ [params.id]: result?.url },
  }))
  .reset([pageUnmounted, signout, changePage]);

forward({
  from: pageMounted,
  to: fxGetHouses,
});

$houses
  .on(fxGetHouses.done, (_, { result }) =>
    Array.isArray(result?.buildings) ? result.buildings : []
  )
  .reset([pageUnmounted, signout]);

$isOpenBuildingsSelect
  .on(changeVisibilityBuildingsSelect, (_, visibility) => visibility)
  .reset([pageUnmounted, signout]);

$buildings
  .on(changeBuildings, (_, building) => building)
  .reset([pageUnmounted, signout, fxSetAlarm.done, fxSetAlarm.fail]);

$complex
  .on(changeComplex, (_, complex) => complex)
  .reset([pageUnmounted, signout, fxSetAlarm.done, fxSetAlarm.fail]);

sample({
  clock: setAlarm,
  source: { complex: $complex, building: $buildings },
  fn: ({ complex, building }, action) => {
    const formattedComplex = Object.entries(complex).reduce((acc, value) => {
      if (!value[1]) {
        return acc;
      }

      return [...acc, value[0]];
    }, []);

    return {
      action: Number(Boolean(action)),
      complex_ids:
        formattedComplex.length > 0 ? formattedComplex.join(',') : '',
      buildings_ids: building.map((item) => item.id).join(','),
    };
  },
  target: fxSetAlarm,
});

sample({
  clock: fxSetAlarm.done,
  fn: () => ({
    isOpen: true,
    color: '#1BB169',
    text: t('CommandCompleted'),
    Icon: Done,
  }),
  target: changeNotification,
});
