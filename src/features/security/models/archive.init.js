import { sample } from 'effector';
import { addSeconds } from 'date-fns';

import { signout } from '@features/common';

import {
  getArchive,
  fxGetArchive,
  $archive,
  $archiveDate,
  $currentTimeline,
  $isPlaying,
  changePlaying,
} from './archive.model';
import { pageUnmounted } from './page.model';
import { closeModal } from './modal.model';

$archive
  .on(fxGetArchive.done, (_, { result }) => result)
  .reset([closeModal, pageUnmounted, signout]);

$archiveDate
  .on(getArchive, (_, result) => result?.values?.timestamp)
  .reset([closeModal, pageUnmounted, signout]);

$currentTimeline.on(getArchive, (_, { newTime }) => newTime);

$isPlaying
  .on(changePlaying, (_, isPlaying) => isPlaying)
  .reset([closeModal, pageUnmounted, signout]);

sample({
  clock: getArchive,
  source: $archiveDate,
  fn: (clockData, sourceData) => {
    const { newTime, values, id } = sourceData;

    const newTimestamp = newTime
      ? addSeconds(Number(new Date(clockData)), newTime)
      : null;

    const dt =
      (newTime || newTime === 0) && newTimestamp
        ? new Date(newTimestamp) / 1000
        : new Date(values.timestamp) / 1000;

    return {
      id,
      dt,
    };
  },
  target: fxGetArchive,
});
