import { createEffect, createStore } from 'effector';

const fxGetVehicleIn = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

export { fxGetVehicleIn, $data, $error, $isLoading };
