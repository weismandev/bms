import { restore, combine, createEvent, createStore, createEffect } from 'effector';
import { $cameras, $points, $savedData } from './page.model';

const countPageVideo = 6;

const fxGetHls = createEffect(async ({ url }) => {
  const data = await fetch(url);

  return data.json();
});
export const fxGetHouses = createEffect();
export const fxSetAlarm = createEffect();

const changePage = createEvent();
const changeCamerasModeToEdit = createEvent();
const changedEditMenuVisibility = createEvent();
const changedCamerasListCollapse = createEvent();
const moveCamera = createEvent();
const deleteCamera = createEvent();
const resetCamerasList = createEvent();
const getHls = createEvent();
export const changeVisibilityBuildingsSelect = createEvent();
export const changeBuildings = createEvent();
export const changeComplex = createEvent();
export const setAlarm = createEvent();

export const $isOpenBuildingsSelect = createStore(false);
export const $houses = createStore([]);
export const $buildings = createStore([]);
export const $complex = createStore([]);
const $camerasMode = createStore('view');
const $hlsStreams = createStore({});
const $currentPage = restore(changePage, 1);
const $camerasData = combine($cameras, $points, (cameras, points) => {
  const formattedPoints = points.reduce((acc, value) => {
    return { ...acc, [value.cameraId]: value };
  }, {});

  return cameras.reduce((acc, value) => {
    const point = formattedPoints[value.id];

    const pointsParams = {
      title: point?.title || value?.camTitle,
      complexId: point?.complexId || '',
      action: point?.action || { in: 0, out: 0 },
    };

    return [...acc, { ...value, ...pointsParams }];
  }, []);
});

const $camerasPoint = combine($camerasData, $savedData, (camerasData, savedData) => {
  if (savedData && savedData.cameras.length !== 0) {
    if (camerasData.length === 0) {
      return [];
    }

    const camerasList = camerasData.reduce(
      (acc, camera) => ({ ...acc, ...{ [camera.id]: camera } }),
      {}
    );

    const cameras = savedData.cameras.reduce((acc, camera) => {
      if (!camerasList[camera]) {
        return acc;
      }

      return [...acc, camerasList[camera]];
    }, []);

    const differenceCameras = camerasData.filter(
      (camera) => !cameras.some((currentCameras) => currentCameras.id === camera.id)
    );

    return [...cameras, ...differenceCameras];
  }

  return camerasData;
});
const $currentCameras = combine(
  $camerasPoint,
  $currentPage,
  $hlsStreams,
  (camerasPoint, currentPage, hlsStreams) => {
    const start = (currentPage - 1) * countPageVideo;

    const cameras = camerasPoint.slice(start, start + countPageVideo);

    if (Object.values(hlsStreams).length === 0) {
      cameras.forEach((camera) => getHls({ id: camera.id, url: camera.url }));
    }

    return cameras.reduce((acc, camera) => {
      if (!hlsStreams[camera.id]) {
        camera.streamUrl = '';

        return [...acc, camera];
      }

      const hls = hlsStreams[camera.id];

      camera.streamUrl = hls;

      return [...acc, camera];
    }, []);
  }
);
const $isOpenEditMenu = createStore(null);
const $isCollapseCamerasList = createStore(true);
const $currentCameraId = createStore(null);
const $camerasListEditMenu = combine(
  $camerasPoint,
  $currentCameraId,
  (camerasPoint, currentCameraId) =>
    camerasPoint.filter((camera) => camera.id !== currentCameraId)
);

export {
  $currentPage,
  $currentCameras,
  countPageVideo,
  $points,
  $camerasPoint,
  changePage,
  $camerasMode,
  changeCamerasModeToEdit,
  resetCamerasList,
  changedEditMenuVisibility,
  $isOpenEditMenu,
  changedCamerasListCollapse,
  $isCollapseCamerasList,
  $currentCameraId,
  $camerasListEditMenu,
  moveCamera,
  deleteCamera,
  fxGetHls,
  getHls,
  $hlsStreams,
};
