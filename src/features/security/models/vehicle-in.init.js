import { signout } from '@features/common';
import { securityApi } from '../api';
import { fxGetVehicleIn, $data, $error, $isLoading } from './vehicle-in.model';

fxGetVehicleIn.use(securityApi.getVehiclesIn);

$data
  .on(fxGetVehicleIn.done, (_, { result }) => {
    const vehicles = result.result;

    if (!Array.isArray(vehicles)) {
      return [];
    }

    return vehicles.map((vehicle) => ({
      id: vehicle.vehicle.id,
      title: vehicle.vehicle.id_number,
    }));
  })
  .reset(signout);

$error.on(fxGetVehicleIn.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetVehicleIn.pending, (_, result) => result).reset(signout);
