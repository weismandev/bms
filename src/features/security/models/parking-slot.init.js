import { signout } from '@features/common';
import { securityApi } from '../api';
import { fxGetParkingSlots, $data, $error, $isLoading } from './parking-slot.model';

fxGetParkingSlots.use(securityApi.getPakingSlots);

$data
  .on(fxGetParkingSlots.done, (_, { result }) => {
    const { slots } = result;

    if (!Array.isArray(slots)) {
      return [];
    }

    return slots.map((slot) => ({
      id: slot.id,
      title: slot.number,
    }));
  })
  .reset(signout);

$error.on(fxGetParkingSlots.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetParkingSlots.pending, (_, result) => result).reset(signout);
