import { forward, sample, merge, restore, split, createEvent } from 'effector';

import Done from '@img/done.svg';
import { signout } from '@features/common';
import { changeNotification } from '@features/colored-text-icon-notification';
import { securityApi } from '../api';

import {
  $error,
  $isLoading,
  $cameras,
  fxGetCamerasList,
  pageMounted,
  pageUnmounted,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetPoints,
  $points,
  sendAction,
  fxSendAction,
  fxGetFromStorage,
  fxSaveInStorage,
  saveData,
  $savedData,
  $complexList,
  fxGetComplexList,
  markDrivenIn,
  fxMarkDrivenIn,
  markDrivenOut,
  fxMarkDrivenOut,
} from './page.model';

import {
  $camerasPoint,
  resetCamerasList,
  fxGetHouses,
  fxSetAlarm,
} from './cameras.model';
import { $lists, resetPointsList } from './points-without-camera.model';
import { fxGetArchive } from './archive.model';

fxGetCamerasList.use(securityApi.getCamerasList);
fxGetPoints.use(securityApi.getPoints);
fxSendAction.use(securityApi.sendAction);
fxGetComplexList.use(securityApi.getComplexList);
fxGetArchive.use(securityApi.getArchive);
fxMarkDrivenIn.use(securityApi.markDrivenIn);
fxMarkDrivenOut.use(securityApi.markDrivenOut);
fxGetHouses.use(securityApi.getHouses);
fxSetAlarm.use(securityApi.setAlarm);

$points
  .on(fxGetPoints.done, (_, { result }) => {
    const { point } = result;

    if (!Array.isArray(point)) {
      return [];
    }

    return point.map((item) => ({
      id: item.id,
      title: item.title,
      cameraId: item.cameras[0] || 0,
      complexId: item.complex_id,
      action: item.action,
    }));
  })
  .reset([pageUnmounted, signout]);

$cameras
  .on(fxGetCamerasList.done, (_, { result }) => {
    const { camera } = result;

    if (!Array.isArray(camera)) {
      return [];
    }

    return camera.map((item) => ({
      id: item.id,
      url: item.streams[0]?.url,
      streamUrl: '',
      hasArchive: item.has_archive,
      camTitle: item.title,
      camRatio: item.ratio,
    }));
  })
  .reset([pageUnmounted, signout]);

$complexList
  .on(fxGetComplexList.done, (_, { result }) =>
    result.items.reduce(
      (acc, value) => ({
        ...acc,
        [value.id]: {
          title: value.title,
        },
      }),
      {}
    )
  )
  .reset([pageUnmounted, signout]);

// Миграция формата данных индивидуальных настроек камер и точек доступа экрана охранника
// в следствии изменений из задачи UJIN-815

const mustBeConverted = createEvent();
const returnAsIs = createEvent();

// для начала возьмем сохраненные данные как есть
const $earlierSaved = restore(fxGetFromStorage.doneData, {});

split({
  source: $earlierSaved,
  match: {
    // если функционал сохранения ни разу не использавался то вернется null
    // поэтому обязательно сначала проверить этот кейс
    neverUsed: (data) => !Boolean(data),
    // старый и новый форматы отличаются в частности полем points
    // в старом формате это всегда объект, при условии, что функционалом хоть раз
    // воспользовались
    oldFormat: ({ points }) => !Array.isArray(points),
  },
  cases: {
    neverUsed: returnAsIs,
    oldFormat: mustBeConverted.prepend(convertData),
    // если не один из кейсов не случился, считаем что нам пришел новый формат
    // и возвращаем как есть, как и в случае с null
    __: returnAsIs,
  },
});

function convertData({ cameras = [], points = {} }) {
  const pointList = points?.favoritesList?.favorites?.list || [];
  return {
    cameras: cameras.map(({ id }) => Number(id)),
    points: pointList.map(({ id }) => Number(id)),
  };
}

$savedData.on(returnAsIs, (_, data) => data).on(mustBeConverted, (_, data) => data);

// если формат данных был старый, то сохраняем сконвертированные данные
forward({
  from: mustBeConverted,
  to: fxSaveInStorage.prepend((payload) => ({
    storage_key: 'security',
    data: payload,
  })),
});

// вернуть после того как все УК мигрируют
// $savedData.on(fxGetFromStorage.doneData, (_, data) => data);

// конец миграции /////////////////////////////////////////////////////

const requestOccured = [
  fxGetCamerasList.pending,
  fxGetPoints.pending,
  fxSendAction.pending,
  fxSaveInStorage.pending,
  fxGetFromStorage.pending,
  fxGetComplexList.pending,
  fxGetArchive.pending,
  fxSetAlarm.pending,
];

const errorOccured = merge([
  fxGetCamerasList.fail,
  fxGetPoints.fail,
  fxSendAction.fail,
  fxSaveInStorage.fail,
  fxGetFromStorage.fail,
  fxGetComplexList.fail,
  fxGetArchive.fail,
  fxMarkDrivenIn.fail,
  fxMarkDrivenOut.fail,
  fxGetHouses.fail,
  fxSetAlarm.fail,
]);

$isLoading.on(requestOccured, (_, pending) => pending).reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: saveData,
  source: [$camerasPoint, $lists],
  fn: ([cameras, points]) => {
    const camerasId = cameras.map((camera) => camera.id);

    const pointsList = Object.values(points.pointsLists).map((item) =>
      item.list.reduce((acc, elem) => {
        if (!elem.isFavorit) {
          return acc;
        }

        return [...acc, Number.parseInt(elem.id, 10)];
      }, [])
    );

    return {
      data: { cameras: camerasId, points: pointsList.flat() },
      storage_key: 'security',
    };
  },
  target: fxSaveInStorage,
});

forward({
  from: fxSendAction.done,
  to: changeNotification.prepend(({ params }) => ({
    isOpen: true,
    color: '#1BB169',
    text: params.title,
    Icon: Done,
  })),
});

sample({
  clock: sendAction,
  fn: (_, sourceData) => ({
    id: sourceData.id,
    parking_slot_id: sourceData.parking_slot_id?.id,
    vehicle_number: sourceData.vehicle_number?.title
      ? sourceData.vehicle_number.title
      : sourceData.vehicle_number,
    title: sourceData.title,
  }),
  target: fxSendAction,
});

sample({
  clock: markDrivenIn,
  fn: (_, { vehicleNumber, parkingSlot }) => ({
    id_number: vehicleNumber,
    slot_id: parkingSlot.id,
  }),
  target: fxMarkDrivenIn,
});

sample({
  clock: markDrivenOut,
  fn: (_, { id }) => ({
    vehicle_id: id,
  }),
  target: fxMarkDrivenOut,
});

forward({
  from: resetCamerasList,
  to: [fxGetCamerasList, fxGetFromStorage.prepend(() => ({ storage_key: 'security' }))],
});

forward({
  from: resetPointsList,
  to: [fxGetPoints, fxGetFromStorage.prepend(() => ({ storage_key: 'security' }))],
});

forward({
  from: pageMounted,
  to: [
    fxGetCamerasList,
    fxGetPoints,
    fxGetFromStorage.prepend(() => ({ storage_key: 'security' })),
    fxGetComplexList,
  ],
});
