import { api } from '@api/api2';
import { getNameUserPlatform } from '@tools/getNameUserPlatform';

const getCamerasList = () =>
  api.no_vers('get', 'akado/get-stream-v2', {
    hls: 1,
    platform: getNameUserPlatform(),
  });
const getPoints = () => api.no_vers('get', 'geo/get-points', { ac: 1 });
const sendAction = (payload) => api.no_vers('get', 'geo/send_action', payload);
const getPakingSlots = (payload) =>
  api.v1('get', 'parking/guard/get-available-parking-slots', payload);
const getComplexList = () => api.v1('get', 'complex/list');
const getArchive = (payload) =>
  api.no_vers('get', 'videoarchive/get-archive-stream', payload);
const markDrivenIn = (payload) => api.v1('post', 'parking/guard/drive-in', payload);
const getVehiclesIn = (payload) =>
  api.v1('post', 'parking/guard/search-vehicles-in', payload);
const markDrivenOut = (payload) =>
  api.v1('post', 'parking/guard/drive-out', payload);
const getHouses = () =>
  api.v1('get', 'buildings/get-list-crm', { page: 1, per_page: 1000 });
const setAlarm = (payload) =>
  api.no_vers('get', 'geo/set-points-alarm-state', payload);

export const securityApi = {
  getCamerasList,
  getPoints,
  sendAction,
  getPakingSlots,
  getComplexList,
  getArchive,
  markDrivenIn,
  getVehiclesIn,
  markDrivenOut,
  getHouses,
  setAlarm,
};
