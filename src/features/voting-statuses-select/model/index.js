import { createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { statusesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

const SelectGate = createGate();

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result || !result.items) {
      return [];
    }

    return result.items;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);
fxGetList.use(statusesApi.get);

export { fxGetList, $data, $error, $isLoading, SelectGate };
