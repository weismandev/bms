import { api } from '../../../api/api2';

const get = () => api.v1('get', 'house-vote/crm/statuses');

export const statusesApi = { get };
