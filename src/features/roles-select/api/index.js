import { api } from '../../../api/api2';

const getList = () => api.v1('post', 'role/list', {});

export const rolesApi = {
  getList,
};
