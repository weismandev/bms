import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { competencesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (_, { result }) => (result && result.competences) || [])
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);

fxGetList.use(competencesApi.getList);

export { fxGetList, $data, $error, $isLoading };
