import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  closeWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: '29px 29px 5px',
  },
});
