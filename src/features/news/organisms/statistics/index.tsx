import { useMemo, FC, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { PagingState, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  TableColumnResizing,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Loader,
  CustomScrollbar,
  CloseButton,
  StatisticsTemplate,
  StatisticsPaper,
} from '@ui/index';
import { IStatistic } from '../../interfaces/statistics';
import {
  $isLoading,
  $statistics,
  $tableData,
  columns as columnsLogs,
  $columnWidths,
  columnWidthsChanged,
  $tableDataStatistics,
  $tableColumnsStatistics,
  $rowCount,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $isLoadingStatisticsLogs,
  changeOpenModal,
  $opened,
  changedDetailVisibility,
} from '../../models';
import { NewsModalStatistics } from '../../molecules/statistics-modal';
import { useStyles } from './styles';

const { t } = i18n;

const Row = (props: { children: ReactNode }) => {
  const row = useMemo(() => <TableRow {...props} />, [props.children]);
  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => {
  return (
    <Grid.Root {...props} style={{ height: '100%' }}>
      {children}
    </Grid.Root>
  );
};

const NewsStatistics: FC = () => {
  const isLoading = useStore($isLoading);
  const statistics: null | IStatistic = useStore($statistics);
  const tableDataLogs = useStore($tableData);
  const columnWidthsLogs = useStore($columnWidths);
  const tableDataStatistics = useStore($tableDataStatistics);
  const tableColumnsStatistics = useStore($tableColumnsStatistics);
  const rowCount = useStore($rowCount);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const isLoadingStatisticsLogs = useStore($isLoadingStatisticsLogs);
  const opened = useStore($opened);

  const { title } = opened;
  const classes = useStyles();

  if (isLoading || !statistics) {
    return null;
  }

  const { count, coverage, read, unread } = statistics;

  const viewStatistics = (
    <>
      <StatisticsPaper
        titleOne={t('TotalUsers')}
        valueOne={count}
        titleTwo={t('Coverage')}
        valueTwo={`${coverage}%`}
      />

      <StatisticsPaper
        titleOne={t('GotAcquainted')}
        valueOne={read}
        titleTwo={t('NotFamiliarized')}
        valueTwo={unread}
      />
    </>
  );

  const viewStatisticsTable = (
    <Grid
      rootComponent={Root}
      rows={tableDataStatistics}
      columns={tableColumnsStatistics}
    >
      <Table
        messages={{ noData: t('noData') }}
        cellComponent={TableCell}
        containerComponent={TableContainer}
      />
    </Grid>
  );

  const viewStatusticsLogsTable = !isLoadingStatisticsLogs ? (
    <Grid
      rootComponent={Root}
      rows={tableDataLogs}
      getRowId={(row) => row && row.id}
      columns={columnsLogs}
    >
      <PagingState
        currentPage={currentPage}
        onCurrentPageChange={pageNumberChanged}
        pageSize={pageSize}
        onPageSizeChange={pageSizeChanged}
      />

      <CustomPaging totalCount={rowCount} />

      <Table
        messages={{ noData: t('noData') }}
        rowComponent={Row}
        cellComponent={TableCell}
        containerComponent={TableContainer}
      />

      <TableColumnResizing
        columnWidths={columnWidthsLogs}
        onColumnWidthsChange={columnWidthsChanged}
      />

      <TableHeaderRow cellComponent={TableHeaderCell} />

      <PagingPanel
        pageSizes={[10, 25, 50, 100]}
        messages={{
          rowsPerPage: t('EntriesPerPage'),
          info: () => '',
        }}
      />
    </Grid>
  ) : (
    <Loader isLoading />
  );

  return (
    <>
      <NewsModalStatistics />

      <div className={classes.closeWrapper}>
        <CloseButton onClick={() => changedDetailVisibility(false)} />
      </div>

      <CustomScrollbar style={{ height: 'calc(100% - 130px)' }}>
        <StatisticsTemplate
          openModal={() => changeOpenModal(true)}
          title={title}
          dates={statistics.dates}
          viewStatistics={viewStatistics}
          viewStatisticsTable={viewStatisticsTable}
          viewStatusticsLogsTable={viewStatusticsLogsTable}
        />
      </CustomScrollbar>
    </>
  );
};

export { NewsStatistics };
