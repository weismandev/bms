import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  text: Yup.string().required(t('thisIsRequiredField')),
  title: Yup.string().required(t('thisIsRequiredField')),
  buildings: Yup.array()
    .min(1, t('thisIsRequiredField'))
    .required(t('thisIsRequiredField')),
});
