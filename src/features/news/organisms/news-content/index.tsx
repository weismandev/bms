import { FC } from 'react';
import DatePicker from 'react-datepicker';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import {
  Formik,
  Form,
  Field,
  FieldProps,
  FieldArray,
  FieldArrayRenderProps,
} from 'formik';
import { TargetedContent } from '@features/targeted-content';
import {
  InputField,
  SelectField,
  CustomScrollbar,
  DetailToolbar,
  SelectBuildings,
} from '@ui/index';
import {
  $opened,
  $mode,
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
  changedDeleteVisibilityModal,
  $houses,
  $isLoading,
  $currentPageTab,
} from '../../models';
import { ContentInput } from '../../molecules/content-input';
import { ImageInput } from '../../molecules/images-input';
import { useStyles } from './styles';
import { validationSchema } from './validation';

export const NewsContent: FC = () => {
  const { t } = useTranslation();

  const classes = useStyles();

  const opened = useStore($opened);
  const mode = useStore($mode);
  const houses = useStore($houses);
  const isLoading = useStore($isLoading);
  const currentPageTab = useStore($currentPageTab);

  const isNewNews = !opened.id;

  if (isLoading) {
    return null;
  }

  const today = new Date();

  return (
    <Formik
      initialValues={opened}
      validationSchema={validationSchema}
      onSubmit={detailSubmitted}
      enableReinitialize
      render={({ setFieldValue, values, errors, resetForm }) => (
        <Form className={classes.form}>
          <DetailToolbar
            mode={mode}
            className={classes.detailToolbar}
            hidden={{ send: true }}
            onClose={() => {
              resetForm();
              changedDetailVisibility(false);
            }}
            onDelete={() => changedDeleteVisibilityModal(true)}
            onEdit={() => changeMode('edit')}
            onCancel={() => {
              if (isNewNews) {
                changedDetailVisibility(false);
              } else {
                resetForm();
                changeMode('view');
              }
            }}
          />
          <div className={classes.scrollWrapper}>
            <CustomScrollbar>
              <div className={classes.container}>
                <div>
                  {mode === 'edit' && (
                    <Field
                      name="title"
                      component={InputField}
                      label={t('title')}
                      placeholder={t('EnterTitle')}
                      mode={mode}
                      divider={false}
                      required
                    />
                  )}
                  <Field
                    name="text"
                    onChange={(value: string) => setFieldValue('text', value)}
                    mode={mode}
                    component={ContentInput}
                  />
                </div>
                <div className={classes.detail}>
                  <SelectBuildings
                    houses={houses}
                    buildings={values.buildings}
                    //@ts-ignore
                    error={errors.buildings}
                    setBuildings={(e: { id: number }[]) => setFieldValue('buildings', e)}
                    mode={mode}
                  />
                  <Field
                    name="date"
                    component={InputField}
                    type="date"
                    label={t('Label.date')}
                    mode="view"
                  />
                  {currentPageTab === 'announcement' && (
                    <Field
                      name="expires_at"
                      render={({ field, form, ...props }: FieldProps) => (
                        <DatePicker
                          customInput={
                            <InputField label={t('PublishedBefore')} {...props} />
                          }
                          placeholderText={t('EnterDate')}
                          name={field.name}
                          readOnly={mode === 'view'}
                          selected={field.value}
                          onChange={(value: Date) => {
                            setFieldValue('expires_at', value);
                          }}
                          minDate={today}
                          locale={dateFnsLocale}
                          dateFormat="dd.MM.yyyy"
                        />
                      )}
                    />
                  )}
                  <Field
                    name="status"
                    render={({ field, form }: FieldProps) => (
                      <SelectField
                        field={field}
                        form={form}
                        label={t('Label.status')}
                        options={[
                          { id: 1, title: t('Draft') },
                          { id: 2, title: t('ShePublished') },
                        ]}
                        mode={!isNewNews && field?.value?.id === 2 ? 'view' : mode}
                      />
                    )}
                  />
                  <FieldArray
                    name="images"
                    render={({ push, remove }: FieldArrayRenderProps) => (
                      <ImageInput
                        values={values}
                        push={push}
                        remove={remove}
                        mode={mode}
                      />
                    )}
                  />
                  {mode === 'edit' && opened.id ? null : <TargetedContent mode={mode} />}
                </div>
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};
