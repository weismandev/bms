import { FC } from 'react';
import { useStore } from 'effector-react';
import {
  Wrapper,
  CustomScrollbar,
  Pagination,
  Toolbar,
  FoundInfo,
  Greedy,
  AddButton,
  Tabs,
} from '@ui/index';
import {
  $news,
  $countNews,
  changePageNews,
  changePerPageNews,
  $perPageNews,
  $currentPageNews,
  addClicked,
  $currentPageTab,
  pageTabs,
  changePageTab,
  $isDetailOpen,
  $perPageAnnouncement,
  $currentPageAnnouncement,
  changePageAnnouncement,
  changePerPageAnnouncement,
  $isLoading,
} from '../../models';
import { NoData } from '../../molecules/no-data';
import { NewsCard } from '../news-card';
import { useStyles } from './styles';

const NewsList: FC = () => {
  const classes = useStyles();

  const news = useStore($news);
  const countNews = useStore($countNews);
  const currentPageNews = useStore($currentPageNews);
  const perPageNews = useStore($perPageNews);
  const currentPageTab = useStore($currentPageTab);
  const isDetailOpen = useStore($isDetailOpen);
  const perPageAnnouncement = useStore($perPageAnnouncement);
  const currentPageAnnouncement = useStore($currentPageAnnouncement);
  const isLoading = useStore($isLoading);

  const paginationParams =
    currentPageTab === 'news'
      ? {
          currentPage: currentPageNews,
          changePage: changePageNews,
          perPage: perPageNews,
          changePerPage: changePerPageNews,
        }
      : {
          currentPage: currentPageAnnouncement,
          changePage: changePageAnnouncement,
          perPage: perPageAnnouncement,
          changePerPage: changePerPageAnnouncement,
        };

  return (
    <Wrapper className={classes.container}>
      <Toolbar className={classes.toolbar}>
        <FoundInfo count={countNews} />
        <Tabs
          options={pageTabs}
          currentTab={currentPageTab}
          onChange={(_: unknown, tab: string) => changePageTab(tab)}
          style={{ marginLeft: 20, marginTop: -10 }}
        />
        <Greedy />
        <AddButton onClick={addClicked} disabled={isDetailOpen} />
      </Toolbar>
      {!isLoading && news.length === 0 ? (
        <NoData />
      ) : (
        <>
          <CustomScrollbar style={{ height: 'calc(100% - 95px)' }} autoHide>
            <ul className={classes.newsList}>
              {news.map((item) => (
                <NewsCard key={item.id} {...item} />
              ))}
            </ul>
          </CustomScrollbar>
          <Pagination
            currentPage={paginationParams.currentPage}
            changePage={paginationParams.changePage}
            rowCount={countNews}
            perPage={paginationParams.perPage}
            changePerPage={paginationParams.changePerPage}
            hideTitlePerPage={isDetailOpen}
          />
        </>
      )}
    </Wrapper>
  );
};

export { NewsList };
