import { memo, FC } from 'react';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Wrapper, Tabs } from '@ui/index';
import { changeTab, $currentTab, $tabs, $opened } from '../../models';
import { NewsContent } from '../news-content';
import { NewsStatistics } from '../statistics';
import { useStyles } from './styles';

const Detail: FC = memo(() => {
  const classes = useStyles();
  const currentTab = useStore($currentTab);
  const opened = useStore($opened);
  const tabs = useStore($tabs);

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(_: unknown, tab: string) => changeTab(tab)}
        isNew={isNew}
        tabEl={<Tab />}
      />
      {currentTab === 'statistics' && <NewsStatistics />}
      {currentTab === 'card' && <NewsContent />}
    </Wrapper>
  );
});

export { Detail };
