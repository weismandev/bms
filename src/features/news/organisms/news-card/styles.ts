import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  card: {
    backgroundColor: ({ isActive }: { isActive: boolean }) =>
      isActive ? '#edf6ff' : 'white',
  },
  visibilityOffIcon: {
    margin: '10%',
    width: '40%',
    height: '40%',
    color: 'white',
  },
  visibilityOffIconWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7E7EC',
    height: 250,
    width: '100%',
  },
  actionArea: {
    height: '100%',
  },
  cardContent: {
    height: 145,
  },
  cardBottom: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '25px 10px 10px',
  },
});
