import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import i18n from '@shared/config/i18n';
import {
  FilterMainDetailLayout,
  ErrorMessage,
  Loader,
  DeleteConfirmDialog,
} from '@ui/index';
import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isLoading,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  deleteNews,
} from '../models';
import { Detail } from '../organisms/detail';
import { NewsList } from '../organisms/news-list';

const { t } = i18n;

const NewsPage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <DeleteConfirmDialog
        content={t('TheNewsWillBeDeletedFromTheRecipients')}
        header={t('Removal')}
        close={() => changedDeleteVisibilityModal(false)}
        isOpen={isOpenDeleteModal}
        confirm={() => deleteNews()}
      />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={null}
        main={<NewsList />}
        detail={isDetailOpen && <Detail />}
        params={{
          mainWidth: !isDetailOpen ? 'auto' : '150px',
          detailWidth: '1fr',
        }}
      />
    </>
  );
};

const RestrictedNewsPage: FC = () => (
  <HaveSectionAccess>
    <NewsPage />
  </HaveSectionAccess>
);

export { RestrictedNewsPage as NewsPage };
