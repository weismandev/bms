export interface IStatisticResponse {
  statistic: IStatistic;
}

export interface IStatistic {
  count: number;
  coverage: number;
  dates: { date: string; count: number }[] | [];
  read: number;
  unread: number;
}

export interface IStatisticPayload {
  id: number;
  start_date?: string;
  finish_date?: string;
  export?: number;
}

export interface IStatisticLogsPayload {
  id: number;
  page?: number;
  per_page?: number;
}

export interface IStatisticLogs {
  apartment: { id: number; address: string };
  user: { id: number; fullname: string };
  readed_at: null | string;
}

export interface IStatisticLogsResponse {
  meta: {
    total?: number;
  };
  logs: IStatisticLogs[];
}

export interface IFiltredStatisticResponse {
  statistic: {
    dates: IFiltredDates[];
  };
}

export interface IFiltredDates {
  date: string;
  count: number;
}

export interface IDates {
  start_date: string;
  finish_date: string;
}
