export interface INewsFormated {
  id: number;
  text: string;
  title: string;
  image: string | null;
  date: string;
  statusNews: string;
}

export interface INewsData {
  id: number;
  text: string;
  title: string;
  images: IImages[];
  date: string;
  status: number;
  expires_at: null | string;
}

export interface INewsResponse {
  items: INewsData[];
  meta: {
    total: number;
  };
}

export interface INewsPayload {
  page: number;
  per_page: number;
  type: string;
}

export interface INewsByIdPayload {
  id: number;
}

interface IImages {
  id: number;
  url: string;
}

export interface IHousesResponse {
  buildings: IBuildings[];
}

export interface IComplex {
  id: number;
  title: string;
}

export interface IBuildings {
  id: number;
  building: {
    id: number;
  };
  complex: IComplex;
}

interface IStatus {
  id: number;
}

export interface ICreateUpdateNewsPayload {
  id: number | null;
  text: string;
  title: string;
  date: string;
  status: number | IStatus;
  type?: string;
  images: {
    id?: number;
    url?: string;
    file?: File;
  }[];
  buildings: IBuildings[] | number[];
  expires_at?: string;
}

export interface INewsItem {
  id: number;
  text: string;
  title: string;
  date: string;
  status: number;
  images: {
    id: number;
    url: string;
  }[];
  apartments: string | [];
  buildings: number[];
  expires_at: null | string;
  targeting: {
    age_from: number;
    age_to: number;
    genders: string[];
    options: string[];
  };
}

export interface INews {
  item: INewsItem;
}

export interface IDeleteNewsPayload {
  id: number;
}

export interface IDeleteNewsResponse {
  deleted: boolean;
}
