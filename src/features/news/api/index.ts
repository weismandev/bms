import { api } from '@api/api2';
import {
  INews,
  INewsPayload,
  INewsResponse,
  INewsByIdPayload,
  ICreateUpdateNewsPayload,
  IDeleteNewsPayload,
  IDeleteNewsResponse,
  IHousesResponse,
} from '../interfaces/news';
import {
  IStatisticPayload,
  IStatisticLogsPayload,
  IStatisticResponse,
  IStatisticLogsResponse,
} from '../interfaces/statistics';

const getNews = (payload: INewsPayload): INewsResponse =>
  api.v1('get', 'news/list', payload);
const getNewsById = ({ id }: INewsByIdPayload): INews =>
  api.v1('get', 'news/view', { id });
const createNews = (payload: ICreateUpdateNewsPayload): INews =>
  api.v1('post', 'news/create', payload);
const updateNews = (payload: ICreateUpdateNewsPayload): INews =>
  api.v1('post', 'news/update', payload);
const deleteNews = ({ id }: IDeleteNewsPayload): IDeleteNewsResponse =>
  api.v1('post', 'news/delete', { id });
const getHouses = (): IHousesResponse =>
  api.v1('get', 'buildings/get-list-crm', { page: 1, per_page: 1000 });
const getNewsStatistics = (payload: IStatisticPayload): IStatisticResponse =>
  api.v1('get', 'news/statistic', payload);
const exportNewsStatistics = (payload: IStatisticPayload): Blob =>
  api.v1('request', 'news/statistic', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });
const getNewsStatisticsLogs = (payload: IStatisticLogsPayload): IStatisticLogsResponse =>
  api.v1('get', 'news/statistic/logs', payload);

export const newsApi = {
  getNews,
  getNewsById,
  createNews,
  updateNews,
  deleteNews,
  getHouses,
  getNewsStatistics,
  getNewsStatisticsLogs,
  exportNewsStatistics,
};
