import { createEvent, createStore } from 'effector';
import format from 'date-fns/format';

import i18n from '@shared/config/i18n';

import { IFiltredDates, IDates } from '../interfaces/statistics';
import { $filtredStatistic } from './statistics.model';

const { t } = i18n;

const changeOpenModal = createEvent<boolean>();
const saveDates = createEvent<IDates>();

const $isOpenModal = createStore<boolean>(false);
const $dates = createStore<IDates>({ start_date: '', finish_date: '' });

const $tableFiltredStatistic = $filtredStatistic.map((dates) =>
  dates ? formatDates(dates) : []
);

const formatDates = (dates: IFiltredDates[]) =>
  dates.reduce((acc: { date: string; views: number }[], value) => {
    const { date, count } = value;

    if (!date) {
      return [
        ...acc,
        {
          date: '',
          views: count,
        },
      ];
    }

    return [
      ...acc,
      {
        date: format(new Date(date), 'dd.MM.yyyy'),
        views: count,
      },
    ];
  }, []);

const columnsFiltredStatistic = [
  { name: 'date', title: t('Label.date') },
  { name: 'views', title: t('Views') },
];

export {
  changeOpenModal,
  saveDates,
  $isOpenModal,
  $dates,
  $tableFiltredStatistic,
  columnsFiltredStatistic,
};
