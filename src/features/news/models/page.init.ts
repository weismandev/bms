import { forward, merge, guard } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';

import i18n from '@shared/config/i18n';
import { signout } from '@features/common';

import { newsApi } from '../api';
import { INewsFormated, INewsData } from '../interfaces/news';
import {
  fxGetNews,
  $news,
  pageUnmounted,
  pageMounted,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changedErrorDialogVisibility,
  $countNews,
  changePerPageNews,
  $currentPageNews,
  $perPageNews,
  fxGetHouses,
  $houses,
  changePageTab,
  $currentPageTab,
  $currentPageAnnouncement,
  $perPageAnnouncement,
  changePerPageAnnouncement,
} from './page.model';
import { fxGetNewsById, fxCreateNews, fxUpdateNews, fxDeleteNews } from './detail.model';
import {
  fxGetStatistics,
  fxGetFiltredStatistics,
  fxGetStatisticsLogs,
  fxExportStatistics,
} from './statistics.model';

const { t } = i18n;

fxGetNews.use(newsApi.getNews);
fxGetHouses.use(newsApi.getHouses);

const errorOccured = merge([
  fxGetNews.fail,
  fxGetNewsById.fail,
  fxGetStatistics.fail,
  fxGetFiltredStatistics.fail,
  fxGetStatisticsLogs.fail,
  fxExportStatistics.fail,
  fxCreateNews.fail,
  fxUpdateNews.fail,
  fxDeleteNews.fail,
  fxGetHouses.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetNews,
        fxGetStatistics,
        fxGetNewsById,
        fxCreateNews,
        fxUpdateNews,
        fxDeleteNews,
        fxGetHouses,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$news
  .on(fxGetNews.done, (_, { result }) =>
    Array.isArray(result?.items) ? result.items.map((item) => formatNews(item)) : []
  )
  .reset([signout, pageUnmounted, changePageTab]);

$countNews
  .on(fxGetNews.done, (_, { result }) => result?.meta?.total || 0)
  .reset([signout, pageUnmounted, changePageTab]);

forward({
  from: pageMounted,
  to: fxGetHouses,
});

$houses
  .on(fxGetHouses.done, (_, { result }) =>
    Array.isArray(result?.buildings) ? result.buildings : []
  )
  .reset([signout, pageUnmounted]);

guard({
  clock: [
    pageMounted,
    $currentPageNews,
    changePerPageNews,
    changePageTab,
    fxCreateNews.done,
    fxUpdateNews.done,
    fxDeleteNews.done,
  ],
  source: {
    currentPage: $currentPageNews,
    perPage: $perPageNews,
    tab: $currentPageTab,
  },
  filter: ({ tab }) => tab === 'news',
  target: fxGetNews.prepend(
    ({
      currentPage,
      perPage,
      tab,
    }: {
      currentPage: number;
      perPage: number;
      tab: string;
    }) => ({
      page: currentPage,
      per_page: perPage,
      type: tab,
    })
  ),
});

guard({
  clock: [
    pageMounted,
    $currentPageAnnouncement,
    changePerPageAnnouncement,
    changePageTab,
    fxCreateNews.done,
    fxUpdateNews.done,
    fxDeleteNews.done,
  ],
  source: {
    currentPage: $currentPageAnnouncement,
    perPage: $perPageAnnouncement,
    tab: $currentPageTab,
  },
  filter: ({ tab }) => tab === 'announcement',
  target: fxGetNews.prepend(
    ({
      currentPage,
      perPage,
      tab,
    }: {
      currentPage: number;
      perPage: number;
      tab: string;
    }) => ({
      page: currentPage,
      per_page: perPage,
      type: tab,
    })
  ),
});

const formatNews = ({
  id,
  title,
  text,
  images,
  date,
  status,
  expires_at,
}: INewsData): INewsFormated => {
  const maxTitleLength = 39;
  const maxTextLength = 139;
  const formattedText = text.replace(/<\/?[^>]+>/g, '').replace(/&nbsp;/g, '');

  const expiresDate = expires_at && new Date(expires_at);

  if (expiresDate) {
    expiresDate.setHours(23, 59, 59);
  }

  const statusNews =
    expires_at && Number(new Date()) > Number(expiresDate)
      ? t('Archived')
      : status === 2
      ? t('ShePublished')
      : t('Draft');

  return {
    id,
    title: title.length > maxTitleLength ? `${title.slice(0, maxTitleLength)}...` : title,
    text:
      formattedText.length > maxTextLength
        ? `${formattedText.slice(0, maxTextLength)}...`
        : formattedText,
    image: images.length > 0 ? images[0]?.url : null,
    date: format(new Date(date), 'dd.MM.yyyy'),
    statusNews,
  };
};

$currentPageNews
  .on(fxGetNews.done, (page, { params: { type }, result: { items } }) => {
    if (type === 'news' && Array.isArray(items) && items.length === 0) {
      return page === 1 ? page : page - 1;
    }

    return page;
  })
  .reset(changePerPageNews);

$currentPageAnnouncement
  .on(fxGetNews.done, (page, { params: { type }, result: { items } }) => {
    if (type === 'announcement' && Array.isArray(items) && items.length === 0) {
      return page === 1 ? page : page - 1;
    }

    return page;
  })
  .reset(changePerPageAnnouncement);

$currentPageTab.on(changePageTab, (_, tab) => tab);
