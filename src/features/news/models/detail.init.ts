import { sample, forward, attach } from 'effector';
import { format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { signout } from '@features/common';
import { $targetingList, TargetingGroup } from '@features/targeted-content';
import i18n from '@shared/config/i18n';
import { newsApi } from '../api';
import { ICreateUpdateNewsPayload, IBuildings, INews } from '../interfaces/news';
import {
  $currentTab,
  changeTab,
  $opened,
  open,
  $isDetailOpen,
  fxGetNewsById,
  $mode,
  entityApi,
  fxUpdateNews,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  fxDeleteNews,
  deleteNews,
  fxCreateNews,
  addClicked,
  $news,
  detailClosed,
} from './detail.model';
import { pageUnmounted, $currentPageTab, changePageTab } from './page.model';

const { t } = i18n;

fxGetNewsById.use(newsApi.getNewsById);
fxCreateNews.use(newsApi.createNews);
fxUpdateNews.use(newsApi.updateNews);
fxDeleteNews.use(newsApi.deleteNews);

$currentTab.on(changeTab, (_, tab) => tab);

$isOpenDeleteModal.on(changedDeleteVisibilityModal, (_, visibility) => visibility);

$opened.reset([pageUnmounted, signout, addClicked, changePageTab, detailClosed]);
$isDetailOpen.reset([fxDeleteNews.done, fxCreateNews.done, changePageTab]);
$mode.reset([fxCreateNews.done, fxUpdateNews.done, fxDeleteNews.done]);
$currentTab.reset([addClicked, changePageTab]);
$isOpenDeleteModal.reset([pageUnmounted, signout, fxDeleteNews.done]);

sample({
  clock: open,
  fn: (id) => ({ id }),
  target: fxGetNewsById,
});

forward({
  from: deleteNews,
  to: attach({
    source: $opened,
    mapParams: (_, opened) => ({
      id: Number(opened.id),
    }),
    effect: fxDeleteNews,
  }),
});

sample({
  clock: fxUpdateNews.done,
  fn: ({ result }) => ({ id: result?.item?.id }),
  target: fxGetNewsById,
});

$news
  .on(fxGetNewsById.done, (_, { result }) => result)
  .reset([pageUnmounted, signout, addClicked, changePageTab, detailClosed]);

sample({
  source: { list: $targetingList, news: $news },
  filter: ({ news }) => Boolean(news?.item),
  fn: ({ news, list }) => formatGetUpdateResponse(news, list),
  target: $opened,
});

sample({
  clock: entityApi.create,
  source: $currentPageTab,
  fn: (tab, data) => {
    const payload = prepareSubmittedPayload(data);

    const targeting = {
      age_from: data.age[0],
      age_to: data.age[1],
      genders: data.genders.map((gender) => gender.id),
      options: data.targetingOptions.map((option) => option.map((i) => i.id)).flat(),
    };

    return { ...payload, type: tab, targeting };
  },
  target: fxCreateNews,
});

forward({
  from: entityApi.update.map(prepareSubmittedPayload),
  to: attach({
    source: $currentPageTab,
    mapParams: (payload, tab) => ({ ...payload, type: tab }),
    effect: fxUpdateNews,
  }),
});

function formatGetUpdateResponse(result: INews, targetingList: TargetingGroup[]) {
  if (!result?.item) {
    return {};
  }

  const { item } = result;

  const targetOptionsCount = targetingList?.length || 0;

  return {
    id: item.id,
    images: item.images || [],
    text: Boolean(item) && Boolean(item.text) ? item.text : '',
    title: item.title || '',
    date: item.date,
    status:
      item?.status === 2
        ? { id: 2, title: t('ShePublished') }
        : { id: 1, title: t('Draft') },
    buildings: item.buildings || [],
    expires_at: item.expires_at && new Date(item.expires_at),
    apartments:
      item.apartments && typeof item.apartments === 'string'
        ? Array.from(new Set(item.apartments.split(',')))
        : [],
    targetingOptions:
      Array.isArray(item?.targeting?.options) && item.targeting.options.length > 0
        ? [...Array(targetOptionsCount)].map(() => item.targeting.options)
        : [],
    genders:
      Array.isArray(item?.targeting?.genders) && item.targeting.genders.length > 0
        ? item.targeting.genders
        : [],
    age:
      (item?.targeting?.age_from || item?.targeting?.age_from === 0) &&
      item?.targeting?.age_to
        ? [item.targeting.age_from, item.targeting.age_to]
        : [],
  };
}

function prepareSubmittedPayload({
  id,
  title,
  text,
  date,
  images,
  buildings,
  status,
  expires_at,
}: ICreateUpdateNewsPayload): ICreateUpdateNewsPayload {
  const views = images.map((item) => (item.url ? { id: item.id } : { file: item.file }));

  const payload: any = {
    id,
    text,
    title,
    images: views,
    status: typeof status === 'object' ? status.id : status,
    date: format(new Date(date), 'yyyy-MM-dd', { locale: dateFnsLocale }),
    buildings: (buildings as IBuildings[]).map((i) => i.id),
  };

  if (expires_at) {
    payload.expires_at = format(new Date(expires_at), 'yyyy-MM-dd');
  }

  return payload;
}
