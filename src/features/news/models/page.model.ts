import { createEffect, createStore, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';
import {
  INewsFormated,
  INewsResponse,
  INewsPayload,
  IHousesResponse,
  IBuildings,
} from '../interfaces/news';

const { t } = i18n;

const pageTabs = [
  {
    label: t('News'),
    value: 'news',
  },
  {
    label: t('Announcement'),
    value: 'announcement',
  },
];

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const fxGetNews = createEffect<INewsPayload, INewsResponse, Error>();
const fxGetHouses = createEffect<undefined, IHousesResponse, Error>();

const changePageTab = createEvent<string>();
const changedErrorDialogVisibility = createEvent<boolean>();
const changePageNews = createEvent<number>();
const changePerPageNews = createEvent<number>();
const changePageAnnouncement = createEvent<number>();
const changePerPageAnnouncement = createEvent<number>();

const $news = createStore<INewsFormated[]>([]);
const $currentPageTab = createStore<string>('news');
const $isLoading = createStore<boolean>(false);
const $error = createStore<null | Error>(null);
const $isErrorDialogOpen = createStore<boolean>(false);
const $countNews = createStore<number>(0);
const $currentPageNews = restore<number>(changePageNews, 1);
const $perPageNews = restore<number>(changePerPageNews, 10);
const $houses = createStore<IBuildings[]>([]);
const $currentPageAnnouncement = restore<number>(changePageAnnouncement, 1);
const $perPageAnnouncement = restore<number>(changePerPageAnnouncement, 10);

export {
  $error,
  $isLoading,
  PageGate,
  pageMounted,
  pageUnmounted,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetNews,
  $news,
  $countNews,
  changePageNews,
  changePerPageNews,
  $currentPageNews,
  $perPageNews,
  fxGetHouses,
  $houses,
  pageTabs,
  changePageTab,
  $currentPageTab,
  $currentPageAnnouncement,
  $perPageAnnouncement,
  changePageAnnouncement,
  changePerPageAnnouncement,
};
