import './detail.init';
import './page.init';
import './statistics.init';
import './tabel-statistics-modal.init';
import './table-statistcis-logs.init';

export {
  PageGate,
  $news,
  $countNews,
  $isErrorDialogOpen,
  $error,
  $isLoading,
  changedErrorDialogVisibility,
  $houses,
  changePageNews,
  changePerPageNews,
  $perPageNews,
  $currentPageNews,
  $currentPageTab,
  pageTabs,
  changePageTab,
  $perPageAnnouncement,
  $currentPageAnnouncement,
  changePageAnnouncement,
  changePerPageAnnouncement,
} from './page.model';

export {
  $tabs,
  changeTab,
  $currentTab,
  $isDetailOpen,
  open,
  changedDetailVisibility,
  $opened,
  $mode,
  changeMode,
  detailSubmitted,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  deleteNews,
  addClicked,
} from './detail.model';

export {
  $statistics,
  $isLoadingStatisticsLogs,
  getFiltredStatistic,
  $filtredStatistic,
  $isLoadingInModal,
  exportClicked,
  fxGetFiltredStatistics,
} from './statistics.model';

export {
  $tableData,
  columns,
  $columnWidths,
  columnWidthsChanged,
  $rowCount,
  $currentPage,
  pageNumChanged,
  $pageSize,
  pageSizeChanged,
} from './table-statistics-logs.model';

export {
  $isOpenModal,
  changeOpenModal,
  $tableFiltredStatistic,
  columnsFiltredStatistic,
  saveDates,
} from './table-statistics-modal.model';

export { $tableColumnsStatistics, $tableDataStatistics } from './table-statistics.model';
