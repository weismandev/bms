import { createEvent, createStore, createEffect } from 'effector';
import { format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';
import {
  ICreateUpdateNewsPayload,
  INews,
  IDeleteNewsResponse,
  INewsByIdPayload,
  IDeleteNewsPayload,
} from '../interfaces/news';
import { $currentPageTab } from './page.model';

const { t } = i18n;

const $tabs = $currentPageTab.map((tab) => [
  {
    label: tab === 'news' ? t('News') : t('Advertisement'),
    onCreateIsDisabled: false,
    value: 'card',
  },
  {
    label: t('navMenu.statistics.title'),
    onCreateIsDisabled: true,
    value: 'statistics',
  },
]);

const newEntity = {
  id: null,
  images: [],
  text: '',
  title: '',
  date: format(new Date(), 'yyyy-MM-dd', { locale: dateFnsLocale }),
  status: { id: 1, title: t('Draft') },
  apartments: [],
  buildings: [],
  targetingOptions: [],
  genders: [],
  age: [0, 100],
};

const changeTab = createEvent<string>();
const changedDeleteVisibilityModal = createEvent<boolean>();
const deleteNews = createEvent<void>();

const fxGetNewsById = createEffect<INewsByIdPayload, INews, Error>();
const fxCreateNews = createEffect<ICreateUpdateNewsPayload, INews, Error>();
const fxUpdateNews = createEffect<ICreateUpdateNewsPayload, INews, Error>();
const fxDeleteNews = createEffect<IDeleteNewsPayload, IDeleteNewsResponse, Error>();

const $currentTab = createStore<string>('card');
const $isOpenDeleteModal = createStore<boolean>(false);
export const $news = createStore<INews>({});

export {
  $tabs,
  changeTab,
  $currentTab,
  fxGetNewsById,
  fxUpdateNews,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  deleteNews,
  fxDeleteNews,
  fxCreateNews,
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
