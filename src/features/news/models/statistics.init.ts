import { sample, guard, merge } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { newsApi } from '../api';
import { IStatisticPayload, IStatisticLogsPayload } from '../interfaces/statistics';
import { $currentTab, changeTab, $opened, open } from './detail.model';
import { pageUnmounted } from './page.model';
import {
  fxGetStatistics,
  $statistics,
  fxGetStatisticsLogs,
  $statisticsLogs,
  $isLoadingStatisticsLogs,
  getFiltredStatistic,
  fxGetFiltredStatistics,
  $filtredStatistic,
  $isLoadingInModal,
  exportClicked,
  fxExportStatistics,
} from './statistics.model';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $pageSize,
  $currentPage,
} from './table-statistics-logs.model';
import { changeOpenModal, $dates } from './table-statistics-modal.model';

fxGetStatistics.use(newsApi.getNewsStatistics);
fxGetFiltredStatistics.use(newsApi.getNewsStatistics);
fxGetStatisticsLogs.use(newsApi.getNewsStatisticsLogs);
fxExportStatistics.use(newsApi.exportNewsStatistics);

$isLoadingInModal
  .on(
    [fxGetFiltredStatistics.pending, fxExportStatistics.pending],
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$isLoadingStatisticsLogs
  .on(fxGetStatisticsLogs.pending, (_, pending) => pending)
  .reset([pageUnmounted, signout]);

guard({
  source: merge([
    sample($opened, changeTab, (opened, tab) => ({
      id: opened.id,
      tab,
    })),
    sample($currentTab, open, (tab, id) => ({ tab, id })),
  ]),
  filter: ({ id, tab }) => Boolean(id) && tab === 'statistics',
  target: fxGetStatistics.prepend(({ id }: IStatisticPayload) => ({ id })),
});

guard({
  source: merge([
    sample($opened, changeTab, (opened, tab) => ({
      id: opened.id,
      tab,
    })),
    sample($currentTab, open, (tab, id) => ({
      tab,
      id,
    })),
  ]),
  filter: ({ id, tab }) => Boolean(id) && tab === 'statistics',
  target: fxGetStatisticsLogs.prepend(({ id }: IStatisticLogsPayload) => ({
    id,
  })),
});

sample({
  clock: getFiltredStatistic,
  source: $opened,
  fn: ({ id }, { start_date, finish_date }) => ({
    id,
    start_date,
    finish_date,
  }),
  target: fxGetFiltredStatistics,
});

$statistics.on(fxGetStatistics.done, (_, { result }) => {
  if (!result?.statistic) {
    return {
      count: 0,
      coverage: 0,
      dates: [],
      read: 0,
      unread: 0,
    };
  }

  const { statistic } = result;

  return {
    count: statistic?.count || 0,
    coverage: statistic?.coverage || 0,
    dates: statistic?.dates.filter((item) => item.date) || [],
    read: statistic?.read || 0,
    unread: statistic?.unread || 0,
  };
});

$filtredStatistic
  .on(fxGetFiltredStatistics.done, (_, { result }) => result.statistic?.dates)
  .reset(changeOpenModal);

sample({
  clock: exportClicked,
  source: { opened: $opened, dates: $dates },
  fn: ({ opened, dates }) => ({
    id: opened.id,
    ...dates,
    export: 1,
  }),
  target: fxExportStatistics,
});

sample({
  clock: [pageNumberChanged, pageSizeChanged],
  source: [$pageSize, $currentPage, $opened],
  fn: ([pageSize, currentPage, opened]) => ({
    id: opened.id,
    page: currentPage + 1,
    per_page: pageSize,
  }),
  target: fxGetStatisticsLogs,
});

$statisticsLogs.on(fxGetStatisticsLogs.done, (_, { result }) => result);

sample({
  source: fxExportStatistics.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `news_statistics_${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});
