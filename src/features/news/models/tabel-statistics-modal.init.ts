import {
  changeOpenModal,
  $isOpenModal,
  $dates,
  saveDates,
} from './table-statistics-modal.model';

$isOpenModal.on(changeOpenModal, (_, isOpen) => isOpen);

$dates.on(saveDates, (_, dates) => dates).reset(changeOpenModal);
