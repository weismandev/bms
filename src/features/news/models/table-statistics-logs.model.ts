import format from 'date-fns/format';

import i18n from '@shared/config/i18n';
import { createTableBag, IColumns, IWidths } from '@tools/factories/table';

import { IStatisticLogs } from '../interfaces/statistics';
import { $statisticsLogs } from './statistics.model';

const { t } = i18n;

export const $rowCount = $statisticsLogs.map(({ meta }) => meta?.total || 0);

export const $tableData = $statisticsLogs.map(({ logs }) =>
  logs?.length > 0 ? formatLogs(logs) : []
);

const formatLogs = (logs: IStatisticLogs[]) => {
  const formatDate = (date: { date: string } | string) =>
    typeof date === 'object'
      ? format(new Date(date.date), 'dd.MM.yyyy')
      : format(new Date(date), 'dd.MM.yyyy');

  return logs
    .map((log) => ({
      id: log.user?.id,
      fullname: log.user?.fullname || t('Unknown'),
      address: log.apartment?.address || t('Unknown'),
      status: log.readed_at ? t('Viewed') : t('NotViewed'),
      readed_at: log.readed_at ? formatDate(log.readed_at) : '',
    }))
    .sort((a, b) => {
      if (a.readed_at.length > 0 && b.readed_at.length > 0) {
        const date1 = new Date(a.readed_at);
        const date2 = new Date(b.readed_at);

        return Number(date2) - Number(date1);
      }

      return 0;
    });
};

export const columns: IColumns[] = [
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'address', title: t('Label.address') },
  { name: 'status', title: t('Label.status') },
  { name: 'readed_at', title: t('ViewingDate') },
];

const columnsWidth: IWidths[] = [
  { columnName: 'fullname', width: 200 },
  { columnName: 'address', width: 400 },
  { columnName: 'status', width: 150 },
  { columnName: 'readed_at', width: 190 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $columnWidths,
} = createTableBag(columns, {
  widths: columnsWidth,
  order: [],
});
