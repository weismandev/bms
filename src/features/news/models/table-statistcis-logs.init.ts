import { fxGetStatistics } from './statistics.model';
import { $pageSize, $currentPage } from './table-statistics-logs.model';

$pageSize.reset(fxGetStatistics);
$currentPage.reset(fxGetStatistics);
