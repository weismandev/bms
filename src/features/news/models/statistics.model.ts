import { createEvent, createStore, createEffect } from 'effector';
import {
  IStatistic,
  IStatisticResponse,
  IStatisticPayload,
  IStatisticLogsPayload,
  IStatisticLogsResponse,
  IFiltredStatisticResponse,
  IFiltredDates,
} from '../interfaces/statistics';

const getFiltredStatistic = createEvent<{
  start_date: string;
  finish_date: string;
}>();
const exportClicked = createEvent<void>();

const fxGetStatistics = createEffect<IStatisticPayload, IStatisticResponse, Error>();
const fxGetFiltredStatistics = createEffect<
  IStatisticPayload,
  IFiltredStatisticResponse,
  Error
>();
const fxGetStatisticsLogs = createEffect<
  IStatisticLogsPayload,
  IStatisticLogsResponse,
  Error
>();
const fxExportStatistics = createEffect<IStatisticPayload, Blob, Error>();

const $statistics = createStore<null | IStatistic>(null);
const $statisticsLogs = createStore<IStatisticLogsResponse>({
  meta: {},
  logs: [],
});
const $filtredStatistic = createStore<null | IFiltredDates[]>(null);
const $isLoadingStatisticsLogs = createStore<boolean>(false);
const $isLoadingInModal = createStore<boolean>(false);

export {
  fxGetStatistics,
  $statistics,
  fxGetStatisticsLogs,
  $statisticsLogs,
  $isLoadingStatisticsLogs,
  getFiltredStatistic,
  fxGetFiltredStatistics,
  $filtredStatistic,
  $isLoadingInModal,
  exportClicked,
  fxExportStatistics,
};
