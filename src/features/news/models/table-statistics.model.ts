import format from 'date-fns/format';

import i18n from '@shared/config/i18n';

import { IStatistic } from '../interfaces/statistics';
import { $statistics } from './statistics.model';

const { t } = i18n;

const maxCountDay = 7;

export const $tableColumnsStatistics = $statistics.map((statistic) =>
  statistic ? formatColumns(statistic) : []
);

const formatColumns = (statistic: IStatistic) => {
  const dates = statistic.dates.slice(0, maxCountDay);

  const columns = dates.map((_, index) => ({
    name: `date_${index}`,
  }));
  return [{ name: 'column_name' }, ...columns];
};

export const $tableDataStatistics = $statistics.map((statistic) =>
  statistic ? formatData(statistic) : []
);

const formatData = (statistic: IStatistic) => {
  const dates = statistic.dates.slice(0, maxCountDay);

  if (dates.length === 0) {
    return [];
  }

  const dateForTable = dates.reduce(
    (acc, value, index) => ({
      ...acc,
      [`date_${index}`]: format(new Date(value.date), 'dd.MM.yyyy'),
    }),
    {}
  );

  const readForTable = dates.reduce(
    (acc, value, index) => ({
      ...acc,
      [`date_${index}`]: value.count,
    }),
    {}
  );

  return [
    { column_name: t('Label.date'), ...dateForTable },
    { column_name: t('Views'), ...readForTable },
  ];
};
