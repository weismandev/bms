import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  imageWrapper: {
    position: 'relative',
    display: 'inline-block',
    verticalAlign: 'top',
  },
  image: {
    width: '100%',
    padding: '5px',
  },
  deleteButton: {
    right: 0,
    position: 'absolute',
  },
  addButton: {
    width: 'auto',
    height: 34,
    padding: '0 8px',
    minWidth: '34px',
    borderRadius: '17px',
    backgroundColor: '#0394E3',
    color: '#fff',
    boxShadow:
      '0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%)',
    '&:hover': {
      backgroundColor: '#35A9E9',
    },
  },
});
