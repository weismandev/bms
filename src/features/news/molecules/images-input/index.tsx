import { FC } from 'react';
import { Button } from '@mui/material';
import i18n from '@shared/config/i18n';
import { FileControl, DeleteButton } from '@ui/index';
import { useStyles } from './styles';
import { IImage, IImageInput, IFile } from './types';

const { t } = i18n;

const Preview: FC<IImage> = ({ mode, index, remove, url, name, preview }) => {
  const classes = useStyles();
  return (
    <div className={classes.imageWrapper}>
      {mode === 'edit' && (
        <DeleteButton onClick={() => remove(index)} className={classes.deleteButton} />
      )}
      <img src={url || preview} alt={name} className={classes.image} />
    </div>
  );
};

export const ImageInput: FC<IImageInput> = ({ push, remove, values, mode }) => {
  const classes = useStyles();

  const images = Array.isArray(values.images)
    ? values.images.map((props: object, idx: number) => (
        <Preview {...props} mode={mode} remove={remove} index={idx} key={idx} />
      ))
    : [];

  return (
    <>
      {images}
      <FileControl
        name="image"
        encoding="file"
        value=""
        onChange={(value: IFile) => push(value)}
        renderButton={() =>
          mode === 'edit' && (
            <Button disableRipple className={classes.addButton} component="span">
              {t('UploadImage')}
            </Button>
          )
        }
        renderPreview={() => null}
        inputProps={{
          accept: 'image/jpeg, image/png, image/bpm',
        }}
      />
    </>
  );
};
