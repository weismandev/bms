import makeStyles from '@mui/styles/makeStyles';

export const contentInputStyles = makeStyles({
  contentWrapper: {
    height: '100%',
    fontSize: 14,
    marginRight: 40,
  },
  control: {
    width: '100%',
  },
  errorMessage: {
    color: '#d32f2f',
    fontSize: '0.75rem',
  },
});

export const contentStyles = {
  h2: {
    marginTop: 30,
    paddingLeft: 21,
    fontWeight: 'bold' as 'bold',
    fontSize: 24,
    color: '#212529',
  },
  ul: {
    listStyleType: 'disc' as 'disc',
    marginLeft: 30,
  },
  content: {
    paddingLeft: 21,
    color: '#65657B',
    '& > ul': {
      listStyleType: 'disc' as 'disc',
      marginLeft: 40,
    },
  },
};
