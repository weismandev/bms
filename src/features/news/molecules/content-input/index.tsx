//@ts-nocheck
import { FC, useMemo } from 'react';
import { Typography } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import { Editor } from '@ui/index';
import { contentStyles, contentInputStyles } from './styles';
import { IContentInput, IStyledProps } from './types';

const Content: FC<IStyledProps> = ({ classes, text, title }) => (
  <>
    <Typography variant="h2" classes={{ h2: classes.h2 }}>
      {title}
    </Typography>
    <br />
    <div className={classes.content} dangerouslySetInnerHTML={{ __html: text }} />
  </>
);
const StyledContent = withStyles(contentStyles)(Content);

export const ContentInput: FC<IContentInput> = (props) => {
  const { mode, form, ...restProps } = props;
  const classes = contentInputStyles();

  const errorMessage = form.errors.text ?? null;
  const Control = mode === 'view' ? StyledContent : Editor;

  const input = useMemo(() => {
    return (
      <div className={classes.contentWrapper}>
        <Control
          mode={mode}
          text={form.values.text}
          title={form.values.title}
          error={Boolean(errorMessage)}
          name="text"
          inputProps={{
            type: 'text',
          }}
          className={classes.control}
          {...restProps}
        />
        {errorMessage && <span className={classes.errorMessage}>{errorMessage}</span>}
      </div>
    );
  }, [mode, form.values.text, form.errors.text]);

  return input;
};
