import { FC } from 'react';
import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import i18n from '@shared/config/i18n';
import { Dumb } from '@ui/index';
import { $currentPageTab } from '../../models';
import { useStyles } from './styles';

const { t } = i18n;

const NoData: FC = () => {
  const currentPageTab = useStore($currentPageTab);

  const classes = useStyles();

  return (
    <Dumb>
      <Typography classes={{ root: classes.text }} variant="h3">
        {currentPageTab === 'news' ? t('NoNews') : t('NoAnnouncement')}
      </Typography>
    </Dumb>
  );
};

export { NoData };
