import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  text: {
    fontWeight: 900,
    color: '#9EA1AB',
    fontSize: 24,
    lineHeight: '44px',
    fontFamily: 'Montserrat',
    marginBottom: 25,
  },
}));
