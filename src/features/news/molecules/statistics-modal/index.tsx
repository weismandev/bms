import { FC } from 'react';
import { useStore } from 'effector-react';
import { StatisticsModal } from '@ui/index';
import {
  getFiltredStatistic,
  $filtredStatistic,
  $isLoadingInModal,
  $tableFiltredStatistic,
  columnsFiltredStatistic,
  exportClicked,
  $isOpenModal,
  changeOpenModal,
  saveDates,
} from '../../models';

const NewsModalStatistics: FC = () => {
  const filtredStatistic = useStore($filtredStatistic);
  const isLoadingInModal = useStore($isLoadingInModal);
  const tableFiltredStatistic = useStore($tableFiltredStatistic);
  const isOpenModal = useStore($isOpenModal);

  return (
    <StatisticsModal
      isOpen={isOpenModal}
      onClose={() => changeOpenModal(false)}
      getFiltredStatistic={getFiltredStatistic}
      filtredStatistic={filtredStatistic}
      isLoading={isLoadingInModal}
      tableData={tableFiltredStatistic}
      columns={columnsFiltredStatistic}
      exportClicked={exportClicked}
      saveDates={saveDates}
    />
  );
};

export { NewsModalStatistics };
