import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../ui';
import { $data, fxGetList, $isLoading, $error } from '../model';

const RoomsSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl isLoading={isLoading} error={error} options={options} {...props} />
  );
};

const RoomsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RoomsSelect />} onChange={onChange} {...props} />;
};

export { RoomsSelectField, RoomsSelect };
