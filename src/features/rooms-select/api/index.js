import { api } from '../../../api/api2';

const getRooms = (payload = {}, config = {}) =>
  api.no_vers(
    'get',
    'bmsdevice/get-rooms/',
    payload,
    // { limit: 500, offset: 1000, ...payload },
    config
  );

export const roomsApi = {
  getRooms,
};
