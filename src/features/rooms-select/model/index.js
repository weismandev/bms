import { createEffect, createEvent, createStore, restore, sample } from 'effector';
import { signout } from '../../common';
import { roomsApi } from '../api';

const fxGetList = createEffect();

const changeBuilding = createEvent();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

const $building = restore(changeBuilding, '');

$data.on(fxGetList.done, (_, { result }) => result.rooms || []).reset(signout);
$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);

fxGetList.use(roomsApi.getRooms);

sample({
  clock: $building,
  fn: (buildings_id) => ({ buildings_id }),
  target: fxGetList,
});

export { fxGetList, $data, $error, $isLoading, changeBuilding, $building };
