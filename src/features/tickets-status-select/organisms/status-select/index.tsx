import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { StatusData } from '../../interfaces';
import { $data, $isLoading, fxGetList } from '../../models/status-select';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const StatusSelect: FC<object> = (props) => {
  useEffect(() => {
    fxGetList();
  }, []);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

const StatusSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: StatusData) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StatusSelect />} onChange={onChange} {...props} />;
};

export { StatusSelect, StatusSelectField };
