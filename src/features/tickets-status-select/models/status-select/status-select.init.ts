import { signout } from '@features/common';
import { ticketStatusApi } from '../../api';
import { fxGetList, $data, $isLoading } from './status-select.model';

fxGetList.use(ticketStatusApi.getStatusList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.statuses)) {
      return [];
    }

    return result.statuses.map((item) => ({
      id: item.name,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
