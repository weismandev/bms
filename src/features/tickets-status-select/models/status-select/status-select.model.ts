import { createEffect, createStore } from 'effector';
import { StatusResponse, StatusData } from '../../interfaces';

const fxGetList = createEffect<void, StatusResponse, Error>();

const $data = createStore<StatusData[] | []>([]);
const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
