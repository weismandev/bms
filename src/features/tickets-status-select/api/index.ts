import { api } from '@api/api2';

const getStatusList = () => api.v1('get', 'tck/statuses/list');

export const ticketStatusApi = {
  getStatusList,
};
