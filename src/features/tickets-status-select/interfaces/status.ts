export interface Status {
  name: string;
  title: string;
}

export interface StatusData {
  id: string;
  title: string;
}

export interface StatusResponse {
  statuses: Status[];
}
