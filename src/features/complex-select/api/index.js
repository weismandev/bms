import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'complex/list');

export const complexApi = { getList };
