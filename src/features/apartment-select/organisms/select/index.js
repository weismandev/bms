import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, $isLoading, fxGetList } from '../../models';

const ApartmentSelect = ({ building_id, building_ids = [], ...props }) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    if (building_id) {
      fxGetList({ building_id });
    }
  }, [building_id]);

  useEffect(() => {
    if (building_ids.length > 0) {
      fxGetList({ building_ids });
    }
  }, [building_ids]);

  return (
    <SelectControl
      options={options[building_id]}
      isLoading={isLoading}
      filterOption={({ label }, input) =>
        typeof label === 'string' ? label.includes(input) : true
      }
      {...props}
    />
  );
};

const ApartmentSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ApartmentSelect />} onChange={onChange} {...props} />;
};

export { ApartmentSelect, ApartmentSelectField };
