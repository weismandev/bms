import { createEffect, createStore } from 'effector';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

export { fxGetList, $data, $error, $isLoading };
