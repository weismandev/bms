import { signout } from '@features/common';
import { apartmentApi } from '../../api';
import { fxGetList, $data, $error, $isLoading } from './apartment-select.model';

fxGetList.use(apartmentApi.getList);

$data
  .on(fxGetList.done, (state, { result, params }) => {
    if (!result.items) {
      return state;
    }

    return {
      ...state,
      [params.building_id]: result.items.map(({ apartment }) => apartment),
    };
  })
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
