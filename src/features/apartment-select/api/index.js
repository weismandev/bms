import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'apartment/simple-list', { ...payload, per_page: 1000000 });

export const apartmentApi = { getList };
