import { api } from '@api/api2';

const getList = (payload) => api.v1('get', 'vehicle/search', payload);
const create = (payload) => api.v1('post', 'vehicle/create', payload);
const update = (payload) => api.v1('post', 'vehicle/update', payload);
const getById = (id) => api.v1('get', 'vehicle/get', { vehicle_id: id });
const deleteItem = (id) => api.v1('post', 'vehicle/delete', { vehicle_id: id });
const exportVehicles = (payload) =>
  api.v1(
    'request',
    'vehicle/export',
    { ...payload, limit: 1000000 },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );

export { getList, create, update, getById, deleteItem, exportVehicles };
