import { combine } from 'effector';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import i18n from '@shared/config/i18n';
import {
  FilterMainDetailLayout,
  DeleteConfirmDialog,
  Loader,
  ErrorMessage,
} from '@ui/index';
import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isDeleteDialogOpen,
  changeDeleteDialogVisibility,
  confirmDelete,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changeErrorDialogVisibility,
} from '../models/page.model';
import { Table, Filter, Detail } from '../organisms';

const { t } = i18n;

const $stores = combine({
  isFilterOpen: $isFilterOpen,
  isDetailOpen: $isDetailOpen,
  isDeleteDialogOpen: $isDeleteDialogOpen,
  isErrorDialogOpen: $isErrorDialogOpen,
  isLoading: $isLoading,
  error: $error,
});

const VehiclesPage = () => {
  const {
    isFilterOpen,
    isDetailOpen,
    isDeleteDialogOpen,
    isErrorDialogOpen,
    isLoading,
    error,
  } = useStore($stores);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changeErrorDialogVisibility(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => changeDeleteDialogVisibility(false)}
        confirm={confirmDelete}
        header={t('RemovingAVehicle')}
        content={t('AreYouSureYouWantToDeleteTheVehicle')}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedVehiclesPage = (props) => (
  <HaveSectionAccess>
    <VehiclesPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedVehiclesPage as VehiclesPage };
