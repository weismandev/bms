import { useMemo, memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, SortingState } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  TableColumnVisibility,
  PagingPanel,
  SortingLabel,
} from '@ui/index';
import { open, $opened } from '../../models/detail.model';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  columns,
  $rowCount,
  excludedColumnsFromSort,
  $sorting,
  sortChanged,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const onRowClick = () => open(props.row.id);
  const opened = useStore($opened);

  const isSelected = String(opened.id) === String(props.row.id);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [props.children, isSelected]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const $stores = combine({
  data: $tableData,
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnWidths: $columnWidths,
  columnOrder: $columnOrder,
  hiddenColumnNames: $hiddenColumnNames,
  count: $rowCount,
  sorting: $sorting,
});

const Table = memo(() => {
  const {
    data,
    currentPage,
    pageSize,
    columnWidths,
    columnOrder,
    hiddenColumnNames,
    count,
    sorting,
  } = useStore($stores);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <DragDropProvider />

        <CustomPaging totalCount={count} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <PagingPanel {...{ columns, hiddenColumnNames }} />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
