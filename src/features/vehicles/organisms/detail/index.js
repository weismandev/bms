import { memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { EnterprisesEmployeeSelectField } from '@features/enterprises-employee-select';
import { AsyncPeopleSelect } from '@features/people-async-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  CustomScrollbar,
} from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
} from '../../models/detail.model';
import { changeDeleteDialogVisibility } from '../../models/page.model';
import { $currentCompany } from '../../models/table.model';

const { t } = i18n;

const $stores = combine({
  mode: $mode,
  opened: $opened,
});

const PeopleField = (props) => {
  const currentCompany = useStore($currentCompany);

  if (currentCompany) {
    return <SelectField component={<EnterprisesEmployeeSelectField />} {...props} />;
  }

  return (
    <SelectField
      component={<AsyncPeopleSelect enterprise_id={currentCompany?.id} />}
      {...props}
    />
  );
};

const validationSchema = Yup.object().shape({
  userdata: Yup.mixed().required(t('thisIsRequiredField')),
  brand: Yup.string().required(t('thisIsRequiredField')),
  id_number: Yup.string().required(t('thisIsRequiredField')),
});

const Detail = memo(() => {
  const { mode, opened } = useStore($stores);

  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        enableReinitialize
        validationSchema={validationSchema}
        render={({ resetForm }) => (
          <Form style={{ height: '100%', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onDelete={() => changeDeleteDialogVisibility(true)}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm();
                }
              }}
            />
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    label={t('Owner')}
                    placeholder={t('StartTypingTheOwnerName')}
                    component={PeopleField}
                    name="userdata"
                    mode={mode}
                    required
                  />
                  <Field
                    label={t('TransportBrand')}
                    component={InputField}
                    placeholder={t('EnterTransportBrand')}
                    name="brand"
                    mode={mode}
                    required
                  />
                  <Field
                    name="id_number"
                    label={t('GovNumber')}
                    placeholder="A000AA000"
                    helpText={t('FormatGovNumber')}
                    mode={mode}
                    component={InputField}
                    required
                  />
                  <Field
                    label={t('Note')}
                    component={InputField}
                    placeholder={t('EnterNote')}
                    name="comment"
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
});

export { Detail };
