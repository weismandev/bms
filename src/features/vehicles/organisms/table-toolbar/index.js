import { combine } from 'effector';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { $isCompany } from '@features/common';
import { EnterprisesCompaniesSelect } from '@features/enterprises-companies-select';
import i18n from '@shared/config/i18n';
import {
  Toolbar,
  FilterButton,
  SearchInput,
  Greedy,
  AddButton,
  FoundInfo,
} from '@ui/index';
import { $mode, addClicked } from '../../models/detail.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import {
  searchChanged,
  $search,
  $rowCount,
  $currentCompany,
  changeCompany,
} from '../../models/table.model';
import { ExportButton } from '../export-button';

const useStyles = makeStyles({
  margin: {
    margin: '0 10px',
  },
});

const { t } = i18n;

const $stores = combine({
  mode: $mode,
  searchValue: $search,
  count: $rowCount,
  isFilterOpen: $isFilterOpen,
});

export function TableToolbar() {
  const classes = useStyles();
  const { mode, searchValue, count, isFilterOpen } = useStore($stores);
  const currentCompany = useStore($currentCompany);
  const isCompany = useStore($isCompany);

  return (
    <>
      <Toolbar>
        {!isCompany ? (
          <FilterButton
            onClick={() => changedFilterVisibility(true)}
            disabled={isFilterOpen}
            className={classes.margin}
          />
        ) : null}
        <FoundInfo count={count} style={{ margin: '0 10px' }} />
        {isCompany ? (
          <div style={{ marginLeft: 20, width: 300 }}>
            <EnterprisesCompaniesSelect
              onChange={changeCompany}
              value={currentCompany}
              label={null}
              divider={false}
              placeholder={t('ChooseCompany')}
              firstAsDefault
            />
          </div>
        ) : null}
        <SearchInput
          className={classes.margin}
          value={searchValue}
          onChange={(e) => searchChanged(e.target.value)}
          handleClear={() => searchChanged('')}
        />
        <Greedy />
        <ExportButton />
        <AddButton
          className={classes.margin}
          onClick={addClicked}
          disabled={mode === 'edit'}
        />
      </Toolbar>
    </>
  );
}
