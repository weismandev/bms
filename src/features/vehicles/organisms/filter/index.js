import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ApartmentSelectField } from '@features/apartment-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter } from '@ui/index';
import {
  $filters,
  filtersSubmitted,
  changedFilterVisibility,
  defaultFilters,
} from '../../models/filter.model';

const { t } = i18n;

const Filter = memo(() => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%' }}>
      <FilterToolbar
        style={{ padding: 24 }}
        closeFilter={() => changedFilterVisibility(false)}
      />
      <Formik
        initialValues={filters}
        onSubmit={filtersSubmitted}
        onReset={() => filtersSubmitted(defaultFilters)}
        enableReinitialize
        render={({ values, setFieldValue }) => {
          return (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <>
                <Field
                  name="building"
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  onChange={(value) => {
                    setFieldValue('apartment', null);
                    setFieldValue('building', value);
                  }}
                />
                <Field
                  name="apartment"
                  component={ApartmentSelectField}
                  label={t('Label.flat')}
                  building_id={values.building?.id}
                  placeholder={t('selectThatApartment')}
                />
              </>
              <FilterFooter isResettable={true} />
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});

export { Filter };
