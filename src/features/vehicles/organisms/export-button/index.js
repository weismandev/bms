import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import { VerticalAlignBottom, PictureAsPdfOutlined } from '@mui/icons-material';
import Xlsx from '@img/xlsx.svg';
import i18n from '@shared/config/i18n';
import { Popover, Menu, ActionIconButton } from '@ui/index';
import { exportVehicles, $isLoading } from '../../models/page.model';
import { $rowCount } from '../../models/table.model';

const { t } = i18n;

const $stores = combine({
  count: $rowCount,
  isLoading: $isLoading,
});

const moreItems = [
  {
    title: t('ExportToXLS'),
    icon: <Xlsx />,
    onClick: () => exportVehicles('excel'),
  },
  {
    title: t('ExportToPDF'),
    icon: <PictureAsPdfOutlined />,
    onClick: () => exportVehicles('pdf'),
  },
];

const ExportButton = () => {
  const { count, isLoading } = useStore($stores);

  return (
    <Tooltip title={t('export')}>
      <div>
        <Popover
          trigger={
            <ActionIconButton disabled={isLoading || count === 0}>
              <VerticalAlignBottom />
            </ActionIconButton>
          }
          disabled={isLoading || count === 0}
        >
          <Menu items={moreItems} />
        </Popover>
      </div>
    </Tooltip>
  );
};

export { ExportButton };
