import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  building: '',
  apartment: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
