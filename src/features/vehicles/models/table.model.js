import { createEvent, restore } from 'effector';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { formatPhone } from '@tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'id', title: '№' },
  { name: 'owner', title: t('Owner') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'address', title: t('Label.address') },
  { name: 'comment', title: t('Note') },
  { name: 'brand', title: t('TransportBrand') },
  { name: 'id_number', title: t('TransportNumber') },
];

const widths = [
  { columnName: 'id', width: 100 },
  { columnName: 'owner', width: 300 },
  { columnName: 'phone', width: 150 },
  { columnName: 'address', width: 300 },
  { columnName: 'comment', width: 250 },
  { columnName: 'brand', width: 150 },
  { columnName: 'id_number', width: 150 },
];

export const excludedColumnsFromSort = ['comment', 'brand', 'id_number'];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  sortChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $search,
  $tableParams,
  $sorting,
} = createTableBag(columns, { widths });

export const $tableData = $raw.map(({ data }) => formatTableData(data));
export const $rowCount = $raw.map(({ meta }) => (meta && meta.total) || 0);

export const changeCompany = createEvent();
export const $currentCompany = restore(changeCompany, '');

$currentCompany.reset(signout);

function formatTableData(data) {
  return Array.isArray(data)
    ? data.map(({ id, brand, id_number, comment, address, userdata }) => ({
        id,
        owner: userdata ? userdata.full_name : t('isNotDefined'),
        phone:
          userdata && userdata.phone ? formatPhone(userdata.phone) : t('isNotDefined'),
        address: (address && address.full_address) || t('isNotDefined'),
        comment: comment || t('thereIsNo'),
        brand: brand || t('isNotDefined'),
        id_number: id_number || t('isNotDefined'),
      }))
    : [];
}
