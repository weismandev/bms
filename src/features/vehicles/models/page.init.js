import { guard, sample } from 'effector';
import { pending } from 'patronum/pending';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout, $isCompany } from '@features/common';
import * as api from '../api';
import { $filters } from './filter.model';
import {
  PageGate,
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  $raw,
  $isLoading,
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  fxExportVehicles,
  exportVehicles,
} from './page.model';
import {
  $tableParams,
  $currentCompany,
  $columnOrder,
  $hiddenColumnNames,
} from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxGetList.use(api.getList);
fxCreate.use(api.create);
fxUpdate.use(api.update);
fxDelete.use(api.deleteItem);
fxExportVehicles.use(api.exportVehicles);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$raw
  .on(fxGetList.doneData, (_, { meta, vehicles }) => {
    const vehiclesArray = Array.isArray(vehicles) ? vehicles : [];

    return {
      data: vehiclesArray,
      meta: { total: (meta && meta.total) || vehiclesArray.length },
    };
  })
  .on(fxCreate.doneData, (state, { vehicle }) => insert(state, vehicle))
  .on(fxUpdate.doneData, (state, { vehicle }) => replace(state, vehicle))
  .on(fxDelete.done, (state, { params }) => deleteItem(state, params))
  .reset([PageGate.close, signout]);

$isLoading
  .on(
    pending({
      effects: [fxGetList, fxCreate, fxUpdate, fxDelete, fxExportVehicles],
    }),
    (_, isLoading) => isLoading
  )
  .reset([PageGate.close, signout]);

$error
  .on(
    [
      fxGetList.failData,
      fxCreate.failData,
      fxUpdate.failData,
      fxDelete.failData,
      fxExportVehicles.failData,
    ],
    (_, error) => error
  )
  .reset([guard({ source: $isLoading, filter: Boolean }), PageGate.close, signout]);

$isDeleteDialogOpen.on(fxDelete.done, () => false).reset([PageGate.close, signout]);

$isErrorDialogOpen
  .on(guard({ source: $error, filter: Boolean }), () => true)
  .reset([PageGate.close, signout]);

sample({
  clock: [PageGate.open, $tableParams, $currentCompany, $settingsInitialized],
  source: {
    table: $tableParams,
    currentCompany: $currentCompany,
    isCompany: $isCompany,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ currentCompany, isCompany, settingsInitialized }) =>
    settingsInitialized && isCompany && Boolean(currentCompany?.id),
  fn: ({ table, currentCompany }) => {
    const { search, ...restTableParams } = table;

    const payload = { ...restTableParams };

    if (search) {
      payload.term = search;
    }

    payload.enterprise_id = currentCompany.id;

    return payload;
  },
  target: fxGetList,
});

sample({
  clock: [PageGate.open, $tableParams, $filters, $settingsInitialized],
  source: {
    table: $tableParams,
    filters: $filters,
    isCompany: $isCompany,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized, isCompany }) => settingsInitialized && !isCompany,
  fn: ({ table, filters }) => formatData(table, filters),
  target: fxGetList,
});

const formatData = (table, filters) => {
  const { search, ...restTableParams } = table;
  const { building, apartment } = filters;

  const payload = { ...restTableParams };

  if (search) {
    payload.term = search;
  }

  if (building) {
    payload.building_id = building.id;
  }

  if (apartment) {
    payload.apartment_id = apartment.id;
  }

  return payload;
};

function insert(state, res) {
  const { data, meta } = state;

  return {
    data: [].concat(res, data),
    meta: { total: meta.total + 1 },
  };
}

function replace(state, res) {
  const { data, meta } = state;

  const idx = data.findIndex((i) => String(i.id) === String(res.id));

  return idx > -1
    ? {
        meta: { ...meta },
        data: data
          .slice(0, idx)
          .concat(res)
          .concat(data.slice(idx + 1)),
      }
    : state;
}

function deleteItem(state, removedId) {
  const { data, meta } = state;

  return {
    data: data.filter((i) => String(i.id) !== String(removedId)),
    meta: { ...meta, total: meta.total - 1 },
  };
}

sample({
  clock: exportVehicles,
  source: {
    table: $tableParams,
    filters: $filters,
    columnOrder: $columnOrder,
    hiddenColumnNames: $hiddenColumnNames,
  },
  fn: ({ table, filters, columnOrder, hiddenColumnNames }, exportType) => {
    const payload = formatData(table, filters);

    const columns = columnOrder.filter((item) => !hiddenColumnNames.includes(item));

    payload.file_format = exportType;
    payload.columns = columns;

    return payload;
  },
  target: fxExportVehicles,
});

fxExportVehicles.done.watch(({ params: { file_format }, result }) => {
  if (file_format === 'excel') {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
    FileSaver.saveAs(URL.createObjectURL(blob), `vehicles-${dateTimeNow}.xlsx`);
  }

  if (file_format === 'pdf') {
    const blob = new Blob([result], {
      type: 'application/pdf',
    });
    const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
    FileSaver.saveAs(URL.createObjectURL(blob), `vehicles-${dateTimeNow}.pdf`);
  }
});
