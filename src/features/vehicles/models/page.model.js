import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const changeDeleteDialogVisibility = createEvent();
const changeErrorDialogVisibility = createEvent();
const confirmDelete = createEvent();
const exportVehicles = createEvent();

const fxGetList = createEffect();
const fxCreate = createEffect();
const fxUpdate = createEffect();
const fxDelete = createEffect();
const fxExportVehicles = createEffect();

const $raw = createStore({ data: [], meta: { total: 0 } });
const $normalized = $raw.map(({ data }) =>
  Array.isArray(data) ? data.reduce((acc, i) => ({ ...acc, [i.id]: i }), {}) : {}
);

const $isLoading = createStore(false);
const $error = createStore(null);
const $isDeleteDialogOpen = restore(changeDeleteDialogVisibility, false);
const $isErrorDialogOpen = restore(changeErrorDialogVisibility, false);

export {
  PageGate,
  changeDeleteDialogVisibility,
  changeErrorDialogVisibility,
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  $raw,
  $normalized,
  $isLoading,
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  confirmDelete,
  exportVehicles,
  fxExportVehicles,
};
