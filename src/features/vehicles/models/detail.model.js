import { createDetailBag } from '@tools/factories';

const newEntity = {
  userdata: '',
  id_number: '',
  brand: '',
  comment: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
