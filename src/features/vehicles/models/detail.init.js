import { sample, forward } from 'effector';
import { $opened, open, entityApi, $mode, $isDetailOpen } from './detail.model';
import { confirmDelete, $normalized, fxCreate, fxUpdate, fxDelete } from './page.model';
import { $currentCompany } from './table.model';

$opened.on(
  [
    sample($normalized, open, (data, id) => data[id]),
    fxCreate.doneData.map(({ vehicle }) => vehicle),
    fxUpdate.doneData.map(({ vehicle }) => vehicle),
  ],
  (_, opened) => formatOpened(opened)
);

$mode.on([fxCreate.done, fxUpdate.done], () => 'view');

$isDetailOpen.on(fxDelete.done, () => false);

sample({
  source: $currentCompany,
  clock: entityApi.create,
  fn: formatPayload,
  target: fxCreate,
});

sample({
  source: $currentCompany,
  clock: entityApi.update,
  fn: formatPayload,
  target: fxUpdate,
});

forward({
  from: sample($opened, confirmDelete),
  to: fxDelete.prepend(({ id }) => id),
});

function formatOpened(opened) {
  const { id_number, brand, userdata, id, comment } = opened;

  return {
    id,
    id_number: id_number || '',
    brand: brand || '',
    comment: comment || '',
    userdata: userdata ? { id: userdata.id, title: userdata.full_name } : '',
  };
}

function formatPayload(currentCompany, data) {
  const { id, id_number, brand, comment, userdata } = data;

  const payload = { id_number, brand, comment };

  if (id) {
    payload.vehicle_id = id;
  }

  if (userdata) {
    payload.userdata_id = userdata.id;
  }

  if (currentCompany && currentCompany.id) {
    payload.enterprise_id = currentCompany.id;
  }

  return payload;
}
