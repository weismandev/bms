import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { companyApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.companies) {
      return [];
    }

    return result.companies;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(companyApi.getList);

export { fxGetList, $data, $error, $isLoading };
