import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const MarketCompanySelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const MarketCompanySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<MarketCompanySelect />} onChange={onChange} {...props} />
  );
};

export { MarketCompanySelect, MarketCompanySelectField };
