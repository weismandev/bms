import { api } from '../../../api/api2';

const getList = () => api.v2('/client/admin/marketplace/companies/feature/index');

export const companyApi = { getList };
