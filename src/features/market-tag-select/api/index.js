import { api } from '../../../api/api2';

const getList = () => api.v2('/client/admin/marketplace/tags/list');

export const tagApi = { getList };
