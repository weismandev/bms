import { api } from '../../../api/api2';

const getList = ({ params, data }) =>
  api.v2('/client/admin/marketplace/companies/list', data, params);

const getById = (id) => {
  return api
    .v2('/client/admin/marketplace/companies/details', { companies: [id] })
    .then((result) => {
      return { company: result.companies[0] };
    });
};

const create = (payload) => api.v2('/client/admin/marketplace/companies/create', payload);
const update = (payload) => api.v2('/client/admin/marketplace/companies/update', payload);
const deleteItem = (id) => api.v2('/client/admin/marketplace/companies/delete', { id });
const checkPaidTicketsEnabled = () => api.v1('get', 'buildings/check-paid-tickets-enabled');
const getTypesList = () => api.v4('get', 'ticket/types/list', { archived: 0 });

export const marketCompaniesApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteItem,
  checkPaidTicketsEnabled,
  getTypesList,
};
