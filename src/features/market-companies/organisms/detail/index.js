import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Button, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  Divider,
  BaseInputLabel,
  FileControl,
  ActionFileWrapper,
  CustomScrollbar,
  SwitchField,
  BaseInputHelperText,
} from '../../../../ui';
import { CategoriesSelectField } from '../../../categories-select';
import { ComplexSelectField } from '../../../complex-select';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  deleteClicked,
} from '../../models';
import { states, types } from '../../models/detail.model';
import { $paidTicketsTypes, $paidTicketsInfo } from '../../models/page.model';

const Detail = memo(() => {
  const { t } = useTranslation();

  const [mode, opened, paidTicketTypes, paidTicketsInfo] = useUnit([
    $mode,
    $opened,
    $paidTicketsTypes,
    $paidTicketsInfo,
  ]);

  const isNew = !opened.id;
  const isPaidTicketsEnabled = !!paidTicketsInfo?.enabled_for_bms;
  const isPaidTicketsTypesExist = !!paidTicketTypes?.length;
  const orderToTicketsMode =
    !isPaidTicketsEnabled || !isPaidTicketsTypesExist || mode === 'view';
  const isTicketsTypeFieldEnabled = isPaidTicketsEnabled && isPaidTicketsTypesExist;

  const validationSchema = Yup.object().shape({
    title: Yup.string().required(t('thisIsRequiredField')),
    state: Yup.mixed().required(t('thisIsRequiredField')),
    paid_tickets_type_id: Yup.object().when('is_paid_tickets_enabled', {
      is: true,
      then: (schema) =>
        schema
          .shape({
            id: Yup.number(),
          })
          .required(t('thisIsRequiredField'))
          .nullable(),
      otherwise: (schema) => schema.nullable(),
    }),
  });

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ resetForm, values, errors }) => {
          const hasError = Object.values(errors).length > 0;
          const isTicketTypeRequired = !!values?.is_paid_tickets_enabled;

          return (
            <Form style={{ height: 'inherit' }}>
              <DetailToolbar
                style={{ padding: 24 }}
                mode={mode}
                hasError={hasError}
                onEdit={() => changeMode('edit')}
                hidden={{ edit: !opened.edit_enabled }}
                onDelete={() => deleteClicked()}
                onClose={() => changedDetailVisibility(false)}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    changeMode('view');
                    resetForm();
                  }
                }}
              />
              <div
                style={{
                  height: 'calc(100% - 82px)',
                  padding: '0 6px 24px 24px',
                }}
              >
                <CustomScrollbar>
                  <div style={{ paddingRight: 18 }}>
                    <Field
                      name="title"
                      label={t('Name')}
                      placeholder={t('EnterTheTitle')}
                      helpText={t('Max255Symbols')}
                      mode={mode}
                      component={InputField}
                      required
                    />
                    <Field
                      name="state"
                      label={t('Label.status')}
                      placeholder={t('selectStatuse')}
                      component={SelectField}
                      options={states}
                      mode={mode}
                      required
                    />
                    <Field
                      name="tagline"
                      label={t('Signature')}
                      helpText={t('Max255Symbols')}
                      placeholder={t('EnterSignature')}
                      mode={mode}
                      component={InputField}
                    />
                    <FieldArray
                      name="highlights"
                      render={({ push, remove }) => {
                        const highlightsList = values.highlights.map(
                          (item, idx, array) => (
                            <div
                              key={idx}
                              style={{ display: 'flex', alignItems: 'center' }}
                            >
                              <div style={{ width: '100%' }}>
                                <Field
                                  name={`highlights[${idx}]`}
                                  label={`${t('Feature')} ${idx + 1}`}
                                  component={InputField}
                                  placeholder={t('EnterFeature')}
                                  mode={mode}
                                />
                              </div>
                              {mode === 'edit' && (
                                <div>
                                  <IconButton onClick={() => remove(idx)} size="large">
                                    <Close />
                                  </IconButton>
                                </div>
                              )}
                            </div>
                          )
                        );

                        return (
                          <>
                            {highlightsList}
                            {mode === 'edit' && (
                              <>
                                <Button color="primary" onClick={() => push('')}>
                                  {t('AddFeature')}
                                </Button>
                                <Divider />
                              </>
                            )}
                          </>
                        );
                      }}
                    />
                    <Field
                      name="summary"
                      label={t('ShortDescription')}
                      helpText={t('WithoutRestrictions')}
                      placeholder={t('EnterDescription')}
                      multiline
                      rowsMax={5}
                      mode={mode}
                      component={InputField}
                    />
                    <Field
                      name="description"
                      label={t('FullDescription')}
                      helpText={t('WithoutRestrictionsFullDescription')}
                      placeholder={t('EnterDescription')}
                      multiline
                      rowsMax={20}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="delivery"
                      label={t('InformationAboutDelivery')}
                      helpText={t('WithoutRestrictionsFullDescription')}
                      placeholder={t('FillField')}
                      multiline
                      rowsMax={5}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="categories"
                      component={CategoriesSelectField}
                      label={t('Categories')}
                      placeholder={t('SelectCategories')}
                      isMulti
                      mode={mode}
                    />
                    <Field
                      name="is_paid_tickets_enabled"
                      component={SwitchField}
                      label={t('TranferOrdersToTickets')}
                      labelPlacement="end"
                      disabled={orderToTicketsMode}
                      divider={false}
                    />
                    <div style={{ marginLeft: 52 }}>
                      {isTicketsTypeFieldEnabled
                        ? isTicketTypeRequired && (
                          <Field
                            name="paid_tickets_type_id"
                            component={SelectField}
                            options={paidTicketTypes}
                            label={null}
                            placeholder={t('SelectRequestType')}
                            mode={mode}
                            divider={false}
                            required={isTicketTypeRequired}
                            menuPlacement="top"
                          />
                        )
                        : mode === 'edit' && (
                          <BaseInputHelperText>
                            <a target="__blanc" href="/tickets-types">
                              {t('create')}
                            </a>
                            {` ${t('orderToTicketWithTipeFieldHelpText')}`}
                          </BaseInputHelperText>
                        )}
                    </div>
                    <Divider />
                    <Field
                      name="whitelist"
                      render={({ field }) => (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('WhiteLists')}
                          </BaseInputLabel>
                          <div style={{ display: 'flex' }}>
                            <div style={{ width: '10%' }} />
                            <div style={{ width: '90%' }}>
                              <Field
                                name={`${field.name}.complex`}
                                label={t('ResidentialComplexes')}
                                placeholder={t('ChooseComplex')}
                                component={ComplexSelectField}
                                isMulti
                                mode={mode}
                                divider={false}
                              />
                            </div>
                          </div>
                          <Divider />
                        </>
                      )}
                    />
                    <FieldArray
                      name="contacts"
                      render={({ push, remove }) => {
                        const mapIdToData = {
                          email: {
                            label: 'Email',
                            helpText: t('ExampleEmail'),
                          },
                          phone: {
                            label: t('Label.phone'),
                            helpText: t('ExamplePhone'),
                          },
                          website: {
                            label: t('Website'),
                            helpText: t('ExampleSite'),
                          },
                          address: {
                            label: t('Label.address'),
                            helpText: t('AnyString'),
                          },
                        };

                        const contactsList = values.contacts.map((i, idx) => {
                          const contactType = values.contacts[idx].type;

                          const type = contactType && mapIdToData[contactType.id];

                          const label = type ? type.label : t('Value');
                          const helpText = type ? type.helpText : t('AnyString');

                          return (
                            <div key={idx}>
                              <BaseInputLabel style={{ marginBottom: 10 }}>
                                {`${t('Contact')} ${idx + 1}`}
                              </BaseInputLabel>
                              <div style={{ display: 'flex' }}>
                                <div style={{ width: '10%' }} />
                                <div style={{ width: '90%' }}>
                                  <Field
                                    name={`contacts[${idx}].type`}
                                    label={t('type')}
                                    component={SelectField}
                                    options={types}
                                    mode={mode}
                                  />
                                  <Field
                                    name={`contacts[${idx}].value`}
                                    label={label}
                                    helpText={helpText}
                                    component={InputField}
                                    mode={mode}
                                  />
                                  <Field
                                    name={`contacts[${idx}].details`}
                                    label={t('Label.comment')}
                                    component={InputField}
                                    mode={mode}
                                    divider={false}
                                  />
                                </div>
                              </div>
                              {mode === 'edit' && (
                                <div style={{ textAlign: 'right', marginTop: 10 }}>
                                  <Button
                                    onClick={() => remove(idx)}
                                    style={{ color: 'red' }}
                                  >
                                    {t('RemoveContact')}
                                  </Button>
                                </div>
                              )}
                              <Divider />
                            </div>
                          );
                        });

                        return (
                          <>
                            {contactsList}
                            {mode === 'edit' && (
                              <>
                                <Button
                                  color="primary"
                                  onClick={() =>
                                    push({
                                      type: { id: 'email', title: 'Email' },
                                      value: '',
                                      details: '',
                                    })}
                                >
                                  {t('AddContacts')}
                                </Button>
                                <Divider />
                              </>
                            )}
                          </>
                        );
                      }}
                    />
                    <Field
                      name="logo"
                      render={({ field, form }) => (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('Logo')}
                          </BaseInputLabel>
                          <FileControl
                            inputProps={{ accept: 'image/*' }}
                            name={field.name}
                            value={field.value}
                            loadable
                            renderButton={() => (
                              <Button component="span" color="primary">
                                {field.value ? t('ReplaceLogo') : t('UploadLogo')}
                              </Button>
                            )}
                            onChange={(value) => form.setFieldValue('logo', value)}
                            mode={mode}
                          />
                          <Divider />
                        </>
                      )}
                    />
                    <Field
                      name="background"
                      render={({ field, form }) => (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('BackgroundImage')}
                          </BaseInputLabel>
                          <FileControl
                            inputProps={{ accept: 'image/*' }}
                            name={field.name}
                            value={field.value}
                            loadable
                            renderButton={() => (
                              <Button component="span" color="primary">
                                {field.value ? t('ReplaceImage') : t('UploadImage')}
                              </Button>
                            )}
                            onChange={(value) => form.setFieldValue('background', value)}
                            mode={mode}
                          />
                          <Divider />
                        </>
                      )}
                    />
                    <FieldArray
                      name="images"
                      render={({ push, remove }) => {
                        const images = values.images.map((i, idx) => {
                          const { meta, ...rest } = i;

                          return (
                            <ActionFileWrapper
                              key={idx}
                              deleteFn={mode === 'edit' ? () => remove(idx) : null}
                              url={rest.preview}
                              {...rest}
                              {...meta}
                            />
                          );
                        });

                        return (
                          <>
                            <BaseInputLabel style={{ marginBottom: 10 }}>
                              {t('CompanyImages')}
                            </BaseInputLabel>
                            <div
                              style={{
                                display: 'flex',
                                flexWrap: 'wrap',
                                justifyContent: 'center',
                              }}
                            >
                              {images}
                              <FileControl
                                inputProps={{ accept: 'image/*' }}
                                name="image"
                                value=""
                                loadable
                                renderButton={() => (
                                  <Button
                                    style={{ width: 150, height: 150 }}
                                    component="span"
                                    color="primary"
                                  >
                                    {t('Downloads')}
                                  </Button>
                                )}
                                renderPreview={() => null}
                                onChange={(value) => value && push(value)}
                                mode={mode}
                              />
                            </div>
                            <Divider />
                          </>
                        );
                      }}
                    />

                    <Field
                      name="accent_color"
                      render={({ field }) => (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('Color')}
                          </BaseInputLabel>
                          <input
                            style={{ width: '100%' }}
                            disabled={mode === 'view'}
                            name={field.name}
                            onChange={field.onChange}
                            value={field.value}
                            type="color"
                          />
                        </>
                      )}
                    />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
});

export { Detail };
