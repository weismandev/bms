import { useStore } from 'effector-react';
import {
  Toolbar,
  FoundInfo,
  AddButton,
  Greedy,
  SearchInput,
  FilterButton,
} from '../../../../ui';
import {
  $rowCount,
  addClicked,
  $mode,
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
} from '../../models';

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const mode = useStore($mode);
  const search = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ margin: '0 10px' }} />
      <SearchInput
        value={search}
        handleClear={() => searchChanged('')}
        onChange={(e) => searchChanged(e.target.value)}
      />
      <Greedy />
      <AddButton
        disabled={mode === 'edit'}
        onClick={addClicked}
        style={{ margin: '0 10px' }}
      />
    </Toolbar>
  );
};

export { TableToolbar };
