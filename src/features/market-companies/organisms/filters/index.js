import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectField,
} from '../../../../ui';
import { MarketCategoriesSelectField } from '../../../market-categories-select';
import {
  defaultFilters,
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
  states,
} from '../../models';

const { t } = i18n;

const Filters = (props) => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted(defaultFilters)}
          enableReinitialize
          render={({ values }) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="categories"
                  component={MarketCategoriesSelectField}
                  label={t('CategoriesCompany')}
                  placeholder={t('SelectCategories')}
                  isMulti
                />
                <Field
                  name="states"
                  component={SelectField}
                  options={states}
                  label={t('StatusesCompany')}
                  placeholder={t('selectStatuses')}
                  isMulti
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};

export { Filters };
