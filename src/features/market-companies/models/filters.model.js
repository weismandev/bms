import i18n from '@shared/config/i18n';
import { createFilterBag } from '../../../tools/factories';

const { t } = i18n;

export const defaultFilters = {
  states: [],
  categories: [],
};

export const states = [
  { title: t('Draft'), name: 'draft' },
  { title: t('Published'), name: 'published' },
  { title: t('Hidden'), name: 'hidden' },
];

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
