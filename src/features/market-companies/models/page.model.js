import { createEffect, createStore } from 'effector';

import { $pathname } from '@features/common';
import { createPageBag } from '../../../tools/factories';
import { marketCompaniesApi } from '../api';

export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);
export const $paidTicketsInfo = createStore(null);
export const $paidTicketsTypes = createStore([]);

export const fxCheckPaidTicketsEnabled = createEffect(marketCompaniesApi.checkPaidTicketsEnabled);
export const fxTicketTypes = createEffect(marketCompaniesApi.getTypesList);

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  marketCompaniesApi,
  {},
  {
    idAttribute: 'id',
    itemsAttribute: 'companies',
    createdPath: 'company',
    updatedPath: 'company',
  }
);
