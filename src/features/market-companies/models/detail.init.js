import { forward, attach, merge, guard, sample } from 'effector';
import { delay } from 'patronum';
import { $pathname, history } from '@features/common';
import { formatFile } from '@tools/formatFile';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import {
  entityApi,
  $opened,
  $mode,
  openViaUrl,
  $isDetailOpen,
  states,
  types,
  newEntity,
  changedDetailVisibility,
} from './detail.model';
import {
  fxCreate,
  fxUpdate,
  fxDelete,
  fxGetById,
  pageUnmounted,
  fxGetList,
  $entityId,
  $path,
} from './page.model';

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetById.prepend(({ entityId }) => entityId),
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxGetById.done,
  fn: ({ result: { company } }) => {
    if (!Boolean(company)) {
      history.push('.');

      return false;
    }

    return true;
  },
  target: $isDetailOpen,
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        company: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$opened
  .on(merge([fxCreate.done, fxUpdate.done, fxGetById.done]), (state, { result }) =>
    formatOpened(result.company)
  )
  .on(fxDelete.done, () => newEntity)
  .reset(pageUnmounted);

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetById.done, () => true)
  .on(fxDelete.done, () => false)
  .reset([pageUnmounted, fxDelete.done, fxGetById.fail]);

$mode.on(merge([fxCreate.done, fxUpdate.done, fxDelete.done]), () => 'view');

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: formatPayload,
  }),
});

forward({
  from: entityApi.update,
  to: attach({
    effect: fxUpdate,
    mapParams: formatPayload,
  }),
});

forward({ from: openViaUrl, to: fxGetById });

function formatOpened(data) {
  const {
    state,
    tagline,
    highlights,
    summary,
    description,
    delivery,
    contacts,
    logo,
    background,
    images,
    accent_color,
    id,
    title,
    categories,
    blacklist,
    whitelist,
    edit_enabled,
    is_paid_tickets_enabled,
    paid_tickets_type_id,
  } = data;

  return {
    id: id || '',
    edit_enabled,
    title: title || '',
    state: states.find((i) => i.id === state) || '',
    tagline: tagline || '',
    highlights: Array.isArray(highlights) ? highlights : [],
    summary: summary || '',
    description: description || '',
    delivery: delivery || '',
    blacklist: blacklist ? blacklist : { localities: [], complex: [] },
    whitelist: whitelist ? whitelist : { localities: [], complex: [] },
    categories: Array.isArray(categories) ? categories : [],
    logo: logo ? formatFile(logo) : '',
    background: background ? formatFile(background) : '',
    images: Array.isArray(images) ? images.map(formatFile) : [],
    accent_color: accent_color ? accent_color : '#000000',
    contacts: contacts.map((i) => ({
      ...i,
      type: types.find((t) => t.id === i.type) || '',
    })),
    is_paid_tickets_enabled,
    paid_tickets_type_id: paid_tickets_type_id || null,
  };
}

function formatPayload(payload) {
  const {
    state,
    categories,
    background,
    images,
    logo,
    blacklist,
    whitelist,
    contacts,
    edit_enabled,
    is_paid_tickets_enabled,
    paid_tickets_type_id,
    ...rest
  } = payload;

  const orderToTicketPayload = (type, isEnabled) => {
    if (!isEnabled || !type?.id) {
      return { paid_tickets_type_id: null, is_paid_tickets_enabled: false };
    }
    return { paid_tickets_type_id: type.id, is_paid_tickets_enabled: true };
  };

  return {
    state: state ? state.id : '',
    categories: Array.isArray(categories)
      ? categories.map((i) => (typeof i === 'object' ? i.id : i))
      : [],
    background_file_id: background ? background.id : '',
    images: Array.isArray(images) ? images.map((i) => i.id) : [],
    logo_file_id: logo ? logo.id : '',
    contacts: Array.isArray(contacts)
      ? contacts.map((i) => ({ ...i, type: i.type.id }))
      : [],
    blacklist: blacklist
      ? {
        localities: Array.isArray(blacklist.localities)
          ? blacklist.localities.map((i) => (typeof i === 'object' ? i.id : i))
          : [],
        complex: Array.isArray(blacklist.complex)
          ? blacklist.complex.map((i) => (typeof i === 'object' ? i.id : i))
          : [],
      }
      : {},
    whitelist: whitelist
      ? {
        localities: Array.isArray(whitelist.localities)
          ? whitelist.localities.map((i) => (typeof i === 'object' ? i.id : i))
          : [],
        complex: Array.isArray(whitelist.complex)
          ? whitelist.complex.map((i) => (typeof i === 'object' ? i.id : i))
          : [],
      }
      : {},
    ...orderToTicketPayload(paid_tickets_type_id, is_paid_tickets_enabled),
    ...rest,
  };
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
