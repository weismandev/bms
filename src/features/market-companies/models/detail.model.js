import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

export const newEntity = {
  state: '',
  title: '',
  tagline: '',
  highlights: [],
  summary: '',
  description: '',
  delivery: '',
  logo: null,
  background: null,
  contacts: [],
  categories: [],
  images: [],
  accent_color: '#000000',
  blacklist: { localities: [], complex: [] },
  whitelist: { localities: [], complex: [] },
  is_paid_tickets_enabled: false,
  paid_tickets_type_id: null,
};

export const states = [
  { id: 'draft', title: t('Draft') },
  { id: 'published', title: t('Published') },
  { id: 'hidden', title: t('Hidden') },
];

export const types = [
  { id: 'email', title: 'Email' },
  { id: 'phone', title: t('Label.phone') },
  { id: 'website', title: t('Website') },
  { id: 'address', title: t('Label.address') },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
