import { forward, attach, sample } from 'effector';
import { $opened } from './detail.model';
import { $filters } from './filters.model';
import {
  pageMounted,
  fxGetList,
  fxDelete,
  deleteConfirmed,
  $isLoading,
  $paidTicketsInfo,
  $paidTicketsTypes,
  fxCheckPaidTicketsEnabled,
  fxTicketTypes,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

sample({
  clock: [pageMounted, $tableParams, $filters, $settingsInitialized],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }) => makePayload({ filters, table }),
  target: fxGetList,
});

forward({
  from: deleteConfirmed,
  to: attach({
    effect: fxDelete,
    source: $opened,
    mapParams: (payload, { id }) => id,
  }),
});

function makePayload({ filters, table }) {
  const { page, per_page, search, sorting } = table;
  const { categories, states } = filters;

  const params = { page, per_page };
  const data = {};

  if (search) data.query = search;

  if (sorting.length > 0) {
    data.sorting_column = sorting[0].name;
    data.order = sorting[0].order;
  }

  if (categories.length > 0)
    data.categories = categories.reduce((acc, { id }) => [...acc, id], []);

  if (states.length > 0)
    data.states = states.reduce((acc, { name }) => [...acc, name], []);

  return { params, data };
}

/**
 * Работа с типами заявок, проверка, фильтрация
 */

sample({
  clock: pageMounted,
  target: fxCheckPaidTicketsEnabled,
});

sample({
  clock: fxCheckPaidTicketsEnabled.doneData,
  target: $paidTicketsInfo,
});

sample({
  clock: $paidTicketsInfo,
  // Запросить типы заявок только в случае если включены платные заявки
  filter: (_, clock) => clock?.enabled_for_bms,
  target: fxTicketTypes,
});

sample({
  clock: fxTicketTypes.doneData,
  // Фильтр только для платных типов заявок
  fn: (_, { types }) => Array.isArray(types) ? types.filter(({ paid }) => !!paid) : [],
  target: $paidTicketsTypes,
});
