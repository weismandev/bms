import { $filters } from './filters.model';
import { $currentPage, $search } from './table.model';

$currentPage.reset([$filters, $search]);
