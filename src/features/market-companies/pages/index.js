import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '../../../ui';

import { Detail, Filters, Table } from '../organisms';

import {
  $error,
  $isDeleteDialogOpen,
  $isDetailOpen,
  $isErrorDialogOpen,
  $isFilterOpen,
  $isLoading,
  $path,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models';

const MarketCompaniesPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const error = useStore($error);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        params={{ detailWidth: '440px' }}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        filter={isFilterOpen && <Filters />}
      />
    </>
  );
};

const RestrictedMarketCompaniesPage = (props) => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <MarketCompaniesPage />
    </HaveSectionAccess>
  );
};

export { RestrictedMarketCompaniesPage as MarketCompaniesPage };
