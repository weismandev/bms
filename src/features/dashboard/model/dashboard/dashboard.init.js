import { forward } from 'effector';
import { fxGetList as fxGetComplexes } from '@features/complex-select/model';
import { fxGetList as fxGetHouses } from '@features/house-select/models';
import { dashboardMounted } from './dashboard.model';

// Запрашиваем вспомогающие сторы для списков в фильтрах
forward({
  from: dashboardMounted,
  to: [fxGetComplexes, fxGetHouses],
});
