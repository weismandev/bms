import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { $userRole } from '@features/common';
import { WidgetDevices } from '@features/widgets/widget-devices';
import { WidgetIndicators } from '@features/widgets/widget-indicators';
import { WidgetLateness } from '@features/widgets/widget-lateness';
import { WidgetOpenDoors } from '@features/widgets/widget-open-doors';
import { WidgetSchedule } from '@features/widgets/widget-schedule';
import { WidgetSignals } from '@features/widgets/widget-signals';
import { CustomScrollbar } from '@ui/index';

const useStyles = makeStyles({
  wrap: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
  containerOneColumn: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
  containerTwoColumns: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
  column: {
    display: 'grid',
    gap: 24,
    alignItems: 'start',
    alignContent: 'start',
  },
});

export const WidgetsGrid = () => {
  const { widgets } = useStore($userRole);
  const classes = useStyles();

  return (
    <CustomScrollbar trackHorizontalStyle={null}>
      <div className={classes.wrap}>
        {widgets.indicators && (
          <div className={classes.containerOneColumn}>
            <WidgetIndicators />
          </div>
        )}

        <div className={classes.containerTwoColumns}>
          <div className={classes.column}>
            {widgets.schedule && <WidgetSchedule />}
            {widgets.devices && <WidgetDevices />}
          </div>

          <div className={classes.column}>
            {widgets.lateness && <WidgetLateness />}
            {widgets.doors && <WidgetOpenDoors />}
            {widgets.signals && <WidgetSignals />}
          </div>
        </div>
      </div>
    </CustomScrollbar>
  );
};
