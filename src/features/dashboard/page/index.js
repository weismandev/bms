import { FilterMainDetailLayout } from '@ui/templates';

// import { Filter } from '../organisms/filter';
import { WidgetsGrid } from '../organisms/widgets-grid';

import { DashboardGate } from '../model/dashboard';
import { HaveSectionAccess } from '../../common';

const DashboardPage = () => (
  <>
    <DashboardGate />
    <FilterMainDetailLayout
      // filter={<Filter />}
      main={<WidgetsGrid />}
      params={{ filterWidth: '400px' }}
    />
  </>
);

const RestrictedDashboardPage = () => (
  <HaveSectionAccess>
    <DashboardPage />
  </HaveSectionAccess>
);

export { RestrictedDashboardPage as DashboardPage };
