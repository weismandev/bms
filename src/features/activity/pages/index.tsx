import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, ErrorMessage, Loader } from '@ui/index';
import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isLoading,
  $isFilterOpen,
  $isDetailOpen,
} from '../models';
import { ActivityTable, Filter, Detail } from '../organisms';

const ActivityPage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<ActivityTable />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedActivityPage: FC = () => (
  <HaveSectionAccess>
    <ActivityPage />
  </HaveSectionAccess>
);

export { RestrictedActivityPage as ActivityPage };
