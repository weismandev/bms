import { api } from '@api/api2';
import { JournalListPayload } from '../interfaces';

const getList = (payload: JournalListPayload) =>
  api.v4('get', 'journal-event/list-page', payload);
const getFiltersData = () => api.v4('get', 'journal-event/event-filter/list');
const exportList = (payload: JournalListPayload) =>
  api.v4('get', 'journal-event/export/list', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });

export const activityApi = {
  getList,
  getFiltersData,
  exportList,
};
