export interface JournalListPayload {
  page: number;
  per_page: number;
  search: string;
  sort?: string;
  order?: string;
  event_type_slug_list?: string[];
  dictionary_slug_list?: string[];
  user_ids?: string[];
  building_ids?: number[];
  start_date?: string;
  end_date?: string;
  export_type?: string;
}

export interface JournalListResponse {
  meta: {
    total: number;
  };
  journal_list: JournalItem[];
}

export interface JournalItem {
  uuid: string;
  building: null;
  date_time: string;
  dictionary_name: string;
  dictionary_slug: string;
  event_type_name: string;
  event_type_slug: string;
  file_name: null;
  file_url: null;
  id: number;
  new_values: object;
  old_values: object;
  personal_account_element_type_name: string;
  personal_account_element_type_slug: string;
  user_full_name: string;
}

export interface GetFiltersDataResponse {
  journal__event_filter_list: {
    building_list: [];
    dictionary_type_list: {
      [key: string]: string;
    };
    event_type_list: {
      [key: string]: string;
    };
    user_list: {
      [key: string]: string;
    }[];
  };
}

export interface Value {
  id: string | number;
  title: string;
}

export interface ValueCard {
  label: string;
  value: string;
}
