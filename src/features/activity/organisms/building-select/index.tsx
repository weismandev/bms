import { FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { $buildingsList } from '../../models/page';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

const BuildingSelect: FC<Props> = (props) => {
  const options = useStore($buildingsList);

  return <SelectControl options={options} {...props} />;
};

const BuildingSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<BuildingSelect />} onChange={onChange} {...props} />;
};

export { BuildingSelect, BuildingSelectField };
