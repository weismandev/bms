import { FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { $dictionaryTypeList } from '../../models/page';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

const DictionarySelect: FC<Props> = (props) => {
  const options = useStore($dictionaryTypeList);

  return <SelectControl options={options} {...props} />;
};

const DictionarySelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<DictionarySelect />} onChange={onChange} {...props} />;
};

export { DictionarySelect, DictionarySelectField };
