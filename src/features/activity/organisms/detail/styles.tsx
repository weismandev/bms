import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  formHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: 15,
    paddingRight: 24,
  },
  closeIcon: {
    color: '#7D7D8E',
    marginTop: 2,
  },
  form: {
    height: '100%',
    position: 'relative',
  },
  formContainer: {
    height: 'calc(100% - 64px)',
    padding: '0 0 24px 24px',
  },
  formContent: {
    paddingRight: 24,
  },
  field: {
    width: '50%',
    padding: '15px 0px 10px 10px',
    '@media (max-width: 1024px)': {
      width: '100%',
    },
  },
  twoField: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
}));

export { useStyles };
