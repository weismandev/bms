import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  CustomScrollbar,
  InputField,
  FormSectionHeader,
  CloseButton,
  Divider,
} from '@ui/index';
import { ValueCard } from '../../interfaces';
import { $opened, changedDetailVisibility } from '../../models';
import { useStyles } from './styles';

const Detail = memo(() => {
  const { t } = useTranslation();
  const opened = useStore($opened);

  const classes = useStyles();

  const onClick = () => changedDetailVisibility(false);

  const { old_values, new_values } = opened;

  const isExistValues = old_values.length > 0 && new_values.length > 0;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        enableReinitialize
        //@ts-ignore
        onSubmit={() => null}
        render={() => (
          <Form className={classes.form}>
            <div className={classes.formContainer}>
              <div className={classes.formHeader}>
                <FormSectionHeader header={opened.personal_account_element_type_name} />
                <CloseButton classes={{ root: classes.closeIcon }} onClick={onClick} />
              </div>
              <CustomScrollbar>
                <div className={classes.formContent}>
                  <Field
                    name="number"
                    component={InputField}
                    label="ID"
                    mode="view"
                    placeholder="ID"
                  />
                  <Field
                    name="date_time"
                    component={InputField}
                    label={t('dateAndTime')}
                    mode="view"
                    placeholder={t('dateAndTime')}
                  />
                  <Field
                    name="event_type_name"
                    component={InputField}
                    label={t('Label.action')}
                    mode="view"
                    placeholder={t('action')}
                    divider={null}
                  />
                  <Divider gap="10px 0 0" />
                  {isExistValues && (
                    <>
                      {old_values.map((item: ValueCard, index: number) => (
                        <div key={index} className={classes.twoField}>
                          <div
                            className={classes.field}
                            style={{
                              backgroundColor: 'rgba(235, 87, 87, 0.15)',
                            }}
                          >
                            <Field
                              value={item.value}
                              component={InputField}
                              label={`${item.label} - ${t('was')}`}
                              mode="view"
                              placeholder=""
                              divider={null}
                            />
                          </div>
                          <div
                            className={classes.field}
                            style={{
                              backgroundColor: 'rgba(200, 230, 201, 0.4)',
                            }}
                          >
                            <Field
                              value={new_values[index]?.value || ''}
                              component={InputField}
                              label={
                                new_values[index]?.label
                                  ? `${new_values[index].label} - ${t('became')}`
                                  : ''
                              }
                              mode="view"
                              placeholder=""
                              divider={null}
                            />
                          </div>
                        </div>
                      ))}
                    </>
                  )}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
});

export { Detail };
