import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import { VerticalAlignBottom } from '@mui/icons-material';
import Xlsx from '@img/xlsx.svg';
import Xml from '@img/xml.svg';
import i18n from '@shared/config/i18n';
import { Popover, Menu, ActionIconButton } from '@ui/index';
import { $count, exportList, $isLoading } from '../../models';

const { t } = i18n;

const moreItems = [
  {
    title: `${t('t')} XLS`,
    icon: <Xlsx />,
    onClick: () => exportList('xlsx'),
  },
  {
    title: `${t('t')} XLS`,
    icon: <Xml />,
    onClick: () => exportList('xml'),
  },
];

const ExportButton: FC = () => {
  const totalCount = useStore($count);
  const isLoading = useStore($isLoading);

  return (
    <Tooltip title={t('export') as string}>
      <div>
        <Popover
          trigger={
            <ActionIconButton disabled={totalCount === 0 || isLoading}>
              <VerticalAlignBottom />
            </ActionIconButton>
          }
          disabled={totalCount === 0 || isLoading}
        >
          <Menu items={moreItems} />
        </Popover>
      </div>
    </Tooltip>
  );
};

export { ExportButton };
