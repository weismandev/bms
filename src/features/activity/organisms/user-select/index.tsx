import { FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { $userList } from '../../models/page';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

const UserSelect: FC<Props> = (props) => {
  const options = useStore($userList);

  return <SelectControl options={options} {...props} />;
};

const UserSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<UserSelect />} onChange={onChange} {...props} />;
};

export { UserSelect, UserSelectField };
