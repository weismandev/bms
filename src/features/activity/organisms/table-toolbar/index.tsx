import { FC } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, Greedy, AdaptiveSearch, FilterButton } from '@ui/index';
import {
  $count,
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
} from '../../models';
import { ExportButton } from '../export-button';

const TableToolbar: FC = () => {
  const totalCount = useStore($count);
  const searchValue = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        style={{ marginRight: 10 }}
        onClick={() => changedFilterVisibility(true)}
      />
      <AdaptiveSearch
        {...{ totalCount, searchValue, searchChanged, isAnySideOpen: false }}
      />
      <Greedy />
      <ExportButton />
    </Toolbar>
  );
};

export { TableToolbar };
