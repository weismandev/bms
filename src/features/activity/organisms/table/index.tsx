import { useMemo, ReactNode, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, SortingState } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  PagingPanel,
  SortingLabel,
} from '@ui/index';
import {
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $count,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $columnOrder,
  columnOrderChanged,
  $sorting,
  sortChanged,
  excludedColumnsFromSort,
  $opened,
  open,
} from '../../models';
import { TableToolbar } from '../table-toolbar';

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [props.children, isSelected]
  );

  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: ReactNode) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const ActivityTable: React.FC = () => {
  const { t } = useTranslation();
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($count);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const columnOrder = useStore($columnOrder);
  const sorting = useStore($sorting);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <CustomPaging totalCount={rowCount} />

        <DragDropProvider />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { ActivityTable };
