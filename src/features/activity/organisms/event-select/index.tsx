import { FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Value } from '../../interfaces';
import { $eventTypeList } from '../../models/page';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

const EventSelect: FC<Props> = (props) => {
  const options = useStore($eventTypeList);

  return <SelectControl options={options} {...props} />;
};

const EventSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<EventSelect />} onChange={onChange} {...props} />;
};

export { EventSelect, EventSelectField };
