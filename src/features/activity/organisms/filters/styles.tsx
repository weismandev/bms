import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  form: {
    padding: 24,
    paddingTop: 0,
  },
}));

export { useStyles };
