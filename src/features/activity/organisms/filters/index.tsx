import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
} from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { BuildingSelectField } from '../building-select';
import { DictionarySelectField } from '../dictionary-select';
import { EventSelectField } from '../event-select';
import { UserSelectField } from '../user-select';
import { useStyles } from './styles';

const Filter = memo(() => {
  const filters = useStore($filters);
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={() => (
            <Form className={classes.form}>
              <Field
                label={t('startingFromTheDate')}
                placeholder={t('selectDate')}
                component={InputField}
                name="start_date"
                type="datetime-local"
              />
              <Field
                label={t('untilDate')}
                placeholder={t('selectDate')}
                component={InputField}
                name="end_date"
                type="datetime-local"
              />
              <Field
                label={t('Label.action')}
                placeholder={t('selectAction')}
                component={EventSelectField}
                name="events"
                isMulti
              />
              <Field
                label={t('directory')}
                placeholder={t('selectDirectory')}
                component={DictionarySelectField}
                name="dictionaries"
                isMulti
              />
              <Field
                label={t('user')}
                placeholder={t('selectUser')}
                component={UserSelectField}
                name="users"
                isMulti
              />
              <Field
                label={t('building')}
                placeholder={t('selectBbuilding')}
                component={BuildingSelectField}
                name="buildings"
                isMulti
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
