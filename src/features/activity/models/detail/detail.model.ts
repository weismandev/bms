import { createDetailBag } from '@tools/factories';

export const {
  changedDetailVisibility,
  open,
  detailClosed,

  $isDetailOpen,
  $opened,
} = createDetailBag({});
