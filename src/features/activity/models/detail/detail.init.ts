import { sample } from 'effector';
import { JournalItem, ValueCard } from '../../interfaces';
import { $normalized } from '../page/page.model';
import { open, $opened } from './detail.model';

interface Result {
  id: number | string;
  number: number;
  date_time: string;
  event_type_name: string;
  old_values?: ValueCard[];
  new_values?: ValueCard[];
  personal_account_element_type_name: string;
}

sample({
  clock: open,
  source: $normalized,
  fn: (data: { [key: number]: JournalItem }, id: number) => {
    const opened = data[id];

    const result: Result = {
      id: Boolean(opened.uuid) ? opened.uuid : opened.id,
      number: opened.id,
      date_time: opened.date_time,
      event_type_name: opened.event_type_name,
      personal_account_element_type_name: opened.personal_account_element_type_name,
    };

    const oldValues = opened?.old_values
      ? Object.entries(opened.old_values).map((item) => ({
          label: item[0],
          value: item[1],
        }))
      : [];

    const newValues = opened?.new_values
      ? Object.entries(opened.new_values).map((item) => ({
          label: item[0],
          value: item[1],
        }))
      : [];

    result.old_values = oldValues;
    result.new_values = newValues;

    return result;
  },
  target: $opened,
});
