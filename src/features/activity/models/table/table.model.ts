import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { JournalItem } from '../../interfaces';
import { $list } from '../page/page.model';

const { t } = i18n;

export const columns = [
  { title: '№ пп', name: 'number' },
  { title: t('dateAndTime'), name: 'date' },
  { title: t('Label.action'), name: 'event_type_name' },
  { title: t('directory'), name: 'dictionary_name' },
  { title: t('elementPA'), name: 'personal_account_element_type_name' },
  { title: t('Label.FullName'), name: 'user_name' },
  { title: t('Label.building'), name: 'building' },
  { title: t('fileUpload/download'), name: 'file_name' },
];

const columnsWidth = [
  { columnName: 'number', width: 120 },
  { columnName: 'date', width: 200 },
  { columnName: 'event_type_name', width: 180 },
  { columnName: 'dictionary_name', width: 230 },
  { columnName: 'personal_account_element_type_name', width: 260 },
  { columnName: 'user_name', width: 300 },
  { columnName: 'building', width: 300 },
  { columnName: 'file_name', width: 250 },
];

const formatData = (data: JournalItem[]) =>
  data.map((item) => ({
    id: item.uuid ? item.uuid : item.id,
    number: item.id,
    date: item?.date_time || '-',
    event_type_name: item?.event_type_name || '-',
    dictionary_name: item?.dictionary_name || '-',
    personal_account_element_type_name: item?.personal_account_element_type_name || '-',
    user_name: item?.user_full_name || '-',
    building: item?.building || '-',
    file_name: item?.file_name || '-',
  }));

export const $tableData = $list.map((data) => {
  if (!Array.isArray(data)) {
    return [];
  }

  return formatData(data);
});

export const excludedColumnsFromSort = ['building', 'file_name'];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  columnOrderChanged,
  sortChanged,

  $search,
  $columnOrder,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
  $sorting,
} = createTableBag(columns, {
  widths: columnsWidth,
  order: [],
  pageSize: 25,
  hiddenColumns: [],
});
