import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import {
  JournalListPayload,
  JournalListResponse,
  JournalItem,
  GetFiltersDataResponse,
  Value,
} from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const exportList = createEvent<string>();

export const fxGetList = createEffect<JournalListPayload, JournalListResponse, Error>();
export const fxGetFiltersData = createEffect<void, GetFiltersDataResponse, Error>();
export const fxExportList = createEffect<JournalListPayload, ArrayBuffer, Error>();

export const $list = createStore<JournalItem[]>([]);
export const $count = createStore<number>(0);
export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $dictionaryTypeList = createStore<Value[]>([]);
export const $eventTypeList = createStore<Value[]>([]);
export const $buildingsList = createStore<Value[]>([]);
export const $userList = createStore<Value[]>([]);
export const $normalized = $list.map((data) =>
  data.reduce(
    (acc, item) => ({
      ...acc,
      [Boolean(item.uuid) ? item.uuid : item.id]: item,
    }),

    {}
  )
);
