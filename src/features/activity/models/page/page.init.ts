import { merge, sample, forward } from 'effector';
import { pending } from 'patronum/pending';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { activityApi } from '../../api';
import { JournalListPayload, Value } from '../../interfaces';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  fxGetList,
  $list,
  $count,
  $error,
  $isLoading,
  $isErrorDialogOpen,
  fxGetFiltersData,
  $dictionaryTypeList,
  $eventTypeList,
  $userList,
  exportList,
  fxExportList,
  $buildingsList,
} from './page.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';

interface Payload {
  page: number;
  per_page: number;
  search: string;
  sorting: {
    name: string;
    order: string;
  }[];
}

fxGetList.use(activityApi.getList);
fxGetFiltersData.use(activityApi.getFiltersData);
fxExportList.use(activityApi.exportList);

const { t } = i18n;

const errorOccured = merge([fxGetList.fail, fxGetFiltersData.fail, fxExportList.fail]);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading
  .on(
    pending({
      effects: [fxGetList, fxGetFiltersData, fxExportList],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

sample({
  clock: [pageMounted, $tableParams, $filters, $settingsInitialized],
  source: {
    tableParams: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ tableParams, filters }) => formatPayload(tableParams, filters),
  target: fxGetList,
});

const formatPayload = ({ page, per_page, search, sorting }: Payload, filters: any) => {
  const payload: JournalListPayload = {
    page,
    per_page,
    search,
  };

  if (sorting.length > 0) {
    payload.sort = sorting[0].name;
    payload.order = sorting[0].order.toUpperCase();
  }

  if (filters.start_date.length > 0) {
    const date = format(new Date(filters.start_date), 'yyyy-MM-dd HH:mm:ss');

    payload.start_date = date;
  }

  if (filters.end_date.length > 0) {
    const date = format(new Date(filters.end_date), 'yyyy-MM-dd HH:mm:ss');

    payload.end_date = date;
  }

  if (filters.events.length > 0) {
    const events = filters.events.map((event: Value) => event.id);

    payload.event_type_slug_list = events;
  }

  if (filters.dictionaries.length > 0) {
    const dictionaries = filters.dictionaries.map((dictionary: Value) => dictionary.id);

    payload.dictionary_slug_list = dictionaries;
  }

  if (filters.users.length > 0) {
    const users = filters.users.map((user: Value) => user.id);

    payload.user_ids = users;
  }

  if (filters.buildings.length > 0) {
    const buildings = filters.buildings.map((building: Value) => building.id);

    payload.building_ids = buildings;
  }

  return payload;
};

$count
  .on(fxGetList.done, (_, { result }) => result?.meta?.total || 0)
  .reset([pageUnmounted, signout]);

$list
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.journal_list)) {
      return [];
    }

    return result.journal_list;
  })
  .reset([pageUnmounted, signout]);

forward({
  from: pageMounted,
  to: fxGetFiltersData,
});

$dictionaryTypeList
  .on(fxGetFiltersData.done, (_, { result: { journal__event_filter_list } }) => {
    if (!journal__event_filter_list?.dictionary_type_list) {
      return [];
    }

    return Object.entries(journal__event_filter_list.dictionary_type_list).map(
      (item) => ({
        id: item[0],
        title: item[1],
      })
    );
  })
  .reset([pageUnmounted, signout]);

$eventTypeList
  .on(fxGetFiltersData.done, (_, { result: { journal__event_filter_list } }) => {
    if (!journal__event_filter_list?.event_type_list) {
      return [];
    }

    return Object.entries(journal__event_filter_list.event_type_list).map((item) => ({
      id: item[0],
      title: item[1],
    }));
  })
  .reset([pageUnmounted, signout]);

$userList
  .on(fxGetFiltersData.done, (_, { result: { journal__event_filter_list } }) => {
    if (!journal__event_filter_list?.user_list) {
      return [];
    }

    const formattedUsers = journal__event_filter_list.user_list.map((user) =>
      Object.entries(user).map((item) => ({
        id: item[0],
        title: item[1],
      }))
    );

    return formattedUsers.flat();
  })
  .reset([pageUnmounted, signout]);

$buildingsList
  .on(fxGetFiltersData.done, (_, { result: { journal__event_filter_list } }) => {
    if (!journal__event_filter_list?.building_list) {
      return [];
    }

    const formattedBuildings = journal__event_filter_list.building_list.map((item) =>
      Object.entries(item).map((elem: any) => ({
        id: Number.parseInt(elem[0], 10),
        title: elem[1].toString(),
      }))
    );

    return formattedBuildings.flat();
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: exportList,
  source: { tableParams: $tableParams, filters: $filters },
  fn: ({ tableParams, filters }, exportType) => {
    const payload = formatPayload(tableParams, filters);

    return { ...payload, ...{ export_type: exportType } };
  },
  target: fxExportList,
});

fxExportList.done.watch(({ params: { export_type }, result }) => {
  if (export_type === 'xlsx') {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `${dateTimeNow}_${t('upload_user_actions')}.xls`
    );
  }

  if (export_type === 'xml') {
    const blob = new Blob([result], {
      type: 'application/xml',
    });
    const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `${dateTimeNow}_${t('upload_user_actions')}.xml`
    );
  }
});
