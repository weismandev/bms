import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  start_date: '',
  end_date: '',
  events: [],
  dictionaries: [],
  users: [],
  buildings: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
