import { createEffect } from 'effector';
import { storageApi } from '../api';

export const fxGetFromStorage = createEffect();
export const fxSaveInStorage = createEffect();

fxGetFromStorage.use(storageApi.getFromStorage);
fxSaveInStorage.use(storageApi.saveInStorage);
