import { createGate } from 'effector-react';

export const AppGate = createGate();
export const { open: appMounted, close: appUnmounted, state: appState } = AppGate;
