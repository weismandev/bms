import { createStore } from 'effector';

const $timezone = createStore(Intl.DateTimeFormat().resolvedOptions().timeZone);

export { $timezone };
