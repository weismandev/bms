import { allSettled, fork } from 'effector';
import {
  $history,
  $historySearch,
  $pathname,
  routingDomain,
  searchPush,
  updateLocation,
} from '../routing';

describe('routing model test', () => {
  describe('history store and his mapped store get correct value', () => {
    const scope = fork(routingDomain);

    beforeEach(async () => {
      await allSettled(updateLocation, {
        scope,
        params: {
          search: '?id=101&tab=info',
          pathname: 'people',
        },
      });
    });

    test('set right params to $history store', () => {
      const history = { search: '?id=101&tab=info', pathname: 'people' };

      expect(scope.getState($history)).toEqual(history);
    });

    test('mapped store $search get correct value from $history', () => {
      const id = scope.getState($historySearch).get('id');
      const tab = scope.getState($historySearch).get('tab');

      expect(id).toEqual('101');
      expect(tab).toEqual('info');
    });

    test('mapped store $pathname get correct value from $history', () => {
      const pathname = scope.getState($pathname);

      expect(pathname).toEqual('people');
    });
  });

  describe('search event works correctly', () => {
    const scope = fork(routingDomain);

    beforeEach(async () => {
      await allSettled(updateLocation, {
        scope,
        params: {
          search: '?employee=303404&status=active',
          pathname: 'companies',
        },
      });
    });

    test('searchPush add search param event work correctly', async () => {
      await allSettled(searchPush, {
        scope,
        params: ['id', '707'],
      });

      const id = scope.getState($historySearch).get('id');
      const employee = scope.getState($historySearch).get('employee');
      const status = scope.getState($historySearch).get('status');

      expect(id).toEqual('707');
      expect(employee).toEqual('303404');
      expect(status).toEqual('active');
    });

    test('searchPush remove search param event work correctly', async () => {
      await allSettled(searchPush, { scope, params: ['status'] });

      const employee = scope.getState($historySearch).get('employee');
      const status = scope.getState($historySearch).get('status');

      expect(employee).toEqual('303404');
      expect(status).toBeNull();
    });
  });
});
