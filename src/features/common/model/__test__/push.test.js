import { fork, allSettled } from 'effector';
import { push, close, $pushQueue, pushDomain } from '../push';

describe('push model test', () => {
  const scope = fork(pushDomain);

  test('initial queue is empty array', () => {
    const queue = scope.getState($pushQueue);

    expect(queue).toEqual([]);
  });

  describe('queue events works correctly', () => {
    beforeEach(async () => {
      await allSettled(push, { scope, params: { text: 'first' } });
      await allSettled(push, { scope, params: { text: 'second' } });
    });

    test('push event works correctly', async () => {
      const payload = { text: 'push event' };

      await allSettled(push, {
        scope,
        params: payload,
      });

      const [_, __, current] = scope.getState($pushQueue);

      expect(current.text).toEqual(payload.text);
      expect(current).toHaveProperty('_id');
    });

    test('close event works correctly', async () => {
      const [first] = scope.getState($pushQueue);

      await allSettled(close, {
        scope,
        params: first._id,
      });

      const [second] = scope.getState($pushQueue);
      expect(second.text).toEqual('second');
    });

    test('queue objects id is different', () => {
      const [first, second] = scope.getState($pushQueue);

      expect(first._id).not.toEqual(second._id);
    });
  });
});
