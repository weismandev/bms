import { api } from '../../../api/api2';

const getSections = () => api.v1('get', 'section-descriptions/get-sections');

export const sectionsApi = {
  getSections,
};
