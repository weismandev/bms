import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
  Modal as NotificationModal,
} from '@ui/index';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $isDetailOpen,
  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
  $isNotificationDialogOpen,
  notificationVisibilityChanged,
  $notification,
} from '../models';
import { Table, Detail } from '../organisms';

const QuarantinePage = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isNotificationDialogOpen = useStore($isNotificationDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);

  const error = useStore($error);
  const notification = useStore($notification);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <NotificationModal
        isOpen={isNotificationDialogOpen}
        onClose={() => notificationVisibilityChanged(false)}
        header="Действие успешно выполено"
        content={notification}
      />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout main={<Table />} detail={isDetailOpen && <Detail />} />
    </>
  );
};

const RestrictedQuarantinePage = () => (
  <HaveSectionAccess>
    <QuarantinePage />
  </HaveSectionAccess>
);

export { RestrictedQuarantinePage as QuarantinePage };
