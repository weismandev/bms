import { createEffect, createStore, createEvent } from 'effector';
import { createPageBag } from '@tools/factories';
import { quarantineApi } from '../api';

export const fxGetEnterprises = createEffect();
export const $enterprises = createStore([]);
export const created = createEvent();
export const createdWithNotification = createEvent();

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  quarantineApi,
  {},
  { idAttribute: 'id', itemsAttribute: 'list', createdPath: 'item' }
);
