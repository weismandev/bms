import { forward, sample } from 'effector';
import format from 'date-fns/format';
import { entityApi, $opened, $mode, open, $isDetailOpen } from './detail.model';
import { fxCreate, $normalized, fxDelete, $enterprises, created } from './page.model';

$isDetailOpen.on(fxDelete.done, () => false);

$opened.on(
  sample({
    source: { normalized: $normalized, enterprises: $enterprises },
    clock: open,
    fn: ({ normalized, enterprises }, id) => formatOpened(normalized[id], enterprises),
  }),
  (_, opened) => opened
);

$mode.on(fxCreate.done, () => 'view');

sample({
  source: fxCreate.doneData,
  fn: ({ item }) => item.id,
  target: open,
});

forward({
  from: entityApi.create,
  to: fxCreate.prepend(formatPayload),
});

function formatPayload({ birthday, expired_at, enterprise, ...rest }) {
  const payload = {
    ...rest,
    expired_at: format(expired_at, 'yyyy-MM-dd'),
    enterprise_id: enterprise.id,
  };

  if (birthday) {
    payload.birthday = format(birthday, 'yyyy-MM-dd');
  }

  return payload;
}

function formatOpened(
  { birthday, expired_at, enterprise_id, patronymic_name, ...rest },
  enterprises
) {
  const payload = {
    ...rest,
    expired_at: new Date(expired_at),
    enterprise: enterprises.find((enterprise) => enterprise.id === enterprise_id),
    patronymic_name: patronymic_name || '',
  };

  if (birthday) {
    payload.birthday = new Date(birthday);
  }

  return payload;
}
