import { sample } from 'effector';
import format from 'date-fns/format';
import { $raw } from './page.model';
import { $enterprises } from './page.model';
import { $tableData } from './table.model';

sample({
  source: { raw: $raw, enterprises: $enterprises },
  fn: ({ raw, enterprises }) => formatTable(raw, enterprises),
  target: $tableData,
});

function formatTable({ data }, enterprises) {
  const formatDate = (value) => (value ? format(new Date(value), 'dd.MM.yyyy') : '');

  const getEnterpriseLabel = (enterpriseId) => {
    if (!enterpriseId || !Array.isArray(enterprises)) {
      return '';
    }
    return enterprises.find((item) => item.id === enterpriseId)?.title || '';
  };

  return data.map((item) => ({
    ...item,
    enterprise: getEnterpriseLabel(item.enterprise_id),
    birthday: formatDate(item.birthday),
    expired_at: formatDate(item.expired_at),
  }));
}
