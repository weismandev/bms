import { createStore } from 'effector';
import { createDialogBag } from '@tools/factories/page';

export const {
  $isDialogOpen: $isNotificationDialogOpen,
  dialogVisibilityChanged: notificationVisibilityChanged,
} = createDialogBag();

export const $notification = createStore(null);
