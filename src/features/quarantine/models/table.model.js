import { createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';

const { t } = i18n;

export const columns = [
  { name: 'last_name', title: t('Label.lastname') },
  { name: 'first_name', title: t('Label.name') },
  { name: 'patronymic_name', title: t('Label.patronymic') },
  { name: 'birthday', title: t('Birthday') },
  { name: 'enterprise', title: t('company') },
  { name: 'expired_at', title: t('Expires') },
];

export const $tableData = createStore([]);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns);
