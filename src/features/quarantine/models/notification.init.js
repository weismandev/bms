import { signout } from '@features/common';
import {
  $notification,
  $isNotificationDialogOpen,
  notificationVisibilityChanged,
} from './notification.model';
import { createdWithNotification } from './page.model';

$notification.on(createdWithNotification, (_, { message }) => message).reset(signout);

$isNotificationDialogOpen
  .on(createdWithNotification, () => true)
  .on(notificationVisibilityChanged, (_, state) => state)
  .reset(signout);
