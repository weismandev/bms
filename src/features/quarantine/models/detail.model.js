import { addYears } from 'date-fns';
import { createDetailBag } from '@tools/factories';

const newEntity = {
  first_name: '',
  last_name: '',
  patronymic_name: '',
  birthday: '',
  enterprise: '',
  expired_at: addYears(new Date(), 1),
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
