import { forward, attach, split, sample } from 'effector';
import { signout } from '@features/common';
import { quarantineApi } from '../api';
import { $opened } from './detail.model';
import {
  pageMounted,
  fxGetList,
  fxCreate,
  fxDelete,
  deleteConfirmed,
  fxGetEnterprises,
  $enterprises,
  created,
  createdWithNotification,
  $isLoading,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxGetEnterprises.use(quarantineApi.getEnterprisesList);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

split({
  source: fxCreate.doneData,
  match: (payload) => (Boolean(payload?.message) ? 'hasActivePass' : 'basic'),
  cases: {
    basic: created,
    hasActivePass: createdWithNotification,
  },
});

$enterprises
  .on(fxGetEnterprises.doneData, (_, { enterprises }) => enterprises)
  .reset(signout);

sample({
  clock: [pageMounted, $tableParams, $settingsInitialized],
  source: { tableParams: $tableParams, settingsInitialized: $settingsInitialized },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ tableParams }) => ({
    page: tableParams.page,
    per_page: tableParams.per_page,
  }),
  target: fxGetList,
});

forward({
  from: pageMounted,
  to: fxGetEnterprises,
});

forward({
  from: deleteConfirmed,
  to: attach({
    effect: fxDelete,
    source: $opened,
    mapParams: (payload, { id }) => id,
  }),
});
