import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    padding: 24,
  },
  gridRoot: {
    height: '100%',
  },
  toolbarRoot: {
    padding: '0',
    marginBottom: '12px',
    minHeight: '36px',
    height: '36px',
    flexWrap: 'nowrap',
    borderBottom: 'none',
  },
});
