import { useMemo, useCallback, memo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { TableToolbar } from '..';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $opened,
  open,
  columns,
  $rowCount,
} from '../../models';
import { useStyles } from './styles';

const { t } = i18n;

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => {
  const { gridRoot } = useStyles();
  return (
    <Grid.Root {...props} className={gridRoot}>
      {children}
    </Grid.Root>
  );
};

const ToolbarRoot = (props) => {
  const { toolbarRoot } = useStyles();
  return <Toolbar.Root {...props} className={toolbarRoot} />;
};

const Table = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const count = useStore($rowCount);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <DragDropProvider />

        <CustomPaging totalCount={count} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
