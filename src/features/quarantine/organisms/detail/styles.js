import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '89vh',
  },
  form: {
    padding: 24,
  },
  detailToolbar: {
    marginBottom: 20,
  },
});
