import { isValid } from 'date-fns';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const defaultErrorMessage = t('thisIsRequiredField');
const dateErrorMessage = t('PleaseEnterAValidDate');

const correctDate = Yup.date()
  .typeError(dateErrorMessage)
  .test(
    'expires-is-valid',
    dateErrorMessage,
    (date) => !date || (date && isValid(new Date(date)))
  );

const requiredString = Yup.string().required(defaultErrorMessage);

export const validationSchema = Yup.object().shape({
  last_name: requiredString,
  first_name: requiredString,
  patronymic_name: Yup.string(),
  birthday: correctDate,
  enterprise: Yup.object().required(defaultErrorMessage).typeError(defaultErrorMessage),
  expired_at: correctDate.required(dateErrorMessage),
});
