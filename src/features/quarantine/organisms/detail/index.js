import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { EnterpriseSelectField } from '@features/enterprise-select';
import i18n from '@shared/config/i18n';
import { Wrapper, DetailToolbar, InputField, CustomScrollbar } from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  deleteClicked,
} from '../../models';
import { BirthdayInputField, ExpiresInputField } from '../date-field';
import { useStyles } from './styles';
import { validationSchema } from './validation';

const { t } = i18n;

export const Detail = () => {
  const classes = useStyles();
  const mode = useStore($mode);
  const opened = useStore($opened);

  const isNew = !opened.id;

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <Formik
          initialValues={opened}
          onSubmit={detailSubmitted}
          validationSchema={validationSchema}
          enableReinitialize
          render={(props) => (
            <Form className={classes.form}>
              <DetailToolbar
                className={classes.detailToolbar}
                mode={mode}
                onEdit={() => changeMode('edit')}
                hidden={{ edit: true }}
                onDelete={() => deleteClicked()}
                onClose={() => changedDetailVisibility(false)}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    changeMode('view');
                    props.resetForm();
                  }
                }}
              />
              <Field
                name="last_name"
                label={t('Label.lastname')}
                placeholder={t('EnterLastName')}
                mode={mode}
                component={InputField}
                required
              />
              <Field
                name="first_name"
                label={t('Label.name')}
                placeholder={t('EnterYourName')}
                mode={mode}
                component={InputField}
                required
              />
              <Field
                name="patronymic_name"
                label={t('Label.patronymic')}
                placeholder={t('EnterMiddleName')}
                mode={mode}
                component={InputField}
              />
              <Field name="birthday" component={BirthdayInputField} mode={mode} />
              <Field
                name="enterprise"
                component={EnterpriseSelectField}
                label={t('company')}
                placeholder={t('ChooseCompany')}
                mode={mode}
                required
              />
              <Field name="expired_at" component={ExpiresInputField} mode={mode} />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};
