import { forwardRef } from 'react';
import DatePicker from 'react-datepicker';
import MaskedInput from 'react-text-mask';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { InputAdornment } from '@mui/material';
import TodayIcon from '@mui/icons-material/Today';
import i18n from '@shared/config/i18n';
import { InputField } from '@ui/index';

const { t } = i18n;

const dateMask = [/[0-3]/, /\d/, '.', /[0-1]/, /\d/, '.', /(1|2)/, /\d/, /\d/, /\d/];

const DateMaskedInput = (props) => (
  <MaskedInput {...props} keepCharPositions placeholder="__.__.____" mask={dateMask} />
);

const CustomDateInput = forwardRef(
  ({ value, onClick, field, form, onChange, mode }, ref) => (
    <InputField
      field={field}
      form={form}
      label={t('Birthday')}
      value={value}
      mode={mode}
      inputComponent={DateMaskedInput}
      divider
      onChange={onChange}
      endAdornment={
        mode === 'edit' && (
          <InputAdornment position="end">
            <TodayIcon
              style={{ zIndex: 1, height: 16, width: 16, cursor: 'pointer' }}
              ref={ref}
              onClick={onClick}
            />
          </InputAdornment>
        )
      }
    />
  )
);

const BirthdayInputField = ({ form, field, mode, ...restProps }) => (
  <DatePicker
    placeholderText={t('selectDate')}
    readOnly={mode === 'view'}
    customInput={<CustomDateInput form={form} field={field} mode={mode} {...restProps} />}
    name={field.name}
    selected={field.value}
    onChange={(value) => form.setFieldValue(field.name, value)}
    maxDate={new Date()}
    dateFormat="dd.MM.yyyy"
    dateFormatCalendar="LLLL yyyy"
    yearDropdownItemNumber={100}
    scrollableYearDropdown
    showYearDropdown
    showMonthDropdown
    locale={dateFnsLocale}
  />
);

const ExpiresInputField = ({ form, field, mode }) => (
  <DatePicker
    placeholderText={t('selectDate')}
    readOnly={mode === 'view'}
    customInput={<InputField field={field} form={form} label={t('Expires')} divider />}
    name={field.name}
    selected={field.value}
    onChange={(value) => form.setFieldValue(field.name, value)}
    minDate={new Date()}
    dateFormat="dd.MM.yyyy"
    locale={dateFnsLocale}
    required
  />
);

export { BirthdayInputField, ExpiresInputField };
