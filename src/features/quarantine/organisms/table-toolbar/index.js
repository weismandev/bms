import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Toolbar, FoundInfo, AddButton, Greedy } from '../../../../ui';
import { $rowCount, addClicked, $mode } from '../../models';

const useStyles = makeStyles({
  withMargin: {
    margin: '0 10px',
  },
});

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const mode = useStore($mode);
  const { withMargin } = useStyles();

  return (
    <Toolbar>
      <FoundInfo count={count} className={withMargin} />
      <Greedy />
      <AddButton disabled={mode === 'edit'} onClick={addClicked} className={withMargin} />
    </Toolbar>
  );
};

export { TableToolbar };
