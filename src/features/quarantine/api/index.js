import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'quarantine/list', payload);
const getById = (id) => api.v1('past', 'quarantine/get', { id });
const create = (payload) => api.v1('post', 'quarantine/create', payload);
const deleteItem = (id) => api.v1('post', 'quarantine/delete', { id });
const getEnterprisesList = () => api.v1('get', 'enterprises/get-simple-list');

export const quarantineApi = {
  getList,
  getById,
  create,
  delete: deleteItem,
  getEnterprisesList,
};
