import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesPropertiesSelect = (props) => {
  const { enterprise_id, excludeIds = [] } = props;

  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList({ enterprise_id });
  }, [enterprise_id]);

  const options = data.filter((item) => excludeIds.every((e) => item.id !== e));
  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const EnterprisesPropertiesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesPropertiesSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesPropertiesSelect, EnterprisesPropertiesSelectField };
