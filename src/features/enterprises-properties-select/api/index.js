import { api } from '../../../api/api2';

export const getList = (payload = {}) =>
  api.v1('get', 'enterprises/company-objects', payload);
