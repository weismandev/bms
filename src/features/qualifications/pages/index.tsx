import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';

import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '@ui/index';
import { HaveSectionAccess } from '@features/common';

import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedVisibilityDeleteModal,
  deleteQualification,
} from '../models';
import { Table, Detail } from '../organisms';

const QualificationsPage: FC = () => {
  const { t } = useTranslation();
  const [error, isErrorDialogOpen, isLoading, isDetailOpen, isOpenDeleteModal] = useUnit([
    $error,
    $isErrorDialogOpen,
    $isLoading,
    $isDetailOpen,
    $isOpenDeleteModal,
  ]);

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changedVisibilityDeleteModal(false);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <DeleteConfirmDialog
        header={t('DeletingAQualification')}
        content={t('ConfirmQualificationDeletion')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteQualification}
      />

      <FilterMainDetailLayout
        filter={null}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedQualificationsPage: FC = () => (
  <HaveSectionAccess>
    <QualificationsPage />
  </HaveSectionAccess>
);

export { RestrictedQualificationsPage as QualificationsPage };
