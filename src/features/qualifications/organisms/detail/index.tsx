import { FC, memo } from 'react';
import { useUnit } from 'effector-react';
import { Tab } from '@mui/material';

import { Tabs } from '@ui/index';

import { $opened, tabs, $currentTab, changeTab } from '../../models';
import { StyledWrapper } from './styled';
import { DetailInfo } from '../detail-info';
import { DetailEmployeesList } from '../detail-employees-list';

export const Detail: FC = memo(() => {
  const [opened, currentTab] = useUnit([$opened, $currentTab]);

  const isNew = !opened.id;

  const onChangeTab = (_: unknown, tab: string) => changeTab(tab);

  return (
    <StyledWrapper>
      <>
        <Tabs
          options={tabs}
          currentTab={currentTab}
          onChange={onChangeTab}
          isNew={isNew}
          tabEl={<Tab />}
        />
        {currentTab === 'info' && <DetailInfo opened={opened} isNew={isNew} />}
        {currentTab === 'employees' && <DetailEmployeesList />}
      </>
    </StyledWrapper>
  );
});
