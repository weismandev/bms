import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { CustomScrollbar, InputField } from '@ui/index';

import {
  detailSubmitted,
  $mode,
  changedDetailVisibility,
  changeMode,
  changedVisibilityDeleteModal,
} from '../../models';
import { InfoText } from '../../atoms';
import {
  StyledForm,
  StyledContent,
  StyledFormContent,
  StyledField,
  StyledDetailToolbar,
  StyledInfoTextWrapper,
} from './styled';
import { Opened } from '../../interfaces';
import { TicketTypesTree } from '../ticket-types-tree';
import { DeviceCategoriesSelectField } from '../device-categories-select';
import { ClassesSelectField } from '../classes-select';
import { validationSchema } from './validation';

interface Props {
  opened: Opened;
  isNew: boolean;
}

export const DetailInfo: FC<Props> = ({ opened, isNew }) => {
  const mode = useUnit($mode);
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={opened}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={detailSubmitted}
      render={({ values, resetForm, setFieldValue }) => {
        const onClose = () => changedDetailVisibility(false);

        const onCancel = () => {
          if (isNew) {
            changedDetailVisibility(false);
          } else {
            changeMode('view');
            resetForm();
          }
        };

        const onEdit = () => changeMode('edit');
        const onDelete = () => changedVisibilityDeleteModal(true);

        return (
          <StyledForm>
            <StyledDetailToolbar
              mode={mode}
              onEdit={onEdit}
              onClose={onClose}
              onCancel={onCancel}
              onDelete={onDelete}
            />
            <StyledContent>
              <CustomScrollbar>
                <StyledFormContent>
                  <StyledField>
                    <Field
                      name="title"
                      label={t('TitleOfQualification')}
                      placeholder={t('EnterTheTitle')}
                      component={InputField}
                      mode={mode}
                      required
                    />
                  </StyledField>
                  {mode === 'edit' && (
                    <StyledInfoTextWrapper>
                      <InfoText text={t('YouCanChooseServiceCategories')} />
                    </StyledInfoTextWrapper>
                  )}
                  <TicketTypesTree values={values} setFieldValue={setFieldValue} />
                  <StyledField>
                    <Field
                      name="classes"
                      label={t('RequestsClasses')}
                      placeholder={t('SelectRequestClass')}
                      component={ClassesSelectField}
                      mode={mode}
                      isMulti
                      required
                    />
                  </StyledField>
                  <StyledField>
                    <Field
                      name="deviceCategories"
                      label={t('DeviceCategories')}
                      placeholder={t('SelectCategory')}
                      component={DeviceCategoriesSelectField}
                      mode={mode}
                      isMulti
                      required
                    />
                  </StyledField>
                </StyledFormContent>
              </CustomScrollbar>
            </StyledContent>
          </StyledForm>
        );
      }}
    />
  );
};
