import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { Opened } from '../../interfaces';

const { t } = i18n;

export function typesRequired(this: { parent: Opened }) {
  const { types } = this.parent;

  const isExistTypes = types.filter((type) => type.value).length > 0;

  const isExistChildren =
    types.filter((item) => {
      if (Array.isArray(item?.children)) {
        return item.children.filter((i) => i.value).length > 0;
      }

      return false;
    }).length > 0;

  const isExistLastChildren =
    types.filter((element) => {
      if (Array.isArray(element?.children)) {
        return (
          element.children.filter((elem) => {
            if (Array.isArray(elem?.children)) {
              return elem.children.filter((child) => child.value).length > 0;
            }

            return false;
          }).length > 0
        );
      }

      return false;
    }).length > 0;

  return isExistTypes || isExistChildren || isExistLastChildren;
}

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  types: Yup.array().test('types-required', t('thisIsRequiredField'), typesRequired),
  classes: Yup.array().min(1, t('thisIsRequiredField')),
  deviceCategories: Yup.array().min(1, t('thisIsRequiredField')),
});
