import styled from '@emotion/styled';
import { Form } from 'formik';

import { DetailToolbar } from '@ui/index';

interface StyledFormProps {
  className?: string;
  children: JSX.Element[];
}

interface DetailToolbarProps {
  className?: string;
  mode?: string;
  onEdit?: () => string;
  onClose?: () => boolean;
  onCancel?: () => void;
  onDelete?: () => boolean;
}

export const StyledForm = styled(({ className, children }: StyledFormProps) => (
  <Form className={className}>{children}</Form>
))`
  height: calc(100% - 102px);
  position: relative;
`;

export const StyledContent = styled.div(() => ({
  height: 'calc(100% - 32px)',
  padding: '0px 0px 24px 24px',
}));

export const StyledFormContent = styled.div(() => ({
  paddingRight: 24,
}));

export const StyledField = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'repeat(2, 1fr)',
  '@media (max-width: 1100px)': {
    gridTemplateColumns: 'repeat(1, 1fr)',
  },
  gap: 15,
}));

export const StyledDetailToolbar = styled(
  ({ className, ...props }: DetailToolbarProps) => (
    <DetailToolbar className={className} {...props} />
  )
)`
  padding: 24px;
`;

export const StyledInfoTextWrapper = styled.div(() => ({
  marginBottom: 20,
}));
