import { FC } from 'react';
import { useUnit } from 'effector-react';
import { ListItemButton, ListItemText, Divider } from '@mui/material';

import { Greedy, CloseButton, CustomScrollbar } from '@ui/index';

import { $employees, changedDetailVisibility } from '../../models';
import { styles } from './styles';
import { StyledToolbarWrapper, StyledList } from './styled';

export const DetailEmployeesList: FC = () => {
  const employees = useUnit($employees);

  const onClose = () => changedDetailVisibility(false);

  return (
    <>
      <StyledToolbarWrapper>
        <Greedy />
        <CloseButton style={styles.closeButton} onClick={onClose} />
      </StyledToolbarWrapper>
      {employees.length > 0 && (
        <CustomScrollbar style={styles.scrollbar}>
          <StyledList>
            {employees.map((employee) => {
              const onClick = () =>
                window.open(`${window.location.origin}/users/${employee.id}`);

              return (
                <div key={employee.id}>
                  <ListItemButton onClick={onClick}>
                    <ListItemText primary={employee.title} />
                  </ListItemButton>
                  <Divider />
                </div>
              );
            })}
          </StyledList>
        </CustomScrollbar>
      )}
    </>
  );
};
