import styled from '@emotion/styled';
import { List } from '@mui/material';

interface ListProps {
  children: JSX.Element[];
}

export const StyledToolbarWrapper = styled.div(() => ({
  display: 'flex',
  padding: '24px 24px 10px',
}));

export const StyledList = styled(({ children, ...props }: ListProps) => (
  <List {...props}>{children}</List>
))(() => ({
  margin: '0px 24px 0px',
}));
