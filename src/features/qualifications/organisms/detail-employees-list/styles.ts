export const styles = {
  closeButton: {
    margin: '4px 5px 0px',
  },
  scrollbar: {
    height: 'calc(100% - 118px)',
  },
};
