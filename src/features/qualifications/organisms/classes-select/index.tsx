import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, fxGetList, $isLoading } from '../../models/classes-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const ClassesSelect: FC<object> = (props) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const ClassesSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ClassesSelect />} onChange={onChange} {...props} />;
};
