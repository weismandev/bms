import { FC, useState, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { FieldArray, Field } from 'formik';
import { Collapse } from '@mui/material';
import { IndeterminateCheckBox } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { SwitchField } from '@ui/index';

import { ExpandCollapseButton } from '../../atoms';
import { TicketTypeElement } from '../../molecules';
import { Opened } from '../../interfaces';
import { $mode } from '../../models';
import {
  StyledTicketTypesTreeWrapper,
  StyledTypographyError,
  StyledWrapper,
  StyledTypes,
} from './styled';

interface Props {
  values: Opened;
  setFieldValue: (name: string, value: any) => void;
}

export const TicketTypesTree: FC<Props> = ({ values, setFieldValue }) => {
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);
  const { t } = useTranslation();

  const countTypesChecked = values.types.reduce((acc: number, item) => {
    if (Array.isArray(item?.children) && item.children.length > 0) {
      const children = item.children.reduce((childrenAcc: number, i) => {
        if (Array.isArray(i?.children) && i.children.length > 0) {
          const lastChildren = i.children.reduce((childAcc: number, child) => {
            if (child.value) {
              return childAcc + 1;
            }

            return childAcc;
          }, 0);

          return childrenAcc + lastChildren;
        }

        if (i.value) {
          return childrenAcc + 1;
        }

        return childrenAcc;
      }, 0);

      return acc + children;
    }

    if (item.value) {
      return acc + 1;
    }

    return acc;
  }, 0);

  const countTypes = values.types.reduce((typeAcc: number, type) => {
    if (Array.isArray(type?.children) && type.children.length > 0) {
      const childrens = type.children.reduce((typeChildAcc: number, child) => {
        if (Array.isArray(child?.children) && child.children.length > 0) {
          return typeChildAcc + child.children.length;
        }

        return typeChildAcc + 1;
      }, 0);

      return typeAcc + childrens;
    }

    return typeAcc + 1;
  }, 0);

  const indeterminate = countTypesChecked > 0 && countTypesChecked < countTypes;

  const onChange = ({ target: { checked } }: { target: { checked: boolean } }) => {
    setFieldValue('isAllTypes', checked);

    values.types.forEach((type, index) => {
      setFieldValue(`types[${index}].value`, checked);

      if (Array.isArray(type?.children) && type.children.length > 0) {
        type.children.forEach((item, childrenIndex) => {
          setFieldValue(`types[${index}].children[${childrenIndex}].value`, checked);

          if (Array.isArray(item?.children) && item.children.length > 0) {
            item.children.forEach((_, idx) => {
              setFieldValue(
                `types[${index}].children[${childrenIndex}].children[${idx}].value`,
                checked
              );
            });
          }
        });
      }
    });
  };

  const handleExpanded = () => setExpanded(!expanded);

  useEffect(() => {
    values.types.forEach((type) => {
      if (Array.isArray(type?.children) && type.children.length > 0) {
        if (type.children.every((item) => item.value)) {
          setFieldValue('isAllTypes', true);
        }
        if (type.children.every((item) => !item.value)) {
          setFieldValue('isAllTypes', false);
        }
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indeterminate, values.types]);

  if (values.types.length === 0) {
    return null;
  }

  return (
    <StyledTicketTypesTreeWrapper>
      <StyledWrapper>
        <ExpandCollapseButton expanded={expanded} handleExpanded={handleExpanded} />
        <Field
          component={SwitchField}
          name="isAllTypes"
          label={t('AllTypesOfRequests')}
          labelPlacement="end"
          divider={false}
          disabled={mode === 'view'}
          onChange={onChange}
          indeterminateIcon={<IndeterminateCheckBox />}
          indeterminate={indeterminate}
        />
      </StyledWrapper>
      <StyledTypes>
        <Collapse in={expanded} timeout="auto">
          <FieldArray
            name="types"
            render={({ form: { errors } }) => (
              <>
                {mode === 'edit' && errors?.types && (
                  <StyledTypographyError>{errors.types as string}</StyledTypographyError>
                )}
                {values.types.map((type, index) => (
                  <TicketTypeElement
                    key={type.id}
                    type={type}
                    index={index}
                    setFieldValue={setFieldValue}
                  />
                ))}
              </>
            )}
          />
        </Collapse>
      </StyledTypes>
    </StyledTicketTypesTreeWrapper>
  );
};
