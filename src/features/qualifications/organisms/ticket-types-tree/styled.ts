import styled from '@emotion/styled';

import { Typography } from '@mui/material';

export const StyledTicketTypesTreeWrapper = styled.div(() => ({
  marginBottom: 20,
}));

export const StyledTypographyError = styled(Typography)`
  font-weight: 400;
  font-size: 13px;
  color: #d32f2f;
`;

export const StyledWrapper = styled.div(() => ({
  display: 'flex',
}));

export const StyledTypes = styled.div(() => ({
  marginLeft: 33,
}));
