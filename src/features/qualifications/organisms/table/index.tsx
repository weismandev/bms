import { memo, FC } from 'react';
import { useUnit } from 'effector-react';

import { DataGrid } from '@features/data-grid';

import {
  $tableData,
  $isLoadingTable,
  $selectRow,
  selectionRowChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  openViaUrl,
} from '../../models';
import { CustomToolbar } from '../../molecules';
import { useStyles } from './styles';
import { StyledWrapper } from './styled';

export const Table: FC = memo(() => {
  const [
    tableData,
    isLoading,
    selectRow,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
  ] = useUnit([
    $tableData,
    $isLoadingTable,
    $selectRow,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
  ]);
  const classes = useStyles();

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    visibilityColumns,
    visibilityColumnsChanged,
    open: openViaUrl,
    isLoading,
    selectionRowChanged,
    selectRow,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <StyledWrapper>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </StyledWrapper>
  );
});
