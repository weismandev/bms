import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetQualificationsPayload, GetQualificationsResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetQualificationsResponse>({
  qualifications: [],
  meta: { total: 0 },
});
export const $isLoadingTable = createStore<boolean>(false);

export const fxGetQualifications = createEffect<
  GetQualificationsPayload,
  GetQualificationsResponse,
  Error
>();
