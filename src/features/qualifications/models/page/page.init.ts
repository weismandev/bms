import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';

import { signout } from '@features/common';

import { qualificationsApi } from '../../api';
import {
  $isErrorDialogOpen,
  $error,
  $isLoading,
  pageUnmounted,
  changedErrorDialogVisibility,
  pageMounted,
  fxGetQualifications,
  $rawData,
  $isLoadingTable,
} from './page.model';
import { $tableParams } from '../table/table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  fxGetQualification,
  fxGetTicketTypes,
  fxCreateQualification,
  fxUpdateQualification,
  fxDeleteQualification,
} from '../detail/detail.model';

fxGetQualifications.use(qualificationsApi.getQualifications);
fxGetQualification.use(qualificationsApi.getQualification);
fxGetTicketTypes.use(qualificationsApi.getTypes);
fxCreateQualification.use(qualificationsApi.createQualification);
fxUpdateQualification.use(qualificationsApi.updateQualification);
fxDeleteQualification.use(qualificationsApi.deleteQualification);

const errorOccured = merge([
  fxGetQualifications.fail,
  fxGetQualification.fail,
  fxGetTicketTypes.fail,
  fxCreateQualification.fail,
  fxUpdateQualification.fail,
  fxDeleteQualification.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetQualification,
        fxGetTicketTypes,
        fxCreateQualification,
        fxUpdateQualification,
        fxDeleteQualification,
      ],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

// во время получения пользовательских данных отображается лоадер
sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

// запрос списка квалификаций
sample({
  clock: [
    pageMounted,
    $tableParams,
    $settingsInitialized,
    fxCreateQualification.done,
    fxUpdateQualification.done,
    fxDeleteQualification.done,
  ],
  source: { table: $tableParams, settingsInitialized: $settingsInitialized },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table }) => ({
    page: table.page,
    per_page: table.per_page,
    search: table.search,
  }),
  target: fxGetQualifications,
});

$rawData
  .on(fxGetQualifications.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetQualifications.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);
