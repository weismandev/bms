import { signout } from '@features/common';

import { qualificationsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './device-categories-select.model';

fxGetList.use(qualificationsApi.getDeviceCategories);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.['device-catigories'])) {
      return [];
    }

    return result['device-catigories'].map((item) => ({
      id: Number.parseInt(item.id, 10),
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
