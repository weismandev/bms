import { createEffect, createStore } from 'effector';

import { GetDeviceCategoriesResponse, DeviceCategoryItem } from '../../interfaces';

export const fxGetList = createEffect<void, GetDeviceCategoriesResponse, Error>();

export const $data = createStore<DeviceCategoryItem[]>([]);
export const $isLoading = createStore<boolean>(false);
