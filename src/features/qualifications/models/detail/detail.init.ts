import { sample } from 'effector';
import { delay } from 'patronum';

import { signout, historyPush } from '@features/common';

import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  openViaUrl,
  $isDetailOpen,
  $entityId,
  changedDetailVisibility,
  $path,
  fxGetQualification,
  $currentTab,
  changeTab,
  addClicked,
  $opened,
  $types,
  fxGetTicketTypes,
  entityApi,
  fxCreateQualification,
  $mode,
  fxUpdateQualification,
  fxDeleteQualification,
  deleteQualification,
  $employees,
} from './detail.model';
import { formatTypes } from '../../libs';
import { Opened } from '../../interfaces';

$isDetailOpen
  .on(fxGetQualification.done, () => true)
  .off(openViaUrl)
  .reset([signout, pageUnmounted, fxDeleteQualification.done]);

// при маунте, если в url есть id,то запрашивается квалификация по id
sample({
  clock: pageMounted,
  source: $entityId,
  filter: (id) => id && id.length > 0,
  fn: (id) => ({
    id,
  }),
  target: fxGetQualification,
});

// если запрос на получение квалификацию падает, то из url убирается id
sample({
  clock: delay({ source: fxGetQualification.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// при закрытии карточки из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);

// при клике на строку в таблице отправляется запрос на получение квалификации по id
sample({
  clock: openViaUrl,
  fn: (id) => ({ id }),
  target: fxGetQualification,
});

$currentTab.on(changeTab, (_, tab) => tab).reset([pageUnmounted, signout, addClicked]);

$opened
  .on(fxGetQualification.done, (_, { result: { qualification } }) => ({
    id: qualification?.id,
    title: qualification?.title || '-',
    isAllTypes: false,
    typesId: Array.isArray(qualification?.ticket_types)
      ? qualification.ticket_types.map((type) => type.id)
      : [],
    types: [],
    classes: Array.isArray(qualification?.ticket_classes)
      ? qualification.ticket_classes.map((item) => ({ id: item.id, title: item.title }))
      : [],
    deviceCategories: Array.isArray(qualification?.device_categories)
      ? qualification.device_categories.map((i) => ({ id: i.id, title: i.title }))
      : [],
  }))
  .reset([pageUnmounted, signout, fxGetQualification]);

$types
  .on(fxGetTicketTypes.done, (_, { result }) => {
    if (!Array.isArray(result?.types)) {
      return [];
    }

    const types = result.types.map((type) => ({
      id: type.id,
      title: type.title,
      parentId: type.parent_id,
      children: type.children,
      level: type.level,
      value: false,
    }));

    return formatTypes(types, 0, [...types]);
  })
  .reset([pageUnmounted, signout]);

// если открыты детали и нет типов заявок, то отправляется запрос
sample({
  source: { isDetailOpen: $isDetailOpen, types: $types },
  filter: ({ isDetailOpen, types }) =>
    isDetailOpen && Array.isArray(types) && types.length === 0,
  target: fxGetTicketTypes,
});

const formatData = (data: Opened) => {
  const types = data.types.reduce((acc: number[], type) => {
    if (!type.value) {
      return acc;
    }

    return [...acc, type.id];
  }, []);

  const childrenTypes = data.types
    .map((children) => {
      if (Array.isArray(children?.children)) {
        return children.children.reduce((acc: number[], item) => {
          if (!item.value) {
            return acc;
          }

          return [...acc, item.id];
        }, []);
      }

      return [];
    })
    .flat();

  const lastChildrenTypes = data.types
    .map((element) => {
      if (Array.isArray(element?.children)) {
        return element.children
          .map((item) => {
            if (Array.isArray(item?.children)) {
              return item.children.reduce((acc: number[], i) => {
                if (!i.value) {
                  return acc;
                }

                return [...acc, i.id];
              }, []);
            }

            return [];
          })
          .flat();
      }

      return [];
    })
    .flat();

  return {
    title: data.title,
    ticket_type_ids: [...types, ...childrenTypes, ...lastChildrenTypes],
    device_category_ids: data.deviceCategories.map((item) => item.id),
    ticket_class_ids: data.classes.map((i) => i.id),
  };
};

// создание квалификации
sample({
  clock: entityApi.create,
  fn: (data) => formatData(data),
  target: fxCreateQualification,
});

$mode.reset([
  pageUnmounted,
  signout,
  fxCreateQualification.done,
  fxUpdateQualification.done,
]);

// после создания квалификации отправляется запрос на получение квалификации
sample({
  clock: fxCreateQualification.done,
  fn: ({
    result: {
      qualification: { id },
    },
  }) => ({ id }),
  target: fxGetQualification,
});

// после создания квалификации в url добавляется id
sample({
  clock: fxCreateQualification.done,
  source: $path,
  filter: (
    _,
    {
      result: {
        qualification: { id },
      },
    }
  ) => Boolean(id),
  fn: (
    path,
    {
      result: {
        qualification: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

// после редактирования запрашивается квалификация
sample({
  clock: entityApi.update,
  fn: (data) => ({ id: data.id, ...formatData(data) }),
  target: fxUpdateQualification,
});

// после редактирования квалификации отправляется запрос на получение квалификации
sample({
  clock: fxUpdateQualification.done,
  fn: ({
    result: {
      qualification: { id },
    },
  }) => ({ id }),
  target: fxGetQualification,
});

// удаление квалификации
sample({
  clock: deleteQualification,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxDeleteQualification,
});

// после удаления убирается id из url
sample({
  clock: fxDeleteQualification.done,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

$employees
  .on(fxGetQualification.done, (_, { result: { qualification } }) => {
    if (!Array.isArray(qualification?.employees)) {
      return [];
    }

    return qualification.employees.map((item) => ({
      id: item.id,
      title: item.full_name,
    }));
  })
  .reset([pageUnmounted, signout, fxGetQualification]);
