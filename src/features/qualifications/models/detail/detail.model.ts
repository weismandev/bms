import { createEffect, createStore, createEvent, combine } from 'effector';

import { $pathname } from '@features/common';
import { createDetailBag } from '@tools/factories';
import i18n from '@shared/config/i18n';
import { TypesResponse } from '@features/tickets-new-type-select';

import {
  GetQualificationPayload,
  GetQualificationResponse,
  Opened,
  FormattedTypes,
  CreateUpdateQualificationPayload,
  DeleteQualificationPayload,
  Value,
} from '../../interfaces';

const { t } = i18n;

const newEntity = {
  id: null,
  title: '',
  isAllTypes: false,
  typesId: [],
  types: [],
  classes: [],
  deviceCategories: [],
};

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'employees',
    onCreateIsDisabled: true,
    label: t('Employees'),
  },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const changeTab = createEvent<string>();
export const deleteQualification = createEvent<void>();

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map(
  (pathname: string) => pathname.split('/')[2] || null
);
export const $currentTab = createStore<string>('info');
export const $types = createStore<FormattedTypes[]>([]);
export const $employees = createStore<Value[]>([]);
export const $formattedOpened = combine($opened, $types, (opened: Opened, types) => {
  if (!Array.isArray(types)) {
    return opened;
  }

  const newOpened = { ...opened };

  const formattedTypes = types.map((item) => {
    const newItem = { ...item };

    const isExistType = newOpened.typesId.find((type) => newItem.id === type);

    newItem.value = Boolean(isExistType);

    if (Array.isArray(newItem?.children)) {
      const children = newItem.children.map((child) => {
        const newChild = { ...child };

        const isExistChildrenType = newOpened.typesId.find(
          (type) => newChild.id === type
        );

        newChild.value = Boolean(isExistChildrenType);

        if (Array.isArray(newChild?.children)) {
          const lastChildren = newChild.children.map((i) => {
            const newI = { ...i };

            const isExistLastChildrenType = newOpened.typesId.find(
              (type) => newI.id === type
            );

            newI.value = Boolean(isExistLastChildrenType);

            return newI;
          });

          newChild.children = lastChildren;
        }

        return newChild;
      });

      newItem.children = children;
    }

    return newItem;
  });

  const isAllTypes = formattedTypes.every((type) => type.value);

  newOpened.isAllTypes = isAllTypes;
  newOpened.types = formattedTypes;

  return newOpened;
});

export const fxGetQualification = createEffect<
  GetQualificationPayload,
  GetQualificationResponse,
  Error
>();
export const fxGetTicketTypes = createEffect<void, TypesResponse, Error>();
export const fxCreateQualification = createEffect<
  CreateUpdateQualificationPayload,
  GetQualificationResponse,
  Error
>();
export const fxUpdateQualification = createEffect<
  CreateUpdateQualificationPayload,
  GetQualificationResponse,
  Error
>();
export const fxDeleteQualification = createEffect<
  DeleteQualificationPayload,
  object,
  Error
>();
