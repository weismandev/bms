import { signout } from '@features/common';

import { qualificationsApi } from '../../api';
import { fxGetList, $data, $isLoading } from './classes-select.model';

fxGetList.use(qualificationsApi.getClassList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.classes)) {
      return [];
    }

    return result.classes.map((item) => ({ id: item.id, title: item.title }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
