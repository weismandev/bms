import { createEffect, createStore } from 'effector';

import { ClassesResponse, ClassesData } from '../../interfaces';

export const fxGetList = createEffect<void, ClassesResponse, Error>();

export const $data = createStore<ClassesData[]>([]);
export const $isLoading = createStore<boolean>(false);
