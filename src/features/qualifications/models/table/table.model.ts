import { combine } from 'effector';

import i18n from '@shared/config/i18n';
import { createTableBag, Columns } from '@features/data-grid';

import { Qualification } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns = [
  {
    field: 'id',
    headerName: '№',
    width: 100,
    sortable: false,
  },
  {
    field: 'title',
    headerName: t('TitleOfQualification'),
    width: 250,
    sortable: false,
  },
];

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} = createTableBag({
  columns,
  pageSize: 25,
});

const formatData = (qualifications: Qualification[]) =>
  qualifications.map((qualification) => ({
    id: qualification.id,
    title: qualification?.title || '-',
  }));

export const $savedColumns = $columns.map((currentColumn: Columns[]) =>
  currentColumn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
export const $count = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ qualifications }) =>
  Array.isArray(qualifications) && qualifications.length > 0
    ? formatData(qualifications)
    : []
);
export const $countPage = combine($count, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);
