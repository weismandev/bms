import { sample } from 'effector';

import { Columns } from '@features/data-grid';
import { signout } from '@features/common';

import { pageMounted, pageUnmounted } from '../page/page.model';
import { fxGetFromStorage } from '../user-settings/user-settings.model';
import {
  $entityId,
  fxCreateQualification,
  detailClosed,
  addClicked,
} from '../detail/detail.model';
import { $columns, $savedColumns, $selectRow } from './table.model';

// если при маунте в url есть id, то выбирается строка в таблице
sample({
  clock: pageMounted,
  source: { id: $entityId, selectRow: $selectRow },
  filter: ({ selectRow, id }) =>
    Boolean(id) && Array.isArray(selectRow) && selectRow.length === 0,
  fn: ({ id }) => [Number.parseInt(id as string, 10)],
  target: $selectRow,
});

// сохраненные столбцы дополняются необходимыми данными
sample({
  clock: fxGetFromStorage.done,
  source: { columns: $columns, savedColumns: $savedColumns },
  fn: ({ columns, savedColumns }) => {
    const formattedColumns = savedColumns.reduce((acc: Columns[], column) => {
      const findedColumn = columns.find((item) => item.field === column.field);

      return findedColumn
        ? [
            ...acc,
            {
              field: column.field,
              headerName: findedColumn.headerName,
              width: column.width,
              sortable: findedColumn.sortable,
            },
          ]
        : acc;
    }, []);

    const newColumns = columns.filter(
      (item) => !formattedColumns.some((element: Columns) => element.field === item.field)
    );

    newColumns.forEach((item) => {
      const index = columns.map((column) => column.field).indexOf(item.field);

      formattedColumns.splice(index, 0, item);
    });

    return formattedColumns;
  },
  target: $columns,
});

$selectRow
  .on(fxCreateQualification.done, (state, { params, result }) =>
    !params.id && result.qualification.id ? [result.qualification.id] : state
  )
  .reset([signout, pageUnmounted, detailClosed, addClicked]);
