import { signout } from '@features/common';

import { pageUnmounted } from '../page/page.model';
import { deleteQualification } from '../detail/detail.model';
import { $isOpenDeleteModal, changedVisibilityDeleteModal } from './delete-modal.model';

$isOpenDeleteModal
  .on(changedVisibilityDeleteModal, (_, visibility) => visibility)
  .reset([signout, pageUnmounted, deleteQualification]);
