import { FC } from 'react';
import { IconButton } from '@mui/material';
import { ExpandMore, ExpandLess } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

interface Props {
  expanded: boolean;
  handleExpanded: () => void;
}

export const ExpandCollapseButton: FC<Props> = ({ expanded, handleExpanded }) => {
  const { t } = useTranslation();

  return (
    <IconButton
      onClick={handleExpanded}
      size="large"
      sx={{ width: 25, height: 25, marginTop: 1 }}
    >
      {expanded ? (
        <ExpandMore titleAccess={t('Collapse')} />
      ) : (
        <ExpandLess titleAccess={t('Expand')} />
      )}
    </IconButton>
  );
};
