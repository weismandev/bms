import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const StyledWrapper = styled.div(() => ({
  display: 'flex',
  wordBreak: 'break-word',
  background: '#E9F4FE',
  padding: 20,
  borderRadius: 16,
}));

export const StyledTypography = styled(Typography)`
  font-weight: 500;
  font-size: 14px;
  color: #334852;
  margin: 2px 0px 0px 15px;
`;
