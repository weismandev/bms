import { FC } from 'react';
import { ErrorOutlineOutlined } from '@mui/icons-material';

import { StyledWrapper, StyledTypography } from './styled';

interface Props {
  text: string;
}

export const InfoText: FC<Props> = ({ text }) => (
  <StyledWrapper>
    <ErrorOutlineOutlined color="primary" />
    <StyledTypography>{text}</StyledTypography>
  </StyledWrapper>
);
