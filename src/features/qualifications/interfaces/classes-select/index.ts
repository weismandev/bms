export interface ClassesResponse {
  classes: {
    id: number;
    name: string;
    title: string;
  }[];
}

export interface ClassesData {
  id: number;
  title: string;
}
