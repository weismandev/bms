export interface Qualification {
  id: number;
  title: string;
  ticket_types: TicketType[];
  ticket_classes: TicketClass[];
  employees: {
    full_name: string;
    id: number;
    name: string;
    patronymic: string;
    surname: string;
  }[];
  device_categories: DeviceCategory[];
}

export interface TicketType {
  archived: boolean;
  children: number[];
  id: number;
  is_readonly: boolean;
  is_show_in_mobile: boolean;
  level: number;
  parent_id: number;
  planned_duration_minutes: number | null;
  title: string;
}

export interface TicketClass {
  id: number;
  name: string;
  title: string;
}

export interface DeviceCategory {
  id: number;
  name: string;
  title: string;
  parent_id: number | null;
}

export interface GetQualificationPayload {
  id: number;
}

export interface GetQualificationResponse {
  qualification: Qualification;
}

export interface Value {
  id: number;
  title: string;
}

export interface Opened {
  id: number | null;
  title: string;
  isAllTypes: boolean;
  types: FormattedTypes[];
  typesId: number[];
  classes: Value[];
  deviceCategories: Value[];
}

export interface FormattedTypes {
  id: number;
  title: string;
  value: boolean;
  parentId: number | null;
  level: number;
  children: {
    id: number;
    title: string;
    value: boolean;
    parentId: number;
    level: number;
    children: {
      id: number;
      title: string;
      value: boolean;
      parentId: null;
      level: number;
      children: [];
    }[];
  }[];
}

export interface CreateUpdateQualificationPayload {
  id?: number;
  title: string;
  ticket_type_ids: number[];
  device_category_ids: number[];
  ticket_class_ids: number[];
}

export interface DeleteQualificationPayload {
  id: number;
}
