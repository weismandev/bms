export interface GetDeviceCategoriesResponse {
  'device-catigories': {
    id: string;
    name: string;
    title: string;
    parent_id: null;
    children: [];
  }[];
}

export interface DeviceCategoryItem {
  id: number;
  title: string;
}
