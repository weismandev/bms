import { Qualification } from '../qualification';

export interface GetQualificationsPayload {
  per_page?: number;
  page?: number;
  search?: string;
}

export interface GetQualificationsResponse {
  meta: {
    total: number;
  };
  qualifications: Qualification[];
}
