import { FC, useState, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { IndeterminateCheckBox } from '@mui/icons-material';

import { SwitchField } from '@ui/index';

import { $mode } from '../../models';
import { FormattedTypes } from '../../interfaces';
import { TicketTypeChildrenElement } from '../ticket-type-children-element';
import { ExpandCollapseButton } from '../../atoms';
import { StyledWrapper } from './styled';

interface Props {
  type: FormattedTypes;
  index: number;
  setFieldValue: (name: string, value: any) => void;
}

export const TicketTypeElement: FC<Props> = ({ type, index, setFieldValue }) => {
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);

  const isExistChildren = Array.isArray(type?.children) && type.children.length > 0;
  const fieldName = `types[${index}].value`;

  const countTypesChecked = Array.isArray(type?.children)
    ? type.children.reduce((acc: number, item) => {
        if (Array.isArray(item?.children) && item.children.length > 0) {
          const childrenCount = item.children.reduce(
            (childAcc: number, value) => (value.value ? childAcc + 1 : childAcc),
            0
          );

          return acc + childrenCount;
        }

        if (item.value) {
          return acc + 1;
        }

        return acc;
      }, 0)
    : 0;

  const countElements = Array.isArray(type?.children)
    ? type.children.reduce((acc, item) => {
        if (Array.isArray(item?.children) && item.children.length > 0) {
          return acc + item.children.length;
        }

        return acc + 1;
      }, 0)
    : 0;

  const indeterminate = countTypesChecked > 0 && countTypesChecked < countElements;

  const handleExpanded = () => setExpanded(!expanded);

  const onChange = ({ target: { checked } }: { target: { checked: boolean } }) => {
    setFieldValue(fieldName, checked);

    if (Array.isArray(type?.children) && type.children.length > 0) {
      type.children.forEach((child, childIndex) => {
        setFieldValue(`types[${index}].children[${childIndex}].value`, checked);

        if (Array.isArray(child?.children) && child.children.length > 0) {
          child.children.forEach((_, idx) => {
            setFieldValue(
              `types[${index}].children[${childIndex}].children[${idx}].value`,
              checked
            );
          });
        }
      });
    }
  };

  useEffect(() => {
    if (Array.isArray(type?.children) && type.children.length > 0) {
      if (type.children.every((item) => item.value)) {
        setFieldValue(`${fieldName}`, true);
      }

      if (type.children.every((item) => !item.value)) {
        setFieldValue(`${fieldName}`, false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indeterminate, type.children]);

  return (
    <>
      <StyledWrapper isExistChildren={isExistChildren}>
        {isExistChildren && (
          <ExpandCollapseButton expanded={expanded} handleExpanded={handleExpanded} />
        )}
        <Field
          component={SwitchField}
          name={fieldName}
          label={type.title}
          labelPlacement="end"
          divider={false}
          disabled={mode === 'view'}
          onChange={onChange}
          indeterminateIcon={<IndeterminateCheckBox />}
          indeterminate={indeterminate}
        />
      </StyledWrapper>
      {isExistChildren &&
        type.children.map((item, childrenIndex) => {
          const name = `types[${index}].children[${childrenIndex}]`;

          return (
            <Collapse in={expanded} timeout="auto" key={item.id}>
              <TicketTypeChildrenElement
                childrenType={item}
                name={name}
                setFieldValue={setFieldValue}
              />
            </Collapse>
          );
        })}
    </>
  );
};
