import styled from '@emotion/styled';

export const StyledWrapper = styled.div(({ isExistChildren }: any) => ({
  display: 'flex',
  marginLeft: !isExistChildren ? -7 : 0,
}));
