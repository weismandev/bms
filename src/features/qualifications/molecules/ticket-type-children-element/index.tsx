import { FC, useState, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { IndeterminateCheckBox } from '@mui/icons-material';

import { SwitchField } from '@ui/index';

import { $mode } from '../../models';
import { ExpandCollapseButton } from '../../atoms';
import { StyledWrapper, StyledChildrenWrapper } from './styled';

interface Props {
  childrenType: {
    id: number;
    title: string;
    value: boolean;
    parentId: number;
    level: number;
    children: {
      id: number;
      title: string;
      value: boolean;
      parentId: null;
      level: number;
      children: [];
    }[];
  };
  name: string;
  setFieldValue: (name: string, value: any) => void;
}

export const TicketTypeChildrenElement: FC<Props> = ({
  childrenType,
  name,
  setFieldValue,
}) => {
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);

  const isExistChildren =
    Array.isArray(childrenType?.children) && childrenType.children.length > 0;
  const countTypesChecked = Array.isArray(childrenType?.children)
    ? childrenType.children.filter((element) => element.value).length
    : 0;
  const indeterminate = Array.isArray(childrenType?.children)
    ? countTypesChecked > 0 && countTypesChecked < childrenType.children.length
    : false;

  const handleExpanded = () => setExpanded(!expanded);

  const onChange = ({ target: { checked } }: { target: { checked: boolean } }) => {
    setFieldValue(`${name}.value`, checked);

    if (Array.isArray(childrenType?.children) && childrenType.children.length > 0) {
      childrenType.children.forEach((_, childIndex) => {
        setFieldValue(`${name}.children[${childIndex}].value`, checked);
      });
    }
  };

  useEffect(() => {
    if (Array.isArray(childrenType?.children) && childrenType.children.length > 0) {
      if (childrenType.children.every((item) => item.value)) {
        setFieldValue(`${name}.value`, true);
      }

      if (childrenType.children.every((item) => !item.value)) {
        setFieldValue(`${name}.value`, false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indeterminate]);

  return (
    <>
      <StyledWrapper isExistChildren={isExistChildren}>
        {isExistChildren && (
          <ExpandCollapseButton expanded={expanded} handleExpanded={handleExpanded} />
        )}
        <Field
          component={SwitchField}
          name={`${name}.value`}
          label={childrenType.title}
          labelPlacement="end"
          divider={false}
          disabled={mode === 'view'}
          onChange={onChange}
          indeterminateIcon={<IndeterminateCheckBox />}
          indeterminate={indeterminate}
        />
      </StyledWrapper>
      {isExistChildren && (
        <Collapse in={expanded} timeout="auto">
          {childrenType.children.map((item, index) => (
            <StyledChildrenWrapper key={item.id}>
              <div>
                <Field
                  component={SwitchField}
                  name={`${name}.children[${index}].value`}
                  label={item.title}
                  labelPlacement="end"
                  divider={false}
                  disabled={mode === 'view'}
                />
              </div>
            </StyledChildrenWrapper>
          ))}
        </Collapse>
      )}
    </>
  );
};
