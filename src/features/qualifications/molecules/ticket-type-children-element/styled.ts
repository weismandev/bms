import styled from '@emotion/styled';

export const StyledWrapper = styled.div(({ isExistChildren }: any) => ({
  display: 'flex',
  marginLeft: isExistChildren ? 54 : 48,
}));

export const StyledChildrenWrapper = styled.div(() => ({
  display: 'flex',
  marginLeft: 106,
}));
