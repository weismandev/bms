import styled from '@emotion/styled';

import { AddButton } from '@ui/index';

interface ButtonProps {
  disabled?: boolean;
  onClick?: () => void;
}

export const StyledAddButton = styled((props: ButtonProps) => <AddButton {...props} />)`
  margin: 0px 10px 0px 0px;
`;
