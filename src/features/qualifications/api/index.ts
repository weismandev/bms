import { api } from '@api/api2';

import {
  GetQualificationsPayload,
  GetQualificationPayload,
  CreateUpdateQualificationPayload,
  DeleteQualificationPayload,
} from '../interfaces';

const getQualifications = (payload: GetQualificationsPayload) =>
  api.v4('get', 'qualifications/list', payload);
const getQualification = (payload: GetQualificationPayload) =>
  api.v4('get', 'qualifications/view', payload);
const getTypes = () => api.v1('get', 'tck/types/list', { archived: 0 });
const createQualification = (payload: CreateUpdateQualificationPayload) =>
  api.v4('post', 'qualifications/create', payload);
const updateQualification = (payload: CreateUpdateQualificationPayload) =>
  api.v4('post', 'qualifications/update', payload);
const deleteQualification = (payload: DeleteQualificationPayload) =>
  api.v4('post', 'qualifications/delete', payload);
const getDeviceCategories = () =>
  api.no_vers(
    'post',
    'https://api.ujin.tech/api/device_categories/get-device-catigories',
    {
      tree: 1,
    }
  );
const getClassList = () => api.v4('get', 'ticket/classes/list');

export const qualificationsApi = {
  getQualifications,
  getQualification,
  getTypes,
  createQualification,
  updateQualification,
  deleteQualification,
  getDeviceCategories,
  getClassList,
};
