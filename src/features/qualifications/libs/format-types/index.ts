import { FormattedTypes } from '../../interfaces';

interface Types {
  id: number;
  title: string;
  parentId: number | null;
  children: number[];
  level: number;
}

const getTypeInfo = (id: number, types: Types[]) => types.find((type) => type.id === id);

export const formatTypes = (
  types: Types[],
  level: number,
  restTypes: Types[]
): FormattedTypes[] => {
  const iter = (
    iterTypes: Types[],
    iterLevel: number,
    iterRestTypes: Types[],
    acc: Types[]
  ): Types[] => {
    const firstLevel = iterTypes.filter((item) => {
      if (item.level === iterLevel) {
        const findedId = iterRestTypes.findIndex((element) => element.id === item.id);

        iterRestTypes.splice(findedId, 1);

        return true;
      }

      return false;
    });

    const formattedTypes = firstLevel.map((item) => {
      if (Array.isArray(item?.children) && item.children.length === 0) {
        return item;
      }

      const childrens = item.children
        .map((child: number) => getTypeInfo(child, iterTypes))
        .filter((childrenItem) => {
          if (childrenItem) {
            const findedId = restTypes.findIndex(
              (childrenElement) => childrenElement.id === childrenItem.id
            );

            restTypes.splice(findedId, 1);

            return true;
          }

          return false;
        });

      const nestedChildrens = (childrens as Types[]).map((childItem: Types) => {
        if (Array.isArray(childItem?.children) && childItem.children.length === 0) {
          return childItem;
        }

        const childChildrens = childItem.children
          .map((child: number) => getTypeInfo(child, iterTypes))
          .filter((i) => {
            if (i) {
              const findedId = restTypes.findIndex(
                (childChildrenItem) => childChildrenItem.id === i.id
              );

              restTypes.splice(findedId, 1);

              return true;
            }

            return false;
          });

        childItem.children = childChildrens;

        return childItem;
      });

      item.children = nestedChildrens;

      return item;
    });

    if (restTypes.length === 0) {
      return [...acc, ...formattedTypes];
    }

    return iter(
      iterRestTypes,
      level + 1,
      [...iterRestTypes],
      [...acc, ...formattedTypes]
    );
  };

  return iter(types, level, restTypes, []);
};
