import { createEffect, createStore, createEvent } from 'effector';
import { signout } from '../../common';
import { servicesApi } from '../api';

const fxGetList = createEffect();

const setServicesItems = createEvent();

const $services = createStore([]);
const $itemsBySelectedServices = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$services.on(fxGetList.done, (_, { result: { services } }) => services).reset(signout);

$itemsBySelectedServices.on(setServicesItems, (_, items) => items).reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

fxGetList.use(servicesApi.getList);

export {
  fxGetList,
  $services,
  $error,
  $isLoading,
  $itemsBySelectedServices,
  setServicesItems,
};
