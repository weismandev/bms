import { api } from '../../../api/api2';

const getList = (payload = {}, config = {}) =>
  api.no_vers('get', '/v1/enterprises/services/list', payload);

export const servicesApi = { getList };
