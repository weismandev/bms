import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $services, $isLoading, $error, fxGetList, setServicesItems } from '../../model';

const ServicesSelect = (props) => {
  const options = useStore($services);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const ServicesSelectField = (props) => {
  const onChange = (value) => {
    const items = value
      .filter((item) => item.item_group && item.item_group.items)
      .flatMap(({ id, item_group: { items } }) => {
        return items.map((item) => ({ ...item, parentId: id }));
      });
    setServicesItems(items);
    props.form.setFieldValue(props.field.name, value);
  };

  return <SelectField component={<ServicesSelect />} onChange={onChange} {...props} />;
};

export { ServicesSelect, ServicesSelectField };
