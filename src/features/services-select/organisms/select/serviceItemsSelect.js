import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $itemsBySelectedServices } from '../../model';

const ServiceItemsSelect = (props) => {
  const options = useStore($itemsBySelectedServices);

  return <SelectControl options={options} {...props} />;
};

const ServiceItemsSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<ServiceItemsSelect />} onChange={onChange} {...props} />
  );
};

export { ServiceItemsSelect, ServiceItemsSelectField };
