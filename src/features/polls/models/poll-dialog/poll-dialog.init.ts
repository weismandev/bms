import { signout } from '@features/common';
import { fxDeletePoll, fxPusblishPoll, fxFinishPoll } from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import {
  changeDialogVisibility,
  $isOpenDialog,
  $kind,
  changeKind,
} from './poll-dialog.model';

$isOpenDialog
  .on(changeDialogVisibility, (_, visibility) => visibility)
  .reset([
    signout,
    pageUnmounted,
    fxDeletePoll.done,
    fxPusblishPoll.done,
    fxFinishPoll.done,
  ]);

$kind
  .on(changeKind, (_, kind) => kind)
  .reset([
    signout,
    pageUnmounted,
    fxDeletePoll.done,
    fxPusblishPoll.done,
    fxFinishPoll.done,
  ]);
