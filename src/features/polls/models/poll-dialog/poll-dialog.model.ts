import { createStore, createEvent } from 'effector';

export const changeDialogVisibility = createEvent<boolean>();
export const changeKind = createEvent<string>();

export const $isOpenDialog = createStore<boolean>(false);
export const $kind = createStore<string>('');
