import i18n from '@shared/config/i18n';
import { DayStatistic } from '../../interfaces';
import { $dailyStatistics } from '../detail/detail.model';

const { t } = i18n;

export const $columnsDailyStatistics = $dailyStatistics.map((data) =>
  data && data.length > 0 ? formatColumns(data) : []
);

const formatColumns = (data: DayStatistic[]) => {
  const column = {
    field: 'id',
    headerName: t('Label.date'),
    width: 120,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  };

  const otherColumns = data.map(({ date }, index) => ({
    field: `date_${index}`,
    headerName: date,
    width: 120,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  }));

  return [column, ...otherColumns];
};

export const $tableDailyStatisticsData = $dailyStatistics.map((data) =>
  data && data.length > 0 ? formatData(data) : []
);

const formatData = (data: DayStatistic[]) => {
  const firstElement = { id: t('Views') };

  const otherElements = data.reduce(
    (acc: { [key: string]: number }, { viewed_count }, index) => ({
      ...{ [`date_${index}`]: viewed_count },
      ...acc,
    }),
    {}
  );

  return [{ ...firstElement, ...otherElements }];
};
