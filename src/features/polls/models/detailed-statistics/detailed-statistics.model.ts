import i18n from '@shared/config/i18n';
import { Resident } from '../../interfaces';
import { $detailedStatistics } from '../detail/detail.model';

const { t } = i18n;

export const columnsDetailedStatistics = [
  {
    field: 'id',
    headerName: 'id',
    width: 120,
    headerClassName: 'super-app-theme--header',
    sortable: false,
    hide: true,
  },
  {
    field: 'fullname',
    headerName: t('Label.FullName'),
    width: 250,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
  {
    field: 'address',
    headerName: t('Label.address'),
    width: 350,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
  {
    field: 'status',
    headerName: t('Label.status'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
  {
    field: 'date',
    headerName: t('ViewingDate'),
    width: 120,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
];

export const $tableDetailedStatisticsData = $detailedStatistics.map((data) =>
  data && data.length > 0 ? formatData(data) : []
);

const formatData = (data: Resident[]) =>
  data.map((item) => ({
    id: item.id,
    fullname: item?.full_name || '',
    address: item?.apartment?.address || '',
    status: item?.status?.label || '',
    date: item?.status?.date || '',
  }));
