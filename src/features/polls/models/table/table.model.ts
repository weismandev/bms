import { combine } from 'effector';
import { format } from 'date-fns';

import i18n from '@shared/config/i18n';
import { createTableBag } from '@features/data-grid';

import { PollItem } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

const columns = [
  {
    field: 'id',
    headerName: '№',
    width: 120,
    headerClassName: 'super-app-theme--header',
    sortable: true,
  },
  {
    field: 'title',
    headerName: t('Poll'),
    width: 350,
    headerClassName: 'super-app-theme--header',
    sortable: true,
  },
  {
    field: 'status',
    headerName: t('Label.status'),
    width: 160,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
  {
    field: 'voted',
    headerName: t('Result'),
    width: 100,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
  {
    field: 'active_from',
    headerName: t('Start'),
    width: 110,
    headerClassName: 'super-app-theme--header',
    sortable: true,
  },
  {
    field: 'active_to',
    headerName: t('End'),
    width: 110,
    headerClassName: 'super-app-theme--header',
    sortable: true,
  },
  {
    field: 'buildings',
    headerName: t('Buildings'),
    width: 350,
    headerClassName: 'super-app-theme--header',
    sortable: false,
  },
];

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} = createTableBag({
  columns,
  pageSize: 25,
});

export const $savedColumns = $columns.map((columns) =>
  columns.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
export const $count = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ polls }) =>
  polls && polls.length > 0 ? formatData(polls) : []
);
export const $countPage = combine($count, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);

const formatData = (polls: PollItem[]) => {
  return polls.map((poll) => {
    const formattedBuildings =
      poll.buildings && poll.buildings.length > 0
        ? poll.buildings.map((building) => building.title)
        : '-';

    const voted =
      poll?.statistic?.voted_percent === 0
        ? '0%'
        : Boolean(poll?.statistic?.voted_percent)
        ? `${poll.statistic.voted_percent}%`
        : '-';

    const active_from = Boolean(poll.active_from)
      ? format(new Date(poll.active_from), 'dd.MM.yyyy')
      : '';
    const active_to = Boolean(poll.active_to)
      ? format(new Date(poll.active_to), 'dd.MM.yyyy')
      : '';

    return {
      id: poll.id,
      title: poll.title || '-',
      status: poll.status,
      voted,
      active_from,
      active_to,
      buildings: formattedBuildings,
    };
  });
};
