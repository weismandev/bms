import { createFilterBag } from '@tools/factories';

const defaultFilters = {
  published_at_from: '',
  published_at_to: '',
  buildings: [],
  statuses: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
