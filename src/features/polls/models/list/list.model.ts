import { $tableData } from '../table/table.model';

export const $list = $tableData.map((tableData) => {
  if (tableData && tableData.length > 0) {
    return tableData.map((item) => ({
      id: item.id,
      poll: item.title,
      status: item.status,
    }));
  }

  return [];
});
