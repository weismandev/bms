import { createEffect, createEvent, createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';
import {
  PollPayload,
  GetPollResponse,
  CreateUpdatePollPayload,
  GetStatisticsPayload,
  GetStatisticsResponse,
  GeneralStatistic,
  DayStatistic,
  Resident,
  ExportPollResults,
} from '../../interfaces';

const { t } = i18n;

export const tabs = [
  {
    value: 'settings',
    onCreateIsDisabled: false,
    label: t('ProfilePage.Settings'),
  },
  {
    value: 'statistics',
    onCreateIsDisabled: true,
    label: t('navMenu.statistics.title'),
  },
];

const newEntity = {
  title: '',
  active_to: '',
  buildings: [],
  status: { slug: 'draft', label: t('Draft') },
  question: { id: null, title: '' },
  question_option_1: { id: null, title: '' },
  question_option_2: { id: null, title: '' },
  question_options: [],
  is_multiple_choice: false,
};

export const changeTab = createEvent<string>();
export const deletePoll = createEvent<void>();
export const publishPoll = createEvent<void>();
export const finishPoll = createEvent<void>();
export const exportPollResults = createEvent<void>();

export const $currentTab = createStore<string>('settings');
export const $generalStatistic = createStore<GeneralStatistic>({
  count: 0,
  coverage: 0,
  voted: 0,
  notVoted: 0,
});
export const $dailyStatistics = createStore<DayStatistic[]>([]);
export const $detailedStatistics = createStore<Resident[]>([]);

export const fxGetPoll = createEffect<PollPayload, GetPollResponse, Error>();
export const fxCreatePoll = createEffect<
  CreateUpdatePollPayload,
  GetPollResponse,
  Error
>();
export const fxUpdatePoll = createEffect<
  CreateUpdatePollPayload,
  GetPollResponse,
  Error
>();
export const fxDeletePoll = createEffect<PollPayload, object, Error>();
export const fxPusblishPoll = createEffect<
  CreateUpdatePollPayload,
  GetPollResponse,
  Error
>();
export const fxFinishPoll = createEffect<PollPayload, object, Error>();
export const fxGetStatistics = createEffect<
  GetStatisticsPayload,
  GetStatisticsResponse,
  Error
>();
export const fxExportPollResults = createEffect<ExportPollResults, ArrayBuffer, Error>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
