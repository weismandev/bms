import { sample, forward, guard, merge } from 'effector';
import format from 'date-fns/format';
import FileSaver from 'file-saver';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import Done from '@img/done.svg';
import i18n from '@shared/config/i18n';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { Value } from '../../interfaces';
import { pageUnmounted } from '../page/page.model';
import {
  $opened,
  open,
  $isDetailOpen,
  fxGetPoll,
  $currentTab,
  changeTab,
  addClicked,
  entityApi,
  fxCreatePoll,
  deletePoll,
  fxDeletePoll,
  fxUpdatePoll,
  fxPusblishPoll,
  publishPoll,
  $mode,
  finishPoll,
  fxFinishPoll,
  fxGetStatistics,
  $generalStatistic,
  $dailyStatistics,
  $detailedStatistics,
  exportPollResults,
  fxExportPollResults,
} from './detail.model';

const { t } = i18n;

interface Data {
  title: string;
  active_to: string;
  is_multiple_choice: boolean;
  buildings: { id: number; title: string }[];
  question_option_1: Value;
  question_option_2: Value;
  question_options: Value[];
  question: Value;
}

$currentTab
  .on(changeTab, (_, tab) => tab)
  .on(addClicked, () => 'settings')
  .reset([signout, pageUnmounted]);

$isDetailOpen
  .on(fxGetPoll.done, () => true)
  .off(open)
  .reset([signout, pageUnmounted, fxDeletePoll.done]);

$mode.reset([fxCreatePoll.done, fxUpdatePoll.done]);

sample({
  clock: open,
  fn: (id) => ({ poll_id: id }),
  target: fxGetPoll,
});

sample({
  clock: fxCreatePoll.done,
  fn: ({
    result: {
      poll: { id },
    },
  }) => ({ poll_id: id }),
  target: fxGetPoll,
});

sample({
  clock: fxUpdatePoll.done,
  fn: ({
    result: {
      poll: { id },
    },
  }) => ({ poll_id: id }),
  target: fxGetPoll,
});

const formatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(new Date(localDate), "yyyy-MM-dd'T'HH:mm");
};

$opened
  .on(fxGetPoll.done, (_, { result }) => {
    if (Object.values(result?.poll).length === 0) {
      return {};
    }

    const { poll } = result;

    const question = Boolean(poll?.questions[0])
      ? { id: poll.questions[0].id, title: poll.questions[0].title }
      : { id: null, title: '' };

    const options = Boolean(poll?.questions[0]?.options) ? poll.questions[0].options : [];

    const create_at = Boolean(poll?.created_at) ? formatDate(poll.created_at) : '';
    const updated_at = Boolean(poll?.updated_at) ? formatDate(poll.updated_at) : '';
    const poll_activity =
      Boolean(poll?.active_from) && Boolean(poll?.active_to)
        ? `${format(new Date(poll.active_from), 'dd.MM.yyyy')} - ${format(
            new Date(poll.active_to),
            'dd.MM.yyyy'
          )}`
        : '';

    return {
      id: poll.id,
      title: poll?.title || '',
      active_to: poll?.active_to || '',
      poll_activity,
      create_at,
      updated_at,
      author: poll?.author.fullname || '',
      buildings: Array.isArray(poll?.buildings)
        ? poll.buildings.map((item) => item?.id)
        : [],
      status: poll?.status || null,
      question,
      question_option_1: Boolean(options[0]) ? options[0] : { id: null, title: '' },
      question_option_2: Boolean(options[1]) ? options[1] : { id: null, title: '' },
      question_options:
        Boolean(options) && options.slice(2).length > 0 ? options.slice(2) : [],
      options,
      is_multiple_choice: Boolean(poll?.is_multiple_choice),
    };
  })
  .reset([signout, pageUnmounted]);

sample({
  clock: entityApi.create,
  fn: (data) => ({ ...formatPayload(data), published: 0 }),
  target: fxCreatePoll,
});

sample({
  clock: entityApi.update,
  fn: (data) => ({ ...{ id: data.id }, ...formatPayload(data), published: 0 }),
  target: fxUpdatePoll,
});

const formatPayload = (data: Data) => {
  const buildings = data.buildings.map((item) =>
    typeof item === 'object' ? item.id : item
  );

  const options = [
    data.question_option_1,
    data.question_option_2,
    ...data.question_options,
  ];

  const questions = [
    {
      ...data.question,
      options,
    },
  ];

  return {
    title: data.title,
    active_to: format(new Date(data.active_to), 'dd.MM.yyyy'),
    is_multiple_choice: Number(Boolean(data.is_multiple_choice)),
    buildings,
    questions,
  };
};

sample({
  clock: deletePoll,
  source: $opened,
  fn: ({ id }) => ({ poll_id: id }),
  target: fxDeletePoll,
});

sample({
  clock: publishPoll,
  source: $opened,
  fn: (data) => ({ ...{ id: data.id }, ...formatPayload(data), published: 1 }),
  target: fxPusblishPoll,
});

forward({
  from: fxCreatePoll.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('PollCreated'),
    Icon: Done,
  })),
});

sample({
  clock: finishPoll,
  source: $opened,
  fn: ({ id }) => ({ poll_id: id }),
  target: fxFinishPoll,
});

sample({
  clock: fxPusblishPoll.done,
  fn: ({
    result: {
      poll: { id },
    },
  }) => ({ poll_id: id }),
  target: fxGetPoll,
});

sample({
  clock: fxFinishPoll.done,
  fn: ({ params: { poll_id } }) => ({ poll_id }),
  target: fxGetPoll,
});

guard({
  source: merge([
    sample($opened, changeTab, (opened, tab) => ({
      id: opened.id,
      tab,
    })),
    sample($currentTab, open, (tab, id) => ({ tab, id })),
  ]),
  filter: ({ id, tab }) => Boolean(id) && tab === 'statistics',
  target: fxGetStatistics.prepend(({ id }: { id: number }) => ({
    poll_id: id,
  })),
});

$generalStatistic
  .on(
    fxGetStatistics.done,
    (
      _,
      {
        result: {
          poll: { statistic },
        },
      }
    ) => ({
      count: statistic?.resident_count || 0,
      coverage: statistic?.voted_percent || 0,
      voted: statistic?.voted_count || 0,
      notVoted: statistic?.not_voted_count || 0,
    })
  )
  .reset([signout, pageUnmounted]);

$dailyStatistics
  .on(
    fxGetStatistics.done,
    (
      _,
      {
        result: {
          poll: { statistic_by_days },
        },
      }
    ) => statistic_by_days
  )
  .reset([signout, pageUnmounted]);

$detailedStatistics
  .on(
    fxGetStatistics.done,
    (
      _,
      {
        result: {
          poll: { residents },
        },
      }
    ) => residents
  )
  .reset([signout, pageUnmounted]);

sample({
  clock: exportPollResults,
  source: $opened,
  fn: ({ id }) => ({
    poll_id: id,
    export: 1,
  }),
  target: fxExportPollResults,
});

fxExportPollResults.done.watch(({ params: { poll_id }, result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const dateTimeNow = format(new Date(), 'dd_MM_yyyy_HH_mm');
  FileSaver.saveAs(
    URL.createObjectURL(blob),
    `${t('PollResults')}_№${poll_id}_${dateTimeNow}.xls`
  );
});
