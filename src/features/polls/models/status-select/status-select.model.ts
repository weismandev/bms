import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  {
    id: 'draft',
    title: t('Draft'),
  },
  {
    id: 'active',
    title: t('Active'),
  },
  {
    id: 'finished',
    title: t('Completed'),
  },
];
