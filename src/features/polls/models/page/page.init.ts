import { merge, forward, guard, attach } from 'effector';
import { pending } from 'patronum/pending';
import { format } from 'date-fns';
import { signout } from '@features/common';
import { pollsApi } from '../../api';
import { GetPollsPayload, Value } from '../../interfaces';
import {
  fxGetPoll,
  fxCreatePoll,
  fxDeletePoll,
  fxUpdatePoll,
  fxPusblishPoll,
  fxFinishPoll,
  fxGetStatistics,
  fxExportPollResults,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import { $tableParams } from '../table/table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  $isErrorDialogOpen,
  $error,
  $isLoading,
  pageUnmounted,
  changedErrorDialogVisibility,
  fxGetPolls,
  pageMounted,
  $rawData,
  $isLoadingPolls,
} from './page.model';

fxGetPolls.use(pollsApi.getPolls);
fxGetPoll.use(pollsApi.getPoll);
fxCreatePoll.use(pollsApi.createUpdatePoll);
fxUpdatePoll.use(pollsApi.createUpdatePoll);
fxDeletePoll.use(pollsApi.deletePoll);
fxPusblishPoll.use(pollsApi.createUpdatePoll);
fxFinishPoll.use(pollsApi.finishPoll);
fxGetStatistics.use(pollsApi.getStatistics);
fxExportPollResults.use(pollsApi.exportPollResults);

const errorOccured = merge([
  fxGetPolls.fail,
  fxGetPoll.fail,
  fxCreatePoll.fail,
  fxDeletePoll.fail,
  fxUpdatePoll.fail,
  fxPusblishPoll.fail,
  fxFinishPoll.fail,
  fxGetStatistics.fail,
  fxExportPollResults.fail,
]);

forward({
  from: $isGettingSettingsFromStorage,
  to: $isLoading,
});

$isLoading
  .on(
    pending({
      effects: [
        fxGetPoll,
        fxCreatePoll,
        fxDeletePoll,
        fxUpdatePoll,
        fxPusblishPoll,
        fxFinishPoll,
        fxGetStatistics,
        fxExportPollResults,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$isLoadingPolls
  .on(fxGetPolls.pending, (_, pending) => pending)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

guard({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxDeletePoll.done,
    fxCreatePoll.done,
    fxUpdatePoll.done,
    fxPusblishPoll.done,
    fxFinishPoll.done,
    $settingsInitialized,
  ],
  filter: $settingsInitialized,
  target: attach({
    effect: fxGetPolls,
    source: { table: $tableParams, filters: $filters },
    mapParams: (_, { table, filters }) => {
      const payload: GetPollsPayload = {
        page: table.page,
        per_page: table.per_page,
        search: table.search,
      };

      if (table.sorting.length > 0) {
        payload.sort = table.sorting[0].field;
        payload.order = table.sorting[0].sort;
      }

      if (filters.published_at_from.length > 0) {
        const date = format(new Date(filters.published_at_from), 'dd.MM.yyyy');

        payload.published_at_from = date;
      }

      if (filters.published_at_to.length > 0) {
        const date = format(new Date(filters.published_at_to), 'dd.MM.yyyy');

        payload.published_at_to = date;
      }

      if (filters.buildings.length > 0) {
        const buildings = filters.buildings.map((item: Value) => item.id);

        payload.buildings = buildings;
      }

      if (filters.statuses.length > 0) {
        const statuses = filters.statuses.map((item: Value) => item.id);

        payload.statuses = statuses;
      }

      return payload;
    },
  }),
});

$rawData.on(fxGetPolls.done, (_, { result }) => result).reset([pageUnmounted, signout]);
