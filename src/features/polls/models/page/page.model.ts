import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { GetPollsResponse, GetPollsPayload } from '../../interfaces';

export const statusColor: {
  [key: string]: { background: string; text: string };
} = {
  draft: {
    background: '#E4C000',
    text: '#FFFFFF',
  },
  finished: {
    background: '#1BB169',
    text: '#FFFFFF',
  },
  active: {
    background: '#FF8A00',
    text: '#FFFFFF',
  },
};

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const fxGetPolls = createEffect<GetPollsPayload, GetPollsResponse, Error>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetPollsResponse>({
  polls: [],
  meta: { total: 0 },
});
export const $isLoadingPolls = createStore<boolean>(false);
