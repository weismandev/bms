import { pollsApi } from '@features/polls/api';
import {
  $dates,
  $filtredStatistic,
  $isLoadingInModal,
  $isOpenModalStat,
  changeOpenModalStat,
  exportClicked,
  fxExportStatistics,
  fxGetFiltredStatistics,
  getFiltredStatistic,
  saveDates,
} from './model';
import { pageUnmounted } from '../page';
import { signout } from '@features/common';
import { sample } from 'effector';
import { $opened } from '../detail';
import { format } from 'date-fns';
import FileSaver from 'file-saver';

const formatDateForViews = (date: string) => {
  const [day, month, year] = date.split('.');

  return `${year}-${month}-${day}`;
};

$isOpenModalStat.on(changeOpenModalStat, (_, isOpen) => isOpen);

fxGetFiltredStatistics.use(pollsApi.getStatistics);
fxExportStatistics.use(pollsApi.exportPollStatistics);

/** Ставлю загрузку в true при наличии активного запроса */
$isLoadingInModal
  .on(
    [fxGetFiltredStatistics.pending, fxExportStatistics.pending],
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

/** Запусчкаю статистику по дням после получения дат для фильтрации */
sample({
  clock: getFiltredStatistic,
  source: $opened,
  fn: ({ id }, { start_date, finish_date }) => ({
    poll_id: id,
    start_date,
    finish_date,
  }),
  target: fxGetFiltredStatistics,
});

$filtredStatistic
  .on(
    fxGetFiltredStatistics.done,
    (_, { result }) =>
      result.poll?.statistic_by_days.map((day) => ({
        date: formatDateForViews(day.date),
        count: day.viewed_count,
      })) ?? []
  )
  .reset(changeOpenModalStat);

$dates.on(saveDates, (_, dates) => dates).reset(changeOpenModalStat);

/** Посылаю данные для выгрузки */
sample({
  clock: exportClicked,
  source: { opened: $opened, dates: $dates },
  fn: ({ opened, dates }) => ({
    poll_id: opened.id,
    start_date: dates.start_date,
    finish_date: dates.finish_date,
    export: 1,
  }),
  target: fxExportStatistics,
});

/** Сохраняю выгрузку */
sample({
  source: fxExportStatistics.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `news_statistics_${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});
