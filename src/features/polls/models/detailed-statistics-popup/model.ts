import {
  FilterDates,
  GetStatisticsPayload,
  GetStatisticsResponse,
  IFiltredDates,
} from '@features/polls/interfaces';
import { createEffect, createEvent, createStore } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const $isOpenModalStat = createStore(false);
export const changeOpenModalStat = createEvent<boolean>();

export const getFiltredStatistic = createEvent<FilterDates>();

export const exportClicked = createEvent<void>();

export const saveDates = createEvent<FilterDates>();

export const $dates = createStore<FilterDates>({} as FilterDates);

export const $filtredStatistic = createStore<IFiltredDates[]>([]);

export const $isLoadingInModal = createStore<boolean>(false);

export const fxGetFiltredStatistics = createEffect<
  GetStatisticsPayload,
  GetStatisticsResponse,
  Error
>();

export const fxExportStatistics = createEffect<GetStatisticsPayload, Blob, Error>();

export const columnsFiltredStatistic = [
  { name: 'date', title: t('Label.date') },
  { name: 'views', title: t('Views') },
];

const formatDates = (dates: IFiltredDates[]) =>
  dates.reduce((acc: { date: string; views: number }[], value) => {
    const { date, count } = value;

    if (!date) {
      return [
        ...acc,
        {
          date: '',
          views: count,
        },
      ];
    }

    return [
      ...acc,
      {
        date: format(new Date(date), 'dd.MM.yyyy'),
        views: count,
      },
    ];
  }, []);

export const $tableFiltredStatistic = $filtredStatistic.map((dates) =>
  dates ? formatDates(dates) : []
);
