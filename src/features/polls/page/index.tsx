import { FC } from 'react';
import { useStore } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  $isFilterOpen,
  $isDetailOpen,
  $isOpenDialog,
  changeDialogVisibility,
} from '../models';
import { Table, Filters, List, Detail, PollDialog } from '../organisms';

const PollsPage: FC = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isOpenDialog = useStore($isOpenDialog);

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);
  const onCloseDialog = () => changeDialogVisibility(false);

  // const main = isDetailOpen ? <List /> : <Table />; // пока не используется List

  return (
    <>
      <PageGate />

      <PollDialog isOpen={isOpenDialog} onClose={onCloseDialog} />
      <ColoredTextIconNotification />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{
          filterWidth: '400px',
          detailWidth: isFilterOpen ? 'minmax(370px, 40%)' : 'minmax(370px, 50%)', // если используется List, то 75%
        }}
      />
    </>
  );
};

const RestrictedPollsPage: FC = () => (
  <HaveSectionAccess>
    <PollsPage />
  </HaveSectionAccess>
);

export { RestrictedPollsPage as PollsPage };
