export function startDateAfterFinishDate(this: {
  parent: {
    published_at_from: string;
    published_at_to: string;
  };
}) {
  const isDatesDefined =
    Boolean(this.parent.published_at_from) && Boolean(this.parent.published_at_to);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.published_at_from)) >
      Number(new Date(this.parent.published_at_to))
  ) {
    return false;
  }

  return true;
}
