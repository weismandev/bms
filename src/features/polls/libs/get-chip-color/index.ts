import { statusColor } from '../../models';

export const getChipColor = (
  slug: string
): {
  color: string;
  backgroundColor: string;
} => ({
  color: statusColor[slug].text,
  backgroundColor: statusColor[slug].background,
});
