export function dateAfterCurrentDate(this: { parent: { active_to: string } }) {
  const isDataExist = Boolean(this.parent.active_to);
  const date = new Date();

  if (isDataExist) {
    const currentDate = new Date(date.setSeconds(0, 0));
    const active_to = new Date(this.parent.active_to);

    if (active_to < currentDate) {
      return false;
    }

    return true;
  }

  return false;
}
