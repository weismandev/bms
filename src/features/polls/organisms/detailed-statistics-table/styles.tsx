import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  root: {
    height: 400,
    cursor: 'pointer',
  },
  title: {
    fontWeight: 500,
    fontSize: 22,
    color: '#3B3B50',
    margin: '20px 0 20px 0',
  },
}));
