import { memo, FC } from 'react';
import { useStore } from 'effector-react';

import i18n from '@shared/config/i18n';
import { DataGrid } from '@features/data-grid';

import { columnsDetailedStatistics, $tableDetailedStatisticsData } from '../../models';
import { useStyles } from './styles';

const { t } = i18n;

export const DetailedStatisticsTable: FC = memo(() => {
  const data = useStore($tableDetailedStatisticsData);

  const classes = useStyles();

  return (
    <DataGrid.Mini
      rows={data}
      columns={columnsDetailedStatistics}
      classes={{ root: classes.root }}
      disableColumnMenu
      disableMultipleSelection
      hideFooter
      density="compact"
    />
  );
});
