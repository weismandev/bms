import { FC, ChangeEvent } from 'react';
import { useStore } from 'effector-react';

import { AddButton, Greedy, AdaptiveSearch, FilterButton, SearchInput } from '@ui/index';

import {
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
  $count,
  $isDetailOpen,
  addClicked,
  $mode,
  $opened,
} from '../../models';
import { useStyles } from './styles';

export const CustomToolbar: FC = () => {
  const searchValue = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);
  const totalCount = useStore($count);
  const isDetailOpen = useStore($isDetailOpen);
  const mode = useStore($mode);
  const opened = useStore($opened);

  const isDisabledAddClicked = mode === 'edit' && !opened.id;

  const isAnySideOpen = isFilterOpen && isDetailOpen;

  const classes = useStyles();

  const onClickFilter = () => changedFilterVisibility(true);
  const onSearch = (e: ChangeEvent<HTMLInputElement>) => searchChanged(e.target.value);
  const handleClear = () => searchChanged('');

  // где комментарии, List в данный момент не используется

  return (
    <>
      <FilterButton
        disabled={isFilterOpen}
        className={classes.margin}
        onClick={onClickFilter}
      />
      {/* {!isDetailOpen && ( */}
      {/* )} */}
      {/* {isDetailOpen ? (
        <SearchInput
          value={searchValue}
          onChange={onSearch}
          handleClear={handleClear}
        />
      ) : ( */}
      <AdaptiveSearch
        {...{
          totalCount,
          searchValue,
          searchChanged,
          isAnySideOpen,
        }}
      />
      {/* )} */}
      <Greedy />
      <AddButton
        disabled={isDisabledAddClicked}
        onClick={addClicked}
        className={classes.margin}
      />
    </>
  );
};
