import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  margin: {
    marginRight: 10,
  },
});
