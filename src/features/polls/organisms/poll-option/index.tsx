import { FC } from 'react';
import { Field } from 'formik';
import { Grid } from '@mui/material';
import { Close } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { InputField, IconButton } from '@ui/index';
import { useStyles } from './styles';

interface Props {
  name: string;
  label: string | null;
  remove: () => void;
}

const { t } = i18n;

export const Option: FC<Props> = ({ name, label, remove }) => {
  const classes = useStyles();

  return (
    <Grid container alignItems="center" className={classes.optionWrapper}>
      <Grid item className={classes.option}>
        <Field
          name={name}
          component={InputField}
          label={label}
          placeholder={t('EnterAnAnswer')}
          divider={false}
        />
      </Grid>
      <Grid item className={classes.optionButton}>
        <IconButton onClick={remove} size="large">
          <Close />
        </IconButton>
      </Grid>
    </Grid>
  );
};
