import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  optionWrapper: {
    marginBottom: 15,
  },
  option: {
    flexGrow: 1,
  },
  optionButton: {
    marginLeft: 10,
  },
}));
