import { FC } from 'react';
import { useStyles } from './styles';

interface Props {
  title: string;
  percent: number;
  count: number;
}

export const VoteProgress: FC<Props> = ({ title, percent, count }) => {
  const classes = useStyles({ percent });

  return (
    <>
      <div className={classes.title}>{title}</div>
      <div className={classes.progressWrap}>
        <div className={classes.progress} />
        <div className={classes.count}>{`${count} (${percent})%`}</div>
      </div>
    </>
  );
};
