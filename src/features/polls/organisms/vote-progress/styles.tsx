import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  title: {
    fontWeight: 500,
    fontSize: 14,
    color: '#7D7D8E',
  },
  progressWrap: {
    display: 'flex',
  },
  progress: {
    width: '50%',
    border: `1px solid #0394E3`,
    borderRadius: 8,
    height: 12,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    background: ({ percent }: { percent: number }) =>
      `linear-gradient(to right, #0394E3 ${percent}%, #fff ${percent}%)`,
  },
  count: {
    fontWeight: 500,
    fontSize: 14,
    color: '#0394E3',
    margin: '-3px 0 0 10px',
  },
});
