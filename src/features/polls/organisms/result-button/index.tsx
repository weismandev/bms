import { FC } from 'react';
import { VerticalAlignBottom } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { ActionIconButton } from '@ui/index';

const { t } = i18n;

interface Props {
  style?: object;
  onClick: () => void;
}

export const ResultButton: FC<Props> = ({ style, onClick, ...props }) => (
  <ActionIconButton style={style} onClick={onClick} {...props}>
    <VerticalAlignBottom titleAccess={t('Results')} />
  </ActionIconButton>
);
