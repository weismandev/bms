import { FC } from 'react';
import { useStore } from 'effector-react';
import { List as MuiList } from '@mui/material';
import { Wrapper, CustomScrollbar } from '@ui/index';
import { $list, open } from '../../models';
import { CustomToolbar } from '../custom-toolbar';
import { Item } from '../list-item';
import { useStyles } from './styles';

export const List: FC = () => {
  const list = useStore($list);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar trackVerticalStyle={{ paddingTop: 2, paddingBottom: 2 }}>
        <div className={classes.toolbar}>
          <CustomToolbar />
        </div>
        <MuiList>
          {list.map((item) => {
            const onClick = () => open(item.id);

            return <Item key={item.id} onClick={onClick} {...item} />;
          })}
        </MuiList>
      </CustomScrollbar>
    </Wrapper>
  );
};
