import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  root: {
    height: 87,
    cursor: 'pointer',
  },
  cell: {
    fontWeight: 500,
    color: '#7D7D8E',
  },
  title: {
    fontWeight: 500,
    fontSize: 22,
    color: '#3B3B50',
    margin: '20px 0 20px 0',
  },
  noData: {
    height: 87,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailedView: {
    textAlign: 'right',
    fontWeight: 500,
    fontSize: 16,
    color: '#0394E3',
    cursor: 'pointer',
  },
}));
