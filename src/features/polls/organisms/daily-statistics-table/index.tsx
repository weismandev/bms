import { memo, FC } from 'react';
import { useStore } from 'effector-react';

import i18n from '@shared/config/i18n';
import { DataGrid } from '@features/data-grid';

import {
  $columnsDailyStatistics,
  $tableDailyStatisticsData,
  changeOpenModalStat,
} from '../../models';
import { useStyles } from './styles';
import { NewsModalStatistics } from '@features/polls/molecules/statistics-modal';

const { t } = i18n;

export const DailyStatisticsTable: FC = memo(() => {
  const data = useStore($tableDailyStatisticsData);
  const columns = useStore($columnsDailyStatistics);

  const classes = useStyles();

  return (
    <>
      <div className={classes.title}>{t('NumberOfViewsPerWeek') as string}</div>
      {data.length > 0 ? (
        <>
          <p className={classes.detailedView}>
            <span onClick={() => changeOpenModalStat(true)}>
              {t('DetailedView') as string}
            </span>
          </p>
          <DataGrid.Mini
            rows={data}
            columns={columns}
            classes={{ root: classes.root, cell: classes.cell }}
            disableColumnMenu
            disableMultipleSelection
            hideFooter
            density="compact"
          />
          <NewsModalStatistics />
        </>
      ) : (
        <div className={classes.noData}>{t('noData') as string}</div>
      )}
    </>
  );
});
