import { FC } from 'react';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';
import i18n from '@shared/config/i18n';
import { CloseButton, CustomScrollbar, StatisticsPaper } from '@ui/index';
import { getChipColor } from '../../libs';
import {
  $isLoading,
  $opened,
  changedDetailVisibility,
  $generalStatistic,
} from '../../models';
import { DailyStatisticsTable } from '../daily-statistics-table';
import { useStyles as useStylesDetail } from '../detail/styles';
import { DetailedStatisticsTable } from '../detailed-statistics-table';
import { useStyles, styles } from './styles';

const { t } = i18n;

export const PollStatistics: FC = () => {
  const isLoading = useStore($isLoading);
  const opened = useStore($opened);
  const generalStatistic = useStore($generalStatistic);

  const classes = useStyles();
  const classesDetail = useStylesDetail();

  const { color, backgroundColor } = getChipColor(opened.status.slug);

  const onClose = () => changedDetailVisibility(false);

  return (
    <>
      <div className={classes.header}>
        <div className={classes.headerTitle}>
          <span className={classesDetail.pollId}>{`${t('Poll')} №${opened.id}`}</span>
          {Boolean(opened.status) && (
            <Chip
              label={opened.status.label}
              size="small"
              style={{
                backgroundColor,
                color,
              }}
            />
          )}
        </div>
        <CloseButton onClick={onClose} />
      </div>
      {!isLoading && (
        <CustomScrollbar style={styles.scrollbar}>
          <div className={classes.statisticWrap}>
            <div className={classes.generalStatistic}>
              <StatisticsPaper
                titleOne={t('TotalUsers')}
                valueOne={generalStatistic.count}
                titleTwo={t('Coverage')}
                valueTwo={`${generalStatistic.coverage}%`}
              />
              <StatisticsPaper
                titleOne={t('PassedThePoll')}
                valueOne={generalStatistic.voted}
                titleTwo={t('DidnotCompleteThePoll')}
                valueTwo={generalStatistic.notVoted}
              />
            </div>
            <DailyStatisticsTable />
            <DetailedStatisticsTable />
          </div>
        </CustomScrollbar>
      )}
    </>
  );
};
