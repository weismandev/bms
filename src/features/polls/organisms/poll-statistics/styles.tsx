import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '15px 24px 0 15px',
  },
  headerTitle: {
    display: 'flex',
  },
  statisticWrap: {
    padding: 24,
  },
  generalStatistic: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(250px, 1fr))',
    gap: '15px calc(5px + 2%)',
  },
}));

export const styles = {
  scrollbar: {
    height: 'calc(100% - 94px)',
  },
};
