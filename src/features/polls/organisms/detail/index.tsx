import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Wrapper, Tabs } from '@ui/index';
import { tabs, $currentTab, changeTab, $opened } from '../../models';
import { PollStatistics } from '../poll-statistics';
import { SettingsPoll } from '../settings-poll';
import { useStyles } from './styles';

export const Detail: FC = memo(() => {
  const currentTab = useStore($currentTab);
  const opened = useStore($opened);

  const classes = useStyles();

  const isNew = !opened.id;

  const onChange = (_: unknown, tab: string) => changeTab(tab);

  return (
    <Wrapper className={classes.wrapper}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={onChange}
        isNew={isNew}
        tabEl={<Tab />}
      />
      {currentTab === 'settings' && <SettingsPoll />}
      {currentTab === 'statistics' && <PollStatistics />}
    </Wrapper>
  );
});
