import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  pollId: {
    fontWeight: 900,
    fontSize: 18,
    color: '#3B3B50',
    marginRight: 20,
  },
}));
