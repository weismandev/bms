import { FC } from 'react';
import { Done } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { ActionIconButton } from '@ui/index';

const { t } = i18n;

interface Props {
  style?: object;
  onClick: () => void;
}

export const FinishButton: FC<Props> = ({ style, onClick, ...props }) => (
  <ActionIconButton kind="positive" style={style} onClick={onClick} {...props}>
    <Done titleAccess={t('ToComplete')} />
  </ActionIconButton>
);
