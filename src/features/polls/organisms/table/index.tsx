import { memo, FC } from 'react';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';

import { Wrapper, Popover, IconButton } from '@ui/index';
import { DataGrid } from '@features/data-grid';

import {
  $tableData,
  open,
  $isLoadingPolls,
  $selectRow,
  selectionRowChanged,
  sortChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} from '../../models';
import { CustomToolbar } from '../custom-toolbar';
import { getChipColor } from '../../libs';
import { useStyles } from './styles';

export const Table: FC = memo(() => {
  const tableData = useStore($tableData);
  const isLoading = useStore($isLoadingPolls);
  const selectRow = useStore($selectRow);
  const sorting = useStore($sorting);
  const visibilityColumns = useStore($visibilityColumns);
  const columns = useStore($columns);
  const pageSize = useStore($pageSize);
  const currentPage = useStore($currentPage);
  const countPage = useStore($countPage);

  const classes = useStyles();

  const statusIndex = columns.findIndex((column) => column.field === 'status');
  const buildingsIndex = columns.findIndex((column) => column.field === 'buildings');

  columns[statusIndex].renderCell = ({
    value: { slug, label },
  }: {
    value: { slug: string; label: string };
  }) => {
    const { color, backgroundColor } = getChipColor(slug);

    return (
      <Chip
        label={label}
        size="small"
        style={{
          backgroundColor,
          color,
        }}
      />
    );
  };

  columns[buildingsIndex].renderCell = ({ value }: { value: string[] }) => {
    if (!value || value[0]?.length === 0) {
      return '';
    }

    return (
      <div className={classes.formatterContainer}>
        <div className={classes.firstValue}>{value[0]}</div>
        {value[1] && (
          <Popover
            trigger={
              <IconButton size="large">
                <span>+{value.slice(1).length}</span>
              </IconButton>
            }
          >
            <div className={classes.otherValues}>
              {value.slice(1).map((item: string, index: number) => (
                <div key={index}>{item}</div>
              ))}
            </div>
          </Popover>
        )}
      </div>
    );
  };

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    open,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <Wrapper className={classes.wrapper}>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </Wrapper>
  );
});
