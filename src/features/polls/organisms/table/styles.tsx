import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    padding: 24,
  },
  tableRoot: {
    cursor: 'pointer',
  },
  formatterContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'start',
  },
  firstValue: {
    marginTop: 5,
  },
  otherValues: {
    display: 'grid',
    gap: 10,
    padding: 10,
    background: '#fcfcfc',
  },
});
