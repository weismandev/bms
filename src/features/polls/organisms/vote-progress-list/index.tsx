import { FC } from 'react';
import { QuestionOption } from '../../interfaces';
import { VoteProgress } from '../vote-progress';
import { useStyles } from './styles';

interface Props {
  options: QuestionOption[];
}

export const VotingProgressList: FC<Props> = ({ options }) => {
  const classes = useStyles();

  if (options && options.length === 0) {
    return null;
  }

  return (
    <div>
      {options.map((item) => {
        const percent = item?.vote_statistic?.percent || 0;
        const count = item?.vote_statistic?.count || 0;

        return (
          <div key={item.id} className={classes.option}>
            <VoteProgress title={item.title} percent={percent} count={count} />
          </div>
        );
      })}
    </div>
  );
};
