import { FC } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import { Chip, Button } from '@mui/material';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  CustomScrollbar,
  InputField,
  Divider,
  SwitchField,
} from '@ui/index';
import { Value } from '../../interfaces';
import { getChipColor } from '../../libs';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  changeDialogVisibility,
  changeKind,
  $isDetailOpen,
  $isFilterOpen,
  exportPollResults,
} from '../../models';
import { useStyles as useStylesDetail } from '../detail/styles';
import { FinishButton } from '../finish-button';
import { Option } from '../poll-option';
import { PublishButton } from '../publish-button';
import { ResultButton } from '../result-button';
import { VotingProgressList } from '../vote-progress-list';
import { useStyles, styles } from './styles';
import { validationSchema } from './validation';

const { t } = i18n;

export const SettingsPoll: FC = () => {
  const mode = useStore($mode);
  const opened = useStore($opened);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  const classes = useStyles();
  const classesDetail = useStylesDetail();

  const isNew = !opened.id;

  const field = Boolean(isDetailOpen && isFilterOpen)
    ? classes.fieldWithOpenDetalFilter
    : classes.field;

  return (
    <Formik
      initialValues={opened}
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      enableReinitialize
      render={({ values, resetForm }) => {
        const onEdit = () => changeMode('edit');
        const onClose = () => changedDetailVisibility(false);
        const onCancel = () => {
          if (isNew) {
            changedDetailVisibility(false);
          } else {
            changeMode('view');
            resetForm();
          }
        };
        const onDelete = () => {
          changeDialogVisibility(true);
          changeKind('delete');
        };
        const onPublish = () => {
          changeDialogVisibility(true);
          changeKind('publish');
        };
        const onFinish = () => {
          changeDialogVisibility(true);
          changeKind('finish');
        };

        const { color, backgroundColor } = getChipColor(values.status.slug);

        return (
          <Form className={classes.form}>
            <div className={classes.pollHeader}>
              <span className={classesDetail.pollId}>
                {isNew ? t('NewPoll') : `${t('Poll')} №${opened.id}`}
              </span>
              {Boolean(values.status) && (
                <Chip
                  label={values.status.label}
                  size="small"
                  style={{
                    backgroundColor,
                    color,
                  }}
                />
              )}
            </div>
            <DetailToolbar
              className={classes.toolbar}
              mode={mode}
              onEdit={onEdit}
              onClose={onClose}
              onCancel={onCancel}
              onDelete={onDelete}
              hidden={{ edit: opened.status.slug !== 'draft' }}
            >
              {values.status.slug === 'draft' && mode === 'view' && (
                <PublishButton style={styles.button} onClick={onPublish} />
              )}
              {values.status.slug === 'active' && mode === 'view' && (
                <FinishButton
                  style={{ ...styles.button, ...{ order: 10 } }}
                  onClick={onFinish}
                />
              )}
              {values.status.slug !== 'draft' && mode === 'view' && (
                <ResultButton
                  style={{ ...styles.button, ...{ order: 10 } }}
                  onClick={exportPollResults}
                />
              )}
            </DetailToolbar>
            <div className={classes.content}>
              <CustomScrollbar>
                <div className={classes.fields}>
                  <div className={field}>
                    <Field
                      name="title"
                      component={InputField}
                      label={t('PollTopic')}
                      placeholder={t('EnterPollTopic')}
                      required
                      mode={mode}
                      rowsMax={4}
                      multiline
                    />
                  </div>
                  {values.status.slug === 'draft' ? (
                    <>
                      <div>
                        <div className={field}>
                          <Field
                            name="active_to"
                            component={InputField}
                            label={t('EndOfTheEvent')}
                            placeholder={t('EnterPollTopic')}
                            mode={mode}
                            required
                            type="date"
                            divider={false}
                          />
                        </div>
                        <div className={classes.helperText}>
                          {t('SuggestedPollDuration')}
                        </div>
                      </div>
                      <Divider />
                    </>
                  ) : (
                    <>
                      <div className={classes.twoFields}>
                        <div className={field}>
                          <Field
                            name="poll_activity"
                            component={InputField}
                            label={t('Period')}
                            placeholder={t('SpecifyThePeriod')}
                            mode="view"
                          />
                        </div>
                        <div className={field}>
                          <Field
                            name="create_at"
                            component={InputField}
                            label={t('DateOfCreation')}
                            placeholder={t('SpecifyCreationDate')}
                            mode="view"
                            type="datetime-local"
                          />
                        </div>
                      </div>
                      <div className={field}>
                        <Field
                          name="updated_at"
                          component={InputField}
                          label={t('LastModified')}
                          placeholder={t('SpecifyTheDateOfTheLastModification')}
                          mode="view"
                          type="datetime-local"
                        />
                      </div>
                    </>
                  )}
                  {!isNew && (
                    <div className={field}>
                      <Field
                        name="author"
                        component={InputField}
                        label={t('Author')}
                        placeholder={t('SpecifyTheAuthor')}
                        mode="view"
                      />
                    </div>
                  )}
                  <Field
                    name="buildings"
                    component={HouseSelectField}
                    label={t('Buildings')}
                    placeholder={t('selectBbuildings')}
                    mode={mode}
                    isMulti
                    required
                  />
                  {values.status.slug === 'draft' ? (
                    <>
                      <div className={field}>
                        <Field
                          name="question.title"
                          component={InputField}
                          label={t('QuestionText')}
                          placeholder={t('EnterQuestionText')}
                          required
                          mode={mode}
                          rowsMax={4}
                          multiline
                          divider={false}
                        />
                      </div>
                      <div className={classes.optionsTitle}>{t('AnswerOptions')}</div>
                      {mode === 'view' ? (
                        <div className={field}>
                          {values.options &&
                            values.options.map((option: Value) => (
                              <Chip
                                key={option.id}
                                label={option.title}
                                size="small"
                                classes={{
                                  root: classes.optionChip,
                                  label: classes.optionChipLabel,
                                }}
                              />
                            ))}
                        </div>
                      ) : (
                        <>
                          <div
                            className={classes.helperText}
                            style={styles.marginBottomTitle}
                          >
                            {t('CreateAtLeastTwoAnswers')}
                          </div>
                          <FieldArray
                            name="question_options"
                            render={({ push, remove }) => {
                              const isExistOtherOptions =
                                Boolean(values.question_options) &&
                                values.question_options.length > 0;

                              const options =
                                isExistOtherOptions &&
                                values.question_options.map(
                                  (_: unknown, index: number) => {
                                    const handleRemove = () => remove(index);

                                    return (
                                      <div className={field} key={index}>
                                        <Option
                                          name={`question_options[${index}].title`}
                                          label={null}
                                          remove={handleRemove}
                                        />
                                      </div>
                                    );
                                  }
                                );

                              const onClick = () => push({ id: null, title: '' });

                              return (
                                <>
                                  <div className={field}>
                                    <div className={classes.questionOption}>
                                      <Field
                                        name="question_option_1.title"
                                        component={InputField}
                                        label={null}
                                        placeholder={t('EnterAnAnswer')}
                                        required
                                        divider={false}
                                        mode={mode}
                                      />
                                    </div>
                                    <div className={classes.questionOption}>
                                      <Field
                                        name="question_option_2.title"
                                        component={InputField}
                                        label={null}
                                        placeholder={t('EnterAnAnswer')}
                                        required
                                        divider={false}
                                        mode={mode}
                                      />
                                    </div>
                                  </div>
                                  {options}
                                  {mode === 'edit' && (
                                    <Button color="primary" onClick={onClick}>
                                      {t('AddAnAnswerOption')}
                                    </Button>
                                  )}
                                </>
                              );
                            }}
                          />
                        </>
                      )}
                      {/**TODO: удалить после ноября 2023 и выпилить параметр из запроса*/}
                      {/* <div className={field} style={styles.switchField}>
                        <Field
                          component={SwitchField}
                          disabled={mode === 'view'}
                          name="is_multiple_choice"
                          label={t('AllowMultipleSelections')}
                          labelPlacement="end"
                          divider={false}
                        />
                      </div> */}
                    </>
                  ) : (
                    <>
                      <div
                        className={classes.questionTitle}
                        style={styles.marginBottomTitle}
                      >
                        {values.question.title}
                      </div>
                      <VotingProgressList options={values.options} />
                    </>
                  )}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};
