import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    height: 'calc(100% - 91px)',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
  },
  content: {
    height: 'calc(100% - 82px)',
    padding: '0 0 24px 24px',
  },
  fields: {
    paddingRight: 24,
  },
  helperText: {
    fontWeight: 400,
    fontSize: 12,
    color: '#7D7D8E',
    marginTop: 10,
  },
  pollHeader: {
    display: 'flex',
    flexDirection: 'row',
    margin: '15px 0 0 15px',
  },
  optionsTitle: {
    paddingTop: 20,
    fontWeight: 500,
    fontSize: 14,
    color: '#7D7D8E',
  },
  field: {
    paddingBottom: 10,
    paddingRight: 15,
    width: '50%',
    '@media (max-width: 1278px)': {
      width: '100%',
    },
  },
  fieldWithOpenDetalFilter: {
    // когда будет использоваться List, скорее всего нужно будет убрать или переписать стили (max-width или удалить этот стиль)
    paddingBottom: 10,
    paddingRight: 15,
    width: '50%',
    '@media (max-width: 1554px)': {
      width: '100%',
    },
  },
  questionOption: {
    marginBottom: 10,
  },
  optionChip: {
    height: '100%',
    padding: '6px 8px',
    backgroundColor: '#E7E7EC',
    '&:focus': {
      backgroundColor: '#E7E7EC',
    },
    margin: '10px 10px 0 0',
  },
  optionChipLabel: {
    color: '#65657B',
    fontSize: 13,
    fontStyle: 'normal',
    fontFamily: 'Roboto, sans-serif',
    fontWeight: 500,
    padding: '0 5px',
    overflowWrap: 'break-word',
    whiteSpace: 'normal',
    textOverflow: 'clip',
  },
  twoFields: {
    // когда будет использоваться List, скорее всего нужно будет убрать или переписать стили (max-width)
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
  questionTitle: {
    fontWeight: 500,
    fontSize: 16,
    color: '#3B3B50',
  },
}));

export const styles = {
  button: {
    order: 30,
    margin: '0 5px',
  },
  switchField: {
    marginTop: 20,
  },
  marginBottomTitle: {
    marginBottom: 10,
  },
};
