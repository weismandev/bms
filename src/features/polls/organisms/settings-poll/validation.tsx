import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { dateAfterCurrentDate } from '../../libs';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  active_to: Yup.string().test(
    'date-after-current-date',
    t('TheEndDateMustBeLaterThanTheCurrentOne'),
    dateAfterCurrentDate
  ),
  buildings: Yup.array()
    .min(1, t('thisIsRequiredField'))
    .required(t('thisIsRequiredField')),
  question: Yup.object().shape({
    title: Yup.string().required(t('thisIsRequiredField')).nullable(),
  }),
  question_option_1: Yup.object().shape({
    title: Yup.string().required(t('thisIsRequiredField')).nullable(),
  }),
  question_option_2: Yup.object().shape({
    title: Yup.string().required(t('thisIsRequiredField')).nullable(),
  }),
});
