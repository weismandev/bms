import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 120,
    position: 'relative',
    padding: '24px 24px 15px 24px',
    '&:hover': {
      background: '#E1EBFF !important',
      "& + *[role='separator']": {
        background: '#E1EBFF !important',
      },
      "& *[class*='headerText']": {
        color: '#0394E3 !important',
      },
    },
  },
  divider: {
    padding: '0 24px',
    height: 'auto',
    '& > *': {
      width: '100%',
    },
  },
  selected: {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  },
  itemHeader: {
    display: 'flex',
    flexDirection: 'row',
  },
  itemId: {
    fontWeight: 500,
    fontSize: 16,
    color: '#7D7D8E',
    marginRight: 20,
  },
  itemStatus: {
    fontWeight: 500,
    fontSize: 16,
  },
  itemPoll: {
    fontSize: 18,
    fontWeight: 900,
    color: '#3B3B50',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    width: '100%',
  },
}));
