import { FC } from 'react';
import { Typography, ListItem } from '@mui/material';
import { useHover, Divider } from '@ui/index';
import { statusColor } from '../../models';
import { useStyles } from './styles';

interface Props {
  id: number;
  poll: string;
  status: { slug: string; label: string };
  onClick: () => void;
}

export const Item: FC<Props> = ({ id, poll, status, ...rest }) => {
  const { handleMouseIn, handleMouseOut } = useHover('li');

  const classes = useStyles();

  const color = statusColor[status.slug].background;

  return (
    <>
      <ListItem
        classes={{ root: classes.item, selected: classes.selected }}
        button
        component="li"
        onMouseOver={handleMouseIn}
        onMouseOut={handleMouseOut}
        {...rest}
      >
        <div className={classes.itemHeader}>
          <Typography className={classes.itemId}>{id}</Typography>
          <Typography className={classes.itemStatus} style={{ color }}>
            {status.label}
          </Typography>
        </div>
        <Typography className={classes.itemPoll}>{poll}</Typography>
      </ListItem>
      <ListItem classes={{ root: classes.divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};
