import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    padding: 24,
    paddingTop: 0,
  },
  title: {
    color: '#8F91A3',
    marginBottom: 12,
    fontSize: 14,
  },
  dateField: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  dateDivider: {
    margin: '7px 10px 0',
    color: '#8F91A3',
  },
}));

export const styles = {
  toolbar: {
    padding: 24,
  },
};
