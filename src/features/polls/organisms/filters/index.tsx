import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
  Divider,
} from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { StatusSelectField } from '../status-select';
import { useStyles, styles } from './styles';
import { validationSchema } from './validation';

const { t } = i18n;

export const Filters = memo(() => {
  const filters = useStore($filters);

  const classes = useStyles();

  const closeFilter = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar style={styles.toolbar} closeFilter={closeFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          validationSchema={validationSchema}
          render={() => (
            <Form className={classes.form}>
              <>
                <p className={classes.title}>{t('startDate')}</p>
                <div className={classes.dateField}>
                  <div>
                    <Field
                      name="published_at_from"
                      label={null}
                      divider={false}
                      component={InputField}
                      placeholder=""
                      type="date"
                    />
                  </div>
                  <p className={classes.dateDivider}>-</p>
                  <div>
                    <Field
                      name="published_at_to"
                      label={null}
                      divider={false}
                      component={InputField}
                      placeholder=""
                      type="date"
                    />
                  </div>
                </div>
                <Divider />
              </>
              <Field
                name="buildings"
                label={t('Buildings')}
                placeholder={t('selectBbuildings')}
                component={HouseSelectField}
                isMulti
              />
              <Field
                name="statuses"
                label={t('statuses')}
                placeholder={t('selectStatuses')}
                component={StatusSelectField}
                isMulti
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
