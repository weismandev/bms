import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { startDateAfterFinishDate } from '../../libs';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  published_at_from: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeOrEqualEndDate'),
      startDateAfterFinishDate
    )
    .nullable(),
  published_at_to: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeOrEqualEndDate'),
      startDateAfterFinishDate
    )
    .nullable(),
});
