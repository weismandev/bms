import { FC } from 'react';
import { useStore } from 'effector-react';
import { Toolbar } from '@mui/material';
import { Delete, Close, Save, Done } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { ActionButton, CloseButton, Modal } from '@ui/index';
import { $kind, deletePoll, publishPoll, finishPoll } from '../../models';
import { useStyles, styles } from './styles';

const { t } = i18n;

interface DialogProps {
  onClose: () => void;
  isOpen: boolean;
}

interface ToolbarProps {
  onClose: () => void;
}

const CustomToolbar: FC<ToolbarProps> = ({ onClose }) => {
  const kind = useStore($kind);

  const classes = useStyles();

  const handleDelete = () => deletePoll();
  const handlePusblish = () => publishPoll();
  const handleFinish = () => finishPoll();

  return (
    <Toolbar disableGutters variant="dense" classes={{ root: classes.root }}>
      {kind === 'delete' && (
        <ActionButton
          icon={<Delete />}
          kind="negative"
          onClick={handleDelete}
          classes={{ root: classes.margin }}
        >
          {t('remove')}
        </ActionButton>
      )}
      {kind === 'publish' && (
        <ActionButton
          icon={<Save />}
          kind="positive"
          onClick={handlePusblish}
          classes={{ root: classes.margin }}
        >
          {t('Publish')}
        </ActionButton>
      )}
      {kind === 'finish' && (
        <ActionButton
          icon={<Done />}
          kind="positive"
          onClick={handleFinish}
          classes={{ root: classes.margin }}
        >
          {t('ToComplete')}
        </ActionButton>
      )}
      <ActionButton
        icon={<Close />}
        onClick={onClose}
        classes={{ root: classes.margin }}
        style={styles.closeButton}
      >
        {t('Cancellation')}
      </ActionButton>
    </Toolbar>
  );
};

const config: { [key: string]: { header: string; content: string } } = {
  delete: {
    header: t('DeletingPoll'),
    content: t('ConfirmThatYouWantToDeleteThisPoll'),
  },
  publish: {
    header: t('PublicationOfThePoll'),
    content: t('ConfirmThatYouWantToPostThisPoll'),
  },
  finish: {
    header: t('EndPoll'),
    content: t('ConfirmThatYouWantToCompleteThisPoll'),
  },
  '': {
    header: '',
    content: '',
  },
};

export const PollDialog: FC<DialogProps> = ({ onClose, isOpen }) => {
  const kind = useStore($kind);

  const classes = useStyles();

  return (
    <Modal
      header={
        <span className={classes.header}>
          <span>{config[kind].header}</span>
          <CloseButton onClick={onClose} />
        </span>
      }
      content={config[kind].content}
      actions={<CustomToolbar onClose={onClose} />}
      isOpen={isOpen}
      onClose={onClose}
    />
  );
};
