import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  root: {
    display: 'flex',
    alignItems: 'center',
    minHeight: 'auto',
    justifyContent: 'flex-end',
    width: '100%',
  },
  margin: {
    margin: '0 10px',
  },
}));

export const styles = {
  closeButton: {
    marginRight: 0,
  },
};
