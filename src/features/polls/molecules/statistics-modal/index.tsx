import { FC } from 'react';
import { useStore } from 'effector-react';

import { StatisticsModal } from '@ui/index';

import {
  $filtredStatistic,
  $isLoadingInModal,
  $isOpenModalStat,
  $tableFiltredStatistic,
  changeOpenModalStat,
  columnsFiltredStatistic,
  exportClicked,
  getFiltredStatistic,
  saveDates,
} from '../../models';

const NewsModalStatistics: FC = () => {
  const isOpenModal = useStore($isOpenModalStat);
  const filtredStatistic = useStore($filtredStatistic);
  const isLoadingInModal = useStore($isLoadingInModal);
  const tableFiltredStatistic = useStore($tableFiltredStatistic);

  return (
    <StatisticsModal
      isOpen={isOpenModal}
      onClose={() => changeOpenModalStat(false)}
      getFiltredStatistic={getFiltredStatistic}
      filtredStatistic={filtredStatistic}
      isLoading={isLoadingInModal}
      tableData={tableFiltredStatistic}
      columns={columnsFiltredStatistic}
      exportClicked={exportClicked}
      saveDates={saveDates}
    />
  );
};

export { NewsModalStatistics };
