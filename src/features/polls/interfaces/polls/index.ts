import { Value } from '../index';

export interface GetPollsPayload {
  page?: number;
  per_page?: number;
  published_at_from?: string;
  published_at_to?: string;
  buildings?: number[];
  statuses?: string[];
  search?: string;
  sort?: string;
  order?: string | null;
}

export interface GetPollsResponse {
  meta: {
    total: number;
  };
  polls: PollItem[];
}

export interface PollItem {
  active_from: string;
  active_to: string;
  author: {
    id: number;
    fullname: string;
  };
  buildings: Value[];
  created_at: string;
  finished_at: string;
  id: number;
  is_multiple_choice: boolean;
  policies: {
    store: boolean;
    publish: boolean;
    finish: boolean;
    export: boolean;
    delete: boolean;
  };
  published_at: string;
  questions: Question[];
  statistic: {
    resident_count: number;
    voted_count: number;
    voted_percent: number;
    not_voted_count: number;
  };
  status: { slug: string; label: string };
  title: string;
  updated_at: string;
}

export interface Question {
  id: number;
  title: null | string;
  options: QuestionOption[];
}

export interface QuestionOption {
  id: number;
  title: string;
  vote_statistic: {
    count: number;
    percent: number;
  };
}
