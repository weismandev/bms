export interface GetStatisticsPayload {
  poll_id: number;
  start_date?: string;
  finish_date?: string;
  export?: number;
}

export interface GetStatisticsResponse {
  poll: {
    id: number;
    residents: Resident[];
    statistic: Statistic;
    statistic_by_days: DayStatistic[];
  };
}

export interface Resident {
  id: number;
  full_name: string;
  apartment: {
    id: number;
    address: string;
  };
  status: {
    date: string;
    label: string;
    slug: string;
  };
}

export interface Statistic {
  not_voted_count: number;
  resident_count: number;
  voted_count: number;
  voted_percent: number;
}

export interface DayStatistic {
  date: string;
  viewed_count: number;
  voted_count: number;
}

export interface GeneralStatistic {
  count: number;
  coverage: number;
  voted: number;
  notVoted: number;
}

export interface IFiltredDates {
  date: string;
  count: number;
}

export interface FilterDates {
  start_date: string;
  finish_date: string;
}
