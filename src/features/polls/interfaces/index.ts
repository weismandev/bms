export * from './polls';
export * from './poll';
export * from './statistics';
export * from './staticstics-popup';

export interface Value {
  id: number;
  title: string;
}
