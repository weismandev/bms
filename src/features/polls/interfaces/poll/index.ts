import { PollItem } from '../polls';

export interface PollPayload {
  poll_id: number;
}

export interface GetPollResponse {
  poll: PollItem;
}

export interface CreateUpdatePollPayload {
  id?: number;
  title: string;
  active_to: string;
  is_multiple_choice: number;
  buildings: number[];
  questions: {
    id: number;
    title: string;
    options: { id: number; title: string }[];
  }[];
  published?: number;
}

export interface ExportPollResults {
  poll_id: number;
  export: number;
}
