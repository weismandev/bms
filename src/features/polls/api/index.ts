import { api } from '@api/api2';
import {
  GetPollsPayload,
  PollPayload,
  CreateUpdatePollPayload,
  GetStatisticsPayload,
  ExportPollResults,
} from '../interfaces';

const getPolls = (payload: GetPollsPayload) => api.v4('get', 'polls/list', payload);

const getPoll = (payload: PollPayload) => api.v4('get', 'polls/view', payload);

const createUpdatePoll = (payload: CreateUpdatePollPayload) =>
  api.v4('post', 'polls/store', payload);

const deletePoll = (payload: PollPayload) => api.v4('post', 'polls/delete', payload);

const finishPoll = (payload: PollPayload) => api.v4('post', 'polls/finish', payload);

const getStatistics = (payload: GetStatisticsPayload) =>
  api.v4('get', 'polls/statistic', payload);

const exportPollStatistics = (payload: GetStatisticsPayload) =>
  api.v4('get', 'polls/statistic', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });

const exportPollResults = (payload: ExportPollResults) =>
  api.v4('get', 'polls/view', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });

export const pollsApi = {
  getPolls,
  getPoll,
  createUpdatePoll,
  deletePoll,
  finishPoll,
  getStatistics,
  exportPollResults,
  exportPollStatistics,
};
