import {
  ComplexListPrepand,
  ComplexListSchedulePayload,
  BuildingSchedulePrepand,
  BuildingSchedulePayload,
  CategorySchedulePrepand,
  CategorySchedulePayload
} from '../types';

function prependComplexList(
  { filters, ...payload }: ComplexListPrepand
): ComplexListSchedulePayload {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { complex_ids, building_ids, category_ids } = filters;
  return {
    ...payload,
    complex_ids: complex_ids.map(({ id }) => id),
    building_ids: building_ids.map(({ id }) => id),
    category_ids: category_ids.map(({ id }) => id)
  };
}

function prependBuildingList(
  { filters, ...payload }: BuildingSchedulePrepand
): BuildingSchedulePayload {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { building_ids, category_ids } = filters;
  return {
    ...payload,
    building_ids: building_ids.map(({ id }) => id),
    category_ids: category_ids.map(({ id }) => id)
  };
}

function prependCategoryList(
  { filters, ...payload }: CategorySchedulePrepand
): CategorySchedulePayload {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { category_ids } = filters;
  return {
    ...payload,
    category_ids: category_ids.map(({ id }) => id)
  };
}

export { prependComplexList, prependBuildingList, prependCategoryList };
