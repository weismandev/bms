import { SchedulePayload } from '../types';

type Option = {
  id: number;
  title: string;
};

type Payload = {
  year: number;
  search?: string;
  filters: {
    complex_ids: Option[];
    building_ids: Option[];
    category_ids: Option[];
  };
};

function preparePayload<T>({
  filters: { complex_ids, building_ids, category_ids },
  ...payalod
}: Payload & T): T & SchedulePayload {
  return {
    ...payalod,
    filters: {
      complex_ids: complex_ids.map(({ id }) => id),
      building_ids: building_ids.map(({ id }) => id),
      category_ids: category_ids.map(({ id }) => id),
    }
  };
}

export { preparePayload };
