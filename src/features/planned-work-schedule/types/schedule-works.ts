import { statusColor } from '@features/tickets/models';

export type WorkTypes = {
  id: number;
  title: string;
};

export type Devices = {
  id: number;
  title: string;
  model_title: string;
  serial_number: string;
};

export type RepeatType = {
  id: number;
  slug: 'day' | 'week' | 'month' | 'year';
};

export type Ticket = {
  id: number;
  number: string;
  description: string;
  scheduled_date: string;
  create_at: string;
  sla_date: string | null;
  actual_end_at: string | null;
  status_name: keyof typeof statusColor;
  status_title: string;
};

export type PlannedWork = {
  id: number;
  archived_at: string | null;
  created_at: string | null;
  deleted_at: string | null;
  repeat_frequency: number[];
  repeat_type: RepeatType;
  repeat_until_at: string | null;
  service_first_at: string | null;
  devices: Devices[];
  work_types: WorkTypes[];
};
