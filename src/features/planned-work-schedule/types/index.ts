import { PlannedWork, Ticket } from './schedule-works';

export * from './object-tree';
export * from './schedule-works';

type FoundationList = {
  year: number;
  search?: string;
};

export type FilterValue = {
  id: number,
  title: string;
};

export type Filters = {
  complex_ids: FilterValue[];
  building_ids: FilterValue[];
  category_ids: FilterValue[];
};

export type ComplexListPrepand = FoundationList & {
  filters: Filters;
};

export type ComplexListSchedulePayload = {
  year: number;
  search?: string;
  complex_ids: number[];
  building_ids: number[];
  category_ids: number[];
};

export type ScheduleResponce = Raw[];

export type Raw = {
  id: number;
  title: string;
  schedule_counts: number[][];
  schedule_total_count: number;
  children?: Raw[];
  serial_number?: string;
};

export type BuildingSchedulePrepand = FoundationList & {
  complex_id: number;
  filters: Filters;
};

export type BuildingSchedulePayload = Omit<BuildingSchedulePrepand, 'filters'> & {
  building_ids: number[];
  category_ids: number[];
};

export type BuildingScheduleResponce = {
  id: number;
  address: string;
  schedule_counts: number[];
  schedule_total_count: number;
  children?: Raw[];
}[];

export type CategorySchedulePrepand = FoundationList & {
  building_id: number;
  filters: Filters;
};

export type CategorySchedulePayload = Omit<CategorySchedulePrepand, 'filters'> & {
  category_ids: number[];
};

export type CategoryScheduleResponce = {
  id: number;
  title: string;
  schedule_counts: number[];
  schedule_total_count: number;
  children?: Raw[];
}[];

export type DeviceSchedulePayload = ComplexListSchedulePayload & {
  building_id: number;
  category_id: number;
};

export type DeviceScheduleResponce = {
  id: number;
  name: string;
  schedule_counts: number[];
  schedule_total_count: number;
}[];

export type TicketSchedule = {
  date_from: string;
  date_to: string;
  device_serial_number?: string;
};

export type TicketSchedulePayload = TicketSchedule & ComplexListSchedulePayload;

export type ScheduledWorksResponce = {
  date: string;
  planned_work: PlannedWork;
  ticket: Ticket | Ticket[];
};
