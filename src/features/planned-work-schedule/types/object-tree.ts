export type Depth = 'complex' | 'building' | 'category' | 'device';

export type TreeItemType = {
  id: number;
  title: string;
  depth: Depth;
  schedule_counts?: number[];
  schedule_total_count?: number;
  serial_number?: string;
  children?: any[];
};
