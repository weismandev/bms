import { useState, useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { nanoid } from 'nanoid';

import { Box, Typography, Divider } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { TreeView } from '@mui/x-tree-view';

import {
  CorporateFareRounded as ComplexIcon,
  CottageOutlined as BuildingIcon,
  ListAlt as CategoryIcon,
} from '@mui/icons-material';

import { Loader } from '@ui/index';

import {
  $isLoading,
  $objectTree,
  $isLoadingObject,
  $year,
  getBuildingList,
  getCategoryList,
  getDeviceList,
  changedVisibilityChildren,
} from '../../models';
import { TreeItemType, Depth } from '../../types';

import {
  TreeItemStyled,
  TreeItemLabelStyled,
  EmptyStyled,
  WrapperLoaderStyled,
  BackdropStyled
} from './styled';

type Node = {
  id: number;
  depth: string;
  serial_number?: string;
  parentLevel?: string;
  parentId?: number;
};

type Tree = Node & {
  children: Tree[];
};

function insertSliceTree(tree: Tree[], node: Node): Tree[] {
  for (let i = 0; i < tree.length; i++) {
    if (tree[i].id === node.parentId && tree[i].depth === node.parentLevel) {
      tree[i].children.push({ ...node, children: [] });
      break;
    } else if (Array.isArray(tree[i].children)) {
      insertSliceTree(tree[i].children, node);
    }
  }
  return tree;
}

function removeSliceTree(tree: Tree[], node: Node): Tree[] {
  for (let i = 0; i < tree.length; i++) {
    if (tree[i].id === node.parentId && tree[i].depth === node.parentLevel) {
      const founded = tree[i].children.findIndex((item) => item.id === node.id);
      tree[i].children.splice(founded, 1);
      break;
    } else if (Array.isArray(tree[i].children)) {
      removeSliceTree(tree[i].children, node);
    }
  }
  return tree;
}

function toFlatStruct(expanded: string[], tree: Tree[]): string[] {
  for (let i = 0; i < tree.length; i++) {
    const { children, ...node } = tree[i];
    expanded.push(JSON.stringify(node));
    if (Array.isArray(children) && children.length > 0) {
      toFlatStruct(expanded, children);
    }
  }
  return expanded;
}

const LabelIcon = {
  complex: ComplexIcon,
  building: BuildingIcon,
  category: CategoryIcon,
  device: undefined,
};

const TreeItemLabel = (iconName: Depth, labelText: string) => (
  <TreeItemLabelStyled>
    <Box component={LabelIcon[iconName]} color="#AAAAAA" sx={{ mr: 1 }} style={{ zIndex: 2 }} />
    <div style={{ overflow: 'hidden' }}>
      <Typography sx={{ color: '#7D7D8E', fontWeight: 500, fontSize: 14 }}>
        {labelText}
      </Typography>
    </div>
    <Divider />
  </TreeItemLabelStyled>
);

const ObjectTree: FC = () => {
  const { t } = useTranslation();
  const isLoading = useStore($isLoading);
  const isLoadingObject = useStore($isLoadingObject);
  const objectTree = useStore($objectTree);
  const year = useStore($year);

  const [expanded, setExpanded] = useState<string[]>([]);
  const [expandedTree, setExpandedTree] = useState<Tree[]>([]);

  const handleDeviceClick = (serialnumber: string) => {
    window.open(`/equipment/?serialnumber=${serialnumber}`, '_blank');
  };

  const handleHover = (nodeId: string, title: string, type: 'hover' | 'unhover') =>
    (event: React.SyntheticEvent<HTMLElement, MouseEvent>) => {
      const node: { depth: string, serial_number: string } = JSON.parse(nodeId);
      if (event.target instanceof HTMLElement && event.target.parentNode instanceof HTMLElement) {
        const textEl = event.target.parentNode.querySelector('p');
        if (textEl === null) return;
        const isDevice = node.depth === 'device';
        if (isDevice && type === 'hover') {
          textEl.textContent = `${title} (${node.serial_number})`;
        } else if (isDevice && type === 'unhover') {
          textEl.textContent = title;
        }
        if (textEl.clientWidth < textEl.scrollWidth) {
          textEl.classList.add('translate-text');
        }
      }
    };

  const updateStateAfterRemove = (newExpandedTree: Tree[]) => {
    const newExpanded = toFlatStruct([], newExpandedTree);
    setExpanded(newExpanded);
    setExpandedTree(newExpandedTree);
    changedVisibilityChildren({
      forceUpdateStore: nanoid(),
      expandedTree: newExpandedTree
    });
  };

  const handleRemoveNode = (node: Node) => {
    if (node.depth !== 'complex') {
      const newExpandedTree = removeSliceTree(expandedTree, node);
      updateStateAfterRemove(newExpandedTree);
    } else {
      const newExpandedTree = expandedTree
        .filter((item) => item.id !== node.id && item.depth === node.depth);
      updateStateAfterRemove(newExpandedTree);
    }
  };

  const handleInsertNode = (node: Node) => {
    setExpanded(expanded.concat(JSON.stringify(node)));
    if (node.depth !== 'complex') {
      const newExpandedTree = insertSliceTree(expandedTree, node);
      setExpandedTree(newExpandedTree);
      changedVisibilityChildren({
        forceUpdateStore: nanoid(),
        expandedTree: newExpandedTree
      });
    } else {
      const newExpandedTree = expandedTree.concat({ ...node, children: [] });
      setExpandedTree(newExpandedTree);
      changedVisibilityChildren({
        forceUpdateStore: nanoid(),
        expandedTree: newExpandedTree
      });
    }
  };

  const expandList = (node: Node) => {
    switch (node.depth) {
      case 'complex':
        getBuildingList({ complex_id: node.id });
        break;
      case 'building':
        getCategoryList({ building_id: node.id });
        break;
      case 'category':
        if (node.parentLevel === 'building' && node.parentId) {
          getDeviceList({ category_id: node.id, building_id: node.parentId });
        }
        break;
      case 'device':
        break;
      default:
        break;
    }
  };

  const onNodeSelect = (_: React.SyntheticEvent, nodeId: string) => {
    const node: Node = JSON.parse(nodeId);
    if (expanded.includes(nodeId)) {
      handleRemoveNode(node);
    } else {
      handleInsertNode(node);
      expandList(node);
    }
    if (node.depth === 'device' && node.serial_number) {
      handleDeviceClick(node.serial_number);
    }
  };

  const renderTree = (
    objectTreeData: TreeItemType[],
    parent: { parentLevel: Depth; parentId: number } | null
  ) =>
    objectTreeData.map(({ id, depth, title, serial_number = '', children }) => {
      const nodeId = JSON.stringify({
        ...(parent || {}),
        ...(serial_number ? { serial_number } : {}),
        depth,
        id,
      });
      const label = TreeItemLabel(depth, title);
      return (
        <TreeItemStyled
          key={id}
          nodeId={nodeId}
          label={label}
          onMouseOver={handleHover(nodeId, title, 'hover')}
          onMouseOut={handleHover(nodeId, title, 'unhover')}
        >
          {Array.isArray(children) && children.length > 0
            ? renderTree(children, { parentLevel: depth, parentId: id })
            : depth !== 'device' && <div />}
        </TreeItemStyled>
      );
    });

  useEffect(() => () => {
      setExpanded([]);
      setExpandedTree([]);
    }, [year]);

  if (isLoading) {
    return (
      <WrapperLoaderStyled>
        <Loader isLoading={isLoading} />
      </WrapperLoaderStyled>
    );
  }
  if (objectTree.length === 0) {
    return (
      <EmptyStyled>{t('ScheduledMaintenanceNotScheduled')}</EmptyStyled>
    );
  }

  return (
    <Box style={{ position: 'sticky', left: 0 }}>
      <BackdropStyled open style={{ paddingTop: 90, width: '95%' }}>
        <Loader isLoading={isLoadingObject} />
        <TreeView
          aria-label="rich object"
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
          sx={{ flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
          onNodeSelect={onNodeSelect}
          expanded={expanded}
        >
          {renderTree(objectTree, null)}
        </TreeView>
      </BackdropStyled>
    </Box>
  );
};

export { ObjectTree };
