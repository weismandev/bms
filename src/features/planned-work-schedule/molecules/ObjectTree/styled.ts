import styled from '@emotion/styled';
import { TreeItem } from '@mui/x-tree-view';
import Backdrop from '@mui/material/Backdrop';

export const BackdropStyled = styled(Backdrop)`
  position: absolute;
  background-color: white;
  align-items: flex-start;
  left: auto;
  top: auto;
  bottom: auto;
  right: auto;
`;

export const TreeItemStyled = styled(TreeItem)`
  & .MuiTreeItem-content,
  .Mui-expanded {
    margin-bottom: 5px;
  }
`;

export const TreeItemLabelStyled = styled.div`
  display: flex;
  align-items: center;
  text-align: center;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  border-bottom: 1px solid #f4f7fa;
  padding: 9px;
  height: 40px;

  p {
    transition-delay: 0s;
    transition-duration: 2s;
    transition-property: translateX,
  }

  &:hover .translate-text {
    transform: translateX(-150px);
  }
`;

export const EmptyStyled = styled.div`
  color: #aaaaaa;
  font-weight: 500;
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const WrapperLoaderStyled = styled.div`
  position: relative;
`;
