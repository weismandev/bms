import { eachWeekOfInterval, getISOWeek, isEqual, getDay } from 'date-fns';

/* Return array of weeks by years */

interface IDaysInMonth {
  start: Date;
  end: Date;
}

const daysofWeek = [1, 2, 3, 4];

const daysInMonth = (year: number, month: number): IDaysInMonth => {
  const start = new Date(year, month, 1);
  const end = new Date(year, month + 1, 0);
  return { start, end };
};

const eachWeekOfMonth = (interval: IDaysInMonth): Date[] => {
  return eachWeekOfInterval({ ...interval }, { weekStartsOn: 1 });
};

const formatDateToNumberWeek = (weeksOfMonth: Date[]): number[] =>
  weeksOfMonth.map(getISOWeek);

const getWeeks = (year: number) => {
  const daysInMonths = Array(12).fill(year).map(daysInMonth);
  const weeksOfMonth = daysInMonths.map(eachWeekOfMonth);
  return weeksOfMonth
    .reduce((acc: Date[][], curr: Date[], monthIndex: number) => {
      const prevMonth = acc.at(-1);
      if (prevMonth instanceof Array) {
        const lastWeekMonth = prevMonth.at(-1) || 0;
        const firstWeekMonth = curr.at(0) || -1;
        if (isEqual(lastWeekMonth, firstWeekMonth)) {
          const firstDayCurrMonth = daysInMonths[monthIndex].start;
          if (daysofWeek.includes(getDay(firstDayCurrMonth))) {
            const shiftPrevMonth = acc.pop() || [];
            shiftPrevMonth?.pop();
            return acc.concat([shiftPrevMonth], [curr]);
          }
          curr.shift();
          return acc.concat([curr]);
        }
        return acc.concat([curr]);
      }
      return acc.concat([curr]);
    }, [])
    .map(formatDateToNumberWeek);
};

export { getWeeks };
