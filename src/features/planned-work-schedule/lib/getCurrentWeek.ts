import { getISOWeek } from 'date-fns';

export type PositionCurrentWeekType = {
  month: number;
  week: number;
};

const isExistsWeekInThisYear = (position: boolean | PositionCurrentWeekType): position is PositionCurrentWeekType => {
  return position instanceof Object && 'month' in position && 'week' in position;
};

const getCurrentWeek = (year: number): boolean | PositionCurrentWeekType => {
  const date = new Date();
  if (year !== date.getFullYear()) return false;
  return {
    month: date.getMonth(),
    week: getISOWeek(date),
  };
};

export { getCurrentWeek, isExistsWeekInThisYear };
