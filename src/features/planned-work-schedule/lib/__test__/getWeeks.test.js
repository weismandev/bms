import { getWeeks } from '../getWeeks';

describe('get production weeks', () => {
  const weeks = getWeeks(2022);

  test('months of 2022 years', () => {
    expect(weeks.length).toEqual(12);
    expect(weeks[11]).toEqual([48, 49, 50, 51, 52]);
  });

  test('production week in december 2022 years', () => {
    expect(weeks[11]).toEqual([48, 49, 50, 51, 52]);
  });
});
