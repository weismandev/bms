import { isValid, format } from 'date-fns';

const FORMAT = 'dd.MM.yyyy';

const formatDate = (dateStr: string, customFormat?: string): string => {
  const date = new Date(dateStr);
  const isValidDate = isValid(date);
  if (!isValidDate) return '\xa0';
  return format(date, customFormat ?? FORMAT);
};

export { formatDate };
