import { parse, addDays, format } from 'date-fns';
import { convertNumberToHours } from '@tools/convertNumberToHours';

type Parsed = {
  date_from: string;
  date_to: string;
};

const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

const parseWeekToDate = (year: number, week: number): Parsed => {
  const dateFrom = parse(String(week), 'I', new Date());
  const dateTo = addDays(dateFrom, 6);
  dateTo.setHours(23, 59, 59);

  return {
    date_from: `${format(dateFrom, FORMAT)}${convertNumberToHours(
      dateFrom.getTimezoneOffset()
    )}`,
    date_to: `${format(dateTo, FORMAT)}${convertNumberToHours(
      dateTo.getTimezoneOffset()
    )}`,
  };
};

export { parseWeekToDate };
