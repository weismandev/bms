import { api } from '@api/api2';

import {
  ComplexListSchedulePayload,
  ScheduleResponce,
  BuildingSchedulePayload,
  BuildingScheduleResponce,
  CategorySchedulePayload,
  CategoryScheduleResponce,
  DeviceSchedulePayload,
  DeviceScheduleResponce,
  TicketSchedulePayload,
  ScheduledWorksResponce,
} from '../types';

const complexListSchedule = (
  payload: ComplexListSchedulePayload
): Promise<ScheduleResponce> =>
  api.v4('get', '/planned-work/complex-list-schedule-counts', payload);

const buildingListSchedule = (
  payload: BuildingSchedulePayload
): Promise<BuildingScheduleResponce> =>
  api.v4('get', '/planned-work/building-list-schedule-counts', payload);

const categoryListSchedule = (
  payload: CategorySchedulePayload
): Promise<CategoryScheduleResponce> =>
  api.v4('get', '/planned-work/device-category-list-schedule-counts', payload);

const deviceListSchedule = (
  payload: DeviceSchedulePayload
): Promise<DeviceScheduleResponce> =>
  api.v4('get', '/planned-work/device-list-schedule-counts', payload);

const getTicketSchedule = (
  payload: TicketSchedulePayload
): Promise<ScheduledWorksResponce[]> =>
  api.no_vers('get', 'v4/planned-work/ticket-schedule-list-for-plot-card', payload);

export const maintenanceApi = {
  complexListSchedule,
  buildingListSchedule,
  categoryListSchedule,
  deviceListSchedule,
  getTicketSchedule,
};
