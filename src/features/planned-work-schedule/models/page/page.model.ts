import { createEffect, createStore, createEvent, combine, restore } from 'effector';
import { createGate } from 'effector-react';
import {
  ComplexListSchedulePayload,
  ScheduleResponce,
  BuildingSchedulePayload,
  BuildingScheduleResponce,
  CategorySchedulePayload,
  CategoryScheduleResponce,
  DeviceSchedulePayload,
  DeviceScheduleResponce,
  Raw,
  Depth,
} from '../../types';

type Raws = Raw & {
  depth: Depth;
};

type Node = {
  id: number;
  depth: string;
  serial_number?: string;
  parentLevel?: string;
  parentId?: number;
};

type ScheduleCell = {
  id: number;
  level: Depth;
  disabled: boolean;
  visibilityChildren: boolean;
  scheduleCounts: number[][];
  scheduleTotalCount: number;
  serial_number?: string;
};

type Tree = {
  id: number;
  depth: Depth;
  serial_number?: string;
  parentLevel?: string;
  parentId?: number;
  children: Tree[];
};

type VisibilityTree = {
  forceUpdateStore: string;
  expandedTree: Tree[];
};

type PathRoot = {
  category_ids: number[];
  building_ids: number[];
  complex_ids: number[];
};

// eslint-disable-next-line prefer-destructuring
export const API_URL = process.env.API_URL;

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

export const fxComplexListSchedule = createEffect<
  ComplexListSchedulePayload,
  ScheduleResponce,
  Error
>();
export const fxBuildingListSchedule = createEffect<
  BuildingSchedulePayload,
  BuildingScheduleResponce,
  Error
>();
export const fxCategoryListSchedule = createEffect<
  CategorySchedulePayload,
  CategoryScheduleResponce,
  Error
>();
export const fxDeviceListSchedule = createEffect<
  DeviceSchedulePayload,
  DeviceScheduleResponce,
  Error
>();

export const changePageTab = createEvent<string>();
export const changedErrorDialogVisibility = createEvent<boolean>();
export const changeYear = createEvent<number>();
export const searchChanged = createEvent<string>();
export const getBuildingList = createEvent<{ complex_id: number }>();
export const getCategoryList = createEvent<{ building_id: number }>();
export const getDeviceList = createEvent<{ building_id: number; category_id: number }>();
export const changedVisibilityChildren = createEvent<VisibilityTree>();
export const exportWorks = createEvent<void>();

const $isLoading = createStore<boolean>(false);
const $isLoadingObject = createStore<boolean>(false);
const $error = createStore<null | Error>(null);
const $isErrorDialogOpen = createStore<boolean>(false);
const $year = createStore<number>(new Date().getFullYear());
export const $raw = createStore<Raws[] | []>([]);

export const $search = restore(searchChanged, '');
export const $complexId = restore(getBuildingList, null);
export const $visibilityChildren = restore<VisibilityTree>(changedVisibilityChildren, {
  forceUpdateStore: '',
  expandedTree: [],
});

export const $totalCount = combine($raw, (raw) =>
  raw.reduce((acc, curr) => acc + curr.schedule_total_count, 0)
);

const depthPathRoot = {
  complex: 'complex_id',
  category: 'category_id',
  building: 'building_id',
  device: 'device_ids',
};

const getPathRootByDepth = (depth: Depth): string => depthPathRoot[depth];

function merge(
  raw: Raws[],
  nodes: Tree[],
  cell: ScheduleCell[] = [],
  pathRoot: PathRoot
): ScheduleCell[] {
  for (let i = 0; i < raw.length; i++) {
    const path = {
      ...pathRoot,
      [getPathRootByDepth(raw[i].depth)]: raw[i].id,
    };
    const foundedNode = nodes.find(
      (node) => node.id === raw[i].id && node.depth === raw[i].depth
    );
    if (foundedNode) {
      cell.push({
        ...raw[i],
        disabled: true,
        visibilityChildren: true,
      });
      merge(raw[i]?.children || [], foundedNode.children, cell, path);
    } else {
      cell.push({
        ...raw[i],
        pathRoot: path,
        disabled: false,
        visibilityChildren: true,
      });
    }
  }
  return cell;
}

export const $scheduleCounts = combine(
  $raw,
  $visibilityChildren,
  (raw, { expandedTree }) => merge(raw, expandedTree, [])
);

export {
  $error,
  $isLoading,
  $isLoadingObject,
  PageGate,
  pageMounted,
  pageUnmounted,
  $isErrorDialogOpen,
  $year,
};
