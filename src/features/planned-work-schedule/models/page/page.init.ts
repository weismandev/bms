import { sample, merge } from 'effector';
import { throttle } from 'patronum/throttle';
import { signout, $token, $timezone } from '@features/common';
import { maintenanceApi } from '../../api';
import {
  prependComplexList,
  prependBuildingList,
  prependCategoryList,
} from '../../prepare-data/prepand-schedule';
import { $filters } from '../filter';
import {
  pageMounted,
  fxComplexListSchedule,
  fxBuildingListSchedule,
  fxCategoryListSchedule,
  fxDeviceListSchedule,
  $isLoading,
  $isLoadingObject,
  $raw,
  $year,
  $search,
  $visibilityChildren,
  changeYear,
  getBuildingList,
  getCategoryList,
  getDeviceList,
  searchChanged,
  exportWorks,
  API_URL,
} from './page.model';

fxComplexListSchedule.use(maintenanceApi.complexListSchedule);
fxBuildingListSchedule.use(maintenanceApi.buildingListSchedule);
fxCategoryListSchedule.use(maintenanceApi.categoryListSchedule);
fxDeviceListSchedule.use(maintenanceApi.deviceListSchedule);

const throttledSearch = throttle({
  source: searchChanged,
  timeout: 1000,
});

const pendingOccured = merge([
  fxBuildingListSchedule.pending,
  fxCategoryListSchedule.pending,
  fxDeviceListSchedule.pending,
]);

$visibilityChildren.reset([changeYear, signout]);
$isLoading.on(fxComplexListSchedule.pending, (_, pending) => pending);
$isLoadingObject.on(pendingOccured, (_, pending) => pending);
$year.on(changeYear, (_, year) => year);

$raw
  .on(fxComplexListSchedule.done, (_, { result }) =>
    Array.isArray(result)
      ? result.map((complex) => ({ ...complex, depth: 'complex' }))
      : []
  )
  .on(fxBuildingListSchedule.done, (state, { params: { complex_id }, result }) => {
    const children = result.map(({ address, ...item }) => ({
      ...item,
      title: address,
      depth: 'building',
    }));
    return state.map((item) => (item.id !== complex_id ? item : { ...item, children }));
  })
  .on(fxCategoryListSchedule.done, (state, { params: { building_id }, result }) => {
    const children = result.map((category) => ({
      ...category,
      depth: 'category',
    }));
    return state.map((complex) => ({
      ...complex,
      children: complex.children?.map((building) =>
        building.id !== building_id ? building : { ...building, children }
      ),
    }));
  })
  .on(
    fxDeviceListSchedule.done,
    (state, { params: { building_id, category_id }, result }) => {
      const children = result.map(({ name, ...device }) => ({
        ...device,
        title: name || 'Без названия',
        depth: 'device',
      }));
      return state.map((complex) => ({
        ...complex,
        children: complex.children?.map((building) =>
          building.id !== building_id
            ? building
            : {
                ...building,
                children: building.children?.map((category) =>
                  category.id !== category_id ? category : { ...category, children }
                ),
              }
        ),
      }));
    }
  )
  .reset([changeYear, signout]);

sample({
  source: { year: $year, search: $search, filters: $filters },
  clock: [pageMounted, $filters, changeYear, throttledSearch],
  target: fxComplexListSchedule.prepend(prependComplexList),
});

sample({
  source: { year: $year, filters: $filters },
  clock: getBuildingList,
  fn: (sourceData, clockData) => ({ ...sourceData, ...clockData }),
  target: fxBuildingListSchedule.prepend(prependBuildingList),
});

sample({
  source: { year: $year, filters: $filters },
  clock: getCategoryList,
  fn: (sourceData, clockData) => ({ ...sourceData, ...clockData }),
  target: fxCategoryListSchedule.prepend(prependCategoryList),
});

sample({
  source: { year: $year },
  clock: getDeviceList,
  fn: (sourceData, clockData) => ({ ...sourceData, ...clockData }),
  target: fxDeviceListSchedule,
});

sample({
  clock: exportWorks,
  source: { year: $year, token: $token, timezone: $timezone },
  fn: ({ year, token, timezone }) => {
    window.open(
      `${API_URL}/v4/planned-work/export?year=${year}&token=${token}&timezone=${timezone}`
    );
  },
});
