import { createEvent, createStore } from 'effector';
import { createFilterBag } from '@tools/factories';

type SlicedFilter = {
  complex_ids: number[];
  building_ids: number[];
  category_ids: number[];
};

const defaultFilters = {
  complex_ids: [],
  building_ids: [],
  category_ids: [],
};

export const $slicedFilters = createStore<SlicedFilter>(defaultFilters);

const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

export const changedGroupVisibility = createEvent();
export const sliceFilters = createEvent();

export { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters };
