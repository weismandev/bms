import { sample } from 'effector';
import { maintenanceApi } from '../../api';

import { $filters } from '../filter';
import { $year, $search } from '../page';
import { fxTicketScheduleList, $scheduleWorks, $isLoadingDetail, getTicketScheduleList } from './detail.model';

import { preparePayload } from '../../prepare-data/prepare-payload';

fxTicketScheduleList.use(maintenanceApi.getTicketSchedule);

$isLoadingDetail.on(fxTicketScheduleList.pending, (_, loading) => loading);
$scheduleWorks.on(fxTicketScheduleList.done, (_, { result }) =>
  (Array.isArray(result) ? result : []));

sample({
  source: { year: $year, search: $search, filters: $filters },
  clock: getTicketScheduleList,
  fn: (sourceData, { complex_id, building_id, category_id, ...clockData }) => {
    const { filters, ...payload } = preparePayload(sourceData);
    const mergedFilters = mergePrepare(filters, {
      complex_id, building_id, category_id
    });
    return { ...mergedFilters, ...payload, ...clockData };
  },
  target: fxTicketScheduleList,
});

function mergePrepare(filters, { complex_id, building_id, category_id }) {
  const { complex_ids = [], building_ids = [], category_ids = [] } = filters;
  if (complex_id) {
    if (!complex_ids.some((id) => id === complex_id)) {
      complex_ids.push(complex_id);
    }
  }
  if (building_id) {
    if (!building_ids.some((id) => id === complex_id)) {
      building_ids.push(building_id);
    }
  }
  if (category_id) {
    if (!category_ids.some((id) => id === complex_id)) {
      category_ids.push(category_id);
    }
  }
  return {
    ...(complex_ids.length > 0 ? { complex_ids } : {}),
    ...(building_ids.length > 0 ? { building_ids } : {}),
    ...(category_ids.length > 0 ? { category_ids } : {}),
  };
}
