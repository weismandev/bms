import { createEffect, createEvent, createStore } from 'effector';
import { createDetailBag } from '@tools/factories';

import {
  TicketSchedule,
  TicketSchedulePayload,
  ScheduledWorksResponce,
} from '../../types';

type DispatchTicketSchedule = TicketSchedule & {
  complex_id?: number,
  building_id?: number,
  category_id?: number
};

export const fxTicketScheduleList = createEffect<
TicketSchedulePayload,
ScheduledWorksResponce,
Error
>();

export const getTicketScheduleList = createEvent<DispatchTicketSchedule>();

export const $isLoadingDetail = createStore<boolean>(false);
export const $scheduleWorks = createStore<ScheduledWorksResponce[]>([]);

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag({});
