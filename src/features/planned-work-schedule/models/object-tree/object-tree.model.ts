import { createStore, createEvent } from 'effector';

import { TreeItemType } from '../../types';

export const $objectTree = createStore<TreeItemType[]>([]);
