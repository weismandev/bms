import {
  FC,
  useRef,
  useLayoutEffect,
  SyntheticEvent,
  useState,
  useEffect,
} from 'react';
import { useStore } from 'effector-react';

import { Loader, CustomScrollbar } from '@ui/index';
import {
  $isLoading,
  $year,
  $scheduleCounts,
  $isDetailOpen,
  changedDetailVisibility,
  getTicketScheduleList,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { ObjectTree } from '../../molecules/ObjectTree';

import {
  getWeeks,
  getCurrentWeek,
  isExistsWeekInThisYear,
  parseWeekToDate,
} from '../../lib';

import {
  WrapperStyled,
  ContentStyled,
  TableStyled,
  HeaderMonthStyled,
  TitleMonthStyle,
  HeaderWeekStyled,
  EmptyColumnStyled,
  GroupCellStyled,
  CellStyled,
  TotalHeaderStyled,
  TotalCellStyled,
} from './styled';

const getMonths = (): string[] => {
  const lang: 'ru' | 'en' = localStorage.getItem('lang') || 'ru';
  const months = {
    en: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],
    ru: [
      'Янв',
      'Фев',
      'Мар',
      'Апр',
      'Мая',
      'Июн',
      'Июл',
      'Авг',
      'Сен',
      'Окт',
      'Ноя',
      'Дек',
    ]
  };
  return months[lang];
};

const scrollIntoViewOptions = (
  inline: 'center' | 'start'
): {
  block: 'nearest';
  inline: 'center' | 'start';
  behavior: 'smooth';
} => ({
  block: 'nearest',
  inline,
  behavior: 'smooth',
});

const closeIcon = `
  <svg width="14" height="14" viewBox="0 0 14 14" fill="none">
    <path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="white"/>
  </svg>
`;

const Table: FC = () => {
  const isLoading = useStore($isLoading);
  const year = useStore($year);
  const scheduleCounts = useStore($scheduleCounts);
  const isDetailOpen = useStore($isDetailOpen);

  const tableRef = useRef<HTMLDivElement>(null);
  const cellRef = useRef<any>();

  const [isPressedCell, togglePresseCell] = useState<boolean>(false);

  const weeks = getWeeks(year);
  const currentWeek = getCurrentWeek(year);

  const handleClickTable = (event: SyntheticEvent<EventTarget>): void => {
    if (
      event.target instanceof HTMLDivElement &&
      event.target.parentNode instanceof HTMLDivElement
    ) {
      const {
        dataset: { weekIndex, index },
      } = event.target;
      const {
        dataset: { monthIndex },
      } = event.target.parentNode;
      const rowIndex = Number(index);

      const valueCell = Number(event.target.textContent);
      if (weekIndex && monthIndex && !isNaN(rowIndex) && valueCell > 0) {
        const selectedSchedule = scheduleCounts[rowIndex];
        const weekNumber = weeks[Number(monthIndex)][Number(weekIndex)];
        event.target.setAttribute('textContent', event.target.textContent || '');
        event.target.innerHTML = closeIcon;
        event.target.style.backgroundColor = '#FFC078';
        cellRef.current = event.target;

        getTicketScheduleList({
          ...parseWeekToDate(year, weekNumber),
          ...(selectedSchedule.serial_number ? { device_serial_number: selectedSchedule.serial_number } : {}),
          ...(selectedSchedule ? selectedSchedule.pathRoot : {})
        });
        changedDetailVisibility(true);
        togglePresseCell(true);
      }
    }
  };

  useEffect(() => {
    if (isExistsWeekInThisYear(currentWeek)) {
      const monthsEl = tableRef.current?.querySelectorAll(`[data-month="${currentWeek.month}"]`);
      monthsEl?.forEach((el) => {
        const weekEl = el?.querySelector(`[data-week="${currentWeek.week}"]`);
        weekEl?.classList.add('current-week');
        weekEl?.scrollIntoView(scrollIntoViewOptions('center'));
      });
    } else {
      const firstElementChild = tableRef.current?.firstElementChild;
      const [weekEl] = tableRef.current?.getElementsByClassName('current-week') || [];
      weekEl?.classList.remove('current-week');
      firstElementChild?.scrollIntoView(scrollIntoViewOptions('start'));
    }
  }, [year, scheduleCounts, isPressedCell]);

  useEffect(() => {
    if (!isDetailOpen) {
      togglePresseCell(false);
      if (cellRef.current instanceof HTMLElement) {
        const textContent = cellRef.current?.getAttribute('textContent') || '';
        cellRef.current.textContent = textContent;
        cellRef.current.style.removeProperty('background-color');
      }
    }
  }, [isDetailOpen]);

  const months = getMonths();
  return (
    <WrapperStyled>
      <Loader isLoading={isLoading} />
      <TableToolbar />
      <CustomScrollbar autoHide style={{ zIndex: 3, marginTop: 24, height: 'calc(100% - 60px)' }}>
        <ContentStyled isDetailOpen={isDetailOpen}>
          <ObjectTree />
          <TableStyled ref={tableRef} onClick={handleClickTable}>
            {months.map((month, monthIndex) => (
              <HeaderMonthStyled
                key={month}
                data-month={monthIndex}
                width={weeks[monthIndex].length}
              >
                <TitleMonthStyle>{month}</TitleMonthStyle>
                <GroupCellStyled>
                  {weeks[monthIndex].map((weekNumber) => (
                    <HeaderWeekStyled key={weekNumber} data-week={weekNumber}>
                      {weekNumber}
                    </HeaderWeekStyled>
                  ))}
                </GroupCellStyled>
              </HeaderMonthStyled>
            ))}
            <TotalHeaderStyled>Итого</TotalHeaderStyled>
            {scheduleCounts
              .filter((row) => row.visibilityChildren)
              .map((row, rowIndex) => (
                <>
                  {row.schedule_counts.map((month, index) => (
                    <GroupCellStyled
                      key={index}
                      data-depth={row.depth}
                      data-month={index}
                      data-month-index={index}
                    >
                      {month.map((count: number, countIndex: number) => (
                        <CellStyled
                          key={weeks[index][countIndex]}
                          disabled={isPressedCell || row.disabled}
                          cellValue={count}
                          data-week={weeks[index][countIndex]}
                          data-week-index={countIndex}
                          data-index={rowIndex}
                        >
                          {count}
                        </CellStyled>
                      ))}
                    </GroupCellStyled>
                  ))}
                  <TotalCellStyled>{row.schedule_total_count}</TotalCellStyled>
                </>
              ))}
            {scheduleCounts.length === 0 &&
              months.map((i) => <EmptyColumnStyled key={i} />)}
          </TableStyled>
        </ContentStyled>
      </CustomScrollbar>
    </WrapperStyled>
  );
};

export { Table };
