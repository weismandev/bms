import styled from '@emotion/styled';
import { Wrapper } from '@ui/index';

type WidthColumnsMonth = {
  width: number;
};

type Cell = {
  disabled: boolean;
  cellValue: number;
};

export const WrapperStyled = styled(Wrapper)`
  height: 100%;
  padding: 24px;
  padding-bottom: 12px;
  position: relative;
  overflow: hidden;
`;

export const ContentStyled = styled.div`
  display: grid;
  grid-template-columns: ${({ isDetailOpen }: { isDetailOpen: boolean }) => isDetailOpen ? '35% 65%' : '30% 70%'};
  height: 100%;
  overflow: auto;
`;

export const TableStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(13, 1fr);
  grid-template-rows: 90px repeat(auto-fill, 45px);
  // overflow: auto;
  // overflow-y: hidden;
`;

export const HeaderMonthStyled = styled.div`
  border-radius: 16px;
  height: 88px;
  display: flex;
  flex-direction: column;
  margin-bottom: 4px;
  width: ${(props: WidthColumnsMonth) => props.width === 5 ? 225 : 180 }px;

  &:nth-child(even) {
    background-color: #f4f7fa;
  }
`;

export const EmptyColumnStyled = styled.div`
  width: 100%;
  height: 60vh;
  border-radius: 16px;
  margin-top: 6px;

  &:nth-child(even) {
    background-color: #f4f7fa;
  }
`;

export const TitleMonthStyle = styled.div`
  color: #3b3b50;
  font-weight: 900;
  font-size: 18px;
  padding: 10px;
`;

export const HeaderWeekStyled = styled.div`
  font-family: 'Roboto';
  font-weight: 500;
  font-size: 16px;
  width: 40px;
  height: 40px;
  line-height: 40px;

  &.current-week {
    color: white;
    background-color: #FFC078;
    border-radius: 3.05208px;
    transition: 0.2s background-color;
  }
`;

export const CellStyled = styled.div`
  width: 40px;
  height: 40px;
  background-color: ${({ cellValue, disabled }: Cell) =>
    (cellValue === 0 ? '#FFFFFF' : disabled ? '#D7EEFB' : '#0394E3')};
  pointer-events: ${({ disabled }: Cell) => (disabled ? 'none' : 'auto')};
  color: white;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  line-height: 40px;
  transition: background-color 0.1s ease-in-out;

  &.current-week {
    background-color: #FFC078;
    transition: unset;
  }
`;

export const GroupCellStyled = styled.div`
  display: flex;
  justify-content: space-around;
  text-align: center;
  margin-bottom: 4px;
`;

export const TotalHeaderStyled = styled.div`
  width: 100px;
  background-color: white;
  color: #0394e3;
  font-weight: 900;
  font-size: 18px;
  text-align: center;
  padding: 10px;
`;

export const TotalCellStyled = styled.div`
  color: #0394e3;
  font-weight: 500;
  font-size: 16px;
  background: #ffffff;
  text-align: center;
  height: 40px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  line-height: 40px;
  margin-left: 2px;
  margin-right: 2px;
`;
