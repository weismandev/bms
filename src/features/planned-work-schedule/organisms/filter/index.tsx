import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { useSelect } from '@entities/SelectControl/hooks/useSelect';

import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectControl,
} from '@ui/index';

import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models';

const Filter = memo(() => {
  const { t } = useTranslation();
  const filters = useStore($filters);

  const handleReset = () => filtersSubmitted('');
  const handleCloseFilter = () => changedFilterVisibility(false);

  const { data: complexes, isLoading: isLoadingComplexes } = useSelect({
    name: 'complex_ids',
    url: '/complex/list',
    path: 'items',
  });

  const { data: buildings, isLoading: isLoadingBuildings }: any = useSelect({
    name: 'building_ids',
    url: '/buildings/get-list-crm',
    path: 'buildings',
    payload: { per_page: 1000000 },
  });

  const { data: categories } = useSelect({
    name: 'category_ids',
    url: '/device_categories/get-device-catigories',
    path: 'device-catigories',
    apiVersion: 'no_vers',
  });

  const buildingsData = buildings?.reduce(
    (acc: any, house: any) =>
      (house && house.building
        ? acc.concat({ id: house.id, title: house.building.title, ...house })
        : acc),
    []
  );

  const filterBuildingsByComplex =
    (complexes: { id: number }[]) => (building: { complex: { id: number } }) =>
      complexes.some(({ id }) => id === building.complex.id);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={handleCloseFilter}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={handleReset}
          enableReinitialize
          render={({ setFieldValue, values }) => {
            const buildingOptions =
              values.complex_ids.length > 0
                ? buildingsData.filter( filterBuildingsByComplex(values.complex_ids))
                : buildingsData;
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="complex_ids"
                  label={t('AnObject')}
                  placeholder={t('selectObject')}
                  options={complexes}
                  component={SelectControl}
                  isLoading={isLoadingComplexes}
                  value={values.complex_ids}
                  onChange={(value: { id: number; title: string }[]) => {
                    setFieldValue('complex_ids', value);
                  }}
                  isMulti
                />
                <Field
                  name="building_ids"
                  label={t('Buildings')}
                  placeholder={t('selectBbuildings')}
                  options={buildingOptions}
                  component={SelectControl}
                  isLoading={isLoadingBuildings}
                  value={values.building_ids}
                  onChange={(value: { id: number; title: string }[]) => {
                    setFieldValue('building_ids', value);
                  }}
                  isMulti
                />
                <Field
                  name="category_ids"
                  label={t('Category')}
                  placeholder={t('SelectCategory')}
                  options={categories}
                  component={SelectControl}
                  value={values.category_ids}
                  onChange={(value: { id: number; title: string }[]) => {
                    setFieldValue('category_ids', value);
                  }}
                  isMulti
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
