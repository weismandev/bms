import { useStore } from 'effector-react';
import {
  FilterButton,
  ExportButton,
  Greedy,
  Toolbar,
  AdaptiveSearch,
  YearControl,
} from '@ui/index';
import {
  $isFilterOpen,
  $search,
  $totalCount,
  changedFilterVisibility,
  changeYear,
  searchChanged,
  exportWorks,
  $objectTree,
} from '../../models';

function TableToolbar() {
  const totalCount = useStore($totalCount);
  const isFilterOpen = useStore($isFilterOpen);
  const searchValue = useStore($search);
  const objectTree = useStore($objectTree);

  const mode = 'view'; // useStore($mode);
  const isEditMode = false; // mode === 'edit';
  const isAnySideOpen = isFilterOpen || isEditMode;

  const handleFilterVisibility = () => {
    changedFilterVisibility(true);
  };

  const handleChangeYear = ({ year }: { year: string }) => {
    const toNumberYear = Number(year);
    if (!isNaN(toNumberYear)) {
      changeYear(toNumberYear);
    }
  };

  return (
    <Toolbar>
      <FilterButton disabled={isFilterOpen} onClick={handleFilterVisibility} />
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <Greedy />
      <div style={{ width: 220, marginRight: 24 }}>
        <YearControl onChange={handleChangeYear} />
      </div>
      <ExportButton
        {...{ columns: [], tableData: [], hiddenColumnNames: [] }}
        onClick={exportWorks}
        active
        disabled={objectTree.length === 0}
      />
    </Toolbar>
  );
}

export { TableToolbar };
