import styled from '@emotion/styled';

import { Card, CardActions, CardHeader } from '@mui/material';
import { ActionIconButton } from '@ui/index';

type CardHeaderSubTitle = {
  isCreated: boolean;
};

export const HeaderStyled = styled.div`
  display: fles;
  justify-content: space-between;
  margin-bottom: 32px;
`;

export const TitleStyled = styled.div`
  font-weight: 700;
  font-size: 24px;
  color: #3B3B50;
`;

export const TitleCartStyled = styled.div`
  font-weight: 900;
  font-size: 18px;
  color: #3B3B50;
`;

export const WrapCardHeaderStyled = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const CardeaderTitleStyled = styled.div`
  & > span {
    color: #3B3B50;
    font-weight: 900;
    font-size: 18px;
  }
`;

export const CardStyled = styled(Card)`
  background-color: #E1EBFF;
  margin-bottom: 32px;
`;

export const CardActionsStyled = styled(CardActions)`
  padding: 0px;
  top: -7px;
  position: relative;
`;

export const CardHeaderStyled = styled(CardHeader)`
  padding-bottom: 16px;
`;

export const IconButtonStyled = styled(ActionIconButton)`
  background-color: #0394E3;
  margin-left: 24px;
`;

export const CardHeaderSubTitleStyled = styled.span`
  color: ${({ isCreated }: CardHeaderSubTitle) => (isCreated ? '#AAAAAA' : '#65657B')} !important;
  font-weight: 500 !important;
  font-size: 16px !important;
`;

export const ReadingFieldStyled = styled.div`
  margin-bottom: 24px;
  min-width: 48%;

  & p {
    margin: 0;
    margin-bottom: 16px;
    color: #AAAAAA;
    font-weight: 500;
    font-size: 14px;
  }
`;

export const ReadingChildFieldStyled = styled.div`
  display: flex;
  line-height: 8px;
`;

export const ReadingFieldChildrenStyled = styled.div`
  color: #65657B;
  font-weight: 500;
  font-size: 16px;
`;
