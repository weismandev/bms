import { FC, useState } from 'react';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { styled } from '@mui/material/styles';

import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Chip, Stack, CardContent, Collapse } from '@mui/material';
import { wordDeclension, wordGender } from '@tools/word-declension';
import { Cases } from '@tools/word-declension/word-declension.types';
import { formatAtom } from '@tools/formatAtom';

import { ReadingField } from './ReadingField';
import { TicketWork } from './TicketWork';
import { ScheduledWorksResponce } from '../../types';
import { CardStyled, WrapCardHeaderStyled, CardeaderTitleStyled, CardActionsStyled, CardHeaderStyled, ReadingChildFieldStyled, IconButtonStyled } from './styled';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const { t } = i18n;

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export const repeatTypes = [
  { id: 1, title: t('Daily'), type: 'day' },
  { id: 2, title: t('Weekly'), type: 'week' },
  { id: 3, title: t('Monthly'), type: 'month' },
  { id: 4, title: t('Annually'), type: 'year' },
];

const ScheduleWorks: FC<ScheduledWorksResponce> = ({ date, planned_work, ticket }) => {
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const workTypes = planned_work?.work_types ?? [];
  const devices = planned_work?.devices ?? [];
  const repeatType = repeatTypes.find((item) => item.id === planned_work.repeat_type.id);

  const handleClickLink = () => {
    window.open(`/planned-works/${planned_work.id}`, '_blank');
  };

  const frequency = Array.isArray(planned_work.repeat_frequency)
    ? planned_work.repeat_frequency[0]
    : planned_work.repeat_frequency;

  const { slug } = planned_work.repeat_type;

  const renderCardHeaderTitle = (
    <WrapCardHeaderStyled>
      <CardeaderTitleStyled>
        <span>{`${t('ScheduledMaintenance')} ${t('On')} ${format(new Date(date), 'dd.MM.yyyy')}`}</span>
        <IconButtonStyled onClick={handleClickLink}>
          <ExitToAppIcon sx={{ color: 'white', fontSize: 16 }} />
        </IconButtonStyled>
      </CardeaderTitleStyled>
      <CardActionsStyled disableSpacing>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActionsStyled>
    </WrapCardHeaderStyled>
  );

  return (
    <CardStyled>
      <CardHeaderStyled title={renderCardHeaderTitle} />
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <ReadingField title={t('NameOfWorks')}>
            <Stack direction="row" spacing={1} style={{ flexWrap: 'wrap', fontSize: 13 }}>
              {workTypes.map(({ id, title }) => <Chip key={id} label={title} style={{ background: '#E7E7EC' }} />)}
            </Stack>
          </ReadingField>
          <ReadingField title={t('Equipment')}>
            <Stack direction="row" spacing={1} style={{ flexWrap: 'wrap', fontSize: 13 }}>
              {devices.map(({ id, title }) => <Chip key={id} label={title} style={{ background: '#E7E7EC' }} />)}
            </Stack>
          </ReadingField>
          <WrapCardHeaderStyled>
            <ReadingField title={t('DateOfFirstService')}>
              {formatAtom(planned_work.service_first_at ?? '')}
            </ReadingField>
            <ReadingField title={t('ServiceSchedule')}>
              {repeatType?.title ?? ''}
            </ReadingField>
          </WrapCardHeaderStyled>
          <WrapCardHeaderStyled>
            <ReadingField title={t('RepeatUntil')}>
              {formatAtom(planned_work.repeat_until_at ?? '')}
            </ReadingField>
            <ReadingField title="">
              <ReadingChildFieldStyled>
                {planned_work.repeat_frequency ? (
                  <>
                    <p style={{ textTransform: 'capitalize' }}>{wordDeclension('each', frequency, Cases.NOMINATIVE, wordGender[slug])}</p>
                    &nbsp;
                    {frequency}
                    &nbsp;
                    <p>{wordDeclension(slug, frequency)}</p>
                  </>
                ) : <p>&nbsp;&nbsp;</p>}
              </ReadingChildFieldStyled>
            </ReadingField>
          </WrapCardHeaderStyled>
        </CardContent>
      </Collapse>
      {Array.isArray(ticket)
        ? ticket.map((item) => <TicketWork key={item.id} {...item} />)
        : <TicketWork {...ticket} />}
    </CardStyled>
  );
};

export { ScheduleWorks };
