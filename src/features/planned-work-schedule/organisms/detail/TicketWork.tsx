import { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';

import Collapse from '@mui/material/Collapse';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { CardContent, Divider, Chip } from '@mui/material';

import { statusColor } from '@features/tickets/models';
import { ReadingField } from './ReadingField';
import { formatDate } from '../../lib/formatDate';
import { Ticket } from '../../types';
import { WrapCardHeaderStyled, CardActionsStyled, CardHeaderStyled, CardHeaderSubTitleStyled, CardeaderTitleStyled, IconButtonStyled } from './styled';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const TicketWork: FC<Ticket> = ({ number, status_name, ...props }) => {
  const { t } = useTranslation();
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const createAt = formatDate(props.create_at, `dd.MM.yyyy '${t('in')}' HH:mm`);

  const stylesChip = {
    marginLeft: 16,
    ...statusColor[status_name] ?? {}
  };

  const handleClickLink = () => {
    window.open(`/tickets/${props.id}`, '_blank');
  };

  const renderCardHeaderTitle = (
    <WrapCardHeaderStyled>
      <CardeaderTitleStyled>
        <span>
          {`${t('Request')} ${number ? `№${number}` : ''} ${t('On')} ${formatDate(props.scheduled_date ?? '')}`}
        </span>
      </CardeaderTitleStyled>
      <CardActionsStyled disableSpacing>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActionsStyled>
    </WrapCardHeaderStyled>
  );

  const renderCreatedTicket = (
    <>
      <CardHeaderSubTitleStyled isCreated>{`${t('Created')}  ${createAt}`}</CardHeaderSubTitleStyled>
      <Chip
        label={props.status_title}
        size="medium"
        style={stylesChip}
      />
      <IconButtonStyled onClick={handleClickLink}>
        <ExitToAppIcon sx={{ color: 'white', fontSize: 16 }} />
      </IconButtonStyled>
    </>
  );
  const renderFutureTicket = (
    <>
      <CardHeaderSubTitleStyled isCreated={false}>
        {`${t('WillBeCreatedAutomatically')} ${createAt}`}
      </CardHeaderSubTitleStyled>
      <Divider style={{ marginTop: 10, width: '90%' }} />
    </>
  );

  return (
    <Card style={{ backgroundColor: '#E1EBFF', marginBottom: 16, boxShadow: 'none', paddingLeft: 24 }}>
      <CardHeaderStyled
        title={renderCardHeaderTitle}
        subheader={number ? renderCreatedTicket : renderFutureTicket}
      />
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <WrapCardHeaderStyled>
            <ReadingField title={t('DateOfFirstService')}>
              {formatDate(props?.actual_end_at ?? '')}
            </ReadingField>
            <ReadingField title={t('ServiceSchedule')}>
              {formatDate(props.sla_date ?? '')}
            </ReadingField>
          </WrapCardHeaderStyled>
          <ReadingField title={t('ServiceSchedule')}>
            {props.description}
          </ReadingField>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export { TicketWork };
