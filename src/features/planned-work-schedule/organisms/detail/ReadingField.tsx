import { FC, ReactNode } from 'react';
import { Divider } from '@mui/material';
import { ReadingFieldStyled, ReadingFieldChildrenStyled } from './styled';

type Props = {
  title: string;
  children: ReactNode;
};

const ReadingField: FC<Props> = ({ title, children }) => (
  <ReadingFieldStyled>
    <p>{title || '\xa0'}</p>
    <ReadingFieldChildrenStyled>{children}</ReadingFieldChildrenStyled>
    <Divider style={{ marginTop: 20 }} />
  </ReadingFieldStyled>
);

export { ReadingField };
