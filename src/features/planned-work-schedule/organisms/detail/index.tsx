import { memo, FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

import { Wrapper, CustomScrollbar, Loader } from '@ui/index';
import { $scheduleWorks, $isLoadingDetail, changedDetailVisibility } from '../../models';

import { ScheduleWorks } from './scheduleWorks';

import { HeaderStyled, TitleStyled } from './styled';

const Detail: FC = memo(() => {
  const { t } = useTranslation();

  const scheduleWorks = useStore($scheduleWorks);
  const isLoading = useStore($isLoadingDetail);

  return (
    <Wrapper style={{ height: '100%', padding: 24, overflow: 'hidden' }}>
      <HeaderStyled>
        <TitleStyled>{t('ScheduledWorks')}</TitleStyled>
        <IconButton
          onClick={() => changedDetailVisibility(false)}
          size="large"
          style={{ padding: 0 }}
        >
          <CloseIcon />
        </IconButton>
      </HeaderStyled>
      <Loader isLoading={isLoading} />
      <CustomScrollbar autoHide>
        {scheduleWorks.map((scheduleWork, index) => (
          <ScheduleWorks key={JSON.stringify({ [index]: scheduleWork.date })} {...scheduleWork} />
        ))}
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Detail };
