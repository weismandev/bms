import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import { FilterMainDetailLayout, ErrorMessage } from '@ui/index';

import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  PageGate,
  $isDetailOpen,
  $isFilterOpen,
} from '../models';
import { Table } from '../organisms/table';
import { Detail } from '../organisms/detail';
import { Filter } from '../organisms/filter';

const MaintenanceSchedulePage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedMaintenanceSchedulePage: FC = () => (
  <HaveSectionAccess>
    <MaintenanceSchedulePage />
  </HaveSectionAccess>
);

export { RestrictedMaintenanceSchedulePage as MaintenanceSchedulePage };
