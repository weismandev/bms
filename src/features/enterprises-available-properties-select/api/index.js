import { api } from '../../../api/api2';

export const getList = (payload = {}) =>
  api.v1('get', 'tickets/get-available-properties', payload);
