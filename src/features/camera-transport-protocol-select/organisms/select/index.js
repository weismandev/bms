import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data } from '../../model';

const CameraTransportProtocolSelect = (props) => {
  const options = useStore($data);

  return <SelectControl options={options} {...props} />;
};

const CameraTransportProtocolSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<CameraTransportProtocolSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { CameraTransportProtocolSelect, CameraTransportProtocolSelectField };
