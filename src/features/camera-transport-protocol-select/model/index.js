import { createStore } from 'effector';

const $data = createStore([
  {
    id: '1',
    title: 'udp',
  },
  {
    id: '2',
    title: 'tcp',
  },
]);

export { $data };
