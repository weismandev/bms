import { forward, attach, merge, guard, sample } from 'effector';
import { delay } from 'patronum';
import i18n from '@shared/config/i18n';
import { IsExistIdInUrl } from '../../../tools/is-exist-id-in-url';
import { getPathOr } from '../../../tools/path';
import { signout, $pathname, history } from '../../common';
import {
  $opened,
  $currentTab,
  selectTab,
  addClicked,
  entityApi,
  $mode,
  newEntity,
  $isDetailOpen,
  $archive,
  archiveRequested,
  openViaUrl,
  changedDetailVisibility,
  $isModalVisible,
  setModalVisible,
} from './detail.model';
import {
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,
  fxGetArchive,
  pageUnmounted,
  fxGetList,
  $entityId,
  $path,
} from './page.model';

const { t } = i18n;

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetById.prepend(({ entityId }) => entityId),
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

$opened
  .on(merge([fxGetById.done, fxCreate.done, fxUpdate.done]), (state, { result }) =>
    formatDetailData(result)
  )
  .on(fxDelete.done, () => newEntity)
  .reset(pageUnmounted);

$mode.on(
  merge([fxCreate.done, fxUpdate.done, fxGetById.done, fxDelete.done]),
  () => 'view'
);

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetById.done, () => true)
  .on(fxDelete.done, () => false)
  .reset([pageUnmounted, fxDelete.done, fxGetById.fail]);

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        camera: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: [fxCreate.done, fxUpdate.done],
  fn: ({
    result: {
      camera: { id },
    },
  }) => {
    if (id) {
      return true
    }
  },
  target: $isModalVisible
});

sample({
  clock: setModalVisible,
  target: $isModalVisible,
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$currentTab
  .on(selectTab, (state, selectedTab) => selectedTab)
  .on(addClicked, () => 'settings')
  .reset(signout);

$archive
  .on(fxGetArchive.done, (state, { result }) => result.url)
  .reset(selectTab)
  .reset(signout);

forward({
  from: entityApi.create,
  to: attach({ effect: fxCreate, mapParams: formatPayload }),
});

forward({
  from: entityApi.update,
  to: attach({ effect: fxUpdate, mapParams: formatPayload }),
});

forward({
  from: archiveRequested,
  to: attach({
    effect: fxGetArchive,
    source: $opened,
    mapParams: ({ timestamp }, opened) => ({
      timestamp: new Date(timestamp) / 1000,
      id: opened.id,
    }),
  }),
});

function formatDetailData(data) {
  return {
    id: getPathOr('camera.id', data, ''),
    title: getPathOr('camera.title', data, ''),
    protocol: getPathOr('camera.type', data, ''),
    ip: getPathOr('camera.ip', data, ''),
    port: getPathOr('camera.port', data, ''),
    url: getPathOr('camera.url', data, ''),
    login: getPathOr('camera.login', data, ''),
    height: getPathOr('camera.height', data, '') || 0,
    bitrate: getPathOr('camera.bitrate', data, '') || 0,
    fps: getPathOr('camera.fps', data, '') || 0,
    password: getPathOr('camera.password', data, ''),
    transport: getPathOr('camera.transport', data, ''),
    buildings: getPathOr('buildings', data, ''),
    rizer:
      data.rizer.map((rizer) => {
        if (String(rizer) === '0') {
          return { id: '0', title: t('gate') };
        }
        return { id: rizer, title: `${t('rizer')} №${rizer}` };
      }) || [],
    external_id: getPathOr('camera.external_id', data, '') || '',
    has_archive: getPathOr('camera.has_archive', data, ''),

    preview: getPathOr('preview_url', data, ''),
    stream: getPathOr('stream.mjpeg', data, ''),
    available_for_residents: Boolean(
      getPathOr('camera.available_for_residents', data, '')
    ),
    has_ptz: Boolean(getPathOr('camera.has_ptz', data, '')),
    http_port: getPathOr('camera.http_port', data, '80'),
  };
}

function formatPayload(payload) {
  const { protocol, rizer, buildings, transport, has_ptz, http_port, ...rest } = payload;

  return {
    type_id: protocol && protocol.id ? protocol.id : '',
    transport_id: transport && transport.id ? transport.id : '',
    rizer: Array.isArray(rizer) ? rizer.map((i) => i.id) : [],
    has_ptz,
    http_port: has_ptz ? http_port || '80' : null,
    buildings:
      Array.isArray(buildings) && buildings.length > 0
        ? buildings.map((build) => (typeof build === 'object' ? build.id : build))
        : '',
    ...rest,
  };
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
