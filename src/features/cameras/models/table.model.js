import { combine, createEvent, createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { getPathOr } from '../../../tools/path';
import { $filters } from './filter.model';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'camera', title: t('CameraName') },
  { name: 'city', title: t('Label.city') },
  { name: 'complexes', title: t('RC') },
  { name: 'buildings', title: `${t('Label.street')}, ${t('Label.building')}` },
  { name: 'status', title: t('Label.status') },
  { name: 'actions', title: t('Label.actions') },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: [
    { columnName: 'camera', width: 300 },
    { columnName: 'buildings', width: 400 },
    { columnName: 'status', width: 150 },
    { columnName: 'actions', width: 120 },
  ],
});

export const rowOrderChanged = createEvent();
export const $order = createStore([]);

export const $tableData = combine(
  $raw,
  $order,
  $search,
  $filters,
  ({ data }, order, search, filters) => {
    const formattedCameras = formatCameras(data);
    const orderedCameras = putInOrder(formattedCameras, order);
    const searchedCameras = filterBySeacrh(orderedCameras, search);
    const filteredCameras = applyFilters(searchedCameras, filters);

    return filteredCameras;
  }
);

function formatCameras(cameras = []) {
  return cameras.map((camera) => ({
    id: camera.camera.id,
    city: getPathOr('buildings[0].building.address.city', camera, t('isNotDefined')),
    complexes: getPathOr('buildings[0].complex.title', camera, t('isNotDefined')),
    buildings: camera.buildings.map((build) => {
      const street = getPathOr('building.address.street', build, '');
      const houseNumber = getPathOr('building.address.houseNumber', build, '');
      return street && houseNumber ? `${street}, ${houseNumber}` : '';
    }),
    camera: getPathOr('camera.title', camera, t('Nameless')),
    status: camera.camera.available ? t('Online') : t('Offline'),
  }));
}

function putInOrder(cameras = [], order = []) {
  return order.reduce((acc, id) => {
    const match = cameras.find((camera) => String(camera.id) === String(id));
    return match ? acc.concat(match) : acc;
  }, []);
}

function filterBySeacrh(cameras = [], search = '') {
  const regexp = new RegExp(search, 'gi');

  const matches = cameras.filter((item) => {
    const { city, complexes, buildings, camera } = item;

    if (search) {
      const hasCityMatch = regexp.test(city);
      const hasComplexMatch = regexp.test(complexes);
      const hasBuildingsMatch =
        buildings.filter((building) => regexp.test(building)).length > 0;
      const hasCameraMatch = regexp.test(camera);

      return hasCityMatch || hasComplexMatch || hasBuildingsMatch || hasCameraMatch;
    }

    return true;
  });

  return matches;
}

function applyFilters(cameras = [], { buildings, status }) {
  let filteredCameras = [...cameras];
  if (buildings) {
    const buildingTitles = buildings.map(
      (build) => `${build.building.address.street}, ${build.building.address.houseNumber}`
    );

    filteredCameras = cameras.filter((camera) =>
      camera.buildings.some((building) => buildingTitles.includes(building))
    );
  }
  return status
    ? filteredCameras.filter((camera) => camera.status === status.label)
    : filteredCameras;
}
