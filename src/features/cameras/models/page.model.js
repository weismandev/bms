import { combine, createEffect } from 'effector';
import { $pathname } from '@features/common';
import { createPageBag } from '../../../tools/factories';
import { $data as $buildings } from '../../house-select';
import { camerasApi } from '../api';

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw: $rawData,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  camerasApi,
  {
    normalize: ({ data = [] }) =>
      data.reduce((acc, i) => ({ ...acc, [i.camera.id]: i }), {}),
    handleGetById: handleUpdate,
    handleUpdate,
    handleCreate,
  },
  { idAttribute: 'id', itemsAttribute: 'cameras' }
);

export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export const fxSaveOrder = createEffect();
export const fxGetArchive = createEffect();

export const $raw = combine($rawData, $buildings, (cameras, buildings) => {
  return {
    ...cameras,
    data: cameras.data.map((item) => ({
      ...item,
      buildings: item.buildings.map(
        (buildingId) => buildings.find((building) => building.id === buildingId) || []
      ),
    })),
  };
});

function handleUpdate(state, result) {
  const cameraIdx = state.data.findIndex(
    (item) => String(item.camera.id) === String(result.camera.id)
  );

  if (cameraIdx || cameraIdx === 0) {
    return {
      ...state,
      data: state.data
        .slice(0, cameraIdx)
        .concat(result)
        .concat(state.data.slice(cameraIdx + 1)),
    };
  }
  return state;
}

function handleCreate(state, result) {
  return {
    data: state.data.concat(result),
    meta: { ...state.meta, total: state.meta.total + 1 },
  };
}
