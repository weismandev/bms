import { signout } from '../../common';
import { $raw, fxCreate, fxDelete } from './page.model';
import { rowOrderChanged, $order, $sorting } from './table.model';

$sorting.reset(rowOrderChanged);

$order
  .on(rowOrderChanged, (state, payload) => {
    const rowIdx = state.findIndex((id) => String(payload.rowId) === String(id));

    const prevEls = state.slice(0, rowIdx);
    const movedElement = state[rowIdx];
    const postEls = state.slice(rowIdx + 1);

    if (payload.action === 'up') {
      return prevEls
        .slice(0, -1)
        .concat(movedElement)
        .concat(prevEls.slice(-1))
        .concat(postEls);
    }
    if (payload.action === 'down') {
      return prevEls
        .concat(postEls.slice(0, 1))
        .concat(movedElement)
        .concat(postEls.slice(1));
    }

    return null;
  })
  .on($raw, (state, { data }) => {
    if (Array.isArray(state) && state.length !== 0) {
      return state;
    }
    return data.map((i) => i.camera.id);
  })
  .on(fxCreate.done, (state, { result }) => state.concat(result.camera.id))
  .on(fxDelete.done, (state, { params }) =>
    state.filter((i) => String(i) !== String(params))
  )
  .reset(signout);
