import { guard, sample } from 'effector';
import { accessApi } from '../api';
import {
  AccessFormGate,
  submit,
  fxGetCameraApartment,
  fxUpdateCameraAccess,
  $mode,
  $access,
} from './access.model';

fxGetCameraApartment.use(accessApi.getCameraApartment);
fxUpdateCameraAccess.use(accessApi.updateCameraAccess);

$access
  .on(fxGetCameraApartment.doneData, (state, { apartments, properties }) => ({
    ...state,
    apartments,
    properties,
  }))
  .on(fxUpdateCameraAccess.done, (state, { params }) => ({
    apartments: params.apartments,
    properties: params.properties,
  }))
  .reset([AccessFormGate.state, AccessFormGate.close]);

$mode.on(fxUpdateCameraAccess.done, () => 'view').reset(AccessFormGate.close);

sample({
  source: AccessFormGate.state,
  clock: submit,
  fn: ({ camera_id }, { objects }) => ({
    camera_id,
    apartments: objects?.apartments,
    properties: objects?.properties,
  }),
  target: fxUpdateCameraAccess,
});

guard({
  source: AccessFormGate.state,
  filter: ({ camera_id }) => Boolean(camera_id) || camera_id === 0,
  target: fxGetCameraApartment,
});
