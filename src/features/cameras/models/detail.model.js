import { createStore, createEvent } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

export const newEntity = {
  title: '',
  buildings: '',
  protocol: '',
  ip: '',
  port: '',
  url: '',
  password: '',
  login: '',
  rizer: '',
  height: 0,
  bitrate: 0,
  fps: 0,
  transport: '',
  external_id: '',
  has_archive: false,
  available_for_residents: true,
  has_ptz: false,
  http_port: '80',
};

export const tabsConfig = [
  {
    label: t('camera'),
    value: 'entity',
    onCreateIsDisabled: true,
  },
  {
    label: t('Access'),
    value: 'access',
    onCreateIsDisabled: true,
  },
  {
    label: t('settings'),
    value: 'settings',
    onCreateIsDisabled: false,
  },
];

export const selectTab = createEvent();
export const archiveRequested = createEvent();
export const setModalVisible = createEvent();

export const $isModalVisible = createStore(false);
export const $currentTab = createStore('entity');
export const $archive = createStore('');

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
