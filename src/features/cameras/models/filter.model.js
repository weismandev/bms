import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  buildings: '',
  status: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
