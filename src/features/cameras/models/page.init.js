import { forward, guard, sample, merge } from 'effector';
import { fxGetList as fxGetBuildings } from '../../house-select';
import { camerasApi } from '../api';
import { openViaUrl, $opened } from './detail.model';
import {
  $raw,
  pageMounted,
  fxGetList,
  fxSaveOrder,
  fxGetArchive,
  fxGetById,
  $rowCount,
  deleteConfirmed,
  fxDelete,
  $isLoading,
  $error,
  $isErrorDialogOpen,
} from './page.model';
import { $order, rowOrderChanged, $tableData } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxSaveOrder.use(camerasApi.saveOrder);
fxGetArchive.use(camerasApi.getArchive);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$rowCount.on($tableData, (state, data = []) => data.length);

$raw.on(fxDelete.done, (state, { params: id }) => ({
  meta: { total: state.meta.total - 1 },
  data: state.data.filter((i) => String(i.camera.id) !== String(id)),
}));

$isLoading.on(
  merge([fxSaveOrder.pending, fxGetArchive.pending]),
  (state, isLoading) => isLoading
);
$error.on(merge([fxSaveOrder.fail, fxGetArchive.fail]), (state, { error }) => error);
$isErrorDialogOpen.on(merge([fxSaveOrder.fail, fxGetArchive.fail]), () => true);

forward({ from: pageMounted, to: fxGetBuildings });

sample({
  clock: [pageMounted, $settingsInitialized],
  source: $settingsInitialized,
  filter: (settingsInitialized) => settingsInitialized,
  target: fxGetList,
});

forward({ from: openViaUrl, to: fxGetById });
forward({
  from: sample($opened, deleteConfirmed, (opened) => opened.id),
  to: fxDelete,
});

guard({
  source: sample($order, rowOrderChanged),
  filter: (order) => Array.isArray(order) && order.length > 0,
  target: fxSaveOrder,
});
