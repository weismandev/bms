import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const AccessFormGate = createGate();

const changeMode = createEvent();
const submit = createEvent();

const fxGetCameraApartment = createEffect();
const fxUpdateCameraAccess = createEffect();

const $mode = restore(changeMode, 'view');
const $access = createStore({ apartments: [], properties: [] });

export {
  AccessFormGate,
  changeMode,
  submit,
  fxGetCameraApartment,
  fxUpdateCameraAccess,
  $mode,
  $access,
};
