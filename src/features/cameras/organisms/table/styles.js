import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: { height: '100%', padding: 24 },
  toolbar: {
    padding: 0,
    marginBottom: '12px',
    minHeight: '36px',
    height: '36px',
    flexWrap: 'nowrap',
    borderBottom: 'none !important',
  },
  gridRoot: {
    height: '100%',
  },
  addressSpan: {
    display: 'block',
  },
  statusSpan: {
    fontWeight: 500,
  },
  statusOnline: {
    color: '#1BB169',
  },
  statusOffline: {
    color: '#EB5757',
  },
  margin: {
    margin: 5,
  },
});
