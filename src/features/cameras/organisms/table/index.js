import { useMemo, useCallback, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import classNames from 'classnames';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { Template } from '@devexpress/dx-react-core';
import {
  DataTypeProvider,
  SearchState,
  IntegratedFiltering,
  SortingState,
  IntegratedSorting,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  TableHeaderRow,
  Table as DXTable,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import { TableToolbar } from '..';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  ActionIconButton,
  SortingLabel,
  TableColumnVisibility,
} from '../../../../ui';
import { $opened, openViaUrl } from '../../models/detail.model';
import {
  $tableData,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  columns,
  searchChanged,
  $search,
  rowOrderChanged,
  $sorting,
  sortChanged,
} from '../../models/table.model';
import { useStyles } from './styles';

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => openViaUrl(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => {
  const classes = useStyles();
  return (
    <Grid.Root {...props} className={classes.gridRoot}>
      {children}
    </Grid.Root>
  );
};

const ToolbarRoot = (props) => {
  const { toolbar } = useStyles();
  return <Toolbar.Root {...props} className={toolbar} disableGutters />;
};

const AddressFormatter = ({ value }) => {
  const { addressSpan } = useStyles();
  return (
    Array.isArray(value) &&
    value.map((i, idx) => (
      <span key={idx} className={addressSpan}>
        {i}
      </span>
    ))
  );
};

const ActionFormatter = ({ row }) => {
  const { margin } = useStyles();
  return (
    <>
      <ActionIconButton
        onClick={(e) => {
          e.stopPropagation();
          rowOrderChanged({ rowId: row.id, action: 'up' });
        }}
        className={margin}
      >
        <ArrowUpwardIcon />
      </ActionIconButton>
      <ActionIconButton
        onClick={(e) => {
          e.stopPropagation();
          rowOrderChanged({ rowId: row.id, action: 'down' });
        }}
        className={margin}
      >
        <ArrowDownwardIcon />
      </ActionIconButton>
    </>
  );
};

const StatusFormatter = ({ value }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <span
      className={classNames(
        classes.statusSpan,
        value === t('Online') ? classes.statusOnline : classes.statusOffline
      )}
    >
      {value}
    </span>
  );
};

const AddressTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={AddressFormatter} {...props} />
);

const ActionTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={ActionFormatter} {...props} />
);

const StatusTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StatusFormatter} {...props} />
);

const Table = memo(() => {
  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const searchValue = useStore($search);
  const sortValue = useStore($sorting);

  const classes = useStyles();

  const { t } = useTranslation();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
        className={classes.gridRoot}
      >
        <DragDropProvider />
        <AddressTypeProvider for={['buildings']} />
        <ActionTypeProvider for={['actions']} />
        <StatusTypeProvider for={['status']} />

        <SearchState value={searchValue} onValueChange={searchChanged} />
        <IntegratedFiltering />

        <SortingState sorting={sortValue} onSortingChange={sortChanged} />
        <IntegratedSorting
          columnExtensions={[{ columnName: 'camera', compare: camerasSort }]}
        />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          showSortingControls
          sortLabelComponent={SortingLabel}
          cellComponent={TableHeaderCell}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };

function camerasSort(cam1, cam2) {
  const firstCam = cam1.toLowerCase();
  const secondCam = cam2.toLowerCase();

  if (firstCam === secondCam) return 0;

  return firstCam < secondCam ? -1 : 1;
}
