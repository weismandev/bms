import { useStore } from 'effector-react';
import {
  Toolbar,
  FoundInfo,
  AddButton,
  Greedy,
  SearchInput,
  FilterButton,
} from '../../../../ui';
import { $mode, addClicked } from '../../models/detail.model';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';
import { $search, searchChanged } from '../../models/table.model';
import { useStyles } from './styles';

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const search = useStore($search);
  const mode = useStore($mode);
  const isFilterOpen = useStore($isFilterOpen);

  const { margin } = useStyles();

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} className={margin} />
      <SearchInput
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <AddButton disabled={mode === 'edit'} onClick={addClicked} className={margin} />
    </Toolbar>
  );
};

export { TableToolbar };
