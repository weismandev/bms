import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  margin: {
    margin: '0 10px',
  },
});
