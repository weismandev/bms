import { useState } from 'react';

import { IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';

import { ArchivalStream, ArchiveDateEvents } from '@widgets/MediaPlayer';

import { Modal } from '@ui/index';

const useStyles = makeStyles({
  paper: {
    width: '100%',
    minWidth: '100%',
    height: '100%',
    maxHeight: '100%',
    borderRadius: 0,
    padding: 0,
    // overflow: 'hidden',
    '& > h2': {
      minHeight: 40,
      paddingLeft: 24,
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    minHeight: 40,
  },
  wrapper: {
    position: 'relative',
    // marginBottom: 24,

    '& > div': {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      // height: 'calc(100vh - 50px)',

      '& > div:first-child': {
        height: 'calc(100vh - 150px)',
      },
    },
  },
});

const getYesterday = () => {
  const today = new Date();
  const yesterday = new Date();
  yesterday.setDate(today.getDate() - 1);
  return Math.floor(yesterday.getTime() / 1000);
};

const ArchiveModal = ({ id, isOpen, changeVisibility, title }) => {
  const { paper, header, wrapper } = useStyles();

  const [dt, setDt] = useState(getYesterday());

  const closeModal = () => {
    changeVisibility(false);
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={closeModal}
      classes={{ paper }}
      header={(
        <div className={header}>
          <span>{title}</span>
          <ArchiveDateEvents id={id} onClick={setDt} />
          <IconButton onClick={closeModal} size="large">
            <Close />
          </IconButton>
        </div>
      )}
      content={(
        <div>
          <div className={wrapper}>
            <ArchivalStream id={id} dt={dt} isOpen={isOpen} />
          </div>
        </div>
      )}
    />
  );
};

export { ArchiveModal };
