import { useState, useMemo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import Scrollbars from 'react-custom-scrollbars';
import { Stream } from '@widgets/MediaPlayer';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  Tabs,
  SwitchField,
  ActionButton,
  CustomScrollbar,
  Modal
} from '@ui';

import {
  $opened,
  $mode,
  changeMode,
  tabsConfig,
  selectTab,
  $currentTab,
  detailSubmitted,
  changedDetailVisibility,
  archiveRequested,
  $archive,
  $isModalVisible,
  setModalVisible
} from '../../models/detail.model';

import styled from '@emotion/styled';

import {
  changeMode as changeAccessMode,
  $mode as $accessMode,
  $access,
  submit,
  AccessFormGate,
} from '../../models/access.model';

import { deleteDialogVisibilityChanged } from '../../models/page.model';

import { HouseSelectField } from '../../../house-select';
import { CameraProtocolSelectField } from '../../../camera-protocol-select';
import { CameraTransportProtocolSelectField } from '../../../camera-transport-protocol-select';
import { RizerSelectField } from '../../../rizer-select';
import { ObjectsTreeSelect } from '../../../objects-tree-select';
import { ArchiveModal } from './archive-modal';

const Detail = () => {
  const mode = useStore($mode);
  const currentTab = useStore($currentTab);
  const opened = useStore($opened);
  const isModalVisible = useStore($isModalVisible);

  const [isVisibilityArchive, toggleVisibilityArchive] = useState(false);

  const isNew = !opened.id;
  const renderStream = useMemo(() => {
    return <Stream id={opened.id} />;
  }, [opened.id]);

  const mapTabToContent = {
    settings: <SettingsForm mode={mode} isNew={isNew} isModalVisible={isModalVisible} />,
    entity: (
      <>
        <DetailToolbar
          style={{ height: 84, padding: 24 }}
          hidden={{
            delete: true,
            edit: true,
            videoLibrary: !opened.has_archive,
          }}
          onClose={() => changedDetailVisibility(false)}
          onVideoLibrary={() => toggleVisibilityArchive(true)}
        />
        {opened.id && renderStream}
      </>
    ),
    access: <AccessForm camera_id={opened.id} />,
  };

  return (
    <Wrapper style={{ height: '89vh' }}>
      <Tabs
        options={tabsConfig}
        currentTab={currentTab}
        onChange={(e, value) => selectTab(value)}
        isNew={isNew}
      />
      {mapTabToContent[currentTab] || null}
      {opened.id && (
        <ArchiveModal
          id={opened.id}
          isOpen={isVisibilityArchive}
          changeVisibility={toggleVisibilityArchive}
          title={opened.title}
        />
      )}
    </Wrapper>
  );
};

function AccessForm(props) {
  const { camera_id } = props;
  const mode = useStore($accessMode);
  const access = useStore($access);

  return (
    <>
      <AccessFormGate camera_id={camera_id} />
      <Formik
        initialValues={{ objects: access }}
        onSubmit={submit}
        enableReinitialize
        onReset={() => {
          changeAccessMode('view');
        }}
        render={() => (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ height: 84, padding: 24 }}
              hidden={{ delete: true }}
              mode={mode}
              onClose={() => changedDetailVisibility(false)}
              onEdit={() => changeAccessMode('edit')}
            />
            <div
              style={{
                height: 'calc(100% - 84px)',
                position: 'relative',
                padding: '0 6px 24px 24px',
              }}
            >
              <Scrollbars autoHide>
                <div
                  style={{
                    height: 'calc(100% - 82px)',
                    paddingRight: '12px',
                  }}
                >
                  <Field
                    name="objects"
                    render={({ field, form }) => (
                      <ObjectsTreeSelect
                        value={field.value}
                        onChange={(value) => form.setFieldValue(field.name, value)}
                        mode={mode}
                      />
                    )}
                  />
                </div>
              </Scrollbars>
            </div>
          </Form>
        )}
      />
    </>
  );
}

function SettingsForm(props) {
  const { mode, isNew, isModalVisible } = props;
  const { t } = useTranslation();
  const opened = useStore($opened);
  const validationSchema = Yup.object().shape({});

  const AttentionModal = () => {
    const ContentStyled = styled.span`
      font-size: 16px;
      font-weight: 400;
      line-height: 24px;
      letter-spacing: 0px;
      text-align: left;
      color: rgba(37, 40, 52, 0.87);
    `;

    const ButtonAccept = styled.button`
      background: rgba(225, 63, 61, 1);
      border-radius: 16px;
      border: none;
      padding: 4px 16px 4px 16px;
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
    `;

    const ButtonTypo = styled.span`
      color: rgba(255, 255, 255, 1);
      font-size: 14px;
      font-weight: 500;
      line-height: 24px;
      letter-spacing: 0px;
    `;

    const content = <ContentStyled>{t('AfterSavingTheCamera')}</ContentStyled>

    const actions = 
      <ButtonAccept onClick={() => setModalVisible(false)}>
          <ButtonTypo>{t('Ok')}</ButtonTypo>
      </ButtonAccept>
    return (
        <Modal
            isOpen={isModalVisible}
            content={content}
            actions={actions}
        />
    )
  }

  return (
    <>
      <AttentionModal />
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm }) => (
          <Form style={{ height: 'calc(100% - 60px)', position: 'relative' }}>
            <DetailToolbar
              style={{ height: 84, padding: 24 }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm();
                }
              }}
              onDelete={() => deleteDialogVisibilityChanged(true)}
            />

            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    name="title"
                    label={t('Name')}
                    labelPlacement="start"
                    placeholder={t('EnterTheTitle')}
                    component={InputField}
                    mode={mode}
                  />
                  <Field
                    name="buildings"
                    label={t('objects')}
                    labelPlacement="top"
                    placeholder={t('selectObjects')}
                    component={HouseSelectField}
                    isMulti
                    mode={mode}
                  />
                  <Field
                    name="protocol"
                    label={t('Protocol')}
                    labelPlacement="start"
                    placeholder={t('SelectProtocol')}
                    component={CameraProtocolSelectField}
                    mode={mode}
                  />
                  {values.protocol && values.protocol.title.toUpperCase() === 'RTSP' && (
                    <>
                      <Field
                        name="transport"
                        label={t('transport')}
                        labelPlacement="start"
                        placeholder={t('SelectProtocol')}
                        component={CameraTransportProtocolSelectField}
                        mode={mode}
                      />
                      <Field
                        name="ip"
                        label={t('IPaddress')}
                        labelPlacement="start"
                        placeholder={t('EnterIpAddress')}
                        component={InputField}
                        mode={mode}
                      />
                      <Field
                        name="port"
                        label={t('port')}
                        labelPlacement="start"
                        placeholder={t('EnterThePort')}
                        component={InputField}
                        mode={mode}
                      />
                      <Field
                        name="height"
                        label={t('FrameHeight')}
                        labelPlacement="start"
                        placeholder={t('EnterHeight')}
                        type="number"
                        component={InputField}
                        mode={mode}
                      />
                      <Field
                        name="fps"
                        label={t('FramesPerSecond')}
                        labelPlacement="start"
                        placeholder={t('EnterQuantity')}
                        type="number"
                        component={InputField}
                        mode={mode}
                      />
                      <Field
                        name="bitrate"
                        label={t('bitrate')}
                        labelPlacement="start"
                        placeholder={t('EnterBitrate')}
                        type="number"
                        component={InputField}
                        mode={mode}
                      />
                    </>
                  )}
                  <Field
                    name="url"
                    label={t('URLAddress')}
                    labelPlacement="top"
                    placeholder={t('EnterURLAddress')}
                    component={InputField}
                    mode={mode}
                  />
                  <Field
                    name="login"
                    label={t('login')}
                    labelPlacement="start"
                    placeholder={t('Enterlogin')}
                    component={InputField}
                    mode={mode}
                  />
                  <Field
                    name="password"
                    type="password"
                    label={t('password')}
                    labelPlacement="start"
                    placeholder={t('Enterpassword')}
                    component={InputField}
                    mode={mode}
                  />
                  {values.available_for_residents ? (
                    <Field
                      name="rizer"
                      label={t('rizer')}
                      placeholder={t('ChooseEntrance')}
                      component={RizerSelectField}
                      mode={mode}
                      isMulti
                      buildings={values.buildings}
                      helpText={t('WhenChoosing2MoreObjectsOnlyCommonGateCalledAvailable')}
                    />
                  ) : null}
                  {values.protocol && values.protocol.title !== 'RTSP' && (
                    <Field
                      name="external_id"
                      label={t('ExternalIdentifier')}
                      placeholder={t('EnterID')}
                      component={InputField}
                      mode={mode}
                    />
                  )}

                  <Field
                    component={SwitchField}
                    label={t('haveArchive')}
                    labelPlacement="start"
                    name="has_archive"
                    disabled={mode === 'view'}
                  />

                  <Field
                    component={SwitchField}
                    label={t('accessiblePeople')}
                    labelPlacement="start"
                    name="available_for_residents"
                    disabled={mode === 'view'}
                  />
                  <Field
                    component={SwitchField}
                    label={t('AvailabilityPTZrotarymechanism')}
                    labelPlacement="start"
                    name="has_ptz"
                    disabled={mode === 'view'}
                  />
                  {values.has_ptz && (
                    <Field
                      name="http_port"
                      label={t('httpport')}
                      placeholder={t('default80')}
                      component={InputField}
                      mode={mode}
                      disabled={mode === 'view'}
                    />
                  )}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </>
    
  );
}

export { Detail };
