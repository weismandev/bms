import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar, SelectField } from '@ui';
import { Formik, Form, Field } from 'formik';
import { HouseSelectField } from '../../../house-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';
import { useStyles } from './styles';

const Filter = memo(() => {
  const filters = useStore($filters);
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          className={classes.filterToolbar}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={() => (
            <Form className={classes.form}>
              <Field
                name="buildings"
                isMulti
                component={HouseSelectField}
                label={t('buildings')}
                placeholder={t('selectHouses')}
              />
              <Field
                name="status"
                component={SelectField}
                placeholder={t('selectStatuse')}
                options={[
                  { id: 0, label: t('Online') },
                  { id: 1, label: t('Offline') },
                ]}
                label={t('Label.status')}
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
