import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    maxHeight: '89vh',
  },
  filterToolbar: {
    padding: 24,
  },
  form: {
    padding: 24,
    paddingTop: 0,
  },
});
