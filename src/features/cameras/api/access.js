import { api } from '../../../api/api2';

const getCameraApartment = (payload) =>
  api.v1('get', 'cameras/get-cameras-apartment', payload);

const updateCameraAccess = (payload = {}) =>
  api.v1('post', 'cameras/update-cameras-access', payload);

export const accessApi = {
  getCameraApartment,
  updateCameraAccess,
};
