import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('post', 'cameras/get-by-company', payload);
const getById = (id) => api.v1('get', 'cameras/get-by-id', { id });
const create = (payload) =>
  api.v1('post', 'cameras/create', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const update = (payload) =>
  api.v1('post', 'cameras/update', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const deleteCamera = (id) => api.v1('post', 'cameras/delete', { id });
const saveOrder = (order) =>
  api.v1('post', 'cameras/save-order', { cameras_order: order });
const getArchive = (payload) => api.v1('get', 'cameras/get-archive', payload);

export const camerasApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteCamera,
  saveOrder,
  getArchive,
};
