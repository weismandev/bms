import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '../../../ui';
import { Detail, Filter, Table } from '../organisms';

import '../models';
import {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models/page.model';

import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';

const CamerasPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedCamerasPage = (props) => {
  return (
    <HaveSectionAccess>
      <CamerasPage {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedCamerasPage as CamerasPage };
