import { api } from '@api/api2';

const getAccessGroupList = () =>
  api.no_vers('get', 'scud/get-access-groups', { limit: 9999999999 });

export const accessGroupSelectApi = {
  getAccessGroupList,
};
