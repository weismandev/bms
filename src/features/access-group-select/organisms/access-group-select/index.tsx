import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, $isLoading, fxGetList } from '../../models';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Value {
  id: number;
}

export const AccessGroupSelect: FC<object> = (props) => {
  useEffect(() => {
    fxGetList();
  }, []);

  const options = useStore($data);
  const isLoading = useStore($isLoading);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const AccessGroupSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<AccessGroupSelect />} onChange={onChange} {...props} />;
};
