import { createEffect, createStore } from 'effector';

interface Value {
  id: number;
  title: string;
}

export const fxGetList = createEffect<void, { groups: Value[] }, Error>();

export const $data = createStore<Value[]>([]);
export const $isLoading = createStore<boolean>(false);
