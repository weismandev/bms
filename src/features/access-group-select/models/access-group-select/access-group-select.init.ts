import { signout } from '@features/common';

import { accessGroupSelectApi } from '../../api';
import { fxGetList, $data, $isLoading } from './access-group-select.model';

fxGetList.use(accessGroupSelectApi.getAccessGroupList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.groups)) {
      return [];
    }

    return result.groups.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
