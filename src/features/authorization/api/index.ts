import { api } from '@api/api2';
import { RegistrationData } from '../interfaces';

const registration = (payload: RegistrationData) =>
  api.v1('post', 'management/registration-tariff-form', payload);

export const authorizationApi = {
  registration,
};
