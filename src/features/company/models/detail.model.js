import { createEvent, createStore } from 'effector';

const newEntity = {
  logo: null,
  text: '',
  title: '',
  information: {
    phone: [],
    email: [],
    inn: [],
  },
};

const open = createEvent();
const changedDetailVisibility = createEvent();
const detailSubmitted = createEvent();
const changeMode = createEvent();

const $opened = createStore(newEntity);
const $isDetailOpen = createStore(false);
const $mode = createStore('view');

export {
  newEntity,
  open,
  $opened,
  $mode,
  $isDetailOpen,
  changedDetailVisibility,
  detailSubmitted,
  changeMode,
};
