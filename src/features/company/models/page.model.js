import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const errorDialogVisibilityChanged = createEvent();

const $raw = createStore({ data: [], meta: { total: 0 } });
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);

const $normalized = $raw.map((raw) =>
  raw.data.reduce((acc, i) => ({ ...acc, [i.id]: i }), {})
);

const fxGetCompanyInfo = createEffect();
const fxUpdate = createEffect();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  fxGetCompanyInfo,
  fxUpdate,
  $raw,
  $normalized,
  $isLoading,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
};
