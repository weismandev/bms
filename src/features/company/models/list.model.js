import { createStore } from 'effector';
import { $raw } from './page.model';

const $isListOpen = createStore(false);
const $list = $raw.map((raw) => raw.data);

export { $isListOpen, $list };
