import { sample, guard, forward } from 'effector';
import { isObject } from '../../../tools/type';
import { signout } from '../../common';
import {
  open,
  $opened,
  $isDetailOpen,
  changedDetailVisibility,
  changeMode,
  $mode,
  detailSubmitted,
} from './detail.model';
import { $list } from './list.model';
import { $raw, $normalized, fxUpdate } from './page.model';

$opened
  .on(
    sample($normalized, open, (normalized, id) => formatOpened(normalized[id])),
    (state, opened) => opened
  )
  .reset(signout);

$isDetailOpen
  .on($raw.updates, (state, { data }) => data.length > 0)
  .on(changedDetailVisibility, (state, visibility) => visibility)
  .reset(signout);

$mode
  .on(changeMode, (state, mode) => mode)
  .on(open, () => 'view')
  .reset(signout);

guard({
  source: $list.updates.map((list) => list[0] && list[0].id),
  filter: Boolean,
  target: open,
});

forward({
  from: detailSubmitted,
  to: fxUpdate,
});

function formatOpened(opened) {
  const { id, logo, title, text, information } = opened;

  return {
    id,
    logo,
    title: title ? title : '',
    text: text ? text : '',
    information: isObject(information)
      ? {
          phone: Array.isArray(information.phone) ? information.phone : [],
          email: Array.isArray(information.email) ? information.email : [],
          inn: Array.isArray(information.inn) ? information.inn : [],
        }
      : {
          phone: [],
          email: [],
          inn: [],
        },
  };
}
