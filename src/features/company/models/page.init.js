import { forward, attach, merge } from 'effector';
import { signout } from '../../common';
import { companyApi } from '../api';
import {
  pageMounted,
  fxGetCompanyInfo,
  fxUpdate,
  $raw,
  $isLoading,
  $error,
  $isErrorDialogOpen,
} from './page.model';

const request = [fxGetCompanyInfo.pending, fxUpdate.pending];

const errorOccured = merge([fxGetCompanyInfo.failData, fxUpdate.failData]);

$isLoading.on(request, (state, isLoading) => isLoading).reset(signout);

$error
  .on(errorOccured, (state, { error }) => error)
  .on(request, () => null)
  .reset(signout);

$isErrorDialogOpen
  .on(errorOccured, () => true)
  .on(request, () => false)
  .reset(signout);

fxGetCompanyInfo.use(companyApi.getCompanyInfo);
fxUpdate.use(companyApi.update);

$raw
  .on(fxGetCompanyInfo.done, (state, { result }) => {
    const data = hasData(result) ? [result] : [];

    return {
      meta: { total: data.length },
      data,
    };
  })
  .reset(signout);

forward({
  from: pageMounted,
  to: fxGetCompanyInfo,
});

forward({
  from: fxUpdate.done,
  to: attach({ effect: fxGetCompanyInfo, mapParams: () => ({}) }),
});

function hasData(object) {
  return Object.keys(object).length > 0;
}
