import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import makeStyles from '@mui/styles/makeStyles';
import { Button } from '@mui/material';
import i18n from '@shared/config/i18n';
import { $opened, $mode, detailSubmitted, changeMode } from '../../models/detail.model';
import {
  AvatarControl,
  CustomScrollbar,
  DetailToolbar,
  Editor,
  EditorContent,
  FormSectionHeader,
  InputField,
  Wrapper,
} from '../../../../ui';

const { t } = i18n;

const useStyles = makeStyles({
  buttonMargin: {
    marginRight: 10,
  },
  iconMargin: {
    marginRight: 5,
  },
  scrollbarView: {
    padding: 24,
  },
});

function textRequired() {
  const formattedText = this.parent.text.replace(/<[^>]+>/g, '').trim();

  if (formattedText && formattedText.length > 0) {
    return true;
  }

  return false;
}

const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField')),
  text: Yup.string().test('text-required', t('thisIsRequiredField'), textRequired),
});

const Detail = () => {
  const { scrollbarView } = useStyles();
  const opened = useStore($opened);
  const mode = useStore($mode);
  const isNew = !opened.id;
  const isEditMode = mode === 'edit';

  return (
    <Wrapper style={{ height: '89vh' }}>
      <Formik
        onSubmit={detailSubmitted}
        initialValues={opened}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm, errors, setFieldValue, handleSubmit }) => (
          <Form>
            <div>
              <DetailToolbar
                mode={mode}
                hidden={{ delete: true, close: true }}
                style={{ padding: 24 }}
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  if (isNew) {
                    resetForm();
                  } else {
                    resetForm();
                    changeMode('view');
                  }
                }}
              />
              <div style={{ height: 'calc(89vh - 110px)' }}>
                <CustomScrollbar
                  autoHide
                  renderView={(props) => <div {...props} className={scrollbarView} />}
                >
                  {isEditMode ? (
                    <div>
                      <div
                        style={{
                          display: 'grid',
                          justifyContent: 'start',
                          justifyItems: 'start',
                          gap: 15,
                          marginBottom: 20,
                        }}
                      >
                        <Field
                          name="logo"
                          render={({ field }) => (
                            <AvatarControl
                              encoding="base64"
                              mode="edit"
                              isModifyResult={false}
                              onChange={(value) => {
                                setFieldValue('logo', value);
                              }}
                              value={field.value}
                            />
                          )}
                        />

                        {values.logo ? (
                          <Button
                            style={{ marginLeft: 30 }}
                            onClick={() => {
                              setFieldValue('logo', null);
                            }}
                          >
                            Удалить логотип
                          </Button>
                        ) : null}
                      </div>

                      <Field name="title" label={t('title')} component={InputField} />
                      <Field
                        name="text"
                        label={null}
                        render={({ field, form }) => (
                          <div>
                            <Editor
                              id={values.id}
                              text={field.value}
                              placeholder={t(
                                'DescriptionMCForExampleAddressWorkScheduleContactsSocialNetworks'
                              )}
                              onChange={(value) => form.setFieldValue(field.name, value)}
                            />
                            {Boolean(errors?.text) && (
                              <span
                                style={{
                                  color: '#d32f2f',
                                  fontSize: '0.75rem',
                                }}
                              >
                                {t('thisIsRequiredField')}
                              </span>
                            )}
                          </div>
                        )}
                      />
                    </div>
                  ) : (
                    <>
                      {values.logo ? (
                        <div
                          style={{
                            display: 'grid',
                            justifyContent: 'start',
                            justifyItems: 'start',
                            marginBottom: 20,
                          }}
                        >
                          <Field
                            name="logo"
                            render={({ field }) => (
                              <AvatarControl
                                encoding="base64"
                                mode="view"
                                isModifyResult={false}
                                value={field.value}
                              />
                            )}
                          />
                        </div>
                      ) : null}

                      <FormSectionHeader header={values.title} />
                      <EditorContent markup={values.text} />
                    </>
                  )}
                </CustomScrollbar>
              </div>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

export { Detail };
