import { useStore, useList } from 'effector-react';
import { List as MuiList, ListItem, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, Divider, CustomScrollbar } from '../../../../ui';
import { open, $opened } from '../../models/detail.model';
import { $list } from '../../models/list.model';

const useStyles = makeStyles((theme) => {
  const acentItemStyles = {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  };

  return {
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      height: 120,
      padding: '24px 24px 15px 24px',
      '&:hover': acentItemStyles,
    },
    selected: acentItemStyles,
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },

    headerText: {
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: '100%',
    },
  };
});

const ItemHeader = (props) => {
  const { header } = props;
  const { headerText } = useStyles();

  return <Typography className={'headerText ' + headerText}>{header}</Typography>;
};

const Item = (props) => {
  const { children, ...rest } = props;
  const { item, divider, selected } = useStyles();
  return (
    <>
      <ListItem classes={{ root: item, selected }} button component="li" {...rest}>
        {children}
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const List = (props) => {
  const opened = useStore($opened);

  const list = useList($list, {
    keys: [opened],
    fn: ({ id, text, title }) => {
      const selectItem = () => open(id);

      return (
        <Item selected={String(opened.id) === String(id)} onClick={selectItem}>
          <ItemHeader header={title} />
        </Item>
      );
    },
  });

  return (
    <Wrapper style={{ height: '89vh' }}>
      <CustomScrollbar style={{ height: '89vh' }} autoHide>
        <MuiList disablePadding>{list}</MuiList>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { List };
