import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { TwoColumnLayout, Loader, ErrorMessage, NoDataPage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import '../models';
import { $isDetailOpen, changedDetailVisibility } from '../models/detail.model';
import { $isListOpen } from '../models/list.model';
import {
  PageGate,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
} from '../models/page.model';
import { List, Detail } from '../organisms';

const CompanyPage = (props) => {
  const { t } = useTranslation();
  const isListOpen = useStore($isListOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);

  const list = isListOpen ? <List /> : null;
  const detail = isDetailOpen ? <Detail /> : null;
  const hasContentToShow = list || detail;
  const waitForContent = !hasContentToShow && isLoading;

  const content = hasContentToShow ? (
    <TwoColumnLayout left={list} right={detail} />
  ) : waitForContent ? (
    <Loader isLoading />
  ) : (
    <NoDataPage
      onAdd={() => changedDetailVisibility(true)}
      text={t('ThereIsNoMeeting')}
    />
  );

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />
      {content}
    </>
  );
};

const RestrictedCompanyPage = (props) => {
  return (
    <HaveSectionAccess>
      <CompanyPage {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedCompanyPage as CompanyPage };
