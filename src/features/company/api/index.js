import { api } from '../../../api/api2';

const getCompanyInfo = () => api.no_vers('get', 'information/get-management-company-v2');

const update = (payload) =>
  api.no_vers('post', 'information/update-management-company', payload);

export const companyApi = {
  getCompanyInfo,
  update,
};
