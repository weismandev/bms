import { useEffect } from 'react';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const { t } = i18n;

const EmployeeByCompanyIdSelect = (props) => {
  const { company_id } = props;

  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (company_id) {
      fxGetList(company_id);
    }
  }, [company_id]);

  const getLabel = (opt) => (opt && opt.fullname) || t('Unnamed');
  const getValue = (opt) => opt && opt.employee_id;

  return (
    <SelectControl
      getOptionLabel={getLabel}
      getOptionValue={getValue}
      options={data}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const EmployeeByCompanyIdSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EmployeeByCompanyIdSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EmployeeByCompanyIdSelect, EmployeeByCompanyIdSelectField };
