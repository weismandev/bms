import { api } from '../../../api/api2';

const getList = (id) => api.v1('get', 'employee/get-enterprise-employees', { id });

export { getList };
