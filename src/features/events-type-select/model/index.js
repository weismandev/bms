import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { eventsTypesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(eventsTypesApi.getList);

$data
  .on(fxGetList.done, (state, { result }) => {
    return result.types.length ? result.types : [];
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, pending) => pending).reset(signout);

export { $data, $error, $isLoading, fxGetList };
