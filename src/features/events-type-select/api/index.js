import { api } from '../../../api/api2';

const getList = () => api.no_vers('get', 'information/get-events-types');

export const eventsTypesApi = {
  getList,
};
