import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, SelectGate } from '../../model';

const ParkingSlotForUserVehicleSelect = (props) => {
  const { userdata_id, ...rest } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const getLabel = ({ number = '---', zone = '' }) => `[${number}] ${zone}`;

  return (
    <>
      <SelectGate userdata_id={userdata_id} />
      <SelectControl
        options={options}
        isLoading={isLoading}
        error={error}
        getOptionLabel={getLabel}
        {...rest}
      />
    </>
  );
};

const ParkingSlotForUserVehicleSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ParkingSlotForUserVehicleSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { ParkingSlotForUserVehicleSelect, ParkingSlotForUserVehicleSelectField };
