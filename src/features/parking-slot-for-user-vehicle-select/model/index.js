import { createEffect, createStore, forward, guard } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { parkingApi } from '../api';

const SelectGate = createGate();
const { open, close, state } = SelectGate;

const fxGetList = createEffect(parkingApi.getGuestSlots);

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.doneData, (_, { slots }) => (Array.isArray(slots) ? slots : []))
  .reset([signout, close]);

$error.on(fxGetList.fail, (_, { error }) => error).reset([signout, close]);

$isLoading.on(fxGetList.pending, (_, result) => result).reset([signout, close]);

const hasUser = guard({
  source: state,
  filter: Boolean,
});

forward({
  from: hasUser,
  to: fxGetList,
});

export { SelectGate, open, close, state, fxGetList, $data, $error, $isLoading };
