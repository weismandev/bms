import { api } from '../../../api/api2';

const getGuestSlots = (payload) => api.v1('post', 'parking/slots/list-by-user', payload);

export const parkingApi = {
  getGuestSlots,
};
