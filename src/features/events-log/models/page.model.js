import { createEffect, createEvent } from 'effector';
import { createPageBag } from '../../../tools/factories';
import { eventsApi } from '../api';

export { fxGetList as fxGetEventTypes } from '../../events-type-select';

export const exportEventsClicked = createEvent();
export const fxExportEventsList = createEffect();

export const EXPORT_LIMIT = 10000;

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,

  $isLoading,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,

  $raw,
  $normalized,
  $rowCount,
} = createPageBag(eventsApi, {}, { idAttribute: 'id', itemsAttribute: 'events' });
