import { forward, sample, split } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import ErrorIcon from '@mui/icons-material/Error';
import { changeNotification } from '@features/colored-text-icon-notification';
import { $isCompany } from '@features/common';
import i18n from '@shared/config/i18n';
import { eventsApi } from '../api';
import { $filters } from './filter.model';
import {
  pageMounted,
  fxGetList,
  fxGetEventTypes,
  fxExportEventsList,
  EXPORT_LIMIT,
  $rowCount,
  exportEventsClicked,
  $isLoading,
} from './page.model';
import { $tableParams, $search, $currentCompany } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

const { t } = i18n;

const formatDate = (date) => (date ? new Date(date) / 1000 : '');
const formatParams = (param) =>
  Array.isArray(param) && param.length > 0 ? param.map(({ id }) => id).join(',') : '';

const formatPayload = ({
  per_page,
  page,
  date_start,
  date_end,
  complex,
  buildings,
  events,
  enterprise,
  search,
  currentCompany,
  isCompany,
  key_types,
  categories,
}) => {
  const payload = {
    for_company: Number(isCompany),
    limit: per_page,
    offset: (page - 1) * per_page,
  };

  if (date_start) payload.date_start = formatDate(date_start);
  if (date_end) payload.date_end = formatDate(date_end);

  if (complex.length > 0 && buildings.length === 0) {
    payload.complex_id = formatParams(complex);
  }

  if (buildings.length > 0) {
    payload.building_id = formatParams(buildings);
  }

  if (events) payload.type = formatParams(events);
  if (enterprise) payload.enterprise_id = enterprise.id;
  if (search) payload.search = search;
  if (currentCompany?.id) payload.enterprise_id = currentCompany.id;
  if (key_types.length > 0)
    payload.key_types = key_types.map((type) => type.id).join(',');
  if (categories.length > 0) {
    payload.category_id = categories.map((category) => category.id).join(',');
  }

  return payload;
};

fxExportEventsList.use(eventsApi.exportList);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

forward({
  from: fxExportEventsList.pending,
  to: $isLoading,
});

forward({
  from: pageMounted,
  to: fxGetEventTypes,
});

sample({
  clock: [pageMounted, $tableParams, $filters, $currentCompany, $settingsInitialized],
  source: {
    params: $tableParams,
    filters: $filters,
    currentCompany: $currentCompany,
    isCompany: $isCompany,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ params, filters, currentCompany, isCompany }) =>
    formatPayload({ ...params, ...filters, currentCompany, isCompany }),
  target: fxGetList,
});

const checkExportLimit = sample({
  source: $rowCount,
  clock: exportEventsClicked,
  fn: (count) => count < EXPORT_LIMIT,
});

const exportEvents = split(checkExportLimit, {
  accepted: (bool) => bool,
  rejected: (bool) => !bool,
});

sample({
  source: {
    filters: $filters,
    search: $search,
    isCompany: $isCompany,
    table: $tableParams,
  },
  clock: exportEvents.accepted,
  fn: ({ filters, search, isCompany, table: { page } }) =>
    formatPayload({ search, per_page: EXPORT_LIMIT, isCompany, page, ...filters }),
  target: fxExportEventsList,
});

forward({
  from: exportEvents.rejected,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#FFA500',
    text: t('attention'),
    Icon: ErrorIcon,
    message: `${t('YouRequestedExport')} ${$rowCount.getState()} ${t('ofEvents')}.
      ${t('MaximumAmount')} - ${EXPORT_LIMIT} ${t('eventsPerExport')}.
      ${t('PleaseLimitTheNumberUploadedPostsUsingFiltersandorDearch')}.`,
  })),
});

sample({
  source: fxExportEventsList.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `Events_${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});
