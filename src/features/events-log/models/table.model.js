import { combine, createEvent, restore } from 'effector';
import format from 'date-fns/format';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { createTableBag } from '../../../tools/factories';
import { $data as $eventsTypes } from '../../events-type-select';
import { $filters } from './filter.model';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'date', title: t('Label.date') },
  { name: 'time', title: t('Label.time') },
  { name: 'event', title: t('Event') },
  { name: 'address', title: t('Label.address') },
  { name: 'company', title: t('company') },
  { name: 'initiator', title: t('Initiator') },
  { name: 'autorization_type', title: t('Authorization') },
  { name: 'vehicle', title: t('transport') },
  { name: 'terminal', title: t('AccessPoint') },
  { name: 'additionally', title: t('Additionally') },
];

const widths = [
  { columnName: 'date', width: 80 },
  { columnName: 'time', width: 100 },
  { columnName: 'event', width: 200 },
  { columnName: 'address', width: 350 },
  { columnName: 'company', width: 250 },
  { columnName: 'initiator', width: 300 },
  { columnName: 'autorization_type', width: 230 },
  { columnName: 'vehicle', width: 180 },
  { columnName: 'terminal', width: 150 },
  { columnName: 'additionally', width: 400 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths });

export const changeCompany = createEvent();

export const $currentCompany = restore(changeCompany, '').reset(signout);

export const $tableData = combine($raw, $eventsTypes, ({ data, meta }, events) =>
  formatTableData({ data, meta, events })
).reset(signout);

$currentPage.reset([$search, $filters]);

function formatTableData({ data = [], meta, events }) {
  return data.map((i) => {
    const type = i.type_id && events.find((j) => String(j.id) === String(i.type_id));

    return {
      id: i.id,
      date:
        i.dt && i.dt.create
          ? format(new Date(i.dt.date), 'dd MMMM yyyy', { locale: dateFnsLocale })
          : t('isNotDefinedshe'),
      time:
        i.dt && i.dt.create
          ? { time: i.dt.time.slice(0, -3), timezone: i.dt.timezone }
          : t('isNotDefinedit'),
      event:
        type && i.icon
          ? { type: type.title, icon: `${meta.img_url}${i.icon}` }
          : { type: t('Another'), icon: `${meta.img_url}e7.png` },
      address: i.object ? i.object.title : t('isNotDefined'),
      sn: i.device ? i.device.sn : t('isNotDefined'),
      initiator: i.initiator ? i.initiator.title : t('isNotDefined'),
      additionally: i.title || t('isNotDefinedit'),
      vehicle: [{ license_plate: i?.vehicle?.number, brand: i?.vehicle?.title }],
      terminal: i?.vehicle?.point || '-',
      company: i?.vehicle?.company?.title || '-',
      autorization_type: i?.document?.title || '-',
    };
  });
}
