import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  date_start: '',
  date_end: '',
  complex: '',
  buildings: '',
  events: '',
  key_types: [],
  categories: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
