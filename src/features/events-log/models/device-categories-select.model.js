import { createEffect, createStore } from 'effector';

export const fxGetList = createEffect();

export const $data = createStore([]);
export const $isLoading = createStore(false);
