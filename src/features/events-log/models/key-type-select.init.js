import { signout } from '@features/common';
import { eventsApi } from '../api';
import { $data, $error, $isLoading, fxGetList } from './key-type-select.model';

fxGetList.use(eventsApi.getKeyTypeList);

$data
  .on(fxGetList.done, (_, { result }) => (Array.isArray(result) ? result : []))
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
