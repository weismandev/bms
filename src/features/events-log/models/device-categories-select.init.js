import { signout } from '@features/common';
import { eventsApi } from '../api';
import { $data, $isLoading, fxGetList } from './device-categories-select.model';

fxGetList.use(eventsApi.getDeviceCategories);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result['device-catigories'])) {
      return [];
    }

    return result['device-catigories'].map((item) => ({
      id: item.id,
      title: item?.title || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
