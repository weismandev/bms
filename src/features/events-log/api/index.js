import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.no_vers('get', 'information/events-v2', { ...payload, guest: 1 });

const exportList = (payload = {}) =>
  api.v1(
    'request',
    'information/events-v2/export',
    { ...payload, guest: 1 },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );

const getKeyTypeList = () =>
  api.no_vers('get', 'script/universal-types', { type: 'key_type' });
const getDeviceCategories = () =>
  api.no_vers('get', 'device_categories/get-device-catigories');

export const eventsApi = {
  getList,
  exportList,
  getKeyTypeList,
  getDeviceCategories,
};
