import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, $isLoading, $error, fxGetList } from '../../models/key-type-select.model';

const KeyTypeSelect = (props) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={data} isLoading={isLoading} error={error} {...props} />;
};

const KeyTypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<KeyTypeSelect />} onChange={onChange} {...props} />;
};

export { KeyTypeSelect, KeyTypeSelectField };
