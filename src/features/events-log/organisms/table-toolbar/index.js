import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  Toolbar,
  FilterButton,
  FoundInfo,
  SearchInput,
  Greedy,
  ExportButton,
} from '../../../../ui';
import { $isCompany } from '../../../common/model';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select/organisms/select';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $rowCount, exportEventsClicked, $isLoading } from '../../models/page.model';
import {
  $search,
  searchChanged,
  changeCompany,
  $currentCompany,
  $tableData,
  $hiddenColumnNames,
  columns,
} from '../../models/table.model';

const useStyles = makeStyles({
  margin: {
    margin: '0 10px 0 0',
  },
  select: {
    maxWidth: 300,
    width: '100%',
  },
});

const { t } = i18n;

function TableToolbar() {
  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);
  const currentCompany = useStore($currentCompany);
  const isCompany = useStore($isCompany);

  const isLoading = useStore($isLoading);
  const classes = useStyles();
  const tableData = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <FoundInfo
        countTitle={`${t('TotalEvents')}: `}
        count={totalCount}
        className={classes.margin}
      />
      <SearchInput
        className={classes.margin}
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      {isCompany ? (
        <div className={classes.select}>
          <EnterprisesCompaniesSelect
            onChange={changeCompany}
            value={currentCompany}
            placeholder={t('ChooseCompany')}
            divider={false}
            label={null}
          />
        </div>
      ) : null}
      <Greedy />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        className={classes.margin}
        disabled={isLoading || !tableData.length}
        onClick={exportEventsClicked}
      />
    </Toolbar>
  );
}

export { TableToolbar };
