import { useMemo } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  GroupingState,
  IntegratedGrouping,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  DragDropProvider,
  TableColumnReordering,
  TableColumnResizing,
  TableHeaderRow,
  TableGroupRow,
  Table as DXTable,
  ColumnChooser,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  TableHeaderCell,
  VehiclePlate,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import { $rowCount } from '../../models/page.model';
import {
  $tableData,
  columns,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  hiddenColumnChanged,
  columnOrderChanged,
  columnWidthsChanged,
  $currentPage,
  $pageSize,
  $hiddenColumnNames,
  $columnOrder,
  $columnWidths,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const Row = (props) => {
  const row = useMemo(
    () => <TableRow {...props} style={{ cursor: 'default' }} />,
    [props.children]
  );

  return row;
};

const GroupCell = ({ row }) => {
  const colSpan = columns.length + 1;
  return (
    <TableCell colSpan={colSpan}>
      <span
        style={{
          height: '40px',
          display: 'flex',
          alignItems: 'center',
          fontSize: '16px',
          fontWeight: '500',
          color: 'black',
          cursor: 'default',
        }}
      >
        {row?.value}
      </span>
    </TableCell>
  );
};

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      margin: '0 0 14px 0',
      minHeight: '36px',
      height: '36px',
      borderBottom: 'none',
      flexWrap: 'nowrap',
    }}
  />
);

const IconFormatter = ({ value }) => (
  <>
    <img src={value.icon} alt="" width="20" height="20" />
    <span>{` ${value.type}`}</span>
  </>
);

const IconTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={IconFormatter} {...props} />
);

const VehicleFormatter = ({ value }) => {
  const vehiclesList = value.map(({ license_plate, brand }, idx) => {
    const licensePlate =
      Boolean(license_plate) && typeof license_plate === 'string' ? license_plate : ' - ';
    const brandName = Boolean(brand) && typeof brand === 'string' ? brand : '';
    const key = licensePlate || idx;

    return <VehiclePlate key={key} number={licensePlate} brand={brandName} />;
  });

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'repeat(auto-fill, 1fr)',
        gridGap: 3,
        padding: 4,
      }}
    >
      {vehiclesList}
    </div>
  );
};

const VehicleProvider = (props) => (
  <DataTypeProvider formatterComponent={VehicleFormatter} {...props} />
);

const TimeFormatter = ({ value }) => (
  <Tooltip title={value.timezone} arrow>
    <span>{value.time}</span>
  </Tooltip>
);

const TimeProvider = (props) => (
  <DataTypeProvider formatterComponent={TimeFormatter} {...props} />
);

export const Table = () => {
  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);

  const expandedGroups =
    data.length > 0
      ? data
          .map(({ date }) => date)
          .reduce(
            (unique, date) => (unique.includes(date) ? unique : [...unique, date]),
            []
          )
      : [];

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid rootComponent={Root} getRowId={(row) => row.id} columns={columns} rows={data}>
        <GroupingState
          expandedGroups={expandedGroups}
          grouping={[{ columnName: 'date' }]}
        />
        <IntegratedGrouping />
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />
        <DragDropProvider />
        <TimeProvider for={['time']} />
        <IconTypeProvider for={['event']} />
        <VehicleProvider for={['vehicle']} />
        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableGroupRow cellComponent={GroupCell} showColumnsWhenGrouped={false} />
        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
          columnExtensions={[{ columnName: 'date', togglingEnabled: false }]}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <TableHeaderRow cellComponent={TableHeaderCell} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};
