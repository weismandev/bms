import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { $isCompany } from '@features/common';
import { ComplexSelectField } from '@features/complex-select';
import { EnterpriseSelectField } from '@features/enterprise-select';
import { EventsTypesSelectField } from '@features/events-type-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  InputField,
  CustomScrollbar,
} from '@ui/index';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';
import { DeviceCategoriesSelectField } from '../device-categories-select';
import { KeyTypeSelectField } from '../key-type-select';

const $stores = combine({
  filters: $filters,
  isCompany: $isCompany,
});

const { t } = i18n;

const Filter = () => {
  const { filters, isCompany } = useStore($stores);

  return (
    <Wrapper style={{ height: '100%', paddingTop: 10 }}>
      <CustomScrollbar autoHide style={{ height: 'calc(100% - 15px)' }}>
        <FilterToolbar
          style={{ padding: '14px 24px 24px' }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            const onChangeComplex = (value) => {
              setFieldValue('complex', value);
              setFieldValue('buildings', []);
            };

            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="date_start"
                  component={InputField}
                  label={t('startingFromTheDate')}
                  type="date"
                  placeholder={t('chooseStartDate')}
                />
                <Field
                  name="date_end"
                  component={InputField}
                  label={t('untilDate')}
                  type="date"
                  placeholder={t('chooseEndDate')}
                />
                {!isCompany && (
                  <Field
                    name="complex"
                    component={ComplexSelectField}
                    label={t('complex')}
                    placeholder={t('ChooseComplex')}
                    isMulti
                    onChange={onChangeComplex}
                  />
                )}
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  complex={Array.isArray(values.complex) ? values.complex : []}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  isDisabled={values.complex.length === 0}
                  isMulti
                />
                <Field
                  name="events"
                  component={EventsTypesSelectField}
                  label={t('EventTypes')}
                  placeholder={t('SelectEventType')}
                  isMulti
                />
                <Field
                  name="enterprise"
                  component={EnterpriseSelectField}
                  label={t('company')}
                  placeholder={t('ChooseCompany')}
                />
                <Field
                  name="key_types"
                  component={KeyTypeSelectField}
                  label={t('PassType')}
                  placeholder={t('SelectPassType')}
                  isMulti
                />
                <Field
                  name="categories"
                  component={DeviceCategoriesSelectField}
                  label={t('DeviceCategories')}
                  placeholder={t('SelectCategories')}
                  isMulti
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};

export { Filter };
