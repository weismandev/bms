import { useEffect } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import {
  $data,
  $isLoading,
  fxGetList,
} from '../../models/device-categories-select.model';

export const DeviceCategoriesSelect = (props) => {
  const [data, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={data} isLoading={isLoading} {...props} />;
};

export const DeviceCategoriesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<DeviceCategoriesSelect />} onChange={onChange} {...props} />
  );
};
