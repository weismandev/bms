import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { HaveSectionAccess } from '../../common';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../models/page.model';
import { Filter, Table } from '../organisms';

const EventsLogPage = () => {
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />
      <ColoredTextIconNotification />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        error={error}
        onClose={() => errorDialogVisibilityChanged(false)}
      />

      <FilterMainDetailLayout main={<Table />} filter={isFilterOpen && <Filter />} />
    </>
  );
};

const RestrictedEventsLogPage = () => (
  <HaveSectionAccess>
    <EventsLogPage />
  </HaveSectionAccess>
);

export { RestrictedEventsLogPage as EventsLogPage };
