import { useState } from 'react';
import DatePicker from 'react-datepicker';
import { useTranslation } from 'react-i18next';
import { useList, useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field } from 'formik';
import {
  LabeledContent,
  ActionButton,
  CustomScrollbar,
  Divider,
  SaveButton,
  CancelButton,
  InputField,
  Loader,
} from '@ui/index';
import { RelatedVehicleSelectField } from '../../../user-related-vehicles';
import { revoke, extend, $isLoading } from '../../models/detail.model';
import { PassGate, $passesList, attach, detach } from '../../models/employee-pass.model';
import { useStyles } from './styles';

const PassesList = () => {
  const { listOuter, listInner } = useStyles();
  const list = useList($passesList, (pass) => <PassCard {...pass} />);
  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
    </div>
  );
};

const PassCard = (props) => {
  const { t } = useTranslation();
  const isLoading = useStore($isLoading);

  const { comment, active_from, active_to, enterprise, id, state, vehicles } = props;

  const { part, card, text, extend_form, extend_btns, passRevoked, relatedVehicle } =
    useStyles();

  const { user_id } = useStore(PassGate.state);

  const [extendEditVisible, setExtendEditVisible] = useState(false);

  const handleExtendEditVisible = () => setExtendEditVisible(!extendEditVisible);

  const handleExtendSubmit = (values) => {
    setExtendEditVisible(!extendEditVisible);
    extend(values);
  };

  const isPassNotActive = state.slug !== 'active';
  const isPassNotRevoked = state.slug !== 'revoked';

  const renderPassControl = () => {
    if (extendEditVisible && isPassNotRevoked) {
      return (
        <Formik
          initialValues={{
            pass_id: id,
            active_from,
            active_to: '',
            time_from: '23:59',
            time_to: '23:59',
          }}
          enableReinitialize
          onSubmit={handleExtendSubmit}
          render={({ handleSubmit }) => (
            <div className={extend_form}>
              <Form>
                <Field
                  name="active_to"
                  required
                  render={({ field, form }) => (
                    <DatePicker
                      customInput={
                        <InputField
                          field={field}
                          form={form}
                          label={t('ExtendUntil')}
                          divider={false}
                        />
                      }
                      required
                      name={field.name}
                      selected={field.value}
                      onChange={(value) => form.setFieldValue(field.name, value)}
                      locale={dateFnsLocale}
                      dateFormat="dd MMMM yyyy"
                      popperPlacement="bottom"
                    />
                  )}
                />
              </Form>
              <div className={extend_btns}>
                <SaveButton
                  kind="positive"
                  onClick={handleSubmit}
                  style={{ order: 60 }}
                />

                <CancelButton
                  kind="negative"
                  style={{ order: 70 }}
                  onClick={handleExtendEditVisible}
                />
              </div>
            </div>
          )}
        />
      );
    }
    if (isPassNotRevoked) {
      return (
        <div className={passRevoked}>
          <ActionButton onClick={() => revoke({ id })} kind="negative">
            {t('Revoke')}
          </ActionButton>
          {}
          <ActionButton onClick={handleExtendEditVisible} kind="basic">
            {t('Extend')}
          </ActionButton>
        </div>
      );
    }

    return null;
  };

  return (
    <div className={card}>
      <Loader isLoading={isLoading} />
      <LabeledContent className={part} label={t('Condition')} labelPlacement="start">
        <span className={text}>{state.title}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('company')} labelPlacement="start">
        <span className={text}>{enterprise.title}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('ActualFrom')} labelPlacement="start">
        <span className={text}>{active_from}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('ActualTo')} labelPlacement="start">
        <span className={text}>{active_to}</span>
      </LabeledContent>
      {Array.isArray(vehicles) &&
        vehicles.map(({ id, brand, id_number }) => (
          <div key={id}>
            <LabeledContent
              className={part}
              label={t('TransportBrand')}
              labelPlacement="start"
            >
              {brand}
            </LabeledContent>
            <LabeledContent
              className={part}
              label={t('TransportNumber')}
              labelPlacement="start"
            >
              {id_number}
            </LabeledContent>
          </div>
        ))}
      <LabeledContent className={part} label={t('Label.comment')}>
        <span className={text}>{comment}</span>
      </LabeledContent>

      {renderPassControl()}

      <Divider />

      <Formik
        initialValues={{ pass_id: id, vehicle: '' }}
        onSubmit={attach}
        onReset={detach}
        render={({ values }) => {
          const isAttached = vehicles.some(
            (i) => values.vehicle && values.vehicle.id === i.id
          );

          return (
            <Form>
              <Field
                component={RelatedVehicleSelectField}
                label={t('vhcl')}
                labelPlacement="start"
                placeholder={t('SelectVehicle')}
                divider={false}
                name="vehicle"
                userdata_id={user_id}
                mode={isPassNotActive ? 'view' : 'edit'}
              />
              {isPassNotRevoked ? (
                <div className={relatedVehicle}>
                  {!isAttached ? (
                    <ActionButton
                      disabled={isPassNotActive}
                      style={{ width: 200 }}
                      kind="positive"
                      type="submit"
                    >
                      {t('Pin')}
                    </ActionButton>
                  ) : (
                    <ActionButton
                      disabled={isPassNotActive}
                      style={{ width: 200 }}
                      kind="negative"
                      type="reset"
                    >
                      {t('Unpin')}
                    </ActionButton>
                  )}
                </div>
              ) : null}
            </Form>
          );
        }}
      />
    </div>
  );
};

export { PassesList };
