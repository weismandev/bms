import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 0 24px',
    height: '100%',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    position: 'relative',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  part: {
    marginBottom: 20,
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  extend_form: {
    display: 'grid',
    gridTemplateColumns: '1fr auto',
    alignItems: 'end',
    gap: 10,
  },
  extend_btns: {
    display: 'grid',
    alignItems: 'center',
    gridTemplateColumns: 'auto auto',
    justifyItems: 'start',
    justifyContent: 'start',
    gap: 5,
  },
  passRevoked: {
    textAlign: 'center',
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    alignItems: 'center',
    gap: 15,
  },
  relatedVehicle: {
    textAlign: 'center',
    marginTop: 15,
  },
});
