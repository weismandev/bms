import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter, SelectField } from '@ui/index';
import { EnterprisesPassStateSelectField } from '../../../enterprises-pass-state-select';
import { EnterprisesPassTypeSelectField } from '../../../enterprises-pass-type-select';
import {
  $filters,
  filtersSubmitted,
  changedFilterVisibility,
  defaultFilters,
} from '../../models/filter.model';
import { useStyles } from './styles';

const { t } = i18n;

const Filter = (props) => {
  const classes = useStyles();
  const filters = useStore($filters);

  return (
    <Wrapper className={classes.wrapper}>
      <FilterToolbar
        className={classes.filterToolbar}
        closeFilter={() => changedFilterVisibility(false)}
      />
      <Formik
        initialValues={filters}
        onSubmit={filtersSubmitted}
        onReset={() => filtersSubmitted(defaultFilters)}
        enableReinitialize
        render={({ values }) => {
          return (
            <Form className={classes.form}>
              <Field
                name="strategy"
                component={EnterprisesPassTypeSelectField}
                label={t('TypeOfPass')}
                placeholder={t('SelectView')}
              />
              <Field
                name="state"
                component={EnterprisesPassStateSelectField}
                label={t('PassState')}
                placeholder={t('selectState')}
              />
              <Field
                name="with_car"
                component={SelectField}
                label={t('ByVehicleAvailability')}
                options={[
                  { id: 'with', title: t('OnlyWithVehicle') },
                  { id: 'without', title: t('OnlyWithoutVehicle') },
                ]}
                placeholder={t('SelectFilter')}
              />
              <FilterFooter isResettable={true} />
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

export { Filter };
