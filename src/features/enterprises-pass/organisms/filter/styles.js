import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
  },
  filterToolbar: {
    padding: 24,
  },
  form: {
    padding: 24,
    paddingTop: 0,
  },
});
