import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { InputField, DetailToolbar, CustomScrollbar } from '@ui/index';
import { passSubmitted, changeMode, $mode } from '../../models/employee-pass.model';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  vehicles: Yup.array().of(
    Yup.object().shape({
      id_number: Yup.string().required(t('thisIsRequiredField')),
    })
  ),
});

const EmployeePassForm = () => {
  const mode = useStore($mode);
  return (
    <Formik
      initialValues={{
        active_from: '',
        active_to: '',
        comment: '',
        vehicles: [],
        strategy: 'service',
      }}
      validationSchema={validationSchema}
      onSubmit={passSubmitted}
      render={({ values }) => (
        <Form style={{ height: '100%' }}>
          <DetailToolbar
            hidden={{ edit: true, delete: true, close: true }}
            onCancel={() => changeMode('view')}
            style={{ padding: 24, height: 82 }}
            mode="edit"
          />
          <div
            style={{
              padding: 24,
              paddingRight: 12,
              paddingTop: 0,
              height: 'calc(100% - 82px)',
            }}
          >
            <CustomScrollbar>
              <div style={{ paddingRight: 12 }}>
                <Field
                  name="active_from"
                  render={({ field, form }) => (
                    <DatePicker
                      customInput={
                        <InputField field={field} form={form} label={t('ValidFrom')} />
                      }
                      required
                      name={field.name}
                      selected={field.value}
                      onChange={(value) => form.setFieldValue(field.name, value)}
                      locale={dateFnsLocale}
                      dateFormat="dd MMMM yyyy"
                      popperPlacement="bottom"
                    />
                  )}
                />
                <Field
                  name="active_to"
                  render={({ field, form }) => (
                    <DatePicker
                      customInput={
                        <InputField field={field} form={form} label={t('ValidTo')} />
                      }
                      required
                      name={field.name}
                      selected={field.value}
                      onChange={(value) => form.setFieldValue(field.name, value)}
                      locale={dateFnsLocale}
                      dateFormat="dd MMMM yyyy"
                      popperPlacement="bottom"
                    />
                  )}
                />
                <FieldArray
                  name="vehicles"
                  render={() => (
                    <>
                      {values.vehicles.map((i, idx) => (
                        <Field
                          name={`vehicles[${idx}]`}
                          key={idx}
                          render={({ field }) => (
                            <>
                              <Field
                                name={`${field.name}.brand`}
                                component={InputField}
                                label={t('TransportBrand')}
                                placeholder={t('EnterTransportBrand')}
                              />
                              <Field
                                name={`${field.name}.id_number`}
                                label={t('GovNumber')}
                                placeholder="A000AA000"
                                helpText={t('FormatGovNumber')}
                                mode={mode}
                                component={InputField}
                                required
                              />
                            </>
                          )}
                        />
                      ))}
                    </>
                  )}
                />
                <Field
                  name="comment"
                  component={InputField}
                  label={t('Label.comment')}
                  placeholder={t('EnterComment')}
                />
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};

export { EmployeePassForm };
