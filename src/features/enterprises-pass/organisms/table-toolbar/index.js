import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Toolbar, Greedy, AddButton, FilterButton, AdaptiveSearch } from '@ui';
import i18n from '@shared/config/i18n';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select';
import { add, $mode } from '../../models/detail.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';
import {
  changeCompany,
  $currentCompany,
  $search,
  searchChanged,
} from '../../models/table.model';
import { useStyles } from './styles';

const { t } = i18n;

const $stores = combine({
  totalCount: $rowCount,
  search: $search,
  mode: $mode,
  currentCompany: $currentCompany,
  isFilterOpen: $isFilterOpen,
});

function TableToolbar() {
  const { totalCount, search, mode, currentCompany, isFilterOpen } = useStore($stores);
  const classes = useStyles();
  const isAnySideOpen = isFilterOpen || mode === 'edit';

  return (
    <Toolbar className={classes.toolbar}>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <AdaptiveSearch
        {...{ totalCount, searchValue: search, searchChanged, isAnySideOpen }}
      />
      <div className={classes.select}>
        <EnterprisesCompaniesSelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={false}
          placeholder={t('ChooseCompany')}
          firstAsDefault
        />
      </div>
      <Greedy />
      <AddButton disabled={mode === 'edit'} onClick={add} className={classes.margin} />
    </Toolbar>
  );
}

export { TableToolbar };
