import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  toolbar: {
    alignItems: 'flex-end',
  },
  select: {
    marginLeft: 10,
    maxWidth: 300,
    width: '100%',
  },
});
