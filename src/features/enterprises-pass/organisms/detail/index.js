import { useState } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { isValid } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field, FieldArray } from 'formik';
import { Button, Tooltip, Alert } from '@mui/material';
import {
  FileCopyOutlined,
  SmsOutlined,
  ControlPointDuplicate,
} from '@mui/icons-material';
import { EnterprisesCompaniesSelectField } from '@features/enterprises-companies-select';
import { EnterprisesPassTypeSelectField } from '@features/enterprises-pass-type-select';
import { FreeSlotsRadioGroupField } from '@features/free-slots-radio-group';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  ActionButton,
  Loader,
  ErrorMsg as ErrorMessage,
  ActionIconButton,
  FastDateSetter,
  CustomScrollbar,
  PhoneMaskedInput,
  EmailMaskedInput,
} from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  submitDetail,
  verify,
  revoke,
  $isLoading,
  $error,
  duplicatePass,
  $enterprisePermissions,
  currentEnterpriseChanged,
  $currentEnterpriseData,
} from '../../models/detail.model';
import { copy, sendSms, $isSmsSending } from '../../models/page.model';
import { CancelDialog } from '../../molecules/cancel-dialog';
import { useStyles } from './styles';
import { checkRightsToAddVehicles, validationSchema } from './validation';

const { t } = i18n;

const Detail = () => {
  const classes = useStyles();

  const mode = useStore($mode);
  const opened = useStore($opened);
  const isLoading = useStore($isLoading);
  const isSmsSending = useStore($isSmsSending);
  const error = useStore($error);
  const enterprisePermissions = useStore($enterprisePermissions);
  const currentEnterpriseData = useStore($currentEnterpriseData);

  const [isCancelDialogOpen, changeCancelDialogVisibility] = useState(false);

  const isNew = !opened.id;
  const today = new Date();

  const getAvailableMaxDate = () => {
    if (currentEnterpriseData == null) return null;

    const mode = currentEnterpriseData.is_custom_guest_scud_pass_limit;
    const modeValue = mode
      ? currentEnterpriseData.guest_scud_pass_limit
      : currentEnterpriseData.overload_guest_scud_pass_limit;

    if (modeValue == 0) return null;

    return {
      date: new Date(today).setDate(today.getDate() + modeValue),
      days: modeValue,
    };
  };

  const availableMaxDate = { ...getAvailableMaxDate() };

  const calcMaxRange = (startDate) => {
    if (!startDate) return null;

    return new Date(startDate).setDate(startDate.getDate() + availableMaxDate.days);
  };

  return (
    <Wrapper className={classes.wrapper}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <CancelDialog
        isOpen={isCancelDialogOpen}
        changeVisibility={changeCancelDialogVisibility}
        revoke={revoke}
      />
      <Formik
        initialValues={opened}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={submitDetail}
        render={({ resetForm, values, setFieldValue }) => {
          const {
            vehicles,
            active_from,
            enterprise,
            active_to,
            work_time_from,
            work_time_to,
            state,
            strategy,
          } = values;

          const isOneTimePass =
            Boolean(strategy) && (strategy === 'onetime' || strategy.slug === 'onetime');
          const isActive = state && state.slug === 'active';
          const isAutoSendingEnabled =
            opened?.enterprise?.is_guest_pass_sms_auto_sending_enabled;
          const isPassWithStuff = opened.with_stuff;

          return (
            <Form className={classes.form}>
              <DetailToolbar
                hidden={{ edit: true, delete: true }}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    resetForm();
                    changeMode('view');
                  }
                }}
                onClose={() => changedDetailVisibility(false)}
                mode={mode}
                className={classes.detailToolbar}
              >
                {!isNew && (
                  <>
                    <Tooltip title={t('CopyToClipboard')}>
                      <ActionIconButton
                        onClick={copy}
                        style={{ order: 5 }}
                        disabled={!isActive}
                      >
                        <FileCopyOutlined />
                      </ActionIconButton>
                    </Tooltip>
                    <Tooltip title={t('SendInvitationBySMS')}>
                      <ActionIconButton
                        disabled={isSmsSending || isAutoSendingEnabled || !isActive}
                        onClick={sendSms}
                        style={{
                          order: 6,
                          margin: '0 10px',
                        }}
                      >
                        <SmsOutlined />
                      </ActionIconButton>
                    </Tooltip>
                    <Tooltip
                      title={
                        isPassWithStuff
                          ? t('YouCanNotDuplicateThePassWithGoodsAndMaterials')
                          : t('DuplicatePass')
                      }
                    >
                      <ActionIconButton
                        onClick={duplicatePass}
                        style={{ order: 7 }}
                        disabled={isPassWithStuff}
                      >
                        <ControlPointDuplicate />
                      </ActionIconButton>
                    </Tooltip>
                  </>
                )}
              </DetailToolbar>
              <div className={classes.scrollOuter}>
                <CustomScrollbar>
                  <div className={classes.scrollInner}>
                    <Field
                      name="user_name"
                      component={InputField}
                      label={t('Label.name')}
                      required
                      placeholder={t('EnterYourName')}
                      mode={mode}
                    />
                    <Field
                      name="user_surname"
                      component={InputField}
                      label={t('Label.lastname')}
                      required
                      placeholder={t('EnterLastName')}
                      mode={mode}
                    />
                    <Field
                      name="user_patronymic"
                      component={InputField}
                      label={t('Label.patronymic')}
                      placeholder={t('EnterMiddleName')}
                      mode={mode}
                    />
                    <Field
                      name="user_email"
                      render={({ field, form }) => (
                        <InputField
                          mode={mode}
                          field={field}
                          form={form}
                          inputComponent={EmailMaskedInput}
                          label="Email"
                          placeholder={t('EnterEmail')}
                        />
                      )}
                    />
                    <Field
                      name="user_phone"
                      render={({ field, form }) => (
                        <InputField
                          field={field}
                          form={form}
                          inputComponent={PhoneMaskedInput}
                          label={t('Label.phone')}
                          placeholder={t('EnterPhone')}
                          mode={mode}
                        />
                      )}
                    />
                    <Field
                      name="user_company_name"
                      label={t('GuestCompany')}
                      component={InputField}
                      mode={mode}
                      placeholder={t('EnterGuestCompany')}
                    />
                    <Field
                      name="enterprise"
                      component={EnterprisesCompaniesSelectField}
                      label={t('company')}
                      placeholder={t('ChooseCompany')}
                      mode={mode}
                      required
                      firstAsDefault
                      onChange={(value) => {
                        setFieldValue('active_from', '');
                        setFieldValue('active_to', '');
                        setFieldValue('enterprise', value);
                        currentEnterpriseChanged(value.id);
                      }}
                    />
                    <Field
                      name="strategy"
                      component={EnterprisesPassTypeSelectField}
                      label={t('TypeOfPass')}
                      placeholder={t('SelectView')}
                      mode={mode}
                      required
                    />
                    {mode === 'edit' && (
                      <FastDateSetter
                        onChange={(value) => {
                          setFieldValue('active_from', value);
                          setFieldValue('active_to', value);

                          setFieldValue(
                            'work_time_from',
                            work_time_from instanceof Date
                              ? work_time_from
                              : new Date(value.setHours(0, 0, 0))
                          );

                          setFieldValue(
                            'work_time_to',
                            work_time_to instanceof Date
                              ? work_time_to
                              : new Date(value.setHours(23, 59, 0))
                          );
                        }}
                        value={
                          active_from && isValid(active_from) ? active_from : new Date()
                        }
                      />
                    )}

                    {availableMaxDate.days ? (
                      <Alert severity="info" style={{ marginBottom: 15 }}>
                        {`${t('MaxNumberOfDays')}: ${availableMaxDate.days}`}
                      </Alert>
                    ) : null}

                    <Field
                      name="active_from"
                      render={({ field, form }) => (
                        <DatePicker
                          required
                          customInput={
                            <InputField
                              field={field}
                              form={form}
                              label={isOneTimePass ? t('DateOfVisit') : t('startDate')}
                            />
                          }
                          name={field.name}
                          readOnly={mode === 'view'}
                          selected={field.value}
                          onChange={(value) => {
                            form.setFieldValue(field.name, value);
                            form.setFieldValue('active_to', '');
                          }}
                          locale={dateFnsLocale}
                          dateFormat="dd MMMM yyyy"
                          popperPlacement="top"
                          minDate={today}
                        />
                      )}
                    />
                    {!isOneTimePass ? (
                      <Field
                        name="active_to"
                        render={({ field, form }) => (
                          <DatePicker
                            required
                            customInput={
                              <InputField
                                field={field}
                                form={form}
                                label={t('expirationDate')}
                              />
                            }
                            name={field.name}
                            readOnly={mode === 'view'}
                            selected={field.value}
                            onChange={(value) => form.setFieldValue(field.name, value)}
                            locale={dateFnsLocale}
                            dateFormat="dd MMMM yyyy"
                            popperPlacement="top"
                            minDate={values.active_from}
                            maxDate={calcMaxRange(values.active_from)}
                          />
                        )}
                      />
                    ) : null}
                    <Field
                      name="work_time_from"
                      render={({ field, form }) => (
                        <DatePicker
                          required
                          readOnly={mode === 'view'}
                          customInput={
                            <InputField
                              field={field}
                              form={form}
                              label={t('WorkingHoursFrom')}
                              placeholder="09:00"
                            />
                          }
                          name={field.name}
                          selected={field.value}
                          onChange={(value) => form.setFieldValue(field.name, value)}
                          locale={dateFnsLocale}
                          showTimeSelect
                          showTimeSelectOnly
                          timeIntervals={60}
                          timeCaption={t('TimeSince')}
                          dateFormat="HH:mm"
                        />
                      )}
                    />
                    <Field
                      name="work_time_to"
                      render={({ field, form }) => (
                        <DatePicker
                          required
                          readOnly={mode === 'view'}
                          customInput={
                            <InputField
                              field={field}
                              form={form}
                              label={t('WorkingHoursTill')}
                              placeholder="18:00"
                            />
                          }
                          name={field.name}
                          selected={field.value}
                          onChange={(value) => form.setFieldValue(field.name, value)}
                          locale={dateFnsLocale}
                          showTimeSelect
                          showTimeSelectOnly
                          timeIntervals={60}
                          timeCaption={t('TimeBefore')}
                          dateFormat="HH:mm"
                        />
                      )}
                    />
                    {values.vehicles.length > 0 && (
                      <Field
                        name="slot"
                        component={FreeSlotsRadioGroupField}
                        mode={mode}
                        label={t('parkingSpace')}
                        placeholder={t('ChooseAPlace')}
                        enterprise_id={enterprise && enterprise.id}
                        active_from={active_from}
                        active_to={active_to}
                        time_from={work_time_from}
                        time_to={work_time_to}
                      />
                    )}
                    <FieldArray
                      name="vehicles"
                      render={({ push, remove }) => (
                        <>
                          {vehicles.map((i, idx) => (
                            <Field
                              name={`vehicles[${idx}]`}
                              key={idx}
                              render={({ field }) => (
                                <>
                                  <Field
                                    name={`${field.name}.brand`}
                                    component={InputField}
                                    label={t('TransportBrand')}
                                    placeholder={t('EnterTransportBrand')}
                                    mode={mode}
                                  />
                                  <Field
                                    name={`${field.name}.id_number`}
                                    label={t('GovNumber')}
                                    placeholder="A000AA000"
                                    helpText={t('FormatGovNumber')}
                                    mode={mode}
                                    component={InputField}
                                    required
                                  />
                                </>
                              )}
                            />
                          ))}
                          {checkRightsToAddVehicles(
                            values.enterprise.id,
                            enterprisePermissions
                          ) &&
                            mode === 'edit' &&
                            Array.isArray(vehicles) &&
                            vehicles.length === 0 && (
                              <div className={classes.vehiclesWrapper}>
                                <Button
                                  color="primary"
                                  onClick={() => push({ brand: '', id_number: '' })}
                                >
                                  {t('AddVehicle')}
                                </Button>
                              </div>
                            )}
                          {mode === 'edit' &&
                            Array.isArray(vehicles) &&
                            vehicles.length > 0 && (
                              <div className={classes.vehiclesWrapper}>
                                <Button color="primary" onClick={() => remove(0)}>
                                  {t('CancelAddingVehicle')}
                                </Button>
                              </div>
                            )}
                        </>
                      )}
                    />
                    <Field
                      name="persons_count"
                      component={InputField}
                      type="number"
                      label={t('NumberOfPersons')}
                      labelPlacement="start"
                      placeholder={t('EnterNumberOfPersons')}
                      mode={mode}
                    />
                    <Field
                      name="comment"
                      component={InputField}
                      label={t('Label.comment')}
                      placeholder={t('EnterComment')}
                      mode={mode}
                    />
                  </div>
                  {state?.slug === 'waiting' && (
                    <div className={classes.waitingPassWrapper}>
                      <ActionButton
                        className={classes.waitingPassVerify}
                        kind="positive"
                        onClick={() => verify()}
                      >
                        {t('Confirm')}
                      </ActionButton>
                      <ActionButton
                        className={classes.waitingPassCancel}
                        kind="negative"
                        onClick={() => changeCancelDialogVisibility(true)}
                      >
                        {t('Revoke')}
                      </ActionButton>
                    </div>
                  )}
                  {state && state.slug === 'active' && (
                    <div className={classes.activePassWrapper}>
                      <ActionButton
                        className={classes.activePassCancel}
                        kind="negative"
                        onClick={() => changeCancelDialogVisibility(true)}
                      >
                        {t('Revoke')}
                      </ActionButton>
                    </div>
                  )}
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

export { Detail };
