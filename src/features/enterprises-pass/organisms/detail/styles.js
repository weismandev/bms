import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
  },
  form: {
    height: '100%',
  },
  detailToolbar: {
    padding: 24,
    height: 82,
  },
  scrollOuter: {
    padding: 24,
    paddingRight: 12,
    paddingTop: 0,
    height: 'calc(100% - 82px)',
  },
  scrollInner: {
    paddingRight: 12,
  },
  vehiclesWrapper: {
    textAlign: 'center',
    margin: '10px 0 20px 0',
  },
  waitingPassWrapper: {
    display: 'flex',
    justifyContent: 'center',
    margin: '25px 0',
  },
  waitingPassVerify: {
    width: '152px',
    height: '30px',
  },
  waitingPassCancel: {
    width: '128px',
    height: '30px',
    marginLeft: '16px',
  },
  activePassWrapper: {
    textAlign: 'center',
    margin: '25px 0',
  },
  activePassCancel: {
    width: '230px',
  },
});
