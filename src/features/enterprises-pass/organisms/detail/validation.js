import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const mixed = Yup.mixed().required(t('thisIsRequiredField'));
const string = Yup.string().required(t('thisIsRequiredField'));

const checkStrategy = (strategy) =>
  strategy === 'temporary' || strategy.slug === 'temporary';

function checkTime() {
  const { work_time_from, work_time_to } = this.parent;
  return work_time_to > work_time_from;
}

export const checkRightsToAddVehicles = (selectedEnterpriseId, enterprisePermissions) => {
  const selectedEnterpriseRights = enterprisePermissions[selectedEnterpriseId];

  if (!selectedEnterpriseRights) {
    return false;
  }

  return selectedEnterpriseRights.includes('issue-a-pass-with-vehicle');
};

export const validationSchema = Yup.object().shape({
  user_email: Yup.string().email(t('incorrectEmail')),
  user_phone: Yup.string().matches(/\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}/g, {
    message: t('invalidNumber'),
    excludeEmptyString: true,
  }),
  user_name: string,
  user_surname: string,
  enterprise: mixed,
  strategy: mixed,
  active_from: mixed,
  active_to: Yup.mixed().when('strategy', {
    is: checkStrategy,
    then: mixed,
    otherwise: Yup.mixed().nullable(),
  }),
  vehicles: Yup.array().of(
    Yup.object().shape({
      id_number: Yup.string().required(t('thisIsRequiredField')),
    })
  ),
  work_time_from: Yup.mixed().when('strategy', {
    is: (strat) => !checkStrategy(strat),
    then: mixed.test(
      'work_time_from-before-work_time_to',
      t('TheStartTimeMustBeBeforeTheEndTime'),
      checkTime
    ),
    otherwise: mixed,
  }),
  work_time_to: Yup.mixed().when('strategy', {
    is: (strat) => !checkStrategy(strat),
    then: mixed.test(
      'work_time_to-after-work_time_from',
      t('TheEndTimeMustBeLaterThanTheStartTime'),
      checkTime
    ),
    otherwise: mixed,
  }),
});
