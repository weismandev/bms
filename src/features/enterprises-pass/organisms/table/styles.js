import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  grid: {
    height: '100%',
  },
  toolbar: {
    padding: '0',
    minHeight: '36px',
    height: '60px',
    alignItems: 'flex-start',
    borderBottom: 'none',
    flexWrap: 'nowrap',
  },
  tableWrapper: {
    height: '100%',
    padding: 24,
  },
});
