import { useCallback, useMemo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  Toolbar,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  TableHeaderCell,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import { $opened, open } from '../../models/detail.model';
import { $rowCount } from '../../models/page.model';
import {
  $tableData,
  columns,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  hiddenColumnChanged,
  $hiddenColumnNames,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';
import { useStyles } from './styles';

const { t } = i18n;

const Root = ({ children, ...props }) => {
  const { grid } = useStyles();
  return (
    <Grid.Root {...props} className={grid}>
      {children}
    </Grid.Root>
  );
};

const ToolbarRoot = (props) => {
  const { toolbar } = useStyles();
  return <Toolbar.Root {...props} className={toolbar} />;
};

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Table = () => {
  const { tableWrapper } = useStyles();

  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  return (
    <Wrapper className={tableWrapper}>
      <Grid rootComponent={Root} rows={data} getRowId={(row) => row.id} columns={columns}>
        <DragDropProvider />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { Table };
