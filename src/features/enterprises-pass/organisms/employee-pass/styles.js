import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  newPassWrapper: {
    padding: 24,
    textAlign: 'center',
  },
  passesListWrapper: {
    position: 'relative',
    height: 'calc(100% - 110px)',
  },
});
