import { useStore } from 'effector-react';
import { Button } from '@mui/material';
import i18n from '@shared/config/i18n';
import { $mode, changeMode, PassGate } from '../../models/employee-pass.model';
import { EmployeePassForm } from '../employee-pass-form';
import { PassesList } from '../employee-pass-list';
import { useStyles } from './styles';

const { t } = i18n;

const EmployeePass = ({ user_id, enterprise_id }) => {
  const mode = useStore($mode);
  const classes = useStyles();
  const form = mode === 'edit' ? <EmployeePassForm /> : null;

  const addNewButton =
    mode === 'view' ? (
      <div className={classes.newPassWrapper}>
        <Button onClick={() => changeMode('edit')} color="primary">
          {t('CreateAPass')}
        </Button>
      </div>
    ) : null;

  const list =
    mode === 'view' ? (
      <div className={classes.passesListWrapper}>
        <PassesList />
      </div>
    ) : null;

  return (
    <>
      <PassGate user_id={user_id} enterprise_id={enterprise_id} />
      {addNewButton}
      {form}
      {list}
    </>
  );
};

export { EmployeePass };
