import { api } from '../../../api/api2';

const getList = (payload) =>
  api.v1('post', 'scud/client/management/pass/list-essential', payload);

const getById = (id) => api.v1('post', 'scud/client/management/pass/get', { id });

const create = (payload) => api.v1('post', 'scud/client/management/pass/create', payload);

const revoke = (payload) => api.v1('post', 'scud/client/management/pass/revoke', payload);

const extend = (payload) =>
  api.v1('post', 'scud/client/management/pass/update-expiration-date', payload);

const verify = (id) => api.v1('post', 'scud/client/management/pass/activate', { id });

const getEmployeePassesList = (payload) =>
  api.v1('get', 'scud/client/management/pass/for-employee-list', payload);

const createEmployeePass = (payload) =>
  api.v1('post', 'scud/client/management/pass/create-for-user', payload);

const copy = (payload = {}) =>
  api.v1('get', 'scud/client/management/pass/share-text', payload);

const sendSms = (payload = {}) =>
  api.v1('post', 'scud/client/management/pass/send-sms-invite', payload);

const attachVehicle = (payload = {}) =>
  api.v1('post', 'scud/client/management/pass/attach-vehicle', payload);

const removeVehicle = (payload = {}) =>
  api.v1('post', 'scud/client/management/pass/remove-vehicle', payload);
const getEnterprise = (payload) => api.v1('get', 'enterprises/get', payload);

export const enterprisesPassApi = {
  getList,
  getById,
  create,
  revoke,
  extend,
  verify,
  getEmployeePassesList,
  createEmployeePass,
  copy,
  sendSms,
  attachVehicle,
  removeVehicle,
  getEnterprise,
};
