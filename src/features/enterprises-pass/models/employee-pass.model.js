import { createEffect, createStore, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';

const PassGate = createGate();

const passSubmitted = createEvent();
const attach = createEvent();
const detach = createEvent();
const changeMode = createEvent();

const fxGetEmployeePassesList = createEffect();
const fxCreateEmployeePass = createEffect();

const fxAttachVehicle = createEffect();
const fxRemoveVehicle = createEffect();

const $passesList = createStore([]);
const $mode = restore(changeMode, 'view');

export {
  passSubmitted,
  attach,
  detach,
  fxGetEmployeePassesList,
  fxCreateEmployeePass,
  fxAttachVehicle,
  fxRemoveVehicle,
  $passesList,
  changeMode,
  $mode,
  PassGate,
};
