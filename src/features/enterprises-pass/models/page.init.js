import { sample, guard, merge, forward } from 'effector';
import copyToClipboard from 'copy-to-clipboard';
import { CheckCircle } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { changeNotification } from '../../colored-text-icon-notification';
import { signout } from '../../common';
import { enterprisesPassApi } from '../api';
import {
  fxCreate,
  fxRevoke,
  fxVerify,
  fxGetById,
  fxGetCompanyList,
  $opened,
} from './detail.model';
import { $filters } from './filter.model';
import {
  fxGetList,
  $raw,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  copy,
  fxCopy,
  $isNotificationDialogOpen,
  sendSms,
  fxSendSms,
  pageMounted,
  fxGetBuildings,
} from './page.model';
import { $tableParams, $currentCompany } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

const { t } = i18n;

fxGetList.use(enterprisesPassApi.getList);

fxCopy.use(async (params) => {
  const res = await enterprisesPassApi.copy(params);

  if (res.text) {
    copyToClipboard(res.text, {
      message: t('CopyText'),
    });
  } else {
    Promise.reject(new Error(t('FailedToCopyToClipboard')));
  }
});

fxSendSms.use(enterprisesPassApi.sendSms);

const errorOccured = merge([
  fxGetList.failData,
  fxCopy.failData,
  fxSendSms.failData,
  fxGetCompanyList.failData,
]);

const requestStarted = guard({ source: $isLoading, filter: Boolean });

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$raw
  .on(fxGetList.doneData, writeData)
  .on(fxCreate.doneData, insert)
  .on(fxGetById.doneData, replaceOrInsert)
  .on([fxRevoke.doneData, fxVerify.doneData], replace)
  .reset(signout);

$error.on(errorOccured, (_, error) => error).reset([requestStarted, signout]);
$isErrorDialogOpen.on($error, (state, error) => Boolean(error)).reset(signout);
$isNotificationDialogOpen.on(fxCopy.done, () => true).reset(signout);

sample({
  source: [
    fxGetList.pending,
    fxCopy.pending,
    fxSendSms.pending,
    fxGetCompanyList.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

sample({
  source: {
    table: $tableParams,
    company: $currentCompany,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ company, settingsInitialized }) =>
    settingsInitialized && company && company.id,
  fn: ({ table, company, filters }) => formatPayload({ table, company, filters }),
  target: fxGetList,
});

forward({
  from: sample($opened, copy, ({ id }) => ({ id })),
  to: fxCopy,
});

forward({
  from: sample($opened, sendSms, ({ id }) => ({ id })),
  to: fxSendSms,
});

forward({
  from: fxSendSms.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('SmsSentForSending'),
    Icon: CheckCircle,
  })),
});

forward({ from: pageMounted, to: [fxGetBuildings, fxGetCompanyList] });

function writeData(_, res) {
  const { passes = [], meta } = res;

  return {
    data: passes,
    meta: { total: Number(meta.total) || passes.length || 0 },
  };
}

function insert(state, res) {
  const { data, meta } = state;
  const { pass } = res;

  return {
    data: [].concat(pass, data),
    meta: { total: meta.total + 1 },
  };
}

function replace(state, res) {
  const { data, meta } = state;
  const { pass } = res;

  const idx = data.findIndex((i) => String(i.id) === String(pass.id));

  return idx > -1
    ? {
        meta: { ...meta },
        data: data
          .slice(0, idx)
          .concat(pass)
          .concat(data.slice(idx + 1)),
      }
    : state;
}

function replaceOrInsert(state, res) {
  const { data } = state;
  const { pass } = res;

  const hasPassInStore = data.findIndex((i) => String(i.id) === String(pass.id)) > -1;

  if (hasPassInStore) {
    replace(state, res);
  } else {
    insert(state, res);
  }
}

function formatPayload({ table, company, filters }) {
  const { strategy, state, with_car } = filters;

  const payload = {
    enterprise_id: company && company.id,
    page: table.page,
    per_page: table.per_page,
  };

  if (table.search) payload.search = table.search;

  const filter = {};

  if (strategy) filter.strategy = strategy.slug;
  if (state) filter.state = state.slug;
  if (with_car) filter.with_car = with_car.id;

  return { ...payload, filter };
}
