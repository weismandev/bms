import { attach } from 'effector';
import {
  createSavedUserSettings,
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';

import { pageMounted } from './page.model';
import {
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $pageSize,
} from './table.model';

export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const settings = {
  section: 'enterprises-passes',
  params: {
    table: { $columnWidths, $columnOrder, $hiddenColumnNames, $pageSize },
  },
  pageMounted,
  storage: {
    fxSaveInStorage,
    fxGetFromStorage,
  },
};

export const [$settingsInitialized, $isGettingSettingsFromStorage] =
  createSavedUserSettings(settings);
