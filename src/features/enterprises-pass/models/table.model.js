import { restore, createEvent } from 'effector';
import add from 'date-fns/add';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { formatPhone } from '@tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

export const changeCompany = createEvent();
export const $currentCompany = restore(changeCompany, '');

export const columns = [
  { name: 'pass_number', title: '№' },
  { name: 'enterprise', title: t('company') },
  { name: 'active_from', title: t('ValidFrom') },
  { name: 'active_to', title: t('ValidTo') },
  { name: 'work_time_interval', title: t('TimeOfAction') },
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'type', title: t('type') },
  { name: 'strategy', title: t('View') },
  { name: 'state', title: t('Label.status') },
  { name: 'received_at', title: t('Issued') },
];

const columnWidths = [
  { columnName: 'pass_number', width: 120 },
  { columnName: 'enterprise', width: 280 },
  { columnName: 'active_from', width: 150 },
  { columnName: 'active_to', width: 160 },
  { columnName: 'work_time_interval', width: 190 },
  { columnName: 'fullname', width: 315 },
  { columnName: 'phone', width: 160 },
  { columnName: 'type', width: 100 },
  { columnName: 'strategy', width: 120 },
  { columnName: 'state', width: 120 },
];

const formatDate = (date) => {
  if (!date) return t('isNotDefinedshe');

  return format(
    add(new Date(date), { minutes: new Date().getTimezoneOffset() }),
    'dd.MM.yyyy'
  );
};

const formatInterval = ({ from, to }) => {
  if (Boolean(from) && Boolean(to)) {
    return `${t('From')} ${from} ${t('To')} ${to}`;
  }
  return t('Unknown');
};

export const $tableData = $raw
  .map(({ data }) => data.map((item) => formatPass(item)))
  .reset(changeCompany);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
  $search,
} = createTableBag(columns, {
  widths: columnWidths,
});

function formatPass({
  id,
  user,
  active_from,
  active_to,
  user_phone,
  type,
  strategy,
  state,
  enterprise,
  received_at,
  work_time_from: from,
  work_time_to: to,
  sms_pass_mode,
}) {
  const fullname =
    (user && user.fullname) ||
    (user.surname || '') + ' ' + (user.name || '') + ' ' + (user.patronymic || '') ||
    t('isNotDefinedit');

  return {
    id,
    pass_number: id || '-',
    active_from: formatDate(active_from),
    active_to: formatDate(active_to),
    fullname,
    phone: user_phone ? formatPhone(user_phone) : t('isNotDefined'),
    type: type ? type.title : t('isNotDefined'),
    strategy: strategy ? strategy.title : t('isNotDefined'),
    state: state ? state.title : t('isNotDefined'),
    enterprise: enterprise ? enterprise.title : t('isNotDefinedshe'),
    received_at: received_at
      ? format(new Date(received_at), 'dd.MM.yyyy HH:mm')
      : t('isNotDefinedit'),
    work_time_interval: formatInterval({ from, to }),
    sms_pass_mode,
  };
}
