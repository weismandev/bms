import { sample, forward, merge, guard } from 'effector';
import addMinutes from 'date-fns/addMinutes';
import format from 'date-fns/format';
import set from 'date-fns/set';
import { signout } from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { enterpriseApi } from '../../enterprise-company/api';
import { enterprisesPassApi } from '../api';
import {
  $opened,
  $mode,
  $isDetailOpen,
  $isLoading,
  $error,
  open,
  entityApi,
  verify,
  revoke,
  extend,
  changedDetailVisibility,
  newEntity,
  add,
  fxCreate,
  fxRevoke,
  fxExtend,
  fxVerify,
  fxGetById,
  duplicatePass,
  $workInterval,
  fxGetCompanyList,
  $enterprisePermissions,
  fxGetEnterprise,
  $currentEnterpriseData,
  currentEnterpriseChanged,
} from './detail.model';
import { fxGetBuildings, pageUnmounted } from './page.model';

fxCreate.use(enterprisesPassApi.create);
fxGetById.use(enterprisesPassApi.getById);
fxRevoke.use(enterprisesPassApi.revoke);
fxExtend.use(enterprisesPassApi.extend);
fxVerify.use(enterprisesPassApi.verify);
fxGetCompanyList.use(enterpriseApi.getCompanyList);
fxGetEnterprise.use(enterprisesPassApi.getEnterprise);

const detailClosed = guard({
  source: changedDetailVisibility,
  filter: (visibility) => !visibility,
});

const gotPass = merge([
  fxCreate.doneData,
  fxRevoke.doneData,
  fxExtend.doneData,
  fxVerify.doneData,
  fxGetById.doneData,
]);

const errorOccured = merge([
  fxCreate.failData,
  fxRevoke.failData,
  fxExtend.failData,
  fxVerify.failData,
  fxGetById.failData,
  fxGetBuildings.failData,
  fxGetEnterprise.failData,
]);

const requestStarted = guard({ source: $isLoading, filter: Boolean });

$isDetailOpen
  .on([add, open, gotPass], () => true)
  .on(detailClosed, () => false)
  .reset(signout);

$opened
  .on(duplicatePass, (state, _) => makePassClone(state))
  .on(
    sample({
      source: $workInterval,
      clock: add,
    }),
    (_, source) => ({
      ...newEntity,
      work_time_from: source.start_work_time,
      work_time_to: source.end_work_time,
    })
  )
  .on(gotPass, formatOpened)
  .reset([signout, detailClosed]);

$mode
  .on([gotPass, detailClosed], () => 'view')
  .on([add, duplicatePass], () => 'edit')
  .reset(signout);

$error
  .on(errorOccured, (_, error) => error)
  .reset([requestStarted, signout])
  .reset(signout);

$isLoading.reset(signout);

$workInterval
  .on(fxGetBuildings.doneData, (_, { buildings = [] }) => getWorkInterval(buildings))
  .reset(signout);

$enterprisePermissions
  .on(fxGetCompanyList.doneData, (_, { enterprises = [] }) => {
    const idToPermissionsMapping = {};
    enterprises.forEach(({ id, employee_permissions }) => {
      idToPermissionsMapping[id] = employee_permissions;
    });
    return idToPermissionsMapping;
  })
  .reset(signout);

sample({
  source: [
    fxCreate.pending,
    fxRevoke.pending,
    fxExtend.pending,
    fxVerify.pending,
    fxGetById.pending,
    fxGetBuildings.pending,
    fxGetEnterprise.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

forward({ from: open, to: fxGetById });

forward({
  from: entityApi.create,
  to: fxCreate.prepend(formatPayload),
});

guard({
  source: sample($opened, verify, ({ id }) => id),
  filter: Boolean,
  target: fxVerify,
});

guard({
  source: sample($opened, revoke, ({ id }, { comment }) => ({
    id,
    comment,
  })),
  filter: ({ id }) => Boolean(id),
  target: fxRevoke,
});

forward({
  from: [extend],
  to: fxExtend.prepend(formatExtendPayload),
});

//  guard({
//    source: sample($opened, extend, (data) => data),
//    filter: ({ pass_id }) => Boolean(pass_id),
//    target: fxExtend.prepend(formatExtendPayload),
//  });

// Запрос данных о компании для информации о временных рамках
sample({
  clock: currentEnterpriseChanged,
  fn: (data) => data,
  target: fxGetEnterprise.prepend((data) => ({
    id: data,
  })),
});

$currentEnterpriseData.on(fxGetEnterprise.doneData, (_, data) => data.enterprise);

function makePassClone(pass) {
  return {
    ...pass,
    id: '',
    active_from: '',
    active_to: '',
    work_time_from: '',
    work_time_to: '',
    state: null,
  };
}

function formatOpened(state, { pass: opened }) {
  const {
    active_from,
    active_to,
    work_time_from,
    work_time_to,
    user_email,
    user_company_name,
    user_phone,
    slot,
    ...rest
  } = opened;

  return {
    ...rest,
    user_phone: formatPhone(user_phone),
    active_from: paramDateOrNowDate(formatDateOrNull(active_from)),
    active_to: paramDateOrNowDate(formatDateOrNull(active_to)),
    work_time_from: getDateTimeFromString(work_time_from),
    work_time_to: getDateTimeFromString(work_time_to),
    user_email: user_email || '',
    user_company_name: user_company_name || '',
    slot: slot || '',
  };
}

function getDateTimeFromString(string) {
  const regex = /^\d{2}:\d{2}$/;

  if (typeof string === 'string' && regex.test(string)) {
    const [hours, minutes] = string.split(':');
    return set(new Date(), { hours, minutes });
  }

  return null;
}

function getWorkInterval(buildings) {
  if (
    Array.isArray(buildings) &&
    buildings.length > 0 &&
    Boolean(buildings[0].building)
  ) {
    const { start_work_time, end_work_time } = buildings[0].building;

    const checkTimeFormat = (time) => {
      const timeRegexp = /^(?<hours>\d{1,2}):(?<minutes>\d{1,2})$/;

      if (Boolean(time) && timeRegexp.test(time)) {
        const { hours, minutes } = time.match(timeRegexp).groups;

        return set(new Date(), { hours, minutes });
      }

      return '';
    };

    return {
      start_work_time: checkTimeFormat(start_work_time),
      end_work_time: checkTimeFormat(end_work_time),
    };
  }

  return { start_work_time: '', end_work_time: '' };
}

function formatDateOrNull(date) {
  const regex = /(\d{2})\.(\d{2})\.(\d{4})/g;
  const template = '$3-$2-$1 ';

  if (typeof date === 'string') {
    return date.replace(regex, template);
  }

  return null;
}

function paramDateOrNowDate(date) {
  return date ? addMinutes(new Date(date), new Date().getTimezoneOffset()) : null;
}

function formatPayload(params) {
  const {
    user_name,
    user_surname,
    user_patronymic,
    user_phone,
    enterprise,
    strategy,
    vehicles,
    comment,
    active_from,
    active_to,
    user_email,
    user_company_name,
    persons_count,
    slot,
    work_time_from,
    work_time_to,
  } = params;

  const payload = {
    user_name,
    user_surname,
    user_patronymic,
    user_phone,
    comment,
    vehicles: vehicles.filter((i) => Boolean(i.id_number)),
    persons_count: persons_count > 0 ? persons_count : 1,
  };

  const isOneTimePass =
    Boolean(strategy) && (strategy === 'onetime' || strategy.slug === 'onetime');

  if (enterprise) {
    payload.enterprise_id = enterprise.id;
  }

  if (strategy) {
    payload.strategy = typeof strategy === 'string' ? strategy : strategy.slug;
  }

  if (active_from) {
    payload.active_from = format(new Date(active_from), 'yyyy-MM-dd');
  }

  if (active_from && isOneTimePass) {
    payload.active_to = format(new Date(active_from), 'yyyy-MM-dd');
  }

  if (active_to && !isOneTimePass) {
    payload.active_to = format(new Date(active_to), 'yyyy-MM-dd');
  }

  if (user_email) {
    payload.user_email = user_email;
  }

  if (user_company_name) {
    payload.user_company_name = user_company_name;
  }

  if (slot) {
    payload.slot_id = typeof slot === 'object' ? slot.id : slot;
  }

  if (work_time_from) {
    payload.time_from = format(work_time_from, 'HH:mm');
  }

  if (work_time_to) {
    payload.time_to = format(work_time_to, 'HH:mm');
  }

  return payload;
}

function formatExtendPayload(params) {
  const { pass_id, active_from, active_to, time_from, time_to } = params;

  const payload = {
    pass_id: pass_id,
    active_from: formatDateOrNull(active_from),
    active_to: format(new Date(active_to), 'yyyy-MM-dd'),
    time_from: time_from,
    time_to: time_to,
  };

  return payload;
}
