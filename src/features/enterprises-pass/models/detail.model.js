import { createEvent, createEffect, restore, createStore, split } from 'effector';

export const newEntity = {
  user_name: '',
  user_surname: '',
  user_patronymic: '',
  user_phone: '',
  enterprise: '',
  strategy: 'temporary',
  active_from: '',
  active_to: '',
  work_time_from: '',
  work_time_to: '',
  comment: '',
  vehicles: [],
  slot: '',
  user_email: '',
  user_company_name: '',
  persons_count: 1,
};

const changedDetailVisibility = createEvent();
const changeMode = createEvent();
const submitDetail = createEvent();
const open = createEvent();
const add = createEvent();
const verify = createEvent();
const revoke = createEvent();
const extend = createEvent();
const duplicatePass = createEvent();
const currentEnterpriseChanged = createEvent();

const fxGetById = createEffect();
const fxCreate = createEffect();
const fxRevoke = createEffect();
const fxExtend = createEffect();
const fxVerify = createEffect();
const fxGetCompanyList = createEffect();
const fxGetEnterprise = createEffect();

const entityApi = split(submitDetail, {
  create: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

const $isDetailOpen = restore(changedDetailVisibility, false);
const $mode = restore(changeMode, 'view');
const $opened = createStore(newEntity);
const $isLoading = createStore(false);
const $error = createStore(null);
const $workInterval = createStore({ start_work_time: '', end_work_time: '' });
const $enterprisePermissions = createStore({});
const $currentEnterpriseData = createStore(null);

export {
  changedDetailVisibility,
  changeMode,
  submitDetail,
  open,
  add,
  verify,
  revoke,
  extend,
  entityApi,
  fxGetById,
  fxCreate,
  fxRevoke,
  fxExtend,
  fxVerify,
  $isDetailOpen,
  $mode,
  $opened,
  $isLoading,
  $error,
  duplicatePass,
  $workInterval,
  $enterprisePermissions,
  fxGetCompanyList,
  fxGetEnterprise,
  $currentEnterpriseData,
  currentEnterpriseChanged,
};
