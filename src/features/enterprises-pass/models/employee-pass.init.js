import { guard, sample } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { pick } from '../../../tools/pick';
import { signout } from '../../common';
import { enterprisesPassApi } from '../api';
import { revoke, fxRevoke, extend, fxExtend } from './detail.model';
import {
  passSubmitted,
  attach,
  detach,
  fxGetEmployeePassesList,
  fxCreateEmployeePass,
  fxAttachVehicle,
  fxRemoveVehicle,
  $passesList,
  $mode,
  PassGate,
} from './employee-pass.model';

const { t } = i18n;

fxGetEmployeePassesList.use(enterprisesPassApi.getEmployeePassesList);
fxCreateEmployeePass.use(enterprisesPassApi.createEmployeePass);
fxAttachVehicle.use(enterprisesPassApi.attachVehicle);
fxRemoveVehicle.use(enterprisesPassApi.removeVehicle);

$passesList
  .on(fxGetEmployeePassesList.doneData, (state, { passes }) => formatList(passes))
  .on(fxCreateEmployeePass.doneData, (state, { pass }) => [formatItem(pass), ...state])
  .on(fxRevoke.doneData, (data, { pass }) => {
    const changedIdx = data.findIndex((i) => String(i.id) === String(pass.id));

    return changedIdx > -1
      ? data
          .slice(0, changedIdx)
          .concat(pass)
          .concat(data.slice(changedIdx + 1))
      : [...data];
  })
  .on(fxExtend.doneData, (data, { pass }) => {
    const changedIdx = data.findIndex((i) => String(i.id) === String(pass.id));

    return changedIdx > -1
      ? formatList(
          data
            .slice(0, changedIdx)
            .concat(pass)
            .concat(data.slice(changedIdx + 1))
        )
      : [...data];
  })
  .reset([signout, PassGate.close]);

$mode.on(fxCreateEmployeePass.done, () => 'view').reset([signout, PassGate.close]);

guard({
  source: sample({
    source: PassGate.state,
    clock: [PassGate.state, fxAttachVehicle.done, fxRemoveVehicle.done],
  }),
  filter: ({ user_id, enterprise_id }) => Boolean(user_id) && Boolean(enterprise_id),
  target: fxGetEmployeePassesList,
});

guard({
  source: sample(
    PassGate.state,
    passSubmitted,
    (params, { active_from, active_to, ...restPayload }) => ({
      ...params,
      ...restPayload,
      active_from: format(active_from, 'yyyy-MM-dd'),
      active_to: format(active_to, 'yyyy-MM-dd'),
    })
  ),
  filter: ({ user_id, enterprise_id }) => Boolean(user_id) && Boolean(enterprise_id),
  target: fxCreateEmployeePass,
});

guard({
  source: revoke,
  filter: ({ id, comment }) => Boolean(id) && !comment,
  target: fxRevoke,
});

sample({
  source: attach,
  fn: ({ pass_id, vehicle }) => ({
    pass_id,
    vehicle_id: vehicle ? vehicle.id : '',
  }),
  target: fxAttachVehicle,
});

sample({
  source: detach,
  fn: ({ pass_id, vehicle }) => ({
    pass_id,
    vehicle_id: vehicle ? vehicle.id : '',
  }),
  target: fxRemoveVehicle,
});

function formatList(list = []) {
  return list.map(formatItem);
}

function formatItem(item = {}) {
  const { active_from, active_to, enterprise, comment, id, state, vehicles } = item;

  const activeFrom =
    active_from && typeof active_from === 'string'
      ? new Date(active_from.replace(/(\d{2})\.(\d{2})\.(\d{4})/g, '$3-$2-$1'))
      : new Date();

  const activeTo =
    active_to && typeof active_to === 'string'
      ? new Date(active_to.replace(/(\d{2})\.(\d{2})\.(\d{4})/g, '$3-$2-$1'))
      : new Date();

  return {
    id,
    active_from: format(activeFrom, 'dd.MM.yyyy'),
    active_to: format(activeTo, 'dd.MM.yyyy'),
    enterprise: pick(['id', 'title'], enterprise),
    comment: comment ? comment : t('NoComment'),
    state: state ? state : { title: t('isNotDefined'), slug: 'not_defined' },
    vehicles: Array.isArray(vehicles) ? vehicles : [],
  };
}
