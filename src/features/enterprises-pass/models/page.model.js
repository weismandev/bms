import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';
import { fxGetList as fxGetBuildings } from '../../house-select';

const normalize = ({ data }) => data.reduce((acc, i) => ({ ...acc, [i.id]: i }), {});

const getRowCount = ({ meta }) => Number(meta.total);

const PageGate = createGate();
const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;
const errorDialogVisibilityChanged = createEvent();
const copy = createEvent();
const changeNotificationDialogVisibility = createEvent();
const sendSms = createEvent();

const fxGetList = createEffect();
const fxCopy = createEffect();
const fxSendSms = createEffect();

const $raw = createStore({ data: [], meta: { total: 0 } });
const $normalized = $raw.map(normalize);
const $rowCount = $raw.map(getRowCount);

const $isLoading = createStore(false);
const $error = createStore(null);
const $isSmsSending = restore(fxSendSms.pending, false);
const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);
const $isNotificationDialogOpen = restore(changeNotificationDialogVisibility, false);

export {
  fxGetList,
  PageGate,
  pageMounted,
  pageUnmounted,
  $raw,
  $normalized,
  $rowCount,
  $isLoading,
  $isSmsSending,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
  copy,
  fxCopy,
  changeNotificationDialogVisibility,
  $isNotificationDialogOpen,
  sendSms,
  fxSendSms,
  fxGetBuildings,
};
