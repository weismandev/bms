import { signout } from '../../common';
import { $isFilterOpen, $filters } from './filter.model';

$isFilterOpen.reset(signout);
$filters.reset(signout);
