import { createEvent, restore } from 'effector';

const defaultFilters = {
  state: '',
  strategy: '',
  with_car: '',
};

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = restore(changedFilterVisibility, false);
const $filters = restore(filtersSubmitted, defaultFilters);

export {
  changedFilterVisibility,
  filtersSubmitted,
  $isFilterOpen,
  $filters,
  defaultFilters,
};
