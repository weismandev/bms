import { signout } from '../../common';
import { $currentCompany } from './table.model';

$currentCompany.reset(signout);
