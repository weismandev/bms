import i18n from '@shared/config/i18n';
import { Modal } from '@ui/index';

const { t } = i18n;

function NotificationDialog(props) {
  const { isOpen, onClose } = props;

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      header={t('ActionCompletedSuccessfully')}
      content={t('ThePassDataIsCopied')}
    />
  );
}

export { NotificationDialog };
