import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  modalHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  toolbar: {
    height: 80,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  submitButton: {
    padding: '0 20px',
    marginRight: 10,
  },
});
