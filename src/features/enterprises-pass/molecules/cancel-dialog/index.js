import { Formik, Form, Field } from 'formik';
import { IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { InputField, ActionButton, Modal, Toolbar } from '@ui/index';
import { useStyles } from './styles';

const { t } = i18n;

export const CancelDialog = ({ isOpen, changeVisibility, revoke }) => {
  const classes = useStyles();
  return (
    <Modal
      isOpen={isOpen}
      header={
        <div className={classes.modalHeader}>
          <span>{t('PassRevoke')}</span>
          <IconButton onClick={() => changeVisibility(false)}>
            <Close />
          </IconButton>
        </div>
      }
      content={
        <Formik
          initialValues={{ comment: '' }}
          onSubmit={(values) => {
            revoke(values);
            changeVisibility(false);
          }}
          render={() => (
            <Form>
              <Field
                style={{ height: 120 }}
                name="comment"
                component={InputField}
                divider={false}
                required
                label={t('ReasonForWithdrawal')}
                placeholder={t('EnterTheReasonForTheReview')}
                rowsMax={5}
                multiline
              />
              <Toolbar className={classes.toolbar}>
                <ActionButton className={classes.submitButton} type="submit">
                  {t('Revoke')}
                </ActionButton>
              </Toolbar>
            </Form>
          )}
        />
      }
    />
  );
};
