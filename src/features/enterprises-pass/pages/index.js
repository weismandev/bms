import { combine } from 'effector';
import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { HaveSectionAccess } from '../../common';
import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
  $isNotificationDialogOpen,
  changeNotificationDialogVisibility,
} from '../models/page.model';
import { NotificationDialog } from '../molecules';
import { Table, Detail, Filter } from '../organisms';

const $stores = combine({
  isDetailOpen: $isDetailOpen,
  isFilterOpen: $isFilterOpen,
  isLoading: $isLoading,
  isErrorDialogOpen: $isErrorDialogOpen,
  error: $error,
  isNotificationDialogOpen: $isNotificationDialogOpen,
});

const EnterprisesPassPage = (props) => {
  const {
    isDetailOpen,
    isFilterOpen,
    isLoading,
    isErrorDialogOpen,
    error,
    isNotificationDialogOpen,
  } = useStore($stores);

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <ColoredTextIconNotification />

      <NotificationDialog
        isOpen={isNotificationDialogOpen}
        onClose={() => changeNotificationDialogVisibility(false)}
      />

      <PageGate />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        filter={isFilterOpen && <Filter />}
      />
    </>
  );
};

const RestrictedEnterprisesPassPage = (props) => (
  <HaveSectionAccess>
    <EnterprisesPassPage />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesPassPage as EnterprisesPassPage };
