import { createStore, createEvent, combine, split, sample } from 'effector';
import FileSaver from 'file-saver';
import i18n from '@shared/config/i18n';
import { $normalizedData, $rawRentObjects } from './page.model';

const { t } = i18n;

const pageNumberChanged = createEvent();
const pageSizeChanged = createEvent();
const columnWidthsChanged = createEvent();
const columnOrderChanged = createEvent();
const hiddenColumnChanged = createEvent();
const searchChanged = createEvent();
const expandedRowIdsChanged = createEvent();
const exportClicked = createEvent();

const $expandedRowIds = createStore([]);
const $currentPage = createStore(0);
const $pageSize = createStore(10);
const $search = createStore('');
const $columnWidths = createStore([
  { columnName: 'id', width: 200 },
  { columnName: 'title', width: 300 },
  { columnName: 'address', width: 350 },
  { columnName: 'area', width: 150 },
  { columnName: 'capacity', width: 150 },
  { columnName: 'floor', width: 150 },
  { columnName: 'renter', width: 250 },
  { columnName: 'owner', width: 250 },
  { columnName: 'status', width: 150 },
]);

const $columnOrder = createStore([
  'id',
  'title',
  'address',
  'area',
  'capacity',
  'floor',
  'renter',
  'owner',
  'status',
]);
const $hiddenColumnNames = createStore([]);

const workplace = split(
  sample($expandedRowIds, expandedRowIdsChanged, (ids, newIds) => ({
    ids,
    id: newIds.length ? newIds[newIds.length - 1] : null,
  })),
  {
    extend: ({ ids, id }) => id !== null && !ids.includes(id),
    collapsed: ({ ids, id }) => id === null || ids.includes(id),
  }
);

const $tableParams = combine(
  $currentPage,
  $pageSize,
  $search,
  (page, per_page, search) => ({
    page: page + 1,
    per_page,
    search,
  })
);

const $tableData = $normalizedData.map((data) => formatDataToTable(Object.values(data)));

const $rowCount = $rawRentObjects.map(({ meta }) => (meta && meta.total) || 0);

function formatDataToTable(items) {
  return items.map(formatItem);
}

function formatItem(item) {
  return {
    id: item.id,
    parent_id: item.parent_id,
    title: item.title || t('Untitled'),
    address: item.buildings.length
      ? item.buildings.map((b) => b.building).join('; ')
      : t('Unknown'),
    area: item.area || t('Unknown'),
    capacity: item.capacity || item.capacity === 0 ? item.capacity : t('Unknown'),
    floor: item.floor === 0 ? t('servicFloor') : item.floor ?? t('Unknown'),
    renter: (item.rent && item.rent.renter) || t('thereIsNo'),
    owner: (item.owner && item.owner.title) || t('thereIsNo'),
    status: item.rent ? t('Busy') : t('Free'),
    type: item.type,
  };
}

export {
  $currentPage,
  $pageSize,
  $rowCount,
  $tableData,
  $tableParams,
  pageNumberChanged as pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  searchChanged,
  $search,
  expandedRowIdsChanged,
  $expandedRowIds,
  workplace,
  exportClicked,
};

exportClicked.watch(() => {
  FileSaver.saveAs(
    'https://files.mysmartflat.ru/crm/templates/properties_import_template.xlsx',
    t('ImportTemplate')
  );
});
