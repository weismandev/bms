import { merge, forward, attach } from 'effector';
import { signout } from '../../common';
import { $filters } from './filter.model';
import { pageUnmounted, fxGetCoworkingWorkplaces } from './page.model';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  searchChanged,
  $search,
  $expandedRowIds,
  workplace,
} from './table.model';

$currentPage
  .on(pageNumberChanged, (state, number) => number)
  .on(merge([searchChanged, pageSizeChanged, $filters]), (state) => 0)
  .reset([pageUnmounted, signout]);

$search.on(searchChanged, (state, value) => value).reset(signout);

$expandedRowIds
  .on(workplace.extend, (state, { id }) => state.concat(id))
  .on(workplace.collapsed, (state, { id, ids }) =>
    id === null ? [] : ids.filter((i) => i === id)
  )
  .reset([pageNumberChanged, pageSizeChanged, searchChanged, signout]);

$columnWidths.on(columnWidthsChanged, (state, widths) => widths).reset(signout);
$columnOrder.on(columnOrderChanged, (state, order) => order).reset(signout);

$hiddenColumnNames.on(hiddenColumnChanged, (state, names) => names).reset(signout);

$pageSize
  .on(pageSizeChanged, (state, qty) => qty)
  .reset(pageUnmounted)
  .reset(signout);

forward({
  from: workplace.extend,
  to: attach({
    effect: fxGetCoworkingWorkplaces,
    mapParams: ({ id }) => id,
  }),
});
