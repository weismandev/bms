import { createEffect, createStore, createEvent, merge } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const changedDeleteDialogVisibility = createEvent();
const changedErrorDialogVisibility = createEvent();
const deleteConfirmed = createEvent();
const fileUploaded = createEvent();

const fxGetPropertyList = createEffect();
const fxGetPropertyTypes = createEffect();
const fxGetPropertyById = createEffect();
const fxCreateProperty = createEffect();
const fxUpdateProperty = createEffect();
const fxDeleteProperty = createEffect();
const fxGetRentsListByPropertyId = createEffect();
const fxCreateRent = createEffect();
const fxUpdateRent = createEffect();
const fxDeleteRent = createEffect();
const fxGetCoworkingWorkplaces = createEffect();
const fxImportRentObjects = createEffect();

const $rawRentObjects = createStore({});
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);
const $isDeleteDialogOpen = createStore(false);

const $normalizedData = $rawRentObjects.map(normalizeData);

const requestOccured = merge([
  fxGetPropertyList.pending,
  fxCreateProperty.pending,
  fxUpdateProperty.pending,
  fxDeleteProperty.pending,
  fxGetPropertyById.pending,
  fxGetRentsListByPropertyId.pending,
  fxCreateRent.pending,
  fxUpdateRent.pending,
  fxDeleteRent.pending,
  fxGetCoworkingWorkplaces.pending,
  fxImportRentObjects.pending,
]);

const errorOccured = merge([
  fxGetPropertyList.fail,
  fxCreateProperty.fail,
  fxUpdateProperty.fail,
  fxDeleteProperty.fail,
  fxGetPropertyById.fail,
  fxGetRentsListByPropertyId.fail,
  fxCreateRent.fail,
  fxUpdateRent.fail,
  fxDeleteRent.fail,
  fxGetCoworkingWorkplaces.fail,
  fxImportRentObjects.fail,
]);

function normalizeData({ items }) {
  return Array.isArray(items)
    ? items.reduce(
        (acc, item) => ({
          ...acc,
          [item.id]: item,
        }),
        {}
      )
    : {};
}

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  changedDeleteDialogVisibility,
  changedErrorDialogVisibility,
  deleteConfirmed,
  fxGetPropertyList,
  fxGetPropertyTypes,
  fxGetPropertyById,
  fxCreateProperty,
  fxUpdateProperty,
  fxDeleteProperty,
  fxGetRentsListByPropertyId,
  fxCreateRent,
  fxUpdateRent,
  fxDeleteRent,
  $rawRentObjects,
  $normalizedData,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $isDeleteDialogOpen,
  requestOccured,
  errorOccured,
  fxGetCoworkingWorkplaces,
  fileUploaded,
  fxImportRentObjects,
};
