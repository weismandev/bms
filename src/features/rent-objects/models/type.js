import { createEffect, createStore, guard, sample } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { rentApi } from '../api';

const SelectGate = createGate();
const selectMounted = SelectGate.open;

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(rentApi.getPropertyTypes);

$data.on(fxGetList.done, (state, { result }) => formatResponse(result)).reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, pending) => pending).reset(signout);

guard({
  source: sample($data, selectMounted),
  filter: notLoadedEarlier,
  target: fxGetList,
});

function formatResponse(response = []) {
  return response.data;
}

function notLoadedEarlier(data = []) {
  return data.length === 0;
}

export { $data, $error, $isLoading, fxGetList, SelectGate };
