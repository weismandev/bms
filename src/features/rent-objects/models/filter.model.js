import { createEvent, createStore } from 'effector';

const defaultFilters = { property_types: [] };

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = createStore(false);
const $filters = createStore(defaultFilters);

export {
  changedFilterVisibility,
  $isFilterOpen,
  filtersSubmitted,
  $filters,
  defaultFilters,
};
