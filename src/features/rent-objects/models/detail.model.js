import { createStore, createEvent, split } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const newRentObject = {
  title: '',
  buildings: '',
  type: '',
  capacity: '',
  area: '',
  floor: '',
  rent_available: false,
  rent_cost: 0,
  photos: [],
  schema: '',
  parent: '',
  owner_enterprise: '',
};

const DEFAULT_START_WORK_HOUR = '00:00';
const DEFAULT_END_WORK_HOUR = '23:59';

const changedDetailVisibility = createEvent();
const changeMode = createEvent();
const detailSubmitted = createEvent();
const addClicked = createEvent();
const open = createEvent();
const selectTab = createEvent();
const deletePropertyRent = createEvent();
const savePropertyRent = createEvent();

const $isDetailOpen = createStore(false);
const $mode = createStore('view');
const $opened = createStore(newRentObject);

const $normalizedBuilds = createStore({});
const $buildWorkTimes = createStore({
  start: DEFAULT_START_WORK_HOUR,
  end: DEFAULT_END_WORK_HOUR,
});

const $currentTab = createStore('entity');
const $propertyRents = createStore([]);

const $tabsConfig = $opened.map((opened) => [
  {
    label: t('RentalObject'),
    value: 'entity',
    onCreateIsDisabled: false,
  },
  {
    label: t('rental'),
    value: 'rent',
    onCreateIsDisabled: true,
    disabled: opened.type && opened.type.name === 'coworking',
  },
]);

const detailClosed = changedDetailVisibility.filterMap((visibility) => {
  if (!visibility) return visibility;
  else return null;
});

const entityApi = split(detailSubmitted, {
  save: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

export {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  entityApi,
  addClicked,
  $isDetailOpen,
  $mode,
  open,
  $opened,
  detailClosed,
  newRentObject,
  $tabsConfig,
  selectTab,
  $currentTab,
  $propertyRents,
  deletePropertyRent,
  savePropertyRent,
  $normalizedBuilds,
  DEFAULT_START_WORK_HOUR,
  DEFAULT_END_WORK_HOUR,
  $buildWorkTimes,
};
