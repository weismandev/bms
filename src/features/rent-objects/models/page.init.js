import { forward, sample, attach } from 'effector';
import {
  rentApi,
  getExtendedType,
  getFormattedBuilding,
  getTypeAttributesNames,
} from '..';
import { pick } from '../../../tools/pick';
import { signout } from '../../common';
import { $opened } from './detail.model';
import { $filters } from './filter.model';
import {
  fxGetPropertyList,
  fxGetPropertyTypes,
  fxGetPropertyById,
  fxCreateProperty,
  fxUpdateProperty,
  fxDeleteProperty,
  fxGetRentsListByPropertyId,
  fxCreateRent,
  fxUpdateRent,
  fxDeleteRent,
  $rawRentObjects,
  pageMounted,
  changedDeleteDialogVisibility,
  $isDeleteDialogOpen,
  deleteConfirmed,
  pageUnmounted,
  requestOccured,
  errorOccured,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetCoworkingWorkplaces,
  fxImportRentObjects,
  fileUploaded,
} from './page.model';
import { $tableParams } from './table.model';
import { $data as $types } from './type';

$rawRentObjects
  .on(fxGetPropertyList.done, (state, { result }) => ({
    ...result,
    items: result.items.map((i) => ({ ...i, parent_id: null })),
  }))
  .on(fxGetCoworkingWorkplaces.done, (state, { params: id, result }) => ({
    ...state,
    items: state.items.concat(result.workplaces),
  }))
  .on(
    sample($types, fxCreateProperty.done, (types, { result }) =>
      formatResponse(result, types)
    ),
    addNewProperty
  )
  .on(
    sample($types, fxUpdateProperty.done, (types, { result }) =>
      formatResponse(result, types)
    ),
    updateProperty
  )
  .on(fxDeleteProperty.done, deleteProperty)
  .reset(signout);

$isDeleteDialogOpen
  .on(changedDeleteDialogVisibility, (state, visibility) => visibility)
  .on(deleteConfirmed, () => false)
  .reset(pageUnmounted)
  .reset(signout);

$isLoading
  .on(requestOccured, (state, pending) => pending)
  .reset([pageUnmounted, signout]);

const newRequestStarted = requestOccured.filter({
  fn: (isLoading) => isLoading,
});

$error
  .on(errorOccured, (state, { error }) => error)
  .on(newRequestStarted, () => null)
  .reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (state, visibility) => visibility)
  .on(errorOccured, () => true)
  .on(newRequestStarted, () => false)
  .reset([pageUnmounted, signout]);

fxGetPropertyList.use(rentApi.getPropertyList);
fxGetPropertyTypes.use(rentApi.getPropertyTypes);
fxGetPropertyById.use(rentApi.getPropertyById);
fxCreateProperty.use(rentApi.createProperty);
fxUpdateProperty.use(rentApi.updateProperty);
fxDeleteProperty.use(rentApi.deleteProperty);
fxGetRentsListByPropertyId.use(rentApi.getRentsListByPropertyId);
fxCreateRent.use(rentApi.createRent);
fxUpdateRent.use(rentApi.updateRent);
fxDeleteRent.use(rentApi.deleteRent);
fxGetCoworkingWorkplaces.use(rentApi.getCoworkingWorkplaces);
fxImportRentObjects.use(rentApi.importRentObjects);

forward({
  from: [pageMounted, $tableParams, $filters, fxImportRentObjects.done],
  to: attach({
    effect: fxGetPropertyList,
    source: { table: $tableParams, filters: $filters },
    mapParams: (params, { table, filters }) => {
      const { search, ...restTableParams } = table;

      const payload = { ...restTableParams };

      if (search) {
        payload.search = search;
      }

      if (Array.isArray(filters.property_types) && filters.property_types.length) {
        payload.filter = {
          property_types: filters.property_types.map((i) => i.id),
        };
      }

      return payload;
    },
  }),
});

forward({ from: pageMounted, to: fxGetPropertyTypes });

forward({
  from: sample($opened, deleteConfirmed, ({ id }) => id),
  to: fxDeleteProperty,
});

forward({ from: fileUploaded, to: fxImportRentObjects });

function formatResponse(response, types) {
  try {
    return formatProperty(response.data.property, types);
  } catch (error) {
    console.error(
      'Error happens! Server response is incorrect! Check it!!!',
      response,
      error
    );
    return {};
  }
}

function formatProperty(property, types) {
  const defaultRent = null;
  const defaultTypeTitle = '';

  const commonProperties = pick(['id', 'title', 'parent_id'], property);
  const rent = property.rent ? property.rent : defaultRent;

  const extendedType = getExtendedType(property.type, types);
  const typeTitle = extendedType ? extendedType.title : defaultTypeTitle;
  const attributesKeys = getTypeAttributesNames(extendedType);
  const attributesProperties = pick(attributesKeys, property);
  const building = getFormattedBuilding(property.building);

  const formatted = {
    ...commonProperties,
    type: typeTitle,
    building: building ? building.title : '',
    complex: getBuildingComplex(property.building),
    ...attributesProperties,
    rent,
    parent_id: property.parent_id || null,
  };

  return formatted;
}

function getBuildingComplex(building) {
  const defaultComplex = '';

  try {
    return building.complex.title;
  } catch (error) {
    console.error('Incorrect input arg!!!', error);
    return defaultComplex;
  }
}

function addNewProperty(state, newProperty) {
  const newState = { items: [], meta: { total: 0 }, ...state };
  return {
    ...newState,
    meta: { ...newState.meta, total: Number(newState.meta.total) + 1 },
    items: [newProperty].concat(newState.items),
  };
}

function updateProperty(state, property) {
  const newState = { items: [], meta: { total: 0 }, ...state };

  const itemIndex = newState.items.findIndex(
    (item) => String(item.id) === String(property.id)
  );

  return {
    ...newState,
    items:
      itemIndex > -1
        ? newState.items
            .slice(0, itemIndex)
            .concat(property)
            .concat(newState.items.slice(itemIndex + 1))
        : newState.items,
  };
}

function deleteProperty(state, { params: id }) {
  const newState = { items: [], meta: { total: 0 }, ...state };

  return {
    ...newState,
    items: newState.items.filter((i) => i.id !== id),
    meta: { ...newState.meta, total: newState.meta.total - 1 },
  };
}
