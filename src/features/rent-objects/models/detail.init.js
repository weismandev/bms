import { merge, forward, sample, guard, attach, split } from 'effector';
import format from 'date-fns/format';
import { signout, readNotifications, RENT_CREATED } from '@features/common';
import { $data as $builds } from '@features/house-select';
import { formatFile } from '@tools/formatFile';
import { pick } from '@tools/pick';
import { getExtendedType, getFormattedBuilding, getTypeAttributesNames } from '..';
import {
  $mode,
  changeMode,
  open,
  $opened,
  changedDetailVisibility,
  $isDetailOpen,
  addClicked,
  detailClosed,
  entityApi,
  newRentObject,
  selectTab,
  $currentTab,
  $propertyRents,
  deletePropertyRent,
  savePropertyRent,
  $normalizedBuilds,
  DEFAULT_START_WORK_HOUR,
  DEFAULT_END_WORK_HOUR,
  $buildWorkTimes,
} from './detail.model';
import {
  fxCreateProperty,
  fxUpdateProperty,
  fxDeleteProperty,
  fxGetPropertyById,
  fxGetRentsListByPropertyId,
  fxDeleteRent,
  fxCreateRent,
  fxUpdateRent,
} from './page.model';
import { $data as $types } from './type';

$isDetailOpen
  .on(changedDetailVisibility, (_, visibility) => visibility)
  .on(merge([addClicked, open]), () => true)
  .on(fxDeleteProperty.done, () => false)
  .reset(signout);

$mode
  .on(changeMode, (_, mode) => mode)
  .on(addClicked, () => 'edit')
  .on(
    merge([
      detailClosed,
      fxCreateProperty.done,
      fxUpdateProperty.done,
      fxDeleteProperty.done,
      fxGetPropertyById.done,
    ]),
    () => 'view'
  )
  .reset(signout);

$opened
  .on(
    sample($types, fxGetPropertyById.done, (types, { result }) => ({
      types,
      data: result,
    })),
    (_, payload) => {
      const { data, types } = payload;
      return formatDetailData(data.property, types);
    }
  )
  .on(
    sample(
      $types,
      merge([fxCreateProperty.done, fxUpdateProperty.done]),
      (types, { result }) => formatDetailData(result.data.property, types)
    ),
    (_, formattedProperty) => formattedProperty
  )
  .on(merge([addClicked, detailClosed, fxDeleteProperty.done]), () => newRentObject)
  .reset(signout);

$currentTab
  .on(selectTab, (_, selectedTab) => selectedTab)
  .on(merge([addClicked, open]), () => 'entity')
  .reset(signout);

$propertyRents
  .on(
    sample($opened, fxGetRentsListByPropertyId.done, (opened, { result: rents }) => ({
      opened,
      rents,
    })),
    formatPropertyRents
  )
  .on(
    sample($opened, fxCreateRent.done, (opened, { result: createdRents }) => ({
      opened,
      createdRents,
    })),
    (state, { opened, createdRents }) => [
      formatPropertyRent(createdRents.rents[0], opened),
      ...state,
    ]
  )
  .on(
    sample($opened, fxUpdateRent.done, (opened, { result: updatedRents }) => ({
      opened,
      updatedRents,
    })),
    (state, { opened, updatedRents }) => {
      const formattedRent = formatPropertyRent(updatedRents.rents[0], opened);
      const updatedRentIdx = state.findIndex(
        (rent) => String(rent.id) === String(formattedRent.id)
      );

      return [
        ...state.slice(0, updatedRentIdx),
        formattedRent,
        ...state.slice(updatedRentIdx + 1),
      ];
    }
  )
  .on(fxDeleteRent, (state, id) => state.filter((rent) => String(rent.id) !== String(id)))
  .on(addClicked, () => [])
  .reset(signout);

forward({
  from: entityApi.save.map(prepareSubmittedPayload),
  to: fxCreateProperty,
});

forward({
  from: entityApi.update.map(prepareSubmittedPayload),
  to: fxUpdateProperty,
});

forward({
  from: open,
  to: fxGetPropertyById,
});

sample({
  clock: fxCreateProperty.doneData,
  fn: (_, { property }) => property.id,
  target: fxGetPropertyById,
});

guard({
  source: $opened,
  filter: ({ id }) => Boolean(id),
  target: attach({
    effect: fxGetRentsListByPropertyId,
    mapParams: ({ id }) => id,
  }),
});

forward({
  from: deletePropertyRent,
  to: fxDeleteRent,
});

const propertyRentApi = split(savePropertyRent, {
  create: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

forward({
  from: propertyRentApi.create,
  to: attach({
    effect: fxCreateRent,
    source: $opened,
    mapParams: formatRentPayload,
  }),
});

forward({
  from: propertyRentApi.update,
  to: attach({
    effect: fxUpdateRent,
    source: $opened,
    mapParams: formatRentPayload,
  }),
});
$normalizedBuilds
  .on($builds, (_, builds) => (Array.isArray(builds) ? normalizeBuilds(builds) : {}))
  .reset(signout);

sample({
  source: [$opened, $normalizedBuilds],
  target: $buildWorkTimes,
  fn: ([{ buildings }, normBuilds]) => getBuildingWorkTimes(normBuilds, buildings),
});

function formatRentPayload({ userdata, start_date, finish_date, ...rest }, opened) {
  const { id, type } = opened;

  const formatStart = (date) => format(date, 'yyyy-MM-dd').concat(' 00:00:00');
  const formatFinish = (date) => format(date, 'yyyy-MM-dd').concat(' 23:59:00');

  return {
    rents: [
      {
        ...rest,
        property_id: id,
        start_date:
          type && type.name === 'pantry'
            ? formatStart(start_date)
            : format(start_date, 'yyyy-MM-dd hh:mm:ss'),
        finish_date:
          type && type.name === 'pantry'
            ? formatFinish(finish_date)
            : format(finish_date, 'yyyy-MM-dd hh:mm:ss'),
        userdata_id: userdata?.director_id || '',
        enterprise_id: userdata?.id,
      },
    ],
  };
}

function formatPropertyRents(_, { opened, rents }) {
  try {
    return rents.items
      .sort((a, b) => Number(b.id) - Number(a.id))
      .map((item) => formatPropertyRent(item, opened));
  } catch (error) {
    console.error('Error happens!', error);
    return [];
  }
}

function formatPropertyRent(rent) {
  const { id, title, description, human_start_date, human_finish_date, enterprise } =
    rent;

  return {
    id,
    userdata: enterprise || '',
    title,
    description,
    start_date: new Date(human_start_date),
    finish_date: new Date(human_finish_date),
  };
}

function prepareSubmittedPayload(payload) {
  const {
    title,
    photos,
    schema,
    type,
    buildings,
    id = null,
    rent_available,
    rent_cost,
    parent,
    owner_enterprise,
  } = payload;

  const attrs = pick(getTypeAttributesNames(type), payload);
  const typeData = typeof type === 'object' && type?.id && type;

  const building_ids = Array.isArray(buildings)
    ? buildings.map((building) => (typeof building === 'object' ? building.id : building))
    : [];

  return {
    id,
    title,
    parent_id:
      typeData && typeData.name === 'workplace' && parent
        ? typeof parent === 'object'
          ? parent.id
          : parent
        : '',
    rent_available: rent_available ? 1 : 0,
    owner_enterprise_id:
      Boolean(owner_enterprise) && typeof owner_enterprise === 'object'
        ? owner_enterprise.id
        : owner_enterprise,
    rent_cost: rent_cost || 0,
    type_id: typeData ? typeData.id : '',
    building_id: building_ids.length > 0 ? building_ids[0] : [],
    building_ids,

    attrs: {
      ...attrs,
      photos:
        attrs.photos && Array.isArray(photos)
          ? photos.map((i) => (i.id ? { id: i.id } : { file: i.file }))
          : [],
      schema:
        (schema && schema.id && { id: schema.id }) ||
        (schema && schema.file && { file: schema.file }) ||
        '',
    },
  };
}

function formatDetailData(data = {}, types = []) {
  const result = {
    ...pick(['id', 'title', 'area', 'floor', 'capacity'], data),
    rent_available: Boolean(data.rent_available),
    owner_enterprise: data.owner_enterprise_id || '',
    parent: data.parent_id || '',
    rent_cost: data.rent_cost ? data.rent_cost : '',
    type: getExtendedType(data.type, types),
    buildings: Array.isArray(data.buildings)
      ? data.buildings.map((building) => getFormattedBuilding(building)?.id || '')
      : [],
    schema:
      data.schema && data.schema.id
        ? formatFile({ ...data.schema, mime_type: 'image/*' })
        : '',
    photos: Array.isArray(data.photos)
      ? data.photos.map((i) => formatFile({ id: i.id, ...i.file, mime_type: 'image/*' }))
      : [],
  };
  return result;
}

function normalizeBuilds(array) {
  return array.reduce((acc, build) => ({ ...acc, [build.id]: build }), {});
}

function getBuildingWorkTimes(normBuilds, buildings) {
  const id = buildings[0]?.id;
  const targetBuilding = normBuilds[id]?.building;

  if (targetBuilding) {
    const { start_work_time, end_work_time } = targetBuilding;
    return {
      start: start_work_time || DEFAULT_START_WORK_HOUR,
      end: end_work_time || DEFAULT_END_WORK_HOUR,
    };
  }

  return {
    start: DEFAULT_START_WORK_HOUR,
    end: DEFAULT_END_WORK_HOUR,
  };
}

$propertyRents.watch((propertyRents) => {
  propertyRents.forEach((rent) =>
    readNotifications({ id: rent.id, event: RENT_CREATED })
  );
});
