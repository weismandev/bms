import { pick } from '../../../tools/pick';

const defaultTypeValue = '';

function getExtendedType(type, types) {
  try {
    return getExtendedTypeFromTypesOrDefault(type, types);
  } catch (error) {
    console.error('Error happens! Check your input args!!!', error);
    return defaultTypeValue;
  }
}

function getExtendedTypeFromTypesOrDefault(type, types) {
  const typeInfo = types.find((i) => i.id === type.id);
  const isTypeInfoFound = typeInfo !== undefined;
  return isTypeInfoFound ? typeInfo : defaultTypeValue;
}

const defaultBuildingValue = '';

function getFormattedBuilding(building) {
  try {
    return getFormattedBuildingOrDefault(building.building);
  } catch (error) {
    console.error('Error happens! Check your input args!!!', error);
    return defaultBuildingValue;
  }
}

function getFormattedBuildingOrDefault(building) {
  const formattedBuilding = pick(['id', 'title'], building);
  return formattedBuilding ? formattedBuilding : defaultBuildingValue;
}

function getTypeAttributesNames(type) {
  const isArgCorrect =
    typeof type === 'object' &&
    Object.prototype.hasOwnProperty.call(type, 'attributes') &&
    Array.isArray(type.attributes);

  return isArgCorrect ? type.attributes.map((i) => i.key) : [];
}

export { getExtendedType, getFormattedBuilding, getTypeAttributesNames };
