import { api } from '../../../api/api2';

const getPropertyList = (payload) => api.v1('get', 'property/get-list-crm', payload);
const getPropertyTypes = () => api.v1('get', 'property/get-types');
const getPropertyById = (id) => api.v1('get', 'property/get-by-id', { id });
const createProperty = (payload) => api.v1('post', 'property/create', payload);
const updateProperty = (payload) => api.v1('post', 'property/update', payload);
const deleteProperty = (id) => api.v1('post', 'property/delete', { id });

const getRentsListByPropertyId = (id) =>
  api.v1('get', 'rents/crm-get-by-property', { property_id: id });
const createRent = (payload) => api.v1('post', 'rents/crm-create', payload);
const updateRent = (payload) => api.v1('post', 'rents/crm-update', payload);
const deleteRent = (id) => api.v1('post', 'rents/crm-delete', { id });

const getCoworkingWorkplaces = (id) =>
  api.v1('get', 'property/get-coworking-workplaces', { id });

const importRentObjects = (file) => api.v1('post', 'property/import', { import: file });

export const rentApi = {
  getPropertyList,
  getPropertyTypes,
  getPropertyById,
  createProperty,
  updateProperty,
  deleteProperty,
  getRentsListByPropertyId,
  createRent,
  updateRent,
  deleteRent,
  getCoworkingWorkplaces,
  importRentObjects,
};
