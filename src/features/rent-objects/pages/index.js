import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  DeleteConfirmDialog,
  Loader,
  ErrorMessage,
} from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isDetailOpen,
  $isFilterOpen,
  $isDeleteDialogOpen,
  changedDeleteDialogVisibility,
  deleteConfirmed,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
} from '../models';
import { RentObjectsTable, Detail, Filter } from '../organisms';

const RentObjectsPage = (props) => {
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);

  const error = useStore($error);
  return (
    <>
      <PageGate />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => changedDeleteDialogVisibility(false)}
        confirm={deleteConfirmed}
      />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<RentObjectsTable />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedRentObjectsPage = (props) => (
  <HaveSectionAccess>
    <RentObjectsPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedRentObjectsPage as RentObjectsPage };
