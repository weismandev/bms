import { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { setHours, setMinutes } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import { AllCompaniesField } from '@features/all-companies-select';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  InputField,
  FormSectionHeader,
  InputControl,
  PhoneMaskedInput,
  CustomScrollbar,
} from '@ui/index';
import {
  $propertyRents,
  savePropertyRent,
  deletePropertyRent,
  $buildWorkTimes,
} from '../../models';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  rents: Yup.array().of(
    Yup.object().shape({
      userdata: Yup.object().required(t('thisIsRequiredField')).nullable(),
      start_date: Yup.string()
        .required(t('thisIsRequiredField'))
        .test(
          'start_date-after-finish_date',
          t('StartDateBeforeEndDate'),
          startDateAfterFinishDate
        )
        .nullable(),
      finish_date: Yup.string()
        .required(t('thisIsRequiredField'))
        .test(
          'start_date-after-finish_date',
          t('EndDateAfterStartDate'),
          startDateAfterFinishDate
        )
        .nullable(),
    })
  ),
});

function startDateAfterFinishDate() {
  const isDatesDefined =
    Boolean(this.parent.start_date) && Boolean(this.parent.finish_date);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.start_date)) >= Number(new Date(this.parent.finish_date))
  ) {
    return this.createError();
  }

  return true;
}

const hasLeastOneNewRent = (rents = []) => rents.filter(({ id }) => !id).length > 0;

function RentForm() {
  const { start, end } = useStore($buildWorkTimes);
  const propertyRents = useStore($propertyRents);

  const getDefaultDates = (time) => {
    const [hour, minute] = time.split(':');
    return setHours(setMinutes(new Date(), Number(minute)), Number(hour));
  };

  const newRent = {
    id: null,
    title: '',
    userdata: '',
    description: '',
    start_date: getDefaultDates(start),
    finish_date: getDefaultDates(end),
  };

  return (
    <Formik
      initialValues={{ rents: propertyRents }}
      validationSchema={validationSchema}
      enableReinitialize
      render={(formikProps) => {
        const { values } = formikProps;
        return (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <div style={{ height: '100%', padding: '0 6px 24px 24px' }}>
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <FieldArray
                    name="rents"
                    render={({ form, remove, unshift }) => (
                      <>
                        <div style={{ padding: 24 }}>
                          <Button
                            color="primary"
                            style={{ width: '100%' }}
                            disabled={hasLeastOneNewRent(values.rents)}
                            onClick={() => unshift(newRent)}
                          >
                            {t('CreateRent')}
                          </Button>
                        </div>
                        {values.rents.map((rent, idx) => (
                          <div key={rent.id ? rent.id : 'new'}>
                            <RentItem
                              form={form}
                              isNewRent={rent.id}
                              id={rent.id}
                              idx={idx}
                              workTimes={{ start, end }}
                              name={`rents[${idx}]`}
                              remove={(idx) => remove(idx)}
                              deleteRent={() => deletePropertyRent(rent.id)}
                              saveRent={(idx) => savePropertyRent(values.rents[idx])}
                            />
                            {rent.userdata && rent.userdata.user ? (
                              <>
                                <InputControl
                                  label={t('ResponsiblePerson')}
                                  readOnly
                                  value={rent.userdata.user.fullname || ''}
                                />
                                <InputControl
                                  inputComponent={PhoneMaskedInput}
                                  label={t('ResponsiblePhone')}
                                  readOnly
                                  value={rent.userdata.user.phone || ''}
                                />
                              </>
                            ) : null}
                          </div>
                        ))}
                      </>
                    )}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
}

const RentItem = (props) => {
  const { form, name, idx, isNewRent, remove, deleteRent, saveRent, workTimes, id } =
    props;

  const [mode, setMode] = useState(isNewRent === null ? 'edit' : 'view');
  const isEditMode = mode === 'edit';
  const { start, end } = workTimes;
  const today = new Date();

  useEffect(() => {
    if (id) {
      setMode('view');
    }
  }, [id]);

  return (
    <>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <FormSectionHeader
          style={{ margin: 0, width: '100%' }}
          header={id ? `${t('rental')} ${id}` : t('NewRent')}
        />
        <DetailToolbar
          mode={mode}
          onEdit={() => setMode('edit')}
          onCancel={() => {
            if (isNewRent !== null) {
              setMode('view');
              form.resetForm();
            } else {
              remove(idx);
            }
          }}
          onSave={() => {
            saveRent(idx);
          }}
          onDelete={deleteRent}
          style={{ height: 80 }}
          hidden={{ close: true, edit: isEditMode, delete: isEditMode }}
        />
      </div>
      <Field
        name={`${name}.title`}
        label={t('Name')}
        component={InputField}
        placeholder={t('EnterTheTitle')}
        mode={mode}
      />
      <Field
        name={`${name}.userdata`}
        label={t('tenant')}
        required
        divider
        component={AllCompaniesField}
        placeholder={t('ChooseTenant')}
        mode={mode}
      />
      <Field
        name={`${name}.start_date`}
        render={({ field, form }) => (
          <DatePicker
            minTime={setHours(
              setMinutes(new Date(), Number(start.split(':')[1])),
              Number(start.split(':')[0])
            )}
            maxTime={setHours(
              setMinutes(new Date(), Number(start.split(':')[1])),
              Number(end.split(':')[0])
            )}
            customInput={<InputField field={field} form={form} label={t('startDate')} />}
            name={field.name}
            readOnly={mode === 'view'}
            selected={field.value}
            onChange={(value) => form.setFieldValue(field.name, value)}
            minDate={today}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={30}
            locale={dateFnsLocale}
            timeCaption={t('Label.time').toLowerCase()}
            dateFormat="dd MMMM yyyy в HH:mm"
            popperPlacement="top"
          />
        )}
      />
      <Field
        name={`${name}.finish_date`}
        render={({ field, form }) => (
          <DatePicker
            minTime={setHours(
              setMinutes(new Date(), Number(end.split(':')[1])),
              Number(start.split(':')[0])
            )}
            maxTime={setHours(
              setMinutes(new Date(), Number(end.split(':')[1])),
              Number(end.split(':')[0])
            )}
            customInput={
              <InputField field={field} form={form} label={t('expirationDate')} />
            }
            readOnly={mode === 'view'}
            name={field.name}
            selected={field.value}
            onChange={(value) => form.setFieldValue(field.name, value)}
            minDate={today}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={30}
            locale={dateFnsLocale}
            timeCaption={t('Label.time').toLowerCase()}
            dateFormat="dd MMMM yyyy в HH:mm"
            popperPlacement="top"
          />
        )}
      />

      <Field
        name={`${name}.description`}
        label={t('Description')}
        multiline
        component={InputField}
        placeholder={t('EnterADescription')}
        mode={mode}
      />
    </>
  );
};

export { RentForm };
