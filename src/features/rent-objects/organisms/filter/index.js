import { memo } from 'react';
import { useStore } from 'effector-react';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import { TypeSelectField } from '..';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';

const { t } = i18n;

const Filter = memo(() => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={() => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="property_types"
                component={TypeSelectField}
                isMulti
                label={t('type')}
                placeholder={t('chooseType')}
              />

              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
