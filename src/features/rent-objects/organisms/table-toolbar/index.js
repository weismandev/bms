import { useStore } from 'effector-react';
import {
  Toolbar,
  FoundInfo,
  AddButton,
  FilterButton,
  Greedy,
  SearchInput,
  FileControl,
  ImportButton,
  DownloadTemplateButton,
} from '@ui';
import { featureFlags } from '@configs/feature-flags';
import {
  $rowCount,
  addClicked,
  $mode,
  changedFilterVisibility,
  $isFilterOpen,
  $search,
  searchChanged,
  fileUploaded,
} from '../../models';
import { exportClicked } from '../../models/table.model';

const isCRUDAllowed = featureFlags.oldRentsCRUD;

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const mode = useStore($mode);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);
  const modeWithCRUDAllowed = isCRUDAllowed ? (mode === 'edit') : true;

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ margin: '0 10px' }} />
      <SearchInput
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />

      <DownloadTemplateButton onClick={exportClicked} disabled={!isCRUDAllowed} />

      <FileControl
        mode="edit"
        name="file"
        encoding="file"
        inputProps={{
          disabled: !isCRUDAllowed,
        }}
        onChange={(value) => {
          if (value && value.file) {
            fileUploaded(value.file);
          }
        }}
        renderPreview={() => null}
        renderButton={() => (
          <ImportButton
            component="span"
            style={{ margin: '0 0 0 10px' }}
            disabled={!isCRUDAllowed}
          />
        )}
      />

      <AddButton
        disabled={modeWithCRUDAllowed}
        onClick={addClicked}
        style={{ margin: '0 10px' }}
      />
    </Toolbar>
  );
};

export { TableToolbar };
