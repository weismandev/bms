import { useState } from 'react';
import { useStore } from 'effector-react';
import {
  DetailToolbar,
  InputField,
  SwitchField,
  ActionFileWrapper,
  FileControl,
  BaseInputLabel,
  Divider,
  ActionButton,
  Modal,
  InputControl,
  PhoneMaskedInput,
  CustomScrollbar,
} from '@ui';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Button, IconButton } from '@mui/material';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { featureFlags } from '@configs/feature-flags';
import { TypeSelectField } from '..';
import {
  CompaniesWithDirectorInfoSelectField,
  findById,
} from '../../../companies-with-director-info-select';
import { HouseSelectField } from '../../../house-select';
import { PropertySelectField } from '../../../property-select';
import {
  $opened,
  changeMode,
  detailSubmitted,
  changedDetailVisibility,
  changedDeleteDialogVisibility,
} from '../../models';

const { t } = i18n;
const isCRUDAllowed = featureFlags.oldRentsCRUD;

const mapTypeToInputType = {
  int: 'number',
  float: 'number',
  number: 'number',
  string: 'text',
  text: 'text',
};

const useStyles = makeStyles({
  fullwidth: {
    width: '100%',
    minWidth: '50%',
    textAlign: 'center',
  },
});

function SchemaPreview(props) {
  const { preview } = props;

  const [isFullScreenOpen, setFullScreen] = useState(false);
  const classes = useStyles();

  const content = (
    <img
      src={preview || ''}
      alt={t('CameraVideo')}
      style={{
        width: 'inherit',
        objectFit: 'contain',
      }}
    />
  );

  return (
    <>
      <div style={{ position: 'relative' }}>
        <div style={{ width: '100%' }}>{content}</div>
        <IconButton
          onClick={() => setFullScreen(true)}
          style={{
            borderRadius: 'unset',
            position: 'absolute',
            right: 0,
            top: 0,
          }}
          size="large"
        >
          <FullscreenIcon />
        </IconButton>
      </div>
      <Modal
        onClose={() => setFullScreen(false)}
        isOpen={isFullScreenOpen}
        content={content}
        classes={{ paper: classes.fullwidth }}
      />
    </>
  );
}

function RentObjectForm(props) {
  const { mode, isNew } = props;

  const opened = useStore($opened);
  const validationSchema = Yup.object().shape({
    buildings: Yup.array().min(1, t('thisIsRequiredField')),
    type: Yup.object().required(t('thisIsRequiredField')).nullable(),
  });

  return (
    <Formik
      initialValues={opened}
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      enableReinitialize
      render={({ values, resetForm }) => {
        const attributes =
          typeof values.type === 'object' && values.type !== null
            ? values.type.attributes
            : [];

        const owner =
          (Boolean(values.owner_enterprise) &&
            typeof values.owner_enterprise === 'object' &&
            values.owner_enterprise) ||
          findById(values.owner_enterprise);

        return (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              hidden={{ edit: !isCRUDAllowed, delete: !isCRUDAllowed }}
              onEdit={() => changeMode('edit')}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm();
                }
              }}
              onDelete={() => changedDeleteDialogVisibility(true)}
            />

            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    name="schema"
                    render={({ field, form }) => (
                      <FileControl
                        inputProps={{ accept: 'image/*' }}
                        encoding="file"
                        onChange={(value) => form.setFieldValue(field.name, value)}
                        mode={mode}
                        name={field.name}
                        value={field.value}
                        renderButton={() => (
                          <>
                            <ActionButton component="span">
                              {field.value ? t('ReplaceSchema') : t('UploadScheme')}
                            </ActionButton>
                            <Divider />
                          </>
                        )}
                        renderPreview={({ preview }) =>
                          field.value && preview ? (
                            <SchemaPreview preview={preview} />
                          ) : null
                        }
                      />
                    )}
                  />

                  <Field
                    name="title"
                    label={t('Name')}
                    labelPlacement="start"
                    placeholder={t('EnterTheTitle')}
                    component={InputField}
                    mode={mode}
                  />

                  <Field
                    name="owner_enterprise"
                    label={t('OwnerCompany')}
                    placeholder={t('ChooseCompany')}
                    component={CompaniesWithDirectorInfoSelectField}
                    mode={mode}
                  />

                  {owner ? (
                    <>
                      <InputControl
                        label={t('owner')}
                        readOnly
                        value={owner.director_fullname || ''}
                      />
                      <InputControl
                        inputComponent={PhoneMaskedInput}
                        label={t('OwnerPhone')}
                        readOnly
                        value={formatPhone(owner.director_phone) || ''}
                      />
                    </>
                  ) : null}

                  <Field
                    name="buildings"
                    component={HouseSelectField}
                    label={t('Label.address')}
                    isMulti
                    placeholder={t('ChooseAddress')}
                    mode={mode}
                    required
                  />

                  <Field
                    name="type"
                    component={TypeSelectField}
                    label={t('type')}
                    placeholder={t('chooseType')}
                    mode={isNew ? mode : 'view'}
                    required
                  />

                  {values.type && String(values.type.name) === 'workplace' ? (
                    <Field
                      name="parent"
                      component={PropertySelectField}
                      label={t('CoworkingSpace')}
                      placeholder={t('ChooseRoom')}
                      mode={mode}
                      required
                    />
                  ) : null}

                  <Field
                    name="rent_available"
                    component={SwitchField}
                    label={t('RentAvailable')}
                    labelPlacement="start"
                    disabled={mode === 'view'}
                  />
                  {!values.owner_enterprise && (
                    <Field
                      name="rent_cost"
                      label={t('RentalPricePerMonth')}
                      component={InputField}
                      mode={mode}
                      type="number"
                    />
                  )}
                  {attributes.map(({ key, required, title, type }) => {
                    const inputType = mapTypeToInputType[type] || 'text';
                    const name = key;

                    return (
                      type !== 'array' && (
                        <Field
                          key={key}
                          component={InputField}
                          name={name}
                          required={required}
                          label={title}
                          placeholder={`${t('Enters')} ${title.toLowerCase()}`}
                          type={inputType}
                          mode={mode}
                        />
                      )
                    );
                  })}
                  <FieldArray
                    name="photos"
                    render={({ push, remove }) => {
                      const photos = values.photos.map((i, idx) => {
                        const { meta, ...rest } = i;

                        return (
                          <ActionFileWrapper
                            key={idx}
                            deleteFn={mode === 'edit' ? () => remove(idx) : null}
                            url={rest.preview}
                            {...rest}
                            {...meta}
                          />
                        );
                      });

                      return (
                        <>
                          <BaseInputLabel style={{ marginBottom: 10 }}>
                            {t('CompanyImages')}
                          </BaseInputLabel>
                          <div
                            style={{
                              display: 'flex',
                              flexWrap: 'wrap',
                              justifyContent: 'center',
                            }}
                          >
                            {photos}
                            <FileControl
                              inputProps={{ accept: 'image/*' }}
                              name="image"
                              encoding="file"
                              value=""
                              renderButton={() => (
                                <Button
                                  style={{ width: 150, height: 150 }}
                                  component="span"
                                  color="primary"
                                >
                                  {t('PictureControlUpload')}
                                </Button>
                              )}
                              renderPreview={() => null}
                              onChange={(value) => value && push(value)}
                              mode={mode}
                            />
                          </div>
                          <Divider />
                        </>
                      );
                    }}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
}

export { RentObjectForm };
