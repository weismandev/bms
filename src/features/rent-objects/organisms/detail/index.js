import { memo } from 'react';
import { useStore } from 'effector-react';
import { Wrapper, Tabs } from '../../../../ui';
import { $opened, $mode, $tabsConfig, selectTab, $currentTab } from '../../models';
import { RentForm, RentObjectForm } from '../index';

const Detail = memo((props) => {
  const mode = useStore($mode);
  const currentTab = useStore($currentTab);
  const opened = useStore($opened);
  const isNew = !opened.id;
  const tabsConfig = useStore($tabsConfig);

  const renerTabs = () =>
    currentTab === 'rent' ? <RentForm /> : <RentObjectForm mode={mode} isNew={isNew} />;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Tabs
        options={tabsConfig}
        currentTab={currentTab}
        onChange={(e, value) => selectTab(value)}
        isNew={isNew}
      />
      {renerTabs()}
    </Wrapper>
  );
});

export { Detail };
