import { useMemo, useCallback, memo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  CustomTreeData,
  TreeDataState,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
  TableTreeColumn,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { TableToolbar } from '..';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  $rowCount,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $opened,
  open,
} from '../../models';
import { expandedRowIdsChanged, $expandedRowIds } from '../../models/table.model';

const { t } = i18n;

const Row = (props) => {
  const opened = useStore($opened);

  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const columns = [
  { name: 'id', title: '№' },
  { name: 'title', title: t('Name') },
  { name: 'address', title: t('Label.address') },
  { name: 'area', title: t('area') },
  {
    name: 'capacity',
    title: t('Capacity'),
  },
  { name: 'floor', title: t('Floor') },
  { name: 'renter', title: t('tenant') },
  { name: 'owner', title: t('PremisesOwner') },
  { name: 'status', title: t('Label.status') },
];

const expandedType = t('Coworking');

const getChildRows = (row, rootRows) => {
  const childRows = rootRows.filter((r) => r.parent_id === (row ? row.id : null));

  if (childRows.length) {
    return childRows;
  }

  return row && row.type === expandedType ? [] : null;
};

const RentObjectsTable = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const expandedRowIds = useStore($expandedRowIds);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={rowCount} />
        <DragDropProvider />

        <TreeDataState
          expandedRowIds={expandedRowIds}
          onExpandedRowIdsChange={expandedRowIdsChanged}
        />
        <CustomTreeData getChildRows={getChildRows} />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableTreeColumn for="id" />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { RentObjectsTable };
