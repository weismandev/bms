import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $error, $isLoading, SelectGate } from '../../models/type';

const TypeSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <SelectGate />
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
    </>
  );
};

const TypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<TypeSelect />} onChange={onChange} {...props} />;
};

export { TypeSelect, TypeSelectField };
