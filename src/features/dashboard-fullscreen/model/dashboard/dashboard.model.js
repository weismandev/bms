import { createEvent } from 'effector';
import { createGate } from 'effector-react';

export const DashboardGate = createGate();
export const dashboardUnmounted = DashboardGate.close;
export const dashboardMounted = DashboardGate.open;

export const getUserInfo = createEvent();
