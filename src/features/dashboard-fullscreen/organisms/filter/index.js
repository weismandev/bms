import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import makeStyles from '@mui/styles/makeStyles';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

import { HouseSelectField } from '@features/house-select';

import {
  Wrapper,
  CustomScrollbar,
  InputField,
  FormSectionHeader,
  Divider,
  FilterFooter,
} from '@ui/index';

import { $filters, filtersSubmitted } from '../../model/filter';

const { t } = i18n;
const useStyles = makeStyles({
  period_title: {
    color: '#65657B',
    fontWeight: 500,
    marginBottom: 12,
  },
  date: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  container: {
    padding: 24,
  },
  footer: {
    '& button': {
      width: '150px !important',
    },
  },
});

const validationSchema = Yup.object().shape({
  buildings: Yup.mixed().required(t('SelectObjectFromList')),
  start: Yup.date().required(),
  finish: Yup.date().required(),
});

export const Filter = () => {
  const classes = useStyles();
  const filters = useStore($filters);

  const filterResettled = () => filtersSubmitted('');

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <div className={classes.container}>
          <FormSectionHeader header={t('Filter')} />
          <Formik
            onSubmit={filtersSubmitted}
            onReset={filterResettled}
            initialValues={filters}
            enableReinitialize
            validationSchema={validationSchema}
            render={() => (
              <Form>
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  label={t('AnObject')}
                  placeholder={t('selectObject')}
                  isMulti
                  isClearable={false}
                />
                <div>
                  <p className={classes.period_title}>{t('period')}</p>
                  <div className={classes.date}>
                    <Field
                      name="start"
                      label={null}
                      divider={false}
                      type="date"
                      component={InputField}
                    />
                    <Field
                      name="finish"
                      label={null}
                      divider={false}
                      type="date"
                      component={InputField}
                    />
                  </div>
                  <Divider isVisible />
                </div>
                <div className={classes.footer}>
                  <FilterFooter isResettable />
                </div>
              </Form>
            )}
          />
        </div>
      </CustomScrollbar>
    </Wrapper>
  );
};
