import { format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';

const today = () => {
  const now = new Date();
  now.setHours(0, 0, 0, 0);

  return now;
};

export const getMonthDays = () => {
  const monthAgo = today();

  const start = format(monthAgo.setMonth(monthAgo.getMonth() - 1), 'yyyy-MM-dd', {
    locale: dateFnsLocale,
  });
  const finish = format(today(), 'yyyy-MM-dd', { locale: dateFnsLocale });

  return {
    start,
    finish,
  };
};
