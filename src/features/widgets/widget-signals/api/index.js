import { api } from '@api/api2';

const getWidgetsList = () => api.no_vers('get', '/bmsdevice/get-widgets');
const getWidgetsData = (payload) =>
  Promise.all(
    payload.map(({ id }) => api.no_vers('get', '/bmsdevice/get-widgets-data', { id }))
  );

export const widgetsApi = {
  getWidgetsList,
  getWidgetsData,
};
