import cn from 'classnames';
import useStyles from './styles';

const Status = ({ isExists }) => {
  const { wrapper, selected } = useStyles();

  return (
    <div className={wrapper}>
      <span className={!isExists ? selected : ''}>0</span>
      <span className={isExists ? selected : ''}>1</span>
    </div>
  );
};

export default Status;
