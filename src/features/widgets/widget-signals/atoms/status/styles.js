import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  wrapper: {
    backgroundColor: '#71C3F1',
    borderRadius: 30,
    width: 50,
    height: 24,
    color: 'white',
    fontWeight: 500,
    fontSize: 14,
    display: 'flex',
    justifyContent: 'space-around',
    lineHeight: '25px',
  },

  selected: {
    backgroundColor: 'white',
    color: '#3B3B50',
    borderRadius: 50,
    height: 20,
    width: 20,
    marginTop: 2,
    lineHeight: '20px',
    textAlign: 'center',
  },
});

export default useStyles;
