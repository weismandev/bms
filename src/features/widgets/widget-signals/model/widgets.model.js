import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const fxGetList = createEffect();
const fxGetData = createEffect();

const $raw = createStore([]);
const $data = createStore([]);
const $isLoading = createStore(false);

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  fxGetList,
  fxGetData,
  $raw,
  $data,
  $isLoading,
};
