import { sample } from 'effector';
import { signout } from '@features/common';
import { dashboardMounted } from '@features/dashboard/model/dashboard/dashboard.model';
import { widgetsApi } from '../api';
import { fxGetList, fxGetData, $isLoading, $raw, $data } from './widgets.model';

fxGetList.use(widgetsApi.getWidgetsList);
fxGetData.use(widgetsApi.getWidgetsData);

$isLoading.on(fxGetList.pending, (_, isLoading) => isLoading).reset(signout);

$raw.on(fxGetList.done, (_, { result: { widgest } }) => widgest).reset(signout);
$data.on(fxGetData.done, (_, { result }) => result).reset(signout);

sample({
  clock: dashboardMounted,
  target: fxGetList,
});

sample({
  clock: $raw,
  target: fxGetData,
});
