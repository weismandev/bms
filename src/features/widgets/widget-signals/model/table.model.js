import { combine } from 'effector';
import Status from '../atoms/status';
import { $raw, $data } from './widgets.model';

export const columns = [
  {
    field: 'title',
    headerName: '',
    width: 600,
  },
  {
    field: 'status',
    headerName: '',
    width: 100,
    renderCell: ({ row: { status, control_type } }) => {
      switch (control_type) {
        case 'text':
          return <div style={{ textAlign: 'center' }}>{status}</div>;
        case 'switch':
          return <Status isExists={status} />;
        default:
          return <div style={{ textAlign: 'center' }}>{status}</div>;
      }
    },
  },
];

export const $tableData = combine($raw, $data, (raw, data) => {
  const res = raw.map((item) => {
    const { widgest = [] } = data.find(
      ({ widgest }) => widgest.filter((list) => list.id === item.id) || {}
    );

    const signalList = widgest
      .map(({ signals }) => signals)
      .flat()
      .reduce((acc, { id, data = [] }) => ({ ...acc, [id]: data[0].v }), {});

    return {
      id: item.id,
      title: item.title,
      rows: item.params.map((row) => ({
        id: row.signal_id,
        title: row.title,
        control_type: row.control_type,
        status: signalList[row.signal_id],
      })),
    };
  });
  return res;
});
