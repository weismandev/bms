import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  wrapper: {
    border: 'none',

    '& .MuiDataGrid-columnsContainer': {
      display: 'none',
    },
    '& .MuiDataGrid-window': {
      top: '0px !important',
    },
  },
});

export default useStyles;
