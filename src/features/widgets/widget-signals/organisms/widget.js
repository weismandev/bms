import { useStore } from 'effector-react';
import { Widget, CustomScrollbar, Loader } from '@ui/';
import { DataGrid } from '@features/data-grid';

import { $isLoading, $tableData, columns } from '../model';

import useStyles from './styles';

export const WidgetSignals = () => {
  const { wrapper } = useStyles();

  const isLoading = useStore($isLoading);
  const tableData = useStore($tableData);

  return tableData.map((widget) => (
    <Widget settings={{ title: widget.title }} key={widget.id}>
      <CustomScrollbar
        style={{ width: '100%' }}
        autoHide
        autoHeight
        autoHeightMin={0}
        autoHeightMax={520}
      >
        <div style={{ width: '100%', height: '100%' }}>
          <Loader isLoading={isLoading} />
          <DataGrid.Mini
            rows={widget.rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            disableColumnMenu
            border={false}
            autoHeight
            hideFooter
            density="compact"
            className={wrapper}
          />
        </div>
      </CustomScrollbar>
    </Widget>
  ));
};
