import { FilterSelect } from '@ui/';

export const HouseSelect = ({ list, complex, ...props }) => {
  const options = complex ? list.filter((item) => item.complex.id === complex) : list;

  return <FilterSelect list={options} {...props} />;
};
