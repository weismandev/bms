import { createStore, createEffect, createEvent } from 'effector';

function createIntervalBag(unmount, interval) {
  const getTick = createEvent();
  const $defaultInterval = createStore(interval);
  const $interval = createStore(setInterval(() => getTick(), interval));

  $interval.on(unmount, (state, {}) => {
    clearInterval(state);
  });

  return {
    getTick,
    $defaultInterval,
    $interval,
  };
}

export { createIntervalBag };
