import { format } from 'date-fns';
import { convertNumberToHours } from './convertNumberToHours';

const formatDate = (sourceDate) => {
  const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  const date = new Date();
  const timezoneOffset = date.getTimezoneOffset();
  const convertedTimezone = convertNumberToHours(timezoneOffset);

  return `${format(sourceDate, FORMAT)}${convertedTimezone}`;
};

export { formatDate };
