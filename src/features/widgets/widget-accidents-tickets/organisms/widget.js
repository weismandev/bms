import { useGate, useStore } from 'effector-react';
import { Filter } from '@features/widgets/widget-accidents/organisms/filter';
import i18n from '@shared/config/i18n';
import { useWindowSize } from '@tools/useWindowSize';
import { Widget } from '@ui/index';
import { $filters, filtersSubmitted } from '../models/filter.model';
import { $tableData } from '../models/table.model';
import { WidgetGate, $isLoading, $error, $refreshRate } from '../models/widget.model';
import { Table } from './table';

const { t } = i18n;

export const WidgetAccidentsTickets = () => {
  useGate(WidgetGate, { widgetType: 'tickets' });
  const windowSize = useWindowSize();
  const refreshRate = useStore($refreshRate);
  const isLoading = useStore($isLoading);

  const settings = {
    title: t('CustomerRequests'),
  };

  const renderFilter = () => (
    <Filter filtersStore={$filters} filtersSubmitted={filtersSubmitted} />
  );

  return (
    <Widget
      settings={settings}
      refreshRate={refreshRate}
      isLoading={isLoading}
      renderTool={windowSize.width > 1180 ? renderFilter : null}
    >
      {windowSize.width <= 1180 && renderFilter()}
      <Table tableDataStore={$tableData} errorStore={$error} />
    </Widget>
  );
};
