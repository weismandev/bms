import { createTableBag } from '@features/widgets/widget-accidents/factories/table';
import { fetchingDone } from './widget.model';

export const { $tableData } = createTableBag(fetchingDone);
