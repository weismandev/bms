import React from 'react';
import { useStore } from 'effector-react';
import { makeStyles } from '@mui/styles';

import { Empty } from '@ui/';
import { DataGrid } from '@features/data-grid';

import i18n from '@shared/config/i18n';

import { $tableData, $tableDataError, columns } from '../models';

const { t } = i18n;

const useStyles = makeStyles({
  tableRoot: {
    cursor: 'pointer',
  },
  handlerWrap: {
    height: '50px',
  },
  tableContent: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
  content: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
});

const Table = React.memo(() => {
  const data = useStore($tableData);
  const error = useStore($tableDataError);
  const classes = useStyles();

  if (!data?.length) {
    return (
      <div className={classes.content}>
        <Empty
          title={t('NoViolationsOfTheScheduleForTheCurrentCalendarDayWereRecorded')}
        />
      </div>
    );
  }
  if (error) {
    return (
      <div className={classes.content}>
        <p>{error}</p>
      </div>
    );
  }
  return (
    <div className={classes.tableContent}>
      <DataGrid.Mini
        rows={data}
        columns={columns}
        disableColumnMenu
        disableSelectionOnClick
        border={false}
        hideFooter
        density="compact"
      />
    </div>
  );
});

export { Table };
