import { useGate, useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { Widget } from '@ui/';
import { ScheduleWidgetGate, $tableDataLoading, intervalValue } from '../models';
import { Table } from './table';

const { t } = i18n;

export const WidgetSchedule = ({ size = {}, data }) => {
  useGate(ScheduleWidgetGate, data);
  const isLoading = useStore($tableDataLoading);
  const settings = { title: t('ScheduleViolations'), size };

  return (
    <Widget settings={settings} refreshRate={intervalValue} isLoading={isLoading}>
      <Table />
    </Widget>
  );
};
