import { makeStyles } from '@mui/styles';
import i18n from '@shared/config/i18n';
import { ActionButton } from '@ui/';

const useStyles = makeStyles({
  linkButton: {
    height: '29px !important',
    fontSize: '13px !important',
    fontWeight: 500,
    textDecoration: 'none',
  },
});

const { t } = i18n;

const LinkButton = ({ data }) => {
  const classes = useStyles();

  const handleClick = (e) => {
    window.open('/job-schedule', '_blank');
  };

  return (
    <ActionButton kind="basic" className={classes.linkButton} onClick={handleClick}>
      {t('ToTheSchedule')}
    </ActionButton>
  );
};

export { LinkButton };
