import { sample, forward } from 'effector';
import { format } from 'date-fns';
import { formatDate } from '@features/widgets/tools/formatDate';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { getRecords } from '../api';
import {
  ScheduleWidgetGate,
  mount,
  unmount,
  startInterval,
  stopInterval,
  tick,
  $tableData,
  $tableDataError,
  $tableDataLoading,
  fxGetRecords,
} from './widget.model';

fxGetRecords.use(getRecords);

$tableData.on(fxGetRecords.doneData, preprocessRecords);

$tableDataError
  .on(fxGetRecords.failData, (_, error) => {
    return error.toString().replace(/Error:/, '');
  })
  .reset($tableData);

$tableDataLoading.on(fxGetRecords.pending, (_, pending) => pending);

sample({
  clock: mount,
  target: startInterval,
});

sample({
  source: ScheduleWidgetGate.state,
  clock: [mount, tick],
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetRecords.prepend(() => ({
    date: formatDate(new Date()),
  })),
});

forward({
  from: unmount,
  to: stopInterval,
});

function preprocessRecords(_, data) {
  if (!data.length) return [];

  const tempData = data.map((item, key) => {
    const planned_start_at =
      item?.planned_start_at && item?.planned_start_at?.length > 0
        ? format(
            convertUTCDateToLocalDate(new Date(item.planned_start_at)),
            'dd.MM.YYY в HH:mm'
          )
        : '';

    return {
      id: key,
      section: item.section,
      name: Array.isArray(item.name) ? item.name.join(', ') : item.name,
      region_name: item.region_name,
      complex_title: item.complex_title,
      address: item.address,
      property_title: item.property_title,
      planned_start_at,
    };
  });

  // Сортируем по дате, более поздние сверху
  return tempData.sort((a, b) => {
    if (a.planned_start_at > b.planned_start_at) return -1;
    if (a.planned_start_at < b.planned_start_at) return 1;
    return 0;
  });
}
