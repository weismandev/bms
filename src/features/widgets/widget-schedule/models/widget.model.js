import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { interval } from 'patronum';
import i18n from '@shared/config/i18n';
import { LinkButton } from '../molecules/linkButton';

const { t } = i18n;

export const ScheduleWidgetGate = createGate();
export const { open: mount, close: unmount } = ScheduleWidgetGate;

export const startInterval = createEvent();
export const stopInterval = createEvent();
export const intervalValue = 900000;
export const { tick, isRunning } = interval({
  timeout: intervalValue,
  start: startInterval,
  stop: stopInterval,
});

export const fxGetRecords = createEffect();

export const getRecordsTick = createEvent();

export const $tableData = createStore([]);
export const $tableDataLoading = createStore(false);
export const $tableDataError = createStore(null);

export const columns = [
  {
    field: 'name',
    headerName: t('FullNameTitle'),
    width: 150,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'complex_title',
    headerName: t('building'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'address',
    headerName: t('room'),
    type: 'number',
    width: 220,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'planned_start_at',
    headerName: t('DatePlan'),
    type: 'number',
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'id',
    headerName: t('More'),
    type: 'number',
    width: 120,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
    renderCell: (cell) => <LinkButton data={cell} />,
  },
];
