import { api } from '@api/api2';

export const getRecords = (payload = null) => {
  return api.v4('get', 'schedule/employee-ticket-work-delay-list', payload);
};
