import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { makeStyles } from '@mui/styles';
import i18n from '@shared/config/i18n';
import { FilterSelect } from '@ui/';
import {
  $filters,
  filtersSubmitted,
  periodFilter,
  criterionFilter,
} from '../models/filters/filters.model';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  form: {
    display: 'grid',
    gridTemplateColumns: 'repeat(2, auto)',
    justifyItems: 'start',
    justifyContent: 'start',
    alignItems: 'center',
    alignContent: 'center',
    gap: 16,
    color: '#68687e',
  },
}));

export const Filter = () => {
  const filters = useStore($filters);
  const classes = useStyles();

  return (
    <Formik
      initialValues={filters}
      onSubmit={filtersSubmitted}
      onReset={() => filtersSubmitted('')}
      enableReinitialize
      render={({ values, setFieldValue, handleSubmit, handleReset }) => (
        <Form className={classes.form}>
          <Field
            name="period"
            label={t('period')}
            list={periodFilter}
            component={FilterSelect}
            noDefault
            onChange={(value) => {
              setFieldValue('period', value);
              setTimeout(handleSubmit, 0);
            }}
          />

          <Field
            name="criterion"
            label={t('Criterion')}
            list={criterionFilter}
            component={FilterSelect}
            noDefault
            onChange={(value) => {
              setFieldValue('criterion', value);
              setTimeout(handleSubmit, 0);
            }}
          />
        </Form>
      )}
    />
  );
};
