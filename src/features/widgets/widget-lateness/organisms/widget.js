import { useGate, useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { Widget } from '@ui/';
import { $tableDataLoading } from '../models/table';
import { intervalValue, LatenessWidgetGate } from '../models/widget';
import { Filter } from './filter';
import { Table } from './table';

const { t } = i18n;

export const WidgetLateness = ({ size = {}, data }) => {
  useGate(LatenessWidgetGate, data);
  const isLoading = useStore($tableDataLoading);

  const settings = {
    title: t('EmployeesBeingLate'),
    size,
  };

  return (
    <Widget
      settings={settings}
      renderTool={() => <Filter />}
      refreshRate={intervalValue}
      isLoading={isLoading}
    >
      <Table />
    </Widget>
  );
};
