import { createEvent } from 'effector';
import { createGate } from 'effector-react';
import { interval } from 'patronum';

export const LatenessWidgetGate = createGate();
export const { open: mount, close: unmount } = LatenessWidgetGate;

export const startInterval = createEvent();
export const stopInterval = createEvent();
export const intervalValue = 900000;
export const { tick, isRunning } = interval({
  timeout: intervalValue,
  start: startInterval,
  stop: stopInterval,
});
