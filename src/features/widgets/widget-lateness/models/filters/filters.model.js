import i18n from '@shared/config/i18n';
import { createFilterBag } from '@tools/factories';

const { t } = i18n;

const defaultFilters = {
  period: 1,
  criterion: 1,
};

const periodFilter = [
  {
    id: 1,
    title: t('Day'),
  },
  {
    id: 2,
    title: t('Week'),
  },
  {
    id: 3,
    title: t('Month'),
  },
  {
    id: 4,
    title: t('DuringAllThisTime'),
  },
];

const criterionFilter = [
  {
    id: 1,
    title: t('NumberOfTardiness'),
  },
  {
    id: 2,
    title: t('TotalDelayTime'),
  },
  {
    id: 3,
    title: t('Absenteeism'),
  },
];

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

export { periodFilter, criterionFilter };
