import { sample, forward, attach } from 'effector';
import { formatDate } from '@features/widgets/tools/formatDate';
import { getRecords } from '../../api';
import { $filters, filtersSubmitted } from '../filters/filters.model';
import {
  LatenessWidgetGate,
  tick,
  mount,
  unmount,
  startInterval,
  stopInterval,
} from '../widget';
import {
  $tableData,
  $tableDataError,
  $tableDataLoading,
  fxGetRecords,
} from './table.model';

fxGetRecords.use(getRecords);

$tableData.on(fxGetRecords.doneData, preprocessRecords);

$tableDataError
  .on(fxGetRecords.failData, (_, error) => {
    return error.toString().replace(/Error:/, '');
  })
  .reset($tableData);

$tableDataLoading.on(fxGetRecords.pending, (_, pending) => pending);

sample({
  clock: mount,
  target: startInterval,
});

sample({
  source: [LatenessWidgetGate.state, $filters],
  clock: [mount, tick, filtersSubmitted],
  target: attach({
    source: $filters,
    effect: fxGetRecords,
    mapParams: (_, filters) => {
      const payload = {};

      // Фмльтр - период
      const today = new Date();
      const todayDate = transformDate(new Date());

      switch (filters.period) {
        case 1:
          payload.date_from = todayDate;
          payload.date_to = todayDate;
          break;

        case 2:
          payload.date_from = transformDate(new Date(today.setDate(today.getDate() - 7)));
          payload.date_to = todayDate;
          break;

        case 3:
          payload.date_from = transformDate(
            new Date(today.setDate(today.getDate() - 31))
          );
          payload.date_to = todayDate;
          break;

        case 4:
          payload.date_from = transformDate(new Date(0));
          payload.date_to = todayDate;
          break;

        default:
          return null;
      }

      // Фмльтр - критерий
      switch (filters.criterion) {
        case 1:
          payload.order_column_name = 'completed_with_late_works_count';
          break;

        case 2:
          payload.order_column_name = 'delay_hours_count';
          break;

        case 3:
          payload.order_column_name = 'skipped_works_count';
          break;

        default:
          return null;
      }

      return payload;
    },
  }),
});

forward({
  from: unmount,
  to: stopInterval,
});

function transformDate(date) {
  return formatDate(date);
}

function preprocessRecords(_, data) {
  if (!data.length) return [];

  return data.map((item) => ({
    id: item.user_id,
    user_id: item.user_id,
    employee_name: item.employee_name,
    employee_position_title: item.employee_position_title,
    completed_with_late_works_count: item.completed_with_late_works_count,
    delay_hours_count: item.delay_hours_count,
    skipped_works_count: item.skipped_works_count,
  }));
}
