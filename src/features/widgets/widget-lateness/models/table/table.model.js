import { createStore, createEffect } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const fxGetRecords = createEffect();

export const $tableData = createStore([]);
export const $tableDataLoading = createStore(false);
export const $tableDataError = createStore(null);

export const columns = [
  {
    field: 'employee_name',
    headerName: t('Label.FullName'),
    width: 150,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'employee_position_title',
    headerName: t('Label.Position'),
    type: 'number',
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'completed_with_late_works_count',
    headerName: t('NumberOfLateArrivalsPerPeriod'),
    type: 'number',
    width: 140,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'delay_hours_count',
    headerName: t('TotalDelayTimeForThePeriodInHours'),
    type: 'number',
    width: 140,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'skipped_works_count',
    headerName: t('Absenteeism'),
    type: 'number',
    width: 140,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
];
