import { createStore } from 'effector';

const $unreadedLevels = createStore(new Set());

export { $unreadedLevels };
