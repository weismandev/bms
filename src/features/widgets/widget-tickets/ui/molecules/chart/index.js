import makeStyles from '@mui/styles/makeStyles';
import { EventTracker } from '@devexpress/dx-react-chart';
import {
  Chart as DXChart,
  BarSeries,
  ValueAxis,
  Tooltip,
} from '@devexpress/dx-react-chart-material-ui';

const useStyles = makeStyles({
  chart: {
    '& > div': {
      height: '100% !important',
    },
  },
});

const barWithCustomColors = (tickets) => (props) => {
  const { argument } = props;

  const { color } = tickets.find((ticket) => ticket.name === argument);

  return <BarSeries.Point {...props} color={color} />;
};

export const Chart = ({ tickets }) => {
  const classes = useStyles();

  return (
    <div className={classes.chart}>
      <DXChart data={tickets}>
        <ValueAxis />

        <BarSeries
          valueField="count"
          argumentField="name"
          pointComponent={barWithCustomColors(tickets)}
        />

        <EventTracker />
        <Tooltip />
      </DXChart>
    </div>
  );
};
