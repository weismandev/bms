import makeStyles from '@mui/styles/makeStyles';
import { Ticket } from '../../atoms/ticket';

const useStyles = makeStyles({
  list: {
    display: 'flex',
    flexDirection: 'column',
  },
});

export const TicketsList = ({ tickets }) => {
  const classes = useStyles();

  const list = tickets.map((ticket) => {
    const key = ticket.title;

    return <Ticket key={key} settings={ticket} />;
  });

  return <div className={classes.list}>{list}</div>;
};
