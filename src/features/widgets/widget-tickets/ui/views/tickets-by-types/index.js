import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  $tickets,
  $ticketsLoading,
  $ticketsError,
} from '../../../model/tickets-by-types';
import { TicketsError } from '../../atoms/error';
import { TicketsLoading } from '../../atoms/loading';
import { Chart } from '../../molecules/chart';
import { TicketsList } from '../../molecules/tickets-list';

const useStyles = makeStyles({
  tickets: {
    height: 'calc(100% - 50px)',
    display: 'grid',
    gridTemplateColumns: '1fr 2fr',
    gap: '10px',
  },
});

const { t } = i18n;

export const TicketsByTypes = () => {
  const classes = useStyles();

  const tickets = useStore($tickets);
  const error = useStore($ticketsError);
  const loading = useStore($ticketsLoading);

  if (loading) {
    return <TicketsLoading />;
  }

  if (!tickets?.length) return <div>{t('noData')}</div>;

  if (error) {
    return <TicketsError error={error} />;
  }

  return (
    <div className={classes.tickets}>
      <TicketsList tickets={tickets} />
      <Chart tickets={tickets} />
    </div>
  );
};
