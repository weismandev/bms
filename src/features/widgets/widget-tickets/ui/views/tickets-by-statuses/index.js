import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  $ticketsByStatuses,
  $ticketsByStatusesError,
  $ticketsByStatusesLoading,
} from '../../../model/tickets-by-statuses';
import { TicketsError } from '../../atoms/error';
import { TicketsLoading } from '../../atoms/loading';
import { Chart } from '../../molecules/chart';
import { TicketsList } from '../../molecules/tickets-list';

const useStyles = makeStyles({
  tickets: {
    height: 'calc(100% - 50px)',
    display: 'grid',
    gridTemplateColumns: '1fr 2fr',
    gap: '10px',
  },
});

const { t } = i18n;

export const TicketsByStatuses = () => {
  const tickets = useStore($ticketsByStatuses);
  const loading = useStore($ticketsByStatusesLoading);
  const error = useStore($ticketsByStatusesError);

  const classes = useStyles();

  if (loading) {
    return <TicketsLoading />;
  }

  if (!tickets?.length) return <div>{t('noData')}</div>;

  if (error) {
    return <TicketsError error={error} />;
  }

  return (
    <div className={classes.tickets}>
      <TicketsList tickets={tickets} />
      <Chart tickets={tickets} />
    </div>
  );
};
