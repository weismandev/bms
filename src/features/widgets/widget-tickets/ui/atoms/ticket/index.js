import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  ticket: {
    display: 'flex',
    alignItems: 'center',
    color: '#fff',
  },
  count: {
    minWidth: '30px',
    height: '30px',
    borderRadius: '4px',
    textAlign: 'center',
    lineHeight: '30px',
    marginRight: '12px',
    marginBottom: '8px',
    background: ({ color }) => color,
  },
  title: {
    color: '#65657B',
    margin: 0,
    fontSize: '12px',
    opacity: '0.7',
  },
});

export const Ticket = ({ settings }) => {
  const { color, title, count } = settings;

  const classes = useStyles({ color });

  return (
    <div className={classes.ticket}>
      <div className={classes.count}>{count}</div>
      <p className={classes.title}>{title}</p>
    </div>
  );
};
