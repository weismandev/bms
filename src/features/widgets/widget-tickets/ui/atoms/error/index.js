import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  error: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 'calc(100% - 80px)',
  },
});

export const TicketsError = ({ error }) => {
  const classes = useStyles();

  return (
    <div className={classes.error}>
      <span>{error}</span>
    </div>
  );
};
