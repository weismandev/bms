import { useStore, useGate } from 'effector-react';
import { Widget } from '@ui/index';
import {
  $currentTab,
  options,
  changeTab,
  TicketsWidgetGate,
} from '../../../model/widget';
import { TicketsByStatuses } from '../../views/tickets-by-statuses';
import { TicketsByTypes } from '../../views/tickets-by-types';

export const WidgetTickets = ({ size, data }) => {
  const currentTab = useStore($currentTab);
  useGate(TicketsWidgetGate, data);

  const tabs = {
    active: true,
    options,
    currentTab,
    changeTab,
  };

  const settings = {
    title: 'Заявки',
    tabs,
    size,
  };

  const currentTicketsView = {
    statuses: <TicketsByStatuses />,
    types: <TicketsByTypes />,
  };

  return <Widget settings={settings}>{currentTicketsView[currentTab]}</Widget>;
};
