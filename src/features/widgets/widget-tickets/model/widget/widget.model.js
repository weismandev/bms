import { createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const options = [
  {
    label: t('statuses'),
    value: 'statuses',
  },
  {
    label: t('Types'),
    value: 'types',
  },
];

export const changeTab = createEvent();

export const $currentTab = createStore('statuses');
export const TicketsWidgetGate = createGate();
