import { sample } from 'effector';
import { getTicketByStatuses } from '../../api/tickets-by-statuses';
import { $currentTab, TicketsWidgetGate } from '../widget';
import {
  $ticketsByStatuses,
  $ticketsByStatusesLoading,
  $ticketsByStatusesError,
  fxGetTicketsByStatuses,
  statuses,
} from './tickets-by-statuses.model';

fxGetTicketsByStatuses.use(getTicketByStatuses);

$ticketsByStatuses.on(fxGetTicketsByStatuses.doneData, preprocessTickets);

$ticketsByStatusesError
  .on(fxGetTicketsByStatuses.failData, (_, error) => {
    return error.toString().replace(/Error:/, '');
  })
  .reset($currentTab);

$ticketsByStatusesLoading.on(fxGetTicketsByStatuses.pending, (_, pending) => pending);

sample({
  source: TicketsWidgetGate.state,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetTicketsByStatuses,
});

function preprocessTickets(_, tickets) {
  if (!tickets.length) return [];

  return tickets.map((ticket) => {
    const type = ticket?.state?.id;
    const count = ticket?.count;

    return {
      count,
      ...statuses[type],
    };
  });
}
