import { createEffect, createStore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const statuses = {
  0: {
    title: t('New'),
    name: 'new',
    color: '#0394E3',
  },
  1: {
    title: t('Appointed'),
    name: 'assigned',
    color: '#7465EB',
  },
  2: {
    title: t('InWork'),
    name: 'work',
    color: '#3B2EA7',
  },
  3: {
    title: t('OnPause'),
    name: 'paused',
    color: '#AAAAAA',
  },
  4: {
    title: t('OnConfirmation'),
    name: 'confirmation',
    color: '#43B469',
  },
  5: {
    title: t('Сompleted'),
    name: 'done',
    color: '#00A400',
  },
  6: {
    title: t('rejected'),
    name: 'denied',
    color: '#EF952B',
  },
  7: {
    title: t('Returned'),
    name: 'returned',
    color: '#EB5757',
  },
  8: {
    title: t('Canceled'),
    name: 'canceled',
    color: '#9C4040',
  },
  9: {
    title: t('Closed'),
    name: 'closed',
    color: '#52526C',
  },
};

export const colorsByStatus = Object.values(statuses).reduce((acc, { name, color }) => {
  return {
    ...acc,
    [name]: color,
  };
}, {});

export const fxGetTicketsByStatuses = createEffect();

export const $ticketsByStatuses = createStore([]);
export const $ticketsByStatusesLoading = createStore(false);
export const $ticketsByStatusesError = createStore(null);
