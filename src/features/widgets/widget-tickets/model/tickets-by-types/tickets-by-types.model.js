import { createEffect, createStore, combine } from 'effector';
import { getRandomColor } from '@tools/getRandomColors';

const colorsByTypes = [
  '#0394E3',
  '#7465EB',
  '#3B2EA7',
  '#AAAAAA',
  '#43B469',
  '#00A400',
  '#EF952B',
  '#EB5757',
  '#9C4040',
  '#52526C',
  '#80B300',
  '#809900',
  '#E6B3B3',
  '#6680B3',
  '#66991A',
  '#FF99E6',
  '#CCFF1A',
];

export const fxGetTicketsByTypes = createEffect();
export const fxGetTicketsCompetenceList = createEffect();

export const $ticketsByTypes = createStore([]);
export const $ticketsCompetence = createStore([]);
export const $ticketsLoading = createStore(false);
export const $ticketsError = createStore(null);

export const $tickets = combine(
  $ticketsByTypes,
  $ticketsCompetence,
  (tickets, competence) => {
    const competenceList = competence.reduce((acc, { section_id, section_title }) => {
      return {
        ...acc,
        [section_id]: {
          section_title,
        },
      };
    }, {});

    let index = 0;
    const result = tickets
      .map(({ count, section: { id } }) => {
        if (id in competenceList) {
          return {
            title: competenceList[id].section_title,
            name: competenceList[id].section_title,
            count,
            color: colorsByTypes[index++] || getRandomColor()[500],
          };
        }
        return null;
      })
      .filter(Boolean);

    return result;
  }
);
