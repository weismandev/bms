import { forward, sample } from 'effector';
import { pending } from 'patronum/pending';
import { getTicketsByTypes } from '../../api/tickets-by-types';
import { getTicketsCompetenceList } from '../../api/tickets-competence-list';
import { $currentTab, TicketsWidgetGate } from '../widget';
import {
  fxGetTicketsCompetenceList,
  fxGetTicketsByTypes,
  $ticketsLoading,
  $ticketsByTypes,
  $ticketsError,
  $ticketsCompetence,
} from './tickets-by-types.model';

fxGetTicketsCompetenceList.use(getTicketsCompetenceList);
fxGetTicketsByTypes.use(getTicketsByTypes);

$ticketsByTypes.on(fxGetTicketsByTypes.doneData, (_, tickets) => tickets);

$ticketsCompetence.on(fxGetTicketsCompetenceList.doneData, (_, { sections }) => sections);

$ticketsError.on(
  [fxGetTicketsByTypes.failData, fxGetTicketsCompetenceList.failData],
  (_, error) => {
    return error.toString().replace(/Error:/, '');
  }
);

$ticketsLoading.on(
  pending({ effects: [fxGetTicketsCompetenceList, fxGetTicketsByTypes] }),
  (_, pending) => pending
);

forward({
  from: TicketsWidgetGate.state,
  to: fxGetTicketsCompetenceList,
});

sample({
  source: TicketsWidgetGate.state,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetTicketsByTypes,
});
