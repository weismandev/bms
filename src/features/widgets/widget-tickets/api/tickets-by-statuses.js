import { api } from '@api/api2';

export const getTicketByStatuses = (building) => {
  return api.v1('get', 'dashboard/widgets/tickets/status-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
