import { api } from '@api/api2';

export const getTicketsCompetenceList = () => {
  return api.v1('get', 'tickets/sections/get-list');
};
