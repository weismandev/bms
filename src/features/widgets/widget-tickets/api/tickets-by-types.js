import { api } from '@api/api2';

export const getTicketsByTypes = (building) => {
  return api.v1('get', 'dashboard/widgets/tickets/types-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
