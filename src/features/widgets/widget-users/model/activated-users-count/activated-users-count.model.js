import { createEvent, createEffect, createStore } from 'effector';

export const retryRequest = createEvent();

export const fxGetActivatedUsersCount = createEffect();

export const $activatedUsersCount = createStore({ count: null, delta: null });
export const $activatedUsersCountLoading = createStore(false);
export const $activatedUsersCountError = createStore(null);
