import { forward, sample, guard } from 'effector';
import { getActivatedUsersCount } from '../../api/activated-users-count';
import { hasRequiredData } from '../../lib/has-required-data';
import { WidgetGate } from '../widget';
import {
  fxGetActivatedUsersCount,
  $activatedUsersCount,
  $activatedUsersCountError,
  $activatedUsersCountLoading,
  retryRequest,
} from './activated-users-count.model';

fxGetActivatedUsersCount.use(getActivatedUsersCount);

$activatedUsersCount.on(fxGetActivatedUsersCount.doneData, (_, data) => {
  return { count: data?.count, delta: data?.delta };
});

$activatedUsersCountError
  .on(fxGetActivatedUsersCount.failData, (_, data) => {
    return data.toString().replace(/Error:/, '');
  })
  .reset(retryRequest);

forward({
  from: fxGetActivatedUsersCount.pending,
  to: $activatedUsersCountLoading,
});

sample({
  source: $activatedUsersCount,
  clock: WidgetGate.open,
  fn: ({ count }) => !count,
  target: $activatedUsersCountLoading,
});

guard({
  source: sample({
    source: WidgetGate.state,
    clock: [retryRequest, WidgetGate.state],
    fn: (state) => ({ buildings: state?.buildings, period: state?.period }),
  }),
  filter: hasRequiredData,
  target: fxGetActivatedUsersCount,
});
