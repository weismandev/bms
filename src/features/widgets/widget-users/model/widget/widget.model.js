import { createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const options = [
  {
    label: t('ResidentApplication'),
    value: 'user_app',
  },
  {
    label: t('WebMC'),
    value: 'web_uk',
  },
  // {
  //   label: 'Приложение исполнителя',
  //   value: 'performer-app',
  // },
];

export const changeTab = createEvent();

export const $currentTab = createStore('user_app');

export const WidgetGate = createGate();
