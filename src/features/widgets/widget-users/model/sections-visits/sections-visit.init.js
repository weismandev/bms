import { sample, forward } from 'effector';
import { getSectionsVisits } from '../../api/sections-visits';
import { WidgetGate } from '../widget';
import {
  $sectionsVisits,
  $sectionsVisitsError,
  $sectionsVisitsLoading,
  fxGetSectionsVisits,
  sections,
} from './sections-visit.model';

fxGetSectionsVisits.use(getSectionsVisits);

$sectionsVisits.on(fxGetSectionsVisits.doneData, formattedSectionsVisits);

$sectionsVisitsError.on(fxGetSectionsVisits.failData, (_, data) => {
  return data.toString().replace(/Error:/, '');
});

forward({
  from: fxGetSectionsVisits.pending,
  to: $sectionsVisitsLoading,
});

sample({
  source: WidgetGate.state,
  clock: WidgetGate.state,
  fn: ({ period }) => ({ period, sections: Object.keys(sections) }),
  target: fxGetSectionsVisits,
});

function formattedSectionsVisits(_, data) {
  if (!data) return [];

  return data;
}
