import { combine, createEffect, createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { schemes } from '../../lib/schemes';

const { t } = i18n;

export const sections = {
  announcements: {
    title: t('Announcement'),
    style: schemes.first,
  },
  counters: {
    title: t('engineeringSystems.counters'),
    style: schemes.second,
  },
  messages: {
    title: t('informationCenter.messages'),
    style: schemes.third,
  },
  tickets: {
    title: t('dispatch.tickets'),
    style: schemes.fourth,
  },
};

export const fxGetSectionsVisits = createEffect();

export const $sectionsVisits = createStore(null);
export const $sectionsVisitsLoading = createStore(false);
export const $sectionsVisitsError = createStore(null);

export const $sectionCards = combine(
  $sectionsVisits,
  $sectionsVisitsLoading,
  $sectionsVisitsError,
  (sectionVisits, loading, error) => {
    if (!sectionVisits) return [];

    const cards = Object.keys(sectionVisits).map((section) => {
      const { title, style } = sections[section];

      return {
        settings: {
          name: section,
          title,
          value: sectionVisits[section],
          style,
        },
        meta: {
          loading,
          error,
          retry: () => {},
        },
      };
    });

    return cards;
  }
);
