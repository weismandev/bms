import { forward, guard, sample } from 'effector';
import { getApartmentsCount } from '../../api/apartments-count';
import { hasRequiredData } from '../../lib/has-required-data';
import { WidgetGate } from '../widget';
import {
  retryRequest,
  fxGetApartmentsCount,
  $apartmentsCount,
  $apartmentsCountLoading,
  $apartmentsCountError,
} from './apartments-count.model';

fxGetApartmentsCount.use(getApartmentsCount);

$apartmentsCount.on(fxGetApartmentsCount.doneData, (_, data) => ({
  count: data?.count,
}));

$apartmentsCountError
  .on(fxGetApartmentsCount.failData, (_, data) => {
    return data.toString().replace(/Error:/, '');
  })
  .reset(retryRequest);

forward({
  from: fxGetApartmentsCount.pending,
  to: $apartmentsCountLoading,
});

sample({
  source: $apartmentsCount,
  clock: WidgetGate.open,
  fn: ({ count }) => !count,
  target: $apartmentsCountLoading,
});

guard({
  source: sample({
    source: WidgetGate.state,
    clock: [retryRequest, WidgetGate.state],
    fn: (state) => ({ buildings: state?.buildings, period: state?.period }),
  }),
  filter: hasRequiredData,
  target: fxGetApartmentsCount,
});
