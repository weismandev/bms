import { createEvent, createEffect, createStore } from 'effector';

export const retryRequest = createEvent();

export const fxGetApartmentsCount = createEffect();

export const $apartmentsCount = createStore({ count: null });
export const $apartmentsCountLoading = createStore(false);
export const $apartmentsCountError = createStore(null);
