import { createEvent, createEffect, createStore } from 'effector';

export const retryRequest = createEvent();

export const fxGetActiveUsersCount = createEffect();

export const $activeUsersCount = createStore({ count: null, delta: null });
export const $activeUsersCountLoading = createStore(false);
export const $activeUsersCountError = createStore(null);
