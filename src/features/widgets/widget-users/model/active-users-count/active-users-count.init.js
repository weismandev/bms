import { forward, guard, sample } from 'effector';
import { getActiveUsersCount } from '../../api/active-users-count';
import { hasRequiredData } from '../../lib/has-required-data';
import { WidgetGate } from '../widget';
import {
  fxGetActiveUsersCount,
  $activeUsersCountError,
  $activeUsersCountLoading,
  $activeUsersCount,
  retryRequest,
} from './active-users-count.model';

fxGetActiveUsersCount.use(getActiveUsersCount);

$activeUsersCount.on(fxGetActiveUsersCount.doneData, (_, data) => {
  return { count: data?.count, delta: data?.delta };
});

$activeUsersCountError
  .on(fxGetActiveUsersCount.failData, (_, data) => {
    return data.toString().replace(/Error:/, '');
  })
  .reset(retryRequest);

forward({
  from: fxGetActiveUsersCount.pending,
  to: $activeUsersCountLoading,
});

sample({
  source: $activeUsersCount,
  clock: WidgetGate.open,
  fn: ({ count }) => !count,
  target: $activeUsersCountLoading,
});

guard({
  source: sample({
    source: WidgetGate.state,
    clock: [retryRequest, WidgetGate.state],
    fn: (state) => ({ buildings: state?.buildings, period: state?.period }),
  }),
  filter: hasRequiredData,
  target: fxGetActiveUsersCount,
});
