import { forward, guard, sample } from 'effector';
import { getSessionsCount } from '../../api/sessions-count';
import { hasRequiredData } from '../../lib/has-required-data';
import { WidgetGate } from '../widget';
import {
  fxGetVisitsCard,
  $visitsCardError,
  $visitsCardLoading,
  $visitsCard,
  retryRequest,
} from './visits-card.model';

fxGetVisitsCard.use(getSessionsCount);

$visitsCard.on(fxGetVisitsCard.doneData, (_, data) => {
  return { count: data?.count, delta: data?.delta };
});

$visitsCardError
  .on(fxGetVisitsCard.failData, (_, data) => {
    return data.toString().replace(/Error:/, '');
  })
  .reset(retryRequest);

forward({
  from: fxGetVisitsCard.pending,
  to: $visitsCardLoading,
});

sample({
  source: $visitsCard,
  clock: WidgetGate.open,
  fn: ({ count }) => !count,
  target: $visitsCardLoading,
});

guard({
  source: sample({
    source: WidgetGate.state,
    clock: [retryRequest, WidgetGate.state],
    fn: (state) => ({ buildings: state?.buildings, period: state?.period }),
  }),
  filter: hasRequiredData,
  target: fxGetVisitsCard,
});
