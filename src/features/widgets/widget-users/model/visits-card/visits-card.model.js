import { createEvent, createEffect, createStore } from 'effector';

export const retryRequest = createEvent();

export const fxGetVisitsCard = createEffect();

export const $visitsCard = createStore({ count: null, delta: null });
export const $visitsCardLoading = createStore(false);
export const $visitsCardError = createStore(null);
