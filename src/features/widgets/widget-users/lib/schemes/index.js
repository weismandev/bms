export const schemes = {
  first:
    'linear-gradient(151deg, rgba(3,184,227,1) 33%, rgba(3,148,227,1) 66%, rgba(3,109,219,1) 100%)',
  second:
    'linear-gradient(151deg, rgba(31,209,123,1) 30%, rgba(27,177,105,1) 66%, rgba(14,142,80,1) 100%)',
  third:
    'linear-gradient(151deg, rgba(149,138,237,1) 30%, rgba(116,101,235,1) 66%, rgba(85,74,175,1) 100%)',
  fourth:
    'linear-gradient(151deg, rgba(235,123,123,1) 30%, rgba(235,87,87,1) 66%, rgba(190,68,68,1) 100%)',
};
