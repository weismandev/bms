export const hasRequiredData = ({ period }) => {
  const hasDates = period?.start && period?.finish;

  return Boolean(hasDates);
};
