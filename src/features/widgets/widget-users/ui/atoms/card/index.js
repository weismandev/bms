import { CircularProgress } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const useStyles = makeStyles({
  card: {
    position: 'relative',
    padding: '12px 16px',
    borderRadius: '10px',
    color: '#fff',
    background: ({ style }) => style,
    height: '128px',
  },
  title: {
    fontSize: '14px',
    fontWeight: 700,
    marginBottom: '5px',
  },
  count: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  value: {
    fontWeight: 500,
    fontSize: '36px',
    lineHeight: '36px',
    marginRight: '8px',
  },
  delta: {
    fontWeight: 500,
    fontSize: '22px',
    opacity: '0.5',
  },
  chart: {
    position: 'absolute',
    bottom: 0,
    left: 16,
    marginRight: 16,
    transform: ({ chartRotate }) => (chartRotate ? 'scale(-1,1)' : ''),
    '& img': {
      width: '100%',
      height: 90,
    },
  },
  loader: {
    display: 'flex',
    margin: '30px auto 0 auto',
    color: '#fff',
  },
  error: {
    display: 'flex',
    height: '100px',
    flexDirection: 'column',
    '& > p': {
      margin: 'auto 0 0 auto',
      cursor: 'pointer',
      '&:hover': {
        opacity: '0.8',
      },
    },
    '& > span': {
      maxHeight: '64px',
      overflow: 'hidden',
    },
  },
});

const Title = ({ classes, title }) => <h2 className={classes.title}>{title}</h2>;

const Count = ({ classes, value = { count: 0, delta: 0 } }) => {
  const { count, delta } = value;

  if (!count && count !== 0) return null;

  return (
    <div className={classes.count}>
      <span className={classes.value}>{count}</span>
      {Boolean(delta) && <span className={classes.delta}>{delta}</span>}
    </div>
  );
};

const Loader = ({ classes }) => (
  <div className={classes.card}>
    <CircularProgress classes={{ root: classes.loader }} />
  </div>
);

const ErrorMessage = ({ classes, error, retryHandler }) => (
  <div className={classes.error}>
    <span>{error}</span>
    <p onClick={retryHandler}>{t('ToTryOneMoreTime')}</p>
  </div>
);

export const Card = ({ settings, meta }) => {
  const { title, value, style } = settings;
  const { loading, error, retry } = meta;

  const classes = useStyles({ style });

  if (loading) {
    return <Loader classes={classes} />;
  }

  return (
    <div className={classes.card}>
      {!error && <Title classes={classes} title={title} />}
      <Count classes={classes} value={value} />
      {error && <ErrorMessage classes={classes} error={error} retryHandler={retry} />}
    </div>
  );
};
