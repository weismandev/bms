import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { schemes } from '../../../lib/schemes';
import {
  $activatedUsersCount,
  $activatedUsersCountError,
  $activatedUsersCountLoading,
  retryRequest,
} from '../../../model/activated-users-count';
import { Card } from '../../atoms/card';

const { t } = i18n;

export const ActivatedUsersCard = () => {
  const activatedUsersCount = useStore($activatedUsersCount);
  const activatedUsersCountError = useStore($activatedUsersCountError);
  const activatedUsersCountLoading = useStore($activatedUsersCountLoading);

  const settings = {
    title: t('ActivatedUsers'),
    value: activatedUsersCount,
    style: schemes.third,
  };

  const meta = {
    loading: activatedUsersCountLoading,
    error: activatedUsersCountError,
    retry: () => retryRequest(),
  };

  return <Card settings={settings} meta={meta} />;
};
