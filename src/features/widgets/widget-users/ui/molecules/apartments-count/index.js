import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { schemes } from '../../../lib/schemes';
import {
  $apartmentsCount,
  $apartmentsCountLoading,
  $apartmentsCountError,
  retryRequest,
} from '../../../model/apartments-count';
import { Card } from '../../atoms/card';

const { t } = i18n;

export const ApartmentsCount = () => {
  const apartmentsCount = useStore($apartmentsCount);
  const apartmentsCountLoading = useStore($apartmentsCountLoading);
  const apartmentsCountError = useStore($apartmentsCountError);

  const settings = {
    title: t('NumberOfApartments'),
    value: apartmentsCount,
    style: schemes.first,
  };

  const meta = {
    loading: apartmentsCountLoading,
    error: apartmentsCountError,
    retry: () => retryRequest(),
  };

  return <Card settings={settings} meta={meta} />;
};
