import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { schemes } from '../../../lib/schemes';
import {
  $activeUsersCount,
  $activeUsersCountLoading,
  $activeUsersCountError,
  retryRequest,
} from '../../../model/active-users-count';
import { Card } from '../../atoms/card';

const { t } = i18n;

export const ActiveUsersCard = () => {
  const activeUserCount = useStore($activeUsersCount);
  const activeUserCountLoading = useStore($activeUsersCountLoading);
  const activeUserCountError = useStore($activeUsersCountError);

  const settings = {
    title: t('ActiveUsers'),
    value: activeUserCount,
    style: schemes.second,
  };

  const meta = {
    loading: activeUserCountLoading,
    error: activeUserCountError,
    retry: () => retryRequest(),
  };

  return <Card settings={settings} meta={meta} />;
};
