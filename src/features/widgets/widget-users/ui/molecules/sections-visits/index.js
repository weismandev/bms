import { useStore } from 'effector-react';
import { $sectionsVisits } from '../../../model/sections-visits';

export const SectionsVisits = () => {
  const sectionsVisits = useStore($sectionsVisits);

  return <div>SectionsVisits</div>;
};
