import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { schemes } from '../../../lib/schemes';
import {
  $visitsCard,
  $visitsCardError,
  $visitsCardLoading,
  retryRequest,
} from '../../../model/visits-card';
import { Card } from '../../atoms/card';

const { t } = i18n;

export const VisitsCard = () => {
  const visitsCard = useStore($visitsCard);
  const visitsCardError = useStore($visitsCardError);
  const visitsCardLoading = useStore($visitsCardLoading);

  const settings = {
    title: t('Visits'),
    value: visitsCard,
    style: schemes.fourth,
  };

  const meta = {
    loading: visitsCardLoading,
    error: visitsCardError,
    retry: () => retryRequest(),
  };

  return <Card settings={settings} meta={meta} />;
};
