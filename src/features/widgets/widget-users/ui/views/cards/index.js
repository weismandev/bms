import { ActivatedUsersCard } from '../../molecules/activated-users-card';
import { ActiveUsersCard } from '../../molecules/active-users-card';
import { ApartmentsCount } from '../../molecules/apartments-count';
import { VisitsCard } from '../../molecules/visits-card';

export const Cards = ({ classes }) => (
  <div className={classes.cards}>
    <ApartmentsCount />
    <ActiveUsersCard />
    <ActivatedUsersCard />
    <VisitsCard />
  </div>
);
