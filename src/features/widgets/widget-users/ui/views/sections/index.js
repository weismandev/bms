import { useStore } from 'effector-react';

import { $sectionCards } from '../../../model/sections-visits';

import { Card } from '../../atoms/card';
import { HaveSectionAccess } from '../../../../../common';

export const Sections = ({ classes }) => {
  const sectionCards = useStore($sectionCards);

  const cards = sectionCards.map(({ settings, meta }) => (
    <HaveSectionAccess>
      <Card settings={settings} meta={meta} />
    </HaveSectionAccess>
  ));

  return <div className={classes.cards}>{cards}</div>;
};
