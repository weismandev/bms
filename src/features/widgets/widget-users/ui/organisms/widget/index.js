import { useStore, useGate } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Widget } from '@ui/index';
import { $currentTab, changeTab, options, WidgetGate } from '../../../model/widget';
import { Cards } from '../../views/cards';
import { Sections } from '../../views/sections';

const useStyles = makeStyles({
  cards: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gap: '16px',
  },
});

const { t } = i18n;

export const WidgetUsers = ({ size = { height: '360px' }, data }) => {
  const classes = useStyles();
  const currentTab = useStore($currentTab);
  useGate(WidgetGate, data);

  const tabs = {
    active: true,
    options,
    currentTab,
    changeTab,
  };

  const settings = {
    title: t('Users'),
    tabs,
    size,
  };

  const currentUsersView = {
    user_app: <Cards classes={classes} />,
    web_uk: <Sections classes={classes} />,
  };

  return <Widget settings={settings}>{currentUsersView[currentTab]}</Widget>;
};
