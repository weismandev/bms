import { api } from '@api/api2';

export const getSessionsCount = (building) => {
  return api.v1('get', 'dashboard/widgets/users/sessions-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
