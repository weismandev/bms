import { api } from '@api/api2';

export const getApartmentsCount = (building) => {
  return api.v1('get', 'dashboard/widgets/users/apartments-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
