import { api } from '@api/api2';

export const getActiveUsersCount = (building) => {
  return api.v1('get', 'dashboard/widgets/users/active-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
