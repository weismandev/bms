import { api } from '@api/api2';

export const getActivatedUsersCount = (building) => {
  return api.v1('get', 'dashboard/widgets/users/activated-count', building, {
    headers: { 'Content-Type': 'application/json' },
  });
};
