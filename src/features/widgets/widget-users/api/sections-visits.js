import { api } from '@api/api2';

export const getSectionsVisits = (sections) => {
  return api.v1('get', 'dashboard/widgets/sections/visits', sections, {
    headers: { 'Content-Type': 'application/json' },
  });
};
