import { api } from '@api/api2';

const getTickets = ({ widgetType, ...payload }) =>
  api.v1('get', `tck/bms/widget/${widgetType}`, { ...payload });

const getObjects = (payload) => api.v1('post', 'tck/objects/detailed', payload);

const getTypesList = () => api.v1('get', 'tck/types/list');
const getUsersInfo = (payload) => api.v1('post', 'tck/bms/users/list', payload);
const getOriginList = () => api.v1('get', 'tck/origins/list');

export const ticketsApi = {
  getTickets,
  getObjects,
  getTypesList,
  getUsersInfo,
  getOriginList,
};
