import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import makeStyles from '@mui/styles/makeStyles';
import { Check, DeleteOutline } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { FilterSelect, FilterDatepicker, IconButton } from '@ui/';
import { HouseSelect } from '../../tools/house-select';
import { $data as $complexesData } from '@features/complex-select/model';
import { $data as $housesData } from '@features/house-select/models';
import { $types } from '@features/tickets-display/model';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  form: {
    display: 'grid',
    gridTemplateColumns: 'repeat(5, auto)',
    justifyItems: 'start',
    justifyContent: 'start',
    gap: 16,
    color: '#68687e',

    '@media (max-width: 1280px)': {
      gap: 5,
    },
  },
  form_buttons: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',

    '@media (max-width: 1280px)': {
      '& *': {
        margin: 0,
      },
    },
  },
}));

export const Filter = ({ filtersStore, filtersSubmitted }) => {
  const filters = useStore(filtersStore);
  const complexesData = useStore($complexesData);
  const housesData = useStore($housesData);
  const ticketsTypesData = useStore($types);
  const classes = useStyles();

  return (
    <Formik
      initialValues={filters}
      onSubmit={filtersSubmitted}
      onReset={() => filtersSubmitted('')}
      enableReinitialize
      render={({ values, setFieldValue, handleSubmit, handleReset }) => (
        <Form className={classes.form}>
          <Field
            name="created_at"
            component={FilterDatepicker}
            onChange={(value) => {
              setFieldValue('created_at', value);
            }}
          />

          <Field
            list={complexesData}
            name="object"
            component={FilterSelect}
            label={t('AnObject')}
            onChange={(value) => {
              setFieldValue('object', value);
              setFieldValue('house', '');
            }}
          />

          <Field
            list={housesData}
            complex={values.object ? values.object : null}
            name="house"
            component={HouseSelect}
            label={t('Label.address')}
            onChange={(value) => {
              setFieldValue('house', value);
            }}
          />

          <Field
            list={ticketsTypesData}
            label={t('type')}
            component={FilterSelect}
            name="type"
            onChange={(value) => {
              setFieldValue('type', value);
            }}
          />

          <div className={classes.form_buttons}>
            <IconButton title={t('Apply')} onClick={handleSubmit} size="large">
              <Check />
            </IconButton>

            <IconButton title={t('Reset')} onClick={handleReset} size="large">
              <DeleteOutline />
            </IconButton>
          </div>
        </Form>
      )}
    />
  );
};
