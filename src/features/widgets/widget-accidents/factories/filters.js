import { createFilterBag } from '@tools/factories';

function createFiltersBag(defaultFilters) {
  const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
    createFilterBag(defaultFilters);

  return {
    changedFilterVisibility,
    filtersSubmitted,
    $isFilterOpen,
    $filters,
  };
}

export { createFiltersBag };
