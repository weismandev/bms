import { createStore } from 'effector';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { format } from 'date-fns';

import i18n from '@shared/config/i18n';
import { getTypesList } from '@features/tickets/libs';

const { t } = i18n;

function createTableBag(fetchingDone) {
  const $tableData = createStore([]);

  $tableData.on(fetchingDone, processWidgetData);

  return { $tableData };
}

function transformStatus(value) {
  switch (value) {
    case 'new':
      return t('New');
    case 'done':
      return t('Сompleted');
    case 'work':
      return t('InWork');
    case 'confirmation':
      return t('OnConfirmation');
    case 'closed':
      return t('closed');
    case 'assigned':
      return t('Appointed');
    case 'canceled':
      return t('Canceled');
    case 'paused':
      return t('OnPause');
    case 'denied':
      return t('rejected');
    case 'returned':
      return t('Returned');
    default:
      return null;
  }
}

function calcSLA(value) {
  if (new Date(value).getTime() < new Date().getTime()) return true;
  else return null;
}

function processWidgetData(_, widgetData) {
  const tickets = widgetData.tickets.map((ticket) => {
    const typesList = ticket.types[0]
      ? getTypesList(ticket.types[0].id, widgetData.ticketsTypes)
      : [];

    const data = {
      ...ticket,

      assignees: ticket.assignees.map((user) => {
        const userData = widgetData.assignees.find((item) => item.id == user.id);
        if (userData != undefined) return userData;
        else return null;
      }),

      objects: ticket.objects.map((object) => {
        const objectData = widgetData.objects.find((item) => item.object.id == object.id);
        if (objectData) return objectData;
        else return null;
      }),

      types: ticket.types.length
        ? ticket.types.map((type) => {
            const typeData = widgetData.ticketsTypes.find((item) => item.id == type.id);
            if (typeData == undefined) return { title: '-' };
            else return typeData;
          })
        : [{ title: '-' }],
    };

    if (typesList[0]) {
      const typesCount = typesList.length - 1;

      const types = typesList.map((item, index) => {
        return index !== typesCount ? `${item.title} -> ` : item.title;
      });

      const formattedTypes = types.join('');

      data.formattedTypes = formattedTypes;
    }

    return data;
  });

  return processTickets(sortTickets(tickets));
}

function sortTickets(tickets) {
  return tickets.sort((a, b) => {
    return Date.parse(b.created_at) - Date.parse(a.created_at);
  });
}

function processTicketsDate(date) {
  const expectedDateRegexp = RegExp(/\d{4}\-\d{2}\-\d{2}\s\d{2}\:\d{2}\:\d{2}/);

  if (!expectedDateRegexp.test(date)) {
    return { localDate: '-', localTime: '-' };
  } else {
    const convertedDateObject = new Date(convertUTCDateToLocalDate(new Date(date)));

    const [localDate, localTime] = format(convertedDateObject, 'dd.MM.yyyy HH:mm').split(
      ' '
    );

    return { localDate, localTime };
  }
}

function processTickets(tickets) {
  if (!tickets.length) return [];

  return tickets.map((item) => {
    const objectData = {};

    try {
      objectData.object = item.objects[0].parts.find(
        (item) => item.type == 'complex'
      ).title;
      objectData.building = item.objects[0].parts.find(
        (item) => item.type == 'building'
      ).title;
    } catch {
      objectData.object = '';
      objectData.building = '';
    }

    return {
      id: item.id,
      time: processTicketsDate(item.created_at).localTime,
      created_at: processTicketsDate(item.created_at).localDate,
      planned_start_at: processTicketsDate(item.planned_start_at).localDate,
      planned_end_at: processTicketsDate(item.planned_end_at).localDate,
      object: objectData.object,
      building: objectData.building,
      type: item.types?.map((type) => type.title).join(', '),
      assignees: item.assignees
        .map((user) => {
          if (user == null) return '';
          return `${user.name ? user.name + ' ' : ''}${user.surname ? user.surname : ''}`;
        })
        .join(', '),
      status: item.status
        ? {
            type: item.status,
            title: transformStatus(item.status),
          }
        : '',
      sla: item.planned_end_at ? calcSLA(item.planned_end_at) : '',
      class: item.class,
      formattedTypes: item.formattedTypes || '',
    };
  });
}

export { createTableBag };
