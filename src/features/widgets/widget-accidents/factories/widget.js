import {
  createStore,
  createEffect,
  createEvent,
  combine,
  sample,
  merge,
  forward,
  attach,
  guard,
} from 'effector';
import { createGate } from 'effector-react';
import { interval } from 'patronum';
import { pending } from 'patronum/pending';
import { ticketsApi } from '../api';
import { formatPayload, removeDuplicates } from '../lib';
import { signout } from '@features/common';
import { $types, fxGetTypes } from '@features/tickets-display/model';

function createWidgetBag($filters) {
  const WidgetGate = createGate();
  const { open: mount, close: unmount } = WidgetGate;

  const fxGetTickets = createEffect();
  const fxGetAssignees = createEffect();
  const fxGetObjects = createEffect();

  const getAssignees = createEvent();
  const getObjects = createEvent();
  const fetchingDone = createEvent();

  const $gateData = createStore(null);
  const $rawData = createStore([]);
  const $assignees = createStore([]);
  const $objects = createStore([]);
  const $isLoading = createStore(false);
  const $error = createStore(null);
  const $refreshRate = createStore(60000);
  const $widgetData = combine(
    $rawData,
    $assignees,
    $objects,
    $types,
    (rawData, assignees, objects, ticketsTypes) => ({
      tickets: rawData,
      assignees,
      objects,
      ticketsTypes,
    })
  );

  const startInterval = createEvent();
  const stopInterval = createEvent();
  const { tick, isRunning } = interval({
    timeout: $refreshRate,
    start: startInterval,
    stop: stopInterval,
  });

  fxGetTickets.use(ticketsApi.getTickets);
  fxGetAssignees.use(ticketsApi.getUsersInfo);
  fxGetObjects.use(ticketsApi.getObjects);

  const requestOccured = pending({
    effects: [fxGetTickets, fxGetAssignees, fxGetObjects, fxGetTypes],
  });

  const errorOccured = merge([
    fxGetTickets.fail,
    fxGetAssignees.fail,
    fxGetObjects.fail,
    fxGetTypes.fail,
  ]);

  $error
    .on(errorOccured, (_, { error }) => {
      return error.toString().replace(/Error:/, '');
    })
    .reset(signout);

  $isLoading.on(requestOccured, (_, pending) => pending).reset(signout);

  // После получения тикетов запрашиваем объекты,
  // очищаем список если менялись фильтры или началось обновление
  $rawData.on(fxGetTickets.doneData, (_, { tickets }) => tickets).reset(signout);

  // Записываем список исполнителей
  $assignees
    .on(fxGetAssignees.doneData, (state, { users }) => {
      if (!Array.isArray(users)) {
        return '';
      }
      return [...state, ...users.map((item) => item)];
    })
    .reset($filters, signout);

  // Записываем список объектов
  $objects.on(fxGetObjects.doneData, (state, data) => data).reset(signout);

  // При маунте запрашиваем списки для фильтров и запускаем интервал
  forward({
    from: mount,
    to: [startInterval, $gateData],
  });

  // Запрашиваем тикеты при тике и маунте
  sample({
    source: [$gateData, $filters],
    clock: [mount, tick],
    fn: (source, clock) => ({ gateData: source[0], filters: source[1] }),
    target: fxGetTickets.prepend((data) => formatPayload(data)),
  });
  // Запрашиваем тикеты при изменении фильтров
  sample({
    source: [$gateData, $filters],
    clock: $filters,
    fn: (source, clock) => ({ gateData: source[0], filters: source[1] }),
    target: fxGetTickets.prepend((data) => formatPayload(data)),
  });

  // После получения тикетов запрашиваем объекты
  forward({
    from: fxGetTickets.done,
    to: getObjects.prepend(({ result: { tickets } }) =>
      tickets.map((item) => item.objects).flat()
    ),
  });
  guard({
    clock: getObjects,
    filter: (objects) => objects.length > 0,
    target: fxGetObjects.prepend((objects) => {
      const payload = objects.map((item) => ({
        id: +item.id,
        type: item.type,
      }));

      return {
        objects: removeDuplicates(payload),
      };
    }),
  });

  // После получения тикетов запрашиваем исполнителей
  forward({
    from: fxGetTickets.done,
    to: getAssignees.prepend(({ result: { tickets } }) =>
      tickets.map((item) => item.assignees).flat()
    ),
  });
  guard({
    clock: getAssignees,
    filter: (assignees) => assignees.length > 0,
    target: fxGetAssignees.prepend((assignees) => {
      const ids = assignees.map((item) => item.id);

      return { users: ids };
    }),
  });

  // Вызываем финальный ивент, сигнализирующий об окончании цепочки зарпосов
  // и пробрасываем все данные туда, чтобы использовать в другом месте
  sample({
    source: $widgetData,
    clock: $isLoading,
    target: fetchingDone,
  });

  // Очищаем интервал после анмаунта
  forward({
    from: unmount,
    to: stopInterval,
  });

  return {
    WidgetGate,
    $rawData,
    $widgetData,
    $assignees,
    $objects,
    $isLoading,
    $error,
    $refreshRate,
    fxGetTickets,
    fxGetAssignees,
    fxGetObjects,
    getAssignees,
    getObjects,
    fetchingDone,
  };
}

export { createWidgetBag };
