import { api } from '@api/api2';

export const getDevices = () => {
  return api.no_vers('get', 'bmsdevice/get-devices', {
    limit: 25,
    offset: 0,
    tag: 'door',
    signal_tag: 'door_state',
    compare_signal_tag: 'door_state',
    compare_type: 'qual',
    compare_value: 1,
  });
};
