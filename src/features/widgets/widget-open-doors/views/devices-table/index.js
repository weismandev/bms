import { memo } from 'react';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';

import { Empty } from '@ui/';
import { DataGrid } from '@features/data-grid';

import { $tableData, $tableDataError, columns } from '../../models';

const { t } = i18n;

const useStyles = makeStyles({
  handlerWrap: {
    height: '50px',
  },
  tableContent: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
  content: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
});

const DevicesTable = memo(() => {
  const data = useStore($tableData);
  const error = useStore($tableDataError);
  const classes = useStyles();

  if (!data?.length)
    return (
      <div className={classes.content}>
        <Empty title={t('AllTechnologicalDoorsAreClosed')} />
      </div>
    );
  if (error)
    return (
      <div className={classes.content}>
        <p>{error}</p>
      </div>
    );

  return (
    <div className={classes.tableContent}>
      <DataGrid.Mini
        rows={data}
        columns={columns}
        pageSize={10}
        disableSelectionOnClick
        disableColumnMenu
        border={false}
        hideFooter
        density="compact"
      />
    </div>
  );
});

export { DevicesTable };
