import { Link } from 'react-router-dom';
import { useGate, useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { Widget } from '@ui/';
import { OpenDoorsWidgetGate, $tableDataLoading, intervalValue } from '../models';
import { DevicesCounter } from '../views/devices-counter';
import { DevicesTable } from '../views/devices-table';

const { t } = i18n;

export const WidgetOpenDoors = ({ size = {}, data }) => {
  useGate(OpenDoorsWidgetGate, data);
  const isLoading = useStore($tableDataLoading);

  const settings = {
    title: t('OpenDoors'),
    size,
  };

  return (
    <Widget
      settings={settings}
      renderTool={() => (
        <Link
          to={{
            pathname: '/doors',
            state: {
              filter: {
                state: 1,
              },
            },
          }}
        >
          <DevicesCounter />
        </Link>
      )}
      refreshRate={intervalValue}
      isLoading={isLoading}
    >
      <DevicesTable />
    </Widget>
  );
};
