import { sample, forward } from 'effector';
import { getDevices } from '../api';
import {
  OpenDoorsWidgetGate,
  unmount,
  mount,
  $tableData,
  $tableDataError,
  $tableDataLoading,
  $devicesTotal,
  fxGetDevices,
  tick,
  startInterval,
  stopInterval,
} from './widget.model';

fxGetDevices.use(getDevices);

$tableData.on(fxGetDevices.doneData, preprocessDevices);
$devicesTotal.on(fxGetDevices.doneData, (_, data) => data.meta.total);

$tableDataError
  .on(fxGetDevices.failData, (_, error) => {
    return error.toString().replace(/Error:/, '');
  })
  .reset($tableData);

$tableDataLoading.on(fxGetDevices.pending, (_, pending) => pending);

sample({
  source: OpenDoorsWidgetGate.state,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetDevices,
});

sample({
  clock: mount,
  target: startInterval,
});

sample({
  source: OpenDoorsWidgetGate.state,
  clock: tick,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetDevices,
});

forward({
  from: unmount,
  to: stopInterval,
});

function preprocessDevices(_, { devices = [] }) {
  if (!devices.length) return [];

  return devices.map((item) => ({
    id: item.serialnumber,
    city: item.complex.region_title,
    object: item.complex.title,
    address: item.apartment.title,
    name: item.name,
    date: '-',
  }));
}
