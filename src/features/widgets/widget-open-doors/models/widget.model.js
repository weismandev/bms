import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { interval } from 'patronum';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const OpenDoorsWidgetGate = createGate();
export const { open: mount, close: unmount } = OpenDoorsWidgetGate;

export const startInterval = createEvent();
export const stopInterval = createEvent();
export const intervalValue = 15000;
export const { tick, isRunning } = interval({
  timeout: intervalValue,
  start: startInterval,
  stop: stopInterval,
});

export const fxGetDevices = createEffect();

export const $tableData = createStore([]);
export const $tableDataLoading = createStore(false);
export const $tableDataError = createStore(null);
export const $devicesTotal = createStore(0);

export const columns = [
  {
    field: 'city',
    headerName: t('Label.city'),
    width: 200,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'object',
    headerName: t('AnObject'),
    width: 200,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'address',
    headerName: t('AddressOfTheObject'),
    width: 300,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'name',
    headerName: t('NameTitle'),
    width: 170,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'date',
    headerName: t('DateAndTimeOfOpening'),
    type: 'number',
    width: 250,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
];
