import { memo } from 'react';
import { useStore } from 'effector-react';
import { Chip, Tooltip } from '@mui/material';
import { ErrorOutlineOutlined } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { useWindowSize } from '@tools/useWindowSize';
import i18n from '@shared/config/i18n';
import { CustomScrollbar, Empty } from '@ui/';
import { DataGrid } from '@features/data-grid';
import Sla from '@img/sla.svg';

const { t } = i18n;

const useStyles = makeStyles({
  helpContent: {
    height: 290,
  },
  tableRoot: {
    cursor: 'pointer',

    '& .row--red': {
      backgroundColor: 'rgba(235, 87, 87, 0.1)',
    },
  },

  handlerWrap: {
    height: '50px',
  },
  tableContent: {
    width: '1300px',
    height: '400px',
  },
  content: {
    width: '100%',
    height: '400px',
  },
  chip__primary: {
    color: 'white',
    backgroundColor: '#0394E3',
  },
  chip__orange: {
    color: 'white',
    backgroundColor: '#EC7F00',
  },
  chip__green: {
    color: 'white',
    backgroundColor: '#1BB169',
  },
  chip__grey: {
    color: 'white',
    backgroundColor: '#AAAAAA',
  },
});

export const Table = memo(({ tableDataStore, errorStore }) => {
  const windowSize = useWindowSize();
  const data = useStore(tableDataStore);
  const error = useStore(errorStore);
  const classes = useStyles();

  const handleRowSelection = (e) => {
    if (e && e[0]) {
      window.open(`/tickets/${e[0]}`, '_blank');
    }
  };

  if (!data?.length)
    return (
      <div className={classes.helpContent}>
        <Empty title={t('DelayOnRequests')} />
      </div>
    );
  if (error)
    return (
      <div className={classes.helpContent}>
        <p>{error}</p>
      </div>
    );

  const getBadgeClass = (type) => {
    switch (type) {
      case 'new':
      case 'work':
        return classes.chip__orange;

      case 'done':
      case 'assigned':
        return classes.chip__green;

      case 'confirmation':
      case 'closed':
      case 'canceled':
      case 'paused':
      case 'denied':
      case 'returned':
        return classes.chip__grey;
      default:
        return null;
    }
  };

  const columns = [
    {
      field: 'time',
      headerName: t('Label.time'),
      width: 90,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'created_at',
      headerName: t('Label.date'),
      width: 100,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'planned_start_at',
      headerName: t('Start'),
      width: 100,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'planned_end_at',
      headerName: t('End'),
      width: 100,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'object',
      headerName: t('AnObject'),
      width: 120,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'building',
      headerName: t('building'),
      width: 180,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'type',
      headerName: t('type'),
      width: 200,
      headerClassName: 'super-app-theme--header',
      renderCell: (cell) => {
        return (
          <>
            {cell.row.formattedTypes.length ? (
              <Tooltip title={cell.row.formattedTypes}>
                <ErrorOutlineOutlined
                  style={{
                    marginRight: 5,
                    color: 'rgb(118, 118, 118)',
                  }}
                />
              </Tooltip>
            ) : null}
            <span>{cell.value}</span>
          </>
        );
      },
    },
    {
      field: 'assignees',
      headerName: t('Performer'),
      width: 150,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'status',
      renderCell: (cell) => {
        return (
          <Chip
            size="small"
            label={cell.value.title}
            classes={{ root: getBadgeClass(cell.value.type) }}
          />
        );
      },
      headerName: t('RequestStatus'),
      width: 150,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'sla',
      renderCell: (cell) => {
        if (cell.value) return <Sla />;
      },
      headerName: 'SLA',
      width: 100,
      headerClassName: 'super-app-theme--header',
    },
  ];

  const renderRow = ({ row = {} }) => row?.assignees?.length ? '' : 'row--red';

  return (
    <div
      style={{
        width: '100%',
        height: `calc(50vh - ${windowSize.width <= 1180 ? '183px' : '150px'})`,
      }}
    >
      <DataGrid.Mini
        classes={{ root: classes.tableRoot }}
        rows={data}
        columns={columns}
        onSelectionModelChange={handleRowSelection}
        disableColumnMenu
        border={false}
        hideFooter
        density="compact"
        getRowClassName={renderRow}
      />
    </div>
  );
});
