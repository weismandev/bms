import { createWidgetBag } from '@features/widgets/widget-accidents/factories/widget';
import { $filters } from './filter.model';

export const {
  WidgetGate,
  $rawData,
  $widgetData,
  $assignees,
  $objects,
  $objectsProperty,
  $isLoading,
  $error,
  $refreshRate,
  fxGetTickets,
  fxGetAssignees,
  fxGetObjects,
  fxGetObjectsByProperty,
  getAssignees,
  getObjects,
  getObjectsByProperty,
  fetchingDone,
} = createWidgetBag($filters);
