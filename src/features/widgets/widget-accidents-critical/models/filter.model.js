import { createFiltersBag } from '@features/widgets/widget-accidents/factories/filters';

const defaultFilters = {
  created_at: '',
  object: '',
  house: '',
  type: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFiltersBag(defaultFilters);
