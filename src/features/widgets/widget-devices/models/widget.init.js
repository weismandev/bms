import { forward, sample } from 'effector';
import { getDevices } from '../api';
import {
  DevicesWidgetGate,
  unmount,
  mount,
  tick,
  startInterval,
  stopInterval,
  $tableData,
  $tableDataError,
  $tableDataLoading,
  $devicesTotal,
  fxGetDevices,
} from './widget.model';

fxGetDevices.use(getDevices);

$tableData.on(fxGetDevices.doneData, preprocessDevices);
$devicesTotal.on(fxGetDevices.doneData, (_, data) => data.meta.total);

$tableDataError
  .on(fxGetDevices.failData, (_, error) => {
    return error.toString().replace(/Error:/, '');
  })
  .reset($tableData);

$tableDataLoading.on(fxGetDevices.pending, (_, pending) => pending);

sample({
  source: DevicesWidgetGate.state,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetDevices,
});

sample({
  clock: mount,
  target: startInterval,
});

sample({
  source: DevicesWidgetGate.state,
  clock: tick,
  fn: (data) => ({
    buildings: data?.buildings,
  }),
  target: fxGetDevices,
});

forward({
  from: unmount,
  to: stopInterval,
});

function preprocessDevices(_, { devices }) {
  if (!devices.length) return [];

  const data = devices.map((item) => ({
    id: item.serialnumber,
    type: item.point.name,
    name: item.name,
    last_activity: item.last_activity
      ? new Date(item.last_activity * 1000).toLocaleString()
      : '',
    address: item.apartment.title,
    object: item.complex.title,
    city: item.complex.region_title,
  }));

  // Сортируем по дате последней активности, более поздние сверху
  return data.sort((a, b) => {
    if (a.last_activity > b.last_activity) return -1;
    if (a.last_activity < b.last_activity) return 1;
    return 0;
  });
}
