import { createStore, createEffect, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { interval } from 'patronum';
import i18n from '@shared/config/i18n';

export const DevicesWidgetGate = createGate();
export const { open: mount, close: unmount } = DevicesWidgetGate;

export const startInterval = createEvent();
export const stopInterval = createEvent();
export const intervalValue = 15000;
export const { tick, isRunning } = interval({
  timeout: intervalValue,
  start: startInterval,
  stop: stopInterval,
});

export const fxGetDevices = createEffect();

export const $tableData = createStore([]);
export const $tableDataLoading = createStore(false);
export const $tableDataError = createStore(null);
export const $devicesTotal = createStore(0);

const { t } = i18n;

export const columns = [
  {
    field: 'type',
    headerName: t('TypeOfEquipment'),
    width: 180,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'name',
    headerName: t('NameTitle'),
    width: 170,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'last_activity',
    headerName: t('DateAndTimeOfConnectionLoss'),
    width: 250,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'address',
    headerName: t('AddressOfTheObject'),
    type: 'number',
    width: 300,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'object',
    headerName: t('AnObject'),
    type: 'number',
    width: 200,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'city',
    headerName: t('Label.city'),
    type: 'number',
    width: 200,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
];
