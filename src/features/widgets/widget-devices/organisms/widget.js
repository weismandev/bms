import { Link } from 'react-router-dom';
import { useGate, useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { Widget } from '@ui/';
import { DevicesWidgetGate, $tableDataLoading, intervalValue } from '../models';
import { DevicesCounter } from '../views/devices-counter';
import { DevicesTable } from '../views/devices-table';

const { t } = i18n;

export const WidgetDevices = ({ size = {}, data }) => {
  useGate(DevicesWidgetGate, data);

  const isLoading = useStore($tableDataLoading);
  const settings = { title: t('DevicesOffline'), size };

  return (
    <Widget
      settings={settings}
      renderTool={() => (
        <Link
          to={{
            pathname: '/equipment',
            state: {
              filter: {
                state: 0,
              },
            },
          }}
        >
          <DevicesCounter />
        </Link>
      )}
      refreshRate={intervalValue}
      isLoading={isLoading}
    >
      <DevicesTable />
    </Widget>
  );
};
