import { api } from '@api/api2';

export const getDevices = () => {
  return api.no_vers('get', 'bmsdevice/get-devices', { state: 0, limit: 10 });
};
