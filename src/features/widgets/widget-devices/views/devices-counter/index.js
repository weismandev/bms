import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { $devicesTotal } from '../../models';

const { t } = i18n;

const useStyles = makeStyles({
  toolContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    gap: '10px',
    alignItems: 'center',
    alignContent: 'center',
  },
  toolTitle: {
    margin: 0,
    color: 'rgb(147, 147, 147)',
    fontSize: 16,
    fontWeight: 400,
    whiteSpace: 'nowrap',
  },
  toolCount: {
    background: '#0394E3',
    color: 'white',
    borderRadius: 14,
    padding: '4px 10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
  },
});

export const DevicesCounter = () => {
  const classes = useStyles();
  const devicesTotal = useStore($devicesTotal);

  return (
    <div className={classes.toolContainer}>
      <Typography classes={{ root: classes.toolTitle }}>
        {t('TotalDevicesOffline')}
      </Typography>
      <div className={classes.toolCount}>{devicesTotal ? devicesTotal : 0}</div>
    </div>
  );
};
