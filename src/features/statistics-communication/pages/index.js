import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui';
import { HaveSectionAccess } from '@features/common';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isFilterOpen,
} from '../models';
import { StatisticsCommunicationTable, Filter } from '../organisms';

const StatisticCommunicationPage = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<StatisticsCommunicationTable />}
        detail={null}
        params={{ filterWidth: '400px' }}
      />
    </>
  );
};

const RestrictedStatisticCommunicationPage = (props) => (
  <HaveSectionAccess>
    <StatisticCommunicationPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedStatisticCommunicationPage as StatisticCommunicationPage };
