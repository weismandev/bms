import { api } from '@api/api2';

const getStatisticsList = (payload) =>
  api.v1('get', 'dashboard/stat/communications', payload);

const exportStatistics = (payload = {}) =>
  api.v1('request', 'dashboard/stat/communications', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });

export const statisticsApi = {
  getStatisticsList,
  exportStatistics,
};
