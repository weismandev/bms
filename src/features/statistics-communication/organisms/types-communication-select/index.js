import { SelectControl, SelectField } from '@ui';
import { data } from '../../models';

const TypesCommunicationSelect = (props) => <SelectControl options={data} {...props} />;

const TypesCommunicationSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<TypesCommunicationSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { TypesCommunicationSelect, TypesCommunicationSelectField };
