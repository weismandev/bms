import { useMemo, memo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  PagingPanel,
  TableColumnResizing,
  ColumnChooser,
  TableColumnVisibility,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
} from '../../../../ui';
import {
  $tableData,
  $rowCount,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columns,
  $columnWidths,
  columnWidthsChanged,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $columnOrder,
  columnOrderChanged,
} from '../../models';
import { StatisticsCommunicationToolbar } from '../table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const row = useMemo(() => <TableRow {...props} />, [props.children]);
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StatisticsCommunicationTable = memo(() => {
  const data = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <DragDropProvider />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          // pageSize={pageSize}
          pageSize={10} //!!!!! ЧТОБЫ БЭК НЕ ПАДАЛ, КОЛИЧЕСТВО ЗАПИСЕЙ ОГРАНИЧЕНО 10!!!!!!
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <StatisticsCommunicationToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel
          // pageSizes={[10, 25, 50, 100]}
          pageSizes={[10]} //!!!!! ЧТОБЫ БЭК НЕ ПАДАЛ, КОЛИЧЕСТВО ЗАПИСЕЙ ОГРАНИЧЕНО 10!!!!!!
          messages={{ rowsPerPage: t('EntriesPerPage'), info: '' }}
        />
      </Grid>
    </Wrapper>
  );
});

export { StatisticsCommunicationTable };
