import { memo } from 'react';
import { useStore } from 'effector-react';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
  Divider,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { TypesCommunicationSelectField } from '../types-communication-select';

import * as Yup from 'yup';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  date_start: Yup.string()
    .test('start_date-after-end_date', t('StartDateBeforeEndDate'), startDateAfterEndDate)
    .nullable(),
  date_end: Yup.string()
    .test('start_date-after-end_date', t('EndDateAfterStartDate'), startDateAfterEndDate)
    .nullable(),
});

function startDateAfterEndDate() {
  const isDatesDefined = Boolean(this.parent.date_start) && Boolean(this.parent.date_end);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.date_start)) >= Number(new Date(this.parent.date_end))
  ) {
    return this.createError();
  }

  return true;
}

const todayDate = `${new Date().getFullYear()}-${String(
  new Date().getMonth() + 1
).padStart(2, '0')}-${String(new Date().getDate()).padStart(2, '0')}`;

const Filter = memo(() => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          validationSchema={validationSchema}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <div>
                  <p
                    style={{
                      color: '#9494A3',
                      fontSize: 14,
                      marginBottom: 12,
                    }}
                  >
                    {t('Label.date')}
                  </p>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Field
                      name="date_start"
                      label={null}
                      divider={false}
                      type="date"
                      component={InputField}
                      inputProps={{ max: todayDate }}
                    />
                    <Field
                      name="date_end"
                      label={null}
                      divider={false}
                      type="date"
                      component={InputField}
                      inputProps={{ max: todayDate }}
                    />
                  </div>
                  <Divider isVisible />
                </div>
                <Field
                  name="complexes"
                  component={ComplexSelectField}
                  label={t('RC')}
                  placeholder={t('selectRC')}
                  isMulti
                  onChange={(value) => {
                    setFieldValue('complexes', value);
                    setFieldValue('buildings', '');
                  }}
                />
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  label={t('AddressOfTheObject')}
                  placeholder={t('selectObject')}
                  isMulti
                  complex={values.complexes}
                  isDisabled={values.complexes.length <= 0}
                />
                <Field
                  name="types"
                  component={TypesCommunicationSelectField}
                  label={t('TypeOfCommunication')}
                  placeholder={t('chooseType')}
                  isMulti
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
