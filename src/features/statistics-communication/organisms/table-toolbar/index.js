import { useStore } from 'effector-react';
import { Toolbar, FoundInfo, Greedy, FilterButton, ExportButton } from '@ui';
import {
  $rowCount,
  columns,
  $tableData,
  $hiddenColumnNames,
  changedFilterVisibility,
  $isFilterOpen,
  exportClicked,
} from '../../models';

const StatisticsCommunicationToolbar = () => {
  const count = useStore($rowCount);
  const tableData = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <Toolbar>
      <FilterButton
        style={{ marginRight: 10 }}
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />

      <FoundInfo count={count} style={{ marginRight: 10 }} />
      <Greedy />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        style={{ marginRight: 10 }}
        onClick={exportClicked}
      />
    </Toolbar>
  );
};

export { StatisticsCommunicationToolbar };
