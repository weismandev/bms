import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const exportLimit = 10000;

const changedErrorDialogVisibility = createEvent();
const exportClicked = createEvent();

const fxGetStatisticsList = createEffect();
const fxExportStatistics = createEffect();

const $rawStatisticsData = createStore({});
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);

export {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  $rawStatisticsData,
  PageGate,
  changedErrorDialogVisibility,
  fxGetStatisticsList,
  pageMounted,
  pageUnmounted,
  exportClicked,
  exportLimit,
  fxExportStatistics,
};
