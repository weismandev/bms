import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  { label: t('News'), value: 'news' },
  { label: t('Announcement'), value: 'announcement' },
  { label: t('Poll'), value: 'poll' },
];
