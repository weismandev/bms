import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  date_start: null,
  date_end: null,
  complexes: [],
  buildings: [],
  types: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
