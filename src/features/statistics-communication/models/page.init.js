import { forward, merge, attach, sample } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { statisticsApi } from '../api';

import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  $rawStatisticsData,
  changedErrorDialogVisibility,
  fxGetStatisticsList,
  pageMounted,
  pageUnmounted,
  exportClicked,
  exportLimit,
  fxExportStatistics,
} from './page.model';

import { $tableParams, pageNumChanged } from './table.model';
import { $filters, filtersSubmitted } from './filter.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from './user-settings.model';

fxGetStatisticsList.use(statisticsApi.getStatisticsList);
fxExportStatistics.use(statisticsApi.exportStatistics);

const requestOccured = [fxGetStatisticsList.pending, fxExportStatistics.pending];

const errorOccured = merge([fxGetStatisticsList.failData, fxExportStatistics.failData]);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading.on(requestOccured, (_, pending) => pending).reset([pageUnmounted, signout]);

$error
  .on(errorOccured, (_, { error }) => error)
  .on(requestOccured, () => null)
  .reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .on(requestOccured, () => false)
  .reset([pageUnmounted, signout]);

$rawStatisticsData
  .on(fxGetStatisticsList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

const formatPayload = ({ filters, page, per_page, isExport }) => {
  const buildingsId =
    filters.buildings.length > 0 ? filters.buildings.map((building) => building.id) : [];

  const complexesId =
    filters.complexes.length > 0 ? filters.complexes.map((complex) => complex.id) : [];

  const type = filters.types.map((type) => type.value);

  return {
    page,
    // per_page,
    per_page: 10, //!!!!! ЧТОБЫ БЭК НЕ ПАДАЛ, КОЛИЧЕСТВО ЗАПИСЕЙ ОГРАНИЧЕНО 10!!!!!!
    date_start: filters.date_start,
    date_end: filters.date_end,
    buildings: buildingsId,
    complex_id: complexesId,
    type,
    export: isExport,
  };
};

/** После применения фильтров скидываю на первую страницу, т.к. не знаю сколько страницу нафильтруется */
sample({
  clock: filtersSubmitted,
  fn: () => 0,
  target: pageNumChanged,
});

sample({
  clock: [pageMounted, $tableParams, $filters, $settingsInitialized],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }) => {
    const { page, per_page } = table;

    const payload = formatPayload({ filters, page, per_page });

    return payload;
  },
  target: fxGetStatisticsList,
});

sample({
  source: { filters: $filters },
  clock: exportClicked,
  fn: ({ filters }) => {
    const payload = formatPayload({
      filters,
      ...{ per_page: exportLimit, isExport: 1 },
    });

    return payload;
  },
  target: fxExportStatistics,
});

sample({
  source: fxExportStatistics.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `Statistics-communication_${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});
