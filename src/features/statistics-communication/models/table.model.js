import { format, parse } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';

import { createTableBag } from '@tools/factories';
import i18n from '@shared/config/i18n';

import { $rawStatisticsData } from './page.model';

const { t } = i18n;

export const $rowCount = $rawStatisticsData.map(({ meta }) => meta?.total || 0);

export const $tableData = $rawStatisticsData.map(
  ({ items }) => (items && formatStatisticsData(items)) || []
);

const formatStatisticsData = (items) => {
  const translateCommunicationType = (type) => {
    switch (type) {
      case 'poll':
        return t('navMenu.informationCenter.polls');
      case 'announcement':
        return t('Announcements');
      default:
        return t('SomeNews');
    }
  };

  return items
    .map((item) => ({
      id: `${item.type}_${item.id}_${item.building_id}`,
      date: format(new Date(item.created_at), 'dd.MM.yyyy'),
      type: translateCommunicationType(item.type),
      name: item.title,
      complex: item.complex_title,
      address: item.building_address,
      people:
        item.statistic?.count || item.statistic?.count === 0
          ? item.statistic.count
          : t('Unknown'),
      read:
        item.statistic?.read || item.statistic?.read === 0
          ? item.statistic.read
          : t('Unknown'),
      unread:
        item.statistic?.unread || item.statistic?.unread === 0
          ? item.statistic.unread
          : t('Unknown'),
      coverage:
        item.statistic?.coverage || item.statistic?.coverage === 0
          ? item.statistic.coverage
          : t('Unknown'),
    }))
    .sort((a, b) => {
      const date1 = parse(a.date, 'dd.MM.yyyy', new Date(), {
        locale: dateFnsLocale,
      });

      const date2 = parse(b.date, 'dd.MM.yyyy', new Date(), {
        locale: dateFnsLocale,
      });

      return Number(date2) - Number(date1);
    });
};

export const columns = [
  { name: 'date', title: t('Label.date') },
  { name: 'type', title: t('TypeOfCommunication') },
  { name: 'name', title: t('Name') },
  { name: 'complex', title: t('RC') },
  { name: 'address', title: t('AddressOfTheObject') },
  { name: 'people', title: t('TotalUsers') },
  { name: 'read', title: t('AcquaintedResidents') },
  { name: 'unread', title: t('NotFamiliarizedResidents') },
  { name: 'coverage', title: t('Coverage') },
];

const columnsWidth = [
  { columnName: 'date', width: 120 },
  { columnName: 'type', width: 205 },
  { columnName: 'name', width: 250 },
  { columnName: 'complex', width: 200 },
  { columnName: 'address', width: 320 },
  { columnName: 'people', width: 180 },
  { columnName: 'read', width: 280 },
  { columnName: 'unread', width: 280 },
  { columnName: 'coverage', width: 120 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
  $columnOrder,
  columnOrderChanged,
} = createTableBag(columns, {
  widths: columnsWidth,
});
