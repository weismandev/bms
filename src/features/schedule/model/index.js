import {
  createEvent,
  createEffect,
  createStore,
  combine,
  guard,
  sample,
  forward,
  split,
  merge,
} from 'effector';
import { createGate } from 'effector-react';
import format from 'date-fns/format';
import { signout } from '../../common';
import { scheduleApi } from '../api';
import {
  formatSettingsResponse,
  formatAppointmentResponse,
  getDate,
  closeAppointmentAvailability,
  openAppointmentAvailability,
} from '../lib';

const emptyScheduleSettings = {
  header: '',
  settings: [
    {
      days_of_week: [],
      cell_duration: '5',
      time_start: '',
      time_end: '',
      breaks: [{ time_start: '', time_end: '' }],
    },
  ],
};

const PageGate = createGate();

const pageUnmounted = PageGate.close;

const changedEmployee = createEvent();
const changedWeek = createEvent();
const switchSettingsVisibility = createEvent();
const switchScheduleVisibility = createEvent();
const switchReportVisibility = createEvent();
const saveScheduleSubmitted = createEvent();
const appointmentSubmitted = createEvent();
const reportSubmitted = createEvent();
const openAppointment = createEvent();
const switchDeleteDialogVisibility = createEvent();
const switchErrorDialogVisibility = createEvent();
const deleteConfirmed = createEvent();
const setAppointmentDialog = createEvent();

const fxLoadSchedule = createEffect();
const fxSaveSettings = createEffect();
const fxLoadSettings = createEffect();
const fxMakeAppointment = createEffect();
const fxLoadAppointment = createEffect();
const fxUpdateAppointment = createEffect();
const fxDeleteAppointment = createEffect();
const fxGetPeriodReport = createEffect();

const $scheduleData = createStore([]);
const $isSettingsOpen = createStore(false);
const $isScheduleOpen = createStore(false);
const $isReportOpen = createStore(false);
const $employee = createStore('');
const $week = createStore(null);
const $scheduleSettings = createStore(emptyScheduleSettings);
const $deleteDialogIsOpen = createStore(false);
const $isAppointmentLoading = createStore(false);
const $isSettingsLoading = createStore(false);
const $isScheduleLoading = createStore(false);
const $isReportLoading = createStore(false);
const $error = createStore(null);
const $erorrDialogIsOpen = createStore(false);
const $reportData = createStore([]);
const $appointmentDialog = createStore({ isOpen: false });

const $commonPayload = combine($employee, $week, (employee, week) => ({
  employee,
  week,
}));

const $schedulePayload = $commonPayload.map(({ employee, week }) =>
  Boolean(employee) && Boolean(week)
    ? {
        user_id: employee.id,
        period_from: format(week.weekStart, 'dd.MM.yyyy'),
        period_to: format(week.weekEnd, 'dd.MM.yyyy'),
      }
    : null
);

const $settingsPayload = combine(
  $schedulePayload,
  $isSettingsOpen,
  (payload, visibility) => ({
    user_id: payload ? payload.user_id : null,
    visibility,
  })
);

const $openedAppointment = combine($employee, (employee) => ({
  to_user: employee,
  user: '',
  date: '',
  time_from: '',
  time_to: '',
  description: '',
}));

const schedule = split(fxLoadSchedule.done, {
  found: ({ result }) => Boolean(result),
  notFound: ({ result }) => result === null,
});

const settingsApi = split(fxLoadSettings.done, {
  found: ({ result }) => Boolean(result),
  notFound: ({ result }) => result === null,
});

const appointmentApi = split(openAppointment, {
  openFree: (payload) => payload !== null && payload.is_open,
  openExist: (payload) => payload !== null && !payload.is_open,
});

const reportSidebarApi = split(switchReportVisibility, {
  opened: (visibility) => visibility,
  closed: (visibility) => !visibility,
});

const gotSchedule = merge([schedule.found, fxSaveSettings.done]);

const createdRequest = merge([
  fxSaveSettings.pending,
  fxLoadSettings.pending,
  fxLoadSchedule.pending,
  fxLoadAppointment.pending,
  fxMakeAppointment.pending,
  fxUpdateAppointment.pending,
  fxDeleteAppointment.pending,
  fxGetPeriodReport.pending,
]).filter({ fn: (isLoad) => isLoad });

const gotError = merge([
  fxSaveSettings.fail,
  fxLoadSettings.fail,
  fxLoadSchedule.fail,
  fxLoadAppointment.fail,
  fxMakeAppointment.fail,
  fxUpdateAppointment.fail,
  fxDeleteAppointment.fail,
  fxGetPeriodReport.fail,
]);

const closedErrorDialog = switchErrorDialogVisibility.filter({
  fn: (visibility) => !visibility,
});

const openedSettingsSidebar = switchSettingsVisibility.filter({
  fn: (visibility) => visibility,
});

const resetSelectedEmployee = changedEmployee.filter({
  fn: (selectedEmployee) => !selectedEmployee,
});

$scheduleData
  .on(gotSchedule, (state, { result }) => result.schedule)
  .on(fxMakeAppointment.done, closeAppointmentAvailability)
  .on(fxDeleteAppointment.done, openAppointmentAvailability)
  .reset([resetSelectedEmployee, schedule.notFound, pageUnmounted, signout]);

$employee
  .on(changedEmployee, (state, selectedEmployee) => selectedEmployee)
  .reset([pageUnmounted, signout]);

$week.on(changedWeek, (state, pickedWeek) => pickedWeek).reset([pageUnmounted, signout]);

$isSettingsOpen
  .on(switchSettingsVisibility, (state, visibility) => visibility)
  .on(schedule.notFound, () => true)
  .on(merge([reportSidebarApi.opened, resetSelectedEmployee]), () => false)
  .reset([pageUnmounted, signout]);

$isScheduleOpen
  .on(switchScheduleVisibility, (state, visibility) => visibility)
  .on(gotSchedule, () => true)
  .on(merge([schedule.notFound, resetSelectedEmployee]), () => false)
  .reset([pageUnmounted, signout]);

$isReportOpen
  .on(switchReportVisibility, (state, visibility) => visibility)
  .on(
    merge([openedSettingsSidebar, resetSelectedEmployee, schedule.notFound]),
    () => false
  )
  .reset([pageUnmounted, signout]);

$scheduleSettings
  .on(settingsApi.found, (state, { result }) => formatSettingsResponse(result))
  .reset([
    schedule.notFound,
    settingsApi.notFound,
    resetSelectedEmployee,
    pageUnmounted,
    signout,
  ]);

$openedAppointment
  .on(fxLoadAppointment.done, (state, { result }) =>
    formatAppointmentResponse(result.appointment)
  )
  .on(
    sample(
      $commonPayload,
      appointmentApi.openFree,
      ({ week, employee }, { dayNum, time_start, time_end }) => ({
        user: '',
        to_user: employee,
        date: getDate(week, dayNum),
        time_from: time_start,
        time_to: time_end,
        description: '',
      })
    ),
    (state, data) => data
  )
  .reset(signout);

$isAppointmentLoading
  .on(
    merge([
      fxLoadAppointment.pending,
      fxMakeAppointment.pending,
      fxUpdateAppointment.pending,
      fxDeleteAppointment.pending,
    ]),
    (state, isLoad) => isLoad
  )
  .reset([appointmentApi.openFree, pageUnmounted, signout]);

$isSettingsLoading
  .on(merge([fxSaveSettings.pending, fxLoadSettings.pending]), (state, isLoad) => isLoad)
  .reset([pageUnmounted, signout]);

$isScheduleLoading
  .on(fxLoadSchedule.pending, (state, isLoad) => isLoad)
  .reset([pageUnmounted, signout]);

$isReportLoading
  .on(fxGetPeriodReport.pending, (state, isLoad) => isLoad)
  .reset([pageUnmounted, signout]);

$deleteDialogIsOpen
  .on(switchDeleteDialogVisibility, (state, visibility) => visibility)
  .on(fxDeleteAppointment.done, (state) => false)
  .reset([pageUnmounted, signout]);

$error
  .on(gotError, (state, { error }) => error)
  .reset([createdRequest, closedErrorDialog, pageUnmounted, signout]);

$erorrDialogIsOpen
  .on(switchErrorDialogVisibility, (state, visibility) => visibility)
  .on(gotError, (state) => true)
  .reset([createdRequest, pageUnmounted, signout]);

$reportData
  .on(fxGetPeriodReport.done, (state, { result }) => result.appointments)
  .reset([
    pageUnmounted,
    reportSidebarApi.closed,
    resetSelectedEmployee,
    openedSettingsSidebar,
    signout,
  ]);

$appointmentDialog
  .on(setAppointmentDialog, (state, newState) => ({ ...state, ...newState }))
  .on(fxMakeAppointment.done, (state, newState) => ({
    ...state,
    ...{ isOpen: false },
  }))
  .reset([pageUnmounted, signout, fxDeleteAppointment.done]);

fxLoadSchedule.use(scheduleApi.loadSchedule);
fxSaveSettings.use(scheduleApi.saveSettings);
fxLoadSettings.use(scheduleApi.loadSettings);
fxMakeAppointment.use(scheduleApi.makeAppointment);
fxLoadAppointment.use(scheduleApi.loadAppointment);
fxUpdateAppointment.use(scheduleApi.updateAppointment);
fxDeleteAppointment.use(scheduleApi.deleteAppointment);
fxGetPeriodReport.use(scheduleApi.getPeriodReport);

guard({
  source: $schedulePayload,
  filter: (payload) => Boolean(payload),
  target: fxLoadSchedule,
});

guard({
  source: sample(
    $schedulePayload,
    saveScheduleSubmitted,
    ({ user_id }, { settings, header }) => ({
      user_id,
      settings,
      header,
    })
  ),
  filter: ({ user_id }) => Boolean(user_id),
  target: fxSaveSettings,
});

guard({
  source: $settingsPayload,
  filter: ({ visibility, user_id }) => user_id && visibility,
  target: fxLoadSettings,
});

guard({
  source: sample(
    $schedulePayload,
    reportSubmitted,
    ({ user_id }, { date_from, date_to }) => ({
      to_user_id: user_id,
      date_from: format(new Date(date_from), 'dd.LL.yyyy'),
      date_to: format(new Date(date_to), 'dd.LL.yyyy'),
      per_page: 1000,
    })
  ),
  filter: ({ to_user_id }) => to_user_id,
  target: fxGetPeriodReport,
});

const preparedSubmittedAppointment = appointmentSubmitted.map(
  ({ user, to_user, ...rest }) => ({
    user_id: user.id || '',
    to_user_id: to_user.id,
    ...rest,
  })
);

const appointmentSubmittedApi = split(preparedSubmittedAppointment, {
  create: ({ is_open }) => is_open,
  update: ({ is_open }) => !is_open,
});

forward({
  from: appointmentSubmittedApi.create,
  to: fxMakeAppointment,
});

forward({
  from: appointmentSubmittedApi.update,
  to: fxUpdateAppointment,
});

forward({
  from: sample(
    $commonPayload,
    appointmentApi.openExist,
    ({ employee, week }, { dayNum, time_start, time_end }) => ({
      to_user_id: employee.id,
      date: getDate(week, dayNum),
      time_from: time_start,
      time_to: time_end,
    })
  ),
  to: fxLoadAppointment,
});

forward({
  from: sample(
    $openedAppointment,
    deleteConfirmed,
    ({ to_user, time_from, time_to, date }) => ({
      to_user_id: to_user?.user?.id,
      date,
      time_from,
      time_to,
    })
  ),
  to: fxDeleteAppointment,
});

export {
  PageGate,
  $employee,
  $week,
  changedEmployee,
  changedWeek,
  $isSettingsOpen,
  switchSettingsVisibility,
  $isScheduleOpen,
  switchScheduleVisibility,
  $scheduleSettings,
  saveScheduleSubmitted,
  $scheduleData,
  appointmentSubmitted,
  $openedAppointment,
  openAppointment,
  $isAppointmentLoading,
  $deleteDialogIsOpen,
  switchDeleteDialogVisibility,
  deleteConfirmed,
  $isSettingsLoading,
  $isScheduleLoading,
  $error,
  $erorrDialogIsOpen,
  switchErrorDialogVisibility,
  switchReportVisibility,
  $isReportOpen,
  reportSubmitted,
  $reportData,
  $isReportLoading,
  fxMakeAppointment,
  $appointmentDialog,
  setAppointmentDialog,
};
