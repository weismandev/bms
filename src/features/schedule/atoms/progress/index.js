import { CircularProgress } from '@mui/material';

const Progress = ({ isOpen }) =>
  isOpen ? (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        zIndex: 20,
        background: 'rgba(255,255,255, 0.5)',
        position: 'absolute',
      }}
    >
      <CircularProgress />
    </div>
  ) : null;

export { Progress };
