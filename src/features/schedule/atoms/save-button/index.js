import SaveOutlinedIcon from '@mui/icons-material/SaveOutlined';
import i18n from '@shared/config/i18n';
import { ActionButton } from '@ui/index';

const { t } = i18n;

const SaveButton = (props) => (
  <ActionButton kind="positive" type="submit" {...props}>
    <SaveOutlinedIcon />
    <span style={{ padding: '0 5px' }}>{t('Save')}</span>
  </ActionButton>
);

export { SaveButton };
