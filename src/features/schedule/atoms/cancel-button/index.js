import ClearIcon from '@mui/icons-material/Clear';
import i18n from '@shared/config/i18n';
import { ActionButton } from '@ui/index';

const { t } = i18n;

const CancelButton = (props) => (
  <ActionButton type="reset" kind="negative" {...props}>
    <ClearIcon />
    <span style={{ padding: '0 5px' }}>{t('cancel')}</span>
  </ActionButton>
);

export { CancelButton };
