import { forwardRef } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Modal } from '@mui/material';
import UpdateIcon from '@mui/icons-material/Update';
import makeStyles from '@mui/styles/makeStyles';
import { HaveSectionAccess } from '@features/common';
import { EmployeeSelect } from '@features/employee-select';
import { AsyncPeopleSelect } from '@features/people-async-select';
import i18n from '@shared/config/i18n';
import {
  ErrorMessage,
  ScheduleLayout,
  Wrapper,
  FormSectionHeader,
  InputField,
  ActionButton,
  DeleteButton,
  CloseButton,
  SelectField,
} from '@ui/index';
import {
  SaveButton,
  CancelButton,
  appointmentSubmitted,
  $openedAppointment,
  $isAppointmentLoading,
  switchDeleteDialogVisibility,
  Progress,
  $appointmentDialog,
  setAppointmentDialog,
  ScheduleSettingsToolbar,
  ScheduleSettingsSidebar,
  PageGate,
  $isSettingsOpen,
  $isScheduleOpen,
  ScheduleGrid,
  $error,
  $erorrDialogIsOpen,
  switchErrorDialogVisibility,
  $isReportOpen,
  ScheduleReportSidebar,
} from '..';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  formPaper: { position: 'relative' },
}));

const EmployeeField = (props) => (
  <SelectField component={<EmployeeSelect />} {...props} />
);

const PeopleField = (props) => (
  <SelectField component={<AsyncPeopleSelect />} {...props} />
);

const ModalWrapper = forwardRef((props, _) => <Wrapper {...props} />);

const AppointmentPopper = (props) => {
  const { isOpen, onClosed, isCellFree, isPastDate } = props;

  const openedAppointment = useStore($openedAppointment);
  const isAppointmentLoading = useStore($isAppointmentLoading);
  const { formPaper } = useStyles();

  const sharedProps = {
    readOnly: true,
    labelPlacement: 'start',
    inputProps: { style: { textAlign: 'end' } },
    divider: true,
    component: InputField,
  };

  const submitButton = isCellFree ? (
    <SaveButton />
  ) : (
    <ActionButton type="submit" kind="basic" style={{ marginRight: '10px' }}>
      <UpdateIcon />
      <span style={{ padding: '0 5px' }}>{t('Refresh')}</span>
    </ActionButton>
  );

  const deleteButton = !isCellFree && (
    <DeleteButton onClick={() => switchDeleteDialogVisibility(true)} />
  );

  return (
    <Modal
      open={isOpen}
      onClose={onClosed}
      disablePortal
      style={{ zIndex: 3, display: 'grid' }}
    >
      <ModalWrapper
        className={formPaper}
        style={{
          height: 'max-content',
          overflow: 'auto',
          width: 500,
          alignSelf: 'center',
          justifySelf: 'center',
        }}
      >
        <Progress isOpen={isAppointmentLoading} />
        <Formik
          initialValues={openedAppointment}
          enableReinitialize
          onSubmit={(values) => appointmentSubmitted({ ...values, is_open: isCellFree })}
          render={() => (
            <Form style={{ padding: 24 }}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <FormSectionHeader
                  header={
                    isCellFree
                      ? isPastDate
                        ? t('EnrollmentHasEnded')
                        : t('Appointment')
                      : isPastDate
                      ? t('ViewingAnArchivedEntry')
                      : t('EditingAPost')
                  }
                />
                <CloseButton onClick={onClosed} />
              </div>
              <Field
                label={t('Visitor')}
                helpText={t('CanBeLeftBlankIfThePersonIsNotLoggedIntoTheSystem')}
                placeholder={
                  isPastDate
                    ? `${t('Visitor')} ${t('UnknownHe')}`
                    : t('StartTypingTheNameOfTheResident')
                }
                component={PeopleField}
                name="user"
                readOnly={isPastDate}
              />
              <Field
                label={t('Employee')}
                name="to_user"
                startAdornment={null}
                {...sharedProps}
                component={EmployeeField}
              />
              <Field name="date" label={t('Label.date')} {...sharedProps} />
              <Field
                name="time_from"
                type="time"
                label={t('StartTime')}
                {...sharedProps}
              />
              <Field name="time_to" type="time" label={t('EndTime')} {...sharedProps} />
              <Field
                component={InputField}
                name="description"
                rowsMax={5}
                multiline
                label={t('AddInformation')}
                placeholder={
                  isPastDate ? t('NoDescription') : t('EnterAShortDescription')
                }
                readOnly={isPastDate}
              />
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                {isPastDate ? null : (
                  <>
                    {deleteButton}
                    {submitButton}
                    <CancelButton onClick={onClosed} />
                  </>
                )}
              </div>
            </Form>
          )}
        />
      </ModalWrapper>
    </Modal>
  );
};

const ScheduleSettingsPage = () => {
  const isSettingsOpen = useStore($isSettingsOpen);
  const isScheduleOpen = useStore($isScheduleOpen);
  const isReportOpen = useStore($isReportOpen);
  const erorrDialogIsOpen = useStore($erorrDialogIsOpen);
  const error = useStore($error);

  const { isOpen, isCellFree, isPastDate } = useStore($appointmentDialog);

  const onClosed = () => {
    setAppointmentDialog({ isOpen: false });
  };

  const closeErrorDialog = () => switchErrorDialogVisibility(false);

  const settings = isSettingsOpen ? <ScheduleSettingsSidebar /> : null;
  const report = isReportOpen ? <ScheduleReportSidebar /> : null;

  return (
    <>
      <PageGate />
      <ErrorMessage error={error} isOpen={erorrDialogIsOpen} onClose={closeErrorDialog} />
      <ScheduleLayout
        inverse={isReportOpen}
        top={<ScheduleSettingsToolbar />}
        left={settings || report}
        right={isScheduleOpen ? <ScheduleGrid /> : null}
      />
      <AppointmentPopper
        onClosed={onClosed}
        isCellFree={isCellFree}
        isOpen={isOpen}
        isPastDate={isPastDate}
      />
    </>
  );
};

const RestrictedSchedulePage = () => (
  <HaveSectionAccess>
    <ScheduleSettingsPage />
  </HaveSectionAccess>
);

export { RestrictedSchedulePage as ScheduleSettingsPage };
