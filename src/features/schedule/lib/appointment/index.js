const changeCellAvailability =
  (availability) =>
  (state, { params }) => {
    const dayIndex = state.findIndex((day) => day.date === params.date);
    const day = dayIndex > -1 && state[dayIndex];

    const cellIndex =
      day && day.cells.findIndex((cell) => cell.time_start === params.time_from);

    const cellOfAppointment = day && day.cells[cellIndex];

    const newAppointment = { ...cellOfAppointment, is_open: availability };
    const updatedDay = day && {
      ...day,
      cells: [
        ...day.cells.slice(0, cellIndex),
        newAppointment,
        ...day.cells.slice(cellIndex + 1),
      ],
    };

    return day
      ? [...state.slice(0, dayIndex), updatedDay, ...state.slice(dayIndex + 1)]
      : state;
  };

const closeAppointmentAvailability = changeCellAvailability(false);
const openAppointmentAvailability = changeCellAvailability(true);

export { closeAppointmentAvailability, openAppointmentAvailability };
