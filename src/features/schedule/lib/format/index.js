import { addDays, format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { isString, isNumber, isArray } from '@tools/type';

const formatDuration = (duration) =>
  isString(duration) || isNumber(duration) ? duration : '5';

const formatDaysOfWeek = (days) => (isArray(days) ? days.map((i) => String(i)) : []);

const formatTime = (time) => (/\d{1,2}:\d{1,2}/.test(time) ? time : '');

const formatBreaks = (breaks) =>
  isArray(breaks)
    ? breaks.map(({ time_start, time_end }) => ({
        time_start: formatTime(time_start),
        time_end: formatTime(time_end),
      }))
    : [];

const formatSettings = (settings) =>
  isArray(settings)
    ? settings.map((setting) => ({
        ...setting,
        days_of_week: formatDaysOfWeek(setting.days_of_week),
        cell_duration: formatDuration(setting.cell_duration),
        time_start: formatTime(setting.time_start),
        time_end: formatTime(setting.time_end),
        breaks: formatBreaks(setting.breaks),
      }))
    : [];

const formatSettingsResponse = ({ header, settings }) => ({
  header: header ? header : '',
  settings: formatSettings(settings),
});

const formatAppointmentResponse = ({
  to_user,
  user,
  time_start,
  time_end,
  date,
  attachments,
  description,
}) => ({
  to_user: { user: to_user },
  user: user ? { ...user, title: user.fullname } : '',
  time_from: formatTime(time_start),
  time_to: formatTime(time_end),
  date,
  attachments,
  description,
});

const getDate = (week, dayNumber) =>
  format(addDays(week.weekStart, dayNumber - 1), 'dd.MM.yyyy', {
    locale: dateFnsLocale,
  });

export { formatSettingsResponse, formatAppointmentResponse, getDate };
