import i18n from '@shared/config/i18n';

const { t } = i18n;

const days = [
  { id: '1', full: t('WeekDays.Monday'), short: t('WeekDays.MondayShort') },
  { id: '2', full: t('WeekDays.Tuesday'), short: t('WeekDays.TuesdayShort') },
  {
    id: '3',
    full: t('WeekDays.Wednesday'),
    short: t('WeekDays.WednesdayShort'),
  },
  { id: '4', full: t('WeekDays.Thursday'), short: t('WeekDays.ThursdayShort') },
  { id: '5', full: t('WeekDays.Friday'), short: t('WeekDays.FridayShort') },
  { id: '6', full: t('WeekDays.Saturday'), short: t('WeekDays.SaturdayShort') },
  { id: '7', full: t('WeekDays.Sunday'), short: t('WeekDays.SundayShort') },
];

export { days };
