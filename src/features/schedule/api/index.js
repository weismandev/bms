import { api } from '../../../api/api2';

const loadSchedule = (payload) =>
  api.v1('post', 'appointment2/crm/load-schedule', payload);
const saveSettings = (payload) =>
  api.v1('post', 'appointment2/crm/save-settings-batch', payload);
const loadSettings = (payload) =>
  api.v1('post', 'appointment2/crm/load-settings-compact', payload);
const makeAppointment = (payload) =>
  api.v1('post', 'appointment2/crm/make-appointment', payload);
const loadAppointment = (payload) =>
  api.v1('post', 'appointment2/crm/appointment-details', payload);
const updateAppointment = (payload) =>
  api.v1('post', 'appointment2/crm/update-appointment', payload);
const deleteAppointment = (payload) =>
  api.v1('post', 'appointment2/crm/revoke-appointment', payload);
const getPeriodReport = (payload) =>
  api.v1('post', 'appointment2/crm/appointments-to-user', payload);

export const scheduleApi = {
  loadSchedule,
  saveSettings,
  loadSettings,
  makeAppointment,
  loadAppointment,
  updateAppointment,
  deleteAppointment,
  getPeriodReport,
};
