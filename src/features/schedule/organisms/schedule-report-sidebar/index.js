import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { Formik, Form, FastField } from 'formik';
import { Grid, Typography, List, ListItem, ListItemText } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  switchReportVisibility,
  reportSubmitted,
  $reportData,
  Progress,
  $isReportLoading,
} from '../..';
import {
  Wrapper,
  FormSectionHeader,
  CloseButton,
  InputField,
  Divider,
  ActionIconButton,
  Dumb,
  CustomScrollbar,
} from '../../../../ui';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  appointmentItem: {
    '&:nth-child(odd)': {
      background: 'rgba(3, 148, 227, .05)',
    },
  },
}));

const ReportForm = (props) => {
  const { dataNotFound } = props;

  const sharedProps = {
    component: InputField,
    type: 'date',
    divider: false,
    required: true,
  };

  const today = format(new Date(), 'yyyy-LL-dd');

  return (
    <Formik
      initialValues={{ date_from: today, date_to: today }}
      onSubmit={reportSubmitted}
      render={({ isSubmitting }) => (
        <Form style={{ padding: 24, paddingBottom: 0 }}>
          <Grid
            container
            wrap="nowrap"
            alignItems="flex-end"
            justifyContent="space-between"
          >
            <Grid item>
              <FastField label={t('startDate')} name="date_from" {...sharedProps} />
            </Grid>
            <Grid item>
              <FastField label={t('expirationDate')} name="date_to" {...sharedProps} />
            </Grid>
            <Grid item>
              <ActionIconButton color="primary" type="submit">
                <SearchIcon />
              </ActionIconButton>
            </Grid>
          </Grid>
          <Divider />
          {isSubmitting && dataNotFound && (
            <Dumb>
              <Typography
                style={{ color: '#7d7d8e', fontWeight: 700, fontSize: 18 }}
                variant="body1"
              >
                {t('NoRecordsFoundForThePeriod')}
              </Typography>
            </Dumb>
          )}
        </Form>
      )}
    />
  );
};

const InfoBlock = (props) => {
  const { title, info } = props;

  return (
    <span style={{ display: 'block' }}>
      <Typography component="span" variant="body2">
        {`${title}: `}
      </Typography>
      <Typography component="span" variant="body1" style={{ fontWeight: 500 }}>
        {info}
      </Typography>
    </span>
  );
};

const ScheduleReportSidebar = () => {
  const closeReportSidebar = () => switchReportVisibility(false);

  const reportData = useStore($reportData);
  const isReportLoading = useStore($isReportLoading);
  const dataNotFound = reportData.length === 0 && !isReportLoading;

  const { appointmentItem } = useStyles();

  return (
    <Wrapper
      style={{
        height: '100%',
        width: '100%',
        position: 'relative',
      }}
    >
      <CustomScrollbar autoHide>
        <Progress isOpen={isReportLoading} />
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 24,
          }}
        >
          <FormSectionHeader style={{ margin: 0 }} header={t('EntriesForThePeriod')} />
          <CloseButton onClick={closeReportSidebar} />
        </div>
        <ReportForm dataNotFound={dataNotFound} />

        <List>
          {reportData.map(({ date, time_start, time_end, user }, idx) => {
            const header = (
              <Typography
                style={{ color: '#7d7d8e', fontWeight: 700, fontSize: 18 }}
                variant="body1"
              >
                {`${t('informationCenter.appointment')} #${idx + 1}`}
              </Typography>
            );

            const content = (
              <>
                <InfoBlock
                  title={t('Visitor')}
                  info={user ? user.fullname : t('UnknownHe')}
                />
                <InfoBlock title={t('Label.date')} info={date} />
                <InfoBlock title={t('StartTime')} info={time_start} />
                <InfoBlock title={t('EndTime')} info={time_end} />
              </>
            );

            return (
              <ListItem classes={{ root: appointmentItem }} key={date + time_start}>
                <ListItemText primary={header} secondary={content} />
              </ListItem>
            );
          })}
        </List>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { ScheduleReportSidebar };
