import { useRef, memo } from 'react';
import { useStore } from 'effector-react';
import { isBefore, add } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Grid, useMediaQuery, ListItem, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Wrapper, DeleteConfirmDialog, CustomScrollbar } from '@ui/index';
import {
  days,
  $scheduleData,
  $openedAppointment,
  openAppointment,
  $deleteDialogIsOpen,
  switchDeleteDialogVisibility,
  deleteConfirmed,
  $week,
  getDate,
  $isScheduleLoading,
  Progress,
  setAppointmentDialog,
} from '../..';

const { t } = i18n;

const useStyles = makeStyles((theme) => ({
  grid: {
    width: '100%',
    display: 'grid',
    padding: 24,
    gridTemplateRows: (props) => `100px repeat(${props.numCells}, 50px)`,
    gridTemplateColumns: 'repeat(7, minmax(50px, 250px))',
    gridGap: 10,
  },
  header: {
    position: 'sticky',
    top: '0',
    background: '#fff',
    zIndex: 3,
  },
  headerCell: {
    width: 150,
    [theme.breakpoints.down('xl')]: { width: 50, fontSize: 18 },
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    textAlign: 'center',
    fontWeight: 900,
    fontSize: 20,
    color: '#65657B',
  },
  cellText: {
    fontSize: 16,
    color: '#65657B',
    [theme.breakpoints.down('xl')]: { fontSize: 12 },
  },
}));

const AppointmentItem = memo((props) => {
  const { idx, dayNum, ...rest } = props;
  const ref = useRef(null);
  const week = useStore($week);

  const { cellText } = useStyles();

  const now = new Date();

  const [hours, minutes] = props.time_start.split(':');
  const date = add(week.weekStart, { days: dayNum - 1, hours, minutes }, { locale: dateFnsLocale });

  const isPastDate = isBefore(date, now);

  const cellTitle = rest.title;
  const isCellFree = rest.is_open;

  const onOpened = (payload) => {
    openAppointment(payload);
    setAppointmentDialog({ isOpen: true, isCellFree, isPastDate, ref });
  };

  const background = isPastDate
    ? 'rgba(235, 87, 87, .5)'
    : isCellFree
    ? 'rgba(50, 168, 82, .1)'
    : 'rgba(235, 87, 87, .5)';

  return (
    <>
      <ListItem
        button={!isPastDate || !isCellFree}
        onClick={() => (isPastDate && isCellFree ? null : onOpened({ ...rest, dayNum }))}
        ref={ref}
        component="div"
        style={{
          gridColumn: `${dayNum}/${dayNum}`,
          gridRow: `${idx + 2}/${idx + 3}`,
          justifyContent: 'center',
          background,
        }}
      >
        <Typography
          classes={{ root: cellText }}
          variantMapping={{ h6: 'span' }}
          variant="h6"
        >
          {cellTitle}
        </Typography>
      </ListItem>
    </>
  );
});

const AppointmentContent = (props) => {
  const { data } = props;

  const appointments = data.map((day) =>
    day.cells.map((cell, idx) => (
      <AppointmentItem key={cell.title} idx={idx} dayNum={day.day_of_week} {...cell} />
    ))
  );

  return appointments;
};

const AppointmentHeader = () => {
  const { header, headerCell } = useStyles();
  const week = useStore($week);

  const lowerMd = useMediaQuery((theme) => theme.breakpoints.down('xl'));
  return days.map((day) => {
    const date = getDate(week, day.id);
    const shortDate = date.split('.').slice(0, 2).join('.');

    return (
      <Grid
        classes={{ root: header }}
        container
        key={day.id}
        alignItems="center"
        justifyContent="center"
      >
        <Grid classes={{ root: headerCell }}>
          <div>{lowerMd ? day.short : day.full}</div>
          <div>{lowerMd ? shortDate : date}</div>
        </Grid>
      </Grid>
    );
  });
};

const DeleteDialog = (props) => {
  const deleteDialogIsOpen = useStore($deleteDialogIsOpen);
  const openedAppointment = useStore($openedAppointment);

  const closeDeleteDialog = () => switchDeleteDialogVisibility(false);
  const confirmDelete = () => deleteConfirmed();

  const deleteDialogContent = `${t('AreYouSureYouWantToDeleteYourAppointment')} ${
    openedAppointment.user ? `${t('For')} ${openedAppointment.user.fullname} ` : ''
  } ${t('On')} ${openedAppointment.date} в ${openedAppointment.time_from}?`;

  return (
    <DeleteConfirmDialog
      content={deleteDialogContent}
      header={t('DeletingAnAppointment')}
      isOpen={deleteDialogIsOpen}
      confirm={confirmDelete}
      close={closeDeleteDialog}
      {...props}
    />
  );
};

const ScheduleGrid = () => {
  const scheduleData = useStore($scheduleData);
  const isScheduleLoading = useStore($isScheduleLoading);

  const numberCells = Math.max(...scheduleData.map((day) => day.cells.length));
  const classes = useStyles({ numCells: numberCells });
  const { grid } = classes;

  return (
    <Wrapper style={{ height: '100%', width: '100%', position: 'relative' }}>
      <Progress isOpen={isScheduleLoading} />
      <CustomScrollbar autoHide>
        <DeleteDialog />
        <div className={grid}>
          <AppointmentHeader />
          <AppointmentContent data={scheduleData} />
        </div>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { ScheduleGrid };
