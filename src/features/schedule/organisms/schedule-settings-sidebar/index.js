import { useStore } from 'effector-react';
import {
  Wrapper,
  FormSectionHeader,
  InputField,
  Divider,
  ActionButton,
  CloseButton,
  BaseInputErrorText,
  ActionIconButton,
  CustomScrollbar,
} from '@ui';
import { Field, FastField, FieldArray, Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Grid, FormGroup, Checkbox, Button, CircularProgress } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  days,
  CancelButton,
  SaveButton,
  $isScheduleOpen,
  $scheduleSettings,
  saveScheduleSubmitted,
  switchSettingsVisibility,
  $isSettingsLoading,
} from '../..';
import { getPathOr } from '../../../../tools/path';

const { t } = i18n;

const sharedProps = {
  required: true,
  divider: false,
  type: 'time',
  component: InputField,
  inputProps: { style: { textAlign: 'center' } },
};

const styles = {
  dayChooser: {
    justifyContent: 'space-between',
    fontWeight: 900,
  },
  formToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
    alignItems: 'center',
  },
};

const newBreak = { time_start: '', time_end: '' };

const newSetting = {
  days_of_week: [],
  cell_duration: 5,
  time_start: '',
  time_end: '',
  breaks: [newBreak],
};

const daysInWeek = 7;

const useStyles = makeStyles(styles);

const DaysChooser = ({ field, form }) => {
  const { dayChooser } = useStyles();
  const settings = form.values.settings;
  const configuredDays = settings.flatMap(({ days_of_week }) => days_of_week);

  const { value, name } = field;

  const errors = [getPathOr(name, form.errors, [])].flat();
  const hasErrors = errors.length > 0;

  return (
    <>
      <FormGroup row className={dayChooser}>
        {days.map((day) => {
          const checked = value.includes(String(day.id));
          const disabled = !checked && configuredDays.includes(String(day.id));

          const check = (newValue) => value.concat(newValue);
          const uncheck = (checkedValue) => value.filter((i) => i !== checkedValue);

          const getNewFieldValue = (e) => {
            const value = e.target.value;
            return checked ? uncheck(value) : check(value);
          };

          const checkbox = (
            <Checkbox
              key={day.id}
              value={day.id}
              checked={checked}
              disabled={disabled}
              onChange={(e) => form.setFieldValue(name, getNewFieldValue(e))}
              icon={<ActionIconButton kind="no-active">{day.short}</ActionIconButton>}
              checkedIcon={<ActionIconButton kind="active">{day.short}</ActionIconButton>}
            />
          );

          return checkbox;
        })}
      </FormGroup>
      {hasErrors && <BaseInputErrorText style={{ color: 'red' }} errors={errors} />}
      <Divider />
    </>
  );
};

const BreaksSettings = ({ field }) => (
  <FieldArray
    name={field.name}
    render={({ push, remove }) =>
      field.value.map((brk, idx) => {
        const addBreak = () => push(newBreak);
        const removeBreak = () => remove(idx);

        const isFirstBreakInList = idx === 0;

        const addButton = (
          <Button onClick={addBreak} color="primary">
            {t('AddBreak')}
          </Button>
        );

        const removeButton = (
          <Button style={{ color: '#EB5757' }} onClick={removeBreak}>
            {t('RemoveBreak')}
          </Button>
        );

        const button = isFirstBreakInList ? addButton : removeButton;

        const brkPath = `${field.name}[${idx}]`;

        return (
          <div key={idx}>
            <Grid
              container
              justifyContent="space-between"
              wrap="nowrap"
              alignItems="flex-end"
            >
              <Grid item>
                <Field
                  name={`${brkPath}.time_start`}
                  label={t('BreakFrom')}
                  {...sharedProps}
                />
              </Grid>
              <Grid item>
                <Field
                  name={`${brkPath}.time_end`}
                  label={t('BreakTo')}
                  {...sharedProps}
                />
              </Grid>
              <Grid>{button}</Grid>
            </Grid>
            <Divider />
          </div>
        );
      })
    }
  />
);

const getColumnsWidth = (isScheduleOpen) =>
  isScheduleOpen ? { xs: 12 } : { xs: 12, md: 6, lg: 4 };

const DaysSettings = ({ field }) => {
  const isScheduleOpen = useStore($isScheduleOpen);
  const columns = getColumnsWidth(isScheduleOpen);

  return (
    <FieldArray
      name={field.name}
      render={({ push, remove }) => {
        const settings = field.value;

        const hasUnsetDays =
          settings.flatMap((setting) => setting.days_of_week).length !== daysInWeek;

        return settings.map((setting, idx, array) => {
          const isLastSettingsInList = idx === array.length - 1;
          const hasMoreOneSettings = array.length > 1;

          const addSetting = () => push(newSetting);
          const removeSetting = () => remove(idx);

          const addButton = (
            <ActionButton onClick={addSetting}>{t('AddSetting')}</ActionButton>
          );

          const removeButton = (
            <ActionButton kind="negative" onClick={removeSetting}>
              {t('RemoveSetting')}
            </ActionButton>
          );

          const addButtonElement =
            isLastSettingsInList && hasUnsetDays ? addButton : null;

          const removeButtonElement = hasMoreOneSettings ? removeButton : null;

          const settingPath = `${field.name}[${idx}]`;

          return (
            <Grid item {...columns} key={idx}>
              <Field name={`${settingPath}.days_of_week`} component={DaysChooser} />
              <Grid container justifyContent="space-between" wrap="nowrap">
                <Grid item>
                  <Field
                    name={`${settingPath}.time_start`}
                    label={t('AcceptFrom')}
                    {...sharedProps}
                  />
                </Grid>
                <Grid item>
                  <Field
                    name={`${settingPath}.time_end`}
                    label={t('AcceptTo')}
                    {...sharedProps}
                  />
                </Grid>
                <Grid item xs={4}>
                  <FastField
                    component={InputField}
                    name={`${settingPath}.cell_duration`}
                    label={t('Duration')}
                    placeholder={t('EnterDuration')}
                    helpText={t('MinDuration')}
                    type="number"
                    inputProps={{ min: 5 }}
                    required
                    divider={false}
                    endAdornment={
                      <span style={{ color: 'rgba(0, 0, 0, 0.54)' }}>{t('Min')}</span>
                    }
                  />
                </Grid>
              </Grid>
              <Divider />
              <FastField name={`${settingPath}.breaks`} component={BreaksSettings} />

              <div style={{ textAlign: 'end', marginBottom: 30 }}>
                {addButtonElement}
                {removeButtonElement}
              </div>
            </Grid>
          );
        });
      }}
    />
  );
};

function startDateAfterFinishDate(startName, finishName) {
  const isTimesDefined =
    Boolean(this.parent[startName]) && Boolean(this.parent[finishName]);

  if (isTimesDefined && this.parent[startName] > this.parent[finishName]) {
    return this.createError();
  }

  return true;
}

function checkTime() {
  return startDateAfterFinishDate.call(this, 'time_start', 'time_end');
}

function diffBreaksAndWorkTime() {
  const { time_start, time_end, breaks } = this.parent;

  if (Array.isArray(breaks)) {
    const hasSameBreakAndWorkTime = (i) =>
      i.time_start === time_start && i.time_end === time_end;

    return breaks.filter(hasSameBreakAndWorkTime).length === 0;
  }

  return true;
}

const validationSchema = Yup.object().shape({
  settings: Yup.array().of(
    Yup.object().shape({
      days_of_week: Yup.array().min(1, t('DaysOfTheWeekMustBeSelected')),
      time_start: Yup.string()
        .required(t('thisIsRequiredField'))
        .test(
          'start_time-after-finish_time',
          t('TheStartTimeMustBeBeforeTheEndTime'),
          checkTime
        )
        .test(
          'different-break-time-and-work-time',
          t('BreakAndReceptionTimeShouldNotBeTheSame'),
          diffBreaksAndWorkTime
        ),
      time_end: Yup.string()
        .required(t('thisIsRequiredField'))
        .test(
          'start_time-after-finish_time',
          t('TheEndTimeMustBeLaterThanTheStartTime'),
          checkTime
        )
        .test(
          'different-break-time-and-work-time',
          t('BreakAndReceptionTimeShouldNotBeTheSame'),
          diffBreaksAndWorkTime
        ),
      cell_duration: Yup.number()
        .transform((value) => (isNaN(value) ? 0 : value))
        .min(5, t('TheMinimumDurationIsAtLeast5Minutes')),
      breaks: Yup.array().of(
        Yup.object().shape({
          time_start: Yup.string()
            .required(t('thisIsRequiredField'))
            .test(
              'start_time-after-finish_time',
              t('TheStartTimeMustBeBeforeTheEndTime'),
              checkTime
            ),
          time_end: Yup.string()
            .required(t('thisIsRequiredField'))
            .test(
              'start_time-after-finish_time',
              t('TheEndTimeMustBeLaterThanTheStartTime'),
              checkTime
            ),
        })
      ),
    })
  ),
});

const ScheduleSettingsSidebar = () => {
  const { formToolbar } = useStyles();
  const scheduleSettings = useStore($scheduleSettings);

  const isScheduleOpen = useStore($isScheduleOpen);
  const isSettingsLoading = useStore($isSettingsLoading);

  const columns = getColumnsWidth(isScheduleOpen);

  const closeSettings = () => switchSettingsVisibility(false);

  return (
    <Wrapper style={{ height: '100%', width: '100%', position: 'relative' }}>
      {isSettingsLoading && (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%',
            position: 'absolute',
            top: 0,
            background: 'rgba(255,255,255, .5)',
            zIndex: 10,
          }}
        >
          <CircularProgress />
        </div>
      )}
      <CustomScrollbar autoHide>
        <Formik
          initialValues={scheduleSettings}
          validationSchema={validationSchema}
          onSubmit={saveScheduleSubmitted}
          enableReinitialize
        >
          <Form style={{ padding: 24 }}>
            <div className={formToolbar}>
              <FormSectionHeader
                style={{ margin: 0 }}
                header={t('ProfilePage.Settings')}
              />
              <div>
                <SaveButton style={{ marginRight: 10 }} />
                <CancelButton style={{ marginRight: 10 }} />
                <CloseButton onClick={closeSettings} />
              </div>
            </div>
            <Grid container spacing={4}>
              <Grid item {...columns}>
                <FastField
                  name="header"
                  component={InputField}
                  label={t('ScheduleName')}
                  placeholder={t('EnterTheTitle')}
                  required
                />
              </Grid>
            </Grid>
            <Grid container spacing={4}>
              <FastField name="settings" component={DaysSettings} />
            </Grid>
          </Form>
        </Formik>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { ScheduleSettingsSidebar };
