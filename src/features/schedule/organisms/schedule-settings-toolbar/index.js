import { useStore } from 'effector-react';
import { Grid, Tooltip } from '@mui/material';
import AssignmentIcon from '@mui/icons-material/Assignment';
import SettingsIcon from '@mui/icons-material/Settings';
import i18n from '@shared/config/i18n';
import {
  $employee,
  changedWeek,
  changedEmployee,
  $isSettingsOpen,
  switchSettingsVisibility,
  switchReportVisibility,
  $isReportOpen,
  $scheduleData,
} from '../..';
import { Wrapper, WeekControl, ActionIconButton } from '../../../../ui';
import { EmployeeSelect } from '../../../employee-select';

const { t } = i18n;

const ScheduleSettingsToolbar = (props) => {
  const selectedEmployee = useStore($employee);
  const isSettingsOpen = useStore($isSettingsOpen);
  const isReportOpen = useStore($isReportOpen);
  const scheduleData = useStore($scheduleData);
  const hasAppointments = scheduleData.length > 0;

  const openSettings = () => switchSettingsVisibility(true);
  const openReport = () => switchReportVisibility(true);

  return (
    <Wrapper
      style={{
        height: '100%',
        width: '100%',
        overflow: 'inherit',
        padding: 24,
      }}
    >
      <Grid container alignItems="center" wrap="nowrap" style={{ height: '100%' }}>
        <Grid item sm={4} md={4}>
          <EmployeeSelect value={selectedEmployee} onChange={changedEmployee} />
        </Grid>
        <Grid item sm={5} md={3} lg={2} style={{ margin: '0 15px 0 auto' }}>
          <WeekControl onChange={changedWeek} />
        </Grid>
        <Grid item sm={2} style={{ textAlign: 'end' }}>
          <Tooltip title={t('ScheduleSettings')}>
            <span>
              <ActionIconButton
                style={{ margin: '0 10px 0 auto' }}
                onClick={openSettings}
                disabled={isSettingsOpen || !selectedEmployee}
                kind="positive"
              >
                <SettingsIcon />
              </ActionIconButton>
            </span>
          </Tooltip>

          <Tooltip title={t('AssignedRecords')}>
            <span>
              <ActionIconButton
                onClick={openReport}
                disabled={isReportOpen || !selectedEmployee || !hasAppointments}
                kind="basic"
              >
                <AssignmentIcon />
              </ActionIconButton>
            </span>
          </Tooltip>
        </Grid>
      </Grid>
    </Wrapper>
  );
};

export { ScheduleSettingsToolbar };
