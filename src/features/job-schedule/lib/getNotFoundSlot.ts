import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { EmployeeDiagramData } from '../interfaces';

const FORMAT = 'yyyy-MM-dd HH:mm:ss';

const { t } = i18n;

const getNotFoundSlot = (date: string): [EmployeeDiagramData] => {
  const selectedDay = date.split('T')[0];

  const dateStart = new Date(selectedDay);
  const dateEnd = new Date(selectedDay);

  dateStart.setHours(0, 0, 0);
  dateEnd.setHours(23, 59, 0);

  return [
    {
      user_id: 1942978,
      user_name: '',
      slots: [
        {
          start: format(dateStart, FORMAT),
          end: format(dateEnd, FORMAT),
          status: 'not_working_time',
          ticket_number: t('EmployeesOnApplicationAreNotAssignedToWorkToday'),
          id: dateStart.getTime(),
          ticket_start: format(dateStart, FORMAT),
          address: 'address',
          complex_title: 'complex_title',
          property_title: 'property_title',
          region_name: 'region_name',
          disabledPreview: true,
          style: { paddingTop: 10, paddingLeft: 16 },
        },
      ],
    },
  ];
};

export { getNotFoundSlot };
