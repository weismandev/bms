import i18n from '@shared/config/i18n';
import { Status } from '../interfaces';

type Description = {
  [key in Status]: {
    name: string;
    color: string;
  };
};

const { t } = i18n;

const descriptionStatuses: Description = {
  white: {
    name: t('NoTicket'),
    color: '#35A9E9',
  },
  planned: {
    name: t('Scheduled'),
    color: '#AAAAAA',
  },
  completed_on_time: {
    name: t('Performed'),
    color: '#1BB169',
  },
  skipped: {
    name: t('NotDone'),
    color: '#EB5757',
  },
  late: {
    name: t('Lag'),
    color: '#EC8A33',
  },
  break: {
    name: t('Break'),
    color: '#7D7D8E',
  },
  completed_with_late: {
    name: t('Lateness'),
    color: '#E4C000',
  },
  not_working_time: {
    name: t('NotWorkingTimeLunchBreak'),
    color: '#65657B',
  },
  vacation: {
    name: t('Vacation'),
    color: '#65657B',
  },
};

const getStatus = (status: Status) => descriptionStatuses[status] || {};

export { getStatus };
