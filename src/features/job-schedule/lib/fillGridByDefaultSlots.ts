import { nanoid } from 'nanoid';
import { Slot } from '../interfaces';

interface ISlotGrid extends Slot {
  gridColumnStart: number;
  gridColumnEnd: number;
}

interface ISlotGridDivider extends Slot {
  gridColumnStart: { startHourDivider: number; startMinuteDivider: number };
  gridColumnEnd: { endHourDivider: number; endMinuteDivider: number };
}

const initSlot = {
  id: '',
  end: '',
  start: '',
  status: '',
  ticket_end: '',
  ticket_number: '',
  ticket_start: '',
  title: '',
  address: '',
  complex_title: '',
  property_title: '',
  region_name: '',
  ticket_object: {
    address: '',
    complex_title: '',
    property_title: '',
    region_name: '',
  },
};

const getStep = () => {
  const array = [];
  let divider = 0;
  for (let i = 1; i < 60; i += 15) {
    array.push(new Array(15).fill(divider));
    divider += 1;
  }
  array[array.length - 1].splice(-1, 1, 4);
  return array.flat().reduce((acc, current, i) => ({ ...acc, [i]: current }), { 59: 4 });
};

const getDefaultProperties = (): Slot => ({
  ...initSlot,
  id: nanoid(),
  start: '2022-02-15T00:00:00.104Z',
  end: '2022-02-15T23:59:00.104Z',
  status: 'white',
});

function fillGridByDefaultSlots(slots: ISlotGridDivider[]) {
  let defaultGridColumnStart: {
    startHourDivider: number;
    startMinuteDivider: number;
  } = {
    startHourDivider: 1,
    startMinuteDivider: 0,
  };

  const filledSlots: ISlotGrid[] = [];
  const dividerByStep = getStep();

  slots.forEach((slot: ISlotGridDivider) => {
    const {
      gridColumnStart: { startHourDivider, startMinuteDivider },
      gridColumnEnd: { endHourDivider, endMinuteDivider },
    } = slot;
    const start = startHourDivider * 4 + dividerByStep[startMinuteDivider] + 1;
    let end = endHourDivider * 4 + dividerByStep[endMinuteDivider] + 1;

    if (start === end) {
      end += 1;
    }

    if (start > defaultGridColumnStart.startHourDivider) {
      const lastSlot = filledSlots[filledSlots.length - 1];
      if (lastSlot && defaultGridColumnStart.startHourDivider !== start) {
        filledSlots.push({
          ...getDefaultProperties(),
          gridColumnStart: defaultGridColumnStart.startHourDivider,
          gridColumnEnd: start,
        });
      } else {
        filledSlots.push({
          ...getDefaultProperties(),
          gridColumnStart: defaultGridColumnStart.startHourDivider,
          gridColumnEnd: start,
        });
      }
      filledSlots.push({
        ...slot,
        gridColumnStart: start,
        gridColumnEnd: end,
      });
    } else {
      filledSlots.push({
        ...slot,
        gridColumnStart: start,
        gridColumnEnd: end,
      });
    }
    defaultGridColumnStart = {
      ...defaultGridColumnStart,
      startHourDivider: end,
    };
  });

  const lastSlot = filledSlots[filledSlots.length - 1];
  if (lastSlot && lastSlot?.gridColumnEnd < 97) {
    filledSlots.push({
      ...getDefaultProperties(),
      gridColumnStart: lastSlot.gridColumnEnd,
      gridColumnEnd: 97,
      style: { padding: 0 },
    });
  }

  return filledSlots;
}

export { fillGridByDefaultSlots };
