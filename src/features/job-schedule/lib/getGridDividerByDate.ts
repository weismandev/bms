import { format } from 'date-fns';

export default (date: Date | string): number[] =>
  format(new Date(date), 'HH:m').split(':').map(Number);
