const setBorderDay = (date: Date, selectedDate: Date): Date => {
  selectedDate.setHours(0, 0, 0);
  if (date < selectedDate) {
    date.setHours(0, 0, 0);
  }

  selectedDate.setHours(23, 59, 0);
  if (date > selectedDate) {
    date.setHours(23, 59, 0);
  }

  return date;
};

export { setBorderDay };
