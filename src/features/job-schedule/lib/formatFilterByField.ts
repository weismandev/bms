type Filed = {
  id: number | string;
  title: string;
};

const formatFilterByField = (fields: Filed[]): string =>
  fields.map(({ id }) => id).join(',');

export { formatFilterByField };
