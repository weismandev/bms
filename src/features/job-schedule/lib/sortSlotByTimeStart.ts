import { Slot } from '../interfaces';

const sortSlotByTimeStart = (a: Slot, b: Slot) =>
  new Date(a.start).getTime() - new Date(b.start).getTime();

export { sortSlotByTimeStart };
