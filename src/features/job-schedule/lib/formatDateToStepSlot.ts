import format from 'date-fns/format';

export const formatDateToStepSlot = (date: Date) =>
  format(date, 'HH:mm').split(':').map(Number);
