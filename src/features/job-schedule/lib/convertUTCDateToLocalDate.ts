export const convertUTCDateToLocalDate = (date: Date): Date => {
  const utcHours = new Date(date.getTime()).getHours();

  const newDate =
    utcHours === 0
      ? new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000)
      : new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

  const offset = date.getTimezoneOffset() / 60;
  const hours = date.getHours();

  newDate.setHours(hours - offset);

  return newDate;
};
