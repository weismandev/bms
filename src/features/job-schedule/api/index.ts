import { api } from '../../../api/api2';
import {
  EmployeeListPayload,
  EmployeeListResponse,
  EmployeeDiagramPayload,
  EmployeeDiagramResponse,
} from '../interfaces';

const getEmployeeTicketList = (
  payload: EmployeeListPayload,
  config = {}
): EmployeeListResponse =>
  api.no_vers('get', 'v4/schedule/employee-ticket-list', payload, config);

const getEmployeeListDiagram = (
  payload: EmployeeDiagramPayload,
  config = {}
): EmployeeDiagramResponse =>
  api.no_vers('get', 'v4/schedule/employee-schedule-list', payload, config);

export const employeeApi = {
  getList: getEmployeeTicketList,
  getEmployeeListDiagram,
};
