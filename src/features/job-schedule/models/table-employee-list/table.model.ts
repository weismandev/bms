import { combine } from 'effector';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { createTableBag } from '../../../../tools/factories';
import { EmployeeList } from '../../interfaces';
import { getStatus } from '../../lib';
import { $raw } from '../page/page.model';

const { t } = i18n;

const convertFormatDate = (date: string | null) => {
  const FORMAT = 'dd.MM.YYY в HH:mm';
  return date && format(convertUTCDateToLocalDate(new Date(date)), FORMAT);
};

export const columns = [
  { name: 'user_name', title: t('NameEmployee') },
  { name: 'region_name', title: t('Label.city') },
  { name: 'complex_title', title: t('AnObject') },
  { name: 'address', title: t('building') },
  { name: 'property_title', title: t('room') },
  { name: 'planned_start_at', title: t('PlannedStartTime') },
  { name: 'fact_start_at', title: t('ActualStartTime') },
  { name: 'status', title: t('Label.status') },
];

export const excludedColumnsFromSort = [
  'user_name',
  'region_name',
  'complex_title',
  'address',
  'property_title',
  'planned_start_at',
  'fact_start_at',
  'status',
];

const widths = [
  { columnName: 'user_name', width: 200 },
  { columnName: 'region_name', width: 100 },
  { columnName: 'complex_title', width: 200 },
  { columnName: 'address', width: 300 },
  { columnName: 'property_title', width: 200 },
  { columnName: 'planned_start_at', width: 175 },
  { columnName: 'fact_start_at', width: 210 },
  { columnName: 'status', width: 200 },
];

export const $tableData = combine($raw, (response) =>
  response.data.map((item: EmployeeList) => ({
    user_id: item.user_id,
    user_name: item.user_name,
    region_name: item.region_name,
    complex_title: item.complex_title,
    address: item.address,
    property_title: item.property_title,
    planned_start_at: convertFormatDate(item.planned_start_at),
    fact_start_at: convertFormatDate(item.fact_start_at),
    status: getStatus(item.status),
    ticket_id: item.ticket_id,
  }))
);

export const $rowCount = $raw.map(({ meta = {} }) => meta?.total || 0);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths });
