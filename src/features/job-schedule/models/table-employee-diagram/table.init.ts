import { forward, guard, attach } from 'effector';
import { debounce } from 'patronum/debounce';
import { format } from 'date-fns';
import { signout } from '@features/common';
import { employeeApi } from '../../api';
import { TableParams, DefaultFilters } from '../../interfaces';
import { convertNumberToHourse, getNotFoundSlot } from '../../lib';
import { $filters, filtersSubmitted } from '../filter';
import {
  pageMounted,
  pageUnmounted,
  $view,
  $date,
  $isErrorDialogOpen,
  setDate,
  updateTimeEmployeeInMilliseconds,
} from '../page';
import { $tableParams } from '../table-employee-list/table.model';
import {
  fxGetEmployeeDiagram,
  $isLoading,
  $raw,
  $error,
  updateEmployeeDiagramByTimeout,
} from './table.model';

const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

fxGetEmployeeDiagram.use(employeeApi.getEmployeeListDiagram);

$isLoading.on(fxGetEmployeeDiagram.pending, (_, pending) => pending);
$raw
  .on(fxGetEmployeeDiagram.done, (_, { result, params: { date } }) => ({
    ...result,
    data: result.data?.length === 0 ? getNotFoundSlot(date) : result.data,
  }))
  .on(setDate, () => ({}))
  .reset(signout);

$error.on(fxGetEmployeeDiagram.fail, (_, { error }) => error);

$isErrorDialogOpen
  .on(fxGetEmployeeDiagram.fail, () => true)
  .reset([pageUnmounted, signout]);

forward({
  from: debounce({
    source: fxGetEmployeeDiagram.done,
    timeout: updateTimeEmployeeInMilliseconds,
  }),
  to: updateEmployeeDiagramByTimeout,
});

guard({
  clock: [
    pageMounted,
    $view,
    $date,
    $tableParams,
    updateEmployeeDiagramByTimeout,
    filtersSubmitted,
  ],
  source: $view,
  filter: (view) => view === 'diagram',
  target: attach({
    source: [$date, $tableParams, $filters],
    effect: fxGetEmployeeDiagram.prepend(
      ([date, tableParams, filters]: [Date, TableParams, DefaultFilters]) => ({
        ...tableParams,
        filter: {
          building_ids: filters.building_ids.map(({ id }) => id),
          user_ids: filters.user_ids.map(({ id }) => id),
          ticket_work_status: filters.status.map(({ id }) => id),
        },
        date: `${format(date, FORMAT)}${convertNumberToHourse(date.getTimezoneOffset())}`,
      })
    ),
  }),
});
