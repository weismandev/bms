import { createEffect, createStore, createEvent, combine } from 'effector';
import { formatDistance } from 'date-fns';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { EmployeeDiagramResponse, Employee, Slot } from '../../interfaces';
import {
  formatDateToStepSlot,
  fillGridByDefaultSlots,
  sortSlotByTimeStart,
  setBorderDay,
} from '../../lib';
import { $date } from '../page';

const { t } = i18n;

const distanceDay = ['about 24 hours', '1 day'];
const columns = [
  { name: 'employee', title: t('NameEmployee') },
  { name: 'slots', title: '' },
];

export const fxGetEmployeeDiagram = createEffect<any, EmployeeDiagramResponse, Error>();

export const updateEmployeeDiagramByTimeout = createEvent<void>();

export const $raw = createStore<EmployeeDiagramResponse>({});
export const $isLoading = createStore<boolean>(false);
export const $error = createStore<Error | null>(null);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: [{ columnName: 'actions', width: 300 }],
});

export const $rowCount = $raw.map(({ meta = {} }) => meta?.total || 0);
export const $employee = combine($raw, ({ data = [] }) =>
  data.map(({ user_id, user_name }: Employee) => ({
    user_id,
    user_name,
  }))
);

export const $slots = combine($raw, $date, ({ data = [] }, date) =>
  data.map(({ slots }) => {
    const gridSlots = slots.map((slot: Slot) => {
      const toDateStart = new Date(slot.start);
      const toDateEnd = new Date(slot.end);
      const isFullDay = distanceDay.includes(formatDistance(toDateStart, toDateEnd));

      let initStart, initEnd;
      if (isFullDay) {
        initStart = toDateStart;
        initEnd = toDateEnd;
      } else {
        initStart = convertUTCDateToLocalDate(toDateStart);
        initEnd = convertUTCDateToLocalDate(toDateEnd);
      }

      const start = setBorderDay(initStart, date);
      const end = setBorderDay(initEnd, date);

      const [startHourDivider, startMinuteDivider] = formatDateToStepSlot(start);
      const [endHourDivider, endMinuteDivider] = formatDateToStepSlot(end);

      const gridColumnStart = { startHourDivider, startMinuteDivider };
      const gridColumnEnd = { endHourDivider, endMinuteDivider };
      return {
        ...slot,
        start,
        end,
        gridColumnStart,
        gridColumnEnd,
      };
    });

    const filteredByTodayRange = gridSlots
      .filter(({ start, end }: { start: Date; end: Date }) => {
        date.setHours(0);
        date.setMinutes(0);
        if (date > start && date > end) return false;
        return end > start;
      })
      .sort(sortSlotByTimeStart);

    return fillGridByDefaultSlots(filteredByTodayRange);
  })
);
