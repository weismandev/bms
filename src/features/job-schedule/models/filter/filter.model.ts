import { createEvent } from 'effector';
import { createFilterBag } from '../../../../tools/factories';

const defaultFilters = {
  building_ids: [],
  user_ids: [],
  status: [],
};

const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

const changedGroupVisibility = createEvent();

export {
  changedFilterVisibility,
  filtersSubmitted,
  $isFilterOpen,
  $filters,
  changedGroupVisibility,
};
