import { signout } from '../../../common';
import { $isFilterOpen, changedGroupVisibility } from './filter.model';

$isFilterOpen.reset(changedGroupVisibility);
