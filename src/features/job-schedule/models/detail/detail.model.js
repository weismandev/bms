import { createEvent, createStore, createEffect } from 'effector';
import { createDetailBag } from '@tools/factories';

const newEntity = {
  title: '',
};

const fxGetTicket = createEffect();
const fxGetUser = createEffect();

const ticketClicked = createEvent();
const userClicked = createEvent();

const $isLoading = createStore(false);
const $error = createStore(null);
const $detailType = createStore('');

export {
  fxGetTicket,
  fxGetUser,
  ticketClicked,
  userClicked,
  $isLoading,
  $error,
  $detailType,
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
