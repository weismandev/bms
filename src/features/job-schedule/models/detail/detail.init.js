import { sample, merge } from 'effector';
import { signout } from '@features/common';
import { needTicket } from '@features/tickets/models/ticket';
import { fxGetUserById } from '@features/users/models/';
import { pageUnmounted } from '../page';
import {
  changedDetailVisibility,
  ticketClicked,
  userClicked,
  fxGetTicket,
  fxGetUser,
  $isLoading,
  $error,
  $detailType,
} from './detail.model';

const requestOccured = [fxGetTicket.pending, fxGetUser.pending];

const errorOccured = merge([fxGetTicket.failData, fxGetUser.failData]);

const clickActions = [ticketClicked, userClicked];

$isLoading.on(requestOccured, (_, pending) => pending).reset([pageUnmounted, signout]);

$error
  .on(errorOccured, (_, { error }) => error)
  .on(requestOccured, () => null)
  .reset([pageUnmounted, signout]);

// Вычисляем тип клика для отрисовки форм
$detailType
  .on(ticketClicked, () => 'ticket')
  .on(userClicked, () => 'user')
  .reset([pageUnmounted, signout]);

// При клике открываем detail
sample({
  clock: clickActions,
  fn: () => true,
  target: changedDetailVisibility,
});

// Выполняем запросы в зависимости от типа detail
sample({
  clock: ticketClicked,
  fn: (id) => ({ ticketId: id }),
  target: needTicket,
});

sample({
  clock: userClicked,
  // fn: (id) => ({ userId: id }),
  target: fxGetUserById,
});
