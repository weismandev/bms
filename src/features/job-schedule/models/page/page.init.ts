import { forward, guard, attach, combine } from 'effector';
import { debounce } from 'patronum/debounce';
import { format } from 'date-fns';
import { $isAuthenticated } from '@features/common';
import { TableParams, DefaultFilters } from '../../interfaces';
import { convertNumberToHourse } from '../../lib';
import { $filters, filtersSubmitted } from '../filter';
import {
  $tableParams,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $currentPage,
} from '../table-employee-list';
import {
  pageMounted,
  fxGetList,
  fxSaveInStorage,
  fxGetFromStorage,
  $date,
  $view,
  setDate,
  toggleView,
  updateTimeEmployeeInMilliseconds,
  updateEmployeeListByTimeout,
} from './page.model';

const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

$view.on(toggleView, (_, view) => view);
$date.on(setDate, (_, date) => date);

$view.watch((view) => {
  localStorage.setItem('jobScheduleView', view);
});

forward({
  from: debounce({
    source: fxGetList.done,
    timeout: updateTimeEmployeeInMilliseconds,
  }),
  to: updateEmployeeListByTimeout,
});

guard({
  clock: [
    pageMounted,
    toggleView,
    setDate,
    $tableParams,
    updateEmployeeListByTimeout,
    filtersSubmitted,
  ],
  source: $view,
  filter: (view: 'list' | 'diagram') => view === 'list',
  target: attach({
    source: [$date, $tableParams, $filters],
    effect: fxGetList.prepend(
      ([date, tableParams, filters]: [Date, TableParams, DefaultFilters]) => ({
        ...tableParams,
        filter: {
          building_ids: filters.building_ids.map(({ id }) => id),
          user_ids: filters.user_ids.map(({ id }) => id),
          ticket_work_status: filters.status.map(({ id }) => id),
        },
        date: `${format(date, FORMAT)}${convertNumberToHourse(date.getTimezoneOffset())}`,
      })
    ),
  }),
});

const $savedParams = combine({
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  hiddenColumnNames: $hiddenColumnNames,
});

guard({
  source: $savedParams,
  filter: $isAuthenticated,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (params) => ({ data: params }),
  }),
});

forward({
  from: pageMounted,
  to: fxGetFromStorage,
});
