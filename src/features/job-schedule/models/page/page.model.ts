import { createStore, createEvent, attach } from 'effector';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { createPageBag } from '@tools/factories';
import { employeeApi } from '../../api';

const STORAGE_KEY = 'job-schedule';

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(
  employeeApi,
  {},
  {
    idAttribute: 'user_id',
    itemsAttribute: 'data',
    createdPath: '',
    updatedPath: '',
  }
);

type View = 'list' | 'diagram';

const savedView = localStorage.getItem('jobScheduleView');
export const updateTimeEmployeeInMilliseconds = 1000 * 60 * 5;

export const $date = createStore<Date>(new Date());
export const $view = createStore<View>(
  savedView === 'list' || savedView === 'diagram' ? savedView : 'list'
);

export const toggleView = createEvent<'list' | 'diagram'>();
export const setDate = createEvent<Date>();
export const updateEmployeeListByTimeout = createEvent<void>();

export const fxSaveInStorage = attach({
  effect: fxSaveInStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
export const fxGetFromStorage = attach({
  effect: fxGetFromStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
