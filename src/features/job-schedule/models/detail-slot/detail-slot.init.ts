import { signout } from '../../../common';
import { $isOpen, toggleDetailSlotVisibility } from './detail-slot.model';

$isOpen.on(toggleDetailSlotVisibility, (state) => !state).reset(signout);
