import { Links, Meta, Status } from './general-types';

export type EmployeeDiagramPayload = {
  date: string;
  page: number;
  per_page: number;
  sorting: Sorting[];
  search: string;
};

export type EmployeeDiagramResponse = {
  data?: { slots: Slot[]; user_id: number; user_name: string }[];
  links?: Links;
  meta?: Meta;
};

export type EmployeeDiagramData = { slots: Slot[] } & Employee;

export type Slot = {
  id: number | string;
  end: string | Date;
  start: string | Date;
  status: Status;
  ticket_end?: string;
  ticket_number?: string;
  ticket_start: string;
  title?: string;
  address: string;
  complex_title: string;
  property_title: string;
  region_name: string;
  ticket_object?: Ticket;
  disabledPreview?: boolean;
  style?: Object;
};

export type Ticket = {
  address: string;
  complex_title: string;
  property_title: string;
  region_name: string;
};

export type Employee = {
  user_id: number;
  user_name: string;
};

export type TableParams = {
  page: number;
  per_page: number;
  sorting: Sorting[];
  search: string;
};

export type Sorting = {
  name: string;
  order: 'asc' | 'desc';
};
