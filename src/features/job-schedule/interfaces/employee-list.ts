import { Links, Meta, Status } from './general-types';

export type EmployeeListPayload = {
  date: string;
};

export type EmployeeListResponse = {
  data: {
    employee_tickets: EmployeeList;
    links: Links;
    meta: Meta;
  };
};

export type EmployeeList = {
  address: string | null;
  complex_title: string | null;
  fact_start_at: string;
  planned_start_at: string;
  property_title: string | null;
  region_name: string | null;
  status: Status;
  user_id: number;
  user_name: string;
  ticket_id: number;
};
