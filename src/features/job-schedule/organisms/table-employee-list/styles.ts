import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    padding: 24,
  },
}));

export default useStyles;
