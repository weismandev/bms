import React, { useMemo, Fragment } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  SortingState,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  TableColumnVisibility,
  Toolbar,
  PagingPanel,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  SortingLabel,
  TableContainer,
  TableHeaderCell,
  TableRow,
  TableCell,
  Wrapper,
} from '../../../../ui';
import { EmployeeList } from '../../interfaces';
import { ticketClicked } from '../../models/detail';
import {
  $tableData,
  columns,
  $currentPage,
  $pageSize,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $columnOrder,
  $columnWidths,
  columnOrderChanged,
  columnWidthsChanged,
  excludedColumnsFromSort,
  $rowCount,
  sortChanged,
  hiddenColumnChanged,
  $hiddenColumnNames,
} from '../../models/table-employee-list/table.model';
import { TableToolbar } from '../table-toolbar';
import useStyles from './styles';

const { t } = i18n;

type EmployeeListMap = EmployeeList & { index: number };

const Root = (props: any): JSX.Element => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {props.children}
  </Grid.Root>
);

const Row = (props: any) => {
  // @ts-ignore
  const handleClickRow = (data) => {
    // @ts-ignore
    ticketClicked(data.row.ticket_id);
  };

  const row = useMemo(
    () => <TableRow {...props} onClick={() => handleClickRow(props)} />,
    [props.children]
  );

  return row;
};

const ToolbarRoot = (props: any) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StateFormatter = ({
  value: { name, color },
}: {
  value: { name: string; color: string };
}) => <span style={{ color }}>{name}</span>;

const StatusProvider = (props: { for: string[] }) => (
  <DataTypeProvider formatterComponent={StateFormatter} {...props} />
);

const columnsExtensions: {
  columnName: string;
  align: 'left' | 'center' | 'right';
}[] = [];

const pagingPanelMessages = {
  showAll: t('ShowAll'),
  rowsPerPage: t('EntriesPerPage'),
  info: (params: { count: number; from: number; to: number }) =>
    `${params.from} - ${params.to} из ${params.count}`,
};

const emptyMessageComponent = () => <div />;

const generateRowId = (row: EmployeeListMap) =>
    row.ticket_id + row.user_id + (row.address?.length || 0) + row.index;

export const TableEmployeeList: React.FC = () => {
  const { wrapper } = useStyles();
  const tableData: EmployeeList[] = useStore($tableData);
  const totalCount = useStore($rowCount);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnOrder = useStore($columnOrder);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  const hasLeastOneVisibleColumn = columns.length !== hiddenColumnNames.length;
  const data: EmployeeListMap[] = tableData.map((item, index) => ({ ...item, index }));

  return (
    <Wrapper className={wrapper}>
      <Grid rootComponent={Root} rows={data} getRowId={generateRowId} columns={columns}>
        <SortingState
          sorting={[{ columnName: '', direction: 'asc' }]}
          onSortingChange={sortChanged}
        />

        <DragDropProvider />

        <StatusProvider for={['status']} />

        <DXTable
          columnExtensions={columnsExtensions}
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnVisibility
          data-test-id="hide-columns-btn"
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
          emptyMessageComponent={emptyMessageComponent}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <TableHeaderRow cellComponent={TableHeaderCell} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        {hasLeastOneVisibleColumn && (
          <PagingPanel pageSizes={[10, 25, 50, 100]} messages={pagingPanelMessages} />
        )}
      </Grid>
    </Wrapper>
  );
};
