import React from 'react';
import { format } from 'date-fns';
import LaunchIcon from '@mui/icons-material/Launch';
import i18n from '@shared/config/i18n';
import { Modal } from '@ui/index';
import HouseIcon from '../../../../img/house';
import ObjectIcon from '../../../../img/object';
import { Slot } from '../../interfaces';
import { getStatus } from '../../lib';
import { ticketClicked } from '../../models/detail';
import useStyles from './styles';

const { t } = i18n;

type DetailSlotType = {
  isOpen: boolean;
  handleClose: () => void;
  positon: { left: number; top: number };
  slot: Slot;
};

const DetailSlot: React.FC<DetailSlotType> = ({
  isOpen,
  handleClose,
  positon,
  slot,
}): JSX.Element => {
  const {
    root,
    header,
    header__titleRow,
    header__title,
    header__launch,
    header__desc,
    content,
    status,
    info,
    footer,
  } = useStyles();

  const dateStart = format(new Date(slot.start), 'HH:mm');
  const dateEnd = format(new Date(slot.end), 'HH:mm');
  const statusInfo = getStatus(slot.status);

  const bodyElement = document.body;
  // eslint-disable-next-line prefer-const
  let { top, left } = positon;
  if (top + 600 > bodyElement.scrollHeight) {
    top = bodyElement.scrollHeight - 420;
  }

  return (
    <Modal
      classes={{ root }}
      isOpen={isOpen}
      onClose={handleClose}
      style={{ top, left }}
      header={
        <div className={header}>
          <div className={header__titleRow}>
            <h1
              className={header__title}
              onClick={(event: React.MouseEvent<HTMLElement>) => {
                handleClose();
                //@ts-ignore
                ticketClicked(slot.ticket_id);
              }}
            >
              {slot.title}
            </h1>
            <LaunchIcon className={header__launch} />
          </div>

          <span className={header__desc}>{slot.ticket_number}</span>
        </div>
      }
      content={
        <div className={content}>
          <div style={{ backgroundColor: statusInfo.color }} className={status}>
            {statusInfo.name}
          </div>
          <div className={info}>
            <HouseIcon />
            <p>{slot.ticket_object?.address}</p>
          </div>
          <div className={info}>
            <ObjectIcon />
            <p>{slot.property_title}</p>
          </div>
        </div>
      }
      actions={
        <div className={footer}>
          <div>
            <p>{t('StartWorkTime')}</p>
            <span>{dateStart}</span>
          </div>
          <div>
            <p>{t('FinishWorkTime')}</p>
            <span>{dateEnd}</span>
          </div>
        </div>
      }
    />
  );
};

export { DetailSlot };
