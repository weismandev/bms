import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  tooltipContainer: {
    display: 'flex',
  },
  root: {
    width: 67,
    height: 34,
    padding: 0,
    borderRadius: 50,
    marginLeft: 30,
    border: '1px solid #E7E7EC',
  },
  label: {
    color: 'rgb(147, 147, 147)',
    fontSize: '18px',
    fontWeight: 400,
  },
  switchBase: {
    backgroundColor: '#0394E3',
    padding: 5,
    '&:hover': {
      backgroundColor: '#0394E3',
    },
  },
  thumb: {
    borderRadius: 50,
  },
  track: {
    backgroundColor: 'white !important',
  },
  checked: {
    backgroundColor: '#0394E3 !important',
    color: 'white !important',
    transform: 'translateX(30px) !important',

    '&:hover': {
      backgroundColor: '#0394E3 !important',
      color: 'white !important',
      transform: 'translateX(30px) !important',
    },
  },
});

export default useStyles;
