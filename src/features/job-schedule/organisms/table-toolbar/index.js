import { useStore } from 'effector-react';
import { Switch, FormControlLabel } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  FilterButton,
  Greedy,
  Toolbar,
  AdaptiveSearch,
  DayControl,
} from '../../../../ui';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter';
import { toggleView, setDate, $view, $date } from '../../models/page/page.model';
import {
  $rowCount as $rowCountSlot,
  $search as $searchDiagram,
  searchChanged as searchChangedDiagram,
} from '../../models/table-employee-diagram';
import {
  $rowCount as $rowCountList,
  $search as $searchList,
  searchChanged as searchChangedList,
} from '../../models/table-employee-list';
import useStyles from './styles';

const { t } = i18n;

function TableToolbar() {
  const classes = useStyles();
  const view = useStore($view);
  const date = useStore($date);
  const rowCountList = useStore($rowCountList);
  const rowCountSlot = useStore($rowCountSlot);

  const totalCount = view === 'list' ? rowCountList : rowCountSlot;
  const isFilterOpen = useStore($isFilterOpen);
  const searchValueList = useStore($searchList);
  const searchValueDiagram = useStore($searchDiagram);

  const mode = 'view'; // useStore($mode);
  const isEditMode = mode === 'edit';
  const isAnySideOpen = isFilterOpen || isEditMode;
  const viewDiagram = view === 'diagram';
  const searchValue = viewDiagram ? searchValueDiagram : searchValueList;
  const searchChanged = viewDiagram ? searchChangedDiagram : searchChangedList;

  const handleChangeViewCalendar = () => {
    toggleView(viewDiagram ? 'list' : 'diagram');
  };

  const handleDayChange = ({ today }) => {
    setDate(today);
  };

  const handleFilterVisibility = () => {
    changedFilterVisibility(true);
  };

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={handleFilterVisibility}
        className={classes.margin}
      />
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <div style={{ width: 300, marginRight: 20 }}>
        <DayControl value={date} onChange={handleDayChange} />
      </div>
      <Greedy />

      <FormControlLabel
        control={<Switch checked={viewDiagram} />}
        onChange={handleChangeViewCalendar}
        label={t('GraphicView')}
        classes={{
          label: classes.label,
        }}
      />
    </Toolbar>
  );
}

export { TableToolbar };
