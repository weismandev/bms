import makeStyles from '@mui/styles/makeStyles';

/*
  @COLORS
  white: '#F4F7FA';
  grey: '#AAAAAA';
  green: '#1BB169;';
  orange: '#EC7F00';
  yellow: '#E4C000';
  red: '#EB5757';
  darkGrey: '#65657B';
*/

const useStyles = makeStyles(() => ({
  wrapper: {
    display: 'grid',
    gridTemplateColumns: 'repeat(96, 20.5px)',
    gridAutoRows: '42px',
    backgroundColor: 'white',
    // overflow: 'auto',

    '& > div': {
      display: 'flex',
      flexDirection: 'column',
      fontWeight: 500,
      color: '#65657B',
      borderRight: '1px solid #E7E7EC',
      position: 'relative',
      borderBottom: '1px solid white',
      textAlign: 'left',
      overflow: 'hidden',

      '&:nth-child(-n+24)': {
        lineHeight: 1,
        paddingTop: 5,
        textAlign: 'center',
      },

      '& > :first-child': {
        fontSize: 18,
      },
      '& > :last-child': {
        fontSize: 14,
      },
    },
    /* нет заявки */
    '& > .white': {
      backgroundColor: 'rgb(225 235 255 / 30%)',
      padding: '11px 16px',
      textAlign: 'left',

      '& > .content > p': {
        fontStyle: 'italic',
        fontWeight: 300,
        fontSize: 14,
        color: '#65657B',
      },

      '& > .resizeble': {
        backgroundColor: '#35A9E9',
      },
    },
    /* Запланировано */
    '& > .planned': {
      backgroundColor: 'rgb(170 170 170 / 30%)',
      fontStyle: 'italic',
      fontWeight: 300,
      fontSize: 14,
      color: '#AAAAAA',
      padding: '4px 0px 0px 16px',
      textAlign: 'left',

      '& > .resizeble': {
        backgroundColor: '#AAAAAA',
      },
    },
    /* Выполнено */
    '& > .completed_on_time': {
      color: '#1BB169',
      backgroundColor: 'rgb(27 177 105 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#1BB169',
      },
    },
    /* Не выполнено */
    '& > .skipped': {
      color: '#EB5757',
      backgroundColor: 'rgb(235 87 87 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#EB5757',
      },
    },
    /* Отставание */
    '& > .late': {
      color: '#EC8A33',
      backgroundColor: 'rgb(236 127 0 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#EC8A33',
      },
    },
    /* Опоздание */
    '& > .completed_with_late': {
      color: '#E4C000',
      backgroundColor: 'rgb(228 192 0 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#E4C000',
      },
    },
    /* Перерыв */
    '& > .break': {
      color: '#7D7D8E',
      backgroundColor: 'rgb(231 231 236 / 70%);',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#7D7D8E',
      },
    },
    /* Отпуск */
    '& > .vacation': {
      color: '#65657B',
      backgroundColor: 'rgb(101 101 123 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#65657B',
      },
    },
    /* Нерабочее время / Отпуск / Обед / перерыв */
    '& > .not_working_time': {
      color: '#65657B',
      backgroundColor: 'rgb(101 101 123 / 30%)',
      padding: '4px 0px 0px 16px',

      '& > .resizeble': {
        backgroundColor: '#65657B',
      },
    },
    '& .content': {
      display: 'flex',
      flexDirection: 'column',
      textAlign: 'left',

      '& > p': {
        margin: 0,
        padding: 0,
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 14,
        lineHeight: '18px',
      },
    },
    '& .resizeble': {
      height: '100%',
      width: 4,
      position: 'absolute',
      left: 0,
      top: 0,
      cursor: 'col-resize',
    },
  },
  '@keyframes load': {
    from: { left: 0 },
    to: { left: '90%' },
  },
  line: {
    float: 'left',
    width: '140px',
    height: '42px',
    marginTop: '-12px',
    borderRadius: '7px',
    backgroundImage: 'linear-gradient(90deg, #f6f9ff 0px, #ffffff 40px, #f6f9ff 80px)',
    position: 'absolute',
    animation: '$load 5s cubic-bezier(0.4, 0.0, 0.2, 1) infinite',
  },

  columnEmployee: {
    minWidth: 200,
    textAlign: 'left',
    position: 'sticky',
    left: 0,
    zIndex: 2,

    '& > div': {
      fontWeight: 500,
      fontSize: 14,
      color: '#767676',
      height: 42,
      padding: '10px 0',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      backgroundColor: 'white',
      borderRight: '1px solid #E7E7EC',
      width: 200,
    },

    '& > .columnEmployeeHeader': {
      fontWeight: 900,
      fontSize: 18,
      color: '#7D7D8E',
      borderBottom: '1px solid #E7E7EC',
      height: 43,
      padding: '10px 0px',
    },
  },

  wrapperPageHeader: {
    display: 'flex',
    border: 0,
    position: 'sticky',
    top: 0,
    zIndex: 3,
  },

  wrapperPage: {
    display: 'flex',
    border: 0,
    height: '77vh',

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.2)',
      borderRadius: 6,
    },
  },
  user: {
    cursor: 'pointer',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  paginationWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    boxShadow: 'inset 0px 5px 15px -10px #DCDCDC',
    position: 'relative',
    width: 'calc(100% + 48px)',
    left: '-24px',
    bottom: '-24px',
    zIndex: 2,
    padding: '12px 24px 0px',
    top: 0,

    '& > div': {
      width: '50%',
    },
  },
  pagination: {
    display: 'flex',
    flexWrap: 'nowrap',
    paddingLeft: '26px',
    minHeight: '10px',

    '& > ul': {
      // position: 'absolute',
      // top: '40px',

      '& button': {
        width: '20px',
        height: '36px',
        fontWeight: 600,
      },
    },
  },
}));

export default useStyles;
