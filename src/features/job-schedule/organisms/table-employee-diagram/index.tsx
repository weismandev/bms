import React, { Fragment, useState, useEffect } from 'react';
import { useStore } from 'effector-react';
import { Pagination, Tooltip } from '@mui/material';
import { PaginationItem } from '@mui/lab';
import { Template } from '@devexpress/dx-react-core';
import { Grid, Toolbar } from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { Loader, Wrapper, CustomScrollbar } from '../../../../ui';
import { Slot } from '../../interfaces';
import { userClicked } from '../../models/detail';
import { $isOpen, toggleDetailSlotVisibility } from '../../models/detail-slot';
import {
  $slots,
  $employee,
  $isLoading,
} from '../../models/table-employee-diagram/table.model';
import {
  $currentPage,
  $pageSize,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $rowCount,
} from '../../models/table-employee-list/table.model';
import { ColumnSelection } from '../../molecules';
import { DetailSlot } from '../detail-slot';
import { TableToolbar } from '../table-toolbar';
import Header from './header';
import useStyles from './styles';

const initSlot: Slot = {
  id: '',
  end: new Date().toISOString(),
  start: new Date().toISOString(),
  status: 'white',
  ticket_end: '',
  ticket_number: '',
  ticket_start: '',
  title: '',
  address: '',
  complex_title: '',
  property_title: '',
  region_name: '',
  ticket_object: {
    address: '',
    complex_title: '',
    property_title: '',
    region_name: '',
  },
};

/*
  @COLORS
  white: '#F4F7FA';
  grey: '#AAAAAA';
  green: '#1BB169;';
  orange: '#EC7F00';
  yellow: '#E4C000';
  red: '#EB5757';
  darkGrey: '#65657B';
*/

const Root = (props: any) => <Grid.Root {...props} />;

const ToolbarRoot = (props: any) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const { t } = i18n;

export const TableEmployeeDiagram: React.FC = () => {
  const classes = useStyles();

  const slots = useStore($slots);
  const employee = useStore($employee);
  const isLoading = useStore($isLoading);
  const isOpenDetailSlot = useStore($isOpen);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);

  const countPage = totalCount ? Math.ceil(totalCount / pageSize) : 0;

  const [sectionHeight, setSectionHeight] = useState<number>(0);
  const [emptySlots, setEmptySlots] = useState<number[]>([0]);
  const [slot, setSlot] = useState<Slot>(initSlot);
  const [positon, setPosition] = useState<{ left: number; top: number }>({
    left: 0,
    top: 0,
  });

  const getEmptySlots = (length: number, height: number): number[] => {
    const count = Math.floor((height - 200) / 42);
    const diff = count - length;
    return new Array(diff > 0 ? diff : 0).fill(0);
  };

  const handleClickSlot = (row: Slot) => (event: React.MouseEvent<HTMLElement>) => {
    if (row.disabledPreview) return;
    if (!isOpenDetailSlot && row.ticket_number) {
      const clientWidth = document.body.clientWidth;
      const widthPreviewSlot = 600;
      const isShift = event.pageX + widthPreviewSlot > clientWidth;
      const left = isShift ? clientWidth - widthPreviewSlot : event.pageX;
      const top = event.pageY;
      setPosition({ left, top });
      setSlot(row);
      toggleDetailSlotVisibility(true);
    }
  };

  const handleCloseDetailSlot = () => toggleDetailSlotVisibility(false);
  const handleClickByEmployee = (id: number | string) => () => userClicked(id as any);

  useEffect(() => {
    const sectionElement = document.querySelectorAll('section');
    setSectionHeight(sectionElement[0].offsetHeight);
  }, []);

  useEffect(() => {
    setEmptySlots(getEmptySlots(slots.length, sectionHeight));
  }, [slots.length, isLoading, pageSize]);

  return (
    <Fragment>
      <Wrapper
        style={{
          height: '100%',
          padding: 24,
          display: 'flex',
          flexDirection: 'column-reverse',
          justifyContent: 'flex-end',
          overflow: 'hidden',
          paddingBottom: 12,
        }}
      >
        <Loader isLoading={isLoading} />
        <DetailSlot
          isOpen={isOpenDetailSlot}
          handleClose={handleCloseDetailSlot}
          positon={positon}
          slot={slot}
        />
        <Grid rootComponent={Root} rows={[]} getRowId={(row) => row.id} columns={[]}>
          <Toolbar rootComponent={ToolbarRoot} />
          <Template name="toolbarContent">
            <TableToolbar />
          </Template>
          <div className={classes.paginationWrap}>
            <ColumnSelection pageSize={pageSize} pageChanged={pageSizeChanged} />
            <Pagination
              onChange={(_, page) => pageNumberChanged(page - 1)}
              page={currentPage + 1}
              count={countPage}
              classes={{ root: classes.pagination }}
              size="small"
              shape="rounded"
              renderItem={(item) => <PaginationItem {...item} />}
            />
          </div>

          <CustomScrollbar autoHide style={{ zIndex: 3 }}>
            <div className={classes.wrapperPageHeader}>
              <div className={classes.columnEmployee}>
                <div className="columnEmployeeHeader">{t('NameEmployee')}</div>
              </div>
              <div className={classes.wrapper}>
                <Header />
              </div>
            </div>

            <div className={classes.wrapperPage}>
              <div className={classes.columnEmployee}>
                {employee.map((item) => (
                  <div
                    key={item.user_id}
                    className={classes.user}
                    onClick={handleClickByEmployee(item.user_id)}
                  >
                    {item.user_name}
                  </div>
                ))}
                {emptySlots.map((slot, index) => (
                  <div key={index} className={classes.user} />
                ))}
              </div>
              {/* <div style={{ overflow: 'hidden' }}> */}
              {/* <CustomScrollbar autoHide> */}
              <div className={classes.wrapper}>
                {slots.map((row) =>
                  row.map(({ gridColumnStart, gridColumnEnd, ...item }) => {
                    const { style = {} } = item;
                    const isSmallSlot = gridColumnEnd - gridColumnStart <= 1;
                    return isSmallSlot ? (
                      <Tooltip title={item.title || ''}>
                        <div
                          className={item.status}
                          key={item.id}
                          style={{ ...style, gridColumnStart, gridColumnEnd }}
                          onClick={handleClickSlot(item)}
                        >
                          <div className="content" />
                          {item.status !== 'white' && <span className="resizeble" />}
                        </div>
                      </Tooltip>
                    ) : (
                      <div
                        className={item.status}
                        key={item.id}
                        style={{ ...style, gridColumnStart, gridColumnEnd }}
                        onClick={handleClickSlot(item)}
                      >
                        <div className="content">
                          <p>{item.ticket_number}</p>
                          <p>{item.title}</p>
                        </div>
                        {item.status !== 'white' && <span className="resizeble" />}
                      </div>
                    );
                  })
                )}
                {slots[0]?.length === 0 && (
                  <div
                    className="white"
                    style={{ gridColumnStart: 1, gridColumnEnd: 98 }}
                  />
                )}
                {emptySlots.map((slot, index) => (
                  <div
                    key={index}
                    className="white"
                    style={{ gridColumnStart: 1, gridColumnEnd: 98 }}
                  />
                ))}
              </div>
              {/* </CustomScrollbar> */}
              {/* </div> */}
            </div>
          </CustomScrollbar>
        </Grid>
      </Wrapper>
    </Fragment>
  );
};
