import React from 'react';

const mapStep = () => {
  let start,
    end = 1;
  return (_: '', index: number) => {
    start = end;
    end += 4;
    return (
      <div key={index} style={{ gridColumn: `${start} / ${end}` }}>
        <span>{`${String('0' + index).slice(-2)}`}</span>
        <span>:00</span>
      </div>
    );
  };
};

const Header: React.FC = () => <>{new Array(24).fill('').map(mapStep())}</>;

export default Header;
