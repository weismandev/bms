import { api } from '../../../api/api2';

const getList = (payload) =>
  api.v1('get', 'parking/enterprise_rent/enterprise-slots', {
    type: 'all',
    ...payload,
  });

export { getList };
