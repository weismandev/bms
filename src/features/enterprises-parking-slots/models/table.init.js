import { mergeColumnWidths } from '../../../tools/merge-column-widths';
import { signout } from '../../common';
import { fxGetFromStorage } from './page.model';
import {
  $currentPage,
  $pageSize,
  $columnWidths,
  $columnOrder,
  $hiddenColumnNames,
  $currentCompany,
} from './table.model';

$currentCompany.reset(signout);

$currentPage.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.currentPage : state
);

$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  result ? Array.from(new Set([...result.columnOrder, ...state])) : state
);

$columnWidths.on(fxGetFromStorage.doneData, (state, data) =>
  mergeColumnWidths(state, data && data.columnWidths)
);

$pageSize.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.pageSize : state
);

$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.hiddenColumnNames : state
);
