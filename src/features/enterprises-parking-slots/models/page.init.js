import { sample, guard, attach, combine, forward } from 'effector';
import { $isAuthenticated } from '../../common';
import { pageMounted, fxGetList, fxSaveInStorage, fxGetFromStorage } from './page.model';
import {
  $currentCompany,
  $tableParams,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $currentPage,
} from './table.model';

guard({
  source: sample({
    source: {
      table: $tableParams,
      company: $currentCompany,
    },
    fn: ({ table, company, filters }) => {
      return {
        enterprise_id: company && company.id,
        page: table.page,
        per_page: table.per_page,
      };
    },
  }),
  filter: ({ enterprise_id }) => Boolean(enterprise_id),
  target: fxGetList,
});

const $savedParams = combine({
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  hiddenColumnNames: $hiddenColumnNames,
});

guard({
  source: $savedParams,
  filter: $isAuthenticated,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (params) => ({ data: params }),
  }),
});

forward({
  from: pageMounted,
  to: fxGetFromStorage,
});
