import { createEvent, restore } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { formatPhone } from '../../../tools/formatPhone';
import { formatParkSlotTitle } from '../../enterprises';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'title', title: t('Name') },
  { name: 'type', title: t('OwnershipType') },
  { name: 'starts_at', title: t('RentFrom') },
  { name: 'ends_at', title: t('RentTo') },
  { name: 'employee', title: t('AssignedTo') },
  { name: 'vehicle', title: t('transport') },
  { name: 'phone', title: t('Label.phone') },
];

export const changeCompany = createEvent();
export const $currentCompany = restore(changeCompany, '');

export const $tableData = $raw.map(({ data }) => data.map(formatTableData));

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths: [{ columnName: 'title', width: 500 }] });

function formatTableData(data) {
  const { id, type, employee, rent, vehicles, rent_id, ...rest } = data;

  return {
    id,
    rent_id,
    title: formatParkSlotTitle(rest),
    type: type === 'ownership' ? t('property') : t('rental'),
    starts_at: getFormattedDateStub(rent && rent.starts_at),
    ends_at: getFormattedDateStub(rent && rent.ends_at),
    employee: employee?.fullname || '-',
    phone: employee?.phone ? formatPhone(employee.phone) : '-',
    vehicle: Array.isArray(vehicles) ? vehicles : [],
  };
}

function getFormattedDateStub(date) {
  if (date) {
    return format(new Date(date), 'dd.MM.yyyy');
  }

  return t('isNotDefinedit');
}
