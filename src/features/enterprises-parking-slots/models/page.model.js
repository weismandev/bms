import { attach } from 'effector';
import { createPageBag } from '../../../tools/factories';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '../../common';
import * as parkingSlotsApi from '../api';

export const {
  PageGate,
  pageUnmounted,
  pageMounted,

  fxGetList,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(parkingSlotsApi, {}, { idAttribute: 'id', itemsAttribute: 'slots' });

const STORAGE_KEY = 'enterprises-parking-slots';

export const fxSaveInStorage = attach({
  effect: fxSaveInStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
export const fxGetFromStorage = attach({
  effect: fxGetFromStorageBase,
  mapParams: (params) => ({ ...params, storage_key: STORAGE_KEY }),
});
