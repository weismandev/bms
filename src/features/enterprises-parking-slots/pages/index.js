import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../models';
import { Table } from '../organisms';

const EnterprisesParkingSlotsPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout main={<Table />} />
    </>
  );
};

const RestrictedEnterprisesParkingSlotsPage = (props) => (
  <HaveSectionAccess>
    <EnterprisesParkingSlotsPage />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesParkingSlotsPage as EnterprisesParkingSlotsPage };
