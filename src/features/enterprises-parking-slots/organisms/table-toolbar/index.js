import { combine } from 'effector';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Toolbar, FoundInfo } from '../../../../ui';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select';
import { $rowCount } from '../../models/page.model';
import { changeCompany, $currentCompany } from '../../models/table.model';

const $stores = combine({
  totalCount: $rowCount,
  currentCompany: $currentCompany,
});

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
});

const { t } = i18n;

function TableToolbar(props) {
  const { totalCount, currentCompany } = useStore($stores);

  const classes = useStyles();

  return (
    <Toolbar style={{ alignItems: 'flex-end' }}>
      <FoundInfo count={totalCount} className={classes.margin} />
      <div style={{ marginLeft: 10, width: 300 }}>
        <EnterprisesCompaniesSelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={false}
          placeholder={t('ChooseCompany')}
          firstAsDefault
        />
      </div>
    </Toolbar>
  );
}

export { TableToolbar };
