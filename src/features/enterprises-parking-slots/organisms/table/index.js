import { useMemo, memo } from 'react';
import { useStore } from 'effector-react';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  VehiclePlate,
  TableColumnVisibility,
  PagingPanel,
} from '@ui';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  columns,
  $rowCount,
} from '../../models';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const isSelected = false;
  const onRowClick = () => null;

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const VehicleFormatter = ({ value }) => {
  const vehiclesList = value.map(({ licence_plate, brand }, idx) => {
    const licensePlate =
      Boolean(licence_plate) && typeof licence_plate === 'string' ? licence_plate : ' - ';
    const brandName = Boolean(brand) && typeof brand === 'string' ? brand : '';

    return <VehiclePlate key={idx} number={licensePlate} brand={brandName} />;
  });

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'repeat(auto-fill, 1fr)',
        gridGap: 3,
        padding: 4,
      }}
    >
      {vehiclesList}
    </div>
  );
};
const VehicleProvider = (props) => (
  <DataTypeProvider formatterComponent={VehicleFormatter} {...props} />
);

const Table = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const count = useStore($rowCount);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.rent_id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <DragDropProvider />

        <VehicleProvider for={['vehicle']} />

        <CustomPaging totalCount={count} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
