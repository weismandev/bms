import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { ticketWorkStatusApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(ticketWorkStatusApi.getList);

$data.on(fxGetList.done, (_, { result }) => formatResponse(result)).reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, pending) => pending).reset(signout);

function formatResponse(response = []) {
  return response.map(({ slug, name }) => ({ id: slug, title: name }));
}

export { $data, $error, $isLoading, fxGetList };
