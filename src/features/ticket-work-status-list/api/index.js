import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v4('get', 'schedule/ticket-work-status-list');

export const ticketWorkStatusApi = { getList };
