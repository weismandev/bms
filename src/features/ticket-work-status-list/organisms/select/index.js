import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { makeStyles } from '@mui/styles';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $error, $isLoading, fxGetList } from '../../models';

export const useStyles = makeStyles({
  wrapper: {
    zIndex: 2,
    background: 'red',
  },
  select: {
    zIndex: 2,
    marginBottom: 10,
  },
});

const TicketWorkStatusSelect = (props) => {
  const { complex = [], include = [], firstAsDefault = false, params = {} } = props;
  const classes = useStyles();
  let options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  options = complex.length
    ? options.filter((build) => complex.find(({ id }) => id === build.complex.id))
    : options;

  options = include.length > 0 ? options.filter((i) => include.includes(i.id)) : options;

  const alreadyLoaded = options.length;

  useEffect(() => {
    if (!isLoading && !alreadyLoaded) {
      fxGetList(params);
    }
  }, []);

  useEffect(() => {
    if (!props.value && firstAsDefault && options.length > 0) {
      props.onChange(options[0]);
    }
  }, [options.length]);

  return (
    <SelectControl
      isLoading={isLoading}
      error={error}
      options={options}
      {...props}
      classes={{ item: classes.select }}
    />
  );
};

const TicketWorkStatusSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<TicketWorkStatusSelect />}
      onChange={props.onChange || onChange}
      {...props}
    />
  );
};

export { TicketWorkStatusSelect, TicketWorkStatusSelectField };
