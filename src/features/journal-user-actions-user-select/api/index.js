import { api } from '../../../api/api2';

const getUsers = () => {
  return api.v1('get', 'journal/filter/get-user-list');
};

export const journalUserActionsSelectApi = {
  getUsers,
};
