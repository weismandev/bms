import { createStore, createEffect, forward } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { camerasProtocolsApi } from '../api';

const ProtocolsGate = createGate();
const { open, close } = ProtocolsGate;

const fxGetProtocolsLIst = createEffect(camerasProtocolsApi.getProtocolsList);

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$error.on(fxGetProtocolsLIst.fail, (_, { error }) => error).reset([close, signout]);

$isLoading.on(fxGetProtocolsLIst.pending, (_, result) => result).reset([close, signout]);

$data.on(fxGetProtocolsLIst.doneData, (_, data) => data).reset([close, signout]);

forward({
  from: open,
  to: fxGetProtocolsLIst,
});

export { $data, ProtocolsGate, $error, $isLoading };
