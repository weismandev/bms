import { api } from '../../../api/api2';

const getProtocolsList = () => api.v1('get', 'cameras/get-available-types');

export const camerasProtocolsApi = { getProtocolsList };
