import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $error, $isLoading, ProtocolsGate } from '../../model';

const CameraProtocolSelect = (props) => {
  const options = useStore($data);
  const error = useStore($error);
  const isLoading = useStore($isLoading);

  return (
    <>
      <ProtocolsGate />
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
    </>
  );
};

const CameraProtocolSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<CameraProtocolSelect />} onChange={onChange} {...props} />
  );
};

export { CameraProtocolSelect, CameraProtocolSelectField };
