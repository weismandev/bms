import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const ParkingAvailableSlotSelect = (props) => {
  const { type, starts_at, ends_at } = props;
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  useEffect(() => {
    if (type === 'rent') {
      fxGetList({ rent: { starts_at, ends_at }, type });
    } else {
      fxGetList();
    }
  }, [type, starts_at, ends_at]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const ParkingAvailableSlotSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ParkingAvailableSlotSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { ParkingAvailableSlotSelect, ParkingAvailableSlotSelectField };
