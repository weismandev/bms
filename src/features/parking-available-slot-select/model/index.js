import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { getList } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result.slots)) {
      return [];
    }

    return result.slots.map((i) => ({
      ...i,
      id: i.id,
      title: formatTitle(i),
    }));
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(getList);

export { fxGetList, $data, $error, $isLoading };

function formatTitle({ number, lot, zone }) {
  const number_ = number || '-';
  const lotTitle = (Boolean(lot) && lot.title) || 'Парковка';
  const zoneTitle = Boolean(zone) && zone.title ? `> ${zone.title}` : '';

  return `[ ${number_} ] ${lotTitle} ${zoneTitle}`;
}
