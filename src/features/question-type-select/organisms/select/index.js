import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $types, $isLoading, $error, SelectGate } from '../../model';

const QuestionTypeSelect = (props) => {
  const options = useStore($types);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
      <SelectGate />
    </>
  );
};

const QuestionTypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<QuestionTypeSelect />} onChange={onChange} {...props} />
  );
};

export { QuestionTypeSelect, QuestionTypeSelectField };
