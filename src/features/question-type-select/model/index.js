import { createEffect, createStore, guard, sample, attach } from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { typesApi } from '../api';

const fxGetTypes = createEffect();

const $types = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

const SelectGate = createGate();

fxGetTypes.use(typesApi.get);

$types
  .on(fxGetTypes.done, (state, { result }) => {
    if (!result || !result.items) {
      return [];
    }

    return result.items;
  })
  .reset(signout);

$error.on(fxGetTypes.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetTypes.pending, (state, result) => result).reset(signout);

guard({
  source: sample($types, SelectGate.open),
  filter: (types) => types.length === 0,
  target: attach({ effect: fxGetTypes, mapParams: (params) => ({}) }),
});

export { fxGetTypes, $types, $error, $isLoading, SelectGate };
