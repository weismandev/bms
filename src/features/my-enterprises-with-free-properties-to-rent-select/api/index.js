import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'enterprises/crm/resident/list-with-owned-properties', payload);

export const enterprisesApi = { getList };
