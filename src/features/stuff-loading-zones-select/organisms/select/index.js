import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, fxGetList, $isLoading, $error } from '../../model';

const StuffLoadingZonesSelect = (props) => {
  const { building_id, starts_at, ends_at } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (building_id && starts_at && ends_at) {
      fxGetList({ building_id, starts_at, ends_at });
    }
  }, [building_id, starts_at, ends_at]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const StuffLoadingZonesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<StuffLoadingZonesSelect />} onChange={onChange} {...props} />
  );
};

export { StuffLoadingZonesSelect, StuffLoadingZonesSelectField };
