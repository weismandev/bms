import { createEffect, createStore, createEvent, guard, sample, attach } from 'effector';
import { signout } from '../../common';
import { parkingApi } from '../api';

const loadData = createEvent();

const fxGetList = createEffect();

const $data = createStore([]);
const $currentDataLot = createStore(null);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.zones) {
      return [];
    }

    return result.zones;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);
$currentDataLot.on(fxGetList.done, (state, { params }) => params.lot_id).reset(signout);

fxGetList.use(parkingApi.getList);

guard({
  source: sample({
    source: { data: $data, isLoading: $isLoading, current: $currentDataLot },
    clock: loadData,
    fn: (data, payload) => ({ ...data, ...payload }),
  }),
  filter: ({ data, isLoading, current, lot_id }) => {
    return !isLoading && lot_id !== current;
  },
  target: attach({
    effect: fxGetList,
    mapParams: ({ lot_id }) => ({ lot_id }),
  }),
});

export { fxGetList, $data, $error, $isLoading, loadData };
