import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, loadData } from '../../model';

const ParkingZoneSelect = (props) => {
  const { lot_id } = props;
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    loadData({ lot_id });
  }, [lot_id]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const ParkingZoneSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ParkingZoneSelect />} onChange={onChange} {...props} />;
};

export { ParkingZoneSelect, ParkingZoneSelectField };
