import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'parking/zones/list', { ...payload, per_page: 1000 });

export const parkingApi = { getList };
