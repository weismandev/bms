import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useGate, useStore } from 'effector-react';
import { ErrorBoundary } from '@features/error';
import { ChatDrawer } from '@features/socket-chats-reworked/components/chat/chat-drawer';
import { ChatFooter } from '@features/socket-chats-reworked/components/chat/chat-footer';
import { ChatHeader } from '@features/socket-chats-reworked/components/chat/chat-header';
import { ChatLoaderScreen } from '@features/socket-chats-reworked/components/chat/chat-loader-screen';
import { ChatNotExist } from '@features/socket-chats-reworked/components/chat/chat-not-exist';
import { MessagesList } from '@features/socket-chats-reworked/components/messages/messages-list';
import { SocketGuard } from '@features/socket-chats-reworked/components/socket-guard';
import { ChatPanelProps } from '@features/socket-chats-reworked/types/common';
import { ActionButton } from '@ui/atoms';
import { Modal } from '@ui/molecules/modal';
import {
  $chatData,
  $confirmDeleteMessagesStatus,
  $isChatLoading,
  ChatGate,
  changeConfirmDeleteMessagesStatus,
  deleteMessages,
} from '../../models';
import { useStyles } from './styles';

export const ChatPanel: FC<ChatPanelProps> = ({
  channelId,
  readonly = false,
  standalone,
  token,
  hideLeaveButton = false,
}) => {
  const { t } = useTranslation();
  useGate(ChatGate, { channelId, readonly });

  const chatData = useStore($chatData);
  const isChatLoading = useStore($isChatLoading);
  const classes = useStyles({ standalone });
  const confirmDeleteMessagesStatus = useStore($confirmDeleteMessagesStatus);

  if (!chatData?.contact_id && !channelId) return <ChatNotExist />;

  if (isChatLoading) return <ChatLoaderScreen />;

  const actions = () => (
    <div className={classes.modal_actions}>
      <ActionButton onClick={deleteMessages}>{t('remove')}</ActionButton>
      <ActionButton
        onClick={() => changeConfirmDeleteMessagesStatus(false)}
        kind="negative"
      >
        {t('Cancellation')}
      </ActionButton>
    </div>
  );

  return (
    <div className={classes.chatPanel}>
      <ErrorBoundary>
        <Modal
          isOpen={confirmDeleteMessagesStatus}
          header={t('ActionConfirmation')}
          content={t('confirmMessageDelete')}
          onClose={() => changeConfirmDeleteMessagesStatus(false)}
          actions={actions()}
        />

        <SocketGuard token={token}>
          <div className={classes.chatPanel__content}>
            <ChatDrawer />
            <ChatHeader hideLeaveButton={hideLeaveButton} />
            <MessagesList />
            <ChatFooter />
          </div>
        </SocketGuard>
      </ErrorBoundary>
    </div>
  );
};
