import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  chatPanel: ({ standalone }: { standalone: ChatsChatPanel['standalone'] }) => ({
    position: 'relative',
    display: 'grid',
    height: '100%',
    background: 'white',
    zIndex: 2,
    overflow: 'hidden',
    borderRadius: standalone ? 12 : '0 12px 12px 0',
  }),
  chatPanel__content: ({ standalone }: { standalone: ChatsChatPanel['standalone'] }) => ({
    display: 'grid',
    gridTemplateRows: 'auto 1fr auto',
    height: '100%',
    // gap: 24,
  }),
  modal_actions: ({ standalone }: { standalone: ChatsChatPanel['standalone'] }) => ({
    display: 'flex',
    gap: 8,
  }),
}));
