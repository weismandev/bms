import { FC } from 'react';
import { useGate } from 'effector-react';
import { ChannelsGate } from '@features/socket-chats-reworked/models';
import { ChannelsToolbar } from '@features/socket-chats-reworked/components/channels/channels-toolbar';
import { ChannelsList } from '@features/socket-chats-reworked/components/channels/channels-list';
import { ErrorBoundary } from '@features/error';
import { useStyles } from './styles';

export const ChannelsPanel: FC = () => {
  useGate(ChannelsGate);

  const classes = useStyles();

  return (
    <div className={classes.channelPanel}>
      <ErrorBoundary>
        <ChannelsToolbar />

        <ChannelsList />
      </ErrorBoundary>
    </div>
  );
};
