import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  channelPanel: {
    background: 'white',
    borderRadius: '12px 0 0 12px',
    borderRight: '1px solid #f6f6f8',
  },
});
