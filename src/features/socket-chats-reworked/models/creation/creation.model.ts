import { createEvent, createStore, Effect } from 'effector';
import { pending } from 'patronum';
import {
  CREATE_DIRECT,
  CREATE_GROUP,
  CREATE_GROUP_BUILDING,
  fx,
  CREATE_TARGET_GROUP,
} from '../socket/socket.model';

export const fxCreateBuildingGroup: Effect<
  ChatsCreateBuildingGroup,
  ChatsChannel,
  Error
> = fx[CREATE_GROUP_BUILDING];
export const fxCreateGroup: Effect<ChatsCreateGroup, ChatsChannel, Error> =
  fx[CREATE_GROUP];
export const fxCreateDirect: Effect<ChatsCreateDirect, ChatsChannel, Error> =
  fx[CREATE_DIRECT];
export const fxCreateTargetedGroup: Effect<any, any, Error> = fx[CREATE_TARGET_GROUP];

export const $isCreationModalOpen = createStore<boolean>(false);
export const $isCreationLoading = pending({
  effects: [fxCreateBuildingGroup, fxCreateGroup, fxCreateTargetedGroup],
});

export const openCreationModal = createEvent<any>();
export const closeCreationModal = createEvent<any>();
export const createBuildingGroup = createEvent<number>();
export const createGroup = createEvent<ChatsCreateGroup>();
export const createDirect = createEvent<ChatsContact['id']>();
export const channelCreated = createEvent<ChatsChannel>();
export const createTargetedGroup = createEvent<any>();
