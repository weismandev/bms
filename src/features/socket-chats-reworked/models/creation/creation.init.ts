import { sample } from 'effector';
import { reset } from 'patronum';
import { $channels } from '../channels/channels.model';
import { openChat } from '../chat/chat.model';
import { $contacts } from '../contacts/contacts.model';
import {
  $isCreationModalOpen,
  channelCreated,
  closeCreationModal,
  createBuildingGroup,
  createDirect,
  createGroup,
  fxCreateBuildingGroup,
  fxCreateDirect,
  fxCreateGroup,
  openCreationModal,
  fxCreateTargetedGroup,
  createTargetedGroup,
} from './creation.model';

/** Модалка с формами для создания чатов */
$isCreationModalOpen.on(openCreationModal, () => true);

/** Создагте общедомового чата */
sample({
  clock: createBuildingGroup,
  fn: (id) => ({ building_id: id }),
  target: fxCreateBuildingGroup,
});

/** Создание групового чата */
sample({
  clock: createGroup,
  target: fxCreateGroup,
});

/** Создание директ чата */
sample({
  source: [$channels, $contacts],
  clock: createDirect,
  fn: ([channels, contacts], contactId) => {
    /** Если чат с этим контактом уже есть, то открываем его */
    const foundChannelId = Object.keys(channels).find(
      (channelId) => channels[channelId].contact_id == contactId
    );

    const foundChannel = foundChannelId ? channels[foundChannelId] : null;

    if (foundChannel) {
      openChat(foundChannel.id);
    } else {
      fxCreateDirect({
        contact: contactId,
      });
    }
  },
});

/** Обработка уcпешного создания чата (домовой и групповой) */
sample({
  clock: [
    fxCreateBuildingGroup.doneData,
    fxCreateGroup.doneData,
    fxCreateDirect.doneData,
    fxCreateTargetedGroup.doneData,
  ],
  target: channelCreated,
});

/** Автооткрытие созданного чата */
sample({
  clock: channelCreated,
  fn: (channel) => channel.id,
  target: openChat,
});

reset({
  clock: [
    closeCreationModal,
    fxCreateBuildingGroup.done,
    fxCreateGroup.done,
    openChat,
    fxCreateTargetedGroup.done,
  ],
  target: $isCreationModalOpen,
});

// создание таргетированного канала
sample({
  clock: createTargetedGroup,
  fn: (data) => ({
    title: data.title,
    age_from: data.age[0],
    age_to: data.age[1],
    genders: data.genders.map((gender) => gender.id),
    target_options: data.targetingOptions.map((option) => option.map((i) => i.id)).flat(),
    complex_id: data.complex?.id,
    contacts: [],
  }),
  target: fxCreateTargetedGroup,
});
