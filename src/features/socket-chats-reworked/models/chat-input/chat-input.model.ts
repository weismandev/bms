import { createEffect, createEvent, createStore } from 'effector';
import { pending } from 'patronum';
import { chatsApi } from '@features/socket-chats-reworked/api';
import { MessageInstance } from '@features/socket-chats-reworked/types/common';
import { File } from '@features/socket-chats-reworked/types/file';
import {
  MessageInputMode,
  SetAttachments,
  UploadFileRequest,
  UploadFileResponse,
} from '@features/socket-chats-reworked/types/messageInput';

export const MAX_FILE_SIZE = 52428800;

export const fxUploadFile = createEffect<
  UploadFileRequest,
  UploadFileResponse,
  Error
>().use(chatsApi.uploadFile);

export const $messageInput = createStore<MessageInstance['text']>('');
export const $attachments = createStore<File[]>([]);
export const $isHidden = createStore<MessageInstance['is_hidden']>(false);
export const $isAttachmentModalVisible = createStore<boolean>(false);
export const $attachmentsLoading = pending({ effects: [fxUploadFile] });
export const $attachmentsError = createStore<Error | null>(null);
export const $hasExcluded = createStore<boolean>(false);
export const $inputMode = createStore<MessageInputMode>('file');

export const changeMessageInput = createEvent<MessageInstance['text']>();
export const setAttachments = createEvent<SetAttachments>();
export const changeIsHidden = createEvent<MessageInstance['is_hidden']>();
export const closeAttachmentModal = createEvent();
export const removeAttachment = createEvent<File['id']>();
export const toggleHasExcluded = createEvent<boolean>();
export const sendAttachments = createEvent();
