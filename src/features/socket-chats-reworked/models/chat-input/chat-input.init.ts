import { sample } from 'effector';
import { reset } from 'patronum';
import { nanoid } from 'nanoid';
import { signout } from '@features/common';
import { getExtensionFromString } from '@features/socket-chats-reworked/lib/file';
import { formatFilzeSize } from '@tools/file';
import { $currentChatId } from '../chat/chat.model';
import { sendMessage } from '../messages/messages.model';
import { chatUnmounted } from '../root/root.model';
import {
  $attachments,
  $attachmentsError,
  $hasExcluded,
  $inputMode,
  $isAttachmentModalVisible,
  $isHidden,
  $messageInput,
  changeIsHidden,
  changeMessageInput,
  closeAttachmentModal,
  fxUploadFile,
  MAX_FILE_SIZE,
  removeAttachment,
  sendAttachments,
  setAttachments,
  toggleHasExcluded,
} from './chat-input.model';

sample({
  clock: changeMessageInput,
  target: $messageInput,
});

sample({
  clock: changeIsHidden,
  target: $isHidden,
});

/** Загрузка файлов в буфер */
sample({
  clock: setAttachments,
  fn: ({ files }) => {
    // @ts-ignore
    const attachments: ChatsFile[] = [...files].map((file: any) => {
      const { lastModified, lastModifiedDate, name, type, webkitRelativePath } = file;

      return {
        id: nanoid(),
        file: file,
        lastModified,
        lastModifiedDate,
        name,
        type,
        webkitRelativePath,
        extension: getExtensionFromString(file.name),
        size: {
          bytes: file.size,
          text: formatFilzeSize(file.size),
        },
        excluded: file.size >= MAX_FILE_SIZE,
      };
    });
    return attachments;
  },
  target: $attachments,
});

/** Прописываем мод загрузки (фото, файл, ...) после поступления файлов */
sample({
  clock: setAttachments,
  fn: ({ mode }) => mode,
  target: $inputMode,
});

/** Видимость модалки с файлами */
sample({
  source: $attachments,
  fn: (data) => Boolean(data.length),
  target: $isAttachmentModalVisible,
});

/** Удаление файла из буфера */
sample({
  clock: removeAttachment,
  source: $attachments,
  fn: (state, id) => state.filter((item) => item.id != id),
  target: $attachments,
});

/** Обработка ошибок связанных с большим размером файла */
sample({
  source: $attachments,
  filter: (data) => Boolean(!data.length),
  fn: (data) => {
    let hasExcluded = false;

    data.forEach((item) => {
      if (item.excluded) {
        hasExcluded = true;
      }
    });

    return Boolean(hasExcluded);
  },
  target: toggleHasExcluded,
});

/** Обновление стейта с информацией об исключенных файлах */
$hasExcluded.on(toggleHasExcluded, (_, data) => data);

/** Проход по всем файлам и их загрузка */
// eslint-disable-next-line effector/no-useless-methods
sample({
  source: { attachments: $attachments, inputMode: $inputMode },
  clock: sendAttachments,
  fn: ({ attachments, inputMode }, clock) => {
    // eslint-disable-next-line array-callback-return
    attachments.map((attachment) => {
      fxUploadFile({
        temp_id: attachment.id,
        type: inputMode,
        file: attachment.file,
      });
    });
  },
});

// eslint-disable-next-line effector/no-useless-methods
sample({
  source: { currentChatId: $currentChatId, isHidden: $isHidden, inputMode: $inputMode },
  clock: fxUploadFile.doneData,
  fn: ({ currentChatId, isHidden, inputMode }, { id }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    currentChatId &&
      sendMessage({
        channel_key: currentChatId,
        is_hidden: isHidden,
        attachment: {
          type: inputMode,
          id,
        },
      });
  },
});

reset({
  clock: [chatUnmounted, $currentChatId, sendMessage, signout],
  target: [
    $messageInput,
    $attachments,
    $isAttachmentModalVisible,
    $attachmentsError,
    $hasExcluded,
  ],
});
reset({
  clock: [chatUnmounted, $currentChatId, signout],
  target: $inputMode,
});
reset({
  clock: closeAttachmentModal,
  target: $isAttachmentModalVisible,
});
