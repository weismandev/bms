import { createStore, createEvent } from 'effector';
import { debounce } from 'patronum';
import { ChatsChannel, ChatsChannelSearch } from '../../types/search';
import { fx, FIND_GROUPS } from '../socket/socket.model';

export const MINIMAL_SEARCH_SYMBOLS = 3;

export const $search = createStore<ChatsChannelSearch>('');
export const $searchedIds = createStore<ChatsChannel['id'][]>([]);
export const $isSearchLoading = createStore<boolean>(false);

export const changeSearch = createEvent<ChatsChannelSearch>();
export const changeSearchDebounce = debounce({
  source: changeSearch,
  timeout: 500,
});

export const fxFindChannels = fx[FIND_GROUPS];
