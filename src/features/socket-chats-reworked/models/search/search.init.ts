import { sample } from 'effector';
import { reset } from 'patronum';
import { ChatsChannel, ChatsChannelSearch } from '../../types/search';
import { globalReset } from '../root/root.model';
import {
  $searchedIds,
  $search,
  changeSearch,
  changeSearchDebounce,
  fxFindChannels,
  $isSearchLoading,
  MINIMAL_SEARCH_SYMBOLS,
} from './search.model';

/** Обработка поиска по каналам */
$search.on(changeSearch, (_, value) => value);

$isSearchLoading.on(changeSearch, (_, value) => value.length >= MINIMAL_SEARCH_SYMBOLS);
$isSearchLoading.on(fxFindChannels.pending, (_, value) => Boolean(value));

/** Запрос с задержкой на поиск по каналам */
sample({
  clock: changeSearchDebounce,
  source: $search,
  filter: (search, query) => Boolean(query) && search.length >= MINIMAL_SEARCH_SYMBOLS,
  fn: (_search, query) => query,
  target: fxFindChannels.prepend((query: ChatsChannelSearch) => ({
    query,
  })),
});

$searchedIds.on(
  fxFindChannels.doneData,
  (state, { resources }: { resources: ChatsChannel[] }) =>
    resources.map((item) => item.id)
);

reset({
  clock: globalReset,
  target: [$search, $searchedIds],
});
