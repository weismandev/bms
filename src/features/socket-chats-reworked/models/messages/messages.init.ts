import { sample } from 'effector';
import { debug, reset } from 'patronum';
import { signout } from '@features/common';
import {
  getExtensionFromString,
  removeExtensionFromString,
} from '@features/socket-chats-reworked/lib/file';
import { processMessages } from '@features/socket-chats-reworked/lib/messages';
import {
  AttachmentInstance,
  MessageInstance,
  Messages,
} from '@features/socket-chats-reworked/types/common';
import { formatFilzeSize } from '@tools/file';
import { $currentChatId, $selectedMessages, openChat } from '../chat/chat.model';
import { $auth } from '../socket/socket.model';
import {
  $confirmDeleteMessagesStatus,
  $currentMessages,
  $messages,
  $messagesAttachments,
  $updatedCount,
  changeConfirmDeleteMessagesStatus,
  deletedMessages,
  deleteMessages,
  fxDeleteMessages,
  fxGetLastMessages,
  fxGetMessages,
  fxGetMessagesAttachments,
  fxMarkMessageAsRead,
  fxSendMessage,
  getLastMessages,
  getMessages,
  lastMessagesProcessed,
  loadedMessagesProcessed,
  localDeleteMessages,
  markMessageAsRead,
  messageRead,
  MESSAGES_LIMIT,
  newMessage,
  newMessageProcessed,
  sendMessage,
  setUpdatedCount,
  startReached,
} from './messages.model';

$confirmDeleteMessagesStatus
  .on(changeConfirmDeleteMessagesStatus, (_, status) => status)
  .reset(deleteMessages);

/** Запрос на последние сообщения всех каналов */
sample({
  clock: getLastMessages,
  target: fxGetLastMessages,
});

/** Логика запроса сообщений */
sample({
  clock: [startReached, getMessages],
  source: { currentMessages: $currentMessages, currentChatId: $currentChatId },
  filter: ({ currentMessages }) => {
    if (!currentMessages) return true;
    if (currentMessages.length === 0) return true;
    if (currentMessages.length === 1) return true;
    if (currentMessages.length < MESSAGES_LIMIT) return false;
    if (currentMessages.length % MESSAGES_LIMIT >= 0) return true;
    return false;
  },
  fn: ({ currentMessages, currentChatId }) =>
    currentMessages.length
      ? {
          channel_key: currentChatId || '',
          message_id: currentMessages[0].id || 0,
          limit: MESSAGES_LIMIT,
        }
      : {
          channel_key: currentChatId || '',
          limit: MESSAGES_LIMIT,
        },
  target: fxGetMessages,
});

/** Обработка получения последних сообщений */
sample({
  clock: fxGetLastMessages.doneData,
  source: $auth,
  fn: (auth, { resources }) => (auth ? processMessages(auth, resources) : []),
  target: lastMessagesProcessed,
});

/** Обработка получения сообщений.
 * Отбираем отдельно не текстовые и асинхронно запрашиваем по ним информацию
 */
sample({
  clock: fxGetMessages.doneData,
  source: $auth,
  fn: (auth, { resources }) => {
    const data: MessageInstance[] = resources;

    const processedMessages = auth ? processMessages(auth, data) : [];

    const messagesWithAttachments = processedMessages
      .filter((message: MessageInstance) => message.type != null)
      .map((message: MessageInstance) => message.id);

    if (messagesWithAttachments.length) {
      fxGetMessagesAttachments({
        messages: messagesWithAttachments,
      });
    }

    return processedMessages;
  },
  target: loadedMessagesProcessed,
});

sample({
  clock: loadedMessagesProcessed,
  filter: (messages) => {
    const lastMessage = messages[messages.length - 1];
    return Boolean(lastMessage?.channel_id) && Boolean(lastMessage?.id);
  },
  fn: (messages) => {
    const lastMessage = messages[messages.length - 1];

    return { channelId: lastMessage?.channel_id, messageId: lastMessage?.id };
  },
  target: markMessageAsRead,
});

/** Обработка полученных сообщений */
sample({
  clock: loadedMessagesProcessed,
  source: $messages,
  fn: (state, data) => {
    // Устанавливаем кол-во пришедших сообщений
    setUpdatedCount(data.length);

    const newState: Messages = state;

    const channelId = data[0].channel_id;

    if (!channelId) return state;

    const messagesFromState = newState[channelId] || [];

    const mergedMessages = [...data, ...messagesFromState];

    if (newState[channelId]?.length) {
      newState[channelId] = [
        ...new Map(mergedMessages.map((item) => [item.id, item])).values(),
      ];
    } else {
      newState[channelId] = data;
    }

    return { ...newState };
  },
  target: $messages,
});

/** Обработка последних сообщений, с проверкой,
 * не дублируется ли сообщение из кеша */
sample({
  clock: lastMessagesProcessed,
  source: $messages,
  fn: (state, data) => {
    const newState: Messages = state;

    // eslint-disable-next-line consistent-return
    data.forEach((message) => {
      const channelId = message.channel_id;

      if (channelId) {
        const messages = newState[channelId];
        const matchedLastMessage = messages?.find(
          (item) => item.channel_id === channelId
        );

        if (!matchedLastMessage && messages) {
          newState[channelId] = [...messages, message];
          return null;
        }

        if (!matchedLastMessage && !messages) {
          newState[channelId] = [message];
          return null;
        }
      }
    });

    return { ...newState };
  },
  target: $messages,
});

/** Обработка новых сообщений */
sample({
  clock: newMessageProcessed,
  source: $messages,
  fn: (state, message) => {
    const newState = state;
    const channelId = message.channel_id;

    if (!channelId) return state;

    const prevMessages = newState[channelId];

    if (message.channel_id) {
      if (prevMessages) {
        // Устанавливаем кол-во пришедших сообщений
        setUpdatedCount(newState[channelId].length);
        newState[channelId] = [...prevMessages, message];
      } else {
        newState[channelId] = [message];
      }
    }

    return { ...newState };
  },
  target: $messages,
});

/** Обработка аттачментов, их складываем отдельно */
sample({
  clock: fxGetMessagesAttachments.doneData,
  source: $messagesAttachments,
  fn: (state, { resources }) => {
    const newState = state;

    resources.forEach((messageAttachment) => {
      const formattedAttachment: AttachmentInstance = {
        ...messageAttachment.attachment,
        /** Преобразование для отображения размера файла */
        size: {
          bytes: messageAttachment.attachment?.bytes || 0,
          text: formatFilzeSize(messageAttachment.attachment?.bytes || 0) || '',
        },
        /** Преобразование для отображения формата файла */
        extension: messageAttachment.attachment?.name?.length
          ? getExtensionFromString(messageAttachment.attachment.name) || ''
          : '',
        name: messageAttachment.attachment?.name?.length
          ? removeExtensionFromString(messageAttachment.attachment.name) || ''
          : messageAttachment.attachment.name,
      };

      newState[messageAttachment.message_id] = formattedAttachment;
    });

    return { ...newState };
  },
  target: $messagesAttachments,
});

/** После получения новых сообщений, записываем их кол-во */
sample({
  clock: setUpdatedCount,
  source: $updatedCount,
  fn: (value) => (value === 1 ? 0 : value),
  target: $updatedCount,
});

/** Обработка нового сообщения (не своего) */
sample({
  clock: newMessage,
  source: $auth,
  fn: (auth, { data }: { data: MessageInstance } | any) => {
    const processedMessages: MessageInstance[] = auth
      ? processMessages(auth, [data])
      : [];

    const messagesWithAttachments = processedMessages
      .filter((message: MessageInstance) => message.type != null)
      .map((message: MessageInstance) => message.id);

    if (messagesWithAttachments.length) {
      fxGetMessagesAttachments({
        messages: messagesWithAttachments,
      });
    }

    return processedMessages[0];
  },
  target: newMessageProcessed,
});

/** Удаляю сообщение, которое было удалено другим пользователем чата */
sample({
  clock: deletedMessages,
  source: $currentChatId,
  fn: (currentChatId, { data }) => ({
    channel_id: currentChatId,
    messages: data[0],
  }),
  target: localDeleteMessages,
});

/** Обработка отправки сообщений. Отправленные сообщения проходят
 * такой-же цикл, как и другие. Для обхода цепочки запросов+
 * используется флаг is_offline
 */
sample({
  clock: sendMessage,
  target: [fxSendMessage],
});

/** Маркировка чужих сообщений о прочтении */
sample({
  clock: markMessageAsRead,
  fn: ({ messageId }) => ({ message_id: messageId }),
  target: fxMarkMessageAsRead,
});

/** Обновление стейта после маркировки */
sample({
  clock: markMessageAsRead,
  source: $messages,
  fn: (state, { channelId, messageId }) => {
    const newState: any = state;

    if (channelId) {
      const messages: MessageInstance[] = newState[channelId];

      if (messages) {
        newState[channelId] = messages.map((message: any) => {
          if (message.id === messageId) return { ...message, read: true };
          return message;
        });
      }
    }

    return { ...newState };
  },
  target: $messages,
});

/** Входящий ивент о маркировке */
sample({
  clock: messageRead,
  source: $messages,
  fn: (state, { message_id }) => {
    const newState: any = state;

    if (message_id) {
      const messages: MessageInstance[] = newState[message_id];

      if (messages) {
        newState[message_id] = messages.map((message: any) => {
          if (message.id === message_id) return { ...message, read: true };
          return message;
        });
      }
    }

    return { ...newState };
  },
  target: $messages,
});

/** Запрос на удаление сообщений на беке и локально */
sample({
  clock: deleteMessages,
  source: { currentChatId: $currentChatId, selectedMessages: $selectedMessages },
  fn: ({ currentChatId, selectedMessages }) => ({
    channel_id: currentChatId,
    messages: selectedMessages,
    for_all: true,
  }),
  target: [fxDeleteMessages, localDeleteMessages],
});

sample({
  clock: localDeleteMessages,
  source: $messages,
  fn: (state, { channel_id, messages }) => {
    if (!channel_id || !messages) return state;

    const newState = state;

    const channelMessages = newState[channel_id];

    newState[channel_id] = [
      ...channelMessages.filter((message) => messages.indexOf(message.id) === -1),
    ];

    return { ...newState };
  },
  target: $messages,
});

reset({
  clock: [newMessage],
  target: $updatedCount,
});

reset({
  clock: openChat,
  target: [$currentMessages, $updatedCount],
});

reset({
  clock: [signout],
  target: [$messages, $messagesAttachments, $currentMessages, $updatedCount],
});
