import { combine, createEvent, createStore, Effect, Event } from 'effector';
import { debug } from 'patronum';
import {
  Attachments,
  ChannelInstance,
  DeleteMessageRequest,
  DeleteMessages,
  GetLastMessagesRequest,
  GetLastMessagesResponse,
  GetMessagesAttachmentsRequest,
  GetMessagesAttachmentsResponse,
  GetMessagesRequest,
  GetMessagesResponse,
  MarkMessageAsRead,
  MarkMessageAsReadRequest,
  MessageInstance,
  MessageRead,
  Messages,
  SendMessage,
} from '@features/socket-chats-reworked/types/common';
import { $currentChatId } from '../chat/chat.model';
import {
  DELETE_MESSAGES,
  fx,
  GET_CHANNEL_MESSAGES,
  GET_LAST_MESSAGES,
  GET_MESSAGES_ATTACHMENTS,
  MARK_MESSAGE_AS_READ,
  MESSAGE_READ,
  MESSAGES_DELETED,
  NEW_MESSAGE,
  receivedEvent,
  SEND_MESSAGE,
} from '../socket/socket.model';

export const MESSAGES_LIMIT = 50;

export const $messages = createStore<Messages>({});
export const $messagesAttachments = createStore<Attachments>({});
export const $currentMessages = combine(
  $messages,
  $currentChatId,
  (messages, currentChatId) => (currentChatId ? messages[currentChatId] : [])
);
/** Стор для кол-ва последних загруженных сообщений.
 * Используется для автоскролла после загрузки новых сообщений */
export const $updatedCount = createStore<number>(0);

export const getMessages = createEvent<void>();
export const getLastMessages = createEvent<{ channels: ChannelInstance['id'][] }>();
export const loadedMessagesProcessed = createEvent<MessageInstance[]>();
export const lastMessagesProcessed = createEvent<MessageInstance[]>();
export const newMessageProcessed = createEvent<MessageInstance>();
export const sendMessage = createEvent<SendMessage>();
export const markMessageAsRead = createEvent<MarkMessageAsRead>();
export const deleteMessages = createEvent();
export const localDeleteMessages = createEvent<DeleteMessages>();
export const newMessage: Event<MessageInstance | any> = receivedEvent[NEW_MESSAGE];
export const deletedMessages: Event<MessageInstance | any> =
  receivedEvent[MESSAGES_DELETED];
export const messageRead: Event<MessageRead | any> = receivedEvent[MESSAGE_READ];
export const startReached = createEvent<void>();
export const setUpdatedCount = createEvent<number>();

export const fxGetMessages: Effect<GetMessagesRequest, GetMessagesResponse> =
  fx[GET_CHANNEL_MESSAGES];
export const fxGetLastMessages: Effect<GetLastMessagesRequest, GetLastMessagesResponse> =
  fx[GET_LAST_MESSAGES];
export const fxGetMessagesAttachments: Effect<
  GetMessagesAttachmentsRequest,
  GetMessagesAttachmentsResponse
> = fx[GET_MESSAGES_ATTACHMENTS];
export const fxSendMessage: Effect<SendMessage, any> = fx[SEND_MESSAGE];
export const fxDeleteMessages: Effect<DeleteMessageRequest, any> = fx[DELETE_MESSAGES];
export const fxMarkMessageAsRead: Effect<MarkMessageAsReadRequest, any> =
  fx[MARK_MESSAGE_AS_READ];

export const $confirmDeleteMessagesStatus = createStore(false);
export const changeConfirmDeleteMessagesStatus = createEvent<boolean>();
