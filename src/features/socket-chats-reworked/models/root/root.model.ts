import { createEvent, createStore, merge } from 'effector';
import { createGate } from 'effector-react';
import { createPersist } from 'effector-storage/local';
import { pending } from 'patronum';
import { every } from 'patronum/every';
import { signout } from '@features/common';
import { ChatGateType } from '@features/socket-chats-reworked/types/common';
import { $isAuth, fxPushToSocket } from '../socket/socket.model';

/**
 * Гейты отдельно для каналов и чата.
 * Сторы с информацией о готовности гейтов базируются на логике:
 * Комопнент смонтировался и есть авторизация
 */

export const persist = createPersist({
  keyPrefix: 'chats/',
});

export const $channelsMounted = createStore<boolean>(false);
export const $chatMounted = createStore<boolean>(false);
export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);

/** Гейт для каналов */
export const ChannelsGate = createGate();
export const { open: channelsMounted, close: channelsUnmounted } = ChannelsGate;
export const $channelsGateReady = every({
  predicate: (value) => Boolean(value),
  stores: [$channelsMounted, $isAuth],
});

/** Гейт для окна с чатом (сообщения) */
export const ChatGate = createGate<ChatGateType>();
export const { open: chatMounted, close: chatUnmounted } = ChatGate;
export const $chatGateReady = every({
  predicate: (value) => Boolean(value),
  stores: [$chatMounted, $isAuth],
});

export const errorCatched = createEvent<Record<string, never>>();
export const $requestOccured = pending({
  effects: [fxPushToSocket],
});
export const errorOccured = merge([errorCatched]);

export const globalReset = [channelsUnmounted, chatUnmounted, signout];
export const baseReset = [errorOccured, signout];
