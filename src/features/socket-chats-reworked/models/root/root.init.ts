import { reset } from 'patronum';
import { signout } from '@features/common';
import {
  $channelsMounted,
  $chatMounted,
  $error,
  $isLoading,
  $requestOccured,
  channelsMounted,
  chatMounted,
  errorOccured,
} from './root.model';

$isLoading.on($requestOccured, (pending) => pending);

$error.on(errorOccured, (_, { error }: any) => error);

$channelsMounted.on(channelsMounted, () => true);

$chatMounted.on(chatMounted, () => true);

reset({
  clock: signout,
  target: [$channelsMounted, $chatMounted, $isLoading, $error],
});
