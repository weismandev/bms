import { sample } from 'effector';
import { reset } from 'patronum';
import { history } from '@features/common';
import { makeInitials } from '@features/socket-chats-reworked/lib';
import { genHsl } from '@features/socket-chats-reworked/lib/colors';
import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import { getContacts } from '../contacts/contacts.model';
import { channelCreated } from '../creation/creation.model';
import {
  fxGetLastMessages,
  getLastMessages,
  markMessageAsRead,
  newMessageProcessed,
} from '../messages/messages.model';
import { $channelsGateReady, baseReset } from '../root/root.model';
import { $search, $searchedIds } from '../search/search.model';
import {
  $channels,
  $channelsIds,
  $pagination,
  DEFAULT_PAGINATION,
  $userChannelsIds,
  baseChannelsLoaded,
  fxGetChannel,
  fxGetChannels,
  fxGetExtendedChannels,
  fxLeaveChannel,
  fxSetChannelNotificationType,
  getChannel,
  leaveChannel,
  loadChannelsRange,
  loadMoreChannels,
  paginationChanged,
  setChannelNotificationType,
} from './channels.model';

/** Запрос каналов */
sample({
  source: $channelsGateReady,
  filter: Boolean,
  target: fxGetChannels,
});

/** Запрос каналов при сбросе поиска */
sample({
  source: $search,
  filter: (search) => search === '',
  fn: () => true,
  target: fxGetChannels,
});

/** Все каналы (пришедшие с бека) фильтруем и сохраняем в общий кеш-стор */
sample({
  clock: fxGetChannels.doneData,
  source: $channels,
  fn: (state, { resources }) => {
    const newState = { ...state };

    resources
      .filter(
        (channel) =>
          channel?.service_type === 'management_company' ||
          channel?.service_type === 'building' ||
          channel?.service_type === 'support' ||
          channel?.service_type === null
      )
      .forEach((channel) => {
        const channelFromState = newState[channel.id] || {};

        newState[channel.id] = {
          ...channelFromState,
          ...channel,
          loaded: false,
        };
      });

    return newState;
  },
  target: [$channels, baseChannelsLoaded],
});

/** Запись id каналов */
sample({
  // @ts-ignore
  clock: fxGetChannels.doneData,
  fn: ({ resources }) =>
    resources
      .filter(
        (channel) =>
          channel?.service_type === 'management_company' ||
          channel?.service_type === 'building' ||
          channel?.service_type === 'support' ||
          channel?.service_type === null
      )
      .map((channel) => channel.id),
  target: $channelsIds,
});

/** Запрос на детальную информацию по каналам */
sample({
  clock: [baseChannelsLoaded, paginationChanged],
  source: { pagination: $pagination, channelsIds: $channelsIds },
  fn: ({ pagination, channelsIds }) => ({
    channels: channelsIds.slice(
      (pagination.page - 1) * pagination.pageSize,
      (pagination.page - 1) * pagination.pageSize + pagination.pageSize
    ),
  }),
  target: loadChannelsRange,
});

sample({
  clock: loadChannelsRange,
  filter: ({ channels }) => channels.length,
  target: fxGetExtendedChannels,
});

/** Запрос на последние сообщения всех каналов */
sample({
  clock: loadChannelsRange,
  target: getLastMessages,
});

/** Обработка пагинации */
sample({
  clock: loadMoreChannels,
  source: $pagination,
  fn: ({ page, pageSize }) => ({
    page: page + 1,
    pageSize,
  }),
  target: $pagination,
});

sample({
  clock: $pagination,
  target: paginationChanged,
});

/** ----------------------------------------------------- */

/** Запрос на технические каналы. Применяется в stand alone версии чата,
 * когда нужно просто открыть окно с чатом (заявки) */
sample({
  clock: getChannel,
  filter: (channelId) => Boolean(channelId),
  fn: (channelId) => [channelId],
  target: fxGetChannel.prepend((data: string[]) => ({
    channels: data,
  })),
});

/** ----------------------------------------------------- */

/** После получения информации о канале запрашиваем контакт каждого канала */
sample({
  clock: fxGetChannels.doneData,
  filter: ({ resources }) => Boolean(resources.length),
  fn: ({ resources }) => resources.map((item) => item.contact_id),
  target: getContacts,
});

/** После получения расширеной информации о канале, запрашиваем контакт каждого канала т.к. при обычном получении информации id уонтактов не приходит */
sample({
  clock: fxGetExtendedChannels.doneData,
  filter: ({ resources }) => Boolean(resources.length),
  fn: ({ resources }) => resources.map((item) => item.contact_id),
  target: getContacts,
});

/** Обработка детальной информации по основным каналам */
sample({
  clock: [fxGetExtendedChannels.doneData, fxGetChannel.doneData],
  source: $channels,
  fn: (state, { resources }: { resources: ChannelInstance[] }) => {
    const newState = state;

    resources.forEach((channel) => {
      const usedChannel = newState[channel.id] ? newState[channel.id] : channel;

      const avatar = usedChannel.contact_id
        ? null
        : {
            avatar: {
              title: makeInitials(`${usedChannel.title || channel.title}`),
              color: genHsl(`${usedChannel.id || channel.id}`),
            },
          };

      newState[channel.id] = {
        ...channel,
        ...usedChannel,
        ...avatar,
        loaded: true,
      };
    });

    return { ...newState };
  },
  target: $channels,
});

/** Изменение количества непрочитанных сообщений при поступлении новых */
sample({
  clock: newMessageProcessed,
  source: $channels,
  fn: (state, message) => {
    const newState = state;
    const channel = newState[message.channel_id];

    // @ts-ignore
    const currentChatId = $currentChatId.stateRef.current;

    if (
      currentChatId !== channel.id &&
      !message.is_owner &&
      channel.id === message.channel_id
    ) {
      newState[message.channel_id] = {
        ...channel,
        unread_messages: channel.unread_messages + 1,
      };
    }
  },
  target: $channels,
});

/** Изменение количества непрочитанных сообщений после прочтения */
sample({
  clock: markMessageAsRead,
  source: $channels,
  fn: (state, { channelId, messageId }) => {
    const newState = state;
    const channel = newState[channelId];

    newState[channelId] = {
      ...channel,
      unread_messages: 0,
    };
  },
  target: $channels,
});

/** Обработка созданного канала */

// Добавляем в общий стор
sample({
  clock: channelCreated,
  source: $channels,
  fn: (state, channel) => {
    const newState = state;

    newState[channel.id] = {
      ...channel,
      avatar: {
        title: channel.contact_id
          ? makeInitials(channel?.title)
          : makeInitials(`${channel.title}`),
        color: channel.contact_id ? genHsl(channel?.id) : genHsl(channel.id),
      },
      loaded: true,
    };

    return { ...newState };
  },
  target: $channels,
});

// Добавляем в свои каналы
// sample({
//   clock: channelCreated,
//   source: $channels,
//   fn: (state, channel) => {
//     const newState = state;
//
//     newState[channel.id] = channel;
//
//     return newState;
//   },
//   target: $channels,
// });
sample({
  clock: channelCreated,
  source: $channelsIds,
  fn: (channelsIds, channel) => [channel?.id, ...channelsIds],
  target: $channelsIds,
});

/** После получения данных поиска, запрашиваю найденные каналы */
sample({
  clock: $searchedIds,
  fn: (channels) => ({
    channels,
  }),
  target: fxGetExtendedChannels,
});

/** Получаю сообщения по найденным каналам */
sample({
  clock: $searchedIds,
  fn: (channels) => ({
    channels,
  }),
  target: fxGetLastMessages,
});

/** Изменение типа уведомлений канала */
sample({
  clock: setChannelNotificationType,
  target: fxSetChannelNotificationType,
});

sample({
  clock: setChannelNotificationType,
  source: $channels,
  fn: (state, { channel_id, notification_type }) => {
    const newState = state;

    newState[channel_id] = {
      ...newState[channel_id],
      notification_type,
    };

    return { ...newState };
  },
  target: $channels,
});

/** Выход из канала */
sample({
  clock: leaveChannel,
  fn: (channel_id) => ({ channel_id }),
  target: fxLeaveChannel,
});

// Перекидываем на стартовый экран
// eslint-disable-next-line effector/no-useless-methods
sample({
  clock: leaveChannel,
  fn: () => history.push('/messages'),
});

// Удаляем из общего хранилища
$channels.on(leaveChannel, (state, channelId) => {
  const newState = state;

  delete newState[channelId];

  return { ...newState };
});

// Удаляем из своих каналов
// sample({
//   clock: leaveChannel,
//   source: $channels,
//   fn: (state, channelId) => state.filter((id) => id !== channelId),
//   target: $channels,
// });
sample({
  clock: leaveChannel,
  source: $channels,
  fn: (channelsIds, channelId) => channelsIds.filter((id) => id !== channelId),
  target: $channels,
});

reset({
  clock: baseReset,
  target: $channels,
});

sample({
  clock: $search,
  filter: (search) => search === '',
  fn: () => DEFAULT_PAGINATION,
  target: $pagination,
});
