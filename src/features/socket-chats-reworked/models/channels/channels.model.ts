import { combine, createEvent, createStore, Effect } from 'effector';
import { debug } from 'patronum';
import {
  ChannelInstance,
  Channels,
  GetChannelRequest,
  GetChannelResponse,
  GetChannelsResponse,
  GetExtendedChannelsRequest,
  GetExtendedChannelsResponse,
  LeaveChannelRequest,
  SetChannelNotificationType,
  SetChannelNotificationTypeRequest,
} from '@features/socket-chats-reworked/types/common';
import { $search, $searchedIds } from '../search/search.model';
import {
  fx,
  GET_ALL_CHANNELS,
  GET_CHANNELS,
  LEAVE_GROUP,
  SET_CHANNEL_NOTIFICATION_TYPE,
} from '../socket/socket.model';

export const DEFAULT_PAGINATION = { page: 1, pageSize: 20 };

export const $channels = createStore<Channels>({});
export const $channelsIds = createStore<ChannelInstance['id'][]>([]);
export const $pagination = createStore(DEFAULT_PAGINATION);

export const loadMoreChannels = createEvent<boolean>();
export const loadChannelsRange = createEvent<{ channels: ChannelInstance['id'][] }>();
export const paginationChanged = createEvent<void>();
export const baseChannelsLoaded = createEvent<Channels>();
export const getChannel = createEvent<ChannelInstance['id']>();
export const setChannelNotificationType = createEvent<SetChannelNotificationType>();
export const leaveChannel = createEvent<ChannelInstance['id']>();

export const fxGetChannels: Effect<any, GetChannelsResponse> = fx[GET_ALL_CHANNELS];
export const fxGetExtendedChannels: Effect<
  GetExtendedChannelsRequest,
  GetExtendedChannelsResponse
> = fx[GET_CHANNELS];

export const fxGetChannel: Effect<GetChannelRequest, GetChannelResponse> =
  fx[GET_CHANNELS];
export const fxSetChannelNotificationType: Effect<
  SetChannelNotificationTypeRequest,
  any
> = fx[SET_CHANNEL_NOTIFICATION_TYPE];
export const fxLeaveChannel: Effect<LeaveChannelRequest, any> = fx[LEAVE_GROUP];

export const $userChannelsIds = combine(
  $channelsIds,
  $pagination,
  $searchedIds,
  $search,
  (channelsIds, { page, pageSize }, searchedIds, search) => {
    if (search === '') {
      return channelsIds.slice(0, page * pageSize);
    }

    return searchedIds;
  }
);
