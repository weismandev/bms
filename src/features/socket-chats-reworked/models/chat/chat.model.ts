import { combine, createEvent, createStore } from 'effector';
import { inFlight } from 'patronum';
import { pending } from 'patronum';
import {
  ChannelInstance,
  MessageInstance,
} from '@features/socket-chats-reworked/types/common';
import { $channels, fxGetChannel } from '../channels/channels.model';
import { fxGetChannelContacts } from '../contacts/contacts.model';

export const $currentChatId = createStore<ChannelInstance['id'] | null>(null);
// export const $chatData = createStore<ChannelInstance | null>(null);
export const $chatData = combine($currentChatId, $channels, (currentChatId, channels) =>
  currentChatId ? channels[currentChatId] : null
);
export const $isReadonly = createStore<boolean>(false);
export const $isChatLoading = pending({
  effects: [fxGetChannelContacts],
});
export const $isSelectMode = createStore<boolean>(false);
export const $selectedMessages = createStore<MessageInstance['id'][]>([]);
export const $isDrawerVisible = createStore<boolean>(false);

export const openChat = createEvent<ChannelInstance['id']>();
export const chatOpened = createEvent<ChannelInstance['id']>();
export const toggleSelectMode = createEvent();
export const selectMessage = createEvent<MessageInstance['id']>();
export const deselectMessage = createEvent<MessageInstance['id']>();
export const setDrawerVisibility = createEvent<boolean>();
