import { sample } from 'effector';
import { debug, reset } from 'patronum';
import { history, NEW_MESSAGE, readNotifications } from '@features/common';
import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import { getChannel, leaveChannel } from '../channels/channels.model';
import { getChannelContacts } from '../contacts/contacts.model';
import { getMessages, localDeleteMessages } from '../messages/messages.model';
import { $chatGateReady, ChatGate, globalReset } from '../root/root.model';
import {
  $chatData,
  $currentChatId,
  $isDrawerVisible,
  $isReadonly,
  $isSelectMode,
  $selectedMessages,
  chatOpened,
  deselectMessage,
  openChat,
  selectMessage,
  setDrawerVisibility,
  toggleSelectMode,
} from './chat.model';

/** Редирект при открытии чата */
sample({
  clock: openChat,
  source: $currentChatId,
  filter: (state, channelId) => state == null || state !== channelId,
  fn: (state, channelId) => {
    history.push(`/messages/${channelId}`);
    return channelId;
  },
  target: $currentChatId,
});

/** Обработка открытия чата по id из роута */
sample({
  clock: sample({
    source: [$chatGateReady, ChatGate.state],
    filter: ([gateReady, gateState]: any) => Boolean(gateReady) && gateState.channelId,
  }),
  source: ChatGate.state,
  fn: (state: any) => state.channelId,
  target: [$currentChatId, chatOpened],
});

/** Запрос на информацию о канале. Если чат есть в списке каналов,
 * то скопируется оттуда, или уйдет запрос и чат сохранится
 * во временный стор с чатом
 */
sample({
  clock: chatOpened,
  fn: (channelId: ChannelInstance['id']) => channelId,
  target: getChannel,
});

/** Запрос на контакты канала */
sample({
  clock: chatOpened,
  target: getChannelContacts,
});

/** Запрос сообщений канала */
sample({
  clock: chatOpened,
  target: getMessages,
});

/** Линк (синхронизация) канала из общего стора с локальным */
// sample({
//   source: { channels: $channels, currentChatId: $currentChatId },
//   clock: sample({
//     source: [$channels, $currentChatId],
//     filter: ([currentId, channels]) => Boolean(currentId && channels),
//   }),
//   fn: ({ channels, currentChatId }) => {
//     if (!currentChatId) return null;
//
//     return channels[currentChatId] || null;
//   },
//   target: $chatData,
// });

/** Обработка состояния выбора сообщений */
sample({
  clock: toggleSelectMode,
  source: $isSelectMode,
  fn: (state) => Boolean(!state),
  target: $isSelectMode,
});

/** Обработка выбора сообщений */
sample({
  clock: selectMessage,
  source: $selectedMessages,
  fn: (state, messageId) => [...state, messageId],
  target: $selectedMessages,
});
sample({
  clock: deselectMessage,
  source: $selectedMessages,
  fn: (state, messageId) => state.filter((id) => messageId != id),
  target: $selectedMessages,
});

/** Изначальная обработка readonly, до получения информации о канале */
sample({
  source: ChatGate.state,
  clock: sample({
    source: [$chatGateReady, ChatGate.state],
    filter: ([gateReady, gateState]: any) => Boolean(gateReady) && gateState.channelId,
  }),
  fn: (state: any) => state.readonly,
  target: $isReadonly,
});

/** Обработка readonly после получения информации о канале.
 * Положительное значение с бека важнее локального
 */
sample({
  clock: $chatData,
  source: $isReadonly,
  fn: (state, chatData) => Boolean(chatData && chatData.readonly),
  target: $isReadonly,
});

/** Обработка изменения видимости дровера с информацией */
sample({
  clock: setDrawerVisibility,
  fn: (value) => Boolean(value),
  target: $isDrawerVisible,
});

sample({
  source: $currentChatId,
  filter: (id) => Boolean(id),
  target: readNotifications.prepend((id) => ({
    id,
    event: NEW_MESSAGE,
  })),
});

reset({
  clock: toggleSelectMode,
  target: $selectedMessages,
});

reset({
  clock: [$currentChatId, localDeleteMessages],
  target: [$isSelectMode, $selectedMessages],
});

reset({
  clock: [...globalReset, leaveChannel],
  target: [$currentChatId, $chatData, $isSelectMode, $selectedMessages, $isDrawerVisible],
});
