import { attach, createEffect, createEvent, createStore, Effect, split } from 'effector';
import { createGate } from 'effector-react';
import { inFlight } from 'patronum';
import {
  OpenSocketRequest,
  PushToSocketRequest,
  SocketAuth,
  SocketAuthRequest,
  SocketGateType,
  SocketState,
} from '@features/socket-chats-reworked/types/socket';

export const IM_URL: string = process.env.IM_URL!;

// Входящие ивенты
export const RESPONSE = 'response';
export const NEW_MESSAGE = 'newMessage';
export const CHANNEL_UPDATED = 'channelUpdated';
export const INVITED_TO_CHANNEL = 'invitedToChannel';
export const MESSAGES_DELETED = 'messageDeleted';
export const MESSAGE_READ = 'messageRead';
export const MESSAGE_UNREAD = 'messageUnread';

// Исходяшие ивенты
export const AUTHENTICATE = 'authenticate';
export const GET_CONTACTS = 'getContacts';
export const FIND_CONTACTS = 'findContacts';
export const BLACKLIST_CONTACT = 'blacklistContact';
export const GET_ALL_CHANNELS = 'getAllChannels';
export const GET_CHANNELS = 'getChannels';
export const FIND_GROUPS = 'findGroups';
export const CREATE_GROUP = 'createGroup';
export const CREATE_GROUP_BUILDING = 'createGroupBuilding';
export const CREATE_DIRECT = 'createDirect';
export const CREATE_TARGET_GROUP = 'createTargetGroup';
export const LEAVE_GROUP = 'leaveGroup';
export const SET_GROUP_ALIAS = 'setGroupAlias';
export const ARCHIVE_CHANNEL = 'archiveChannel';
export const UNARCHIVE_CHANNEL = 'unarchiveChannel';
export const SET_CHANNEL_NOTIFICATION_TYPE = 'setChannelNotificationType';
export const GET_GROUP_CONTACTS = 'getGroupContacts';
export const SET_GROUP_IMAGE = 'setGroupImage';
export const SET_GROUP_TITLE = 'setGroupTitle';
export const ADD_CONTACTS_IN_GROUP = 'addContactsInGroup';
export const GET_CHANNEL_MESSAGES = 'getChannelMessages';
export const GET_LAST_MESSAGES = 'getLastMessages';
export const GET_MESSAGES_ATTACHMENTS = 'getMessagesAttachments';
export const GET_CHANNEL_ATTACHMENTS = 'getChannelAttachments';
export const SEND_MESSAGE = 'sendMessage';
export const FORWARD_MESSAGE = 'forwardMessage';
export const DELETE_MESSAGES = 'deleteMessages';
export const MARK_MESSAGE_AS_READ = 'markMessageAsRead';
export const MARK_MESSAGE_AS_UNREAD = 'markMessageAsUnread';
export const MAKE_CHANNEL = 'makeChannel';

export const incomingEvents = [
  RESPONSE,
  NEW_MESSAGE,
  CHANNEL_UPDATED,
  INVITED_TO_CHANNEL,
  MESSAGES_DELETED,
  MESSAGE_READ,
  MESSAGE_UNREAD,
];

export const outgoingEvents = [
  AUTHENTICATE,
  GET_CONTACTS,
  FIND_CONTACTS,
  BLACKLIST_CONTACT,
  GET_ALL_CHANNELS,
  GET_CHANNELS,
  FIND_GROUPS,
  CREATE_GROUP,
  CREATE_GROUP_BUILDING,
  CREATE_DIRECT,
  CREATE_TARGET_GROUP,
  LEAVE_GROUP,
  SET_GROUP_ALIAS,
  ARCHIVE_CHANNEL,
  UNARCHIVE_CHANNEL,
  SET_CHANNEL_NOTIFICATION_TYPE,
  GET_GROUP_CONTACTS,
  SET_GROUP_IMAGE,
  SET_GROUP_TITLE,
  ADD_CONTACTS_IN_GROUP,
  GET_CHANNEL_MESSAGES,
  GET_LAST_MESSAGES,
  GET_MESSAGES_ATTACHMENTS,
  GET_CHANNEL_ATTACHMENTS,
  SEND_MESSAGE,
  FORWARD_MESSAGE,
  DELETE_MESSAGES,
  MARK_MESSAGE_AS_READ,
  MARK_MESSAGE_AS_UNREAD,
  MAKE_CHANNEL,
];

/** SocketGate служит для трансфера токена из компонента (чат или каналы) в модель.
 * На данный момент такая логика используется только в чате из заявки ЛК компании,
 * т.к необходимо отображать окно чата из-под разных компаний. В компонент чата
 * прокидывается токен через этот гейт.
 * Авторизация использует общий токен или пришедший из SocketGate.
 */
export const SocketGate = createGate<SocketGateType>();
export const { open: socketGateMounted, close: socketGateUnmounted } = SocketGate;

export const connected = createEvent<any>();
export const connecting = createEvent<any>();
export const disconnected = createEvent<any>();
export const reconnected = createEvent<any>();
export const socketEventReceived = createEvent<any>();

/** Генерация ивентов */
export const receivedEvent = split<any, any>(
  socketEventReceived,
  incomingEvents.reduce(
    (schema, event) => ({
      ...schema,
      [event]: ({ event: e }: any) => e === event,
    }),
    {}
  )
);

export const $socket = createStore<any>(null);
export const $socketState = createStore<SocketState>('disconnected');

export const fxOpenSocket = createEffect<OpenSocketRequest, any, Error>();
export const fxPushToSocket = createEffect<PushToSocketRequest, any, Error>();

/** Генерация эффектов на основе fxPushToSocket из списка ивентов */
export const fx: any = outgoingEvents.reduce(
  (acc, event) => ({
    ...acc,
    [event]: attach({
      effect: fxPushToSocket,
      source: $socket,
      mapParams: (data, socket) => ({ socket, event, data }),
    }),
  }),
  {}
);

export const fxAuth: Effect<SocketAuthRequest, SocketAuth> = fx[AUTHENTICATE];

export const $isSocketOpening = inFlight({ effects: [fxOpenSocket] });
export const $isAuthLoading = inFlight({ effects: [fxAuth] });

export const $auth = createStore<SocketAuth | null>(null);
export const $isAuth = createStore<boolean>(false);
