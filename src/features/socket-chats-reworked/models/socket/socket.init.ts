import { sample } from 'effector';
import { reset } from 'patronum';
import { $token, signout } from '@features/common';
import { chatsApi } from '@features/socket-chats-reworked/api';
import {
  $auth,
  $isAuth,
  $socket,
  $socketState,
  connected,
  connecting,
  disconnected,
  fxAuth,
  fxOpenSocket,
  fxPushToSocket,
  IM_URL,
  SocketGate,
  socketGateMounted,
} from './socket.model';

$socketState
  .on(connecting, () => 'connecting')
  .on(connected, () => 'connected')
  .on(disconnected, () => 'disconnected');

fxOpenSocket.use(chatsApi.openSocket);
fxPushToSocket.use(chatsApi.pushToSocket);

/**
 * Инициализация сокета при маунте списка каналов или окна с чатом.
 * Гейтов 2, т.к. окно с чатом может использоваться отдельно в других местах
 * например на странице конкретного тикета. Из-за этого делаем проверку
 * на уже открывающийся сокет.
 */
sample({
  source: $socketState,
  clock: socketGateMounted,
  filter: (socketState, clock) => socketState === 'disconnected',
  target: fxOpenSocket.prepend(() => ({ socketUrl: IM_URL })),
});

/** Сохранение объекта сокета */
$socket.on(fxOpenSocket.doneData, (_, data) => data);

/** Авторизация если есть токен и установленно соединение с сокетом */
sample({
  source: { token: $token, SocketGate: SocketGate.state },
  clock: connected,
  fn: ({ token: bmsToken, SocketGate: socketGate }) => {
    const token = !socketGate || !socketGate.token ? bmsToken || '' : socketGate.token;
    const language = localStorage.getItem('lang') || 'ru';

    return { language, token };
  },
  target: fxAuth,
});

sample({
  clock: fxAuth.doneData,
  target: $auth,
});

sample({
  clock: fxAuth.doneData,
  fn: (userData) => Boolean(userData.id),
  target: $isAuth,
});

reset({
  clock: signout,
  target: [$auth, $isAuth],
});

reset({
  clock: [signout],
  target: [$socket, $socketState],
});
