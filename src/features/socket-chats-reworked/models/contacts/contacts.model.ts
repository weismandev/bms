import { combine, createEvent, createStore, Effect } from 'effector';
import { debounce, pending } from 'patronum';
import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import {
  ContactInstance,
  Contacts,
  ContactsByChannels,
  GetChannelContactsRequest,
  GetChannelContactsResponse,
  GetContactsRequest,
  GetContactsResponse,
} from '@features/socket-chats-reworked/types/contact';
import {
  FIND_CONTACTS,
  fx,
  GET_CONTACTS,
  GET_GROUP_CONTACTS,
} from '../socket/socket.model';

export const MINIMAL_SYMBOLS = 3;

export const fxGetContacts: Effect<GetContactsRequest, GetContactsResponse> =
  fx[GET_CONTACTS];
export const fxGetChannelContacts: Effect<
  GetChannelContactsRequest,
  GetChannelContactsResponse
> = fx[GET_GROUP_CONTACTS];
export const fxFindContacts: Effect<any, any> = fx[FIND_CONTACTS];

export const $contacts = createStore<Contacts>({});
export const $contactsByChannels = createStore<ContactsByChannels>({});

export const clearSearchedContacts = createEvent();
export const $searchedContacts = createStore<Contacts>({}).reset(clearSearchedContacts);
export const $isContactsLoading = pending({ effects: [fxFindContacts] });

export const $searchedContactsArr = combine($searchedContacts, (searchedContacts) => {
  return Object.values(searchedContacts ?? {});
});

export const getContacts = createEvent<ContactInstance['id'][]>();
export const getChannelContacts = createEvent<ChannelInstance['id']>();
export const findContacts = createEvent<string>();
export const findContactsDebounced = debounce({
  source: findContacts,
  timeout: 1000,
});
