import { sample } from 'effector';
import { reset } from 'patronum';
import { makeInitials } from '@features/socket-chats-reworked/lib';
import { genHsl } from '@features/socket-chats-reworked/lib/colors';
import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import { ContactInstance, Contacts } from '@features/socket-chats-reworked/types/contact';
import { baseReset } from '../root/root.model';
import {
  $contacts,
  $contactsByChannels,
  $isContactsLoading,
  $searchedContacts,
  MINIMAL_SYMBOLS,
  findContactsDebounced,
  fxFindContacts,
  fxGetChannelContacts,
  fxGetContacts,
  getChannelContacts,
  getContacts,
} from './contacts.model';

/** Запрос только отсутствующих контактов */
sample({
  source: sample({
    source: $contacts,
    clock: getContacts,
    fn: (contacts, contactsIds) => contactsIds.filter((id) => !(id in contacts)),
  }),
  filter: (data) => Boolean(data.length),
  target: fxGetContacts.prepend((ids: ContactInstance['id'][]) => ({
    contacts: ids,
  })),
});

/** Запрос на контакты канала */
sample({
  clock: getChannelContacts,
  target: fxGetChannelContacts.prepend((id: ChannelInstance['id']) => ({
    channel_key: id,
  })),
});

/** Обработка контактов и их кеширование */
sample({
  clock: [fxGetContacts.done, fxGetChannelContacts.done],
  source: $contacts,
  fn: (state, { result: { resources } }) => {
    const newState: Contacts = {};

    resources.forEach((contact) => {
      newState[contact.id] = {
        ...contact,
        avatar: {
          title: makeInitials(contact?.name),
          color: genHsl(contact.id),
        },
      };
    });

    return { ...state, ...newState };
  },
  target: $contacts,
});

/** Дополнительный стор для хранения контактов по каналам,
 * используется в списке участников чата */
sample({
  clock: fxGetChannelContacts.done,
  source: $contactsByChannels,
  fn: (state, { params: { channel_key }, result: { resources } }) => {
    const newState = state;
    const contacts: Contacts = {};

    resources.forEach((contact) => {
      contacts[contact.id] = {
        ...contact,
        avatar: {
          title: makeInitials(contact?.name),
          color: genHsl(contact.id),
        },
      };
    });

    newState[channel_key] = contacts;

    return { ...state, ...newState };
  },
  target: $contactsByChannels,
});

/** Поиск по контактам */
sample({
  clock: findContactsDebounced,
  filter: (query) => Boolean(query.length >= MINIMAL_SYMBOLS),
  target: fxFindContacts.prepend((query: string) => ({ query })),
});

/** Запись найденных контактов в отдельный стор */
sample({
  clock: fxFindContacts.doneData,
  source: $searchedContacts,
  fn: (state, { resources }: { resources: ContactInstance[] }) => {
    if (resources && Array.isArray(resources)) {
      return resources.reduce((acc, i) => ({ ...acc, [i.id]: i }), state);
    }

    return state;
  },
  target: $searchedContacts,
});

/** Прокидываем найденные контакты в основной стор для кеширования */
sample({
  clock: $searchedContacts,
  source: $contacts,
  fn: (state, contacts) => {
    const newState = state;

    Object.values(contacts).forEach((contact) => {
      if (!newState[contact?.id]) {
        newState[contact?.id] = {
          ...contact,
          avatar: {
            title: makeInitials(contact?.name),
            color: genHsl(contact.id),
          },
        };
      }
    });

    return { ...newState };
  },
  target: $contacts,
});

reset({
  clock: baseReset,
  target: [$contacts, $searchedContacts, $isContactsLoading],
});
