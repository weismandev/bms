import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Alert, Button, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import ErrorIcon from '@mui/icons-material/Error';
import i18n from '@shared/config/i18n';
import { CloseButton, CustomModal, CustomScrollbar } from '@ui/index';
import { ChatsAttachmentItem } from '@features/socket-chats-reworked/types/chat-input';
import {
  $attachments,
  $attachmentsError,
  $hasExcluded,
  $isAttachmentModalVisible,
  closeAttachmentModal,
  removeAttachment,
  sendAttachments,
} from '@features/socket-chats-reworked/models';
import { File } from '../file';
import { useStyles } from './styles';

const { t } = i18n;

const AttachmentItem: FC<ChatsAttachmentItem> = memo(({ attachment }) => {
  const classes = useStyles();

  const handleAttachmentDelete = () => {
    removeAttachment(attachment.id);
  };

  return (
    <div className={classes.attachment}>
      <File
        name={attachment.name}
        size={attachment.size}
        extension={attachment.extension}
      />
      <div className={classes.attachment__info}>
        {attachment.excluded && <ErrorIcon color="error" />}
        <IconButton
          aria-label="delete"
          color="error"
          className={classes.attachment__icon}
          onClick={handleAttachmentDelete}
        >
          <CloseIcon />
        </IconButton>
      </div>
    </div>
  );
});

export const Upload: FC = () => {
  const attachments = useStore($attachments);
  const isUploadVisible = useStore($isAttachmentModalVisible);
  const attachmentsError = useStore($attachmentsError);
  const hasExcluded = useStore($hasExcluded);
  const classes = useStyles();

  const isSendBlock = Boolean(attachments.filter((item) => item.excluded).length);

  const attachmentsList = attachments.map((item) => (
    <AttachmentItem key={item.id} attachment={item} />
  ));

  const handleSendAttachments = () => {
    sendAttachments();
    closeAttachmentModal();
  };

  const renderSizeError = () =>
    hasExcluded ? (
      <Alert severity="error" style={{ marginBottom: 20 }}>
        {t('SomeFilesAreOver50MB')}
      </Alert>
    ) : null;

  const renderError = () =>
    attachmentsError ? (
      <Alert severity="error" style={{ marginBottom: 20 }}>
        {attachmentsError.message}
      </Alert>
    ) : null;

  const renderAttachmentsList = () =>
    attachments.length >= 5 ? (
      <CustomScrollbar style={{ height: 350 }}>
        <div className={classes.modal__files}>{attachmentsList}</div>
      </CustomScrollbar>
    ) : (
      <div className={classes.modal__files}>{attachmentsList}</div>
    );

  const content = (
    <>
      {renderSizeError()}
      {renderError()}
      {renderAttachmentsList()}
    </>
  );

  return (
    <CustomModal
      style={{ padding: 0 }}
      content={content}
      isOpen={isUploadVisible}
      header={
        <div className={classes.modal__header}>
          <CloseButton onClick={closeAttachmentModal} />
          <span className={classes.modal__title}>
            {`${t('SelectedFiles')}: ${attachments.length}`}
          </span>
          <Button
            variant="outlined"
            size="medium"
            onClick={handleSendAttachments}
            disabled={isSendBlock}
          >
            {t('Send')}
          </Button>
        </div>
      }
      titleFontSize={24}
      minWidth="450px"
    />
  );
};
