import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  modal__header: {
    display: 'grid',
    gridTemplateColumns: 'auto 1fr auto',
    alignItems: 'center',
    marginBottom: 10,
    gap: 15,
  },
  modal__title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  modal__files: {
    display: 'grid',
    minHeight: 50,
    maxHeight: 300,
  },
  attachment: {
    display: 'grid',
    gridTemplateColumns: '1fr auto',
    alignItems: 'center',
    gap: 15,
    padding: '7.5px 15px 7.5px 0',

    '&:hover *[class*="attachment__icon"]': {
      visibility: 'visible !important',
    },
  },
  attachment__info: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    gap: 5,
  },
  attachment__icon: {
    visibility: 'hidden',
    opacity: 1,
  },
});
