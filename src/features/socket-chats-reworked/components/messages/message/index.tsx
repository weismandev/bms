import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import PlayArrowRoundedIcon from '@mui/icons-material/PlayArrowRounded';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { getTime } from '@features/socket-chats-reworked/lib';
import { $contacts, $messagesAttachments } from '@features/socket-chats-reworked/models';
import {
  AttachmentMessageProps,
  MessageProps,
} from '@features/socket-chats-reworked/types/common';
import i18n from '@shared/config/i18n';
import { Avatar } from '../../avatar';
import { File } from '../../file';
import { MessageConfirmation } from '../message-confirmation';
import { useStyles } from './styles';

const { t } = i18n;

const TextMessage: FC<AttachmentMessageProps> = memo(({ message }) => {
  const classes = useStyles({
    is_owner: message.is_owner,
    is_hidden: message.is_hidden,
  });

  return <span className={classes.text}>{message.text}</span>;
});

const ImageMessage: FC<AttachmentMessageProps> = memo(({ message, attachment }) => {
  const classes = useStyles({
    is_owner: message.is_owner,
    is_hidden: message.is_hidden,
  });

  const handleImageClick = () => {
    window.open(attachment.url, '_blank');
  };

  return (
    <div className={classes.image__wrap}>
      <div
        className={classes.image}
        style={{
          backgroundImage: `url(${attachment?.url})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }}
        onClick={handleImageClick}
      />
    </div>
  );
});

const FileMessage: FC<AttachmentMessageProps> = memo(({ attachment }) => {
  if (!attachment?.size) return null;

  return (
    <File
      name={attachment.name}
      size={attachment.size}
      extension={attachment.extension}
      url={attachment.url}
    />
  );
});

const VideoMessage: FC<AttachmentMessageProps> = memo(({ message, attachment }) => {
  const classes = useStyles({
    is_owner: message.is_owner,
    is_hidden: message.is_hidden,
  });

  const handleImageClick = () => {
    window.open(attachment.url, '_blank');
  };

  return (
    <div className={classes.image__wrap} onClick={handleImageClick}>
      <PlayArrowRoundedIcon
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          color: 'white',
          fontSize: 74,
          transform: 'translateX(-50%) translateY(-50%)',
          pointerEvents: 'none',
          zIndex: 10,
          cursor: 'pointer',
        }}
      />

      <div
        className={classes.image}
        style={{
          backgroundImage: `url(${attachment?.preview.url})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          filter: 'brightness(0.6)',
        }}
      />
    </div>
  );
});

const VoiceMessage: FC<AttachmentMessageProps> = memo(({ message, attachment }) => {
  if (!attachment) return '...';
  return <audio controls src={attachment.url}></audio>;
});

const ActionMessage: FC<AttachmentMessageProps> = memo(({ attachment }) => {
  const classes = useStyles({});

  if (!attachment) return <div className={classes.action}>...</div>;

  return <div className={classes.action}>{attachment.text}</div>;
});

export const Message: FC<MessageProps> = memo(({ message }) => {
  const contacts = useStore($contacts);
  const messagesAttachments = useStore($messagesAttachments);
  const contact = message?.contact_id ? contacts[message.contact_id] : null;
  const isImage = message.type === 'image' || message.type === 'video';
  const classes = useStyles({
    is_owner: message.is_owner,
    is_hidden: message.is_hidden,
    is_image: isImage,
  });

  const attachment = message.id ? messagesAttachments[message.id] : {};

  if (message.type === 'action') {
    return <ActionMessage message={message} attachment={attachment} />;
  }

  const renderContent = () => {
    switch (message.type) {
      case null:
        return <TextMessage message={message} />;
      case 'image':
        return <ImageMessage message={message} attachment={attachment} />;
      case 'file':
        return <FileMessage message={message} attachment={attachment} />;
      case 'video':
        return <VideoMessage message={message} attachment={attachment} />;
      case 'voice':
        return <VoiceMessage message={message} attachment={attachment} />;
      default:
        return null;
    }
  };

  const renderAvatar = () => {
    if (!message.is_owner)
      return (
        <Avatar
          size="mini"
          title={contact?.avatar?.title}
          color={contact?.avatar?.color}
        />
      );
  };

  const renderName = () => {
    if (isImage) return null;

    const name = contact?.name || t('Anonymous');
    const address = contact?.meta?.address?.number;

    if (!message.is_owner)
      return (
        <div className={classes.message__user}>
          <span className={classes.message__userTitle}> {name}</span>
          <span className={classes.message__userAddress}>
            {address?.prefix}
            {address?.index}
          </span>
        </div>
      );
    return null;
  };

  const renderHiddenInfo = () => {
    if (message.is_hidden)
      return (
        <div className={classes.message__hidden}>
          <span className={classes.message__hiddenText}>
            {t('HiddenFromTheResident')}
          </span>
          <VisibilityOffIcon className={classes.message__hiddenIcon} />
        </div>
      );
  };

  const messageDate = getTime(message.created_at_date);

  return (
    <div className={classes.message}>
      {renderAvatar()}

      <div className={classes.message__content}>
        {renderHiddenInfo()}

        {renderName()}

        <div className={classes.message__inner}>
          {renderContent()}

          <span className={classes.message__meta}>
            <span className={classes.message__date}>{messageDate}</span>
            {message.is_owner && <MessageConfirmation read={message.read} />}
          </span>
        </div>
      </div>
    </div>
  );
});
