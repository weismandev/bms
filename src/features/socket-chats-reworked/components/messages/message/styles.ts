import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  message: ({ is_owner }: ChatsMessageStyles) => ({
    position: 'relative',
    display: 'grid',
    gridTemplateColumns: is_owner ? 'auto' : 'auto auto',
    gap: 5,
    alignItems: 'end',
    alignContent: 'end',
    //  margin: '6px 16px 0 16px',
    maxWidth: is_owner ? 'calc(80% - 16px)' : '67%',
    justifySelf: is_owner ? 'end' : 'start',
    width: 'auto',
    // animation: '$showIn 0.2s',
  }),
  message__content: ({ is_owner, is_hidden, is_image }: ChatsMessageStyles) => ({
    position: 'relative',
    padding: is_image ? '0' : '5px 8px 6px 8px',
    wordBreak: 'break-word',
    overflow: 'hidden',
    margin: 0,
    backgroundColor: is_hidden ? 'white' : is_owner ? '#e5f2ff' : '#f0f2f5',
    borderRadius:
      (is_image && '6px !important') || is_owner
        ? '12px 12px 0px 12px'
        : '12px 12px 12px 0px',
    border: is_hidden ? '1px dashed rgb(162, 172, 180)' : 'unset',
    gridColumn: is_owner ? 1 : 2,
    gridRow: 1,
  }),
  message__meta: ({ is_image }: ChatsMessageStyles) => ({
    position: 'absolute',
    right: 6,
    bottom: is_image ? 6 : 7,
    background: is_image ? 'rgba(0,0,0,.4)' : '',
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyItems: 'end',
    float: 'right',
    marginTop: 2,
    marginLeft: 7,
    opacity: 0.8,
    padding: is_image ? '0 5px' : '',
    borderRadius: '100px',

    '& *': {
      color: is_image ? 'white' : '',
    },
  }),
  message__date: {
    color: 'rgba(104, 108, 114, 0.75)',
    fontSize: 12,
  },
  //
  message__user: ({ is_image }: ChatsMessageStyles) => ({
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'start',
    alignItems: 'center',
    gap: 8,
    padding: is_image ? '5px 8px 6px 8px' : 0,
  }),
  message__userTitle: {
    fontSize: 14,
    color: '#65aadd',
    fontWeight: 500,
  },
  message__userAddress: {
    fontSize: 12,
    opacity: 0.5,
  },
  message__hidden: ({ is_image }: ChatsMessageStyles) => ({
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    gap: 10,
    padding: is_image ? '3px 8px' : 0,
  }),
  message__hiddenText: {
    fontStyle: 'italic',
    fontSize: 12,
    color: 'rgb(162, 172, 180)',
  },
  message__hiddenIcon: {
    fontSize: 18,
    color: 'rgb(162, 172, 180)',
  },
  message__inner: ({ is_image }: ChatsMessageStyles) => ({
    width: is_image ? 320 : 'auto',
    height: is_image ? 320 : 'auto',
  }),
  message__select: {},
  message__selectIcon: {},
  //
  text: {
    paddingRight: 60,
    whiteSpace: 'pre-wrap',
  },
  //
  action: {
    maxWidth: '50%',
    display: 'grid',
    justifyContent: 'center',
    justifyItems: 'center',
    justifySelf: 'center',
    padding: '2px 8px',
    borderRadius: 12,
    background: 'rgba(0, 0, 0, 0.1)',
    color: 'rgb(112,117,121)',
    textAlign: 'center',
    overflowWrap: 'anywhere',
  },
  //
  image__wrap: {
    position: 'relative',
    display: 'grid',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    justifyItems: 'center',
    width: 320,
    height: 320,
    borderRadius: 10,
  },
  image: {
    objectFit: 'cover',
    width: 320,
    height: 320,
    borderRadius: 10,
    cursor: 'pointer',
  },
});
