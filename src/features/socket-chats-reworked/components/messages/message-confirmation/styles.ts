import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  confirmation__icon: {
    width: 15,
    height: 15,
    color: 'rgb(77,205,94)',
    margin: ' 0 4px 0 3px',
  },
});
