import { FC, memo } from 'react';
import DoneRoundedIcon from '@mui/icons-material/DoneRounded';
import DoneAllRoundedIcon from '@mui/icons-material/DoneAllRounded';
import { useStyles } from './styles';

export const MessageConfirmation: FC<ChatsMessageMeta> = memo(({ read }) => {
  const classes = useStyles();

  return read ? (
    <DoneAllRoundedIcon className={classes.confirmation__icon} />
  ) : (
    <DoneRoundedIcon className={classes.confirmation__icon} />
  );
});
