import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Checkbox } from '@mui/material';
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';
import RadioButtonUncheckedRoundedIcon from '@mui/icons-material/RadioButtonUncheckedRounded';
import {
  $isSelectMode,
  $selectedMessages,
  deselectMessage,
  selectMessage,
} from '@features/socket-chats-reworked/models';
import { MessageProps } from '@features/socket-chats-reworked/types/common';
import { Message } from '../message';
import { useStyles } from './styles';

export const MessageContainer: FC<MessageProps> = memo(({ message }) => {
  const selectedMessages = useStore($selectedMessages);
  const isSelectMode = useStore($isSelectMode);
  const isSelectionAlowed = isSelectMode && message.type !== 'action';
  const selected = selectedMessages.indexOf(message.id) > -1;
  const classes = useStyles({ isSelectionAlowed, selected });

  const handleToggleSelect = () => {
    if (isSelectionAlowed) {
      selected ? deselectMessage(message.id) : selectMessage(message.id);
    }
  };

  return (
    <div className={classes.messageContainer} onClick={handleToggleSelect}>
      {message.type !== 'action' && (
        <Checkbox
          className={classes.messageContainer__checkbox}
          icon={
            <RadioButtonUncheckedRoundedIcon
              className={classes.messageContainer__checkboxIcon}
            />
          }
          checkedIcon={<CheckCircleRoundedIcon />}
          checked={selected}
          onClick={handleToggleSelect}
        />
      )}

      <Message message={message} />
    </div>
  );
});
