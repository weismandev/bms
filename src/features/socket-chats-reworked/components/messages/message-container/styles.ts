import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  messageContainer: ({ isSelectionAlowed, selected }: any) => ({
    position: 'relative',
    display: 'grid',
    width: '100%',
    maxWidth: 650,
    margin: '2.5px auto',
    padding: isSelectionAlowed ? '5px 5px 5px 40px' : '5px',
    borderRadius: 10,
    background: selected ? 'rgba(51, 144, 236, 0.05)' : '',
    cursor: isSelectionAlowed ? 'pointer' : '',
    transition: 'all 0.2s',

    '&:hover': {
      background: isSelectionAlowed ? 'rgba(51, 144, 236, 0.05)' : '',
    },

    '& *': {
      pointerEvents: isSelectionAlowed ? 'none' : 'auto',
      userSelect: isSelectionAlowed ? 'none' : 'unset',
    },
  }),
  messageContainer__checkbox: ({ isSelectionAlowed, selected }: any) => ({
    position: 'absolute',
    left: 0,
    bottom: 0,
    transform: isSelectionAlowed ? 'scale(1)' : 'scale(0)',
    transition: 'transform 0.2s',
  }),
  messageContainer__checkboxIcon: {
    color: 'rgba(112,117,121, 0.5)',
  },
}));
