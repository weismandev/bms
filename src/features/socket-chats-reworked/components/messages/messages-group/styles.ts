import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  messagesGroup: {
    display: 'grid',
    alignSelf: 'end',
    justifySelf: 'center',
    justifyContent: 'center',
    maxWidth: '60%',
    minWidth: 650,
    margin: '0 auto',
  },
  messagesGroup_item: {
    maxWidth: 146,
    display: 'grid',
    justifySelf: 'center',
    justifyContent: 'center',
    justifyItems: 'center',
    margin: '12px auto 6px auto',
    padding: '0 8px',
    borderRadius: 12,
    color: 'rgba(112,117,121, 0.7)',
    background: '#ececec',
    textAlign: 'center',
  },
});
