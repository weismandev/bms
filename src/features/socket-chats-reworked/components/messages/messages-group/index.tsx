import { FC, forwardRef } from 'react';
import { useStyles } from './styles';

export const MessagesGroup: FC = forwardRef((props: any, ref: any) => {
  const classes = useStyles();

  return (
    <div className={classes.messagesGroup} ref={ref}>
      <div {...props} className={classes.messagesGroup_item}></div>
    </div>
  );
});
