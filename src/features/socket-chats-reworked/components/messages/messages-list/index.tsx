import { FC, useEffect, useMemo, useRef } from 'react';
import { GroupedVirtuoso, VirtuosoHandle } from 'react-virtuoso';
import { useStore } from 'effector-react';
import lodashGroupBy from 'lodash/groupBy';
import { getRelativeDate } from '@features/socket-chats-reworked/lib';
import {
  $currentChatId,
  $currentMessages,
  $isChatLoading,
  $messages,
  $updatedCount,
  startReached,
} from '@features/socket-chats-reworked/models';
import { ChatEmpty } from '../../chat/chat-empty';
import { ChatLoaderScreen } from '../../chat/chat-loader-screen';
import { MessageContainer } from '../message-container';
import { MessagesGroup } from '../messages-group';
import { useStyles } from './styles';

export const MessagesList: FC = () => {
  const currentChatId = useStore($currentChatId);
  const isChatLoading = useStore($isChatLoading);
  const updatedCount = useStore($updatedCount);
  const messages = useStore($currentMessages);
  const initialIndex = messages ? messages.length - 1 : 0;
  const virtuoso = useRef<VirtuosoHandle>(null);
  const classes = useStyles();

  useEffect(() => {
    if (virtuoso?.current) {
      virtuoso.current.scrollToIndex({
        index: messages.length ?? 0,
      });
    }
  });

  useEffect(() => {
    window.addEventListener('error', (e) => {
      if (
        e.message === 'ResizeObserver loop completed with undelivered notifications.' ||
        e.message === 'ResizeObserver loop limit exceeded'
      ) {
        e.stopImmediatePropagation();
      }

      if (e.message === 'ResizeObserver loop limit exceeded') {
        const resizeObserverErrDiv = document.getElementById(
          'webpack-dev-server-client-overlay-div'
        );
        const resizeObserverErr = document.getElementById(
          'webpack-dev-server-client-overlay'
        );
        if (resizeObserverErr) {
          resizeObserverErr.setAttribute('style', 'display: none');
        }
        if (resizeObserverErrDiv) {
          resizeObserverErrDiv.setAttribute('style', 'display: none');
        }
      }
    });
  }, []);

  const groups = useMemo(
    () =>
      lodashGroupBy(messages, (message) => {
        if (message) return getRelativeDate(message.created_at_date, 'full');
      }),
    [messages]
  );

  const groupCounts = useMemo(
    () => Object.keys(groups).map((key) => groups[key].length),
    [messages]
  );

  if (isChatLoading) return <ChatLoaderScreen />;

  if (!messages?.length) return <ChatEmpty />;

  const groupContent = (index: number) => {
    return Object.keys(groups)[index];
  };

  const itemContent = (index: number) => <MessageContainer message={messages[index]} />;

  return (
    <div className={classes.list__wrap}>
      <GroupedVirtuoso
        ref={virtuoso}
        components={{
          Group: MessagesGroup,
        }}
        initialTopMostItemIndex={initialIndex}
        groupCounts={groupCounts}
        groupContent={groupContent}
        itemContent={itemContent}
        defaultItemHeight={32}
        followOutput
        atTopStateChange={(isReached) => {
          if (isReached) {
            startReached();
          }
        }}
      />
    </div>
  );
};
