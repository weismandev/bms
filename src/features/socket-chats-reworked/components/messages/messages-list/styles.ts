import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  list: {
    display: 'grid',
  },
  list__wrap: {
    height: 'calc(100%)',
    padding: '0',

    '& *::-webkit-scrollbar': {
      width: 7.5,
    },

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      borderRadius: 6,
    },
  },
});
