import { CircularProgress } from '@mui/material';

import i18n from '@shared/config/i18n';

const { t } = i18n;

export const MessagesHeader = () => {
  const loading = false;

  return (
    <div
      style={{
        padding: '2rem',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      {loading ? <CircularProgress /> : <button>{t('DownloadEarlyPosts')}</button>}
    </div>
  );
};
