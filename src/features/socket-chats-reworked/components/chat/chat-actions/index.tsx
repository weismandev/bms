import { FC } from 'react';
import { useStore } from 'effector-react';
import { IconButton } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import NotificationsOffOutlinedIcon from '@mui/icons-material/NotificationsOffOutlined';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import i18n from '@shared/config/i18n';
import { Menu, Popover } from '@ui/index';
import {
  $chatData,
  $isReadonly,
  $isSelectMode,
  leaveChannel,
  setChannelNotificationType,
  setDrawerVisibility,
  toggleSelectMode,
} from '@features/socket-chats-reworked/models';
import { useStyles } from './styles';

const { t } = i18n;

interface Props {
  hideLeaveButton: boolean;
}

export const ChatActions: FC<Props> = ({ hideLeaveButton }) => {
  const chatData = useStore($chatData);
  const isSelectMode = useStore($isSelectMode);
  const isReadonly = useStore($isReadonly);
  const isMuted = chatData?.notification_type === 'none';
  const classes = useStyles();

  if (!chatData?.id || isReadonly) return null;

  const handleToggleSelect = () => {
    toggleSelectMode();
  };

  const handleMuteClick = () => {
    setChannelNotificationType({
      channel_id: chatData.id,
      notification_type: isMuted ? 'all' : 'none',
    });
  };

  const handleLeaveClick = () => {
    leaveChannel(chatData?.id);
  };

  const handleInfoClick = () => {
    setDrawerVisibility(true);
  };

  const baseActions = [
    {
      title: isSelectMode ? t('Deselect') : t('HighlightMessages'),
      icon: <CheckCircleOutlineIcon />,
      onClick: handleToggleSelect,
    },
    {
      title: isMuted ? t('EnableNotifications') : t('DisableNotifications'),
      icon: isMuted ? (
        <NotificationsNoneOutlinedIcon />
      ) : (
        <NotificationsOffOutlinedIcon />
      ),
      onClick: handleMuteClick,
    },
  ];

  const actions = hideLeaveButton
    ? baseActions
    : [
        ...baseActions,
        {
          title: t('LeaveTheChat'),
          icon: <DeleteOutlineIcon />,
          onClick: handleLeaveClick,
        },
      ];

  return (
    <div className={classes.actionsContainer}>
      <Popover
        trigger={
          <IconButton size="medium">
            <MoreVertIcon />
          </IconButton>
        }
      >
        <Menu items={actions} />
      </Popover>

      <IconButton size="medium" onClick={handleInfoClick}>
        <InfoOutlinedIcon />
      </IconButton>
    </div>
  );
};
