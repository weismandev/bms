import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  actionsContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    gap: 10,
  },
});
