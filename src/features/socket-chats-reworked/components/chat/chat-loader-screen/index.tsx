import { FC } from 'react';
import { CircularProgress } from '@mui/material';
import { useStyles } from './styles';

export const ChatLoaderScreen: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.chatLoader__wrap}>
      <div className={classes.chatLoader}>
        <CircularProgress className={classes.chatLoader__progress} />
      </div>
    </div>
  );
};
