import { FC } from 'react';
import { useStore } from 'effector-react';
import { IconButton } from '@mui/material';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import DeleteOutlineRoundedIcon from '@mui/icons-material/DeleteOutlineRounded';
import ReplyAllRoundedIcon from '@mui/icons-material/ReplyAllRounded';
import i18n from '@shared/config/i18n';
import {
  $selectedMessages,
  changeConfirmDeleteMessagesStatus,
  deleteMessages,
  toggleSelectMode,
} from '../../../models';
import { useStyles } from './styles';

const { t } = i18n;

export const ChatSelectPanel: FC = () => {
  const selectedMessages = useStore($selectedMessages);
  const selectedLength = selectedMessages.length;
  const classes = useStyles();

  const handleCancelClick = () => {
    toggleSelectMode();
  };

  const handleDeleteClick = () => {
    changeConfirmDeleteMessagesStatus(true);
    //deleteMessages();
  };

  return (
    <div className={classes.chatSelect}>
      <div className={classes.chatSelect__content}>
        <IconButton size="medium" onClick={handleCancelClick}>
          <CloseRoundedIcon />
        </IconButton>

        <span className={classes.chatSelect__count}>
          {`${t('MessagesSelected')}: ${selectedLength}`}
        </span>

        <div className={classes.chatSelect__actions}>
          <IconButton
            size="medium"
            style={{ transform: 'scale(-1, 1)' }}
            color="primary"
            disabled
          >
            <ReplyAllRoundedIcon />
          </IconButton>

          <IconButton
            size="medium"
            color="error"
            disabled={!selectedLength}
            onClick={handleDeleteClick}
          >
            <DeleteOutlineRoundedIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
};
