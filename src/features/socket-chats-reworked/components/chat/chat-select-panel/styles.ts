import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatSelect: {
    height: 66,
    display: 'grid',
    alignSelf: 'end',
    padding: '0 16px',
    background: 'white',
  },
  chatSelect__content: {
    display: 'grid',
    gridTemplateColumns: 'auto 1fr auto auto',
    alignItems: 'center',
    gap: 15,
    width: 650,
    margin: '0 auto',
  },
  chatSelect__count: {
    fontSize: 16,
    fontWeight: 500,
  },
  chatSelect__actions: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    gap: 5,
  },
});
