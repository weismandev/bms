import { FC, memo, MouseEvent, useState } from 'react';
import ReactFileReader from 'react-file-reader';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import SendIcon from '@mui/icons-material/Send';
import PhotoIcon from '@mui/icons-material/Photo';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import {
  CircularProgress,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  TextareaAutosize,
  Tooltip,
} from '@mui/material';
import { useStore } from 'effector-react';
import {
  $attachmentsLoading,
  $chatData,
  $isHidden,
  $messageInput,
  changeIsHidden,
  changeMessageInput,
  sendMessage,
  setAttachments,
} from '@features/socket-chats-reworked/models';
import { useTranslation } from 'react-i18next';
import { Upload } from '../../upload';

import { useStyles } from './styles';

export const ChatInput: FC = memo(() => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const messageInput = useStore($messageInput);
  const chatData = useStore($chatData);
  const isHidden = useStore($isHidden);
  const attachmentsLoading = useStore($attachmentsLoading);
  const classes = useStyles({ isHidden });

  const { t } = useTranslation();

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMessageSend = () => {
    if (messageInput && chatData) {
      sendMessage({
        channel_key: chatData.id,
        text: messageInput,
        is_hidden: isHidden,
      });
    }
  };

  const handleKeyDown = (e: any) => {
    if (e.key === 'Enter' && !e.shiftKey) {
      handleMessageSend();
      e.preventDefault();
    }

    if (e.shiftKey && e.key === 'Enter') {
      e.preventDefault();
      changeMessageInput(`${messageInput}\n`);
    }

    return null;
  };

  const handleChangeMessageInput = (e: any) => {
    changeMessageInput(e.target.value);
  };

  const handleChangeMessageHidden = () => {
    changeIsHidden(!isHidden);
  };

  const handleFiles = (files: any) => {
    setAttachments({ mode: 'file', files: files });
    handleClose();
  };

  const handleImageFiles = (files: any) => {
    setAttachments({ mode: 'image', files: files });
    handleClose();
  };

  const renderHiddenModeTitle = () =>
    isHidden ? t('Chats.HideMessageFromResident') : t('Chats.ShowMessageForAllMembers');

  return (
    <div className={classes.chatInput__wrapper}>
      <Upload />

      <div className={classes.chatInput}>
        <div className={classes.chatInput__header}>
          <span className={classes.chatInput__hiddenModeTitle}>
            {renderHiddenModeTitle()}
          </span>
        </div>

        <div className={classes.chatInput__content}>
          <Tooltip title={t('Chats.WillHideMessageFromResident') as string}>
            <IconButton
              onClick={handleChangeMessageHidden}
              className={classes.chatInput__icon}
              style={{ color: isHidden ? 'white' : '#9EA6AE' }}
            >
              <VisibilityOffIcon />
            </IconButton>
          </Tooltip>

          <TextareaAutosize
            className={classes.chatInput__input}
            value={messageInput}
            placeholder={t('Message')}
            maxRows={4}
            onChange={handleChangeMessageInput}
            onKeyDown={handleKeyDown}
          />

          {attachmentsLoading ? (
            <CircularProgress
              className={classes.chatInput__icon}
              color="primary"
              size={24}
            />
          ) : (
            <IconButton className={classes.chatInput__icon} onClick={handleClick}>
              <AttachFileIcon />
            </IconButton>
          )}

          <IconButton color="primary" onClick={handleMessageSend}>
            <SendIcon className={classes.chatInput__icon} />
          </IconButton>

          <Menu
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            <ReactFileReader
              fileTypes={['image/jpeg', 'image/png']}
              multipleFiles={true}
              handleFiles={handleImageFiles}
            >
              <MenuItem>
                <ListItemIcon style={{ color: 'rgb(162, 172, 180)' }}>
                  <PhotoIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText>{t('Photo')}</ListItemText>
              </MenuItem>
            </ReactFileReader>

            <ReactFileReader fileTypes="*" multipleFiles={true} handleFiles={handleFiles}>
              <MenuItem>
                <ListItemIcon style={{ color: 'rgb(162, 172, 180)' }}>
                  <InsertDriveFileIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText>{t('File')}</ListItemText>
              </MenuItem>
            </ReactFileReader>
          </Menu>
        </div>
      </div>
    </div>
  );
});
