import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatInput__wrapper: ({ isHidden }: any) => ({
    height: 'auto',
    display: 'grid',
    alignSelf: 'end',
    padding: '5px 16px 5px 16px',
    borderRadius: 12,
    background: isHidden ? 'rgb(51, 144, 236)' : 'white',
    transition: '0.2s background',
  }),
  chatInput__content: {
    display: 'grid',
    gridTemplateColumns: 'auto 1fr auto auto',
    alignItems: 'center',
    gap: 2,
  },
  chatInput: {
    display: 'grid',
    width: 650,
    margin: '0 auto',
  },
  chatInput__input: ({ isHidden }: any) => ({
    border: 'none',
    outline: 'none',
    fontSize: 16,
    fontWeight: 'normal !important',
    whiteSpace: 'pre-wrap',
    verticalAlign: 'middle',
    overflowY: 'scroll',
    msOverflowStyle: 'none',
    scrollbarWidth: 'none',
    cursor: 'text',
    resize: 'none',
    background: 'transparent',
    transition: '0.2s color',
    color: isHidden ? 'white' : '',

    '&::-webkit-scrollbar': {
      display: 'none',
    },

    '&::placeholder': {
      color: isHidden ? 'rgba(255, 255, 255, 0.7)' : '#a2acb4',
    },
  }),
  chatInput__icon: ({ isHidden }: any) => ({
    color: isHidden ? 'rgba(255, 255, 255, 0.7)' : '#a2acb4',
    transition: '0.2s color',
  }),
  chatInput__header: {
    display: 'grid',
    paddingLeft: 12,
  },
  chatInput__hiddenModeTitle: ({ isHidden }: any) => ({
    gridColumn: '1, -1',
    fontStyle: 'italic',
    fontSize: 13,
    color: isHidden ? 'rgba(255, 255, 255, 0.7)' : ' #9EA6AE',
    transition: '0.2s all',
    userSelect: 'none',
  }),
});
