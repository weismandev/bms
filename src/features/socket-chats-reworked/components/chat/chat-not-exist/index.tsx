import { CircularProgress } from '@mui/material';
import { FC } from 'react';
import i18n from '@shared/config/i18n';
import { useStyles } from './styles';

const { t } = i18n;

export const ChatNotExist: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.notExist__wrap}>
      <div className={classes.notExist}>
        <span className={classes.notExist__title}>{t('ChatIsMissing')}</span>
      </div>
    </div>
  );
};
