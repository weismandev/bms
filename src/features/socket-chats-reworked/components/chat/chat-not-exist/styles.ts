import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  notExist__wrap: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    width: '100%',
    height: '100%',
    background: 'white',
    borderRadius: 12,
  },
  notExist: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    gap: 10,
    padding: '12px 16px',
    background: '#ececec',
    borderRadius: 24,
  },
  notExist__title: {
    fontSize: 16,
    fontWeght: 'bold',
    color: 'rgba(112,117,121, 0.7)',
  },
});
