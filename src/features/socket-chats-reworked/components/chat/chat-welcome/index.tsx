import { FC } from 'react';
import i18n from '@shared/config/i18n';
import { useStyles } from './styles';

const { t } = i18n;

export const ChatWelcome: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.chatWelcome}>
      <span className={classes.chatWelcome__title}>
        {t('ChooseWhoYouWouldLikeToWriteTo')}
      </span>
    </div>
  );
};
