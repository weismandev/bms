import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatWelcome: {
    display: 'grid',
    height: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    background: 'white',
    borderRadius: '0 12px 12px 0',
  },
  chatWelcome__title: {
    display: 'grid',
    padding: '12px 16px',
    borderRadius: 20,
    background: '#ececec',
    color: 'rgba(112,117,121, 0.7)',
  },
});
