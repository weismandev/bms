import { FC } from 'react';
import { useStore } from 'effector-react';
import { Skeleton } from '@mui/material';
import { $chatData, $contacts } from '@features/socket-chats-reworked/models';
import { Toolbar } from '@ui/index';
import { Avatar } from '../../avatar';
import { ChatActions } from '../chat-actions';
import { useStyles } from './styles';

interface Props {
  hideLeaveButton: boolean;
}

export const ChatHeader: FC<Props> = ({ hideLeaveButton }) => {
  const chatData = useStore($chatData);
  const contacts = useStore($contacts);
  const contact = chatData?.contact_id ? contacts[chatData.contact_id] : null;
  const classes = useStyles();

  const renderAvatar = () => {
    const title = contact ? contact.avatar?.title : chatData?.avatar?.title;
    const color = contact ? contact.avatar?.color : chatData?.avatar?.color;

    return <Avatar size="small" title={title} color={color} />;
  };

  const renderTitle = () => {
    const title = chatData?.title;

    if (title) return <span className={classes.chatHeader__title}>{title}</span>;

    return null;
  };

  const renderDesc = () => {
    const desc = contact?.address;

    return desc ? (
      <span className={classes.chatHeader__desc}>{contact.address}</span>
    ) : null;
  };

  return (
    <Toolbar classes={{ root: classes.chatHeader }}>
      <div className={classes.chatHeader__content}>
        {renderAvatar()}
        <div className={classes.chatHeader__info}>
          {renderTitle()}

          {renderDesc()}
        </div>
      </div>

      <ChatActions hideLeaveButton={hideLeaveButton} />
    </Toolbar>
  );
};
