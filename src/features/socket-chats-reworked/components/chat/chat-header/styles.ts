import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  chatHeader: {
    height: 60,
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'space-between',
    padding: '0 16px',
    borderBottom: '1px solid #f6f6f8',
  },
  chatHeader__content: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    alignContent: 'center',
    gap: 10,
  },
  chatHeader__info: {
    display: 'grid',
    gap: 5,
  },
  chatHeader__title: {
    fontSize: 16,
    fontWeight: 500,
    lineHeight: 1,
  },
  chatHeader__desc: {
    fontSize: 12,
    color: 'rgb(112,117,121)',
    lineHeight: 1,
  },
});
