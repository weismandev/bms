import { FC } from 'react';
import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { IconButton, Skeleton, Tooltip, Typography } from '@mui/material';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import {
  DrawerContact,
  DrawerContactAddress,
  DrawerContactInfo,
  DrawerContactMeta,
  DrawerContactName,
  DrawerContacts,
  DrawerContainer,
  DrawerContent,
  DrawerHeader,
  DrawerItem,
  DrawerItemHeader,
  DrawerSkeletons,
  DrawerTitle,
} from '@features/socket-chats-reworked/components/chat/chat-drawer/styled';
import {
  $chatData,
  $contactsByChannels,
  $currentChatId,
  $isChatLoading,
  $isDrawerVisible,
  setDrawerVisibility,
} from '@features/socket-chats-reworked/models';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import { Avatar } from '@features/socket-chats-reworked/components/avatar';
import PodcastsOutlinedIcon from '@mui/icons-material/PodcastsOutlined';
import BadgeOutlinedIcon from '@mui/icons-material/BadgeOutlined';
import SupportOutlinedIcon from '@mui/icons-material/SupportOutlined';
import AodOutlinedIcon from '@mui/icons-material/AodOutlined';

export const ChatDrawer: FC = () => {
  const { t } = useTranslation();
  const isDrawerVisible = useStore($isDrawerVisible);
  const chatData = useStore($chatData);
  const chatId = useStore($currentChatId);
  const contacts = useStore($contactsByChannels);
  const isChatLoading = useStore($isChatLoading);
  const chatContacts = contacts[chatId] || null;
  const chatContactsLength = chatContacts ? Object.keys(chatContacts).length : null;
  const isContactsLoading = isChatLoading && !chatContactsLength;

  const handleCloseClick = () => {
    setDrawerVisibility(false);
  };

  const renderAvatar = (contact) => {
    const title = contact ? contact.avatar?.title : chatData?.avatar?.title;
    const color = contact ? contact.avatar?.color : chatData?.avatar?.color;

    return <Avatar size="medium" title={title} color={color} />;
  };

  const renderRole = (role: string) => {
    switch (role) {
      case 'BOT':
        return (
          <Tooltip title={t('Bot')}>
            <PodcastsOutlinedIcon />
          </Tooltip>
        );
      case 'support':
        return (
          <Tooltip title={t('TechSupport')}>
            <SupportOutlinedIcon />
          </Tooltip>
        );
      case 'mc_manager':
        return (
          <Tooltip title={t('ManagementCE')}>
            <BadgeOutlinedIcon />
          </Tooltip>
        );
      case 'builder_manager':
        return (
          <Tooltip title={t('Management')}>
            <AodOutlinedIcon />
          </Tooltip>
        );
      default:
        return null;
    }
  };

  const renderContacts = () => {
    if (!chatContacts) return null;

    return Object.values(chatContacts).map((contact) => (
      <DrawerContact>
        {renderAvatar(contact)}

        <DrawerContactInfo>
          <DrawerContactName>
            {contact.meta.full_name.name} {contact.meta.full_name.surname}
          </DrawerContactName>
          <DrawerContactAddress>{contact.address}</DrawerContactAddress>
        </DrawerContactInfo>

        <DrawerContactMeta>{renderRole(contact.role)}</DrawerContactMeta>
      </DrawerContact>
    ));
  };

  return (
    <DrawerContainer isVisible={isDrawerVisible}>
      <DrawerHeader>
        <DrawerTitle>{t('ChatInformation')}</DrawerTitle>
        <IconButton size="medium" onClick={handleCloseClick}>
          <CloseRoundedIcon />
        </IconButton>
      </DrawerHeader>

      <DrawerContent>
        {isContactsLoading ? (
          <DrawerSkeletons>
            {[1, 2, 3, 4, 5].map(() => (
              <DrawerContact>
                <Skeleton variant="circular" width={54} height={54} />
                <Skeleton variant="rounded" width={210} height={20} />
              </DrawerContact>
            ))}
          </DrawerSkeletons>
        ) : (
          <DrawerItem>
            <DrawerItemHeader>
              <PeopleAltOutlinedIcon />
              <Typography variant="subtitle2">
                <span>{t('Participants')}</span>
                <span>:</span>
                <span>{chatContactsLength}</span>
              </Typography>
            </DrawerItemHeader>

            <DrawerContacts>{renderContacts()}</DrawerContacts>
          </DrawerItem>
        )}
      </DrawerContent>
    </DrawerContainer>
  );
};
