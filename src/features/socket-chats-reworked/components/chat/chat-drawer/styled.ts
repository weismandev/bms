import styled from '@emotion/styled';

export const DrawerContainer = styled.div<{ isVisible: boolean }>(({ isVisible }) => ({
  position: 'absolute',
  top: 0,
  right: 0,
  width: '100%',
  height: '100%',
  maxWidth: 400,
  background: 'white',
  boxShadow: 'rgb(17 17 26 / 5%) 0px 4px 16px, rgb(17 17 26 / 5%) 0px 8px 32px',
  transition: 'all 0.2s',
  transform: isVisible ? 'translateX(0%)' : 'translateX(100%)',
  zIndex: 100,
  display: 'flex',
  flexDirection: 'column',
}));

export const DrawerHeader = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: '1fr auto',
  alignItems: 'center',
  gap: 15,
  padding: '10px 15px 10px 20px',
}));

export const DrawerTitle = styled.span(() => ({
  fontSize: 16,
  fontWeight: 500,
}));

export const DrawerSkeletons = styled.div(() => ({
  display: 'grid',
  gap: 10,
  padding: 15,
}));

export const DrawerContent = styled.div(() => ({
  display: 'grid',
  gap: 10,
  height: '100%',
  overflow: 'auto',
}));

export const DrawerItem = styled.div(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 15,
}));

export const DrawerItemHeader = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
  gap: 10,
  padding: 15,
  border: '1px solid rgb(244, 244, 245)',
  borderLeft: 'none',
  borderRight: 'none',

  '& svg': {
    fill: 'rgb(162, 172, 180)',
  },
}));

export const DrawerContacts = styled.div(() => ({
  display: 'grid',
  gap: 10,
  padding: '0 15px',
}));

export const DrawerContact = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto 1fr auto',
  justifyContent: 'start',
  justifyItems: 'start',
  alignItems: 'center',
  alignContent: 'center',
  gap: 10,
  overflow: 'scroll',
}));

export const DrawerContactInfo = styled.div(() => ({
  display: 'grid  ',
}));

export const DrawerContactName = styled.span(() => ({
  fontWeight: 500,
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
}));

export const DrawerContactAddress = styled.span(() => ({
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  fontSize: 11,
  color: 'rgb(112,117,121);',
}));

export const DrawerContactMeta = styled.div(() => ({
  display: 'grid',

  '& svg': {
    fill: 'rgb(162, 172, 180)',
  },
}));
