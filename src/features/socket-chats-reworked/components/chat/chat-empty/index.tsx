import { FC } from 'react';
import i18n from '@shared/config/i18n';
import { useStyles } from './styles';

const { t } = i18n;

export const ChatEmpty: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.chatEmpty__wrapper}>
      <div className={classes.chatEmpty}>
        <span className={classes.chatEmpty__title}>{t('ThereAreNoMessagesHereYet')}</span>
      </div>
    </div>
  );
};
