import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatEmpty__wrapper: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    width: '100%',
    height: '100%',
  },
  chatEmpty: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    padding: '12px 16px',
    background: '#ececec',
    borderRadius: 24,
  },
  chatEmpty__title: {
    fontSize: 16,
    fontWeght: 'bold',
    color: 'rgba(112,117,121, 0.7)',
  },
});
