import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatFooter: {},
  chatFooter__readonly: {
    display: 'grid',
    alignItems: 'center',
    justifyContent: 'center',
    height: 58,
    padding: 10,
    color: '#686c72',
    fontSize: 16,
  },
});
