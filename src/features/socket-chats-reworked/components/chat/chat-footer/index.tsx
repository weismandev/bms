import { FC } from 'react';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { ChatInput } from '@features/socket-chats-reworked/components/chat/chat-input';
import { $isReadonly, $isSelectMode } from '@features/socket-chats-reworked/models';
import { ChatSelectPanel } from '../chat-select-panel';
import { useStyles } from './styles';

const { t } = i18n;

export const ChatFooter: FC = () => {
  const isSelectMode = useStore($isSelectMode);
  const isReadonly = useStore($isReadonly);
  const classes = useStyles();

  const renderContent = () => {
    if (isReadonly)
      return (
        <div className={classes.chatFooter__readonly}>{t('YouOnlyHaveViewMode')}</div>
      );
    if (isSelectMode) return <ChatSelectPanel />;
    return <ChatInput />;
  };

  return <div className={classes.chatFooter}>{renderContent()}</div>;
};
