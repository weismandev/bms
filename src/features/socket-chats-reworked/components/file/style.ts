import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  file: {
    display: 'grid',
    gridTemplateColumns: '54px 1fr',
    gap: 12,
    alignItems: 'center',
    width: '100%',
    cursor: 'pointer',
  },
  file__icon: {
    position: 'relative',
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    padding: 5,
    width: 54,
    height: 54,
    background: '#3390ec',
    borderRadius: 6,
  },
  file__iconText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 500,
  },
  file__content: {
    display: 'grid',
  },
  file__title: {
    fontWeight: 500,
    width: '100%',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    minWidth: 100,
    fontSize: 16,
  },
  file__size: {
    opacity: 0.5,
    minWidth: 120,
    fontSize: 14,
  },
});
