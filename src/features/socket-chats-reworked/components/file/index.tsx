import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStyles } from './style';

export const File: FC<ChatsFile> = ({ extension = null, name, size, url = null }) => {
  const classes = useStyles();

  const { t } = useTranslation();

  const extensionCrop = extension?.slice(0, 4);

  const handleFileClick = () => {
    if (url) window.open(url);
  };

  return (
    <div className={classes.file} onClick={handleFileClick}>
      <div className={classes.file__icon}>
        <span className={classes.file__iconText}>{extensionCrop}</span>
      </div>
      <div className={classes.file__content}>
        <span className={classes.file__title}>{name || t('Untitled')}</span>
        <div className={classes.file__size}>{size.text}</div>
      </div>
    </div>
  );
};
