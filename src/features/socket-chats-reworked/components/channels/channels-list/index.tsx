import { FC, useEffect, useRef } from 'react';
import { Virtuoso, VirtuosoHandle } from 'react-virtuoso';
import { useStore } from 'effector-react';
import { Button, Fab, Tooltip } from '@mui/material';
import FilterCenterFocusIcon from '@mui/icons-material/FilterCenterFocus';
import {
  $channels,
  $currentChatId,
  $isSearchLoading,
  $search,
  $searchedIds,
  $userChannelsIds,
  MINIMAL_SEARCH_SYMBOLS,
  loadMoreChannels,
} from '@features/socket-chats-reworked/models';
import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import i18n from '@shared/config/i18n';
import { Empty } from '@ui/index';
import { ChannelItem } from '../../channel/channel-item';
import { useStyles } from './styles';

const { t } = i18n;

export const ChannelsList: FC = () => {
  const channels = useStore($userChannelsIds);
  const search = useStore($search);
  const isSearchLoading = useStore($isSearchLoading);
  const searchedIds = useStore($searchedIds);
  const virtuosoRef = useRef<VirtuosoHandle>(null);
  const currentChatId = useStore($currentChatId);

  useEffect(() => {
    window.addEventListener('error', (e) => {
      if (
        e.message === 'ResizeObserver loop completed with undelivered notifications.' ||
        e.message === 'ResizeObserver loop limit exceeded'
      ) {
        e.stopImmediatePropagation();
      }

      if (e.message === 'ResizeObserver loop limit exceeded') {
        const resizeObserverErrDiv = document.getElementById(
          'webpack-dev-server-client-overlay-div'
        );
        const resizeObserverErr = document.getElementById(
          'webpack-dev-server-client-overlay'
        );
        if (resizeObserverErr) {
          resizeObserverErr.setAttribute('style', 'display: none');
        }
        if (resizeObserverErrDiv) {
          resizeObserverErrDiv.setAttribute('style', 'display: none');
        }
      }
    });
  }, []);

  const processedChannelsIds = channels;

  const classes = useStyles();

  if (search.length > 0 && search.length < MINIMAL_SEARCH_SYMBOLS) {
    return (
      <div style={{ height: '30%' }}>
        <Empty title={t('EnteThreeOrMoreCharacters')} />
      </div>
    );
  }

  if (isSearchLoading) {
    return null;
  }

  if (!processedChannelsIds.length) {
    return (
      <div style={{ height: '30%' }}>
        <Empty title={t('NoChatFound')} />
      </div>
    );
  }

  const focusOnCurrent = () =>
    virtuosoRef && currentChatId
      ? virtuosoRef.current?.scrollToIndex({
          index: Object.keys(channels).indexOf(currentChatId),
        })
      : null;

  const itemContent = (index: number, channelId: ChannelInstance['id']) => (
    <ChannelItem index={index} channelId={channelId} />
  );

  return (
    <div className={classes.list__wrap}>
      <Virtuoso
        ref={virtuosoRef}
        style={{ height: '100%' }}
        atBottomStateChange={loadMoreChannels}
        data={processedChannelsIds}
        itemContent={itemContent}
        overscan={500}
        fixedItemHeight={72}
      />
      {currentChatId && (
        <Tooltip title={t('GoCurrentChat')} onClick={focusOnCurrent}>
          <Fab color="primary" aria-label="add" className={classes.list__button}>
            <FilterCenterFocusIcon />
          </Fab>
        </Tooltip>
      )}
    </div>
  );
};
