import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  list__wrap: {
    position: 'relative',
    height: 'calc(100% - 60px)',

    '& *::-webkit-scrollbar': {
      width: 7.5,
    },

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      borderRadius: 6,
    },

    '&:hover $list__button': {
      visibility: 'visible',
      opacity: 1,
    },
  },
  list__button: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    width: 40,
    height: 40,
    boxShadow: '0px 4px 10px #e7e7ec',
    visibility: 'hidden',
    opacity: 0,
    transition: 'all 0.2s',
  },
});
