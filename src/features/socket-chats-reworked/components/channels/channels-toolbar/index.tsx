import { FC } from 'react';
import { Toolbar } from '@ui/index';
import { ChannelsSearch } from '../channels-search';
import { ChannelCreation } from '../../channel/channel-creation';
import { useStyles } from './styles';

export const ChannelsToolbar: FC = () => {
  const classes = useStyles();

  return (
    <Toolbar classes={{ root: classes.toolbar }}>
      <ChannelsSearch />
      <ChannelCreation />
    </Toolbar>
  );
};
