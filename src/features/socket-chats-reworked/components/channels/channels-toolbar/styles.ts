import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  toolbar: {
    height: 60,
    display: 'grid',
    gridTemplateColumns: '1fr auto',
    gap: 13,
    alignItems: 'center',
    padding: '0 16px',
  },
});
