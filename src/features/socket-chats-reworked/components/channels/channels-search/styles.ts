import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  search: ({ active }: ChatsSearchInput) => ({
    display: 'grid',
    width: '100%',
    gridTemplateColumns: 'auto 1fr auto',
    alignItems: 'center',
    height: 40,
    background: active ? 'white' : '#f4f4f5',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: active ? '#3390ec' : 'transparent',
    borderRadius: 22,
    padding: '0 0 0 10.5px',
    transition: 'all 0.2s',
  }),
  search__clear: {
    position: 'relative',
    display: 'grid',
  },
  search__clearProgress: {
    position: 'absolute',
    top: '50%',
    right: 4,
    height: 28,
    transform: 'translateY(-50%)',
    opacity: 0.5,
  },
  search__icon: ({ active }: ChatsSearchInput) => ({
    color: active ? '#3390ec' : 'rgba(112,117,121, 0.5)',
    transition: 'color 0.2s',
  }),
  search__input: {
    height: '100%',
    border: 'none',
    padding: '0 10.5px',
    outline: 'none',
    background: 'none',
    fontSize: 16,
    fontWeight: '400 !important',

    '&::placeholder': {
      color: '#a2acb9',
    },
  },
  search__clearIcon: {
    color: 'rgba(112,117,121, 0.5)',
    fontSize: 'smaller',
  },
}));
