import { ChangeEvent, FC, useState } from 'react';
import { useStore } from 'effector-react';
import { CircularProgress, IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import i18n from '@shared/config/i18n';
import {
  $isSearchLoading,
  $search,
  changeSearch,
} from '@features/socket-chats-reworked/models';
import { useStyles } from './styles';

const { t } = i18n;

export const ChannelsSearch: FC = () => {
  const search = useStore($search);
  const isSearchLoading = useStore($isSearchLoading);
  const [active, setActive] = useState<boolean>(false);
  const classes = useStyles({ active });

  const handleClear = () => {
    changeSearch('');
  };

  const handleInputFocus = () => {
    setActive(true);
  };

  const handleInputBlur = () => {
    setActive(false);
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    changeSearch(e.target.value);
  };

  return (
    <div className={classes.search}>
      <SearchIcon className={classes.search__icon} />
      <input
        className={classes.search__input}
        value={search}
        placeholder={t('search')}
        onChange={handleInputChange}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
      />
      {search && (
        <div className={classes.search__clear}>
          {isSearchLoading && (
            <div className={classes.search__clearProgress}>
              <CircularProgress size={28} />
            </div>
          )}

          <IconButton aria-label="delete" size="medium" onClick={handleClear}>
            <CloseIcon className={classes.search__clearIcon} />
          </IconButton>
        </div>
      )}
    </div>
  );
};
