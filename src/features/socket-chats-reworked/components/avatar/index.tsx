import { FC, memo } from 'react';
import {
  StyledAvatar,
  StyledAvatarIcon,
  StyledAvatarTitle,
} from '@features/socket-chats-reworked/components/avatar/styled';
import { ChatsAvatar } from '@features/socket-chats-reworked/types/avatar';

export const Avatar: FC<ChatsAvatar> = memo(
  ({ size = 'medium', title, color = '#ececec' }) => (
    <StyledAvatar size={size} color={color}>
      {title ? (
        <StyledAvatarTitle size={size}>{title}</StyledAvatarTitle>
      ) : (
        <StyledAvatarIcon size={size} />
      )}
    </StyledAvatar>
  )
);
