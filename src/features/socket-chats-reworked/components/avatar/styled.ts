import { ChatsAvatarProps } from '@features/socket-chats-reworked/types/avatar';
import { styled } from '@mui/material/styles';
import PersonRoundedIcon from '@mui/icons-material/PersonRounded';

const avatarSizeValues = {
  mini: {
    width: 34,
    height: 34,
    title: 14,
  },
  small: {
    width: 40,
    height: 40,
    title: 17,
  },
  medium: {
    width: 54,
    height: 54,
    title: 21,
  },
  large: {
    width: 200,
    height: 200,
    title: 21,
  },
};

export const StyledAvatar = styled('div')(({ size, color }: ChatsAvatarProps) => ({
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  justifyContent: 'center',
  width: avatarSizeValues[size || 'medium'].width,
  height: avatarSizeValues[size || 'medium'].height,
  borderRadius: '100%',
  objectFit: 'cover',
  background: `linear-gradient(#ffffff -125%, ${color || '#eaeaea'})`,

  '& *': {
    userSelect: 'none',
  },
}));

export const StyledAvatarIcon = styled(PersonRoundedIcon)(
  ({ size }: Omit<ChatsAvatarProps, 'color'>) => ({
    color: 'white',
    fontWeight: '400',
    fontSize: avatarSizeValues[size || 'medium'].title,
    letterSpacing: 1,
    marginLeft: 1,
  })
);

export const StyledAvatarTitle = styled('span')(
  ({ size }: Omit<ChatsAvatarProps, 'color'>) => ({
    color: 'white',
    fontWeight: '400',
    fontSize: avatarSizeValues[size || 'medium'].title,
    letterSpacing: 1,
    marginLeft: 1,
  })
);
