import { useGate, useStore } from 'effector-react';
import { CircularProgress } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  $isAuth,
  $isAuthLoading,
  $isSocketOpening,
  $socketState,
  SocketGate,
} from '@features/socket-chats-reworked/models';
import { useStyles } from './styles';

const { t } = i18n;

export const SocketGuard = ({ token, children }: ChatsSocketGuard) => {
  useGate(SocketGate, { token });

  const isAuthLoading = useStore($isAuthLoading);
  const isAuth = useStore($isAuth);
  const isSocketOpening = useStore($isSocketOpening);
  const socketState = useStore($socketState);
  const ready = Boolean(isAuth) && socketState == 'connected';
  const classes = useStyles();

  const renderTitle = () => {
    if (isSocketOpening || socketState == 'connecting') return t('ConnectToSocket');
    if (isAuthLoading) return t('Authorization');
    if (socketState == 'disconnected') return t('ConnectionLost');
    return t('Loading');
  };

  return ready ? (
    children
  ) : (
    <div className={classes.guard__wrap}>
      <div className={classes.guard}>
        <CircularProgress className={classes.guard__progress} />
        <span className={classes.guard__title}>{renderTitle()}</span>
      </div>
    </div>
  );
};
