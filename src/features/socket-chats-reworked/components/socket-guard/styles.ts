import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  guard__wrap: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    width: '100%',
    height: '100%',
    background: 'white',
    borderRadius: 12,
  },
  guard: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    gap: 15,
    height: 150,
    width: 200,
    padding: '12px 16px',
    background: '#ececec',
    borderRadius: 24,
    textAlign: 'center',
  },
  guard__progress: {
    color: 'rgba(112,117,121, 0.7)',
  },
  guard__title: {
    fontSize: 16,
    fontWeght: 'bold',
    color: 'rgba(112,117,121, 0.7)',
  },
});
