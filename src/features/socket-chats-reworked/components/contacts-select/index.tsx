import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { SelectControl, SelectField } from '@ui/index';
import {
  findContacts,
  $isContactsLoading,
  clearSearchedContacts,
  $searchedContactsArr,
} from '../../models';

const { t } = i18n;

const getOptionLabel = (option: any) => `${option.name} (${option.address})`;
const getOptionValue = (option: any) => option.id;

const ContactsSelect = (props: any) => {
  const searchedContactsArr = useStore($searchedContactsArr);
  const isLoading = useStore($isContactsLoading);

  const handleInputChange = (input: string) => {
    clearSearchedContacts();
    findContacts(input);
    return input;
  };

  const onMenuClose = () => {
    clearSearchedContacts();
  };

  return (
    <SelectControl
      label={t('Contact')}
      placeholder={t('EnterTheContactNameOrAddress')}
      getOptionLabel={getOptionLabel}
      getOptionValue={getOptionValue}
      options={searchedContactsArr}
      onInputChange={handleInputChange}
      filterOption={() => true}
      onMenuClose={onMenuClose}
      isLoading={isLoading}
      {...props}
    />
  );
};

const ContactsSelectField = (props: any) => {
  const onChange = (value: any) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ContactsSelect />} onChange={onChange} {...props} />;
};

export { ContactsSelect, ContactsSelectField };
