import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  channelMessage: {
    width: '100%',
    color: 'rgb(112,117,121)',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  channelMessage__media: {
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    color: 'rgb(112,117,121)',
  },
  channelMessage__text: {
    color: 'rgb(112,117,121)',
  },
  channelMessage__icon: {
    color: 'rgb(112,117,121)',
    marginRight: 2,
    opacity: 0.3,
  },
});
