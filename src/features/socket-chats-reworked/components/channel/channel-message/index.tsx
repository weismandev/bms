import { FC, memo } from 'react';
import { AttachFile, Notifications, Photo } from '@mui/icons-material';
import { ChannelMessageProps } from '@features/socket-chats-reworked/types/channel';
import i18n from '@shared/config/i18n';
import { useStyles } from './styles';

const { t } = i18n;

export const ChannelMessage: FC<ChannelMessageProps> = memo(({ message }) => {
  const classes = useStyles();

  if (!message) return null;

  const getMessageContent = () => {
    switch (message?.type) {
      case 'action':
        return (
          <div className={classes.channelMessage__media}>
            <Notifications className={classes.channelMessage__icon} />
            {t('Notification')}
          </div>
        );

      case 'image':
        return (
          <div className={classes.channelMessage__media}>
            <Photo className={classes.channelMessage__icon} />
            {t('Photo')}
          </div>
        );

      case 'file':
        return (
          <div className={classes.channelMessage__media}>
            <AttachFile className={classes.channelMessage__icon} />
            {t('File')}
          </div>
        );

      default:
        return <span className={classes.channelMessage__text}>{message?.text}</span>;
    }
  };

  return <div className={classes.channelMessage}>{getMessageContent()}</div>;
});
