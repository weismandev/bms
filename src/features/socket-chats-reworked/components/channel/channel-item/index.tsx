import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Skeleton } from '@mui/material';
import CorporateFareRoundedIcon from '@mui/icons-material/CorporateFareRounded';
import HelpOutlineRoundedIcon from '@mui/icons-material/HelpOutlineRounded';
import VolumeOffRoundedIcon from '@mui/icons-material/VolumeOffRounded';
import {
  $channels,
  $contacts,
  $currentChatId,
  $messages,
  openChat,
} from '@features/socket-chats-reworked/models';
import { Avatar } from '../../avatar';
import { ChannelMessage } from '../channel-message';
import { ChannelMeta } from '../channel-meta';
import { useStyles } from './styles';

export const ChannelsItemSkeleton: FC = () => {
  const classes = useStyles({ active: false });

  return (
    <div className={classes.channel}>
      <Skeleton variant="circular" width={54} height={54} />
      <div className={classes.channel__content}>
        <Skeleton />
        <Skeleton />
      </div>
    </div>
  );
};

export const ChannelItem = memo(({ channelId }: ChatsChannelItem) => {
  const currentChatId = useStore($currentChatId);
  const channels = useStore($channels);
  const channel = channels[channelId];
  const isCurrentChat = currentChatId == channel?.id;
  const contacts = useStore($contacts);
  const allMessages = useStore($messages);
  const channelMessages = channel?.id ? allMessages[channel.id] : [];
  const contact = channel?.contact_id ? contacts[channel.contact_id] : null;
  const lastMessage = channelMessages
    ? channelMessages[channelMessages.length - 1]
    : null;
  const classes = useStyles({ active: isCurrentChat });

  if (!channel?.loaded) return <ChannelsItemSkeleton />;

  const handleChannelClick = () => {
    openChat(channel.id);
  };

  const renderTypeIcon = () => {
    switch (channel.service_type) {
      case 'building':
        return <CorporateFareRoundedIcon className={classes.channel__icon} />;
      case 'support':
        return <HelpOutlineRoundedIcon className={classes.channel__icon} />;
    }
  };

  const renderNotificationTypeIcon = () => {
    if (channel.notification_type == 'none')
      return <VolumeOffRoundedIcon className={classes.channel__notificationIcon} />;
  };

  const renderAvatar = () => {
    const title = contact ? contact.avatar?.title : channel?.avatar?.title;
    const color = contact ? contact.avatar?.color : channel?.avatar?.color;

    return <Avatar size="medium" title={title} color={color} />;
  };

  const renderLastMessage = () => {
    if (!lastMessage) return null;
    return <ChannelMessage message={lastMessage} />;
  };

  const renderInfo = () => {
    if (!lastMessage) return null;
    return <ChannelMeta channel={channel} lastMessage={lastMessage} />;
  };

  const renderAddress = () => {
    if (contact?.address)
      return <span className={classes.channel__address}> {contact?.address}</span>;
  };

  return (
    <div className={classes.channel} onClick={handleChannelClick}>
      {renderAvatar()}

      <div className={classes.channel__content}>
        <div className={classes.channel__header}>
          {renderTypeIcon()}
          <span className={classes.channel__title}>{channel.title}</span>
          {renderNotificationTypeIcon()}
        </div>

        {renderAddress()}

        {renderLastMessage()}
      </div>

      {renderInfo()}
    </div>
  );
});
