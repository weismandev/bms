import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  channel: ({ active }: ChatsChannelItemStyle) => ({
    width: 'calc(100% - 14px) !important',
    margin: '0 0 0 8px',
    display: 'grid',
    gridTemplateColumns: '54px 1fr auto',
    alignItems: 'center',
    gap: 8,
    padding: 9,
    cursor: 'pointer',
    borderRadius: 12,
    background: active ? 'rgb(51,144,236)' : '',

    '& *': {
      color: active ? 'white !important' : '',
      userSelect: 'none',
    },

    '&:hover': {
      background: active ? '' : 'rgb(244,244,245)',
    },
  }),
  channel__content: {
    display: 'grid',
    alignItems: 'center',
  },
  channel__header: {
    display: 'grid',
    gridTemplateColumns: 'auto 1fr auto',
    alignItems: 'center',
    width: '100%',
  },
  channel__icon: {
    marginRight: 5,
    fontSize: 20,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  channel__title: {
    fontWeight: 500,
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  channel__notificationIcon: {
    opacity: 0.3,
    marginLeft: 10,
    fontSize: 18,
  },
  channel__address: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    fontSize: 11,
    color: 'rgb(112,117,121);',
  },
}));
