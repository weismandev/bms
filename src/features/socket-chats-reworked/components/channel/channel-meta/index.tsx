import { memo } from 'react';
import { getRelativeDate } from '@features/socket-chats-reworked/lib';
import { MessageConfirmation } from '../../messages/message-confirmation';
import { useStyles } from './styles';

export const ChannelMeta = memo(({ channel, lastMessage }: ChatsChannelMeta) => {
  const classes = useStyles();

  const renderMessageMeta = () => {
    if (!lastMessage) return null;

    const messageDate = getRelativeDate(lastMessage?.created_at_date);

    return (
      <>
        {lastMessage.is_owner && <MessageConfirmation read={lastMessage?.read} />}

        <span className={classes.channelInfo__date}>{messageDate}</span>
      </>
    );
  };

  const renderUnreadCount = (count: number) => {
    if (count > 99) return '99+';
    return count;
  };

  return (
    <div className={classes.channelInfo}>
      <div className={classes.channelInfo__header}>{renderMessageMeta()}</div>

      {channel?.unread_messages ? (
        <div className={classes.channelInfo__unread}>
          <span className={classes.channelInfo__unreadCount}>
            {renderUnreadCount(channel.unread_messages)}
          </span>
        </div>
      ) : null}
    </div>
  );
});
