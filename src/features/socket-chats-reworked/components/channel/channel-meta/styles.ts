import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  channelInfo: {
    display: 'grid',
    justifyItems: 'end',
    alignSelf: 'start',
    paddingTop: 6,
    gap: 4,
  },
  channelInfo__header: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    gap: 2,
  },
  channelInfo__date: {
    fontSize: 12,
    color: '#686c72',
  },
  channelInfo__unread: {
    display: 'grid',
    alignItems: 'center',
    borderRadius: 12,
    background: 'rgb(0,199,62)',
    padding: '0 8.1px',
    height: 24,
  },
  channelInfo__unreadCount: {
    color: 'white',
    fontWeight: 500,
    fontSize: 14,
  },
});
