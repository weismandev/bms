import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  modalHeader: {
    padding: ({ tab }: { tab: string }) =>
      tab === 'targetedGroup' ? '24px 24px 0px 24px' : 0,
  },
  modalPaper: {
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
    padding: ({ tab }: { tab: string }) => (tab === 'targetedGroup' ? 0 : 24),
    width: '27.55vw',
    minWidth: 600,
  },
  creationContent: {
    height: 500,
    padding: '20px 0 !important',
    overflow: ({ tab }: { tab: string }) =>
      tab === 'targetedGroup' ? 'hidden' : 'visible',
  },
  creationForm: {
    display: 'grid',
    gap: 20,
    height: '100%',
    alignContent: 'space-between',
  },
  creationForm__content: {
    display: 'grid',
    gap: 10,
  },
  creationForm__footer: {
    display: 'grid',
    alignSelf: 'bottom',
    justifySelf: 'center',
  },
});
