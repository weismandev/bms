import { FC } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import { createGroup } from '@features/socket-chats-reworked/models';
import i18n from '@shared/config/i18n';
import { InputField } from '@ui/index';
import { ContactsSelectField } from '../../../contacts-select';
import { useStyles } from '../styles';

const { t } = i18n;

export const GroupForm: FC = () => {
  const classes = useStyles();

  const validation = Yup.object({
    title: Yup.string().required(),
    contacts: Yup.mixed().required(),
  }).required();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validation),
    defaultValues: {
      title: '',
      contacts: [],
    },
  });

  const onSubmit = ({ title, contacts }: ChatsCreateGroupSubmit) =>
    createGroup({
      title,
      contacts: contacts.map((contact) => contact.id),
    });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.creationForm}>
      <div className={classes.creationForm__content}>
        <Controller
          name="title"
          control={control}
          render={({ field }) => (
            <InputField
              {...field}
              label={t('Name')}
              placeholder={t('EnterTheTitle')}
              divider={false}
              error={errors.title}
              required
            />
          )}
        />

        <Controller
          name="contacts"
          control={control}
          render={({ field }) => (
            <ContactsSelectField
              {...field}
              label={t('Contacts')}
              placeholder={t('SelectContacts')}
              divider={false}
              error={errors.contacts}
              isMulti
              required
            />
          )}
        />
      </div>

      <div className={classes.creationForm__footer}>
        <Button variant="contained" disableElevation type="submit">
          {t('CreateAGroupChannel')}
        </Button>
      </div>
    </form>
  );
};
