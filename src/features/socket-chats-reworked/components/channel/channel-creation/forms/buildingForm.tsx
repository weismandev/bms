import { FC } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Button } from '@mui/material';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { HouseSelect } from '@features/house-select';
import { createBuildingGroup } from '@features/socket-chats-reworked/models';
import { useStyles } from '../styles';

const { t } = i18n;

export const BuildingForm: FC = () => {
  const classes = useStyles();

  const validation = Yup.object({
    building: Yup.mixed().required(),
  }).required();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validation),
    defaultValues: {
      building: '',
    },
  });

  const onSubmit = (data: any) => {
    const id: number = data.building.id;
    createBuildingGroup(id);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.creationForm}>
      <div className={classes.creationForm__content}>
        <Controller
          name="building"
          control={control}
          render={({ field }) => (
            <HouseSelect
              {...field}
              label={t('Label.building')}
              placeholder={t('selectHouse')}
              divider={false}
              error={errors.building}
              required
            />
          )}
        />
      </div>

      <div className={classes.creationForm__footer}>
        <Button variant="contained" disableElevation type="submit">
          {t('CreateACommunityChannel')}
        </Button>
      </div>
    </form>
  );
};
