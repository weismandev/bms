import { FC } from 'react';
import { useForm, Controller } from 'react-hook-form';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import { yupResolver } from '@hookform/resolvers/yup';
import i18n from '@shared/config/i18n';
import { createDirect } from '@features/socket-chats-reworked/models';
import { useStyles } from '../styles';
import { ContactsSelectField } from '../../../contacts-select';

const { t } = i18n;

export const DirectForm: FC = () => {
  const classes = useStyles();

  const validation = Yup.object({
    contact: Yup.mixed().required(),
  }).required();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validation),
    defaultValues: {
      contact: '',
    },
  });

  const onSubmit = ({ contact }: { contact: any | ChatsContact }) => {
    createDirect(contact.id);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.creationForm}>
      <div className={classes.creationForm__content}>
        <Controller
          name="contact"
          control={control}
          render={({ field }) => (
            <ContactsSelectField
              {...field}
              label={t('Contact')}
              placeholder={t('ChooseAContact')}
              divider={false}
              error={errors.contact}
              required
            />
          )}
        />
      </div>

      <div className={classes.creationForm__footer}>
        <Button variant="contained" disableElevation type="submit">
          {t('CreateAPrivateChat')}
        </Button>
      </div>
    </form>
  );
};
