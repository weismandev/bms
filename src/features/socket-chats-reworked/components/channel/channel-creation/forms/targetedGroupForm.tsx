import { FC } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ComplexSelectField } from '@features/complex-select';
import { TargetedContent } from '@features/targeted-content';
import i18n from '@shared/config/i18n';
import { InputField, CustomScrollbar } from '@ui/index';
import { createTargetedGroup } from '../../../../models';

const { t } = i18n;
const requiredField = t('thisIsRequiredField');

const initialValues = {
  title: '',
  complex: '',
  targetingOptions: [],
  genders: [],
  age: [0, 100],
};

const validationSchema = Yup.object({
  title: Yup.string().required(requiredField),
  complex: Yup.object().nullable().required(requiredField),
});

const useFormStyles = makeStyles({
  form: {
    height: '100%',
    position: 'relative',
    paddingLeft: 24,
  },
  contentForm: {
    height: 'calc(100% - 74px)',
  },
  content: {
    paddingRight: 24,
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
});

export const TargetedGroupForm: FC = () => {
  const classesForm = useFormStyles();

  const onSubmit = (values: any) => createTargetedGroup(values);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      enableReinitialize
      render={() => (
        <Form className={classesForm.form}>
          <div className={classesForm.contentForm}>
            <CustomScrollbar>
              <div className={classesForm.content}>
                <Field
                  name="title"
                  component={InputField}
                  label={t('Name')}
                  placeholder={t('EnterTheTitle')}
                  required
                />
                <Field
                  name="complex"
                  component={ComplexSelectField}
                  label={t('RC')}
                  placeholder={t('selectRC')}
                  required
                />
                <TargetedContent mode="edit" />
              </div>
            </CustomScrollbar>
          </div>
          <div className={classesForm.buttonWrapper}>
            <Button variant="contained" disableElevation type="submit">
              {t('CreateTargetedChannel')}
            </Button>
          </div>
        </Form>
      )}
    />
  );
};
