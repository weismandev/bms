import { FC, SyntheticEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Tooltip, Tabs, Tab } from '@mui/material';
import {
  $isCreationModalOpen,
  closeCreationModal,
  openCreationModal,
} from '@features/socket-chats-reworked/models';
import { ModalCard, ModalCardTitle, ModalCardContent, AddButton } from '@ui/index';
import { BuildingForm } from './forms/buildingForm';
import { DirectForm } from './forms/directForm';
import { GroupForm } from './forms/groupForm';
import { TargetedGroupForm } from './forms/targetedGroupForm';
import { useStyles } from './styles';

export const ChannelCreation: FC = () => {
  const { t } = useTranslation();
  const [tab, setTab] = useState<ChatsCreationTab>('direct');
  const isCreationModalOpen = useStore($isCreationModalOpen);

  const classes = useStyles({ tab });

  const tabsContent = {
    direct: {
      title: t('ChatWithAResident'),
      component: <DirectForm />,
    },
    group: {
      title: t('Channel'),
      component: <GroupForm />,
    },
    building: {
      title: t('CommonHouseChannel'),
      component: <BuildingForm />,
    },
    targetedGroup: {
      title: t('TargetedChannel'),
      component: <TargetedGroupForm />,
    },
  };

  const tabs = Object.entries(tabsContent).map(([tabName, data]) => (
    <Tab value={tabName} key={tabName} label={data.title} />
  ));

  const handleChangeTab = (e: SyntheticEvent, value: any) => {
    setTab(value);
  };

  return (
    <div>
      <Tooltip title={t('CreateAChat') as string} onClick={openCreationModal}>
        <AddButton />
      </Tooltip>

      <ModalCard
        open={isCreationModalOpen}
        onClose={closeCreationModal}
        classes={{ paper: classes.modalPaper }}
      >
        <ModalCardTitle className={classes.modalHeader}>
          <Tabs variant="fullWidth" value={tab} onChange={handleChangeTab}>
            {tabs}
          </Tabs>
        </ModalCardTitle>

        <ModalCardContent className={classes.creationContent}>
          {tabsContent[tab].component}
        </ModalCardContent>
      </ModalCard>
    </div>
  );
};
