import { FC } from 'react';
import { useRouteMatch } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { ChatWelcome } from '../components/chat/chat-welcome';
import { SocketGuard } from '../components/socket-guard';
import { ChannelsPanel } from '../organisms/channels-panel';
import { ChatPanel } from '../organisms/chat-panel';
import { useStyles } from './style';

export const ChatsPage: FC = () => {
  const { url } = useRouteMatch();
  const classes = useStyles();

  return (
    <div className={classes.chatsPage}>
      <SocketGuard>
        <div className={classes.chatsPage__wrap}>
          <ChannelsPanel />
          <Switch>
            <Route
              path={`${url}/:chatId`}
              render={({ match }) => <ChatPanel channelId={match?.params?.chatId} />}
            />
            <Route exact path={url} component={ChatWelcome} />
          </Switch>
        </div>
      </SocketGuard>
    </div>
  );
};
