import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  chatsPage: {
    display: 'grid',
    padding: '12px 24px 24px 24px',
    height: '100%',
    borderRadius: '12px',
    overflow: 'hidden',
  },
  chatsPage__wrap: {
    height: '100%',
    gridTemplateColumns: '474px 1fr',
    display: 'grid',
    boxShadow: 'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
    borderRadius: '12px',
    overflow: 'hidden',
  },
});
