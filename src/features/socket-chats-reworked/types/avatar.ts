export type ChatsAvatar = Partial<{
  size: 'mini' | 'small' | 'medium' | 'large';
  title: string;
  color: string;
}>;

export type ChatsAvatarProps = {
  size: ChatsAvatar['size'];
  color: ChatsAvatar['color'];
};
