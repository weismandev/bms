import { File } from '@features/socket-chats-reworked/types/file';
import { SocketGateType } from '@features/socket-chats-reworked/types/socket';
import { ChatsAvatar } from './avatar';

export type ChannelInstance = {
  id: string;
  type: string;
  service_type:
    | 'flat'
    | 'news'
    | 'support'
    | 'management_company'
    | 'building'
    | 'manager'
    | 'ticket'
    | 'ai';
  contact_id: string;
  image: {
    url: string;
    mime_type: string;
    bytes: number;
    meta: {
      width: number;
      height: number;
    };
    preview: {
      url: string;
      mime_type: string;
      bytes: number;
      meta: {
        width: number;
        height: number;
      };
    };
  };
  title: string;
  unread_messages: number;
  last_read_message_id: number;
  notification_type: 'none' | 'all';
  archived: boolean;
  readonly: boolean;
  avatar: ChatsAvatar;
  loaded: boolean;
};

export type Channels = {
  [key: string]: ChannelInstance;
};

export type ChannelMessageProps = {
  message: MessageInstance;
};

export interface ChannelMeta {
  channel: ChannelInstance;
  lastMessage: MessageInstance;
}

export type ChannelsMode = 'main' | 'search';

export type SetChannelNotificationType = {
  channel_id: ChannelInstance['id'];
  notification_type: ChannelInstance['notification_type'];
};

export type GetChannelsResponse = {
  resources: ChannelInstance[];
};

export type GetChannelRequest = {
  channels: ChannelInstance['id'][];
};

export type GetExtendedChannelsRequest = {
  channels: ChannelInstance['id'][];
};

export type GetExtendedChannelsResponse = {
  resources: ChannelInstance[];
};

export type GetChannelResponse = {
  resources: ChannelInstance[];
};

export type SetChannelNotificationTypeRequest = {
  channel_id: ChannelInstance['id'];
  notification_type: ChannelInstance['notification_type'];
};

export type LeaveChannelRequest = {
  channel_id: ChannelInstance['id'];
};

/** -------------------- */

export type ChatGateType = {
  channelId: string | number;
  readonly?: ChannelInstance['readonly'];
};

export type ChatPanelProps = {
  channelId: string | number;
  standalone?: boolean;
  readonly?: ChannelInstance['readonly'];
  token?: SocketGateType['token'];
  hideLeaveButton?: boolean;
};

/** -------------------- */

export type MessageInstance = {
  id: number;
  message_id: number;
  channel_id: string;
  contact_id: string;
  text: string | '';
  type: null | 'action' | 'image' | 'video' | 'voice' | 'poll' | 'message' | 'file';
  created_at: string;
  is_hidden: boolean;
  forwarded_from: string;
  read: boolean;
  // Не с бека
  channel_key?: string;
  is_offline?: boolean;
  is_owner?: boolean;
  created_at_date?: string | null;
  attachment?: AttachmentInstance;
};

export type Messages = {
  [key: ChannelInstance['id']]: MessageInstance[];
};

export type AttachmentInstance = {
  affected_contact_id?: string;
  data?: any;
  initiator_contact_id?: number;
  text?: string;
  tokens?: string[];
  type?: AttachmentType;
  extension?: string;
  size?: {
    bytes: number;
    text: string;
  };
  url?: string;
  name?: string;
  bytes?: number;
};

export type AttachmentType =
  | null
  | 'invited'
  | 'title_changed'
  | 'photo_changed'
  | 'group_created'
  | 'ticket_created'
  | 'ticket_updated';

export type Attachments = {
  [key: MessageInstance['id']]: AttachmentInstance;
};

export type MessageProps = {
  message: MessageInstance;
};

export type MessageStyles = {
  is_owner?: MessageInstance['is_owner'];
  is_hidden?: MessageInstance['is_hidden'];
  is_image?: boolean;
};

export type SendMessage = {
  channel_key: ChannelInstance['id'];
  text?: string;
  attachment?: {
    type?: AttachmentType;
    id?: File['id'];
  };
  is_hidden?: MessageInstance['is_hidden'];
};

export type MessagesRange = {
  startIndex: number;
  endIndex: number;
};

export type MarkMessageAsRead = {
  channelId: ChannelInstance['id'];
  messageId: MessageInstance['id'];
};

export type MessageRead = {
  message_id: MessageInstance['id'];
};

export type TextMessageProps = {
  message: MessageInstance;
};

export type AttachmentMessageProps = {
  message: MessageInstance;
  attachment: AttachmentInstance;
};

export type DeleteMessages = {
  channel_id: ChannelInstance['id'] | null;
  messages: MessageInstance['id'][];
};

export type MessageMeta = {
  read: MessageInstance['read'];
};

/** ------------------ */

export type GetMessagesRequest = {
  channel_key: ChannelInstance['id'];
  limit: number;
  message_id?: MessageInstance['id'];
};

export type GetMessagesResponse = {
  resources: MessageInstance[];
};

export type GetLastMessagesRequest = {
  channels: ChannelInstance['id'][];
};

export type GetLastMessagesResponse = {
  resources: MessageInstance[];
};

export type GetMessagesAttachmentsRequest = {
  messages: MessageInstance['id'][];
};

export type GetMessagesAttachmentsResponse = {
  resources: {
    attachment: AttachmentInstance;
    message_id: MessageInstance['id'];
    type: AttachmentType;
  }[];
};

export type DeleteMessageRequest = {
  channel_id: ChannelInstance['id'];
  messages: MessageInstance['id'][];
};

export type MarkMessageAsReadRequest = {
  message_id: MessageInstance['id'];
};
