import { ContactInstance } from './contact';

export type CreationTab = 'direct' | 'group' | 'building';

export type CreateBuildingGroup = {
  building_id: number;
};

export type CreateGroupSubmit = {
  title: string;
  contacts: ContactInstance[];
};

export type CreateGroup = {
  title: string;
  contacts: string[];
};

export type CreateDirect = {
  contact: ContactInstance['id'];
};
