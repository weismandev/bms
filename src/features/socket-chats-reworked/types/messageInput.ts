import { File } from './file';

export type MessageInputMode = File['type'];

export type SetAttachments = {
  mode: MessageInputMode;
  files: any;
};

export type UploadFileRequest = {
  temp_id: File['id'];
  type: File['type'];
  file: any;
};

export type UploadFileResponse = {
  id: string;
};
