export type SocketState = 'connected' | 'disconnected' | 'connecting';

export type OpenSocketRequest = {
  socketUrl: string | undefined;
};

export type PushToSocketRequest = {
  socket: SocketIOClient.Socket;
  event: string;
  data: any;
};

export type PushToSocketResponse = {
  [key: string]: any;
  error: {
    message: string;
  };
};

export type SocketAuthRequest = {
  language: string;
  token: string;
};

export type SocketAuth = {
  id: string;
  address: any;
  image: string;
  in_blacklist: boolean;
  last_seen: string;
  name: string;
  phone: string;
  role: any;
};

export type SocketGateType = {
  token?: string;
};

export type SocketGuard = {
  token?: SocketGateType['token'];
  children: any;
};
