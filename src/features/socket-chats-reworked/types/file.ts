export type File = {
  id: string;
  file: any;
  base64: string;
  lastModified?: number;
  lastModifiedDate?: Date;
  name?: string;
  size:
    | any
    | {
        bytes: number;
        text: string;
      };
  extension: string;
  type?: 'file' | 'image' | 'video' | 'voice';
  webkitRelativePath?: string;
  excluded?: boolean;
  url?: string;
};

export type ChatsFileProps = {
  extension?: File['extension'];
  name: File['name'];
  size: File['size'];
  url?: File['url'];
};
