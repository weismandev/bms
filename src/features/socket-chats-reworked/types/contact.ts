import { ChannelInstance } from '@features/socket-chats-reworked/types/common';
import { ChatsAvatar } from './avatar';

export type ContactInstance = {
  id: string;
  role: string;
  name: string;
  image: string;
  phone: string;
  last_seen: string;
  in_blacklist: boolean;
  address: string;
  meta: {
    address: {
      title: string;
      number: {
        prefix: string;
        index: string;
      };
    };
    full_name: {
      name: string;
      patronymic: string;
      surname: string;
    };
  };
  // Не с бека
  avatar: ChatsAvatar;
};

export type Contacts = {
  [key: ContactInstance['id']]: ContactInstance;
};

export type ContactsByChannels = {
  [key: string]: Contacts;
};

export type GetContactsRequest = {
  contacts: ContactInstance['id'][];
};

export type GetContactsResponse = {
  resources: ContactInstance[];
};

export type GetChannelContactsRequest = {
  channel_key: ChannelInstance['id'];
};

export type GetChannelContactsResponse = {
  resources: ContactInstance[];
};
