import { ChannelInstance } from './common';

export type SearchInputProps = {
  active: boolean;
};

export type ChatsChannel = Partial<ChannelInstance> & { id: string };

export type ChatsChannelSearch = string;
