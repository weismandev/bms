import { nanoid } from 'nanoid';
import io from 'socket.io-client';
import { api } from '@api/api2';
import { ChatsUploadFilePayload } from '@features/socket-chats-reworked/types/chat-input';
import i18n from '@shared/config/i18n';
import { createError, isErrorReceived } from '../lib';
import {
  connected,
  connecting,
  disconnected,
  errorCatched,
  IM_URL,
  incomingEvents,
  reconnected,
  socketEventReceived,
} from '../models';
import {
  ChatsOpenSocket,
  ChatsPushToSocketPayload,
  ChatsPushToSocketResponse,
} from '../types/socket';

const { t } = i18n;

const openSocket = ({ socketUrl = '' }: ChatsOpenSocket) => {
  const socket = io(socketUrl, {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: Number.POSITIVE_INFINITY,
    transports: ['websocket'],
  });

  socket.on('connect', connected);
  socket.on('disconnect', disconnected);
  socket.on('reconnect', reconnected);
  socket.on('connecting', connecting);

  Object.values(incomingEvents).forEach((event) => {
    socket.on(event, (data: any) => socketEventReceived({ event, data }));
  });

  return socket.open();
};

const pushToSocket = ({ socket, event, data = {} }: ChatsPushToSocketPayload) => {
  const requestId = nanoid(12);

  return new Promise((resolve, reject) => {
    if (!socket) {
      errorCatched(createError('NONE', t('SocketNotDefined')));
      reject(t('SocketNotDefined'));
    }

    if (!socket.connected) {
      errorCatched(createError('NONE', t('NoSocketConnection')));
      reject(t('NoSocketConnection'));
    }

    if (!event) {
      errorCatched(createError('NONE', t('EventError')));
      reject(t('EventError'));
    }

    socket.emit(
      event,
      { data, request_id: requestId },
      (response: ChatsPushToSocketResponse) => {
        if (isErrorReceived(response)) {
          errorCatched(response);
          reject(response.error.message);
        } else {
          resolve(response);
        }
      }
    );
  });
};

export const uploadFile = async (payload: ChatsUploadFilePayload) => {
  const { id } = await api.no_vers('post', `${IM_URL}/uploadFile`, payload);

  return { ...payload, id };
};

export const chatsApi = {
  openSocket,
  pushToSocket,
  uploadFile,
};
