/** https://gist.github.com/0x263b/2bdd90886c2036a1ad5bcf06d6e6fb37 */
export const genHsl = (value: string): string => {
  let hash = 0;

  if (value.length === 0) return hash.toString();

  for (let i = 0; i < value.length; i += 1) {
    // eslint-disable-next-line no-bitwise
    hash = value.charCodeAt(i) + ((hash << 5) - hash);
    // eslint-disable-next-line no-bitwise
    hash &= hash;
  }

  return `hsl(${hash % 360}, 50%, 60%)`;
};
