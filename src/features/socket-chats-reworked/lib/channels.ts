import { ChatsChannel, ChatsChannels } from '../types/channel';

export const makeInitials = (value: string | undefined) =>
  value
    ? value
        .split(' ')
        .slice(0, 1)
        .map((item) => item[0].toUpperCase())
        .join('')
    : '';

export const processChannels = (channels: ChatsChannel[]) =>
  channels
    .filter(
      (channel) =>
        channel?.service_type === 'management_company' ||
        channel?.service_type === 'building' ||
        channel?.service_type === 'support' ||
        channel?.service_type === null
    )
    .map((channel) => ({ ...channel, loaded: false }));

export const getChannelsIds = (channels: ChatsChannels) =>
  Object.keys(channels).map((id) => id);

export const getNotLoadedChannelsIds = (channels: ChatsChannels) =>
  Object.keys(channels)
    .filter((channelId) => !channels[channelId].loaded)
    .map((channelId) => channelId);
