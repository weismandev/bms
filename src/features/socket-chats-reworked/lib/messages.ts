import { MessageInstance } from '@features/socket-chats-reworked/types/common';
import { ContactInstance } from '../types/contact';
import { isoToDate } from './time';

export const processMessages = (auth: ContactInstance, messages: MessageInstance[]) =>
  messages.map((message) => ({
    ...message,
    is_owner: auth?.id === message.contact_id,
    created_at_date: isoToDate(message.created_at),
  }));
