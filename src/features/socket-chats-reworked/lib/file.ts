export const getExtensionFromString = (value?: string) =>
  value ? value.split('.').slice(-1)[0] : null;

export const removeExtensionFromString = (value?: string) =>
  value ? value.split('.').slice(0, -1).join('.') : null;
