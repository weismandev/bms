import { ChatsChannel } from '@features/socket-chats-reworked/types/channel';
import { ChatsContact } from '../types/contact';
import { ChatsMessage } from '../types/message';

export const processContacts = (contacts: ChatsContact[]) =>
  contacts.map((contact) => contact);

export const getContactsIds = (
  data: ChatsChannel[] | ChatsMessage[]
): ChatsContact['id'][] | null => {
  if (!data.length) return null;

  return data.map((item) => item?.contact_id || '').filter((item) => Boolean(item));
};

export const createTitle = ({
  name = '',
  surname = '',
}: {
  name?: string;
  patronymic?: string;
  surname?: string;
}) => `${surname} ${name}`;
