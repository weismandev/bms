import { format, isThisYear, isToday, isYesterday } from 'date-fns';
import { enUS, ru } from 'date-fns/locale';
import i18n from '@shared/config/i18n';

const locale = localStorage.getItem('lang');
const dateLocale = locale === 'ru' ? ru : enUS;

const { t } = i18n;

export const isoToDate = (isoDate: string | undefined) => {
  if (!isoDate) return null;
  return new Date(isoDate).toString();
};

export const getRelativeDate = (
  date: string | undefined | null,
  mode: 'short' | 'full' = 'short'
) => {
  if (!date) return null;

  const dateProp = new Date(Date.parse(date));

  if (isToday(dateProp) && mode === 'short') {
    return format(dateProp, 'HH:mm');
  }
  if (isToday(dateProp) && mode === 'full') {
    return t('Today');
  }
  if (isYesterday(dateProp) && mode === 'full') {
    return t('Yesterday');
  }
  if (isThisYear(dateProp)) {
    return format(dateProp, mode === 'short' ? 'd MMM' : 'd MMMM', {
      locale: dateLocale,
    });
  }
  return format(dateProp, 'dd.MM.yy');
};

export const getTime = (date: string | undefined | null) => {
  if (!date) return null;

  return format(new Date(Date.parse(date)), 'HH:mm');
};
