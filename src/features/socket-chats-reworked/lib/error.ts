import { isObject } from '@tools/type';
import { ChatsPushToSocketResponse } from '@features/socket-chats-reworked/types/socket';

export const isErrorReceived = (response: ChatsPushToSocketResponse) =>
  isObject(response) && response?.error;

export const createError = (code: string, message: string) => ({
  error: {
    code,
    message,
  },
});
