import { groupBy } from 'lodash';
import { ChatsMessage } from '../types/message';
import { getRelativeDate } from './time';

export const useGroupedMessages = (messages: ChatsMessage[]) => {
  const groups = groupBy(messages, (message) =>
    getRelativeDate(message.created_at_date, 'full')
  );
  const groupCounts = Object.keys(groups).map((key) => groups[key].length);

  return {
    groups,
    groupCounts,
  };
};
