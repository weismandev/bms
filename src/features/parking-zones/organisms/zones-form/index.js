import { useState } from 'react';
import { useStore } from 'effector-react';
import {
  CustomScrollbar,
  FormSectionHeader,
  InputField,
  FileControl,
  ActionButton,
  BaseInputLabel,
  SwitchField,
  PlanImage,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import { Button, Grid } from '@mui/material';
import { Cancel, NavigateBefore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  $opened,
  $mode,
} from '../../models/detail.model';

const { t } = i18n;

const useStyles = makeStyles({
  picturePaper: {
    width: 224,
    height: 192,
    backgroundColor: '#F4F7FA',
    borderRadius: 16,
    marginBottom: 15,
    position: 'relative',
    margin: '0 auto 15px auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  outerImg: {
    width: 128,
    height: 128,
    objectFit: 'contain',
    cursor: 'pointer',
  },
});

export const ZonesForm = (props) => {
  const { renderToolbar, lot_id } = props;
  const opened = useStore($opened);

  const mode = useStore($mode);
  const { picturePaper, outerImg } = useStyles();

  const [isPriceFieldVisible, changePriceFieldVisibility] = useState(() =>
    Boolean(opened.price)
  );

  const isNew = !opened.id;
  const isEditMode = mode === 'edit';

  const cancel = () => {
    changedDetailVisibility(false);
    changeMode('view');
  };

  return (
    <Formik
      onSubmit={(values) => detailSubmitted({ ...values, lot_id })}
      enableReinitialize
      initialValues={opened}
      render={({ values, resetForm, setValues }) => {
        const planeUrl = (values.plane && values.plane.preview) || '';
        const schemaUrl = (values.schema && values.schema.preview) || '';
        const removeButton = () => {
          const clickHandler = () => {
            setValues({ ...values, price: '' });
            changePriceFieldVisibility(false);
          };

          return (
            <Cancel
              style={{
                fill: '#EB5757',
                margin: '0 5px',
                cursor: 'pointer',
              }}
              onClick={clickHandler}
            />
          );
        };

        return (
          <Form style={{ height: 'calc(100% - 90px)', position: 'relative' }}>
            {renderToolbar({
              onCancel: () => {
                if (isNew) {
                  cancel();
                } else {
                  resetForm();
                  changePriceFieldVisibility(false);
                  changeMode('view');
                }
              },
            })}
            <div
              style={{
                height: 'calc(100% - 20px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ padding: 24 }}>
                  <FormSectionHeader
                    style={{ marginBottom: '24px' }}
                    header={
                      <>
                        <NavigateBefore
                          onClick={cancel}
                          style={{ marginRight: 15, cursor: 'pointer' }}
                        />
                        <span>{values.title}</span>
                      </>
                    }
                  />
                  <Grid container justifyContent="space-between">
                    <Field
                      name="plane"
                      render={({ field, form }) => {
                        return (
                          <Grid classes={{ root: picturePaper }} item>
                            <BaseInputLabel
                              style={{
                                margin: '20px 20px 10px 20px',
                                textAlign: 'center',
                              }}
                            >
                              {t('GeneralPlan')}
                            </BaseInputLabel>
                            {planeUrl && (
                              <PlanImage
                                src={planeUrl}
                                className={outerImg}
                                title={t('GeneralPlan')}
                              />
                            )}
                            <PictureControl
                              name={field.name}
                              value={field.value}
                              onChange={(value) => {
                                form.setFieldValue(field.name, value);
                              }}
                              mode={mode}
                            />
                          </Grid>
                        );
                      }}
                    />
                    <Field
                      name="schema"
                      render={({ field, form }) => {
                        return (
                          <Grid classes={{ root: picturePaper }} item>
                            <BaseInputLabel
                              style={{
                                margin: '20px 20px 10px 20px',
                                textAlign: 'center',
                              }}
                            >
                              {t('ParkingZonePlan')}
                            </BaseInputLabel>
                            {schemaUrl && (
                              <PlanImage
                                src={schemaUrl}
                                className={outerImg}
                                title={t('ParkingZonePlan')}
                              />
                            )}
                            <PictureControl
                              name={field.name}
                              value={field.value}
                              onChange={(value) => {
                                form.setFieldValue(field.name, value);
                              }}
                              mode={mode}
                            />
                          </Grid>
                        );
                      }}
                    />
                  </Grid>
                  <Field
                    name="title"
                    component={InputField}
                    label={t('TitleZone')}
                    placeholder={t('EnterTheTitle')}
                    mode={mode}
                  />
                  <Field
                    name="is_common_zone"
                    component={SwitchField}
                    label={t('CommonTerritoryMode')}
                    labelPlacement="start"
                    disabled={mode !== 'edit'}
                    value={values.is_common_zone}
                  />
                  {(values.price || isPriceFieldVisible) && (
                    <Field
                      name="price"
                      type="number"
                      mode={mode}
                      label={t('ShortTermRentalPricePerMonth')}
                      placeholder={t('EnterPrice')}
                      component={InputField}
                      inputButton={removeButton()}
                      required
                    />
                  )}
                  {!values.price && isEditMode && !isPriceFieldVisible && (
                    <Button
                      color="primary"
                      onClick={() => changePriceFieldVisibility(true)}
                    >
                      {`+ ${t('AddPrice')}`}
                    </Button>
                  )}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};

function PictureControl(props) {
  const { value, mode, onChange } = props;

  return (
    <FileControl
      inputProps={{ accept: 'image/*' }}
      loadable
      mode={mode}
      value={value}
      renderPreview={() => null}
      renderButton={() => (
        <ActionButton
          component="span"
          style={{
            width: '75%',
            position: 'absolute',
            bottom: 24,
            left: 0,
            right: 0,
            marginLeft: 'auto',
            marginRight: 'auto',
          }}
        >
          {value ? t('Replace') : t('PictureControlUpload')}
        </ActionButton>
      )}
      onChange={onChange}
    />
  );
}
