import { useList, useStore } from 'effector-react';
import { CustomScrollbar, ErrorMessage } from '@ui';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  Badge,
  Button,
  IconButton,
} from '@mui/material';
import { NavigateNext, Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { featureFlags } from '@configs/feature-flags';
import { addClicked, open, $mode } from '../../models/detail.model';
import {
  $zonesList,
  deleteConfirmed,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
} from '../../models/page.model';

const isCRUDAllowed = featureFlags.oldParkingsCRUD;

const useStyles = makeStyles({
  listRoot: {
    padding: '0 24px',
  },
  itemRoot: {
    height: 72,
    paddingRight: 0,
    paddingLeft: 0,
  },
  primaryText: {
    fontWeight: 500,
    lineHeight: '21px',
    fontSize: 21,
    color: '#3B3B50',
    letterSpacing: '0.15px',
  },
  iconRoot: {
    minWidth: 'unset',
    marginLeft: 24,
  },
  badgeRoot: {
    marginRight: 15,
  },
});

const { t } = i18n;

export const ZonesList = () => {
  const { itemRoot, listRoot, primaryText, iconRoot, badgeRoot } = useStyles();
  const mode = useStore($mode);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);

  const isEditMode = mode === 'edit';

  const zoneItems = useList($zonesList, {
    keys: [isEditMode],
    fn: ({ title, slots_count, id }) => (
      <>
        <ListItem
          onClick={() => open(id)}
          button
          component="li"
          classes={{ root: itemRoot }}
        >
          <ListItemText classes={{ primary: primaryText }} primary={title} />
          <ListItemIcon classes={{ root: iconRoot }}>
            <Badge
              classes={{ root: badgeRoot }}
              color="primary"
              badgeContent={slots_count}
              max={999}
            />
          </ListItemIcon>
          <ListItemIcon classes={{ root: iconRoot }}>
            <NavigateNext />
          </ListItemIcon>
          {isEditMode && (
            <ListItemIcon classes={{ root: iconRoot }}>
              <IconButton
                onClick={(e) => {
                  e.stopPropagation();
                  deleteConfirmed(id);
                }}
                size="large"
              >
                <Close />
              </IconButton>
            </ListItemIcon>
          )}
        </ListItem>
        <Divider component="li" />
      </>
    ),
  });

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <div style={{ padding: '0 12px 24px 0', height: 'calc(100% - 107px)' }}>
        <CustomScrollbar>
          <div>
            <List classes={{ root: listRoot }}>{zoneItems}</List>
            <div style={{ padding: '0 24px' }}>
              <Button onClick={addClicked} color="primary" disabled={!isCRUDAllowed}>
                {`+ ${t('AddZone')}`}
              </Button>
            </div>
          </div>
        </CustomScrollbar>
      </div>
    </>
  );
};
