import { sample, forward, attach } from 'effector';
import i18n from '@shared/config/i18n';
import { formatFile } from '@tools/formatFile';
import {
  open,
  $opened,
  entityApi,
  $mode,
  $isDetailOpen,
  resetDetailOpen,
} from './detail.model';
import { $normalized, fxCreate, fxUpdate, fxDelete, deleteConfirmed } from './page.model';

const { t } = i18n;

$opened
  .on(
    sample($normalized, open, (data, id) => data[id]),
    (_, opened) => formatOpened(opened)
  )
  .on([fxCreate.doneData, fxUpdate.doneData], (_, { zone }) => formatOpened(zone));

$mode.on([fxCreate.done, fxUpdate.done], () => 'view');

$isDetailOpen.reset(resetDetailOpen);

forward({
  from: entityApi.create,
  to: attach({ effect: fxCreate, mapParams: formatPayload }),
});

forward({
  from: entityApi.update,
  to: attach({ effect: fxUpdate, mapParams: formatPayload }),
});

forward({
  from: deleteConfirmed,
  to: fxDelete,
});

function formatOpened(opened) {
  const { id, title = t('NewZone'), plane, schema, price, is_common_zone } = opened;

  return {
    id,
    title,
    plane: plane ? formatFile(plane) : plane,
    schema: schema ? formatFile(schema) : schema,
    is_common_zone,
    price: price || '',
  };
}

function formatPayload(payload) {
  const { plane, schema, price, is_common_zone, ...rest } = payload;

  const formattedPayload = {
    ...rest,
  };

  if (plane && plane.id) {
    formattedPayload.plane_id = plane.id;
  }

  if (schema && schema.id) {
    formattedPayload.schema_id = schema.id;
  }

  if (price) {
    formattedPayload.price = price;
  }

  if (!price) {
    formattedPayload.price = 0;
  }

  formattedPayload.is_common_zone = is_common_zone ? 1 : 0;

  return formattedPayload;
}
