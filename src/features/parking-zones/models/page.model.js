import { merge } from 'effector';
import { createPageBag } from '@tools/factories';
import { parkingZonesApi } from '../api';

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  parkingZonesApi,
  {},
  {
    idAttribute: 'id',
    itemsAttribute: 'zones',
    itemAttribute: 'zone',
    createdPath: 'zone',
    updatedPath: 'zone',
  }
);

export const zonesChanged = merge([fxCreate.done, fxUpdate.done, fxDelete.done]);

export const $zonesList = $raw.map(({ data }) => data || []);
