import './detail.init';

export { fxGetList as fxGetZonesList, zonesChanged } from './page.model';

export {
  resetDetailOpen as resetZonesDetailOpen,
  changeMode as changeZonesMode,
  $mode as $zonesMode,
  $isDetailOpen as $isZonesDetailOpen,
} from './detail.model';
