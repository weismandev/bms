import { createEvent } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';

const { t } = i18n;

export const newEntity = {
  title: t('NewZone'),
  plane: '/#ssd',
  schema: '/#ssd',
};

export const resetDetailOpen = createEvent();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
