import { api } from '@api/api2';

const getList = (payload = {}) => api.v1('get', 'parking/zones/list', payload);
const getById = (id) => api.v1('get', 'parking/zones/get-by-id', { id });
const create = (payload = {}) => api.v1('post', 'parking/zones/create', payload);
const update = (payload = {}) => api.v1('post', 'parking/zones/update', payload);
const deleteItem = (id) => api.v1('post', 'parking/zones/delete', { id });

export const parkingZonesApi = {
  getList,
  create,
  update,
  getById,
  delete: deleteItem,
};
