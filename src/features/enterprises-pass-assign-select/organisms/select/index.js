import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesPassAssignSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const EnterprisesPassAssignSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesPassAssignSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesPassAssignSelect, EnterprisesPassAssignSelectField };
