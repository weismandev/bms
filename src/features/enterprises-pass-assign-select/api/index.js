import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'scud/crm/pass/get-types', payload);

export const enterprisesPassTypeApi = { getList };
