import ProfileInfo from './profileInfo';
import ProfileLogout from './profileLogout';

export { ProfileInfo, ProfileLogout };
