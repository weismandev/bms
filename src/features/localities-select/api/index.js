import { api } from '../../../api/api2';

const getList = () => api.v2('/client/admin/localities/feature/index');

export const localitiesApi = { getList };
