import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const LocalitiesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  const getOptionLabel = (opt) => (opt ? `${opt.abbreviation} ${opt.name}` : '');

  return (
    <SelectControl
      getOptionLabel={getOptionLabel}
      options={options}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const LocalitiesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<LocalitiesSelect />} onChange={onChange} {...props} />;
};

export { LocalitiesSelect, LocalitiesSelectField };
