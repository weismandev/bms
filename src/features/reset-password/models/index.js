import {
  createEffect,
  createStore,
  createEvent,
  restore,
  forward,
  sample,
  guard,
} from 'effector';
import { createGate } from 'effector-react';
import { signout } from '../../common';
import { resetApi } from '../api';

const Gate = createGate();

const confirm = createEvent();
const resetAndNotify = createEvent();
const changeConfirmDialogVisibility = createEvent();
const changeErrorDialogVisibility = createEvent();
const changeResultDialogVisibility = createEvent();

const errorClosed = guard({
  source: changeErrorDialogVisibility,
  filter: (visibility) => !visibility,
});

const fxResetAndNotify = createEffect();

const $error = createStore(null);
const $isConfirmDialogOpen = restore(changeConfirmDialogVisibility, false);
const $isErrorDialogOpen = restore(changeErrorDialogVisibility, false);
const $isResultDialogOpen = restore(changeResultDialogVisibility, false);

fxResetAndNotify.use(({ isMultipleUsers, user_id, userdata_ids, context }) => {
  if (isMultipleUsers) {
    return resetApi.resetAndNotifyUsers({ userdata_ids, context });
  }
  return resetApi.resetAndNotify({ user_id, context });
});

$error.on(fxResetAndNotify.failData, (_, error) => error).reset([errorClosed, signout]);

$isErrorDialogOpen.on(fxResetAndNotify.fail, () => true).reset(signout);
$isResultDialogOpen.on(fxResetAndNotify.done, () => true).reset(signout);

$isConfirmDialogOpen
  .on(resetAndNotify, () => true)
  .on(confirm, () => false)
  .reset(signout);

forward({
  from: sample(Gate.state, confirm),
  to: fxResetAndNotify,
});

export {
  resetAndNotify,
  changeConfirmDialogVisibility,
  changeErrorDialogVisibility,
  changeResultDialogVisibility,
  $error,
  $isConfirmDialogOpen,
  $isErrorDialogOpen,
  $isResultDialogOpen,
  Gate,
  confirm,
};
