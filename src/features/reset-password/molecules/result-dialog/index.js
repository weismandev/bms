import i18n from '@shared/config/i18n';
import { Modal } from '../../../../ui';

const { t } = i18n;

function ResultDialog(props) {
  const { isOpen, onClose, isMultipleUsers } = props;

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      header={t('ActionCompletedSuccessfully')}
      content={isMultipleUsers ? t('NewPasswordsGenerated') : t('NewPasswordGenerated')}
    />
  );
}

export { ResultDialog };
