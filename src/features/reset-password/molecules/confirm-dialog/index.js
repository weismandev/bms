import { Modal, ActionButton } from '@ui';
import i18n from '@shared/config/i18n';

const { t } = i18n;

function ConfirmDialog(props) {
  const { isOpen, onClose, confirm, isMultipleUsers } = props;

  const actions = (
    <>
      <ActionButton style={{ margin: '0 5px' }} kind="negative" onClick={onClose}>
        {t('cancel')}
      </ActionButton>
      <ActionButton style={{ margin: '0 0 0 5px ' }} kind="positive" onClick={confirm}>
        {t('Confirm')}
      </ActionButton>
    </>
  );

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      header={t('ActionConfirmation')}
      content={
        isMultipleUsers ? t('GenerateNewPasswordUsers') : t('GenerateNewPasswordUser')
      }
      actions={actions}
    />
  );
}

export { ConfirmDialog };
