import { api } from '../../../api/api2';

const resetAndNotify = (payload) =>
  api.v1('post', 'auth-operation/reset-password-and-notify', payload);

const resetAndNotifyUsers = (payload) =>
  api.v1('post', 'bulk-operations/reset-residents-password-and-notify', payload);

export const resetApi = { resetAndNotify, resetAndNotifyUsers };
