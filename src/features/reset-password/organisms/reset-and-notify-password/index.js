import { combine } from 'effector';
import { useStore } from 'effector-react';
import { ErrorMessage, SendButton } from '../../../../ui';
import {
  resetAndNotify,
  changeConfirmDialogVisibility,
  changeErrorDialogVisibility,
  changeResultDialogVisibility,
  $error,
  $isConfirmDialogOpen,
  $isErrorDialogOpen,
  $isResultDialogOpen,
  Gate,
  confirm,
} from '../../models';
import { ConfirmDialog, ResultDialog } from '../../molecules';

const stores = combine({
  error: $error,
  isErrorDialogOpen: $isErrorDialogOpen,
  isConfirmDialogOpen: $isConfirmDialogOpen,
  isResultDialogOpen: $isResultDialogOpen,
});

const defaultStyle = { order: 40, margin: '0 5px' };

export const ResetAndNotifyPassword = (props) => {
  const {
    user_id,
    userdata_ids,
    style = defaultStyle,
    disabled = false,
    context,
    isHideTitleAccess,
  } = props;

  const { error, isErrorDialogOpen, isConfirmDialogOpen, isResultDialogOpen } =
    useStore(stores);
  const isMultipleUsers = Boolean(userdata_ids);
  return (
    <>
      <Gate
        isMultipleUsers={isMultipleUsers}
        user_id={user_id}
        userdata_ids={userdata_ids}
        context={context}
      />
      <SendButton
        isMultipleUsers={isMultipleUsers}
        style={isMultipleUsers ? {} : style}
        onClick={resetAndNotify}
        disabled={disabled}
        isHideTitleAccess={isHideTitleAccess}
      />
      <ErrorMessage
        error={error}
        isOpen={isErrorDialogOpen}
        onClose={() => changeErrorDialogVisibility(false)}
      />
      <ConfirmDialog
        isMultipleUsers={isMultipleUsers}
        confirm={confirm}
        isOpen={isConfirmDialogOpen}
        onClose={() => changeConfirmDialogVisibility(false)}
      />
      <ResultDialog
        isMultipleUsers={isMultipleUsers}
        isOpen={isResultDialogOpen}
        onClose={() => changeResultDialogVisibility(false)}
      />
    </>
  );
};
