import { api } from '../../../api/api2';

const get = () => api.v1('get', 'tickets/states/get-list');

export const statesApi = { get };
