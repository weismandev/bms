import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { statesApi } from '../api';

const fxGetStates = createEffect();

const $states = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$states.on(fxGetStates.done, (state, { result }) => result.states || []).reset(signout);

$error.on(fxGetStates.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetStates.pending, (state, pending) => pending).reset(signout);

fxGetStates.use(statesApi.get);

export { fxGetStates, $states, $error, $isLoading };
