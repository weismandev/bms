import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $states, fxGetStates, $isLoading, $error } from '../../model';

const StateSelect = (props) => {
  const options = useStore($states);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetStates();
  }, []);

  return (
    <SelectControl isLoading={isLoading} error={error} options={options} {...props} />
  );
};

const StateSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StateSelect />} onChange={onChange} {...props} />;
};

export { StateSelect, StateSelectField };
