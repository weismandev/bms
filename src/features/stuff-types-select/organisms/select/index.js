import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, fxGetList, $isLoading, $error } from '../../model';

const StuffTypesSelect = (props) => {
  const { building_id } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (building_id) {
      fxGetList(building_id);
    }
  }, [building_id]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const StuffTypesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StuffTypesSelect />} onChange={onChange} {...props} />;
};

export { StuffTypesSelect, StuffTypesSelectField };
