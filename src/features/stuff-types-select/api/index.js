import { api } from '../../../api/api2';

const getList = (building_id) =>
  api.v1('get', 'stuff-pass/crm/types/list', { building_id });

export { getList };
