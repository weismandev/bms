import { api } from '../../../api/api2';
import {
  PreferencesCriteria,
  PreferencesNotifications,
  PreferencesSmsProvider,
} from '../interfaces';
import { SetPreferencesBuildingsData } from '../interfaces/preferencesBuildings';

const getPreferencesCriteria = () => {
  return api.v1('get', 'debtors/preferences/criteria');
};

const getPreferencesNotifications = () => {
  return api.v1('get', 'debtors/preferences/notifications');
};

const getPreferencesSmsProvider = () => {
  return api.v1('get', 'debtors/preferences/sms-provider');
};

const setPreferencesCriteria = (payload: PreferencesCriteria) => {
  return api.v1('post', 'debtors/preferences/criteria/save', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
};

const setPreferencesNotifications = (
  payload: Omit<PreferencesNotifications, 'variables'>
) => {
  return api.v1('post', 'debtors/preferences/notifications/save', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
};

const setPreferencesSmsProvider = (payload: PreferencesSmsProvider) => {
  return api.v1('post', 'debtors/preferences/sms-provider/save', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
};

const getPreferencesBuildings = () => {
  return api.v1('get', 'debtors/preferences/buildings');
};

const setPreferencesBuildings = (payload: SetPreferencesBuildingsData) => {
  return api.v1('post', 'debtors/preferences/buildings/save', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
};

const getBuildings = () => api.v1('get', 'buildings/get-list-crm', { per_page: 1000000 });

export const debitorsSettingsApi = {
  getPreferencesCriteria,
  getPreferencesNotifications,
  getPreferencesSmsProvider,
  getPreferencesBuildings,
  setPreferencesCriteria,
  setPreferencesNotifications,
  setPreferencesSmsProvider,
  getBuildings,
  setPreferencesBuildings,
};
