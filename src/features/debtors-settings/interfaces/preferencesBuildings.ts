export interface GetPreferencesBuildingsData {
  available_count: number;
  buildings: Building[];
}

export interface SetPreferencesBuildingsData {
  buildings: Building[];
}
export interface Building {
  id: number;
}

export type CheckedBuilding = {
  id: number;
  title: string;
  checked: boolean;
};
