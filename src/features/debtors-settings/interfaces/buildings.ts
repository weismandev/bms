export interface GetBuildingsData {
  links: Links;
  meta: Meta;
  buildings: Building[];
}

export interface Links {
  first: string;
  last: string;
  prev: any;
  next: any;
}

export interface Meta {
  current_page: number;
  from: number;
  last_page: number;
  path: string;
  per_page: string;
  to: number;
  total: number;
}

export interface Building {
  id: number;
  complex: Complex;
  building: Building2;
  entrances2: Entrances2[];
  statistics: Statistic[];
}

export interface Complex {
  id: number;
  title: string;
  timezone?: string;
}

export interface Building2 {
  id: number;
  title: string;
  alias: string;
  floor: number;
  start_work_time?: string;
  end_work_time?: string;
  fias?: string;
  apartmentCount: number;
  entranceCount: number;
  address: Address;
  sell_enabled: boolean;
  sell_emails: string[];
  security_number?: string;
  ext_guid: string;
  guest_scud_pass_limit: number;
  buildings_properties_rent_available: boolean;
  resident_request_variant: string;
  apartment_plan_default?: ApartmentPlanDefault;
  meters: Meters;
  paid_tickets_allowed: string;
}

export interface Address {
  city?: string;
  street?: string;
  house: string;
  houseNumber?: string;
  placementNumber: string;
  fullAddress: string;
}

export interface ApartmentPlanDefault {
  id: number;
  name: string;
  mime_type: string;
  bytes: number;
  meta: Meta2;
  url: string;
  updated_at: string;
}

export interface Meta2 {
  width: number;
  height: number;
}

export interface Meters {
  mode: string;
  'manual-update': ManualUpdate;
}

export interface ManualUpdate {
  from_day_of_month?: number;
  until_day_of_month?: number;
  push_day_of_month?: number;
  notification_message?: string;
}

export interface Entrances2 {
  number: number;
  first_apartment: number;
  last_apartment: number;
}

export interface Statistic {
  type: string;
  title: string;
  count: number;
}
