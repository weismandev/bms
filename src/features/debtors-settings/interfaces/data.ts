import { CheckedBuilding, GetPreferencesBuildingsData } from './preferencesBuildings';

export interface PreferencesCriteria {
  is_enabled: boolean | string;
  day_limit: number | null | '';
  debt_limit: number | null | '';
  day_limit_enabled: boolean;
  debt_limit_enabled: boolean;
}

export interface PreferencesNotificationsVariable {
  name: string;
}

export interface PreferencesNotifications {
  text: string;
  variables: PreferencesNotificationsVariable[];
  channels: string[];
  day: number;
  time: string;
  period?: any;
  is_automatic: boolean | string;
}

export interface PreferencesSmsProviderCredentials {
  app_id: string;
}

export interface PreferencesSmsProvider {
  provider_id: number;
  credentials: PreferencesSmsProviderCredentials;
}

export interface SettingsEditData {
  preferencesCriteria: PreferencesCriteria;
  preferencesNotifications: PreferencesNotifications;
  preferencesSmsProvider: PreferencesSmsProvider;
  checkedBuildings: CheckedBuilding[];
  preferencesBuildingsAvailableCount: GetPreferencesBuildingsData['available_count'];
  is_notifications_period: boolean;
}
