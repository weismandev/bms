import { combine, createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { GetBuildingsData } from '@features/debtors-settings/interfaces/buildings';
import {
  GetPreferencesBuildingsData,
  SetPreferencesBuildingsData,
} from '@features/debtors-settings/interfaces/preferencesBuildings';
import i18n from '@shared/config/i18n';
import {
  PageStatus,
  PreferencesCriteria,
  PreferencesNotifications,
  PreferencesSmsProvider,
  SettingsEditData,
} from '../../interfaces';

const { t } = i18n;

const CHANNELS_NAMES: { [key in string]: string } = {
  management_company: t('DebtorsSettings.NoticePush'),
  email: t('DebtorsSettings.NoticeEmail'),
  sms: t('DebtorsSettings.NoticeSms'),
};

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const clearError = createEvent();
const submitSettings = createEvent<SettingsEditData>();

const $preferencesCriteria = createStore<PreferencesCriteria>({} as PreferencesCriteria);
const $preferencesNotifications = createStore<PreferencesNotifications>(
  {} as PreferencesNotifications
);
const $preferencesSmsProvider = createStore<PreferencesSmsProvider>(
  {} as PreferencesSmsProvider
);
const $preferencesBuildings = createStore<GetPreferencesBuildingsData>(
  {} as GetPreferencesBuildingsData
);
const $buildings = createStore<GetBuildingsData['buildings']>(
  [] as GetBuildingsData['buildings']
);
const $checkedBuildings = combine(
  $preferencesBuildings,
  $buildings,
  (preferencesBuildings, buildings) => {
    const checkedIds = (preferencesBuildings?.buildings ?? []).map(
      (building) => building.id
    );

    return buildings
      .map((building) => ({
        id: building.id,
        title: `${building.building.title} ${
          !!building.complex.title ? `(${building.complex.title})` : ''
        } `,
        checked: checkedIds.indexOf(building.id) > -1,
      }))
      .sort((a, b) => (a.checked === b.checked ? 0 : a.checked ? -1 : 1));
  }
);

const $error = createStore(null).on(clearError, () => null);
const $statusPage = createStore<PageStatus>(PageStatus.view);

const changeStatusPage = createEvent<PageStatus>();

const fxGetPreferencesCriteria = createEffect<void, PreferencesCriteria, Error>();
const fxGetPreferencesNotifications = createEffect<
  void,
  PreferencesNotifications,
  Error
>();
const fxGetPreferencesSmsProvider = createEffect<void, PreferencesSmsProvider, Error>();
const fxGetPreferencesBuildings = createEffect<
  void,
  GetPreferencesBuildingsData,
  Error
>();
const fxGetBuildings = createEffect<void, GetBuildingsData, Error>();

const fxSetPreferencesCriteria = createEffect<PreferencesCriteria, void, Error>();
const fxSetPreferencesNotifications = createEffect<
  Omit<PreferencesNotifications, 'variables'>,
  void,
  Error
>();
const fxSetPreferencesSmsProvider = createEffect<PreferencesSmsProvider, void, Error>();
const fxSetAllPreferences = createEffect<SettingsEditData, void, Error>();
const fxSetPreferencesBuildings = createEffect<
  SetPreferencesBuildingsData,
  void,
  Error
>();

const $isLoading = combine(
  fxGetPreferencesCriteria.pending,
  fxGetPreferencesNotifications.pending,
  fxGetPreferencesSmsProvider.pending,
  fxGetPreferencesBuildings.pending,
  fxGetBuildings.pending,

  fxSetPreferencesCriteria.pending,
  fxSetPreferencesNotifications.pending,
  fxSetPreferencesSmsProvider.pending,
  fxSetPreferencesBuildings.pending,
  (
    fxGetPreferencesCriteriaPending,
    fxGetPreferencesNotificationsPending,
    fxGetPreferencesSmsProviderPending,
    fxGetPreferencesBuildingsPending,

    fxSetPreferencesCriteriaPending,
    fxSetPreferencesNotificationsPending,
    fxSetPreferencesSmsProviderPending,
    fxSetPreferencesBuildingsPending
  ) =>
    fxGetPreferencesCriteriaPending ||
    fxGetPreferencesNotificationsPending ||
    fxGetPreferencesSmsProviderPending ||
    fxGetPreferencesBuildingsPending ||
    fxSetPreferencesCriteriaPending ||
    fxSetPreferencesNotificationsPending ||
    fxSetPreferencesSmsProviderPending ||
    fxSetPreferencesBuildingsPending
);

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  $preferencesCriteria,
  $preferencesNotifications,
  $preferencesSmsProvider,
  $isLoading,
  $error,
  clearError,
  fxGetPreferencesCriteria,
  fxGetPreferencesNotifications,
  fxGetPreferencesSmsProvider,
  $statusPage,
  changeStatusPage,
  CHANNELS_NAMES,
  submitSettings,
  fxSetPreferencesCriteria,
  fxSetPreferencesNotifications,
  fxSetPreferencesSmsProvider,
  fxSetAllPreferences,
  fxGetPreferencesBuildings,
  $preferencesBuildings,
  fxGetBuildings,
  $buildings,
  $checkedBuildings,
  fxSetPreferencesBuildings,
};
