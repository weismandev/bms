import { merge, sample } from 'effector';
import { json } from 'stream/consumers';
import { signout } from '@features/common';
import { PageStatus, SettingsEditData } from '@features/debtors-settings/interfaces';
import { debitorsSettingsApi } from '../../api';
import {
  pageMounted,
  $preferencesCriteria,
  $preferencesNotifications,
  $preferencesSmsProvider,
  $error,
  fxGetPreferencesCriteria,
  fxGetPreferencesNotifications,
  fxGetPreferencesSmsProvider,
  $statusPage,
  changeStatusPage,
  submitSettings,
  fxSetPreferencesCriteria,
  fxSetPreferencesNotifications,
  fxSetPreferencesSmsProvider,
  fxSetAllPreferences,
  fxGetPreferencesBuildings,
  $preferencesBuildings,
  fxGetBuildings,
  $buildings,
  pageUnmounted,
  fxSetPreferencesBuildings,
} from './model';

$statusPage.on(changeStatusPage, (_, status) => status).reset(pageMounted);

fxGetPreferencesCriteria.use(debitorsSettingsApi.getPreferencesCriteria);
fxGetPreferencesNotifications.use(debitorsSettingsApi.getPreferencesNotifications);
fxGetPreferencesSmsProvider.use(debitorsSettingsApi.getPreferencesSmsProvider);
fxGetPreferencesBuildings.use(debitorsSettingsApi.getPreferencesBuildings);
fxGetBuildings.use(debitorsSettingsApi.getBuildings);

$preferencesCriteria
  .on(fxGetPreferencesCriteria.doneData, (_, data) => {
    return {
      ...data,
      day_limit_enabled: !!data.day_limit,
      debt_limit_enabled: !!data.debt_limit,
    };
  })
  .reset(signout, pageUnmounted);
$preferencesNotifications
  .on(fxGetPreferencesNotifications.doneData, (_, data) => data)
  .reset(signout, pageUnmounted);
$preferencesSmsProvider
  .on(fxGetPreferencesSmsProvider.doneData, (_, data) => data)
  .reset(signout, pageUnmounted);
$preferencesBuildings
  .on(fxGetPreferencesBuildings.doneData, (_, data) => data)
  .reset(signout, pageUnmounted);
$buildings
  .on(fxGetBuildings.doneData, (_, { buildings }) => buildings)
  .reset(signout, pageUnmounted);

fxSetPreferencesCriteria.use(debitorsSettingsApi.setPreferencesCriteria);
fxSetPreferencesNotifications.use(debitorsSettingsApi.setPreferencesNotifications);
fxSetPreferencesSmsProvider.use(debitorsSettingsApi.setPreferencesSmsProvider);
fxSetPreferencesBuildings.use(debitorsSettingsApi.setPreferencesBuildings);

/** Запуск эффекта с выполнением трех запросов сохранения данных */
sample({
  clock: submitSettings,
  target: fxSetAllPreferences,
});

/** Выполнение трех запросов сохранения настроек */
fxSetAllPreferences.use(
  async ({
    preferencesCriteria,
    preferencesNotifications,
    preferencesSmsProvider,
    checkedBuildings,
  }: SettingsEditData) => {
    const criteria = {
      ...preferencesCriteria,
      day_limit:
        preferencesCriteria.day_limit === '' ? null : preferencesCriteria.day_limit,
      debt_limit:
        preferencesCriteria.debt_limit === '' ? null : preferencesCriteria.debt_limit,
      is_enabled: !!preferencesCriteria.day_limit || !!preferencesCriteria.debt_limit,
    };

    const { text, is_automatic, channels, day, time, period } = preferencesNotifications;
    const notifications = {
      text,
      is_automatic: is_automatic ? '1' : '0',
      channels,
      day,
      time,
      period,
    };

    const checkedBuildingsIds = checkedBuildings
      .filter((building) => building.checked)
      .map(({ id }) => ({ id }));

    await Promise.all([
      fxSetPreferencesCriteria(criteria),
      fxSetPreferencesNotifications(notifications),
      fxSetPreferencesSmsProvider(preferencesSmsProvider),
      fxSetPreferencesBuildings({ buildings: checkedBuildingsIds }),
    ]);
  }
);

/** Запускаю запрос данных */
sample({
  clock: [pageMounted, changeStatusPage],
  target: [
    fxGetPreferencesCriteria,
    fxGetPreferencesNotifications,
    fxGetPreferencesSmsProvider,
    fxGetPreferencesBuildings,
  ],
});

/** Запускаю запрос данных */
sample({
  clock: [pageMounted],
  target: [fxGetBuildings],
});

/** После сохранения настроек перехожу в просмотр */
sample({
  clock: fxSetAllPreferences.done,
  fn: () => PageStatus.view,
  target: changeStatusPage,
});

/** Прооверка наличия запроса, запись в $isLoading */
const isRequest = merge([
  fxGetPreferencesCriteria.pending,
  fxGetPreferencesNotifications.pending,
  fxGetPreferencesSmsProvider.pending,
  fxGetPreferencesBuildings.pending,
  fxGetBuildings.pending,

  fxSetPreferencesCriteria.pending,
  fxSetPreferencesNotifications.pending,
  fxSetPreferencesSmsProvider.pending,
  fxSetPreferencesBuildings.pending,
]);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([
  fxGetPreferencesCriteria.failData,
  fxGetPreferencesNotifications.failData,
  fxGetPreferencesSmsProvider.failData,
  fxGetPreferencesBuildings.failData,
  fxGetBuildings.failData,

  fxSetPreferencesCriteria.failData,
  fxSetPreferencesNotifications.failData,
  fxSetPreferencesSmsProvider.failData,
  fxSetPreferencesBuildings.failData,
]);
$error
  .on(isError, (_, error) => {
    try {
      const errorMess = JSON.parse(error.message);
      return errorMess?.message ?? 'Request error';
    } catch {
      return error.message ?? 'Request error';
    }
  })
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(signout);
