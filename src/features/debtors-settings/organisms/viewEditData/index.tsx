import { useUnit } from 'effector-react';
import i18n from '@shared/config/i18n';
import { CancelButton, EditButton, SaveButton } from '@ui/atoms';
import { CustomScrollbar } from '@ui/molecules';
import { PageStatus } from '../../interfaces';
import {
  $isLoading,
  $preferencesCriteria,
  $preferencesNotifications,
  $preferencesSmsProvider,
  $statusPage,
  changeStatusPage,
  CHANNELS_NAMES,
} from '../../model';
import { EditData } from './editData';
import { PreferencesBuildings } from './preferencesBuildings';
import {
  StyledSettings,
  StyledItem,
  StyledTitle,
  StyledChip,
  StyledText,
  StyledTitleH3,
  StyledTextNote,
  StyledHeaderItem,
  StyledHeaderItemLeft,
  StyledHeaderItemRight,
  StyledLightText,
} from './styled';

const { t } = i18n;

export const SettingsHeader = ({ pageStatus }: { pageStatus: PageStatus }) => {
  const isLoading = useUnit($isLoading);

  if (pageStatus === PageStatus.view) {
    return (
      <StyledHeaderItem>
        <StyledHeaderItemLeft>
          <EditButton
            onClick={() => changeStatusPage(PageStatus.edit)}
            disabled={isLoading}
          />
        </StyledHeaderItemLeft>
      </StyledHeaderItem>
    );
  }

  return (
    <StyledHeaderItem>
      <StyledHeaderItemLeft>
        <EditButton disabled />
      </StyledHeaderItemLeft>
      <StyledHeaderItemRight>
        <SaveButton type={isLoading ? 'button' : 'submit'} />
        <CancelButton onClick={() => changeStatusPage(PageStatus.view)} />
      </StyledHeaderItemRight>
    </StyledHeaderItem>
  );
};

const ViewData = () => {
  const [preferencesCriteria, preferencesNotifications, preferencesSmsProvider] = useUnit(
    [$preferencesCriteria, $preferencesNotifications, $preferencesSmsProvider]
  );

  return (
    <CustomScrollbar autoHide>
      <StyledSettings>
        <SettingsHeader pageStatus={PageStatus.view} />

        <StyledItem fullWidth>
          <StyledTitle>
            {t('DebtorsSettings.AssignmentOfDebtorStatus') as string}
          </StyledTitle>
          {preferencesCriteria.is_enabled ? (
            <StyledChip label={t('DebtorsNotifications.Automatically') as string} />
          ) : (
            <StyledChip label={t('DebtorsNotifications.Manually') as string} />
          )}
        </StyledItem>

        {preferencesCriteria.is_enabled && (
          <>
            <StyledItem>
              <StyledTitle>
                {t('DebtorsSettings.NumberOfDaysToPayBill') as string}
              </StyledTitle>
              <StyledText>{preferencesCriteria.day_limit ?? '-'}</StyledText>
            </StyledItem>

            <StyledItem>
              <StyledTitle>{t('DebtorsSettings.MaximalDebt') as string}</StyledTitle>
              <StyledText>
                {preferencesCriteria.debt_limit ? preferencesCriteria.debt_limit : '-'}
              </StyledText>
            </StyledItem>
          </>
        )}

        <PreferencesBuildings />

        <StyledItem>
          <StyledTitleH3>
            {t('DebtorsSettings.NotificationSettings') as string}
          </StyledTitleH3>
          <StyledTitle>{t('DebtorsSettings.NotificationText') as string}</StyledTitle>
          <StyledText>{preferencesNotifications.text ?? '-'}</StyledText>
        </StyledItem>

        <StyledItem noBottomBorder></StyledItem>

        <StyledItem>
          <StyledTitleH3>
            {t('DebtorsSettings.SendingNotifications') as string}
          </StyledTitleH3>

          <StyledTitle>{t('DebtorsSettings.Notify') as string}</StyledTitle>
          <StyledChip
            label={
              preferencesNotifications.is_automatic
                ? (t('DebtorsNotifications.Automatically') as string)
                : (t('DebtorsNotifications.Manually') as string)
            }
          />

          <StyledTitleH3 marginTop>
            {t('DebtorsSettings.SmsProvider') as string}
          </StyledTitleH3>
          <StyledTitle>App ID</StyledTitle>
          <StyledText>
            {preferencesSmsProvider.credentials?.app_id ?? (
              <StyledLightText>{t('DebtorsSettings.EnterAppId')}</StyledLightText>
            )}
          </StyledText>

          <StyledTitle marginTop>
            {t('DebtorsNotifications.NewsletterService') as string}
          </StyledTitle>
          {preferencesNotifications.channels?.map((channel) => (
            <StyledChip key={channel} label={CHANNELS_NAMES[channel]} />
          ))}
        </StyledItem>

        <StyledItem noBottomBorder></StyledItem>

        <StyledItem>
          <StyledTitle>{t('Label.date') as string}</StyledTitle>
          <StyledText>
            {preferencesNotifications.day ? (
              <>
                {preferencesNotifications.day}{' '}
                <StyledTextNote>
                  {`(${t('DebtorsSettings.dateOfEachMonth') as string})`}
                </StyledTextNote>
              </>
            ) : (
              '-'
            )}
          </StyledText>
        </StyledItem>

        <StyledItem>
          <StyledTitle>{t('Label.time') as string}</StyledTitle>
          <StyledText>{preferencesNotifications.time ?? '-'}</StyledText>
        </StyledItem>

        <StyledItem>
          <StyledTitle>
            {t('DebtorsSettings.SendingNotificationsWithFrequency') as string}
          </StyledTitle>
          <StyledText>{preferencesNotifications.period ?? '-'}</StyledText>
        </StyledItem>

        <StyledItem noBottomBorder></StyledItem>
      </StyledSettings>
    </CustomScrollbar>
  );
};

const ViewEditData = () => {
  const statusPage = useUnit($statusPage);

  return statusPage === PageStatus.view ? <ViewData /> : <EditData />;
};

export { ViewEditData };
