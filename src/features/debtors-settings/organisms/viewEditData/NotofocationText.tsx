import { InputField } from '@ui/molecules';
import { Field } from 'formik';
import { useRef, useState } from 'react';
import {
  StyledVariable,
  StyledVariables,
  StyledVariableTitle,
  StyledWrap,
} from './styled';

interface variable {
  name: string;
}

interface NotificationTextProps {
  name: string;
  setFieldValue: (field: string, value: any) => void;
  text: string;
  label?: string | boolean;
  divider?: string | boolean;
  placeholder: string;
  multiline?: boolean;
  rows?: number | boolean;
  variables?: variable[];
}

export const NotificationText = (props: NotificationTextProps) => {
  const inputRef = useRef<HTMLElement>();
  const [selectionStart, setSelectionStart] = useState(0);
  const {
    name,
    variables,
    setFieldValue,
    text,
    rows,
    multiline,
    placeholder,
    divider,
    label,
  } = props;

  const resolveNewText = (newText: string) => {
    setFieldValue(name, newText);
    inputRef.current?.focus();
  };

  const addToText = (variableText: string) => {
    const newText = `${text.substring(0, selectionStart)}${variableText}${text.substring(
      selectionStart
    )}`;

    resolveNewText(newText);

    setSelectionStart((prev) => prev + variableText.length);
  };

  const removeFromText = (variableText: string) => {
    const screeningVariableText = variableText.replace('[', '\\[').replace(']', '\\]');
    const remove = new RegExp(screeningVariableText, 'g');

    const newPos = text.indexOf(variableText);
    const newText = text.replace(remove, '');

    resolveNewText(newText);

    setSelectionStart(newPos);
  };

  const handleVariableClick = (variableText: string) => {
    if (!isUsedVariable(variableText)) {
      addToText(variableText);

      return;
    }

    removeFromText(variableText);
  };

  const handleEvent = (event: any) =>
    setTimeout(() => setSelectionStart(event.target.selectionStart), 0);

  const isUsedVariable = (variableText: string) => text.includes(variableText);

  return (
    <StyledWrap>
      <Field
        innerRef={(ref: any) => (inputRef.current = ref)}
        name={name}
        component={InputField}
        label={label}
        divider={divider}
        placeholder={placeholder ?? ''}
        rows={rows}
        multiline={multiline}
        onClick={handleEvent}
        onKeyDown={handleEvent}
      />

      {variables && (
        <StyledVariables>
          <StyledVariableTitle>Переменные:</StyledVariableTitle>
          {variables.map((variable, index) => (
            <StyledVariable
              type="button"
              key={index}
              onClick={() => handleVariableClick(variable.name)}
              usedVariable={isUsedVariable(variable.name)}
            >
              {variable.name}
            </StyledVariable>
          ))}
        </StyledVariables>
      )}
    </StyledWrap>
  );
};
