import { FC, forwardRef, memo, useRef } from 'react';
import MaskedInput from 'react-text-mask';
import { useUnit } from 'effector-react';
import { ErrorMessage, Field, FieldArray, FieldProps, Form, Formik } from 'formik';
import { FormControlLabel, Tooltip } from '@mui/material';
import i18n from '@shared/config/i18n';
import { Checkbox } from '@ui/atoms';
import { CustomScrollbar, InputField } from '@ui/molecules';
import { SettingsHeader } from '.';
import { PageStatus, SettingsEditData } from '../../interfaces';
import {
  $checkedBuildings,
  $preferencesBuildings,
  $preferencesCriteria,
  $preferencesNotifications,
  $preferencesSmsProvider,
  CHANNELS_NAMES,
  submitSettings,
} from '../../model';
import { NotificationText } from './NotofocationText';
import {
  StyledSettings,
  StyledItem,
  StyledTitle,
  StyledText,
  StyledTitleH3,
  CheckboxLabel,
  StyledInfoIcon,
  StyledChip,
  StyledPaddingBottom,
  StyledBuildings,
  StyledError,
} from './styled';
import { validationSchema } from './validation';

const { t } = i18n;

export const CustomCheckbox = (props: Record<string, any>) => {
  const { name, label, disabled, checked } = props;

  return (
    <Field
      name={name}
      render={(fieldProps: FieldProps) => {
        const { form, field } = fieldProps;
        return (
          <FormControlLabel
            label={<CheckboxLabel>{label}</CheckboxLabel>}
            control={
              <Checkbox
                size="small"
                onChange={() => form.setFieldValue(field.name, !field.value)}
                checked={field.value ?? checked}
                disabled={disabled}
                {...props}
              />
            }
          />
        );
      }}
    />
  );
};

const TimeMaskedInput: FC = forwardRef((props: any, ref: any) => {
  const timeMask = [/\d/, /\d/, ':', /\d/, /\d/];

  return (
    <MaskedInput
      {...props}
      keepCharPositions
      placeholder="__:__"
      mask={timeMask}
      ref={ref}
    />
  );
});

export const EditData = () => {
  const [
    preferencesCriteria,
    preferencesNotifications,
    preferencesSmsProvider,
    preferencesBuildings,
    checkedBuildings,
  ] = useUnit([
    $preferencesCriteria,
    $preferencesNotifications,
    $preferencesSmsProvider,
    $preferencesBuildings,
    $checkedBuildings,
  ]);

  const preferences: SettingsEditData = {
    preferencesCriteria,
    preferencesNotifications,
    preferencesSmsProvider,
    is_notifications_period: !!preferencesNotifications.period,
    checkedBuildings,
    preferencesBuildingsAvailableCount: preferencesBuildings.available_count,
  };

  const dayRef = useRef<HTMLInputElement>();
  const debtRef = useRef<HTMLInputElement>();

  return (
    <CustomScrollbar autoHide>
      <Formik
        initialValues={preferences}
        validationSchema={validationSchema}
        onSubmit={submitSettings}
        render={({ values, setFieldValue, setFieldTouched }) => {
          const checked = values.checkedBuildings.filter(
            (building) => building.checked
          ).length;

          return (
            <Form placeholder="">
              <StyledSettings>
                <SettingsHeader pageStatus={PageStatus.edit} />

                <StyledItem fullWidth>
                  <StyledTitle>
                    {t('DebtorsSettings.AssignmentOfDebtorStatus') as string}{' '}
                    <Tooltip title={t('DebtorsSettings.StatusAssignment')} arrow>
                      <StyledInfoIcon />
                    </Tooltip>
                  </StyledTitle>
                  {values.preferencesCriteria.is_enabled ? (
                    <StyledChip
                      label={t('DebtorsNotifications.Automatically') as string}
                    />
                  ) : (
                    <StyledChip label={t('DebtorsNotifications.Manually') as string} />
                  )}
                </StyledItem>

                <StyledItem inActive={!values.preferencesCriteria.day_limit_enabled}>
                  <StyledPaddingBottom>
                    <CustomCheckbox
                      name="preferencesCriteria.day_limit_enabled"
                      label={t('DebtorsSettings.NumberOfDaysToPayBill') as string}
                      onChange={() => {
                        setFieldValue('preferencesCriteria.day_limit', '');
                        setFieldValue(
                          'preferencesCriteria.day_limit_enabled',
                          !values.preferencesCriteria.day_limit_enabled
                        );

                        setTimeout(() => dayRef.current?.focus(), 0);
                      }}
                    />
                    <Tooltip title={t('DebtorsSettings.MinimumAllowedValue')} arrow>
                      <StyledInfoIcon />
                    </Tooltip>
                  </StyledPaddingBottom>

                  <Field
                    innerRef={dayRef}
                    name="preferencesCriteria.day_limit"
                    type="number"
                    label={false}
                    divider={false}
                    disabled={!values.preferencesCriteria.day_limit_enabled}
                    placeholder={t('DebtorsSettings.EnterData')}
                    component={InputField}
                  />
                </StyledItem>

                <StyledItem inActive={!values.preferencesCriteria.debt_limit_enabled}>
                  <StyledPaddingBottom>
                    <CustomCheckbox
                      name="preferencesCriteria.debt_limit_enabled"
                      label={t('DebtorsSettings.MaximalDebt') as string}
                      onChange={() => {
                        setFieldValue('preferencesCriteria.debt_limit', '');
                        setFieldValue(
                          'preferencesCriteria.debt_limit_enabled',
                          !values.preferencesCriteria.debt_limit_enabled
                        );

                        setTimeout(() => debtRef.current?.focus(), 0);
                      }}
                    />
                  </StyledPaddingBottom>

                  <Field
                    innerRef={debtRef}
                    name="preferencesCriteria.debt_limit"
                    type="number"
                    label={false}
                    divider={false}
                    disabled={!values.preferencesCriteria.debt_limit_enabled}
                    placeholder={t('DebtorsSettings.EnterData')}
                    component={InputField}
                  />
                </StyledItem>

                <StyledItem fullWidth>
                  <StyledTitle>
                    {t('objects')} (
                    {`${t('DebtorsSettings.MaximumNumberOfBuildings')} - ${
                      values.preferencesBuildingsAvailableCount
                    }`}
                    ){' '}
                    <Tooltip title={t('DebtorsSettings.BuildingsAlert')} arrow>
                      <StyledInfoIcon />
                    </Tooltip>
                  </StyledTitle>
                  <StyledBuildings>
                    {(values.checkedBuildings ?? []).map((building, index) => (
                      <p>
                        <CustomCheckbox
                          name={`checkedBuildings.${index}.checked`}
                          label={building.title}
                          disabled={
                            checked >= values.preferencesBuildingsAvailableCount &&
                            !values.checkedBuildings[index].checked
                          }
                          onChange={() => {
                            setFieldTouched(`preferencesBuildingsAvailableCount`, true);

                            setFieldValue(
                              `checkedBuildings.${index}.checked`,
                              !values.checkedBuildings[index].checked
                            );
                          }}
                        />
                      </p>
                    ))}
                  </StyledBuildings>

                  <div>
                    <ErrorMessage
                      name="preferencesBuildingsAvailableCount"
                      render={(msg) => <StyledError>{msg}</StyledError>}
                    />
                  </div>
                </StyledItem>

                <StyledItem fullWidth>
                  <StyledTitleH3>
                    {t('DebtorsSettings.NotificationSettings') as string}
                  </StyledTitleH3>
                  <StyledTitle>
                    {t('DebtorsSettings.NotificationText') as string}
                  </StyledTitle>
                  <NotificationText
                    text={values?.preferencesNotifications.text ?? ''}
                    setFieldValue={setFieldValue}
                    variables={preferencesNotifications.variables}
                    name="preferencesNotifications.text"
                    label={false}
                    divider={false}
                    placeholder={t('DebtorsSettings.EnterData')}
                    multiline
                    rows={4}
                  />
                </StyledItem>

                <StyledItem>
                  <StyledTitleH3>
                    {t('DebtorsSettings.SendingNotifications') as string}
                  </StyledTitleH3>
                  <CustomCheckbox
                    name="preferencesNotifications.is_automatic"
                    label={t('DebtorsSettings.NotifyAutomatically') as string}
                  />

                  <StyledTitleH3 marginTop>
                    {`${t('DebtorsSettings.SmsProvider')} `}
                    <Tooltip
                      title={`${t('DebtorsList.functionalityOfSendingSms')} ${t(
                        'DebtorsList.SmsGateway'
                      )}`}
                    >
                      <StyledInfoIcon />
                    </Tooltip>
                  </StyledTitleH3>
                  <StyledTitle>App ID</StyledTitle>

                  <Field
                    name="preferencesSmsProvider.credentials.app_id"
                    label={false}
                    divider={false}
                    placeholder={t('DebtorsSettings.EnterAppId')}
                    component={InputField}
                  />

                  <StyledTitle marginTop>
                    {t('DebtorsNotifications.NewsletterService') as string}
                  </StyledTitle>

                  <FieldArray
                    name="preferencesNotifications.channels"
                    render={(arrayHelpers) => {
                      return Object.keys(CHANNELS_NAMES).map((channel, index) => {
                        return (
                          <FormControlLabel
                            key={index}
                            label={
                              <CheckboxLabel>{CHANNELS_NAMES[channel]}</CheckboxLabel>
                            }
                            control={
                              <Checkbox
                                size="small"
                                onChange={() => {
                                  const channels =
                                    arrayHelpers.form.values.preferencesNotifications
                                      .channels;
                                  if (!channels.includes(channel)) {
                                    arrayHelpers.insert(index, channel);

                                    return;
                                  }

                                  arrayHelpers.remove(index);
                                }}
                                checked={(
                                  arrayHelpers.form.values.preferencesNotifications
                                    .channels ?? []
                                ).includes(channel)}
                              />
                            }
                          />
                        );
                      });
                    }}
                  />
                </StyledItem>

                <StyledItem noBottomBorder></StyledItem>

                <StyledItem>
                  <StyledTitle>
                    {t('Label.date') as string}
                    {` (${t('DebtorsSettings.dateOfEachMonth') as string})`}
                  </StyledTitle>
                  <Field
                    name="preferencesNotifications.day"
                    label={false}
                    divider={false}
                    placeholder={t('DebtorsSettings.EnterData')}
                    component={InputField}
                  />
                </StyledItem>

                <StyledItem>
                  <StyledTitle>{t('Label.time') as string}</StyledTitle>
                  <Field
                    name="preferencesNotifications.time"
                    component={InputField}
                    inputComponent={TimeMaskedInput}
                    label={false}
                    placeholder={t('DebtorsSettings.EnterData')}
                    divider={false}
                  />
                </StyledItem>

                <StyledItem>
                  <StyledTitle>
                    {t('DebtorsSettings.SendingNotifications') as string}
                  </StyledTitle>
                  <CustomCheckbox
                    name="is_notifications_period"
                    label={t('DebtorsSettings.NotifyPeriodically')}
                    onChange={() => {
                      if (values.is_notifications_period) {
                        setFieldValue('preferencesNotifications.period', '');
                      }

                      setFieldValue(
                        'is_notifications_period',
                        !values.is_notifications_period
                      );
                    }}
                  />

                  <StyledText marginTop inActive={!values.is_notifications_period}>
                    <Field
                      name="preferencesNotifications.period"
                      label={t('DebtorsSettings.NotifyPeriod')}
                      divider={false}
                      placeholder={t('DebtorsSettings.EnterData')}
                      component={InputField}
                    />
                  </StyledText>
                </StyledItem>

                <StyledItem noBottomBorder></StyledItem>
              </StyledSettings>
            </Form>
          );
        }}
      />
    </CustomScrollbar>
  );
};
