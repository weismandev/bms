import { useUnit } from 'effector-react';
import { Form, Formik } from 'formik';
import { t } from 'i18next';
import { $checkedBuildings, $isLoading, $preferencesBuildings } from '../../model';
import { CustomCheckbox } from './editData';
import { StyledBuildings, StyledItem, StyledTitle } from './styled';

export const PreferencesBuildings = () => {
  const [checkedBuildings, isLoading, preferencesBuildings] = useUnit([
    $checkedBuildings,
    $isLoading,
    $preferencesBuildings,
  ]);

  if (isLoading) return null;

  const initialValues = {
    checkedBuildings,
  };

  return (
    <StyledItem fullWidth>
      <Formik
        initialValues={initialValues}
        onSubmit={() => {}}
        render={({ values }) => (
          <Form>
            <StyledTitle>
              {t('objects')} (
              {`${t('DebtorsSettings.MaximumNumberOfBuildings')} - ${
                preferencesBuildings.available_count
              }`}
              )
            </StyledTitle>
            <StyledBuildings>
              {(values.checkedBuildings ?? []).map((building) => (
                <p key={building.id}>
                  <CustomCheckbox
                    label={building.title}
                    checked={building.checked}
                    disabled
                  />
                </p>
              ))}
            </StyledBuildings>
          </Form>
        )}
      />
    </StyledItem>
  );
};
