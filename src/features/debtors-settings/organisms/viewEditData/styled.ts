import { Chip } from '@mui/material';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import styled from '@emotion/styled';

export const StyledSettings = styled.div`
  display: flex;
  flex-wrap: wrap;
  background: #fff;
  box-shadow: 0px 4px 15px #e7e7ec;
  border-radius: 16px;
  padding: 12px;
  position: relative;
`;

export const StyledItem = styled.div((props: Record<string, any>) => ({
  flex: props.fullWidth ? '0 0 calc(100% - 24px)' : '0 0 calc(50% - 24px)',
  borderBottom: props.noBottomBorder ? 'none' : '1px solid #E7E7EC',
  padding: '22px 0',
  margin: '0 12px',
  opacity: props.inActive ? 0.4 : 1,
}));

export const StyledHeaderItem = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 12px;
  width: 100%;
  position: sticky;
  top: 0;
  background: #fff;
  z-index: 100;
`;

export const StyledHeaderItemLeft = styled.div``;

export const StyledHeaderItemRight = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 10px;
`;

export const StyledTitle = styled.p<{ marginTop?: boolean }>`
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #7d7d8e;
  margin-bottom: 8px;
  margin-top: ${({ marginTop }) => (marginTop ? '24px' : '0')};
`;

export const StyledTitleH3 = styled.h3<{ marginTop?: boolean }>`
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  color: #3b3b50;
  margin-bottom: 24px;
  margin-top: ${({ marginTop }) => (marginTop ? '24px' : '0')};
`;

export const StyledText = styled.div<{
  flex?: boolean;
  marginTop?: boolean;
  inActive?: boolean;
}>`
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  color: #3b3b50;
  display: ${({ flex }) => (flex ? 'flex' : 'block')};
  align-items: center;
  gap: 8px;
  margin-top: ${({ marginTop }) => (marginTop ? '24px' : '0')};
  opacity: ${({ inActive }) => (inActive ? 0.4 : 1)};
`;

export const StyledTextNote = styled.span`
  font-size: 11px;
  line-height: 12px;
  color: #7d7d8e;
`;

export const StyledChip = styled(Chip)(() => ({
  color: '#fff',
  background: '#0394E3',
  fontWeight: 500,
  fontSize: '13px',
  lineHeight: '15px',
  marginRight: '8px',
}));

export const CheckboxLabel = styled.span`
  font-weight: 500;
  font-size: 13px;
  line-height: 15px;
  color: #65657b;
`;

export const StyledWrap = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledVariables = styled.div`
  display: flex;
  gap: 8px;
  justify-content: flex-start;
  margin-top: 14px;
`;

export const StyledVariable = styled.button<{ usedVariable?: boolean }>`
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  color: #ffffff;
  background: ${(props) => (props.usedVariable ? '#AAAAAA' : '#1bb169')};
  border-radius: 2px;
  padding: 2px 4px;
  border: 0;
  cursor: pointer;
`;

export const StyledVariableTitle = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  color: #7d7d8e;
  padding: 2px 0;
`;

export const StyledInfoIcon = styled(ErrorOutlineIcon)`
  width: 20px;
  height: 20px;
`;

export const StyledLightText = styled.span`
  font-weight: 300;
  font-size: 12px;
  line-height: 14px;
  color: #8d8a8a;
`;

export const StyledPaddingBottom = styled.div`
  padding-bottom: 10px;
`;

export const StyledBuildings = styled.div`
  display: inline-block;
  max-height: 209px;
  overflow: auto;
  background: #f4f7fa;
  > p {
    border-bottom: 1px solid #191c291f;
    padding: 8px 16px;
    margin: 0;
  }
`;

export const StyledError = styled.span`
  color: #d32f2f;
  font-weight: 400;
  font-size: 0.75rem;
  line-height: 1.66;
`;
