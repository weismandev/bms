import * as Yup from 'yup';
import { SettingsEditData } from '@features/debtors-settings/interfaces';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  preferencesBuildingsAvailableCount: Yup.number().test(
    'test-preferences-buildings-available-count',
    '',
    testPreferencesBuildingsAvailableCount
  ),
  preferencesCriteria: Yup.object().shape({
    day_limit: Yup.mixed()
      .nullable()
      .test(
        'validate-if-enabled',
        t('DebtorsSettings.ValidationDayLimit') as string,
        testDayLimit
      ),
    debt_limit: Yup.mixed()
      .nullable()
      .test(
        'validate-if-enabled',
        t('DebtorsSettings.ValidationDebtlimit') as string,
        testDebtLimit
      ),
  }),
  preferencesNotifications: Yup.object().shape({
    period: Yup.mixed()
      .nullable()
      .test(
        'validate-if-is_notifications_period',
        t('DebtorsSettings.ValidationPeriod') as string,
        testPeriodLimit
      ),
    day: Yup.mixed()
      .nullable()
      .test('validate-day', t('DebtorsSettings.ValidationDay') as string, testDayAuto),
    time: Yup.mixed()
      .nullable()
      .test('validate-time', t('DebtorsSettings.ValidationTime') as string, testTimeAuto),
  }),
});

function testPreferencesBuildingsAvailableCount(this: any, value: any) {
  const parent = this.parent as SettingsEditData;
  const max = value;
  const factNumber = parent.checkedBuildings.filter(
    (building) => !!building.checked
  ).length;

  if (!factNumber) {
    return this.createError({
      message: t('DebtorsSettings.SelectAtLeastOneObject'),
      path: 'preferencesBuildingsAvailableCount',
    });
  }

  if (factNumber <= max) return true;

  return this.createError({
    message: `${t('DebtorsSettings.MaximumNumberOfBuildings')} - ${max}`,
    path: 'preferencesBuildingsAvailableCount',
  });
}

function testTimeAuto(this: any, value: any) {
  const [hours, minutes] = value.split(':');

  const nHours = Number(hours);
  const nMinutes = Number(minutes);

  if (isNaN(nHours) || nHours < 0 || nHours > 23) return this.createError();

  if (isNaN(nMinutes) || nMinutes < 0 || nMinutes > 59) return this.createError();

  return true;
}

function testDayAuto(this: any, value: any) {
  if (value >= 1 && value <= 31) return true;

  return this.createError();
}

function testDayLimit(this: any, value: any) {
  if (!!this.parent?.day_limit_enabled) {
    if (value === null || value === '') return this.createError();
  } else {
    if (value === null || value === '') return true;
  }

  if (value < 1) return this.createError();

  const reg = new RegExp('^[0-9]{1,}$');

  if (!reg.test(value)) return this.createError();

  return true;
}

function testDebtLimit(this: any, value: any) {
  if (!!this.parent?.debt_limit_enabled) {
    if (value === null || value === '') return this.createError();
  } else {
    if (value === null || value === '') return true;
  }

  if (value < 1) return this.createError();

  const reg = new RegExp('^[0-9]{1,}[.]{0,1}[0-9]{0,}$');

  if (!reg.test(value)) return this.createError();

  return true;
}

function testPeriodLimit(this: any) {
  if (!this.from[1].value.is_notifications_period) return true;

  if (this.parent?.period < 1 || this.parent?.period > 30) return this.createError();

  const reg = new RegExp('^[0-9]{1,}$');

  if (!reg.test(this.parent?.period)) return this.createError();

  return true;
}
