import { HaveSectionAccess } from '@features/common';
import { Loader, Wrapper } from '@ui/atoms';
import { ErrorMessage } from '@ui/index';
import { FilterMainDetailLayout } from '@ui/templates';
import { useUnit } from 'effector-react';
import { $error, $isLoading, clearError, PageGate } from '../model';
import { ViewEditData } from '../organisms';

const DebtorsgeneralSettingsPage = () => {
  const [isLoading, error] = useUnit([$isLoading, $error]);

  return (
    <div>
      <PageGate />
      <ErrorMessage isOpen={error} onClose={clearError} error={error} />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout main={<ViewEditData />} />;
    </div>
  );
};

const DebtorsgeneralSettingsPageAccess = () => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <DebtorsgeneralSettingsPage />
    // </HaveSectionAccess>
  );
};

export { DebtorsgeneralSettingsPageAccess };
