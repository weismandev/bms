import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
}));

export const styles = {
  button: {
    width: 30,
    height: 30,
    margin: '0 5px',
  },
  icon: {
    width: 20,
    height: 20,
  },
};
