import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { nanoid } from 'nanoid';
import { EquipmentSelectField } from '@features/equipment-select';
import { WorkFlowsSelectField } from '@features/work-flows-select';
import { WorkSelectField } from '@features/works-select';
import { FormSectionHeader } from '@ui/index';
import { DeleteButton, CopyButton } from '../../atoms';
import { Works, Device } from '../../interfaces';
import { $modeWorks } from '../../models';
import { useStyles, styles } from '../../organisms/detail/styles';
import { useStyles as useStylesWork, styles as stylesWork } from './styles';

interface Props {
  work: Works;
  index: number;
  push: (obj: any) => void;
  remove: (index: number) => void;
}

export const WorkCard: FC<Props> = ({ work, index, push, remove }) => {
  const { t } = useTranslation();
  const modeWorks = useUnit($modeWorks);
  const classes = useStyles({ external: false });
  const classesWork = useStylesWork();

  const titleIfNotDevice = !work?.id ? t('NewList') : t('WithoutDevice');

  const title = (work.device as Device)?.title
    ? (work.device as Device).title
    : titleIfNotDevice;

  const name = `works[${index}]`;
  const isDeviceRequired = work.works.length === 0 && work.techCards.length === 0;
  const isTechCardRequired = !work.device && work.works.length === 0;
  const isWorkRequired = !work.device && work.techCards.length === 0;

  const isDisabledCopyButton =
    !work.device || (work.techCards.length === 0 && work.works.length === 0);

  const onDelete = () => remove(index);
  const onCopy = () => push({ ...work, id: null, device: '', _id: nanoid(3) });

  return (
    <div className={classes.container} key={work._id}>
      <div className={classesWork.header}>
        <FormSectionHeader header={title} />
        {modeWorks === 'edit' && (
          <div>
            <CopyButton
              iconStyle={stylesWork.icon}
              style={stylesWork.button}
              onClick={onCopy}
              disabled={isDisabledCopyButton}
            />
            <DeleteButton
              iconStyle={stylesWork.icon}
              style={stylesWork.button}
              onClick={onDelete}
              disabled={index === 0}
            />
          </div>
        )}
      </div>
      <div className={classes.field}>
        <Field
          label={t('ServiceObject')}
          placeholder={t('selectObject')}
          component={EquipmentSelectField}
          name={`${name}.device`}
          mode={modeWorks}
          helpText={t('ForListAppearStartTypingName')}
          menuPlacement="auto"
          required={isDeviceRequired}
        />
      </div>
      <div className={classes.field} style={styles.fullWidth}>
        <Field
          label={t('TechnicalMap')}
          placeholder={t('SelectTechnicalMap')}
          component={WorkFlowsSelectField}
          name={`${name}.techCards`}
          mode={modeWorks}
          isMulti
          required={isTechCardRequired}
          path="work-flows"
        />
      </div>
      <div className={classes.field} style={styles.fullWidth}>
        <Field
          label={t('WorkTitle')}
          placeholder={t('ChooseAWork')}
          component={WorkSelectField}
          name={`${name}.works`}
          mode={modeWorks}
          isMulti
          helpText={t('AddWorksOutsideOfTechicalMapIfRequired')}
          required={isWorkRequired}
          path="works"
        />
      </div>
    </div>
  );
};
