import { TicketListExport } from './ux';

export default {
  title: 'TicketListExport',
  component: TicketListExport,
};

const Template = (args: any) => (
  <TicketListExport {...args} />
);

export const Default = Template.bind({});

// @ts-ignore
Default.args = {

};
