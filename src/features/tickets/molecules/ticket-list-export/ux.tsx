import { useEffect, useState } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Control } from '@effector-form';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  LinearProgress,
} from '@mui/material';

import { fxExportTickets, emailForm, $isLoading, ExportTickets } from '../../models/ticket-export';
import { StyledSuccessWrap, StyledSuccessMessage, StyledCheckCircleIcon } from './styled';

type Props = {
  isOpenTicketListExport: boolean;
  onCloseTicketListExport: () => void;
};

export const TicketListExport = ({
  isOpenTicketListExport,
  onCloseTicketListExport
}: Props) => {
  const { t } = useTranslation();
  const { submit } = useUnit(emailForm);
  const isLoading = useUnit($isLoading);
  const [isSendByEmail, setIsSendByEmail] = useState<boolean>(false);

  const subscribefxExportTickets = ({ params }: { params: ExportTickets }) => {
    if (params.email) {
      setIsSendByEmail(true);
    }
  };

  useEffect(() => {
    const unsubscribeFx = fxExportTickets.done.watch(subscribefxExportTickets);
    return unsubscribeFx;
  });

  useEffect(() => {
    setIsSendByEmail(false);
    return () => {
      setIsSendByEmail(false);
    };
  }, [isOpenTicketListExport]);

  const renderDialogContent = (
    <>
      <DialogContent>
        <Control.Email
          fullWidth
          form={emailForm}
          name="email"
          placeholder={t('EnterEmail') ?? ''}
          disabled={isLoading}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onCloseTicketListExport} disabled={isLoading}>
          {t('Cancellation')}
        </Button>
        <Button color="primary" variant="contained" onClick={submit}>{t('Send')}</Button>
      </DialogActions>
    </>
  );
  const renderSuccessContent = (
    <>
      <StyledSuccessWrap>
        <StyledCheckCircleIcon />
        <StyledSuccessMessage>
          {t('DebtorsNotifications.DataSentSuccessfully')}
        </StyledSuccessMessage>
      </StyledSuccessWrap>
      <Button onClick={onCloseTicketListExport} disabled={isLoading}>
        {t('close')}
      </Button>
    </>
  );
  return (
    <Dialog open={isOpenTicketListExport} onClose={onCloseTicketListExport}>
      {isLoading && <LinearProgress />}
      <DialogTitle>{t('ExportinglistOfApplicationsByEmail')}</DialogTitle>
      {isSendByEmail ? renderSuccessContent : renderDialogContent}
    </Dialog>
  );
};
