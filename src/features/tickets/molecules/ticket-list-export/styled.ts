import styled from '@emotion/styled';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

export const StyledSuccessWrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 48px;
  margin-top: 48px;
`;

export const StyledSuccessMessage = styled.div`
  padding-left: 12px;
`;

export const StyledCheckCircleIcon = styled(CheckCircleIcon)({
  color: '#1BB169',
});
