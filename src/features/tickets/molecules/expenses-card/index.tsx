import { FC } from 'react';
import { Field, FieldArray, FieldProps } from 'formik';
import { useTranslation } from 'react-i18next';

import { InputField } from '@ui/index';

import { MaterialCard } from '../material-card';
import { ExpansesMaterial } from '../../interfaces';
import { useStyles } from './styles';

interface Props {
  mode: string;
  parentName: string;
  values: ExpansesMaterial;
}

export const ExpensesCard: FC<Props> = ({ mode, parentName, values }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <>
      <div className={classes.field}>
        <Field
          label={t('TechnicalMap')}
          placeholder=""
          component={InputField}
          mode="view"
          name={`${parentName}.workTypeGroupTitle`}
          divider={false}
        />
      </div>
      <div className={classes.field}>
        <Field
          label={t('Work')}
          placeholder=""
          component={InputField}
          mode="view"
          name={`${parentName}.workTitle`}
          divider={false}
        />
      </div>
      <div className={classes.costField}>
        <Field
          label={t('LaborCosts')}
          placeholder=""
          component={InputField}
          mode={mode}
          type="time"
          name={`${parentName}.workTime`}
          required
          divider={false}
        />
      </div>
      <FieldArray
        name={`${parentName}.materials`}
        render={() => {
          if (!Array.isArray(values?.materials)) {
            return null;
          }

          return values.materials.map((_: unknown, idx: number) => (
            <Field
              key={idx}
              name={`${parentName}.materials[${idx}]`}
              render={({ field }: FieldProps) => (
                <MaterialCard
                  parentName={field.name}
                  mode={mode}
                  index={idx}
                  isLastElement={values.materials.length - 1 === idx}
                />
              )}
            />
          ));
        }}
      />
    </>
  );
};
