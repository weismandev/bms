import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  field: {
    marginBottom: 15,
  },
  costField: {
    width: 100,
    marginBottom: 15,
  },
}));
