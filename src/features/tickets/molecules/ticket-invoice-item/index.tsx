import { FC, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import ReactFileReader from 'react-file-reader';
import { Field } from 'formik';
import { useMediaQuery, Typography } from '@mui/material';
import { Add, AttachFile } from '@mui/icons-material';

import { ActionButton, DeleteButton, InputField } from '@ui/index';
import { InvoicesStatusTypeSelectField } from '@features/tickets-invoices-status-select';

import { DownloadButton } from '../../atoms';
import { $mode, uploadAttachment, $attachment, downloadFile } from '../../models';
import { Ticket, FileInvoice } from '../../interfaces';
import { useStyles, styles } from './styles';

interface Props {
  values: Ticket;
  push: (entity: object) => void;
  remove: (index: number) => void;
  setFieldValue: (name: string, value: any) => void;
  errors: { invoices: { [key: number]: string }[] };
}

interface InvoiceItem {
  id?: number;
  file: { id: number; name: string } | FileInvoice;
  amount: number;
  status: { id: string; title: string };
}

const invoiceEntity = {
  id: null,
  file: null,
  amount: '',
  status: '',
};

export const TicketInvoiceItem: FC<Props> = ({
  push,
  remove,
  values,
  setFieldValue,
  errors,
}) => {
  const { t } = useTranslation();
  const [mode, attachment] = useUnit([$mode, $attachment]);
  const classes = useStyles({ mode });
  const isScreenLess = useMediaQuery('(max-width:1495px)');

  useEffect(() => {
    if (attachment) {
      setFieldValue(`invoices[${attachment.index}].file.id`, attachment.id);
      setFieldValue(`invoices[${attachment.index}].file.name`, attachment.name);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [attachment]);

  const onClick = () => push(invoiceEntity);

  const invoices = Array.isArray(values?.invoices)
    ? values.invoices.map((item: InvoiceItem, index) => {
        const name = `invoices[${index}]`;

        const invoiceError = Array.isArray(errors?.invoices)
          ? errors.invoices[index]
          : false;

        const onDelete = () => remove(index);
        const handleDownload = () => downloadFile(item.file as FileInvoice);

        const handleFiles = (files: File[]) => {
          uploadAttachment({ file: files[0], index });
        };

        const handleAmount = (e: { target: { value: string } }) => {
          const { value } = e.target;
          const splittedValue = value.split('.');

          if (splittedValue[0] && splittedValue[1] && splittedValue[1].length > 2) {
            const newValue = `${splittedValue[0]}.${splittedValue[1].substring(0, 2)}`;

            return setFieldValue(`${name}.amount`, Number.parseFloat(newValue));
          }

          if (!Number.isNaN(Number.parseFloat(value))) {
            return setFieldValue(`${name}.amount`, Number.parseFloat(value));
          }

          return setFieldValue(`${name}.amount`, value);
        };

        const handleKeyDown = (e: { keyCode: number; preventDefault: () => void }) => {
          if (e.keyCode === 110 || e.keyCode === 190) {
            e.preventDefault();
          }
        };

        const deleteButton = mode === 'edit' && (
          <DeleteButton
            onClick={onDelete}
            style={isScreenLess ? styles.deleteButtonScreenLess : styles.deleteButton}
          />
        );

        const fields = (
          <div className={classes.fields}>
            <div className={classes.amountField}>
              <Field
                name={`${name}.amount`}
                label={null}
                placeholder={t('AmountPayable')}
                mode={mode}
                component={InputField}
                divider={false}
                type="number"
                onKeyDown={handleKeyDown}
                onChange={handleAmount}
              />
            </div>
            {item?.id && (
              <div className={classes.statusField}>
                <Field
                  name={`${name}.status`}
                  label={null}
                  placeholder={t('selectStatuse')}
                  mode={mode}
                  component={InvoicesStatusTypeSelectField}
                  divider={false}
                />
              </div>
            )}
          </div>
        );

        const file = item?.file ? (
          <div className={isScreenLess ? classes.fileScreenLess : classes.file}>
            {mode === 'view' && <DownloadButton onClick={handleDownload} />}
            <Typography
              noWrap
              title={item.file.name}
              classes={{ root: classes.fileName }}
            >
              {item.file.name}
            </Typography>
          </div>
        ) : (
          <div>
            <ReactFileReader fileTypes="*" handleFiles={handleFiles}>
              <ActionButton
                icon={<AttachFile style={styles.attachIcon} />}
                kind="positive"
                style={styles.attachButton}
              >
                {t('AttachFile')}
              </ActionButton>
            </ReactFileReader>
            {Boolean((invoiceError as { file: string })?.file) && (
              <Typography classes={{ root: classes.errorFile }}>
                {(invoiceError as { file: string }).file}
              </Typography>
            )}
          </div>
        );

        return (
          <Field
            key={index}
            name={name}
            render={() =>
              isScreenLess ? (
                <div>
                  {deleteButton}
                  {file}
                  {fields}
                </div>
              ) : (
                <div className={classes.invoice}>
                  {file}
                  {fields}
                  {deleteButton}
                </div>
              )
            }
          />
        );
      })
    : null;

  const addButton = mode === 'edit' && (
    <ActionButton
      icon={<Add style={styles.addIcon} />}
      kind="positive"
      style={styles.addButton}
      onClick={onClick}
    >
      {t('AddInvoice')}
    </ActionButton>
  );

  return (
    <>
      {invoices}
      {addButton}
    </>
  );
};
