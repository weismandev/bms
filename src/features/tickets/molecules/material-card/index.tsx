import { FC } from 'react';
import { Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { InputField } from '@ui/index';

import { useStyles } from './styles';

interface Props {
  mode: string;
  parentName: string;
  index: number;
  isLastElement: boolean;
}

export const MaterialCard: FC<Props> = ({ mode, parentName, index, isLastElement }) => {
  const { t } = useTranslation();
  const classes = useStyles({ isEditMode: mode === 'edit', isLastElement });

  return (
    <div className={classes.fields}>
      <div className={classes.field}>
        <Field
          label={index === 0 && t('Material')}
          placeholder=""
          component={InputField}
          mode="view"
          name={`${parentName}.material`}
          divider={false}
        />
      </div>
      <div className={classes.field}>
        <Field
          label={index === 0 && t('Amount')}
          placeholder=""
          component={InputField}
          mode={mode}
          name={`${parentName}.materialCount`}
          type="number"
          required
          divider={false}
        />
      </div>
      <div className={classes.field}>
        <Field
          label={index === 0 && t('UnitsMeasurements')}
          placeholder=""
          component={InputField}
          mode="view"
          name={`${parentName}.materialUnit`}
          divider={false}
        />
      </div>
    </div>
  );
};
