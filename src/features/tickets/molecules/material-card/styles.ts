import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  fields: {
    display: 'grid',
    gridTemplateColumns: '2fr 1fr 100px',
    gap: 20,
    marginBottom: ({ isLastElement }) => isLastElement && 15,
  },
  field: {
    marginBottom: ({ isEditMode }: { isEditMode: boolean }) => (isEditMode ? 10 : 0),
  },
}));
