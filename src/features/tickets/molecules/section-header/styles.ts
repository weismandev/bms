import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  icon: {
    height: 25,
    width: 25,
    margin: '3px 15px 0px 0px',
  },
}));
