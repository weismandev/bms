import { FC } from 'react';
import { IconButton } from '@mui/material';
import { ExpandMore, ExpandLess } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { FormSectionHeader } from '@ui/index';

import { useStyles } from './styles';

interface Props {
  title: string;
  isExpanded: boolean;
  onClick: () => void;
  children?: JSX.Element | false;
  childrenPosition?: string;
}

export const SectionHeader: FC<Props> = ({
  title,
  isExpanded,
  onClick,
  children,
  childrenPosition = 'right',
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const collapse = t('Collapse');
  const expand = t('Expand');

  return (
    <div className={classes.header}>
      <div>
        <FormSectionHeader header={title} />
        {childrenPosition === 'left' && children}
      </div>
      <div>
        {childrenPosition === 'right' && children}
        <IconButton onClick={onClick} className={classes.icon} size="large">
          {isExpanded ? (
            <ExpandMore titleAccess={collapse} />
          ) : (
            <ExpandLess titleAccess={expand} />
          )}
        </IconButton>
      </div>
    </div>
  );
};
