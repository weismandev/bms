import { FC, useState, useEffect } from 'react';
import { useUnit } from 'effector-react';

import { Tooltip, Fade, Menu, MenuItem } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { AddButton, Greedy, AdaptiveSearch, FilterButton, ExportButton } from '@ui/index';
import { TicketListExport } from '../ticket-list-export';
import { exportTickets } from '../../models/ticket-export';
import {
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
  $rowCount,
  $isDetailOpen,
  addClicked,
  $columns,
  $tableData,
  $visibilityColumns,
} from '../../models';
import { styles } from './styles';

export const CustomToolbar: FC = () => {
  const { t } = useTranslation();
  const [
    searchValue,
    isFilterOpen,
    totalCount,
    isDetailOpen,
    columns,
    tableData,
    visibilityColumns,
  ] = useUnit([
    $search,
    $isFilterOpen,
    $rowCount,
    $isDetailOpen,
    $columns,
    $tableData,
    $visibilityColumns,
  ]);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [isOpenTicketListExport, setIsOpenTicketListExport] = useState<boolean>(false);

  const isAnySideOpen = isFilterOpen && isDetailOpen;
  const hiddenColumnNames = Object.values(visibilityColumns).filter((column) => !column);

  const onClickFilter = () => changedFilterVisibility(true);
  const handleClickExportButton = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseTicketListExport = () => {
    setIsOpenTicketListExport(false);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickExport = () => {
    handleClose();
    exportTickets({
      limit: 1000
    });
  };

  const handleClickExportEmail = () => {
    setIsOpenTicketListExport(true);
    handleClose();
  };

  const open = Boolean(anchorEl);

  useEffect(() => () => {
    setIsOpenTicketListExport(false);
  }, []);

  return (
    <>
      <Tooltip title={t('Filters')}>
        <div>
          <FilterButton
            disabled={isFilterOpen}
            style={styles.margin}
            onClick={onClickFilter}
          />
        </div>
      </Tooltip>
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <Greedy />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        onClick={handleClickExportButton}
        style={{ marginRight: 10 }}
      />
      <Tooltip title={t('add')}>
        <div>
          <AddButton disabled={isDetailOpen} onClick={addClicked} />
        </div>
      </Tooltip>
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem onClick={handleClickExport}>
          {`${t('UploadToExcel')} (1000 ${t('ofTickets')})`}
        </MenuItem>
        <MenuItem onClick={handleClickExportEmail}>
          {`${t('SendByEmail')} (40000 ${t('ofTickets')})`}
        </MenuItem>
      </Menu>
      <TicketListExport
        isOpenTicketListExport={isOpenTicketListExport}
        onCloseTicketListExport={handleCloseTicketListExport}
      />
    </>
  );
};
