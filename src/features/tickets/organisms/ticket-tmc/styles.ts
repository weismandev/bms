import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  tmcIcon: {
    marginLeft: 15,
    marginTop: -4,
    color: '#FF8A00',
  },
}));

export const styles = {
  button: {
    padding: '10px 15px',
    marginBottom: 20,
  },
};
