import { useState, FC, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { Field, FieldArray, FormikErrors } from 'formik';
import { Collapse } from '@mui/material';
import { DoneSharp, InfoOutlined } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { FormSectionHeader, ActionButton } from '@ui/index';

import { $mode, setTmc, $ticket } from '../../models';
import { useStyles } from '../detail/styles';
import { Ticket, ExpansesMaterial } from '../../interfaces';
import { useStyles as useStylesTmc, styles as stylesTmc } from './styles';
import { SectionHeader, ExpensesCard } from '../../molecules';

interface Props {
  values: Ticket;
  errors: FormikErrors<Ticket>;
  setFieldValue: (name: string, value: any) => void;
}

export const TicketTmc: FC<Props> = ({ values, errors, setFieldValue }) => {
  const { t } = useTranslation();
  const [ticket, mode] = useUnit([$ticket, $mode]);
  const [expanded, setExpanded] = useState(true);
  const classes = useStyles({ external: false });
  const classesTmc = useStylesTmc();

  useEffect(() => {
    if (ticket.tmc.length > 0 && values.tmc.length > 0) {
      if (JSON.stringify(ticket.tmc) !== JSON.stringify(values.tmc)) {
        setFieldValue('tmc', ticket.tmc);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ticket.tmc]);

  const isActCreated = Array.isArray(values?.tmc)
    ? Boolean(values.tmc[0]?.isActCreated)
    : null;

  const editDisabled =
    typeof isActCreated === 'object' ||
    (typeof isActCreated === 'boolean' && isActCreated);

  const modeTmc = editDisabled ? 'view' : mode;
  const writeOffRequired = t('WriteOffRequired');

  const handleExpanded = () => setExpanded(!expanded);

  const onClick = () => {
    if (Array.isArray(errors?.tmc) && errors.tmc.length > 0) {
      return null;
    }

    return setTmc(values);
  };

  return (
    <div className={classes.container}>
      <SectionHeader
        title={t('CostWriteOff')}
        isExpanded={expanded}
        onClick={handleExpanded}
        childrenPosition="left"
      >
        {typeof isActCreated === 'boolean' && isActCreated === false && (
          <InfoOutlined
            classes={{ root: classesTmc.tmcIcon }}
            titleAccess={writeOffRequired}
          />
        )}
      </SectionHeader>
      <Collapse in={expanded} timeout="auto">
        <FieldArray
          name="tmc"
          render={() => {
            if (!Array.isArray(values?.tmc)) {
              return null;
            }

            return values.tmc.map((tmc, index) => {
              const name = `tmc[${index}]`;

              return (
                <>
                  <FormSectionHeader header={tmc.tmcTitle} />
                  <FieldArray
                    name={`${name}.expenses`}
                    render={() => {
                      if (!Array.isArray(tmc?.expenses)) {
                        return null;
                      }

                      return tmc.expenses.map((item, idx) => (
                        <Field
                          key={idx}
                          name={`expenses[${idx}]`}
                          render={() => (
                            <ExpensesCard
                              parentName={`${name}.expenses[${idx}]`}
                              mode={modeTmc}
                              values={item as ExpansesMaterial}
                            />
                          )}
                        />
                      ));
                    }}
                  />
                </>
              );
            });
          }}
        />
        {typeof isActCreated === 'boolean' && (
          <ActionButton
            icon={<DoneSharp />}
            kind="positive"
            style={stylesTmc.button}
            disabled={mode === 'edit' ? isActCreated : true}
            onClick={onClick}
          >
            {t('WriteOff')}
          </ActionButton>
        )}
      </Collapse>
    </div>
  );
};
