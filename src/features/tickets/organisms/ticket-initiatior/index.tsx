import { useState, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { EnterpriseSelectField } from '@features/enterprise-select';
import { PeopleSelectField } from '@features/people-select';
import { formatPhone } from '@tools/formatPhone';
import { InputField } from '@ui/index';
import { Initiator, Ticket } from '../../interfaces';
import { formatObjects } from '../../libs';
import { $mode } from '../../models';
import { SectionHeader } from '../../molecules';
import { useStyles, styles } from '../detail/styles';

interface Props {
  values: Ticket;
  isNew: boolean;
}

export const TicketInitiator: FC<Props> = ({ isNew, values }) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);
  const handleExpanded = () => setExpanded(!expanded);
  const classes = useStyles({ external: false });

  const formattedOjects = formatObjects(values);

  return (
    <>
      <SectionHeader
        title={t('Declarant')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              label={t('Declarant')}
              placeholder={t('Placeholder.EnterFullName')}
              component={PeopleSelectField}
              name="initiator"
              mode={!isNew ? 'view' : mode}
              object={formattedOjects ? [formattedOjects] : null}
              // required
            />
          </div>
          <div className={classes.field}>
            <Field
              label={t('DeclarantPhone')}
              placeholder=""
              component={InputField}
              mode="view"
              value={formatPhone((values.initiator as Initiator)?.phone) || ''}
            />
          </div>
        </div>
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              label={t('DeclarantEmail')}
              placeholder=""
              component={InputField}
              mode="view"
              value={(values.initiator as Initiator)?.email || ''}
            />
          </div>
        </div>
        <div className={classes.field}>
          <Field
            label={t('company')}
            placeholder={t('ChooseCompany')}
            component={EnterpriseSelectField}
            name="enterprise"
            mode={!isNew ? 'view' : mode}
          />
        </div>
        {!isNew && (
          <>
            <div className={classes.field}>
              <Field
                label={t('DeclarantRating')}
                placeholder=""
                component={InputField}
                mode="view"
                name="rate"
              />
            </div>
            <div className={classes.field} style={styles.fullWidth}>
              <Field
                label={t('Label.comment')}
                placeholder=""
                component={InputField}
                mode="view"
                name="comment"
              />
            </div>
          </>
        )}
      </Collapse>
    </>
  );
};
