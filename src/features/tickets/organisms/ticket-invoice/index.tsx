import { FC, useState, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { FieldArray } from 'formik';
import { Collapse, Typography } from '@mui/material';

import { $mode, $ticket } from '../../models';
import { Ticket } from '../../interfaces';
import { useStyles } from '../detail/styles';
import { useStyles as useStylesInvoice } from './styles';
import { SectionHeader, TicketInvoiceItem } from '../../molecules';

interface Props {
  values: Ticket;
  setFieldValue: (name: string, value: any) => void;
  errors: { invoices: { [key: number]: string }[] };
  isNew: boolean;
}

export const TicketInvoice: FC<Props> = ({ values, setFieldValue, errors, isNew }) => {
  const { t } = useTranslation();
  const [ticket, mode] = useUnit([$ticket, $mode]);
  const [expanded, setExpanded] = useState(true);
  const classes = useStyles({ external: false });
  const classesInvoice = useStylesInvoice();

  const { invoices } = ticket as Ticket;
  const isPaidtype =
    values.types.length > 0 ? Boolean([...values.types].reverse()[0]?.paid) : false;

  const handleExpanded = () => setExpanded(!expanded);

  useEffect(() => {
    if (invoices.length > 0 && values.invoices.length > 0) {
      if (JSON.stringify(invoices) !== JSON.stringify(values.invoices)) {
        setFieldValue('invoices', invoices);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [invoices]);

  if (!isPaidtype || isNew) {
    return null;
  }

  return (
    <div className={classes.container}>
      <SectionHeader
        title={t('Invoices')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        {(values.invoices.length > 0 || mode === 'edit') && (
          <FieldArray
            name="invoices"
            render={({ push, remove }) => (
              <TicketInvoiceItem
                push={push}
                remove={remove}
                values={values}
                setFieldValue={setFieldValue}
                errors={errors}
              />
            )}
          />
        )}
        {values.invoices.length === 0 && mode === 'view' && (
          <Typography classes={{ root: classesInvoice.notInvoicesText }}>
            {t('InvoicesWillBeDisplayedHere')}
          </Typography>
        )}
      </Collapse>
    </div>
  );
};
