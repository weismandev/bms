import { memo, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import { ApartmentSelectField } from '@features/apartment-select';
import { ComplexSelectField } from '@features/complex-select';
import { EmployeeSelectField } from '@features/employee-select';
import { HouseSelectField } from '@features/house-select';
import { AsyncPeopleSelect } from '@features/people-async-select';
import { PropertySelectField } from '@features/property-select';
import { ClassSelectField } from '@features/tickets-class-select';
import { InvoicesStatusTypeSelectField } from '@features/tickets-invoices-status-select';
import { TypeSelectField } from '@features/tickets-new-type-select';
import { TicketsPaymentTypeSelectField } from '@features/tickets-payment-type-select';
import { PrioritySelectField } from '@features/tickets-priority-select';
import { StatusSelectField } from '@features/tickets-status-select';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
  Divider,
  SelectField,
  SwitchField,
} from '@ui/index';
import { Value, TypesData } from '../../interfaces';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { RateSelectField } from '../rate-select';
import { SlaSelectField } from '../sla-select';
import { StuffWriteSelectField } from '../stuff-write-select';
import { useStyles } from './styles';
import { validationSchema } from './validation';

const PeopleField = (props: object) => (
  <SelectField component={<AsyncPeopleSelect enterprise_id={null} />} {...props} />
);

export const Filters: FC = memo(() => {
  const { t } = useTranslation();
  const filters = useUnit($filters);
  const classes = useStyles();

  const onClose = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  const filterByLevelType = (levelType: number) =>
    ({ level }: { level: number }) => levelType === level;

  return (
    <Wrapper className={classes.wrapper}>
      <CustomScrollbar autoHide>
        <FilterToolbar className={classes.toolbar} closeFilter={onClose} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          validationSchema={validationSchema}
          render={({ values, setFieldValue }) => {
            const handlePayment = (value: Value) => {
              setFieldValue('payment', value);
              setFieldValue('invoiceStatus', '');
            };

            const onChangeTypesByLevel = (level: number) => (value: TypesData[]) => {
              setFieldValue(`types.${level}`, value.filter((type) => type.level === level));
            };

            return (
              <Form className={classes.form}>
                <>
                  <p className={classes.title}>{t('DateOfCreation')}</p>
                  <div className={classes.dateField}>
                    <div>
                      <Field
                        name="created_at_from"
                        label={null}
                        divider={false}
                        component={InputField}
                        placeholder=""
                        type="date"
                      />
                    </div>
                    <p className={classes.dateDivider}>-</p>
                    <div>
                      <Field
                        name="created_at_to"
                        label={null}
                        divider={false}
                        component={InputField}
                        placeholder=""
                        type="date"
                      />
                    </div>
                  </div>
                  <Divider />
                </>
                <Field
                  name="statuses"
                  component={StatusSelectField}
                  label={t('RequestStatus')}
                  placeholder={t('SelectRequestStatus')}
                  isMulti
                />
                <Field
                  name="priorities"
                  component={PrioritySelectField}
                  label={t('ExecutionPriority')}
                  placeholder={t('ChooseAPriority')}
                  isMulti
                />
                <Field
                  name="classes"
                  component={ClassSelectField}
                  label={t('RequestClass')}
                  placeholder={t('SelectRequestClass')}
                  isMulti
                />
                <FieldArray
                  name="types"
                  render={() => (
                    <>
                      <Field
                        label={`${t('RequestType')} 1`}
                        placeholder={`${t('SelectRequestType')} 1`}
                        component={TypeSelectField}
                        name="types.0"
                        isMulti
                        onChange={onChangeTypesByLevel(0)}
                      />

                      <Field
                        label={`${t('RequestType')} 2`}
                        placeholder={`${t('SelectRequestType')} 2`}
                        component={TypeSelectField}
                        name="types.1"
                        isMulti
                        onChange={onChangeTypesByLevel(1)}
                      />

                      <Field
                        label={`${t('RequestType')} 3`}
                        placeholder={`${t('SelectRequestType')} 3`}
                        component={TypeSelectField}
                        name="types.2"
                        isMulti
                        onChange={onChangeTypesByLevel(2)}
                      />
                    </>
                  )}
                />
                <Field
                  name="payment"
                  label={t('Payment')}
                  placeholder={t('SelectPayment')}
                  component={TicketsPaymentTypeSelectField}
                  divider={values?.payment?.id !== 1}
                  onChange={handlePayment}
                />
                {typeof values?.payment === 'object' && values.payment?.id === 1 && (
                  <div className={classes.invoiceField}>
                    <Field
                      name="invoiceStatus"
                      label={null}
                      placeholder={t('selectStatuse')}
                      component={InvoicesStatusTypeSelectField}
                    />
                  </div>
                )}
                <Field
                  name="objects"
                  component={ComplexSelectField}
                  label={t('AnObject')}
                  placeholder={t('selectObject')}
                  isMulti
                />
                <Field
                  name="houses"
                  complex={
                    Array.isArray(values?.objects) &&
                    values.objects.length > 0 &&
                    values.objects
                  }
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  isMulti
                />
                <Field
                  name="apartments"
                  building_ids={
                    Array.isArray(values?.houses) &&
                    values.houses.map((house: { id: number }) => house.id)
                  }
                  component={ApartmentSelectField}
                  label={t('Label.flat')}
                  placeholder={t('selectThatApartment')}
                  isMulti
                />
                <Field
                  name="property"
                  houseList={values.houses}
                  component={PropertySelectField}
                  label={t('room')}
                  placeholder={t('ChooseRoom')}
                  isMulti
                />
                <Field
                  label={t('Declarant')}
                  placeholder={t('Placeholder.EnterFullName')}
                  disableCache
                  component={PeopleField}
                  name="initiators"
                  helpText={t('EnterAtLeastOneLetter')}
                  isMulti
                />
                <Field
                  name="stuff_write_off_needed"
                  component={StuffWriteSelectField}
                  label={t('WriteOffRequired')}
                  placeholder={t('SpecifyTheNeedToWriteOff')}
                />
                <Field
                  name="expired"
                  component={SlaSelectField}
                  label={t('SLAStandard')}
                  placeholder={t('SpecifyTheSLAStandard')}
                />
                <Field
                  label={t('Performers')}
                  placeholder={t('SelectPerformers')}
                  component={EmployeeSelectField}
                  name="assignees"
                  isMulti
                  divider
                />
                <Field
                  label={t('Rating')}
                  placeholder={t('SelectRating')}
                  component={RateSelectField}
                  name="rates"
                  isMulti
                  divider
                />
                <Field
                  label={t('HavingUnreadMessages')}
                  component={SwitchField}
                  name="isUnreadMessage"
                  labelPlacement="start"
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
