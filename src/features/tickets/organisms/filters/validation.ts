import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { startDateAfterFinishDateFilters } from '../../libs';

const { t } = i18n;

const startDateBeforeEndDate = t('StartDateBeforeEndDate');
const endDateAfterStartDate = t('EndDateAfterStartDate');

export const validationSchema = Yup.object().shape({
  created_at_from: Yup.string()
    .test(
      'start_date-after-finish_date',
      startDateBeforeEndDate,
      startDateAfterFinishDateFilters
    )
    .nullable(),
  created_at_to: Yup.string()
    .test(
      'start_date-after-finish_date',
      endDateAfterStartDate,
      startDateAfterFinishDateFilters
    )
    .nullable(),
});
