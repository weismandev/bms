import { FC } from 'react';

import { SelectControl, SelectField } from '@ui/index';

import { data } from '../../models/rate-select';
import { Value } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

export const RateSelect: FC<Props> = (props) => (
  <SelectControl options={data} {...props} />
);

export const RateSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RateSelect />} onChange={onChange} {...props} />;
};
