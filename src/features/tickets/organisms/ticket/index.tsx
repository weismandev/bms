import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik } from 'formik';

import { Wrapper } from '@ui/index';

import { $ticket, ticketSubmitted } from '../../models';
import { TicketForm } from '../ticket-form';
import { validationSchema } from './validation';
import { useStyles } from './styles';

interface Props {
  external?: boolean;
}

export const Ticket: FC<Props> = ({ external = false }) => {
  const ticket = useUnit($ticket);
  const classes = useStyles({ external });

  const isNew = !ticket.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={ticket}
        validationSchema={validationSchema}
        onSubmit={ticketSubmitted}
        enableReinitialize
        render={({ values, setFieldValue, resetForm, errors }) => (
          <TicketForm
            values={values}
            setFieldValue={setFieldValue}
            resetForm={resetForm}
            errors={errors}
            external={external}
            isNew={isNew}
          />
        )}
      />
    </Wrapper>
  );
};
