import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import {
  startDateAfterFinishDate,
  requiredTypeRepeat,
  requiredEveryRepeat,
  startDateAfterCurrentDate,
  requiredAmountInvoice,
  positiveAmountInvoice,
  maxValueAmountInvoice,
  requiredFileInvoice,
  requiredStatusInvoice,
  requiredInitiator,
} from '../../libs';

const { t } = i18n;

const thisIsRequiredField = t('thisIsRequiredField');
const startDateBeforeEndDate = t('StartDateBeforeEndDate');
const startDateMustNotBeLaterThanTheCurrentDate = t(
  'StartDateMustNotBeLaterThanTheCurrentDate'
);
const endDateAfterStartDate = t('EndDateAfterStartDate');
const uploadInvoiceFile = t('UploadInvoiceFile');
const amountMustNotBeNegative = t('AmountMustNotBeNegative');
const maximumAmountExceeded = t('MaximumAmountExceeded');

export const validationSchema = Yup.object().shape({
  class: Yup.mixed().required(thisIsRequiredField),
  description: Yup.string().required(thisIsRequiredField),
  priority: Yup.mixed().required(thisIsRequiredField),
  origin: Yup.mixed().required(thisIsRequiredField),
  status: Yup.mixed().required(thisIsRequiredField),
  object: Yup.mixed().required(thisIsRequiredField),
  house: Yup.mixed().required(thisIsRequiredField),
  // initiator: Yup.mixed().test(
  //   'initiator-required',
  //   thisIsRequiredField,
  //   requiredInitiator
  // ),
  planned_start_at: Yup.mixed()
    .test(
      'start_date-after-finish_date',
      startDateBeforeEndDate,
      startDateAfterFinishDate
    )
    .test(
      'start-date-after-current-date',
      startDateMustNotBeLaterThanTheCurrentDate,
      startDateAfterCurrentDate
    ),
  planned_end_at: Yup.mixed().test(
    'start_date-after-finish_date',
    endDateAfterStartDate,
    startDateAfterFinishDate
  ),
  // type_repeat: Yup.mixed().test('type-required', thisIsRequiredField, requiredTypeRepeat),
  // every_repeat: Yup.mixed().test(
  //   'every-required',
  //   thisIsRequiredField,
  //   requiredEveryRepeat
  // ),
  expenses: Yup.array().of(
    Yup.object().shape({
      workTime: Yup.string().required(thisIsRequiredField),
      materials: Yup.array().of(
        Yup.object().shape({
          materialCount: Yup.string().required(thisIsRequiredField),
        })
      ),
    })
  ),
  invoices: Yup.array().of(
    Yup.object().shape({
      file: Yup.mixed().test('file-required', uploadInvoiceFile, requiredFileInvoice),
      amount: Yup.mixed()
        .test('amount-required', thisIsRequiredField, requiredAmountInvoice)
        .test('amount-positive', amountMustNotBeNegative, positiveAmountInvoice)
        .test('amount-max-value', maximumAmountExceeded, maxValueAmountInvoice),
      status: Yup.mixed().test(
        'status-required',
        thisIsRequiredField,
        requiredStatusInvoice
      ),
    })
  ),
});
