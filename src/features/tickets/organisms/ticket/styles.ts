import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: ({ external }: { external: boolean }) =>
      external ? 'calc(100% - 72px)' : 'calc(100% - 50px)',
    boxShadow: 'none',
  },
}));
