import { FC } from 'react';

import { SelectControl, SelectField } from '@ui/index';

import { data } from '../../models/stuff-write-select';
import { Value } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  props?: object;
}

export const StuffWriteSelect: FC<Props> = (props) => (
  <SelectControl options={data} {...props} />
);

export const StuffWriteSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StuffWriteSelect />} onChange={onChange} {...props} />;
};
