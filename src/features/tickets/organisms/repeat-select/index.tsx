import { FC } from 'react';

import { SelectControl, SelectField } from '@ui/index';

import { data } from '../../models/repeat-select';
import { Repeat } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const RepeatSelect: FC<object> = (props) => (
  <SelectControl options={data} {...props} />
);

export const RepeatSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: Repeat) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RepeatSelect />} onChange={onChange} {...props} />;
};
