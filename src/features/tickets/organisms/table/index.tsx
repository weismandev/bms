import { memo, FC } from 'react';
import { useUnit } from 'effector-react';
import { Tooltip, Chip } from '@mui/material';
import { ErrorOutlineOutlined } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { Wrapper, HiddenArray } from '@ui/index';
import { DataGrid } from '@features/data-grid';
import { InvoiceStatusItem } from '@features/tickets-invoices-status-select';
import Paid from '@img/paid.svg';
import Sla from '@img/sla.svg';
import positiveSmile from '@img/positive_smile.png';
import negativeSmile from '@img/negative_smile.png';

import {
  $tableData,
  open,
  $isLoadingList,
  $selectRow,
  selectionRowChanged,
  sortChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  statusColor,
} from '../../models';
import { CustomToolbar } from '../../molecules';
import { useStyles, styles } from './styles';

export const Table: FC = memo(() => {
  const { t } = useTranslation();
  const [
    tableData,
    isLoading,
    selectRow,
    sorting,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
  ] = useUnit([
    $tableData,
    $isLoadingList,
    $selectRow,
    $sorting,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
  ]);
  const classes = useStyles();

  const numberIndex = columns.findIndex((column) => column.field === 'number');
  const typeIndex = columns.findIndex((column) => column.field === 'type');
  const paymentIndex = columns.findIndex((column) => column.field === 'payment');
  const slaIndex = columns.findIndex((column) => column.field === 'sla');
  const statusIndex = columns.findIndex((column) => column.field === 'status');
  const rateIndex = columns.findIndex((column) => column.field === 'rate');
  const deviceIndex = columns.findIndex((column) => column.field === 'devices');
  const unreadMessagesIndex = columns.findIndex(
    (column) => column.field === 'unreadMessages'
  );

  columns[numberIndex].renderCell = ({ value, row: { id } }: { value: string }) => {

    return <a href={`/tickets/${id}`}>{value}</a>;
  };

  columns[typeIndex].renderCell = ({
    value,
    row: { formattedTypes, isTicketPaid },
  }: {
    value: string;
    row: { formattedTypes: string; isTicketPaid: boolean };
  }) => (
    <div className={classes.typeContainer}>
      {formattedTypes.length > 0 ? (
        <Tooltip title={formattedTypes}>
          <div>
            {isTicketPaid ? (
              <Paid style={styles.paidIcon} />
            ) : (
              <ErrorOutlineOutlined style={styles.infoIcon} />
            )}
          </div>
        </Tooltip>
      ) : null}
      <div className={classes.typeName}>{value}</div>
    </div>
  );

  columns[paymentIndex].renderCell = ({ value }: { value: InvoiceStatusItem }) => {
    if (!value) {
      return null;
    }

    const color = value?.id === 'paid' ? '#1BB169' : '#EB5757';
    return <span style={value?.id && { color }}>{value?.title}</span>;
  };

  columns[slaIndex].renderCell = ({ value }: { value: boolean }) => {
    if (!value) {
      return '';
    }

    return <Sla title={t('SLAExpired')} />;
  };

  columns[statusIndex].renderCell = ({ value }: { value: string[] }) => {
    const id = value[0];
    const title = value[1];

    return (
      <Chip
        label={title}
        size="small"
        style={{
          backgroundColor: id && statusColor[id]?.backgroundColor,
          color: id && statusColor[id]?.color,
        }}
      />
    );
  };

  columns[rateIndex].renderCell = ({ value }: { value: number }) => {
    if (!Number.isInteger(value)) {
      return value;
    }
    return (
      <span>
        {value}
        <img src={value > 3 ? positiveSmile : negativeSmile} className={classes.smile} />
      </span>
    );
  };

  columns[deviceIndex].renderCell = ({ value }: { value: string[] }) => {
    if (!Array.isArray(value)) {
      return null;
    }

    return (
      <HiddenArray>
        {value.map((item, index) => (
          <span key={index}>{item}</span>
        ))}
      </HiddenArray>
    );
  };

  columns[unreadMessagesIndex].renderCell = ({ value }: { value: number }) => {
    const style = value
      ? {
          backgroundColor: 'rgb(27, 177, 105)',
          color: 'rgb(255, 255, 255)',
        }
      : {
          backgroundColor: 'rgb(224, 224, 224)',
          color: 'rgba(0, 0, 0, 0.87)',
        };

    return <Chip label={value} size="small" style={style} />;
  };

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    open,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <Wrapper className={classes.wrapper}>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </Wrapper>
  );
});
