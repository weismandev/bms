import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    padding: 24,
  },
  tableRoot: {
    cursor: 'pointer',
  },
  typeContainer: {
    display: 'flex',
  },
  typeName: {
    marginTop: 4,
  },
  smile: {
    marginLeft: 10,
    width: 25,
    height: 25,
  },
});

export const styles = {
  paidIcon: {
    marginRight: 5,
  },
  infoIcon: {
    marginRight: 5,
    color: '#9F9F9F',
  },
};
