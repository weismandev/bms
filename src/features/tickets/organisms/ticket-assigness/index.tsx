import { useState, FC } from 'react';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { EmployeeSelectField } from '@features/employee-select';

import { $mode } from '../../models';
import { useStyles, styles } from '../detail/styles';
import { SectionHeader } from '../../molecules';

export const TicketAssigness: FC = () => {
  const { t } = useTranslation();
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);
  const classes = useStyles({ external: false });

  const handleExpanded = () => setExpanded(!expanded);

  return (
    <div className={classes.container}>
      <SectionHeader
        title={t('Performers')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            label={t('Performer')}
            placeholder={t('SelectPerformer')}
            component={EmployeeSelectField}
            name="assignees"
            divider={false}
            isMulti
            mode={mode}
          />
        </div>
      </Collapse>
    </div>
  );
};
