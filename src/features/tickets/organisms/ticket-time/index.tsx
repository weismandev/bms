import { useState, FC } from 'react';
import { useUnit } from 'effector-react';
import DatePicker from 'react-datepicker';
import { addMinutes, differenceInMinutes } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Field, FieldProps } from 'formik';
import { Collapse } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { InputField } from '@ui/index';

import { $mode } from '../../models';
import { Ticket } from '../../interfaces';
import { useStyles } from '../detail/styles';
import { formatTime } from '../../libs';
import { SectionHeader } from '../../molecules';

interface Props {
  values: Ticket;
  setFieldValue: (name: string, value: object | string) => void;
  errors: { planned_start_at?: Date | string; planned_end_at?: Date | string };
}

const checkExistDates = (start: Date | string, end: Date | string) =>
  start?.toString()?.length > 0 && end?.toString()?.length > 0;

export const TicketTime: FC<Props> = ({
  values: { planned_start_at, planned_end_at, types, planned_duration },
  setFieldValue,
  errors,
}) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);
  const classes = useStyles({ external: false });
  const [expanded, setExpanded] = useState(true);

  const specifyStartTime = t('SpecifyStartTime');
  const specifyEndTime = t('SpecifyEndTime');

  const today = new Date();

  const hasErrorsValidate =
    Boolean(errors.planned_start_at) || Boolean(errors.planned_end_at);

  const durationWork =
    checkExistDates(planned_start_at, planned_end_at) && !hasErrorsValidate
      ? differenceInMinutes(
          new Date(planned_end_at),
          new Date(planned_start_at)
        ).toString()
      : null;

  const plannedTime =
    types.length === 0 || !types[0]
      ? planned_duration
      : types[types.length - 1]?.planned_duration_minutes;

  const plannedTimeValue = durationWork || plannedTime || '';
  const time = plannedTimeValue || planned_duration;
  const formattedTime = time ? formatTime(time) : '';

  const handleExpanded = () => setExpanded(!expanded);

  return (
    <>
      <SectionHeader
        title={t('Timeslot')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              name="planned_start_at"
              render={(props: FieldProps) => {
                const onChangeStartDate = (value: Date) => {
                  setFieldValue('planned_start_at', value);

                  if (!value) {
                    return setFieldValue('planned_end_at', '');
                  }

                  if (time) {
                    const startDate = new Date(value);

                    const minutes = Number.parseInt(time.toString(), 10);

                    return setFieldValue(
                      'planned_end_at',
                      new Date(addMinutes(startDate, minutes))
                    );
                  }

                  return null;
                };

                const error = props.form.errors?.planned_start_at;

                return (
                  <DatePicker
                    customInput={
                      // eslint-disable-next-line react/jsx-wrap-multilines
                      <InputField
                        label={t('PlannedStartTimeWork')}
                        errors={error ? [error] : []}
                        error={Boolean(error)}
                        {...props}
                      />
                    }
                    placeholderText={specifyStartTime}
                    name={props.field.name}
                    readOnly={mode === 'view'}
                    selected={props.field.value}
                    onChange={onChangeStartDate}
                    minDate={today}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    locale={dateFnsLocale}
                    timeCaption={t('Label.time').toLowerCase()}
                    dateFormat="dd.MM.yyyy в HH:mm"
                    popperPlacement="top"
                  />
                );
              }}
            />
          </div>
          <div className={classes.field}>
            <Field
              name="planned_end_at"
              render={(props: FieldProps) => {
                const onChangeEnd = (value: Date) => {
                  setFieldValue('planned_end_at', value);
                };

                const error = props.form.errors?.planned_end_at;

                return (
                  <DatePicker
                    customInput={
                      // eslint-disable-next-line react/jsx-wrap-multilines
                      <InputField
                        label={t('PlannedEndTimeWork')}
                        {...props}
                        errors={error ? [error] : []}
                        error={Boolean(error)}
                      />
                    }
                    placeholderText={specifyEndTime}
                    name={props.field.name}
                    readOnly={mode === 'view'}
                    selected={props.field.value}
                    onChange={onChangeEnd}
                    minDate={today}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    locale={dateFnsLocale}
                    timeCaption={t('Label.time').toLowerCase()}
                    dateFormat="dd.MM.yyyy в HH:mm"
                    popperPlacement="top"
                  />
                );
              }}
            />
          </div>
        </div>
        <div className={classes.field}>
          <Field
            label={t('PlannedDurationOfWork')}
            placeholder=""
            component={InputField}
            value={formattedTime}
            mode="view"
          />
        </div>
      </Collapse>
    </>
  );
};
