import { useState, FC } from 'react';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse } from '@mui/material';
import { useTranslation } from 'react-i18next';

import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { ApartmentSelectField } from '@features/apartment-select';
import { PropertySelectField } from '@features/property-select';

import { $mode, checkAppeal } from '../../models';
import { Ticket, Value } from '../../interfaces';
import { useStyles } from '../detail/styles';
import { SectionHeader } from '../../molecules';

interface Props {
  values: Ticket;
  setFieldValue: (name: string, value: any) => void;
  isNew: boolean;
}

export const TicketObjects: FC<Props> = ({ values, setFieldValue, isNew }) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);
  const [expanded, setExpanded] = useState(true);
  const classes = useStyles({ external: false });

  const handleExpanded = () => setExpanded(!expanded);

  const onChangeObject = (value: Value) => {
    setFieldValue('object', value);
    setFieldValue('house', '');
    setFieldValue('apartment', '');
    setFieldValue('property', '');
    setFieldValue('devices', '');
    setFieldValue('initiator', '');
    setFieldValue('is_appeal', false);
    checkAppeal(value?.id);
  };

  const onChangeHouse = (value: Value) => {
    setFieldValue('house', value);
    setFieldValue('apartment', '');
    setFieldValue('property', '');
    setFieldValue('devices', '');
    setFieldValue('initiator', '');
  };

  const onChangeApartment = (value: Value) => {
    setFieldValue('apartment', value);
    setFieldValue('initiator', '');
  };

  const onChangeProperty = (value: Value) => {
    setFieldValue('property', value);
    setFieldValue('initiator', '');
  };

  return (
    <>
      <SectionHeader
        title={t('RequestObject')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              label={t('AnObject')}
              placeholder={t('selectObject')}
              component={ComplexSelectField}
              name="object"
              mode={!isNew ? 'view' : mode}
              required
              onChange={onChangeObject}
            />
          </div>
          <div className={classes.field}>
            <Field
              label={t('building')}
              placeholder={t('selectBbuilding')}
              component={HouseSelectField}
              complex={values.object ? [values.object] : []}
              name="house"
              mode={!isNew ? 'view' : mode}
              required
              onChange={onChangeHouse}
            />
          </div>
        </div>
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              building_id={(values.house as Value)?.id}
              label={t('Label.flat')}
              placeholder={t('selectThatApartment')}
              component={ApartmentSelectField}
              name="apartment"
              mode={!isNew ? 'view' : mode}
              onChange={onChangeApartment}
            />
          </div>
          <div className={classes.field}>
            <Field
              houseList={(values.house as Value)?.id && [values.house]}
              label={t('room')}
              placeholder={t('ChooseRoom')}
              component={PropertySelectField}
              name="property"
              mode={!isNew ? 'view' : mode}
              onChange={onChangeProperty}
            />
          </div>
        </div>
      </Collapse>
    </>
  );
};
