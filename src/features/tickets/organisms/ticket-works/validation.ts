import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { deviceRequired, techCardsRequired, worksRequired } from '../../libs';

const { t } = i18n;

const thisIsRequiredField = t('thisIsRequiredField');

export const validationSchema = Yup.object().shape({
  works: Yup.array().of(
    Yup.object().shape({
      device: Yup.mixed()
        .test('device-required', thisIsRequiredField, deviceRequired)
        .nullable(),
      techCards: Yup.array().test(
        'tech-cards-required',
        thisIsRequiredField,
        techCardsRequired
      ),
      works: Yup.array().test('works-required', thisIsRequiredField, worksRequired),
    })
  ),
});
