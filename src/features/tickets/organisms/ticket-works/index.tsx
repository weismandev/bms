import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Form, FieldArray } from 'formik';
import { nanoid } from 'nanoid';

import { DetailToolbar, CustomScrollbar } from '@ui/index';

import { AddButton } from '../../atoms';
import { WorkCard } from '../../molecules';
import {
  $works,
  $modeWorks,
  changeWorksMode,
  setTicketWorks,
  $ticket,
} from '../../models';
import { useStyles } from '../detail/styles';
import { useStyles as useStylesWorks } from './styles';
import { validationSchema } from './validation';

export const TicketWorks: FC = () => {
  const [works, modeWorks, ticket] = useUnit([$works, $modeWorks, $ticket]);
  const classes = useStyles({ external: false });
  const classesWorks = useStylesWorks();

  const status = typeof ticket.status === 'object' ? ticket.status.id : ticket.status;
  const isHideEditButton = status !== 'new';

  return (
    <Formik
      initialValues={{ works }}
      validationSchema={validationSchema}
      onSubmit={setTicketWorks}
      enableReinitialize
      render={({ values, resetForm, errors }) => {
        const hasError = Object.values(errors).length > 0 || values.works.length === 0;

        const onEdit = () => changeWorksMode('edit');

        const onCancel = () => {
          resetForm();
          changeWorksMode('view');
        };

        return (
          <Form className={classesWorks.form}>
            <DetailToolbar
              className={classes.toolbar}
              mode={modeWorks}
              hidden={{ edit: isHideEditButton, close: true, delete: true }}
              onEdit={onEdit}
              onCancel={onCancel}
              hasError={hasError}
            />
            <div className={classes.formContent}>
              <CustomScrollbar>
                <div className={classes.content}>
                  <FieldArray
                    name="works"
                    render={({ push, remove }) => {
                      const worksElements = values.works.map((work, index) => (
                        <WorkCard
                          work={work}
                          index={index}
                          key={work._id}
                          push={push}
                          remove={remove}
                        />
                      ));

                      const isDisabledAddButton =
                        values.works.length === 1 ? !values.works[0]?.device : false;

                      const onClick = () =>
                        push({
                          _id: nanoid(3),
                          id: null,
                          device: '',
                          techCards: [],
                          works: [],
                        });

                      return (
                        <>
                          {worksElements}
                          {modeWorks === 'edit' && (
                            <AddButton disabled={isDisabledAddButton} onClick={onClick} />
                          )}
                        </>
                      );
                    }}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};
