import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  form: {
    position: 'relative',
    height: 'calc(100% - 58px)',
  },
}));
