import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Tab } from '@mui/material';

import { Wrapper, Tabs } from '@ui/index';

import { $currentTab, changeTab, tabs, $ticket } from '../../models';
import { Ticket } from '../ticket';
import { TicketWorks } from '../ticket-works';
import { useStyles } from './styles';

export const Detail: FC = () => {
  const [currentTab, ticket] = useUnit([$currentTab, $ticket]);
  const classes = useStyles({ external: false });

  const onChange = (_: unknown, tab: string) => changeTab(tab);

  const isNew = !ticket.id;

  return (
    <Wrapper className={classes.wrapper}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={onChange}
        isNew={isNew}
        tabEl={<Tab />}
      />
      {currentTab === 'info' && <Ticket />}
      {currentTab === 'works' && <TicketWorks />}
    </Wrapper>
  );
};
