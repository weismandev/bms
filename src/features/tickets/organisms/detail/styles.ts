import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    position: 'relative',
    height: ({ external }: { external: boolean }) =>
      external ? '100%' : 'calc(100% - 12px)',
  },
  toolbar: {
    padding: '12px 24px 24px 12px',
  },
  formContent: {
    padding: '0px 0px 24px 24px',
    height: ({ external }: { external: boolean }) =>
      external ? '100%' : 'calc(100% - 64px)',
  },
  content: {
    paddingRight: 16,
    paddingBottom: 15,
  },
  field: {
    paddingBottom: 10,
    paddingRight: 15,
    width: '50%',
    '@media (max-width: 1024px)': {
      width: '100%',
    },
  },
  container: {
    background: '#EDF6FF',
    borderRadius: 16,
    padding: '18px 24px 10px',
    marginBottom: 15,
  },
  fieldsContainer: {
    display: 'grid',
  },
  twoFields: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
}));

export const styles = {
  fullWidth: {
    width: '100%',
  },
};
