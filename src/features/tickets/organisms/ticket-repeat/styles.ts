import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  text: {
    color: '#9494A3',
    fontWeight: 400,
    fontSize: 14,
    marginTop: 5,
    marginLeft: 10,
  },
  repeatEvery: {
    width: ({ mode }: { mode: string }) => (mode === 'view' ? '40%' : '70%'),
  },
  container: {
    display: 'flex',
  },
}));
