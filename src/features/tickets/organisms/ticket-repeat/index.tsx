import { useState, FC } from 'react';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Collapse, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { InputField, SwitchField } from '@ui/index';
import { wordDeclension, wordGender } from '@tools/word-declension';
import { Word, Cases } from '@tools/word-declension/word-declension.types';
import { RepeatSelectField } from '../repeat-select';
import { $mode } from '../../models';
import { Ticket } from '../../interfaces';
import { useStyles } from '../detail/styles';
import { useStyles as useStylesRepeat } from './styles';
import { SectionHeader } from '../../molecules';

interface Props {
  values: Ticket;
  isNew: boolean;
}
export const TicketRepeat: FC<Props> = ({ values, isNew }) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);
  const classes = useStyles({ external: false });
  const classesRepeat = useStylesRepeat({ mode });
  const [expanded, setExpanded] = useState(true);
  const type: Word = values.type_repeat?.id;
  const repeat = values.every_repeat;
  let name = t('day');
  if (type && repeat) {
    name = wordDeclension(type, Number(repeat));
  }
  const isShow = !isNew ? true : Boolean(values.is_repeat_ticket);
  const handleExpanded = () => setExpanded(!expanded);
  return (
    <div className={classes.container}>
      <SectionHeader
        title={t('RepeatRequest')}
        isExpanded={expanded}
        onClick={handleExpanded}
      />
      <Collapse in={expanded} timeout="auto">
        {isNew && (
          <div className={classes.field}>
            <Field
              name="is_repeat_ticket"
              component={SwitchField}
              label={t('RecurringRequest')}
              labelPlacement="end"
            />
          </div>
        )}
        {isShow && (
          <>
            <div className={classes.field}>
              <Field
                label={t('Periodicity')}
                placeholder={t('EnterPeriodicity')}
                component={RepeatSelectField}
                mode={mode}
                divider={false}
                name="type_repeat"
                // required
              />
            </div>
            <div className={classes.twoFields}>
              <div className={classes.field}>
                <div className={classesRepeat.container}>
                  <div className={classesRepeat.repeatEvery}>
                    <Field
                      label={
                        type && repeat ? (
                          <span style={{ textTransform: 'capitalize' }}>
                            {wordDeclension(
                              'each',
                              Number(repeat),
                              Cases.NOMINATIVE,
                              wordGender[type]
                            )}
                          </span>
                        ) : (
                          t('Each')
                        )
                      }
                      placeholder=""
                      component={InputField}
                      mode={mode}
                      labelPlacement="start"
                      divider={false}
                      name="every_repeat"
                      type="number"
                      // required
                    />
                  </div>
                  <Typography
                    classes={{
                      root: classesRepeat.text,
                    }}
                  >
                    {name}
                  </Typography>
                </div>
              </div>
              <div className={classes.field}>
                <Field
                  label={t('RepeatUntil')}
                  component={InputField}
                  mode={mode}
                  type="date"
                  divider={false}
                  labelPlacement="start"
                  name="until_repeat"
                />
              </div>
            </div>
          </>
        )}
      </Collapse>
    </div>
  );
};
