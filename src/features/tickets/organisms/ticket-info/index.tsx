import { useState, FC, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { Field, FieldArray } from 'formik';
import format from 'date-fns/format';
import { Collapse, Chip, Typography, Alert } from '@mui/material';
import { useTranslation } from 'react-i18next';

import Paid from '@img/paid.svg';
import Sla from '@img/sla.svg';
import { InputField, SwitchField, Divider } from '@ui/index';
import { ClassSelectField } from '@features/tickets-class-select';
import { TypeSelectField } from '@features/tickets-new-type-select';
import { OriginSelectField } from '@features/tickets-origin-select';
import { PrioritySelectField } from '@features/tickets-priority-select';
import { StatusSelectField, $data as $statuses } from '@features/tickets-status-select';

import {
  $mode,
  $permissions,
  statusColor,
  $formattedTypes,
  $formattedPrepaymentTypes,
  $ticket,
} from '../../models';
import { Ticket, TypesData } from '../../interfaces';
import { useStyles, styles } from '../detail/styles';
import { isDeadlineExpired, getCost, getPaidData } from '../../libs';
import { useStyles as useStylesInfo, styles as stylesInfo } from './styles';
import { SectionHeader } from '../../molecules';

interface Props {
  values: Ticket;
  setFieldValue: (name: string, value: any) => void;
  isNew: boolean;
}

export const TicketInfo: FC<Props> = ({ values, setFieldValue, isNew }) => {
  const { t } = useTranslation();
  const [statuses, mode, permissions, formattedTypes, formattedPrepaymentTypes, ticket] =
    useUnit([
      $statuses,
      $mode,
      $permissions,
      $formattedTypes,
      $formattedPrepaymentTypes,
      $ticket,
    ]);
  const classes = useStyles({ external: false });
  const classesInfo = useStylesInfo();
  const [expanded, setExpanded] = useState(true);
  const [permMessage, setPermMessage] = useState('');

  const status = statuses.find(
    (item: { id: string }) =>
      item.id === (typeof values.status === 'object' ? values.status?.id : values.status)
  );

  const hasDeadlineExpired = isDeadlineExpired(values.sla, values.actual_end_at);

  const plannedDuration = [...values.types].reverse()[0]?.planned_duration_minutes;

  const modeForSla = !isNew ? 'view' : mode;
  const slaMode = plannedDuration ? 'view' : modeForSla;

  const paidData = getPaidData(values.types, formattedTypes);
  const paidDataForView = getPaidData(ticket.types as TypesData[], formattedTypes);

  const showTypesForView =
    mode === 'view' ||
    (values.id && paidDataForView) ||
    (values.id && values.status !== 'new');

  const paymentType = paidData
    ? formattedPrepaymentTypes[paidData.prepayment_type]
    : null;

  const cost = paidData
    ? getCost(paidData.price_type, paidData.price_lower, paidData.price_upper)
    : null;

  const paymentText =
    paymentType && cost ? (
      <div>
        {paymentType}. {cost}
      </div>
    ) : (
      ''
    );

  useEffect(() => {
    if (!values.id) {
      if (plannedDuration) {
        const today = new Date();
        const slaDate = new Date(today.getTime() + plannedDuration * 60000);
        const formattedSlaDate = format(slaDate, "yyyy-MM-dd'T'HH:mm");

        setFieldValue('sla', formattedSlaDate);
      } else {
        setFieldValue('sla', '');
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [plannedDuration]);

  const handleAppealClick = () => {
    if (mode === 'edit' && !permissions.appeal?.is_enabled && permissions?.appeal?.text) {
      setPermMessage(permissions?.appeal?.text);
    } else setPermMessage('');
  };

  const handleEscalationClick = () => {
    if (
      mode === 'edit' &&
      !permissions.escalation?.is_enabled &&
      permissions?.escalation?.text
    ) {
      setPermMessage(permissions?.escalation?.text);
    } else setPermMessage('');
  };

  const handleExpanded = () => setExpanded(!expanded);

  const onChangeFirstLevelType = (value: TypesData) => {
    setFieldValue('types.0', value);
    setFieldValue('types.1', '');
    setFieldValue('types.2', '');
  };

  return (
    <>
      <SectionHeader title={t('Info')} isExpanded={expanded} onClick={handleExpanded}>
        {hasDeadlineExpired && <Sla title={t('SLAExpired')} style={stylesInfo.sla} />}
      </SectionHeader>
      <Collapse in={expanded} timeout="auto">
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              label={t('RequestClass')}
              placeholder={t('SelectRequestClass')}
              component={ClassSelectField}
              name="class"
              mode={!isNew ? 'view' : mode}
              excludeClassId="auto"
              isNew={isNew}
              required
            />
          </div>
          <div className={classes.field}>
            {mode === 'edit' ? (
              <Field
                label={t('Label.status')}
                placeholder={t('selectStatuse')}
                component={StatusSelectField}
                name="status"
                mode={mode}
                required
              />
            ) : (
              <>
                <Typography
                  classes={{
                    root: classesInfo.text,
                  }}
                >
                  {t('Label.status')}
                </Typography>
                <Chip
                  label={status?.title}
                  size="medium"
                  style={{
                    backgroundColor:
                      status?.id && statusColor[status.id]?.backgroundColor,
                    color: status?.id && statusColor[status.id]?.color,
                  }}
                />
                <Divider />
              </>
            )}
          </div>
        </div>
        {showTypesForView ? (
          <div className={classes.field} style={styles.fullWidth}>
            <div className={classesInfo.type}>
              <div>
                <Typography classes={{ root: classesInfo.typeFieldName }}>
                  {t('RequestType')}
                </Typography>
                <div className={classesInfo.typeField}>
                  {paidData && <Paid style={stylesInfo.paid} />}
                  {values.formattedTypes}
                </div>
              </div>
            </div>
            {paymentText && <div className={classesInfo.paymentText}>{paymentText}</div>}
            <Divider />
          </div>
        ) : (
          <>
            <FieldArray
              name="types"
              render={() => (
                <div className={classes.field}>
                  <Field
                    label={t('RequestType')}
                    placeholder={t('SelectRequestType')}
                    component={TypeSelectField}
                    name="types.0"
                    isFirstSelect
                    onChange={onChangeFirstLevelType}
                    mode={mode}
                    withoutArchived={isNew}
                    ticketId={showTypesForView ? values.id : null}
                    divider={
                      paymentText
                        ? values?.types[0]?.children &&
                          values.types[0].children.length > 0
                        : true
                    }
                  />
                  {Array.isArray(values?.types) &&
                    values.types.map((type: TypesData, index: number) => {
                      const formattedValuesTypes = [type].reduce(
                        (acc: { [key: number]: { [key: string]: number[] } }, value) => {
                          if (Boolean(value?.id) && Boolean(value?.title)) {
                            return {
                              ...acc,
                              ...{
                                [value.id]: {
                                  [value.title]: value?.children || [],
                                },
                              },
                            };
                          }

                          return acc;
                        },
                        {}
                      );

                      const childrenTypes =
                        Object.values(formattedValuesTypes)
                          .map((item) => Object.values(item))
                          .flat(2) || [];

                      if (childrenTypes.length === 0) {
                        return null;
                      }

                      const isExist = Boolean(
                        values.types[index + 1] &&
                          values.types[index + 1]?.children.length > 0
                      );

                      const dividerIfExistChildren = isExist ? index + 1 !== 2 : false;
                      const divider = paymentText ? dividerIfExistChildren : true;

                      const onChangeTypes = (value: TypesData) => {
                        setFieldValue(`types.${index + 1}`, value);
                        setFieldValue(`types.${index + 2}`, '');
                      };

                      return (
                        <Field
                          key={type.id}
                          label={null}
                          placeholder={`${t('SelectRequestType')} ${index + 2}`}
                          component={TypeSelectField}
                          name={`types.${index + 1}`}
                          mode={mode}
                          formattedTypes={formattedValuesTypes}
                          isFilterByChildrenTypes
                          childrenTypes={childrenTypes}
                          withoutArchived={isNew}
                          onChange={onChangeTypes}
                          topLevelValue={[type]}
                          divider={divider}
                        />
                      );
                    })}
                </div>
              )}
            />
            {paymentText && (
              <div className={classes.field}>
                <div className={classesInfo.paymentText}>{paymentText}</div>
                <Divider />
              </div>
            )}
          </>
        )}
        <div className={classes.fieldsContainer}>
          {mode === 'edit' && permMessage && <Alert severity="info">{permMessage}</Alert>}
          <div className={classes.twoFields}>
            <div className={classes.field} onClick={handleAppealClick} aria-hidden="true">
              <Field
                component={SwitchField}
                name="is_appeal"
                label={t('Appeal')}
                disabled={
                  values.is_appeal || !permissions.appeal?.is_enabled || mode !== 'edit'
                }
                labelPlacement="end"
              />
            </div>
            {!isNew && (
              <div
                className={classes.field}
                onClick={handleEscalationClick}
                aria-hidden="true"
              >
                <Field
                  component={SwitchField}
                  name="is_escalation_enabled"
                  label={t('Escalation')}
                  disabled={
                    values.is_escalation_enabled ||
                    !permissions.escalation?.is_enabled ||
                    mode !== 'edit'
                  }
                  labelPlacement="end"
                />
              </div>
            )}
          </div>
        </div>
        <div className={classes.field} style={styles.fullWidth}>
          <Field
            label={t('RequestDescription')}
            placeholder={t('EnterTheTextOfTheDescriptionRequest')}
            component={InputField}
            name="description"
            mode={mode}
            rowsMax={10}
            required
            multiline
          />
        </div>
        <div className={classes.field}>
          <Field
            label={t('Source')}
            placeholder={t('SelectTheSourceOfTheRequest')}
            component={OriginSelectField}
            name="origin"
            mode={!isNew ? 'view' : mode}
            isNew={isNew}
            exclude={['eds', 'app_ios', 'app_android']}
            required
          />
        </div>
        <div className={classes.field}>
          <Field
            label={t('ExecutionPriority')}
            placeholder={t('ChooseAPriority')}
            component={PrioritySelectField}
            name="priority"
            mode={mode}
            required
          />
        </div>
        <div className={classes.twoFields}>
          <div className={classes.field}>
            <Field
              label={t('NormativeDeadline')}
              placeholder=""
              component={InputField}
              name="sla"
              type="datetime-local"
              mode={slaMode}
            />
          </div>
          {!isNew && (
            <div className={classes.field}>
              <Field
                label={t('ActualDeadline')}
                placeholder=""
                component={InputField}
                name="actual_end_at"
                type="datetime-local"
                mode="view"
              />
            </div>
          )}
        </div>
        {!isNew && (
          <div className={classes.twoFields}>
            <div className={classes.field}>
              <Field
                label={t('DateAndTimeTheRequestWasCreated')}
                placeholder=""
                component={InputField}
                name="date_create_ticket"
                type="datetime-local"
                mode="view"
              />
            </div>
            <div className={classes.field}>
              <Field
                label={t('RequestNumber')}
                placeholder=""
                component={InputField}
                name="ticket_number"
                mode="view"
              />
            </div>
          </div>
        )}
      </Collapse>
    </>
  );
};
