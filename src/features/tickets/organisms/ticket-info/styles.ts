import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  text: {
    color: '#9494A3',
    fontWeight: 400,
    fontSize: 14,
    marginTop: -5,
    marginBottom: 10,
  },
  paymentText: {
    fontWeight: 400,
    fontSize: 13,
    color: '#65657B',
    marginTop: 5,
  },
  type: {
    display: 'flex',
  },
  typeFieldName: {
    fontStyle: 'normal',
    fontWeight: 400,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  typeField: {
    margin: '10px 0px 5px 0px',
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
  },
}));

export const styles = {
  paid: {
    margin: '-2px 10px 0px 0px',
  },
  sla: {
    marginRight: 10,
  },
};
