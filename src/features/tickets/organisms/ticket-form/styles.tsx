export const styles = {
  sendButton: {
    order: 69,
    margin: '0 5px',
  },
  exportButton: {
    order: 70,
    margin: '0 5px',
  },
  exportOrderButton: {
    order: 75,
    margin: '0 5px',
  },
  cloneButton: {
    order: 80,
    margin: '0 5px',
  },
};
