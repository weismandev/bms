import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Form } from 'formik';

import { DetailToolbar, CustomScrollbar } from '@ui/index';

import {
  changedMode,
  closeDetail,
  cloneTicket,
  exportTicket,
  notifySupport,
  exportOrder,
  $mode,
  $isShowTmc,
} from '../../models';
import { TicketInfo } from '../ticket-info';
import { TicketObjects } from '../ticket-objects';
import { TicketTime } from '../ticket-time';
import { TicketAssigness } from '../ticket-assigness';
import { TicketInitiator } from '../ticket-initiatior';
import { TicketRepeat } from '../ticket-repeat';
import { TicketTmc } from '../ticket-tmc';
import { TicketInvoice } from '../ticket-invoice';
import { SendButton, ExportOrderButton, CloneButton, ExportButton } from '../../atoms';
import { styles as stylesForm } from './styles';
import { useStyles } from '../detail/styles';

interface Props {
  values: any;
  setFieldValue: (name: string, value: any) => void;
  resetForm: () => void;
  errors: any;
  external: boolean;
  isNew: boolean;
}

export const TicketForm: FC<Props> = ({
  values,
  setFieldValue,
  resetForm,
  errors,
  external,
  isNew,
}) => {
  const [mode, isShowTmc] = useUnit([$mode, $isShowTmc]);
  const classes = useStyles({ external });

  const classTicket = typeof values.class === 'object' ? values.class?.id : values.class;
  const isShowInitiator = classTicket === 'client';
  const isShowPlanned = classTicket === 'planned';
  const isShowTicketToolbar = isNew ? false : mode !== 'edit';
  const hasError = Object.values(errors).length > 0;

  const onEdit = () => changedMode('edit');

  const onCancel = () => {
    if (!isNew) {
      resetForm();
      changedMode('view');
    } else {
      closeDetail();
    }
  };

  return (
    <Form className={classes.form}>
      {!external && (
        <DetailToolbar
          className={classes.toolbar}
          mode={mode}
          hidden={{ close: true, delete: true }}
          onEdit={onEdit}
          onCancel={onCancel}
          hasError={hasError}
        >
          {isShowTicketToolbar && (
            <>
              <SendButton style={stylesForm.sendButton} onClick={notifySupport} />
              <ExportButton style={stylesForm.exportButton} onClick={exportTicket} />
              <ExportOrderButton
                onClick={exportOrder}
                style={stylesForm.exportOrderButton}
              />
              <CloneButton onClick={cloneTicket} style={stylesForm.cloneButton} />
            </>
          )}
        </DetailToolbar>
      )}
      <div className={classes.formContent}>
        <CustomScrollbar>
          <div className={classes.content}>
            <TicketInfo values={values} setFieldValue={setFieldValue} isNew={isNew} />
            <TicketObjects values={values} setFieldValue={setFieldValue} isNew={isNew} />
            {isShowInitiator && <TicketInitiator values={values} isNew={isNew} />}
            {isShowPlanned && <TicketRepeat values={values} isNew={isNew} />}
            <TicketTime values={values} setFieldValue={setFieldValue} errors={errors} />
            <TicketInvoice
              values={values}
              setFieldValue={setFieldValue}
              errors={errors}
              isNew={isNew}
            />
            <TicketAssigness />
            {isShowTmc && !isNew && (
              <TicketTmc values={values} errors={errors} setFieldValue={setFieldValue} />
            )}
          </div>
        </CustomScrollbar>
      </div>
    </Form>
  );
};
