import { FC } from 'react';
import { Event } from 'effector';
import { useTranslation } from 'react-i18next';
import { VerticalAlignBottom } from '@mui/icons-material';

import { ActionIconButton } from '@ui/index';

interface Props {
  style: object;
  onClick: Event<void>;
}

export const ExportButton: FC<Props> = ({ style, onClick }) => {
  const { t } = useTranslation();

  const exportRequest = t('ExportRequest');

  return (
    <ActionIconButton style={style} onClick={onClick}>
      <VerticalAlignBottom titleAccess={exportRequest} />
    </ActionIconButton>
  );
};
