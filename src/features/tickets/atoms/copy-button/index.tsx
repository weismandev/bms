import { FC } from 'react';
import { IconButton } from '@mui/material';
import { ContentCopyOutlined } from '@mui/icons-material';

interface Props {
  iconStyle?: object;
  style?: object;
  onClick: () => void;
  disabled?: boolean;
}

export const CopyButton: FC<Props> = ({ iconStyle, style, onClick, disabled }) => (
  <IconButton style={style} onClick={onClick} disabled={disabled}>
    <ContentCopyOutlined style={iconStyle} />
  </IconButton>
);
