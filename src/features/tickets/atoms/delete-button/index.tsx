import { FC } from 'react';
import { IconButton } from '@mui/material';
import { DeleteOutlined } from '@mui/icons-material';

interface Props {
  iconStyle?: object;
  style?: object;
  onClick: () => void;
  disabled: boolean;
}

export const DeleteButton: FC<Props> = ({ iconStyle, style, onClick, disabled }) => (
  <IconButton style={style} onClick={onClick} disabled={disabled}>
    <DeleteOutlined style={iconStyle} />
  </IconButton>
);
