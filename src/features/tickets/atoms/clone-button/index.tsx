import { FC } from 'react';
import { Event } from 'effector';
import { useTranslation } from 'react-i18next';
import { FileCopyOutlined } from '@mui/icons-material';

import { ActionIconButton } from '@ui/index';

interface Props {
  style: object;
  onClick: Event<void>;
}

export const CloneButton: FC<Props> = ({ style, onClick }) => {
  const { t } = useTranslation();

  const cloneTicket = t('CloneTicket');

  return (
    <ActionIconButton style={style} onClick={onClick}>
      <FileCopyOutlined titleAccess={cloneTicket} />
    </ActionIconButton>
  );
};
