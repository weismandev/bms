import { FC } from 'react';
import { Event } from 'effector';
import { useTranslation } from 'react-i18next';
import { MailOutline } from '@mui/icons-material';

import { ActionIconButton } from '@ui/index';

interface Props {
  style: object;
  onClick: Event<void>;
}

export const SendButton: FC<Props> = ({ style, onClick }) => {
  const { t } = useTranslation();

  const sendToSupport = t('SendToSupport');

  return (
    <ActionIconButton style={style} onClick={onClick}>
      <MailOutline titleAccess={sendToSupport} />
    </ActionIconButton>
  );
};
