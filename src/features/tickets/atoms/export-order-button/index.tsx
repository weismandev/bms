import { FC } from 'react';
import { Event } from 'effector';
import { useTranslation } from 'react-i18next';
import { VerticalAlignBottom } from '@mui/icons-material';

import { ActionButton } from '@ui/index';

interface Props {
  style: object;
  onClick: Event<void>;
}

export const ExportOrderButton: FC<Props> = ({ style, onClick }) => {
  const { t } = useTranslation();

  return (
    <ActionButton style={style} onClick={onClick}>
      <VerticalAlignBottom />
      {t('OrderOutfit')}
    </ActionButton>
  );
};
