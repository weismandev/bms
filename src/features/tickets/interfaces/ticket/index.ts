import { StatusData } from '@features/tickets-status-select';
import { Equipment } from '@features/equipment-select';
import { OriginItem } from '@features/tickets-origin-select';

import { Repeat } from '../repeat';
import { Users } from '../users';
import { AssigneesItem } from '../assignees';
import { TypesData } from '../types';
import { Objects } from '../objects';
import { Initiator } from '../initiator';
import { Materials } from '../tmc';

export interface TicketPayload {
  ticket: {
    id: number;
  };
}

export interface Value {
  id: number;
  title?: string;
}

export interface Ticket {
  id: null | number;
  description: string;
  class: string | { id: string; title: string };
  status: string | StatusData;
  types: [] | TypesData[];
  formattedTypes: string;
  priority: string | { id: string; title: string };
  assignees: string | Users[] | AssigneesItem[];
  date_create_ticket?: string;
  planned_start_at: Date | string;
  planned_end_at: Date | string;
  object: string | { id: number; title?: string };
  house: string | Value;
  apartment: string | Value;
  origin: string | OriginItem;
  ticket_number?: string;
  sla: Date | string;
  is_appeal: boolean;
  is_escalation_enabled: boolean;
  property: string | { id: number; title?: string };
  planned_duration?: null | number;
  initiator: string | Initiator;
  rate?: number | string;
  comment?: string;
  until_repeat: string;
  type_repeat: string | Repeat;
  every_repeat: string;
  actual_end_at: string;
  devices: Equipment[];
  is_repeat_ticket?: boolean;
  channel: number | string;
  tmc?: {
    tmcTitle?: string;
    isActCreated?: boolean;
    expenses?: {
      workTitle: string;
      workTime: string;
      materials: Materials[];
    }[];
  }[];
  work: { id: number; title: string }[];
  enterprise: string | Value | number;
  related_userdata_token?: string;
  invoices: [];
}

export interface CreateTicketPayload {
  id: null | number;
  description: string;
  origin: string;
  status: string;
  sla: string;
  class: string;
  types: number[];
  priority: string;
  assignees: number[];
  planned_start_at: string;
  planned_end_at: string;
  objects: Objects[];
  is_appeal: number;
  initiator: null | {
    id: number;
  };
  repeat?: {
    until?: string;
    type?: string;
    every?: string;
  };
  enterprise_id?: number;
}

export interface TicketResponse {
  ticket: {
    id: number;
    title: string;
    description: string;
    priority: string;
    sla: string;
    status: string;
    class: string;
    planned_start_at: string;
    planned_end_at: string;
    created_at: string;
    assignees: { id: number }[];
    types: any;
    objects: { id: number; type: string }[];
    origin: string;
    number: string;
    is_appeal: boolean;
    is_escalation_enabled: boolean;
    initiator: string | Initiator;
    actual_end_at: null | string;
    devices: any;
    channel: { id: number };
    enterprise_id: number;
  };
}

export interface Permission {
  is_enabled: boolean;
  text: string;
}

export interface Permissions {
  appeal?: Permission;
  escalation?: Permission;
}
