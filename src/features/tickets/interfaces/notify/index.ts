export interface NotifySupportPayload {
  ticket: {
    id: number | null;
  };
}
