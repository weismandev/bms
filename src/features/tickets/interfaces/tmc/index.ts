export interface TicketTmcResponse {
  ticket_data: {
    object_title: string;
    expenses: ExpensesTmc[];
    is_act_created: boolean;
  }[];
}

export interface ExpensesTmc {
  work_type_id: number;
  work_type_title: string;
  work_time: number;
  work_type_group_id: string;
  work_type_group_title: string;
  material_expenses: {
    material: string;
    unit: string;
    quantity: number;
  }[];
}

export interface Tmc {
  tmcTitle: string;
  isActCreated: boolean;
  expenses: {
    workTitle: string;
    workTime: string;
    materials: Materials[];
  }[];
}

export interface MaterialTmc {
  material: string;
  quantity: number;
  unit: string;
}

export interface ExpansesMaterial {
  workId?: number;
  workTitle: string;
  workTime: string;
  workTypeGroupId: string | null;
  workTypeGroupTitle: string | null;
  materials: Materials[];
}

export interface Materials {
  material: string;
  materialCount: number;
  materialUnit: string;
}

export interface SetTmcPayload {
  ticket_id: number | null;
  expenses: ExpensesTmc;
}
