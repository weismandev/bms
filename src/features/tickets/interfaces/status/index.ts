export interface ChangeStatusPayload {
  ticket: {
    id: number | null;
  };
  status: {
    name: string;
  };
}
