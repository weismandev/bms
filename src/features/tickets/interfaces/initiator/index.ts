export interface Initiator {
  id: number;
  title: string;
  email: string;
  phone: string;
}
