export interface SetPriorityPayload {
  ticket: {
    id: number | null;
  };
  priority: {
    name: string;
  };
}
