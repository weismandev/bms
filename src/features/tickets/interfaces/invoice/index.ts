import { Ticket } from '../ticket';

export interface UploadAttachmentPayload {
  type: string;
  file: File;
  index: number;
}

export interface UploadAttachmentResponse {
  id: number;
}

export interface UploadAttachment {
  file: File;
  index: number;
}

export interface Attachment {
  id: number;
  name: string;
  index: number;
}

export interface CreateInvoicePayload {
  ticket_id: number | null;
  invoices: {
    file_id: number;
    total: number;
  }[];
}

export interface GetInvoicesPayload {
  ticket: {
    id: number | null;
  };
}

export interface InvoiceResponse {
  id: number;
  file: string;
  total: number;
  status: string;
  name: string;
}

export interface GetInvoicesResponse {
  invoices: InvoiceResponse[];
}

export interface Invoice {
  id: number;
  file: FileInvoice;
  status: string;
  amount: number;
}

export interface FileInvoice {
  url: string;
  name: string;
}

export interface UpdateInvoicePayload {
  invoices: {
    id: number;
    total: number;
    status: string;
  }[];
}

export interface DeleteInvoicePayload {
  ids: number[];
}

export interface SubmitInvoice {
  newValues: Ticket;
  currentValues: Ticket;
}
