export interface GetTicketWorksPayload {
  ticket_id: number | null;
}

export interface GetTicketWorksResponse {
  ticket_devices: TicketWork[];
}

export interface TicketWork {
  id: number;
  serialnumber: string;
  title: string;
  tech_cards: WorkValue[];
  works: WorkValue[];
}

export interface WorkValue {
  id: number;
  title: string;
}

export interface Device {
  id: string;
  title: string;
}

export interface Works {
  _id: string;
  id: null | number;
  device: Device | string;
  techCards: WorkValue[];
  works: WorkValue[];
}

export interface SetTicketWorkPayload {
  ticket_id: number;
  devices: DevicePayload[];
}

export interface DevicePayload {
  id: number | null;
  serialnumber?: string | null;
  tech_card_ids?: number[];
  work_ids?: number[];
}
