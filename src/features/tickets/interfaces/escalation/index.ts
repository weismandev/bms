export interface SetEscalationPayload {
  ticket: {
    id: number | null;
  };
  escalation: {
    enabled: number;
  };
}
