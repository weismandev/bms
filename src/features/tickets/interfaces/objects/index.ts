export interface Objects {
  id: number;
  type: string;
}

export interface ObjectsPayload {
  objects: Objects[];
}

export interface ObjectParts {
  id: number;
  title: string;
  type: string;
}

export interface ObjectsResponse {
  object: Objects;
  parts: ObjectParts[];
}
