export interface GetDeviceNamePayload {
  serialnumber: number;
}

export interface GetDeviceNameResponse {
  meta: {
    serialnumber: number;
    name: string;
  };
}
