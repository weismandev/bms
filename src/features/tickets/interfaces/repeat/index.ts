export interface Repeat {
  id: string;
  title: string;
}

export interface SetRepeatPayload {
  ticket: {
    id: number | null;
  };
  until?: string;
  type: string;
  every: string;
}

export interface RepeatResponse {
  config: {
    type: string;
    every: string;
    until: string;
  };
}

export interface GetRepeatPayload {
  ticket: {
    id: number | null;
  };
}

export interface RepeatStore {
  type_repeat: string;
  every_repeat: string;
  until_repeat: string;
}
