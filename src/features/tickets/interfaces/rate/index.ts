export interface ListRatePayload {
  ticket: {
    id: number | null;
  };
}

export interface Rate {
  rate: number;
  comment: string;
  user: {
    id: number;
  };
}

export interface ListRateResponse {
  rates: Rate[];
}

export interface RateStore {
  [key: number]: {
    rate: number;
    comment: string;
  };
}

export interface RateItem {
  rate: number;
  comment: string;
}
