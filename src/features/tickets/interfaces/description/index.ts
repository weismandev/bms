export interface SetDescriptionPayload {
  ticket: {
    id: number | null;
  };
  description: string | any;
}
