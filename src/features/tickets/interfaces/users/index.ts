export interface Users {
  user: {
    id: number;
    fullname?: string;
  };
}

export interface UsersPayload {
  users: number[];
}

export interface UserItem {
  id: number;
  surname: string;
  name: string;
  patronymic: string;
}

export interface UsersResponse {
  users: UserItem[];
}
