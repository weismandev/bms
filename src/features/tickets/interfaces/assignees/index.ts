export interface AssigneesItem {
  id: number;
  fullname: string;
  is_archived?: boolean;
}

export interface UpdateAssignessTicketPayload {
  ticket: {
    id: number | null;
  };
  users: number[];
}
