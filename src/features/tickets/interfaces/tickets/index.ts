import { Initiator } from '../initiator';
import { RateItem } from '../rate';

export interface TicketsResponse {
  total?: number;
  tickets: TicketsResponseItem[];
}

export interface TicketsResponseItem {
  id: number;
  title: string;
  description: string;
  priority: string;
  status: string;
  class: string;
  sla: string;
  planned_start_at: string;
  planned_end_at: string;
  created_at: string;
  assignees: { id: number }[];
  types: any;
  objects: { id: number; type: string }[];
  origin: string;
  number: string;
  is_appeal: boolean;
  initiator: string | Initiator;
  actual_end_at: null | string;
  devices: { serialnumber: number }[];
  rate: null | RateItem;
  channel: { id: number; unread_messages: number };
}

export interface TicketsPayload {
  page?: number;
  per_page?: number;
  text?: string;
  created_at_from?: string;
  created_at_to?: string;
  statuses?: string[];
  priorities?: string[];
  classes?: string[];
  types?: number[];
  paid?: boolean;
  invoice_status?: string;
  objects?: { id: number; type: string }[];
  stuff_write_off_needed?: number;
  assignees?: number[];
  rates?: string[];
  expired?: number;
  initiators?: number[];
  has_unread_messages?: number;
}
