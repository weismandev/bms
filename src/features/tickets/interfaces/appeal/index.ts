export interface ChangeAppealPayload {
  ticket: {
    id: number | null;
    is_appeal: number;
  };
}
