export interface SetPlannedDatePayload {
  ticket: {
    id: null | number;
  };
  planned_start_at: string;
  planned_end_at: string;
}
