import { api } from '@api/api2';

import { ListRatePayload } from '../../interfaces';

const getTicketRates = (payload: ListRatePayload) =>
  api.v1('post', 'tck/bms/tickets/item/list-rates', payload);

export const ticketDeclarantApi = {
  getTicketRates,
};
