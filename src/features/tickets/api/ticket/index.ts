import { api } from '@api/api2';

import {
  TicketPayload,
  CreateTicketPayload,
  NotifySupportPayload,
} from '../../interfaces';

const getTicketById = (payload: TicketPayload) =>
  api.v1('post', 'tck/bms/tickets/item', payload);
const createTicket = (payload: CreateTicketPayload) =>
  api.v1('post', 'tck/bms/tickets/create', payload);
const notifySupport = (payload: NotifySupportPayload) =>
  api.v1('post', 'tck/bms/tickets/item/notify-support', payload);

export const ticketCardApi = {
  getTicketById,
  createTicket,
  notifySupport,
};
