import { api } from '@api/api2';

import {
  SetPriorityPayload,
  ChangeStatusPayload,
  ChangeAppealPayload,
  SetEscalationPayload,
  SetDescriptionPayload,
  SetTicketTypePayload,
} from '../../interfaces';

const setTicketPriority = (payload: SetPriorityPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-priority', payload);
const changeTicketStatus = (payload: ChangeStatusPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-status', payload);
const changeAppeal = (payload: ChangeAppealPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-appeal', payload);
const setEscalation = (payload: SetEscalationPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-escalation', payload);
const setDescription = (payload: SetDescriptionPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-description', payload);
const checkEscalation = (payload: any) =>
  api.v1('get', 'tck/bms/check/escalation', payload);
const checkAppeal = (payload: any) => api.v1('get', 'tck/bms/check/appeal', payload);
const updateTicketTypes = (payload: SetTicketTypePayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-types', payload);

export const ticketGeneralApi = {
  setTicketPriority,
  changeTicketStatus,
  changeAppeal,
  setEscalation,
  setDescription,
  checkEscalation,
  checkAppeal,
  updateTicketTypes,
};
