import { api } from '@api/api2';

import { SetRepeatPayload, GetRepeatPayload } from '../../interfaces';

const setRepeat = (payload: SetRepeatPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-repeat', payload);
const getRepeat = (payload: GetRepeatPayload) =>
  api.v1('post', 'tck/bms/tickets/item/get-repeat', payload);

export const ticketRepeatApi = {
  setRepeat,
  getRepeat,
};
