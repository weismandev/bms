import { api } from '@api/api2';

import { UpdateAssignessTicketPayload } from '../../interfaces';

const changeAssignessTicket = (payload: UpdateAssignessTicketPayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-assignees', payload);

export const ticketAssigneesApi = {
  changeAssignessTicket,
};
