import { api } from '@api/api2';

import { SetPlannedDatePayload } from '../../interfaces';

const setPlannedDate = (payload: SetPlannedDatePayload) =>
  api.v1('post', 'tck/bms/tickets/item/set-planned-date', payload);

export const ticketTimeApi = {
  setPlannedDate,
};
