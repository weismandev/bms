import { api } from '@api/api2';
import { UsersPayload, TicketsPayload, GetDeviceNamePayload } from '../../interfaces';

const getUsersInfo = (payload: UsersPayload) =>
  api.v1('post', 'tck/bms/users/list', payload);
const getTickets = (payload: TicketsPayload) =>
  api.v1('post', 'tck/bms/tickets/list', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const getDeviceName = (payload: GetDeviceNamePayload) =>
  api.no_vers('get', 'bmsdevice/get-device-info', payload);
const exportTickets = (payload: TicketsPayload) =>
  api.v1(
    'request',
    'tck/bms/tickets/list/export',
    payload,
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'post',
    }
  );
const getTypesList = () => api.v4('get', 'ticket/types/list', { archived: 0 });

export const ticketsApi = {
  getUsersInfo,
  getTickets,
  getDeviceName,
  exportTickets,
  getTypesList,
};
