import { api } from '@api/api2';

import {
  UploadAttachmentPayload,
  CreateInvoicePayload,
  GetInvoicesPayload,
  UpdateInvoicePayload,
  DeleteInvoicePayload,
} from '../../interfaces';

// eslint-disable-next-line prefer-destructuring
const IM_URL = process.env.IM_URL;

const uploadFile = (payload: UploadAttachmentPayload) =>
  api.no_vers('post', `${IM_URL}/uploadFile`, payload);
const createInvoice = (payload: CreateInvoicePayload) =>
  api.v1('post', 'tck/bms/invoices/create-batch', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const getInvoices = (payload: GetInvoicesPayload) =>
  api.v1('post', 'tck/bms/tickets/item/list-invoices', payload);
const updateInvoice = (payload: UpdateInvoicePayload) =>
  api.v1('post', 'tck/bms/invoices/update-batch', payload);
const deleteInvoice = (payload: DeleteInvoicePayload) =>
  api.v1('post', 'tck/bms/invoices/delete-batch', payload);

export const ticketInvoiceApi = {
  uploadFile,
  createInvoice,
  getInvoices,
  updateInvoice,
  deleteInvoice,
};
