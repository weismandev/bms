import { api } from '@api/api2';

import { SetTmcPayload } from '../../interfaces';

const getTicketTmc = (id: number | null) =>
  api.v4('get', `integration/toir/ticket-specific-data/${id}`);
const setTmc = (payload: SetTmcPayload) =>
  api.v4('post', 'integration/toir/create-act', payload);

export const ticketTmcApi = {
  getTicketTmc,
  setTmc,
};
