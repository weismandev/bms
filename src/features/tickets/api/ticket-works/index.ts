import { api } from '@api/api2';

import { GetTicketWorksPayload, SetTicketWorkPayload } from '../../interfaces';

const getWorks = (payload: GetTicketWorksPayload) =>
  api.v4('get', 'ticket/devices/list', payload);
const setWorks = (payload: SetTicketWorkPayload) =>
  api.v4('post', 'ticket/devices/store', payload);

export const ticketWorksApi = {
  getWorks,
  setWorks,
};
