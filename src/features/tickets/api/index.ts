import { api } from '@api/api2';

import { ticketGeneralApi } from './ticket-general';
import { ticketsApi } from './tickets';
import { ticketCardApi } from './ticket';
import { ticketInvoiceApi } from './ticket-invoice';
import { ticketAssigneesApi } from './ticket-assignees';
import { ticketRepeatApi } from './ticket-repeat';
import { ticketWorksApi } from './ticket-works';
import { ticketTmcApi } from './ticket-tmc';
import { ticketDeclarantApi } from './ticket-declarant';
import { ticketTimeApi } from './ticket-time';
import { ObjectsPayload } from '../interfaces';

const getObjects = (payload: ObjectsPayload) =>
  api.v1('post', 'tck/objects/detailed', payload);

export const ticketApi = {
  getObjects,
  ...ticketGeneralApi,
  ...ticketsApi,
  ...ticketCardApi,
  ...ticketInvoiceApi,
  ...ticketAssigneesApi,
  ...ticketRepeatApi,
  ...ticketWorksApi,
  ...ticketTmcApi,
  ...ticketDeclarantApi,
  ...ticketTimeApi,
};
