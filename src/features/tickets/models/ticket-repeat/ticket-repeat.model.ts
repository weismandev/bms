import { createEffect, createEvent, createStore } from 'effector';

import {
  GetRepeatPayload,
  RepeatResponse,
  RepeatStore,
  SetRepeatPayload,
} from '../../interfaces';

export const $repeat = createStore<RepeatStore | object>({});

export const getRepeat = createEvent<number>();

export const fxSetRepeat = createEffect<SetRepeatPayload, RepeatResponse, Error>();
export const fxGetRepeat = createEffect<GetRepeatPayload, RepeatResponse, Error>();
