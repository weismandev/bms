import { sample } from 'effector';
import { format } from 'date-fns';

import { signout } from '@features/common';

import { data as repeatType } from '../repeat-select';
import { pageUnmounted } from '../ticket/ticket.model';
import { fxSetRepeat, getRepeat, fxGetRepeat, $repeat } from './ticket-repeat.model';

sample({
  clock: getRepeat,
  fn: (id) => ({ ticket: { id } }),
  target: fxGetRepeat,
});

$repeat
  .on(fxGetRepeat.done, (_, { result }) => ({
    type_repeat: result?.config?.type
      ? repeatType.find((item: { id: string }) => item.id === result.config.type)
      : '',
    every_repeat: result?.config?.every || '',
    until_repeat: result?.config?.until
      ? format(new Date(result.config.until), 'yyyy-MM-dd')
      : '',
  }))
  .reset([pageUnmounted, signout]);

sample({
  clock: fxSetRepeat.done,
  fn: ({ params: { ticket } }) => ({ ticket }),
  target: fxGetRepeat,
});
