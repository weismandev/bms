import { Socket } from 'socket.io-client';
import { createEffect, createEvent, attach, restore } from 'effector';

const notificationSocketUrl: string = process.env.NOTIFICATION_URL!;

export const connected = createEvent<void>();
export const disconnected = createEvent<void>();
export const reconnected = createEvent<void>();

export const fxOpenSocket = createEffect<string, Socket, Error>();
export const fxOpenTicketSocket = attach({
  effect: fxOpenSocket,
  mapParams: () => notificationSocketUrl,
});

export const $socket = restore(fxOpenTicketSocket.doneData, null);
