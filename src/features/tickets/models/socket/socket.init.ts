import io from 'socket.io-client';
import { sample } from 'effector';

import { $token, $accounts, signout, loggedIn } from '@features/common';

import { pageMounted, pageUnmounted, changedEvent } from '../page/page.model';
import {
  connected,
  disconnected,
  reconnected,
  fxOpenSocket,
  $socket,
  fxOpenTicketSocket,
} from './socket.model';

fxOpenSocket.use((socketUrl) => {
  if (!socketUrl) {
    return null;
  }

  const socket = io(socketUrl, {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: Number.POSITIVE_INFINITY,
  });

  socket.on('connect', connected);
  socket.on('disconnect', disconnected);
  socket.on('reconnect', reconnected);

  return socket;
});

sample({
  source: fxOpenSocket.doneData,
  clock: [fxOpenSocket, signout, pageUnmounted],
}).watch((socket) => {
  const token = $token.getState();
  const bmsId = $accounts.getState()[0]?.id;

  if (socket && bmsId) {
    const channel = `bms-${bmsId}`;

    socket.removeAllListeners();
    socket.close();
    socket.emit('unsubscribe', { channels: [channel], payload: { token } });
  }
});

sample({
  clock: [pageMounted, loggedIn],
  source: $socket,
  filter: (socket) => !socket,
  target: fxOpenTicketSocket,
});

$socket.watch((socket) => {
  const bmsId = $accounts.getState()[0]?.id;
  const token = $token.getState();

  if (socket && bmsId) {
    const channel = `bms-${bmsId}`;

    socket.emit(
      'subscribe',
      { channels: [channel], payload: { token } },
      (data: { error: string }) => {
        if (data.error) {
          console.error('Notification subscribe failed', data.error);
        } else {
          socket.on(channel, changedEvent);
        }
      }
    );
  }
});

$socket.reset([pageUnmounted, signout]);
