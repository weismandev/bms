export const data = [
  {
    id: 'day',
    title: 'Повторять по дням',
  },
  {
    id: 'week',
    title: 'Повторять по неделям',
  },
  {
    id: 'month',
    title: 'Повторять по месяцам',
  },
  {
    id: 'year',
    title: 'Повторять по годам',
  },
];
