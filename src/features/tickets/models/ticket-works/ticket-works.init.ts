import { sample } from 'effector';
import { nanoid } from 'nanoid';
import { signout } from '@features/common';
import { DevicePayload, Device } from '../../interfaces';
import { $currentTab, changeTab } from '../detail/detail.model';
import { $ticket, pageUnmounted } from '../ticket/ticket.model';
import {
  fxGetTicketWorks,
  $works,
  $modeWorks,
  changeWorksMode,
  setTicketWorks,
  fxSetTicketWorks,
} from './ticket-works.model';

sample({
  source: { tab: $currentTab, ticket: $ticket },
  filter: ({ tab }) => tab === 'works',
  fn: ({ ticket }) => ({ ticket_id: ticket.id }),
  target: fxGetTicketWorks,
});

$works
  .on(fxGetTicketWorks.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_devices)) {
      return [];
    }

    return result.ticket_devices.map((item) => ({
      _id: nanoid(3),
      id: item.id,
      device:
        item?.serialnumber && item?.title
          ? { id: item.serialnumber.toString(), title: item.title }
          : '',
      techCards:
        Array.isArray(item?.tech_cards) && item.tech_cards.length > 0
          ? item.tech_cards.map((card) => ({ id: card.id, title: card.title }))
          : [],
      works:
        Array.isArray(item?.works) && item.works.length > 0
          ? item.works.map((work) => ({ id: work.id, title: work.title }))
          : [],
    }));
  })
  .reset([pageUnmounted, signout, fxGetTicketWorks]);

$modeWorks
  .on(changeWorksMode, (_, result) => result)
  .reset([pageUnmounted, signout, changeTab, fxSetTicketWorks.done]);

sample({
  clock: setTicketWorks,
  source: $ticket,
  fn: (ticket, { works }) => ({
    ticket_id: ticket.id as number,
    devices: works.map((work) => {
      const payload: DevicePayload = {
        id: work.id,
      };

      if ((work.device as Device)?.id) {
        payload.serialnumber = (work.device as Device).id;
      } else {
        payload.serialnumber = null;
      }

      if (work.techCards.length > 0) {
        payload.tech_card_ids = work.techCards.map((card) => card.id);
      }

      if (work.works.length > 0) {
        payload.work_ids = work.works.map((i) => i.id);
      }

      return payload;
    }),
  }),
  target: fxSetTicketWorks,
});

sample({
  clock: fxSetTicketWorks.done,
  source: $ticket,
  fn: ({ id }) => ({ ticket_id: id }),
  target: fxGetTicketWorks,
});
