import { createEffect, createEvent, createStore } from 'effector';

import {
  GetTicketWorksPayload,
  GetTicketWorksResponse,
  Works,
  SetTicketWorkPayload,
} from '../../interfaces';

export const $works = createStore<Works[]>([]);
export const $modeWorks = createStore<string>('view');

export const changeWorksMode = createEvent<string>();
export const setTicketWorks = createEvent<{ works: Works[] }>();

export const fxGetTicketWorks = createEffect<
  GetTicketWorksPayload,
  GetTicketWorksResponse,
  Error
>();
export const fxSetTicketWorks = createEffect<SetTicketWorkPayload, object, Error>();
