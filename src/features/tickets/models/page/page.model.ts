import { createStore, createEvent, createEffect, combine } from 'effector';
import { createGate } from 'effector-react';

import { $data as $classTicket } from '@features/tickets-class-select';
import { $data as $status } from '@features/tickets-status-select';
import { $data as $priority } from '@features/tickets-priority-select';
import { $data as $invoiceStatuses } from '@features/tickets-invoices-status-select';

import {
  TicketsResponse,
  TicketsPayload,
  Users,
  UsersPayload,
  UsersResponse,
  GetDeviceNamePayload,
  GetDeviceNameResponse,
  Objects,
  ObjectsPayload,
  ObjectsResponse,
  GetInvoicesPayload,
  GetInvoicesResponse,
  InvoiceResponse,
  TypesResponse,
  TypesData,
} from '../../interfaces';

export const path = '/tickets';

export const statusColor: {
  [key: string]: {
    backgroundColor: string;
    color: string;
  };
} = {
  new: {
    backgroundColor: 'rgb(27, 177, 105)',
    color: 'rgb(255, 255, 255)',
  },
  work: {
    backgroundColor: 'rgb(28, 144, 251)',
    color: 'rgb(255, 255, 255)',
  },
  done: {
    backgroundColor: 'rgb(160, 167, 189)',
    color: 'rgb(255, 255, 255)',
  },
  closed: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.87)',
  },
  assigned: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  canceled: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  paused: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  confirmation: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  returned: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  denied: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  incorrect: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.87)',
  },
};

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const open = createEvent<number>();
export const addClicked = createEvent<void>();
export const closeDetail = createEvent<void>();
export const getUsersData = createEvent<{ id: number }[]>();
export const getDeviceName = createEvent<GetDeviceNamePayload>();

export const updateTicketList = createEvent<void>();
export const getObjects = createEvent<Objects[]>();
export const changedEvent = createEvent<{ event: string }>();
export const getTicketInvoices = createEvent<number | null>();

export const fxGetTicketInvoices = createEffect<
  GetInvoicesPayload,
  GetInvoicesResponse,
  Error
>();
export const fxGetTicketList = createEffect<TicketsPayload, TicketsResponse, Error>();
export const fxGetUpdatedTicketList = createEffect<
  TicketsPayload,
  TicketsResponse,
  Error
>();
export const fxGetUsersData = createEffect<UsersPayload, UsersResponse, Error>();
export const fxGetDeviceName = createEffect<
  GetDeviceNamePayload,
  GetDeviceNameResponse,
  Error
>();

export const fxGetObjects = createEffect<ObjectsPayload, ObjectsResponse[], Error>();
export const fxGetTypes = createEffect<void, TypesResponse, Error>();

export const $isLoadingList = createStore<boolean>(false);
export const $types = createStore<TypesData[]>([]);
export const $invoices = createStore<{ [key: number]: InvoiceResponse[] }>({});
export const $objects = createStore<ObjectsResponse[]>([]);
export const $rawData = createStore<TicketsResponse>({
  tickets: [],
  total: 0,
});
export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isDetailOpen = createStore<boolean>(false);
export const $usersData = createStore<string | Users[]>('');
export const $equipment = createStore<{ [key: number]: string } | {}>({});
export const $rowCount = createStore<number>(0);
export const $tickets = combine(
  $rawData,
  $status,
  $priority,
  $classTicket,
  $types,
  $equipment,
  $usersData,
  $objects,
  $invoices,
  $invoiceStatuses,
  (
    rawData,
    status,
    priority,
    classTicket,
    type,
    equipment,
    usersData,
    objects,
    invoices,
    invoiceStatuses
  ) => ({
    tickets: [...rawData.tickets],
    status,
    priority,
    classTicket,
    type,
    equipment,
    usersData,
    objects,
    invoices,
    invoiceStatuses,
  })
);
