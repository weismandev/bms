import { merge, sample } from 'effector';
import { pending } from 'patronum/pending';

import i18n from '@shared/config/i18n';
import { signout, history } from '@features/common';
import { fxGetList as fxGetStatus } from '@features/tickets-status-select';
import { fxGetList as fxGetPriority } from '@features/tickets-priority-select';
import { fxGetList as fxGetClass } from '@features/tickets-class-select';
import { fxGetList as fxGetPrepaymentType } from '@features/tickets-prepayment-type-select';
import { fxGetList as fxGetInvoiceStatus } from '@features/tickets-invoices-status-select';

import { ticketApi } from '../../api';
import { fxCreateTicket, cloneTicket } from '../ticket/ticket.model';
import { $tableParams } from '../table/table.model';
import { $filters } from '../filters/filters.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  UserItem,
  Value,
  TicketsResponseItem,
  Objects,
} from '../../interfaces';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  pageMounted,
  pageUnmounted,
  fxGetTicketList,
  $rawData,
  open,
  $isDetailOpen,
  addClicked,
  closeDetail,
  path,
  $usersData,
  getUsersData,
  fxGetUsersData,
  getDeviceName,
  fxGetDeviceName,
  $equipment,
  updateTicketList,
  fxGetUpdatedTicketList,
  $rowCount,
  fxGetObjects,
  $objects,
  getObjects,
  changedEvent,
  getTicketInvoices,
  fxGetTicketInvoices,
  $invoices,
  fxGetTypes,
  $types,
  $isLoadingList,
} from './page.model';
import { formatPayload } from './page.prepend';

import { fxExportTickets } from '../ticket-export';

const { t } = i18n;

const getDevices = (
  tickets: TicketsResponseItem[],
  equipment: { [key: number]: string }
) => {
  const filtredDevices = tickets?.reduce((acc: number[], value) => {
    if (value.devices.length > 0) {
      value.devices.map((device) => {
        if (acc.includes(device.serialnumber)) {
          return acc;
        }

        return acc.push(device.serialnumber);
      });
    }

    return acc;
  }, []);

  if (Object.keys(equipment).length > 0) {
    filtredDevices.forEach((device) => {
      if (!equipment[device]) {
        getDeviceName({ serialnumber: device });
      }
    });
  }

  if (Object.keys(equipment).length === 0) {
    filtredDevices.map((device) => getDeviceName({ serialnumber: device }));
  }
};

fxGetTicketList.use(ticketApi.getTickets);
fxGetUsersData.use(ticketApi.getUsersInfo);
fxGetDeviceName.use(ticketApi.getDeviceName);
fxExportTickets.use(ticketApi.exportTickets);
fxGetUpdatedTicketList.use(ticketApi.getTickets);
fxGetObjects.use(ticketApi.getObjects);
fxGetTicketInvoices.use(ticketApi.getInvoices);
fxGetTypes.use(ticketApi.getTypesList);

const errorOccured = merge([
  fxGetTicketList.fail,
  fxGetStatus.fail,
  fxGetPriority.fail,
  fxGetClass.fail,
  fxCreateTicket.fail,
  fxGetUsersData.fail,
  fxGetDeviceName.fail,
  fxExportTickets.fail,
  fxGetUpdatedTicketList.fail,
  fxGetObjects.fail,
  fxGetTicketInvoices.fail,
  fxGetTypes.fail,
]);

$isLoadingList
  .on(fxGetTicketList.pending, (_, loadingList) => loadingList)
  .reset([pageUnmounted, signout]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetStatus,
        fxGetPriority,
        fxGetClass,
        fxCreateTicket,
        fxGetTypes,
      ],
    }),
    (_, status) => status
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$types
  .on(fxGetTypes.done, (_, { result }) => {
    if (!Array.isArray(result?.types)) {
      return [];
    }

    return result.types;
  })
  .reset([pageUnmounted, signout]);

$usersData
  .on(fxGetUsersData.done, (_, { result }) => {
    if (!Array.isArray(result?.users)) {
      return '';
    }

    return result.users.map((item: UserItem) => ({
      user: {
        id: item.id,
        fullname: `${item.surname} ${item.name} ${item.patronymic}`,
      },
    }));
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: fxGetTicketList.done,
  source: $equipment,
  fn: (equipment, { result }) => getDevices(result?.tickets, equipment),
});

sample({
  clock: fxGetUpdatedTicketList.done,
  source: $equipment,
  fn: (equipment, { result }) => getDevices(result?.tickets, equipment),
});

$rawData
  .on(fxGetTicketList.done, (_, { result }) => result)
  .on(fxGetUpdatedTicketList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

sample({
  clock: getUsersData,
  filter: (assignees) => assignees.length > 0,
  target: fxGetUsersData.prepend((assignees: { id: number }[]) => {
    const ids = assignees.map((item: Value) => item.id);

    return { users: ids };
  }),
});

sample({
  clock: fxGetTicketList.done,
  fn: ({ result: { tickets } }) => tickets.map((item) => item.assignees).flat(),
  target: getUsersData,
});

sample({
  clock: fxGetUpdatedTicketList.done,
  fn: ({ result: { tickets } }) => tickets.map((item) => item.assignees).flat(),
  target: getUsersData,
});

sample({
  clock: open,
  fn: (id) => {
    history.push(`${path}/${id}`);
  },
});

$isDetailOpen
  .on([addClicked, cloneTicket], () => true)
  .reset([closeDetail, signout, pageUnmounted]);

sample({
  clock: [$tableParams, $filters, pageMounted, $settingsInitialized],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }) => formatPayload(table, filters),
  target: fxGetTicketList,
});

sample({
  clock: getDeviceName,
  fn: (device) => device,
  target: fxGetDeviceName,
});

$equipment
  .on(fxGetDeviceName.done, (state, { result }) => {
    const name = result?.meta?.name || t('UnknownDevice');

    return { ...state, ...{ [result.meta?.serialnumber]: name } };
  })
  .reset(signout);

sample({
  clock: updateTicketList,
  source: { table: $tableParams, filters: $filters },
  fn: ({ table, filters }) => formatPayload(table, filters),
  target: fxGetUpdatedTicketList,
});

$rowCount
  .on(fxGetTicketList.done, (_, { result }) => result?.total || 0)
  .on(fxGetUpdatedTicketList.done, (_, { result }) => result?.total || 0)
  .reset([pageUnmounted, signout]);

sample({
  clock: fxGetTicketList.done,
  fn: ({ result: { tickets } }) => tickets.map((item) => item.objects).flat(),
  target: getObjects,
});

sample({
  clock: fxGetUpdatedTicketList.done,
  fn: ({ result: { tickets } }) => tickets.map((item) => item.objects).flat(),
  target: getObjects,
});

sample({
  clock: getObjects,
  filter: (objects) => objects.length > 0,
  fn: (objects: Objects[]) => ({ objects }),
  target: fxGetObjects,
});

$objects
  .on(fxGetObjects.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result;
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: changedEvent,
  filter: ({ event }) => event === 'tickets:refresh',
  target: updateTicketList,
});

sample({
  clock: fxGetTicketList.done,
  source: $types,
  fn: (type, { result }) => {
    result.tickets.map((item) => {
      const findedType = type.find(
        (element: { id: number }) => element?.id === item?.types[0]?.id
      );

      if (findedType?.paid) {
        getTicketInvoices(item.id);
      }
    });
  },
});

sample({
  clock: fxGetUpdatedTicketList.done,
  source: { type: $types, invoices: $invoices },
  fn: ({ type, invoices }, { result }) => {
    result.tickets.map((item) => {
      if (!Array.isArray(invoices[item.id])) {
        const findedType = type.find(
          (element: { id: number }) => element?.id === item?.types[0]?.id
        );

        if (findedType?.paid) {
          getTicketInvoices(item.id);
        }
      }
    });
  },
});

sample({
  clock: getTicketInvoices,
  fn: (id) => ({ ticket: { id } }),
  target: fxGetTicketInvoices,
});

$invoices
  .on(fxGetTicketInvoices.done, (state, { params, result }) => {
    if (!Array.isArray(result?.invoices)) {
      return {};
    }

    if (!params.ticket.id) {
      return state;
    }

    const invoices = { [params.ticket.id]: result.invoices };

    return { ...state, ...invoices };
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: pageMounted,
  target: [
    fxGetStatus,
    fxGetPriority,
    fxGetClass,
    fxGetTypes,
    fxGetInvoiceStatus,
    fxGetPrepaymentType,
  ],
});

sample({
  clock: $isGettingSettingsFromStorage,
  fn: (data) => data,
  target: $isLoading,
});
