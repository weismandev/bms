import type { TicketsPayload, Value } from '../../interfaces';

export const formatPayload = (
  table: { page: number; per_page: number; search: string },
  filters: any
) => {
  const { page, per_page, search } = table;

  const obj = [];

  if (filters.objects.length > 0) {
    const objects = filters.objects.map((item: Value) => ({
      id: item.id,
      type: 'complex',
    }));

    obj.push([objects]);
  }

  if (filters.houses.length > 0) {
    const houses = filters.houses.map((item: Value) => ({
      id: item.id,
      type: 'building',
    }));

    obj.push([houses]);
  }

  if (filters.apartments.length > 0) {
    const apartments = filters.apartments.map((item: Value) => ({
      id: item.id,
      type: 'apartment',
    }));

    obj.push([apartments]);
  }

  if (filters.property.length > 0) {
    const property = filters.property.map((item: Value) => ({
      id: item.id,
      type: 'property',
    }));

    obj.push([property]);
  }

  const objectsForFilter = (obj.reverse()[0] as [])?.flat() || [];

  const payload = {
    page,
    per_page,
    text: search,
  };

  if (filters.created_at_from.length > 0) {
    (payload as TicketsPayload).created_at_from = `${filters.created_at_from} 00:00:00`;
  }

  if (filters.created_at_to.length > 0) {
    (payload as TicketsPayload).created_at_to = `${filters.created_at_to} 23:59:59`;
  }

  if (filters.statuses.length > 0) {
    const statuses = filters.statuses.map((item: Value) => item.id);

    (payload as TicketsPayload).statuses = statuses;
  }

  if (filters.priorities.length > 0) {
    const priorities = filters.priorities.map((item: Value) => item.id);

    (payload as TicketsPayload).priorities = priorities;
  }

  if (filters.classes.length > 0) {
    const classes = filters.classes.map((item: Value) => item.id);

    (payload as TicketsPayload).classes = classes;
  }

  if (filters.types.length > 0) {
    const types = filters.types
      .flat()
      .filter((item: Value) => item?.id)
      .map((item: Value) => item.id);
    (payload as TicketsPayload).types = types;
  }

  if (filters.payment && typeof filters.payment === 'object') {
    (payload as TicketsPayload).paid = Boolean(filters.payment.id);
  }

  if (filters.invoiceStatus && typeof filters.invoiceStatus === 'object') {
    (payload as TicketsPayload).invoice_status = filters.invoiceStatus.id;
  }

  if (typeof filters.stuff_write_off_needed === 'object') {
    (payload as TicketsPayload).stuff_write_off_needed =
      filters.stuff_write_off_needed.id;
  }

  if (typeof filters.expired?.value === 'boolean') {
    (payload as TicketsPayload).expired = Number(filters.expired.value);
  }

  if (filters.assignees.length > 0) {
    const assignees = filters.assignees.map((item: Value) => item.id);

    (payload as TicketsPayload).assignees = assignees;
  }

  if (filters.rates.length > 0) {
    const formatRates = filters.rates.map((item: { id: string }) => item.id);

    (payload as TicketsPayload).rates = formatRates;
  }

  if (filters.initiators.length > 0) {
    const initiators = filters.initiators.map((item: Value) => item.id);

    (payload as TicketsPayload).initiators = initiators;
  }

  (payload as TicketsPayload).objects = objectsForFilter;

  if (filters.isUnreadMessage) {
    (payload as TicketsPayload).has_unread_messages = Number(filters.isUnreadMessage);
  }
  return payload;
};
