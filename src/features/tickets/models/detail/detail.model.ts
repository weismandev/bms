import { createEvent, createStore } from 'effector';

import i18n from '@shared/config/i18n';

const { t } = i18n;

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'works',
    onCreateIsDisabled: true,
    label: t('TicketTasks'),
  },
];

export const changeTab = createEvent<string>();

export const $currentTab = createStore<string>('info');
