import { signout } from '@features/common';

import { $currentTab, changeTab } from './detail.model';
import { addClicked } from '../page/page.model';
import { pageUnmounted } from '../ticket/ticket.model';

$currentTab.on(changeTab, (_, tab) => tab).reset([signout, addClicked, pageUnmounted]);
