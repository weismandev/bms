import { sample } from 'effector';

import { signout } from '@features/common';

import { pageUnmounted } from '../ticket/ticket.model';
import { getRates, fxGetRates, $rates } from './ticket-declarant.model';

sample({
  clock: getRates,
  target: fxGetRates,
});

$rates
  .on(fxGetRates.done, (_, { result }) => {
    if (!Array.isArray(result?.rates)) {
      return {};
    }

    return result.rates.reduce(
      (acc, value) => ({ ...acc, ...{ [value.user.id]: value } }),
      {}
    );
  })
  .reset([pageUnmounted, signout]);
