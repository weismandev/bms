import { createEffect, createEvent, createStore } from 'effector';

import { ListRatePayload, ListRateResponse, RateStore } from '../../interfaces';

export const $rates = createStore<RateStore>({});

export const getRates = createEvent<ListRatePayload>();

export const fxGetRates = createEffect<ListRatePayload, ListRateResponse, Error>();
