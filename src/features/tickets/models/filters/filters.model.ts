import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  created_at_from: '',
  created_at_to: '',
  statuses: [],
  priorities: [],
  classes: [],
  types: [],
  payment: '',
  invoiceStatus: '',
  objects: [],
  houses: [],
  apartments: [],
  property: [],
  initiators: [],
  stuff_write_off_needed: '',
  assignees: [],
  rates: [],
  expired: '',
  isUnreadMessage: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
