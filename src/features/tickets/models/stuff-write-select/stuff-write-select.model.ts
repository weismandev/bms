import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  { id: 0, title: t('Need') },
  { id: 1, title: t('NotNeed') },
];
