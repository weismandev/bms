import { createEvent, createEffect, createStore } from 'effector';
import * as Zod from 'zod';
import { createForm } from '@unicorn/effector-form';
import i18n from '@shared/config/i18n';

import type { EmailForm, ExportTickets } from './ticket-export.types';

const { t } = i18n;

export const fxExportTickets = createEffect<ExportTickets, ArrayBuffer, Error>();
export const exportTickets = createEvent<ExportTickets>();
export const $isLoading = createStore<boolean>(false);

export const emailForm = createForm<EmailForm>({
  initialValues: {
    email: '',
  },
  validationSchema: Zod.object({
    email: Zod
      .string()
      .email({
        message: t('incorrectEmail') ?? ''
      })
  }),
  editable: true,
});
