import { sample } from 'effector';
import FileSaver from 'file-saver';
import { format } from 'date-fns';
import { signout } from '@features/common';
import {
  emailForm,
  exportTickets,
  fxExportTickets,
  $isLoading
} from './ticket-export.model';
import { $filters } from '../filters';
import { $tableParams } from '../table/table.model';
import { formatPayload } from '../page/page.prepend';

$isLoading
  .on(fxExportTickets.pending, (_, pending) => pending)
  .reset(signout);

sample({
  clock: emailForm.submitted,
  fn: ({ email }) => ({ email, limit: 40000 }),
  target: exportTickets
});

sample({
  clock: exportTickets,
  source: [$filters, $tableParams],
  fn: ([filters, tableParams], { email, limit }) => {
    const payload = formatPayload(tableParams, filters);
    if (email) return ({ email, limit, ...payload });
    return ({ limit, ...payload });
  },
  target: fxExportTickets,
});

// eslint-disable-next-line effector/no-watch
fxExportTickets.done.watch(({ params, result }) => {
  // если в запросе не было указано email, тогда выгрузку делаю сразу на комп пользователя
  if (!params.email) {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
    FileSaver.saveAs(URL.createObjectURL(blob), `tickets-${dateTimeNow}.xlsx`);
  }
});
