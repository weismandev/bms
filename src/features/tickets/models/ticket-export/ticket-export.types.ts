export type EmailForm = {
  email: string;
};

export type ExportTickets = {
  email?: string;
  limit: number;
};
