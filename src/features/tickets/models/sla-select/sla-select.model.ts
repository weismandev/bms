import i18n from '@shared/config/i18n';

const { t } = i18n;

export const data = [
  {
    value: true,
    label: t('LateApplications'),
  },
  {
    value: false,
    label: t('IsNotLateApplications'),
  },
];
