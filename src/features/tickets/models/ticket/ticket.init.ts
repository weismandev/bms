import { merge, sample } from 'effector';
import { delay, pending } from 'patronum';
import { format } from 'date-fns';

import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import {
  $token,
  history,
  NEW_TICKET_MESSAGE,
  readNotifications,
  signout,
  TICKET_UPDATED,
} from '@features/common';
import { ClassesData } from '@features/tickets-class-select';
import { PriorityData } from '@features/tickets-priority-select';
import { fxGetList as fxGetStatuses } from '@features/tickets-status-select';
import { fxGetList as fxGetPrepaymentType } from '@features/tickets-prepayment-type-select';

import {
  AssigneesItem,
  CreateTicketPayload,
  Initiator,
  Repeat,
  TicketResponse,
  Value,
  Ticket,
  Objects,
} from '../../interfaces';
import { ticketApi } from '../../api';
import { $isDetailOpen, addClicked, path, fxGetTypes } from '../page/page.model';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  $mode,
  $ticket,
  $ticketData,
  API_URL,
  changedErrorDialogVisibility,
  changedMode,
  cloneTicket,
  exportTicket,
  exportOrder,
  fxCreateTicket,
  fxGetTicketById,
  fxNotifySupport,
  needTicket,
  notifySupport,
  pageMounted,
  pageUnmounted,
  entityApi,
} from './ticket.model';
import {
  fxUploadAttachment,
  fxCreateInvoice,
  fxGetInvoices,
  fxUpdateInvoice,
  fxDeleteInvoice,
} from '../ticket-invoice/ticket-invoice.model';
import {
  fxSetTicketPriority,
  fxChangeTicketStatus,
  fxChangeAppealTicket,
  fxSetEscalation,
  fxSetDescription,
  fxSetTicketType,
  fxCheckAppeal,
  fxCheckEscalation,
} from '../ticket-info/ticket-info.model';
import { fxGetTicketTmc, getTicketTmc, fxSetTmc } from '../ticket-tmc/ticket-tmc.model';
import { fxGetRates, getRates } from '../ticket-declarant';
import {
  fxUpdateAssignessTicket,
  fxGetUsersInfo,
  getUsersInfo,
} from '../ticket-assignees/ticket-assignees.model';
import {
  fxSetRepeat,
  fxGetRepeat,
  getRepeat,
} from '../ticket-repeat/ticket-repeat.model';
import { fxGetTicketWorks, fxSetTicketWorks } from '../ticket-works/ticket-works.model';
import { fxSetPlannedDate } from '../ticket-time/ticket-time.model';
import { fxGetObjects, getObjects } from '../ticket-objects/ticket-objects.model';
import { formatPayloadDate } from '../../libs';
import { changeTab } from '../detail/detail.model';

fxGetTicketById.use(ticketApi.getTicketById);
fxCreateTicket.use(ticketApi.createTicket);
fxGetUsersInfo.use(ticketApi.getUsersInfo);
fxSetTicketPriority.use(ticketApi.setTicketPriority);
fxUpdateAssignessTicket.use(ticketApi.changeAssignessTicket);
fxChangeTicketStatus.use(ticketApi.changeTicketStatus);
fxChangeAppealTicket.use(ticketApi.changeAppeal);
fxGetRates.use(ticketApi.getTicketRates);
fxSetRepeat.use(ticketApi.setRepeat);
fxGetRepeat.use(ticketApi.getRepeat);
fxSetPlannedDate.use(ticketApi.setPlannedDate);
fxGetTicketTmc.use(ticketApi.getTicketTmc);
fxSetTmc.use(ticketApi.setTmc);
fxGetObjects.use(ticketApi.getObjects);
fxSetEscalation.use(ticketApi.setEscalation);
fxNotifySupport.use(ticketApi.notifySupport);
fxGetTicketWorks.use(ticketApi.getWorks);
fxSetDescription.use(ticketApi.setDescription);
fxCheckEscalation.use(ticketApi.checkEscalation);
fxCheckAppeal.use(ticketApi.checkAppeal);
fxSetTicketType.use(ticketApi.updateTicketTypes);
fxUploadAttachment.use(ticketApi.uploadFile);
fxCreateInvoice.use(ticketApi.createInvoice);
fxGetInvoices.use(ticketApi.getInvoices);
fxUpdateInvoice.use(ticketApi.updateInvoice);
fxDeleteInvoice.use(ticketApi.deleteInvoice);
fxSetTicketWorks.use(ticketApi.setWorks);

const errorOccured = merge([
  fxGetTicketById.fail,
  fxCreateTicket.fail,
  fxGetUsersInfo.fail,
  fxSetTicketPriority.fail,
  fxUpdateAssignessTicket.fail,
  fxChangeTicketStatus.fail,
  fxChangeAppealTicket.fail,
  fxGetRates.fail,
  fxSetRepeat.fail,
  fxGetRepeat.fail,
  fxSetPlannedDate.fail,
  fxSetTmc.fail,
  fxGetObjects.fail,
  fxSetEscalation.fail,
  fxNotifySupport.fail,
  fxSetDescription.fail,
  fxCheckEscalation.fail,
  fxCheckAppeal.fail,
  fxGetTicketTmc.fail,
  fxGetTicketWorks.fail,
  fxSetTicketType.fail,
  fxUploadAttachment.fail,
  fxCreateInvoice.fail,
  fxGetInvoices.fail,
  fxUpdateInvoice.fail,
  fxDeleteInvoice.fail,
  fxSetTicketWorks.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetTicketById,
        fxCreateTicket,
        fxGetUsersInfo,
        fxSetTicketPriority,
        fxUpdateAssignessTicket,
        fxChangeTicketStatus,
        fxChangeAppealTicket,
        fxGetRates,
        fxSetRepeat,
        fxGetRepeat,
        fxSetPlannedDate,
        fxGetTicketTmc,
        fxSetTmc,
        fxGetObjects,
        fxSetEscalation,
        fxNotifySupport,
        fxGetTicketWorks,
        fxSetDescription,
        fxCheckEscalation,
        fxCheckAppeal,
        fxSetTicketType,
        fxUploadAttachment,
        fxCreateInvoice,
        fxGetInvoices,
        fxUpdateInvoice,
        fxDeleteInvoice,
        fxSetTicketWorks,
      ],
    }),
    (_, status) => status
  )
  .reset([pageUnmounted, signout]);

$error
  .on(errorOccured, (_, { error }) => error)
  .on(fxGetTicketTmc.fail, (_, { error }) => error)
  .reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$mode
  .on(changedMode, (_, mode) => mode)
  .on(pageMounted, () => 'view')
  .on($isDetailOpen, (_, isDetailOpen) => (isDetailOpen ? 'edit' : 'view'))
  .reset([signout, fxCreateTicket.done, fxUpdateAssignessTicket.done, changeTab]);

sample({
  clock: [pageMounted, needTicket],
  fn: ({ ticketId }) => ({ ticket: { id: ticketId } }),
  target: fxGetTicketById,
});

const formatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(new Date(localDate), "yyyy-MM-dd'T'HH:mm");
};

const formatTicket = (result: TicketResponse) => {
  const { ticket } = result;

  if (ticket.id) {
    getRepeat(ticket.id);
    getTicketTmc(ticket.id);
  }

  if (ticket.objects.length > 0) {
    getObjects(ticket.objects);
  }

  if ((ticket.initiator as Initiator)?.id) {
    getRates({ ticket: { id: ticket.id } });
  }

  if (ticket.assignees.length > 0) {
    getUsersInfo(ticket.assignees);
  }

  return {
    id: ticket.id,
    description: ticket.description || '-',
    class: ticket.class || '',
    types: [ticket.types[0]?.id],
    formattedTypes: '-',
    priority: ticket.priority || '',
    sla: ticket?.sla ? formatDate(ticket.sla) : '',
    assignees: '',
    origin: ticket.origin,
    status: ticket.status || '',
    date_create_ticket: formatDate(ticket.created_at),
    planned_start_at: ticket?.planned_start_at
      ? new Date(convertUTCDateToLocalDate(new Date(ticket?.planned_start_at)))
      : '',
    planned_end_at: ticket?.planned_end_at
      ? new Date(convertUTCDateToLocalDate(new Date(ticket?.planned_end_at)))
      : '',
    object: '',
    house: '',
    apartment: '',
    property: '',
    ticket_number: ticket.number,
    is_appeal: Boolean(ticket.is_appeal),
    is_escalation_enabled: Boolean(ticket.is_escalation_enabled),
    initiator: ticket.initiator,
    until_repeat: '',
    type_repeat: '',
    every_repeat: '',
    actual_end_at: ticket.actual_end_at ? formatDate(ticket.actual_end_at) : '',
    channel: ticket.channel.id,
    enterprise: ticket.enterprise_id,
    invoices: [],
  };
};

$ticketData
  .on(fxGetTicketById.done, (_, { result }) => formatTicket(result))
  .on(fxUpdateAssignessTicket.done, (_, { result }) => formatTicket(result))
  .reset([signout, addClicked, fxGetTicketById.pending]);

sample({
  clock: [pageMounted, needTicket],
  target: fxGetStatuses,
});

sample({
  clock: fxCreateTicket.done,
  fn: ({ result }) => {
    history.push(`${path}/${result.ticket.id}`);
  },
});

const delayedRedirect = delay({ source: fxGetTicketById.fail, timeout: 3000 });

delayedRedirect.watch(() => {
  history.push(path);
});

sample({
  clock: cloneTicket,
  fn: () => {
    history.push(path);
  },
});

$ticket
  .on(cloneTicket, (state) => {
    const isArchivedTypes = Boolean([...state.types].reverse()[0]?.archived);

    state.id = null;

    if (isArchivedTypes) {
      state.types = [];
    }

    return state;
  })
  .reset([signout, addClicked]);

sample({
  clock: exportTicket,
  source: { ticket: $ticket, token: $token },
  fn: ({ ticket: { id }, token }) =>
    window.open(
      `${API_URL}/v1/tck/bms/tickets/item/export/?token=${token}&ticket[id]=${id}&lang=${localStorage.getItem('lang')}`
    ),
});

sample({
  clock: notifySupport,
  source: $ticket,
  fn: ({ id }) => ({ ticket: { id } }),
  target: fxNotifySupport,
});

sample({
  source: $ticket,
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => ({
    id,
    event: TICKET_UPDATED,
  }),
  target: readNotifications,
});

sample({
  source: $ticket,
  filter: ({ id }) => Boolean(id),
  fn: ({ id }) => ({
    id,
    event: NEW_TICKET_MESSAGE,
  }),
  target: readNotifications,
});

sample({
  clock: pageMounted,
  target: fxGetPrepaymentType,
});

sample({
  clock: exportOrder,
  source: { ticket: $ticket, token: $token },
  fn: ({ ticket: { id }, token }) =>
    window.open(
      `${API_URL}/v1/tck/bms/tickets/item/export-order/?token=${token}&ticket[id]=${id}&lang=${localStorage.getItem('lang')}`
    ),
});

sample({
  clock: fxGetTicketById.done,
  target: fxGetTypes,
});

const formatObjects = (
  object: Value,
  house: Value,
  apartment: Value,
  property: Value
) => {
  const objects = [];

  if (object) {
    objects.push({
      id: object?.id || object,
      type: 'complex',
    });
  }

  if (house) {
    objects.push({ id: house?.id || house, type: 'building' });
  }

  if (apartment) {
    objects.push({
      id: apartment?.id || apartment,
      type: 'apartment',
    });
  }

  if (property) {
    objects.push({
      id: property?.id || property,
      type: 'property',
    });
  }

  const reversedObjects = objects.reverse()[0];

  return reversedObjects ? [reversedObjects as Objects] : [];
};

const formatRepeat = (
  until_repeat: string,
  every_repeat: string,
  type_repeat: string | Repeat
) => {
  const repeat: { until?: string; every?: string; type?: string } = {};

  if (until_repeat) {
    repeat.until = until_repeat;
  }

  if (every_repeat) {
    repeat.every = every_repeat;
  }

  if (type_repeat) {
    repeat.type = (type_repeat as Repeat)?.id;
  }

  return repeat;
};

sample({
  clock: entityApi.create,
  fn: (params: Ticket): CreateTicketPayload => {
    const findedType = [...params.types].reverse().find((item: Value) => item);

    const assignees =
      params.assignees.length > 0
        ? (params.assignees as AssigneesItem[]).map((item) => {
            if (item?.user) {
              return item.user.id;
            }

            return item.id;
          })
        : [];

    const payload: CreateTicketPayload = {
      id: params.id,
      description: params.description,
      origin: typeof params.origin === 'object' ? params.origin.name : params.origin,
      status: typeof params.status === 'object' ? params.status.id : params.status,
      types: findedType?.id ? [findedType.id] : [],
      sla: params.sla && formatPayloadDate(params.sla),
      assignees,
      class:
        typeof params.class === 'object'
          ? (params.class as ClassesData)?.id
          : params.class,
      priority:
        typeof params.priority === 'object'
          ? (params.priority as PriorityData)?.id
          : params.priority,
      planned_start_at: params?.planned_start_at
        ? formatPayloadDate(params.planned_start_at)
        : '',
      planned_end_at: params?.planned_end_at
        ? formatPayloadDate(params.planned_end_at)
        : '',
      objects: formatObjects(
        params.object as Value,
        params.house as Value,
        params.apartment as Value,
        params.property as Value
      ),
      is_appeal: Number(Boolean(params?.is_appeal)),
      initiator: (params.initiator as Initiator)?.id
        ? {
            id: (params.initiator as Initiator).id,
          }
        : null,
      repeat: formatRepeat(params.until_repeat, params.every_repeat, params.type_repeat),
    };

    if (typeof params.enterprise === 'object' && Boolean(params.enterprise?.id)) {
      payload.enterprise_id = params.enterprise.id;
    }

    return payload;
  },
  target: fxCreateTicket,
});
