import { combine, createEffect, createEvent, createStore, sample, split } from 'effector';
import { createGate } from 'effector-react';

import { $data as $enterprises } from '@features/enterprise-select';

import {
  CreateTicketPayload,
  Initiator,
  NotifySupportPayload,
  ObjectParts,
  ObjectsResponse,
  RateStore,
  RepeatStore,
  Ticket,
  TicketPayload,
  TicketResponse,
  Value,
} from '../../interfaces';
import { getTypesList } from '../../libs';
import { $invoices } from '../ticket-invoice/ticket-invoice.model';
import { $types } from '../page/page.model';
import { $ticketTmc } from '../ticket-tmc/ticket-tmc.model';
import { $rates } from '../ticket-declarant/ticket-declarant.model';
import { $usersInfo } from '../ticket-assignees/ticket-assignees.model';
import { $repeat } from '../ticket-repeat/ticket-repeat.model';
import { $objects } from '../ticket-objects/ticket-objects.model';

// eslint-disable-next-line prefer-destructuring
export const API_URL = process.env.API_URL;

interface Gate {
  ticketId: number;
}

const entityTicket = {
  id: null,
  description: '',
  class: '',
  origin: '',
  status: '',
  types: [],
  formattedTypes: '-',
  priority: '',
  sla: '',
  assignees: '',
  planned_start_at: '',
  planned_end_at: '',
  object: '',
  house: '',
  apartment: '',
  is_appeal: false,
  is_escalation_enabled: false,
  property: '',
  initiator: '',
  until_repeat: '',
  type_repeat: '',
  every_repeat: '',
  actual_end_at: '',
  channel: '',
  enterprise: '',
  invoices: [],
};

export const TicketGate = createGate<Gate>();

export const pageMounted = TicketGate.open;
export const pageUnmounted = TicketGate.close;

export const ticketSubmitted = createEvent<any>();
export const changedErrorDialogVisibility = createEvent<boolean>();
export const changedMode = createEvent<string>();
export const cloneTicket = createEvent<void>();
export const exportOrder = createEvent<void>();
// Для вызова с других страниц
export const needTicket = createEvent<any>();
export const exportTicket = createEvent<void>();
export const notifySupport = createEvent<void>();
export const checkPermissions = createEvent<void>();

export const fxGetTicketById = createEffect<TicketPayload, TicketResponse, Error>();
export const fxCreateTicket = createEffect<CreateTicketPayload, TicketResponse, Error>();
export const fxNotifySupport = createEffect<NotifySupportPayload, object, Error>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $ticketData = createStore<Ticket>(entityTicket);
export const $mode = createStore<string>('view');

const formatObjects = (objects: ObjectsResponse[]) =>
  objects.reduce((acc: { [key: string]: ObjectParts[] }, item: ObjectsResponse) => {
    if (item.object.type === 'complex') {
      const type = { complex: item.parts };

      return { ...acc, ...type };
    }

    if (item.object.type === 'building') {
      const type = { building: item.parts };

      return { ...acc, ...type };
    }

    if (item.object.type === 'apartment') {
      const type = { apartment: item.parts };

      return { ...acc, ...type };
    }

    if (item.object.type === 'property') {
      const type = { property: item.parts };

      return { ...acc, ...type };
    }

    return acc;
  }, {});

export const $ticket = combine(
  $ticketData,
  $usersInfo,
  $types,
  $rates,
  $repeat,
  $ticketTmc,
  $objects,
  $enterprises,
  $invoices,
  (
    ticketData,
    userInfo,
    types,
    rates,
    repeat,
    ticketTmc,
    objects,
    enterprises,
    invoices
  ) => {
    const currentType: any = ticketData.types[0];

    const typesList = currentType ? getTypesList(currentType, types) : [];

    const formattedObjects = formatObjects(objects);

    if (formattedObjects.property) {
      ticketData.object = formattedObjects.property[0];
      ticketData.house = formattedObjects.property[1]?.id;
      ticketData.property = formattedObjects.property[2];
    }

    if (formattedObjects.apartment) {
      ticketData.object = formattedObjects.apartment[0];
      ticketData.house = formattedObjects.apartment[1]?.id;
      ticketData.apartment = formattedObjects.apartment[2];
    }

    if (formattedObjects.building) {
      ticketData.object = formattedObjects.building[0];
      ticketData.house = formattedObjects.building[1]?.id;
    }

    if (formattedObjects.complex) {
      ticketData.object = formattedObjects.complex[0];
    }

    const planned_duration = types.filter((item: Value) => item.id === currentType)[0]
      ?.planned_duration_minutes;

    if ((ticketData?.initiator as Initiator)?.id && Object.values(rates).length > 0) {
      const initiatorId = (ticketData.initiator as Initiator).id;

      const rateData = (rates as RateStore)[initiatorId];

      ticketData.rate = rateData.rate;
      ticketData.comment = rateData.comment;
    }

    if (Array.isArray(userInfo) && userInfo.length > 0) {
      ticketData.assignees = userInfo;
    }

    if (typesList[0]) {
      const typesCount = typesList.length - 1;

      const types = typesList.map((item: Value, index: number) =>
        index !== typesCount ? `${item.title} -> ` : item.title
      );

      const formattedTypes = types.join('');

      ticketData.types = typesList;
      ticketData.formattedTypes = formattedTypes;
    }

    if (enterprises.length > 0) {
      const formattedEnterprises = enterprises.reduce(
        (acc: { [key: number]: Value }, value: Value) => ({
          ...acc,
          ...{ [value.id]: value },
        }),
        {}
      );

      const enterprise = formattedEnterprises[(ticketData as any).enterprise];

      if (enterprise) {
        ticketData.enterprise = enterprise;
      }
    }

    if (ticketTmc.length > 0) {
      ticketData.tmc = ticketTmc;
    } else {
      ticketData.tmc = [];
    }

    if (invoices.length > 0) {
      ticketData.invoices = invoices;
    } else {
      ticketData.invoices = [];
    }

    ticketData.planned_duration = planned_duration;
    ticketData.type_repeat = (repeat as RepeatStore).type_repeat;
    ticketData.every_repeat = (repeat as RepeatStore).every_repeat;
    ticketData.until_repeat = (repeat as RepeatStore).until_repeat;
    ticketData.is_repeat_ticket = Boolean((repeat as RepeatStore).every_repeat);

    return ticketData;
  }
);

export const entityApi = split(
  sample($ticket, ticketSubmitted, (ticket, payload) => ({
    ...ticket,
    ...payload,
  })),
  {
    create: ({ id }) => !id,
    update: ({ id }) => Boolean(id),
  }
);
