import { sample } from 'effector';

import { entityApi } from '../ticket/ticket.model';
import { fxSetPlannedDate } from './ticket-time.model';
import { formatPayloadDate } from '../../libs';

sample({
  clock: entityApi.update,
  filter: ({ planned_start_at, planned_end_at }) => planned_start_at && planned_end_at,
  fn: ({ id, planned_start_at, planned_end_at }) => ({
    ticket: { id },
    planned_start_at: planned_start_at ? formatPayloadDate(planned_start_at) : '',
    planned_end_at: planned_end_at ? formatPayloadDate(planned_end_at) : '',
  }),
  target: fxSetPlannedDate,
});
