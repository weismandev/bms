import { createEffect } from 'effector';

import { SetPlannedDatePayload, TicketResponse } from '../../interfaces';

export const fxSetPlannedDate = createEffect<
  SetPlannedDatePayload,
  TicketResponse,
  Error
>();
