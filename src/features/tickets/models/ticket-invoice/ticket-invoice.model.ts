import { createStore, createEvent, createEffect } from 'effector';

import {
  UploadAttachmentPayload,
  UploadAttachmentResponse,
  UploadAttachment,
  Attachment,
  CreateInvoicePayload,
  GetInvoicesPayload,
  GetInvoicesResponse,
  Invoice,
  UpdateInvoicePayload,
  DeleteInvoicePayload,
  FileInvoice,
} from '../../interfaces';

export const $attachment = createStore<Attachment | null>(null);
export const $invoices = createStore<Invoice[]>([]);

export const uploadAttachment = createEvent<UploadAttachment>();
export const downloadFile = createEvent<FileInvoice>();

export const fxUploadAttachment = createEffect<
  UploadAttachmentPayload,
  UploadAttachmentResponse,
  Error
>();
export const fxCreateInvoice = createEffect<CreateInvoicePayload, object, Error>();
export const fxGetInvoices = createEffect<
  GetInvoicesPayload,
  GetInvoicesResponse,
  Error
>();
export const fxUpdateInvoice = createEffect<UpdateInvoicePayload, object, Error>();
export const fxDeleteInvoice = createEffect<DeleteInvoicePayload, object, Error>();
