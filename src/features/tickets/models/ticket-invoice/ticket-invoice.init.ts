import { sample } from 'effector';
import FileSaver from 'file-saver';

import { signout } from '@features/common';

import {
  $attachment,
  fxUploadAttachment,
  uploadAttachment,
  fxCreateInvoice,
  fxGetInvoices,
  $invoices,
  fxUpdateInvoice,
  fxDeleteInvoice,
  downloadFile,
} from './ticket-invoice.model';
import {
  pageUnmounted,
  $ticketData,
  changedMode,
  $ticket,
  entityApi,
} from '../ticket/ticket.model';
import {
  UploadAttachment,
  SubmitInvoice,
  Ticket,
  DeleteInvoicePayload,
} from '../../interfaces';

sample({
  clock: uploadAttachment,
  filter: ({ file }) => Boolean(file),
  fn: ({ file, index }: UploadAttachment) => ({
    type: 'file',
    file,
    index,
  }),
  target: fxUploadAttachment,
});

$attachment
  .on(fxUploadAttachment.done, (_, { params, result }) => ({
    id: result.id,
    name: params.file.name,
    index: params.index,
  }))
  .reset([signout, pageUnmounted, fxUploadAttachment.fail, changedMode]);

sample({
  clock: entityApi.update,
  filter: (values: Ticket) =>
    values.invoices.filter((item: { id: number }) => !item.id).length > 0,
  fn: (values: Ticket) => ({
    ticket_id: values.id,
    invoices: Array.isArray(values.invoices)
      ? values.invoices
          .filter((item: { id: number }) => !item.id)
          .map((item: { file: { id: number }; amount: number }) => ({
            file_id: item.file.id,
            total: item.amount,
          }))
      : [],
  }),
  target: fxCreateInvoice,
});

sample({
  source: $ticketData,
  filter: (ticket) => Boolean(ticket.id),
  fn: ({ id }: { id: number | null }) => ({ ticket: { id } }),
  target: fxGetInvoices,
});

$invoices
  .on(fxGetInvoices.done, (_, { result }) => {
    if (!Array.isArray(result?.invoices)) {
      return [];
    }

    return result.invoices.map((invoice) => ({
      id: invoice.id,
      file: {
        url: invoice.file,
        name: invoice.name,
      },
      status: invoice.status,
      amount: invoice.total,
    }));
  })
  .reset([pageUnmounted, signout]);

sample({
  clock: fxCreateInvoice.done,
  source: $ticketData,
  fn: ({ id }) => ({ ticket: { id } }),
  target: fxGetInvoices,
});

sample({
  clock: entityApi.update,
  filter: (values: Ticket) =>
    values.invoices.filter((item: { id: number }) => item.id).length > 0,
  fn: (values: Ticket) => ({
    invoices: Array.isArray(values.invoices)
      ? values.invoices
          .filter((item: { id: number }) => item.id)
          .map((item: { id: number; amount: number; status: { id: string } }) => ({
            id: item.id,
            total: item.amount,
            status: typeof item.status === 'object' ? item.status.id : item.status,
          }))
      : [],
  }),
  target: fxUpdateInvoice,
});

const getDeleteInvoices = (data: SubmitInvoice) =>
  data.currentValues.invoices.filter(
    (currentValue: { id: number }) =>
      !data.newValues.invoices.some(
        (newValue: { id: number }) => newValue.id === currentValue.id
      )
  );

sample({
  clock: entityApi.update,
  source: $ticket,
  filter: (currentValues: any, newValues: any) => {
    const deleteInvoices = getDeleteInvoices({ currentValues, newValues });

    return deleteInvoices.length > 0;
  },
  fn: (currentValues: any, newValues: any): DeleteInvoicePayload => {
    const deleteInvoices = getDeleteInvoices({ currentValues, newValues });

    return {
      ids: deleteInvoices.map((item: { id: number }) => item.id),
    };
  },
  target: fxDeleteInvoice,
});

sample({
  clock: fxDeleteInvoice.done,
  source: $ticketData,
  fn: ({ id }) => ({ ticket: { id } }),
  target: fxGetInvoices,
});

sample({
  clock: fxUpdateInvoice.done,
  source: $ticketData,
  fn: ({ id }) => ({ ticket: { id } }),
  target: fxGetInvoices,
});

downloadFile.watch((file) => FileSaver.saveAs(file.url, file.name));
