import { createEffect, createEvent, createStore } from 'effector';

import { Objects, ObjectsPayload, ObjectsResponse } from '../../interfaces';

export const $objects = createStore<ObjectsResponse[]>([]);

export const getObjects = createEvent<Objects[]>();

export const fxGetObjects = createEffect<ObjectsPayload, ObjectsResponse[], Error>();
