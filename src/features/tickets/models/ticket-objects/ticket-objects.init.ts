import { sample } from 'effector';

import { signout } from '@features/common';

import { Objects } from '../../interfaces';
import { pageUnmounted } from '../ticket/ticket.model';
import { getObjects, fxGetObjects, $objects } from './ticket-objects.model';

sample({
  clock: getObjects,
  fn: (objects: Objects[]) => ({ objects }),
  target: fxGetObjects,
});

$objects
  .on(fxGetObjects.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result;
  })
  .reset([pageUnmounted, signout]);
