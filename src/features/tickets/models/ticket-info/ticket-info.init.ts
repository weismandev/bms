import { sample } from 'effector';

import { signout } from '@features/common';

import { Value, ObjectParts, Ticket } from '../../interfaces';
import { addClicked } from '../page/page.model';
import {
  $ticket,
  fxGetTicketById,
  pageUnmounted,
  entityApi,
} from '../ticket/ticket.model';
import {
  $permissions,
  fxSetTicketPriority,
  fxChangeTicketStatus,
  fxChangeAppealTicket,
  fxSetEscalation,
  fxSetDescription,
  fxSetTicketType,
  checkAppeal,
  fxCheckAppeal,
  fxCheckEscalation,
  $formattedTypes,
  $formattedPrepaymentTypes,
} from './ticket-info.model';
import { $objects } from '../ticket-objects';

sample({
  clock: $objects,
  source: $ticket,
  filter: ({ object }) => Boolean((object as ObjectParts)?.id),
  fn: ({ object }) => ({
    complex_id: (object as ObjectParts).id,
  }),
  target: [fxCheckEscalation, fxCheckAppeal],
});

$permissions
  .on(fxCheckEscalation.doneData, (state, data) => ({
    ...state,
    escalation: data,
  }))
  .on(fxCheckAppeal.doneData, (state, data) => ({
    ...state,
    appeal: data,
  }))
  .reset([pageUnmounted, signout, addClicked]);

sample({
  clock: entityApi.update,
  fn: ({ id, priority }: Ticket) => ({
    ticket: {
      id,
    },
    priority: {
      name: typeof priority === 'object' ? priority.id : priority,
    },
  }),
  target: fxSetTicketPriority,
});

sample({
  clock: entityApi.update,
  fn: ({ id, status }: Ticket) => ({
    ticket: {
      id,
    },
    status: {
      name: typeof status === 'object' ? status.id : status,
    },
  }),
  target: fxChangeTicketStatus,
});

$ticket.on(fxChangeTicketStatus.fail, (state) => {
  const newState = { ...state };

  newState.status = '';

  return newState;
});

sample({
  clock: entityApi.update,
  fn: ({ id, description }: Ticket) => ({
    ticket: { id },
    description,
  }),
  target: fxSetDescription,
});

sample({
  clock: entityApi.update,
  source: { ticket: $ticket, formattedTypes: $formattedTypes },
  filter: ({ ticket, formattedTypes }, values) => {
    const types = [...ticket.types].filter((item) => item);
    const findedTypeId = types[0] ? types.reverse()[0]?.id : null;

    const paidData = findedTypeId ? formattedTypes[findedTypeId]?.paid || false : false;

    return values.status === 'new' && !paidData;
  },
  fn: (_, values) => {
    const findedType = [...values.types].reverse().find((item: Value) => item);

    return {
      ticket: {
        id: values.id,
      },
      types: findedType?.id ? [findedType.id] : [],
    };
  },
  target: fxSetTicketType,
});

sample({
  clock: entityApi.update,
  source: $ticket,
  filter: (ticket, values) => ticket.is_appeal != values.is_appeal,
  fn: (_, { id, is_appeal }) => ({
    ticket: {
      id,
      is_appeal: Number(Boolean(is_appeal)),
    },
  }),
  target: fxChangeAppealTicket,
});

sample({
  clock: entityApi.update,
  source: $ticket,
  filter: (ticket, values) =>
    ticket.is_escalation_enabled != values.is_escalation_enabled,
  fn: (_, { id, is_escalation_enabled }) => ({
    ticket: {
      id,
    },
    escalation: {
      enabled: Number(Boolean(is_escalation_enabled)),
    },
  }),
  target: fxSetEscalation,
});

sample({
  clock: checkAppeal,
  fn: (id) => ({ complex_id: id }),
  target: fxCheckAppeal,
});

$formattedTypes.reset([pageUnmounted, signout]);
$formattedPrepaymentTypes.reset([pageUnmounted, signout]);
