import { createEffect, createEvent, createStore } from 'effector';

import { $data as $prepaymentTypes } from '@features/tickets-prepayment-type-select';

import { $types } from '../page/page.model';
import {
  ChangeAppealPayload,
  ChangeStatusPayload,
  SetDescriptionPayload,
  SetEscalationPayload,
  SetPriorityPayload,
  TicketResponse,
  SetTicketTypePayload,
  Permissions,
  TypesData,
} from '../../interfaces';

export const $permissions = createStore<Permissions>({});
export const $formattedTypes = $types.map((types) => {
  if (!Array.isArray(types)) {
    return {};
  }

  return types.reduce(
    (acc: { [key: number]: TypesData }, type: TypesData) => ({
      ...acc,
      ...{ [type.id]: type },
    }),
    {}
  );
});
export const $formattedPrepaymentTypes = $prepaymentTypes.map((types) => {
  if (!Array.isArray(types)) {
    return {};
  }

  return types.reduce((acc, type) => ({ ...acc, ...{ [type.name]: type.title } }), {});
});

export const checkAppeal = createEvent<number>();

export const fxSetTicketPriority = createEffect<
  SetPriorityPayload,
  TicketResponse,
  Error
>();
export const fxChangeTicketStatus = createEffect<
  ChangeStatusPayload,
  TicketResponse,
  Error
>();
export const fxChangeAppealTicket = createEffect<
  ChangeAppealPayload,
  TicketResponse,
  Error
>();
export const fxSetEscalation = createEffect<
  SetEscalationPayload,
  TicketResponse,
  Error
>();
export const fxSetDescription = createEffect<SetDescriptionPayload, object, Error>();
export const fxSetTicketType = createEffect<SetTicketTypePayload, object, Error>();
export const fxCheckAppeal = createEffect<any, any, any>();
export const fxCheckEscalation = createEffect<any, any, any>();
