import { combine } from 'effector';
import { format } from 'date-fns';

import i18n from '@shared/config/i18n';
import { createTableBag } from '@features/data-grid';
import { formatPhone } from '@tools/formatPhone';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import { ClassesData } from '@features/tickets-class-select';
import { StatusData } from '@features/tickets-status-select';
import { PriorityData } from '@features/tickets-priority-select';
import { InvoiceStatusItem } from '@features/tickets-invoices-status-select';

import { $tickets, $rowCount } from '../page/page.model';
import {
  Users,
  Initiator,
  TicketsResponseItem,
  Value,
  ObjectsResponse,
  Objects,
  InvoiceResponse,
} from '../../interfaces';
import { getTypesList, isDeadlineExpired } from '../../libs';

const { t } = i18n;

interface Data {
  tickets: TicketsResponseItem[];
  status: StatusData[];
  priority: PriorityData[];
  classTicket: ClassesData[];
  type: any;
  equipment: { [key: number]: string };
  usersData: string | Users[];
  objects: ObjectsResponse[];
  invoices: { [key: number]: InvoiceResponse[] };
  invoiceStatuses: InvoiceStatusItem[];
}

interface UserList {
  [key: number]: string;
}

const formatDate = (date: string | null) => {
  if (!date || date?.length === 0) {
    return '-';
  }
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(new Date(localDate), 'dd.MM.yyy в HH:mm');
};

const getTicketType = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  return array.find((item: { id: string }) => item.id === id) || '-';
};

const getTicketInfo = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  return array.find((item: { id: string }) => item.id === id)?.title || '-';
};

const getTicketStatus = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  const findedValue = array.find((item: { id: string }) => item.id === id);

  return [findedValue?.id, findedValue?.title];
};

const formatAssignees = (assignees: { id: number }[], usersList: UserList) => {
  if (assignees.length === 0) {
    return '';
  }

  return assignees.map((item: Value) => usersList[item.id]).join();
};

const formatObjects = (object: any, list: any) => {
  const formattedObject = object.reduce(
    (acc: { [key: string]: Objects[] }, item: Objects) => {
      if (item.type === 'complex') {
        const data = { ['complex']: item };

        return { ...acc, ...data };
      }

      if (item.type === 'building') {
        const data = { ['building']: item };

        return { ...acc, ...data };
      }

      if (item.type === 'apartment') {
        const data = { ['apartment']: item };

        return { ...acc, ...data };
      }

      if (item.type === 'property') {
        const data = { ['property']: item };

        return { ...acc, ...data };
      }

      return acc;
    },
    {}
  );

  const obj = Boolean(formattedObject.property)
    ? formattedObject.property
    : Boolean(formattedObject.apartment)
    ? formattedObject.apartment
    : Boolean(formattedObject.building)
    ? formattedObject.building
    : Boolean(formattedObject.complex)
    ? formattedObject.complex
    : null;

  const objectsInfo = list[obj.type][obj.id];

  if (!objectsInfo) {
    return null;
  }

  const objects: any = {
    object: objectsInfo[0]?.title,
    house: objectsInfo[1]?.title,
  };

  const type = objectsInfo[2]?.type;

  if (type === 'apartment') {
    objects.apartment = objectsInfo[2]?.title;
  }

  if (type === 'property') {
    objects.property = objectsInfo[2]?.title;
  }

  return objects;
};

const formatData = ({
  tickets,
  status,
  priority,
  classTicket,
  type,
  equipment,
  usersData,
  objects,
  invoices,
  invoiceStatuses,
}: Data) => {
  const usersList = Array.isArray(usersData)
    ? usersData.reduce(
        (acc: {}, value: Users) => ({
          ...acc,
          ...{ [value.user.id]: value.user.fullname },
        }),

        {}
      )
    : {};

  const objectsList =
    objects.length > 0
      ? objects.reduce(
          (
            acc: { apartment: {}; building: {}; property: {}; complex: {} },
            { object, parts }: ObjectsResponse
          ) => {
            const newAcc = { ...acc };
            const value = { [object.id]: parts };

            if (object.type === 'complex') {
              newAcc.complex = {
                ...acc.complex,
                ...value,
              };
            }

            if (object.type === 'apartment') {
              newAcc.apartment = {
                ...acc.apartment,
                ...value,
              };
            }

            if (object.type === 'building') {
              newAcc.building = {
                ...acc.building,
                ...value,
              };
            }

            if (object.type === 'property') {
              newAcc.property = {
                ...acc.property,
                ...value,
              };
            }

            return newAcc;
          },
          { apartment: {}, building: {}, property: {}, complex: {} }
        )
      : { apartment: {}, building: {}, property: {}, complex: {} };

  return tickets.map((item: TicketsResponseItem) => {
    const devicesName =
      item.devices.length > 0
        ? item.devices.reduce((acc: string[], { serialnumber }) => {
            if (Object.values(equipment).length > 0) {
              const deviceName = equipment[serialnumber];

              if (deviceName) {
                return [...acc, deviceName];
              }

              return acc;
            }

            return acc;
          }, [])
        : '';

    const objects = formatObjects(item.objects, objectsList);

    const typesList = item.types[0] ? getTypesList(item.types[0].id, type) : [];

    const getSla = (sla: string | null, actual_end_at: string | null) => {
      const slaDate = sla ? convertUTCDateToLocalDate(new Date(sla)) : null;
      const actualEndDate = actual_end_at
        ? convertUTCDateToLocalDate(new Date(actual_end_at))
        : null;

      return isDeadlineExpired(slaDate, actualEndDate);
    };

    const ticketType = getTicketType(item.types[0]?.id, type);

    const invoice = invoices[item.id];
    const isWaitingPayment =
      Array.isArray(invoice) && invoice.length > 0
        ? invoice.some((i) => i.status === 'waiting-for-payment')
        : false;
    const isPaid =
      Array.isArray(invoice) && invoice.length > 0
        ? invoice.every((i) => i.status === 'paid')
        : false;
    const paymentStatus =
      Array.isArray(invoice) && invoice.length > 0
        ? isPaid
          ? 'paid'
          : isWaitingPayment
          ? 'waiting-for-payment'
          : null
        : null;
    const paymentStatusObject = invoiceStatuses.find(
      (invoiceStatus) => invoiceStatus?.id === paymentStatus
    );

    const data = {
      id: item.id,
      number: item.number,
      status: getTicketStatus(item.status, status),
      priority: getTicketInfo(item.priority, priority),
      created_at: formatDate(item.created_at),
      class: getTicketInfo(item.class, classTicket),
      type: ticketType?.title || '-',
      payment: paymentStatusObject,
      object: objects?.object || '',
      house: objects?.house || '',
      apartment: objects?.apartment || '',
      property: objects?.property || '',
      devices: devicesName,
      planned_date: item?.sla ? formatDate(item.sla) : '-',
      actual_end_at: item?.actual_end_at ? formatDate(item.actual_end_at) : '-',
      sla: getSla(item.sla, item.actual_end_at),
      initiator: (item.initiator as Initiator)?.title || '',
      assignees: formatAssignees(item.assignees, usersList),
      initiator_phone: formatPhone((item.initiator as Initiator)?.phone) || '',
      rate: item?.rate?.rate || '',
      rate_comment: item?.rate?.comment || '',
      formattedTypes: '',
      isTicketPaid: Boolean(ticketType?.paid),
      unreadMessages: item?.channel?.unread_messages || 0,
    };

    if (typesList[0]) {
      const typesCount = typesList.length - 1;

      const types = typesList.map((item: Value, index: number) => {
        return index !== typesCount ? `${item.title} -> ` : item.title;
      });

      const formattedTypes = types.join('');

      data.formattedTypes = formattedTypes;
    }

    return data;
  });
};

const columns = [
  {
    field: 'number',
    headerName: '№',
    width: 100,
    sortable: false,
  },
  {
    field: 'status',
    headerName: t('Label.status'),
    width: 140,
    sortable: false,
  },
  {
    field: 'unreadMessages',
    headerName: t('UnreadMessages'),
    width: 210,
    sortable: false,
  },
  {
    field: 'priority',
    headerName: t('Priority'),
    width: 140,
    sortable: false,
  },
  {
    field: 'created_at',
    headerName: t('DateOfCreation'),
    width: 160,
    sortable: false,
  },
  {
    field: 'class',
    headerName: t('RequestClass'),
    width: 140,
    sortable: false,
  },
  {
    field: 'type',
    headerName: t('RequestType'),
    width: 330,
    sortable: false,
  },
  {
    field: 'payment',
    headerName: t('Payment'),
    width: 130,
    sortable: false,
  },
  {
    field: 'object',
    headerName: t('AnObject'),
    width: 200,
    sortable: false,
  },
  {
    field: 'house',
    headerName: t('building'),
    width: 300,
    sortable: false,
  },
  {
    field: 'apartment',
    headerName: t('Label.flat'),
    width: 300,
    sortable: false,
  },
  {
    field: 'property',
    headerName: t('room'),
    width: 200,
    sortable: false,
  },
  {
    field: 'devices',
    headerName: t('table.equipment'),
    width: 230,
    sortable: false,
  },
  {
    field: 'planned_date',
    headerName: t('NormativeDeadline'),
    width: 250,
    sortable: false,
  },
  {
    field: 'actual_end_at',
    headerName: t('ActualDeadline'),
    width: 250,
    sortable: false,
  },
  {
    field: 'sla',
    headerName: t('SLAStandard'),
    width: 130,
    sortable: false,
  },
  {
    field: 'assignees',
    headerName: t('Performer'),
    width: 250,
    sortable: false,
  },
  {
    field: 'initiator',
    headerName: t('Declarant'),
    width: 250,
    sortable: false,
  },
  {
    field: 'initiator_phone',
    headerName: t('DeclarantPhone'),
    width: 220,
    sortable: false,
  },
  {
    field: 'rate',
    headerName: t('Rating'),
    width: 100,
    sortable: false,
  },
  {
    field: 'rate_comment',
    headerName: t('Label.comment'),
    width: 250,
    sortable: false,
  },
];

const visibilityColumns = {
  priority: false,
  created_at: false,
  house: false,
  property: false,
  devices: false,
  planned_date: false,
  actual_end_at: false,
  sla: false,
  assignees: false,
  initiator_phone: false,
  rate: false,
  rate_comment: false,
};

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $columns,
  orderColumnsChanged,
  columnsWidthChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
} = createTableBag({
  columns,
  pageSize: 25,
  visibilityColumns,
});

export const $savedColumns = $columns.map((clmn) =>
  clmn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
export const $tableData = $tickets.map((result) =>
  Array.isArray(result.tickets) && result.tickets.length > 0 ? formatData(result) : []
);
export const $countPage = combine($rowCount, $pageSize, (rowCount, pageSize) =>
  rowCount ? Math.ceil(rowCount / pageSize) : 0
);
