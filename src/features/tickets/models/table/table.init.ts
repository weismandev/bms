import { sample } from 'effector';
import { signout } from '@features/common';
import { Columns } from '@features/data-grid';
import { filtersSubmitted } from '../filters/filters.model';
import { pageUnmounted } from '../page/page.model';
import { fxGetFromStorage } from '../user-settings/user-settings.model';
import { $currentPage, $selectRow, $savedColumns, $columns } from './table.model';

$currentPage.reset(filtersSubmitted);

$selectRow.reset([signout, pageUnmounted]);

sample({
  clock: fxGetFromStorage.done,
  source: { columns: $columns, savedColumns: $savedColumns },
  fn: ({ columns, savedColumns }) => {
    const formattedColumns = savedColumns.reduce((acc: Columns[], column) => {
      const findedColumn = columns.find((item) => item.field === column.field);

      return findedColumn
        ? [
            ...acc,
            {
              field: column.field,
              headerName: findedColumn.headerName,
              width: column.width,
              sortable: findedColumn.sortable,
            },
          ]
        : acc;
    }, []);

    const newColumns = columns.filter(
      (item) => !formattedColumns.some((element: Columns) => element.field === item.field)
    );

    newColumns.forEach((item) => {
      const index = columns.map((column) => column.field).indexOf(item.field);

      formattedColumns.splice(index, 0, item);
    });

    return formattedColumns;
  },
  target: $columns,
});
