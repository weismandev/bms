import { createEffect, createEvent, createStore } from 'effector';

import {
  TicketResponse,
  UpdateAssignessTicketPayload,
  Users,
  UsersPayload,
  UsersResponse,
} from '../../interfaces';

export const $usersInfo = createStore<string | Users[]>('');

export const getUsersInfo = createEvent<{ id: number }[]>();

export const fxUpdateAssignessTicket = createEffect<
  UpdateAssignessTicketPayload,
  TicketResponse,
  Error
>();
export const fxGetUsersInfo = createEffect<UsersPayload, UsersResponse, Error>();
