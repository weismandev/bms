import { sample } from 'effector';

import { signout } from '@features/common';

import { UserItem, Value } from '../../interfaces';
import { pageUnmounted, entityApi } from '../ticket/ticket.model';
import {
  fxUpdateAssignessTicket,
  getUsersInfo,
  fxGetUsersInfo,
  $usersInfo,
} from './ticket-assignees.model';

sample({
  clock: entityApi.update,
  fn: ({ id, assignees }) => {
    const users = Array.isArray(assignees)
      ? assignees.map((item) => ('user' in item ? item.user?.id : item.id))
      : [];

    return {
      ticket: {
        id,
      },
      users,
    };
  },
  target: fxUpdateAssignessTicket,
});

sample({
  clock: getUsersInfo,
  fn: (assignees) => {
    const ids = assignees.map((item: Value) => item.id);

    return { users: ids };
  },
  target: fxGetUsersInfo,
});

$usersInfo
  .on(fxGetUsersInfo.done, (_, { result }) => {
    if (!Array.isArray(result?.users)) {
      return '';
    }
    return result.users.map((item: UserItem) => ({
      user: {
        id: item.id,
        fullname: `${item.surname} ${item.name} ${item.patronymic}`,
      },
    }));
  })
  .reset([pageUnmounted, signout]);
