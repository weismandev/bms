import { sample } from 'effector';

import { signout } from '@features/common';

import { ExpansesMaterial, ExpensesTmc, Materials, MaterialTmc } from '../../interfaces';
import { $ticket, pageUnmounted } from '../ticket/ticket.model';
import {
  $isShowTmc,
  fxGetTicketTmc,
  getTicketTmc,
  $ticketTmc,
  setTmc,
  fxSetTmc,
} from './ticket-tmc.model';

$isShowTmc
  .on(
    fxGetTicketTmc.done,
    (_, { result: { ticket_data } }) => ticket_data && ticket_data.length > 0
  )
  .on(fxGetTicketTmc.fail, () => false)
  .reset([pageUnmounted, signout]);

sample({
  clock: getTicketTmc,
  target: fxGetTicketTmc,
});

$ticketTmc
  .on(fxGetTicketTmc.done, (_, { result }) => {
    if (!Array.isArray(result?.ticket_data)) {
      return [];
    }

    return result.ticket_data.map((data) => {
      const tmcTitle = data.object_title || t('Unknown');
      const isActCreated = data.is_act_created;

      const expenses = data.expenses.map((item: ExpensesTmc) => {
        const hours = item?.work_time ? Math.floor(item.work_time / 60 / 60) : '00';
        const h = typeof hours === 'string' ? Number.parseInt(hours, 10) : hours;

        const minutes = item?.work_time ? Math.floor(item.work_time / 60) - h * 60 : '00';
        const m = minutes.toString().length === 1 ? `0${minutes}` : minutes;

        const formatWorkTime =
          hours.toString().length === 1 ? `0${hours}:${m}` : `${hours}:${m}`;

        const materials = item.material_expenses.map((element: MaterialTmc) => ({
          material: element?.material,
          materialCount: element?.quantity || 0,
          materialUnit: element?.unit,
        }));

        return {
          workId: item?.work_type_id,
          workTitle: item?.work_type_title || t('Unknown'),
          workTypeGroupId: item?.work_type_group_id || null,
          workTypeGroupTitle: item?.work_type_group_title || null,
          workTime: formatWorkTime,
          materials,
        };
      });

      return {
        tmcTitle,
        isActCreated,
        expenses,
      };
    });
  })
  .reset([signout, pageUnmounted]);

sample({
  clock: setTmc,
  source: $ticket,
  fn: ({ id }, params) => {
    const formatedExpenses = params.tmc.map((i) =>
      i.expenses.map((item: ExpansesMaterial) => {
        const formatedMaterial =
          item.materials &&
          item.materials.map((element: Materials) => ({
            material: element.material,
            unit: element.materialUnit,
            quantity: element.materialCount,
          }));

        const time = item.workTime?.split(':');
        const timeInSeconds =
          time && Number.parseInt(time[0], 10) * 3600 + Number.parseInt(time[1], 10) * 60;

        return {
          device_name: i.tmcTitle,
          work_type_id: item.workId,
          work_type_title: item.workTitle,
          work_type_group_id: item.workTypeGroupId,
          work_type_group_title: item.workTypeGroupTitle,
          work_time: timeInSeconds,
          material_expenses: formatedMaterial,
        };
      })
    );

    return {
      ticket_id: id,
      expenses: formatedExpenses.flat(),
    };
  },
  target: fxSetTmc,
});

sample({
  clock: fxSetTmc.done,
  fn: ({ params: { ticket_id } }) => ticket_id,
  target: fxGetTicketTmc,
});
