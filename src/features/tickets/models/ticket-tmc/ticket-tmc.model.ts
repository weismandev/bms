import { createEffect, createEvent, createStore } from 'effector';

import { SetTmcPayload, TicketTmcResponse, Tmc } from '../../interfaces';

export const $ticketTmc = createStore<Tmc[]>([]);
export const $isShowTmc = createStore<boolean>(true);

export const getTicketTmc = createEvent<number>();
export const setTmc = createEvent<any>();

export const fxGetTicketTmc = createEffect<number | null, TicketTmcResponse, Error>();
export const fxSetTmc = createEffect<SetTmcPayload, TicketTmcResponse, Error>();
