import { FC, useEffect } from 'react';
import { useGate, useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';

import { ErrorMessage, FilterMainDetailLayout, Loader } from '@ui/index';
import { $pathname, HaveSectionAccess, history } from '@features/common';
import { currentPageChanged } from '@features/navbar/models';
import { ChatPanel } from '@features/socket-chats-reworked/organisms/chat-panel';

import {
  $errorTicket,
  $isErrorDialogOpenTicket,
  $isLoadingTicket,
  $ticket,
  changedErrorDialogVisibilityTicket,
  path,
  TicketGate,
} from '../models';
import { Detail } from '../organisms';

const TicketPage: FC = () => {
  const { t } = useTranslation();
  const [pathname, isErrorDialogOpen, error, isLoading, ticket] = useUnit([
    $pathname,
    $isErrorDialogOpenTicket,
    $errorTicket,
    $isLoadingTicket,
    $ticket,
  ]);

  const ticketId = Number.parseInt((pathname as string).split('/')[2], 10);

  useGate(TicketGate, { ticketId });

  useEffect(() => {
    (currentPageChanged as any)({
      title: ticket.ticket_number
        ? `${t('Request')} №${ticket.ticket_number}`
        : t('Request'),
      back: () => history.push(path),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ticket]);

  const onClose = () => changedErrorDialogVisibilityTicket(false);

  return (
    <>
      <Loader isLoading={isLoading} />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <FilterMainDetailLayout
        main={<Detail />}
        detail={
          Number(ticket.id) === Number(ticketId) ? (
            <ChatPanel
              channelId={ticket.channel}
              token={ticket.related_userdata_token}
              standalone
              hideLeaveButton
            />
          ) : null
        }
        params={{
          mainWidth: 'auto',
          detailWidth: '0.6fr',
        }}
      />
    </>
  );
};

const RestrictedTicketPage: FC = () => (
  <HaveSectionAccess>
    <TicketPage />
  </HaveSectionAccess>
);

export default RestrictedTicketPage;
