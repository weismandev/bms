import { FC } from 'react';
import { useUnit } from 'effector-react';

import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import { HaveSectionAccess } from '@features/common';

import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  PageGate,
  changedErrorDialogVisibility,
  $isDetailOpen,
  $isFilterOpen,
} from '../models';
import { Table, Detail, Filters } from '../organisms';

const TicketsPage: FC = () => {
  const [isErrorDialogOpen, error, isLoading, isDetailOpen, isFilterOpen] = useUnit([
    $isErrorDialogOpen,
    $error,
    $isLoading,
    $isDetailOpen,
    $isFilterOpen,
  ]);

  const onClose = () => changedErrorDialogVisibility(false);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{
          filterWidth: '400px',
          detailWidth: !isFilterOpen ? 'minmax(370px, 50%)' : '40%',
        }}
      />
    </>
  );
};

const RestrictedTicketsPage: FC = () => (
  <HaveSectionAccess>
    <TicketsPage />
  </HaveSectionAccess>
);

export default RestrictedTicketsPage;
