export const getCost = (priceType: string, priceLower: number, priceUpper: number) => {
  switch (priceType) {
    case 'range': {
      return (
        <span>
          <span>{`От ${priceLower} `}</span>
          <span>&#8381;</span>
          <span>{` до ${priceUpper} `}</span>
          <span>&#8381; </span>
        </span>
      );
    }
    case 'fixed': {
      return (
        <span>
          <span>Цена {priceLower} </span>
          <span>&#8381; </span>
        </span>
      );
    }
    case 'from':
      return (
        <span>
          <span>От {priceLower} </span>
          <span>&#8381; </span>
        </span>
      );
    case 'not-specified':
      return <div />;
    default: {
      return null;
    }
  }
};
