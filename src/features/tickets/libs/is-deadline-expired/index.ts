export const isDeadlineExpired = (
  sla: string | null | Date,
  actual_end_at: string | null | Date
) => {
  if (!sla) {
    return false;
  }

  if (actual_end_at) {
    return Number(new Date(actual_end_at)) > Number(new Date(sla));
  }

  return Number(new Date()) > Number(new Date(sla));
};
