import i18n from '@shared/config/i18n';

const { t } = i18n;

export const getRepeatName = (value: string) => {
  if (value === 'week') {
    return t('week');
  }

  if (value === 'year') {
    return ` ${t('Year').toLowerCase()}`;
  }

  if (value === 'month') {
    return ` ${t('month')}`;
  }

  return t('day');
};
