import { Ticket, Repeat, Invoice } from '../../interfaces';

const maxAmountValue = 1000000;

export function startDateAfterFinishDate(this: { parent: Ticket }) {
  const isDatesDefined =
    Boolean(this.parent.planned_start_at) && Boolean(this.parent.planned_end_at);
  if (
    isDatesDefined &&
    Number(new Date(this.parent.planned_start_at)) >=
      Number(new Date(this.parent.planned_end_at))
  ) {
    return false;
  }

  return true;
}

export function requiredTypeRepeat(this: { parent: Ticket }) {
  if (!this.parent.class) {
    return true;
  }

  const ticketClass =
    typeof this.parent.class === 'object' ? this.parent.class.id : this.parent.class;

  const isPlannedClass = ticketClass === 'planned';

  if (!isPlannedClass) {
    return true;
  }

  if (!this.parent.id) {
    if (!this.parent?.is_repeat_ticket) {
      return true;
    }

    if (!(this.parent.type_repeat as Repeat)?.id) {
      return false;
    }

    return true;
  }

  if (!(this.parent.type_repeat as Repeat)?.id) {
    return false;
  }

  return true;
}

export function requiredEveryRepeat(this: { parent: Ticket }) {
  if (!this.parent.class) {
    return true;
  }

  const ticketClass =
    typeof this.parent.class === 'object' ? this.parent.class.id : this.parent.class;

  const isPlannedClass = ticketClass === 'planned';

  if (!isPlannedClass) {
    return true;
  }

  if (!this.parent.id) {
    if (!this.parent?.is_repeat_ticket) {
      return true;
    }

    if (!this.parent.every_repeat) {
      return false;
    }
  }

  if (!this.parent.every_repeat) {
    return false;
  }

  return true;
}

export function startDateAfterCurrentDate(this: { parent: Ticket }) {
  const isPlannedStartDataExist = Boolean(this.parent.planned_start_at);
  const date = new Date();

  if (isPlannedStartDataExist) {
    const currentDate = new Date(date.setSeconds(0, 0));
    const plannedDate = new Date(this.parent.planned_start_at);

    if (plannedDate < currentDate) {
      return false;
    }

    return true;
  }

  return true;
}

export function requiredAmountInvoice(this: { parent: Invoice }) {
  return typeof this.parent.amount === 'number';
}

export function positiveAmountInvoice(this: { parent: Invoice }) {
  if (!this.parent.amount) {
    return true;
  }

  return Math.sign(this.parent.amount) !== -1;
}

export function maxValueAmountInvoice(this: { parent: Invoice }) {
  const { amount } = this.parent;

  if (!amount) {
    return true;
  }

  if (Math.sign(amount) === 1 && amount >= maxAmountValue) {
    return false;
  }

  return true;
}

export function requiredFileInvoice(this: { parent: Invoice }) {
  return Boolean(this.parent.file);
}

export function requiredStatusInvoice(this: { parent: Invoice }) {
  if (!this.parent.id) {
    return true;
  }

  return Boolean(this.parent.status);
}

export function requiredInitiator(this: { parent: Ticket }) {
  if (!this.parent.class) {
    return true;
  }

  const ticketClass =
    typeof this.parent.class === 'object' ? this.parent.class.id : this.parent.class;

  if (ticketClass === 'client' && !this.parent.initiator) {
    return false;
  }

  return true;
}
