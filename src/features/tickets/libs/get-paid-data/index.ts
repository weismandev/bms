import { TypesData } from '../../interfaces';

export const getPaidData = (
  ticketTtypes: TypesData[],
  formattedTypes: { [key: number]: TypesData }
) => {
  const types = [...ticketTtypes].filter((item) => item);
  const findedTypeId = types[0] ? types.reverse()[0]?.id : null;

  return findedTypeId ? formattedTypes[findedTypeId]?.paid || false : false;
};
