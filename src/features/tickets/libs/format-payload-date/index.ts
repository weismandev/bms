export const formatPayloadDate = (date: Date | string) =>
  new Date(date).toISOString().replace(/T/, ' ').replace(/\..+/, '');
