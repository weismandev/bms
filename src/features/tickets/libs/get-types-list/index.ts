import { Value, TypesData } from '../../interfaces';

const getTypeInfo = (type: number, types: TypesData[]) =>
  types.find((item: Value) => item.id === type);

// Используется в тикетах и на экране диспетчера (widget-accidents)
export const getTypesList = (currentType: number, types: TypesData[]) => {
  const iter = (type: number, acc: TypesData[]): TypesData[] => {
    const findedType: any = getTypeInfo(type, types);

    if (!findedType?.parent_id) {
      return [findedType, ...acc];
    }

    return iter(findedType.parent_id, [findedType, ...acc]);
  };

  return iter(currentType, []);
};
