export function startDateAfterFinishDateFilters(this: {
  parent: {
    created_at_from: string;
    created_at_to: string;
  };
}) {
  const isDatesDefined =
    Boolean(this.parent.created_at_from) && Boolean(this.parent.created_at_to);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.created_at_from)) >=
      Number(new Date(this.parent.created_at_to))
  ) {
    return false;
  }

  return true;
}
