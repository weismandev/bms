import { Ticket } from '../../interfaces';

export const formatObjects = (values: Ticket) => {
  const objects = [];

  if (values.object) {
    objects.push({
      id: typeof values.object === 'object' ? values.object.id : values.object,
      type: 'complex',
    });
  }

  if (values.house) {
    objects.push({
      id: typeof values.house === 'object' ? values.house.id : values.house,
      type: 'building',
    });
  }

  if (values.apartment) {
    objects.push({
      id: typeof values.apartment === 'object' ? values.apartment.id : values.apartment,
      type: 'apartment',
    });
  }

  if (values.property) {
    objects.push({
      id: typeof values.property === 'object' ? values.property.id : values.property,
      type: 'property',
    });
  }

  return objects.reverse()[0];
};
