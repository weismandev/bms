import { Works } from '../../interfaces';

export function deviceRequired(this: {
  parent: Works;
  from: { value: { works: Works[] } }[];
}) {
  if (this.from[1].value.works.length > 1 && !this.parent.device) {
    return false;
  }

  if (this.parent.device) {
    return true;
  }

  if (this.parent.works.length === 0 && this.parent.techCards.length === 0) {
    return false;
  }

  return true;
}

export function techCardsRequired(this: { parent: Works }) {
  if (this.parent.techCards.length > 0) {
    return true;
  }

  if (!this.parent.device && this.parent.works.length === 0) {
    return false;
  }

  return true;
}

export function worksRequired(this: { parent: Works }) {
  if (this.parent.works.length > 0) {
    return true;
  }

  if (!this.parent.device && this.parent.techCards.length === 0) {
    return false;
  }

  return true;
}
