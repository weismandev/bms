export const formatTime = (time: number | string) => {
  const timeInSeconds = Number.parseInt(time as string, 10) * 60;

  const hours = Math.floor(timeInSeconds / 60 / 60);
  const minutes = Math.floor(timeInSeconds / 60) - hours * 60;

  return `${hours ? `${hours} час.` : ''} ${minutes} мин.`;
};
