import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data } from '../../model';

const ObjectTypeSelect = (props) => {
  const options = useStore($data);

  return (
    <SelectControl
      options={options}
      getOptionValue={(opt) => Boolean(opt) && opt.slug}
      {...props}
    />
  );
};

const ObjectTypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ObjectTypeSelect />} onChange={onChange} {...props} />;
};

export { ObjectTypeSelect, ObjectTypeSelectField };
