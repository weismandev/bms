import { createStore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const $data = createStore([
  {
    title: t('flat'),
    slug: 'flat',
  },
  {
    title: t('PM'),
    slug: 'parking_slot',
  },
  {
    title: t('pantry'),
    slug: 'pantry',
  },
]);

export { $data };
