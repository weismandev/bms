import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { urgenciesApi } from '../api';

const fxGetUrgencies = createEffect();

const $urgencies = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$urgencies
  .on(fxGetUrgencies.done, (state, { result }) => {
    if (!result.urgencies) {
      return [];
    }

    return result.urgencies;
  })
  .reset(signout);

$error.on(fxGetUrgencies.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetUrgencies.pending, (state, result) => result).reset(signout);

fxGetUrgencies.use(urgenciesApi.get);

export { fxGetUrgencies, $urgencies, $error, $isLoading };
