import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $urgencies, $isLoading, $error, fxGetUrgencies } from '../../model';

const UrgencySelect = (props) => {
  const options = useStore($urgencies);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetUrgencies();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const UrgencySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<UrgencySelect />} onChange={onChange} {...props} />;
};

export { UrgencySelect, UrgencySelectField };
