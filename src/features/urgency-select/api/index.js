import { api } from '../../../api/api2';

const get = () => api.v1('get', 'tickets/urgencies/get-list');

export const urgenciesApi = { get };
