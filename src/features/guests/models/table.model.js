import { createStore, createEvent, split } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { formatDateWithZone } from '../../../tools/formatDateWithZone';
import { formatPhone } from '../../../tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

export const $columns = createStore([
  { name: 'id', title: '№' },
  { name: 'guest_fullname', title: t('Label.FullName') },
  { name: 'guest_phone', title: t('Label.phone') },
  { name: 'address', title: t('Where') },
  { name: 'date_start', title: t('ActualFrom') },
  { name: 'date_end', title: t('ActualTo') },
  { name: 'comment', title: t('Label.comment') },
  { name: 'brand', title: t('TransportBrand') },
  { name: 'id_number', title: t('TransportNumber') },
]);

export const tabsList = [
  {
    value: 'vehicle',
    label: t('transport'),
  },
  {
    value: 'guest',
    label: t('Guests'),
  },
];

export const changeTab = createEvent();
export const changeTabTo = split(changeTab, {
  guest: (tab) => tab === 'guest',
  vehicle: (tab) => tab === 'vehicle',
});

export const $currentTab = createStore('vehicle');

export const $tableData = $raw.map(({ data }) =>
  data.map((item) => ({
    ...item,
    guest_phone: formatPhone(item.guest_phone),
    address: item.address ? item.address.full_address : t('NotSpecifiedHe'),
    date_start: item.date_start
      ? formatDateWithZone(item.date_start, 'dd.MM.yyyy')
      : t('NotSpecifiedShe'),
    date_end: item.date_end
      ? formatDateWithZone(item.date_end, 'dd.MM.yyyy')
      : t('NotSpecifiedShe'),
    comment: item.comment ? item.comment : t('NoComment'),
    brand: item.brand ? item.brand : t('NotSpecifiedShe'),
    id_number: item.id_number ? item.id_number : t('NotSpecifiedHe'),
  }))
);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag($columns.getState(), {
  widths: [
    { columnName: 'id', width: 100 },
    { columnName: 'guest_fullname', width: 300 },
    { columnName: 'address', width: 400 },
  ],
});
