import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  building: '',
  apartment: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
