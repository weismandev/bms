import { sample } from 'effector';
import { $filters } from './filter.model';
import { pageMounted, fxGetList } from './page.model';
import { $currentTab, changeTab, $tableParams } from './table.model';
import { $settingsInitialized } from './user-settings.model';

sample({
  clock: [pageMounted, changeTab, $tableParams, $filters, $settingsInitialized],
  source: {
    type: $currentTab,
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ type, table, filters }) => {
    const payload = {
      type,
      page: table.page,
      per_page: table.per_page,
    };

    if (table.search) {
      payload.search = table.search;
    }

    if (filters.apartment) {
      payload.apartment_id = filters.apartment.id;
    }

    return payload;
  },
  target: fxGetList,
});
