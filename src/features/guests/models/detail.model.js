import { createDetailBag } from '../../../tools/factories';

export const newEntity = {
  building: '',
  apartment: '',
  id_number: '',
  guest_fullname: '',
  guest_phone: '',
  type: '',
  comment: '',
  brand: '',
  date_start: '',
  date_end: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
