import { sample, forward, attach, guard } from 'effector';
import { delay } from 'patronum';
import { $pathname, history } from '@features/common';
import i18n from '@shared/config/i18n';
import { formatDateWithZone } from '@tools/formatDateWithZone';
import { formatPhone } from '@tools/formatPhone';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import {
  $opened,
  openViaUrl,
  newEntity,
  $isDetailOpen,
  entityApi,
  $mode,
  changedDetailVisibility,
} from './detail.model';
import {
  fxGetById,
  fxDelete,
  fxCreate,
  fxUpdate,
  deleteConfirmed,
  pageUnmounted,
  $entityId,
  $path,
  fxGetList,
} from './page.model';

const { t } = i18n;

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetById.prepend(({ entityId }) => entityId),
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        pass: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$opened
  .on([fxGetById.doneData, fxCreate.doneData, fxUpdate.doneData], (state, { pass }) =>
    formatDetail(pass)
  )
  .on([fxDelete.done, fxGetById.fail], () => newEntity)
  .reset(pageUnmounted);

$mode.on([fxCreate.done, fxUpdate.done, fxGetById.done], () => 'view');

$isDetailOpen
  .off(openViaUrl)
  .on([fxDelete.done, fxGetById.fail], () => false)
  .on(fxGetById.done, () => true)
  .reset(pageUnmounted);

forward({
  from: openViaUrl,
  to: fxGetById,
});

forward({
  from: sample($opened, deleteConfirmed, (opened) => opened.id),
  to: fxDelete,
});

forward({
  from: entityApi.create,
  to: attach({ effect: fxCreate, mapParams: formatPayload }),
});

forward({
  from: entityApi.update,
  to: attach({ effect: fxUpdate, mapParams: formatPayload }),
});

function formatDetail(data) {
  const {
    comment,
    date_start,
    date_end,
    brand,
    guest_fullname,
    type,
    guest_phone,
    id_number,
    address,
    id,
  } = data;

  return {
    id,
    building: address ? address.building_id : '',
    apartment: address ? address.id : '',
    id_number: id_number ? id_number : '',
    guest_fullname: guest_fullname ? guest_fullname : '',
    guest_phone: guest_phone ? formatPhone(guest_phone) : '',
    type:
      type === 'vehicle'
        ? { id: 'vehicle', title: t('transport') }
        : { id: 'guest', title: t('Guest') },
    comment: comment ? comment : '',
    brand: brand ? brand : '',
    date_start: formatDateWithZone(date_start, 'yyyy-MM-dd'),
    date_end: formatDateWithZone(date_end, 'yyyy-MM-dd'),
  };
}

function formatPayload(payload) {
  const {
    id,
    apartment,
    id_number,
    guest_fullname,
    guest_phone,
    type,
    comment,
    brand,
    date_start,
    date_end,
  } = payload;

  let formattedPayload = {
    apartment_id: apartment
      ? typeof apartment === 'string' || typeof apartment === 'number'
        ? apartment
        : apartment.id
      : '',
    guest_fullname,
    guest_phone,
    type: type ? type.id : '',
    comment,
    date_end: new Date(date_end) / 1000,
    date_start: new Date(date_start) / 1000,
  };

  if (id) {
    formattedPayload.pass_id = id;
  }

  if (type.id === 'vehicle') {
    formattedPayload = { ...formattedPayload, id_number, brand };
  }

  return formattedPayload;
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
