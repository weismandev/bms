import { signout } from '../../common';
import { filtersSubmitted } from './filter.model';
import { fxCreate } from './page.model';
import {
  changeTab,
  changeTabTo,
  $currentTab,
  $columns,
  $currentPage,
  searchChanged,
} from './table.model';

$currentTab
  .on(changeTab, (state, tab) => tab)
  .on(fxCreate.doneData, (state, { pass }) => pass.type)
  .reset(signout);

$currentPage.on([changeTab, searchChanged, filtersSubmitted], () => 0);

$columns
  .on(changeTabTo.guest, (state, tab) =>
    state.filter((i) => i.name !== 'brand' && i.name !== 'id_number')
  )
  .reset(changeTabTo.vehicle);
