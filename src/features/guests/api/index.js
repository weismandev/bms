import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'pass/crm/get-list', payload);
const getById = (id) => api.no_vers('get', 'pass/get', { pass_id: id });
const create = (payload) => api.no_vers('post', 'pass/create', payload);
const update = (payload) => api.no_vers('post', 'pass/update', payload);
const deleteItem = (id) => api.no_vers('post', 'pass/delete', { pass_id: id });

export const guestsApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteItem,
};
