import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '../../../../ui';
import { ApartmentSelectField } from '../../../apartment-select';
import { HouseSelectField } from '../../../house-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const { t } = i18n;

const Filter = memo((props) => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values }) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="building"
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectHouse')}
                />

                <Field
                  name="apartment"
                  component={ApartmentSelectField}
                  label={t('flat')}
                  placeholder={t('selectThatApartment')}
                  helpText={t('NeedSelectHouse')}
                  building_id={values.building ? values.building.id : ''}
                />

                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
