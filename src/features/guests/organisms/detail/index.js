import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { ApartmentSelectField } from '@features/apartment-select';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { validateSomePhone } from '@ui/atoms/some-phone-masked-input';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  CustomScrollbar,
  PhoneMaskedInput,
  SomePhoneMaskedInput,
} from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
} from '../../models/detail.model';
import { deleteDialogVisibilityChanged } from '../../models/page.model';

const { t } = i18n;

const Detail = memo(() => {
  const mode = useStore($mode);
  const opened = useStore($opened);

  const isNew = !opened.id;

  function startDateAfterFinishDate() {
    const isDatesDefined =
      Boolean(this.parent.date_start) && Boolean(this.parent.date_end);

    if (isDatesDefined && this.parent.date_start > this.parent.date_end) {
      return this.createError();
    }

    return true;
  }

  const string = Yup.string();
  const mixed = Yup.mixed();

  const requiredString = string.required(t('thisIsRequiredField'));
  const mixedString = mixed.required(t('thisIsRequiredField'));

  const validationSchema = Yup.object().shape({
    type: mixedString,
    guest_fullname: string.when('type', {
      is: (value) => Boolean(value?.id) && value.id === 'guest',
      then: requiredString,
      otherwise: string,
    }),
    building: mixedString.nullable(),
    apartment: mixedString.nullable(),
    date_start: requiredString.test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    ),
    date_end: requiredString.test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    ),
    guest_phone: Yup.string()
      .required(t('thisIsRequiredField'))
      .test('phone-length', t('invalidNumber'), validateSomePhone),
    id_number: Yup.string().when('type', {
      is: (value) => Boolean(value?.id) && value.id === 'vehicle',
      then: requiredString,
    }),
    brand: string.when('type', {
      is: (value) => value.id === 'vehicle',
      then: string,
    }),
  });

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm }) => (
          <Form style={{ height: '100%', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onDelete={() => deleteDialogVisibilityChanged(true)}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm();
                }
              }}
            />
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    component={SelectField}
                    label={t('TypeGuestPass')}
                    placeholder={t('chooseType')}
                    options={[
                      { id: 'guest', title: t('Guest') },
                      { id: 'vehicle', title: t('transport') },
                    ]}
                    name="type"
                    mode={opened.id ? 'view' : mode}
                    required
                  />
                  <Field
                    component={InputField}
                    label={t('NameGuest')}
                    name="guest_fullname"
                    mode={mode}
                    required={Boolean(values.type?.id) && values.type.id === 'guest'}
                  />
                  <Field
                    component={HouseSelectField}
                    label={t('building')}
                    placeholder={t('selectHouse')}
                    name="building"
                    mode={mode}
                    required
                  />
                  <Field
                    component={ApartmentSelectField}
                    label={t('flat')}
                    placeholder={t('selectThatApartment')}
                    name="apartment"
                    building_id={
                      values.building
                        ? typeof values.building === 'string' ||
                          typeof values.building === 'number'
                          ? values.building
                          : values.building.id
                        : ''
                    }
                    mode={mode}
                    disabled={mode === 'edit' && !values.building}
                    required
                  />
                  <Field
                    component={InputField}
                    type="date"
                    name="date_start"
                    mode={mode}
                    label={t('StartDatePass')}
                    required
                  />
                  <Field
                    component={InputField}
                    type="date"
                    name="date_end"
                    mode={mode}
                    label={t('EndDatePass')}
                    required
                  />
                  {values.type && values.type.id === 'vehicle' && (
                    <>
                      <Field
                        label={t('TransportBrand')}
                        placeholder={t('EnterTransportBrand')}
                        component={InputField}
                        name="brand"
                        mode={mode}
                      />
                      <Field
                        name="id_number"
                        label={t('GovNumber')}
                        placeholder="A000AA000"
                        helpText={t('FormatGovNumber')}
                        mode={mode}
                        component={InputField}
                        required
                      />
                    </>
                  )}
                  <Field
                    name="guest_phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        inputComponent={SomePhoneMaskedInput}
                        label={t('Label.phone')}
                        placeholder={t('Label.phone')}
                        mode={mode}
                        required
                        inputProps={{ form }}
                      />
                    )}
                  />
                  <Field
                    label={t('Label.comment')}
                    placeholder={t('EnterComment')}
                    component={InputField}
                    name="comment"
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
});

export { Detail };
