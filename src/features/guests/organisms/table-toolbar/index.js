import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import {
  FilterButton,
  AddButton,
  Greedy,
  SearchInput,
  Toolbar,
  FoundInfo,
  Tabs,
} from '../../../../ui';
import { addClicked, $mode } from '../../models/detail.model';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';
import {
  searchChanged,
  $search,
  tabsList,
  $currentTab,
  changeTab,
} from '../../models/table.model';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
});

function TableToolbar(props) {
  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const searchValue = useStore($search);
  const mode = useStore($mode);
  const currentTab = useStore($currentTab);

  const isEditMode = mode === 'edit';
  const classes = useStyles();

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <FoundInfo count={totalCount} className={classes.margin} />
      <SearchInput
        onChange={(e) => searchChanged(e.target.value)}
        value={searchValue}
        handleClear={() => searchChanged('')}
        className={classes.margin}
      />
      <Tabs
        tabEl={<Tab style={{ minWidth: 20, minHeight: 32 }} />}
        currentTab={currentTab}
        options={tabsList}
        onChange={(e, value) => changeTab(value)}
        scrollButtons={false}
      />
      <Greedy />
      <AddButton onClick={addClicked} className={classes.margin} disabled={isEditMode} />
    </Toolbar>
  );
}

export { TableToolbar };
