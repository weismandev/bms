import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '../../../ui';

import { Detail, Filter, Table } from '../organisms';

import '../models';
import {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  $path,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models/page.model';

import { $isFilterOpen } from '../models/filter.model';
import { $isDetailOpen } from '../models/detail.model';

const GuestsPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const error = useStore($error);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedGuestsPage = (props) => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <GuestsPage />
    </HaveSectionAccess>
  );
};

export { RestrictedGuestsPage as GuestsPage };
