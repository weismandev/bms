import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'stuff-pass/crm/car-pass/types', payload);

export { getList };
