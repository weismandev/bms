import { Paper } from '@mui/material';
import styled from '@emotion/styled';

export const PlaygroundContainer = styled(Paper)({
  display: 'grid',
  alignContent: 'start',
  alignItems: 'start',
  gridTemplateColumns: '1fr 1fr',
  gridTemplateRows: '1fr',
  margin: 12,
  padding: 12,
  background: 'white',
  gap: 12,
  height: 'calc(100vh - 100px)',
  overflow: 'scroll',
});

export const PlaygroundFields = styled.div(() => ({
  display: 'grid',
  alignContent: 'start',
  height: '90%',
  gap: 15,
  paddingRight: 10,
  overflow: 'scroll',
}));

export const PlaygroundPanel = styled.div(() => ({
  display: 'grid',
  alignItems: 'start',
  alignContent: 'start',
  gap: 15,
  borderLeft: '1px solid lightgrey',
  paddingLeft: 15,
  overflow: 'scroll',
}));

export const PlaygroundFormControls = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  gap: 5,
  overflow: 'scroll',
}));

export const PlaygroundStats = styled.ul(() => ({
  display: 'grid',
  gap: 10,
  overflow: 'scroll',
}));

export const PlaygroundStatsItem = styled.li(() => ({
  display: 'grid',
  fontSize: 16,
  fontWeight: 500,
  overflow: 'scroll',
}));
