import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { Control, Form } from '@effector-form';
import { Button, Typography } from '@mui/material';
import { ThemeAdapter } from '@shared/theme/adapter';
import { FormGate, testEvent, testForm } from './model';
import {
  PlaygroundContainer,
  PlaygroundFields,
  PlaygroundFormControls,
  PlaygroundPanel,
  PlaygroundStats,
  PlaygroundStatsItem,
} from './styled';

const Playground: FC = () => {
  useGate(FormGate);

  const { values, dirty, editable, valid, submittable, submit, reset } =
    useUnit(testForm);

  return (
    <ThemeAdapter>
      <PlaygroundContainer>
        <PlaygroundFields>
            <Typography variant="h5" gutterBottom>
              ☄️Effector Form
            </Typography>

            <Control.Text form={testForm} name="text" label="Text" required />

            <Control.Textarea form={testForm} name="textarea" label="Textarea" />
            <Control.Number form={testForm} name="number" label="Number" />

            <Control.Complex  form={testForm}name="complex" label="Complex" />
            <Control.Complexes form={testForm} name="complexes" label="Complexes" />
            <Control.Building form={testForm} name="building" label="Building" complex={values.complex} />
            <Control.Buildings
              form={testForm}
              name="buildings"
              label="Buildings"
              complex={values.complex}
            />
            {/*<Control.Apartment*/}
            {/*  name="apartment"*/}
            {/*  label="Apartment"*/}
            {/*  complex={values.complex}*/}
            {/*  building={values.building}*/}
            {/*/>*/}

            <Control.Date  form={testForm}name="date" label="Date" />
            <Control.DateTime  form={testForm}name="dateTime" label="DateTime" />
            <Control.DateRange  form={testForm}name="dateRange" label="DateRange" />
            <Control.DateTimeRange form={testForm} name="dateTimeRange" label="DateTimeRange" />

            <Control.Time  form={testForm}name="time" label="Time" />
            <Control.TimeRange  form={testForm}name="timeRange" label="TimeRange" />

            <Control.Email  form={testForm}name="email" label="Email" />
            <Control.Phone form={testForm} name="phone" label="Phone" />
            <Control.Password  form={testForm}name="password" label="Password" disabled />

            <Control.Switch form={testForm} name="switch" label="Switch" />
            <Control.Checkbox form={testForm} name="checkbox" label="Checkbox" />

            <Control.TicketClass form={testForm} name="ticketClass" label="TicketClass" />
            <Control.TicketOrigin  form={testForm}name="ticketOrigin" label="TicketOrigin" />
            <Control.TicketPriority  form={testForm}name="ticketPriority" label="TicketPriority" />
            <Control.TicketStatus  form={testForm}name="ticketStatus" label="TicketStatus" />
            <Control.TicketType form={testForm} name="ticketType" label="TicketType" />
        </PlaygroundFields>

        <PlaygroundPanel>
          <PlaygroundFormControls>
            <Button onClick={submit} disabled={!submittable} variant="contained">
              Сохранить
            </Button>
            {editable ? (
              <Button variant="contained" onClick={() => testForm.setEditable(false)}>
                Просмотр
              </Button>
            ) : (
              <Button variant="contained" onClick={() => testForm.setEditable(true)}>
                Редактировать
              </Button>
            )}

            <Button variant="contained" onClick={reset}>
              Сбросить
            </Button>

            <Button variant="contained" onClick={() => testEvent()}>
              Test button
            </Button>
          </PlaygroundFormControls>

          <PlaygroundStats>
            <PlaygroundStatsItem>{`isEditable: ${editable.toString()}`}</PlaygroundStatsItem>
            <PlaygroundStatsItem>{`isValid: ${valid.toString()}`}</PlaygroundStatsItem>
            <PlaygroundStatsItem>{`isDirty: ${dirty.toString()}`}</PlaygroundStatsItem>
            <PlaygroundStatsItem>{`isSubmittable: ${submittable.toString()}`}</PlaygroundStatsItem>
          </PlaygroundStats>
        </PlaygroundPanel>
      </PlaygroundContainer>
    </ThemeAdapter>
  );
};

export default Playground;
