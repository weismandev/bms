import { createEvent, sample } from 'effector';
import { createGate } from 'effector-react';
import { createForm } from '@effector-form';
import moment from 'moment';
import * as Yup from 'yup';

export const FormGate = createGate();

export const testEvent = createEvent();

export const testForm = createForm<any>({
  initialValues: {
    text: 'Крутое название',
    textarea: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    number: 500,
    complex: 26,
    complexes: [26],
    building: 721,
    buildings: [721],
    apartment: 21882,
    date: moment('08.04.2023', 'DD.MM.YYYY'),
    dateTime: moment('2023-04-09T12:21:02+0000'),
    dateRange: [moment('08.04.2023', 'DD.MM.YYYY'), moment('2023-04-09T12:21:02+0000')],
    dateTimeRange: [
      moment('08.04.2023', 'DD.MM.YYYY'),
      moment('2023-04-09T12:21:02+0000'),
    ],
    time: moment('2023-04-09T12:21:02+0000'),
    timeRange: [moment('2023-04-09T10:20:02+0000'), moment('2023-04-09T12:20:02+0000')],
    email: 'test@mail.ru',
    phone: '+71234567891',
    password: '12345',
    checkbox: false,
    switch: true,
    group: {
      city: 'Moscow',
      street: 'Lenin st.',
      complex: 'Complex value',
      building: 'Building value',
    },
    array: [
      {
        bookTitle: 'Nice book',
        authors: [{ authorFirstName: 'John' }, { authorFirstName: 'Mike' }],
      },
      {
        bookTitle: 'Good book',
        authors: [{ authorFirstName: 'Drake' }, { authorFirstName: 'Josh' }],
      },
    ],
    ticketClass: 'client',
    ticketOrigin: 'social',
    ticketPriority: 'high',
    ticketStatus: 'work',
    ticketType: [6639, 6622, 6641],
  },
  validationSchema: Yup.object().shape({
    text: Yup.string().strict().max(15).required(),
  }),
  debug: true,
  onSubmit: testEvent,
});

sample({
  clock: testEvent,
  fn: () => {
    alert('submit');
  },
});
