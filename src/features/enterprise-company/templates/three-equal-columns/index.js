import classnames from 'classnames';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme) => {
  return {
    container: {
      display: 'grid',
      gridTemplateColumns: '521px 1fr 1fr',
      gridTemplateRows: '1fr',
      height: '100%',
      backgroundColor: '#f4f7fa',
      gridGap: '24px',
      padding: '24px',
      paddingTop: '12px',
    },
    columnCommon: {
      gridRow: '1 / 2',
      position: 'relative',
    },
    firstColumn: {
      gridColumn: '1 / 2',
    },
    secondColumn: {
      gridColumn: '2 / 3',
    },
    thirdColumn: {
      gridColumn: '3 / 4',
    },
  };
});

const ThreeEqualColumnsLayout = (props) => {
  const { first = <div />, second = <div />, third = <div /> } = props;

  const { container, firstColumn, secondColumn, thirdColumn } = useStyles();

  return (
    <div className={container}>
      <section className={classnames(firstColumn)}>{first}</section>
      <section className={classnames(secondColumn)}>{second}</section>
      <section className={classnames(thirdColumn)}>{third}</section>
    </div>
  );
};

export { ThreeEqualColumnsLayout };
