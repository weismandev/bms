import { useStore } from 'effector-react';
import { Toolbar, AddButton } from '../../../../ui';
import { CompanySearch } from '../../../companies-search';
import { $mode, addCompany, $opened } from '../../models/detail.model';

const ListToolbar = () => {
  const mode = useStore($mode);
  const opened = useStore($opened);

  return (
    <Toolbar
      style={{
        height: 82,
        padding: '0 24px',
        display: 'flex',
        justifyContent: 'flex-end',
      }}
    >
      {mode === 'edit' && !opened.id ? <CompanySearch /> : null}

      <AddButton
        style={{ marginLeft: '10px' }}
        onClick={() => addCompany()}
        disabled={mode === 'edit'}
      />
    </Toolbar>
  );
};

export { ListToolbar };
