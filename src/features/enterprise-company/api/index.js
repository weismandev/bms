import { api } from '../../../api/api2';

const jsonHeader = { headers: { 'Content-Type': 'application/json' } };

const getCompanyList = () => api.v1('get', 'enterprises/my');

const updateCompanyInfo = (payload = {}) =>
  api.v1('post', 'enterprises/update', payload, jsonHeader);

const setDirector = (payload = {}) =>
  api.v1('post', 'enterprises/set-director', payload, jsonHeader);

const setResponsible = (payload = {}) =>
  api.v1('post', 'enterprises/set-responsible', payload, jsonHeader);

const createCompany = (payload = {}) =>
  api.v1('post', 'enterprises/create', payload, jsonHeader);

const getEnterprisesRenters = (payload = {}) =>
  api.v1('get', 'enterprises/crm/resident/get-enterprises-renters', payload);

const setOrCreateEnterpriseRenter = (payload = {}) =>
  api.v1('post', 'enterprises/crm/resident/create-renter', payload, jsonHeader);

const getFlatDepartmentsTree = (payload = {}) =>
  api.v1('get', 'enterprises/crm/resident/enterprise-department/list-flat-tree', payload);

const saveDepartmentsBatch = (payload = {}) =>
  api.v1(
    'post',
    'enterprises/crm/resident/enterprise-department/batch-save',
    payload,
    jsonHeader
  );

const deleteDepartments = (departmensIds = []) => {
  if (departmensIds.length) {
    return Promise.all(
      departmensIds.map((id) =>
        api.v1(
          'post',
          'enterprises/crm/resident/enterprise-department/delete',
          { id },
          jsonHeader
        )
      )
    );
  }

  return Promise.resolve();
};

const getEmployeesList = (payload) =>
  api.v1('get', 'employee/get-enterprise-employees', payload, jsonHeader);

export const enterpriseApi = {
  getCompanyList,
  updateCompanyInfo,
  setDirector,
  setResponsible,
  createCompany,
  getEnterprisesRenters,
  setOrCreateEnterpriseRenter,
  getFlatDepartmentsTree,
  saveDepartmentsBatch,
  deleteDepartments,
  getEmployeesList,
};
