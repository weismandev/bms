import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Done, Close } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  FormSectionHeader,
  SwitchField,
  LogoControlField,
  ItemStatus,
  CustomScrollbar,
  EmployeeCard,
  PhoneMaskedInput,
  SelectField,
} from '@ui/index';
import { BuildingSelectField } from '../../../building-select';
import {
  $opened,
  $employees,
  $mode,
  detailSubmitted,
  changeMode,
  detailCancel,
} from '../../models/detail.model';

const { t } = i18n;

const validationSchema = (bool) => {
  const schema = {
    title: Yup.string().required(`${t('PleaseEnterCompanyName')}.`),
    inn: Yup.string()
      .required(`${t('PleaseEnterYourTIN')}.`)
      .min(10, `${t('AtLeast10Characters')}.`)
      .max(12, `${t('AtLeast10Characters')}.`)
      .matches(/^\d{10,12}$/, {
        message: `${t('UseNumbersFrom0to9')}.`,
      }),
    okved: Yup.string().matches(
      /^(\d{1,3}(\.\d{1,3})+)(\s*,\s*(\d{1,3}(\.\d{1,3})+))*$/,
      {
        message: `${t('InvalidRCEAPformat')}.`,
      }
    ),
    ogrn: Yup.string()
      .required(`${t('PleaseEnterYourBIN')}.`)
      .matches(/^\d{13}$/, {
        message: `${t('Contains13Digits')}.`,
      }),
    kpp: Yup.string().matches(/^\d{9}$/, `${t('Contains9Digits')}.`),
  };

  return bool
    ? Yup.object().shape({
        ...schema,
        building_id: Yup.mixed().required(`${t('PleaseSelectFromTheList')}.`),
      })
    : Yup.object().shape(schema);
};

const Detail = () => {
  const opened = useStore($opened);
  const employees = useStore($employees);
  const mode = useStore($mode);
  const isEditMode = mode === 'edit';
  const isNew = !opened.id;

  const renderManagementFields = (data) => {
    if (!data.director && !data.responsible) return null;

    return !isEditMode ? (
      <>
        <EmployeeCard position={t('Director')} employee={data.director} />

        <EmployeeCard position={t('Responsible')} employee={data.responsible} />
      </>
    ) : (
      <>
        <Field
          name="director"
          component={SelectField}
          label={t('Director')}
          labelPlacement="top"
          placeholder={t('ChooseDirector')}
          options={employees}
          mode="view"
          divider
        />
        <Field
          name="responsible"
          component={SelectField}
          label={t('ResponsiblePerson')}
          labelPlacement="top"
          placeholder={t('ChooseResponsiblePerson')}
          options={employees}
          mode={mode}
          divider
        />
      </>
    );
  };

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        validationSchema={validationSchema(isNew)}
        enableReinitialize
        render={({ values, errors, resetForm }) => {
          return (
            <Form style={{ height: 'inherit' }}>
              <DetailToolbar
                hasError={Object.keys(errors).length > 0}
                mode={mode}
                hidden={{ delete: true, close: true }}
                style={{ padding: 24 }}
                onSave={() => detailSubmitted(values)}
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  detailCancel(values.id);
                  resetForm();
                }}
              />
              <div
                style={{
                  height: 'calc(100% - 82px)',
                  padding: '0 12px 24px 24px',
                }}
              >
                <CustomScrollbar trackVerticalStyle={{ right: 0 }}>
                  <div style={{ paddingRight: 12 }}>
                    {isEditMode ? (
                      <Field
                        name="title"
                        label={t('full_name')}
                        component={InputField}
                        placeholder={t('EnterName')}
                      />
                    ) : (
                      <div
                        style={{
                          display: 'flex',
                          marginBottom: 30,
                          justifyContent: 'space-between',
                        }}
                      >
                        <FormSectionHeader
                          header={values.title}
                          style={{ marginBottom: 0 }}
                        />
                        <ItemStatus is_verified={opened.is_verified} />
                      </div>
                    )}
                    {renderManagementFields(opened)}
                    <Field
                      name="short_title"
                      label={t('shortName')}
                      component={InputField}
                      placeholder={t('EnterName')}
                      mode={mode}
                    />

                    <Field
                      name="phone"
                      render={({ field, form }) => (
                        <InputField
                          mode={mode}
                          field={field}
                          form={form}
                          inputComponent={PhoneMaskedInput}
                          label={t('Label.phone')}
                          placeholder={t('EnterPhone')}
                        />
                      )}
                    />

                    <Field
                      name="contact_phone"
                      render={({ field, form }) => (
                        <InputField
                          mode={mode}
                          field={field}
                          form={form}
                          inputComponent={PhoneMaskedInput}
                          label={t('ContactPhoneNumber')}
                          placeholder={t('EnterPhone')}
                        />
                      )}
                    />

                    <Field
                      name="logo"
                      component={LogoControlField}
                      mode={mode}
                      label={t('CompanyLogo')}
                    />

                    {mode === 'edit' && !opened.id ? (
                      <Field
                        component={BuildingSelectField}
                        name="building_id"
                        label={t('selectBbuilding')}
                      />
                    ) : null}

                    <Field
                      name="actual_address"
                      label={t('CompanyAddress')}
                      placeholder={t('ChooseAddress')}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="ogrn"
                      label={t('BIN')}
                      labelPlacement="start"
                      placeholder={t('EnterYourBIN')}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="inn"
                      label={t('TIN')}
                      labelPlacement="start"
                      placeholder={t('EnterYourTIN')}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="okved"
                      label={t('RCEAP')}
                      labelPlacement="start"
                      placeholder={t('EnterYourRCEAP')}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="kpp"
                      label={t('CAT')}
                      labelPlacement="start"
                      placeholder={t('EnterYourCAT')}
                      mode={mode}
                      component={InputField}
                    />

                    <Field
                      name="pass_confirmation_required"
                      label={t('RequiresConfirmationGuestPassesOrderedEmployees')}
                      labelPlacement="start"
                      component={SwitchField}
                      mode={mode}
                      viewCheckedIcon={<Done style={{ color: '#1BB169' }} />}
                      viewUncheckedIcon={<Close style={{ color: '#EB5757' }} />}
                      divider
                    />
                    <Field
                      name="guest_pass_confirmation_required"
                      label={t('RequiresConfirmationGuestPassesOrderedFrontDesk')}
                      labelPlacement="start"
                      component={SwitchField}
                      mode={mode}
                      viewCheckedIcon={<Done style={{ color: '#1BB169' }} />}
                      viewUncheckedIcon={<Close style={{ color: '#EB5757' }} />}
                      divider
                    />
                    <Field
                      name="is_guest_pass_sms_auto_sending_enabled"
                      component={SwitchField}
                      labelPlacement="start"
                      label={t('AutomaticSendingSMSInvitationsWhenCreatingGuestPass')}
                      disabled={opened.sms_pass_mode === 'prohibited'}
                      mode={mode}
                      viewCheckedIcon={<Done style={{ color: '#1BB169' }} />}
                      viewUncheckedIcon={<Close style={{ color: '#EB5757' }} />}
                    />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

export { Detail };
