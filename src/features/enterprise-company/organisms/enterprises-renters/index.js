import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore, useGate } from 'effector-react';
import { IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import { AddButton, Modal, Loader } from '../../../../ui';
import {
  EnterpriseRentersGate,
  $isLoading,
  $isFormModalOpen,
  add,
  $opened,
  $mode,
  changeFormModalVisibility,
} from '../../models/enterprises-renters.model';
import { EnterprisesRentersForm } from '../enterprises-renters-form';
import { EnterprisesRentersList } from '../enterprises-renters-list';

const $stores = combine({
  isLoading: $isLoading,
  isFormModalOpen: $isFormModalOpen,
  opened: $opened,
  mode: $mode,
});

const EnterprisesRenters = (props) => {
  useGate(EnterpriseRentersGate);
  const { isFormModalOpen, opened, mode, isLoading } = useStore($stores);
  const { tabsbar } = props;
  const { t } = useTranslation();

  const isViewMode = mode === 'view';

  const list = (
    <div style={{ position: 'relative', height: 'calc(100% - 84px)' }}>
      <EnterprisesRentersList />
    </div>
  );

  const form = (
    <div style={{ height: 500, position: 'relative' }}>
      <EnterprisesRentersForm />
    </div>
  );

  const header = (
    <span
      style={{
        height: isViewMode ? 82 : 'auto',
        display: isViewMode ? 'block' : 'inline',
      }}
    >
      {opened.id ? opened.title : t('CreateAssociatedTenant')}
      <IconButton
        style={{ position: 'absolute', top: 12, right: 12 }}
        onClick={() => changeFormModalVisibility(false)}
        size="large"
      >
        <Close />
      </IconButton>
    </span>
  );

  return (
    <>
      <Loader isLoading={isLoading} />
      <div
        style={{
          padding: 24,
          height: 84,
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
        }}
      >
        {tabsbar}
        <AddButton onClick={add} />
      </div>
      <Modal isOpen={isFormModalOpen} header={header} content={form} />
      {list}
    </>
  );
};

export { EnterprisesRenters };
