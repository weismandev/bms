import { useStore, useList } from 'effector-react';
import { List as MuiList, ListItem, Typography, Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, Divider, ItemStatus, CustomScrollbar } from '../../../../ui';
import { $dadata, specify } from '../../../companies-search';
import { open, $opened, $mode } from '../../models/detail.model';
import { $list } from '../../models/list.model';
import { ListToolbar } from '../../molecules';
import { CompanyCard } from '../company-card';

const useStyles = makeStyles((theme) => {
  const acentItemStyles = {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  };

  return {
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      height: 72,
      padding: '24px 24px 15px 24px',
      '&:hover': acentItemStyles,
    },
    selected: acentItemStyles,
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },
    headerText: {
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
    },
  };
});

const ItemHeader = (props) => {
  const { header } = props;
  const { headerText } = useStyles();

  return <Typography className={'headerText ' + headerText}>{header}</Typography>;
};

const Item = (props) => {
  const { children, ...rest } = props;
  const { item, divider, selected } = useStyles();
  return (
    <>
      <ListItem classes={{ root: item, selected }} button component="li" {...rest}>
        {children}
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const List = (props) => {
  const opened = useStore($opened);
  const mode = useStore($mode);

  const list = useList($list, {
    keys: [opened],
    fn: ({ id, title, is_verified }) => {
      const selectItem = () => open(id);

      return (
        <Item selected={String(opened.id) === String(id)} onClick={selectItem}>
          <Grid
            container
            justifyContent="space-between"
            wrap="nowrap"
            alignItems="center"
          >
            <Grid item style={{ width: '60%' }}>
              <ItemHeader header={title} />
            </Grid>
            <Grid item style={{ width: '40%' }}>
              <ItemStatus is_verified={is_verified} />
            </Grid>
          </Grid>
        </Item>
      );
    },
  });

  const dadata = useList($dadata, {
    keys: [opened],
    fn: (item) => {
      const { number } = item;
      const selectItem = () => specify(number);

      return (
        <CompanyCard
          selected={String(opened.number) === String(number)}
          onClick={selectItem}
          item={item}
        />
      );
    },
  });

  return (
    <Wrapper style={{ height: '100%' }}>
      <ListToolbar />
      <CustomScrollbar style={{ height: 'calc(100% - 82px)' }} autoHide>
        <MuiList disablePadding>{mode === 'edit' && !opened.id ? dadata : list}</MuiList>
      </CustomScrollbar>
    </Wrapper>
  );
};

export { List };
