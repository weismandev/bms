import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import {
  DetailToolbar,
  InputField,
  LogoControlField,
  ErrorMsg as ErrorMessage,
  CustomScrollbar,
  PhoneMaskedInput,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import { MyEnterprisesWithFreePropertiesToRentSelectField } from '../../../my-enterprises-with-free-properties-to-rent-select';
import { OwnedByEnterprisePropertiesSelectField } from '../../../owned-by-enterprise-properties-select';
import {
  enterpriseRenterSubmitted,
  $error,
  $opened,
  $mode,
  changeFormModalVisibility,
} from '../../models/enterprises-renters.model';

const $stores = combine({
  error: $error,
  opened: $opened,
  mode: $mode,
});

const EnterprisesRentersForm = () => {
  const { error, opened, mode } = useStore($stores);
  const { t } = useTranslation();
  const isViewMode = mode === 'view';

  return (
    <Formik
      initialValues={opened}
      onSubmit={enterpriseRenterSubmitted}
      render={({ values }) => (
        <>
          <ErrorMessage error={error} />
          <Form style={{ height: '100%' }}>
            {isViewMode ? null : (
              <DetailToolbar
                onCancel={() => changeFormModalVisibility(false)}
                hidden={{
                  edit: true,
                  delete: true,
                  close: true,
                }}
                style={{ padding: 24, height: 82 }}
                mode={mode}
              />
            )}
            <div
              style={{
                height: isViewMode ? '100%' : 'calc(100% - 82px)',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 12 }}>
                  <Field
                    component={LogoControlField}
                    name="logo"
                    label={t('CompanyLogo')}
                    mode={mode}
                  />

                  <Field
                    name="first_name"
                    component={InputField}
                    label={t('DirectorNname')}
                    placeholder={t('EnterYourName')}
                    mode={mode}
                  />

                  <Field
                    name="last_name"
                    component={InputField}
                    label={t('SurnameDirector')}
                    placeholder={t('EnterLastName')}
                    mode={mode}
                  />

                  <Field
                    name="patronymic"
                    component={InputField}
                    label={t('MiddleNameDirector')}
                    placeholder={t('EnterMiddleName')}
                    mode={mode}
                  />

                  <Field
                    name="title"
                    component={InputField}
                    label={t('CompanyName')}
                    placeholder={t('EnterCompanyName')}
                    mode={mode}
                  />

                  <Field
                    name="phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        inputComponent={PhoneMaskedInput}
                        label={t('Label.phone')}
                        placeholder={t('EnterPhone')}
                        mode={mode}
                      />
                    )}
                  />

                  <Field
                    name="inn"
                    component={InputField}
                    label={t('TIN')}
                    placeholder={t('EnterYourTIN')}
                    mode={mode}
                  />

                  <Field
                    name="ogrn"
                    component={InputField}
                    label={t('BIN')}
                    placeholder={t('EnterYourBIN')}
                    mode={mode}
                  />

                  <Field
                    name="okved"
                    component={InputField}
                    label={t('RCEAP')}
                    placeholder={t('EnterYourRCEAP')}
                    mode={mode}
                  />

                  <Field
                    name="kpp"
                    component={InputField}
                    label={t('CAT')}
                    placeholder={t('EnterYourCAT')}
                    mode={mode}
                  />

                  <Field
                    name="enterprise"
                    component={MyEnterprisesWithFreePropertiesToRentSelectField}
                    label={t('TheCompanyOwnerPremises')}
                    placeholder={t('ChooseCompany')}
                    mode={mode}
                  />

                  <Field
                    name="enterprise_property"
                    component={OwnedByEnterprisePropertiesSelectField}
                    enterprise_id={values.enterprise?.id || null}
                    helpText={t('ChooseCompanyFirst')}
                    label={t('CompanyOwnership')}
                    placeholder={t('ChooseProperty')}
                    mode={mode}
                  />

                  <Field
                    name="start_date"
                    component={InputField}
                    type="date"
                    label={t('RentalStartDate')}
                    placeholder={t('EnterStartDate')}
                    mode={mode}
                  />

                  <Field
                    name="finish_date"
                    component={InputField}
                    type="date"
                    label={t('EndDateLease')}
                    placeholder={t('EnterEndDate')}
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        </>
      )}
    />
  );
};

export { EnterprisesRentersForm };
