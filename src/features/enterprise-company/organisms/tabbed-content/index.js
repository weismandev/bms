import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Wrapper, Tabs } from '../../../../ui';
import { tabsList, changeTab, $currentTab } from '../../models/detail.model';
import { Departments } from '../departments';
import { EnterprisesRenters } from '../enterprises-renters';

const $stores = combine({ currentTab: $currentTab });

const TabbedContent = (props) => {
  const { currentTab } = useStore($stores);

  const tabsbar = (
    <Tabs
      style={{ margin: '0 auto' }}
      options={tabsList}
      currentTab={currentTab}
      variant="fullWidth"
      onChange={(e, tab) => changeTab(tab)}
      tabEl={<Tab style={{ minWidth: '50%' }} />}
    />
  );

  return (
    <Wrapper style={{ height: '100%', position: 'relative' }}>
      {currentTab === 'departments' ? (
        <Departments tabsbar={tabsbar} />
      ) : (
        <EnterprisesRenters tabsbar={tabsbar} />
      )}
    </Wrapper>
  );
};

export { TabbedContent };
