import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, FieldArray, Field } from 'formik';
import { nanoid } from 'nanoid';
import { Button, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { InputField, SaveButton, CancelButton, CustomScrollbar } from '../../../../ui';
import {
  $childDepartments,
  changeMode,
  submitDepartments,
} from '../../models/departments.model';

const useStyles = makeStyles({
  button: {
    margin: '0 5px',
  },
  toolbar: {
    padding: '24px 0',
    height: 84,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
});

export const DepartmentsForm = (props) => {
  const { tabsbar } = props;
  const departments = useStore($childDepartments);
  const { button, toolbar } = useStyles();
  const initialValues = { departments };
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onReset={() => changeMode('view')}
      onSubmit={submitDepartments}
      render={({ values }) => {
        return (
          <Form style={{ height: '100%', padding: '0 12px 24px 24px' }}>
            <div className={toolbar}>
              {tabsbar}
              <SaveButton className={button} type="submit" />
              <CancelButton className={button} type="reset" />
            </div>
            <div style={{ height: 'calc(100% - 84px)' }}>
              <CustomScrollbar>
                <div style={{ paddingRight: 12 }}>
                  <FieldArray
                    name="departments"
                    render={({ push, remove }) => {
                      const fields = values.departments.map((i, idx) => (
                        <div style={{ display: 'flex' }} key={i.id || i._id}>
                          <div style={{ flexGrow: 1, marginRight: 10 }}>
                            <Field
                              name={`departments[${idx}].title`}
                              component={InputField}
                              label={t('SubdivisionName')}
                              placeholder={t('EnterTheTitle')}
                            />
                          </div>
                          {i.children_count === 0 && (
                            <div>
                              <IconButton
                                style={{ position: 'relative', bottom: '-20%' }}
                                size="small"
                                onClick={() => remove(idx)}
                              >
                                <Close />
                              </IconButton>
                            </div>
                          )}
                        </div>
                      ));

                      const number = fields.length + 1;

                      const addButton = (
                        <div style={{ padding: '0 24px' }}>
                          <Button
                            onClick={() =>
                              push({
                                _id: nanoid(3),
                                id: null,
                                title: t('Subdivision') + number,
                              })
                            }
                            color="primary"
                          >
                            + {t('AddSubdivision')}
                          </Button>
                        </div>
                      );

                      return (
                        <>
                          {fields}
                          {addButton}
                        </>
                      );
                    }}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};
