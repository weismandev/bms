import { combine } from 'effector';
import { useStore } from 'effector-react';
import { EditButton, Loader, ErrorMsg as ErrorMessage } from '../../../../ui';
import { $mode, changeMode, $isLoading, $error } from '../../models/departments.model';
import { DepartmentsForm } from '../departments-form';
import { DepartmentsList } from '../departments-list';

const $stores = combine({ mode: $mode, isLoading: $isLoading, error: $error });

export const Departments = (props) => {
  const { mode, isLoading, error } = useStore($stores);
  const { tabsbar } = props;

  return (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      {mode === 'view' ? (
        <>
          <div
            style={{
              padding: 24,
              height: 84,
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'flex-end',
            }}
          >
            <EditButton onClick={() => changeMode('edit')} />
            {tabsbar}
          </div>
          <div style={{ position: 'relative', height: 'calc(100% - 84px)' }}>
            <DepartmentsList />
          </div>
        </>
      ) : (
        <DepartmentsForm tabsbar={tabsbar} />
      )}
    </>
  );
};
