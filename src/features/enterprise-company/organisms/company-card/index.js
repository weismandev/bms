import { useTranslation } from 'react-i18next';
import { ListItem, Typography, Box } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Divider } from '../../../../ui/atoms';

const useStyles = makeStyles((theme) => {
  const acentItemStyles = {
    background: '#E1EBFF !important',
    "& + *[role='separator']": {
      background: '#E1EBFF !important',
    },
    "& *[class*='headerText']": {
      color: '#0394E3 !important',
    },
  };

  return {
    root: {
      display: 'grid',
      gridTemplateColumns: '1fr 200px',
      gridTemplateRows: '90px auto',
      width: '100%',
      padding: '10px 15px',
      '&:hover': acentItemStyles,
    },
    selected: acentItemStyles,
    headerText: {
      gridRow: '1/2',
      gridColumn: '1/2',
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: '#65657B',
      overflow: 'hidden',
      width: '100%',
    },
    addressText: {
      gridRow: '2/3',
      gridColumn: '1/3',
      fontSize: '16px',
      lineHeight: '18px',
      fontWeight: 400,
      color: 'rgba(0, 0, 0, 0.54)',
    },
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },
    details: {
      gridRow: '1/2',
      gridColumn: '2/3',
      display: 'flex',
      flexDirection: 'row',
      margin: 'auto',
    },
    detailsText: {
      fontSize: '13px',
      lineHeight: '16px',
      fontWeight: 500,
      color: '#65657B',
    },
    detailsTitles: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: '13px',
      lineHeight: '18px',
      fontWeight: 300,
      color: '#65657B',
      textAlign: 'end',
      marginRight: '6px',
    },
    detailsValues: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: '13px',
      lineHeight: '18px',
      fontWeight: 600,
      color: '#65657B',
    },
  };
});

export const CompanyCard = (props) => {
  const { root, headerText, addressText, selected, divider } = useStyles();
  const { item, ...rest } = props;
  const { title, actual_address, ...etc } = item;

  return (
    <>
      <ListItem classes={{ root, selected }} button conponent="li" {...rest}>
        <Typography className={headerText} component="h2">
          {title}
        </Typography>
        <Typography className={addressText} component="p">
          {actual_address}
        </Typography>
        <CompanyDetails details={etc} />
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const CompanyDetails = (props) => {
  const { t } = useTranslation();
  const { details, detailsTitles, detailsValues } = useStyles();
  const { inn, okved, ogrn } = props.details;

  return (
    <Box className={details}>
      <Box className={detailsTitles}>
        <span>{t('BIN')}:</span>
        <span>{t('TIN')}:</span>
        <span>{t('RCEAP')}:</span>
      </Box>
      <Box className={detailsValues}>
        <span>{ogrn}</span>
        <span>{inn}</span>
        <span>{okved}</span>
      </Box>
    </Box>
  );
};
