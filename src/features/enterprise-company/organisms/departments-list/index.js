import { useList, useStore } from 'effector-react';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  IconButton,
} from '@mui/material';
import { NavigateNext, NavigateBefore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { FormSectionHeader, CustomScrollbar } from '../../../../ui';
import {
  $childDepartments,
  dive,
  rise,
  $currentDepartment,
} from '../../models/departments.model';

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 24px 24px',
  },

  listInner: {
    paddingRight: 12,
  },

  itemRoot: {
    height: 72,
    paddingRight: 0,
    paddingLeft: 0,
  },
  primaryText: {
    fontWeight: 500,
    lineHeight: '21px',
    fontSize: 21,
    color: '#3B3B50',
    letterSpacing: '0.15px',
  },
  iconRoot: {
    minWidth: 'unset',
    marginLeft: 24,
  },
});

export const DepartmentsList = (props) => {
  const { listOuter, listInner, itemRoot, primaryText, iconRoot } = useStyles();
  const currentDepartment = useStore($currentDepartment);

  const departments = useList($childDepartments, ({ title, id }) => (
    <>
      <ListItem
        onClick={() => dive(id)}
        button
        component="li"
        classes={{ root: itemRoot }}
      >
        <ListItemText classes={{ primary: primaryText }} primary={title} />
        <ListItemIcon classes={{ root: iconRoot }}>
          <NavigateNext />
        </ListItemIcon>
      </ListItem>
      <Divider component="li" />
    </>
  ));

  return (
    <>
      {currentDepartment && (
        <div
          style={{
            height: '48px',
            padding: '0 24px',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <IconButton onClick={rise} size="large">
            <NavigateBefore />
          </IconButton>
          <FormSectionHeader
            header={currentDepartment.title}
            style={{
              margin: 0,
              width: 300,
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
            }}
          />
        </div>
      )}
      <div
        className={listOuter}
        style={{ height: currentDepartment ? 'calc(100% - 48px)' : '100%' }}
      >
        <CustomScrollbar>
          <div className={listInner}>
            <List>{departments}</List>
          </div>
        </CustomScrollbar>
      </div>
    </>
  );
};
