import { useTranslation } from 'react-i18next';
import { useList } from 'effector-react';
import { LabeledContent, FormSectionHeader, CustomScrollbar } from '@ui';
import { IconButton } from '@mui/material';
import { InfoOutlined } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { formatPhone } from '@tools/formatPhone';
import { $enterprisesRenters, open } from '../../models/enterprises-renters.model';

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 24px 24px',
    height: '100%',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    display: 'flex',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
    position: 'relative',
  },
  logoContainer: {
    width: '124px',
    paddingRight: '24px',
  },
  logoImg: { width: 100, height: 100, objectFit: 'cover' },
  part: {
    marginBottom: 5,
  },
  header: {
    marginBottom: 10,
    width: 'calc(100% - 25px)',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  infoButton: {
    position: 'absolute',
    top: 24,
    right: 10,
  },
});

const EnterprisesRentersList = () => {
  const { listOuter, listInner } = useStyles();
  const list = useList($enterprisesRenters, (pass) => <PropertyCard {...pass} />);
  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
    </div>
  );
};

const PropertyCard = (props) => {
  const { title, director, inn, ogrn, rent, logo, actual_address, id } = props;
  const { part, card, text, header, logoContainer, logoImg, infoButton } = useStyles();
  const { t } = useTranslation();

  return (
    <div className={card}>
      {logo && (
        <div className={logoContainer}>
          <img className={logoImg} width="100" src={logo.url} alt={t('CompanyLogo')} />
        </div>
      )}
      <div style={{ width: logo ? 'calc(100% - 124px)' : '100%' }}>
        <FormSectionHeader header={title} className={header} />
        {director && (
          <>
            {director.fullname && (
              <LabeledContent
                labelPlacement="start"
                className={part}
                label={t('Director')}
              >
                <span className={text}>{director.fullname}</span>
              </LabeledContent>
            )}
            {director.phone && (
              <LabeledContent
                labelPlacement="start"
                className={part}
                label={t('Label.phone')}
              >
                <span className={text}>{formatPhone(String(director.phone))}</span>
              </LabeledContent>
            )}
          </>
        )}
        {inn && (
          <LabeledContent labelPlacement="start" className={part} label={t('TIN')}>
            <span className={text}>{inn}</span>
          </LabeledContent>
        )}
        {ogrn && (
          <LabeledContent labelPlacement="start" className={part} label={t('BIN')}>
            <span className={text}>{ogrn}</span>
          </LabeledContent>
        )}
        {rent && (
          <>
            <LabeledContent
              labelPlacement="start"
              className={part}
              label={t('RentalProperties')}
            >
              <span className={text}>{rent.property.title}</span>
            </LabeledContent>
            <LabeledContent
              labelPlacement="start"
              className={part}
              label={t('Label.address')}
            >
              <span className={text}>{rent.property.address}</span>
            </LabeledContent>
          </>
        )}
        {actual_address && (
          <LabeledContent labelPlacement="start" className={part} label={t('BIN')}>
            <span className={text}>{actual_address}</span>
          </LabeledContent>
        )}
      </div>
      <IconButton
        className={infoButton}
        size="small"
        color="primary"
        onClick={() => open(id)}
      >
        <InfoOutlined />
      </IconButton>
    </div>
  );
};

export { EnterprisesRentersList };
