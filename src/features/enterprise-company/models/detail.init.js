import { sample, forward, attach, guard } from 'effector';
import { CheckCircle } from '@mui/icons-material';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { formatFile } from '@tools/formatFile';
import { formatPhone } from '@tools/formatPhone';
import { changeNotification } from '../../colored-text-icon-notification';
import { $dadata, specify, resetDaData } from '../../companies-search';
import { enterpriseApi } from '../api';
import {
  open,
  $opened,
  $isDetailOpen,
  $mode,
  $currentTab,
  $companyForUpdate,
  isCompanyChanged,
  changedDetailVisibility,
  changeMode,
  entityApi,
  addCompany,
  cancelDetailsApi,
  newEntity,
  $employees,
  fxGetEmployeesList,
} from './detail.model';
import {
  $raw,
  $normalized,
  fxUpdateInfo,
  fxUpdateResponsible,
  fxUpdateDirector,
  fxCreate,
  fxGetList,
} from './page.model';

const { t } = i18n;

const formatPayload = ({
  guest_pass_confirmation_required,
  pass_confirmation_required,
  actual_address,
  building_id,
  okved,
  title,
  ogrn,
  kpp,
  inn,
  id,
  phone,
  logo,
  is_guest_pass_sms_auto_sending_enabled,
  short_title,
  contact_phone,
}) => {
  const formatId = (id) => (typeof id === 'object' ? id.building_id : id);
  const formatPass = (bool) => (bool ? 1 : 0);

  return {
    id: id || '',
    enterprise: {
      building_id: formatId(building_id),
      pass_confirmation_required: formatPass(pass_confirmation_required),
      guest_pass_confirmation_required: formatPass(guest_pass_confirmation_required),
      actual_address,
      okved,
      title,
      ogrn,
      kpp,
      inn,
      phone,
      logo_id: logo ? logo.id : '',
      is_guest_pass_sms_auto_sending_enabled: is_guest_pass_sms_auto_sending_enabled
        ? 1
        : 0,
      short_title,
      contact_phone,
    },
  };
};

const formatOpened = (opened) => {
  const {
    id,
    title,
    inn,
    ogrn,
    okved,
    kpp,
    building_id,
    is_verified,
    actual_address,
    pass_confirmation_required,
    guest_pass_confirmation_required,
    phone,
    logo,
    is_guest_pass_sms_auto_sending_enabled,
    short_title,
    contact_phone,
    sms_pass_mode,
    director,
    responsible,
  } = opened;

  return {
    id: id || '',
    actual_address: actual_address || '',
    title: title || '',
    ogrn: ogrn || '',
    inn: inn || '',
    okved: okved || '',
    kpp: kpp || '',
    is_verified,
    building_id: building_id || '',
    pass_confirmation_required,
    guest_pass_confirmation_required,
    phone: phone ? formatPhone(phone) : '',
    logo: logo ? formatFile(logo) : '',
    is_guest_pass_sms_auto_sending_enabled: Boolean(
      is_guest_pass_sms_auto_sending_enabled
    ),
    short_title: short_title || '',
    contact_phone: contact_phone ? formatPhone(contact_phone) : '',
    sms_pass_mode: sms_pass_mode || '',
    director: director || '',
    responsible: responsible || '',
  };
};

fxGetEmployeesList.use(enterpriseApi.getEmployeesList);

$opened
  .on(
    sample($normalized, open, (normalized, id) => normalized[id]),
    (state, opened) => formatOpened(opened)
  )
  .on(fxUpdateInfo.doneData, (_, { enterprise }) => formatOpened(enterprise))
  .on(fxUpdateDirector.doneData, (state, { enterprise = {} }) => ({
    ...state,
    director: enterprise.director,
  }))
  .on(fxUpdateResponsible.doneData, (state, { enterprise = {} }) => ({
    ...state,
    responsible: enterprise.responsible,
  }))
  .on(
    sample($dadata, specify, (dadata, id) => dadata[id]),
    (_, opened) => formatOpened(opened)
  )
  .on(addCompany, () => newEntity)
  .reset(signout);

guard({
  source: $companyForUpdate,
  clock: isCompanyChanged,
  filter: (_, { isInfoChanged }) => isInfoChanged,
  target: fxUpdateInfo.prepend((comp) => formatPayload(comp)),
});

guard({
  source: $companyForUpdate,
  clock: isCompanyChanged,
  filter: (_, { isResChanged }) => isResChanged,
  target: fxUpdateResponsible.prepend(({ id, responsible }) => ({
    responsible_id: responsible.id,
    enterprise_id: id,
  })),
});

guard({
  source: $companyForUpdate,
  clock: isCompanyChanged,
  filter: (_, { isDirChanged }) => isDirChanged,
  target: fxUpdateDirector.prepend(({ id, director }) => ({
    enterprise_id: id,
    director_id: director.id,
  })),
});

$employees
  .on(fxGetEmployeesList.doneData, (_, { items }) =>
    Array.isArray(items) ? formatEmlployee(items) : []
  )
  .reset([open, signout]);

$isDetailOpen
  .on($raw.updates, (state, { data }) => data.length > 0)
  .on(changedDetailVisibility, (state, visibility) => visibility)
  .on(cancelDetailsApi.onEmpty, () => false)
  .on(addCompany, () => true)
  .reset(signout);

$mode
  .on(changeMode, (state, mode) => mode)
  .on(
    [
      open,
      cancelDetailsApi.onUpdate,
      fxUpdateInfo.done,
      fxUpdateDirector.done,
      fxUpdateResponsible.done,
    ],
    () => 'view'
  )
  .on(addCompany, () => 'edit')
  .reset(signout);

$currentTab.reset(signout);

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: (payload) => formatPayload(payload),
  }),
});

forward({
  from: [cancelDetailsApi.onEmpty, cancelDetailsApi.onCreate],
  to: resetDaData,
});

forward({
  from: sample(
    $normalized,
    cancelDetailsApi.onCreate,
    (normalized) => Object.values(normalized)[0].id
  ),
  to: open,
});

forward({
  from: fxCreate.done,
  to: resetDaData,
});

forward({
  from: fxCreate.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('CompanyAddedAwaitingVerification'),
    Icon: CheckCircle,
  })),
});

const openFirstIfNothingOpened = guard({
  source: sample($opened, fxGetList.doneData, (opened, data) => ({
    opened,
    data,
  })),
  filter: ({ opened, data }) => !opened.id && Boolean(data.enterprises[0]),
});

forward({
  from: openFirstIfNothingOpened.map(({ data }) => data.enterprises[0].id),
  to: open,
});

forward({
  from: open,
  to: fxGetEmployeesList.prepend((id) => ({ id })),
});

function formatEmlployee(employees) {
  return employees.map(({ employee_id, fullname }) => ({
    id: employee_id || null,
    fullname: fullname || `-${t('noName')}-`,
  }));
}
