import './departments.init';
import './detail.init';
import './enterprises-renters.init';
import './list.init';
import './page.init';

export * from './page.model';
