import { guard, sample, forward, restore } from 'effector';
import { signout } from '../../common';
import { enterpriseApi } from '../api';
import {
  fxGetFlatDepartmentsTree,
  fxSaveDepartmentsBatch,
  fxDeleteDepartments,
  $enterprisesDepartments,
  $departmentsIdsLinkedList,
  dive,
  rise,
  submitDepartments,
  $currentDepartmentId,
  $mode,
  $isLoading,
  $error,
  $childDepartments,
} from './departments.model';
import { $opened } from './detail.model';

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

fxGetFlatDepartmentsTree.use(enterpriseApi.getFlatDepartmentsTree);
fxSaveDepartmentsBatch.use(enterpriseApi.saveDepartmentsBatch);
fxDeleteDepartments.use(enterpriseApi.deleteDepartments);

$enterprisesDepartments
  .on(fxGetFlatDepartmentsTree.doneData, (state, { items }) => items)
  .reset([$opened.updates, signout]);

$departmentsIdsLinkedList
  .on(dive, (state, id) => state.concat(id))
  .on(rise, (state) => state.slice(0, -1))
  .reset([$opened.updates, signout]);

$mode.on(fxSaveDepartmentsBatch.done, () => 'view').reset(signout);

$error
  .on(
    [
      fxGetFlatDepartmentsTree.failData,
      fxSaveDepartmentsBatch.failData,
      fxDeleteDepartments.failData,
    ],
    (state, error) => error
  )
  .reset([signout, newRequestStarted]);

$isLoading.reset(signout);

sample({
  source: [
    fxSaveDepartmentsBatch.pending,
    fxGetFlatDepartmentsTree.pending,
    fxDeleteDepartments.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

guard({
  source: sample({
    source: $opened,
    clock: [$opened.updates, fxSaveDepartmentsBatch.done],
    fn: (opened) => opened.id,
  }),
  filter: Boolean,
  target: fxGetFlatDepartmentsTree.prepend((enterprise_id) => ({
    enterprise_id,
  })),
});

forward({
  from: sample(
    $childDepartments,
    submitDepartments,
    (childDepartments, { departments: submitDepartments }) => {
      const idsForRemoval = childDepartments
        .filter((i) => !submitDepartments.find((j) => j.id === i.id))
        .map((i) => i.id);

      return idsForRemoval;
    }
  ),
  to: fxDeleteDepartments,
});

guard({
  source: sample({
    source: {
      company: $opened,
      departmentId: $currentDepartmentId,
      submitted: restore(submitDepartments, {}),
    },
    clock: fxDeleteDepartments.done,
    fn: ({ company, departmentId, submitted }) => ({
      enterprise_id: company && company.id,
      parent_id: departmentId,
      ...submitted,
    }),
  }),
  filter: ({ enterprise_id }) => Boolean(enterprise_id),
  target: fxSaveDepartmentsBatch.prepend(
    ({ departments = [], enterprise_id, parent_id }) => ({
      departments: departments.map(({ _id, ...rest }) => ({
        ...rest,
        enterprise_id,
        parent_id,
      })),
    })
  ),
});
