import { forward, guard, sample } from 'effector';
import { signout } from '@features/common';
import { formatFile } from '@tools/formatFile';
import { formatPhone } from '@tools/formatPhone';
import { enterpriseApi } from '../api';
import {
  EnterpriseRentersGate,
  enterpriseRenterSubmitted,
  fxSetOrCreateEnterpriseRenter,
  fxGetEnterprisesRenters,
  $enterprisesRenters,
  $mode,
  $isLoading,
  $error,
  changeFormModalVisibility,
  open,
  $normalizedRenters,
  $opened,
  newEntity,
  add,
  closeForm,
  $isFormModalOpen,
} from './enterprises-renters.model';

fxGetEnterprisesRenters.use(enterpriseApi.getEnterprisesRenters);
fxSetOrCreateEnterpriseRenter.use(enterpriseApi.setOrCreateEnterpriseRenter);

$enterprisesRenters
  .on(fxGetEnterprisesRenters.doneData, (state, { enterprises }) =>
    Array.isArray(enterprises) ? enterprises : state
  )
  .reset(signout);

$mode
  .on([fxSetOrCreateEnterpriseRenter.done, open], () => 'view')
  .on(add, () => 'edit')
  .reset([signout, EnterpriseRentersGate.close]);

$opened
  .on(
    sample($normalizedRenters, open, (data, id) => data[id]),
    (_, opened) => formatOpened(opened)
  )
  .on(add, () => newEntity)
  .reset([closeForm, signout]);

$isFormModalOpen.reset(signout);

forward({
  from: [EnterpriseRentersGate.open, fxSetOrCreateEnterpriseRenter.done.map(() => ({}))],
  to: fxGetEnterprisesRenters,
});

forward({
  from: enterpriseRenterSubmitted.map(
    ({
      first_name,
      last_name,
      patronymic,
      title,
      phone,
      inn,
      ogrn,
      okved,
      kpp,
      start_date,
      finish_date,
      enterprise,
      enterprise_property,
      logo,
    }) => ({
      enterprise: {
        first_name,
        last_name,
        patronymic,
        title,
        phone,
        inn,
        ogrn,
        okved,
        kpp,
        logo_id: logo ? logo.id : '',
      },
      property_id: enterprise_property ? enterprise_property.id : '',
      start_date,
      finish_date,
    })
  ),
  to: fxSetOrCreateEnterpriseRenter,
});

forward({
  from: [open, add],
  to: changeFormModalVisibility.prepend(() => true),
});

sample({
  source: [fxGetEnterprisesRenters.pending, fxSetOrCreateEnterpriseRenter.pending],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

$isLoading.reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [fxGetEnterprisesRenters.failData, fxSetOrCreateEnterpriseRenter.failData],
    (state, error) => error
  )
  .reset([signout, newRequestStarted, closeForm]);

function formatOpened(opened) {
  const { id, title, inn, ogrn, okved, kpp, logo, phone, enterprise, director } = opened;

  return {
    id,
    first_name: (director && director.name) || '',
    last_name: (director && director.surname) || '',
    patronymic: (director && director.patronymic) || '',
    title: title || '',
    phone: phone ? formatPhone(phone) : '',
    inn: inn || '',
    ogrn: ogrn || '',
    okved: okved || '',
    kpp: kpp || '',
    start_date: '',
    finish_date: '',
    enterprise: enterprise || '',
    enterprise_property: '',
    logo: logo ? formatFile(logo) : '',
  };
}
