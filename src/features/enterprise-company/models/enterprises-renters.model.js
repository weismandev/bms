import { createEvent, createEffect, createStore, restore, guard } from 'effector';
import { createGate } from 'effector-react';

const newEntity = {
  id: null,
  first_name: '',
  last_name: '',
  patronymic: '',
  title: '',
  phone: '',
  inn: '',
  ogrn: '',
  okved: '',
  kpp: '',
  start_date: '',
  finish_date: '',
  enterprise: '',
  enterprise_property: '',
  logo: '',
};

const EnterpriseRentersGate = createGate();

const changeMode = createEvent();
const enterpriseRenterSubmitted = createEvent();
const changeFormModalVisibility = createEvent();
const open = createEvent();
const add = createEvent();

const closeForm = guard({
  source: changeFormModalVisibility,
  filter: (visibility) => !visibility,
});

const fxGetEnterprisesRenters = createEffect();
const fxSetOrCreateEnterpriseRenter = createEffect();

const $enterprisesRenters = createStore([]);

const $normalizedRenters = $enterprisesRenters.map((data) =>
  data.reduce((acc, i) => ({ ...acc, [i.id]: i }), {})
);

const $opened = createStore(newEntity);

const $mode = restore(changeMode, 'view');
const $isLoading = createStore(false);
const $error = createStore(null);
const $isFormModalOpen = restore(changeFormModalVisibility, false);

export {
  changeMode,
  enterpriseRenterSubmitted,
  fxGetEnterprisesRenters,
  fxSetOrCreateEnterpriseRenter,
  $mode,
  $enterprisesRenters,
  EnterpriseRentersGate,
  $isLoading,
  $error,
  changeFormModalVisibility,
  $isFormModalOpen,
  $opened,
  $normalizedRenters,
  open,
  add,
  newEntity,
  closeForm,
};
