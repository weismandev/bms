import { createEvent, createEffect, createStore, combine, restore } from 'effector';

const dive = createEvent();
const rise = createEvent();
const changeMode = createEvent();
const submitDepartments = createEvent();

const fxGetFlatDepartmentsTree = createEffect();
const fxSaveDepartmentsBatch = createEffect();
const fxDeleteDepartments = createEffect();

const $enterprisesDepartments = createStore([]);
const $departmentsIdsLinkedList = createStore([null]);
const $mode = restore(changeMode, 'view');
const $isLoading = createStore(false);
const $error = createStore(null);

const $currentDepartmentId = $departmentsIdsLinkedList.map((departments) => {
  const [currentDepartment] = departments.slice().reverse();
  return currentDepartment;
});

const $currentDepartment = combine(
  $enterprisesDepartments,
  $currentDepartmentId,
  (departments, currentId) => {
    let currentDepartment;

    if (currentId) {
      currentDepartment = departments.find((i) => i.id === currentId);
    }

    return currentDepartment || null;
  }
);

const $childDepartments = combine(
  $enterprisesDepartments,
  $currentDepartmentId,
  (departments, currentId) => departments.filter((i) => i.parent_id === currentId)
);

export {
  fxGetFlatDepartmentsTree,
  fxSaveDepartmentsBatch,
  fxDeleteDepartments,
  $childDepartments,
  $departmentsIdsLinkedList,
  $enterprisesDepartments,
  dive,
  rise,
  $mode,
  $currentDepartment,
  changeMode,
  submitDepartments,
  $currentDepartmentId,
  $isLoading,
  $error,
};
