import { signout } from '../../common';
import { addCompany, cancelDetailsApi } from './detail.model';
import { $isListOpen, changedListVisibility } from './list.model';
import { $raw } from './page.model';

$isListOpen
  .on($raw.updates, (state, { data }) => data.length > 0)
  .on(changedListVisibility, (state, visibility) => visibility)
  .on(cancelDetailsApi.onEmpty, () => false)
  .on(addCompany, () => true)
  .reset(signout);
