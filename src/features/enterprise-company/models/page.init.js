import { forward, attach, merge, guard } from 'effector';
import { signout } from '../../common';
import { enterpriseApi } from '../api';
import {
  pageMounted,
  fxGetList,
  fxUpdateInfo,
  fxUpdateResponsible,
  fxUpdateDirector,
  fxCreate,
  $raw,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from './page.model';

fxGetList.use(enterpriseApi.getCompanyList);
fxUpdateInfo.use(enterpriseApi.updateCompanyInfo);
fxCreate.use(enterpriseApi.createCompany);
fxUpdateResponsible.use(enterpriseApi.setResponsible);
fxUpdateDirector.use(enterpriseApi.setDirector);

const request = [
  fxGetList.pending,
  fxUpdateInfo.pending,
  fxUpdateResponsible.pending,
  fxUpdateDirector.pending,
  fxCreate.pending,
];
const gotError = merge([
  fxGetList.fail,
  fxCreate.fail,
  fxUpdateInfo.fail,
  fxUpdateResponsible.fail,
  fxUpdateDirector.fail,
]);

$isLoading.on(request, (_, isLoading) => isLoading).reset(signout);

$error.on(gotError, (_, { error }) => error).reset(signout);

$isErrorDialogOpen
  .on(gotError, () => true)
  .on(errorDialogVisibilityChanged, (_, visibility) => visibility)
  .reset(signout);

$raw
  .on(fxGetList.done, (_, { result }) => {
    const data = result.enterprises?.length > 0 ? result.enterprises : [];

    return {
      meta: { total: data.length },
      data,
    };
  })
  .reset(signout);

forward({
  from: pageMounted,
  to: fxGetList,
});

forward({
  from: [fxUpdateInfo.done, fxCreate.done],
  to: attach({ effect: fxGetList, mapParams: () => ({}) }),
});
