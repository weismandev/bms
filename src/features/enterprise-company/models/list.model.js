import { createStore, createEvent } from 'effector';
import { $raw } from './page.model';

const $isListOpen = createStore(false);
const $list = $raw.map((raw) => raw.data);

const changedListVisibility = createEvent();

export { $isListOpen, $list, changedListVisibility };
