import { createEvent, createStore, createEffect, split, sample, restore } from 'effector';
import i18n from '@shared/config/i18n';
import { isDeepEqual } from '@tools/isDeepEqual';
import { $list } from './list.model';

const { t } = i18n;

const newEntity = {
  id: null,
  title: '',
  inn: '',
  ogrn: '',
  okved: '',
  is_verified: '',
  actual_address: '',
  pass_confirmation_required: true,
  guest_pass_confirmation_required: true,
  building_id: '',
  phone: '',
  logo: '',
  is_guest_pass_sms_auto_sending_enabled: false,
  short_title: '',
  contact_phone: '',
  kpp: '',
};

const tabsList = [
  {
    value: 'departments',
    label: t('Subdivisions'),
  },
  {
    value: 'renters',
    label: t('Tenants'),
  },
];

const open = createEvent();
const changedDetailVisibility = createEvent();
const detailSubmitted = createEvent();
const changeMode = createEvent();
const addCompany = createEvent();
const detailCancel = createEvent();
const changeTab = createEvent();

const entityApi = split(detailSubmitted, {
  create: (payload) => !payload.id,
  update: (payload) => payload.id,
});

const cancelDetailsApi = split(
  sample($list, detailCancel, (list, id) => ({ list, id })),
  {
    onUpdate: ({ id }) => id,
    onCreate: ({ list, id }) => !id && list.length,
    onEmpty: ({ list }) => list.length === 0,
  }
);

const $opened = createStore(newEntity);
const $isDetailOpen = createStore(false);
const $mode = createStore('view');
const $currentTab = restore(changeTab, 'renters');
const $companyForUpdate = restore(entityApi.update, {});
const $employees = createStore([]);

const fxGetEmployeesList = createEffect();

const isCompanyChanged = sample({
  source: $opened,
  clock: entityApi.update,
  fn: (
    { responsible: prevRes = {}, director: prevDir = {}, ...prev },
    { responsible: nextRes = {}, director: nextDir = {}, ...next }
  ) => ({
    isResChanged: !isDeepEqual(prevRes, nextRes),
    isDirChanged: !isDeepEqual(prevDir, nextDir),
    isInfoChanged: !isDeepEqual(prev, next),
  }),
});

export {
  newEntity,
  open,
  $opened,
  $mode,
  $isDetailOpen,
  $companyForUpdate,
  changedDetailVisibility,
  detailSubmitted,
  detailCancel,
  entityApi,
  changeMode,
  addCompany,
  cancelDetailsApi,
  tabsList,
  changeTab,
  $currentTab,
  $employees,
  fxGetEmployeesList,
  isCompanyChanged,
};
