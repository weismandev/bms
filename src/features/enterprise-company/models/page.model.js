import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const errorDialogVisibilityChanged = createEvent();

const $raw = createStore({ data: [], meta: { total: 0 } });
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);

const $normalized = $raw.map((raw) =>
  raw.data.reduce((acc, i) => ({ ...acc, [i.id]: i }), {})
);

const fxGetList = createEffect();
const fxCreate = createEffect();
const fxUpdateInfo = createEffect();
const fxUpdateResponsible = createEffect();
const fxUpdateDirector = createEffect();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  fxGetList,
  fxUpdateInfo,
  fxUpdateResponsible,
  fxUpdateDirector,
  fxCreate,
  $raw,
  $normalized,
  $isLoading,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
};
