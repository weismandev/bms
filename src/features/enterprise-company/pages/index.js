import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Loader, ErrorMessage, NoDataPage } from '../../../ui';
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { HaveSectionAccess } from '../../common';
import '../models';
import { $isDetailOpen, addCompany } from '../models/detail.model';
import { $isListOpen } from '../models/list.model';
import {
  PageGate,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
} from '../models/page.model';
import { List, Detail, TabbedContent } from '../organisms';
import { ThreeEqualColumnsLayout } from '../templates';

const EnterpriseCompanyPage = () => {
  const { t } = useTranslation();
  const isListOpen = useStore($isListOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);

  const list = isListOpen ? <List /> : null;
  const detail = isDetailOpen ? <Detail /> : null;
  const hasContentToShow = list || detail;
  const waitForContent = !hasContentToShow && isLoading;

  const content = hasContentToShow ? (
    <ThreeEqualColumnsLayout first={list} second={detail} third={<TabbedContent />} />
  ) : waitForContent ? (
    <Loader isLoading />
  ) : (
    <NoDataPage onAdd={() => addCompany()} text={t('ThereIsNoCompany')} />
  );

  return (
    <>
      <PageGate />
      <ColoredTextIconNotification />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />
      {content}
    </>
  );
};

const RestrictedCompanyPage = (props) => {
  return (
    <HaveSectionAccess>
      <EnterpriseCompanyPage {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedCompanyPage as EnterpriseCompanyPage };
