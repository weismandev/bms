import { createEffect, createStore } from 'effector';
import format from 'date-fns/format';
import { signout } from '../../common';
import { companyRentedApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result.rents)) {
      return [];
    }

    return result.rents.map(({ id, title, finish_date, start_date }) => ({
      id,
      title: `${title} ( ${format(new Date(start_date), 'dd.MM.yy HH:mm')} - ${format(
        new Date(finish_date),
        'dd.MM.yy HH:mm'
      )} )`,
    }));
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(companyRentedApi.getList);

export { fxGetList, $data, $error, $isLoading };
