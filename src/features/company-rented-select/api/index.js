import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'rents/crm-get-by-enterprise', { ...payload, per_page: 1000 });

export const companyRentedApi = { getList };
