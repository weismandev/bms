import { useState } from "react";
import { ComplexList } from "../organisms/ComplexList";
import styled from '@emotion/styled';
import { UserAppGate } from "../models/userApp.model";
import { HaveSectionAccess } from "@features/common";
import { Themes } from "../organisms/Themes";
import { AcceptModal } from "../organisms/AcceptModal";

const Container = styled.div(() => ({
    padding: '12px 24px 24px 24px',
    display: 'flex',
    gap: '24px',
}));

const UserAppPage = () => {

    return (
        <>
            <UserAppGate />
            
            <AcceptModal />
            
            <Container>
                <ComplexList />
                <Themes />
            </Container>
        </>
    )
};

const RestrictedUserAppPage = () => (
    <HaveSectionAccess>
        <UserAppPage />
    </HaveSectionAccess>
);

export { RestrictedUserAppPage as UserAppPage };