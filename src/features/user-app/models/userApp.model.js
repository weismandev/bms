import { createEvent, createStore, createEffect } from 'effector';
import { createGate } from 'effector-react';

export const UserAppGate = createGate();

export const getComplexInfo = createEvent();
export const getComplexConfig = createEvent();
export const getBmsConfig = createEvent();
export const setBmsConfig = createEvent();
export const setActiveComplex = createEvent();
export const closeError = createEvent();
export const saveComplexConfig = createEvent();
export const changeModalVisibility = createEvent();
export const setPayloadToModal = createEvent();
export const setToDeletePhoto = createEvent();

export const fxGetComplexes = createEffect();
export const fxGetComplexConfig = createEffect();
export const fxGetBmsConfig = createEffect();
export const fxSetComplexConfig = createEffect();
export const fxSetBmsConfig = createEffect();

export const $complexes = createStore([]);
export const $error = createStore(null);
export const $activeId = createStore('');
export const $activeComplex = createStore({ id: '', title: '' });
export const $complexConfig = createStore([]);
export const $modalVisibility = createStore(false);
export const $modalPayload = createStore();
export const $isDelete = createStore(false);