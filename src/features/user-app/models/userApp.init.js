import { forward, sample, guard, merge, createEffect, restore } from 'effector';
import { userAppApi } from '../api';
import {
    getComplexInfo,
    fxGetComplexes,
    $complexes,
    UserAppGate,
    $error,
    closeError,
    $activeComplex,
    setActiveComplex,
    getComplexConfig,
    $complexConfig,
    fxGetComplexConfig,
    fxGetBmsConfig,
    getBmsConfig,
    fxSetComplexConfig,
    saveComplexConfig,
    changeModalVisibility,
    $modalVisibility,
    setPayloadToModal,
    $modalPayload,
    setBmsConfig,
    fxSetBmsConfig,
    setToDeletePhoto,
    $isDelete
} from './userApp.model';

import { $files } from '../atoms/Theme/model';

const newRequestStarted = guard({
    source: merge([
        fxGetComplexes.pending,
        fxGetComplexConfig.pending,
        fxGetBmsConfig.pending,
        fxSetComplexConfig.pending,
        fxSetBmsConfig.pending
    ]),
    filter: Boolean,
});

fxGetComplexes.use(userAppApi.getComplexes);
fxGetComplexConfig.use(userAppApi.getComplexConfig);
fxGetBmsConfig.use(userAppApi.getBMSConfig);
fxSetComplexConfig.use(userAppApi.saveComplexConfig);
fxSetBmsConfig.use(userAppApi.saveBMSConfig);

$error
  .on(fxGetComplexes.failData, (_, error) => error)
  .on(fxGetComplexConfig.failData, (_, error) => error)
  .on(fxGetBmsConfig.failData, (_, error) => error)
  .on(fxSetComplexConfig.failData, (_, error) => error)
  .on(fxSetBmsConfig.failData, (_, error) => error)
  .reset([UserAppGate.close, newRequestStarted, closeError]);

$complexes.on(
    fxGetComplexes.doneData,
    (_, {items}) => items || []
)

$complexConfig.on(
    [fxGetComplexConfig.doneData, fxGetBmsConfig.doneData],
    (_, {config}) => config.items || []
)

$files.on(
    [fxGetComplexConfig.doneData, fxGetBmsConfig.doneData],
    (_, {config}) => config.items.map(item => item.url && item.url !== "null" ? item.url : null) || []
)

$modalVisibility.on(changeModalVisibility, (state) => !state);

$modalPayload.on(setPayloadToModal, (_, item) => item);

$isDelete.on(setToDeletePhoto, (state) => !state);

forward({
    from: [UserAppGate.open, getComplexInfo],
    to: fxGetComplexes
});

forward({
    from: getComplexConfig,
    to: fxGetComplexConfig
});

forward({
    from: getComplexConfig,
    to: fxGetComplexConfig
});

forward({
    from: [UserAppGate.open, getBmsConfig],
    to: fxGetBmsConfig
});

forward({
    from: saveComplexConfig,
    to: fxSetComplexConfig
});

forward({
    from: setBmsConfig,
    to: fxSetBmsConfig
});

$activeComplex.on(setActiveComplex, (_, {id, title}) => ({id, title}));