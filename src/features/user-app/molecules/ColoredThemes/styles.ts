import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const Container = styled.div(() => ({
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: '12px',
}));

export const ThemesContainer = styled.div(() => ({
    height: '100%',
    display: 'flex',
    gap: '24px',    
}));

export const StyledHeaderTypography = styled(Typography)`
    font-size: 16px;
    font-weight: 500;
    line-height: 24px;
    letter-spacing: 0px;
    color: rgba(0, 0, 0, 1);
`;