import { Container, StyledHeaderTypography, ThemesContainer } from './styles';
import { Theme } from '@features/user-app/atoms/Theme';

interface IProps {
    colorLabel: string;
    ids: number[];
};

export const ColoredThemes = (props: IProps) => {
    const { colorLabel, ids } = props;

    return (
        <Container>
            <StyledHeaderTypography>{colorLabel}</StyledHeaderTypography>
            <ThemesContainer>
                <Theme id={ids[0]}/>
                <Theme id={ids[1]}/>
            </ThemesContainer>
        </Container>
    );
};