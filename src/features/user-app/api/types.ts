export interface IBMSConfig {
    name: string;
    value: string | null;
};

export interface IComplexConfig {
    id: number;
    name: string;
    value: string | null;
};