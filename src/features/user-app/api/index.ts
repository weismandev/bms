import { api } from '../../../api/api2';
import { IBMSConfig, IComplexConfig } from './types';

const getOptions = (payload = {}) => api.v1('get', 'app-config/get-options', payload);
const getBMSConfig = (payload = {}) => api.v1('get', 'app-config/get-bms-config', payload);
const getComplexConfig = (id: number) => api.v1('get', 'app-config/get-complex-config', {id});
const saveBMSConfig = (payload: IBMSConfig) => api.v1('post', 'app-config/set-bms-config', payload);
const saveComplexConfig = (payload: IComplexConfig) => api.v1('post', 'app-config/set-complex-config', payload);
const getComplexes = (payload = {}) => api.v1('get', 'complex/list', payload);

export const userAppApi = {
    getOptions,
    getBMSConfig,
    getComplexConfig,
    saveBMSConfig,
    saveComplexConfig,
    getComplexes
};
