import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const Container = styled.div(() => ({
    width: '324px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: '8px',
}));

export const StyledTheme = styled.div((props: Record<string, unknown>) => ({
    background: 'rgba(244, 247, 250, 1)',
    borderRadius: '5px',
    height: '640px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundImage: `url(${props.img})`,
    backgroundSize: 'contain',
}));

export const StyledThemeLockedFilter = styled.div`
    background: rgba(25, 28, 41, 0.06);
    height: 100%;
    width: 100%;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center; 
`;

export const StyledThemeEmptyFilter = styled.div`
    background: rgba(25, 28, 41, 0.87);
    height: 100%;
    width: 100%;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const StyledThemeFilter = styled.div`
    height: 100%;
    width: 100%;
    border-radius: 5px;
    background: rgba(25, 28, 41, 0.5);
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
`;

export const StyledInput = styled.input(() => ({
    display: 'none',
}));

export const StyledLabel = styled.label`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    cursor: pointer;

    &:hover > svg > path {
        fill: white;
    }

    &:hover > span {
        color: white;
    }
`;

export const SavePanel = styled.div(() => ({
    paddingTop: '8px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
}));

export const SavePanelButtons = styled.div(() => ({
    display: 'flex',
    gap: '15px',
}));

export const BlockedPanelTypo = styled(Typography)`
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: 0px;
    color: rgba(0, 0, 0, 0.6);
`;

export const StyledLabelTypo = styled.span`
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0px;
    text-align: center;
    color: rgba(255, 255, 255, 0.5);
`;

export const SaveLabelTypo = styled(Typography)`
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0px;
    color: rgba(25, 28, 41, 0.87);
    width: 239px;
`;

export const SizeLabel = styled(Typography)`
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: 0px;
    color: rgba(25, 28, 41, 0.6);
`;

export const SvgIcon = styled.svg(() => ({
    cursor: 'pointer',
}));