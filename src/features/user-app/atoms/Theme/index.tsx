import { useState, useEffect } from 'react';
import { useGate, useUnit, useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import {
    $selectedImage,
    deleteImage,
    ImageFieldGate,
    selectImage,
    $file,
    $files,
    setNewFiles,
} from './model';
import { $complexConfig, $activeComplex, $complexes, setPayloadToModal, changeModalVisibility, setToDeletePhoto, $isDelete, $modalPayload } from '@features/user-app/models';
import { IComplexConfig } from '@features/user-app/api/types';
import { SvgIcon, StyledThemeLockedFilter, StyledThemeEmptyFilter, StyledLabel, StyledInput, StyledLabelTypo, StyledThemeFilter, Container, StyledTheme, SavePanel, SaveLabelTypo, SavePanelButtons, SizeLabel, BlockedPanelTypo} from './styled';
import { Alert } from '@mui/material';

interface IProps {
    id: number;
};

export const Theme = (props: IProps) => {
    const { t } = useTranslation();
    const { id } = props;
    useGate(ImageFieldGate);

    const [selectedImage, file, files, activeComplex, isDelete, modalPayload, ] = useUnit([$selectedImage, $file, $files, $activeComplex, $isDelete, $modalPayload, $complexConfig]);
    const complexConfig: any = useStore($complexConfig);
    const complexes: any = useStore($complexes);
    const [isHovered, setIsHovered] = useState(false);
    const [isSetFromHere, setIsSetFromHere] = useState(false);
    const [payload, setPayload] = useState<IComplexConfig>()

    useEffect(() => {
        if (isSetFromHere) {
            setNewFiles({file, id});
        }
    }, [file]);

    useEffect(() => {
        if (isDelete && complexConfig[id].name === modalPayload.name) {
            discardPhoto();
            setToDeletePhoto();
        }
    }, [isDelete]);

    useEffect(() => {
        const data = {
            id: +activeComplex?.id,
            name: complexConfig[id]?.name,
            value: files[id] || null,
        }
        setPayload(data);
        //@ts-ignore
        setPayloadToModal(data);
        setIsSetFromHere(false);
    }, [files[id]]);

    const selectFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { files } = event.target;
        const selectedFiles = files as FileList;
        setIsSetFromHere(true);
        selectImage(selectedFiles?.[0]);
    };

    const setNewPhoto = (item: IComplexConfig | undefined) => {
        if (item) {
            // @ts-ignore
            setPayloadToModal(item);
            changeModalVisibility();
        }
    };

    const setPayloadToDiscardPhoto = () => {
        const data = {
            id: +activeComplex?.id,
            name: complexConfig[id]?.name,
            value: null,
        }
        setPayload(data);
        //@ts-ignore
        setPayloadToModal(data);
    }

    const discardPhoto = () => {
        deleteImage();
        setNewFiles({file: null, id});
    }

    const deletePhoto = () => {
        setPayloadToDiscardPhoto();
        discardPhoto();
    }

    const setModalToDiscard = () => {
        setPayloadToDiscardPhoto();
        changeModalVisibility();
    }

    const EmptyIcon = () => {
        return (
            <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M49.5833 6.41667V49.5833H6.41667V6.41667H49.5833ZM49.5833 0.25H6.41667C3.025 0.25 0.25 3.025 0.25 6.41667V49.5833C0.25 52.975 3.025 55.75 6.41667 55.75H49.5833C52.975 55.75 55.75 52.975 55.75 49.5833V6.41667C55.75 3.025 52.975 0.25 49.5833 0.25ZM34.5983 27.5683L25.3483 39.5008L18.75 31.515L9.5 43.4167H46.5L34.5983 27.5683Z" fill="#D2DFE8"/>
            </svg>
        )
    }

    const SaveIcon = () => {
        return (    
            <SvgIcon onClick={() => setNewPhoto(payload)} width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14 0H2C0.89 0 0 0.9 0 2V16C0 17.1 0.89 18 2 18H16C17.1 18 18 17.1 18 16V4L14 0ZM16 16H2V2H13.17L16 4.83V16ZM9 9C7.34 9 6 10.34 6 12C6 13.66 7.34 15 9 15C10.66 15 12 13.66 12 12C12 10.34 10.66 9 9 9ZM3 3H12V7H3V3Z" fill="#00A947"/>
            </SvgIcon>
        );
    };

    const DeleteCrossIcon = () => {
        return (
            <SvgIcon style={{paddingTop: '3px'}} onClick={() => setModalToDiscard()} width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#E13F3D"/>
            </SvgIcon>
        )
    }

    const UploadIcon = () => {
        return (
            <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16 19H2V5H11V3H2C0.9 3 0 3.9 0 5V19C0 20.1 0.9 21 2 21H16C17.1 21 18 20.1 18 19V10H16V19ZM8.21 15.83L6.25 13.47L3.5 17H14.5L10.96 12.29L8.21 15.83ZM18 3V0H16V3H13C13.01 3.01 13 5 13 5H16V7.99C16.01 8 18 7.99 18 7.99V5H21V3H18Z" fill="rgba(255, 255, 255, 0.5)"/>
            </svg>
        )
    }

    const DeleteIcon = () => {
        return (
            <svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11 6V16H3V6H11ZM9.5 0H4.5L3.5 1H0V3H14V1H10.5L9.5 0ZM13 4H1V16C1 17.1 1.9 18 3 18H11C12.1 18 13 17.1 13 16V4Z" fill="rgba(255, 255, 255, 0.5)"/>
            </svg>
        )
    }

    const LockedIcon = () => {
        return (
            <svg width="50" height="65" viewBox="0 0 50 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M43.5 21.6667H40.4167V15.5C40.4167 6.99004 33.51 0.083374 25 0.083374C16.49 0.083374 9.58334 6.99004 9.58334 15.5V21.6667H6.50001C3.10834 21.6667 0.333344 24.4417 0.333344 27.8334V58.6667C0.333344 62.0584 3.10834 64.8334 6.50001 64.8334H43.5C46.8917 64.8334 49.6667 62.0584 49.6667 58.6667V27.8334C49.6667 24.4417 46.8917 21.6667 43.5 21.6667ZM15.75 15.5C15.75 10.3817 19.8817 6.25004 25 6.25004C30.1183 6.25004 34.25 10.3817 34.25 15.5V21.6667H15.75V15.5ZM43.5 58.6667H6.50001V27.8334H43.5V58.6667ZM25 49.4167C28.3917 49.4167 31.1667 46.6417 31.1667 43.25C31.1667 39.8584 28.3917 37.0834 25 37.0834C21.6083 37.0834 18.8333 39.8584 18.8333 43.25C18.8333 46.6417 21.6083 49.4167 25 49.4167Z" fill="#191C29" fill-opacity="0.26"/>
            </svg>
        )
    }

    const ThemeLogic = () => {
        if (!complexConfig[id]?.can_change) {    
            return (
                <StyledThemeLockedFilter>
                    <LockedIcon />
                </StyledThemeLockedFilter>
            )
        }
        if (isHovered && !files[id]) {
            return (
                <StyledThemeEmptyFilter>
                    <StyledLabel>
                        <StyledInput type="file" accept="image/png" onChange={selectFile} />
                        <UploadIcon />
                        <StyledLabelTypo>{t('UploadImage')}</StyledLabelTypo>
                    </StyledLabel>
                </StyledThemeEmptyFilter>
            )
        } else if (!files[id]) {
            return (
                <EmptyIcon />
            )
        }
        if (isHovered && files[id]) {
            return (
                <StyledThemeFilter>
                    <StyledLabel>
                        <StyledInput type="file" accept="image/png" onChange={selectFile} />
                        <UploadIcon />
                        <StyledLabelTypo>{t('UploadImage')}</StyledLabelTypo>
                    </StyledLabel>
                    <StyledLabel onClick={() => deletePhoto()}>
                        <DeleteIcon />
                        <StyledLabelTypo>{t('remove')}</StyledLabelTypo>
                    </StyledLabel>
                </StyledThemeFilter>
            )
        }

        return <></>
    }

    return (
        <Container>
            <StyledTheme img={files[id]} onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
                <ThemeLogic />
            </StyledTheme>
            <SavePanel>
                <SaveLabelTypo>{id % 2 ? t('BackgroundForMain') : t('BackgroundForSplash')}</SaveLabelTypo>
                <SavePanelButtons>
                    {complexConfig[id]?.can_change &&
                        <>
                            <SaveIcon />
                            <DeleteCrossIcon />
                        </>
                    }
                </SavePanelButtons>
            </SavePanel>
            <SizeLabel>{t('RequiredImageSize')}</SizeLabel>
            {!complexConfig[id]?.can_change &&
                <Alert severity="info">
                    <BlockedPanelTypo>{t('TheSettingIsBlockedAccordingToYourTariff')}</BlockedPanelTypo>
                </Alert>
            }
        </Container>
    )
}



