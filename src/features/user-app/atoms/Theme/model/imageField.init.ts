// @ts-nocheck
import { sample } from 'effector';
import { reset } from 'patronum';
import {
  $metaData,
  $selectedImage,
  deleteImage,
  fxUploadImage,
  imageFieldMounted,
  imageFieldUnmounted,
  selectImage,
  $file,
  $files,
  setNewFiles
} from './imageField.model';
import { signout } from '@features/common';
import { imageFieldApi } from '@features/objects/molecules/image-field/api';

fxUploadImage.use(imageFieldApi.uploadFile);

$metaData.on(imageFieldMounted, (_, data) => data);

/** При выборе фото записываем его */
$selectedImage.on(selectImage, (_, image) => image).reset(deleteImage);

/** При изменении выбранного фото загружаем его на бек */
sample({
  source: $selectedImage,
  filter: (image) => Boolean(image),
  fn: (image) => ({
    type: 'image',
    file: image,
  }),
  target: fxUploadImage,
});

$file.on(fxUploadImage.doneData, (_, {file}) => file || null).reset(deleteImage);

$files.on(setNewFiles, (state, {file, id}) => {
  return state.map((item, i) => i == id ? file?.url : item)
});

/** При успешной загрузке фото устанавливаем значение в форму */
// sample({
//   source: $metaData,
//   clock: fxUploadImage.doneData,
//   fn: (uploadedImage) => {
//     form.setFieldValue(field.name, uploadedImage.file);
//   },
// });

/** При удалении фото устанавливаем значение в форму */
// sample({
//   source: $metaData,
//   clock: deleteImage,
//   fn: () => {
//     $fileName.reset;
//   },
// });

/** Сброс */
reset({
  clock: [imageFieldUnmounted, signout],
  target: [$selectedImage, $file],
});
