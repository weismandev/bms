import { ObjectImage } from '@features/objects/types/object';

export type ImageFieldProps = {
  field: {
    name: string;
    value: null | ObjectImage;
  };
  form: any;
  mode: 'view' | 'edit';
};

export type ImageFieldImage = File | null;

export type ImageFieldFileType = 'image' | 'video';

export type ImageFieldFileUpload = {
  data: {
    type: any;
    file: any;
  };
  auth: {
    type: 'api-token' | '';
    token: string;
  };
};

export type ImageLoaded = {
  bytes: number;
  id: number;
  meta: {
    width: number;
    height: number;
  };
  mime_type: string;
  updated_at: string;
  url: string;
} | null;

export type ImageAndId = {
  file: ImageLoaded;
  id: number;
};
