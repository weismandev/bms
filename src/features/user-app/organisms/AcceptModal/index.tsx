import { Modal, CloseButton } from '@ui/index';
import { changeModalVisibility, $modalVisibility, $modalPayload, saveComplexConfig, $activeComplex, setBmsConfig, setToDeletePhoto } from '@features/user-app/models';
import { useUnit } from 'effector-react';
import { Header, HeaderStyled, ContentStyled, ButtonContainter, ButtonAccept, ButtonTypo, ButtonDenied } from './styles';
import { useTranslation } from 'react-i18next';

export const AcceptModal = () => {
    const { t } = useTranslation();
    const [modalVisibility, modalPayload, activeComplex] = useUnit([$modalVisibility, $modalPayload, $activeComplex]);

    const saveChanges = () => {
        if (activeComplex.id != 'all') {
            saveComplexConfig(modalPayload);
        } else {
            //@ts-ignore
            setBmsConfig({ name: modalPayload.name, value: modalPayload.value });
        }
        if (modalPayload.value === null) {
            setToDeletePhoto();
        }
        changeModalVisibility();
    };

    const header = 
        <Header>
            <HeaderStyled>{t('Attention')}</HeaderStyled>
            <CloseButton onClick={() => changeModalVisibility()}/>
        </Header>
    ;

    const content = <ContentStyled>{t('AreYouSureYouWantToSaveYourChanges')}</ContentStyled>

    const actions = 
        <ButtonContainter>
            <ButtonAccept onClick={() => saveChanges()}>
                <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.00016 11.17L1.83016 7L0.410156 8.41L6.00016 14L18.0002 2L16.5902 0.589996L6.00016 11.17Z" fill="white"/>
                </svg>
                <ButtonTypo>{t('Confirm')}</ButtonTypo>
            </ButtonAccept>
            <ButtonDenied onClick={() => changeModalVisibility()}>
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="white"/>
                </svg>
                <ButtonTypo>{t('Cancellation')}</ButtonTypo>
            </ButtonDenied>
        </ButtonContainter>
    return (
        <Modal
            isOpen={modalVisibility}
            onClose={() => changeModalVisibility()}
            header={header}
            content={content}
            actions={actions}
        />
    )
}