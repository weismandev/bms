import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const Container = styled.div(() => ({
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '0px 4px 15px 0px rgba(231, 231, 236, 1)',
    background: 'rgba(255, 255, 255, 1)',
    borderRadius: '12px',
    padding: '24px 24px 28px 24px'
}));

export const ThemesContainer = styled.div(() => ({
    display: 'flex',
    justifyContent: 'space-between',

    '@media (max-width: 1820px)': {
        flexDirection: 'column',
        gap: '8px'
    }
}));

export const StyledHeaderTypography = styled(Typography)`
    font-size: 20px;
    font-weight: 500;
    line-height: 32px;
    letter-spacing: 0px;
    color: rgba(25, 28, 41, 0.87);
    padding-bottom: 24px;
`;