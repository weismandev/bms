import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import { ColoredThemes } from '@features/user-app/molecules/ColoredThemes';
import { $activeComplex } from '@features/user-app/models';
import { useStore } from 'effector-react';
import { CustomScrollbar } from '../../../../ui';
import { Container, StyledHeaderTypography, ThemesContainer } from './styles';

export const Themes = () => {
    const { t } = useTranslation();
    const activeComplex = useStore($activeComplex);
    const [isBms, setIsBms] = useState(true);

    useEffect(() => {
        if (activeComplex.id == 'all') {
            setIsBms(true);
        } else {
            setIsBms(false);
        }
    }, [activeComplex.id]);

    if (!activeComplex.id) return <></>;

    return (
        <Container>
            <StyledHeaderTypography>{activeComplex.title}</StyledHeaderTypography>
            <CustomScrollbar
                style={{ height: '100%' }}
                trackVerticalStyle={{ right: '0px' }}
                autoHide
            >
                <ThemesContainer>
                    <ColoredThemes colorLabel={t('LightTheme')} ids={isBms ? [2, 3] : [0, 1]} />
                    <ColoredThemes colorLabel={t('BlackTheme')} ids={isBms ? [0, 1] : [2, 3]} />
                </ThemesContainer>
            </CustomScrollbar>
        </Container>
    );
};