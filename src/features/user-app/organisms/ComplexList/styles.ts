import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const Container = styled.div(() => ({
    width: '320px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '0px 4px 15px 0px rgba(231, 231, 236, 1)',
    background: 'rgba(255, 255, 255, 1)',
    borderRadius: '12px',
}));

export const ListHeader = styled.div(() => ({
    padding: '24px',
}));

export const StyledHeaderTypography = styled(Typography)`
    color: rgba(25, 28, 41, 0.87);
    font-size: 20px;
    font-weight: 500;
    line-height: 32px;
    letter-spacing: 0px;
`;

export const ListItem = styled.div`
    padding: 12px 24px 12px 24px;
    border-bottom: 1px solid #191c2939;
    cursor: pointer;
    &:hover {
        background: rgba(31, 135, 229, 0.08);
    };
    &.clicked {
        background: rgba(31, 135, 229, 0.08);
    };
    &:first-child {
        border-radius: 12px 12px 0 0;
    }
    &:last-child {
        border-bottom: none;
        border-radius: 0 0 12px 12px;
    }
`;

export const StyledItemTypography = styled(Typography)`
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0px;
    color: rgba(25, 28, 41, 0.87);
`;