import { useUnit } from "effector-react";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { $complexes, $activeComplex, setActiveComplex, getComplexConfig, getBmsConfig } from '../../models/';
import { CustomScrollbar } from '../../../../ui';
import { Container, ListItem, StyledItemTypography} from './styles';

export const ComplexList = () => {
    const { t } = useTranslation();
    const [activeComplex, complexes] = useUnit([$activeComplex, $complexes]);

    const setComplex = (item: any) => {
        setActiveComplex(item);
        if (item.id != 'all') {
            getComplexConfig(item.id);
        } else {
            getBmsConfig();
        }
    }

    useEffect(() => {
        if (complexes.length) {
            setComplex({id: 'all', title: t('AllComplexes')});
        }
    }, [complexes]);

    const ComplexList = () => {
        return complexes.map(({id, title}) => {
            return (
                <ListItem key={id} onClick={() => setComplex({id, title})} className={activeComplex.id == id ? 'clicked' : ''}>
                    <StyledItemTypography>{title}</StyledItemTypography>
                </ListItem>
            )
        })
    }

    return (
        <Container >
            <CustomScrollbar
                style={{ height: '100%' }}
                trackVerticalStyle={{ right: '0px' }}
                autoHide
            >
                <ListItem key={'all'} onClick={() => setComplex({id: 'all', title: t('AllComplexes')})} className={activeComplex.id == 'all' ? 'clicked' : ''}>
                    <StyledItemTypography>{t('AllComplexes')}</StyledItemTypography>
                </ListItem>
                <ComplexList />
            </CustomScrollbar>
        </Container>
    )
};