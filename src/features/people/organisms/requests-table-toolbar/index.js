import { useStore } from 'effector-react';
import { Greedy, Toolbar, FoundInfo, FilterButton, SearchInput } from '../../../../ui';
import {
  $isFilterOpen,
  changedFilterVisibility,
} from '../../models/requests-filter.model';
import { $count } from '../../models/requests-page.model';
import { $search, searchChanged } from '../../models/requests-table.model';
import { TableChanger } from '../../molecules';

function RequestsTableToolbar() {
  const totalCount = useStore($count);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);

  return (
    <Toolbar>
      <FilterButton
        style={{ margin: '0 5px' }}
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={totalCount} style={{ margin: '0 5px' }} />
      <SearchInput
        style={{ margin: '0 5px' }}
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <TableChanger />
    </Toolbar>
  );
}

export { RequestsTableToolbar };
