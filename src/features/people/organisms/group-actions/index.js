import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Wrapper, FilterToolbar, CustomScrollbar, Divider } from '@ui';
import makeStyles from '@mui/styles/makeStyles';
import { declOfNum as declOfNumber } from '../../../../tools/declOfNum';
import { ResetAndNotifyPassword } from '../../../reset-password';
import { changedGroupVisibility } from '../../models/filter.model';
import { $selection } from '../../models/table.model';

const useStyles = makeStyles({
  container: {
    padding: 24,
    paddingTop: 0,
  },
  notion: {
    fontSize: 12,
    margin: '16px 0',
    color: '#65657B',
    opacity: 0.7,
  },
  numberText: {
    marginBottom: 8,
    color: '#65657B',
    fontWeight: 500,
    opacity: 0.7,
  },
  numberBlock: {
    width: 'auto',
    height: 34,
    padding: '0 12px',
    minWidth: 34,
    borderRadius: 17,
    backgroundColor: '#E7E7E7',
    color: '#65657B',
    display: 'inline-flex',
    alignItems: 'center',
    fontWeight: 500,
  },
});

const GroupActions = memo(() => {
  const { t } = useTranslation();
  const selection = useStore($selection);
  const usersNumber = selection.length;

  const classes = useStyles();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedGroupVisibility(false)}
          title={t('groupSettings')}
        />
        <div className={classes.container}>
          <div className={classes.notion}>
            {t('applyGroupSettingsSelectUsersFromList')}
          </div>
          <div className={classes.numberText}>{t('totalSelected')}</div>
          <div className={classes.numberBlock}>
            {usersNumber}{' '}
            {declOfNumber(usersNumber, [
              t('пользователь'),
              t('пользователя'),
              t('пользователей'),
            ])}
          </div>
          <Divider gap="15px 0" />
          <ResetAndNotifyPassword
            context="resident"
            disabled={!usersNumber}
            userdata_ids={selection}
          />
        </div>
      </CustomScrollbar>
    </Wrapper>
  );
});

export { GroupActions };
