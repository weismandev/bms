import { useMemo, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Check, Close } from '@mui/icons-material';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  GroupingState,
  IntegratedGrouping,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableGroupRow,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import { IconButton, Popover } from '@ui/index';
import {
  TableHeaderCell,
  TableContainer,
  ActionButton,
  TableRow,
  TableCell,
  Wrapper,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import { $count } from '../../models/requests-page.model';
import {
  columns,
  $tableData,
  $columnWidths,
  $columnOrder,
  $currentPage,
  $pageSize,
  $hiddenColumnNames,
  pageSizeChanged,
  hiddenColumnChanged,
  columnOrderChanged,
  columnWidthsChanged,
  pageNumChanged as pageNumberChanged,
  acceptRequest,
  rejectClicked,
} from '../../models/requests-table.model';
import { RequestsTableToolbar } from '../requests-table-toolbar';
import { StyledDetailsRoot, StyledAdditionalDetails } from './styled';

const Root = (props) => <Grid.Root {...props} style={{ height: '100%' }} />;

const Row = (props) => {
  const row = useMemo(
    () => <TableRow {...props} style={{ cursor: 'default' }} />,
    [props.children]
  );

  return row;
};

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const GroupCell = (props) => {
  const colSpan = columns.length + 1;
  return (
    <TableCell colSpan={colSpan}>
      <span
        style={{
          height: '40px',
          display: 'flex',
          alignItems: 'center',
          fontSize: '16px',
          fontWeight: '500',
          color: 'black',
          cursor: 'default',
        }}
      >
        {props.row.value}
      </span>
    </TableCell>
  );
};

const ActingsFormatter = (props) => {
  const { t } = useTranslation();

  return props.row.is_verified === null ? (
    <div
      style={{
        width: 260,
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
        margin: '0 auto',
      }}
    >
      <ActionButton
        kind="positive"
        onClick={(e) => {
          e.stopPropagation();
          acceptRequest(props.row.id);
        }}
      >
        <Check style={{ marginRight: 5 }} />
        {t('Confirm')}
      </ActionButton>
      <ActionButton
        kind="negative"
        onClick={(e) => {
          e.stopPropagation();
          rejectClicked(props.row.id);
        }}
      >
        <Close style={{ marginRight: 5 }} />
        {t('Reject')}
      </ActionButton>
    </div>
  ) : null;
};

const ActingsProvider = (props) => (
  <DataTypeProvider formatterComponent={ActingsFormatter} {...props} />
);

const $stores = combine({
  data: $tableData,
  totalCount: $count,
  currentPage: $currentPage,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  pageSize: $pageSize,
  hiddenColumnNames: $hiddenColumnNames,
});

const DetailFormatter = ({ value, row }) => {
  return (
    <StyledDetailsRoot>
      <span>{value[0]}</span>
      {value[1] && (
        <Popover
          trigger={
            <IconButton size="small">
              <span>+{value.slice(1).length}</span>
            </IconButton>
          }
        >
          <StyledAdditionalDetails>
            {value.map((detail, index) => (
              <span key={index}>{detail}</span>
            ))}
          </StyledAdditionalDetails>
        </Popover>
      )}
    </StyledDetailsRoot>
  );
};

const DetailTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={DetailFormatter} {...props} />
);

export const RequestsTable = memo((props) => {
  const { t } = useTranslation();
  const {
    data,
    totalCount,
    currentPage,
    columnOrder,
    columnWidths,
    pageSize,
    hiddenColumnNames,
  } = useStore($stores);

  const expandedGroups = data.length
    ? data
        .map(({ date }) => date)
        .reduce(
          (unique, date) => (unique.includes(date) ? unique : [...unique, date]),
          []
        )
    : [];

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid rootComponent={Root} rows={data} columns={columns}>
        <GroupingState
          expandedGroups={expandedGroups}
          grouping={[{ columnName: 'date' }]}
        />
        <IntegratedGrouping />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />

        <DetailTypeProvider for={['details']} />

        <DragDropProvider />
        <ActingsProvider for={['actings']} />
        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableGroupRow cellComponent={GroupCell} indentColumnWidth={0} />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow cellComponent={TableHeaderCell} />
        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <RequestsTableToolbar />
        </Template>
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});
