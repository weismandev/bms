import styled from '@emotion/styled';

export const StyledDetailsRoot = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'start',
  alignItems: 'center',
}));

export const StyledAdditionalDetails = styled.div(() => ({
  display: 'grid',
  gap: 10,
  padding: 10,
  background: '#fcfcfc',
}));
