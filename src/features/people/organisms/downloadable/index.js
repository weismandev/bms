import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';

import makeStyles from '@mui/styles/makeStyles';

import { ActionButton, Modal } from '@ui';
import { DataGrid } from '@features/data-grid';

import {
  columns,
  $tableData,
  $isDownloadaleTableOpen,
  confirmTask,
  cancelTask,
} from '../../models/downloadable.model';

const useStyles = makeStyles({
  modal: {
    width: '100%',
    minWidth: '80%',
  },
  tableRoot: {
    cursor: 'pointer',
  },
  handlerWrap: {
    height: '50px',
  },
  tableContent: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
  content: {
    width: '100%',
    height: 'calc(50vh - 150px)',
  },
});

const DownloadableTable = () => {
  const { t } = useTranslation();
  const data = useStore($tableData);
  const isOpen = useStore($isDownloadaleTableOpen);
  const classes = useStyles();

  const onSave = () => confirmTask();

  const onCancel = () => cancelTask();

  const renderActions = (
    <>
      <ActionButton kind="positive" onClick={onSave} style={{ marginRight: '10px' }}>
        {t('saveAllReasonable')}
      </ActionButton>
      <ActionButton kind="negative" onClick={onCancel}>
        {t('cancelTheDownload')}
      </ActionButton>
    </>
  );

  const content = (
    <div className={classes.tableContent}>
      <DataGrid.Mini
        classes={{ root: classes.tableRoot }}
        rows={data}
        columns={columns}
        disableColumnMenu
        border={false}
        hideFooter
        density="compact"
        disableVirtualization={false}
      />
    </div>
  );

  return (
    <Modal
      header={t('downloadableResidents')}
      content={content}
      isOpen={isOpen}
      classes={{ paper: classes.modal }}
      actions={renderActions}
    />
  );
};

export { DownloadableTable };
