import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Field } from 'formik';
import { IconButton, Grid, Chip } from '@mui/material';
import { Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { InputField, ShareMaskedInput, FormSectionHeader, SelectField } from '@ui/index';
import { isNumber, isString } from '../../../../tools/type';
import { ApartmentSelectField } from '../../../apartment-select';
import { HouseSelectField } from '../../../house-select';
import { ObjectTypeSelectField } from '../../../object-type-select';
import { ParkingSlotForUserPropertySelectField } from '../../../parking-slot-for-user-property-select';
import { $objectsStatuses } from '../../models';
import { PantrySelectField } from '../select-pantry';

const useStyles = makeStyles({
  cardContainer: {
    padding: '18px 24px 24px 24px',
    width: '100%',
    background: '#EDF6FF',
    borderRadius: 16,
    marginTop: 24,
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
});

const RemoveButtonOrStatusChip = ({ label, remove, isEditMode }) => {
  return isEditMode ? (
    <IconButton size="small" onClick={remove}>
      <Close />
    </IconButton>
  ) : label ? (
    <Chip style={{ height: 24 }} color="primary" label={label} />
  ) : null;
};

const ObjectsCard = ({
  remove,
  parentName,
  values,
  mode,
  userdata_id,
  setFieldValue,
}) => {
  const { cardContainer, cardHeader } = useStyles();
  const objectsStatuses = useStore($objectsStatuses);
  const { t } = useTranslation();

  const isEditMode = mode === 'edit';
  const isFlat = values.type?.slug === 'flat';
  const isParkingSlot = values.type?.slug === 'parking_slot';
  const isPantry = values.type?.slug === 'pantry';
  const typeDefined = Boolean(values.type);
  const header = typeDefined ? values.type.title : '';

  const building_id = values.building
    ? isNumber(values.building) || isString(values.building)
      ? values.building
      : values.building.id
    : '';

  const apartmentField = typeDefined && isFlat && (
    <>
      <Grid item xs={12}>
        <Field
          label={t('Label.building')}
          placeholder={t('chooseHouse')}
          component={HouseSelectField}
          divider={false}
          name={`${parentName}.building`}
          mode={mode}
          onChange={(value) => {
            setFieldValue(`${parentName}.building`, value);
            setFieldValue(`${parentName}.entity`, '');
          }}
          required
        />
      </Grid>
      <Grid item xs={12} xl={6}>
        <Field
          building_id={building_id}
          label={t('Label.flat')}
          placeholder={t('chooseFlat')}
          divider={false}
          component={ApartmentSelectField}
          name={`${parentName}.entity`}
          getOptionLabel={(opt) => (opt ? opt.flat : '')}
          mode={mode}
          required
        />
      </Grid>
      <Grid item xs={12} xl={6}>
        <Field
          name={`${parentName}.share`}
          render={({ field, form }) => (
            <InputField
              field={field}
              form={form}
              inputComponent={ShareMaskedInput}
              label={t('share')}
              placeholder={t('enterShare')}
              divider={false}
              mode={mode}
            />
          )}
        />
      </Grid>
    </>
  );

  const buildingField = typeDefined && isParkingSlot && (
    <>
      <Grid item xs={12}>
        <Field
          label={t('Label.building')}
          placeholder={t('selectHouse')}
          component={HouseSelectField}
          divider={false}
          name={`${parentName}.building`}
          mode={mode}
          required
          onChange={(value) => {
            setFieldValue(`${parentName}.building`, value);
            setFieldValue(`${parentName}.entity`, '');
          }}
        />
      </Grid>
    </>
  );

  const parkingSlotField = typeDefined && isParkingSlot && building_id && (
    <Grid item xs={12} xl={6}>
      <Field
        label={t('p/m')}
        placeholder={t('choosePM')}
        component={ParkingSlotForUserPropertySelectField}
        userdata_id={userdata_id}
        divider={false}
        сurrentSlot={values.entity}
        buildings={building_id ? [building_id] : []}
        name={`${parentName}.entity`}
        mode={mode}
        required
      />
    </Grid>
  );

  const pantryField = typeDefined && isPantry && (
    <>
      <Grid item xs={12}>
        <Field
          label={t('Label.building')}
          placeholder={t('chooseHouse')}
          component={HouseSelectField}
          divider={false}
          name={`${parentName}.building`}
          mode={mode}
          required
          onChange={(value) => {
            setFieldValue(`${parentName}.building`, value);
            setFieldValue(`${parentName}.entity`, '');
          }}
        />
      </Grid>
      <Grid item xs={12} xl={6}>
        <Field
          label={t('pantry')}
          buildingId={building_id}
          placeholder={t('choosePantry')}
          divider={false}
          component={PantrySelectField}
          name={`${parentName}.entity`}
          mode={mode}
          required
        />
      </Grid>
      <Grid item xs={12} xl={6}>
        <Field
          name={`${parentName}.share`}
          render={({ field, form }) => (
            <InputField
              field={field}
              form={form}
              inputComponent={ShareMaskedInput}
              label={t('share')}
              placeholder={t('enterShare')}
              divider={false}
              mode={mode}
            />
          )}
        />
      </Grid>
    </>
  );

  return (
    <div className={cardContainer}>
      <div className={cardHeader}>
        <FormSectionHeader header={header} />
        <RemoveButtonOrStatusChip
          remove={remove}
          isEditMode={isEditMode}
          label={values.ownership_type && values.ownership_type.title}
        />
      </div>
      <Grid container spacing={2}>
        {isEditMode ? (
          <>
            <Grid item xs={12} xl={6}>
              <Field
                label={t('Label.typeProperty')}
                placeholder={t('chooseType')}
                divider={false}
                component={ObjectTypeSelectField}
                name={`${parentName}.type`}
                mode={mode}
                required
              />
            </Grid>
            <Grid item xs={12} xl={6}>
              <Field
                label={t('Label.status')}
                placeholder={t('selectStatuse')}
                divider={false}
                required
                component={SelectField}
                options={objectsStatuses}
                name={`${parentName}.ownership_type`}
                mode={mode}
              />
            </Grid>
          </>
        ) : null}
        {apartmentField}
        {buildingField}
        {parkingSlotField}
        {pantryField}
      </Grid>
    </div>
  );
};

export default ObjectsCard;
