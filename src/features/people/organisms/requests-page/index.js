import { useStore } from 'effector-react';
import { RequestsTable, RequestsFilter } from '..';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../../ui';
import { $isFilterOpen } from '../../models/requests-filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../../models/requests-page.model';
import { RequestRejectDialog } from '../../molecules';

export const RequestsPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <RequestRejectDialog />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<RequestsTable />}
        filter={isFilterOpen && <RequestsFilter />}
      />
    </>
  );
};
