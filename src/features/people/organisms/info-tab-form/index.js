import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Tooltip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { ApartmentSelectField } from '@features/apartment-select';
import { HouseSelectField } from '@features/house-select';
import { ResetAndNotifyPassword } from '@features/reset-password';
import { TypesOwnershipSelectField } from '@features/types-ownership-select';
import i18n from '@shared/config/i18n';
import { createEmailOrPhoneValidateFn } from '@tools/validations';
import { validateSomePhone } from '@ui/atoms/some-phone-masked-input';
import {
  DetailToolbar,
  InputField,
  AvatarControl,
  ShareMaskedInput,
  CustomScrollbar,
  EditButton,
  SaveButton,
  CancelButton,
  DeleteButton,
  SomePhoneMaskedInput,
  EmailMaskedInput,
  SelectField,
  FormSectionHeader,
} from '@ui/index';
import {
  $bleKeys,
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
  $peopleStatuses,
} from '../../models/detail.model';
import { deleteDialogVisibilityChanged } from '../../models/people-page.model';
import { DebtList } from '../../molecules';

const testEmailOrPhoneProvided = createEmailOrPhoneValidateFn();

const { t } = i18n;

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email(t('incorrectEmail'))
    .test(
      'email-or-phone-mandatory',
      t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
      testEmailOrPhoneProvided
    ),
  phone: Yup.string()
    .test(
      'email-or-phone-mandatory',
      t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
      testEmailOrPhoneProvided
    )
    .test('phone-length', t('invalidNumber'), validateSomePhone),
  apartment: Yup.object().required(t('thisIsRequiredField')).nullable(),
  surname: Yup.string().required(t('thisIsRequiredField')),
  name: Yup.string().required(t('thisIsRequiredField')),
  building: Yup.mixed().required(t('thisIsRequiredField')).nullable(),
  size: Yup.string(),
  statuses: Yup.array().of(Yup.mixed()).required(t('thisIsRequiredField')),
});

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  tooltip: {
    background: 'rgb(0, 0, 0, 0.8)',
    fontSize: 15,
    color: 'rgb(255, 255, 255)',
    fontWeight: 400,
  },
  key_container: {
    display: 'grid',
    gap: 10,
    marginBottom: 20,
  },
  key_card: {
    display: 'grid',
    gap: 10,
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    gridTemplateColumns: 'auto auto',
    background: '#edf6ff',
    padding: '10px 15px',
    borderRadius: 16,
  },
  key_title: {
    fontWeight: 'bold',
  },
});

const InfoTabForm = ({ opened, mode, isNew }) => {
  const classes = useStyles();
  const peopleStatuses = useStore($peopleStatuses);
  const bleKeys = useStore($bleKeys);

  return (
    <>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={(props) => (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onClose={() => changedDetailVisibility(false)}
              hidden={{ edit: true, delete: true, save: true, cancel: true }}
            >
              <Tooltip
                title={opened.readonly ? t('dataCannotBeChanged') : t('edit')}
                classes={{ tooltip: classes.tooltip }}
              >
                <div style={{ order: 10 }}>
                  <EditButton
                    className={classes.margin}
                    onClick={() => changeMode('edit')}
                    disabled={mode === 'edit' || opened.readonly}
                    isHideTitleAccess
                  />
                </div>
              </Tooltip>
              <Tooltip
                title={opened.readonly ? t('dataCannotBeChanged') : t('remove')}
                classes={{ tooltip: classes.tooltip }}
              >
                <div style={{ order: 20 }}>
                  <DeleteButton
                    kind="negative"
                    className={classes.margin}
                    disabled={mode === 'edit' || opened.readonly}
                    onClick={() => deleteDialogVisibilityChanged(true)}
                    isHideTitleAccess
                  />
                </div>
              </Tooltip>
              {!isNew && (
                <Tooltip
                  title={t('sendAuthenticationPair')}
                  classes={{ tooltip: classes.tooltip }}
                >
                  <div style={{ order: 30 }}>
                    <ResetAndNotifyPassword
                      context="resident"
                      user_id={opened.id}
                      isHideTitleAccess
                    />
                  </div>
                </Tooltip>
              )}
              {mode === 'edit' && (
                <>
                  <Tooltip title={t('Save')} classes={{ tooltip: classes.tooltip }}>
                    <div style={{ order: 60 }}>
                      <SaveButton
                        className={classes.margin}
                        type="submit"
                        isHideTitleAccess
                      />
                    </div>
                  </Tooltip>
                  <Tooltip title={t('cancel')} classes={{ tooltip: classes.tooltip }}>
                    <div style={{ order: 70 }}>
                      <CancelButton
                        className={classes.margin}
                        isHideTitleAccess
                        onClick={() => {
                          if (isNew) {
                            changedDetailVisibility(false);
                          } else {
                            changeMode('view');
                            props.resetForm();
                          }
                        }}
                      />
                    </div>
                  </Tooltip>
                </>
              )}
            </DetailToolbar>
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    name="avatar"
                    render={({ field, form }) => (
                      <AvatarControl
                        encoding="base64"
                        mode={mode}
                        onChange={(value) => form.setFieldValue(field.name, value)}
                        isModifyResult={false}
                        value={field.value}
                      />
                    )}
                  />
                  <Field
                    name="surname"
                    component={InputField}
                    label={t('Label.lastname')}
                    required
                    mode={mode}
                  />
                  <Field
                    name="name"
                    component={InputField}
                    label={t('Label.name')}
                    required
                    mode={mode}
                  />
                  <Field
                    name="patronymic"
                    component={InputField}
                    label={t('Label.patronymic')}
                    mode={mode}
                  />
                  <Field
                    name="account_number"
                    component={InputField}
                    label={t('Label.personalAccount')}
                    type="number"
                    mode={mode}
                  />
                  <Field
                    name="building"
                    component={HouseSelectField}
                    onChange={(value) => {
                      props.setFieldValue('building', value);
                      props.setFieldValue('apartment', '');
                    }}
                    label={t('Label.building')}
                    required
                    placeholder={t('selectHouse')}
                    mode={mode}
                  />
                  <Field
                    name="apartment"
                    mode={mode}
                    component={ApartmentSelectField}
                    isDisabled={!props.values.building}
                    building_id={
                      props.values.building && typeof props.values.building === 'object'
                        ? props.values.building.id
                        : props.values.building
                    }
                    helpText={t('firstAndForemostChooseBuilding')}
                    label={t('flat')}
                    required
                    placeholder={t('selectThatApartment')}
                  />
                  <Field
                    name="statuses"
                    component={SelectField}
                    options={peopleStatuses}
                    mode={mode}
                    isMulti
                    required
                    label={t('statuses')}
                    placeholder={t('selectStatuses')}
                  />
                  <Field
                    name="type"
                    component={TypesOwnershipSelectField}
                    mode={mode}
                    label={t('Label.typeProperty')}
                    placeholder={t('selectPropertyType')}
                  />
                  <Field
                    name="phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        required={!form.values.email}
                        inputComponent={SomePhoneMaskedInput}
                        label={t('Label.phone')}
                        placeholder={t('Label.phone')}
                        mode={mode}
                        inputProps={{ form }}
                      />
                    )}
                  />
                  <Field
                    name="email"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        required={!form.values.phone}
                        inputComponent={EmailMaskedInput}
                        label="Email"
                        mode={mode}
                      />
                    )}
                  />
                  <Field
                    name="size"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        inputComponent={ShareMaskedInput}
                        label={t('share')}
                        placeholder={t('share')}
                        mode={mode}
                      />
                    )}
                  />
                  <Field
                    name="comment"
                    label={t('Label.comment')}
                    component={InputField}
                    mode={mode}
                    placeholder={t('Label.comment')}
                    multiline
                    rowsMax={5}
                  />
                  <FormSectionHeader
                    style={{ marginBottom: 15 }}
                    variant="h6"
                    header={t('navMenu.security.accessKeys')}
                  />
                  {bleKeys?.length ? (
                    <div className={classes.key_container}>
                      {bleKeys.map((item, index) => (
                        <div key={index} className={classes.key_card}>
                          <span className={classes.key_title}>{item.key_title}</span>
                          <span>{item.key_value ? item.key_value : t('NoValue')}</span>
                        </div>
                      ))}
                    </div>
                  ) : (
                    <span>{t('NoKeys')}</span>
                  )}

                  {opened && <DebtList accounts={opened.accounts} />}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </>
  );
};

export default InfoTabForm;
