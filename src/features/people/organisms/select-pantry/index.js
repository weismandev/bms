import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { fxGetList, $data, $isLoading, $error } from '../../models/pantry.model';

const PantrySelect = (props) => {
  const { buildingId } = props;

  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (buildingId) {
      const filter = {
        filter: {
          buildings: [buildingId],
        },
      };
      fxGetList(filter);
    }
  }, [buildingId]);

  const options = buildingId ? data : [];

  return (
    <SelectControl
      isLoading={isLoading}
      disabled={isLoading}
      error={error}
      options={options}
      {...props}
    />
  );
};

export const PantrySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PantrySelect />} onChange={onChange} {...props} />;
};
