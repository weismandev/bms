import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { useMediaQuery } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Tooltip from '@mui/material/Tooltip';
import makeStyles from '@mui/styles/makeStyles';
import {
  FilterButton,
  GroupButton,
  ExportButton,
  AddButton,
  ImportButton,
  MoreButton,
  DownloadTemplateButton,
  Greedy,
  Toolbar,
  AdaptiveSearch,
  FileControl,
} from '../../../../ui';
import { addClicked, $mode } from '../../models/detail.model';
import {
  $isFilterOpen,
  $isGroupOpen,
  changedFilterVisibility,
  changedGroupVisibility,
} from '../../models/filter.model';
import { $rowCount, $isTransferLoading } from '../../models/people-page.model';
import {
  searchChanged,
  $search,
  exportListClicked,
  downloadTemplateClicked,
  fileUploaded,
  $tableData,
  $hiddenColumnNames,
  columns,
} from '../../models/table.model';
import { TableChanger } from '../../molecules';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  tooltipContainer: {
    display: 'flex',
  },
});

const useTooltipClasses = makeStyles((theme) => ({
  tooltip: {
    backgroundColor: '#fff',
    boxShadow: '0px 5px 15px #DCDCDC',
    borderRadius: 25,
  },
}));

function TableToolbar(props) {
  const { t } = useTranslation();
  const [tooltipOpen, setTooltip] = useState(false);
  const handleTooltipClose = () => setTooltip(false);
  const handleTooltipOpen = () => setTooltip(true);

  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const isGroupOpen = useStore($isGroupOpen);
  const searchValue = useStore($search);
  const tableData = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  const mode = useStore($mode);
  const isTransferLoading = useStore($isTransferLoading);
  const isEditMode = mode === 'edit';
  const isAnySideOpen = isFilterOpen || isGroupOpen || isEditMode;
  const isScreenLess1440 = useMediaQuery('(max-width:1440px)');

  const classes = useStyles();
  const tooltipClasses = useTooltipClasses();

  const rightToolbar = (
    <>
      <DownloadTemplateButton
        className={classes.margin}
        onClick={downloadTemplateClicked}
      />
      <FileControl
        mode="edit"
        name="file"
        encoding="file"
        onChange={(value) => {
          if (value && value.file) {
            fileUploaded(value.file);
          }
        }}
        renderPreview={() => null}
        renderButton={() => (
          <ImportButton component="span" disabled={isTransferLoading} />
        )}
      />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        onClick={exportListClicked}
        className={classes.margin}
      />
    </>
  );
  const rightTooltip = (
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <div>
        <Tooltip
          placement="bottom"
          classes={tooltipClasses}
          className={classes.margin}
          title={<div className={classes.tooltipContainer}>{rightToolbar}</div>}
          PopperProps={{
            disablePortal: true,
          }}
          onClose={handleTooltipClose}
          open={tooltipOpen}
          disableFocusListener
          disableHoverListener
          disableTouchListener
        >
          <MoreButton onClick={handleTooltipOpen} disabled={tooltipOpen} />
        </Tooltip>
      </div>
    </ClickAwayListener>
  );

  const getResponsiveOrFullElement = (fullElement, responsiveElement) => {
    if (isScreenLess1440 || isAnySideOpen) {
      return responsiveElement;
    }
    return fullElement;
  };

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <GroupButton
        style={{ display: 'none' }}
        disabled={isGroupOpen}
        onClick={() => changedGroupVisibility(true)}
        className={classes.margin}
      />
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <Greedy />
      {isTransferLoading && (
        <Tooltip title={t('residentsAreBeingImported')}>
          <div
            style={{
              width: 34,
              height: 34,
              margin: '0px 10px',
              padding: '3px 0',
            }}
          >
            <CircularProgress size={30} />
          </div>
        </Tooltip>
      )}
      <TableChanger isAnySideOpen={isAnySideOpen} />
      {getResponsiveOrFullElement(rightToolbar, rightTooltip)}
      <AddButton onClick={addClicked} className={classes.margin} disabled={isEditMode} />
    </Toolbar>
  );
}

export { TableToolbar };
