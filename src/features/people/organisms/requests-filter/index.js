import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  SelectField,
  CustomScrollbar,
} from '../../../../ui';
import {
  statusesOptions,
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/requests-filter.model';

export const RequestsFilter = memo((props) => {
  const filters = useStore($filters);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={() => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="status"
                  component={SelectField}
                  label={t('verificationStatus')}
                  placeholder={t('selectStatuse')}
                  options={statusesOptions}
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
