import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Tooltip, Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  CustomScrollbar,
  EditButton,
  SaveButton,
  CancelButton,
} from '@ui/index';
import {
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
  $userObjects,
  newObject,
} from '../../models/detail.model';
import ObjectsCard from '../object-card';

const { t } = i18n;

const mixed = Yup.mixed().required(t('thisIsRequiredField'));

const objectValidationSchema = Yup.object().shape({
  objects: Yup.array().of(
    Yup.object().shape({
      ownership_type: mixed,
      building: Yup.mixed().when('type', {
        is: ({ slug }) => slug === 'parking_slot',
        then: Yup.mixed().nullable(),
        otherwise: mixed,
      }),
      entity: mixed,
      type: mixed.nullable(),
    })
  ),
});

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  tooltip: {
    background: 'rgb(0, 0, 0, 0.8)',
    fontSize: 15,
    color: 'rgb(255, 255, 255)',
    fontWeight: 400,
  },
});

const ObjectTabForm = ({ mode, isNew, opened }) => {
  const userObjects = useStore($userObjects);
  const userdata_id = (opened && opened.id) || '';

  const classes = useStyles();

  const isEditMode = mode === 'edit';
  return (
    <Formik
      initialValues={{ objects: userObjects }}
      validationSchema={objectValidationSchema}
      onSubmit={detailSubmitted}
      enableReinitialize
      render={({ resetForm, values }) => (
        <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
          <DetailToolbar
            style={{ padding: 24 }}
            mode={mode}
            hidden={{ edit: true, delete: true, save: true, cancel: true }}
            onClose={() => changedDetailVisibility(false)}
          >
            <Tooltip
              title={opened.readonly ? t('dataCannotBeChanged') : t('edit')}
              classes={{ tooltip: classes.tooltip }}
            >
              <div style={{ order: 10 }}>
                <EditButton
                  className={classes.margin}
                  onClick={() => changeMode('edit')}
                  disabled={mode === 'edit' || opened.readonly}
                  isHideTitleAccess
                />
              </div>
            </Tooltip>
            {mode === 'edit' && (
              <>
                <Tooltip title={t('Save')} classes={{ tooltip: classes.tooltip }}>
                  <div style={{ order: 60 }}>
                    <SaveButton
                      className={classes.margin}
                      type="submit"
                      isHideTitleAccess
                    />
                  </div>
                </Tooltip>
                <Tooltip title={t('cancel')} classes={{ tooltip: classes.tooltip }}>
                  <div style={{ order: 70 }}>
                    <CancelButton
                      className={classes.margin}
                      isHideTitleAccess
                      onClick={() => {
                        if (isNew) {
                          changedDetailVisibility(false);
                        } else {
                          changeMode('view');
                          resetForm();
                        }
                      }}
                    />
                  </div>
                </Tooltip>
              </>
            )}
          </DetailToolbar>
          <div
            style={{
              height: 'calc(100% - 82px)',
              padding: '0 6px 24px 24px',
            }}
          >
            <CustomScrollbar>
              <div style={{ paddingRight: 18 }}>
                <FieldArray
                  name="objects"
                  render={(helpers) => {
                    const objectCards = values.objects.map((object, idx) => (
                      <Field
                        key={idx}
                        name={`objects[${idx}]`}
                        render={({ field, form }) => (
                          <ObjectsCard
                            remove={() => helpers.remove(idx)}
                            values={field.value}
                            userdata_id={userdata_id}
                            parentName={field.name}
                            mode={mode}
                            setFieldValue={form.setFieldValue}
                          />
                        )}
                      />
                    ));

                    const addButton = isEditMode ? (
                      <Button
                        style={{ marginTop: 16 }}
                        color="primary"
                        onClick={() => helpers.push(newObject)}
                      >
                        + {t('addObject')}
                      </Button>
                    ) : null;

                    return (
                      <>
                        {objectCards}
                        {addButton}
                      </>
                    );
                  }}
                />
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};

export default ObjectTabForm;
