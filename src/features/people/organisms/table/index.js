import { useCallback, useMemo, useState, memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Field, Form } from 'formik';
import { t } from 'i18next';
import { Typography } from '@mui/material';
import { SmsOutlined } from '@mui/icons-material';
import { Template } from '@devexpress/dx-react-core';
import {
  SortingState,
  SelectionState,
  PagingState,
  CustomPaging,
  IntegratedSelection,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  Toolbar,
  TableSelection,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import {
  SortingLabel,
  TableColumnVisibility,
  PagingPanel,
  TableContainer,
  TableHeaderCell,
  SaveButton,
  CancelButton,
  ActionButton,
  InputField,
  TableRow,
  TableCell,
  Wrapper,
  Popper,
  useHover,
} from '../../../../ui';
import { $opened, openViaUrl } from '../../models/detail.model';
import { $rowCount } from '../../models/people-page.model';
import {
  TableGate,
  quarantineSubmitted,
  $tableData,
  columns,
  $currentPage,
  $pageSize,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $columnOrder,
  $columnWidths,
  columnOrderChanged,
  columnWidthsChanged,
  excludedColumnsFromSort,
  $sorting,
  sortChanged,
  $selection,
  selectionChanged,
  hiddenColumnChanged,
  $hiddenColumnNames,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const Root = (props) => <Grid.Root {...props} style={{ height: '100%' }} />;

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => openViaUrl(id), []);

  const row = useMemo(
    () => (
      <TableRow
        {...props}
        isSelected={isSelected}
        onRowClick={onRowClick}
        style={{
          background: props.row.has_debts ? 'rgb(235, 87, 87,.15)' : 'rgb(255, 255, 255)',
        }}
      />
    ),
    [isSelected, props.children]
  );
  return row;
};

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const ActionFormatter = ({ value, row }) => {
  const { t } = useTranslation();
  const [mode, setMode] = useState('view');
  const isEditMode = mode === 'edit';

  return !value ? (
    isEditMode ? (
      <Formik
        initialValues={{ expired_at: '' }}
        onSubmit={(values) => {
          quarantineSubmitted({ userdata_id: row.id, ...values });
          setMode('view');
        }}
        render={() => (
          <Form
            style={{ display: 'flex', alignItems: 'center', wrap: 'nowrap' }}
            onClick={(e) => e.stopPropagation()}
          >
            <Field
              name="expired_at"
              component={InputField}
              divider={false}
              label={null}
              labelPlacement="start"
              placeholder={t('quarantinePeriod')}
              type="date"
              required
            />
            <SaveButton type="submit" style={{ margin: '0 5px' }} />
            <CancelButton style={{ margin: '0 5px' }} onClick={() => setMode('view')} />
          </Form>
        )}
      />
    ) : (
      <ActionButton
        onClick={(e) => {
          e.stopPropagation();
          setMode('edit');
        }}
      >
        {t('assignQuarantine')}
      </ActionButton>
    )
  ) : null;
};

const ActionTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={ActionFormatter} {...props} />
);

const CommentFormatter = ({ value, row }) => {
  const { t } = useTranslation();
  const { anchorEl, hover, handleMouseIn, handleMouseOut } = useHover('td');
  if (value === t('no')) {
    return value;
  }
  return (
    <span onMouseOver={handleMouseIn} onMouseOut={handleMouseOut}>
      <SmsOutlined color="primary" />
      <Popper open={hover} header={t('Label.comment')} anchorEl={anchorEl}>
        <Typography
          component="span"
          style={{
            color: '#65657B',
            fontSize: 13,
            lineHeight: '15px',
            fontWeight: 500,
          }}
        >
          {value}
        </Typography>
      </Popper>
    </span>
  );
};

const CommentTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={CommentFormatter} {...props} />
);

const columnsExtensions = [{ columnName: 'comment', align: 'center' }];

export const Table = memo((props) => {
  const { t } = useTranslation();
  const data = useStore($tableData);
  const totalCount = useStore($rowCount);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnOrder = useStore($columnOrder);
  const columnWidths = useStore($columnWidths);
  const sorting = useStore($sorting);
  const selection = useStore($selection);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  return (
    <>
      <TableGate />
      <Wrapper style={{ height: '100%', padding: 24 }}>
        <Grid
          rootComponent={Root}
          rows={data}
          getRowId={(row) => row.id}
          columns={columns}
        >
          <SelectionState selection={selection} onSelectionChange={selectionChanged} />

          <SortingState sorting={sorting} onSortingChange={sortChanged} />

          <PagingState
            currentPage={currentPage}
            onCurrentPageChange={pageNumberChanged}
            pageSize={pageSize}
            onPageSizeChange={pageSizeChanged}
          />
          <CustomPaging totalCount={totalCount} />

          <DragDropProvider />
          <IntegratedSelection />
          <ActionTypeProvider for={['actions']} />
          <CommentTypeProvider for={['comment']} />
          <DXTable
            columnExtensions={columnsExtensions}
            messages={{ noData: t('noData') }}
            rowComponent={Row}
            cellComponent={TableCell}
            containerComponent={TableContainer}
          />
          <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
          <TableColumnResizing
            columnWidths={columnWidths}
            onColumnWidthsChange={columnWidthsChanged}
          />
          <TableHeaderRow
            cellComponent={TableHeaderCell}
            showSortingControls
            sortLabelComponent={(props) => (
              <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
            )}
          />
          <TableSelection showSelectAll />
          <TableColumnVisibility
            hiddenColumnNames={hiddenColumnNames}
            onHiddenColumnNamesChange={hiddenColumnChanged}
          />
          <Toolbar rootComponent={ToolbarRoot} />
          <Template name="toolbarContent">
            <TableToolbar />
          </Template>
          <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
          <PagingPanel {...{ columns, hiddenColumnNames }} />
        </Grid>
      </Wrapper>
    </>
  );
});
