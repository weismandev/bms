import { memo } from 'react';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Wrapper, Tabs } from '../../../../ui';
import { VehiclesForm } from '../../../user-related-vehicles';
import {
  $mode,
  changedDetailVisibility,
  $opened,
  $currentTab,
  changeTab,
  tabs,
} from '../../models/detail.model';
import InfoTabForm from '../info-tab-form';
import ObjectTabForm from '../object-tab-form';

const Detail = memo(() => {
  const mode = useStore($mode);
  const opened = useStore($opened);
  const currentTab = useStore($currentTab);

  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(e, tab) => changeTab(tab)}
        isNew={isNew}
        tabEl={<Tab style={{ minWidth: '33.3%' }} />}
      />

      {currentTab === 'info' && <InfoTabForm mode={mode} isNew={isNew} opened={opened} />}

      {currentTab === 'objects' && (
        <ObjectTabForm mode={mode} isNew={isNew} opened={opened} />
      )}

      {currentTab === 'vehicles' && (
        <div style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
          <VehiclesForm
            changeDetailVisibility={changedDetailVisibility}
            userdata_id={opened.id}
          />
        </div>
      )}
    </Wrapper>
  );
});

export { Detail };
