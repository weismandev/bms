import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Table, DownloadableTable, Filter, GroupActions, Detail } from '..';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '../../../../ui';
import { $isDetailOpen } from '../../models/detail.model';
import { $isFilterOpen, $isGroupOpen } from '../../models/filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $acceptanceError,
  $isAcceptanceErrorDialogOpen,
  errorDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
  acceptanceErrorDialogVisibilityChanged,
  forceDeleteClicked,
} from '../../models/people-page.model';

export const PeoplePage = (props) => {
  const { t } = useTranslation();
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);
  const isGroupOpen = useStore($isGroupOpen);

  const acceptanceError = useStore($acceptanceError);
  const isAcceptanceErrorDialogOpen = useStore($isAcceptanceErrorDialogOpen);

  const getFilter = () => {
    if (isFilterOpen) return <Filter />;
    if (isGroupOpen) return <GroupActions />;
    return null;
  };

  const filter = getFilter();

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />

      <DeleteConfirmDialog
        header={t('attention')}
        content={acceptanceError}
        isOpen={isAcceptanceErrorDialogOpen}
        close={() => acceptanceErrorDialogVisibilityChanged(false)}
        confirm={() => forceDeleteClicked()}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<Table />}
        filter={filter}
        detail={isDetailOpen && <Detail />}
      />

      <DownloadableTable />
    </>
  );
};
