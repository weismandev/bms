import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SelectField,
} from '@ui/index';
import { HouseSelectField } from '../../../house-select';
import { TypesOwnershipSelectField } from '../../../types-ownership-select';
import { $peopleStatuses } from '../../models/detail.model';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const Filter = memo(() => {
  const filters = useStore($filters);
  const peopleStatuses = useStore($peopleStatuses);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={(bag) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  label={t('buildings')}
                  placeholder={t('selectHouses')}
                  isMulti
                />
                <Field
                  name="statuses"
                  component={SelectField}
                  label={t('statuses')}
                  placeholder={t('selectStatuses')}
                  options={peopleStatuses}
                  isMulti
                />
                <Field
                  name="types"
                  component={TypesOwnershipSelectField}
                  label={t('ownershipTypes')}
                  placeholder={t('chooseTypes')}
                  isMulti
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
