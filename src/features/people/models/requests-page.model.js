import { createEvent, createStore, createEffect, restore, merge } from 'effector';
import { createGate } from 'effector-react';

export const fxGetRequestsList = createEffect();
export const fxAcceptRequest = createEffect();
export const fxRejectRequest = createEffect();

export const PageGate = createGate();
export const pageMounted = PageGate.open;
export const pageUnmounted = PageGate.close;

export const errorDialogVisibilityChanged = createEvent();

export const errorOccured = merge([
  fxGetRequestsList.failData,
  fxAcceptRequest.failData,
  fxRejectRequest.failData,
]);

export const requestPending = merge([
  fxGetRequestsList.pending,
  fxAcceptRequest.pending,
  fxRejectRequest.pending,
]);

export const $isLoading = restore(requestPending, false);
export const $error = restore(errorOccured, null);
export const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);

export const $raw = createStore({ data: [], meta: { total: 0 } });
export const $count = $raw.map(({ meta }) => meta.total);
export const $normalized = $raw.map(({ data }) =>
  data.reduce((acc, request) => ({ ...acc, [request.id]: request }), {})
);
