import { createEvent, createStore } from 'effector';
import Ok from '@mui/icons-material/Done';
import NotOk from '@mui/icons-material/HighlightOff';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const columns = [
  {
    field: 'is_valid',
    headerName: t('Label.status'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'center',
    headerAlign: 'center',
    renderCell: (cell) => {
      const icon = cell.value.state ? (
        <Ok className="text-green" />
      ) : (
        <NotOk className="text-red" />
      );

      return <span>{icon}</span>;
    },
  },
  {
    field: 'city',
    headerName: t('Label.city'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'street',
    headerName: t('Label.street'),
    width: 250,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'building',
    headerName: t('Label.building'),
    width: 100,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'flat',
    headerName: t('Label.flat'),
    width: 100,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'name',
    headerName: t('Label.name'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'lastname',
    headerName: t('Label.lastname'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'patronymic',
    headerName: t('Label.patronymic'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'phone',
    headerName: t('Label.phone'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'email',
    headerName: t('Label.email'),
    width: 200,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'comment',
    headerName: t('Label.comment'),
    width: 250,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
];

export const downloadableTableVisibilityChanged = createEvent();
export const confirmTask = createEvent();
export const cancelTask = createEvent();
export const cancelExistTask = createEvent();

export const $isDownloadaleTableOpen = createStore(false);
export const $raw = createStore([]);
export const $tableData = createStore([]);
