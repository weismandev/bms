import { createEvent, restore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const tabsList = [
  {
    value: 'people',
    label: t('activeResidents'),
    icon: 'People',
  },
  {
    value: 'requests',
    label: t('applicationJoin'),
    icon: 'PersonAdd',
  },
];

export const changeTab = createEvent();
export const $currentTab = restore(changeTab, 'people');
