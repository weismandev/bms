import { sample } from 'effector';
import { signout } from '../../common';
import { $filters } from './requests-filter.model';
import {
  pageMounted,
  pageUnmounted,
  fxGetRequestsList,
  fxAcceptRequest,
  fxRejectRequest,
  $normalized,
} from './requests-page.model';
import {
  acceptRequest,
  rejectClicked,
  rejectConfirmed,
  rejectDialogVisibilityChanged,
  $currentPage,
  $willReject,
  $rejectMssg,
  $tableParams,
  $isRejectDialogOpen,
} from './requests-table.model';

sample({
  source: { params: $tableParams, filters: $filters },
  clock: [pageMounted, $tableParams, $filters],
  fn: ({ params, filters }) => ({
    verified: filters.status.value,
    per_page: params.per_page,
    page: params.page,
    search_query: params.search,
  }),
  target: fxGetRequestsList,
});

$currentPage.reset([$filters]);

$rejectMssg.reset([rejectDialogVisibilityChanged, rejectConfirmed]);

$isRejectDialogOpen.on(rejectClicked, () => true).on(rejectConfirmed, () => false);

sample({
  source: $normalized,
  clock: acceptRequest,
  fn: (requests, id) => ({
    userdata_id: requests[id].userdata.id,
    apartment_id: requests[id].apartment.id,
  }),
  target: fxAcceptRequest,
});

sample({
  source: $normalized,
  clock: rejectClicked,
  fn: (requests, id) => ({
    userdata_id: requests[id].userdata.id,
    apartment_id: requests[id].apartment.id,
  }),
  target: $willReject,
});

sample({
  source: $willReject,
  clock: rejectConfirmed,
  fn: (willReject, mssg) => ({ ...willReject, comment: mssg }),
  target: fxRejectRequest,
});

$willReject.reset([pageUnmounted, signout]);
$isRejectDialogOpen.reset(signout);
$rejectMssg.reset(signout);
