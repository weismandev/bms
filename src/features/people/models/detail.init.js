import { sample, combine, forward, guard, merge } from 'effector';
import { condition, delay } from 'patronum';
import { signout, $pathname, history } from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { isNumber, isString } from '@tools/type';
import { $data as $buildings } from '../../house-select';
import { $data as $objectTypes } from '../../object-type-select';
// import { $data as $statuses } from '../../statuses-select';
import { $data as $rights } from '../../rights-select';
import { $data as $types } from '../../types-ownership-select';
import { peopleApi } from '../api';
import {
  $opened,
  $keys,
  entityApi,
  $mode,
  newEntity,
  $isDetailOpen,
  changeTab,
  $currentTab,
  $userObjects,
  $peopleStatuses,
  $objectsStatuses,
  addClicked,
  detailSubmitted,
  openViaUrl,
  changedDetailVisibility,
} from './detail.model';
import {
  $normalized,
  fxCreate,
  fxUpdate,
  fxGetKey,
  fxDelete,
  fxGetObjectsList,
  fxSaveObjectsList,
  fxGetById,
  deleteConfirmed,
  acceptanceDeleteErrorOccurred,
  defaultDeleteErrorOccurred,
  forceDeleteClicked,
  fxForceDeleteItem,
  pageMounted,
  pageUnmounted,
  $path,
  $entityId,
} from './people-page.model';

fxForceDeleteItem.use(peopleApi.forceDeleteItem);

$keys.on(fxGetKey.done, (_, { result }) => result?.key ?? []);

const prependEntityId = ({ entityId }) => entityId;
guard({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: [fxGetById.prepend(prependEntityId), fxGetKey.prepend(prependEntityId)],
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (path, { result: { id } }) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxDelete.done,
  source: $path,
  fn: (path, { params }) => {
    if (params) {
      history.push(`../${path}`);
    }
  },
});

const $bag = combine({
  normalized: $normalized,
  buildings: $buildings,
  statuses: $peopleStatuses,
  rights: $rights,
  types: $types,
});

$currentTab
  .on(changeTab, (state, tab) => tab)
  .on(addClicked, () => 'info')
  .reset(signout);

const openWithBag = sample($bag, fxGetById.done, (bag, { result }) => ({
  bag,
  id: result.userdata && result.userdata.id,
}));

forward({ from: openViaUrl, to: [fxGetById, fxGetKey] });

const openAfterCreateWithBag = sample($bag, fxCreate.done, (bag, { result }) => ({
  bag,
  id: result.id,
}));

const openWithUpdatedBag = sample($opened, $bag, ({ id }, bag) => ({
  bag,
  id,
})).updates.filter({
  fn: ({ id, bag }) => Boolean(id) && Boolean(bag.normalized[id]),
});

$opened
  .on([openWithBag, openWithUpdatedBag, openAfterCreateWithBag], (state, { bag, id }) =>
    formatOpened({ bag, id })
  )
  .on([fxDelete.done, fxGetById.fail, fxForceDeleteItem.done], () => newEntity)
  .reset(pageUnmounted);

$userObjects.reset([
  openViaUrl,
  fxDelete.done,
  fxForceDeleteItem.done,
  addClicked,
  signout,
]);

sample({
  source: [$objectTypes, $objectsStatuses],
  clock: [fxGetObjectsList.doneData, fxSaveObjectsList.doneData],
  fn: ([types, statuses], { objects }) => formatUserObjects(types, statuses, objects),
  target: $userObjects,
});

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetById.done, () => true)
  .on([fxDelete.done, fxGetById.fail, fxForceDeleteItem.done], () => false)
  .reset(pageUnmounted);

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$mode.on([fxCreate.done, fxUpdate.done, fxSaveObjectsList.done], () => 'view');

sample({
  source: $opened,
  clock: forceDeleteClicked,
  fn: ({ id }) => id,
  target: fxForceDeleteItem,
});

condition({
  source: fxDelete.fail,
  if: ({ error }) => error.error_code === 'has_approval_schedule',
  then: acceptanceDeleteErrorOccurred,
  else: defaultDeleteErrorOccurred,
});

guard({
  source: sample($currentTab, entityApi.create, (tab, payload) => ({
    tab,
    payload,
  })),
  filter: ({ tab }) => tab === 'info',
  target: fxCreate.prepend(formatPayload),
});

guard({
  source: sample($currentTab, entityApi.update, (tab, payload) => ({
    tab,
    payload,
  })),
  filter: ({ tab }) => tab === 'info',
  target: fxUpdate.prepend(formatPayload),
});

guard({
  source: sample({
    source: { tab: $currentTab, opened: $opened },
    clock: detailSubmitted,
    fn: ({ tab, opened }, payload) => ({ tab, opened, payload }),
  }),
  filter: ({ tab, opened }) => tab === 'objects' && Boolean(opened.id),
  target: fxSaveObjectsList.prepend(formatObjectsPayload),
});

forward({
  from: sample($opened, deleteConfirmed, ({ id }) => id),
  to: fxDelete,
});

guard({
  source: merge([
    sample($opened, changeTab, (opened, tab) => ({
      id: opened.id,
      tab,
    })),
    sample($currentTab, openViaUrl, (tab, id) => ({ tab, id })),
  ]),
  filter: ({ id, tab }) => Boolean(id) && tab === 'objects',
  target: fxGetObjectsList.prepend(({ id }) => id),
});

function formatOpened({ bag, id }) {
  const {
    avatar,
    accounts,
    phone,
    email,
    title,
    comment,
    apartment_title,
    apartment_id,
    ownership_numerator,
    ownership_denominator,
    status,
    rights,
    typeOwnership,
    building_id,
    surname,
    name,
    patronymic,
    readonly,
  } = bag.normalized[id];

  const [title_surname, title_name, title_patronymic] = title.split(' ');

  return {
    surname: surname || title_surname || '',
    name: name || title_name || '',
    patronymic: patronymic || title_patronymic || '',
    id,
    avatar: avatar || '',
    fullname: title || '',
    building: building_id || building_id === 0 ? String(building_id) : '',
    apartment: {
      id: apartment_id || '',
      title: apartment_title || '',
    },
    statuses:
      (Array.isArray(status) &&
        status.length > 0 &&
        status.reduce((acc, stat) => [...acc, String(stat)], [])) ||
      '',
    type: Boolean(typeOwnership) || typeOwnership === 0 ? String(typeOwnership) : '',
    phone,
    email: email || '',
    comment: comment || '',
    size: `${ownership_numerator || '0'}/${ownership_denominator || '1'}`,
    rights: (Array.isArray(rights) && rights.length > 0 && Number(rights[0])) || '',
    accounts: accounts || [],
    account_number:
      Array.isArray(accounts) && accounts.length > 0
        ? Boolean(accounts[0].account_number) && accounts[0].account_number
        : '',
    readonly,
  };
}

function formatUserObjects(types, statuses, objects) {
  return Array.isArray(objects)
    ? objects.map((object) => {
        const {
          id,
          building_id,
          type,
          ownership,
          zone = '',
          number = '',
          title = '',
        } = object;

        const userObject = {
          building: building_id || '',
          type: types.find((i) => String(i.slug) === String(type)) || '',
          share:
            ownership && ownership.share
              ? `${ownership.share.numerator || 0}/${ownership.share.denominator || 0}`
              : '',
          ownership_type:
            ownership && ownership.type !== 'none'
              ? statuses.find((i) => String(i.id) === String(ownership.type))
              : '',
        };

        if (type === 'flat') userObject.entity = id;
        if (type === 'parking_slot')
          userObject.entity = {
            id,
            zone: zone || '',
            number: number || '[ - ]',
          };
        if (type === 'pantry') {
          userObject.share = ownership
            ? `${ownership.numerator || 0}/${ownership.denominator}`
            : '';

          userObject.entity = { id, title };
        }

        return userObject;
      })
    : [];
}

function formatPayload({ payload }) {
  const {
    avatar,
    building,
    rights,
    statuses,
    apartment,
    type,
    fullname,
    size,
    account_number,
    surname,
    name,
    patronymic,
    ...restParams
  } = payload;

  return {
    title: `${surname} ${name} ${patronymic}`,
    surname,
    name,
    patronymic,
    avatar: typeof avatar === 'string' ? avatar : avatar.file,
    building_id:
      Boolean(building) && typeof building === 'object'
        ? building.id
        : typeof building === 'number' && !Number.isNaN(building)
        ? String(building)
        : typeof building === 'string' && /^\d*$/.test(building)
        ? building
        : '',
    apartment_id: apartment && apartment.id ? apartment.id : '',
    apartment_title: apartment && apartment.title ? apartment.title : '',
    rights:
      Boolean(rights) && typeof rights === 'object'
        ? [rights.id]
        : typeof rights === 'number' && !Number.isNaN(rights)
        ? [rights]
        : typeof rights === 'string' && /^\d*$/.test(rights)
        ? [Number(rights)]
        : [],
    status: statuses.map((stat) =>
      Boolean(stat) && typeof stat === 'object'
        ? stat.id
        : typeof stat === 'number' && !Number.isNaN(stat)
        ? String(stat)
        : typeof stat === 'string' && /^\d*$/.test(stat)
        ? stat
        : ''
    ),
    typeOwnership:
      Boolean(type) && typeof type === 'object'
        ? type.id
        : typeof type === 'number' && !Number.isNaN(type)
        ? String(type)
        : typeof type === 'string' && /^\d*$/.test(type)
        ? type
        : '',
    account_number:
      Boolean(account_number) &&
      (typeof account_number === 'string' || typeof account_number === 'number')
        ? account_number
        : '',
    ...getNumeratorDenominator(size),
    ...restParams,
  };
}

function formatObjectsPayload({ opened, payload }) {
  return {
    userdata_id: opened.id,
    objects: payload.objects.map((object) => {
      const share = getNumeratorDenominator(object.share);

      return {
        id:
          isNumber(object.entity) || isString(object.entity)
            ? object.entity
            : object.entity.id,
        type:
          isString(object.type) || isNumber(object.type)
            ? object.type
            : object.type && object.type.slug,
        ownership_type:
          isString(object.ownership_type) || isNumber(object.ownership_type)
            ? object.ownership_type
            : object.ownership_type && object.ownership_type.id,

        share_numerator: share.ownership_numerator,
        share_denominator: share.ownership_denominator,
      };
    }),
  };
}

function getNumeratorDenominator(string) {
  const matches =
    string &&
    typeof string === 'string' &&
    string.match(/(?<ownership_numerator>\d+)\/(?<ownership_denominator>\d+)/);

  return {
    ownership_numerator:
      matches && matches.groups ? matches.groups.ownership_numerator : '',
    ownership_denominator:
      matches && matches.groups ? matches.groups.ownership_denominator : '',
  };
}
