import { createEvent, createStore, combine } from 'effector';
import { createGate } from 'effector-react';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { formatPhone } from '../../../tools/formatPhone';
import { $data as $rights } from '../../rights-select';
import { $data as $typesOwnership } from '../../types-ownership-select';
import { $peopleStatuses } from './detail.model';
import { $raw } from './people-page.model';

const { t } = i18n;

export const columns = [
  { name: 'title', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'email', title: t('Label.email') },
  { name: 'size', title: t('Label.sizeStake') },
  { name: 'typeOwnership', title: t('Label.typeProperty') },
  { name: 'comment', title: t('Label.comment') },
  { name: 'address', title: t('Label.address') },
  { name: 'userdata_use_last', title: t('Label.lastActivity') },
  { name: 'status', title: t('Label.status') },
  { name: 'hasDebts', title: t('Label.debt') },
  { name: 'last_receipt_date', title: t('Label.dateLastReceipt') },
  { name: 'actions', title: t('Label.actions') },
  { name: 'account_number', title: t('Label.personalAccount') },
  { name: 'created_at', title: t('Label.creationDatesAccounts') },
];

export const excludedColumnsFromSort = [
  'size',
  'typeOwnership',
  'rights',
  'comment',
  'hasDebts',
  'status',
  'actions',
  'account_number',
  'created_at',
];

const getLastDate = (array) => {
  const [lastDate] = array
    .map((item) => item.last_operation_date)
    .sort()
    .reverse();

  return lastDate ? format(new Date(lastDate), 'dd.MM.yyyy HH:mm') : t('isNotDefinedshe');
};

export const TableGate = createGate();
export const tableMounted = TableGate.open;
export const tableUnmounted = TableGate.close;

export const $tableData = combine(
  $raw,
  $rights,
  $peopleStatuses,
  $typesOwnership,
  ({ data }, rights, statuses, types) =>
    data.map(
      ({
        id,
        phone,
        email,
        ownership_numerator,
        ownership_denominator,
        title,
        typeOwnership,
        comment,
        apartment,
        apartment_title,
        last_active_rfc,
        status,
        has_debts,
        on_quarantine,
        accounts,
        rights: itemRights,
        created_at,
      }) => ({
        id,
        title: title || t('isNotDefinedit'),
        phone: phone ? `+${phone}` : t('isNotDefined'),
        email: email || t('isNotDefined'),
        size: `${ownership_numerator || '0'} / ${ownership_denominator || '0'}`,
        typeOwnership: getTypeOwnership(typeOwnership, types),
        rights: getRight(itemRights, rights),
        comment: comment || t('thereIsNo'),
        address: apartment || apartment_title || t('isNotDefined'),
        userdata_use_last: last_active_rfc
          ? format(new Date(last_active_rfc), 'dd.MM.yyyy HH:mm')
          : t('isNotDefinedit'),
        status: getStatus(status, statuses),
        has_debts,
        hasDebts: has_debts ? t('yes') : t('no'),
        actions: on_quarantine,
        last_receipt_date:
          Array.isArray(accounts) && accounts.length
            ? getLastDate(accounts)
            : t('isNotDefinedshe'),
        account_number:
          Array.isArray(accounts) && accounts.length
            ? Boolean(accounts[0].account_number) && accounts[0].account_number
            : t('isNotSpecified'),
        created_at: created_at
          ? format(new Date(created_at * 1000), 'dd.MM.yyyy HH:mm')
          : t('isNotDefinedshe'),
      })
    )
);

export const quarantineSubmitted = createEvent();
export const selectionChanged = createEvent();
export const exportListClicked = createEvent();
export const downloadTemplateClicked = createEvent();
export const fileUploaded = createEvent();
export const updateUserList = createEvent();

export const $selection = createStore([]);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: [{ columnName: 'actions', width: 300 }],
});

function getRight(itemRights, rights) {
  const hasLeastOneRight = Array.isArray(itemRights) && itemRights.length > 0;
  let result = t('isNotDefinedits');

  if (hasLeastOneRight) {
    const [firstRight] = itemRights;
    const matchRight = rights.find((right) => String(right.id) === String(firstRight));
    result = matchRight ? matchRight.title : result;
  }

  return result;
}

function getStatus(itemStatuses, statuses) {
  const hasLeastOneStatus = Array.isArray(itemStatuses) && itemStatuses.length > 0;
  let result = t('isNotDefined');

  if (hasLeastOneStatus) {
    const [firstStatus] = itemStatuses;
    const matchStatus = statuses.find(
      (status) => String(status.id) === String(firstStatus)
    );
    result = matchStatus ? matchStatus.title : result;
  }

  return result;
}

function getTypeOwnership(itemType, types) {
  const isValidType = Boolean(itemType) || itemType === 0;
  let result = t('isNotDefined');

  if (isValidType) {
    const mathcType = types.find((type) => String(type.id) === String(itemType));
    result = mathcType ? mathcType.title : result;
  }

  return result;
}
