import { restore, createEvent } from 'effector';
import { createFilterBag } from '../../../tools/factories';
import { signout } from '../../common';

const defaultFilters = {
  buildings: '',
  statuses: '',
  rights: '',
  types: '',
};

const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

const changedGroupVisibility = createEvent();

const $isGroupOpen = restore(changedGroupVisibility, false).reset([
  signout,
  changedFilterVisibility,
]);

$isFilterOpen.reset(changedGroupVisibility);

export {
  changedFilterVisibility,
  filtersSubmitted,
  $isFilterOpen,
  $filters,
  $isGroupOpen,
  changedGroupVisibility,
};
