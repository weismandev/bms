import i18n from '@shared/config/i18n';
import { createFilterBag } from '../../../tools/factories';

const { t } = i18n;

export const statusesOptions = [
  { value: 'all', label: t('allRequests') },
  { value: 'not_verified', label: t('rejected') },
  { value: 'verified', label: t('verifieds') },
  { value: 'waiting', label: t('expectingVerification') },
];

const defaultFilters = { status: { value: 'all', label: t('allRequests') } };

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
