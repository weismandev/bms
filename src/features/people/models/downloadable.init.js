import { forward, merge, attach, sample } from 'effector';

import i18n from '@shared/config/i18n';

import { getPathOr } from '../../../tools/path';
import { signout } from '../../common';
import {
  $raw,
  $tableData,
  downloadableTableVisibilityChanged,
  $isDownloadaleTableOpen,
  confirmTask,
  cancelTask,
  cancelExistTask,
} from './downloadable.model';
import { fxPreloadFile, fxConfirmTask, fxCancelTask } from './people-page.model';

const { t } = i18n;

const taskComplited = merge([fxConfirmTask.done, fxCancelTask.done]);

$raw
  .on(fxPreloadFile.done, (state, { result }) => result)
  .reset([taskComplited, signout]);

sample({
  clock: $raw,
  filter: (_, { task }) => Array.isArray(task.items) && task.state === 'ready',
  fn: (_, { task }) => task.items.map(({ comment, context, is_valid, }, key) => {
    const { city, street, building, flat, name, lastname, patronymic, phone, email } = context;

    // --- костыли для кривого бэка ---

    const validateString = (param) => !!param && typeof param === 'string' ? param : null;

    function fixPhone(param) {
      if (!validateString(param)) return null;
      if (param.startsWith('+')) return param;
      return `+${param}`;
    }

    // ---

    return {
      id: key,
      comment: validateString(comment) ?? t('isNotDefined'),
      is_valid: { state: is_valid, comment: validateString(comment) ?? t('isNotDefined') },
      city: validateString(city) ?? t('isNotDefined'),
      street: validateString(street) ?? t('isNotDefinedshe'),
      building: validateString(building) ?? t('isNotDefined'),
      flat: validateString(flat) ?? t('isNotDefinedshe'),
      name: validateString(name) ?? t('isNotDefinedit'),
      lastname: validateString(lastname) ?? t('isNotDefinedshe'),
      patronymic: validateString(patronymic) ?? t('isNotDefinedit'),
      phone: fixPhone(phone) ?? t('isNotDefined'),
      email: validateString(email) ?? t('isNotDefined'),
      errorTip: validateString(comment) ?? t('isNotDefined'),
    };
  }),
  target: $tableData,
});

$isDownloadaleTableOpen
  .on(downloadableTableVisibilityChanged, (state, visibility) => visibility)
  .on(fxPreloadFile.done, () => true)
  .on(taskComplited, () => false)
  .reset(signout);

forward({
  from: confirmTask,
  to: attach({
    effect: fxConfirmTask,
    source: $raw,
    mapParams: (params, { task }) => task.id,
  }),
});

forward({
  from: cancelTask,
  to: attach({
    effect: fxCancelTask,
    source: $raw,
    mapParams: (params, { task }) => task.id,
  }),
});

sample({
  clock: cancelExistTask,
  fn: (id) => id,
  target: fxCancelTask,
});
