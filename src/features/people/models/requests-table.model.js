import { createEvent, createStore, restore } from 'effector';
import format from 'date-fns/format';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $raw } from './requests-page.model';

const { t } = i18n;

export const columns = [
  { name: 'date', title: t('Label.date') },
  { name: 'time', title: t('Label.time') },
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'address', title: t('Label.address') },
  { name: 'email', title: t('Label.email') },
  { name: 'status', title: t('Label.status') },
  { name: 'actings', title: t('Label.actions') },
  { name: 'reject_comment', title: t('Label.comment') },
  { name: 'decision_made_at', title: t('Label.dateTimeAction') },
  { name: 'details', title: t('Label.Detailing') },
];

export const columnsWidths = [
  { columnName: 'date', width: 0 },
  { columnName: 'time', width: 100 },
  { columnName: 'fullname', width: 300 },
  { columnName: 'phone', width: 250 },
  { columnName: 'address', width: 300 },
  { columnName: 'email', width: 200 },
  { columnName: 'status', width: 200 },
  { columnName: 'actings', width: 300 },
  { columnName: 'decision_made_at', width: 300 },
  { columnName: 'reject_comment', width: 500 },
  { columnName: 'details', width: 200 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths: columnsWidths });

export const acceptRequest = createEvent();
export const rejectClicked = createEvent();
export const rejectConfirmed = createEvent();
export const rejectMessageChanged = createEvent();
export const rejectDialogVisibilityChanged = createEvent();

export const $willReject = createStore(null);
export const $isRejectDialogOpen = restore(rejectDialogVisibilityChanged, false);
export const $rejectMssg = restore(rejectMessageChanged, '');

export const $tableData = $raw.map(({ data }) => data.map(formatRequest));

function formatRequest({
  id,
  apartment,
  reject_comment,
  userdata,
  created_at,
  is_verified,
  decision_made_at,
  status,
  details,
}) {
  return {
    id,
    is_verified,
    status: formatVerifyStatus(is_verified, status),
    reject_comment: reject_comment || '',
    phone: userdata && userdata.phone ? userdata.phone : t('isNotSpecified'),
    email: userdata && userdata.email ? userdata.email : t('isNotSpecified'),
    time: created_at ? format(new Date(created_at), 'HH:mm') : '--:--',
    date: created_at
      ? format(new Date(created_at), 'dd MMMM yyyy', { locale: dateFnsLocale })
      : t('isNotKnown'),
    fullname: userdata && userdata.fullname ? userdata.fullname : t('isNotDefinedit'),
    address: apartment && apartment.title ? apartment.title : t('isNotSpecified'),
    decision_made_at:
      decision_made_at && decision_made_at.date
        ? format(new Date(decision_made_at.date), 'dd MMMM yyyy', {
            locale: dateFnsLocale,
          })
        : t('isNotKnown'),
    details,
  };
}

function formatVerifyStatus(is_verified, status) {
  switch (is_verified) {
    case null:
      return status.title;
    case true:
      return t('verified');
    default:
      return t('rejected');
  }
}
