import { createEffect, createStore, attach, createEvent, combine } from 'effector';
import { createPageBag, createDialogBag } from '../../../tools/factories';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
  $pathname,
} from '../../common';
import { peopleApi } from '../api';

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(
  peopleApi,
  { handleCreate, handleUpdate, handleGetById: handleUpdate },
  { idAttribute: 'id', itemsAttribute: 'userdata' }
);

export const {
  $isDialogOpen: $isAcceptanceErrorDialogOpen,
  dialogVisibilityChanged: acceptanceErrorDialogVisibilityChanged,
} = createDialogBag();

export const $acceptanceError = createStore(null);
export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export const defaultDeleteErrorOccurred = createEvent();
export const acceptanceDeleteErrorOccurred = createEvent();
export const forceDeleteClicked = createEvent();
export const startCheckTransfer = createEvent();
export const stopCheckTransfer = createEvent();
export const startInterval = createEvent();
export const stopInterval = createEvent();

export const fxAssignQuarantine = createEffect();
export const fxExportList = createEffect();
export const fxDownloadTemplate = createEffect();
export const fxPreloadFile = createEffect();
export const fxConfirmTask = createEffect();
export const fxCancelTask = createEffect();
export const fxGetObjectsList = createEffect();
export const fxSaveObjectsList = createEffect();
export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });
export const fxForceDeleteItem = createEffect();
export const fxGetLastTask = createEffect();
export const fxFirstGetLastTask = createEffect();
export const fxGetKey = createEffect();

export const $isTransferLoading = createStore(false);

function handleCreate(state, result, params) {
  const meta = { total: Number(state.meta.total) + 1 };
  const data = state.data.concat({ ...result, ...params });

  return { meta, data };
}

function handleUpdate(state, result, params) {
  let newState = { ...state };
  const updatedIdx = state.data.findIndex(
    (i) => String(i.id) === String(result.userdata.id)
  );

  if (updatedIdx > -1) {
    newState = {
      ...newState,
      data: newState.data
        .slice(0, updatedIdx)
        .concat({ ...state.data[updatedIdx], ...params, ...result.userdata })
        .concat(newState.data.slice(updatedIdx + 1)),
    };
  } else {
    newState = handleCreate(newState, result.userdata, params);
  }

  return newState;
}
