import { signout } from '../../common';
import { peopleApi } from '../api';
import {
  pageMounted,
  errorOccured,
  fxGetRequestsList,
  fxAcceptRequest,
  fxRejectRequest,
  $isErrorDialogOpen,
  $raw,
  $error,
  $isLoading,
} from './requests-page.model';

fxGetRequestsList.use(peopleApi.getRequestsList);
fxAcceptRequest.use(peopleApi.acceptRequest);
fxRejectRequest.use(peopleApi.rejectRequest);

$raw
  .on(fxGetRequestsList.doneData, (_, { requests, meta }) => ({
    data: requests,
    meta: { total: meta.total },
  }))
  .on(fxAcceptRequest.doneData, (raw, { request }) => updateRaw({ raw, request }))
  .on(fxRejectRequest.doneData, (raw, { request }) => updateRaw({ raw, request }))
  .reset([pageMounted, signout]);

$isErrorDialogOpen.on(errorOccured, () => true).reset(signout);
$error.reset(signout);
$isLoading.reset(signout);

function updateRaw({ raw, request }) {
  const idx = raw.data.findIndex((item) => item.id === request.id);
  if (idx === -1) return raw;

  return {
    ...raw,
    data: [...raw.data.slice(0, idx), request, ...raw.data.slice(idx + 1)],
  };
}
