import { signout } from '../../common';
import { peopleApi } from '../api';
import { fxGetList, $data, $isLoading, $error } from './pantry.model';

$data.on(fxGetList.done, (_, { result }) => result.items).reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

fxGetList.use(peopleApi.getPantries);
