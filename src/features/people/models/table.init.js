import { forward, merge } from 'effector';
import FileSaver from 'file-saver';
import format from 'date-fns/format';
import { mergeColumnWidths } from '../../../tools/merge-column-widths';
import { signout } from '../../common';
import { $filters } from './filter.model';
import {
  fxGetFromStorage,
  fxAssignQuarantine,
  fxDownloadTemplate,
} from './people-page.model';
import {
  quarantineSubmitted,
  $selection,
  selectionChanged,
  downloadTemplateClicked,
  $search,
  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
} from './table.model';

$selection.on(selectionChanged, (state, selection) => selection).reset(signout);

$currentPage.on(merge([$search, $filters]), () => 0);

$currentPage.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.currentPage : state
);
$pageSize.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.pageSize : state
);
$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  result ? Array.from(new Set([...result.columnOrder, ...state])) : state
);
$columnWidths.on(fxGetFromStorage.doneData, (state, data) =>
  mergeColumnWidths(state, data && data.columnWidths)
);

$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  result ? result.hiddenColumnNames : state
);

forward({ from: quarantineSubmitted, to: fxAssignQuarantine });
forward({ from: downloadTemplateClicked, to: fxDownloadTemplate });

fxDownloadTemplate.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `people-template-${dateTimeNow}.xlsx`);
});
