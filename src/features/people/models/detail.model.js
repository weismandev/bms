import { createEvent, createStore, combine } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

export const newEntity = {
  avatar: '',
  surname: '',
  name: '',
  patronymic: '',
  building: '',
  apartment: '',
  statuses: '',
  type: '',
  rights: '2',
  phone: '',
  email: '',
  size: '',
  comment: '',
};

export const newObject = {
  type: { slug: 'flat', title: t('flat') },
  building: null,
  entity: null,
  ownership_type: {
    title: t('owner'),
    id: '1',
  },
  share: '',
};

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'objects',
    onCreateIsDisabled: true,
    label: t('objects'),
  },
  {
    value: 'vehicles',
    onCreateIsDisabled: true,
    label: t('transport'),
  },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,
  openViaUrl,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const changeTab = createEvent();

export const $keys = createStore([]);

export const $bleKeys = combine($keys, (keys) =>
  keys.filter(({ interface_type }) => interface_type.includes('ble'))
);

export const $currentTab = createStore('info');
export const $userObjects = createStore([]);

export const $objectsStatuses = createStore([
  {
    title: t('owner'),
    id: '1',
  },
  {
    title: t('tenant'),
    id: '4',
  },
  {
    title: t('user'),
    id: '8',
  },
]);

export const $peopleStatuses = createStore([
  {
    title: t('owner'),
    id: '1',
  },
  {
    title: t('resident'),
    id: '3',
  },
  {
    title: t('tenant'),
    id: '4',
  },
  {
    title: t('familyMember'),
    id: '5',
  },
  {
    title: t('child'),
    id: '7',
  },
]);
