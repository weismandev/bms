import { forward, attach, merge, guard, combine, sample } from 'effector';
import { interval } from 'patronum/interval';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { createIntervalBag } from '@features/widgets/tools/interval';
import { $isAuthenticated, push } from '../../common';
import { fxGetList as fxGetTypesOwnershipList } from '../../types-ownership-select';
import { peopleApi } from '../api';
import { cancelExistTask } from './downloadable.model';
import { $filters } from './filter.model';
import {
  pageMounted,
  pageUnmounted,
  fxGetList,
  fxAssignQuarantine,
  fxExportList,
  fxDownloadTemplate,
  fxPreloadFile,
  fxGetKey,
  $raw,
  fxConfirmTask,
  fxCancelTask,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  fxGetObjectsList,
  fxSaveObjectsList,
  fxSaveInStorage,
  fxGetFromStorage,
  fxDelete,
  $acceptanceError,
  $isAcceptanceErrorDialogOpen,
  defaultDeleteErrorOccurred,
  acceptanceDeleteErrorOccurred,
  fxForceDeleteItem,
  fxGetLastTask,
  fxFirstGetLastTask,
  $isTransferLoading,
  startCheckTransfer,
  stopCheckTransfer,
  startInterval,
  stopInterval,
} from './people-page.model';
import {
  $tableParams,
  exportListClicked,
  $selection,
  fileUploaded,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  $currentPage,
  updateUserList,
} from './table.model';

const $savedParams = combine({
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnOrder: $columnOrder,
  columnWidths: $columnWidths,
  hiddenColumnNames: $hiddenColumnNames,
  filter: $filters,
});

fxAssignQuarantine.use(peopleApi.assignQuarantine);
fxExportList.use(peopleApi.exportList);
fxDownloadTemplate.use(peopleApi.downloadTemplate);
fxPreloadFile.use(peopleApi.preloadFile);
fxConfirmTask.use(peopleApi.confirmTask);
fxCancelTask.use(peopleApi.cancelTask);
fxGetObjectsList.use(peopleApi.getObjectsList);
fxSaveObjectsList.use(peopleApi.saveObjectsList);
fxGetLastTask.use(peopleApi.getLastTask);
fxFirstGetLastTask.use(peopleApi.getLastTask);
fxGetKey.use(peopleApi.getKey);

$isLoading.on(
  merge([
    fxAssignQuarantine.pending,
    fxExportList.pending,
    fxDownloadTemplate.pending,
    fxConfirmTask.pending,
    fxCancelTask.pending,
    fxGetObjectsList.pending,
    fxSaveObjectsList.pending,
    fxForceDeleteItem.pending,
  ]),
  (state, isLoading) => isLoading
);

const errorOccured = merge([
  fxAssignQuarantine.fail,
  fxExportList.fail,
  fxDownloadTemplate.fail,
  fxPreloadFile.fail,
  fxConfirmTask.fail,
  fxCancelTask.fail,
  fxGetObjectsList.fail,
  fxSaveObjectsList.fail,
]);

$error
  .on(errorOccured, (state, { error }) => error)
  .on(defaultDeleteErrorOccurred, (state, { error }) => error.message)
  .off(fxDelete.fail);

$isErrorDialogOpen
  .on([errorOccured, defaultDeleteErrorOccurred], () => true)
  .off(fxDelete.fail);

$acceptanceError.on(acceptanceDeleteErrorOccurred, (_, { error }) => error.message);

$isAcceptanceErrorDialogOpen
  .on(acceptanceDeleteErrorOccurred, () => true)
  .reset(fxForceDeleteItem);

$raw
  .on(fxAssignQuarantine.done, ({ data = [], meta }, { params: { userdata_id } }) => {
    const personOnQuarantineIdx = data.findIndex(
      (person) => String(person.id) === String(userdata_id)
    );

    let newData = [...data];

    if (personOnQuarantineIdx > -1) {
      newData = data
        .slice(0, personOnQuarantineIdx)
        .concat({ ...data[personOnQuarantineIdx], on_quarantine: 1 })
        .concat(data.slice(personOnQuarantineIdx + 1));
    }

    return {
      meta,
      data: newData,
    };
  })
  .on(fxForceDeleteItem.done, (state, { params: id }) => {
    const meta = { ...state.meta, total: state.meta.total - 1 };
    const data = state.data.filter((item) => String(item.id) !== String(id));

    return { meta, data };
  });

forward({
  from: exportListClicked,
  to: attach({
    effect: fxExportList,
    source: [$tableParams, $filters, $selection],
    mapParams: collectPayload,
  }),
});

forward({
  from: [pageMounted, $tableParams, $filters, fxConfirmTask.done, updateUserList],
  to: attach({
    effect: fxGetList,
    source: [$tableParams, $filters],
    mapParams: collectPayload,
  }),
});

forward({ from: pageMounted, to: fxGetTypesOwnershipList });

guard({
  source: $savedParams,
  filter: $isAuthenticated,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (params) => ({ data: params, storage_key: 'people' }),
  }),
});

forward({
  from: pageMounted,
  to: fxGetFromStorage.prepend(() => ({ storage_key: 'people' })),
});

fxExportList.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `people-list-${dateTimeNow}.xls`);
});

/* Логика проверки трансфера csv */

// При маунте проверяем есть ли таски в работе
sample({
  source: pageMounted,
  target: fxFirstGetLastTask,
});
// Если есть, отменяем все
fxFirstGetLastTask.doneData.watch((data) => {
  if (data.task_exists) {
    cancelExistTask(data.task.id);
  }
});

forward({ from: fileUploaded, to: fxPreloadFile });

$isTransferLoading
  .on(
    merge([fxPreloadFile.pending, fxConfirmTask.pending, fxCancelTask.pending]),
    (state, isLoading) => isLoading
  )
  .on(startCheckTransfer, () => true)
  .reset(pageUnmounted, stopCheckTransfer);

// После подтверждения начинаем опрос по статусу таски
sample({
  clock: fxConfirmTask.done,
  target: startCheckTransfer,
});

// Интервал на опрос таски
const { tick } = interval({
  timeout: 1000,
  start: startCheckTransfer,
  stop: stopCheckTransfer,
});

sample({
  clock: tick,
  target: fxGetLastTask,
});

// Если во врмея опроса таска была завершена или уже удалилась из очереди,
// то останавливаем интервальные запросы
fxGetLastTask.doneData.watch((data) => {
  if (data.task_exists) {
    switch (data.task.state) {
      case 'done':
        stopCheckTransfer();
      case 'fail':
      case 'skip':
        push({
          hader: 'Импорт',
          content: 'Операция завершилась с ошибкой',
          color: 'red',
        });
        stopCheckTransfer();
    }
  }
  if (!data.task_exists) stopCheckTransfer();
});

/* --- */

function collectPayload(params, [tableParams, filters, selection]) {
  const payload = {};

  if (tableParams.sorting.length > 0) {
    payload.sort = tableParams.sorting[0].order;
    payload.column = tableParams.sorting[0].name;
  }

  if (tableParams.search) {
    payload.search = tableParams.search;
  }

  if (tableParams.per_page) {
    payload.limit = tableParams.per_page;
  }

  if (tableParams.page) {
    payload.offset = tableParams.per_page * (tableParams.page - 1);
  }

  if (Array.isArray(selection) && selection.length > 0) {
    payload.ids = selection;
  }

  if (typeof filters === 'object') {
    if (Array.isArray(filters.buildings) && filters.buildings.length > 0) {
      payload.buildings = filters.buildings.map((build) => build.id);
    }

    if (Array.isArray(filters.statuses) && filters.statuses.length > 0) {
      payload.statuses = filters.statuses.map((status) => status.id);
    }

    if (Array.isArray(filters.rights) && filters.rights.length > 0) {
      payload.rights = filters.rights.map((right) => right.id);
    }

    if (Array.isArray(filters.types) && filters.types.length > 0) {
      payload.types = filters.types.map((type) => type.id);
    }

    if (filters.share_size_min) {
      payload.share_size_min = filters.share_size_min;
    }

    if (filters.share_size_max) {
      payload.share_size_max = filters.share_size_max;
    }
  }

  return payload;
}
