export { PeoplePage as default } from './pages';

export { Detail } from './organisms';

export {
  open,
  $isDetailOpen,
  changedDetailVisibility,
  $opened,
  $isLoading,
  fxGetById,
} from './models';
