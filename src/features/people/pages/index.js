import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';

import { PeoplePage, RequestsPage } from '../organisms';

import { $currentTab } from '../models/page.model';
import { $path } from '../models/people-page.model';

const PageRoot = () => {
  const currentTab = useStore($currentTab);

  const isRequestsTabOpen = currentTab === 'requests';

  return isRequestsTabOpen ? <RequestsPage /> : <PeoplePage />;
};

const RestrictedPeoplePage = (props) => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <PageRoot {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedPeoplePage as PeoplePage };
