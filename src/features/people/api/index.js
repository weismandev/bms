import { api } from '../../../api/api2';


const getList = (payload, config = {}) =>
  api.no_vers('get', 'admin/get-userdata', payload, config);
const getById = (id) =>
  api
    .no_vers('get', 'admin/get-userdata', { id })
    .then((res) => ({ userdata: res.userdata[0] }));

const create = (payload) => api.no_vers('post', 'user/update-user-bms', payload);
const update = (payload) =>
  api.no_vers('post', 'user/update-user-bms', payload).then((res) => ({ userdata: res }));
const deleteItem = (id) =>
  api.no_vers('post', 'user/delete-user-bms', { id }, { fullError: true });
const forceDeleteItem = (id) =>
  api.no_vers('post', 'user/delete-user-bms', { id, force: true });
const assignQuarantine = (payload) => api.v1('post', 'quarantine/user/add', payload);

const exportList = (payload) =>
  api.no_vers(
    'request',
    'admin/get-userdata',
    { ...payload, limit: 1000000, export: 1, lang: localStorage.getItem('lang') },
    {
      config: {
        responseType: 'arraybuffer',
      },
    }
  );

const downloadTemplate = () => api.v1(
  'request',
  'crm-user/download-template',
  { lang: localStorage.getItem('lang') },
  {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  }
);

const preloadFile = (file) => api.v1('post', 'crm-user/preload-file', { import: file });

const confirmTask = (id) => api.v1('post', 'crm-user/confirm-task', { id });
const cancelTask = (id) => api.v1('post', 'crm-user/cancel-task', { id });
const resetPasswordAndNotify = (id) =>
  api.v1('post', 'auth-operation/reset-password-and-notify', { user_id: id });

const getObjectsList = (id) =>
  api.v1('get', 'users/related/objects/list', {
    userdata_id: id,
    types: ['flat', 'parking_slot', 'pantry'],
  });

const saveObjectsList = (payload) =>
  api.v1('post', 'users/related/objects/save', {
    types: ['flat', 'parking_slot', 'pantry'],
    ...payload,
  });

const getRequestsList = (payload) => api.v1('get', 'residents/requests/list', payload);
const acceptRequest = (payload) => api.v1('post', 'residents/requests/accept', payload);
const rejectRequest = (payload) => api.v1('post', 'residents/requests/reject', payload);

const getPantries = (payload) => api.v1('get', 'property/get-list-crm', payload);

const getLastTask = () => api.v1('get', 'crm-user/last-task');

const getKey = (user_id) =>
  api.no_vers('get', 'acms/get-key', {
    user_id,
    type: 'universal',
    use_bms: 1,
    key_format: 'hex4bits',
  });

export const peopleApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteItem,
  forceDeleteItem,
  assignQuarantine,
  exportList,
  downloadTemplate,
  preloadFile,
  confirmTask,
  cancelTask,
  resetPasswordAndNotify,
  getObjectsList,
  saveObjectsList,
  getRequestsList,
  acceptRequest,
  rejectRequest,
  getPantries,
  getLastTask,
  getKey,
};
