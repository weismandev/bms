import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { DeleteConfirmDialog, BaseInput } from '../../../../ui';
import {
  $rejectMssg,
  $isRejectDialogOpen,
  rejectMessageChanged,
  rejectConfirmed,
  rejectDialogVisibilityChanged,
} from '../../models/requests-table.model';

export const RequestRejectDialog = (props) => {
  const { t } = useTranslation();
  const rejectMssg = useStore($rejectMssg);
  const isRejectDialogOpen = useStore($isRejectDialogOpen);

  const rejectInput = (
    <BaseInput
      onChange={(e) => rejectMessageChanged(e.target.value)}
      value={rejectMssg}
      placeholder={t('Placeholder.TheReasonForRejection')}
      style={{ margin: '24px 0 24px' }}
      rowsMax={5}
      multiline
      fullWidth
    />
  );

  return (
    <DeleteConfirmDialog
      isOpen={isRejectDialogOpen}
      close={() => rejectDialogVisibilityChanged(false)}
      confirm={() => rejectConfirmed(rejectMssg)}
      header={t('rejectionOfAnApplication')}
      content={rejectInput}
    />
  );
};
