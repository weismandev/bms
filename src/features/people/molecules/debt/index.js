import { useTranslation } from 'react-i18next';
import { Divider } from '../../../../ui';

export const Debt = (props) => {
  const { t } = useTranslation();
  const { company_title, account_number, service_title, balance } = props;
  const style = {
    li: { display: 'flex', justifyContent: 'space-between' },
    title: { marginRight: '40px' },
    value: { textAlign: 'end' },
  };
  return (
    <>
      <li className="label" style={style.li}>
        <span style={style.title}>{t('company')}</span>
        <span style={style.value}>
          <b>{company_title}</b>
        </span>
      </li>
      <li className="label" style={style.li}>
        <span style={style.title}>{t('service')}</span>
        <span style={style.value}>
          <b>{service_title}</b>
        </span>
      </li>
      <li className="label" style={style.li}>
        <span style={style.title}>№ {t('numberAccount')}</span>
        <span style={style.value}>
          <b>{account_number}</b>
        </span>
      </li>
      <li className="label" style={style.li}>
        <span style={style.title}>{t('balance')}</span>
        <span className="text-red" style={style.value}>
          <b>{balance} р.</b>
        </span>
      </li>
      <Divider margin="5px" />
    </>
  );
};
