import { useTranslation } from 'react-i18next';
import { Debt } from '../debt';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  label: {
    margin: '20px 0 10px',
    fontSize: '1.25rem',
    fontWeight: '500',
  },
});

export const DebtList = (props) => {
  const { accounts = [] } = props;
  const { t } = useTranslation();
  const debts = accounts.filter((acc) => acc.balance < 0);

  const classes = useStyles();

  return debts.length !== 0 ? (
    <div>
      <div className={classes.label}>{t('Label.debt')}</div>
      <ul style={{ marginLeft: '20px' }}>
        {debts.map((debt) => (
          <Debt key={debt.id} {...debt} />
        ))}
      </ul>
    </div>
  ) : null;
};
