import { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  useMediaQuery,
  ClickAwayListener,
  Tooltip,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import * as Icons from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { ActionButton } from '../../../../ui';
import { tabsList, $currentTab, changeTab } from '../../models/page.model';

const useStyles = makeStyles({
  labelsList: {
    minWidth: 100,
    backgroundColor: '#fff',
    boxShadow: '0px 5px 15px #DCDCDC',
    padding: 0,
    margin: '14px 0',
    color: '#000',
  },
  commonMargin: {
    margin: '0 5px',
  },
  changerLabel: {
    whiteSpace: 'nowrap',
    '& > span': {
      paddingLeft: 10,
    },
  },
});

export const TableChanger = ({ isAnySideOpen }) => {
  const { t } = useTranslation();
  const { labelsList, commonMargin, changerLabel } = useStyles();
  const isWide = useMediaQuery('(max-width:1440px)') || isAnySideOpen;
  const currentTab = useStore($currentTab);
  const [isExpanded, changeExpand] = useState(false);

  const handleExpand = (state) => changeExpand(state);
  const handleChangeTable = (tableName) => {
    handleExpand(false);
    changeTab(tableName);
  };

  const getLTablesList = (list, tab, handler) => {
    const itemsList = list.map(({ value, label, icon }) => {
      const ItemIcon = Icons[icon];

      return (
        <ListItem
          button
          key={value}
          kind="positive"
          disabled={tab === value}
          onClick={() => handler(value)}
        >
          <ListItemIcon>
            <ItemIcon />
          </ListItemIcon>
          <ListItemText primary={label} />
        </ListItem>
      );
    });

    return <List>{itemsList}</List>;
  };

  const getChangerButton = (list, tab, handler) => {
    const { icon, label } = list.find(({ value }) => value === tab);
    const ButtonIcon = Icons[icon];

    return (
      <Tooltip title={isWide ? t('activeResidents') : ''}>
        <ActionButton
          kind="positive"
          onClick={() => handler(true)}
          classes={{ label: changerLabel }}
          style={{
            width: 'max-content',
            minWidth: 'unset',
            padding: !isWide ? '0 20px' : '0 5px',
          }}
        >
          <ButtonIcon />
          {!isWide ? <span>{label}</span> : null}
        </ActionButton>
      </Tooltip>
    );
  };

  const TablesList = useMemo(
    () => getLTablesList(tabsList, currentTab, handleChangeTable),
    [tabsList]
  );

  const ChangerButton = useMemo(
    () => getChangerButton(tabsList, currentTab, handleExpand),
    [currentTab, isWide]
  );

  return (
    <ClickAwayListener onClickAway={() => handleExpand(false)}>
      <Tooltip
        classes={{ tooltip: labelsList }}
        className={commonMargin}
        placement={isWide ? 'bottom' : 'bottom-end'}
        open={isExpanded}
        title={TablesList}
        disableFocusListener
        disableHoverListener
        disableTouchListener
      >
        {ChangerButton}
      </Tooltip>
    </ClickAwayListener>
  );
};
