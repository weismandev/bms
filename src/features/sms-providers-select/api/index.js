import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'sms-provider/crm/list');

export const smsProvidersApi = { getList };
