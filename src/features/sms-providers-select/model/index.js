import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { smsProvidersApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data.on(fxGetList.doneData, (_, { items }) => items).reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);

fxGetList.use(smsProvidersApi.getList);

export { fxGetList, $data, $error, $isLoading };
