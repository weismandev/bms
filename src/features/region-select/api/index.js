import { api } from '../../../api/api2';

const getList = () => api.v4('get', 'localities/list');

export const regionApi = { getList };
