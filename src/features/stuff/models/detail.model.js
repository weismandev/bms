import { createEvent, createEffect, createStore, restore, split } from 'effector';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

const newEntity = {
  id: null,
  building: '',
  enterprise: '',
  applicant: '',
  date: format(new Date(), 'yyyy-MM-dd'),
  estimated_time: format(new Date(), 'HH:mm'),
  accompanying_fullname: '',
  accompanying_phone: '',
  stuff_types: '',
  action_type: '',
  pass_number: '',
  status: null,
  registry: [],

  car_parking: false,
  brand: '',
  id_number: '',
  assigned_type: '',
  reference: '',
  time_from: '',
  time_to: '',
  comment: '',
};

const newStuffList = {
  items: [],
};

const tabsList = [
  {
    value: 'info',
    label: t('information'),
  },
  {
    value: 'registry',
    label: t('ListOfGoodsAndMaterials'),
    onCreateIsDisabled: true,
  },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

const changeTab = createEvent();
const changeStatus = createEvent();
const changeEnterprise = createEvent();

const changeStatusTo = split(changeStatus, {
  reject: ({ status }) => status === 'reject',
  approve: ({ status }) => status === 'approve',
  qualification: ({ status }) => status === 'qualification',
  coordinationTop: ({ status }) => status === 'coordination-top',

  issue: ({ status }) => status === 'issue',
  revoke: ({ status }) => status === 'revoke',
  forApproval: ({ status }) => status === 'for-approval',
});

const fxGetById = createEffect();
const fxSyncItemById = createEffect();
const fxCreate = createEffect();
const fxUpdate = createEffect();

const fxReject = createEffect();
const fxApprove = createEffect();
const fxQualification = createEffect();
const fxCoordinationTop = createEffect();

const fxIssue = createEffect();
const fxRevoke = createEffect();
const fxForApproval = createEffect();
const fxRegistryAssign = createEffect();

const $registry = createStore({ id: null, items: [] });
const $isLoading = createStore(false);
const $error = createStore(null);
const $currentTab = restore(changeTab, 'info');

export {
  newStuffList,
  tabsList,
  changeTab,
  changeStatus,
  changeStatusTo,
  changeEnterprise,
  fxGetById,
  fxReject,
  fxApprove,
  fxQualification,
  fxCoordinationTop,
  fxCreate,
  fxUpdate,
  fxIssue,
  fxRevoke,
  fxForApproval,
  fxRegistryAssign,
  fxSyncItemById,
  $registry,
  $currentTab,
  $isLoading,
  $error,
};
