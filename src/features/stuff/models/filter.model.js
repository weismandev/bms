import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  statuses: '',
  date_from: '',
  date_to: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
