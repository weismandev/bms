import { createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';
import { fxSaveInStorage, fxGetFromStorage } from '@features/common';

const PageGate = createGate();

const fxGetList = createEffect();

const $isLoading = createStore(false);
const $error = createStore(null);
const $savedParams = createStore(null);

const $rawData = createStore({ data: [], meta: { total: 0 } });

export {
  PageGate,
  fxGetList,
  $rawData,
  $isLoading,
  $error,
  fxSaveInStorage,
  fxGetFromStorage,
  $savedParams,
};
