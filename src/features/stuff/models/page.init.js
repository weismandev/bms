import { forward, sample, guard } from 'effector';
import { $isCompany, signout } from '../../common';
import { crmApi, companyApi } from '../api';
import {
  fxReject,
  fxApprove,
  fxQualification,
  fxCoordinationTop,
  fxUpdate,
  fxIssue,
  fxRevoke,
  fxForApproval,
  fxGetById,
  fxCreate,
  fxSyncItemById,
} from './detail.model';
import { $filters } from './filter.model';
import {
  fxGetList,
  PageGate,
  $rawData,
  $isLoading,
  $error,
  fxSaveInStorage,
  fxGetFromStorage,
  $savedParams,
} from './page.model';
import { $tableParams, $currentCompany, $pageSizeTable } from './table.model';

const requestStarted = guard({ source: $isLoading, filter: Boolean });

$isCompany.watch((isCompany) => {
  if (isCompany) {
    fxGetList.use(companyApi.getList);
  } else {
    fxGetList.use(crmApi.getList);
  }
});

$isLoading
  .on(
    [fxGetList.pending, fxSaveInStorage.pending, fxGetFromStorage.pending],
    (_, isLoading) => isLoading
  )
  .reset(signout);

$error
  .on(
    [fxGetList.failData, fxSaveInStorage.failData, fxGetFromStorage.failData],
    (_, error) => error
  )
  .reset([signout, requestStarted]);

$rawData
  .on(fxGetList.doneData, (_, { items = [], meta }) => ({
    data: items,
    meta: { total: (meta && meta.total) || items.length },
  }))
  .on(
    [
      fxReject.doneData,
      fxApprove.doneData,
      fxQualification.doneData,
      fxCoordinationTop.doneData,
      fxUpdate.doneData,
      fxIssue.doneData,
      fxRevoke.doneData,
      fxForApproval.doneData,
      fxGetById.doneData,
      fxSyncItemById.doneData,
    ],
    (state, { pass }) => updateItem(state, pass)
  )
  .on(fxCreate.doneData, (state, { pass }) => addItem(state, pass))
  .reset(signout);

sample({
  clock: PageGate.open,
  source: $isCompany,
  fn: (isCompany) => ({ storage_key: isCompany ? 'enterprises-stuff' : 'stuff' }),
  target: fxGetFromStorage,
});

$savedParams.on(fxGetFromStorage.done, (_, { result }) => result).reset(signout);

forward({
  from: sample({
    source: {
      page: PageGate.status,
      table: $tableParams,
      filters: $filters,
      enterprise: $currentCompany,
      pageSize: $pageSizeTable,
    },
    fn: ({ table, filters, enterprise, pageSize }) => {
      const { statuses, date_from, date_to } = filters;
      const { search, page } = table;

      const payload = {
        page,
        per_page: pageSize,
        filter: {},
      };

      if (Array.isArray(statuses) && statuses.length) {
        payload.filter.statuses = statuses.map((i) => i.id);
      }

      if (date_from) {
        payload.filter.date_from = date_from;
      }

      if (date_to) {
        payload.filter.date_to = date_to;
      }

      if (search) {
        payload.search_query = search;
      }

      if (enterprise) {
        payload.enterprise_id = enterprise.id;
      }

      return payload;
    },
  }),
  to: fxGetList,
});

function addItem(state, item) {
  const { data, meta } = state;

  if (Array.isArray(data) && item) {
    return {
      meta: { ...meta, total: meta.total + 1 },
      data: [item].concat(data),
    };
  }

  return state;
}

function updateItem(state, item) {
  const { data, meta } = state;

  if (Array.isArray(data) && item) {
    const updatedIdx = data.findIndex((i) => i.id === item.id);

    if (updatedIdx > -1) {
      const newData = []
        .concat(data.slice(0, updatedIdx))
        .concat(item)
        .concat(data.slice(updatedIdx + 1));

      return {
        meta: { ...meta },
        data: newData,
      };
    }

    return state;
  } else return null;
}
