import { createEvent, restore, combine } from 'effector';
import format from 'date-fns/format';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { getPathOr } from '../../../tools/path';
import { $rawData, $savedParams } from './page.model';

const { t } = i18n;

const changeCompany = createEvent();
const changeSearch = createEvent();

const $currentCompany = restore(changeCompany, '');

const $tableData = $rawData.map(({ data }) => data.map(formatTableData));
const $rowCount = $rawData.map((raw) => (raw.meta ? raw.meta.total : 0));

export const columns = [
  { name: 'pass_number', title: t('NumberPass') },
  {
    name: 'status',
    title: t('Label.status'),
  },
  {
    name: 'enterprise',
    title: t('company'),
  },
  { name: 'pass_date', title: t('PassDate') },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $search,
  $tableParams,
} = createTableBag(columns, { widths: [{ columnName: 'status', width: 250 }] });

export const $savedHiddenColumns = restore(hiddenColumnChanged, null);
export const $hiddenColumns = combine(
  $savedHiddenColumns,
  $savedParams,
  (savedHiddenColumns, savedParams) => {
    if (savedHiddenColumns) {
      return savedHiddenColumns;
    }

    if (savedParams?.hiddenColumns?.length > 0) {
      return savedParams.hiddenColumns;
    }

    return [];
  }
);

export const $savedOrderColumns = restore(columnOrderChanged, null);
export const $orderColumns = combine(
  $savedOrderColumns,
  $savedParams,
  $columnOrder,
  (savedOrderColumns, savedParams, columnOrder) => {
    if (savedOrderColumns) {
      return savedOrderColumns;
    }

    if (savedParams?.orderColumns?.length > 0) {
      return savedParams.orderColumns;
    }

    return columnOrder;
  }
);

export const $savedWidthColumns = restore(columnWidthsChanged, null);
export const $widthColumns = combine(
  $savedWidthColumns,
  $savedParams,
  $columnWidths,
  (savedWidthColumns, savedParams, columnWidths) => {
    if (savedWidthColumns) {
      return savedWidthColumns;
    }

    if (savedParams?.widthColumns?.length > 0) {
      return savedParams.widthColumns;
    }

    return columnWidths;
  }
);

export const $pageSizeSaved = restore(pageSizeChanged, null);
export const $pageSizeTable = combine(
  $pageSizeSaved,
  $savedParams,
  $pageSize,
  (pageSizeSaved, savedParams, pageSize) => {
    if (pageSizeSaved) {
      return pageSizeSaved;
    }

    if (savedParams?.pageSizeTable) {
      return savedParams.pageSizeTable;
    }

    return pageSize;
  }
);

export { changeCompany, changeSearch, $currentCompany, $rowCount, $tableData };

function formatTableData(data) {
  const { pass_number, applicant, pass_date, scud_pass, ...rest } = data;

  return {
    pass_number:
      Boolean(scud_pass) && scud_pass.id
        ? `${pass_number} - [ ${scud_pass.id} ]`
        : pass_number,
    enterprise: getPathOr('enterprise.title', applicant, t('isNotDefinedshe')),
    pass_date: pass_date
      ? format(new Date(pass_date), 'dd.MM.yyyy')
      : t('isNotDefinedshe'),
    ...rest,
  };
}
