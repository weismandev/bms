import { guard, sample } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';
import { signout } from '../../common';
import { stuffTypesApi } from '../api';
import { getDateTimeFromString } from '../lib';
import {
  $building,
  $stuffTypes,
  $isLoading,
  $error,
  submitStuffTypes,
  fxGetList,
  fxSave,
} from './stuff-types.model';

const buildingCleared = guard({
  source: $building,
  filter: (building) => !building,
});

const requestStarted = guard({ source: $isLoading, filter: Boolean });

fxGetList.use(stuffTypesApi.getList);
fxSave.use(stuffTypesApi.save);

$building.reset(signout);

$stuffTypes
  .on([fxGetList.doneData, fxSave.doneData], (state, { items }) => ({
    items: formatList(items),
  }))
  .reset([buildingCleared, signout]);

$isLoading
  .on(pending({ effects: [fxGetList, fxSave] }), (_, isLoading) => isLoading)
  .reset(signout);

$error
  .on([fxGetList.failData, fxSave.failData], (_, error) => error)
  .reset([requestStarted, signout]);

guard({
  source: $building,
  filter: (building) => Boolean(building) && (building.id || building.id === 0),
  target: fxGetList.prepend((building) => ({ building_id: building.id })),
});

guard({
  source: sample($building, submitStuffTypes, formatPayload),
  filter: ({ building_id }) => Boolean(building_id),
  target: fxSave,
});

function formatList(list) {
  if (Array.isArray(list)) {
    return list.map(({ start_time, finish_time, ...rest }) => ({
      ...rest,
      start_time: getDateTimeFromString(start_time),
      finish_time: getDateTimeFromString(finish_time),
    }));
  }

  return [];
}

function formatPayload(building, data) {
  const payload = {};

  if (building) {
    payload.building_id = building.id;
  }

  if (data && Array.isArray(data.items)) {
    payload.items = data.items.map(({ id, title, start_time, finish_time }) => {
      const item = {
        title: title || '',
        start_time: start_time ? format(start_time, 'HH:mm:ss') : '',
        finish_time: finish_time ? format(finish_time, 'HH:mm:ss') : '',
      };

      if (id) {
        item.id = id;
      }

      return item;
    });
  }

  return payload;
}
