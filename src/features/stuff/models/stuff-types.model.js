import { createEvent, createEffect, createStore, restore } from 'effector';

const changeBuilding = createEvent();
const submitStuffTypes = createEvent();

const fxGetList = createEffect();
const fxSave = createEffect();

const $stuffTypes = createStore({ items: [] });
const $isLoading = createStore(false);
const $error = createStore(null);
const $building = restore(changeBuilding, '');

export {
  changeBuilding,
  submitStuffTypes,
  $building,
  $stuffTypes,
  $isLoading,
  $error,
  fxGetList,
  fxSave,
};
