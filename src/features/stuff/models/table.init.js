import { sample } from 'effector';
import { signout, $isCompany } from '@features/common';
import { fxSaveInStorage } from './page.model';
import {
  $currentCompany,
  hiddenColumnChanged,
  columnOrderChanged,
  columnWidthsChanged,
  pageSizeChanged,
  $hiddenColumns,
  $orderColumns,
  $widthColumns,
  $pageSizeTable,
} from './table.model';

sample({
  clock: [hiddenColumnChanged, columnOrderChanged, columnWidthsChanged, pageSizeChanged],
  source: {
    hiddenColumns: $hiddenColumns,
    orderColumns: $orderColumns,
    widthColumns: $widthColumns,
    pageSizeTable: $pageSizeTable,
    isCompany: $isCompany,
  },
  fn: ({ hiddenColumns, orderColumns, widthColumns, pageSizeTable, isCompany }) => ({
    data: {
      hiddenColumns,
      orderColumns,
      widthColumns,
      pageSizeTable,
    },
    storage_key: isCompany ? 'enterprises-stuff' : 'stuff',
  }),
  target: fxSaveInStorage,
});

$currentCompany.reset(signout);
