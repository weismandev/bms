import { forward, sample, guard, split } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';
import {
  signout,
  $isCompany,
  readNotifications,
  receivedNotificationAbout,
  STUFF_PASS_STATUS_CHANGED,
} from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { crmApi, companyApi } from '../api';
import { getDateTimeFromString } from '../lib';
import {
  open,
  fxGetById,
  fxReject,
  fxApprove,
  fxQualification,
  fxCoordinationTop,
  fxCreate,
  fxUpdate,
  fxIssue,
  fxRevoke,
  fxForApproval,
  fxRegistryAssign,
  fxSyncItemById,
  $opened,
  $registry,
  changeStatusTo,
  $isLoading,
  $error,
  $currentTab,
  detailSubmitted,
  $mode,
  addClicked,
  changeStatus,
} from './detail.model';

const requestStarted = guard({ source: $isLoading, filter: Boolean });

const tabDetailSubmitted = sample({
  source: $currentTab,
  clock: detailSubmitted,
  fn: (tab, payload) => ({ tab, payload }),
});

const { infoSubmitted, registrySubmitted } = split(tabDetailSubmitted, {
  infoSubmitted: ({ tab }) => tab === 'info',
  registrySubmitted: ({ tab }) => tab === 'registry',
});

const { createPass, updatePass } = split(infoSubmitted, {
  createPass: ({ payload }) => !payload.id,
  updatePass: ({ payload }) => Boolean(payload.id),
});

$isCompany.watch((isCompany) => {
  if (isCompany) {
    fxGetById.use(companyApi.getById);
    fxSyncItemById.use(companyApi.getById);
    fxCreate.use(companyApi.create);
    fxUpdate.use(companyApi.update);
    fxIssue.use(companyApi.issue);
    fxRevoke.use(companyApi.revoke);
    fxForApproval.use(companyApi.forApproval);
    fxRegistryAssign.use(companyApi.registryAssign);
  } else {
    fxGetById.use(crmApi.getById);
    fxSyncItemById.use(crmApi.getById);
    fxReject.use(crmApi.reject);
    fxApprove.use(crmApi.approve);
    fxQualification.use(crmApi.qualification);
    fxCoordinationTop.use(crmApi.coordinationTop);
  }
});

$opened
  .on(
    [
      fxGetById.doneData,
      fxReject.doneData,
      fxApprove.doneData,
      fxQualification.doneData,
      fxCoordinationTop.doneData,

      fxCreate.doneData,
      fxUpdate.doneData,
      fxIssue.doneData,
      fxRevoke.doneData,
      fxForApproval.doneData,
    ],
    (_, { pass }) => formatOpened(pass)
  )
  .on(fxRegistryAssign.doneData, updateRegistry)
  .reset(signout);

$mode.on([fxCreate.done, fxUpdate.done, fxRegistryAssign.done], () => 'view');

$currentTab.on(addClicked, () => 'info').reset(signout);

$isLoading
  .on(
    pending({
      effects: [
        fxGetById,
        fxReject,
        fxApprove,
        fxQualification,
        fxCoordinationTop,

        fxCreate,
        fxUpdate,
        fxIssue,
        fxRevoke,
        fxForApproval,
        fxRegistryAssign,
      ],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

$error
  .on(
    [
      fxGetById.failData,
      fxReject.failData,
      fxApprove.failData,
      fxQualification.failData,
      fxCoordinationTop.failData,

      fxCreate.failData,
      fxUpdate.failData,
      fxIssue.failData,
      fxRevoke.failData,
      fxForApproval.failData,
      fxRegistryAssign.failData,
    ],
    (_, error) => error
  )
  .reset([signout, requestStarted, open]);

$registry
  .on($opened, (_, { registry, id }) =>
    Array.isArray(registry) ? { id, items: registry } : { id: null, items: [] }
  )
  .reset(signout);

forward({ from: open, to: fxGetById });

forward({
  from: receivedNotificationAbout[STUFF_PASS_STATUS_CHANGED],
  to: fxSyncItemById.prepend(({ data }) => data && data.stuff_pass_id),
});

sample({
  source: $opened,
  fn: ({ id }) => ({ id, event: STUFF_PASS_STATUS_CHANGED }),
  target: readNotifications,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.reject,
    fn: formatPayloadForStatusChange,
  }),
  to: fxReject,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.approve,
    fn: formatPayloadForStatusChange,
  }),
  to: fxApprove,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.qualification,
    fn: formatPayloadForStatusChange,
  }),
  to: fxQualification,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.coordinationTop,
    fn: formatPayloadForStatusChange,
  }),
  to: fxCoordinationTop,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.forApproval,
    fn: formatPayloadForStatusChange,
  }),
  to: fxForApproval,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.revoke,
    fn: formatPayloadForStatusChange,
  }),
  to: fxRevoke,
});

forward({
  from: sample({
    source: $opened,
    clock: changeStatusTo.issue,
    fn: formatPayloadForStatusChange,
  }),
  to: fxIssue,
});

forward({
  from: createPass,
  to: fxCreate.prepend(({ payload }) => formatPayload(payload)),
});

forward({
  from: updatePass,
  to: fxUpdate.prepend(({ payload }) => formatPayload(payload)),
});

forward({
  from: registrySubmitted,
  to: fxRegistryAssign.prepend(({ payload }) => ({
    pass_id: payload && payload.id,
    registry: payload ? payload.items : [],
  })),
});

function formatPayloadForStatusChange(opened, data) {
  const payload = {};

  if (opened && opened.id) {
    payload.id = opened.id;
  }

  if (data && data.payload && data.payload.comment) {
    payload.comment = data.payload.comment;
  }

  return payload;
}

function updateRegistry(opened, registry) {
  return { ...opened, registry: registry ? registry.items : [] };
}

function formatOpened(opened) {
  const {
    id,
    building_id,
    pass_date,
    estimated_time,
    pass_number,
    applicant,
    stuff_types,
    status,
    accompanying,
    action_type,
    registry,
    car_parkings,
    comment,
    last_comment,
  } = opened;

  const hasParking = Boolean(car_parkings);

  const data = {
    id,
    building: building_id || '',
    enterprise: (applicant && applicant.enterprise) || '',
    applicant: applicant || '',
    date: pass_date || '',
    estimated_time: estimated_time || '',
    accompanying_fullname: (accompanying && accompanying.fullname) || '',
    accompanying_phone: accompanying?.phone ? formatPhone(accompanying.phone) : '',
    stuff_types: Array.isArray(stuff_types) ? stuff_types : '',
    action_type: action_type || '',
    pass_number,
    status,
    registry: Array.isArray(registry) ? registry : [],
    car_parking: hasParking,
    brand: (hasParking && car_parkings.brand) || '',
    id_number: (hasParking && car_parkings.id_number) || '',
    assigned_type: (hasParking && car_parkings.assigned_type) || '',
    reference: (hasParking && car_parkings.reference_id) || '',
    time_from: hasParking && getDateTimeFromString(car_parkings.time_from),
    time_to: hasParking && getDateTimeFromString(car_parkings.time_to),
    comment: comment || '',
    lastComment: (last_comment && last_comment.text) || '',
  };

  return data;
}

function formatPayload(data) {
  const {
    id,
    building,
    date,
    estimated_time,
    applicant,
    action_type,
    enterprise,
    accompanying_fullname,
    accompanying_phone,
    stuff_types,
    comment,
    car_parking,

    brand,
    id_number,
    assigned_type,
    reference,
    time_from,
    time_to,
  } = data;

  const payload = {
    building_id: getObjectIdOrId(building),
    date,
    estimated_time,
    applicant_id: getObjectIdOrId(applicant) || getObjectIdOrId(applicant, 'employee_id'),
    action_type_id: getObjectIdOrId(action_type),
    enterprise_id: getObjectIdOrId(enterprise),
    accompanying_fullname,
    accompanying_phone,
    available_stuff_types: Array.isArray(stuff_types)
      ? stuff_types.map((i) => getObjectIdOrId(i))
      : [],
    comment,
  };

  if (id) {
    payload.id = id;
  }

  if (car_parking) {
    payload.car_parking = {
      brand,
      id_number,
      time_from: time_from && format(time_from, 'HH:mm:ss'),
      time_to: time_to && format(time_to, 'HH:mm:ss'),
    };

    if (assigned_type) {
      payload.car_parking.assigned_type_id = getObjectIdOrId(assigned_type);
    }

    if (reference) {
      payload.car_parking.reference_id = getObjectIdOrId(reference);
    }
  }

  return payload;
}

function getObjectIdOrId(objectOrId, idName = 'id') {
  return Boolean(objectOrId) && typeof objectOrId === 'object'
    ? objectOrId[idName]
    : objectOrId;
}
