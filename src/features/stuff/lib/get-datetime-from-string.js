import set from 'date-fns/set';

export function getDateTimeFromString(timeString) {
  if (timeString) {
    const [hours = 0, minutes = 0, seconds = 0] = timeString.split(':');

    return set(new Date(), { hours, minutes, seconds });
  }

  return '';
}
