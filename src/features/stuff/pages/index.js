import { combine } from 'effector';
import { useStore } from 'effector-react';
import { FilterMainDetailLayout } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';
import { PageGate } from '../models/page.model';
import { Filter, Table, Detail } from '../organisms';

const $stores = combine({
  isFilterOpen: $isFilterOpen,
  isDetailOpen: $isDetailOpen,
});

const StuffPage = () => {
  const { isFilterOpen, isDetailOpen } = useStore($stores);

  return (
    <>
      <PageGate />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedStuffPage = () => (
  <HaveSectionAccess>
    <StuffPage />
  </HaveSectionAccess>
);

export { RestrictedStuffPage as StuffPage };
