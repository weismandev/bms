import { api } from '../../../api/api2';

const getList = (payload) => api.v1('post', 'stuff-pass/crm/list', payload);

const getById = (id) => api.v1('get', 'stuff-pass/crm/get', { id });

const reject = (payload) => api.v1('post', 'stuff-pass/crm/status/reject', payload);

const approve = (payload) => api.v1('post', 'stuff-pass/crm/status/approve', payload);

const qualification = (payload) =>
  api.v1('post', 'stuff-pass/crm/status/qualification', payload);

const coordinationTop = (payload) =>
  api.v1('post', 'stuff-pass/crm/status/coordination-top', payload);

export const crmApi = {
  getList,
  getById,
  reject,
  approve,
  qualification,
  coordinationTop,
};
