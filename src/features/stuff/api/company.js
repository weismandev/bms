import { api } from '../../../api/api2';

const getList = (payload) => api.v1('post', 'stuff-pass/management/list', payload);

const getById = (id) => api.v1('get', 'stuff-pass/management/get', { id });

const create = (payload) => api.v1('post', 'stuff-pass/management/create', payload);

const update = (payload) => api.v1('post', 'stuff-pass/management/update', payload);

const issue = (payload) => api.v1('post', 'stuff-pass/management/status/issued', payload);

const revoke = (payload) =>
  api.v1('post', 'stuff-pass/management/status/revoke', payload);

const forApproval = (payload) =>
  api.v1('post', 'stuff-pass/management/status/for-approval', payload);

const registryAssign = (payload) =>
  api.v1('post', 'stuff-pass/crm/registry/assign', payload);

export const companyApi = {
  getList,
  getById,
  create,
  update,
  issue,
  revoke,
  forApproval,
  registryAssign,
};
