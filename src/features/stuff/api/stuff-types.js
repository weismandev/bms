import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'stuff-pass/crm/types/list', payload);
const save = (payload) => api.v1('post', 'stuff-pass/crm/types/save', payload);

export const stuffTypesApi = {
  getList,
  save,
};
