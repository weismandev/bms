import { useState } from 'react';
import { ActionButton, Toolbar, InputField } from '@ui';
import { Formik, Form, Field } from 'formik';
import { Dialog } from '@mui/material';
import { Check } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { changeStatus } from '../../models/detail.model';
import { DialogHeader } from '../../molecules';

const { t } = i18n;

export const DetailFooter = (props) => {
  const { status } = props;

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: '100%',
      }}
    >
      {(status === 'new' || status === 'qualification') && (
        <NewAndQualificationStatusButtons />
      )}

      {(status === 'for_approval' || status === 'coordination_top') && (
        <ForApprovalStatusButtons />
      )}

      {status === 'approved' && <ApproveStatusButtons />}

      {status === 'issued' && <IssueStatusButtons />}
    </div>
  );
};

const NewAndQualificationStatusButtons = () => (
  <ActionButton kind="negative" onClick={() => changeStatus({ status: 'for-approval' })}>
    {t('SendForApproval')}
  </ActionButton>
);

const ForApprovalStatusButtons = () => (
  <>
    <ActionButton kind="positive" onClick={() => changeStatus({ status: 'approve' })}>
      {t('Accept')}
    </ActionButton>
    <QualificationButtonWithDialog />
    <RejectButtonWithDialog />
  </>
);

const ApproveStatusButtons = () => (
  <ActionButton kind="positive" onClick={() => changeStatus({ status: 'issue' })}>
    {t('Extradite')}
  </ActionButton>
);

const IssueStatusButtons = () => {
  const [isOpen, setOpen] = useState(false);

  const open = () => setOpen(true);
  const close = () => setOpen(false);

  const submitForm = (values) => {
    changeStatus({ status: 'revoke', payload: values });
    close();
  };

  return (
    <>
      <ActionButton kind="negative" onClick={open}>
        {t('Revoke')}
      </ActionButton>
      <DialogWithComment
        header={t('RevokeAPass')}
        isOpen={isOpen}
        close={close}
        submitForm={submitForm}
      />
    </>
  );
};

const QualificationButtonWithDialog = () => {
  const [isOpen, setOpen] = useState(false);

  const open = () => setOpen(true);
  const close = () => setOpen(false);

  const submitForm = (values) => {
    changeStatus({ status: 'qualification', payload: values });
    close();
  };

  return (
    <>
      <ActionButton kind="negative-outlined" onClick={open}>
        {t('ForClarification')}
      </ActionButton>
      <DialogWithComment
        header={t('PassRefinement')}
        isOpen={isOpen}
        close={close}
        submitForm={submitForm}
      />
    </>
  );
};

const RejectButtonWithDialog = () => {
  const [isOpen, setOpen] = useState(false);

  const open = () => setOpen(true);
  const close = () => setOpen(false);

  const submitForm = (values) => {
    changeStatus({ status: 'reject', payload: values });
    close();
  };

  return (
    <>
      <ActionButton kind="negative" onClick={open}>
        {t('Reject')}
      </ActionButton>
      <DialogWithComment
        header={t('Reject')}
        isOpen={isOpen}
        close={close}
        submitForm={submitForm}
      />
    </>
  );
};

const useStyles = makeStyles({
  paper: {
    width: '550px',
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
  },
  label: {
    color: '#65657B',
    fontSize: 13,
    lineHeight: '15px',
    fontWeight: 500,
  },
  toolbar: {
    padding: 24,
    height: 84,
  },
});

const DialogWithComment = (props) => {
  const { isOpen, close, header, submitForm } = props;
  const { paper } = useStyles();

  return (
    <Dialog classes={{ paper }} open={isOpen} onClose={close}>
      <DialogHeader headerText={header} close={close} />
      <CommentForm submitForm={submitForm} />
    </Dialog>
  );
};

const CommentForm = (props) => {
  const { submitForm } = props;
  const { toolbar } = useStyles();

  return (
    <Formik onSubmit={submitForm} initialValues={{ comment: '' }}>
      {() => (
        <Form>
          <div style={{ padding: '0 24px' }}>
            <Field
              name="comment"
              label={t('Label.comment')}
              component={InputField}
              rowsMax={5}
              multiline
            />
          </div>
          <Toolbar className={toolbar}>
            <ActionButton kind="positive" type="submit">
              <Check />
              {t('Confirm')}
            </ActionButton>
          </Toolbar>
        </Form>
      )}
    </Formik>
  );
};
