import { combine } from 'effector';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Toolbar, FoundInfo, Greedy, AddButton, FilterButton } from '../../../../ui';
import { $isCompany } from '../../../common';
import { EnterpriseSelect } from '../../../enterprise-select';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select';
import { addClicked, $mode } from '../../models/detail.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import { changeCompany, $currentCompany, $rowCount } from '../../models/table.model';
import { StuffTypes } from '../stuff-types';

const { t } = i18n;

const $stores = combine({
  totalCount: $rowCount,
  mode: $mode,
  currentCompany: $currentCompany,
  isFilterOpen: $isFilterOpen,
  isCompanyAccount: $isCompany,
});

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
});

function TableToolbar() {
  const { totalCount, mode, currentCompany, isFilterOpen, isCompanyAccount } =
    useStore($stores);

  const classes = useStyles();

  const CompanySelect = isCompanyAccount ? EnterprisesCompaniesSelect : EnterpriseSelect;

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <FoundInfo count={totalCount} className={classes.margin} />
      <div style={{ marginLeft: 10, width: 300 }}>
        <CompanySelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={false}
          placeholder={t('ChooseCompany')}
          firstAsDefault
          approvedOnly
        />
      </div>
      <Greedy />
      {!isCompanyAccount && <StuffTypes />}
      {isCompanyAccount && (
        <AddButton
          disabled={mode === 'edit'}
          onClick={addClicked}
          className={classes.margin}
        />
      )}
    </Toolbar>
  );
}

export { TableToolbar };
