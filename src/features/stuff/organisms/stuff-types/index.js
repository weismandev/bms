import { useState } from 'react';
import DatePicker from 'react-datepicker';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { InputField, ActionButton, Toolbar, Loader, ErrorMsg as ErrorMessage } from '@ui';
import classNames from 'classnames';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, FastField, FieldArray } from 'formik';
import { nanoid } from 'nanoid';
import {
  Dialog,
  TableRow,
  TableCell,
  Button,
  IconButton,
  TableHead,
  TableBody,
  TableContainer,
  InputAdornment,
  Table,
} from '@mui/material';
import { SaveOutlined, Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { StuffMyBuildingsSelect } from '../../../stuff-my-buildings-select';
import {
  changeBuilding,
  $building,
  $stuffTypes,
  submitStuffTypes,
  $isLoading,
  $error,
} from '../../models/stuff-types.model';
import { DialogHeader } from '../../molecules';

const $stores = combine({
  building: $building,
  stuffTypes: $stuffTypes,
  isLoading: $isLoading,
  error: $error,
});

const { t } = i18n;

export const StuffTypes = () => {
  const [isOpen, setOpen] = useState(false);

  const open = () => setOpen(true);
  const close = () => setOpen(false);

  const submitForm = submitStuffTypes;

  return (
    <>
      <ActionButton style={{ padding: '0 24px' }} onClick={open}>
        {t('TMCTypes')}
      </ActionButton>
      <StuffTypesDialog
        header={t('TMCTypes')}
        isOpen={isOpen}
        close={close}
        submitForm={submitForm}
      />
    </>
  );
};

const useStyles = makeStyles({
  paper: {
    position: 'relative',
    width: '75%',
    maxWidth: 1000,
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
  },
  label: {
    color: '#65657B',
    fontSize: 13,
    lineHeight: '15px',
    fontWeight: 500,
  },
  toolbar: {
    padding: 24,
    height: 84,
  },
  headCell: {
    lineHeight: '16px',
    fontSize: 14,
    color: '#9494A3',
    fontWeight: 500,
    whiteSpace: 'nowrap',
  },

  cellHeadRoot: {
    padding: '20px 5px',
  },

  adornment: {
    zIndex: 1,
  },

  startAdornment: {
    marginRight: 10,
  },
});

const StuffTypesDialog = (props) => {
  const { isOpen, close, header, submitForm } = props;
  const { paper } = useStyles();

  const { building, stuffTypes, isLoading, error } = useStore($stores);

  return (
    <Dialog classes={{ paper }} open={isOpen} onClose={close}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <DialogHeader headerText={header} close={close} />
      <StuffTypesForm
        submitForm={submitForm}
        building={building}
        stuffTypes={stuffTypes}
      />
    </Dialog>
  );
};

const StuffTypesForm = (props) => {
  const { submitForm, building, stuffTypes } = props;

  const { toolbar, cellHeadRoot, headCell, adornment, startAdornment } = useStyles();

  const headers = (
    <TableRow>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('NameTitle')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('ActionStartTime')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('ActionEndTime')}
      </TableCell>
      <TableCell />
    </TableRow>
  );

  return (
    <Formik onSubmit={submitForm} initialValues={stuffTypes} enableReinitialize>
      {({ values }) => {
        const content = (
          <FieldArray
            name="items"
            render={({ push, remove }) => {
              const addButton = (
                <TableRow>
                  <TableCell colSpan={4}>
                    <Button
                      onClick={() =>
                        push({
                          start_time: '',
                          finish_time: '',
                          title: '',
                          _id: nanoid(3),
                        })
                      }
                      color="primary"
                    >
                      {`+ ${t('AddTMCtype')}`}
                    </Button>
                  </TableCell>
                </TableRow>
              );

              const stuffRows =
                Array.isArray(values.items) &&
                values.items.map((i, idx) => {
                  const deleteButton = (
                    <TableCell>
                      <IconButton onClick={() => remove(idx)} size="large">
                        <Close />
                      </IconButton>
                    </TableCell>
                  );

                  return (
                    <div key={i.id || i._id}>
                      <TableRow>
                        <TableCell>
                          <FastField
                            name={`items[${idx}.title]`}
                            label={null}
                            divider={false}
                            component={InputField}
                            required
                          />
                        </TableCell>
                        <TableCell>
                          <FastField
                            name={`items[${idx}.start_time]`}
                            label={null}
                            divider={false}
                            render={({ field, form }) => (
                              <DatePicker
                                required
                                customInput={
                                  <InputField
                                    field={field}
                                    form={form}
                                    label={null}
                                    divider={false}
                                    startAdornment={
                                      <InputAdornment
                                        classes={{
                                          root: classNames(adornment, startAdornment),
                                        }}
                                        position="end"
                                      >
                                        {t('From')}
                                      </InputAdornment>
                                    }
                                  />
                                }
                                name={field.name}
                                selected={field.value}
                                onChange={(value) =>
                                  form.setFieldValue(field.name, value)
                                }
                                locale={dateFnsLocale}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption={t('Label.time')}
                                dateFormat="HH:mm"
                              />
                            )}
                          />
                        </TableCell>
                        <TableCell>
                          <FastField
                            name={`items[${idx}.finish_time]`}
                            label={null}
                            divider={false}
                            render={({ field, form }) => (
                              <DatePicker
                                required
                                customInput={
                                  <InputField
                                    field={field}
                                    form={form}
                                    label={null}
                                    divider={false}
                                    startAdornment={
                                      <InputAdornment
                                        classes={{
                                          root: classNames(adornment, startAdornment),
                                        }}
                                        position="end"
                                      >
                                        {t('To')}
                                      </InputAdornment>
                                    }
                                  />
                                }
                                name={field.name}
                                selected={field.value}
                                onChange={(value) =>
                                  form.setFieldValue(field.name, value)
                                }
                                locale={dateFnsLocale}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption={t('Label.time')}
                                dateFormat="HH:mm"
                              />
                            )}
                          />
                        </TableCell>
                        {deleteButton}
                      </TableRow>
                    </div>
                  );
                });

              return (
                <>
                  {stuffRows}
                  {addButton}
                </>
              );
            }}
          />
        );

        return (
          <Form>
            <div style={{ padding: '0 24px' }}>
              <StuffMyBuildingsSelect
                name="building"
                label={t('building')}
                placeholder={t('selectBbuilding')}
                onChange={changeBuilding}
                value={building}
                firstAsDefault
              />
              <FieldArray
                name="items"
                render={() => (
                  <TableContainer>
                    <Table>
                      <TableHead>{headers}</TableHead>
                      <TableBody>{content}</TableBody>
                    </Table>
                  </TableContainer>
                )}
              />
            </div>
            <Toolbar className={toolbar}>
              <ActionButton kind="positive" type="submit">
                <SaveOutlined />
                {t('Save')}
              </ActionButton>
            </Toolbar>
          </Form>
        );
      }}
    </Formik>
  );
};
