import { memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  InputField,
  CustomScrollbar,
} from '../../../../ui';
import { StuffTicketStatusSelectField } from '../../../stuff-ticket-status-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const $stores = combine({ filters: $filters });

const { t } = i18n;

const Filter = memo((props) => {
  const { filters } = useStore($stores);

  const sharedProps = {
    component: InputField,
    type: 'date',
  };

  const reset = () => filtersSubmitted('');
  const close = () => changedFilterVisibility(false);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar style={{ padding: 24 }} closeFilter={close} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={reset}
          enableReinitialize
          render={(bag) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field label={t('startDate')} name="date_from" {...sharedProps} />
                <Field label={t('expirationDate')} name="date_to" {...sharedProps} />
                <Field
                  name="statuses"
                  component={StuffTicketStatusSelectField}
                  label={t('statuses')}
                  placeholder={t('selectStatuses')}
                  isMulti
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
