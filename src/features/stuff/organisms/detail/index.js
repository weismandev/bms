import { useState, memo } from 'react';
import DatePicker from 'react-datepicker';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import classNames from 'classnames';
import { format, isValid, addMinutes } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field, FieldArray } from 'formik';
import { nanoid } from 'nanoid';
import * as Yup from 'yup';
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Tab,
  Button,
  IconButton,
  Divider as MuiDivider,
  InputAdornment,
  Grid,
  Typography,
} from '@mui/material';
import { red } from '@mui/material/colors';
import { Close, Error as ErrorIcon } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { $isCompany } from '@features/common';
import { EmployeeByCompanyIdSelectField } from '@features/employee-by-company-id-select';
import { ParkingSlotEnterpriseSelectField } from '@features/parking-slot-enterprise-select';
import { StuffActionTypesSelectField } from '@features/stuff-action-types-select';
import { StuffCarPassSelectField } from '@features/stuff-car-pass-select';
import { StuffLoadingZonesSelectField } from '@features/stuff-loading-zones-select';
import { StuffMyApprovedCompaniesSelectField } from '@features/stuff-my-approved-companies-select';
import { StuffMyBuildingsSelectField } from '@features/stuff-my-buildings-select';
import { StuffTypesSelectField } from '@features/stuff-types-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  Tabs,
  FormSectionHeader,
  InputField,
  SwitchField,
  Loader,
  ErrorMsg as ErrorMessage,
  Divider,
  FastDateSetter,
  CustomScrollbar,
  Modal,
  PhoneMaskedInput,
} from '@ui/index';
import { PassStatus } from '../../atoms';
import {
  tabsList,
  $opened,
  $registry,
  $currentTab,
  changeTab,
  changedDetailVisibility,
  $mode,
  $error,
  $isLoading,
  changeMode,
  detailSubmitted,
  changeEnterprise,
} from '../../models/detail.model';
import { DetailFooter } from '../detail-footer';

const { t } = i18n;

const $stores = combine({
  opened: $opened,
  registry: $registry,
  isCompanyAccount: $isCompany,
  currentTab: $currentTab,
  mode: $mode,
  error: $error,
  isLoading: $isLoading,
});

const headerHeight = 35;
const tabsHeight = 50;
const toolbarHeight = 84;
const footerHeight = 80;

const crmStatuses = ['for_approval', 'coordination_top'];
const companyStatuses = ['new', 'qualification', 'approved', 'issued'];
const editableStatuses = ['new', 'qualification'];

const Toolbar = (props) => {
  const { mode, isNew, editAvailable, header, currentTab, children } = props;

  const close = () => changedDetailVisibility(false);
  const edit = () => changeMode('edit');

  return (
    <>
      <DetailToolbar
        style={{ padding: 24, height: toolbarHeight }}
        hidden={{ delete: true, edit: !editAvailable }}
        onEdit={edit}
        onClose={close}
        mode={mode}
      >
        {children}
      </DetailToolbar>

      <FormSectionHeader
        style={{ padding: '0 24px', margin: 0, height: headerHeight }}
        header={header}
      />

      <Tabs
        style={{ margin: '0 auto', height: tabsHeight }}
        options={tabsList}
        currentTab={currentTab}
        variant="fullWidth"
        onChange={(e, tab) => changeTab(tab)}
        tabEl={<Tab style={{ minWidth: '50%' }} />}
        isNew={isNew}
      />
    </>
  );
};

const Detail = memo(() => {
  const { opened, registry, isCompanyAccount, currentTab, mode, error, isLoading } =
    useStore($stores);

  const status = opened && opened.status;
  const isNew = !opened.id;

  const showFooter =
    mode === 'view' &&
    ((!isCompanyAccount && crmStatuses.includes(status && status.slug)) ||
      (isCompanyAccount && companyStatuses.includes(status && status.slug)));

  const editAvailable =
    isCompanyAccount && editableStatuses.includes(status && status.slug);

  const header = isNew ? t('NewPass') : `${t('PassNum')} ${opened.pass_number}`;

  const showLastComment = Boolean(opened.lastComment);

  const onChangeEnterprise = (form) => {
    form.setFieldValue('reference', '');
    changeEnterprise();
  };

  const toolbar = (
    <Toolbar
      mode={mode}
      editAvailable={editAvailable}
      header={header}
      currentTab={currentTab}
      isNew={isNew}
      showLastComment={showLastComment}
      lastComment={opened.lastComment}
    >
      {status && <PassStatus status={status} style={{ order: 55 }} />}
    </Toolbar>
  );

  return (
    <Wrapper style={{ height: '100%', position: 'relative' }}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />

      {currentTab === 'info' && (
        <InfoTabForm
          initialValues={opened}
          mode={mode}
          toolbar={toolbar}
          showFooter={showFooter}
          showLastComment={showLastComment}
          lastComment={opened.lastComment}
          onChangeEnterprise={onChangeEnterprise}
        />
      )}

      {currentTab === 'registry' && (
        <StuffTabForm
          initialValues={registry}
          mode={mode}
          toolbar={toolbar}
          showFooter={showFooter}
          showLastComment={showLastComment}
        />
      )}

      {showFooter && (
        <div style={{ height: footerHeight, background: '#EDF6FF' }}>
          <DetailFooter status={status && status.slug} />
        </div>
      )}
    </Wrapper>
  );
});

const useFormStyles = makeStyles({
  form: {
    height: (props) => `calc(100% - ${props.showFooter ? footerHeight : 0}px)`,
    position: 'relative',
  },
  outerContainer: {
    height: `calc(100% - ${toolbarHeight}px - ${headerHeight}px - ${tabsHeight}px )`,
    padding: '24px 6px 24px 24px',
  },
  innerContainer: {
    paddingRight: 18,
    position: 'relative',
  },
  adornment: {
    zIndex: 1,
  },
  startAdornment: {
    marginRight: 10,
  },
  headCell: {
    lineHeight: '16px',
    fontSize: 14,
    color: '#9494A3',
    fontWeight: 500,
    whiteSpace: 'nowrap',
  },
  cellHeadRoot: {
    padding: '20px 5px',
  },
});

const mixed = Yup.mixed();
const mixedRequired = mixed.required(t('thisIsRequiredField'));

const string = Yup.string();
const stringRequired = string.required(t('thisIsRequiredField'));

const validationSchema = Yup.object().shape({
  building: mixedRequired,
  enterprise: mixedRequired,
  applicant: mixedRequired,
  date: stringRequired,
  accompanying_fullname: stringRequired,
  accompanying_phone: stringRequired.matches(/^\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/, {
    message: t('invalidNumber'),
    excludeEmptyString: true,
  }),
  action_type: mixedRequired,
  stuff_types: mixed.when('building', {
    is: (value) => Boolean(value),
    then: mixedRequired,
    otherwise: mixed.required(t('firstAndForemostChooseBuilding')),
  }),
  car_parking: Yup.boolean(),
  brand: string.when('car_parking', {
    is: true,
    then: string,
  }),
  id_number: string.when('car_parking', {
    is: true,
    then: stringRequired,
    otherwise: string,
  }),
  assigned_type: mixed.when('car_parking', {
    is: true,
    then: mixedRequired,
    otherwise: mixed,
  }),
  reference: mixed.when(['car_parking', 'assigned_type'], {
    is: (car_parking, assigned_type) =>
      car_parking &&
      ((assigned_type && assigned_type.slug === 'parking') ||
        (assigned_type && assigned_type.slug === 'loading_zone')),
    then: mixedRequired,
    otherwise: mixed,
  }),
  time_from: string.when('car_parking', {
    is: true,
    then: stringRequired
      .test(
        'time_from-before-time_to',
        t('TheTimeMustBeBeforeTheParkingEndTime'),
        checkTime
      )
      .nullable(),
    otherwise: string,
  }),
  time_to: string.when('car_parking', {
    is: true,
    then: stringRequired
      .test(
        'time_to-after-time_from',
        t('TheTimeMustBeAfterTheParkingStartTime'),
        checkTime
      )
      .nullable(),
    otherwise: string,
  }),
});

function checkTime() {
  const { time_from, time_to } = this.parent;
  return time_to > time_from;
}

const InfoTabForm = (props) => {
  const {
    mode,
    initialValues,
    toolbar,
    showFooter,
    showLastComment,
    lastComment,
    onChangeEnterprise,
  } = props;

  const { form, outerContainer, innerContainer, adornment, startAdornment } =
    useFormStyles({
      showFooter,
      showLastComment,
    });

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={detailSubmitted}
      onReset={() =>
        initialValues.id ? changeMode('view') : changedDetailVisibility(false)
      }
    >
      {({ values, setFieldValue }) => {
        const building_id =
          Boolean(values.building) && typeof values.building === 'object'
            ? values.building.id
            : values.building;

        const starts_at = formatZonePayload(values.date, values.time_from);
        const ends_at = formatZonePayload(values.date, values.time_to);

        return (
          <Form className={form}>
            {toolbar}
            <div className={outerContainer}>
              <CustomScrollbar>
                <div className={innerContainer}>
                  {showLastComment && (
                    <LastCommentStatusBlock
                      header={t('LastComment')}
                      text={lastComment}
                      Icon={ErrorIcon}
                      color={red}
                    />
                  )}
                  <Field
                    name="building"
                    component={StuffMyBuildingsSelectField}
                    mode={mode}
                    label={t('building')}
                    placeholder={t('selectBbuilding')}
                    required
                  />
                  <Field
                    name="enterprise"
                    component={StuffMyApprovedCompaniesSelectField}
                    label={t('company')}
                    mode={mode}
                    firstAsDefault
                    placeholder={t('ChooseCompany')}
                    onChangeEnterprise={onChangeEnterprise}
                    required
                  />
                  <Field
                    name="applicant"
                    component={EmployeeByCompanyIdSelectField}
                    company_id={values.enterprise && values.enterprise.id}
                    label={t('Declarant')}
                    mode={mode}
                    placeholder={t('ChooseAnApplicant')}
                    required
                  />
                  {mode === 'edit' && (
                    <FastDateSetter
                      onChange={(value) =>
                        setFieldValue('date', format(value, 'yyyy-MM-dd'))
                      }
                      value={formatDate(values.date)}
                    />
                  )}
                  <Field
                    name="date"
                    component={InputField}
                    type="date"
                    label={t('Label.date')}
                    mode={mode}
                    placeholder={t('PassDate')}
                    required
                  />
                  <Field
                    name="estimated_time"
                    component={InputField}
                    type="time"
                    label={t('Label.time')}
                    mode={mode}
                    placeholder={t('PassTime')}
                    required
                  />
                  <Field
                    name="action_type"
                    component={StuffActionTypesSelectField}
                    label={t('RequestType')}
                    mode={mode}
                    placeholder={t('SelectRequestType')}
                    required
                  />
                  <Field
                    name="stuff_types"
                    component={StuffTypesSelectField}
                    label={t('TypeOfGoodsAndMaterials')}
                    mode={mode}
                    isMulti
                    building_id={building_id}
                    placeholder={t('SelectTheTypeOfGoodsAndMaterials')}
                    required
                  />
                  <Field
                    name="accompanying_fullname"
                    component={InputField}
                    label={t('NameAccompany')}
                    mode={mode}
                    placeholder={t('EnterNameAccompany')}
                    required
                  />
                  <Field
                    name="accompanying_phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        inputComponent={PhoneMaskedInput}
                        label={t('PhoneAccompany')}
                        placeholder={t('EnterPhoneAccompany')}
                        mode={mode}
                        required
                      />
                    )}
                  />
                  <Field
                    component={SwitchField}
                    name="car_parking"
                    label={t('Vehicle')}
                    labelPlacement="start"
                    disabled={mode === 'view'}
                    divider={false}
                  />
                  {values.car_parking && (
                    <div style={{ display: 'flex' }}>
                      <div style={{ width: '10%' }}>
                        <MuiDivider orientation="vertical" variant="middle" />
                      </div>
                      <div style={{ width: '90%' }}>
                        <Field
                          name="brand"
                          label={t('TransportBrand')}
                          labelPlacement="start"
                          placeholder={t('EnterTransportBrand')}
                          component={InputField}
                          mode={mode}
                        />
                        <Field
                          name="id_number"
                          label={t('GovNumber')}
                          placeholder="A000AA000"
                          helpText={t('FormatGovNumber')}
                          mode={mode}
                          component={InputField}
                          labelPlacement="start"
                          required={Boolean(values.car_parking)}
                        />
                        <Field
                          name="assigned_type"
                          component={StuffCarPassSelectField}
                          label={t('Entry')}
                          labelPlacement="start"
                          placeholder={t('ChooseDestination')}
                          mode={mode}
                          required={Boolean(values.car_parking)}
                        />
                        {values.assigned_type?.slug === 'parking' && (
                          <Field
                            name="reference"
                            component={ParkingSlotEnterpriseSelectField}
                            buildings={building_id ? [building_id] : []}
                            label={t('PM')}
                            labelPlacement="start"
                            placeholder={t('choosePM')}
                            mode={mode}
                            required={Boolean(values.car_parking)}
                            enterprise={values.enterprise || {}}
                          />
                        )}
                        <Field
                          name="time_from"
                          render={({ field, form }) => (
                            <DatePicker
                              readOnly={mode === 'view'}
                              customInput={
                                <InputField
                                  label={t('ParkingTime')}
                                  labelPlacement="start"
                                  mode={mode}
                                  field={field}
                                  form={form}
                                  startAdornment={
                                    <InputAdornment
                                      classes={{
                                        root: classNames(adornment, startAdornment),
                                      }}
                                      position="end"
                                    >
                                      {t('From')}
                                    </InputAdornment>
                                  }
                                />
                              }
                              name={field.name}
                              selected={field.value}
                              onKeyDown={(e) => e.preventDefault()}
                              onChange={(value) => {
                                form.setFieldValue(field.name, value);
                                form.setFieldValue('time_to', '');
                              }}
                              placeholderText={t('chooseTime')}
                              locale={dateFnsLocale}
                              showTimeSelect
                              showTimeSelectOnly
                              timeIntervals={15}
                              timeCaption={t('Label.time')}
                              dateFormat="HH:mm"
                              required={Boolean(values.car_parking)}
                            />
                          )}
                        />
                        <Field
                          name="time_to"
                          render={({ field, form }) => (
                            <DatePicker
                              readOnly={mode === 'view'}
                              customInput={
                                <InputField
                                  label={t('ParkingTime')}
                                  labelPlacement="start"
                                  mode={mode}
                                  field={field}
                                  form={form}
                                  startAdornment={
                                    <InputAdornment
                                      classes={{
                                        root: classNames(adornment, startAdornment),
                                      }}
                                      position="end"
                                    >
                                      {t('To')}
                                    </InputAdornment>
                                  }
                                />
                              }
                              name={field.name}
                              selected={field.value}
                              onKeyDown={(e) => e.preventDefault()}
                              onChange={(value) => form.setFieldValue(field.name, value)}
                              placeholderText={t('chooseTime')}
                              locale={dateFnsLocale}
                              showTimeSelect
                              showTimeSelectOnly
                              timeIntervals={15}
                              timeCaption={t('Label.time')}
                              dateFormat="HH:mm"
                              minTime={setTimeInterval({
                                time: values.time_from,
                                value: 15,
                              })}
                              maxTime={setTimeInterval({
                                time: values.time_from,
                                value: 30,
                              })}
                              required={Boolean(values.car_parking)}
                            />
                          )}
                        />
                        {values.assigned_type?.slug === 'loading_zone' && (
                          <Field
                            name="reference"
                            component={StuffLoadingZonesSelectField}
                            building_id={building_id}
                            starts_at={starts_at}
                            ends_at={ends_at}
                            label={t('LoadingArea')}
                            labelPlacement="start"
                            placeholder={t('ChooseAZone')}
                            mode={mode}
                            required={Boolean(values.car_parking)}
                            divider={false}
                          />
                        )}
                      </div>
                    </div>
                  )}
                  <Divider />
                  <Field
                    name="comment"
                    component={InputField}
                    label={t('Label.comment')}
                    placeholder={t('EnterComment')}
                    mode={mode}
                    rowsMax={5}
                    multiline
                    divider={false}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

const StuffTabForm = (props) => {
  const { mode, initialValues, showFooter, toolbar, showLastComment } = props;

  const { form, outerContainer, innerContainer, cellHeadRoot, headCell } = useFormStyles({
    showFooter,
    showLastComment,
  });

  const isEditMode = mode === 'edit';

  const headers = (
    <TableRow>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('NameTitle')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('Amount')}
      </TableCell>
      {isEditMode && <TableCell />}
    </TableRow>
  );

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={detailSubmitted}
      onReset={() =>
        initialValues.id ? changeMode('view') : changedDetailVisibility(false)
      }
    >
      {({ values }) => {
        const content = (
          <FieldArray
            name="items"
            render={({ push, remove }) => {
              const addButton = (
                <TableRow>
                  <TableCell colSpan={isEditMode ? 3 : 2}>
                    <Button
                      onClick={() => push({ total_count: 0, title: '', _id: nanoid(3) })}
                      color="primary"
                    >
                      {`+ ${t('AddTmts')}`}
                    </Button>
                  </TableCell>
                </TableRow>
              );

              const stuffRows =
                Array.isArray(values.items) &&
                values.items.map((i, idx) => {
                  const deleteButton = (
                    <TableCell>
                      <IconButton onClick={() => remove(idx)} size="large">
                        <Close />
                      </IconButton>
                    </TableCell>
                  );

                  return (
                    <TableRow key={i.id || i._id}>
                      <TableCell>
                        <Field
                          name={`items[${idx}.title]`}
                          label={null}
                          divider={false}
                          component={InputField}
                          mode={mode}
                        />
                      </TableCell>
                      <TableCell>
                        <Field
                          name={`items[${idx}.total_count]`}
                          label={null}
                          divider={false}
                          component={InputField}
                          mode={mode}
                        />
                      </TableCell>
                      {isEditMode && deleteButton}
                    </TableRow>
                  );
                });

              return (
                <>
                  {stuffRows}
                  {isEditMode && addButton}
                </>
              );
            }}
          />
        );

        return (
          <Form className={form}>
            {toolbar}
            <div className={outerContainer}>
              <CustomScrollbar>
                <div className={innerContainer}>
                  <TableContainer style={{ paddingRight: 12 }}>
                    <Table>
                      <TableHead>{headers}</TableHead>
                      <TableBody>{content}</TableBody>
                    </Table>
                  </TableContainer>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

const LastCommentStatusBlock = (props) => {
  const maxLength = 80;
  const { header, text, Icon, color } = props;
  const isLongComment = text.length > maxLength;
  const [isOpen, setOpen] = useState(false);
  const openModal = () => setOpen(true);
  const closeModal = () => setOpen(false);
  return (
    <>
      <div
        style={{
          background: color[50],
          padding: 8,
          position: 'sticky',
          top: 0,
          left: 0,
          zIndex: 2,
          marginBottom: 30,
          cursor: isLongComment ? 'pointer' : 'initial',
        }}
        onClick={isLongComment ? openModal : null}
      >
        <Grid container spacing={2} wrap="nowrap">
          <Grid item>
            <Icon style={{ color: color[500], width: 48, height: 48 }} />
          </Grid>
          <Grid item>
            <Typography
              component="h6"
              style={{
                color: color[500],
                fontSize: 18,
                lineHeight: '21px',
                fontWeight: 'bold',
              }}
            >
              {header}
            </Typography>
            {text ? (
              <Typography style={{ color: '#65657B', fontSize: 14 }}>
                <span>{isLongComment ? `${text.slice(0, maxLength)}...` : text}</span>
              </Typography>
            ) : null}
          </Grid>
        </Grid>
      </div>
      <Modal
        isOpen={isOpen}
        onClose={closeModal}
        header={t('LastComment')}
        content={
          <CustomScrollbar style={{ minHeight: 400 }}>
            <div style={{ paddingRight: 20 }}>{text}</div>
          </CustomScrollbar>
        }
      />
    </>
  );
};

function formatDate(date) {
  const currentDate = new Date(date);
  return isValid(currentDate) ? currentDate : new Date();
}

function formatZonePayload(date, time) {
  return time ? `${date} ${format(time, 'HH:mm:ss')}` : null;
}

function setTimeInterval({ time, value }) {
  return time instanceof Date ? addMinutes(time, value) : new Date();
}

export { Detail };
