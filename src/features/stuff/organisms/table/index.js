import { useMemo, memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  Loader,
  ErrorMsg as ErrorMessage,
  TableColumnVisibility,
  PagingPanel,
} from '@ui';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { PassStatus } from '../../atoms';
import { open, $opened } from '../../models/detail.model';
import { $isLoading, $error } from '../../models/page.model';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  columnWidthsChanged,
  columnOrderChanged,
  hiddenColumnChanged,
  columns,
  $rowCount,
  $hiddenColumns,
  $orderColumns,
  $widthColumns,
  $pageSizeTable,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = (rowData, rowId) => open(rowId);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );

  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StatusFormatter = ({ value }) => <PassStatus status={value} />;

const StatusTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StatusFormatter} {...props} />
);

const NewStatusRowFormatter = ({ value, row }) => {
  if (row.status && row.status.slug === 'new') {
    return <b style={{ color: '#3B3B50' }}>{value}</b>;
  }

  return value;
};

const NewStatusRowTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={NewStatusRowFormatter} {...props} />
);

const $stores = combine({
  data: $tableData,
  currentPage: $currentPage,
  count: $rowCount,
  isLoading: $isLoading,
  error: $error,
});

const Table = memo(() => {
  const { data, currentPage, count, isLoading, error } = useStore($stores);

  const hiddenColumns = useStore($hiddenColumns);
  const orderColumns = useStore($orderColumns);
  const widthColumns = useStore($widthColumns);
  const pageSizeTable = useStore($pageSizeTable);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />

      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <StatusTypeProvider for={['status']} />
        <NewStatusRowTypeProvider for={['pass_number', 'enterprise', 'pass_date']} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSizeTable}
          onPageSizeChange={pageSizeChanged}
        />

        <DragDropProvider />

        <CustomPaging totalCount={count} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={widthColumns}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={orderColumns} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumns}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames: hiddenColumns }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
