import { Grid, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { Header } from '../../atoms';

const useStyles = makeStyles({
  headerContainer: {
    padding: '18px 18px 20px 24px',
  },
});

export const DialogHeader = (props) => {
  const { close, headerText } = props;
  const { headerContainer } = useStyles();

  return (
    <Grid
      container
      alignItems="center"
      justifyContent="space-between"
      classes={{ root: headerContainer }}
      wrap="nowrap"
    >
      <Grid item>
        <Header headerText={headerText} />
      </Grid>
      <Grid item>
        <IconButton onClick={close} size="large">
          <Close />
        </IconButton>
      </Grid>
    </Grid>
  );
};
