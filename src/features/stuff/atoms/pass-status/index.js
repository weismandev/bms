import classnames from 'classnames';
import { Chip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

const useStausStyles = makeStyles(() => {
  const blue = '#0394E3';
  const green = '#1BB169';
  const white = '#fff';
  const red = '#EB5757';
  const grey = '#AAA';

  return {
    chip: {
      fontWeight: 500,
      width: 150,
      color: white,
    },
    new: {
      background: blue,
    },
    approved: {
      background: white,
      color: green,
      border: `2px solid ${green}`,
    },
    qualification: {
      background: white,
      color: red,
      border: `2px solid ${red}`,
    },
    rejected: {
      background: red,
    },
    issued: {
      background: green,
    },
    on_territory: {
      background: white,
      border: `2px solid ${blue}`,
      color: blue,
    },
    overdue: {
      background: red,
    },
    finished: {
      background: grey,
    },
    expired: {
      background: red,
    },
    revoked: {
      background: red,
    },
  };
});

export const PassStatus = (props) => {
  const { status } = props;

  const classes = useStausStyles();

  const chipRootClassName = classnames(classes.chip, classes[status.slug]);

  return (
    <Chip
      component="span"
      label={status.title}
      classes={{ root: chipRootClassName }}
      {...props}
    />
  );
};
