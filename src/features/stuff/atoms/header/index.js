import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  header: {
    fontSize: 24,
    lineHeight: '28px',
    fontWeight: 900,
    color: '#3B3B50',
  },
});

export const Header = (props) => {
  const { headerText, ...rest } = props;
  const { header } = useStyles();

  return (
    <Typography classes={{ root: header }} {...rest}>
      {headerText}
    </Typography>
  );
};
