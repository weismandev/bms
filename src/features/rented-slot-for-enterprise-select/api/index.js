import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'parking/enterprise_rent/rented-slots-for-enterprise', {
    ...payload,
    per_page: 1000,
  });

export { getList };
