import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const RentedSlotForEnterpriseSelect = (props) => {
  const { enterprise_id, exclude } = props;

  let options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  if (Array.isArray(exclude) && exclude.length) {
    options = options.filter((i) => !exclude.includes(i.id));
  }

  useEffect(() => {
    if (enterprise_id) {
      fxGetList({ enterprise_id });
    }
  }, [enterprise_id]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const RentedSlotForEnterpriseSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<RentedSlotForEnterpriseSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { RentedSlotForEnterpriseSelect, RentedSlotForEnterpriseSelectField };
