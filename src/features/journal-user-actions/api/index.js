import { api } from '../../../api/api2';

const config = {
  default: {
    headers: { 'Content-Type': 'application/json' },
  },
  export: {
    config: {
      responseType: 'arraybuffer',
    },
  },
};

const getList = (payload) => {
  return api.v1('get', 'journal/list', payload, config.default);
};

const exportList = (payload) => {
  return api.v1('request', 'journal/export', payload, config.export);
};

const getFilteredList = (payload) => {
  return api.v1('post', 'journal/filter/list', payload, config.default);
};

const getUsersCompanyInfo = () => {
  return api.v1('get', 'journal/company', {}, config.default);
};

const downloadFile = (payload) => {
  return api.v1('request', 'journal/file', payload, {
    ...config.export,
    method: 'get',
  });
};

export const journalUserActionsApi = {
  getList,
  exportList,
  getFilteredList,
  getUsersCompanyInfo,
  downloadFile,
};
