import AddCircleIcon from '@mui/icons-material/AddCircle';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import MobileFriendlyIcon from '@mui/icons-material/MobileFriendly';
import SendIcon from '@mui/icons-material/Send';
import ExportIcon from '@mui/icons-material/VerticalAlignBottom';
import ImportIcon from '@mui/icons-material/VerticalAlignTop';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const events = [
  {
    type: 1,
    name: t('Authorization'),
    icon: <MobileFriendlyIcon />,
  },
  {
    type: 2,
    name: t('wayout'),
    icon: <ExitToAppIcon />,
  },
  {
    type: 3,
    name: t('Create'),
    icon: <AddCircleIcon />,
  },
  {
    type: 4,
    name: t('Editing'),
    icon: <EditIcon />,
  },
  {
    type: 5,
    name: t('Removal'),
    icon: <DeleteIcon />,
  },
  {
    type: 6,
    name: t('GeneratingSendingALoginPasswordForUser'),
    icon: <AutorenewIcon />,
  },
  {
    type: 7,
    name: t('Unloading'),
    icon: <ExportIcon />,
  },
  {
    type: 8,
    name: t('Upload'),
    icon: <ImportIcon />,
  },
  {
    type: 9,
    name: t('Sending'),
    icon: <SendIcon />,
  },
];

export { events };
