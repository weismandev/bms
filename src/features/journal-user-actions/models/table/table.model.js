import { createEvent, sample, createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../../tools/factories';
import { $rawData } from '../page';

const { t } = i18n;

const columns = [
  { name: 'number', title: '№' },
  { name: 'id', title: 'ID' },
  { name: 'date', title: t('Label.date') },
  { name: 'time', title: t('Label.time') },
  { name: 'event', title: t('Event') },
  { name: 'building', title: t('building') },
  { name: 'user', title: t('Label.FullName') },
  { name: 'company', title: t('company') },
  { name: 'file_id', title: t('File') },
];

const tableBagConfig = {
  widths: [
    { columnName: 'number', width: 75 },
    { columnName: 'id', width: 75 },
    { columnName: 'date', width: 150 },
    { columnName: 'time', width: 100 },
    { columnName: 'event', width: 500 },
    { columnName: 'building', width: 300 },
    { columnName: 'user', width: 350 },
    { columnName: 'company', width: 250 },
    { columnName: 'file_id', width: 75 },
  ],
  pageSize: 15,
};

const exportTableClicked = createEvent();

export const {
  $tableParams,
  $columnOrder,
  $columnWidths,
  $currentPage,
  $pageSize,
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
} = createTableBag(columns, tableBagConfig);

const $tableData = $rawData.map(({ journal_list, meta }) => {
  return journal_list.map((data, number) => formatTableData(data, number, meta));
});

const $rowCount = $rawData.map(({ meta }) => (meta ? meta.total : 0));

function formatTableData(data, number, { current_page, per_page }) {
  return { ...data, number: (current_page - 1) * per_page + number + 1 };
}

export { columns, $rowCount, $tableData, exportTableClicked };
