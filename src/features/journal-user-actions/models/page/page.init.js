import { guard, attach, sample, forward } from 'effector';
import { condition } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import i18n from '@shared/config/i18n';
import { signout } from '../../../common';
import { journalUserActionsApi } from '../../api';
import { $filters } from '../filter';
import { $tableParams, exportTableClicked } from '../table';
import {
  $isLoading,
  $error,
  $rawData,
  $usersCompany,
  PageGate,
  fxGetJournalList,
  fxGetFilteredJournalList,
  fxExportJournalList,
  fxGetUsersCompany,
  fxDownloadFile,
  saveJournalList,
  fileClicked,
} from './page.model';

const { t } = i18n;

fxGetJournalList.use(journalUserActionsApi.getList);
fxGetFilteredJournalList.use(journalUserActionsApi.getFilteredList);
fxExportJournalList.use(journalUserActionsApi.exportList);
fxGetUsersCompany.use(journalUserActionsApi.getUsersCompanyInfo);
fxDownloadFile.use(journalUserActionsApi.downloadFile);

const requestStarted = guard({ source: $isLoading, filter: Boolean });

$isLoading.on(
  [
    fxGetJournalList.pending,
    fxGetFilteredJournalList.pending,
    fxExportJournalList.pending,
    fxDownloadFile.pending,
  ],
  (_, loading) => loading
);

$error
  .on(
    [
      fxGetJournalList.failData,
      fxGetFilteredJournalList.failData,
      fxExportJournalList.failData,
      fxDownloadFile.failData,
    ],
    (_, error) => error
  )
  .reset([signout, requestStarted]);

$rawData.on(
  [fxGetJournalList.doneData, fxGetFilteredJournalList.doneData],
  formatRawData
);

$usersCompany.on(fxGetUsersCompany.doneData, (_, { company }) => company);

condition({
  source: [$filters, $tableParams, PageGate.status],
  if: isAnyFilterParamsActive,
  then: attach({
    effect: fxGetFilteredJournalList,
    mapParams: collectFilteredJournalListParams,
  }),
  else: attach({
    effect: fxGetJournalList,
    mapParams: ([_, tableParams]) => tableParams,
  }),
});

forward({
  from: PageGate.status,
  to: fxGetUsersCompany,
});

sample({
  source: $filters,
  clock: exportTableClicked,
  fn: (filters) => collectFilteredJournalListParams([filters, {}]),
  target: fxExportJournalList,
});

sample({
  source: $usersCompany,
  clock: fxExportJournalList.doneData,
  fn: (company, data) => ({ company, data }),
  target: saveJournalList.prepend(({ company, data }) => {
    const blob = new Blob([data], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `${format(new Date(), 'dd-MM-yyyy_HH-mm')}_${t('UploadActionUsers')}_${
        company.title
      }.xlsx`
    );
  }),
});

sample({
  source: fileClicked,
  fn: (file_id) => ({ file_id }),
  target: fxDownloadFile,
});

fxDownloadFile.done.watch(({ params, result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  FileSaver.saveAs(URL.createObjectURL(blob), `file_${params.file_id}.xlsx`);
});

function formatRawData(_, { journal_list = [], meta }) {
  return {
    journal_list,
    meta: {
      total: meta?.total || journal_list.length,
      per_page: meta?.per_page || journal_list.length,
      current_page: meta?.current_page || 1,
    },
  };
}

function collectFilteredJournalListParams([params, tableParams]) {
  const { date_from, date_to } = params;

  const event_type =
    params.event_type && params.event_type.key && `${params.event_type.key}`;
  const userdata_id =
    params.userdata_id && params.userdata_id.key && `${params.userdata_id.key}`;
  const building_id = params.building_id && `${params.building_id.key}`;
  const company_id =
    params.company_id && params.company_id.key && `${params.company_id.key}`;

  const data = {
    ...tableParams,
    date_from,
    date_to,
    event_type,
    userdata_id,
    building_id,
    company_id,
  };

  return filterNullableData(data);
}

function isAnyFilterParamsActive([filters]) {
  return Object.keys(filters).filter((key) => filters[key]).length;
}

function filterNullableData(data) {
  const filtered = Object.keys(data)
    .filter((key) => Boolean(data[key]))
    .reduce((acc, key) => ({ ...acc, [key]: data[key] }), {});

  return prependTimeToDate(filtered);
}

function prependTimeToDate(data) {
  const time = '00:00:00';

  if ('date_from' in data && 'date_to' in data) {
    return {
      ...data,
      date_from: `${data.date_from} ${time}`,
      date_to: `${data.date_to} ${time}`,
    };
  }
  return data;
}
