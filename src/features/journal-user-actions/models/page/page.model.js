import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';

const $rawData = createStore({
  journal_list: [],
  meta: { total: 0, current_page: 0, per_page: 0 },
});
const $isLoading = createStore(false);
const $error = createStore(null);
const $usersCompany = createStore({ id: '', title: '' });

const PageGate = createGate();

const fxGetJournalList = createEffect();
const fxGetFilteredJournalList = createEffect();
const fxExportJournalList = createEffect();
const fxGetUsersCompany = createEffect();
const fxDownloadFile = createEffect();
const saveJournalList = createEvent();
const fileClicked = createEvent();

export {
  $rawData,
  $isLoading,
  $error,
  $usersCompany,
  PageGate,
  fxGetJournalList,
  fxGetFilteredJournalList,
  fxExportJournalList,
  fxGetUsersCompany,
  fxDownloadFile,
  saveJournalList,
  fileClicked,
};
