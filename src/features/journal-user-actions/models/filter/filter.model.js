import { createFilterBag } from '../../../../tools/factories';

const defaultFilters = {
  date_from: '',
  date_to: '',
  event_type: '',
  userdata_id: '',
  building_id: '',
  company_id: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
