import { forward } from 'effector';
import { pageNumChanged as pageNumberChanged } from '../table';
import { filtersSubmitted } from './filter.model';

forward({
  from: filtersSubmitted,
  to: pageNumberChanged.prepend(() => 0),
});
