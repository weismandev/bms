import { useStore } from 'effector-react';
import { FilterMainDetailLayout } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import { $isFilterOpen } from '../models/filter';
import { PageGate } from '../models/page';
import { Filter, Table } from '../organisms';

const JournalPage = () => {
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <FilterMainDetailLayout filter={isFilterOpen && <Filter />} main={<Table />} />
    </>
  );
};

const RestrictedJournalPage = () => (
  <HaveSectionAccess>
    <JournalPage />
  </HaveSectionAccess>
);

export { RestrictedJournalPage as JournalPage };
