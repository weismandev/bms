import { memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  TableColumnReordering,
  Toolbar,
  PagingPanel,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableRow,
  TableContainer,
  TableCell as CustomTableCell,
  Wrapper,
  Loader,
  ErrorMsg as ErrorMessage,
  SortingLabel,
} from '../../../../ui';
import { $isLoading, $error } from '../../models/page';
import {
  $tableData,
  $rowCount,
  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  columnWidthsChanged,
  columnOrderChanged,
  columns,
} from '../../models/table';
import { EventCell } from '../event-cell';
import { FileCell } from '../file-cell';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const TableHeaderCell = ({ children, ...props }) => (
  <TableHeaderRow.Cell
    {...props}
    style={{
      padding: '16px 8px',
      fontWeight: 700,
      fontSize: 18,
      color: '#7D7D8E',
      cursor: 'default',
    }}
  >
    {children}
  </TableHeaderRow.Cell>
);

const TableCell = (props) => (
  <CustomTableCell
    {...props}
    additionalStyle={{ verticalAlign: 'text-top', cursor: 'default' }}
  />
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const EventFormatter = ({ value }) => <EventCell value={value} />;

const EventTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={EventFormatter} {...props} />
);

const FileFormatter = ({ value }) => <FileCell value={value} />;

const FileTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={FileFormatter} {...props} />
);

const $stores = combine({
  data: $tableData,
  currentPage: $currentPage,
  pageSize: $pageSize,
  columnWidths: $columnWidths,
  columnOrder: $columnOrder,
  count: $rowCount,
  isLoading: $isLoading,
  error: $error,
});

const Table = memo(() => {
  const {
    data,
    currentPage,
    pageSize,
    columnWidths,
    columnOrder,
    count,
    isLoading,
    error,
  } = useStore($stores);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <DragDropProvider />

        <EventTypeProvider for={['event']} />
        <FileTypeProvider for={['file_id']} />

        <CustomPaging totalCount={count} />
        <DXTable
          messages={{ noData: t('noData') }}
          columnExtensions={[{ columnName: 'event', wordWrapEnabled: true }]}
          rowComponent={TableRow}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          sortLabelComponent={SortingLabel}
          cellComponent={TableHeaderCell}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <PagingPanel messages={{ rowsPerPage: t('EntriesPerPage'), info: '' }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
