import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  InputField,
  CustomScrollbar,
} from '../../../../ui';
import { JournalUserActionsBuildingSelectField } from '../../../journal-user-actions-building-select';
import { JournalUserActionsCompanySelectField } from '../../../journal-user-actions-company-select';
import { JournalUserActionsEventsSelectField } from '../../../journal-user-actions-events-select';
import { JournalUserActionsUserSelectField } from '../../../journal-user-actions-user-select';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models/filter';

const { t } = i18n;

export const Filter = () => {
  const filters = useStore($filters);

  const resetFilterHandler = () => filtersSubmitted('');
  const closeFilterHandler = () => changedFilterVisibility(false);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar closeFilter={closeFilterHandler} style={{ padding: 24 }} />
        <Formik
          onSubmit={filtersSubmitted}
          initialValues={filters}
          onReset={resetFilterHandler}
          enableReinitialize
          render={() => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                label={t('startDate')}
                name="date_from"
                type="date"
                component={InputField}
                required
              />
              <Field
                label={t('expirationDate')}
                name="date_to"
                type="date"
                component={InputField}
                required
              />
              <Field
                name="event_type"
                component={JournalUserActionsEventsSelectField}
                label={t('EventTypes')}
                placeholder={t('SelectEventType')}
              />
              <Field
                name="userdata_id"
                component={JournalUserActionsUserSelectField}
                label={t('user')}
                placeholder={t('AddUser')}
              />
              <Field
                name="building_id"
                component={JournalUserActionsBuildingSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
              />
              <Field
                name="company_id"
                component={JournalUserActionsCompanySelectField}
                label={t('company')}
                placeholder={t('ChooseCompany')}
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};
