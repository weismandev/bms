import { Description } from '@mui/icons-material';
import { fileClicked } from '../../models/page/page.model';

const FileCell = ({ value }) => {
  if (!value) return null;

  const clickHandler = () => fileClicked(value);

  return (
    <div
      onClick={clickHandler}
      style={{ color: '#0394E3', textAlign: 'center', cursor: 'pointer' }}
    >
      <Description />
    </div>
  );
};

export { FileCell };
