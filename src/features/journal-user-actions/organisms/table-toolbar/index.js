import { useStore } from 'effector-react';
import { Toolbar, FoundInfo, ExportButton, FilterButton, Greedy } from '@ui';
import i18n from '@shared/config/i18n';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter';
import { $rowCount, exportTableClicked, $tableData } from '../../models/table';

const { t } = i18n;

export const TableToolbar = () => {
  const count = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const tableData = useStore($tableData);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo
        count={count}
        countTitle={`${t('TotalEvents')}: `}
        style={{ margin: '0 10px' }}
      />
      <Greedy />
      <ExportButton {...{ tableData }} onClick={exportTableClicked} />
    </Toolbar>
  );
};
