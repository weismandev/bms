import makeStyles from '@mui/styles/makeStyles';
import { events } from '../../models/event-cell';

const useStyles = makeStyles({
  container: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    padding: 15,
  },
  icon: { position: 'absolute', top: 19, left: 11, color: '#0394E3' },
  name: { margin: '5px 0 15px 0', fontSize: 18, fontWeight: 'bold' },
  info: { paddingLeft: 25 },
  text: {
    '&:first-letter': {
      textTransform: 'capitalize',
    },
  },
});

const EventCell = ({ value }) => {
  const classes = useStyles();
  const { name, details, type } = value;

  const event = events.find((e) => e.type === type);
  const icon = (event && event.icon) || null;

  return (
    <>
      <div className={classes.container}>
        <span className={classes.icon}>{icon}</span>
        <div className={classes.info}>
          <p className={classes.name}>{name}</p>
          <p className={classes.text}>{details}</p>
        </div>
      </div>
    </>
  );
};

export { EventCell };
