import { api } from '../../../api/api2';

const getList = (payload) => api.v1('post', 'enterprises/contacts/get-list', payload);

const update = (payload) => api.v1('post', 'enterprises/contacts/update', payload);

const create = (payload) => api.v1('post', 'enterprises/contacts/add', payload);

const deleteItem = (payload) => api.v1('post', 'enterprises/contacts/delete', payload);

export const enterprisesContactsApi = {
  getList,
  update,
  create,
  delete: deleteItem,
};
