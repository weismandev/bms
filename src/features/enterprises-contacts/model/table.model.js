import { createEvent, restore } from 'effector';
import i18n from '@shared/config/i18n';
import { formatPhone } from '../../../tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

const formatTableData = (data = []) =>
  data
    .map((item) => {
      const { contact_id, sip, comment, employee } = item;
      return {
        id: contact_id,
        sip: sip ? formatPhone(String(sip)) : t('isNotDefined'),
        comment: comment || t('isNotDefined'),
        employee_fullname:
          employee && employee.fullname ? employee.fullname : t('isNotDefinedit'),
      };
    })
    .sort((a, b) => {
      if (a.id > b.id) return 1;
      if (a.id < b.id) return -1;
      return 0;
    });

const columns = [
  { name: 'employee_fullname', title: t('Label.FullName') },
  { name: 'sip', title: t('Label.phone') },
  { name: 'comment', title: t('Label.comment') },
];

const columnWidthsChanged = createEvent();
const columnOrderChanged = createEvent();
const changeCompany = createEvent();
const hiddenColumnChanged = createEvent();

const $columnWidths = restore(columnWidthsChanged, [
  { columnName: 'employee_fullname', width: 400 },
  { columnName: 'sip', width: 200 },
  { columnName: 'comment', width: 500 },
]);

const $columnOrder = restore(columnOrderChanged, ['employee_fullname', 'sip', 'comment']);

const $currentCompany = restore(changeCompany, '');
const $totalCount = $raw.map(({ meta }) => meta.total);
const $tableData = $raw.map(({ data }) => formatTableData(data));
const $hiddenColumnNames = restore(hiddenColumnChanged, []);

export {
  columns,
  $tableData,
  $columnWidths,
  $columnOrder,
  columnWidthsChanged,
  columnOrderChanged,
  $totalCount,
  $currentCompany,
  changeCompany,
  hiddenColumnChanged,
  $hiddenColumnNames,
};
