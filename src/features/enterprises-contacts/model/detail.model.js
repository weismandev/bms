import { createEvent, createStore, restore, guard, split } from 'effector';

const emptyEntity = {
  enterprise: null,
  employee: null,
  sip: '',
  comment: '',
};

const $newEntity = createStore(emptyEntity);

const changedDetailVisibility = createEvent();
const addClicked = createEvent();
const changeMode = createEvent();
const open = createEvent();
const detailSubmitted = createEvent();
const deleteContact = createEvent();

const detailClosed = guard({
  source: changedDetailVisibility,
  filter: (bool) => !bool,
});

const entityApi = split(detailSubmitted, {
  create: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

const $isDetailOpen = restore(changedDetailVisibility, false);
const $mode = restore(changeMode, 'view');
const $opened = $newEntity.map((entity) => entity);

export {
  changedDetailVisibility,
  $isDetailOpen,
  addClicked,
  $mode,
  changeMode,
  emptyEntity,
  $newEntity,
  $opened,
  open,
  detailClosed,
  detailSubmitted,
  deleteContact,
  entityApi,
};
