import { signout } from '../../common';
import {
  $columnWidths,
  $columnOrder,
  $currentCompany,
  $hiddenColumnNames,
} from './table.model';

$columnWidths.reset(signout);
$columnOrder.reset(signout);
$currentCompany.reset(signout);
$hiddenColumnNames.reset(signout);
