import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;

const fxGetList = createEffect();
const fxCreate = createEffect();
const fxUpdate = createEffect();
const fxDelete = createEffect();

const errorDialogVisibilityChanged = createEvent();
const deleteDialogVisibilityChanged = createEvent();
const deleteConfirmed = createEvent();

const $isDeleteDialogOpen = restore(deleteDialogVisibilityChanged, false);
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);

const $raw = createStore({ data: [], meta: { total: 0 } });
const $normalized = $raw.map(({ data }) =>
  data.reduce(
    (acc, item) => ({
      ...acc,
      [item.contact_id]: { ...item, id: item.contact_id },
    }),
    {}
  )
);

export {
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  PageGate,
  pageMounted,
  pageUnmounted,
  $raw,
  $normalized,
  errorDialogVisibilityChanged,
  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteConfirmed,
  $isLoading,
  $error,
  $isErrorDialogOpen,
};
