import { sample, forward, attach, merge } from 'effector';
import { formatPhone } from '../../../tools/formatPhone';
import { signout } from '../../common';
import {
  $isDetailOpen,
  addClicked,
  $mode,
  emptyEntity,
  $newEntity,
  $opened,
  open,
  detailClosed,
  deleteContact,
  entityApi,
} from './detail.model';
import {
  $normalized,
  fxCreate,
  fxUpdate,
  fxDelete,
  fxGetList,
  deleteConfirmed,
} from './page.model';
import { $currentCompany, changeCompany } from './table.model';

const formatOpened = (opened) => {
  const { sip, contact_id, comment, employee, enterprise } = opened;

  return {
    id: contact_id,
    sip: sip ? formatPhone(String(sip)) : '',
    comment: comment ? comment : '',
    employee: employee
      ? {
          id: employee.id ? employee.id : '',
          fullname: employee.fullname ? employee.fullname : '',
        }
      : '',
    enterprise,
  };
};

$isDetailOpen
  .on([addClicked, open], () => true)
  .on([changeCompany, fxDelete.done], () => false)
  .reset(signout);

$mode
  .on(addClicked, () => 'edit')
  .on([open, detailClosed, changeCompany, fxCreate.done, fxUpdate.done], () => 'view')
  .reset(signout);

$opened
  .on(
    sample({
      source: { $currentCompany, $newEntity },
      clock: merge([addClicked, changeCompany]),
      fn: ({ $currentCompany, $newEntity }) => {
        return {
          ...$newEntity,
          enterprise: $currentCompany,
        };
      },
    }),
    (state, opened) => formatOpened(opened)
  )
  .on(
    sample({
      source: { $normalized, $currentCompany },
      clock: open,
      fn: ({ $normalized, $currentCompany }, id) => {
        return {
          ...$normalized[id],
          enterprise: {
            ...$currentCompany,
          },
        };
      },
    }),
    (state, opened) => formatOpened(opened)
  )
  .on(detailClosed, () => emptyEntity)
  .reset(signout);

$newEntity.reset(signout);

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: (payload) => {
      const { comment, sip, enterprise, employee } = payload;

      return {
        enterprise_id: enterprise.id,
        userdata_id: employee.id,
        contacts: [
          {
            comment,
            sip,
          },
        ],
      };
    },
  }),
});

forward({
  from: entityApi.update,
  to: attach({
    effect: fxUpdate,
    mapParams: (payload) => {
      const { comment, sip, enterprise, id, employee } = payload;

      return {
        enterprise_id: enterprise.id,
        userdata_id: employee.id,
        contacts: [
          {
            comment,
            sip,
            contact_id: id,
          },
        ],
      };
    },
  }),
});

forward({
  from: deleteContact,
  to: attach({
    effect: fxDelete,
    mapParams: (payload) => {
      const { id, enterprise } = payload;
      return {
        enterprise_id: enterprise.id,
        contact_id: id,
      };
    },
  }),
});

sample({
  source: $currentCompany,
  clock: merge([fxUpdate.done, fxCreate.done, fxDelete.done]),
  target: fxGetList,
});

sample({
  source: $opened,
  clock: deleteConfirmed,
  fn: (opened, event) => opened,
  target: deleteContact,
});
