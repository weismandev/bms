import { guard, merge, sample } from 'effector';
import { signout } from '../../common';
import { enterprisesContactsApi } from '../api';
import {
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  $raw,
  $isErrorDialogOpen,
  deleteConfirmed,
  $isDeleteDialogOpen,
  $error,
  errorDialogVisibilityChanged,
  $isLoading,
} from './page.model';
import { $currentCompany } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxGetList.use(enterprisesContactsApi.getList);
fxCreate.use(enterprisesContactsApi.create);
fxUpdate.use(enterprisesContactsApi.update);
fxDelete.use(enterprisesContactsApi.delete);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

sample({
  source: [fxCreate.pending, fxUpdate.pending, fxDelete.pending, fxGetList.pending],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

const gotError = merge([fxGetList.fail, fxCreate.fail, fxUpdate.fail, fxDelete.fail]);

const closeError = guard({
  source: errorDialogVisibilityChanged,
  filter: (bool) => !bool,
});

$raw
  .on(
    fxGetList.doneData,
    (state, { contacts = [], meta = { total: contacts.length } }) => {
      return {
        meta,
        data: contacts,
      };
    }
  )
  .reset(signout);

$isDeleteDialogOpen.on(deleteConfirmed, () => false).reset(signout);
$error.on(gotError, (state, { error }) => error).reset([signout, closeError]);
$isErrorDialogOpen.on(gotError, () => true).reset(signout);
$isLoading.reset(signout);

sample({
  source: { currentCompany: $currentCompany, settingsInitialized: $settingsInitialized },
  filter: ({ currentCompany, settingsInitialized }) =>
    currentCompany && Boolean(currentCompany.id) && settingsInitialized,
  fn: ({ currentCompany }) => currentCompany,
  target: fxGetList,
});
