import { useStore } from 'effector-react';
import { Wrapper, DetailToolbar, InputField, PhoneMaskedInput } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { EnterprisesCompaniesSelectField } from '../../../enterprises-companies-select';
import { EnterprisesEmployeeSelectField } from '../../../enterprises-employee-select';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
} from '../../model/detail.model';
import { deleteDialogVisibilityChanged } from '../../model/page.model';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  sip: Yup.string()
    .matches(/^\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/, {
      message: t('invalidNumber'),
      excludeEmptyString: true,
    })
    .required(t('PleaseFillInThisField')),
  employee: Yup.mixed().required(t('PleaseSelectFromTheList')),
  enterprise: Yup.mixed().required(t('PleaseSelectFromTheList')),
});

const Detail = (props) => {
  const mode = useStore($mode);
  const opened = useStore($opened);
  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={opened}
        enableReinitialize
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        render={({ resetForm, values }) => {
          return (
            <Form>
              <DetailToolbar
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    resetForm();
                    changeMode('view');
                  }
                }}
                onClose={() => changedDetailVisibility(false)}
                onDelete={() => deleteDialogVisibilityChanged(true)}
                mode={mode}
                style={{ padding: 24 }}
              />
              <div style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="enterprise"
                  component={EnterprisesCompaniesSelectField}
                  label={t('company')}
                  placeholder={t('ChooseCompany')}
                  required
                  mode={mode}
                />
                <Field
                  name="employee"
                  component={EnterprisesEmployeeSelectField}
                  label={t('Employee')}
                  placeholder={t('ChooseEmployee')}
                  required
                  company_id={values.enterprise ? values.enterprise.id : null}
                  mode={mode}
                />
                <Field
                  name="sip"
                  render={({ field, form }) => (
                    <InputField
                      field={field}
                      form={form}
                      required
                      inputComponent={PhoneMaskedInput}
                      label={t('Label.phone')}
                      placeholder={t('EnterPhone')}
                      mode={mode}
                    />
                  )}
                />
                <Field
                  name="comment"
                  component={InputField}
                  label={t('Label.comment')}
                  placeholder={t('EnterComment')}
                  mode={mode}
                />
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

export { Detail };
