import { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  TableHeaderCell,
  TableColumnVisibility,
} from '@ui';
import { Template } from '@devexpress/dx-react-core';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  Toolbar,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import { $opened, open } from '../../model/detail.model';
import {
  $tableData,
  columns,
  $columnWidths,
  $columnOrder,
  columnWidthsChanged,
  columnOrderChanged,
  hiddenColumnChanged,
  $hiddenColumnNames,
} from '../../model/table.model';
import { TableToolbar } from '../table-toolbar';

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      minHeight: '36px',
      height: '36px',
      alignItems: 'felx-start',
      borderBottom: 'none',
      flexWrap: 'nowrap',
    }}
  />
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((data) => {
    open(data.id);
  }, []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Table = () => {
  const { t } = useTranslation();
  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  return (
    <Wrapper style={{ height: '100%', padding: '24px' }}>
      <Grid rootComponent={Root} rows={data} columns={columns}>
        <DragDropProvider />
        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow cellComponent={TableHeaderCell} />
        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
      </Grid>
    </Wrapper>
  );
};

export { Table };
