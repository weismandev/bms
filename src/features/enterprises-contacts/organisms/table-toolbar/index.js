import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Toolbar, FoundInfo, Greedy, AddButton } from '../../../../ui';
import { EnterprisesCompaniesSelect } from '../../../enterprises-companies-select';
import { addClicked, $mode } from '../../model/detail.model';
import { $totalCount, $currentCompany, changeCompany } from '../../model/table.model';

const useStyles = makeStyles({
  margin: { margin: '0 10px' },
});

const TableToolbar = () => {
  const { t } = useTranslation();
  const totalCount = useStore($totalCount);
  const currentCompany = useStore($currentCompany);
  const mode = useStore($mode);
  const classes = useStyles();

  return (
    <Toolbar>
      <FoundInfo className={classes.margin} count={totalCount} />
      <div style={{ marginLeft: 20, width: 300 }}>
        <EnterprisesCompaniesSelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={false}
          placeholder={t('ChooseCompany')}
          firstAsDefault
        />
      </div>
      <Greedy />
      <AddButton
        className={classes.margin}
        onClick={addClicked}
        disabled={mode === 'edit' || !currentCompany}
      />
    </Toolbar>
  );
};

export { TableToolbar };
