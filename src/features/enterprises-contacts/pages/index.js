import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '../../../ui';
import { HaveSectionAccess } from '../../common';
import '../model';
import { $isDetailOpen } from '../model/detail.model';
import {
  PageGate,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteConfirmed,
} from '../model/page.model';
import { Table, Detail } from '../organisms';

const EnterprisesContacts = (props) => {
  const { t } = useTranslation();
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const error = useStore($error);

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />
      <DeleteConfirmDialog
        content={t('AreYouSureYouWantDeleteThisContact')}
        header={t('DeletingContact')}
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <PageGate />
      <FilterMainDetailLayout main={<Table />} detail={isDetailOpen && <Detail />} />
    </>
  );
};

const RestrictedEnterprisesContacts = (props) => (
  <HaveSectionAccess>
    <EnterprisesContacts />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesContacts as EnterprisesContacts };
