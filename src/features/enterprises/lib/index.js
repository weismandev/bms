import i18n from '@shared/config/i18n';

const { t } = i18n;

export function formatParkSlotTitle({ number, lot, zone }) {
  const number_ = number || '[ - ]';
  const lotTitle = (Boolean(lot) && lot.title) || t('parking');
  const zoneTitle = Boolean(zone) && zone.title ? `> ${zone.title}` : '';

  return `${number_} ${lotTitle} ${zoneTitle}`;
}
