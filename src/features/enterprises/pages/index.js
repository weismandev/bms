import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isDetailOpen,
  $isFilterOpen,
} from '../models';
import { Table, Detail, Filter } from '../organisms';

const EnterprisesPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  const error = useStore($error);
  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedEnterprisesPage = (props) => (
  <HaveSectionAccess>
    <EnterprisesPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesPage as EnterprisesPage };
