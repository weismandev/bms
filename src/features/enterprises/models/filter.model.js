import { createEvent, createStore } from 'effector';

const defaultFilters = {
  is_verified: 'all',
  is_owner: 'all',
  is_renter: 'all',
};

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = createStore(false);
const $filters = createStore(defaultFilters);

export {
  changedFilterVisibility,
  $isFilterOpen,
  filtersSubmitted,
  $filters,
  defaultFilters,
};
