import { sample, guard, merge, forward } from 'effector';
import { signout } from '../../common';
import { companiesApi } from '../api';
import {
  fxSetOwner,
  fxGetOwnedProperty,
  $ownedProperty,
  propertySubmitted,
  $mode,
  EnterprisePropertyGate,
  $isLoading,
  $error,
  deletePropertyClicked,
  deletePropertyConfirmed,
  deleteProrpertyDialogVisibility,
  $propertyWillBeDeleted,
  fxDeleteProperty,
} from './enterprises-property.model';

fxSetOwner.use(companiesApi.setOwner);
fxGetOwnedProperty.use(companiesApi.getOwnedProperty);
fxDeleteProperty.use(companiesApi.deleteProperty);

const { open, close, state } = EnterprisePropertyGate;

$ownedProperty
  .on(fxGetOwnedProperty.doneData, (state, { items }) =>
    Array.isArray(items) ? items : []
  )
  .reset([signout, state, close]);

$isLoading.reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [fxSetOwner.failData, fxGetOwnedProperty.failData, fxDeleteProperty.failData],
    (_, error) => error
  )
  .reset([signout, newRequestStarted]);

forward({
  from: deletePropertyClicked,
  to: deleteProrpertyDialogVisibility.prepend(() => true),
});

forward({
  from: fxDeleteProperty.done,
  to: deleteProrpertyDialogVisibility.prepend(() => false),
});

$propertyWillBeDeleted.on(deletePropertyClicked, (_, prop) => prop);

sample({
  source: $propertyWillBeDeleted,
  clock: deletePropertyConfirmed,
  fn: ({ id }) => ({ property_id: id }),
  target: fxDeleteProperty,
});

guard({
  source: sample(state, propertySubmitted, ({ enterprise_id }, { property }) => ({
    enterprise_id,
    property_id: property.id,
  })),
  filter: ({ enterprise_id }) => Boolean(enterprise_id) || enterprise_id === 0,
  target: fxSetOwner,
});

guard({
  source: state,
  clock: [fxSetOwner.done, fxDeleteProperty.done, state.updates],
  filter: ({ enterprise_id }) => Boolean(enterprise_id),
  target: fxGetOwnedProperty,
});

sample({
  source: [fxSetOwner.pending, fxGetOwnedProperty.pending, fxDeleteProperty.pending],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

$mode.reset([signout, fxGetOwnedProperty.done]);
