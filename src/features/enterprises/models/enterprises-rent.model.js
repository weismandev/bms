import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const EnterpriseRentGate = createGate();

const changeMode = createEvent();
const rentSubmitted = createEvent();

const fxRentsCreate = createEffect();
const fxGetRented = createEffect();

const $rentsList = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);

const $mode = restore(changeMode, 'view');

export {
  changeMode,
  rentSubmitted,
  fxRentsCreate,
  fxGetRented,
  $mode,
  $rentsList,
  EnterpriseRentGate,
  $isLoading,
  $error,
};
