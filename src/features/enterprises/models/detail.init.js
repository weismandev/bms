import { merge, forward, attach, sample, guard } from 'effector';
import {
  signout,
  readNotifications,
  ENTERPRISE_CREATED_BY_OWNER,
  NEW_ENTERPRISE_ON_VERIFICATION,
} from '@features/common';
import i18n from '@shared/config/i18n';
import { formatFile } from '@tools/formatFile';
import { formatPhone } from '@tools/formatPhone';
import { pick } from '@tools/pick';
import {
  $mode,
  changeMode,
  open,
  $opened,
  changedDetailVisibility,
  $isDetailOpen,
  addClicked,
  detailClosed,
  entityApi,
  newEntity,
  $currentTab,
  fxGetEmployeesList,
  $employees,
} from './detail.model';
import {
  fxCreate,
  fxUpdate,
  fxGetById,
  fxVerify,
  fxRejectToNotVerified,
} from './page.model';

const { t } = i18n;

$isDetailOpen
  .on(changedDetailVisibility, (state, visibility) => visibility)
  .on([addClicked, fxGetById.done], () => true)
  .reset(signout);

$mode
  .on(changeMode, (_, mode) => mode)
  .on(addClicked, () => 'edit')
  .on([detailClosed, fxCreate.done, fxUpdate.done, open], () => 'view')
  .reset(signout);

$opened
  .on(merge([addClicked, detailClosed]), () => newEntity)
  .on(
    sample({
      source: $opened,
      clock: merge([fxVerify.done, fxRejectToNotVerified.done]),
      fn: matchIdDetails,
    }),
    (_, details) => details
  )
  .on([fxGetById.doneData, fxCreate.doneData, fxUpdate.doneData], (_, { enterprise }) =>
    formatOpenedEntity(enterprise)
  )
  .reset(signout);

$employees
  .on(fxGetEmployeesList.doneData, (_, { items }) =>
    Array.isArray(items) ? formatEmlployee(items) : []
  )
  .reset([open, signout]);

$currentTab.on($opened.updates, () => 'entity').reset(signout);

function formatEmlployee(employees) {
  return employees.map(({ employee_id, fullname }) => ({
    id: employee_id || null,
    fullname: fullname || `-${t('noName')}-`,
  }));
}

function formatOpenedEntity(entity) {
  return {
    ...pick(['id', 'title', 'director_id'], entity),
    ogrn: entity.ogrn ? entity.ogrn : '',
    inn: entity?.inn || '',
    kpp: entity?.kpp || '',
    okved: entity?.okved || '',
    director: entity?.director || {},
    pass_limit: entity?.pass_limit || 0,
    attachments: entity?.attachments || [],
    building: entity?.building_id || '',
    confirmation_mode: entity?.confirmation_mode || '',
    guest_confirmation_mode: entity?.guest_confirmation_mode || '',
    sms_pass_mode: entity.sms_pass_mode,
    logo: entity.logo ? formatFile(entity.logo) : '',
    is_verified: entity.is_verified,
    responsible: entity?.responsible || {},
    is_custom_guest_scud_pass_limit: entity.is_custom_guest_scud_pass_limit,
    guest_scud_pass_limit: Number(entity?.guest_scud_pass_limit) || 0,
  };
}

function addResponsibleToPayload(payload) {
  const { name, phone, email } = payload.responsible;

  if (!name && (!phone || !email)) {
    delete payload.responsible;
  }

  return payload;
}

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: ({
      building,
      pass_limit,
      confirmation_mode,
      guest_confirmation_mode,
      logo,
      sms_pass_mode,
      ...params
    }) => {
      const payload = addResponsibleToPayload({
        ...params,
        building_id: typeof building === 'object' ? building.id : building || '',
      });

      if (pass_limit > 0) {
        payload.pass_limit = pass_limit;
      }

      if (confirmation_mode) {
        payload.confirmation_mode = confirmation_mode.slug;
      }

      if (guest_confirmation_mode) {
        payload.guest_confirmation_mode = guest_confirmation_mode.slug;
      }

      if (logo) {
        payload.logo_id = logo.id;
      }

      if (sms_pass_mode) {
        payload.sms_pass_mode = sms_pass_mode.value ? sms_pass_mode.value : sms_pass_mode;
      }

      return {
        enterprise: { ...payload },
      };
    },
  }),
});

forward({
  from: entityApi.update,
  to: attach({
    effect: fxUpdate,
    mapParams: ({
      building,
      pass_limit,
      confirmation_mode,
      guest_confirmation_mode,
      logo,
      director_id,
      director,
      responsible,
      sms_pass_mode,
      ...params
    }) => {
      const payload = {
        ...params,
        director: director_id,
        phone: director?.phone ? formatPhone(director.phone) : '',
        email: director?.email || '',
        responsible_id: responsible?.id || null,
      };

      if (pass_limit > 0) {
        payload.pass_limit = pass_limit;
      }

      if (confirmation_mode) {
        payload.confirmation_mode = confirmation_mode.slug;
      }

      if (guest_confirmation_mode) {
        payload.guest_confirmation_mode = guest_confirmation_mode.slug;
      }

      if (logo) {
        payload.logo_id = logo.id;
      }

      if (sms_pass_mode) {
        payload.sms_pass_mode = sms_pass_mode.value ? sms_pass_mode.value : sms_pass_mode;
      }

      return {
        enterprise: { ...payload },
      };
    },
  }),
});

forward({
  from: open,
  to: fxGetEmployeesList.prepend((id) => ({ id })),
});

function matchIdDetails(opened, { result: { enterprise } }) {
  if (opened.id === enterprise.id) {
    return {
      ...opened,
      is_verified: enterprise.is_verified,
    };
  }
  return opened;
}

guard({
  source: $opened,
  filter: ({ id }) => Boolean(id),
  target: readNotifications.prepend(({ id }) => ({
    id,
    event: ENTERPRISE_CREATED_BY_OWNER,
  })),
});

guard({
  source: $opened,
  filter: ({ id }) => Boolean(id),
  target: readNotifications.prepend(({ id }) => ({
    id,
    event: NEW_ENTERPRISE_ON_VERIFICATION,
  })),
});
