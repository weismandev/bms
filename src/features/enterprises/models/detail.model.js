import { createStore, createEvent, split, restore, guard, createEffect } from 'effector';
import i18n from '@shared/config/i18n';
import { companiesApi } from '../api';

const { t } = i18n;

const newEntity = {
  name: '',
  phone: '',
  email: '',
  responsible: {
    name: '',
    phone: '',
    email: '',
  },
  logo: '',
  building: '',
  title: '',
  ogrn: '',
  inn: '',
  kpp: '',
  okved: '',
  confirmation_mode: '',
  guest_confirmation_mode: '',
  sms_pass_mode: 'configurable_by_renter',
  pass_limit: 100,
  is_verified: null,
  is_custom_guest_scud_pass_limit: null,
  guest_scud_pass_limit: 0,
};

const fxGetEmployeesList = createEffect(companiesApi.getEmployeesList);

const selectTab = createEvent();
const changedDetailVisibility = createEvent();
const changeMode = createEvent();
const detailSubmitted = createEvent();
const addClicked = createEvent();
const open = createEvent();

const $isDetailOpen = createStore(false);
const $mode = createStore('view');
const $opened = createStore(newEntity);
const $currentTab = restore(selectTab, 'entity');
const $employees = createStore([]);

const detailClosed = guard($isDetailOpen, { filter: (bool) => !bool });

const entityApi = split(detailSubmitted, {
  create: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

const tabsConfig = [
  {
    label: t('information'),
    value: 'entity',
    onCreateIsDisabled: false,
  },
  {
    label: t('OwnedPremises'),
    value: 'property',
    onCreateIsDisabled: true,
  },
  {
    label: t('PremisesForRent'),
    value: 'rents',
    onCreateIsDisabled: true,
  },
  {
    label: t('navMenu.enterprises-parking'),
    value: 'parking',
    onCreateIsDisabled: true,
  },
];

export {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  entityApi,
  addClicked,
  $isDetailOpen,
  $mode,
  open,
  $opened,
  detailClosed,
  newEntity,
  selectTab,
  $currentTab,
  tabsConfig,
  fxGetEmployeesList,
  $employees,
};
