import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const ParkingSlotsGate = createGate();

const changeMode = createEvent();
const slotSubmitted = createEvent();
const changeDeleteData = createEvent();
const confirm = createEvent();

const fxGetParkingSlots = createEffect();
const fxAddParkingSlot = createEffect();
const fxDeleteParkingSlot = createEffect();

const $slots = createStore([]);
const $slotsPageParams = createStore({});
const $isLoading = createStore(false);
const $error = createStore(null);
const $mode = restore(changeMode, 'view');
const $deleteData = restore(changeDeleteData, { isOpen: false });
const changeSlotsPage = createEvent();
const $currentPage = restore(changeSlotsPage, 1);

const $slotForExtendRent = createStore(null);
const clickExtendSlotRent = createEvent();
const submitExtendSlotRent = createEvent();
const fxExtendSlotRent = createEffect();

export {
  fxGetParkingSlots,
  changeSlotsPage,
  fxAddParkingSlot,
  fxDeleteParkingSlot,
  ParkingSlotsGate,
  changeMode,
  slotSubmitted,
  $slots,
  $currentPage,
  $slotsPageParams,
  $isLoading,
  $error,
  $mode,
  changeDeleteData,
  $deleteData,
  confirm,
  $slotForExtendRent,
  clickExtendSlotRent,
  submitExtendSlotRent,
  fxExtendSlotRent,
};
