import { signout } from '../../common';
import {
  changedFilterVisibility,
  $isFilterOpen,
  filtersSubmitted,
  $filters,
  defaultFilters,
} from './filter.model';

$isFilterOpen
  .on(changedFilterVisibility, (state, visibility) => visibility)
  .reset(signout);

$filters
  .on(filtersSubmitted, (state, filters) => (filters ? filters : defaultFilters))
  .reset(signout);
