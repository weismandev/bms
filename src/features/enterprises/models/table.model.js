import { createStore, createEvent, combine, restore } from 'effector';
import { throttle } from 'patronum/throttle';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

const pageNumberChanged = createEvent();
const pageSizeChanged = createEvent();
const columnWidthsChanged = createEvent();
const columnOrderChanged = createEvent();
const hiddenColumnChanged = createEvent();
const sortChanged = createEvent();
const verifyClicked = createEvent();
const searchChanged = createEvent();

const $currentPage = createStore(0);
const $pageSize = createStore(10);

const $columnWidths = createStore([
  { columnName: 'title', width: 300 },
  { columnName: 'fullname', width: 350 },
  { columnName: 'responsible_fullname', width: 350 },
  { columnName: 'email', width: 300 },
  { columnName: 'parking_slots_count', width: 350 },
  { columnName: 'phone', width: 150 },
  { columnName: 'limit', width: 350 },
  { columnName: 'qty', width: 350 },
  { columnName: 'is_verified', width: 400 },
  { columnName: 'is_renter', width: 150 },
  { columnName: 'is_owner', width: 150 },
]);

const $columnOrder = createStore([
  'title',
  'fullname',
  'responsible_fullname',
  'email',
  'parking_slots_count',
  'phone',
  'limit',
  'qty',
  'is_verified',
  'is_renter',
  'is_owner',
]);

const $sorting = createStore([]);

const $hiddenColumnNames = createStore([]);
const $search = createStore('');

const $tableParams = combine(
  $currentPage,
  $pageSize,
  restore(throttle({ source: searchChanged, timeout: 500 }), ''),
  $sorting,
  (page, per_page, search, [sorting]) => ({
    page: page + 1,
    per_page,
    search,
    order: sorting
      ? {
          key: sorting.columnName,
          order: sorting.direction,
        }
      : [],
  })
);

const $tableData = $raw.map((data) =>
  formatDataToTable(Array.isArray(data.enterprises) ? data.enterprises : [])
);

const $rowCount = $raw.map((data) => (data.meta ? data.meta.total : 0));

function formatDataToTable(items) {
  return items.map(formatItem);
}

function formatItem(item = {}) {
  return {
    id: item.id,
    title: item.title || t('Untitled'),
    fullname: (item.director && item.director.fullname) || t('isNotDefinedit'),
    responsible_fullname: item.responsible?.fullname || t('isNotDefinedit'),
    email: (item.director && item.director.email) || t('isNotDefined'),
    parking_slots_count:
      item.parking_slots_count || item.parking_slots_count === 0
        ? item.parking_slots_count
        : t('isNotDefinedit'),
    phone: item?.director?.phone ? `+${item.director.phone}` : 'Не определен',
    limit:
      item.pass_limit || item.pass_limit === 0 ? item.pass_limit : t('isNotDefinedit'),
    qty: item.pass_count || item.pass_count === 0 ? item.pass_count : t('isNotDefinedit'),
    is_verified: Boolean(item.is_verified),
    is_renter: item.is_renter ? t('yes') : t('no'),
    is_owner: item.is_owner ? t('yes') : t('no'),
  };
}

export {
  $currentPage,
  $pageSize,
  $rowCount,
  $tableData,
  $tableParams,
  pageNumberChanged as pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  verifyClicked,
  $sorting,
  sortChanged,
  searchChanged,
  $search,
};
