import { createEffect, createStore, createEvent, merge } from 'effector';
import { createPageBag } from '@tools/factories';
import { companiesApi } from '../api';

export const changedErrorDialogVisibility = createEvent();
export const fileLoadingSubmitted = createEvent();
export const downloadTemplateClicked = createEvent();
export const reject = createEvent();
export const rejectToNotVerified = createEvent();

export const fxVerify = createEffect();
export const fxImportFile = createEffect();
export const fxReject = createEffect();
export const fxRejectToNotVerified = createEffect();

export const {
  PageGate,
  pageMounted,
  pageUnmounted,
  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,

  $isLoading,
  $error,
  $raw,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
} = createPageBag(companiesApi);

export const requestOccured = merge([
  fxGetList.pending,
  fxGetById.pending,
  fxVerify.pending,
  fxCreate.pending,
  fxUpdate.pending,
  fxImportFile.pending,
  fxRejectToNotVerified.pending,
]);

export const errorOccured = merge([
  fxGetList.fail,
  fxGetById.fail,
  fxVerify.fail,
  fxCreate.fail,
  fxUpdate.fail,
  fxImportFile.fail,
  fxRejectToNotVerified.fail,
]);

export const $normalizedData = $raw.map((data) =>
  Array.isArray(data.enterprises)
    ? data.enterprises.reduce((acc, i) => ({ ...acc, [i.id]: i }), {})
    : {}
);
