import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const EnterprisePropertyGate = createGate();

const changeMode = createEvent();
const propertySubmitted = createEvent();
const deleteProrpertyDialogVisibility = createEvent();
const deletePropertyClicked = createEvent();
const deletePropertyConfirmed = createEvent();

const fxSetOwner = createEffect();
const fxDeleteProperty = createEffect();
const fxGetOwnedProperty = createEffect();

const $ownedProperty = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);
const $propertyWillBeDeleted = createStore({ title: '' });
const $isDeletePropertyDialogOpen = restore(deleteProrpertyDialogVisibility, false);

const $mode = restore(changeMode, 'view');

export {
  changeMode,
  propertySubmitted,
  fxSetOwner,
  fxGetOwnedProperty,
  $mode,
  $ownedProperty,
  EnterprisePropertyGate,
  $isLoading,
  $error,
  deletePropertyClicked,
  deletePropertyConfirmed,
  deleteProrpertyDialogVisibility,
  $isDeletePropertyDialogOpen,
  $propertyWillBeDeleted,
  fxDeleteProperty,
};
