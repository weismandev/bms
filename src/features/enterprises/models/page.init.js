import { forward, attach, merge, guard, sample } from 'effector';
import FileSaver from 'file-saver';
import { signout } from '../../common';
import { companiesApi } from '../api';
import { open } from './detail.model';
import { $filters } from './filter.model';
import {
  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxVerify,
  fxImportFile,
  $raw,
  pageMounted,
  pageUnmounted,
  requestOccured,
  errorOccured,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fileLoadingSubmitted,
  downloadTemplateClicked,
  fxReject,
  reject,
  rejectToNotVerified,
  fxRejectToNotVerified,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

$raw
  .on(fxGetList.done, (state, { result }) => result)
  .on(
    merge([fxGetById.done, fxVerify.done, fxRejectToNotVerified.done]),
    (state, { result, params: updatedId }) =>
      mergeCompaniesDataAndUpdatedCompany(state, updatedId, result.enterprise)
  )
  .on(fxUpdate.done, (state, { result }) =>
    mergeCompaniesDataAndUpdatedCompany(
      state,
      result.enterprise ? result.enterprise.id : -1,
      result.enterprise
    )
  )
  .on(fxCreate.done, mergeCompaniesDataAndCreatedCompany)
  .on(fxReject.done, ({ enterprises, meta = {} }, { params }) => {
    return {
      enterprises: Array.isArray(enterprises)
        ? enterprises.filter(({ id }) => String(id) !== String(params.enterprise_id))
        : [],
      meta: { ...meta, total: meta.total - 1 },
    };
  })
  .reset(signout);

$isLoading
  .on(requestOccured, (state, pending) => pending)
  .reset([pageUnmounted, signout]);

const newRequestStarted = requestOccured.filter({
  fn: (isLoading) => isLoading,
});

$error
  .on(errorOccured, (state, { error }) => error)
  .on(newRequestStarted, () => null)
  .reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (state, visibility) => visibility)
  .on(errorOccured, () => true)
  .on(newRequestStarted, () => false)
  .reset([pageUnmounted, signout]);

fxVerify.use(companiesApi.verify);
fxImportFile.use(companiesApi.uploadFile);
fxReject.use(companiesApi.reject);
fxRejectToNotVerified.use(companiesApi.rejectToNotVerified);

guard({
  source: [$tableParams, $filters],
  clock: [pageMounted, $tableParams, $filters, fxImportFile.done, $settingsInitialized],
  filter: $settingsInitialized,
  target: fxGetList.prepend(([tableParams, filters], params) => ({
    ...params,
    ...tableParams,
    filter: {
      is_verified: filters.is_verified.id ? filters.is_verified.id : 'all',
      is_owner: filters.is_owner.id ? filters.is_owner.id : 'all',
      is_renter: filters.is_renter.id ? filters.is_renter.id : 'all',
    },
  })),
});

forward({
  from: fileLoadingSubmitted.map(({ file: fileData, building }) => ({
    import: fileData && fileData.file,
    building_id: building && building.id,
  })),
  to: fxImportFile,
});

forward({
  from: open,
  to: fxGetById,
});

forward({ from: reject, to: fxReject });
forward({ from: rejectToNotVerified, to: fxRejectToNotVerified });

forward({ from: $isGettingSettingsFromStorage, to: $isLoading });

downloadTemplateClicked.watch(() => {
  FileSaver.saveAs(
    'https://files.mysmartflat.ru/crm/templates/enterprise_import_template.xlsx',
    'enterprise_import_template.xlsx'
  );
});

function mergeCompaniesDataAndUpdatedCompany(state, updatedId, updatedCompany) {
  const updatedIdx = Array.isArray(state.enterprises)
    ? state.enterprises.findIndex((i) => String(i.id) === String(updatedId))
    : -1;

  const enterprises =
    updatedIdx > -1
      ? state.enterprises
          .slice(0, updatedIdx)
          .concat(updatedCompany)
          .concat(state.enterprises.slice(updatedIdx + 1))
      : state.enterprises;

  return {
    ...state,
    enterprises,
  };
}

function mergeCompaniesDataAndCreatedCompany(state, { result }) {
  return {
    ...state,
    enterprises: state.enterprises.concat(result.enterprise),
    meta: { ...state.meta, total: state.meta.total + 1 },
  };
}
