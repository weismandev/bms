import { sample, guard, merge } from 'effector';
import { signout } from '../../common';
import { companiesApi } from '../api';
import {
  fxRentsCreate,
  fxGetRented,
  $rentsList,
  rentSubmitted,
  $mode,
  EnterpriseRentGate,
  $isLoading,
  $error,
} from './enterprises-rent.model';

fxRentsCreate.use(companiesApi.rentsCreate);
fxGetRented.use(companiesApi.getCompanyRented);

$rentsList
  .on(fxGetRented.doneData, (state, { properties }) =>
    Array.isArray(properties) ? properties : []
  )
  .reset([signout, EnterpriseRentGate.state]);

$isLoading.reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on([fxGetRented.failData, fxRentsCreate.failData], (state, error) => error)
  .reset([signout, newRequestStarted]);

guard({
  source: sample(
    EnterpriseRentGate.state,
    rentSubmitted,
    ({ userdata_id, company_id }, { property, start_date, finish_date }) => ({
      rents: [
        {
          company_id,
          userdata_id,
          property_id: property.id,
          start_date: start_date ? new Date(start_date) / 1000 : '',
          finish_date: finish_date ? new Date(finish_date) / 1000 : '',
        },
      ],
    })
  ),
  filter: ({ rents }) =>
    (Boolean(rents[0]) && Boolean(rents[0].userdata_id)) || rents[0].userdata_id === 0,
  target: fxRentsCreate,
});

const savedSuccess = fxRentsCreate.done.map(({ params }) => ({
  company_id: params.rents && params.rents[0].company_id,
}));

guard({
  source: merge([savedSuccess, EnterpriseRentGate.state]),
  filter: ({ company_id }) => Boolean(company_id),
  target: fxGetRented,
});

sample({
  source: [fxGetRented.pending, fxRentsCreate.pending],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

$mode.reset([signout, EnterpriseRentGate.close, fxGetRented.done]);
