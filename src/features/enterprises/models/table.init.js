import { forward, merge } from 'effector';
import { signout } from '../../common';
import { $filters } from './filter.model';
import { pageUnmounted, fxVerify } from './page.model';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  verifyClicked,
  $sorting,
  sortChanged,
  $search,
  searchChanged,
} from './table.model';

$currentPage
  .on(pageNumberChanged, (state, number) => number)
  .on(merge([pageSizeChanged, searchChanged, $filters]), (state) => 0)
  .reset([pageUnmounted, signout]);

$sorting.on(sortChanged, (state, payload) => payload).reset([pageUnmounted, signout]);

$search.on(searchChanged, (state, value) => value).reset([pageUnmounted, signout]);

$columnWidths.on(columnWidthsChanged, (state, widths) => widths).reset(signout);
$columnOrder.on(columnOrderChanged, (state, order) => order).reset(signout);

$hiddenColumnNames.on(hiddenColumnChanged, (state, names) => names).reset(signout);

$pageSize.on(pageSizeChanged, (state, qty) => qty).reset([signout]);

forward({ from: verifyClicked, to: fxVerify });
