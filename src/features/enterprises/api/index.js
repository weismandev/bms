import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'enterprises/get-list', payload);
const getById = (id) => api.v1('get', 'enterprises/get', { id });
const create = (payload) =>
  api.v1('post', 'enterprises/admin/create', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const update = (payload) =>
  api.v1('post', 'enterprises/admin/update', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const verify = (id) => api.v1('post', 'enterprises/admin/verify', { id });
const reject = (payload) => api.v1('post', 'enterprises/admin/reject', payload);
const rejectToNotVerified = (id) =>
  api.v1('post', 'enterprises/admin/reject-on-to-not-verified', { id });
const uploadFile = (payload) => api.v1('post', 'enterprises/admin/import', payload);

const getOwnedProperty = (payload) =>
  api.v1('get', 'property/get-owned-by-enterprise', payload);
const setOwner = (payload) => api.v1('post', 'property/set-owner', payload);
const deleteProperty = (payload) => api.v1('post', 'property/remove-owner', payload);

const getCompanyRented = (payload) =>
  api.v1('get', 'property/get-company-rented', { per_page: 1000, ...payload });

const rentsCreate = (payload) => api.v1('post', 'rents/crm-create', payload);

const getParkingSlots = (payload) =>
  api.v1('get', 'parking/enterprise_rent/enterprise-slots', {
    ...payload,
    type: 'all',
  });

const addParkingSlot = (payload) =>
  api.v1('post', 'parking/enterprise_rent/add-slot-to-enterprise', payload);

const deleteParkingSlot = (payload) =>
  api.v1('post', 'parking/enterprise_rent/delete-slot-rent-from-enterprise', payload);

const extendParkingSlotRent = (payload) =>
  api.v1('post', 'parking/enterprise_rent/extend-rent', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const getEmployeesList = (payload) =>
  api.v1('get', 'employee/get-enterprise-employees', payload);

export const companiesApi = {
  getList,
  getById,
  create,
  update,
  verify,
  reject,
  rejectToNotVerified,
  uploadFile,
  getOwnedProperty,
  setOwner,
  deleteProperty,
  getCompanyRented,
  rentsCreate,
  getParkingSlots,
  addParkingSlot,
  deleteParkingSlot,
  extendParkingSlotRent,
  getEmployeesList,
};
