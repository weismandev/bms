import { useStore } from 'effector-react';
import {
  Toolbar,
  FoundInfo,
  AddButton,
  FilterButton,
  Greedy,
  SearchInput,
  DownloadTemplateButton,
} from '../../../../ui';
import {
  $rowCount,
  addClicked,
  $mode,
  changedFilterVisibility,
  $isFilterOpen,
  $search,
  searchChanged,
  downloadTemplateClicked,
} from '../../models';
import { ImportForm } from '../import-form';

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const mode = useStore($mode);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ margin: '0 10px' }} />
      <SearchInput
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <DownloadTemplateButton
        onClick={downloadTemplateClicked}
        style={{ marginRight: 10 }}
      />
      <ImportForm />
      <AddButton
        disabled={mode === 'edit'}
        onClick={addClicked}
        style={{ margin: '0 10px' }}
      />
    </Toolbar>
  );
};

export { TableToolbar };
