import { useMemo, useCallback, memo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  DataTypeProvider,
  SortingState,
  CustomPaging,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar as DXToolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  SortingLabel,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $opened,
  open,
  $sorting,
  sortChanged,
  $rowCount,
} from '../../models';
import { StatusActions } from '../status-actions';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <DXToolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const columns = [
  { name: 'title', title: t('CompanyName') },
  { name: 'fullname', title: t('nameDirector') },
  { name: 'responsible_fullname', title: t('NameResponsiblePerson') },
  { name: 'email', title: 'Email' },
  { name: 'parking_slots_count', title: t('CountOfParkingSpaces') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'limit', title: t('PassLimit') },
  { name: 'qty', title: t('PassesIssued') },
  { name: 'is_verified', title: t('Label.status') },
  { name: 'is_renter', title: t('tenant') },
  { name: 'is_owner', title: t('owner') },
];

const VerifiedTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StatusActions} {...props} />
);

const Table = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const sorting = useStore($sorting);
  const rowCount = useStore($rowCount);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <DragDropProvider />
        <SortingState sorting={sorting} onSortingChange={sortChanged} />
        <CustomPaging totalCount={rowCount} />
        <VerifiedTypeProvider for={['is_verified']} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel
              excludeSortColumns={[
                'fullname',
                'responsible_fullname',
                'email',
                'phone',
                'parking_slots_count',
                'limit',
                'is_verified',
                'qty',
                'is_renter',
                'is_owner',
              ]}
              {...props}
            />
          )}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <DXToolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
