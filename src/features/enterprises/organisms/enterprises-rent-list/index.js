import { useTranslation } from 'react-i18next';
import { useList } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { LabeledContent, CustomScrollbar } from '../../../../ui';
import { $rentsList } from '../../models/enterprises-rent.model';

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 0 24px',
    height: '100%',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  part: {
    marginBottom: 20,
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
});

const EnterprisesRentList = (props) => {
  const { listOuter, listInner } = useStyles();
  const list = useList($rentsList, (property) => <RentCard {...property} />);
  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
    </div>
  );
};

const RentCard = (props) => {
  const { title } = props;
  const { t } = useTranslation();
  const { part, card, text } = useStyles();

  return (
    <div className={card}>
      <LabeledContent className={part} label={t('Name')}>
        <span className={text}>{title}</span>
      </LabeledContent>
    </div>
  );
};

export { EnterprisesRentList };
