import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  ActionFileWrapper,
  Tabs,
  LogoControlField,
  CustomScrollbar,
  SelectField,
  EmployeeCard,
  Checkbox,
  EmailMaskedInput,
  SomePhoneMaskedInput,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import { Tab as MuiTab, FormControlLabel } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { EnterprisesConfirmationModeSelectField } from '../../../enterprises-confirmation-mode-select';
import { HouseSelectField } from '../../../house-select';
import { ResetAndNotifyPassword } from '../../../reset-password';
import { SmsPassModeSelectField } from '../../../sms-pass-mode-select';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  $employees,
} from '../../models';
import { tabsConfig, $currentTab, selectTab } from '../../models/detail.model';
import { EnterprisesParking } from '../enterprises-parking';
import { EnterprisesProperty } from '../enterprises-property';
import { EnterprisesRent } from '../enterprises-rent';
import { StatusActions } from '../status-actions';
import { validationSchema } from './validation';

const ResponsibleFields = () => {
  const { t } = useTranslation();

  return (
    <>
      <Field
        name="responsible.name"
        component={InputField}
        required
        render={({ field, form }) => (
          <InputField
            field={field}
            form={form}
            label={t('nameResponsible')}
            placeholder={t('EnterTheFullNameOfTheResponsible')}
          />
        )}
      />
      <Field
        name="responsible.phone"
        required
        render={({ field, form }) => (
          <InputField
            field={field}
            form={form}
            inputComponent={SomePhoneMaskedInput}
            label={t('Label.phone')}
            placeholder={t('EnterPhone')}
            inputProps={{ form }}
          />
        )}
      />
      <Field
        name="responsible.email"
        required
        render={({ field, form }) => (
          <InputField
            field={field}
            form={form}
            inputComponent={EmailMaskedInput}
            label="Email"
            placeholder={t('EnterEmail')}
          />
        )}
      />
    </>
  );
};

const Detail = () => {
  const currentTab = useStore($currentTab);
  const opened = useStore($opened);
  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Tabs
        options={tabsConfig}
        currentTab={currentTab}
        onChange={(e, value) => selectTab(value)}
        isNew={isNew}
        tabEl={<MuiTab style={{ minWidth: '50%' }} />}
      />

      <div style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
        {currentTab === 'entity' && <CompanyForm opened={opened} isNew={isNew} />}
        {currentTab === 'property' && <EnterprisesProperty enterprise_id={opened.id} />}
        {currentTab === 'rents' && (
          <EnterprisesRent company_id={opened.id} userdata_id={opened.director_id} />
        )}
        {currentTab === 'parking' && <EnterprisesParking enterprise_id={opened.id} />}
      </div>
    </Wrapper>
  );
};

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(100% - 82px)',
    padding: '0 6px 24px 24px',
  },
  padding: { paddingRight: 18 },
  labelCheckbox: {
    color: '#9494A3',
    fontSize: 14,
    margin: 0,
  },
  labelCheckboxMarginBottom: {
    color: '#9494A3',
    fontSize: 14,
    margin: '0 0 10px',
  },
  helpText: {
    color: '#9494A3',
    fontSize: 14,
    margin: '0 0 20px 10px',
    paddingBottom: 7,
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
});

const CompanyForm = ({ opened, isNew }) => {
  const { t } = useTranslation();
  const mode = useStore($mode);
  const employees = useStore($employees);
  const { container, padding, labelCheckbox, helpText, labelCheckboxMarginBottom } =
    useStyles();
  const [isResponsible, setIsResponsible] = useState(false);

  const resetResponsibleFields = (form) => {
    Object.keys(form.values.responsible).forEach((item) => {
      form.setFieldValue(`responsible.${item}`, '');
    });
  };

  return (
    <Formik
      initialValues={opened}
      onSubmit={detailSubmitted}
      validationSchema={validationSchema({ isNew, isResponsible })}
      enableReinitialize
      render={(props) => (
        <Form style={{ height: '100%' }}>
          <DetailToolbar
            style={{ padding: 24 }}
            mode={mode}
            onEdit={() => changeMode('edit')}
            onClose={() => changedDetailVisibility(false)}
            onCancel={() => {
              if (isNew) {
                changedDetailVisibility(false);
              } else {
                changeMode('view');
                props.resetForm();
              }
            }}
            hidden={{ delete: true }}
          >
            {!isNew && (
              <ResetAndNotifyPassword context="enterprise" user_id={opened.director_id} />
            )}
          </DetailToolbar>
          <div className={container}>
            <CustomScrollbar>
              <div className={padding}>
                {isNew && (
                  <>
                    <Field
                      name="name"
                      component={InputField}
                      required
                      render={({ field, form }) => (
                        <InputField
                          field={field}
                          form={form}
                          label={t('nameDirector')}
                          placeholder={t('EnterTheFullNameOfTheDirector')}
                        />
                      )}
                    />
                    <Field
                      name="phone"
                      required
                      render={({ field, form }) => (
                        <InputField
                          field={field}
                          form={form}
                          inputComponent={SomePhoneMaskedInput}
                          label={t('Label.phone')}
                          placeholder={t('EnterPhone')}
                          inputProps={{ form }}
                        />
                      )}
                    />
                    <Field
                      name="email"
                      required
                      render={({ field, form }) => (
                        <InputField
                          field={field}
                          form={form}
                          inputComponent={EmailMaskedInput}
                          label="Email"
                          placeholder={t('EnterEmail')}
                        />
                      )}
                    />
                    <>
                      <FormControlLabel
                        classes={{ root: labelCheckboxMarginBottom }}
                        label={t('addTheResponsible')}
                        control={
                          <Field
                            name="is_responsible"
                            mode={mode}
                            helpText="test"
                            render={() => (
                              <Checkbox
                                checked={isResponsible}
                                onChange={() => {
                                  setIsResponsible((prev) => !prev);
                                  if (isResponsible) {
                                    resetResponsibleFields(props);
                                  }
                                }}
                              />
                            )}
                          />
                        }
                      />
                      {isResponsible && <ResponsibleFields />}
                      {!isResponsible && (
                        <p className={helpText}>{t('ResponsibleDefault')}</p>
                      )}
                    </>
                  </>
                )}
                {!isNew && (
                  <>
                    <EmployeeCard position={t('Director')} employee={opened.director} />
                    {mode === 'view' ? (
                      <EmployeeCard
                        position={t('Responsible')}
                        employee={opened.responsible}
                      />
                    ) : (
                      <Field
                        name="responsible"
                        component={SelectField}
                        label={`${t('Responsible')} (${t('ResponsibleDefault')})`}
                        labelPlacement="top"
                        placeholder={t('ChooseResponsiblePerson')}
                        options={employees}
                        mode={mode}
                        divider
                      />
                    )}
                  </>
                )}
                <Field
                  name="logo"
                  component={LogoControlField}
                  label={t('CompanyLogo')}
                  mode={mode}
                />
                <Field
                  name="building"
                  component={HouseSelectField}
                  label={t('Structure')}
                  mode={!isNew ? 'view' : mode}
                  placeholder={t('ChooseBuilding')}
                  required
                />
                <Field
                  name="title"
                  mode={mode}
                  component={InputField}
                  required
                  label={t('company')}
                  placeholder={t('EnterCompanyName')}
                />
                <Field
                  name="ogrn"
                  mode={mode}
                  component={InputField}
                  required
                  label={t('BIN')}
                  labelPlacement="start"
                  placeholder={t('EnterYourBIN')}
                />
                <Field
                  name="inn"
                  mode={mode}
                  component={InputField}
                  required
                  label={t('TIN')}
                  labelPlacement="start"
                  placeholder={t('EnterYourTIN')}
                />
                <Field
                  name="kpp"
                  mode={mode}
                  component={InputField}
                  label={t('CAT')}
                  labelPlacement="start"
                  placeholder={t('EnterYourCAT')}
                />
                <Field
                  name="okved"
                  mode={mode}
                  component={InputField}
                  label={t('RCEAP')}
                  labelPlacement="start"
                  placeholder={t('EnterYourRCEAP')}
                />
                <Field
                  name="confirmation_mode"
                  component={EnterprisesConfirmationModeSelectField}
                  label={t('PassConfirmationFromEmployees')}
                  placeholder={t('SelectMode')}
                  mode={mode}
                />
                <Field
                  name="guest_confirmation_mode"
                  component={EnterprisesConfirmationModeSelectField}
                  label={t('ConfirmationPassesFromGuests')}
                  placeholder={t('SelectMode')}
                  mode={mode}
                />
                <Field
                  component={SmsPassModeSelectField}
                  name="sms_pass_mode"
                  mode={mode}
                  label={t('SendingPassViaSMS')}
                />
                <Field
                  name="pass_limit"
                  mode={mode}
                  component={InputField}
                  inputProps={{ type: 'number', min: 0 }}
                  label={t('PassCreationLimit')}
                  placeholder={t('EnterPassLimit')}
                  endAdornment={<span>{t('thg')}.</span>}
                />
                {Array.isArray(opened.attachments) &&
                  opened.attachments.length > 0 &&
                  opened.attachments.map((file) => (
                    <ActionFileWrapper
                      key={file.name}
                      name={file.name}
                      mime={file.mime_type}
                      url={file.url}
                    />
                  ))}
                <FormControlLabel
                  classes={{ root: labelCheckbox }}
                  label={t('individualDurationGuestPass')}
                  control={
                    <Field
                      name="is_custom_guest_scud_pass_limit"
                      mode={mode}
                      render={({ field, form }) => (
                        <Checkbox
                          field={field}
                          form={form}
                          checked={form.values[field.name]}
                          onChange={(e) =>
                            form.setFieldValue(field.name, e.target.checked)
                          }
                          disabled={mode === 'view'}
                        />
                      )}
                    />
                  }
                />
                <Field
                  name="guest_scud_pass_limit"
                  mode={mode}
                  render={({ field, form }) => (
                    <InputField
                      mode={mode}
                      type="number"
                      field={field}
                      form={form}
                      label=""
                      placeholder={t('EnterExpirationDateGuestPass')}
                      disabled={!form?.values.is_custom_guest_scud_pass_limit}
                    />
                  )}
                />
              </div>
            </CustomScrollbar>
            <StatusActions
              row={{ id: opened.id }}
              value={opened.is_verified}
              cancelAction={() => changedDetailVisibility(false)}
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                padding: '20px 0',
              }}
            />
          </div>
        </Form>
      )}
    />
  );
};

export { Detail };
