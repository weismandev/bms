import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { validateSomePhone } from '@ui/atoms/some-phone-masked-input';

const { t } = i18n;

const validationMessage = t('FieldFilledIncorrectly');
const requiredMessage = t('thisIsRequiredField');

export const validationSchema = ({ isNew, isResponsible }) => {
  return Yup.object().shape({
    name: isNew ? Yup.string().required(requiredMessage) : Yup.string().nullable(),
    email: isNew
      ? Yup.string()
          .email(t('incorrectEmail'))
          .test(
            'email-or-phone-mandatory',
            t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
            testEmailOrPhoneProvided
          )
      : Yup.string().nullable(),
    phone: isNew
      ? Yup.string()
          .test(
            'email-or-phone-mandatory',
            t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
            testEmailOrPhoneProvided
          )
          .test('phone-length', t('invalidNumber'), validateSomePhone)
      : Yup.string().nullable(),

    responsible:
      isNew &&
      isResponsible &&
      Yup.object().shape({
        name:
          isNew && isResponsible
            ? Yup.string().required(requiredMessage)
            : Yup.string().nullable(),
        email:
          isNew && isResponsible
            ? Yup.string()
                .email(t('incorrectEmail'))
                .test(
                  'email-or-phone-mandatory',
                  t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
                  testEmailOrPhoneProvided
                )
            : Yup.string().nullable(),
        phone:
          isNew && isResponsible
            ? Yup.string()
                .test(
                  'email-or-phone-mandatory',
                  t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
                  testEmailOrPhoneProvided
                )
                .matches(/^\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/, {
                  message: t('invalidNumber'),
                  excludeEmptyString: true,
                })
            : Yup.string().nullable(),
      }),

    building: isNew ? Yup.mixed().required(requiredMessage) : Yup.mixed(),
    inn: Yup.string()
      .matches(/^\d*$/, t('UseNumbersFrom0to9'))
      .min(10, t('AtLeast10Characters'))
      .max(12, t('AtLeast12Characters'))
      .required(t('PleaseEnterYourTIN')),
    kpp: Yup.string().matches(/^\d{9}$/, t('Contains9Digits')),
    ogrn: Yup.string()
      .matches(/^\d{13}$/, t('Contains13Digits'))
      .required(t('PleaseEnterYourBIN')),
    title: Yup.string().required(requiredMessage),
    guest_scud_pass_limit: Yup.mixed().when('is_custom_guest_scud_pass_limit', {
      is: true,
      then: Yup.number()
        .min(0, validationMessage)
        .required(validationMessage)
        .typeError(validationMessage),
      otherwise: Yup.mixed().nullable(),
    }),
  });
};

function testEmailOrPhoneProvided() {
  if (!this.parent.email && !this.parent.phone) {
    return this.createError();
  }

  return true;
}
