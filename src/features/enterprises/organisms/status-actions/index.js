import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Field, Form, Formik } from 'formik';
import { Chip, IconButton, Toolbar } from '@mui/material';
import { Close } from '@mui/icons-material';
import { ActionButton, InputField, Modal } from '../../../../ui';
import { verifyClicked } from '../../models';
import { reject, rejectToNotVerified } from '../../models/page.model';

function CancelDialog(props) {
  const { isOpen, changeVisibility, revoke } = props;
  const { t } = useTranslation();

  return (
    <Modal
      onClick={(e) => e.stopPropagation()}
      isOpen={isOpen}
      header={
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <span>{t('CompanyRejection')}</span>
          <IconButton onClick={() => changeVisibility(false)} size="large">
            <Close />
          </IconButton>
        </div>
      }
      content={
        <Formik
          initialValues={{ comment: '' }}
          onSubmit={(values) => {
            changeVisibility(false);
            revoke(values);
          }}
          render={() => (
            <Form>
              <Field
                style={{ height: 120 }}
                name="comment"
                component={InputField}
                divider={false}
                required
                label={t('ReasonForRejection')}
                placeholder={t('EnterTheReasonForRejection')}
                rowsMax={5}
                multiline
              />
              <Toolbar
                style={{
                  height: 80,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <ActionButton
                  style={{ padding: '0 20px', marginRight: 10 }}
                  type="submit"
                >
                  {t('Reject')}
                </ActionButton>
              </Toolbar>
            </Form>
          )}
        />
      }
    />
  );
}

const StatusActions = ({ value, row, style, cancelAction }) => {
  const { t } = useTranslation();
  const [isCancelDialogOpen, changeCancelDialogVisibility] = useState(false);

  const actionHandler = (e, actionButton, params) => {
    e.stopPropagation();
    actionButton(params);
  };

  const cancelHandler = (values) => {
    if (cancelAction && typeof cancelAction === 'function') {
      cancelAction();
    }
    reject({ ...values, enterprise_id: row.id });
  };

  if (value === null) return <div style={style} />;

  return (
    <div style={style}>
      {value ? (
        <>
          <Chip label={t('Confirmed')} style={{ background: '#1BB169' }} />
          <ActionButton
            kind="negative"
            onClick={(e) => actionHandler(e, rejectToNotVerified, row.id)}
          >
            {t('BacktoReview')}
          </ActionButton>
        </>
      ) : (
        <>
          <ActionButton onClick={(e) => actionHandler(e, verifyClicked, row.id)}>
            {t('Confirm')}
          </ActionButton>
          <ActionButton
            kind="negative"
            onClick={(e) => actionHandler(e, changeCancelDialogVisibility, true)}
          >
            {t('Reject')}
          </ActionButton>
          <CancelDialog
            isOpen={isCancelDialogOpen}
            changeVisibility={changeCancelDialogVisibility}
            revoke={cancelHandler}
          />
        </>
      )}
    </div>
  );
};

export { StatusActions };
