import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Button } from '@mui/material';
import { Loader, ErrorMsg as ErrorMessage } from '../../../../ui';
import {
  $mode,
  changeMode,
  EnterpriseRentGate,
  $isLoading,
  $error,
} from '../../models/enterprises-rent.model';
import { EnterprisesRentForm } from '../enterprises-rent-form';
import { EnterprisesRentList } from '../enterprises-rent-list';

const EnterprisesRent = (props) => {
  const { userdata_id, company_id } = props;
  const { t } = useTranslation();
  const mode = useStore($mode);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const form = mode === 'edit' ? <EnterprisesRentForm /> : null;

  const addNewButton =
    mode === 'view' ? (
      <div
        style={{
          padding: 24,
          textAlign: 'center',
        }}
      >
        <Button onClick={() => changeMode('edit')} color="primary">
          {t('AddRental')}
        </Button>
      </div>
    ) : null;

  const list =
    mode === 'view' ? (
      <div
        style={{
          position: 'relative',
          height: 'calc(100% - 110px)',
        }}
      >
        <EnterprisesRentList />
      </div>
    ) : null;

  return (
    <>
      <ErrorMessage error={error} />
      <Loader isLoading={isLoading} />
      <EnterpriseRentGate userdata_id={userdata_id} company_id={company_id} />
      {addNewButton}
      {form}
      {list}
    </>
  );
};

export { EnterprisesRent };
