import { useTranslation } from 'react-i18next';
import { Formik, Form, Field } from 'formik';
import { DetailToolbar, InputField, CustomScrollbar } from '../../../../ui';
import { PropertySelectField } from '../../../property-select';
import { rentSubmitted, changeMode } from '../../models/enterprises-rent.model';

const EnterprisesRentForm = (props) => {
  const { t } = useTranslation();
  return (
    <Formik
      initialValues={{
        property: '',
        start_date: '',
        finish_date: '',
      }}
      onSubmit={rentSubmitted}
      render={({ resetForm, values, setFieldValue }) => {
        return (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              onCancel={() => changeMode('view')}
              hidden={{ edit: true, delete: true, close: true }}
              style={{ padding: 24, height: 82 }}
              mode="edit"
            />
            <div
              style={{
                padding: 24,
                paddingRight: 12,
                paddingTop: 0,
                height: 'calc(100% - 82px)',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 12 }}>
                  <Field
                    name="property"
                    component={PropertySelectField}
                    label={t('room')}
                    placeholder={t('ChooseRoom')}
                  />

                  <Field
                    name="start_date"
                    component={InputField}
                    label={t('startDate')}
                    type="date"
                  />

                  <Field
                    name="finish_date"
                    component={InputField}
                    label={t('expirationDate')}
                    type="date"
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};

export { EnterprisesRentForm };
