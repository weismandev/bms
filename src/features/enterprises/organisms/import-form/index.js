import { useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ImportButton,
  Popper,
  FileControl,
  BaseInputLabel,
  Divider,
  CloseButton,
  ActionButton,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import { Button, ClickAwayListener } from '@mui/material';
import { HouseSelectField } from '../../../house-select';
import { fileLoadingSubmitted } from '../../models';

export const ImportForm = () => {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);

  const rootRef = useRef();

  const close = () => setOpen(false);
  const open = () => setOpen(true);

  return (
    <ClickAwayListener onClickAway={close}>
      <div style={{ position: 'relative' }} ref={rootRef}>
        <ImportButton disabled={isOpen} onClick={open} />
        <Popper
          placement="bottom-end"
          open={isOpen}
          header={
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                flexWrap: 'nowrap',
              }}
            >
              <span>{t('ImportListCompanies')}</span>
              <CloseButton onClick={close} />
            </div>
          }
          anchorEl={rootRef.current}
        >
          <div style={{ width: 250 }}>
            <Formik
              onSubmit={fileLoadingSubmitted}
              initialValues={{ file: '', building: '' }}
              render={() => (
                <Form>
                  <Field
                    name="building"
                    component={HouseSelectField}
                    label={t('building')}
                    placeholder={t('selectBbuilding')}
                  />
                  <Field
                    name="file"
                    render={({ field, form }) => (
                      <>
                        <BaseInputLabel style={{ marginBottom: 10 }}>
                          {t('File')}
                        </BaseInputLabel>
                        <FileControl
                          encoding="file"
                          name={field.name}
                          value={field.value}
                          renderButton={() => (
                            <Button component="span" color="primary">
                              {field.value ? t('ReplaceFile') : t('UploadFile')}
                            </Button>
                          )}
                          renderPreview={() =>
                            field.value ? (
                              <span style={{ wordBreak: 'break-all' }}>
                                {`${t('Loaded')}: ${field.value.meta.name}`}
                              </span>
                            ) : null
                          }
                          onChange={(value) => form.setFieldValue(field.name, value)}
                          mode="edit"
                        />
                        <Divider />
                        <ActionButton
                          style={{ width: '100%' }}
                          type="submit"
                          kind="positive"
                        >
                          {t('Send')}
                        </ActionButton>
                      </>
                    )}
                  />
                </Form>
              )}
            />
          </div>
        </Popper>
      </div>
    </ClickAwayListener>
  );
};
