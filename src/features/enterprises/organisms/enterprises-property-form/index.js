import { useTranslation } from 'react-i18next';
import { Formik, Form, Field } from 'formik';
import { DetailToolbar, CustomScrollbar } from '../../../../ui';
import { FreeToRentPropertySelectField } from '../../../free-to-rent-property-select';
import { propertySubmitted, changeMode } from '../../models/enterprises-property.model';

const EnterprisesPropertyForm = (props) => {
  const { t } = useTranslation();
  return (
    <Formik
      initialValues={{
        property: '',
      }}
      onSubmit={propertySubmitted}
      render={({ resetForm, values, setFieldValue }) => {
        return (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              onCancel={() => changeMode('view')}
              hidden={{ edit: true, delete: true, close: true }}
              style={{ padding: 24, height: 82 }}
              mode="edit"
            />
            <div
              style={{
                padding: 24,
                paddingRight: 12,
                paddingTop: 0,
                height: 'calc(100% - 82px)',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 12 }}>
                  <Field
                    name="property"
                    component={FreeToRentPropertySelectField}
                    label={t('room')}
                    placeholder={t('ChooseRoom')}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};

export { EnterprisesPropertyForm };
