import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  SelectField,
  CustomScrollbar,
} from '../../../../ui';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';

const Filter = memo((props) => {
  const { t } = useTranslation();
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={(bag) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="is_verified"
                  component={SelectField}
                  label="Статус"
                  options={[
                    { id: 'yes', title: t('OnlyConfirmed') },
                    { id: 'no', title: t('OnlyUnconfirmed') },
                    { id: 'all', title: t('all') },
                  ]}
                />
                <Field
                  name="is_renter"
                  component={SelectField}
                  label="Арендатор"
                  options={[
                    { id: 'yes', title: t('yes') },
                    { id: 'no', title: t('no') },
                    { id: 'all', title: t('all') },
                  ]}
                />
                <Field
                  name="is_owner"
                  component={SelectField}
                  label="Собственник"
                  options={[
                    { id: 'yes', title: t('yes') },
                    { id: 'no', title: t('no') },
                    { id: 'all', title: t('all') },
                  ]}
                />

                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
