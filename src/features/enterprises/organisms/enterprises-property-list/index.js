import { useTranslation } from 'react-i18next';
import { useList } from 'effector-react';
import { LabeledContent, CustomScrollbar, CloseButton } from '@ui';
import makeStyles from '@mui/styles/makeStyles';
import {
  $ownedProperty,
  deletePropertyClicked,
} from '../../models/enterprises-property.model';

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 0 24px',
    height: '100%',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    position: 'relative',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    display: 'flex',
    flexDirection: 'column',
    gap: 24,
    '&:hover > svg': {
      opacity: 0.4,
    },
    '&:hover > svg:hover': {
      opacity: 1,
    },
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  delete_btn: {
    position: 'absolute',
    opacity: 0,
    top: 6,
    right: 6,
    width: 20,
    height: 20,
  },
});

const EnterprisesPropertyList = () => {
  const { listOuter, listInner } = useStyles();
  const list = useList($ownedProperty, (pass) => <PropertyCard {...pass} />);
  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
    </div>
  );
};

const PropertyCard = (props) => {
  const { t } = useTranslation();
  const { title, type, building } = props;
  const { card, text, delete_btn } = useStyles();

  return (
    <div className={card}>
      <CloseButton className={delete_btn} onClick={() => deletePropertyClicked(props)} />
      <LabeledContent label={t('Name')}>
        <span className={text}>{title}</span>
      </LabeledContent>
      <LabeledContent label={t('Label.typeProperty')}>
        <span className={text}>{type}</span>
      </LabeledContent>
      <LabeledContent label={t('building')}>
        <span className={text}>{building}</span>
      </LabeledContent>
    </div>
  );
};

export { EnterprisesPropertyList };
