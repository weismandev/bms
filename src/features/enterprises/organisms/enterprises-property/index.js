import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Loader, ErrorMsg as ErrorMessage, DeleteConfirmDialog } from '@ui';
import { Button } from '@mui/material';
import {
  $mode,
  changeMode,
  EnterprisePropertyGate,
  $isLoading,
  $error,
  deletePropertyConfirmed,
  deleteProrpertyDialogVisibility,
  $isDeletePropertyDialogOpen,
  $propertyWillBeDeleted,
} from '../../models/enterprises-property.model';
import { EnterprisesPropertyForm } from '../enterprises-property-form';
import { EnterprisesPropertyList } from '../enterprises-property-list';

const EnterprisesProperty = (props) => {
  const { enterprise_id } = props;
  const { t } = useTranslation();
  const mode = useStore($mode);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isDeleteDialogOpen = useStore($isDeletePropertyDialogOpen);
  const { title } = useStore($propertyWillBeDeleted);

  const form = mode === 'edit' ? <EnterprisesPropertyForm /> : null;

  const addNewButton =
    mode === 'view' ? (
      <div style={{ padding: 24, textAlign: 'center' }}>
        <Button onClick={() => changeMode('edit')} color="primary">
          {t('AddProperty')}
        </Button>
      </div>
    ) : null;

  const list =
    mode === 'view' ? (
      <div style={{ position: 'relative', height: 'calc(100% - 110px)' }}>
        <EnterprisesPropertyList />
      </div>
    ) : null;

  return (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteProrpertyDialogVisibility(false)}
        confirm={() => deletePropertyConfirmed()}
        content={
          <span>
            {t('AreYouSureYouWantDeleteRoom')} <b>{title}</b> {t('fromProperty')}{' '}
            {t('ThisActionCannotUndone')}
          </span>
        }
        header="Удаление объекта собственности"
      />
      <EnterprisePropertyGate enterprise_id={enterprise_id} />
      {addNewButton}
      {form}
      {list}
    </>
  );
};

export { EnterprisesProperty };
