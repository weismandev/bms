import { SelectControl } from '../../../../ui';
import { companiesApi } from '../../api';

const AsyncEnterprisesEmployees = (props) => {
  return (
    <SelectControl
      kind="async"
      cacheOptions
      loadOptions={(inputValue) =>
        companiesApi.getEmployeesList({ search: inputValue }).then((result) => {
          if (!Array.isArray(result.tenants)) {
            return [];
          }

          return result.tenants.map(({ user }) => ({
            id: user.id,
            title: user.fullname,
            phone: user.phone,
            email: user.email,
          }));
        })
      }
      {...props}
    />
  );
};

export { AsyncEnterprisesEmployees };
