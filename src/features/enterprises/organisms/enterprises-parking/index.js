import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Button } from '@mui/material';
import { Loader, ErrorMsg as ErrorMessage, DeleteConfirmDialog } from '../../../../ui';
import {
  $mode,
  changeMode,
  ParkingSlotsGate,
  $isLoading,
  $error,
  $deleteData,
  confirm,
  changeDeleteData,
} from '../../models/parking-slots.model';
import { EnterprisesExtendForm } from '../enterprises-extend-form';
import { EnterprisesParkingForm } from '../enterprises-parking-form';
import { EnterprisesParkingList } from '../enterprises-parking-list';

const $stores = combine({
  mode: $mode,
  isLoading: $isLoading,
  error: $error,
  deleteData: $deleteData,
});

const EnterprisesParking = (props) => {
  const { enterprise_id } = props;
  const { t } = useTranslation();
  const { mode, isLoading, error, deleteData } = useStore($stores);

  const addParkingSlotForm = mode === 'edit' ? <EnterprisesParkingForm /> : null;
  const extendParkingSlotForm = mode === 'extend' ? <EnterprisesExtendForm /> : null;

  const addNewButton =
    mode === 'view' ? (
      <div style={{ padding: 24, textAlign: 'center' }}>
        <Button onClick={() => changeMode('edit')} color="primary">
          {t('AddParkingSpace')}
        </Button>
      </div>
    ) : null;

  const list =
    mode === 'view' ? (
      <div style={{ position: 'relative', height: 'calc(100% - 110px)' }}>
        <EnterprisesParkingList />
      </div>
    ) : null;

  return (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <DeleteConfirmDialog
        isOpen={deleteData.isOpen}
        confirm={confirm}
        header={t('RemovingParkingSpace')}
        close={() => changeDeleteData({ isOpen: false })}
      />
      <ParkingSlotsGate enterprise_id={enterprise_id} />
      {addNewButton}
      {addParkingSlotForm}
      {extendParkingSlotForm}
      {list}
    </>
  );
};

export { EnterprisesParking };
