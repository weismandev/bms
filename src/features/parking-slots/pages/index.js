import { useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  Detail as PeopleDetail,
  $isDetailOpen as $isPeopleDetailOpen,
} from '../../people';
import '../models';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
} from '../models/page.model';
import { Table, Filter } from '../organisms';

const ParkingPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isPeopleDetailOpen = useStore($isPeopleDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen ? <Filter /> : null}
        detail={isPeopleDetailOpen ? <PeopleDetail /> : null}
      />
    </>
  );
};

const RestrictedParkingPage = (props) => (
  <HaveSectionAccess>
    <ParkingPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedParkingPage as ParkingPage };
