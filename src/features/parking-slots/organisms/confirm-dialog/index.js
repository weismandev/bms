import { DeleteConfirmDialog, CustomScrollbar } from '@ui';
import { isAfter, parse } from 'date-fns';
import i18n from '@shared/config/i18n';
import { fxDeleteSlots } from '../../models/detail-table-form.model';

const { t } = i18n;

const getActualArray = (array, type, number) => {
  const res = !array ? [] : Array.isArray(array) ? array : [array];

  const now = new Date();
  return res
    .filter((item) =>
      item.finish_date
        ? isAfter(parse(item.finish_date, 'yyyy-MM-dd HH:mm:ss', now), now)
        : item
    )
    .map(({ userdata: { fullname }, start_date, finish_date }) => ({
      number,
      fullname,
      start_date,
      finish_date,
      type,
    }));
};

const reduceAndMapSlots = (slots) =>
  slots.reduce((prev, { number, users }) => {
    const ownership = getActualArray(users.ownership, 'ownership', number);
    const rent = getActualArray(users.rent, 'rent', number);

    if (!ownership.length && !rent.length) {
      return [...prev, { number, type: 'notBinded' }];
    }
    return [...prev, ...ownership, ...rent];
  }, []);

const getSlotText = (slot) => {
  const { number, type, fullname, start_date, finish_date } = slot;
  const isDates = start_date && finish_date;
  const isRent = type === 'rent';
  if (type === 'notBinded') {
    return `${t('parkingSpace')} ${number} ${t('WillBeDeleted')}.`;
  }
  return `${t('parkingSpace')} ${number} ${t('Located').toLowerCase()} ${
    isRent ? t('onLease') : t('owned')
  } у ${fullname}${isDates ? ` с ${start_date} до ${finish_date}` : ''}. ${t(
    'AfterDelete'
  )} ${isRent ? t('RentalWillBeTerminated') : t('OwnershipWillBeTerminated')}.`;
};

function ConfirmDialog(props) {
  const { isOpen, onClose, slot_ids } = props;

  let { slots } = props;
  slots = reduceAndMapSlots(slots);
  const isMultipleSlots = slots.length - 1;

  const confirm = () => {
    fxDeleteSlots({ slot_ids });
    onClose();
  };
  return (
    <DeleteConfirmDialog
      isOpen={isOpen}
      confirm={confirm}
      onClose={onClose}
      header={isMultipleSlots ? t('RemoveParkingSpaces') : t('RemoveParkingSpace')}
      close={onClose}
      content={
        <CustomScrollbar style={{ minHeight: 400 }}>
          {slots.map((slot, i) => (
            <div style={{ marginTop: 10 }} key={i}>
              {getSlotText(slot)}
            </div>
          ))}
        </CustomScrollbar>
      }
    />
  );
}

export { ConfirmDialog };
