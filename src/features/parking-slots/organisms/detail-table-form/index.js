import { useState } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import { nanoid } from 'nanoid';
import {
  Grid,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Checkbox,
  Button,
  Pagination,
} from '@mui/material';
import {
  Done,
  Close,
  Add,
  Delete,
  DriveEtaOutlined,
  LocalActivityOutlined,
} from '@mui/icons-material';
import { PaginationItem } from '@mui/lab';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  SearchInput,
  ActionIconButton,
  InputField,
  SwitchField,
  CustomScrollbar,
  Loader,
  ErrorMsg as ErrorMessage,
  DownloadTemplateButton,
  ImportButton,
  FileControl,
} from '@ui';
import { featureFlags } from '@configs/feature-flags';
import { $parkingLotsList } from '../../../parking-lots/models/list.model';
import { ParkingZoneSelect, ParkingZoneSelectField } from '../../../parking-zone-select';
import {
  $mode,
  changeMode,
  $search,
  searchChanged,
  $zoneFilter,
  filterByZone,
  DetailTableFormGate,
  $selectedSlots,
  slotSelected,
  $zoneToMove,
  zoneToMoveChanged,
  moveSubmitted,
  moveCanceled,
  detailSubmitted,
  $slots,
  $tableParams,
  changeSlotsPage,
  $isLoading,
  $error,
  downloadTemplate,
  fileUploaded,
} from '../../models/detail-table-form.model';
import { ConfirmDialog } from '../confirm-dialog';

const isCRUDAllowed = featureFlags.oldParkingsCRUD;

const useStyles = makeStyles({
  headCell: {
    lineHeight: '16px',
    fontSize: 14,
    color: '#9494A3',
    fontWeight: 500,
    whiteSpace: 'nowrap',
    zIndex: 2,
  },

  cellBodyRoot: {
    padding: 5,
  },

  cellHeadRoot: {
    padding: '20px 5px',
  },
  numberCellHeadRoot: {
    padding: '20px 15px',
  },
  toolbar: {
    height: 58,
    padding: '24px 24px 0 24px',
  },
  detailToolbar: {
    height: 65,
    padding: '24px 24px 20px 24px',
  },
  gap: {
    marginRight: 10,
  },
  checkedIconRentRoot: {
    width: 34,
    height: 34,
    background: '#54BD43',
    padding: 5,
    borderRadius: '50%',
    fill: '#fff',
    filter: 'drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.25))',
  },
  uncheckedIconRoot: {
    width: 34,
    height: 34,
    background: '#fff',
    padding: 5,
    borderRadius: '50%',
    fill: '#aaa',
    filter: 'drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.25))',
  },
  checkedIconSubRoot: {
    width: 34,
    height: 34,
    background: '#FC9E0F',
    padding: 5,
    borderRadius: '50%',
    fill: '#fff',
    filter: 'drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.25))',
  },
});

const { t } = i18n;

export const DetailTableForm = (props) => {
  const { tabs, closeCard, lot_id } = props;

  const {
    headCell,
    toolbar,
    cellBodyRoot,
    cellHeadRoot,
    numberCellHeadRoot,
    checkedIconRentRoot,
    checkedIconSubRoot,
    uncheckedIconRoot,
  } = useStyles();

  const mode = useStore($mode);
  const isEditMode = mode === 'edit';
  const isModeEditWithCRUDAllowed = isCRUDAllowed && mode === 'view';

  const selectedSlots = useStore($selectedSlots);

  const headers = (
    <TableRow>
      {isEditMode && <TableCell />}
      <TableCell classes={{ root: numberCellHeadRoot, head: headCell }}>
        {t('Number')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('AreaInM')}
        <sup>2</sup>
      </TableCell>
      <TableCell
        classes={{ root: cellHeadRoot, head: headCell }}
        style={{ paddingRight: '100px' }}
      >
        {t('ParkingArea')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('rental')}
      </TableCell>
      <TableCell classes={{ root: cellHeadRoot, head: headCell }}>
        {t('Subscription')}
      </TableCell>
    </TableRow>
  );

  const slots = useStore($slots);
  const tableParams = useStore($tableParams);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const parkingLotsList = useStore($parkingLotsList);
  const initialNumber = parkingLotsList.length
    ? parkingLotsList.reduce((prev, cur) => prev + cur.slots_count, 0) + 1
    : 1;
  const [number, setNumber] = useState(initialNumber);

  return (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />

      <Formik
        enableReinitialize
        onSubmit={detailSubmitted}
        initialValues={{ slots, lot_id }}
        render={({ values, resetForm }) => {
          const slotsArray = (
            <FieldArray
              name="slots"
              render={({ push }) => {
                const addButton = (
                  <TableRow>
                    <TableCell colSpan={6}>
                      <Button
                        onClick={() => {
                          push({
                            _id: nanoid(4),
                            number,
                            area: '',
                            zone: null,
                            is_rent_available: false,
                            is_private_rent_available: false,
                          });
                          setNumber(number + 1);
                        }}
                        color="primary"
                      >
                        <Add />
                        {t('AddAParkingSpace')}
                      </Button>
                    </TableCell>
                  </TableRow>
                );

                const slotRows = values.slots.map((slot, idx) => {
                  const id = slot._id ? slot._id : slot.id;

                  const isPrivateRentCheckboxDisabled =
                    mode === 'view' || slot.is_taken || !slot.zone || !slot.zone.price;

                  return (
                    <TableRow key={id}>
                      {isEditMode && (
                        <TableCell classes={{ root: cellBodyRoot }} padding="none">
                          <Checkbox
                            size="small"
                            checked={selectedSlots.includes(String(id))}
                            value={id}
                            onChange={(e) => slotSelected(e.target.value)}
                          />
                        </TableCell>
                      )}
                      <TableCell classes={{ root: cellBodyRoot }}>
                        <Field
                          name={`slots[${idx}].number`}
                          component={InputField}
                          mode={mode}
                          label={null}
                          divider={false}
                          placeholder={t('noData')}
                        />
                      </TableCell>
                      <TableCell classes={{ root: cellBodyRoot }}>
                        <Field
                          type="number"
                          name={`slots[${idx}].area`}
                          component={InputField}
                          mode={mode}
                          label={null}
                          divider={false}
                          placeholder={t('noData')}
                        />
                      </TableCell>
                      <TableCell classes={{ root: cellBodyRoot }}>
                        <Field
                          name={`slots[${idx}].zone`}
                          lot_id={lot_id}
                          component={ParkingZoneSelectField}
                          mode={mode}
                          label={null}
                          divider={false}
                          placeholder={t('noData')}
                        />
                      </TableCell>
                      <TableCell classes={{ root: cellBodyRoot }}>
                        <Field
                          name={`slots[${idx}].is_rent_available`}
                          disabled={mode === 'view'}
                          component={SwitchField}
                          mode={mode}
                          label={null}
                          divider={false}
                          disableRipple
                          showTooltip
                          tooltipTitle={
                            slot.is_rent_available
                              ? t('AvailableForRent')
                              : t('NotAvailableForRent')
                          }
                          customCheckedIcon={
                            <DriveEtaOutlined
                              classes={{
                                root: checkedIconRentRoot,
                              }}
                            />
                          }
                          customUncheckedIcon={
                            <DriveEtaOutlined classes={{ root: uncheckedIconRoot }} />
                          }
                        />
                      </TableCell>
                      <TableCell classes={{ root: cellBodyRoot }}>
                        <Field
                          name={`slots[${idx}].is_private_rent_available`}
                          disabled={isPrivateRentCheckboxDisabled}
                          component={SwitchField}
                          mode={mode}
                          label={null}
                          divider={false}
                          disableRipple
                          showTooltip
                          tooltipTitle={
                            slot.is_private_rent_available
                              ? t('AvailableForSubscription')
                              : t('NotAvailableForSubscriptionPurchase')
                          }
                          customCheckedIcon={
                            <LocalActivityOutlined
                              classes={{
                                root: checkedIconSubRoot,
                              }}
                            />
                          }
                          customUncheckedIcon={
                            <LocalActivityOutlined
                              classes={{ root: uncheckedIconRoot }}
                            />
                          }
                        />
                      </TableCell>
                    </TableRow>
                  );
                });
                return (
                  <>
                    {slotRows}
                    {mode === 'edit' && addButton}
                  </>
                );
              }}
            />
          );

          return (
            <Form style={{ height: '100%' }}>
              <DetailTableFormGate lot_id={lot_id} />
              <DetailToolbar
                hidden={{ delete: true, edit: !isCRUDAllowed }}
                className={toolbar}
                onClose={closeCard}
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  changeMode('view');
                  resetForm();
                }}
                mode={mode}
              >
                <DownloadTemplateButton
                  onClick={downloadTemplate}
                  disabled={!isModeEditWithCRUDAllowed}
                  style={{
                    order: 12,
                    margin: '0 5px',
                  }}
                />
                <div
                  style={{
                    order: 14,
                    margin: '0 5px',
                  }}
                >
                  <FileControl
                    mode="edit"
                    name="file"
                    encoding="file"
                    inputProps={{
                      disabled: !isCRUDAllowed
                    }}
                    onChange={(value) => {
                      if (value && value.file) {
                        fileUploaded(value.file);
                      }
                    }}
                    renderPreview={() => null}
                    renderButton={() => (
                      <ImportButton
                        component="span"
                        disabled={!isModeEditWithCRUDAllowed}
                      />
                    )}
                  />
                </div>
              </DetailToolbar>
              {tabs && <div style={{ padding: '0 24px' }}>{tabs}</div>}

              <SelectionToolbar
                lot_id={lot_id}
                isEditMode={isEditMode}
                selectedSlots={selectedSlots}
                slots={slots}
              />

              <div
                style={{
                  padding: '0 12px 24px 24px',
                  height: 'calc(100% - 165px - 36px - 35px)',
                }}
              >
                <CustomScrollbar style={{ height: '100%' }} autoHide>
                  <TableContainer style={{ overflowX: 'initial' }}>
                    <Table>
                      <TableHead>{headers}</TableHead>
                      <TableBody>{slotsArray}</TableBody>
                    </Table>
                  </TableContainer>
                </CustomScrollbar>
                <Pagination
                  disabled={mode === 'edit'}
                  style={{
                    padding: 24,
                    display: 'flex',
                    justifyContent: 'center',
                    flexWrap: 'nowrap',
                  }}
                  onChange={(_, page) => changeSlotsPage(page)}
                  page={tableParams.current_page}
                  count={tableParams.last_page}
                  size="small"
                  shape="rounded"
                  renderItem={(item) => (
                    <PaginationItem
                      {...item}
                      style={{
                        width: 20,
                        height: 36,
                        fontWeight: 600,
                      }}
                    />
                  )}
                />
              </div>
            </Form>
          );
        }}
      />
    </>
  );
};

function SelectionToolbar(props) {
  const { lot_id, selectedSlots, slots } = props;
  const selectedCount = selectedSlots.length;
  const hasSelectedSlots = selectedCount > 0;

  const { detailToolbar, gap } = useStyles();

  const search = useStore($search);
  const zoneFilter = useStore($zoneFilter);
  const zoneToMove = useStore($zoneToMove);

  const [isConfirmOpen, setConfirm] = useState(false);
  const openConfirm = () => setConfirm(true);
  const closeConfirm = () => setConfirm(false);

  const filters = (
    <>
      <Grid item xs={6} className={gap}>
        <SearchInput
          value={search}
          onChange={(e) => searchChanged(e.target.value)}
          handleClear={() => searchChanged('')}
        />
      </Grid>
      <Grid item xs={6}>
        <ParkingZoneSelect
          placeholder={t('Zone')}
          label={null}
          divider={false}
          lot_id={lot_id}
          value={zoneFilter}
          onChange={filterByZone}
        />
      </Grid>
    </>
  );
  const filteredSlots = slots.filter((item) =>
    selectedSlots.some((i) => Number(i) === item.id)
  );

  const movingForm = (
    <>
      <Grid item xs={7} className={gap}>
        <ParkingZoneSelect
          placeholder={t('MoveToZone')}
          label={null}
          divider={false}
          lot_id={lot_id}
          value={zoneToMove}
          onChange={zoneToMoveChanged}
        />
      </Grid>
      <Grid item xs={5} style={{ textAlign: 'end' }}>
        <ActionIconButton kind="negative" className={gap} onClick={openConfirm}>
          <Delete />
        </ActionIconButton>
        <ActionIconButton onClick={moveSubmitted} className={gap}>
          <Done />
        </ActionIconButton>
        <ActionIconButton onClick={moveCanceled} style={{ background: '#828282' }}>
          <Close />
        </ActionIconButton>
      </Grid>
      <ConfirmDialog
        isOpen={isConfirmOpen}
        onClose={closeConfirm}
        slot_ids={selectedSlots}
        slots={filteredSlots}
      />
    </>
  );

  return (
    <Grid container alignItems="center" className={detailToolbar} wrap="nowrap">
      {hasSelectedSlots ? movingForm : filters}
    </Grid>
  );
}
