import { useCallback, useMemo, memo } from 'react';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';
import { PermIdentity } from '@mui/icons-material';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, DataTypeProvider, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { TableToolbar } from '..';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  ActionButton,
  Popper,
  useHover,
  LabeledContent,
  TableColumnVisibility,
  PagingPanel,
  VehiclePlate,
  Popover,
  IconButton,
} from '../../../../ui';
import { open as openPersonCard, $opened as $openedPerson } from '../../../people';
import { $opened, open } from '../../models/detail.model';
import { $rowCount } from '../../models/page.model';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  columns,
} from '../../models/table.model';

const { t } = i18n;

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const VehicleNumber = ({ number }) => (
  <span
    style={{
      background: '#fff',
      borderRadius: 2,
      border: '1px solid #E7E7EC',
      height: 24,
      fontSize: 13,
      lineHeight: '15px',
      marginRight: 10,
      padding: 4,
      fontWeight: 500,
      color: '#65657B',
    }}
  >
    {number}
  </span>
);

const OwnerFormatter = ({ value, row }) => {
  const { anchorEl, hover, handleMouseIn, handleMouseOut } = useHover('td');

  const openedPerson = useStore($openedPerson);

  return value ? (
    <span
      onMouseOver={handleMouseIn}
      onMouseOut={handleMouseOut}
      style={{
        color:
          hover || String(openedPerson.id) === String(row.owner && row.owner.id)
            ? '#0394E3'
            : 'inherit',
      }}
    >
      {value.name}
      <Popper open={hover} header={value.name} anchorEl={anchorEl}>
        <LabeledContent label={t('navMenu.enterprises-parking')}>
          <Chip
            style={{ marginRight: 10, color: '#767676', fontWeight: 500 }}
            label={row.number}
          />
          <Chip
            label={t('owner')}
            style={{ background: '#EF952B', color: '#fff', fontWeight: 500 }}
          />
        </LabeledContent>

        {row.owner &&
        Array.isArray(row.owner.vehicles) &&
        row.owner.vehicles.length > 0 ? (
          <LabeledContent label={t('TiedVehicles')} style={{ marginTop: 20 }}>
            {row.owner.vehicles.map((i) => (
              <VehicleNumber number={i.licence_plate} key={i.id} />
            ))}
          </LabeledContent>
        ) : null}

        <ActionButton
          onClick={() => {
            openPersonCard(row.owner.id);
          }}
          style={{ marginTop: 20, padding: '0 20px 0 10px' }}
          kind="positive"
        >
          <PermIdentity /> {t('OpenCard')}
        </ActionButton>
      </Popper>
    </span>
  ) : (
    t('isNotDefined')
  );
};

const OwnerTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={OwnerFormatter} {...props} />
);

const UsersFormatter = ({ value, row }) =>
  value ? <UserChip parkingNumber={row.number} guest={value} /> : t('isNotDefined');

function UserChip(props) {
  const { guest, parkingNumber } = props;
  const { name, vehicle, date_from, date_to } = guest;

  const { anchorEl, hover, handleMouseIn, handleMouseOut } = useHover('td');

  return (
    <Chip
      style={{
        background: hover ? '#0394E3' : '#e0e0e0',
        color: hover ? '#fff' : 'rgba(0,0,0,.87)',
        marginRight: 10,
        cursor: 'pointer',
      }}
      onMouseOut={handleMouseOut}
      onMouseOver={handleMouseIn}
      label={
        <span>
          {name}
          <Popper open={hover} header={name} anchorEl={anchorEl}>
            <LabeledContent label={t('parkingSpace')}>
              <Chip
                style={{
                  marginRight: 10,
                  color: '#767676',
                  fontWeight: 500,
                }}
                label={parkingNumber}
              />
              <Chip
                label={t('Guest')}
                style={{
                  background: '#0394E3',
                  color: '#fff',
                  fontWeight: 500,
                }}
              />
            </LabeledContent>

            <LabeledContent
              style={{
                marginTop: 20,
                fontSize: 13,
                lineHeight: '15px',
                fontWeight: 500,
                color: '#65657B',
              }}
              label={t('RanksFrom')}
            >
              {date_from}
            </LabeledContent>

            <LabeledContent
              style={{
                marginTop: 20,
                fontSize: 13,
                lineHeight: '15px',
                fontWeight: 500,
                color: '#65657B',
              }}
              label={t('TakesUpTo')}
            >
              {date_to}
            </LabeledContent>

            {vehicle ? (
              <LabeledContent label={t('Vehicle')} style={{ marginTop: 20 }}>
                <VehicleNumber number={vehicle.licence_plate} />
              </LabeledContent>
            ) : null}
          </Popper>
        </span>
      }
    />
  );
}

const UsersTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={UsersFormatter} {...props} />
);

const VehiclesFormatter = ({ value, row }) => {
  const vehicles = row.allowed_vehicles;

  if (!vehicles || !Array.isArray(vehicles) || !vehicles.length > 0) return '';

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 'auto auto',
        justifyContent: 'start',
      }}
    >
      <VehiclePlate
        key={vehicles[0].id}
        number={vehicles[0].licence_plate}
        brand={vehicles[0].brand}
      />
      {vehicles[1] && (
        <Popover
          trigger={
            <IconButton size="large">
              <span>+{vehicles.slice(1).length}</span>
            </IconButton>
          }
        >
          <div
            style={{
              display: 'grid',
              gap: 10,
              padding: 10,
              background: '#fcfcfc',
            }}
          >
            {vehicles.slice(1).map((i) => (
              <VehiclePlate key={i.id} number={i.licence_plate} brand={i.brand} />
            ))}
          </div>
        </Popover>
      )}
    </div>
  );

  return <ShortList list={vehiclesElemnts} component={VehiclePlate} />;
};

const VehiclesTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={VehiclesFormatter} {...props} />
);

const StateFormatter = ({ value, row }) => {
  const mapSlugToData = {
    no_information: {
      title: t('NoInformation'),
      background: '#e0e0e0',
      color: 'rgba(0,0,0,.87',
    },
    empty: { title: t('Free'), background: '#1BB169', color: '#fff' },
    taken: { title: t('Busy'), background: '#EB5757', color: '#fff' },
  };

  const title = mapSlugToData[value.slug] && mapSlugToData[value.slug].title;
  const background = mapSlugToData[value.slug] && mapSlugToData[value.slug].background;
  const color = mapSlugToData[value.slug] && mapSlugToData[value.slug].color;

  return (
    <>
      <Chip label={title} style={{ background, color, marginRight: 10 }} />
      {value.slug === 'taken' && value.vehicle ? (
        <VehicleNumber number={value.vehicle.licence_plate} />
      ) : null}
    </>
  );
};

const StateTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StateFormatter} {...props} />
);

const Table = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const rowCount = useStore($rowCount);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <DragDropProvider />

        <CustomPaging totalCount={rowCount} />

        {/* <OwnerTypeProvider for={['owner']} /> */}
        {/* <UsersTypeProvider for={['nearest_guest']} /> */}
        <VehiclesTypeProvider for={['allowed_vehicles']} />
        <StateTypeProvider for={['state']} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
