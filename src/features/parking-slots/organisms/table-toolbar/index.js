import { useStore } from 'effector-react';
import { Toolbar, FoundInfo, FilterButton, Greedy, SearchInput } from '../../../../ui';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';
import { $search, searchChanged } from '../../models/table.model';

const TableToolbar = (props) => {
  const count = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ margin: '0 10px' }} />
      <SearchInput
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
    </Toolbar>
  );
};

export { TableToolbar };
