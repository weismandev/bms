import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'parking/slots/list', payload);
const getSimpleList = (payload) => api.v1('get', 'parking/slots/simple-list', payload);
const moveSlots = (payload) => api.v1('post', 'parking/slots/move-slots', payload);
const deleteSlots = (payload) => api.v1('post', 'parking/slots/delete', payload);

const create = (payload) => api.v1('post', 'parking/slots/create-or-update', payload);
const update = create;

const importFileSlots = (payload) => api.v1('post', 'parking/slots/import', payload);

export const parkingApi = {
  getList,
  create,
  update,
  getSimpleList,
  moveSlots,
  deleteSlots,
  importFileSlots,
};
