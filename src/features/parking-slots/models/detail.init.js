import { sample } from 'effector';
import { combineEvents } from 'patronum/combine-events';
import { signout } from '../../common';
import { fxGetById as fxGetPersonById } from '../../people';
import { $opened, open, detailClosed, newEntity } from './detail.model';
import { $normalized } from './page.model';

const openedAndGotPerson = combineEvents({
  events: {
    openedParkingId: open,
    data: fxGetPersonById.done,
  },
});

$opened
  .on(
    sample(
      $normalized,
      openedAndGotPerson,
      (data, { openedParkingId }) => data[openedParkingId]
    ),
    (state, opened) => opened
  )
  .on(detailClosed, () => newEntity)
  .reset(signout);
