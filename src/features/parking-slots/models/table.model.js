import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $normalized } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'number', title: t('p/m') },
  {
    name: 'owner',
    title: t('owner'),
  },
  {
    name: 'used',
    title: t('user'),
  },
  {
    name: 'allowed_vehicles',
    title: t('ApprovedVehicles'),
  },
  {
    name: 'state',
    title: t('Label.status'),
  },
];

export const $tableData = $normalized.map((data) => formatTableData(Object.values(data)));

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: [
    { columnName: 'used', width: 300 },
    { columnName: 'allowed_vehicles', width: 300 },
  ],
});

function formatTableData(data) {
  return data.map((item) => ({
    area: item.area,
    buildings: item.buildings,
    enterprise: item.enterprise,
    guests: item.guests,
    id: item.id,
    is_rent_available: item.is_rent_available,
    is_rent_available_can_be_changed: item.is_rent_available_can_be_changed,
    nearest_guest: item.nearest_guest,
    number: item.number,
    owner: item.owner?.title || '',
    parking_lot: item.parking_lot,
    renter: item.renter,
    state: item.state,
    used: item.used?.title || '',
    users: item.users,
    zone: item.zone,
    allowed_vehicles: [
      item.used?.vehicles?.map((item) => item),
      item.users?.map((user) => user.vehicles?.map((item) => item)).flat(),
      item.used?.vehicles?.map((item) => item),
    ]
      .flat()
      .filter(Boolean),
  }));
}

// if (nearest_guest) {
//   formattedItem.nearest_guest = {
//     ...nearest_guest,
//     date_from: format(
//       new Date(nearest_guest.date_from),
//       'dd.MM.yyyy HH:mm'
//     ),
//     date_to: format(new Date(nearest_guest.date_to), 'dd.MM.yyyy HH:mm'),
//   };
// }
