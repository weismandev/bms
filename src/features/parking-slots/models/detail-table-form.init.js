import { guard, sample, merge, forward } from 'effector';
import { saveAs } from 'file-saver';
import i18n from '@shared/config/i18n';
import { signout } from '../../common';
import { parkingApi } from '../api';
import {
  pageUnmounted,
  pageState,
  detailSubmitted,
  fxUpdateSlots,
  $mode,
  $search,
  $zoneFilter,
  fxGetSlots,
  slotSelected,
  $selectedSlots,
  changeMode,
  $zoneToMove,
  moveCanceled,
  moveSubmitted,
  $movingForm,
  fxMoveSlots,
  fxDeleteSlots,
  $tableParams,
  $currentPage,
  $isLoading,
  $error,
  downloadTemplate,
  fileUploaded,
  fxImportFileSlots,
} from './detail-table-form.model';

const { t } = i18n;

fxGetSlots.use(parkingApi.getSimpleList);
fxMoveSlots.use(parkingApi.moveSlots);
fxDeleteSlots.use(parkingApi.deleteSlots);
fxUpdateSlots.use(parkingApi.update);
fxImportFileSlots.use(parkingApi.importFileSlots);

sample({
  source: [
    fxGetSlots.pending,
    fxMoveSlots.pending,
    fxUpdateSlots.pending,
    fxDeleteSlots.pending,
    fxImportFileSlots.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

$isLoading.reset(signout);

$error
  .on(
    [
      fxGetSlots.fail,
      fxMoveSlots.fail,
      fxUpdateSlots.fail,
      fxDeleteSlots.fail,
      fxImportFileSlots.fail,
    ],
    (_, { error }) => error
  )
  .reset([pageUnmounted, guard($isLoading, { filter: Boolean }), signout]);

$selectedSlots.on(slotSelected, (state, id) =>
  state.includes(String(id))
    ? state.filter((i) => String(i) !== String(id))
    : state.concat(String(id))
);

$mode.reset([pageUnmounted, signout]).on(fxUpdateSlots.done, () => 'view');
$search.reset([pageUnmounted, signout]);
$zoneFilter.reset([pageUnmounted, signout]);
$tableParams.reset([pageUnmounted, signout]);
$currentPage.reset([$search, $zoneFilter, pageUnmounted, signout]);

$selectedSlots.reset([
  pageUnmounted,
  changeMode,
  moveCanceled,
  fxMoveSlots.done,
  fxDeleteSlots.done,
  signout,
]);

$zoneToMove.reset([pageUnmounted, changeMode, moveCanceled, fxMoveSlots.done, signout]);

const $slotsQueryParams = sample({
  source: {
    innerData: pageState,
    search: $search,
    zone: $zoneFilter,
    currentPage: $currentPage,
  },
  fn: ({ innerData: { lot_id }, search, zone, currentPage }) => {
    const payload = { lot_id };

    if (search) {
      payload.search = search;
    }

    if (zone && zone.id) {
      payload.zone_id = zone.id;
    }

    if (currentPage) {
      payload.page = currentPage;
    }

    return payload;
  },
});

const slotsNeedUpdate = merge([
  $slotsQueryParams.updates,
  fxMoveSlots.done,
  fxDeleteSlots.done,
  fxUpdateSlots.done,
  fxImportFileSlots.done,
]);

guard({
  source: sample($slotsQueryParams, slotsNeedUpdate),
  filter: ({ lot_id }) => Boolean(lot_id),
  target: fxGetSlots,
});

guard({
  source: sample($movingForm, moveSubmitted),
  filter: ({ zone_id }) => Boolean(zone_id),
  target: fxMoveSlots,
});

forward({
  from: detailSubmitted,
  to: fxUpdateSlots.prepend(prepareSlots),
});

sample({
  source: $slotsQueryParams,
  clock: fileUploaded,
  fn: ({ lot_id }, file) => ({ lot_id, import: file }),
  target: fxImportFileSlots,
});

downloadTemplate.watch(() =>
  saveAs('https://files.mysmartflat.ru/crm/templates/parking_slots_import_template.xlsx')
);

function prepareSlots({ lot_id, slots = [] }) {
  return {
    lot_id,
    slots: slots.map(
      ({
        _id,
        id,
        zone,
        number,
        area,
        is_rent_available,
        is_private_rent_available,
        ...rest
      }) => {
        const payload = {
          ...rest,
          zone_id: zone && zone.id,
          is_rent_available: Number(is_rent_available),
          is_private_rent_available: Number(is_private_rent_available),
          number: number || t('NewSlot'),
          area: area || 0,
        };

        if (id) payload.id = id;

        return payload;
      }
    ),
  };
}
