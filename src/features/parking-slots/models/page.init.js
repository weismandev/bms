import { sample } from 'effector';
import { fxGetById as fxGetGuestById } from '../../guests';
import { fxGetById as fxGetPersonById } from '../../people';
import { $filters } from './filter.model';
import {
  pageMounted,
  fxGetList,
  $isLoading,
  $error,
  $isErrorDialogOpen,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading.on(
  [fxGetPersonById.pending, fxGetGuestById.pending],
  (state, isLoading) => isLoading
);

$error.on([fxGetGuestById.failData, fxGetPersonById.failData], (state, error) => error);

$isErrorDialogOpen.on([fxGetGuestById.fail, fxGetPersonById.fail], () => true);

sample({
  clock: [pageMounted, $filters, $tableParams, $settingsInitialized],
  source: {
    filters: $filters,
    tableParams: $tableParams,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ filters, tableParams }) => ({
    per_page: tableParams.per_page,
    page: tableParams.page,
    search: tableParams.search,
    states:
      Array.isArray(filters.states) && filters.states.length > 0
        ? filters.states.map((i) => i.id)
        : [],
    buildings:
      Array.isArray(filters.buildings) && filters.buildings.length > 0
        ? filters.buildings.map((i) => i.id)
        : [],
  }),
  target: fxGetList,
});
