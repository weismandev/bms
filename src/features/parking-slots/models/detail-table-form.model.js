import { createStore, createEvent, createEffect, restore, combine } from 'effector';
import { createGate } from 'effector-react';

export const DetailTableFormGate = createGate();
export const {
  open: pageMounted,
  close: pageUnmounted,
  state: pageState,
} = DetailTableFormGate;

export const newEntity = {};

export const changeMode = createEvent();
export const searchChanged = createEvent();
export const filterByZone = createEvent();
export const slotSelected = createEvent();
export const zoneToMoveChanged = createEvent();
export const moveSubmitted = createEvent();
export const moveCanceled = createEvent();
export const detailSubmitted = createEvent();
export const changeSlotsPage = createEvent();
export const downloadTemplate = createEvent();
export const fileUploaded = createEvent();

export const fxGetSlots = createEffect();
export const fxMoveSlots = createEffect();
export const fxDeleteSlots = createEffect();
export const fxUpdateSlots = createEffect();
export const fxImportFileSlots = createEffect();

export const $mode = restore(changeMode, 'view');
export const $search = restore(searchChanged, '');
export const $zoneFilter = restore(filterByZone, '');
export const $zoneToMove = restore(zoneToMoveChanged, '');
export const $currentPage = restore(changeSlotsPage, 1);

export const $isLoading = createStore(false);
export const $error = createStore(null);
export const $isErrorDialogOpened = createStore(false);

export const $slots = restore(
  fxGetSlots.doneData.map(({ slots }) =>
    slots.map((slot) => ({ ...slot, area: slot.area || '' }))
  ),
  []
);

export const $tableParams = restore(
  fxGetSlots.doneData.map(({ meta }) => ({
    current_page: meta.current_page,
    per_page: meta.per_page,
    last_page: meta.last_page,
  })),
  { current_page: 1, per_page: 10, last_page: 1 }
);

export const $selectedSlots = createStore([]);

export const $movingForm = combine(
  { slot_ids: $selectedSlots, zone: $zoneToMove },
  ({ slot_ids, zone }) => ({ slot_ids, zone_id: zone.id })
);
