import { createStore, createEvent, guard } from 'effector';
import { $opened as $openedGuest } from '../../guests';
import { $opened as $openedPerson } from '../../people';

export const newEntity = {};

export const open = createEvent();

export const $opened = createStore(newEntity);

export const detailClosed = guard({
  source: [$openedPerson, $openedGuest],
  filter: ([person, guest]) => Boolean(!person.id && !guest.id),
});
