import { $filters } from './filter.model';
import { searchChanged, $currentPage } from './table.model';

$currentPage.reset([searchChanged, $filters]);
