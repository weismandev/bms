import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'enterprises/get-list-with-employees');

export const enterprisesEmployeeApi = { getList };
