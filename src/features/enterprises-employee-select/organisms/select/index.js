import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesEmployeeSelect = (props) => {
  const { company_id, excludeIds = [] } = props;
  const { t } = useTranslation();

  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const preparedEmployees = () => {
    const formatEmployees = ({ id, fullname }) => ({
      id,
      fullname: fullname ? fullname : t('isNotDefinedit'),
    });

    if (data.length && company_id) {
      const company = data.find((comp) => comp.id === company_id);

      return company
        ? company.employees.map(({ userdata }) => formatEmployees(userdata))
        : [];
    }
    if (data.length && !company_id) {
      const employees = Object.values(
        data
          .reduce((acc, { employees }) => acc.concat(employees), [])
          .map(({ userdata }) => formatEmployees(userdata))
          .reduce((acc, employee) => ({ ...acc, [employee.id]: employee }), {})
      );

      return employees;
    }
    return [];
  };

  const options = preparedEmployees().filter((item) =>
    excludeIds.every((e) => item.id !== e)
  );
  useEffect(() => {
    fxGetList();
  }, []);

  const getOptionLabel = (option) => option && option.fullname;

  return (
    <SelectControl
      options={options}
      getOptionLabel={getOptionLabel}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const EnterprisesEmployeeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesEmployeeSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesEmployeeSelect, EnterprisesEmployeeSelectField };
