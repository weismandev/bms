import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'scud/client/common/pass/strategies', payload);

export const enterprisesPassTypeApi = { getList };
