import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const EnterprisesPassTypeSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl
      getOptionValue={(option) => (option ? option.slug : '')}
      options={options}
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const EnterprisesPassTypeSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<EnterprisesPassTypeSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { EnterprisesPassTypeSelect, EnterprisesPassTypeSelectField };
