import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { enterprisesPassTypeApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.strategies) {
      return [];
    }

    return result.strategies;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(enterprisesPassTypeApi.getList);

function findById(id) {
  const data = $data.getState();

  return data.find((i) => String(i.id) === String(id));
}

export { fxGetList, $data, $error, $isLoading, findById };
