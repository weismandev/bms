import { createStore, createEvent } from 'effector';

export const changeVisibilityDeleteModal = createEvent<boolean>();

export const $isOpenDeleteModal = createStore<boolean>(false);
