import { sample, guard } from 'effector';
import { Error } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { deleteTemplate, $opened } from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import { $isOpenDeleteModal, changeVisibilityDeleteModal } from './delete-modal.model';

const { t } = i18n;

sample({
  clock: guard({
    clock: changeVisibilityDeleteModal,
    source: $opened,
    filter: ({ complexes, buildings }: { complexes: number[]; buildings: number[] }) =>
      complexes.length > 0 || buildings.length > 0,
  }),
  fn: () => ({
    isOpen: true,
    text: t('attention'),
    color: '#FFA500',
    Icon: Error,
    message: t('TheTemplateIsBoundToAnAddressAndCannotBeDeleted'),
  }),
  target: changeNotification,
});

sample({
  clock: changeVisibilityDeleteModal,
  source: $opened,
  fn: (
    { complexes, buildings }: { complexes: number[]; buildings: number[] },
    visibility
  ) => {
    if (complexes.length === 0 && buildings.length === 0) {
      return visibility;
    }

    return false;
  },
  target: $isOpenDeleteModal,
});

$isOpenDeleteModal
  .on(changeVisibilityDeleteModal, (_, visibility) => visibility)
  .reset([pageUnmounted, signout, deleteTemplate]);
