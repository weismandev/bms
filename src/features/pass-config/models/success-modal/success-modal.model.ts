import { createStore, createEvent } from 'effector';

export const $isOpenSuccessModal = createStore<boolean>(false);
export const $isShowSuccessModalAlert = createStore<boolean>(false);

export const closeSuccessModal = createEvent<void>();
