import { signout } from '@features/common';
import { fxCreateTemplate } from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import {
  $isOpenSuccessModal,
  closeSuccessModal,
  $isShowSuccessModalAlert,
} from './success-modal.model';

$isOpenSuccessModal
  .on(fxCreateTemplate.done, () => true)
  .reset([signout, pageUnmounted, closeSuccessModal]);

$isShowSuccessModalAlert
  .on(
    fxCreateTemplate.done,
    (_, { result: { template } }) =>
      template.complexList?.length === 0 && template?.buildingList?.length === 0
  )
  .reset([signout, pageUnmounted, closeSuccessModal]);
