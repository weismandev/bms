import { createEffect, createStore } from 'effector';
import { GuestTypesReponse, GuestType } from '../../interfaces';

export const fxGetList = createEffect<void, GuestTypesReponse, Error>();

export const $data = createStore<GuestType[]>([]);
export const $isLoading = createStore<boolean>(false);
