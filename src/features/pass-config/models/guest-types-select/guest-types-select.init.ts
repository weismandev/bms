import { signout } from '@features/common';
import { passConfigApi } from '../../api';
import { fxGetList, $data, $isLoading } from './guest-types-select.model';

fxGetList.use(passConfigApi.getGuestTypes);

$data
  .on(fxGetList.done, (_, { result }) =>
    Array.isArray(result?.guestTypeList) && result?.guestTypeList?.length
      ? result.guestTypeList
      : []
  )
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
