import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { passConfigApi } from '../../api';
import { GetTemplateListPayload } from '../../interfaces';
import {
  fxGetTemplate,
  fxUpdateTemplate,
  fxGetDefaultOptions,
  fxCreateTemplate,
  fxDeleteTemplate,
  fxValidateObjects,
} from '../detail/detail.model';
import { $tableParams } from '../table/table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  $isLoading,
  $error,
  pageUnmounted,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetTemplateList,
  pageMounted,
  $rawData,
} from './page.model';

fxGetTemplateList.use(passConfigApi.getTemplateList);
fxGetTemplate.use(passConfigApi.getTemplate);
fxUpdateTemplate.use(passConfigApi.updateTemplate);
fxGetDefaultOptions.use(passConfigApi.getDefaultOptions);
fxCreateTemplate.use(passConfigApi.createTemplate);
fxDeleteTemplate.use(passConfigApi.deleteTemplate);
fxValidateObjects.use(passConfigApi.validateObjects);

const errorOccured = merge([
  fxGetTemplateList.fail,
  fxGetTemplate.fail,
  fxUpdateTemplate.fail,
  fxGetDefaultOptions.fail,
  fxCreateTemplate.fail,
  fxDeleteTemplate.fail,
  fxValidateObjects.fail,
]);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading
  .on(
    pending({
      effects: [
        fxGetTemplateList,
        fxGetTemplate,
        fxUpdateTemplate,
        fxGetDefaultOptions,
        fxCreateTemplate,
        fxDeleteTemplate,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

const getColumnNameForSorting = (value: string) => {
  switch (value) {
    case 'title':
      return 'title';
    default:
      return null;
  }
};

sample({
  clock: [
    pageMounted,
    $tableParams,
    fxUpdateTemplate.done,
    fxCreateTemplate.done,
    fxDeleteTemplate.done,
    $settingsInitialized,
  ],
  source: { tableParams: $tableParams, settingsInitialized: $settingsInitialized },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ tableParams: { page, per_page, search, sorting } }) => {
    const payload: GetTemplateListPayload = {
      page,
      per_page,
      search,
    };

    if (sorting.length > 0) {
      const name = getColumnNameForSorting(sorting[0]?.name);

      if (name) {
        payload.sort = name;
        payload.order = sorting[0].order.toUpperCase();
      }
    }

    return payload;
  },
  target: fxGetTemplateList,
});

$rawData
  .on(fxGetTemplateList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);
