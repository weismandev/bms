import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';
import { GetTemplateListPayload, GetTemplateListResponse } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $rawData = createStore<GetTemplateListResponse>({
  templateList: [],
  meta: { total: 0 },
});

export const fxGetTemplateList = createEffect<
  GetTemplateListPayload,
  GetTemplateListResponse,
  Error
>();
