import { createEffect, createEvent, createStore } from 'effector';
import { createDetailBag } from '@tools/factories';
import {
  TemplatePayload,
  GetTemplateResponse,
  CreateUpdateTemplatePayload,
  ValidateObjectPayload,
  Value,
  ValidateObjectResponse,
  ErrorsValidationObject,
} from '../../interfaces';

const newEntity = {
  title: '',
  complexes: [],
  buildings: [],
  strategies: [2],
  guestTypes: [],
  preFillingNames: [],
  options: [],
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const deleteTemplate = createEvent<void>();
export const validateObject = createEvent<{
  templateId?: number;
  complexes: Value[];
  buildings: Value[];
}>();

export const fxGetTemplate = createEffect<TemplatePayload, GetTemplateResponse, Error>();
export const fxUpdateTemplate = createEffect<
  CreateUpdateTemplatePayload,
  GetTemplateResponse,
  Error
>();
export const fxGetDefaultOptions = createEffect<void, GetTemplateResponse, Error>();
export const fxCreateTemplate = createEffect<
  CreateUpdateTemplatePayload,
  GetTemplateResponse,
  Error
>();
export const fxDeleteTemplate = createEffect<TemplatePayload, object, Error>();
export const fxValidateObjects = createEffect<
  ValidateObjectPayload,
  ValidateObjectResponse,
  Error
>();

export const $errorsValidationObjects = createStore<null | ErrorsValidationObject>(null);
