import { sample, forward } from 'effector';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import {
  Value,
  SectionItem,
  PropertyItem,
  FormattedPropertyList,
  ValidateObjectsError,
  ObjectType,
  ErrorValidationObject,
  ValidateObjectPayload,
} from '../../interfaces';
import { pageUnmounted } from '../page/page.model';
import {
  open,
  fxGetTemplate,
  $opened,
  $isDetailOpen,
  entityApi,
  fxUpdateTemplate,
  $mode,
  addClicked,
  fxGetDefaultOptions,
  fxCreateTemplate,
  fxDeleteTemplate,
  deleteTemplate,
  validateObject,
  fxValidateObjects,
  $errorsValidationObjects,
  changedDetailVisibility,
} from './detail.model';

const { t } = i18n;

interface List {
  property: string;
  required: {
    disabled: boolean;
    value: boolean;
  };
  title: string;
  visibilty: {
    disabled: boolean;
    value: boolean;
  };
}

interface Data {
  title: string;
  complexes: Value[];
  buildings: Value[];
  strategies: Value[];
  guestTypes: Value[];
  preFillingNames: Value[];
  options: {
    title: string;
    list: List[];
    subList: {
      list: List[];
    }[];
  }[];
}

sample({
  clock: open,
  fn: (id) => ({ id }),
  target: fxGetTemplate,
});

const formatSection = (array: SectionItem[]): any =>
  array.map((item) => {
    const subList = formatPropertyList(item.sectionList);

    const list =
      item?.propertyList && item?.propertyList?.length > 0
        ? item.propertyList.reduce(
            (acc: FormattedPropertyList[], element: PropertyItem) => {
              if (element.property === 'has_stuff') {
                return acc; // для скрытия тмц. Задача UJIN-7802
              }
              return [
                ...acc,
                {
                  title: element?.property_text || '',
                  property: element?.property || '',
                  visibilty: {
                    disabled: !element?.manage_visible,
                    value: element?.visible || false,
                  },
                  required: {
                    disabled: !element?.manage_required,
                    value: element?.required || false,
                  },
                },
              ];
            },
            []
          )
        : [];

    return {
      title: item?.title || '',
      sectionName: item.name,
      visibiltyValue:
        list.length > 0 ? list.every((item) => item.visibilty.value) : false,
      visibilityDisabled: item.disabled,
      requiredValue: list.length > 0 ? list.every((item) => item.required.value) : false,
      requiredDisabled:
        list.length > 0 ? list.every((item) => item.required.disabled) : false,
      list,
      subList,
    };
  });

const formatPropertyList = (propertySectionList: SectionItem[]) => {
  const x = formatSection(propertySectionList);

  return x.filter((item) => item.sectionName !== 'stuff'); // для скрытия тмц в задаче UJIN-7802
};

$opened
  .on(fxGetTemplate.done, (_, { result: { template } }) => ({
    id: template.id,
    title: template?.title || '',
    complexes:
      template?.complexList && template?.complexList?.length > 0
        ? template.complexList.map((item) => item.id)
        : [],
    buildings:
      template?.buildingList && template?.buildingList?.length > 0
        ? template.buildingList.map((item) => item.id)
        : [],
    strategies:
      template?.strategyList && template?.strategyList?.length > 0
        ? template.strategyList.map((item) => item.id)
        : [],
    guestTypes:
      template?.guestTypeList && template?.guestTypeList?.length > 0
        ? template.guestTypeList.map((item) => item)
        : [],
    preFillingNames:
      template?.predefineGuestTypeList && template?.predefineGuestTypeList?.length > 0
        ? template.predefineGuestTypeList.map((item) => item.id)
        : [],
    options:
      template?.propertySectionList && template?.propertySectionList?.length > 0
        ? formatPropertyList(template.propertySectionList)
        : [],
  }))
  .on(fxGetDefaultOptions.done, (state, { result: { template } }) => ({
    ...state,
    options:
      template?.propertySectionList && template?.propertySectionList?.length > 0
        ? formatPropertyList(template.propertySectionList)
        : [],
  }))
  .reset([signout, pageUnmounted]);

$isDetailOpen
  .on(fxGetTemplate.done, () => true)
  .off(open)
  .reset([signout, pageUnmounted, fxDeleteTemplate.done]);

sample({
  clock: entityApi.update,
  fn: ({ id, ...restData }) => ({ id, ...formatData(restData) }),
  target: fxUpdateTemplate,
});

sample({
  clock: entityApi.create,
  fn: (data) => formatData(data),
  target: fxCreateTemplate,
});

const formatData = (data: Data) => {
  const properties = data.options.map((option) => {
    const list = option.list.map((item) => ({
      property: item.property,
      visible: item.visibilty.value,
      required: item.required.value,
    }));

    const subList = option.subList
      .map((item) =>
        item.list.map((element) => ({
          property: element.property,
          visible: element.visibilty.value,
          required: element.required.value,
        }))
      )
      .flat();

    return [...list, ...subList];
  });

  return {
    title: data.title,
    complex_ids: data.complexes.map((complex) =>
      typeof complex === 'object' ? complex.id : complex
    ),
    building_ids: data.buildings.map((building) =>
      typeof building === 'object' ? building.id : building
    ),
    strategy_ids: data.strategies.map((strategy) =>
      typeof strategy === 'object' ? strategy.id : strategy
    ),
    guest_type_ids: data.guestTypes.map((type) =>
      typeof type === 'object' ? type.id : type
    ),
    predefine_guest_type_ids: data.preFillingNames.map((type) =>
      typeof type === 'object' ? type.id : type
    ),
    properties: properties.length > 0 ? properties.flat() : [],
  };
};

$mode.reset([signout, pageUnmounted, fxUpdateTemplate.done, fxCreateTemplate.done]);

sample({
  clock: fxUpdateTemplate.done,
  fn: ({
    result: {
      template: { id },
    },
  }) => ({ id }),
  target: fxGetTemplate,
});

forward({
  from: addClicked,
  to: fxGetDefaultOptions,
});

sample({
  clock: fxCreateTemplate.done,
  fn: ({
    result: {
      template: { id },
    },
  }) => ({ id }),
  target: fxGetTemplate,
});

sample({
  clock: deleteTemplate,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxDeleteTemplate,
});

/** Валидация объектов */
sample({
  clock: validateObject,
  filter: (data) => data?.complexes?.length > 0 || data?.buildings?.length > 0,
  fn: (data) => {
    const complexes = data.complexes.map((complex) =>
      typeof complex === 'object' ? complex?.id : complex
    );

    const buildings = data.buildings.map((building) =>
      typeof building === 'object' ? building?.id : building
    );

    const payload: ValidateObjectPayload = {
      complex_ids: complexes,
      building_ids: buildings,
    };

    if (data.templateId) {
      payload.template_id = data.templateId;
    }

    return payload;
  },
  target: fxValidateObjects,
});

const getErrorText = (text: string) => {
  switch (text) {
    case 'complex_has_building_with_template':
      return t('BuildingsSelectedObjectAlreadyUsing');
    case 'building_has_template':
      return t('SelectedBuildingsAlreadyUsedTemplates');
    case 'building_belongs_to_complex_with_template':
      return t('SelectedBuildingsAlreadyUsedOtherTemplatesUndeObject');
    default:
      return null;
  }
};

const formatObjectErrors = (array: ValidateObjectsError[], type: ObjectType) =>
  array.reduce((acc: ErrorValidationObject, value) => {
    const errorText = getErrorText(value?.category);

    if (!errorText || value.object_type !== type) {
      return acc;
    }

    if (acc[errorText]) {
      return {
        ...acc,
        ...{ [errorText]: [...acc[errorText], value.object_id] },
      };
    }

    return { ...acc, ...{ [errorText]: [value.object_id] } };
  }, {});

$errorsValidationObjects
  .on(fxValidateObjects.done, (_, { result }) => {
    if (!Array.isArray(result?.warnings)) {
      return null;
    }

    return {
      complexes: formatObjectErrors(result?.warnings, 'complex'),
      buildings: formatObjectErrors(result?.warnings, 'building'),
    };
  })
  .reset([signout, pageUnmounted, changedDetailVisibility]);
