import i18n from '@shared/config/i18n';
import { createTableBag, IColumns, IWidths } from '@tools/factories/table';
import { TemplateItem } from '../../interfaces';
import { $rawData } from '../page/page.model';

const { t } = i18n;

export const columns: IColumns[] = [
  { name: 'title', title: t('TemplateName') },
  { name: 'complexes', title: t('objects') },
  { name: 'buildings', title: t('Buildings') },
  { name: 'guestTypes', title: t('GuestType') },
  { name: 'strategies', title: t('PassesType') },
];

const columnsWidth: IWidths[] = [
  { columnName: 'title', width: 270 },
  { columnName: 'complexes', width: 290 },
  { columnName: 'buildings', width: 320 },
  { columnName: 'guestTypes', width: 250 },
  { columnName: 'strategies', width: 180 },
];

export const excludedColumnsFromSort = [
  'complexes',
  'buildings',
  'guestTypes',
  'strategies',
];

export const {
  columnWidthsChanged,
  $tableParams,
  $columnWidths,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $columnOrder,
  columnOrderChanged,
  $currentPage,
  $pageSize,
  $search,
  $sorting,
  sortChanged,
  searchChanged,
  pageNumChanged,
  pageSizeChanged,
} = createTableBag(columns, {
  widths: columnsWidth,
  pageSize: 25,
});

const formatData = (list: TemplateItem[]) =>
  list.map((item) => {
    const complexes =
      item?.complexList && item?.complexList?.length > 0
        ? item.complexList.map((complex) => complex.title)
        : null;

    const buildings =
      item?.buildingList && item?.buildingList?.length > 0
        ? item.buildingList.map((building) => building.title)
        : null;

    const guestTypes =
      item?.guestTypeList && item?.guestTypeList?.length > 0
        ? item.guestTypeList.map((type) => type.title)
        : null;

    const strategies =
      item?.strategyList && item?.strategyList?.length > 0
        ? item.strategyList.map((type) => type.title)
        : null;

    return {
      id: item.id,
      title: item?.title || '-',
      complexes,
      buildings,
      guestTypes,
      strategies,
    };
  });

export const $rowCount = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ templateList }) =>
  templateList && templateList?.length > 0 ? formatData(templateList) : []
);
