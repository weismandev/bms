import { api } from '@api/api2';
import {
  GetTemplateListPayload,
  TemplatePayload,
  CreateUpdateTemplatePayload,
  ValidateObjectPayload,
} from '../interfaces';

const getTemplateList = (payload: GetTemplateListPayload) =>
  api.v4('get', 'scud-pass/template/list', payload);
const getTemplate = (payload: TemplatePayload) =>
  api.v4('get', 'scud-pass/template/get', payload);
const updateTemplate = (payload: CreateUpdateTemplatePayload) =>
  api.v4('post', 'scud-pass/template/update', payload);
const getDefaultOptions = () => api.v4('get', 'scud-pass/template/option-list');
const createTemplate = (payload: CreateUpdateTemplatePayload) =>
  api.v4('post', 'scud-pass/template/create', payload);
const deleteTemplate = (payload: TemplatePayload) =>
  api.v4('post', 'scud-pass/template/delete', payload);
const validateObjects = (payload: ValidateObjectPayload) =>
  api.v4('post', 'scud-pass/template/validate-objects', payload);
const getGuestTypes = () => api.v4('get', 'scud-pass/guest-type/list');

export const passConfigApi = {
  getTemplateList,
  getTemplate,
  updateTemplate,
  getDefaultOptions,
  createTemplate,
  deleteTemplate,
  validateObjects,
  getGuestTypes,
};
