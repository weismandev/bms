import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '@ui/index';
import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  $isDetailOpen,
  changeVisibilityDeleteModal,
  $isOpenDeleteModal,
  deleteTemplate,
} from '../models';
import { Table, Detail, SuccessModal } from '../organisms';

const PassConfigPage: FC = () => {
  const { t } = useTranslation();
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);

  const onClose = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changeVisibilityDeleteModal(false);

  return (
    <>
      <PageGate />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <Loader isLoading={isLoading} />

      <SuccessModal />
      <ColoredTextIconNotification />

      <DeleteConfirmDialog
        header={t('DeletingTemplate')}
        content={t('AreYouSureYouWantToDeleteTheTemplate')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteTemplate}
      />

      <FilterMainDetailLayout
        filter={null}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedPassConfigPage: FC = () => (
  <HaveSectionAccess>
    <PassConfigPage />
  </HaveSectionAccess>
);

export { RestrictedPassConfigPage as PassConfigPage };
