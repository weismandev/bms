export type GuestTypesReponse = {
  guestTypeList: GuestType[];
};

export type GuestType = {
  id: number;
  title: string;
};
