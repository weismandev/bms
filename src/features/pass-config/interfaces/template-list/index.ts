import { Value } from '../index';

export interface GetTemplateListPayload {
  page?: number;
  per_page?: number;
  search?: string;
  sort?: string;
  order?: string;
}

export interface GetTemplateListResponse {
  templateList: TemplateItem[];
  meta: {
    total: number;
  };
}

export interface TemplateItem {
  id: number;
  title: string;
  complexList: Value[];
  buildingList: Value[];
  guestTypeList: Value[];
  strategyList: Value[];
}
