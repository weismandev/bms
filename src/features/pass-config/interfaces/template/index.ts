import { Value } from '../index';

export interface TemplatePayload {
  id: number;
}

export interface GetTemplateResponse {
  template: {
    id: number;
    title: string;
    complexList: Value[];
    buildingList: Value[];
    strategyList: Value[];
    guestTypeList: Value[];
    predefineGuestTypeList: Value[];
    propertySectionList: SectionItem[];
  };
}

export interface SectionItem {
  name: string;
  sectionList: [];
  title: string;
  visible: boolean;
  disabled: boolean;
  propertyList: PropertyItem[];
}

export interface PropertyItem {
  manage_required: boolean;
  manage_visible: boolean;
  property: string;
  property_text: string;
  required: boolean;
  visible: boolean;
}

export interface CreateUpdateTemplatePayload {
  id?: number;
  title: string;
  complex_ids: number[];
  building_ids: number[];
  guest_type_ids: number[];
  predefine_guest_type_ids: number[];
  properties: {
    property: string;
    visible: boolean;
    required: boolean;
  }[];
}

export interface FormattedPropertyList {
  title: string;
  property: string;
  required: {
    disabled: boolean;
    value: boolean;
  };
  visibilty: {
    disabled: boolean;
    value: boolean;
  };
}

export interface SubList {
  title: string;
  sectionName: string;
  requiredDisabled: boolean;
  requiredValue: boolean;
  visibilityDisabled: boolean;
  visibiltyValue: boolean;
  list: {
    property: string;
    title: string;
    required: {
      disabled: boolean;
      value: boolean;
    };
    visibilty: {
      disabled: boolean;
      value: boolean;
    };
  }[];
}

export type ValidateObjectPayload = {
  template_id?: number;
  complex_ids: number[];
  building_ids: number[];
};

export type ValidateObjectResponse = {
  warnings: ValidateObjectsError[];
};

export type ValidateObjectsError = {
  category: string;
  object_type: ObjectType;
  object_id: number;
};

export type ObjectType = 'complex' | 'building';

export type ErrorValidationObject = {
  [key: string]: number[];
};

export type ErrorsValidationObject = {
  complexes: ErrorValidationObject;
  buildings: ErrorValidationObject;
};
