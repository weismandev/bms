export * from './template-list';
export * from './template';
export * from './guest-types';

export interface Value {
  id: number;
  title: string;
}
