import { FC, useEffect } from 'react';
import { FormattedPropertyList, SubList } from '../../interfaces';
import { getOptionParams } from '../../libs';
import { OptionsList } from '../options-list';

interface Props {
  name: string;
  option: {
    title: string;
    list: FormattedPropertyList[];
    subList: SubList[];
    visibilityDisabled: boolean;
    requiredDisabled: boolean;
    sectionName: string;
  };
  setFieldValue: (field: string, value: boolean) => void;
  values: any;
}

export const Option: FC<Props> = ({
  name,
  option: { title, list, visibilityDisabled, requiredDisabled, subList, sectionName },
  setFieldValue,
  values,
}) => {
  const formattedList = list.filter(
    (item) => !(item.property === 'guest_type' && !values.guestTypes.length)
  );

  const {
    mergedList,
    indeterminateVisibility,
    indeterminateRequired,
    onChangeVisibility,
    onChangeRequired,
  } = getOptionParams(formattedList, subList, setFieldValue, name);

  useEffect(() => {
    if (mergedList.length > 0) {
      if (mergedList.every((item) => item.visibilty.value)) {
        setFieldValue(`${name}.visibiltyValue`, true);
      }

      if (mergedList.every((item) => !item.visibilty.value)) {
        setFieldValue(`${name}.visibiltyValue`, false);
      }
    }
  }, [indeterminateVisibility]);

  useEffect(() => {
    if (mergedList.length > 0) {
      if (mergedList.every((item) => item.required.value)) {
        setFieldValue(`${name}.requiredValue`, true);
      }

      if (mergedList.every((item) => !item.required.value)) {
        setFieldValue(`${name}.requiredValue`, false);
      }
    }
  }, [indeterminateRequired]);

  return (
    <OptionsList
      title={title}
      list={list}
      subList={subList}
      name={name}
      visibilityDisabled={visibilityDisabled}
      requiredDisabled={requiredDisabled}
      indeterminateVisibility={indeterminateVisibility}
      indeterminateRequired={indeterminateRequired}
      onChangeVisibility={onChangeVisibility}
      onChangeRequired={onChangeRequired}
      setFieldValue={setFieldValue}
      sectionName={sectionName}
      values={values}
    />
  );
};
