import { FC } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, AddButton, Greedy, AdaptiveSearch } from '@ui/index';
import {
  $search,
  searchChanged,
  addClicked,
  $opened,
  $mode,
  $rowCount,
} from '../../models';
import { useStyles } from './styles';

export const TableToolbar: FC = () => {
  const searchValue = useStore($search);
  const opened = useStore($opened);
  const mode = useStore($mode);
  const totalCount = useStore($rowCount);

  const classes = useStyles();

  return (
    <Toolbar>
      <AdaptiveSearch
        {...{
          totalCount,
          searchValue,
          searchChanged,
          isAnySideOpen: false,
        }}
      />
      <Greedy />
      <AddButton
        className={classes.margin}
        onClick={addClicked}
        disabled={!opened.id && mode === 'edit'}
      />
    </Toolbar>
  );
};
