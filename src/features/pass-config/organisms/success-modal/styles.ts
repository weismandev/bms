import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  notificationPaper: {
    width: 360,
    minHeight: 280,
    minWidth: 'unset',
  },
  header: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  iconWrapper: {
    textAlign: 'center',
  },
  iconClass: {
    margin: '30px auto',
    height: 90,
    width: 90,
    color: '#00A947',
  },
  text: {
    color: '#191C29',
    textAlign: 'center',
    fontSize: 24,
    lineHeight: '32px',
    fontWeight: 500,
  },
  alert: {
    marginTop: 16,
  },
}));
