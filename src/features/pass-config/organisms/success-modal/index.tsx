import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { IconButton, Alert } from '@mui/material';
import { CheckCircleOutline, Close } from '@mui/icons-material';
import { Modal } from '@ui/index';
import {
  $isOpenSuccessModal,
  closeSuccessModal,
  $isShowSuccessModalAlert,
} from '../../models';
import { useStyles } from './styles';

export const SuccessModal: FC = () => {
  const { t } = useTranslation();
  const [isOpenSuccessModal, isShowSuccessModalAlert] = useUnit([
    $isOpenSuccessModal,
    $isShowSuccessModalAlert,
  ]);
  const classes = useStyles();

  if (!isOpenSuccessModal) {
    return false;
  }

  const handleClose = () => closeSuccessModal();

  const content = (
    <>
      <div className={classes.header}>
        <IconButton onClick={handleClose} size="large">
          <Close />
        </IconButton>
      </div>
      <div className={classes.iconWrapper}>
        <CheckCircleOutline className={classes.iconClass} />
      </div>
      <div className={classes.text}>{t('PassTemplateCreatedSuccessfully')}</div>
      {isShowSuccessModalAlert && (
        <Alert classes={{ root: classes.alert }} severity="warning">
          {t('WithoutSelectedComplexesBuildingsTemplate')}
        </Alert>
      )}
    </>
  );

  return (
    <Modal
      isOpen={isOpenSuccessModal}
      content={content}
      onClose={handleClose}
      classes={{
        paper: classes.notificationPaper,
      }}
    />
  );
};
