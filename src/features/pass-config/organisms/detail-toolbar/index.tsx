import { FC } from 'react';
import {
  CloseButton,
  SaveButton,
  CancelButton,
  EditButton,
  DeleteButton,
} from '@ui/index';
import { useStyles } from './styles';

type Props = {
  style: object;
  mode: string;
  onEdit: () => string;
  onClose: () => boolean;
  onCancel: () => void;
  onDelete: () => boolean;
  title: string;
  dirty: boolean;
  isValid: boolean;
};

export const DetailToolbar: FC<Props> = ({
  style = {},
  mode = 'view',
  onEdit,
  onClose,
  onCancel,
  onDelete,
  title = '',
  dirty,
  isValid,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.toolbar} style={style}>
      <div className={classes.firstLevel}>
        <div className={classes.title}>{title}</div>
        <CloseButton onClick={onClose} style={{ marginTop: 3 }} />
      </div>
      <div className={classes.secondLevel}>
        {mode === 'edit' ? (
          <>
            <SaveButton type="submit" disabled={!isValid || !dirty} />
            <CancelButton onClick={onCancel} />
          </>
        ) : (
          <>
            <EditButton onClick={onEdit} />
            <DeleteButton onClick={onDelete} />
          </>
        )}
      </div>
    </div>
  );
};
