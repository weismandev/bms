import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  toolbar: {
    display: 'flex',
    flexDirection: 'column',
    gap: 24,
  },
  firstLevel: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    fontWeight: 500,
    fontStyle: 'normal',
    color: 'rgba(25, 28, 41, 0.87)',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  secondLevel: {
    display: 'flex',
    gap: 8,
  },
});
