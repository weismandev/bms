import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    padding: 24,
  },
  formatterContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'start',
  },
  chip: {
    marginTop: 2,
  },
  otherValues: {
    display: 'grid',
    gap: 10,
    padding: 10,
    background: '#fcfcfc',
  },
  colorCount: {
    color: '#1F87E5',
  },
});

export const styles = {
  root: {
    height: '100%',
  },
  toolbar: {
    padding: '0',
    marginBottom: '12px',
    minHeight: '36px',
    height: '36px',
    flexWrap: 'nowrap',
    borderBottom: 'none',
  },
};
