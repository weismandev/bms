import { useMemo, useCallback, FC, ReactNode, memo } from 'react';
import { useStore } from 'effector-react';
import { Chip } from '@mui/material';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  SortingState,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  SortingLabel,
  PagingPanel,
  Popover,
  IconButton,
} from '@ui/index';
import {
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $sorting,
  sortChanged,
  excludedColumnsFromSort,
  columnOrderChanged,
  $columnOrder,
  open,
  $opened,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { useStyles, styles } from './styles';

const { t } = i18n;

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);

  const onRowClick = useCallback((_, id) => open(id), []);
  const isSelected = props.row.id === opened.id;

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={styles.root}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: any) => <Toolbar.Root {...props} style={styles.toolbar} />;

const FormatterComponent = ({ value }: { value: string[] }) => {
  const classes = useStyles();

  if (!value || value[0]?.length === 0 || !value[0]) {
    return '-';
  }

  return (
    <div className={classes.formatterContainer}>
      <Chip classes={{ root: classes.chip }} label={value[0]} size="small" />
      {value[1] && (
        <Popover
          trigger={
            <IconButton size="large">
              <span className={classes.colorCount}>
                + {Number(value.slice(1).length)}
              </span>
            </IconButton>
          }
        >
          <div className={classes.otherValues}>
            {value.slice(1).map((item: string, index: number) => (
              <div key={index}>{item}</div>
            ))}
          </div>
        </Popover>
      )}
    </div>
  );
};

const ComplexesTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

const BuildingsTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

const GuestTypesTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

const StrategiesTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

export const TableTemplate: FC = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const sorting = useStore($sorting);
  const columnOrder = useStore($columnOrder);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={({ id = null }) => id}
        columns={columns}
      >
        <DragDropProvider />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <ComplexesTypeProvider for={['complexes']} />
        <BuildingsTypeProvider for={['buildings']} />
        <GuestTypesTypeProvider for={['guestTypes']} />
        <StrategiesTypeProvider for={['strategies']} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});
