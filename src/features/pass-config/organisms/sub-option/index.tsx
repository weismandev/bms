import { FC, useEffect } from 'react';
import { SubList } from '../../interfaces';
import { getSubOptionParams } from '../../libs';
import { OptionsList } from '../options-list';

interface SubOptionProps {
  item: SubList;
  name: string;
  setFieldValue: (field: string, value: boolean) => void;
  values: any;
}

export const SubOption: FC<SubOptionProps> = ({
  item: {
    title,
    visibilityDisabled,
    requiredDisabled,
    list,
    visibiltyValue,
    sectionName,
  },
  name,
  setFieldValue,
  values,
}) => {
  const {
    indeterminateVisibility,
    indeterminateRequired,
    onChangeVisibility,
    onChangeRequired,
  } = getSubOptionParams(list, setFieldValue, name);

  useEffect(() => {
    if (list.length > 0 && list.every((item) => item.visibilty.value)) {
      setFieldValue(`${name}.visibiltyValue`, true);
    }

    if (list.length > 0 && list.every((item) => !item.visibilty.value)) {
      setFieldValue(`${name}.visibiltyValue`, false);
    }
  }, [indeterminateVisibility]);

  useEffect(() => {
    if (list.length > 0 && list.every((item) => item.required.value)) {
      setFieldValue(`${name}.requiredValue`, true);
    }

    if (list.length > 0 && list.every((item) => !item.required.value)) {
      setFieldValue(`${name}.requiredValue`, false);
    }
  }, [indeterminateRequired]);

  return (
    <OptionsList
      title={title}
      list={list}
      name={name}
      visibilityDisabled={visibilityDisabled}
      requiredDisabled={requiredDisabled}
      indeterminateVisibility={indeterminateVisibility}
      indeterminateRequired={indeterminateRequired}
      onChangeVisibility={onChangeVisibility}
      onChangeRequired={onChangeRequired}
      visibiltyValue={visibiltyValue}
      sectionName={sectionName}
      values={values}
      setFieldValue={setFieldValue}
    />
  );
};
