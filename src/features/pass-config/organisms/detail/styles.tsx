import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    height: '100%',
  },
});
