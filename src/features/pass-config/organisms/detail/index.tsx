import { FC, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik } from 'formik';
import { Wrapper } from '@ui/index';
import { $opened, detailSubmitted } from '../../models';
import { DetailForm } from '../detail-form';
import { useStyles } from './styles';
import { validationSchema } from './validation';

export const Detail: FC = memo(() => {
  const opened = useStore($opened);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={detailSubmitted}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm, setFieldValue, dirty, isValid }) => (
          <DetailForm
            values={values}
            resetForm={resetForm}
            setFieldValue={setFieldValue}
            dirty={dirty}
            isValid={isValid}
          />
        )}
      />
    </Wrapper>
  );
});
