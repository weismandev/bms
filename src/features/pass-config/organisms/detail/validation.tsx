import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(t('thisIsRequiredField') ?? ''),
  strategies: Yup.array()
    .required(t('thisIsRequiredField') ?? '')
    .min(1, t('thisIsRequiredField') ?? ''),
});
