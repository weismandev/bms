import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  form: {
    height: 'calc(100% - 50px)',
    position: 'relative',
  },
  content: {
    height: 'calc(100% - 88px)',
    padding: '0 0 24px 24px',
  },
  formContent: {
    paddingRight: 24,
  },
  settingsHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontWeight: 500,
    fontSize: 14,
    color: '#7D7D8E',
    opacity: 0.7,
  },
  settingsHeaderTooltip: {
    background: '#65657B',
    fontSize: 13,
    color: '#FFFFFF',
    fontWeight: 500,
    borderRadius: 4,
  },
  settingsRequiredTitle: {
    cursor: 'pointer',
  },
  alert: {
    margin: '24px 0px',
  },
});

export const styles = {
  toolbar: {
    padding: 24,
  },
};
