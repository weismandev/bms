import { FC, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { Form, FieldArray, FastField, Field } from 'formik';
import { Tooltip, Alert } from '@mui/material';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { PassTypesSelectField } from '@features/pass-types-select';
import i18n from '@shared/config/i18n';
import { CustomScrollbar, InputField, Divider, SelectField } from '@ui/index';
import {
  FormattedPropertyList,
  SubList,
  ErrorValidationObject,
  Value,
} from '../../interfaces';
import {
  $opened,
  $mode,
  changeMode,
  changedDetailVisibility,
  changeVisibilityDeleteModal,
  validateObject,
  $errorsValidationObjects,
} from '../../models';
import { DetailToolbar } from '../detail-toolbar';
import { GuestTypesSelectField } from '../guest-types-select';
import { Option } from '../option';
import { useStyles, styles } from './styles';

const { t } = i18n;

type Props = {
  values: any;
  resetForm: () => void;
  setFieldValue: (field: string, value: any) => void;
  dirty: boolean;
  isValid: boolean;
};

const formatErrors = (data: ErrorValidationObject) => {
  if (!data) {
    return null;
  }

  const errorsData = Object.entries(data);

  if (errorsData?.length) {
    const result: {
      error?: string;
      errorIds?: number[];
    } = {};

    if (errorsData[0][0]) {
      // eslint-disable-next-line prefer-destructuring
      result.error = errorsData[0][0];
    }

    const ids = errorsData.reduce((acc: number[], item) => [...acc, ...item[1]], []);
    const uniqueIds = Array.from(new Set(ids));

    if (uniqueIds.length) {
      result.errorIds = uniqueIds;
    }

    if (result?.error && result?.errorIds) {
      return result;
    }
  }

  return null;
};

export const DetailForm: FC<Props> = ({
  resetForm,
  values,
  setFieldValue,
  dirty,
  isValid,
}) => {
  const [mode, opened, errorsValidationObjects] = useUnit([
    $mode,
    $opened,
    $errorsValidationObjects,
  ]);
  const classes = useStyles();

  const isNew = !opened.id;
  const { complexes, buildings } = values;
  const headerTitle = isNew ? t('NewTemplate') : opened?.title;

  const handleEdit = () => changeMode('edit');

  const handleClose = () => changedDetailVisibility(false);

  const handleCancel = () => {
    if (isNew) {
      changedDetailVisibility(false);
    } else {
      changeMode('view');
      resetForm();
    }
  };

  const handleDelete = () => changeVisibilityDeleteModal(true);

  useEffect(() => {
    const params = opened.id
      ? { templateId: opened.id, complexes, buildings }
      : { complexes, buildings };

    validateObject(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [complexes, buildings]);

  const complexesError = formatErrors(
    errorsValidationObjects?.complexes as ErrorValidationObject
  );

  const isShowErrorComplexes = values.complexes?.length > 0 && complexesError?.error;

  const buildingsError = formatErrors(
    errorsValidationObjects?.buildings as ErrorValidationObject
  );

  const isShowErrorBuildings = values.buildings?.length > 0 && buildingsError?.error;

  const handleGuestType = (value: Value[]) => {
    setFieldValue('guestTypes', value);
    setFieldValue('preFillingNames', []);
  };

  return (
    <Form className={classes.form}>
      <DetailToolbar
        style={styles.toolbar}
        mode={mode}
        onEdit={handleEdit}
        onClose={handleClose}
        onCancel={handleCancel}
        onDelete={handleDelete}
        title={headerTitle}
        dirty={dirty}
        isValid={isValid}
      />
      <div className={classes.content}>
        <CustomScrollbar>
          <div className={classes.formContent}>
            {mode === 'edit' ? (
              <>
                <FastField
                  name="title"
                  component={InputField}
                  label={t('NameTitle')}
                  placeholder={t('EnterName')}
                  required
                />
                <Field
                  name="complexes"
                  component={ComplexSelectField}
                  label={t('complex')}
                  placeholder={t('ChooseComplex')}
                  isMulti
                  divider={false}
                  warningIds={complexesError?.errorIds}
                />
                {isShowErrorComplexes ? (
                  <Alert classes={{ root: classes.alert }} severity="warning">
                    {complexesError.error}
                  </Alert>
                ) : (
                  <Alert classes={{ root: classes.alert }} severity="info">
                    {t('WhenSelectComplexTemplateApplyAllBuildingsComplex')}
                  </Alert>
                )}
                <Divider />
                <Field
                  name="buildings"
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  isMulti
                  divider={false}
                  warningIds={buildingsError?.errorIds}
                  escapeSybmols={/\[(.*)\]/}
                />
                {isShowErrorBuildings ? (
                  <Alert classes={{ root: classes.alert }} severity="warning">
                    {buildingsError.error}
                  </Alert>
                ) : (
                  <Alert classes={{ root: classes.alert }} severity="info">
                    {t('ForSelectedBuildingsTemplatePriority')}
                  </Alert>
                )}
                <Divider />
                <FastField
                  name="strategies"
                  component={PassTypesSelectField}
                  label={t('AvailableTypesPasses')}
                  placeholder={t('SelectPassTypePlaceholder')}
                  isMulti
                  required
                />
                <FastField
                  name="guestTypes"
                  component={GuestTypesSelectField}
                  label={t('AvailableGuestTypes')}
                  placeholder={t('SelectGuestType')}
                  isMulti
                  onChange={handleGuestType}
                />
                {values?.guestTypes?.length > 0 && (
                  <Field
                    name="preFillingNames"
                    component={SelectField}
                    label={t('PreFillingLastNameAndFirstName')}
                    placeholder={t('SelectGuestType')}
                    options={values?.guestTypes ?? []}
                    isMulti
                  />
                )}
              </>
            ) : (
              <>
                <FastField
                  name="title"
                  component={InputField}
                  label={t('NameTitle')}
                  placeholder={t('EnterName')}
                  required
                  mode="view"
                />
                <FastField
                  name="complexes"
                  component={ComplexSelectField}
                  label={t('complex')}
                  placeholder={t('ChooseComplex')}
                  isMulti
                  mode="view"
                />
                <FastField
                  name="buildings"
                  component={HouseSelectField}
                  label={t('building')}
                  placeholder={t('selectBbuilding')}
                  isMulti
                  mode="view"
                />
                <FastField
                  name="strategies"
                  component={PassTypesSelectField}
                  label={t('AvailableTypesPasses')}
                  placeholder={t('SelectPassTypePlaceholder')}
                  isMulti
                  required
                  mode="view"
                />
                <FastField
                  name="guestTypes"
                  component={GuestTypesSelectField}
                  label={t('AvailableGuestTypes')}
                  placeholder={t('SelectGuestType')}
                  isMulti
                  mode="view"
                />
                {values?.guestTypes?.length > 0 && (
                  <Field
                    name="preFillingNames"
                    component={SelectField}
                    label={t('PreFillingLastNameAndFirstName')}
                    placeholder={t('SelectGuestType')}
                    options={values?.guestTypes ?? []}
                    isMulti
                    mode="view"
                  />
                )}
              </>
            )}
            <div className={classes.settingsHeader}>
              <div>{t('ProfilePage.Settings')}</div>
              <Tooltip
                title={t('TheFieldWillBeRequiredByTheUser')}
                classes={{ tooltip: classes.settingsHeaderTooltip }}
              >
                <div className={classes.settingsRequiredTitle}>{t('Required')}</div>
              </Tooltip>
            </div>
            <Divider />
            <FieldArray
              name="options"
              render={() => {
                if (!values?.options) {
                  return null;
                }

                return values.options.map(
                  (
                    item: {
                      title: string;
                      list: FormattedPropertyList[];
                      subList: SubList[];
                      visibilityDisabled: boolean;
                      requiredDisabled: boolean;
                      sectionName: string;
                    },
                    index: number
                  ) => (
                    <Option
                      key={index}
                      option={item}
                      name={`options[${index}]`}
                      setFieldValue={setFieldValue}
                      values={values}
                    />
                  )
                );
              }}
            />
          </div>
        </CustomScrollbar>
      </div>
    </Form>
  );
};
