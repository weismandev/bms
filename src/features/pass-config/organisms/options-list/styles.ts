import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  wrapper: {
    padding: '0px 0px 0px 20px',
  },
  checkboxWrapper: {
    display: 'flex',
    width: '100%',
  },
  headerCheckboxes: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  icon: {
    height: 25,
    width: 25,
    marginTop: 8,
    marginRight: 15,
  },
  childCheckboxes: {
    marginLeft: 30,
  },
  checkBoxes: {
    display: 'flex',
    justifyContent: 'space-between',
    marginLeft: 30,
  },
});
