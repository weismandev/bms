import { FC, useState, useEffect } from 'react';
import { useStore } from 'effector-react';
import { Field, FieldArray, FastField } from 'formik';
import { Collapse, IconButton } from '@mui/material';
import { ExpandMore, ExpandLess, IndeterminateCheckBox } from '@mui/icons-material';
import { SwitchField, Divider } from '@ui/index';
import { FormattedPropertyList, SubList } from '../../interfaces';
import { $mode } from '../../models';
import { SubOptions } from '../sub-options';
import { useStyles } from './styles';

interface OptionsListProps {
  title: string;
  list: FormattedPropertyList[];
  subList?: SubList[];
  name: string;
  visibilityDisabled: boolean;
  requiredDisabled: boolean;
  indeterminateVisibility: boolean;
  indeterminateRequired: boolean;
  onChangeVisibility: (params: any) => void;
  onChangeRequired: (params: any) => void;
  setFieldValue: (field: string, value: boolean) => void;
  sectionName: string;
  values: any;
}

export const OptionsList: FC<OptionsListProps> = ({
  title,
  list,
  subList = [],
  name,
  visibilityDisabled,
  requiredDisabled,
  indeterminateVisibility,
  indeterminateRequired,
  onChangeVisibility,
  onChangeRequired,
  setFieldValue,
  sectionName,
  values,
}) => {
  const mode = useStore($mode);

  const [expanded, setExpanded] = useState(true);
  const handleExpanded = () => setExpanded(!expanded);

  const classes = useStyles();

  useEffect(() => {
    if (sectionName === 'guest_data') {
      const guestData = values?.options?.find(
        (item) => item?.sectionName === 'guest_data'
      );

      if (guestData) {
        const guestTypeIndex = guestData.list.findIndex(
          (item) => item.property === 'guest_type'
        );

        const guestDataIndex = values.options.findIndex(
          (item) => item.sectionName === 'guest_data'
        );

        if (
          values?.guestTypes?.length > 0 &&
          (guestTypeIndex || guestTypeIndex === 0) &&
          (!values.options[guestDataIndex]?.list[guestTypeIndex]?.visibilty?.value ||
            !values.options[guestDataIndex]?.list[guestTypeIndex]?.visibilty?.disabled)
        ) {
          setFieldValue(
            `${name}.list[${guestTypeIndex as number}].visibilty.value`,
            true
          );
          setFieldValue(
            `${name}.list[${guestTypeIndex as number}].visibilty.disabled`,
            true
          );
        } else if (
          values?.guestTypes?.length === 0 &&
          (guestTypeIndex || guestTypeIndex === 0) &&
          values.options[guestDataIndex]?.list[guestTypeIndex]?.visibilty?.value
        ) {
          setFieldValue(
            `${name}.list[${guestTypeIndex as number}].visibilty.value`,
            false
          );
          setFieldValue(
            `${name}.list[${guestTypeIndex as number}].required.value`,
            false
          );
        }
      }
    }

    if (sectionName === 'vehicle') {
      const section = values?.options?.find((item) => item?.sectionName === 'vehicle');

      if (section) {
        const vehicleLicencePlateIndex = section.list.findIndex(
          (item) => item?.property === 'vehicle_licence_plate'
        );

        const { visibilty, required } = section.list[vehicleLicencePlateIndex];

        if (visibilty?.value !== required?.value) {
          setFieldValue(
            `${name}.list[${vehicleLicencePlateIndex as number}].required.value`,
            Boolean(visibilty.value)
          );
        }
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.checkboxWrapper}>
        <IconButton onClick={handleExpanded} className={classes.icon} size="large">
          {expanded ? (
            <ExpandMore titleAccess="Свернуть" />
          ) : (
            <ExpandLess titleAccess="Развернуть" />
          )}
        </IconButton>
        <div className={classes.headerCheckboxes}>
          <Field
            name={`${name}.visibiltyValue`}
            component={SwitchField}
            label={title}
            labelPlacement="end"
            divider={false}
            indeterminateIcon={<IndeterminateCheckBox />}
            indeterminate={indeterminateVisibility}
            disabled={mode === 'view' || visibilityDisabled}
            onChange={onChangeVisibility}
          />
          <Field
            name={`${name}.requiredValue`}
            component={SwitchField}
            label={null}
            labelPlacement="end"
            divider={false}
            indeterminateIcon={<IndeterminateCheckBox />}
            indeterminate={indeterminateRequired}
            disabled={mode === 'view' || requiredDisabled}
            onChange={onChangeRequired}
          />
        </div>
      </div>
      <Divider />
      <Collapse in={expanded} timeout="auto">
        <FieldArray
          name={`${name}.list`}
          render={() => {
            if (!list) {
              return null;
            }

            return list.map((element, index) => {
              const fieldName = `${name}.list[${index}]`;

              if (element.property === 'guest_type' && values?.guestTypes?.length === 0) {
                return null;
              }

              return (
                <div key={element.property} className={classes.childCheckboxes}>
                  <div className={classes.checkBoxes}>
                    {mode === 'view' || element.visibilty.disabled ? (
                      <FastField
                        name={`${fieldName}.visibilty.value`}
                        component={SwitchField}
                        label={element.title}
                        labelPlacement="end"
                        disabled
                        divider={false}
                      />
                    ) : (
                      <FastField
                        name={`${fieldName}.visibilty.value`}
                        component={SwitchField}
                        label={element.title}
                        labelPlacement="end"
                        divider={false}
                      />
                    )}
                    {mode === 'view' || element.required.disabled ? (
                      <FastField
                        name={`${fieldName}.required.value`}
                        component={SwitchField}
                        label={null}
                        labelPlacement="end"
                        disabled
                        divider={false}
                      />
                    ) : (
                      <FastField
                        name={`${fieldName}.required.value`}
                        component={SwitchField}
                        label={null}
                        labelPlacement="end"
                        divider={false}
                      />
                    )}
                  </div>
                  <Divider />
                </div>
              );
            });
          }}
        />
        {subList.length > 0 && setFieldValue && (
          <SubOptions
            subList={subList}
            name={name}
            setFieldValue={setFieldValue}
            values={values}
          />
        )}
      </Collapse>
    </div>
  );
};
