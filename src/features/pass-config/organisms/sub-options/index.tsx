import { FC } from 'react';
import { FieldArray } from 'formik';
import { SubList } from '../../interfaces';
import { SubOption } from '../sub-option';

interface SubOptionsProps {
  subList: SubList[];
  name: string;
  setFieldValue: (field: string, value: boolean) => void;
  values: any;
}

export const SubOptions: FC<SubOptionsProps> = ({
  subList,
  name,
  setFieldValue,
  values,
}) =>
  Array.isArray(subList) && subList.length > 0 ? (
    <FieldArray
      name={`${name}.subList`}
      render={() =>
        subList.map((item, index) => (
          <SubOption
            key={index}
            name={`${name}.subList[${index}]`}
            item={item}
            setFieldValue={setFieldValue}
            values={values}
          />
        ))
      }
    />
  ) : null;
