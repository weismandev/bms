import { FormattedPropertyList, SubList } from '../../interfaces';

export const getOptionParams = (
  list: FormattedPropertyList[],
  subList: SubList[],
  setFieldValue: (field: string, value: boolean) => void,
  name: string
) => {
  const sbList = subList.map((item) => item.list.map((element) => element)).flat();

  const mergedList = [...list, ...sbList];

  const filtredVisibilityList = list.filter((item) => item.visibilty.value);
  const filtredVisibilitySubList = subList
    .map((item) => item.list.filter((element) => element.visibilty.value))
    .flat();

  const filtredRequiredList = list.filter((item) => item.required.value);
  const filtredRequiredSubList = subList
    .map((item) => item.list.filter((element) => element.required.value))
    .flat();

  const mergedFiltredVisibility = [...filtredVisibilityList, ...filtredVisibilitySubList];

  const mergedFiltredRequired = [...filtredRequiredList, ...filtredRequiredSubList];

  const indeterminateVisibility =
    mergedFiltredVisibility.length > 0 &&
    mergedFiltredVisibility.length < mergedList.length;

  const indeterminateRequired =
    mergedFiltredRequired.length > 0 && mergedFiltredRequired.length < mergedList.length;

  const isAllCheckedRequiredOptions = list
    .filter((item) => !item.required.disabled)
    .every((item) => item.required.value);

  const isDisabledCheckedRequiredOptions =
    list.filter((item) => item.required.value && item.required.disabled).length > 0;

  const onChangeVisibility = ({
    target: { checked },
  }: {
    target: { checked: boolean };
  }) => {
    setFieldValue(`${name}.visibiltyValue`, checked);

    list.forEach((_, index) => {
      const listName = `${name}.list[${index}].visibilty.value`;

      setFieldValue(listName, checked);
    });

    subList.forEach((value, index) => {
      const listName = `${name}.subList[${index}]`;

      setFieldValue(`${listName}.visibiltyValue`, checked);

      value.list.forEach((_, idx) => {
        const fieldName = `${listName}.list[${idx}].visibilty.value`;

        setFieldValue(fieldName, checked);
      });
    });
  };

  const onChangeRequired = ({
    target: { checked },
  }: {
    target: { checked: boolean };
  }) => {
    setFieldValue(`${name}.requiredValue`, checked);

    list.forEach(({ required: { disabled } }, index) => {
      const listName = `${name}.list[${index}].required.value`;

      if (!isAllCheckedRequiredOptions && !checked && !disabled) {
        return setFieldValue(listName, true);
      }

      if (isAllCheckedRequiredOptions && !isDisabledCheckedRequiredOptions) {
        return setFieldValue(listName, false);
      }

      if (!disabled) {
        return setFieldValue(listName, checked);
      }

      return null;
    });

    subList.forEach((value, index) => {
      const listName = `${name}.subList[${index}]`;

      if (
        !indeterminateRequired &&
        !checked &&
        isAllCheckedRequiredOptions &&
        !value.requiredDisabled
      ) {
        setFieldValue(`${listName}.requiredValue`, false);
      }

      if (
        !indeterminateRequired &&
        checked &&
        !isAllCheckedRequiredOptions &&
        !value.requiredDisabled
      ) {
        setFieldValue(`${listName}.requiredValue`, true);
      }

      if (
        indeterminateRequired &&
        checked &&
        !isAllCheckedRequiredOptions &&
        !value.requiredDisabled
      ) {
        setFieldValue(`${listName}.requiredValue`, true);
      }

      value.list.forEach(({ required: { disabled } }, idx) => {
        const fieldName = `${listName}.list[${idx}].required.value`;

        if (
          indeterminateRequired &&
          checked &&
          isAllCheckedRequiredOptions &&
          !disabled
        ) {
          setFieldValue(fieldName, true);
        }

        if (
          indeterminateRequired &&
          !checked &&
          isAllCheckedRequiredOptions &&
          !disabled
        ) {
          setFieldValue(fieldName, false);
        }

        if (
          indeterminateRequired &&
          checked &&
          !isAllCheckedRequiredOptions &&
          !disabled
        ) {
          setFieldValue(fieldName, true);
        }

        if (
          !indeterminateRequired &&
          !checked &&
          isAllCheckedRequiredOptions &&
          !disabled
        ) {
          setFieldValue(fieldName, false);
        }

        if (
          !indeterminateRequired &&
          checked &&
          !isAllCheckedRequiredOptions &&
          !disabled
        ) {
          setFieldValue(fieldName, true);
        }
      });
    });
  };

  return {
    mergedList,
    indeterminateVisibility,
    indeterminateRequired,
    onChangeVisibility,
    onChangeRequired,
  };
};
