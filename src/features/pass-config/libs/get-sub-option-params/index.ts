import { FormattedPropertyList } from '../../interfaces';

export const getSubOptionParams = (
  list: FormattedPropertyList[],
  setFieldValue: (field: string, value: boolean) => void,
  name: string
) => {
  const filtredVisibilityList = list.filter((item) => item.visibilty.value);

  const filtredRequiredList = list.filter((item) => item.required.value);

  const indeterminateVisibility =
    filtredVisibilityList.length > 0 && filtredVisibilityList.length < list.length;

  const indeterminateRequired =
    filtredRequiredList.length > 0 && filtredRequiredList.length < list.length;

  const isAllCheckedRequiredOptions = list
    .filter((item) => !item.required.disabled)
    .every((item) => item.required.value);

  const isDisabledCheckedRequiredOptions =
    list.filter((item) => item.required.value && item.required.disabled).length > 0;

  const onChangeVisibility = ({
    target: { checked },
  }: {
    target: { checked: boolean };
  }) => {
    setFieldValue(`${name}.visibiltyValue`, checked);

    list.forEach((_, index) => {
      const listName = `${name}.list[${index}].visibilty.value`;

      setFieldValue(listName, checked);
    });
  };

  const onChangeRequired = ({
    target: { checked },
  }: {
    target: { checked: boolean };
  }) => {
    setFieldValue(`${name}.requiredValue`, checked);

    list.forEach(({ required: { disabled } }, index) => {
      const listName = `${name}.list[${index}].required.value`;

      if (!isAllCheckedRequiredOptions && !checked && !disabled) {
        return setFieldValue(listName, true);
      }

      if (isAllCheckedRequiredOptions && !isDisabledCheckedRequiredOptions) {
        return setFieldValue(listName, false);
      }

      if (!disabled) {
        return setFieldValue(listName, checked);
      }

      return null;
    });
  };

  return {
    indeterminateVisibility,
    indeterminateRequired,
    onChangeVisibility,
    onChangeRequired,
  };
};
