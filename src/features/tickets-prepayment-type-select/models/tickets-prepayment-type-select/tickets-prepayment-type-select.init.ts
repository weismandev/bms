import { signout } from '@features/common';

import { ticketsPrepaymentTypeSelectApi } from '../../api';
import { fxGetList, $data, $isLoading } from './tickets-prepayment-type-select.model';

fxGetList.use(ticketsPrepaymentTypeSelectApi.getPaymentTypes);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.prepayments)) {
      return [];
    }

    return result.prepayments.map((item) => ({
      id: item.name,
      name: item.name,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
