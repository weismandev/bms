import { createEffect, createStore } from 'effector';

import { GetPaymentTypesResponse, PaymentItem } from '../../interfaces';

export const fxGetList = createEffect<void, GetPaymentTypesResponse, Error>();

export const $data = createStore<PaymentItem[]>([]);
export const $isLoading = createStore<boolean>(false);
