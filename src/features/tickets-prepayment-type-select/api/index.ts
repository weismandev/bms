import { api } from '@api/api2';

const getPaymentTypes = () => api.v1('get', '/tck/payment-types/list');

export const ticketsPrepaymentTypeSelectApi = {
  getPaymentTypes,
};
