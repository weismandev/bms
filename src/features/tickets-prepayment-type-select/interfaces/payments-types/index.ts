export interface GetPaymentTypesResponse {
  invoice_statuses: PaymentItem[];
  prepayments: PaymentItem[];
  prices: PaymentItem[];
}

export interface PaymentItem {
  id: number | string;
  name: string;
  title: string;
}
