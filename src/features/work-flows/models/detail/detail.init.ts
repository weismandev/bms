import { sample } from 'effector';
import { delay } from 'patronum';
import { nanoid } from 'nanoid';

import { signout, historyPush } from '@features/common';

import { pageUnmounted, pageMounted } from '../page/page.model';
import {
  openViaUrl,
  $isDetailOpen,
  changedDetailVisibility,
  fxGetTechCard,
  $opened,
  entityApi,
  $mode,
  $path,
  $entityId,
  fxCreateTechCard,
  fxUpdateTechCard,
  fxDeleteTechCard,
  deleteTechCard,
} from './detail.model';
import { Opened } from '../../interfaces';

$isDetailOpen
  .on(fxGetTechCard.done, () => true)
  .reset([signout, pageUnmounted, fxGetTechCard.fail, fxDeleteTechCard.done]);

// при маунте, если в url есть id,то запрашивается техкарта по id
sample({
  clock: pageMounted,
  source: $entityId,
  filter: (id) => id && id.length > 0,
  fn: (id) => ({
    id,
  }),
  target: fxGetTechCard,
});

// если запрос на получение техкарты падает, то из url убирается id
sample({
  clock: delay({ source: fxGetTechCard.fail, timeout: 1000 }),
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// при закрытии карточки из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

$entityId.reset([pageUnmounted, signout]);

// при клике на строку в таблице отправляется запрос на получение техкарты по id
sample({
  clock: openViaUrl,
  fn: (id) => ({ id }),
  target: fxGetTechCard,
});

$opened
  .on(fxGetTechCard.done, (_, { result: { tech_card } }) => ({
    id: tech_card.id,
    title: tech_card?.title || '-',
    works: Array.isArray(tech_card?.works)
      ? tech_card.works.map((item) => ({
          _id: nanoid(3),
          work: {
            id: item.id,
            title: item.title,
          },
        }))
      : [],
  }))
  .reset([pageUnmounted, signout, fxGetTechCard]);

const formatPayload = (data: Opened) => ({
  title: data.title,
  work_ids:
    Array.isArray(data?.works) && data.works.length > 0
      ? data.works.map((item) => item.work.id)
      : [],
});

// отправка запроса на создание техкарты
sample({
  clock: entityApi.create,
  fn: (data) => formatPayload(data),
  target: fxCreateTechCard,
});

$mode.reset([pageUnmounted, signout, fxCreateTechCard.done, fxUpdateTechCard.done]);

// повторный запрос техкарты после создания
sample({
  clock: fxCreateTechCard.done,
  fn: ({
    result: {
      tech_card: { id },
    },
  }) => ({ id }),
  target: fxGetTechCard,
});

// после создания техкаты в url добавляется id
sample({
  clock: fxCreateTechCard.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        tech_card: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

// отправка запроса на обновления техкарты
sample({
  clock: entityApi.update,
  fn: ({ id, ...data }) => ({
    id,
    ...formatPayload(data),
  }),
  target: fxUpdateTechCard,
});

// повторный запрос техкарты после обновления
sample({
  clock: fxUpdateTechCard.done,
  fn: ({
    result: {
      tech_card: { id },
    },
  }) => ({ id }),
  target: fxGetTechCard,
});

// отправка запроса на удаление техкарты
sample({
  clock: deleteTechCard,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxDeleteTechCard,
});

// после удаления из url удаляется id
sample({
  clock: fxDeleteTechCard.done,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});
