import { createEffect, createEvent } from 'effector';

import { createDetailBag } from '@tools/factories';
import { $pathname } from '@features/common';

import { getWorkEntity } from '../../libs';
import {
  GetTechCardPayload,
  GetTechCardResponse,
  CreateUpdateTechCardPayload,
  DeleteTechCardPayload,
} from '../../interfaces';

const newEntity = {
  id: null,
  title: '',
  works: [getWorkEntity()],
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map(
  (pathname: string) => pathname.split('/')[2] || null
);

export const fxGetTechCard = createEffect<
  GetTechCardPayload,
  GetTechCardResponse,
  Error
>();
export const fxCreateTechCard = createEffect<
  CreateUpdateTechCardPayload,
  GetTechCardResponse,
  Error
>();
export const fxUpdateTechCard = createEffect<
  CreateUpdateTechCardPayload,
  GetTechCardResponse,
  Error
>();
export const fxDeleteTechCard = createEffect<DeleteTechCardPayload, object, Error>();

export const deleteTechCard = createEvent<void>();
