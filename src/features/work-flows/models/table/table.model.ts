import { combine } from 'effector';

import i18n from '@shared/config/i18n';
import { createTableBag, Columns } from '@features/data-grid';

import { $rawData } from '../page/page.model';
import { TechCard } from '../../interfaces';

const { t } = i18n;

const columns = [
  {
    field: 'id',
    headerName: t('№pp'),
    width: 70,
    sortable: false,
  },
  {
    field: 'title',
    headerName: t('NameTechnicalMap'),
    width: 210,
    sortable: true,
  },
  {
    field: 'works',
    headerName: t('NameOfWorks'),
    width: 230,
    sortable: false,
  },
  {
    field: 'qualifications',
    headerName: t('Qualifications'),
    width: 230,
    sortable: false,
  },
  {
    field: 'plannedDuration',
    headerName: t('WorkingHours'),
    width: 140,
    sortable: true,
  },
];

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} = createTableBag({
  columns,
  pageSize: 25,
});

const getTime = (durationInMinutes: number) => {
  const hours = Math.floor(durationInMinutes / 60);
  const minutes = durationInMinutes % 60;

  return `${hours ? `${hours} ч.` : ''} ${minutes ? `${minutes} мин.` : ''}`;
};

const formatData = (cards: TechCard[]) =>
  cards.map((card) => ({
    id: card.id,
    title: card?.title || '-',
    works:
      Array.isArray(card?.works) && card.works.length > 0
        ? card.works.map((work) => ({
            id: work.id,
            title: work?.title || t('Unknown'),
          }))
        : [{ id: 1, title: '-' }],
    qualifications:
      Array.isArray(card?.qualifications) && card.qualifications.length > 0
        ? card.qualifications.map((qualification) => ({
            id: qualification.id,
            title: qualification?.title || t('Unknown'),
          }))
        : [{ id: 1, title: '-' }],
    plannedDuration: card?.total_planned_duration_minutes
      ? getTime(card.total_planned_duration_minutes)
      : '-',
  }));

export const $count = $rawData.map(({ meta }) => meta?.total || 0);
export const $tableData = $rawData.map(({ tech_cards }) =>
  Array.isArray(tech_cards) && tech_cards.length > 0 ? formatData(tech_cards) : []
);
export const $countPage = combine($count, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);
export const $savedColumns = $columns.map((currentColumn: Columns[]) =>
  currentColumn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
