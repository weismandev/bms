import { createStore, createEvent } from 'effector';

export const changedVisibilityDeleteModal = createEvent<boolean>();

export const $isOpenDeleteModal = createStore<boolean>(false);
