import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetTechCardsResponse, GetTechCardsPayload } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isLoadingTable = createStore<boolean>(false);
export const $rawData = createStore<GetTechCardsResponse>({
  meta: { total: 0 },
  tech_cards: [],
});

export const fxGetTechCards = createEffect<
  GetTechCardsPayload,
  GetTechCardsResponse,
  Error
>();
