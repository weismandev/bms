import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';

import { signout } from '@features/common';

import { techCardsApi } from '../../api';
import {
  $isErrorDialogOpen,
  $error,
  $isLoading,
  pageUnmounted,
  changedErrorDialogVisibility,
  pageMounted,
  $isLoadingTable,
  fxGetTechCards,
  $rawData,
} from './page.model';
import { $tableParams } from '../table/table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  fxGetTechCard,
  fxCreateTechCard,
  fxUpdateTechCard,
  fxDeleteTechCard,
} from '../detail/detail.model';
import { GetTechCardsPayload } from '../../interfaces';

fxGetTechCards.use(techCardsApi.getTechCards);
fxGetTechCard.use(techCardsApi.getTechCard);
fxCreateTechCard.use(techCardsApi.createTechCard);
fxUpdateTechCard.use(techCardsApi.updateTechCard);
fxDeleteTechCard.use(techCardsApi.deleteTechCard);

const errorOccured = merge([
  fxGetTechCards.fail,
  fxGetTechCard.fail,
  fxCreateTechCard.fail,
  fxUpdateTechCard.fail,
  fxDeleteTechCard.fail,
]);

$isLoading
  .on(
    pending({
      effects: [fxGetTechCard, fxCreateTechCard, fxUpdateTechCard, fxDeleteTechCard],
    }),
    (_, loading) => loading
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$isLoadingTable
  .on(fxGetTechCards.pending, (_, loadingTable) => loadingTable)
  .reset([pageUnmounted, signout]);

// запрос техкарт
sample({
  clock: [
    pageMounted,
    $tableParams,
    $settingsInitialized,
    fxCreateTechCard.done,
    fxUpdateTechCard.done,
    fxDeleteTechCard.done,
  ],
  source: { table: $tableParams, settingsInitialized: $settingsInitialized },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table }) => {
    const payload: GetTechCardsPayload = {
      page: table.page,
      per_page: table.per_page,
      search: table.search,
    };

    if (table.sorting.length > 0) {
      if (table.sorting[0]?.field === 'title') {
        payload.order_column = 'title';
      }

      if (table.sorting[0]?.field === 'plannedDuration') {
        payload.order_column = 'total_planned_duration_minutes';
      }

      payload.order_direction = table.sorting[0]?.sort || 'asc';
    }

    return payload;
  },
  target: fxGetTechCards,
});

$rawData
  .on(fxGetTechCards.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

// во время получения пользовательских данных отображается лоадер
sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});
