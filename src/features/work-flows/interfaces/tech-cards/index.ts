import { TechCard } from '../tech-card';

export interface GetTechCardsResponse {
  meta: { total: number };
  tech_cards: TechCard[];
}

export interface GetTechCardsPayload {
  search: string;
  page: number;
  per_page: number;
  order_direction?: string;
  order_column?: string;
}
