export interface Value {
  id: number;
  title: string;
}

export interface TechCard {
  id: number;
  title: string;
  total_planned_duration_minutes: number;
  qualifications: Value[];
  works: Value[];
}

export interface GetTechCardPayload {
  id: number;
}

export interface GetTechCardResponse {
  tech_card: TechCard;
}

export interface Work {
  _id: string;
  work: Value;
}

export interface Opened {
  id: number | null;
  title: string;
  works: Work[];
}

export interface CreateUpdateTechCardPayload {
  id?: number;
  title: string;
  work_ids: number[];
}

export interface DeleteTechCardPayload {
  id: number;
}
