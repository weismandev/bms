import { api } from '@api/api2';

import {
  GetTechCardsPayload,
  GetTechCardPayload,
  CreateUpdateTechCardPayload,
  DeleteTechCardPayload,
} from '../interfaces';

const getTechCards = (payload: GetTechCardsPayload) =>
  api.v4('get', 'tech-cards/list', payload);
const getTechCard = (payload: GetTechCardPayload) =>
  api.v4('get', 'tech-cards/view', payload);
const createTechCard = (payload: CreateUpdateTechCardPayload) =>
  api.v4('post', 'tech-cards/create', payload);
const updateTechCard = (payload: CreateUpdateTechCardPayload) =>
  api.v4('post', 'tech-cards/update', payload);
const deleteTechCard = (payload: DeleteTechCardPayload) =>
  api.v4('post', 'tech-cards/delete', payload);

export const techCardsApi = {
  getTechCards,
  getTechCard,
  createTechCard,
  updateTechCard,
  deleteTechCard,
};
