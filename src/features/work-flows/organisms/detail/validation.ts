import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

const { t } = i18n;

const requiredMessage = t('thisIsRequiredField');

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(requiredMessage),
  works: Yup.array().of(
    Yup.object().shape({
      work: Yup.object().required(requiredMessage).nullable(),
    })
  ),
});
