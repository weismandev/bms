import { FC, memo } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { CustomScrollbar, InputField } from '@ui/index';

import {
  detailSubmitted,
  $mode,
  changedDetailVisibility,
  changeMode,
  $opened,
  changedVisibilityDeleteModal,
} from '../../models';
import {
  StyledForm,
  StyledContent,
  StyledFormContent,
  StyledDetailToolbar,
  StyledWrapper,
} from './styled';
import { Works } from '../works';
import { Opened } from '../../interfaces';
import { validationSchema } from './validation';

export const Detail: FC = memo(() => {
  const [opened, mode] = useUnit([$opened, $mode]);
  const { t } = useTranslation();

  const isNew = !(opened as Opened).id;

  return (
    <StyledWrapper>
      <Formik
        initialValues={opened}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={detailSubmitted}
        render={({ resetForm, values, setFieldValue }) => {
          const onClose = () => changedDetailVisibility(false);

          const onCancel = () => {
            if (isNew) {
              changedDetailVisibility(false);
            } else {
              changeMode('view');
              resetForm();
            }
          };

          const onEdit = () => changeMode('edit');
          const onDelete = () => changedVisibilityDeleteModal(true);

          return (
            <StyledForm>
              <StyledDetailToolbar
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                onDelete={onDelete}
              />
              <StyledContent>
                <CustomScrollbar>
                  <StyledFormContent>
                    <Field
                      name="title"
                      label={t('NameTechnicalMap')}
                      placeholder={t('EnterNameTechnicalMap')}
                      component={InputField}
                      mode={mode}
                      required
                    />
                    <Works
                      works={(values as Opened).works}
                      setFieldValue={setFieldValue}
                    />
                  </StyledFormContent>
                </CustomScrollbar>
              </StyledContent>
            </StyledForm>
          );
        }}
      />
    </StyledWrapper>
  );
});
