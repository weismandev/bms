import styled from '@emotion/styled';
import { Form } from 'formik';

import { DetailToolbar, Wrapper } from '@ui/index';

interface StyledFormProps {
  className?: string;
  children: JSX.Element[];
}

interface DetailToolbarProps {
  className?: string;
  mode?: string;
  onEdit?: () => string;
  onClose?: () => boolean;
  onCancel?: () => void;
  onDelete?: () => boolean;
}

interface StyledWrapperProps {
  className?: string;
  children: JSX.Element;
}

export const StyledWrapper = styled(({ className, children }: StyledWrapperProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
`;

export const StyledForm = styled(({ className, children }: StyledFormProps) => (
  <Form className={className}>{children}</Form>
))`
  height: calc(100% - 54px);
  position: relative;
`;

export const StyledContent = styled.div(() => ({
  height: 'calc(100% - 32px)',
  padding: '0px 0px 24px 24px',
}));

export const StyledFormContent = styled.div(() => ({
  paddingRight: 24,
}));

export const StyledDetailToolbar = styled(
  ({ className, ...props }: DetailToolbarProps) => (
    <DetailToolbar className={className} {...props} />
  )
)`
  padding: 24px;
`;
