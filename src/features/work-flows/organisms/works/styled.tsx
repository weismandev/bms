import styled from '@emotion/styled';
import { Typography } from '@mui/material';

import { AddButton } from '../../atoms';

interface AddButtonProps {
  onClick: () => void;
  className?: string;
}

export const StyledWorkWrapper = styled.div(() => ({
  background: '#ECF6FF',
  borderRadius: 16,
  padding: 24,
}));

export const StyledTitle = styled(Typography)`
  font-weight: 500;
  font-size: 18px;
  color: #3b3b50;
`;

export const StyledWorkField = styled.div(({ mode }: { mode: string }) => ({
  display: 'grid',
  gridTemplateColumns: mode === 'edit' ? '20px 1fr 30px' : '1fr',
  gap: 30,
  marginTop: 15,
}));

export const StyledAddButton = styled((props: AddButtonProps) => (
  <AddButton {...props} />
))`
  margin-top: 20px;
`;
