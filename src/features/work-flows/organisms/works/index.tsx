import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Work } from '../../interfaces';
import { $mode } from '../../models';
import { WorksEditField, WorksViewField } from '../../molecules';
import { StyledWorkWrapper, StyledTitle } from './styled';

interface Props {
  works: Work[];
  setFieldValue: (name: string, value: any) => void;
}

export const Works: FC<Props> = ({ works, setFieldValue }) => {
  const mode = useUnit($mode);
  const { t } = useTranslation();

  return (
    <StyledWorkWrapper>
      <StyledTitle>{t('Tasks')}</StyledTitle>
      {mode === 'view' && <WorksViewField works={works} />}
      {mode === 'edit' && <WorksEditField works={works} setFieldValue={setFieldValue} />}
    </StyledWorkWrapper>
  );
};
