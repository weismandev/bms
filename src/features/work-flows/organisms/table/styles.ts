import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  tableRoot: {
    cursor: 'pointer',
  },
});
