import { memo, FC } from 'react';
import { useUnit } from 'effector-react';

import { DataGrid } from '@features/data-grid';
import { HiddenArray } from '@ui/index';

import {
  $tableData,
  $isLoadingTable,
  $selectRow,
  selectionRowChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  $sorting,
  sortChanged,
  openViaUrl,
} from '../../models';
import { CustomToolbar } from '../../molecules';
import { useStyles } from './styles';
import { StyledWrapper } from './styled';
import { Value } from '../../interfaces';

export const Table: FC = memo(() => {
  const [
    tableData,
    isLoading,
    selectRow,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
    sorting,
  ] = useUnit([
    $tableData,
    $isLoadingTable,
    $selectRow,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
    $sorting,
  ]);
  const classes = useStyles();

  const worksIndex = columns.findIndex((column) => column.field === 'works');
  const qualificationsIndex = columns.findIndex(
    (column) => column.field === 'qualifications'
  );

  columns[worksIndex].renderCell = ({ value }: { value: Value[] }) => (
    <HiddenArray>
      {value.map((item) => (
        <span key={item.id}>{item.title}</span>
      ))}
    </HiddenArray>
  );

  columns[qualificationsIndex].renderCell = ({ value }: { value: Value[] }) => (
    <HiddenArray>
      {value.map((item) => (
        <span key={item.id}>{item.title}</span>
      ))}
    </HiddenArray>
  );

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    open: openViaUrl,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <StyledWrapper>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </StyledWrapper>
  );
});
