import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Add } from '@mui/icons-material';

import { ActionButton } from '@ui/index';

interface Props {
  onClick: () => void;
}

export const AddButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionButton kind="positive" onClick={onClick} {...props}>
      <Add />
      {t('Add')}
    </ActionButton>
  );
};
