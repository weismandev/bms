import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { HighlightOff } from '@mui/icons-material';

import { IconButton } from '@ui/index';

import { styles } from './styles';

interface Props {
  onClick: () => unknown;
}

export const DeleteButton: FC<Props> = ({ onClick }) => {
  const { t } = useTranslation();

  const titleAccess = t('remove');

  return (
    <IconButton style={styles.button} onClick={onClick}>
      <HighlightOff color="error" titleAccess={titleAccess} />
    </IconButton>
  );
};
