export const styles = {
  button: {
    borderRadius: 25,
  },
  icon: {
    color: 'rgba(25, 28, 41, 0.54)',
  },
};
