import { FC } from 'react';
import { DragIndicator } from '@mui/icons-material';

import { IconButton } from '@ui/index';

import { styles } from './styles';

export const DragButton: FC = (props) => (
  <IconButton style={styles.button} {...props}>
    <DragIndicator style={styles.icon} />
  </IconButton>
);
