import { nanoid } from 'nanoid';

export const getWorkEntity = () => ({
  _id: nanoid(3),
  work: null,
});
