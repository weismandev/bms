import { Work } from '../../interfaces';

export const reorderWorks = (list: Work[], startIndex: number, endIndex: number) => {
  const newList = [...list];

  const [removed] = newList.splice(startIndex, 1);
  newList.splice(endIndex, 0, removed);

  return newList;
};
