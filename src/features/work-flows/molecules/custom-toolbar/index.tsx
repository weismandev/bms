import { FC } from 'react';
import { useUnit } from 'effector-react';

import { Greedy, AdaptiveSearch } from '@ui/index';

import { $search, searchChanged, $count, $mode, $opened, addClicked } from '../../models';
import { Opened } from '../../interfaces';
import { StyledAddButton } from './styled';

export const CustomToolbar: FC = () => {
  const [searchValue, totalCount, mode, opened] = useUnit([
    $search,
    $count,
    $mode,
    $opened,
  ]);

  const isDisabledAddClicked = mode === 'edit' && !(opened as Opened).id;

  return (
    <>
      <AdaptiveSearch
        {...{
          totalCount,
          searchValue,
          searchChanged,
          isAnySideOpen: false,
        }}
      />
      <Greedy />
      <StyledAddButton disabled={isDisabledAddClicked} onClick={addClicked} />
    </>
  );
};
