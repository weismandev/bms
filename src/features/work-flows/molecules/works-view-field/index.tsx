import { FC } from 'react';

import { StyledWork } from './styled';
import { Work } from '../../interfaces';

interface Props {
  works: Work[];
}

export const WorksViewField: FC<Props> = ({ works }) => (
  <>
    {works.map(({ work }, index) => {
      if (!work) {
        return null;
      }

      const onClick = () => window.open(`${window.location.origin}/works/${work.id}`);

      return (
        <StyledWork key={work.id} onClick={onClick}>
          {`${index + 1}. ${work.title}`}
        </StyledWork>
      );
    })}
  </>
);
