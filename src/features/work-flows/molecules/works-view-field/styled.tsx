import styled from '@emotion/styled';
import { Typography } from '@mui/material';

export const StyledWork = styled(Typography)`
  font-weight: 500;
  font-size: 13px;
  color: #65657b;
  margin-top: 20px;
  cursor: pointer;
  &:hover {
    color: #1f87e5;
  }
`;
