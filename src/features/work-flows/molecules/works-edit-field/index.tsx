import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, FieldArray } from 'formik';
import { useTranslation } from 'react-i18next';
import {
  DragDropContext,
  Droppable,
  Draggable,
  OnDragEndResponder,
} from 'react-beautiful-dnd';

import { WorkSelectField } from '@features/works-select';

import { DeleteButton, DragButton } from '../../atoms';
import { $mode } from '../../models';
import { StyledWorkField, StyledAddButton } from './styled';
import { Work } from '../../interfaces';
import { reorderWorks, getWorkEntity } from '../../libs';

interface Props {
  works: Work[];
  setFieldValue: (name: string, value: any) => void;
}

interface DragParams {
  destination: {
    droppableId: string;
    index: number;
  };
  source: {
    droppableId: string;
    index: number;
  };
}

export const WorksEditField: FC<Props> = ({ works, setFieldValue }) => {
  const mode = useUnit($mode);
  const { t } = useTranslation();

  return (
    <FieldArray
      name="works"
      render={({ push, remove }) => {
        const workCard = works.map((work, index) => {
          const onDelete = () => remove(index);

          return (
            <Draggable key={work._id} draggableId={work._id} index={index}>
              {(provided) => (
                <StyledWorkField
                  mode={mode}
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                >
                  {mode === 'edit' && <DragButton {...provided.dragHandleProps} />}
                  <Field
                    name={`works[${index}].work`}
                    label={null}
                    placeholder={t('EnterNameTechnicalMap')}
                    component={WorkSelectField}
                    mode={mode}
                    divider={false}
                  />
                  {mode === 'edit' && index !== 0 && <DeleteButton onClick={onDelete} />}
                </StyledWorkField>
              )}
            </Draggable>
          );
        });

        const onClickAdd = () => push(getWorkEntity());

        const handleDragEnd = (data: DragParams) => {
          if (!data.destination) {
            return null;
          }

          const newWorks = reorderWorks(works, data.source.index, data.destination.index);

          return setFieldValue('works', newWorks);
        };

        return (
          <>
            <DragDropContext onDragEnd={handleDragEnd as OnDragEndResponder}>
              <Droppable droppableId="droppable">
                {(provided) => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    {workCard}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
            {mode === 'edit' && <StyledAddButton onClick={onClickAdd} />}
          </>
        );
      }}
    />
  );
};
