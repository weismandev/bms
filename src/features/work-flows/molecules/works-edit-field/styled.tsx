import styled from '@emotion/styled';

import { AddButton } from '../../atoms';

interface AddButtonProps {
  onClick: () => void;
  className?: string;
}

export const StyledWorkField = styled.div(({ mode }: { mode: string }) => ({
  display: 'grid',
  gridTemplateColumns: mode === 'edit' ? '20px 1fr 30px' : '1fr',
  gap: 30,
  marginTop: 15,
}));

export const StyledAddButton = styled((props: AddButtonProps) => (
  <AddButton {...props} />
))`
  margin-top: 20px;
`;
