import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';

import { HaveSectionAccess } from '@features/common';
import {
  FilterMainDetailLayout,
  ErrorMessage,
  Loader,
  DeleteConfirmDialog,
} from '@ui/index';

import { Table, Detail } from '../organisms';
import {
  PageGate,
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  $isDetailOpen,
  $isOpenDeleteModal,
  changedVisibilityDeleteModal,
  deleteTechCard,
} from '../models';

const WorkFlowsPage: FC = () => {
  const { t } = useTranslation();
  const [error, isErrorDialogOpen, isLoading, isDetailOpen, isOpenDeleteModal] = useUnit([
    $error,
    $isErrorDialogOpen,
    $isLoading,
    $isDetailOpen,
    $isOpenDeleteModal,
  ]);

  const onCloseErrorMessage = () => changedErrorDialogVisibility(false);
  const onCloseDeleteModal = () => changedVisibilityDeleteModal(false);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={onCloseErrorMessage}
        error={error}
      />

      <DeleteConfirmDialog
        header={t('DeletingTechnicalMap')}
        content={t('ConfirmTheDeletionOfTheTechnicalMap')}
        isOpen={isOpenDeleteModal}
        close={onCloseDeleteModal}
        confirm={deleteTechCard}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        filter={null}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedWorkFlowsPage: FC = () => (
  <HaveSectionAccess>
    <WorkFlowsPage />
  </HaveSectionAccess>
);

export { RestrictedWorkFlowsPage as WorkFlowsPage };
