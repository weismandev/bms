import { FC, useState } from 'react';
import { useStore, useUnit } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import PageContext, { PageContextType } from '@features/fabrics/app/pageContext';
import { Detail } from '@features/fabrics/devices/detail';
import { $isOpenDetail } from '@features/fabrics/devices/detail/model/detail';
import { Filter } from '@features/fabrics/filter';
import { $isModalShown } from '@features/fabrics/modal/model';
import { Modal } from '@features/fabrics/modal/ui/template';
import { InstructionGate } from '@features/fabrics/processes/instructions/models/instructions';
import { Page } from '@features/fabrics/shared/types';
import { Table } from '@features/fabrics/table';
import { FilterMainDetailLayout } from '@ui/index';

const PageRoot: FC = () => {
  const pageName: Page = 'boundsactions';

  const [isFilterOpen, visibleFilter] = useState(false);
  const [isOpenDetail, isModalShown] = useUnit([$isOpenDetail, $isModalShown]);

  const providerValues: PageContextType = {
    page: pageName,
    filter: {
      isFilterOpen,
      visibleFilter,
    },
  };

  return (
    <PageContext.Provider value={providerValues}>
      <InstructionGate page={pageName} with_data={1} />

      <ColoredTextIconNotification />

      {isModalShown && <Modal />}

      <FilterMainDetailLayout
        params={{ detailWidth: 'minmax(370px, 50%)' }}
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isOpenDetail && <Detail />}
      />
    </PageContext.Provider>
  );
};

const RestrictedEventServicePage: FC = (props: any) => (
  <HaveSectionAccess>
    <PageRoot {...props} />
  </HaveSectionAccess>
);

export { RestrictedEventServicePage as EventServicePage };
