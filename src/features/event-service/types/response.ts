type Row = {
  line: any[];
  link: string;
  url_device: string;
  url_devices: string;
};

type Column = {
  name: string;
  sort: boolean;
  title: string;
  type: string;
};

export interface IInstructionsResponse {
  filter: {
    main: Object[];
  };
  top: Object[];
  meta: {
    title: string;
  };
  table: {
    count: number;
    meta: Column[];
    data: Row[];
  };
}

export interface ITableResponse {
  table: {
    count: number;
  };
  data: Row[];
}
