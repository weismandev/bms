import { useMemo, memo } from 'react';
import { useStore } from 'effector-react';
import { PagingState, CustomPaging } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  columns,
  $rowCount,
} from '../../models';

const { t } = i18n;

const Row = (props) => {
  const isSelected = false;
  const onRowClick = () => null;

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const Table = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const count = useStore($rowCount);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <DragDropProvider />

        <CustomPaging totalCount={count} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
});

export { Table };
