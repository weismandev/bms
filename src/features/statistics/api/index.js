import { api } from '../../../api/api2';

const getList = (payload) => api.v1('get', 'dashboard/stat', payload);

export const statisticApi = {
  getList,
};
