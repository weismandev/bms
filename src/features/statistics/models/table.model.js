import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'title', title: t('Name') },
  { name: 'value', title: t('Value') },
];

export const $tableData = $raw.map(({ data }) => data);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths: [{ columnName: 'title', width: 500 }] });
