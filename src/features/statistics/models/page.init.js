import { sample } from 'effector';
import { pageMounted, fxGetList, $isLoading } from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

sample({
  clock: [pageMounted, $tableParams, $settingsInitialized],
  source: {
    tableParams: $tableParams,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ tableParams }) => ({
    page: tableParams.page,
    per_page: tableParams.per_page,
  }),
  target: fxGetList,
});
