import { createPageBag } from '../../../tools/factories';
import { statisticApi } from '../api';

export const {
  PageGate,
  pageUnmounted,
  pageMounted,

  fxGetList,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(statisticApi, {}, { idAttribute: 'id', itemsAttribute: 'items' });
