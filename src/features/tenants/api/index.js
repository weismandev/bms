import { api } from '../../../api/api2';

const getTenantsList = (payload) => api.v1('get', 'tenants/get-list', payload);
const getStatuses = () => api.v1('get', 'tenants/get-statuses');
const getTenantById = (id) => api.v1('get', 'tenants/get-by-id', { id });
const createTenant = (payload) => api.v1('post', 'tenants/create', payload);
const updateTenant = (payload) => api.v1('post', 'tenants/update', payload);
const deleteTenant = (id) => api.v1('post', 'tenants/delete', { id });
const downloadList = (payload) =>
  api.v1('request', 'tenants/download-list/', payload, {
    config: {
      responseType: 'arraybuffer',
    },
  });
const getUserRents = (payload) => api.v1('get', 'rents/renters/get-user-rents', payload);
const addUserToRent = (payload) => api.v1('post', 'rents/renters/add', payload);
const removeUserFromRent = (payload) => api.v1('post', 'rents/renters/remove', payload);

export const tenantsApi = {
  getTenantsList,
  getStatuses,
  getTenantById,
  createTenant,
  updateTenant,
  deleteTenant,
  downloadList,
  getUserRents,
  addUserToRent,
  removeUserFromRent,
};
