import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  DeleteConfirmDialog,
  Loader,
  ErrorMessage,
} from '../../../ui';
import { HaveSectionAccess } from '../../common';
import {
  PageGate,
  $isDetailOpen,
  $isDeleteDialogOpen,
  changedDeleteDialogVisibility,
  deleteConfirmed,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
} from '../models';
import { $isFilterOpen } from '../models/filter.model';
import { TenantsTable, TenantDetail, Filter } from '../organisms';

const TenantsPage = () => {
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => changedDeleteDialogVisibility(false)}
        confirm={deleteConfirmed}
        content="Вы действительно хотите удалить арендатора?"
        header="Удаление арендатора"
      />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<TenantsTable />}
        detail={isDetailOpen ? <TenantDetail /> : null}
        filter={isFilterOpen ? <Filter /> : null}
      />
    </>
  );
};

const RestrictedTenantsPage = (props) => (
  <HaveSectionAccess>
    <TenantsPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedTenantsPage as TenantsPage };
