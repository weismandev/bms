import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SwitchField,
} from '../../../../ui';
import { EnterpriseSelectField } from '../../../enterprise-select';
import { PropertySelectField } from '../../../property-select';
import { TypeSelectField as RentTypeSelectField } from '../../../rent-objects';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const { t } = i18n;

const Filter = memo(() => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={(data) => {
            const { enterprise } = data.values;
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="enterprise"
                  component={EnterpriseSelectField}
                  label={t('company')}
                  placeholder={t('ChooseCompany')}
                />

                <Field
                  name="property"
                  enterprise={enterprise}
                  component={PropertySelectField}
                  label={t('Office')}
                  placeholder={t('SelectOffice')}
                />

                <Field
                  name="property_types"
                  component={RentTypeSelectField}
                  label={t('Label.typeProperty')}
                  placeholder={t('chooseType')}
                  isMulti
                />
                <Field
                  name="is_secretary"
                  component={SwitchField}
                  labelPlacement="start"
                  label={t('FunctionalityOfTheSecretary')}
                />
                <Field
                  name="isset_vehicle"
                  component={SwitchField}
                  labelPlacement="start"
                  label={t('HavingCar')}
                />
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
