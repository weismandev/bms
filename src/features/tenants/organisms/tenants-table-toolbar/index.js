import { useStore } from 'effector-react';
import {
  Toolbar,
  FoundInfo,
  AddButton,
  ExportButton,
  Greedy,
  SearchInput,
  FilterButton,
} from '../../../../ui';
import {
  $rowCount,
  exportClicked,
  $search,
  searchChanged,
  addClicked,
} from '../../models';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $tableData } from '../../models/table.model';

const TenantsTableToolbar = () => {
  const count = useStore($rowCount);
  const search = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);
  const tableData = useStore($tableData);

  return (
    <Toolbar>
      <FilterButton
        style={{ marginRight: 10 }}
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} style={{ marginRight: 10 }} />
      <SearchInput
        value={search}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <ExportButton {...{ tableData }} onClick={exportClicked} />
      {/* На беке отключен функционал добавление резидентов
       <AddButton onClick={addClicked} style={{ marginLeft: 10 }} />
       */}
    </Toolbar>
  );
};

export { TenantsTableToolbar };
