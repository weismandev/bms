import { useMemo, useCallback, memo } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  Wrapper,
  VehiclePlate,
  PagingPanel,
} from '../../../../ui';
import {
  $tableData,
  $rowCount,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  $openedTenant,
  openTenant,
  columns,
  $columnWidths,
  columnWidthsChanged,
} from '../../models';
import { TenantsTableToolbar } from '../tenants-table-toolbar';

const { t } = i18n;

const Row = (props) => {
  const openedTenant = useStore($openedTenant);

  const isSelected = openedTenant && openedTenant.id === props.row.id;
  const onRowClick = useCallback((data, id) => openTenant(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const CompaniesTitlesFormatter = ({ value }) => (
  <ul style={{ margin: '7px 0' }}>
    {value.map((title, idx) => (
      <li key={idx}>{title}</li>
    ))}
  </ul>
);

const CompaniesTitlesProvider = (props) => (
  <DataTypeProvider formatterComponent={CompaniesTitlesFormatter} {...props} />
);

const VehicleFormatter = ({ value }) => {
  const vehiclesList = value.map(({ license_plate, brand }, idx) => {
    const licensePlate =
      Boolean(license_plate) && typeof license_plate === 'string' ? license_plate : ' - ';
    const brandName = Boolean(brand) && typeof brand === 'string' ? brand : '';

    return <VehiclePlate key={idx} number={licensePlate} brand={brandName} />;
  });

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'repeat(auto-fill, 1fr)',
        gridGap: 3,
        padding: 4,
      }}
    >
      {vehiclesList}
    </div>
  );
};

const VehicleProvider = (props) => (
  <DataTypeProvider formatterComponent={VehicleFormatter} {...props} />
);

const TenantsTable = memo(() => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <CompaniesTitlesProvider for={['enterprises']} />
        <VehicleProvider for={['vehicle']} />
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={rowCount} />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TenantsTableToolbar />
        </Template>
        <PagingPanel />
      </Grid>
    </Wrapper>
  );
});

export { TenantsTable };
