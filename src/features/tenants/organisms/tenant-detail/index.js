import { useState, useEffect, memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Tab, Button } from '@mui/material';
import { CompanyRentedSelectField } from '@features/company-rented-select';
import { EnterpriseSelectField } from '@features/enterprise-select';
import { ResetAndNotifyPassword } from '@features/reset-password';
import i18n from '@shared/config/i18n';
import { getPathOr } from '@tools/path';
import { createEmailOrPhoneValidateFn } from '@tools/validations';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  AvatarControl,
  Tabs,
  FormSectionHeader,
  CustomScrollbar,
  PhoneMaskedInput,
  EmailMaskedInput,
} from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $openedTenant,
  detailSubmitted,
  $statuses,
  changedDeleteDialogVisibility,
} from '../../models';
import {
  changeTab,
  $currentTab,
  tabs,
  $userRents,
  addUserToRent,
  removeUserFromRent,
} from '../../models/detail.model';

const { t } = i18n;

const TenantDetail = memo(() => {
  const mode = useStore($mode);
  const openedTenant = useStore($openedTenant);
  const currentTab = useStore($currentTab);

  const isNew = !openedTenant.id;

  return (
    <Wrapper style={{ height: '89vh' }}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(e, tab) => changeTab(tab)}
        isNew={isNew}
        tabEl={<Tab style={{ minWidth: '50%' }} />}
      />
      {currentTab === 'info' && (
        <InfoTab mode={mode} openedTenant={openedTenant} isNew={isNew} />
      )}
      {currentTab === 'rents' && <RentTab />}
    </Wrapper>
  );
});

const testEmailOrPhoneProvided = createEmailOrPhoneValidateFn();

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email(t('incorrectEmail'))
    .test(
      'email-or-phone-mandatory',
      t('AtLeastOneOfTheFieldsEmailPhoneMustBeFilled'),
      testEmailOrPhoneProvided
    ),
  phone: Yup.string()
    .test(
      'email-or-phone-mandatory',
      t('AtLeastOneOfTheFieldsEmailPhoneMustBeFilled'),
      testEmailOrPhoneProvided
    )
    .matches(/^\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/, {
      message: t('invalidNumber'),
      excludeEmptyString: true,
    }),
});

function InfoTab(props) {
  const { isNew, openedTenant, mode } = props;

  const statuses = useStore($statuses);

  return (
    <Formik
      initialValues={openedTenant}
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      enableReinitialize
      render={(props) => {
        return (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onClose={() => changedDetailVisibility(false)}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  props.resetForm();
                }
              }}
              // На беке отключен функционал удаения резидентов
              hidden={{ delete: true }}
              onDelete={() => changedDeleteDialogVisibility(true)}
            >
              {mode === 'view' ? (
                <ResetAndNotifyPassword user_id={openedTenant.id} context="default" />
              ) : null}
            </DetailToolbar>
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <Field
                    name="avatar"
                    render={({ field, form }) => (
                      <AvatarControl
                        encoding="file"
                        mode={mode}
                        onChange={(value) => form.setFieldValue(field.name, value)}
                        value={field.value}
                      />
                    )}
                  />
                  <Field
                    name="fullname"
                    component={InputField}
                    label={t('Label.FullName')}
                    required
                    mode={mode}
                  />
                  <Field
                    name="phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        required={!form.values.email}
                        inputComponent={PhoneMaskedInput}
                        label={t('Label.phone')}
                        placeholder={t('Label.phone')}
                        mode={mode}
                      />
                    )}
                  />
                  <Field
                    name="email"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        required={!form.values.phone}
                        inputComponent={EmailMaskedInput}
                        label="Email"
                        mode={mode}
                      />
                    )}
                  />
                  <Field
                    name="enterprises"
                    component={EnterpriseSelectField}
                    label={t('company')}
                    mode={mode}
                    isMulti
                    placeholder={t('ChooseCompany')}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
}

const isNewRent = (rent) => !rent.id;
const hasLeastOneNewRent = (rents = []) => rents.filter(({ id }) => !id).length > 0;

const newRent = {
  id: null,
  property: '',
  company: '',
};

function RentTab() {
  const userRents = useStore($userRents);

  return (
    <Formik
      initialValues={{ rents: userRents }}
      enableReinitialize
      render={(formikProps) => {
        const { values } = formikProps;

        return (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <div style={{ height: '100%', padding: '0 6px 24px 24px' }}>
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <FieldArray
                    name="rents"
                    render={(helpers) => {
                      return (
                        <>
                          <div style={{ padding: 24 }}>
                            <Button
                              color="primary"
                              style={{ width: '100%' }}
                              disabled={hasLeastOneNewRent(values.rents)}
                              onClick={() => helpers.unshift(newRent)}
                            >
                              {t('CreateRent')}
                            </Button>
                          </div>
                          {values.rents.map((rent, idx) => {
                            return (
                              <div key={rent.id ? rent.id : 'new'}>
                                <RentItem
                                  isNewRent={isNewRent(rent)}
                                  id={rent.id}
                                  enterprise_id={getPathOr(
                                    `rents[${idx}].company.id`,
                                    values,
                                    null
                                  )}
                                  idx={idx}
                                  name={`rents[${idx}]`}
                                  parent_name="rents"
                                  remove={(idx) => helpers.remove(idx)}
                                  deleteRent={() => removeUserFromRent(values.rents[idx])}
                                  saveRent={() => addUserToRent(values.rents[idx])}
                                />
                              </div>
                            );
                          })}
                        </>
                      );
                    }}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
}

function RentItem(props) {
  const { name, idx, isNewRent, remove, saveRent, id, enterprise_id, parent_name } =
    props;

  const [mode, setMode] = useState(isNewRent ? 'edit' : 'view');

  useEffect(() => {
    if (id) {
      setMode('view');
    }
  }, [id]);

  return (
    <>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <FormSectionHeader
          style={{ margin: 0, width: '100%' }}
          header={`${t('rental')} ${idx + 1}`}
        />
        {isNewRent && (
          <DetailToolbar
            mode={mode}
            onCancel={() => (!isNewRent ? setMode('view') : remove(idx))}
            onSave={saveRent}
            style={{ height: 80 }}
            hidden={{
              close: true,
              edit: true,
              delete: true,
            }}
          />
        )}
      </div>
      {isNewRent && (
        <Field
          name={`${name}.company`}
          label={t('company')}
          component={EnterpriseSelectField}
          placeholder={t('ChooseCompany')}
          mode={mode}
          parent_name={parent_name}
          idx={idx}
        />
      )}
      <Field
        name={`${name}.property`}
        enterprise_id={enterprise_id}
        label={t('RentedOffice')}
        component={isNewRent ? CompanyRentedSelectField : SelectField}
        placeholder={t('ChooseProperty')}
        mode={mode}
      />
    </>
  );
}

export { TenantDetail };
