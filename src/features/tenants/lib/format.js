const isValidEnterprises = (enterprises) =>
  Array.isArray(enterprises) && enterprises.length > 0;

export { isValidEnterprises };
