import { createStore, createEvent, combine, restore } from 'effector';
import i18n from '@shared/config/i18n';
import { $rawTenantsData } from './page.model';

const { t } = i18n;

const pageNumberChanged = createEvent();
const pageSizeChanged = createEvent();
const searchChanged = createEvent();
const exportClicked = createEvent();
const columnWidthsChanged = createEvent();

const $currentPage = createStore(0);
const $pageSize = createStore(10);
const $search = createStore('');

const $tableParams = combine(
  $currentPage,
  $pageSize,
  $search,
  (page, per_page, search) => ({
    page: page + 1,
    per_page,
    search,
  })
);

const $rowCount = $rawTenantsData.map(({ meta }) => (meta && meta.total) || 0);

const $tableData = createStore([]);

const columns = [
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'sips', title: 'SIP' },
  { name: 'email', title: 'Email' },
  { name: 'enterprises', title: t('company') },
  { name: 'vehicle', title: t('Car') },
  { name: 'last_active', title: t('Label.lastActivity') },
];

const $columnWidths = restore(columnWidthsChanged, [
  { columnName: 'fullname', width: 300 },
  { columnName: 'phone', width: 200 },
  { columnName: 'sips', width: 200 },
  { columnName: 'email', width: 200 },
  { columnName: 'enterprises', width: 300 },
  { columnName: 'vehicle', width: 350 },
  { columnName: 'last_active', width: 260 },
]);

const formatTablePayload = ({ table, filters, forExport = false }) => {
  const payload = {
    filter: {},
  };

  if (!forExport) {
    payload.page = table.page;
    payload.per_page = table.per_page;
  }

  if (table.search) {
    payload.search = table.search;
  }

  if (filters.enterprise) {
    payload.filter.enterprise_id = filters.enterprise?.id;
  }

  if (filters.property) {
    payload.filter.property_id = filters.property?.id;
  }

  if (filters.property_types) {
    payload.filter.property_types =
      Array.isArray(filters.property_types) && filters.property_types.map((i) => i?.id);
  }

  if ('is_secretary' in filters) {
    payload.filter.is_secretary = Number(filters.is_secretary);
  }

  if ('isset_vehicle' in filters) {
    payload.filter.isset_vehicle = Number(filters.isset_vehicle);
  }

  return payload;
};

export {
  $currentPage,
  $pageSize,
  $rowCount,
  $search,
  $tableData,
  $tableParams,
  exportClicked,
  pageNumberChanged as pageNumChanged,
  pageSizeChanged,
  searchChanged,
  columns,
  $columnWidths,
  columnWidthsChanged,
  formatTablePayload,
};
