import { merge, forward, guard, sample } from 'effector';
import no_avatar from '../../../img/no_avatar.png';
import { formatPhone } from '../../../tools/formatPhone';
import { hasPath } from '../../../tools/path';
import { signout } from '../../common';
import { tenantsApi } from '../api';
import { isValidEnterprises } from '../lib';
import {
  changedDetailVisibility,
  changeMode,
  detailClosed,
  addClicked,
  tenantDetailApi,
  $isDetailOpen,
  $mode,
  openTenant,
  $openedTenant,
  newTenant,
  fxGetUserRents,
  $currentTab,
  $userRents,
  addUserToRent,
  removeUserFromRent,
  fxAddUserToRent,
  fxRemoveUserFromRent,
} from './detail.model';
import {
  fxDeleteTenant,
  pageUnmounted,
  fxCreateTenant,
  fxUpdateTenant,
  $normalizedData,
  $isDeleteDialogOpen,
  changedDeleteDialogVisibility,
  deleteConfirmed,
} from './page.model';

fxGetUserRents.use(tenantsApi.getUserRents);
fxAddUserToRent.use(tenantsApi.addUserToRent);
fxRemoveUserFromRent.use(tenantsApi.removeUserFromRent);

$isDetailOpen
  .on(changedDetailVisibility, (state, visibility) => visibility)
  .on(merge([openTenant, addClicked]), () => true)
  .on(fxDeleteTenant.done, () => false)
  .reset([pageUnmounted, signout]);

$userRents.on(fxGetUserRents.doneData, (state, { rents = [] }) => rents).reset(signout);

$mode
  .on(changeMode, (state, mode) => mode)
  .on(addClicked, () => 'edit')
  .on(merge([openTenant, fxCreateTenant.done, fxUpdateTenant.done]), () => 'view')
  .reset([pageUnmounted, signout]);

$openedTenant
  .on(
    sample($normalizedData, openTenant, (data, id) => data[id]),
    (state, tenant) => formatDetailTanant(tenant)
  )
  .on(merge([addClicked, detailClosed, fxDeleteTenant.done]), () => newTenant)
  .on(
    sample($normalizedData, fxCreateTenant.done, (data, { result }) => {
      if (hasPath('tenant.user.id', result)) return data[result.tenant.user.id];
      else return null;
    }),
    (state, tenant) => formatDetailTanant(tenant)
  )
  .reset([pageUnmounted, signout]);

$currentTab.reset([addClicked, signout]);

$isDeleteDialogOpen
  .on(changedDeleteDialogVisibility, (state, visibility) => visibility)
  .on(deleteConfirmed, () => false)
  .reset([pageUnmounted, signout]);

forward({
  from: sample($openedTenant, deleteConfirmed, ({ id }) => id),
  to: fxDeleteTenant,
});

forward({
  from: tenantDetailApi.save.map(prepareSubmittedPayload),
  to: fxCreateTenant,
});

forward({
  from: tenantDetailApi.update.map(prepareSubmittedPayload),
  to: fxUpdateTenant,
});

guard({
  source: { opened: $openedTenant, tab: $currentTab },
  filter: ({ opened, tab }) => Boolean(opened.id) && tab === 'rents',
  target: fxGetUserRents.prepend(({ opened }) => ({ userdata_id: opened.id })),
});

forward({
  from: sample(
    $openedTenant,
    merge([fxAddUserToRent.done, fxRemoveUserFromRent.done]),
    ({ id: userdata_id }) => ({ userdata_id })
  ),
  to: fxGetUserRents,
});

guard({
  source: sample($openedTenant, addUserToRent, ({ id }, { property }) => ({
    userdata_id: id,
    rent_id:
      property && (typeof property === 'string' || typeof property === 'number')
        ? property
        : property.id,
  })),
  filter: ({ userdata_id, rent_id }) => Boolean(userdata_id) && Boolean(rent_id),
  target: fxAddUserToRent,
});

guard({
  source: sample($openedTenant, removeUserFromRent, ({ id }, { id: rent_id }) => ({
    userdata_id: id,
    rent_id,
  })),
  filter: ({ userdata_id, rent_id }) => Boolean(userdata_id) && Boolean(rent_id),
  target: fxRemoveUserFromRent,
});

function formatDetailTanant(tenant) {
  return {
    id: tenant.id,
    fullname: tenant.fullname || '',
    phone: tenant.phone ? formatPhone(tenant.phone) : '',
    email: tenant.email || '',
    enterprises: isValidEnterprises(tenant.enterprises) ? tenant.enterprises : '',
    avatar: tenant.avatar || no_avatar,
    status: tenant.status ? tenant.status : '',
  };
}

function prepareSubmittedPayload({ status, avatar, enterprises, fullname, ...rest }) {
  const [surname, name, patronymic] = fullname.split(' ');
  return {
    ...rest,
    fullname,
    surname,
    name,
    patronymic,
    status_id: status ? status.id : '',
    enterprises: Array.isArray(enterprises) ? enterprises.map((i) => i.id) : [],
    avatar:
      avatar === no_avatar || !Object.prototype.hasOwnProperty.call(avatar, 'file')
        ? ''
        : avatar.file,
  };
}
