import { forward, sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { hasPath } from '@tools/path';
import { tenantsApi } from '../api';
import { $filters, filtersSubmitted } from './filter.model';
import {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  $rawTenantsData,
  changedDeleteDialogVisibility,
  changedErrorDialogVisibility,
  deleteConfirmed,
  fxCreateTenant,
  fxDeleteTenant,
  fxDownloadList,
  fxGetTenantById,
  fxGetTenantsList,
  fxUpdateTenant,
  pageMounted,
  pageUnmounted,
  fxGetFromStorage,
  fxSaveInStorage,
} from './page.model';
import { $tableParams, formatTablePayload } from './table.model';

fxGetTenantsList.use(tenantsApi.getTenantsList);
fxGetTenantById.use(tenantsApi.getTenantById);
fxCreateTenant.use(tenantsApi.createTenant);
fxUpdateTenant.use(tenantsApi.updateTenant);
fxDeleteTenant.use(tenantsApi.deleteTenant);
fxDownloadList.use(tenantsApi.downloadList);

const errorOccured = merge([
  fxGetTenantsList.fail,
  fxGetTenantById.fail,
  fxCreateTenant.fail,
  fxUpdateTenant.fail,
  fxDeleteTenant.fail,
  fxDownloadList.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetTenantsList,
        fxGetTenantById,
        fxCreateTenant,
        fxUpdateTenant,
        fxDeleteTenant,
        fxDownloadList,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

forward({
  from: pageMounted,
  to: fxGetFromStorage.prepend(() => ({ storage_key: 'tenants' })),
});

$rawTenantsData
  .on(fxGetTenantsList.done, (state, { result }) => result)
  .on(fxCreateTenant.done, (state, { result }) => {
    const stateCopy = { tenants: [], meta: { total: 0 }, ...state };

    return {
      tenants: [result.tenant, ...stateCopy.tenants],
      meta: { ...stateCopy.meta, total: stateCopy.meta.total + 1 },
    };
  })
  .on(fxUpdateTenant.done, (state, { result }) => {
    const stateCopy = { tenants: [], meta: { total: 0 }, ...state };

    return {
      ...stateCopy,
      tenants: stateCopy.tenants.reduce(
        (acc, tenant) =>
          acc.concat(
            (hasPath('user.id', tenant) && String(tenant.user.id)) ===
              (hasPath('tenant.user.id', result) && String(result.tenant.user.id))
              ? result.tenant
              : tenant
          ),
        []
      ),
    };
  })
  .on(fxDeleteTenant.done, (state, { params: id }) => {
    const stateCopy = { tenants: [], meta: { total: 0 }, ...state };

    return {
      ...stateCopy,
      meta: { ...stateCopy.meta, total: stateCopy.meta.total - 1 },
      tenants: stateCopy.tenants.filter(
        (tenant) => (hasPath('user.id', tenant) && tenant.user.id) !== id
      ),
    };
  })
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (state, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (state, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$isDeleteDialogOpen
  .on(changedDeleteDialogVisibility, (state, visibility) => visibility)
  .on(deleteConfirmed, () => false)
  .reset([pageUnmounted, signout]);

sample({
  clock: filtersSubmitted,
  source: $filters,
  fn: (_, clockData) => ({ data: clockData, storage_key: 'tenants' }),
  target: fxSaveInStorage,
});

sample({
  clock: [pageMounted, $tableParams, $filters],
  source: { table: $tableParams, filters: $filters },
  fn: ({ table, filters }, _) => formatTablePayload({ table, filters }),
  target: fxGetTenantsList,
});
