import { createStore, createEvent, createEffect, split, restore } from 'effector';
import i18n from '@shared/config/i18n';
import no_avatar from '../../../img/no_avatar.png';

const { t } = i18n;

const newTenant = {
  id: null,
  fullname: '',
  phone: '',
  email: '',
  enterprises: '',
  avatar: no_avatar,
  status: '',
};

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'rents',
    onCreateIsDisabled: true,
    label: t('rental'),
  },
];

const changeTab = createEvent();
const changedDetailVisibility = createEvent();
const changeMode = createEvent();
const detailSubmitted = createEvent();
const addClicked = createEvent();
const openTenant = createEvent();
const addUserToRent = createEvent();
const removeUserFromRent = createEvent();

const fxGetUserRents = createEffect();
const fxAddUserToRent = createEffect();
const fxRemoveUserFromRent = createEffect();

const $isDetailOpen = createStore(false);
const $mode = createStore('view');
const $openedTenant = createStore(newTenant);
const $currentTab = restore(changeTab, 'info');
const $userRents = createStore([]);

const detailClosed = changedDetailVisibility.filterMap((visibility) => {
  if (!visibility) return visibility;
  else return null;
});

const tenantDetailApi = split(detailSubmitted, {
  save: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

export {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  detailClosed,
  tenantDetailApi,
  addClicked,
  $isDetailOpen,
  $mode,
  openTenant,
  $openedTenant,
  newTenant,
  changeTab,
  $currentTab,
  fxGetUserRents,
  $userRents,
  addUserToRent,
  removeUserFromRent,
  fxAddUserToRent,
  fxRemoveUserFromRent,
};
