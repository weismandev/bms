import { guard } from 'effector';
import { signout } from '../../common';
import { tenantsApi } from '../api';
import { $isDetailOpen } from './detail.model';
import { pageUnmounted } from './page.model';
import { $statuses, fxGetStatuses } from './status.model';

fxGetStatuses.use(tenantsApi.getStatuses);

$statuses
  .on(fxGetStatuses.done, (state, { result }) => result.statuses)
  .reset([pageUnmounted, signout]);

guard({
  source: $isDetailOpen,
  filter: (visibility) => visibility,
  target: fxGetStatuses,
});
