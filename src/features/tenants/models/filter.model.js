import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  enterprise: '',
  property_types: '',
  property: '',
  is_secretary: false,
  isset_vehicle: false,
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
