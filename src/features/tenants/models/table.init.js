import { sample, merge } from 'effector';
import FileSaver from 'file-saver';
import i18n from '@shared/config/i18n';
import { isValidEnterprises } from '..';
import { formatPhone } from '../../../tools/formatPhone';
import { signout } from '../../common';
import { $filters } from './filter.model';
import { pageUnmounted, fxDownloadList, $rawTenantsData } from './page.model';
import {
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  searchChanged,
  $currentPage,
  $pageSize,
  $search,
  exportClicked,
  $tableData,
  $tableParams,
  formatTablePayload,
} from './table.model';

const { t } = i18n;

sample({
  source: {
    tenants: $rawTenantsData.map(({ tenants }) => tenants),
    filter: $filters.map(({ enterprise }) =>
      Boolean(enterprise) && typeof enterprise === 'object' ? enterprise.id : null
    ),
  },
  fn: ({ filter, tenants }) => formatTenantsWithFilter({ filter, tenants }),
  target: $tableData,
});

$currentPage
  .on(pageNumberChanged, (state, number) => number)
  .on(merge([pageSizeChanged, searchChanged]), () => 0)
  .reset([pageUnmounted, signout]);

$pageSize.on(pageSizeChanged, (state, qty) => qty).reset([signout, pageUnmounted]);

$search.on(searchChanged, (state, value) => value).reset([signout, pageUnmounted]);

sample({
  source: {
    filters: $filters,
    table: $tableParams,
  },
  clock: exportClicked,
  fn: ({ table, filters }, _) => formatTablePayload({ table, filters, forExport: true }),
  target: fxDownloadList,
});

fxDownloadList.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  FileSaver.saveAs(URL.createObjectURL(blob), `Tenants.xls`);
});

function formatSips(sips) {
  return sips.map((sip) => formatPhone(String(sip))).join(', ');
}

function formatTenantsWithFilter({ filter, tenants }) {
  const getEnterprisesTitlesArray = ({ enterprises, filter }) => {
    if (!filter) return enterprises.map(({ title }) => title);

    return enterprises.filter(({ id }) => id === filter).map(({ title }) => title);
  };

  return tenants.map(({ user, status, enterprises, vehicles }) => {
    const { id, fullname, last_active, phone, email } = user;

    return {
      id,
      fullname: fullname || t('thereIsNo'),
      last_active: last_active || t('Unknown'),
      phone: (phone && formatPhone(String(phone))) || t('thereIsNo'),
      sips: (enterprises[0]?.sips && formatSips(enterprises[0].sips)) || t('thereIsNo'),
      email: email || t('thereIsNo'),
      status,
      enterprises: isValidEnterprises(enterprises)
        ? getEnterprisesTitlesArray({ enterprises, filter })
        : [t('thereIsNo')],
      vehicle: Array.isArray(vehicles) ? vehicles : [],
    };
  });
}
