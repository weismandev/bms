import { createEffect, createStore } from 'effector';

const fxGetStatuses = createEffect();
const $statuses = createStore([]);

export { fxGetStatuses, $statuses };
