import { createEffect, createStore, createEvent, attach } from 'effector';
import { createGate } from 'effector-react';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const changedDeleteDialogVisibility = createEvent();
const changedErrorDialogVisibility = createEvent();
const deleteConfirmed = createEvent();

const fxGetTenantsList = createEffect();
const fxGetTenantById = createEffect();
const fxCreateTenant = createEffect();
const fxUpdateTenant = createEffect();
const fxDeleteTenant = createEffect();
const fxDownloadList = createEffect();
export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

const $rawTenantsData = createStore({});
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = createStore(false);
const $isDeleteDialogOpen = createStore(false);

const $normalizedData = $rawTenantsData.map(normalizeTenantsData);

function normalizeTenantsData({ tenants }) {
  return Array.isArray(tenants)
    ? tenants.reduce(
        (acc, tenant) => ({
          ...acc,
          [tenant.user.id]: {
            ...tenant.user,
            enterprises: tenant.enterprises,
            status: tenant.status,
          },
        }),
        {}
      )
    : {};
}

export {
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  $normalizedData,
  $rawTenantsData,
  PageGate,
  changedDeleteDialogVisibility,
  changedErrorDialogVisibility,
  deleteConfirmed,
  fxCreateTenant,
  fxDeleteTenant,
  fxDownloadList,
  fxGetTenantById,
  fxGetTenantsList,
  fxUpdateTenant,
  pageMounted,
  pageUnmounted,
};
