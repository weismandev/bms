import { $filters } from './filter.model';
import { fxGetFromStorage } from './page.model';

$filters.on(fxGetFromStorage.done, (state, { result }) => {
  return Object.entries(result).reduce((acc, [key, value]) => {
    if (value) {
      acc[key] = value;
    } else {
      acc[key] = state[key];
    }

    return acc;
  }, {});
});
