import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'buildings/get-list-crm', { ...payload, per_page: 1000000 });

export const houseApi = { getList };

// Селектор предназначен для использования в ЛК УК
