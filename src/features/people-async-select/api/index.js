import { api } from '../../../api/api2';

const search = (payload) => api.v1('get', 'crm/userdata/list', payload);

export const peopleApi = {
  search,
};
