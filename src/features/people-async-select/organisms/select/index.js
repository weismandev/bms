import { SelectControl } from '@ui/index';
import { peopleApi } from '../..';

const AsyncPeopleSelect = ({
  enterprise_id,
  object = null,
  disableCache = false,
  ...props
}) => (
  <SelectControl
    kind="async"
    cacheOptions={!disableCache}
    loadOptions={(inputValue) =>
      peopleApi
        .search(
          enterprise_id ? { q: inputValue, enterprise_id } : { q: inputValue, object }
        )
        .then((res) => res.list)
    }
    {...props}
  />
);

export { AsyncPeopleSelect };
