import { api } from '../../../api/api2';
import {
  IConnectionsResponse,
  IConnectionResponse,
  IConnectionDeleteResponse,
  IConnectionCreatePayload,
  IConnectionUpdatePayload,
} from '../interfaces/connections';
import { IProvider } from '../interfaces/providers';

const getProviders = (): IProvider[] => api.v1('get', 'ats-integration/get-providers');

const getList = (): IConnectionsResponse =>
  api.v1('get', 'ats-integration/get-available-connections');

const getById = ({ id }: { id: number }): IConnectionResponse =>
  api.v1('get', 'ats-integration/get-connection', { id });

const create = (payload: IConnectionCreatePayload): IConnectionResponse =>
  api.v1('post', 'ats-integration/create-connection', {
    ...payload,
    provider: payload.provider.id,
  });

const update = (payload: IConnectionUpdatePayload) =>
  api.v1('post', 'ats-integration/update-connection', {
    ...payload,
    provider: payload.provider.id,
  });

const deleteItem = (id: number): IConnectionDeleteResponse =>
  api.v1('post', 'ats-integration/delete-connection', { id });

export const connectionsApi = {
  getProviders,
  getList,
  getById,
  create,
  update,
  delete: deleteItem,
};
