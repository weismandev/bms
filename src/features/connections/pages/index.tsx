import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '@ui/index';
import { $isDetailOpen } from '../models/detail.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
} from '../models/page.model';
import { Detail, Table } from '../organisms';

const ConnectionsPage = () => {
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);

  return (
    <>
      <PageGate />
      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        error={error}
        onClose={() => errorDialogVisibilityChanged(false)}
      />
      <DeleteConfirmDialog
        close={() => deleteDialogVisibilityChanged(false)}
        isOpen={isDeleteDialogOpen}
        confirm={deleteConfirmed}
      />
      <FilterMainDetailLayout
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(500px, 25%)' }}
      />
    </>
  );
};

const RestrictedConnectionsPage: FC = () => (
  <HaveSectionAccess>
    <ConnectionsPage />
  </HaveSectionAccess>
);

export { RestrictedConnectionsPage as ConnectionsPage };
