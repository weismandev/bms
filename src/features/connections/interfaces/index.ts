export interface IMode {
  mode: 'view' | 'edit';
}

export interface IAtsPhones extends IMode {
  onChange: (field: string, value: string, shouldValidate?: boolean) => void;
  value: string[];
}

export interface IAtsPhoneLabel {
  index: number;
}
