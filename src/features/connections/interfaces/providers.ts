export interface IProviderSchema {
  type: string;
  order: number;
  title: string;
}

export type IProviderCredentials = Record<number, string[]>;
export interface IProvider {
  id: number;
  title: string;
  schema: Record<string, IProviderSchema>;
}

export interface IProvidersResponse {
  items: IProvider[];
}
