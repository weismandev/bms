import { IProvider } from './providers';

export interface IConnection {
  id: number;
  title: string;
  provider: Omit<IProvider, 'schema'>;
}

export interface IConnectionsResponse {
  items: IConnection[];
}

export interface IConnectionResponse extends IConnection {
  provider: IProvider;
  credentials: Record<keyof IConnectionResponse['provider'], string>;
  phones: string[];
}

export interface IConnectionDeleteResponse {
  deleted: boolean;
}

export interface IConnectionCreatePayload {
  title: string;
  provider: IProvider;
  credentials: Record<string, string>;
  phones: string[];
}

export interface IConnectionUpdatePayload extends IConnectionCreatePayload {
  id: number;
}
