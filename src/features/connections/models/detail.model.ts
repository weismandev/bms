import { createEffect, createStore } from 'effector';
import { createDetailBag } from '@tools/factories';
import { IProvider, IProviderCredentials } from '../interfaces/providers';

export const newEntity = {
  title: '',
  provider: '',
  credentials: {},
  phones: [],
};

export const $providers = createStore<IProvider[]>([]);
export const $providersCredentials = createStore<IProviderCredentials>({});

export const fxGetProvidersList = createEffect<void, IProvider[], Error>();

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
