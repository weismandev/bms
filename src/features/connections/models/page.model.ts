import { createPageBag } from '@tools/factories';
import { connectionsApi } from '../api';

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,

  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteClicked,
  deleteConfirmed,

  $raw,
  $normalized,
} = createPageBag(connectionsApi);
