import { forward, merge } from 'effector';
import { signout } from '@features/common';
import {
  entityApi,
  $opened,
  $mode,
  open,
  $isDetailOpen,
  newEntity,
  $providers,
  $providersCredentials,
  fxGetProvidersList,
} from './detail.model';
import { fxCreate, fxUpdate, fxDelete, fxGetById } from './page.model';

$opened
  .on(merge([fxCreate.done, fxUpdate.done, fxGetById.done]), (_, { result }) => result)
  .on(fxDelete.done, () => newEntity)
  .reset(signout);

$isDetailOpen.on(fxDelete.done, () => false);

$mode.on(merge([fxCreate.done, fxUpdate.done, fxDelete.done]), () => 'view');

$providers.on(fxGetProvidersList.doneData, (_, data) => data).reset(signout);

$providersCredentials.on($providers.updates, (_, payload) => {
  const credentials: Record<number, string[]> = {};
  payload.forEach(({ id, schema }) => {
    credentials[id] = Object.keys(schema);
  });
  return credentials;
});

forward({
  from: entityApi.create,
  to: fxCreate,
});

forward({
  from: entityApi.update,
  to: fxUpdate,
});

forward({
  from: open,
  to: fxGetById.prepend((id) => ({ id })),
});
