import { forward, attach } from 'effector';
import { signout } from '@features/common';
import { connectionsApi } from '../api';
import { fxGetProvidersList, $opened } from './detail.model';
import {
  pageMounted,
  fxGetList,
  fxDelete,
  $isLoading,
  deleteConfirmed,
  fxCreate,
} from './page.model';
import { $tableData } from './table.model';

fxGetProvidersList.use(connectionsApi.getProviders);

$tableData.on(fxGetList.doneData, (_, { items }) => items).reset(signout);

forward({
  from: pageMounted,
  to: [fxGetList, fxGetProvidersList],
});

forward({
  from: [fxCreate.done, fxDelete.done],
  to: fxGetList,
});

forward({
  from: fxGetProvidersList.pending,
  to: $isLoading,
});

forward({
  from: deleteConfirmed,
  to: attach({
    effect: fxDelete,
    source: $opened,
    mapParams: (_, { id }) => id,
  }),
});
