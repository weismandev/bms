import { createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { IProvider } from '../interfaces/providers';

const { t } = i18n;

export const columns = [
  { name: 'id', title: '№' },
  { name: 'title', title: t('ConnectionName') },
  {
    name: 'provider',
    title: t('provider'),
    getCellValue: (cell: { provider: IProvider }) => `${cell.provider.title}`,
  },
];

const widths = [
  { columnName: 'id', width: 100 },
  { columnName: 'title', width: 318 },
  { columnName: 'provider', width: 250 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $search,
  $tableParams,
} = createTableBag(columns, { widths, order: [] });

export const $tableData = createStore([]);
