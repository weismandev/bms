import { FC, ChangeEvent } from 'react';
import { useStore } from 'effector-react';
import { Toolbar, AddButton, Greedy, SearchInput } from '@ui/index';
import { $mode, addClicked } from '../../models/detail.model';
import { $search, searchChanged } from '../../models/table.model';

const ConnectionsToolbar: FC = () => {
  const search = useStore($search);
  const mode = useStore($mode);

  return (
    <Toolbar>
      <SearchInput
        value={search}
        onChange={(e: ChangeEvent<HTMLInputElement>) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
      />
      <Greedy />
      <AddButton onClick={addClicked} disabled={mode === 'edit'} />
    </Toolbar>
  );
};

export default ConnectionsToolbar;
