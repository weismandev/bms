//@ts-nocheck
import { useMemo, useCallback } from 'react';
import { useStore } from 'effector-react';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { TableRow } from '@ui/index';
import { $opened, open } from '../../models/detail.model';

const Row = (props: Table.DataRowProps) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );

  return row;
};

export default Row;
