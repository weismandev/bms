import { Field, FieldProps } from 'formik';
import { InputField } from '@ui/index';
import { IMode } from '../../interfaces';
import { IProviderSchema } from '../../interfaces/providers';

const Credentials = (props: FieldProps & IMode) => {
  const schema: IProviderSchema = props.form.values.provider?.schema;
  const mode = props.mode;
  return (
    <>
      {schema &&
        Object.entries(schema)
          .sort(([nameA, valueA], [nameB, valueB]) => valueA.order - valueB.order)
          .map(([name, value], index) => (
            <Field
              key={`df-${index}`}
              name={`${props.field.name}.${name}`}
              onChange={props.field.onChange}
            >
              {(inputProps: FieldProps & any) => (
                <InputField {...inputProps} label={value.title} divider mode={mode} />
              )}
            </Field>
          ))}
    </>
  );
};

export { Credentials };
