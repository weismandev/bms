import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'flex-start',
  },
  phonesWrapper: {
    marginLeft: 30,
  },
  phoneFieldWrapper: {
    position: 'relative',
  },
  labelSpan: {
    position: 'static',
    display: 'grid',
    gridTemplateColumns: '1fr auto',
    gap: 10,
    width: '100%',
    marginTop: 13,
  },
  labelSpan__title: {
    fontSize: '1rem',
    lineHeight: 1.5,
    color: '#65657B',
  },
  closeButton: {
    position: 'absolute',
    top: 23,
    right: 12,
    color: '#EB5757',
    cursor: 'pointer',
  },
  addPhoneButton: {
    alignSelf: 'flex-end',
    marginTop: 10,
  },
  label: {
    fontSize: '0.875rem',
    color: '#65657b',
  },
});

export { useStyles };
