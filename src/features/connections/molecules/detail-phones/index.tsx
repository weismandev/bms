import { FC, ChangeEvent } from 'react';
import { Field, FieldArray } from 'formik';
import { Tooltip, Button } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import i18n from '@shared/config/i18n';
import { InputField, PhoneMaskedInput } from '@ui/index';
import { IAtsPhoneLabel, IAtsPhones } from '../../interfaces';
import { useStyles } from './styles';

const { t } = i18n;

const AtsPhoneLabel: FC<IAtsPhoneLabel> = ({ index }) => {
  const classes = useStyles();

  return (
    <div className={classes.labelSpan}>
      <span className={classes.labelSpan__title}>
        {`${t('Label.phone')} ${index + 1}`}
      </span>
    </div>
  );
};

const AtsPhones: FC<IAtsPhones> = ({ mode, value = [], onChange }) => {
  const classes = useStyles();

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.name, e.target.value);
  };

  return (
    <FieldArray name="phones">
      {({ name, remove, push }) => {
        const phones = value.map((action, index) => (
          <div className={classes.phoneFieldWrapper} key={index}>
            <Field
              key={`${name}-${index}`}
              name={`${name}[${index}]`}
              className={classes.root}
              component={InputField}
              inputComponent={PhoneMaskedInput}
              placeholder={t('EnterPhone')}
              divider={false}
              value={action}
              label={<AtsPhoneLabel index={index} />}
              onChange={handleChange}
              mode={mode}
            />
            {mode === 'edit' && (
              <Tooltip title={t('DeletePhone') as string} placement="left">
                <CloseIcon
                  className={classes.closeButton}
                  onClick={() => remove(index)}
                />
              </Tooltip>
            )}
          </div>
        ));

        return (
          <>
            {value.length > 0 && <span className={classes.label}>{t('phones')}</span>}
            <div className={classes.phonesWrapper}>{phones}</div>
            {mode === 'edit' && (
              <Button
                className={classes.addPhoneButton}
                color="primary"
                onClick={() => push('')}
              >
                {t('AddPhone')}
              </Button>
            )}
          </>
        );
      }}
    </FieldArray>
  );
};

export { AtsPhones };
