import { memo, FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  CustomScrollbar,
  DetailToolbar,
  SelectField,
  InputField,
  Wrapper,
} from '@ui/index';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  $providers,
  $providersCredentials,
} from '../../models/detail.model';
import { deleteClicked } from '../../models/page.model';
import { Credentials } from '../../molecules/detail-credentials';
import { AtsPhones } from '../../molecules/detail-phones';
import { useStyles } from './styles';
import { validationSchema } from './validation';

const ConnectionsForm = memo(() => {
  const { t } = useTranslation();
  const classes = useStyles();

  const mode = useStore($mode);
  const opened = useStore($opened);
  const providers = useStore($providers);
  const credentials = useStore($providersCredentials);

  const isNew = !opened.id;

  const resetFormWithChangeMode = ({ handleReset }: { handleReset: () => void }) => {
    changeMode('view');
    handleReset();
  };

  return (
    <Formik
      initialValues={opened}
      validationSchema={validationSchema(credentials)}
      onSubmit={detailSubmitted}
      enableReinitialize
      render={({ values, setFieldValue, handleReset }) => (
        <Form className={classes.detailForm}>
          <DetailToolbar
            className={classes.detailToolbar}
            mode={mode}
            onEdit={() => changeMode('edit')}
            hidden={{ delete: isNew }}
            onDelete={deleteClicked}
            onClose={() => changedDetailVisibility(false)}
            onCancel={() =>
              isNew
                ? changedDetailVisibility(false)
                : resetFormWithChangeMode({ handleReset })
            }
          />
          <div className={classes.detailScrollWrapper}>
            <CustomScrollbar>
              <div className={classes.detailScrollInner}>
                <Field
                  name="title"
                  label={t('ConnectionName')}
                  placeholder={t('EnterTheTitle')}
                  mode={mode}
                  component={InputField}
                />
                <Field
                  name="provider"
                  label={t('provider')}
                  placeholder={t('ChooseProvider')}
                  component={SelectField}
                  options={providers}
                  mode={mode}
                  disabled={!isNew}
                />
                <Field
                  name="credentials"
                  component={Credentials}
                  onChange={setFieldValue}
                  value={values.credentials}
                  mode={mode}
                />
                <Field
                  label={t('phones')}
                  component={AtsPhones}
                  onChange={setFieldValue}
                  value={values.phones}
                  mode={mode}
                />
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
});

const ConnectionsDetail: FC = () => {
  const classes = useStyles();
  return (
    <Wrapper className={classes.detailWrapper}>
      <ConnectionsForm />
    </Wrapper>
  );
};

export { ConnectionsDetail as Detail };
