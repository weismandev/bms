import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  detailWrapper: {
    width: '100%',
    height: '100%',
  },
  detailForm: {
    height: 'inherit',
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '89vh',
    padding: '24px 12px 24px 24px',
    '& .MuiFormLabel-root': {
      fontSize: '1rem',
      margin: '10px 0',
      lineHeight: '1.5',
    },
  },
  detailToolbar: {
    paddingBottom: 26,
    paddingRight: 12,
  },
  detailScrollWrapper: {
    height: 'calc(100% - 82px)',
    padding: '0 12px 0 0px',
  },
  detailScrollInner: {
    display: 'flex',
    flexDirection: 'column',
    paddingRight: 12,
  },
});

export { useStyles };
