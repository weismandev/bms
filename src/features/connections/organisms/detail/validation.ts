import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { IProviderCredentials } from '../../interfaces/providers';

const { t } = i18n;

const validationSchema = (credentials: IProviderCredentials) => {
  const validationRules: Record<number | string, Record<string, Yup.StringSchema>> = {};

  Object.entries(credentials).forEach(([id, fields]) => {
    const providerRules: Record<string, Yup.StringSchema> = {};
    fields.forEach((fieldName) => {
      providerRules[fieldName] = Yup.string().test(
        fieldName,
        t('thisIsRequiredField'),
        (value) => Boolean(value)
      );
    });
    validationRules[id] = providerRules;
  });

  return Yup.lazy((value) =>
    Yup.object().shape({
      title: Yup.string().required(t('thisIsRequiredField')).max(255),
      provider: Yup.object()
        .required(t('YouMustSelectProvider'))
        .typeError(t('YouMustSelectProvider')),
      credentials: Yup.object().shape({
        ...validationRules[value?.provider?.id],
      }),
      phones: Yup.array().of(
        Yup.string()
          .matches(/\+?\d\s?\(?\d{3}\)?\s?\d{3}-?\d{2}-?\d{2}/, t('invalidNumber'))
          .required(t('thisIsRequiredField'))
      ),
    })
  );
};

export { validationSchema };
