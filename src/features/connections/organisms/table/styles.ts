import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    '& td': {
      paddingLeft: '24px !important',
    },
  },
  gridRoot: {
    height: '87vh',
    padding: 24,
  },
  toolbarRoot: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 'auto',
    borderBottom: 'none !important',
    padding: '0 10px',
    zIndex: 5,
  },
});

export { useStyles };
