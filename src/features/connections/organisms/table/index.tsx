import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  IntegratedFiltering,
  IntegratedSorting,
  SearchState,
  SortingState,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import {
  SortingLabel,
  TableHeaderCell,
  TableContainer,
  TableCell,
  Wrapper,
} from '@ui/index';
import {
  $tableData,
  columns,
  sortChanged,
  columnOrderChanged,
  columnWidthsChanged,
  $sorting,
  $search,
  $columnOrder,
  $columnWidths,
} from '../../models/table.model';
import Row from '../../molecules/table-row';
import TableToolbar from '../../molecules/table-toolbar';
import { useStyles } from './styles';

const Root: FC<Grid.RootProps> = ({ children, ...props }) => {
  const classes = useStyles();
  return (
    <Grid.Root {...props} className={classes.gridRoot}>
      {children}
    </Grid.Root>
  );
};

const ToolbarRoot: FC = (props) => {
  const classes = useStyles();
  return <Toolbar.Root {...props} className={classes.toolbarRoot} />;
};

const ConnectionsTable: FC = () => {
  const { t } = useTranslation();
  const classes = useStyles();

  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const sortValue = useStore($sorting);
  const searchValue = useStore($search);

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row?.id}
        columns={columns}
      >
        <SortingState sorting={sortValue} onSortingChange={sortChanged} />
        <IntegratedSorting />
        <SearchState value={searchValue} />
        <IntegratedFiltering />
        <DragDropProvider />
        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableHeaderRow
          cellComponent={TableHeaderCell}
          sortLabelComponent={SortingLabel}
          showSortingControls
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
      </Grid>
    </Wrapper>
  );
};

export { ConnectionsTable as Table };
