import { GridSortModel } from '@mui/x-data-grid-pro';

export interface Columns {
  field: string;
  headerName: string;
  width?: number;
  sortable?: boolean;
  headerClassName?: string;
  renderCell?: (data: any) => any;
  hideable?: boolean;
}

export interface Config {
  columns: Columns[];
  search?: string;
  currentPage?: number;
  pageSize?: number;
  sorting?: GridSortModel;
  visibilityColumns?: { [key: string]: boolean };
  selectRow?: number[];
  columnsWidth?: { [key: string]: number };
  orderColumn?: {
    [key: string]: {
      oldIndex: number;
      targetIndex: number;
    };
  };
  pinnedColumns?: PinnedColumns;
}

export interface Column {
  colDef: {
    field: string;
    width: number;
  };
  oldIndex: number;
  targetIndex: number;
}

export interface PinnedColumns {
  left: string[];
  right: string[];
}
