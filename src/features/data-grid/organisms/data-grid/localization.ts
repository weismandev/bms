import i18n from '@shared/config/i18n';

const { t } = i18n;

export const localeText = {
  noRowsLabel: t('DataGrid.noRowsLabel'),
  noResultsOverlayLabel: t('DataGrid.noResultsOverlayLabel'),
  errorOverlayDefaultLabel: t('DataGrid.errorOverlayDefaultLabel'),
  toolbarDensity: t('DataGrid.toolbarDensity'),
  toolbarDensityLabel: t('DataGrid.toolbarDensityLabel'),
  toolbarDensityCompact: t('DataGrid.toolbarDensityCompact'),
  toolbarDensityStandard: t('DataGrid.toolbarDensityStandard'),
  toolbarDensityComfortable: t('DataGrid.toolbarDensityComfortable'),
  toolbarColumns: t('DataGrid.toolbarColumns'),
  toolbarColumnsLabel: t('DataGrid.toolbarColumnsLabel'),
  toolbarFilters: t('DataGrid.toolbarFilters'),
  toolbarFiltersLabel: t('DataGrid.toolbarFiltersLabel'),
  toolbarFiltersTooltipHide: t('DataGrid.toolbarFiltersTooltipHide'),
  toolbarFiltersTooltipShow: t('DataGrid.toolbarFiltersTooltipShow'),
  toolbarFiltersTooltipActive: (count: number) => {
    let pluralForm = t('DataGrid.tooltipActiveThey');
    const lastDigit = count % 10;

    if (lastDigit > 1 && lastDigit < 5) {
      pluralForm = t('DataGrid.tooltipActiveNo');
    } else if (lastDigit === 1) {
      pluralForm = t('DataGrid.tooltipActiveHe');
    }

    return `${count} ${pluralForm}`;
  },
  toolbarQuickFilterPlaceholder: t('DataGrid.toolbarQuickFilterPlaceholder'),
  toolbarQuickFilterLabel: t('DataGrid.toolbarQuickFilterLabel'),
  toolbarQuickFilterDeleteIconLabel: t('DataGrid.toolbarQuickFilterDeleteIconLabel'),
  toolbarExport: t('DataGrid.toolbarExport'),
  toolbarExportLabel: t('DataGrid.toolbarExportLabel'),
  toolbarExportCSV: t('DataGrid.toolbarExportCSV'),
  toolbarExportPrint: t('DataGrid.toolbarExportPrint'),
  toolbarExportExcel: t('DataGrid.toolbarExportExcel'),
  columnsPanelTextFieldLabel: t('DataGrid.columnsPanelTextFieldLabel'),
  columnsPanelTextFieldPlaceholder: t('DataGrid.columnsPanelTextFieldPlaceholder'),
  columnsPanelDragIconLabel: t('DataGrid.columnsPanelDragIconLabel'),
  columnsPanelShowAllButton: t('DataGrid.columnsPanelShowAllButton'),
  columnsPanelHideAllButton: t('DataGrid.columnsPanelHideAllButton'),
  filterPanelAddFilter: t('DataGrid.filterPanelAddFilter'),
  filterPanelDeleteIconLabel: t('DataGrid.filterPanelDeleteIconLabel'),
  filterPanelOperators: t('DataGrid.filterPanelOperators'),
  filterPanelOperatorAnd: t('DataGrid.filterPanelOperatorAnd'),
  filterPanelOperatorOr: t('DataGrid.filterPanelOperatorOr'),
  filterPanelColumns: t('DataGrid.filterPanelColumns'),
  filterPanelInputLabel: t('DataGrid.filterPanelInputLabel'),
  filterPanelInputPlaceholder: t('DataGrid.filterPanelInputPlaceholder'),
  filterOperatorContains: t('DataGrid.filterOperatorContains'),
  filterOperatorEquals: t('DataGrid.filterOperatorEquals'),
  filterOperatorStartsWith: t('DataGrid.filterOperatorStartsWith'),
  filterOperatorEndsWith: t('DataGrid.filterOperatorEndsWith'),
  filterOperatorIs: t('DataGrid.filterOperatorIs'),
  filterOperatorNot: t('DataGrid.filterOperatorNot'),
  filterOperatorAfter: t('DataGrid.filterOperatorAfter'),
  filterOperatorOnOrAfter: t('DataGrid.filterOperatorOnOrAfter'),
  filterOperatorBefore: t('DataGrid.filterOperatorBefore'),
  filterOperatorOnOrBefore: t('DataGrid.filterOperatorOnOrBefore'),
  filterOperatorIsEmpty: t('DataGrid.filterOperatorIsEmpty'),
  filterOperatorIsNotEmpty: t('DataGrid.filterOperatorIsNotEmpty'),
  filterOperatorIsAnyOf: t('DataGrid.filterOperatorIsAnyOf'),
  filterValueAny: t('DataGrid.filterValueAny'),
  filterValueTrue: t('DataGrid.filterValueTrue'),
  filterValueFalse: t('DataGrid.filterValueFalse'),
  columnMenuLabel: t('DataGrid.columnMenuLabel'),
  columnMenuShowColumns: t('DataGrid.columnMenuShowColumns'),
  columnMenuFilter: t('DataGrid.columnMenuFilter'),
  columnMenuHideColumn: t('DataGrid.columnMenuHideColumn'),
  columnMenuUnsort: t('DataGrid.columnMenuUnsort'),
  columnMenuSortAsc: t('DataGrid.columnMenuSortAsc'),
  columnMenuSortDesc: t('DataGrid.columnMenuSortDesc'),
  columnHeaderFiltersTooltipActive: (count: number) => {
    let pluralForm = t('DataGrid.tooltipActiveThey');
    const lastDigit = count % 10;

    if (lastDigit > 1 && lastDigit < 5) {
      pluralForm = t('DataGrid.tooltipActiveNo');
    } else if (lastDigit === 1) {
      pluralForm = t('tooltipActiveHe');
    }

    return `${count} ${pluralForm}`;
  },
  columnHeaderFiltersLabel: t('DataGrid.columnHeaderFiltersLabel'),
  columnHeaderSortIconLabel: t('DataGrid.columnHeaderSortIconLabel'),
  footerRowSelected: (count: number) => {
    let pluralForm = t('DataGrid.footerRowSelectedThey');
    const lastDigit = count % 10;

    if (lastDigit > 1 && lastDigit < 5) {
      pluralForm = t('DataGrid.footerRowSelectedAll');
    } else if (lastDigit === 1) {
      pluralForm = t('DataGrid.footerRowSelectedOne');
    }

    return `${count} ${pluralForm}`;
  },
  footerTotalRows: t('DataGrid.footerTotalRows'),
  footerTotalVisibleRows: (visibleCount: number, totalCount: number) =>
    `${visibleCount.toLocaleString()} ${t('from')} ${totalCount.toLocaleString()}`,
  checkboxSelectionHeaderName: t('DataGrid.checkboxSelectionHeaderName'),
  booleanCellTrueLabel: t('DataGrid.booleanCellTrueLabel'),
  booleanCellFalseLabel: t('DataGrid.booleanCellFalseLabel'),
  actionsCellMore: t('DataGrid.actionsCellMore'),
  pinToLeft: t('DataGrid.pinToLeft'),
  pinToRight: t('DataGrid.pinToRight'),
  unpin: t('DataGrid.unpin'),
  treeDataGroupingHeaderName: t('DataGrid.treeDataGroupingHeaderName'),
  treeDataExpand: t('DataGrid.treeDataExpand'),
  treeDataCollapse: t('DataGrid.treeDataCollapse'),
  groupingColumnHeaderName: t('DataGrid.groupingColumnHeaderName'),
  groupColumn: (name: string) => `${t('DataGrid.groupColumn')} ${name}`,
  unGroupColumn: (name: string) => `${t('DataGrid.unGroupColumn')} ${name}`,
  expandDetailPanel: t('DataGrid.expandDetailPanel'),
  collapseDetailPanel: t('DataGrid.collapseDetailPanel'),
};
