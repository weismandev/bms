import { FC } from 'react';
import { DataGridPro as DataGridMui, LicenseInfo } from '@mui/x-data-grid-pro';

import { localeText } from './localization';

LicenseInfo.setLicenseKey(process.env.X_DATA_GRID_PRO_LICENSE as string);

export const DataGrid: FC<any> = ({
  rows,
  columns,
  disableVirtualization = true,
  ...rest
}) => (
  <DataGridMui
    localeText={localeText}
    rows={rows}
    columns={columns}
    disableVirtualization={disableVirtualization}
    {...rest}
  />
);
