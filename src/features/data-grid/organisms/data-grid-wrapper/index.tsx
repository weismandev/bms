import { FC, useState } from 'react';
import { Event } from 'effector';
import { GridSortModel } from '@mui/x-data-grid-pro';
import { Column, Columns, PinnedColumns } from '../../interfaces';
import { TableToolbarWrapper, TableLoader, TablePaginaton } from '../../molecules';
import { DataGrid } from '../data-grid';

export interface VisibilityColumn {
  [key: string]: boolean;
}

export interface Props {
  params: {
    classes?: { [key: string]: string };
    columns: Columns[];
    isLoading: boolean;
    tableData: any[];
    initialState?: {
      sorting?: {
        sortModel: GridSortModel;
      };
      columns?: {
        columnVisibilityModel?: VisibilityColumn;
      };
    };
    visibilityColumns: VisibilityColumn;
    visibilityColumnsChanged?: Event<VisibilityColumn>;
    open?: Event<number>;
    selectionRowChanged?: Event<number[]>;
    selectRow?: number[] | string[];
    sortChanged?: Event<GridSortModel>;
    columnsWidthChanged?: Event<Column>;
    orderColumnsChanged?: Event<Column>;
    pageSize?: number;
    currentPage?: number;
    countPage?: number;
    pageNumberChanged?: Event<number>;
    pageSizeChanged?: Event<number>;
    openRow?: string | number | null;
    openRowChanged?: Event<string | number | null>;
    pinnedColumns?: PinnedColumns;
    changePinnedColumns?: Event<PinnedColumns>;
  };
  toolbar?: JSX.Element;
  checkboxSelection?: boolean;
  disableRowSelectionOnClick?: boolean;
  getRowClassName?: any;
  treeData?: boolean;
  getTreeDataPath?: any;
  sortingMode?: 'server' | 'client';
  groupingColDef?: any;
  hidePagination?: boolean;
  hideFooter?: boolean;
  isCustomToolbar?: boolean;
  disableColumnMenu?: boolean;
}

export const DataGridWrapper: FC<Props> = ({
  params,
  toolbar,
  checkboxSelection,
  disableRowSelectionOnClick,
  getRowClassName,
  treeData = false,
  getTreeDataPath,
  sortingMode = 'server',
  groupingColDef,
  hidePagination = false,
  hideFooter = false,
  isCustomToolbar = false,
  disableColumnMenu = true,
}) => {
  const [columnsButtonEl, setColumnsButtonEl] = useState(null);

  const {
    isLoading,
    tableData,
    visibilityColumnsChanged,
    open,
    selectionRowChanged,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    classes,
    columns,
    initialState,
    selectRow,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
    openRow,
    openRowChanged,
    visibilityColumns,
    changePinnedColumns,
    pinnedColumns,
  } = params;

  const data = isLoading ? [] : tableData;
  const panelProps = isCustomToolbar
    ? {}
    : { anchorEl: columnsButtonEl, placement: 'bottom-end' };

  const handleVisibility = (column: VisibilityColumn) => {
    if (visibilityColumnsChanged) {
      visibilityColumnsChanged(column);
    }
  };

  const handleOnCellClick = ({ id, field }: { id: number; field: string }) => {
    if (checkboxSelection && field === '__check__') {
      return null;
    }

    if (open) {
      return open(id);
    }

    return null;
  };

  const handleSelection = (row: number[]) => {
    if (selectionRowChanged && checkboxSelection) {
      selectionRowChanged(row ?? []);
    }

    if (selectionRowChanged && !checkboxSelection && row.length > 0) {
      selectionRowChanged(row);
    }
  };

  const handleSorting = (sort: GridSortModel) => {
    if (sortChanged) {
      sortChanged(sort);
    }
  };

  const handleColumnWidth = (column: Column) => {
    if (columnsWidthChanged) {
      columnsWidthChanged(column);
    }
  };

  const handleColumnOrder = (column: Column) => {
    if (orderColumnsChanged) {
      if (checkboxSelection) {
        column.oldIndex--;
        column.targetIndex--;
      }

      orderColumnsChanged(column);
    }
  };

  const handleRowClick = ({ id }: { id: number | string }) => {
    openRowChanged?.(id);
  };

  const handlePinnedColumn = (pinnedColumn: PinnedColumns) => {
    if (changePinnedColumns) {
      changePinnedColumns(pinnedColumn);
    }
  };

  return (
    <DataGrid
      classes={classes}
      rows={data || []}
      columns={columns || []}
      initialState={initialState || {}}
      columnVisibilityModel={visibilityColumns ?? {}}
      onColumnVisibilityModelChange={handleVisibility}
      onCellClick={handleOnCellClick}
      loading={isLoading || false}
      onRowSelectionModelChange={handleSelection}
      rowSelectionModel={selectRow}
      onSortModelChange={handleSorting}
      onColumnWidthChange={handleColumnWidth}
      onColumnOrderChange={handleColumnOrder}
      pagination={!hidePagination}
      disableColumnMenu={disableColumnMenu}
      disableMultipleSelection
      hideFooterSelectedRowCount
      density="compact"
      sortingMode={sortingMode}
      hideFooter={hideFooter}
      componentsProps={{
        pagination: {
          pageSize,
          currentPage,
          countPage,
          pageNumberChanged,
          pageSizeChanged,
        },
        panel: panelProps,
        toolbar: {
          setColumnsButtonEl,
          toolbar,
          isCustomToolbar,
        },
      }}
      components={{
        Toolbar: TableToolbarWrapper,
        Pagination: TablePaginaton,
        LoadingOverlay: TableLoader,
      }}
      onRowClick={handleRowClick}
      checkboxSelection={checkboxSelection}
      disableRowSelectionOnClick={disableRowSelectionOnClick}
      getRowClassName={getRowClassName}
      treeData={treeData}
      getTreeDataPath={getTreeDataPath}
      groupingColDef={groupingColDef}
      onPinnedColumnsChange={handlePinnedColumn}
      pinnedColumns={pinnedColumns}
    />
  );
};
