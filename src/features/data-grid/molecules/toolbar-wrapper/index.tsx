import { FC, Ref } from 'react';
import { Toolbar, Greedy } from '@ui/index';
import { DataGridColumnsButton } from '../../atoms';

interface ToolbarProps {
  toolbar?: JSX.Element;
  setColumnsButtonEl: Ref<HTMLButtonElement>;
  isCustomToolbar: boolean;
}

export const TableToolbarWrapper: FC<ToolbarProps> = ({
  toolbar,
  setColumnsButtonEl,
  isCustomToolbar,
}) => {
  if (isCustomToolbar) {
    return toolbar;
  }

  if (!toolbar) {
    return (
      <Toolbar>
        <Greedy />
        <DataGridColumnsButton buttonRef={setColumnsButtonEl} />
      </Toolbar>
    );
  }

  return (
    <Toolbar>
      {toolbar}
      <DataGridColumnsButton buttonRef={setColumnsButtonEl} />
    </Toolbar>
  );
};
