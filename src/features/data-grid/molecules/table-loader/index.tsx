import { FC } from 'react';
import { Box, Skeleton } from '@mui/material';

export const TableLoader: FC = () => (
  <Box
    sx={{
      height: 'max-content',
    }}
  >
    {[...Array(17)].map((_, index) => (
      <Skeleton key={index} variant="rectangular" sx={{ my: 1, mx: 1 }} />
    ))}
  </Box>
);
