import { FC } from 'react';
import { Pagination, PaginationItem } from '@mui/material';
import { ColumnSelection } from '../column-selection';
import { useStyles } from './styles';

interface Props {
  pageSize: number;
  currentPage: number;
  countPage: number;
  pageNumberChanged: (page: number) => void;
  pageSizeChanged: () => void;
}

export const TablePaginaton: FC<Props> = ({
  pageSize = 10,
  currentPage = 0,
  countPage = 0,
  pageNumberChanged,
  pageSizeChanged,
}) => {
  const classes = useStyles();

  const onChangePagination = (_: unknown, page: number) => {
    if (pageNumberChanged) {
      pageNumberChanged(page - 1);
    }
  };

  return (
    <div className={classes.paginationWrap}>
      <ColumnSelection pageSize={pageSize} pageChanged={pageSizeChanged} />
      <Pagination
        onChange={onChangePagination}
        page={currentPage + 1}
        count={countPage}
        classes={{ root: classes.pagination }}
        size="small"
        shape="rounded"
        renderItem={(item) => <PaginationItem {...item} />}
      />
    </div>
  );
};
