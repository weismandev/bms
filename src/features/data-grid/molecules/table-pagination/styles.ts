import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  paginationWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'relative',
    width: 'calc(100% + 48px)',
    bottom: '-24px',
    zIndex: 2,
    padding: '12px 0px 0px 0px',
    top: 0,
    '& > div': {
      width: '50%',
    },
  },
  pagination: {
    display: 'flex',
    flexWrap: 'nowrap',
    paddingLeft: '26px',
    minHeight: '10px',
    '& > ul': {
      '& button': {
        width: '20px',
        height: '36px',
        fontWeight: 600,
      },
    },
  },
}));
