import { DataGrid as DataGridMini, DataGridWrapper } from './organisms';

export type { Columns, Column } from './interfaces';
export { createTableBag } from './models';

export const DataGrid = {
  Mini: DataGridMini,
  Full: DataGridWrapper,
};
