import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  columnsButton: {
    paddingLeft: 15,
    borderRadius: '100%',
    minWidth: 43,
    height: 49,
    color: 'rgba(0, 0, 0, 0.54)',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.04)',
    },
  },
});

export const styles = {
  icon: {
    color: 'rgba(0, 0, 0, 0.54)',
    width: 25,
    height: 25,
  },
};
