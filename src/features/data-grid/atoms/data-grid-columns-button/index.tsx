import { FC, Ref } from 'react';
import { useTranslation } from 'react-i18next';
import { Tooltip } from '@mui/material';
import { VisibilityOff } from '@mui/icons-material';
import { GridToolbarColumnsButton } from '@mui/x-data-grid-pro';
import { useStyles, styles } from './styles';

interface Props {
  buttonRef?: Ref<HTMLButtonElement>;
}

export const DataGridColumnsButton: FC<Props> = ({ buttonRef }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <Tooltip title={t('showColumnSelection')}>
      <div>
        <GridToolbarColumnsButton
          className={classes.columnsButton}
          startIcon={<VisibilityOff style={styles.icon} />}
          ref={buttonRef}
          variant="text"
        />
      </div>
    </Tooltip>
  );
};
