import { createStore, createEvent, combine, restore } from 'effector';
import { throttle } from 'patronum/throttle';
import { GridSortModel } from '@mui/x-data-grid-pro';
import { signout } from '@features/common';
import { Columns, Config, Column, PinnedColumns } from '../../interfaces';

/*
  Можно сохранять количество строк в таблице ($pageSize), ширину и порядок столбцов ($columns),
  видимость столбцов ($visibilityColumns). Пример реализации сохранения можно посмотреть в фиче polls. Файлы:
    1) модель сохранения /features/polls/models/user-settings/user-settings.model,
    2) обработка полученных значений с бэка/features/polls/models/table/table.init,
    3) запрос сохраненных данных /features/polls/models/page/page.init
*/

const getConfig = (config: Config) => ({
  search: '',
  currentPage: 0,
  pageSize: 10,
  sorting: [],
  visibilityColumns: {},
  selectRow: [],
  pinnedColumns: { left: [], right: [] },
  openRow: null,
  ...config,
});

export const createTableBag = (customConfig: Config) => {
  const config = getConfig(customConfig);

  const pageNumberChanged = createEvent<number>();
  const pageSizeChanged = createEvent<number>();
  const sortChanged = createEvent<GridSortModel>();
  const searchChanged = createEvent<string>();
  const selectionRowChanged = createEvent<number[]>();
  const visibilityColumnsChanged = createEvent<{ [key: string]: boolean }>();
  const columnsWidthChanged = createEvent<Column>();
  const orderColumnsChanged = createEvent<Column>();
  const openRowChanged = createEvent<number | string | null>();
  const changePinnedColumns = createEvent<PinnedColumns>();

  const throttledSearch = throttle({
    source: searchChanged,
    timeout: 1000,
  });

  const $currentPage = createStore<number>(config.currentPage);
  const $pageSize = createStore<number>(config.pageSize);
  const $sorting = createStore<GridSortModel>(config.sorting);
  const $search = createStore<string>(config.search);
  const $columns = createStore<Columns[]>(config.columns);
  const $selectRow = createStore<number[] | string[]>(config.selectRow);
  const $openRow = createStore<number | string | null>(config.openRow);
  const $pinnedColumns = createStore<PinnedColumns>(config.pinnedColumns);

  /*
  $selectRow хранит какая строка в таблице выбрана. В каждой фиче, где используется DataGrid:
    1) в модели дополнительно сбрасывать $selectRow при срабатывании ивентов detailClosed, addClicked,
    если используется фабрика деталей.
    2) после успешного выполнения запроса на создание сущности (fxCreate.done), из result взять id и передать
    в стор $selectRow в модели фичи (нужно чтобы после создания сущности в таблице отображалась выбранная строка)

    Пример можно посмотреть в /features/polls/models/table/table.init
  */
  const $visibilityColumns = createStore<{ [key: string]: boolean }>(
    config.visibilityColumns
  );
  /*
    $visibilityColumns представляет из себя объект. Структура объекта: название колонки и
    значение видимости. Если нет необходимости сохранять скрытые столбцы при переходе между разделами или
    сохранять на бэке, то можно не использовать

    Пример можно посмотреть в /features/polls/organisms/table/index.tsx
  */
  const $tableParams = combine(
    $currentPage,
    $pageSize,
    $sorting,
    restore(throttledSearch, ''),
    (page, per_page, sorting, search) => ({
      page: page + 1,
      per_page,
      sorting: sorting.map((item) => ({ field: item.field, sort: item.sort })),
      search,
    })
  );

  $currentPage
    .on(pageNumberChanged, (_, number) => number)
    .reset([signout, searchChanged, pageSizeChanged]);

  $pageSize.on(pageSizeChanged, (_, size) => size).reset([signout]);
  $sorting.on(sortChanged, (_, sort) => sort).reset(signout);
  $search.on(searchChanged, (_, search) => search).reset(signout);

  $openRow.on(openRowChanged, (_, row) => row).reset(signout);

  $selectRow.on(selectionRowChanged, (_, row) => row).reset(signout);

  $visibilityColumns.on(visibilityColumnsChanged, (_, column) => column).reset(signout);

  $columns
    .on(columnsWidthChanged, (state, columnsWidth) =>
      state.map((item) =>
        item.field === columnsWidth.colDef.field
          ? { ...item, width: columnsWidth.colDef.width || item.width }
          : item
      )
    )
    .on(orderColumnsChanged, (state, data) => {
      const newState = [...state];

      [newState[data.oldIndex], newState[data.targetIndex]] = [
        newState[data.targetIndex],
        newState[data.oldIndex],
      ];

      return newState;
    })
    .reset(signout);

  $pinnedColumns.on(changePinnedColumns, (_, columns) => columns).reset(signout);

  return {
    pageNumberChanged,
    pageSizeChanged,
    sortChanged,
    searchChanged,
    $currentPage,
    $pageSize,
    $sorting,
    $search,
    $tableParams,
    $selectRow,
    selectionRowChanged,
    $visibilityColumns,
    visibilityColumnsChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    $columns,
    $openRow,
    openRowChanged,
    $pinnedColumns,
    changePinnedColumns,
  };
};
