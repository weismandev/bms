import { useCallback, useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const RizerSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const { buildings } = props;

  const gateTitle = props?.title || 'Калитка';
  const optionsWithGate = options.map((rizer) => {
    if (Number(rizer.id) === 0) {
      return { id: 0, title: gateTitle };
    }
    return rizer;
  });

  const building_id = typeof buildings[0] === 'object' ? buildings[0].id : buildings[0];

  const fetchListOfRizers = useCallback(() => {
    fxGetList({
      building_id,
    });
  }, [building_id]);

  useEffect(() => {
    if (Array.isArray(buildings) && buildings.length === 1) {
      fetchListOfRizers();
    }
  }, [fetchListOfRizers]);

  return (
    <SelectControl
      options={
        Array.isArray(buildings) && buildings.length > 1
          ? [{ id: '0', title: gateTitle }]
          : Array.isArray(buildings) && buildings.length === 0
          ? []
          : optionsWithGate
      }
      isLoading={isLoading}
      error={error}
      {...props}
    />
  );
};

const RizerSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RizerSelect />} onChange={onChange} {...props} />;
};

export { RizerSelect, RizerSelectField };
