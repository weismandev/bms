import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.no_vers('get', 'admin/get-rizer', { ...payload, per_page: 1000 });

export const rizerApi = { getList };
