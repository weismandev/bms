import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { rizerApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.rizers) {
      return [];
    }

    return result.rizers.map((rizerNumber) => {
      if (String(rizerNumber) === '0') {
        return { id: '0', title: `Калитка` };
      }
      return { id: rizerNumber, title: `Подъезд №${rizerNumber}` };
    });
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(rizerApi.getList);

export { fxGetList, $data, $error, $isLoading };
