import { useStore } from 'effector-react';

import { HaveSectionAccess } from '../../common';

import { ErrorMessage, FilterMainDetailLayout, Loader } from '../../../ui';

import { Detail, Filter, Table } from '../organisms';

import '../models';

import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  errorDialogVisibilityChanged,
  PageGate,
} from '../models/page.model';

import { $isFilterOpen } from '../models/filter.model';
import { $isDetailOpen } from '../models/detail.model';

const AlarmsPage = (props) => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />
      <Loader isLoading={isLoading} />
      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedAlarmsPage = (props) => {
  return (
    <HaveSectionAccess>
      <AlarmsPage {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedAlarmsPage as AlarmsPage };
