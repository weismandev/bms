import { forward, sample } from 'effector';
import { load as loadAlarmTypes } from '../../alarms-type-select';
import { receivedNotificationAbout, NEW_ALARM } from '../../common';
import { alarmsApi } from '../api';
import { $filters } from './filter.model';
import {
  pageMounted,
  fxGetList,
  fxGetHistory,
  fxUpdateCrashState,
  $raw,
  $isLoading,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxGetHistory.use(alarmsApi.getHistory);
fxUpdateCrashState.use(alarmsApi.updateCrashState);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$raw.on(fxUpdateCrashState.done, (state, { params: id }) => {
  const { meta = {}, data = [] } = state;

  const updatedIdx = data.findIndex((i) => String(i.id) === String(id));

  if (updatedIdx !== -1) {
    return {
      meta: { ...meta },
      data: data
        .slice(0, updatedIdx)
        .concat({ ...data[updatedIdx], state: 0 })
        .concat(data.slice(updatedIdx + 1)),
    };
  }

  return state;
});

forward({
  from: pageMounted,
  to: loadAlarmTypes,
});

sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    receivedNotificationAbout[NEW_ALARM],
    $settingsInitialized,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }) => {
    const payload = {
      limit: table.per_page,
      offset: (table.page - 1) * table.per_page,
    };

    if (filters.new) {
      payload.new = 1;
    }

    if (filters.type) {
      payload.type = filters.type.id;
    }

    if (filters.state && filters.state.id > -1) {
      payload.state = filters.state.id;
    }

    if (filters.date_start) {
      payload.date_start = new Date(filters.date_start) / 1000;
    }

    if (filters.date_end) {
      payload.date_end = new Date(filters.date_end) / 1000;
    }

    if (filters.building) {
      payload.building_id = filters.building.id;
    }

    if (filters.complex) {
      payload.complex_id = filters.complex.id;
    }

    return payload;
  },
  target: fxGetList,
});
