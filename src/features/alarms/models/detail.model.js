import { createEvent, createStore, restore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const tabsList = [
  {
    value: 'info',
    label: t('information'),
  },
  {
    value: 'history',
    label: t('history'),
  },
];

export const changeTab = createEvent();
export const open = createEvent();
export const changedDetailVisibility = createEvent();

export const $currentTab = restore(changeTab, 'info');
export const $opened = createStore({});
export const $isDetailOpen = restore(changedDetailVisibility, false);
export const $history = createStore({});
