import { createEffect, createEvent } from 'effector';
import { createPageBag } from '../../../tools/factories';
import { alarmsApi } from '../api';

export const changeAlarmState = createEvent();

export const fxGetHistory = createEffect();
export const fxUpdateCrashState = createEffect();

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(alarmsApi, { handleGetById }, { itemsAttribute: 'events' });

function handleGetById(state, result, id) {
  let newState = { ...state };
  const updatedIdx = state.data.findIndex((i) => String(i.id) === String(id));

  if (updatedIdx > -1) {
    newState = {
      ...newState,
      data: newState.data
        .slice(0, updatedIdx)
        .concat(result.event)
        .concat(newState.data.slice(updatedIdx + 1)),
    };
  } else {
    newState = {
      data: [...newState.data, result.event],
      meta: { ...newState.meta, total: newState.meta.total + 1 },
    };
  }

  return newState;
}
