import { forward, sample, split, attach, guard } from 'effector';
import format from 'date-fns/format';
import {
  RoomOutlined,
  CalendarTodayOutlined,
  ErrorOutlineOutlined,
  PersonOutlineOutlined,
  MemoryOutlined,
} from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { load as loadAlarmTypes, $data as $types } from '../../alarms-type-select';
import { signout, readNotifications, NEW_ALARM } from '../../common';
import {
  open,
  $opened,
  $isDetailOpen,
  $history,
  changedDetailVisibility,
} from './detail.model';
import {
  fxGetById,
  fxGetHistory,
  $normalized,
  fxUpdateCrashState,
  changeAlarmState,
} from './page.model';

const { t } = i18n;

const openApi = split(
  sample($normalized, open, (data, id) => ({ data, id })),
  {
    exist: ({ data, id }) => data[id],
    new: ({ data, id }) => !data[id],
  }
);

const detailClosed = guard({
  source: changedDetailVisibility,
  filter: (visibility) => !visibility,
});

const openGotById = sample($types, fxGetById.doneData, (types, opened) => ({
  types,
  opened: opened.event,
}));

const openExist = sample($types, openApi.exist, (types, { data, id }) => ({
  types,
  opened: data[id],
}));

$opened
  .on([openGotById, openExist], (state, { types, opened }) =>
    formatOpened({ opened, types })
  )
  .on(fxUpdateCrashState.done, (state) => ({ ...state, state: 0 }))
  .on(detailClosed, () => ({}))
  .reset(signout);

$history
  .on(fxGetHistory.doneData, (state, { history = [] }) => formatHistory(history))
  .reset(signout);

$isDetailOpen
  .on([fxGetById.done, openApi.exist], () => true)
  .on(detailClosed, () => false)
  .reset(signout);

forward({ from: open, to: [loadAlarmTypes, fxGetHistory] });

forward({
  from: fxUpdateCrashState.done,
  to: attach({ effect: fxGetHistory, mapParams: ({ params: id }) => id }),
});

forward({
  from: openApi.new,
  to: attach({ effect: fxGetById, mapParams: ({ id }) => id }),
});

forward({
  from: sample($opened, changeAlarmState, (opened) => opened.id),
  to: fxUpdateCrashState,
});

sample({
  source: $opened,
  fn: ({ id }) => ({ id, event: NEW_ALARM }),
  target: readNotifications,
});

function formatOpened({ opened, types }) {
  const { id, title, state, object, dt, type_id, initiator, device } = opened;

  const type = types.find((i) => i.id === type_id);

  return {
    id,
    title: title || t('accident'),
    state,
    info: [
      {
        label: t('Lable.address'),
        value: object ? object.title : t('isNotDefined'),
        Icon: RoomOutlined,
      },
      {
        label: t('dateTimeCreation'),
        value: dt
          ? format(new Date(dt.create * 1000), 'dd.MM.yyyy в HH:mm')
          : t('isNotDefinedit'),
        Icon: CalendarTodayOutlined,
      },
      {
        label: t('typeAccident'),
        value: type ? type.title : t('isNotDefined'),
        Icon: ErrorOutlineOutlined,
      },
      {
        label: t('changeInitiator'),
        value: initiator ? initiator.title : t('isNotDefined'),
        Icon: PersonOutlineOutlined,
      },
      {
        label: t('serialNumber'),
        value: device ? device.sn : t('isNotDefined'),
        Icon: MemoryOutlined,
      },
    ],
  };
}

function formatHistory(history = []) {
  return history.map((i) => ({
    label: format(new Date(i.dt * 1000), 'dd.MM.yyyy в HH:mm'),
    value: i.text,
  }));
}
