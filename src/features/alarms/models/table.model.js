import { combine } from 'effector';
import format from 'date-fns/format';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $data as $alarmTypes } from '../../alarms-type-select';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'alarmType', title: t('typeAccident') },
  { name: 'date', title: t('Label.date') },
  { name: 'time', title: t('Label.time') },
  { name: 'address', title: t('Label.address') },
  { name: 'sn', title: t('serialNumber') },
  { name: 'state', title: t('Label.status') },
  { name: 'initiator', title: t('changeInitiator') },
];

const widths = [
  { columnName: 'alarmType', width: 700 },
  { columnName: 'date', width: 110 },
  { columnName: 'time', width: 95 },
  { columnName: 'address', width: 400 },
  { columnName: 'sn', width: 360 },
  { columnName: 'state', width: 150 },
  { columnName: 'initiator', width: 400 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths });

export const $tableData = combine($raw, $alarmTypes, ({ data }, types) =>
  formatTableData({ events: data, types })
);

function formatTableData({ events, types }) {
  return events.map((i) => {
    const type = i.type_id && types.find((j) => String(j.id) === String(i.type_id));

    return {
      id: i.id,
      alarmType: {
        title: i.title || t('isNotDefinedit'),
        icon: i.icon || '',
      },
      new: i.new,
      date:
        i.dt && i.dt.create
          ? format(new Date(i.dt.date), 'dd.MM.yyyy', {
              locale: dateFnsLocale,
            })
          : t('isNotDefinedshe'),
      time:
        i.dt && i.dt.create
          ? { time: i.dt.time.slice(0, -3), timezone: i.dt.timezone }
          : t('isNotDefinedit'),
      address: i.object ? i.object.title : t('isNotDefined'),
      type: type ? type.title : t('isNotDefined'),
      sn: i.device ? i.device.sn : t('isNotDefined'),
      state: i.state,
      initiator: i.initiator ? i.initiator.title : t('isNotDefined'),
    };
  });
}
