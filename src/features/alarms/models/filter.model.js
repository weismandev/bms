import i18n from '@shared/config/i18n';
import { createFilterBag } from '../../../tools/factories';

const { t } = i18n;

const defaultFilters = {
  complex: '',
  building: '',
  date_start: '',
  date_end: '',
  type: '',
  state: { id: 1, label: t('onlyActive') },
  new: false,
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
