import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  InputField,
  SelectField,
  SwitchField,
  CustomScrollbar,
} from '../../../../ui';
import { AlarmsTypeSelectField } from '../../../alarms-type-select';
import { ComplexSelectField } from '../../../complex-select';
import { HouseSelectField } from '../../../house-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const Filter = memo((props) => {
  const filters = useStore($filters);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values }) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="state"
                  component={SelectField}
                  label={t('incidentStatus')}
                  options={[
                    { id: -1, label: t('all') },
                    { id: 0, label: t('onlyArchived') },
                    { id: 1, label: t('onlyActive') },
                  ]}
                  placeholder={t('selectState')}
                />

                <Field
                  name="type"
                  component={AlarmsTypeSelectField}
                  label={t('typeAccident')}
                  placeholder={t('chooseType')}
                />

                <Field
                  name="complex"
                  component={ComplexSelectField}
                  label={t('RC')}
                  placeholder={t('selectRC')}
                />

                <Field
                  name="building"
                  component={HouseSelectField}
                  complex={values.complex ? [values.complex] : []}
                  label={t('house')}
                  placeholder={t('selectHouse')}
                />

                <Field
                  name="date_start"
                  component={InputField}
                  label={t('startingFromTheDate')}
                  type="date"
                  placeholder={t('chooseStartDate')}
                />

                <Field
                  name="date_end"
                  component={InputField}
                  label={t('untilDate')}
                  type="date"
                  placeholder={t('chooseEndDate')}
                />

                <Field
                  component={SwitchField}
                  name="new"
                  label={t('unreadOnly')}
                  labelPlacement="start"
                />

                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
