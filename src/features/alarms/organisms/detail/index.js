import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { CheckBoxOutlined } from '@mui/icons-material';
import {
  Wrapper,
  Toolbar,
  FormSectionHeader,
  CloseButton,
  Greedy,
  Tabs,
  ActionButton,
  CustomScrollbar,
} from '../../../../ui';
import {
  changedDetailVisibility,
  $opened,
  tabsList,
  changeTab,
  $currentTab,
  $history,
} from '../../models/detail.model';
import { changeAlarmState } from '../../models/page.model';
import { StateChip } from '../atoms';
import { InfoBlock } from '../molecules';

const Detail = memo(() => {
  const opened = useStore($opened);
  const currentTab = useStore($currentTab);
  const history = useStore($history);

  return (
    <Wrapper style={{ height: '89vh' }}>
      <CustomScrollbar autoHide>
        <div style={{ padding: 24 }}>
          <Toolbar>
            <FormSectionHeader
              style={{
                margin: 0,
                whiteSpace: 'nowrap',
                width: 'calc(100% - 100px)',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
              }}
              header={opened.title}
            />
            <Greedy />
            <StateChip state={opened.state} />
            <CloseButton
              style={{ marginLeft: 10 }}
              onClick={() => changedDetailVisibility(false)}
            />
          </Toolbar>

          <Tabs
            options={tabsList}
            tabEl={<Tab style={{ minWidth: '50%' }} />}
            onChange={(e, tab) => changeTab(tab)}
            currentTab={currentTab}
            style={{ marginBottom: 24 }}
          />
          {currentTab === 'info' && <InfoTabContent opened={opened} />}
          {currentTab === 'history' && <HistoryTabContent history={history} />}
        </div>
      </CustomScrollbar>
    </Wrapper>
  );
});

function InfoTabContent(props) {
  const { opened } = props;
  const { t } = useTranslation();

  const infoBlocks = opened.info.map((i, idx) => <InfoBlock {...i} key={idx} />);

  return (
    <>
      {infoBlocks}
      {opened.state === 1 && (
        <div style={{ textAlign: 'center' }}>
          <ActionButton onClick={changeAlarmState}>
            <CheckBoxOutlined style={{ marginRight: 5 }} /> {t('closeAccident')}
          </ActionButton>
        </div>
      )}
    </>
  );
}

function HistoryTabContent(props) {
  const { history } = props;
  return history.map((i, idx) => <InfoBlock {...i} key={idx} />);
}

export { Detail };
