import { useTranslation } from 'react-i18next';
import { Chip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  chip: {
    color: '#fff',
    fontWeight: 500,
    fontSize: 13,
    lineHeight: '15px',
  },
});

export const StateChip = (props) => {
  const { state } = props;
  const { chip } = useStyles();
  const { t } = useTranslation();

  return state ? (
    <Chip label={t('active')} className={chip} style={{ background: '#EB5757' }} />
  ) : (
    <Chip label={t('closed')} className={chip} color="primary" />
  );
};
