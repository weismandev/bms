import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Divider } from '../../../../../ui';

const useStyles = makeStyles({
  subtitle1: {
    color: '#9494A3',
    fontSize: 14,
    lineHeight: '15px',
    fontWeight: 500,
    marginBottom: 12,
  },
  subtitle2: {
    color: '#65657B',
    fontSize: 13,
    lineHeight: '15px',
    fontWeight: 500,
  },
});

export const InfoBlock = (props) => {
  const { label, value, Icon } = props;
  const { subtitle1, subtitle2 } = useStyles();
  return (
    <div>
      <Typography classes={{ subtitle1 }} variant="subtitle1">
        {label}
      </Typography>
      <Typography classes={{ subtitle2 }} variant="subtitle2">
        {Icon && <Icon style={{ width: '14px', height: '14px', marginRight: 10 }} />}
        {value}
      </Typography>
      <Divider />
    </div>
  );
};
