import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { FilterButton, Greedy, Toolbar, FoundInfo } from '../../../../ui';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
});

function TableToolbar(props) {
  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const classes = useStyles();

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <FoundInfo count={totalCount} className={classes.margin} />
      <Greedy />
    </Toolbar>
  );
}

export { TableToolbar };
