import { useCallback, useMemo } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import { Template } from '@devexpress/dx-react-core';
import { PagingState, CustomPaging, DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  Toolbar,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  TableHeaderCell,
  TableColumnVisibility,
  PagingPanel,
} from '../../../../ui';
import { open, $opened } from '../../models/detail.model';
import { $rowCount } from '../../models/page.model';
import {
  $tableData,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $hiddenColumnNames,
  columns,
} from '../../models/table.model';
import { StateChip } from '../atoms';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const isNew = Boolean(props.row.new);

  const row = useMemo(
    () => (
      <TableRow
        {...props}
        style={{ background: isNew ? '#FDEFEF' : 'inherit' }}
        isSelected={isSelected}
        onRowClick={onRowClick}
      />
    ),
    [isSelected, props.children]
  );
  return row;
};

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      minHeight: '36px',
      height: '60px',
      alignItems: 'flex-start',
      borderBottom: 'none',
      flexWrap: 'nowrap',
    }}
  />
);

const StateFormatter = ({ value }) => <StateChip state={value} />;

const StateTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={StateFormatter} {...props} />
);

const AlarmFormatter = ({ value: { title, icon } }) => (
  <div style={{ display: 'flex' }}>
    <img src={icon} width="20" height="20" alt="" />
    <div style={{ marginRight: 15 }}>{title}</div>
  </div>
);

const AlarmTypeProvider = (props) => (
  <DataTypeProvider formatterComponent={AlarmFormatter} {...props} />
);

const TimeFormatter = ({ value }) => (
  <Tooltip title={value.timezone} arrow>
    <span>{value.time}</span>
  </Tooltip>
);

const TimeProvider = (props) => (
  <DataTypeProvider formatterComponent={TimeFormatter} {...props} />
);

export const Table = () => {
  const data = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid rootComponent={Root} rows={data} getRowId={(row) => row.id} columns={columns}>
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />
        <DragDropProvider />

        <StateTypeProvider for={['state']} />
        <AlarmTypeProvider for={['alarmType']} />

        <TimeProvider for={['time']} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};
