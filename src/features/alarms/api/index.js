import { api } from '../../../api/api2';

const getList = (payload) =>
  api.no_vers('get', 'information/crash-v2', payload).then(format);

const getById = (id) =>
  api
    .no_vers('get', 'information/crash-v2', { id })
    .then(format)
    .then(({ events }) => ({ event: events[0] }));

const getHistory = (id) => api.no_vers('get', 'information/get-crash-history', { id });

const updateCrashState = (id) =>
  api.no_vers('get', 'information/update-crash-state', { id, state: 0 });

function format(response) {
  const { meta, events = [] } = response;
  const formattedEvents = events.map((i) => ({
    ...i,
    icon: meta.img_url + i.icon,
  }));
  return { events: formattedEvents, meta };
}

export const alarmsApi = {
  getList,
  getById,
  getHistory,
  updateCrashState,
};
