export interface WorkFlowsResponse {
  tech_cards: WorkFlows[];
}

export interface WorkFlows {
  id: number;
  title: string;
}
