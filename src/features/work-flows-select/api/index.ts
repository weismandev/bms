import { api } from '@api/api2';

const getWorkFlows = () => api.v4('get', 'tech-cards/list', { per_page: 9999 });

export const workFlowsSelectApi = {
  getWorkFlows,
};
