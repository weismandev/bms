import { createEffect, createStore } from 'effector';

import { WorkFlowsResponse, WorkFlows } from '../../interfaces';

export const fxGetList = createEffect<void, WorkFlowsResponse, Error>();

export const $data = createStore<WorkFlows[]>([]);
export const $isLoading = createStore<boolean>(false);
