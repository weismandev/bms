import { signout } from '@features/common';

import { workFlowsSelectApi } from '../../api';
import { fxGetList, $data, $isLoading } from './work-flows-select.model';

fxGetList.use(workFlowsSelectApi.getWorkFlows);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.tech_cards)) {
      return [];
    }

    return result.tech_cards.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
