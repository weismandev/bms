export interface ClassesResponse {
  classes: {
    name: string;
    title: string;
  }[];
}

export interface ClassesData {
  id: string;
  title: string;
}
