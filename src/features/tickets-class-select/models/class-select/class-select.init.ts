import { signout } from '@features/common';
import { ticketClassApi } from '../../api';
import { fxGetList, $data, $isLoading } from './class-select.model';

fxGetList.use(ticketClassApi.getClassList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.classes)) {
      return [];
    }

    return result.classes.map((item) => ({ id: item.name, title: item.title }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
