import { createEffect, createStore } from 'effector';
import { ClassesResponse, ClassesData } from '../../interfaces';

const fxGetList = createEffect<void, ClassesResponse, Error>();

const $data = createStore<ClassesData[] | []>([]);
const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
