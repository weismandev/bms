import { api } from '@api/api2';

const getClassList = () => api.v1('get', 'tck/classes/list');

export const ticketClassApi = {
  getClassList,
};
