import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { ClassesData } from '../../interfaces';
import { $data, fxGetList, $isLoading } from '../../models';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  excludeClassId?: string;
  isNew?: boolean;
}

const ClassSelect: FC<Props> = ({ excludeClassId, isNew, ...restProps }) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  const options = isNew
    ? excludeClassId
      ? data.filter((item: { id: string }) => item.id !== excludeClassId)
      : data
    : data;

  return <SelectControl options={options} isLoading={isLoading} {...restProps} />;
};

const ClassSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: ClassesData) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ClassSelect />} onChange={onChange} {...props} />;
};

export { ClassSelect, ClassSelectField };
