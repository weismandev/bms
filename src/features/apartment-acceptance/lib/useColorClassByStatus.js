import makeStyles from '@mui/styles/makeStyles';

const cBlue = '#0394E3';
const cGrey = '#aaa';
const cGreyDark = '#65657B';
const cRed = '#EB5757';
const cGreen = '#1BB169';

const useStyles = makeStyles((theme) => {
  return {
    green: { color: cGreen },
    red: { color: cRed },
    grey: { color: cGreyDark },
    blue: { color: cBlue },

    bcg_green: { background: cGreen },
    bcg_red: { background: cRed },
    bcg_grey: { background: cGrey },
    bcg_blue: { background: cBlue },
  };
});

export const useColorClassByStatus = function (status) {
  const { green, red, blue, grey, bcg_green, bcg_red, bcg_grey, bcg_blue } = useStyles();

  const isApproved = status.slug === 'approved';
  const isIssues = status.slug === 'issues';
  const isNotApproved = status.slug === 'not_approved';
  const isIssuesClosed = status.slug === 'issues_closed';
  const isScheduled = status.slug === 'scheduled';
  const isInviteSent = status.slug === 'invite_sent';

  const color = {
    [green]: isApproved,
    [red]: isIssues,
    [blue]: isIssuesClosed,
    [grey]: isNotApproved || isScheduled || isInviteSent,
  };

  const bcg = {
    [bcg_green]: isApproved,
    [bcg_red]: isIssues,
    [bcg_blue]: isIssuesClosed,
    [bcg_grey]: isNotApproved || isScheduled || isInviteSent,
  };

  return { color, bcg };
};
