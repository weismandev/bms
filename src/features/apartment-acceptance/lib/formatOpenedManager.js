import { formatPhone } from '@tools/formatPhone';

export const formatOpenedManager = (opened) => {
  const { email, id, phone, surname, name, patronymic } = opened;

  return {
    email: email || '',
    name: name || '',
    surname: surname || '',
    patronymic: patronymic || '',
    phone: phone ? formatPhone(phone) : '',
    type: 'manager',
    id,
  };
};
