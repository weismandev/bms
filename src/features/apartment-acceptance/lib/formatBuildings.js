export const formatBuildings = (builds) =>
  Array.isArray(builds)
    ? builds.map(({ building, id }) => {
        const { street = '', houseNumber = '' } = building.address;

        return {
          label:
            street && houseNumber
              ? `${street}, ${houseNumber}`
              : building.address?.fullAddress || '',
          id,
        };
      })
    : [];
