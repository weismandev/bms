export const formatName = (names) =>
  Array.isArray(names)
    ? names
        .reduce(
          (acc, item) =>
            Boolean(item) && typeof item === 'string' ? [...acc, item] : acc,
          []
        )
        .join(' ')
    : 'не определено';
