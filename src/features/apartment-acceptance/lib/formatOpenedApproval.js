import set from 'date-fns/set';
import { formatPhone } from '@tools/formatPhone';
import { formatDurations } from './formatDurations';
import { formatManagers } from './formatManagers';

export const formatOpenedApproval = (opened) => {
  const {
    id,
    buildings,
    starts_at,
    ends_at,
    default_duration,
    tomorrow_appointment_closes_at: taca,
    durations,
    managers,
    reminder_file_url,
    sales_phone,
    legal_text,
  } = opened;

  return {
    id,
    type: 'approval',
    buildings: buildings || [],
    starts_at: starts_at ? new Date(starts_at) : '',
    ends_at: ends_at ? new Date(ends_at) : '',
    default_duration: default_duration || 30,
    tomorrow_appointment_closes_at:
      taca && typeof taca === 'string'
        ? (() => {
            const hourMinute = taca.split(':');
            return set(new Date(), {
              hours: hourMinute[0],
              minutes: hourMinute[1],
              seconds: 0,
            });
          })()
        : set(new Date(), { hours: 18, minutes: 0, seconds: 0 }),
    durations: durations ? formatDurations(durations) : '',
    managers: managers ? formatManagers(managers) : '',
    emails: [],
    test_email: '',
    template: {
      id: null,
    },
    reminder_file_url:
      Boolean(reminder_file_url) && typeof reminder_file_url === 'string'
        ? reminder_file_url
        : '',
    sales_phone: sales_phone ? formatPhone(sales_phone) : '',
    legal_text: legal_text || '',
  };
};
