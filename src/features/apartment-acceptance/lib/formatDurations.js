import { durationOptions, typeOptions } from '../models/detail.model';

const createKeymapFromArray = (entitys) =>
  entitys.reduce((acc, entity) => ({ ...acc, [entity.value]: entity }), {});

const typeObjectsMap = createKeymapFromArray(typeOptions);
const durationObjactsMap = createKeymapFromArray(durationOptions);

const removeBrokenDurations = (durations) =>
  durations.filter(({ duration, type }) => duration && type);

const transformDurationsParamsToObjects = (durations) =>
  durations.map(({ duration, type }) => ({
    duration: durationObjactsMap[duration],
    type: typeObjectsMap[type],
  }));

export const formatDurations = (durations) => {
  const cleanedDurations = removeBrokenDurations(durations);
  const formattedDurations = transformDurationsParamsToObjects(cleanedDurations);

  return formattedDurations;
};
