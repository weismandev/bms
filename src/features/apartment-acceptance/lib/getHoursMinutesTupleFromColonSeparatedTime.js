export function getHoursMinutesTupleFromColonSeparatedTime(time) {
  return typeof time === 'string' ? time.split(':') : [null, null];
}
