import { formatName } from './formatName';

export const formatManagers = (managers) =>
  Array.isArray(managers)
    ? managers.map(({ id, surname = '', name = '', patronymic = '' }) => ({
        fio: formatName([surname, name, patronymic]),
        id,
        isFixed: true,
      }))
    : [];
