import { combine } from 'effector';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { ErrorMessage, DeleteConfirmDialog, Loader } from '../../../ui';
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { HaveSectionAccess } from '../../common';
import '../models/detail.init';
import { $isDetailOpen } from '../models/detail.model';
import '../models/page.init';
import {
  PageGate,
  deleteConfirmed,
  $currentTable,
  $isErrorDialogOpen,
  $isDeleteDialogOpen,
  $isLoading,
  $error,
  errorDialogVisibilityChange,
  deleteDialogVisibilityChange,
} from '../models/page.model';
import {
  ApprovalsTable,
  ApprovalsDetail,
  ManagersTable,
  ManagersDetail,
} from '../organisms';
import { PageLayout } from '../template';

const $stores = combine({
  currentTable: $currentTable,
  isDetailOpen: $isDetailOpen,
  isLoading: $isLoading,
  error: $error,
  isErrorDialogOpen: $isErrorDialogOpen,
  isDeleteDialogOpen: $isDeleteDialogOpen,
});

const { t } = i18n;

const AcceptanceDashboardPage = () => {
  const {
    currentTable,
    isDetailOpen,
    isLoading,
    error,
    isErrorDialogOpen,
    isDeleteDialogOpen,
  } = useStore($stores);

  return (
    <>
      <PageGate />
      <ColoredTextIconNotification />
      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChange(false)}
        error={error}
      />
      <DeleteConfirmDialog
        content={
          currentTable === 'approvals'
            ? t('AreYouSureYouWantToDeleteThisAcceptance')
            : t('AreYouSureYouWantToRemoveThisManager')
        }
        header={
          currentTable === 'approvals' ? t('DeletingAcceptance') : t('RemovingManager')
        }
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChange(false)}
        confirm={deleteConfirmed}
      />
      <PageLayout
        left={currentTable === 'approvals' ? <ApprovalsTable /> : <ManagersTable />}
        right={
          isDetailOpen ? (
            currentTable === 'approvals' ? (
              <ApprovalsDetail />
            ) : (
              <ManagersDetail />
            )
          ) : null
        }
      />
    </>
  );
};

const RestrictedAcceptanceDashboardPage = () => (
  <HaveSectionAccess>
    <AcceptanceDashboardPage />
  </HaveSectionAccess>
);

export default RestrictedAcceptanceDashboardPage;
