import { useEffect } from 'react';
import { useStore } from 'effector-react';
import format from 'date-fns/format';

import { ScheduleLayout } from '@ui/index';
import { history, HaveSectionAccess } from '@features/common';
import { currentPageChanged } from '@features/navbar/models';

import i18n from '@shared/config/i18n';

import { AcceptanceList, Calendar, Filters, SectionHeader, Settings } from '../organisms';

import { $currentSectionTab } from '../models/page.model';
import { $acceptance, AcceptanceListGate } from '../models/acceptance-list.model';
import { $isFilterOpen } from '../models/filters.model';
import '../models/page.init';
import '../models/acceptance-list.init';

const { t } = i18n;

const SchedulePage = () => {
  const sectionTab = useStore($currentSectionTab);
  const acceptance = useStore($acceptance);
  const isFilterOpen = useStore($isFilterOpen);

  useEffect(() => {
    currentPageChanged({
      title:
        acceptance && acceptance.starts_at
          ? `${t('AcceptanceFrom')} ${format(
            new Date(acceptance.starts_at),
            'dd.MM.yyyy'
          )}`
          : t('Acceptance'),
      back: () => history.push('/apartment-acceptance'),
    });
  }, [acceptance]);

  return (
    <>
      <AcceptanceListGate />
      <ScheduleLayout
        top={<SectionHeader />}
        left={sectionTab === 'schedule' ? <Settings /> : <AcceptanceList />}
        right={<Calendar />}
        filter={sectionTab === 'appointments' && isFilterOpen && <Filters />}
      />
    </>
  );
};

const RestrictedSchedulePage = (props) => (
  <HaveSectionAccess>
    <SchedulePage {...props} />
  </HaveSectionAccess>
);

export default RestrictedSchedulePage;
