import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
} from '@ui/index';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filters.model';
import { StatusSelectField } from '../status-select';

const { t } = i18n;

const Filters = memo(() => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={() => (
            <Form style={{ padding: '0 24px 24px' }}>
              <>
                <Field
                  name="scheduled_from"
                  label={t('dateFrom')}
                  component={InputField}
                  placeholder=""
                  type="datetime-local"
                />
                <Field
                  name="scheduled_to"
                  label={t('dateBy')}
                  component={InputField}
                  placeholder=""
                  type="datetime-local"
                />
                <Field
                  name="status"
                  label={t('Label.status')}
                  component={StatusSelectField}
                  placeholder={t('selectStatuse')}
                />
              </>
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filters };
