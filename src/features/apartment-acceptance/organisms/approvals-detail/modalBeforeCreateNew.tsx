import { useTranslation } from 'react-i18next';
import { ActionButton, Modal } from '@ui/index';
import { detailSubmitted } from '../../models/detail.model';
import { StyledActions } from './styled';

export const ModalBeforeCreateNew = ({
  values,
  setIsModalBeforeCreate,
}: {
  values: any;
  setIsModalBeforeCreate: (data: boolean) => void;
}) => {
  const { t } = useTranslation();

  const actions = (
    <StyledActions>
      <ActionButton
        kind="basic"
        onClick={() => {
          setIsModalBeforeCreate(false);
          detailSubmitted(values);
        }}
        style={{ marginRight: 12 }}
      >
        {t('Save')}
      </ActionButton>

      <ActionButton onClick={() => setIsModalBeforeCreate(false)} kind="negative">
        {t('cancel')}
      </ActionButton>
    </StyledActions>
  );

  return (
    <Modal
      isOpen={true}
      onClose={() => setIsModalBeforeCreate(false)}
      header={t('CheckAcceptanceDataIsEnteredCorrectly')}
      content={t('AfterCreatingAcceptanceBlockSomeSections')}
      actions={actions}
    />
  );
};
