import { useState } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { format } from 'date-fns';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Tab } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  Tabs,
  DetailToolbar,
  ActionButton,
  CustomScrollbar,
} from '../../../../ui';
import { openAcceptance } from '../../models/acceptance-list.model';
import '../../models/detail.init';
import {
  $mode,
  $opened,
  $currentDetailsTab,
  $detailTabs,
  changeDetailsTab,
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
} from '../../models/detail.model';
import '../../models/page.init';
import { deleteDialogVisibilityChange, $buildings } from '../../models/page.model';
import '../../models/sms-detail.init';
import { $providerScheme } from '../../models/sms-detail.model';
import { MainDetails, SmsDetails, MailingDetails } from '../../molecules';
import { ModalBeforeCreateNew } from './modalBeforeCreateNew';

const { t } = i18n;

const dateError = t('PleaseSelectDate');
const validationSchemeForMain = Yup.object().shape({
  buildings: Yup.array().required(t('PleaseSelectObjectsFromTheList')),
  starts_at: Yup.date().required(dateError).typeError(dateError),
  ends_at: Yup.date().required(dateError).typeError(dateError),
  tomorrow_appointment_closes_at: Yup.mixed().nullable().required(),
  durations: Yup.array().of(
    Yup.object().shape({
      type: Yup.object().nullable().required(t('PleaseSelectObjectType')),
      duration: Yup.object().nullable().required(t('andDurationAcceptance')),
    })
  ),
  managers: Yup.array().required(t('PleaseSelectManagersFromList')),
  sales_phone: Yup.string().matches(/\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}/, {
    message: t('invalidNumber'),
    excludeEmptyString: true,
  }),
});

const useStyles = makeStyles({
  headerRoot: {
    padding: '16px 0 17px',
    minHeight: 62,
    overflowWrap: 'anywhere',
  },
  headerText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '28px',
  },
});

const $stores = combine({
  mode: $mode,
  opened: $opened,
  currentDetailsTab: $currentDetailsTab,
  detailTabs: $detailTabs,
  buildings: $buildings,
  providerScheme: $providerScheme,
});

export const ApprovalsDetail = () => {
  const { headerText, headerRoot } = useStyles();
  const { mode, opened, currentDetailsTab, detailTabs } = useStore($stores);
  const [isModalBeforeCreate, setIsModalBeforeCreate] = useState(false);

  const isNew = !opened.id;

  const setValidationSchema = () => {
    switch (currentDetailsTab) {
      case 'main':
        return validationSchemeForMain;
      case 'sms':
        return {};
      default:
        return {};
    }
  };

  const detailSubmittedHandler = (values) => {
    if (!isNew) {
      detailSubmitted(values);
      return;
    }

    setIsModalBeforeCreate(true);
  };

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Formik
        enableReinitialize
        validationSchema={setValidationSchema}
        initialValues={opened}
        onSubmit={detailSubmittedHandler}
        render={({ resetForm, values }) => (
          <>
            {isModalBeforeCreate && (
              <ModalBeforeCreateNew
                values={values}
                setIsModalBeforeCreate={setIsModalBeforeCreate}
              />
            )}

            <Form style={{ height: '100%' }}>
              <DetailToolbar
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    resetForm();
                    changeMode('view');
                  }
                }}
                onClose={() => changedDetailVisibility(false)}
                onDelete={() => deleteDialogVisibilityChange(true)}
                mode={mode}
              >
                <ActionButton
                  style={{
                    padding: '0 20px',
                    order: 22,
                    marginLeft: 5,
                  }}
                  kind="basic"
                  onClick={() => openAcceptance(values.id)}
                  disabled={mode === 'edit'}
                >
                  {t('GoToAcceptance')}
                </ActionButton>
              </DetailToolbar>
              <div className={headerRoot}>
                <span className={headerText}>
                  {values.starts_at
                    ? `${t('AcceptanceFrom')} ${format(
                        new Date(values.starts_at),
                        'dd.MM.yyyy'
                      )}`
                    : t('NewAcceptance')}
                </span>
              </div>
              <Tabs
                isNew={isNew}
                tabEl={
                  <Tab style={{ minWidth: 20, minHeight: 32, padding: '6px 10px' }} />
                }
                currentTab={currentDetailsTab}
                options={detailTabs}
                onChange={(e, value) => changeDetailsTab(value)}
                scrollButtons={false}
              />
              <div
                style={{
                  width: 'calc(100% + 19px)',
                  height: 'calc(100% - 136px)',
                }}
              >
                <CustomScrollbar style={{ height: '100%' }} autoHide>
                  <div
                    style={{
                      width: 'calc(100% - 19px)',
                      paddingTop: 14,
                      display: 'flex',
                      flexDirection: 'column',
                    }}
                  >
                    {currentDetailsTab === 'main' ? (
                      <MainDetails values={values} />
                    ) : null}
                    {currentDetailsTab === 'sms' ? <SmsDetails values={values} /> : null}
                    {currentDetailsTab === 'mailing' ? (
                      <MailingDetails values={values} />
                    ) : null}
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          </>
        )}
      />
    </Wrapper>
  );
};
