import makeStyles from '@mui/styles/makeStyles';

const cellHeight = 32;

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    zIndex: 3,
    position: 'relative',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    gap: 24,
    padding: '24px 24px 0',
  },
  form: {
    height: 'inherit',
    position: 'relative',
  },
  content: {
    height: 'calc(100% - 20px)',
    position: 'relative',
  },
  overlay: {
    position: 'fixed',
    background: 'rgba(0, 0, 0, .5)',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 2,
  },
  dialogContent: {
    fontSize: 14,
  },
  dialogContentLabel: {
    color: 'black',
    fontWeight: 500,
  },
});

const useCellStyles = makeStyles(() => ({
  simple_cell: {
    height: 32,
  },
  cell: {
    position: 'relative',
    height: 32,
    '&:hover::before': {
      content: "''",
      position: 'absolute',
      top: 0,
      display: 'block',
      left: 0,
      width: '100%',
      zIndex: 30,
      height: (props) => `calc(32px * ${props.intervalsCount})`,
      background: (props) => (props.availableForAppointment ? '#0394E3' : '#EB5757'),
      opacity: '.2',
    },
  },
  label: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0,
  },
}));

const useAppointementStyles = makeStyles({
  appointment: {
    opacity: 0.5,
    width: 'min-content',
    height: '96%',
    justifyContent: 'flex-start',
    alignItems: 'normal',
    '&:hover': {
      cursor: 'pointer',
      background: '#0394E3',
    },
  },
  text: {
    color: '#FFFFFF',
    fontWeight: 600,
    fontSize: 16,
    flexGrow: 10,
    padding: '8px 8px',
  },
});

const useDayScaleCellStyles = makeStyles({
  root: {
    display: 'grid',
    gridTemplateColumns: '40px 1fr',
    gridTemplateRows: '24px 18px',
  },

  selecaAll: {
    gridColumn: '1 / 2',
    gridRow: '1 / span 1',
  },

  dayAndMonth: {
    gridColumn: '1 / 3',
    gridRow: '1 / 2',
    fontWeight: 500,
    fontSize: 18,
    lineHeight: '21px',
    color: '#65657B',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },

  dayAndMonthWithCheckbox: {
    gridColumn: '2',
  },

  dayOfWeek: {
    gridColumn: '1 / 3',
    gridRow: '2',
    fontWeight: 500,
    fontSize: 14,
    lineHeight: '21px',
    color: '#65657B',
    opacity: '.7',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },

  dayOfWeekWithCheckbox: {
    gridColumn: '2',
  },

  today: {
    color: '#0394E3',
  },
});

const useLayoutStyles = makeStyles({
  root: {
    height: '100%',
    position: 'relative',
    display: 'grid',
    gridTemplateColumns: '60px 1fr',
    gridTemplateRows: '64px calc(100% - 64px)',
  },

  head: {
    gridColumn: '2',
    height: '64px',
  },

  body: {
    gridColumn: '1 / 3',
    gridRow: '2 / 3',
  },

  bodyContainer: {
    display: 'grid',
    overflow: 'auto',
    overflowX: 'hidden',
    height: '100%',
    gridTemplateColumns: '60px 1fr',
    gridTemplateRows: '1fr',
  },

  timeScale: {
    gridColumn: 1,
  },

  timeTable: {
    gridColumn: 2,
    position: 'relative',
    // Фикс для Firefox, в противном случае ячейки не разделены границами
    '& table': {
      borderCollapse: 'separate',
    },
  },
});

const scaleStyles = makeStyles({
  scaleCell: {
    height: cellHeight,
    lineHeight: `${cellHeight}px`,
    '&:first-child': {
      height: cellHeight / 1.84,
    },
  },
});

export {
  useStyles,
  useCellStyles,
  useAppointementStyles,
  useDayScaleCellStyles,
  useLayoutStyles,
  scaleStyles,
};
