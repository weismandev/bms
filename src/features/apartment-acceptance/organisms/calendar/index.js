import { useRef, memo } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import classnames from 'classnames';
import add from 'date-fns/add';
import format from 'date-fns/format';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { FormControlLabel, Chip } from '@mui/material';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  Appointments,
} from '@devexpress/dx-react-scheduler-material-ui';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  WeekControl,
  Checkbox,
  useHover,
  Loader,
  ErrorMsg as ErrorMessage,
  DeleteConfirmDialog,
  CustomScrollbar,
} from '@ui/index';
import { Header } from '../../atoms';
import { useColorClassByStatus } from '../../lib';
import {
  $intervalsCountForAppointment,
  $opened,
  $normalized,
  $listData
} from '../../models/acceptance-list.model';
import {
  changeWeek,
  changeCell,
  changeCalendarMode,
  $calendarMode,
  cancelEdit,
  $calendarForm,
  submitCalendar,
  $week,
  $isAppointmentsMode,
  changeAppointmentsCloseDialogVisibility,
  changeAppointmentsAcceptanceDialogVisibility,
  $events,
  $calendarSettings,
  $isLoading,
  $error,
  changeAllCellByDay,
  $daysHasSelectedCells,
  $isDeleteConfirmDialogOpen,
  changeDeleteConfirmDialogVisibility,
  deleteAppointmentConfirmed,
  $defaultDuration,
  $savedEvents,
  getApartmentInfo,
  $apartmentInfo,
} from '../../models/calendar.model';
import '../../models/page.init';
import { $currentSectionTab } from '../../models/page.model';
import { AppointmentToAcceptanceDialog } from '../appointment-to-acceptance-dialog';
import { CloseAppointmentDialog } from '../close-appointment-dialog';
import {
  useStyles,
  useCellStyles,
  useAppointementStyles,
  useDayScaleCellStyles,
  useLayoutStyles,
  scaleStyles,
} from './styles';

const { t } = i18n;

const $stores = combine({
  week: $week,
  calendarSettings: $calendarSettings,
  savedEvents: $savedEvents,
  isLoading: $isLoading,
  error: $error,
  isDeleteConfirmDialogOpen: $isDeleteConfirmDialogOpen,
});

const $dayScaleStores = combine({
  mode: $calendarMode,
  tab: $currentSectionTab,
  daysHasSelectedCells: $daysHasSelectedCells,
});

const $toolbarStores = combine({
  mode: $calendarMode,
  tab: $currentSectionTab,
});

const $scheduleCellStores = combine({
  mode: $calendarMode,
  calendarForm: $calendarForm,
});

const $appointmentCellStores = combine({
  intervalsCount: $intervalsCountForAppointment,
  calendarForm: $calendarForm,
  opened: $opened,
  defaultDuration: $defaultDuration,
});

export const Calendar = () => {
  const { wrapper, content } = useStyles();
  const {
    week,
    calendarSettings,
    isLoading,
    error,
    isDeleteConfirmDialogOpen,
    savedEvents,
  } = useStore($stores);

  const apartmentInfo = useStore($apartmentInfo);

  const classes = useStyles();

  const scheduleContainerRef = useRef(null);
  const height = scheduleContainerRef.current
    ? scheduleContainerRef.current.clientHeight
    : 0;

  const deleteDialogText = apartmentInfo ? (
    <>
      <div style={{ margin: '5px 0' }}>
        {apartmentInfo?.flat && (
          <div className={classes.dialogContent}>
            <span className={classes.dialogContentLabel}>Квартира </span>
            <span>{`№ ${apartmentInfo.flat}`}</span>
          </div>
        )}
        {apartmentInfo?.owner && (
          <div className={classes.dialogContent}>
            <span className={classes.dialogContentLabel}>Собственник: </span>
            <span>{apartmentInfo.owner}</span>
          </div>
        )}
        {apartmentInfo?.phone && (
          <div className={classes.dialogContent}>
            <span className={classes.dialogContentLabel}>Тел.: </span>
            <span>{apartmentInfo.phone}</span>
          </div>
        )}
        {apartmentInfo?.isExistObject && (
          <div className={classes.dialogContent}>
            <span className={classes.dialogContentLabel}>Объекты собственности:</span>
            <div style={{ marginLeft: 15 }}>
              {apartmentInfo?.objects?.flat.length > 0 && (
                <div>
                  <span className={classes.dialogContentLabel}>Квартиры: </span>
                  <span>{`№ ${apartmentInfo.objects.flat.join(', ')}`}</span>
                </div>
              )}
              {apartmentInfo?.objects?.parking_slot.length > 0 && (
                <div>
                  <span className={classes.dialogContentLabel}>Парковочные места: </span>
                  <span>{`№ ${apartmentInfo.objects.parking_slot.join(', ')}`}</span>
                </div>
              )}
              {apartmentInfo?.objects?.pantry.length > 0 && (
                <div>
                  <span className={classes.dialogContentLabel}>Кладовые: </span>
                  <span>{`№ ${apartmentInfo.objects.pantry.join(', ')}`}</span>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
      <div className={classes.dialogContent} style={{ marginBottom: 5 }}>
        {t('AreYouSureYouWantToDeleteTheAcceptanceEntry')}
      </div>
    </>
  ) : (
    <div className={classes.dialogContent} style={{ marginBottom: 5 }}>
      {t('AreYouSureYouWantToDeleteTheAcceptanceEntry')}
    </div>
  );

  return (
    <>
      <Overlay />
      <DeleteConfirmDialog
        isOpen={isDeleteConfirmDialogOpen.isOpen}
        close={() => changeDeleteConfirmDialogVisibility({ isOpen: false })}
        confirm={deleteAppointmentConfirmed}
        content={deleteDialogText}
        header={t('DeletingReceiptEntry')}
      />
      <CloseAppointmentDialog />
      <AppointmentToAcceptanceDialog />
      <Wrapper className={wrapper} onClick={(e) => e.stopPropagation()}>
        <Loader isLoading={isLoading} />
        <ErrorMessage error={error} />
        <Toolbar />
        <CustomScrollbar autoHide>
          <div className={content} ref={scheduleContainerRef}>
            <Scheduler
              data={savedEvents}
              locale="ru-RU"
              firstDayOfWeek={1}
              height={height}
            >
              <ViewState currentDate={week ? week.weekStart : new Date()} />
              <WeekView
                timeTableCellComponent={TimeTableCellComponent}
                timeScaleTickCellComponent={TimeScaleTickCell}
                timeScaleLabelComponent={TimeScaleLabel}
                dayScaleCellComponent={DayScaleCell}
                layoutComponent={LayoutComponent}
                {...calendarSettings}
              />
              <Appointments
                appointmentComponent={Appointment}
                containerComponent={AppointmentContainer}
              />
            </Scheduler>
          </div>
        </CustomScrollbar>
      </Wrapper>
    </>
  );
};

const TimeTableCellComponent = (props) => {
  const tab = useStore($currentSectionTab);

  return tab === 'schedule' ? (
    <ScheduleTimeTableCell {...props} />
  ) : (
    <AppointmentTimeTableCell {...props} />
  );
};

const Overlay = () => {
  const { overlay } = useStyles();
  const isAppointmentsMode = useStore($isAppointmentsMode);

  return (
    <div
      className={overlay}
      style={{ display: isAppointmentsMode ? 'block' : 'none' }}
      onClick={() => changeAppointmentsCloseDialogVisibility(true)}
      aria-hidden
    />
  );
};

const Toolbar = () => {
  const { toolbar } = useStyles();
  const { mode, tab } = useStore($toolbarStores);
  const isAppointmentTab = tab === 'appointments';

  return (
    <DetailToolbar
      className={toolbar}
      hidden={{
        delete: true,
        close: true,
        edit: isAppointmentTab,
        save: isAppointmentTab,
        cancel: isAppointmentTab,
      }}
      mode={mode}
      onEdit={() => changeCalendarMode('edit')}
      onCancel={cancelEdit}
      onSave={submitCalendar}
    >
      <Header style={{ order: 40, marginLeft: 10 }} headerText={t('Calendar')} />
      <div
        style={{
          width: 240,
          order: 50,
          display: mode === 'edit' ? 'none' : 'block',
        }}
      >
        <WeekControl onChange={changeWeek} />
      </div>
    </DetailToolbar>
  );
};

const ScheduleTimeTableCell = (props) => {
  const { startDate: cellStartDate, endDate: cellEndDate, ...rest } = props;
  const { mode, calendarForm } = useStore($scheduleCellStores);
  const name = getKey({ fromDate: cellStartDate, toDate: cellEndDate });
  const checked = Boolean(calendarForm[name]) && calendarForm[name].is_enabled;
  const isEditMode = mode === 'edit';

  return (
    <Cell
      checked={checked}
      isEditMode={isEditMode}
      name={name}
      ends_at={format(cellEndDate, 'yyyy-MM-dd HH:mm')}
      starts_at={format(cellStartDate, 'yyyy-MM-dd HH:mm')}
      {...rest}
    />
  );
};

const Cell = memo((props) => {
  const { simple_cell, label } = useCellStyles();
  const { checked, isEditMode, name, ends_at, starts_at, ...rest } = props;
  const background = checked ? '#A4E0C3' : '#F3F3F6';

  const checkbox = isEditMode ? (
    <FormControlLabel
      classes={{ root: label }}
      control={(
        <Checkbox
          name={name}
          size="small"
          disabled={new Date() > new Date(starts_at)}
          checked={checked}
          onChange={(e, isChecked) =>
            changeCell({
              name: e.target.name,
              isChecked,
              ends_at,
              starts_at,
            })}
        />
      )}
      label=""
    />
  ) : null;

  return (
    <WeekView.TimeTableCell {...rest} style={{ background }} className={simple_cell}>
      {checkbox}
    </WeekView.TimeTableCell>
  );
});

const AppointmentTimeTableCell = (props) => {
  const { startDate: cellStartDate, endDate: cellEndDate } = props;
  const { intervalsCount, calendarForm, opened } =
    useStore($appointmentCellStores);
  const isAppointmentsMode = useStore($isAppointmentsMode);
  const savedEvents = useStore($savedEvents);

  const key = getKey({ fromDate: cellStartDate, toDate: cellEndDate });
  const checked = Boolean(calendarForm[key]) && calendarForm[key].is_enabled;

  const { hover, handleMouseIn, handleMouseOut } = useHover('td');

  const isAvailable = isAvailableSlot({
    isAppointmentsMode,
    events: savedEvents,
    startTime: cellStartDate,
    endTime: cellEndDate,
  });

  const isAfterFreeSlot = checkFreeAfterSlot({
    startTime: cellStartDate,
    events: savedEvents,
    isAppointmentsMode,
  });

  const isBeforeFreeSlot = checkFreeBeforeSlot({
    startTime: cellStartDate,
    events: savedEvents,
    isAppointmentsMode,
    next: intervalsCount,
    defaultDuration: $defaultDuration.getState(),
  });

  const isNextAvailable = checkAvailabilityNext({
    startTime: cellStartDate,
    endTime: cellEndDate,
    register: calendarForm,
    next: intervalsCount,
    defaultDuration: $defaultDuration.getState(),
  });

  const availableForAppointment =
    hover &&
    checked &&
    isNextAvailable &&
    opened &&
    isAvailable &&
    isAfterFreeSlot &&
    isBeforeFreeSlot;

  const { cell } = useCellStyles({ intervalsCount, availableForAppointment });

  const background = checked ? '#fff' : '#F3F3F6';

  return (
    <WeekView.TimeTableCell
      {...props}
      onMouseOver={handleMouseIn}
      onMouseOut={handleMouseOut}
      style={{ background }}
      className={cell}
      onClick={() => {
        if (availableForAppointment) {
          changeAppointmentsAcceptanceDialogVisibility({
            isOpen: true,
            start: cellStartDate,
          });
        }
      }}
    />
  );
};

const TimeScaleTickCell = () => null;

const TimeScaleLabel = (props) => {
  const { scaleCell } = scaleStyles();
  return <WeekView.TimeScaleLabel className={scaleCell} {...props} />;
};

const AppointmentContainer = (props) => <div {...props} />;

const Appointment = (props) => {
  const isAppointmentsMode = useStore($isAppointmentsMode);
  const events = useStore($events);
  const listData = useStore($listData);
  const normalized = useStore($normalized);

  const { appointment, text } = useAppointementStyles();

  const findedEvents = events.filter((event) => event.title === props.data.title);
  const findedfromListData = listData.find(
    (listItem) => listItem.number === props.data.title
  );

  const status = (() => {
    if (findedfromListData?.state) return findedfromListData?.state;

    return findedEvents?.length > 1
      ? findedEvents[findedEvents.length - 1].state
      : props.data.state;
  })();

  const { bcg } = useColorClassByStatus(status);

  const findedEvent = events.find((event) => event.id === props.data.id);

  if (isAppointmentsMode && !findedEvent) {
    return (
      <div
        style={{
          marginLeft: -1,
          marginTop: -1,
          height: '100%',
          background: '#EB5757',
          width: '100%',
          opacity: '.5',
        }}
      />
    );
  }

  if (!isAppointmentsMode && !findedEvent) {
    return null;
  }

  const findedData = Object.values(normalized).filter((item) =>
    item.events.find((event) => event.id === props.data.id)
  )[0];

  const onClick = () => {
    if (findedData?.id) {
      getApartmentInfo({ id: findedData.id, cellId: props.data.id });
    }
  };

  return (
    <Chip
      {...props}
      onClick={onClick}
      classes={{ root: appointment, label: text }}
      className={bcg}
      label={props.data.title}
      clickable={false}
    />
  );
};

const DayScaleCell = (props) => {
  const { mode, tab, daysHasSelectedCells } = useStore($dayScaleStores);

  const showCheckbox = mode === 'edit' && tab === 'schedule';
  const day = format(props.startDate, 'dd.MM.yyyy');
  const checked = daysHasSelectedCells[day];

  const {
    root,
    selecaAll,
    dayAndMonth,
    dayAndMonthWithCheckbox,
    dayOfWeek,
    dayOfWeekWithCheckbox,
    today,
  } = useDayScaleCellStyles();

  const dayAndMonthClass = classnames(dayAndMonth, {
    [dayAndMonthWithCheckbox]: showCheckbox,
    [today]: props.today,
  });

  const dayOfWeekClass = classnames(dayOfWeek, {
    [dayOfWeekWithCheckbox]: showCheckbox,
    [today]: props.today,
  });

  return (
    <td>
      <div className={root}>
        {showCheckbox && (
          <div className={selecaAll}>
            <Checkbox checked={checked} onChange={() => changeAllCellByDay(day)} />
          </div>
        )}
        <div className={dayAndMonthClass}>
          {format(props.startDate, 'dd LLL', { locale: dateFnsLocale })}
        </div>
        <div className={dayOfWeekClass}>
          {format(props.startDate, 'EEEE', { locale: dateFnsLocale })}
        </div>
      </div>
    </td>
  );
};

const LayoutComponent = ({
  dayScaleComponent: DayScale,
  timeTableComponent: TimeTable,
  timeScaleComponent: TimeScale,
}) => {
  const { root, head, body, bodyContainer, timeScale, timeTable } = useLayoutStyles();

  return (
    <div className={root}>
      <div className={head}>
        <DayScale />
      </div>

      <div className={body}>
        <CustomScrollbar trackVerticalStyle={{ right: '10px' }}>
          <div className={bodyContainer}>
            <div className={timeScale}>
              <TimeScale />
            </div>
            <div className={timeTable}>
              <TimeTable />
            </div>
          </div>
        </CustomScrollbar>
      </div>
    </div>
  );
};

function getKey({ fromDate, toDate }) {
  return `${format(fromDate, 'yyyy-MM-dd HH:mm')}_${format(toDate, 'yyyy-MM-dd HH:mm')}`;
}

function checkAvailabilityNext(args) {
  const { startTime, endTime, register, next, defaultDuration } = args;

  for (let i = 1; i < next; i += 1) {
    const nextStartDate = add(startTime, { minutes: i * defaultDuration });
    const nextEndDate = add(endTime, { minutes: i * defaultDuration });

    const key = getKey({
      fromDate: nextStartDate,
      toDate: nextEndDate,
    });

    if (!(register[key] && register[key].is_enabled)) {
      return false;
    }
  }

  return true;
}

const checkFreeBeforeSlot = ({
  startTime,
  events,
  isAppointmentsMode,
  next,
  defaultDuration,
}) => {
  if (!isAppointmentsMode || next === 1) {
    return true;
  }

  const countArray = next - 1 >= 0 ? next - 1 : 0;

  const iterArray = new Array(countArray);

  for (let i = 1; i < next; i += 1) {
    iterArray[i - 1] = add(startTime, { minutes: i * defaultDuration });
  }

  const findedEvents = iterArray
    .map((item) =>
      events.find(
        (event) =>
          Number(item) >= Number(new Date(event.startDate)) &&
          Number(item) < Number(new Date(event.endDate))
      )
    )
    .filter((item) => item);

  return !(findedEvents.length > 0);
};

const checkFreeAfterSlot = ({ startTime, events, isAppointmentsMode }) => {
  if (!isAppointmentsMode) {
    return true;
  }

  const findedAfterSlotEvent = events.find(
    (event) =>
      Number(new Date(event.startDate)) < Number(startTime) &&
      Number(startTime) < Number(new Date(event.endDate))
  );

  return !findedAfterSlotEvent;
};

const isAvailableSlot = ({ isAppointmentsMode, events, startTime, endTime }) => {
  if (!isAppointmentsMode) {
    return true;
  }

  const formattedStartTime = format(startTime, 'yyyy-MM-dd HH:mm');

  const findedEvent = events.find(
    (event) =>
      event.startDate === formattedStartTime &&
      Number(new Date(event.startDate)) < Number(endTime) &&
      Number(startTime) <= Number(new Date(event.endDate))
  );

  return !findedEvent;
};
