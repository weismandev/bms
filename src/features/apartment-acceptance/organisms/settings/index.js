import DatePicker from 'react-datepicker';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import classNames from 'classnames';
import format from 'date-fns/format';
import { Formik, Form, FieldArray, Field } from 'formik';
import { nanoid } from 'nanoid';
import { Grid, InputAdornment, Paper, Button, IconButton, Chip } from '@mui/material';
import { Close } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  LabeledContent,
  SwitchField,
  Loader,
  ErrorMessage,
  CustomScrollbar,
} from '../../../../ui';
import { Header } from '../../atoms';
import { $defaultDuration } from '../../models/calendar.model';
import {
  $currentManagerSettings,
  submitSettings,
  changeSettingsMode,
  $settingsMode,
  $error,
  $isLoading,
} from '../../models/manager.model';
import { $isErrorDialogOpen, errorDialogVisibilityChange } from '../../models/page.model';
import { useStyles, useDaySettingsStyles } from './styles';

const $stores = combine({
  settings: $currentManagerSettings,
  mode: $settingsMode,
  error: $error,
  isErrorDialogOpen: $isErrorDialogOpen,
  isLoading: $isLoading,
  defaultDuration: $defaultDuration,
});

const { t } = i18n;

export const Settings = () => {
  const { settings, mode, error, isErrorDialogOpen, isLoading, defaultDuration } =
    useStore($stores);
  const { wrapper, toolbar, outer, inner, dayContainer } = useStyles();

  return (
    <Wrapper className={wrapper}>
      <Loader isLoading={isLoading} />
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChange(false)}
        error={error}
      />
      <Formik
        onSubmit={submitSettings}
        enableReinitialize
        initialValues={settings}
        render={({ values, resetForm }) => (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              className={toolbar}
              hidden={{ delete: true, close: true }}
              mode={mode}
              onEdit={() => changeSettingsMode('edit')}
              onCancel={() => {
                changeSettingsMode('view');
                resetForm();
              }}
            >
              <Header
                style={{ order: 40, marginLeft: 10 }}
                headerText={t('ScheduleSetting')}
              />
            </DetailToolbar>
            <div className={outer}>
              <CustomScrollbar>
                <div className={inner}>
                  <LabeledContent
                    className={dayContainer}
                    label={mode === 'edit' ? t('SetupScheduleGrid') : t('ScheduleGrid')}
                  >
                    <ul>
                      {values.days.map((day, idx) =>
                        mode === 'edit' ? (
                          <EditDaySettings
                            key={day.num}
                            defaultDuration={defaultDuration}
                            {...day}
                            parentName={`days[${idx}]`}
                          />
                        ) : (
                          <ViewDaySettings key={day.num} {...day} />
                        )
                      )}
                    </ul>
                  </LabeledContent>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

const EditDaySettings = (props) => {
  const { active = false, title, intervals, parentName, defaultDuration } = props;

  const { paper, paperActive, adornment, startAdornment, flexContainer } =
    useDaySettingsStyles();

  return (
    <Paper
      classes={{ root: classNames(paper, { [paperActive]: active }) }}
      component="li"
    >
      <Field
        name={`${parentName}.active`}
        component={SwitchField}
        label={title}
        labelPlacement="end"
        divider={false}
      />
      {active && (
        <LabeledContent label={t('SelectWorkingIntervals')}>
          <FieldArray
            name={`${parentName}.intervals`}
            render={({ push, remove }) => (
              <>
                {intervals.map((i, idx) => (
                  <Grid
                    classes={{ root: flexContainer }}
                    key={i._id}
                    container
                    wrap="nowrap"
                    alignItems="baseline"
                  >
                    <Grid style={{ marginRight: 10, flexGrow: 1 }} item>
                      <Field
                        name={`${parentName}.intervals[${idx}].time_start`}
                        render={({ field, form }) => (
                          <DatePicker
                            required
                            customInput={
                              <InputField
                                field={field}
                                form={form}
                                label={null}
                                divider={false}
                                placeholder="09:00"
                                startAdornment={
                                  <InputAdornment
                                    classes={{
                                      root: classNames(adornment, startAdornment),
                                    }}
                                    position="end"
                                  >
                                    с
                                  </InputAdornment>
                                }
                              />
                            }
                            name={field.name}
                            selected={field.value}
                            onChange={(value) => form.setFieldValue(field.name, value)}
                            locale={dateFnsLocale}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={defaultDuration}
                            timeCaption={t('Label.time')}
                            dateFormat="HH:mm"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}
                          />
                        )}
                      />
                    </Grid>
                    <Grid style={{ marginRight: 10, flexGrow: 1 }} item>
                      <Field
                        name={`${parentName}.intervals[${idx}].time_end`}
                        render={({ field, form }) => (
                          <DatePicker
                            required
                            customInput={
                              <InputField
                                field={field}
                                form={form}
                                label={null}
                                divider={false}
                                placeholder="21:00"
                                startAdornment={
                                  <InputAdornment
                                    classes={{
                                      root: classNames(adornment, startAdornment),
                                    }}
                                    position="end"
                                  >
                                    {t('to')}
                                  </InputAdornment>
                                }
                              />
                            }
                            name={field.name}
                            selected={field.value}
                            onChange={(value) => form.setFieldValue(field.name, value)}
                            locale={dateFnsLocale}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={defaultDuration}
                            timeCaption={t('Label.time')}
                            dateFormat="HH:mm"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}
                          />
                        )}
                      />
                    </Grid>
                    <Grid item>
                      <IconButton size="small" onClick={() => remove(idx)}>
                        <Close />
                      </IconButton>
                    </Grid>
                  </Grid>
                ))}
                <Button
                  color="primary"
                  onClick={() => push({ _id: nanoid(3), time_start: '', time_end: '' })}
                >
                  + {t('AddSpacing')}
                </Button>
              </>
            )}
          />
        </LabeledContent>
      )}
    </Paper>
  );
};

const ViewDaySettings = (props) => {
  const { active, title, intervals } = props;

  const { paper, paperActive, chip, chipInactive, chipActive } = useDaySettingsStyles();

  const inactiveChip = (
    <Chip classes={{ root: classNames(chip, chipInactive) }} label={t('NonWorkingDay')} />
  );

  const activeChips = intervals.map((i, idx, array) => (
    <Chip
      key={i._id}
      style={{ marginRight: idx !== array.length - 1 ? 5 : 0 }}
      classes={{ root: classNames(chip, chipActive) }}
      label={`${format(i.time_start, 'HH:mm')} - ${format(i.time_end, 'HH:mm')}`}
    />
  ));

  return (
    <Paper
      classes={{ root: classNames(paper, { [paperActive]: active }) }}
      component="li"
    >
      <LabeledContent label={title} labelPlacement="start">
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          {active ? activeChips : inactiveChip}
        </div>
      </LabeledContent>
    </Paper>
  );
};
