import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
    height: 84,
  },
  outer: {
    height: 'calc(100% - 84px)',
    padding: '0 12px 24px 24px',
  },
  inner: {
    paddingRight: 12,
  },
  dayContainer: {
    '& ul': {
      marginBottom: 0,
      '& > *:not(:last-child)': {
        marginBottom: 10,
      },
    },
  },
});

const useDaySettingsStyles = makeStyles({
  paper: {
    padding: 16,
    background: '#E7E7EC',
    borderRadius: 16,
    boxShadow: 'none',
  },
  paperActive: {
    background: '#EDF6FF',
  },
  adornment: {
    zIndex: 1,
  },
  startAdornment: {
    marginRight: 10,
  },
  flexContainer: {
    marginBottom: 12,
  },
  chip: {
    color: '#fff',
    fontSize: 13,
    lineHeight: '15px',
    fontWeight: 500,
  },
  chipInactive: {
    background: '#AAA',
  },
  chipActive: {
    background: '#1BB169',
  },
});

export { useStyles, useDaySettingsStyles };
