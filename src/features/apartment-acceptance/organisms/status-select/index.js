import { SelectControl, SelectField } from '@ui/index';
import { statuses } from '../../models/acceptance-list.model';

const StatusSelect = (props) => <SelectControl options={statuses} {...props} />;

const StatusSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<StatusSelect />} onChange={onChange} {...props} />;
};

export { StatusSelect, StatusSelectField };
