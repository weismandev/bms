import { combine } from 'effector';
import { useStore } from 'effector-react';
import {
  Wrapper,
  InputField,
  Divider,
  DetailToolbar,
  PhoneMaskedInput,
  EmailMaskedInput,
} from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import '../../models/detail.init';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
} from '../../models/detail.model';
import '../../models/page.init';
import { deleteDialogVisibilityChange } from '../../models/page.model';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Обязяательное поле'),
});

const useStyles = makeStyles((theme) => ({
  headerRoot: {
    padding: '16px 0 17px',
    minHeight: 62,
    overflowWrap: 'anywhere',
  },
  headerText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '28px',
  },
  nameBlock: {
    display: 'grid',
    gridTemplateRows: '1fr',
    gridTemplateColumns: 'repeat(3, 1fr)',
    gridGap: 16,
  },
}));

const NameInput = (props) => <InputField {...props} divider={false} />;

const $stores = combine({
  mode: $mode,
  opened: $opened,
});

export const ManagersDetail = (props) => {
  const { headerText, headerRoot, nameBlock } = useStyles();
  const { mode, opened } = useStore($stores);
  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={opened}
        onSubmit={detailSubmitted}
        render={(props) => {
          return (
            <Form>
              <DetailToolbar
                onEdit={() => changeMode('edit')}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    props.resetForm();
                    changeMode('view');
                  }
                }}
                onClose={() => changedDetailVisibility(false)}
                onDelete={() => deleteDialogVisibilityChange(true)}
                onSave={() => props.submitForm()}
                mode={mode}
              />
              <div className={headerRoot}>
                <span className={headerText}>{`
                ${props.values.surname}
                ${props.values.name}
                ${props.values.patronymic}
                `}</span>
              </div>
              <Divider gap="0 0 19px" />
              {mode === 'view' ? null : (
                <>
                  <div className={nameBlock}>
                    <Field
                      name="surname"
                      component={NameInput}
                      mode="edit"
                      label={t('Label.lastname')}
                      placeholder={t('EnterLastName')}
                    />
                    <Field
                      name="name"
                      required
                      component={NameInput}
                      mode="edit"
                      label={t('Label.name')}
                      placeholder={t('EnterYourName')}
                    />
                    <Field
                      name="patronymic"
                      component={NameInput}
                      mode="edit"
                      label={t('Label.patronymic')}
                      placeholder={t('EnterMiddleName')}
                    />
                  </div>
                  <Divider gap="24px 0 19px" />
                </>
              )}
              <Field
                name="phone"
                render={({ field, form }) => (
                  <InputField
                    field={field}
                    form={form}
                    inputComponent={PhoneMaskedInput}
                    label={t('Label.phone')}
                    placeholder={t('EnterPhone')}
                    mode={mode}
                  />
                )}
              />
              <Field
                name="email"
                render={({ field, form }) => (
                  <InputField
                    field={field}
                    form={form}
                    inputComponent={EmailMaskedInput}
                    label="E-mail"
                    placeholder={t('enterYourEmailAddress')}
                    mode={mode}
                  />
                )}
              />
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};
