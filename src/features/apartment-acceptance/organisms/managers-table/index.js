import { useMemo, useCallback } from 'react';
import { useStore } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { Template } from '@devexpress/dx-react-core';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { Wrapper, TableContainer, TableRow } from '../../../../ui';
import '../../models/detail.init';
import { $opened, open } from '../../models/detail.model';
import '../../models/table.init';
import {
  managersColumns,
  managersColumnsWidths,
  $managersTableData,
} from '../../models/table.model';
import { TableToolbar } from '../../molecules';

const { t } = i18n;

const useStyles = makeStyles({
  tableCell: {
    color: '#767676',
    fontFamily: 'Roboto, sans-serif',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '16px',
    lineHeight: '16px',
    padding: '11px 6px 12px',
    '&:first-child': {
      paddingLeft: 4,
    },
  },
  headerCell: {
    color: '#7D7D8E',
    fontWeight: '900',
    fontFamily: 'Roboto, sans-serif',
    fontSize: '1.5em',
    padding: '12px 6px',
    '&:first-child': {
      paddingLeft: 0,
    },
  },
});

const TableCell = (props) => {
  const { tableCell } = useStyles();
  const { value, ...restProps } = props;
  return <DXTable.Cell {...restProps} value={value} classes={{ cell: tableCell }} />;
};

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const TableHeaderCell = (props) => {
  const { headerCell } = useStyles();
  return <TableHeaderRow.Cell {...props} classes={{ cell: headerCell }} />;
};

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

export const ManagersTable = (props) => {
  const data = useStore($managersTableData);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        getRowId={(row) => row.id}
        rootComponent={Root}
        columns={managersColumns}
        rows={data}
      >
        <DragDropProvider />
        <DXTable
          rowComponent={Row}
          containerComponent={TableContainer}
          messages={{ noData: t('ThereAreNoManagersYet') }}
          cellComponent={TableCell}
        />
        <TableColumnResizing columnWidths={managersColumnsWidths} />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <TableHeaderRow cellComponent={TableHeaderCell} />
      </Grid>
    </Wrapper>
  );
};
