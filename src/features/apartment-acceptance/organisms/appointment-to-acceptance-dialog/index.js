import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import format from 'date-fns/format';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Dialog, Typography } from '@mui/material';
import { Done, Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { ActionButton, Toolbar } from '../../../../ui';
import '../../models/acceptance-list.init';
import { $acceptanceForm } from '../../models/acceptance-list.model';
import '../../models/calendar.init';
import {
  changeAppointmentsAcceptanceDialogVisibility,
  $isAppointmentsAcceptanceDialogOpen,
  approveAppointment,
} from '../../models/calendar.model';
import { DialogHeader } from '../../molecules';

const useStyles = makeStyles({
  paper: {
    width: '550px',
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
  },

  toolbar: {
    padding: 24,
    paddingTop: 0,
    height: 84,
  },

  text: {
    fontWeight: 500,
    fontSize: 18,
    lineHeight: '21px',
    color: '#65657B',
    marginBottom: 10,
  },

  listItem: {
    fontWeight: 500,
    fontSize: 13,
    lineHeight: '15px',
    color: '#65657B',
    '&::before': {
      fontSize: 15,
      content: "' — '",
      color: '#0394E3',
      lineHeight: '15px',
    },
  },

  button: {
    width: '45% !important',
  },
});

const $stores = combine({
  open: $isAppointmentsAcceptanceDialogOpen,
  acceptanceForm: $acceptanceForm,
});

export const AppointmentToAcceptanceDialog = (props) => {
  const { t } = useTranslation();
  const { open, acceptanceForm } = useStore($stores);
  const { paper, toolbar, text, listItem, button } = useStyles();

  const close = () => changeAppointmentsAcceptanceDialogVisibility({ isOpen: false });

  const question = open.start
    ? `${t('ConfirmEntryOfObjectsOn')} ${format(open.start, 'dd MMMM в HH:mm', {
        locale: dateFnsLocale,
      })}?`
    : t('CanConfirmRecordsObject');

  return (
    <Dialog classes={{ paper }} open={open.isOpen} onClose={close}>
      <DialogHeader headerText={t('registrationAacceptance')} close={close} />
      <div style={{ padding: '0 24px' }}>
        <Typography classes={{ root: text }}>{question}</Typography>
        <ul>
          {acceptanceForm.property.map((i) => (
            <Typography key={i.type.title} classes={{ root: listItem }} component="li">
              {i.type.title}
            </Typography>
          ))}
        </ul>
      </div>
      <Toolbar className={toolbar}>
        <ActionButton kind="positive" className={button} onClick={approveAppointment}>
          <Done />
          {t('confirm')}
        </ActionButton>
        <ActionButton kind="negative" className={button} onClick={close}>
          <Close />
          {t('cancel')}
        </ActionButton>
      </Toolbar>
    </Dialog>
  );
};
