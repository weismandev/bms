import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tab, Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { Wrapper, Tabs } from '../../../../ui';
import { HouseSelect } from '../../../house-select';
import '../../models/acceptance-list.init';
import {
  $building,
  changeBuilding,
  $buildingsOptions,
} from '../../models/acceptance-list.model';
import '../../models/page.init';
import { changeSectionTab, $currentSectionTab, $tabsList } from '../../models/page.model';
import { TabbarWithEditDialog } from '../tabbar-with-edit-dialog';

const { t } = i18n;

const useStyles = makeStyles({
  wrapper: {
    height: '100%',
    width: '100%',
    overflow: 'inherit',
  },
  toolbar: {
    height: '100%',
    padding: '0 24px',
  },
});

const $stores = combine({
  currentSectionTab: $currentSectionTab,
  tabsList: $tabsList,
  building: $building,
  buildingOptions: $buildingsOptions,
});

export const SectionHeader = (props) => {
  const { wrapper, toolbar } = useStyles();
  const { currentSectionTab, tabsList, building, buildingOptions } = useStore($stores);

  return (
    <Wrapper className={wrapper}>
      <Grid classes={{ root: toolbar }} container wrap="nowrap" alignItems="center">
        <Grid item>
          <Tabs
            onChange={(e, tab) => changeSectionTab(tab)}
            currentTab={currentSectionTab}
            options={tabsList}
            tabEl={<Tab />}
          />
        </Grid>

        <Grid item style={{ margin: '0 auto' }}>
          <TabbarWithEditDialog />
        </Grid>

        {buildingOptions?.length ? (
          <>
            <Grid item style={{ width: 300, margin: '0 auto' }}>
              <HouseSelect
                label={null}
                include={buildingOptions}
                divider={false}
                value={building}
                onChange={changeBuilding}
                firstAsDefault
                placeholder={t('selectObject')}
              />
            </Grid>
          </>
        ) : null}
      </Grid>
    </Wrapper>
  );
};
