import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore, useList } from 'effector-react';
import classNames from 'classnames';
import format from 'date-fns/format';
import { List as MuiList, ListItem, Typography, Grid, Chip, Button } from '@mui/material';
import {
  MailOutlined,
  CloseOutlined,
  CalendarTodayOutlined,
  DoneAllOutlined,
  ErrorOutlineOutlined,
  DoneOutlined,
  PersonOutlineOutlined,
} from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import {
  Wrapper,
  Toolbar,
  Divider,
  SearchInput,
  Loader,
  ErrorMsg as ErrorMessage,
  FoundInfo,
  CustomScrollbar,
  FilterButton,
} from '@ui/index';
import { useColorClassByStatus } from '../../lib';
import '../../models/acceptance-list.init';
import {
  $listData,
  open,
  $opened,
  changeSearch,
  $search,
  changeAppartmentState,
  changeAcceptanceDialogVisibility,
  $isLoading,
  $error,
  $count,
  toIssueClicked,
} from '../../models/acceptance-list.model';
import '../../models/calendar.init';
import { appointmentsModeApi } from '../../models/calendar.model';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filters.model';
import { InvitesPanel, ReturnToIssueDialog } from '../../molecules';
import { AcceptanceDialog } from '../acceptance-dialog';

const cBlue = '#0394E3';
const cGreyLight = '#E1EBFF';
const cGreyDark = '#65657B';

const useStyles = makeStyles((theme) => {
  const { button: buttonTheme } = theme.status;

  const buttonStyles = ['basic', 'positive', 'dark', 'negative'].reduce(
    (acc, kind) => ({
      ...acc,
      [kind]: {
        backgroundColor: buttonTheme[kind].default,
        '&:hover': {
          backgroundColor: buttonTheme[kind].hover,
        },
        '&:pressed': {
          backgroundColor: buttonTheme[kind].pressed,
        },
        '&:active': {
          backgroundColor: buttonTheme[kind].hover,
        },
        '&:disabled': {
          backgroundColor: buttonTheme[kind].disabled,
        },
      },
    }),
    {}
  );

  const styleObject = {
    item: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'space-between',
      height: 'auto',
      padding: '24px 24px 15px 24px',
    },
    selected: {
      background: `${cGreyLight} !important`,
      "& + *[role='separator']": {
        background: `${cGreyLight} !important`,
      },
      "& *[class*='headerText']": {
        color: `${cBlue} !important`,
      },
    },
    divider: {
      padding: '0 24px',
      height: 'auto',
      '& > *': {
        width: '100%',
      },
    },
    headerText: {
      fontSize: '18px',
      lineHeight: '21px',
      fontWeight: 900,
      color: cGreyDark,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: '100%',
    },
    statusIcon: {
      width: 14,
      height: 14,
      color: cGreyDark,
      marginRight: 5,
    },
    statusText: {
      fontSize: 14,
      lineHeight: '16px',
      color: cGreyDark,
    },
    statusContainer: {
      display: 'flex',
      alignItems: 'center',
    },
    chip: {
      color: '#fff',
      fontSize: 13,
      lineHeight: '15px',
      marginLeft: 4,
      padding: '0 10px',
      height: 20,
    },
    residentText: {
      color: '#7D7D8E',
      fontSize: '16px',
      lineHeight: '19px',
      fontWeight: 400,
      display: 'flex',
      alignItems: 'center',
      wrap: 'nowrap',
    },
    button: {
      height: '30px',
      padding: '0 10px',
      alignSelf: 'flex-end',
      minWidth: '200px',
      borderRadius: '17px',
      color: '#fff',
      marginTop: 4,
      '&:first-of-type': {
        marginTop: 0,
      },
    },
    ...buttonStyles,
    wrapperInfoIcon: {
      display: 'flex',
    },
    infoIconText: {
      paddingLeft: 5,
      fontWeight: 400,
      fontSize: 14,
    },
    infoIcon: {
      width: 20,
      height: 20,
    },
  };

  return styleObject;
});

const ItemStatus = ({ status, lastAppointmentStartDate }) => {
  const { statusIcon, statusText, statusContainer } = useStyles();
  const { color } = useColorClassByStatus(status);

  const iconClassName = classNames(statusIcon, color);
  const textClassName = classNames(statusText, color);

  const mapStatusToIcon = {
    approved: <DoneAllOutlined className={iconClassName} />,
    not_approved: <CloseOutlined className={iconClassName} />,
    issues: <ErrorOutlineOutlined className={iconClassName} />,
    invite_sent: <MailOutlined className={iconClassName} />,
    scheduled: <CalendarTodayOutlined className={iconClassName} />,
    issues_closed: <DoneOutlined className={iconClassName} />,
  };

  const icon = mapStatusToIcon[status.slug] || null;

  return (
    <span className={statusContainer}>
      {icon} <span className={textClassName}>{status.title}</span>
      {lastAppointmentStartDate && (
        <span
          className={textClassName}
          style={{ marginLeft: 5 }}
        >{`(${lastAppointmentStartDate})`}</span>
      )}
    </span>
  );
};

const HeaderChip = ({ label, status }) => {
  const { chip } = useStyles();
  const { bcg } = useColorClassByStatus(status);

  const chipClassName = classNames(chip, bcg);

  return (
    <Chip label={label} size="small" classes={{ root: chipClassName }} component="span" />
  );
};

const ItemHeader = ({ header, numbers = [], status }) => {
  const { headerText } = useStyles();

  const chips = numbers.map((i, idx) => (
    <Grid item key={idx}>
      <HeaderChip label={i} status={status} />
    </Grid>
  ));

  return (
    <Grid container alignItems="center">
      <Grid item>
        <Typography className={headerText}>{header}</Typography>
      </Grid>
      {chips}
    </Grid>
  );
};

const ItemResidentInfo = ({ resident }) => {
  const { residentText } = useStyles();

  return (
    <Typography classes={{ root: residentText }}>
      <PersonOutlineOutlined style={{ height: 20 }} />
      {resident}
    </Typography>
  );
};

const ItemInfoIcons = ({ context }) => {
  const classes = useStyles({ context });
  const { t } = useTranslation();

  if (!context) {
    return null;
  }

  const passportConfig = context.passport_not_changed
    ? {
        color: '#1BB169',
        text: t('PassportValid'),
        icon: <DoneAllOutlined classes={{ root: classes.infoIcon }} />,
      }
    : {
        color: '#EB5757',
        text: t('PassportChanged'),
        icon: <ErrorOutlineOutlined classes={{ root: classes.infoIcon }} />,
      };

  const passport = (
    <div
      className={classes.wrapperInfoIcon}
      style={{ paddingRight: 15, color: passportConfig.color }}
    >
      {passportConfig.icon}
      <Typography classes={{ root: classes.infoIconText }}>
        {passportConfig.text}
      </Typography>
    </div>
  );

  const ownerConfig = context.accept_by_owner
    ? {
        color: '#1BB169',
        text: t('AcceptancePerson'),
        icon: <DoneAllOutlined classes={{ root: classes.infoIcon }} />,
      }
    : {
        color: '#EB5757',
        text: t('AcceptanceProxy'),
        icon: <ErrorOutlineOutlined classes={{ root: classes.infoIcon }} />,
      };

  const owner = (
    <div className={classes.wrapperInfoIcon} style={{ color: ownerConfig.color }}>
      {ownerConfig.icon}
      <Typography classes={{ root: classes.infoIconText }}>{ownerConfig.text}</Typography>
    </div>
  );

  return (
    <div className={classes.wrapperInfoIcon} style={{ marginBottom: 10 }}>
      {passport}
      {owner}
    </div>
  );
};

const Item = (props) => {
  const { children, ...rest } = props;
  const { item, divider, selected } = useStyles();

  return (
    <>
      <ListItem classes={{ root: item, selected }} component="li" {...rest}>
        {children}
      </ListItem>
      <ListItem classes={{ root: divider }} role="separator">
        <Divider gap="0" />
      </ListItem>
    </>
  );
};

const $stores = combine({
  opened: $opened,
  search: $search,
  isLoading: $isLoading,
  error: $error,
  count: $count,
  isFilterOpen: $isFilterOpen,
});

const ApartmentItem = ({
  id: apartment_id,
  selected,
  resident,
  number,
  type,
  state,
  extra,
  events,
}) => {
  const { t } = useTranslation();
  const directApproveStatuses = ['not_approved', 'issues_closed', 'invite_sent'];

  const mapStatusToButtonData = {
    approved: {
      buttonLabel: t('ChangeResult'),
      kind: 'negative',
      action: () => toIssueClicked({ apartment_id }),
    },
    scheduled: {
      buttonLabel: t('SpecifyResults'),
      kind: 'positive',
      action: () => {
        open(apartment_id);
        changeAcceptanceDialogVisibility(true);
      },
    },
    not_approved: {
      buttonLabel: t('sendInvitation'),
      kind: 'dark',
      action: () =>
        changeAppartmentState({
          apartment_id,
          resident_id: resident && resident.id,
          state: 'invite_sent',
        }),
    },
    issues_closed: {
      buttonLabel: t('sendInvitations'),
      kind: 'dark',
      action: () =>
        changeAppartmentState({
          apartment_id,
          resident_id: resident && resident.id,
          state: 'invite_sent',
        }),
    },
    invite_sent: {
      buttonLabel: t('SignUpForAcceptance'),
      kind: 'basic',
      action: () => appointmentsModeApi.open({ apartment_id }),
    },
    issues: {
      buttonLabel: t('problemsHaveBeenEliminated'),
      kind: 'basic',
      action: () => changeAppartmentState({ apartment_id, state: 'issues_closed' }),
    },
  };

  const buttonLabel = mapStatusToButtonData[state.slug].buttonLabel;
  const buttonAction = mapStatusToButtonData[state.slug].action;
  const buttonKind = mapStatusToButtonData[state.slug].kind;

  const formattedAppointment =
    Array.isArray(events) &&
    [...events].sort((a, b) => new Date(a.starts_at) - new Date(b.starts_at));
  const lastAppointment = formattedAppointment[formattedAppointment.length - 1];
  const lastAppointmentStartDate =
    lastAppointment && format(new Date(lastAppointment.starts_at), 'dd.MM.yy');

  const classes = useStyles();

  return (
    <Item selected={selected}>
      <Grid container direction="column">
        <Grid item>
          <Grid container alignItems="center" justifyContent="space-between">
            <Grid item>
              <ItemHeader header={type.title} status={state} numbers={[number]} />
            </Grid>
            <Grid item>
              <ItemStatus
                apartment_id={apartment_id}
                resident_id={resident && resident.id}
                status={state}
                lastAppointmentStartDate={lastAppointmentStartDate}
              />
            </Grid>
          </Grid>
        </Grid>
        {Array.isArray(extra) && (
          <Grid item style={{ marginBottom: 10 }}>
            {extra.map(({ state, number, id, type }) => (
              <Grid item key={id}>
                <ItemHeader header={type.title} status={state} numbers={[number]} />
              </Grid>
            ))}
          </Grid>
        )}
        {resident && resident.name && (
          <Grid item style={{ marginBottom: 10 }}>
            <ItemResidentInfo resident={resident.name} />
          </Grid>
        )}
        <ItemInfoIcons context={lastAppointment?.context} />
        {resident && resident.id && buttonLabel && (
          <Button
            className={classNames(classes[buttonKind], classes.button)}
            onClick={(e) => {
              e.stopPropagation();
              buttonAction();
            }}
            disableRipple
          >
            {buttonLabel}
          </Button>
        )}
        {directApproveStatuses.includes(state.slug) && (
          <Button
            className={classNames(classes.positive, classes.button)}
            onClick={(e) => {
              e.stopPropagation();
              mapStatusToButtonData.scheduled.action();
            }}
            disableRipple
          >
            {t('SpecifyResults')}
          </Button>
        )}
      </Grid>
    </Item>
  );
};

const AcceptanceList = () => {
  const { t } = useTranslation();
  const { opened, search, isLoading, error, count, isFilterOpen } = useStore($stores);

  const list = useList($listData, {
    keys: [opened],
    fn: (props) => {
      const { id } = props;
      const selected = opened && String(opened.id) === String(id);

      return <ApartmentItem selected={selected} {...props} />;
    },
  });

  return (
    <Wrapper style={{ height: '100%', position: 'relative' }}>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <AcceptanceDialog />
      <ReturnToIssueDialog />
      <Toolbar
        style={{
          padding: '24px',
        }}
      >
        <FilterButton
          disabled={isFilterOpen}
          onClick={() => changedFilterVisibility(true)}
          style={{ marginRight: 10 }}
        />
        <FoundInfo
          countTitle={t('AcceptanceFound')}
          count={count}
          style={{ marginRight: 10, marginTop: 5 }}
        />
        <SearchInput
          onChange={(e) => changeSearch(e.target.value)}
          handleClear={() => changeSearch('')}
          value={search}
          name="search"
          style={{
            marginTop: 2,
          }}
        />
      </Toolbar>
      <CustomScrollbar
        style={{
          height: 'calc(100% - 82px - 64px)',
        }}
        autoHide
      >
        <MuiList disablePadding>{list}</MuiList>
      </CustomScrollbar>
      <InvitesPanel />
    </Wrapper>
  );
};

export { AcceptanceList };
