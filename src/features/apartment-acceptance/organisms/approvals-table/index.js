import { useMemo, useCallback } from 'react';
import { useStore } from 'effector-react';
import { Wrapper, TableContainer, TableRow, TableHeaderCell, ActionButton } from '@ui';
import { Chip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Template } from '@devexpress/dx-react-core';
import { DataTypeProvider } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { openAcceptance } from '../../models/acceptance-list.model';
import '../../models/detail.init';
import { $opened, open } from '../../models/detail.model';
import '../../models/page.init';
import '../../models/table.init';
import {
  approvalsColumns,
  approvalsColumnsWidths,
  $approvalsTableData,
} from '../../models/table.model';
import { TableToolbar } from '../../molecules';

const { t } = i18n;

const useStyles = makeStyles({
  tableCell: {
    color: '#767676',
    fontFamily: 'Roboto, sans-serif',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '16px',
    lineHeight: '16px',
    padding: '4px 6px 5px',
    '&:first-child': {
      paddingLeft: 4,
    },
  },
  buildLabel: {
    fontFamily: 'Roboto, sans-serif',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '13px',
    lineHeight: '15px',
    color: '#65657B',
  },
  buildColor: {
    backgroundColor: '#E7E7EC',
  },
  buildContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  buildRoot: {
    marginBottom: 3,
    '&:last-child': {
      marginBottom: 0,
    },
  },
  stateLabel: {
    fontFamily: 'Roboto, sans-serif',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '13px',
    lineHeight: '15px',
    color: '#FFF',
  },
});

const BuildsFormatter = ({ value }) => {
  const { buildLabel, buildColor, buildContainer, buildRoot } = useStyles();
  const builds = value.map(({ id, label }) => (
    <Chip
      classes={{ root: buildRoot, label: buildLabel, colorPrimary: buildColor }}
      key={id}
      label={label}
      size="small"
      color="primary"
    />
  ));
  return <div className={buildContainer}>{builds}</div>;
};

const BuildsProvider = (props) => (
  <DataTypeProvider formatterComponent={BuildsFormatter} {...props} />
);

const StatisticFormatter = ({ value }) => {
  const { all, approved } = value;
  return (
    <>
      <span style={{ color: '#1BB169' }}>{approved}</span>
      <span>{` / ${all}`}</span>
    </>
  );
};

const StatisticProvider = (props) => (
  <DataTypeProvider formatterComponent={StatisticFormatter} {...props} />
);

const StatusFormatter = ({ value }) => {
  const { stateLabel } = useStyles();
  const { slug, title } = value;

  const stateColor = (slug) => {
    switch (slug) {
      case 'new':
        return '#0394E3';
      case 'in_progress':
        return '#F2994A';
      case 'done':
        return '#1BB169';
      default:
        return '#E7E7EC';
    }
  };
  return (
    <Chip
      classes={{ label: stateLabel }}
      style={{ backgroundColor: stateColor(slug) }}
      label={title}
      size="small"
    />
  );
};

const StatusProvider = (props) => (
  <DataTypeProvider formatterComponent={StatusFormatter} {...props} />
);

const ActionFormatter = ({ value }) => (
  <ActionButton
    onClick={(e) => {
      e.stopPropagation();
      openAcceptance(value);
    }}
  >
    {t('GoToAcceptance')}
  </ActionButton>
);

const ActionProvider = (props) => (
  <DataTypeProvider formatterComponent={ActionFormatter} {...props} />
);

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

const TableCell = (props) => {
  const { tableCell } = useStyles();
  const { value, ...restProps } = props;
  return <DXTable.Cell {...restProps} value={value} classes={{ cell: tableCell }} />;
};

export const ApprovalsTable = () => {
  const data = useStore($approvalsTableData);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        getRowId={(row) => row.id}
        rootComponent={Root}
        columns={approvalsColumns}
        rows={data}
      >
        <BuildsProvider for={['buildings']} />
        <StatisticProvider for={['statistics']} />
        <StatusProvider for={['state']} />
        <ActionProvider for={['actions']} />
        <DragDropProvider />
        <DXTable
          rowComponent={Row}
          containerComponent={TableContainer}
          messages={{ noData: t('ThereAreNoAcceptancesYet') }}
          cellComponent={TableCell}
        />
        <TableColumnResizing columnWidths={approvalsColumnsWidths} />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <TableHeaderRow cellComponent={TableHeaderCell} />
      </Grid>
    </Wrapper>
  );
};
