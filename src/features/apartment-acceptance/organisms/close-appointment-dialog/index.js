import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Dialog } from '@mui/material';
import { Done, Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { ActionButton, Toolbar } from '../../../../ui';
import '../../models/calendar.init';
import {
  $isAppointmentsCloseDialogOpen,
  appointmentsModeApi,
  changeAppointmentsCloseDialogVisibility,
} from '../../models/calendar.model';
import { DialogHeader } from '../../molecules';

const { t } = i18n;

const useStyles = makeStyles({
  paper: {
    width: '330px',
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
  },

  toolbar: {
    padding: 24,
    paddingTop: 0,
    height: 84,
  },
});

const $stores = combine({ open: $isAppointmentsCloseDialogOpen });

export const CloseAppointmentDialog = (props) => {
  const { open } = useStore($stores);
  const { paper, toolbar } = useStyles();

  const close = () => changeAppointmentsCloseDialogVisibility(false);

  return (
    <Dialog classes={{ paper }} open={open} onClose={close}>
      <DialogHeader headerText={t('CloseTimeAndDatePicker')} close={close} />
      <Toolbar className={toolbar}>
        <ActionButton
          kind="positive"
          style={{ width: '45%' }}
          onClick={() => {
            appointmentsModeApi.close();
            close();
          }}
        >
          <Done />
          {t('yes')}
        </ActionButton>
        <ActionButton
          kind="negative"
          style={{ width: '45%' }}
          onClick={() => {
            close();
          }}
        >
          <Close />
          {t('no')}
        </ActionButton>
      </Toolbar>
    </Dialog>
  );
};
