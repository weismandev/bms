import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tab, Grid } from '@mui/material';
import { Tabs } from '../../../../ui';
import '../../models/manager.init';
import {
  changeManagerTab,
  $currentManagerTab,
  $managersTabs,
} from '../../models/manager.model';

const $stores = combine({
  currentManagerTab: $currentManagerTab,
  managersTabs: $managersTabs,
});

export const TabbarWithEditDialog = (props) => {
  const { currentManagerTab, managersTabs } = useStore($stores);

  return (
    <Grid container alignItems="center" wrap="nowrap">
      <Grid item>
        <Tabs
          style={{ marginRight: 10, maxWidth: 480 }}
          tabEl={<Tab />}
          onChange={(e, tab) => changeManagerTab(tab)}
          currentTab={currentManagerTab}
          options={managersTabs}
        />
      </Grid>
    </Grid>
  );
};
