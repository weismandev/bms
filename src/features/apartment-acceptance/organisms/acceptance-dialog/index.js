import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import {
  Dialog,
  RadioGroup,
  Grid,
  FormControlLabel,
  Radio,
  Typography,
} from '@mui/material';
import { SaveOutlined } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { LabeledContent, ActionButton, Divider, Toolbar } from '../../../../ui';
import '../../models/acceptance-list.init';
import {
  changeAcceptanceDialogVisibility,
  $isAcceptanceDialogOpen,
  $opened,
  $acceptanceForm,
  submitAcceptanceForm,
} from '../../models/acceptance-list.model';
import { DialogHeader } from '../../molecules';

const useStyles = makeStyles({
  paper: {
    width: '550px',
    borderRadius: 15,
    boxShadow: '0px 4px 15px #6A6A6E',
  },
  label: {
    color: '#65657B',
    fontSize: 13,
    lineHeight: '15px',
    fontWeight: 500,
  },
  toolbar: {
    padding: 24,
    height: 84,
  },
});

const $stores = combine({ open: $isAcceptanceDialogOpen, opened: $opened });

export const AcceptanceDialog = () => {
  const { open, opened } = useStore($stores);
  const { paper } = useStyles();

  const close = () => changeAcceptanceDialogVisibility(false);

  return (
    <Dialog classes={{ paper }} open={open} onClose={close}>
      <DialogHeader headerText={opened && opened.type.title} close={close} />
      <AcceptanceForm />
    </Dialog>
  );
};

const AcceptanceForm = () => {
  const formValues = useStore($acceptanceForm);
  const { label, toolbar } = useStyles();
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={formValues}
      onSubmit={submitAcceptanceForm}
      render={({ values }) => (
        <Form>
          <LabeledContent
            label={
              <span style={{ paddingLeft: 24 }}>{t('SpecifyResultAcceptance')}</span>
            }
          >
            {values.property.map((i, idx) => {
              const name = `property[${idx}].state`;

              return (
                <div key={i._id}>
                  <Grid
                    container
                    alignItems="center"
                    justifyContent="space-between"
                    style={{ padding: '0 24px' }}
                  >
                    <Grid item>
                      <Typography classes={{ root: label }}>{i.type.title}</Typography>
                    </Grid>
                    <Grid item>
                      <Field
                        name={name}
                        render={({ field }) => (
                          <RadioGroup
                            name={field.name}
                            value={field.value}
                            onChange={field.onChange}
                          >
                            <Grid container>
                              <Grid item>
                                <FormControlLabel
                                  classes={{ label }}
                                  value="approved"
                                  control={<Radio />}
                                  label={t('ConfirmedBy')}
                                />
                              </Grid>
                              <Grid item>
                                <FormControlLabel
                                  classes={{ label }}
                                  value="issues"
                                  control={<Radio />}
                                  label={t('haveComments')}
                                />
                              </Grid>
                            </Grid>
                          </RadioGroup>
                        )}
                      />
                    </Grid>
                  </Grid>
                  <Divider />
                </div>
              );
            })}
          </LabeledContent>
          <Toolbar className={toolbar}>
            <ActionButton kind="positive" type="submit">
              <SaveOutlined />
              {t('Save')}
            </ActionButton>
          </Toolbar>
        </Form>
      )}
    />
  );
};
