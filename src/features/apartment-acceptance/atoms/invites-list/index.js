import { useList } from 'effector-react';
import {
  Avatar,
  Checkbox,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { Home } from '@mui/icons-material';
import { makeStyles } from '@mui/styles';
import noAvatar from '@img/no_avatar.png';
import i18n from '@shared/config/i18n';
import { $canBeInvited, inviteToggle } from '../../models/invites.model';

const { t } = i18n;

const classes = makeStyles((theme) => ({
  item: {
    display: 'grid',
    gridTemplateColumns: 'auto auto minmax(200px, 400px) auto max-content auto',
  },
  avatar: {
    backgroundColor: theme.palette.primary.light,
  },
}));

const InvitesListItems = () =>
  useList($canBeInvited, {
    keys: [],
    fn: ({ id, state, type, resident, checked, number = null }) => {
      const { item, avatar } = classes();
      return (
        <ListItemButton
          classes={{ root: item }}
          component="li"
          onClick={() => inviteToggle(id)}
          divider
        >
          <ListItemIcon>
            <Checkbox edge="start" checked={checked} tabIndex={-1} disableRipple />
          </ListItemIcon>
          <ListItemAvatar>
            <Avatar
              alt={`${resident?.name || t('noAvatar')}`}
              src={resident?.avatar || noAvatar}
            />
          </ListItemAvatar>
          <ListItemText primary={`${resident.name || '-'}`} secondary={t('owner')} />
          <ListItemAvatar color="error">
            <Avatar classes={{ root: avatar }}>
              <Home />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={`${
              type?.title || (type?.slug && `тип: ${type.slug}`) || t('ofUnknownType')
            }, ${number || t('withoutNumber')}`}
            secondary={`${state?.title || t('area')} `}
          />
        </ListItemButton>
      );
    },
  });

export const InvitesList = (props) => (
  <List>
    <InvitesListItems {...props} />
  </List>
);
