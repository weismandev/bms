import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Badge, Button } from '@mui/material';
import { Person } from '@mui/icons-material';
import { openInviting, $canBeInvited } from '../../models/invites.model';

export const InvitesButton = () => {
  const canBeInvited = useStore($canBeInvited);
  const status = Boolean(canBeInvited.length);
  const { t } = useTranslation();

  return (
    <Button
      onClick={(e) => {
        e.stopPropagation();
        openInviting();
      }}
      variant="contained"
      disabled={!status}
    >
      {t('INVITETORECEPTION')}&nbsp;
      {status && (
        <Badge badgeContent={canBeInvited.length} color={status ? 'error' : 'default'}>
          <Person />
        </Badge>
      )}
    </Button>
  );
};
