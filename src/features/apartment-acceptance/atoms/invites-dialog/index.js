import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@mui/material';
import { Loader } from '@ui/atoms';
import { InvitesList } from '../../atoms';
import {
  closeInviting,
  submitIinvite,
  $canBeInvited,
  $isInvitingOpened,
  $isLoading,
} from '../../models/invites.model';

const $dialog = combine({
  canBeInvited: $canBeInvited,
  isInvitingOpened: $isInvitingOpened,
  isLoading: $isLoading,
});

export const InvitesDialog = () => {
  const { t } = useTranslation();
  const { canBeInvited, isInvitingOpened, isLoading } = useStore($dialog);
  const isSubmitAllowed = !canBeInvited.some(({ checked }) => checked);

  return (
    <Dialog open={isInvitingOpened} maxWidth="xl">
      <DialogTitle>{t('sendingInvitationsReception')}</DialogTitle>
      <DialogContent>
        <Loader isLoading={isLoading} />
        <DialogContentText>{t('selectFromListOwners')}</DialogContentText>
        <InvitesList />
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          disabled={isSubmitAllowed}
          onClick={submitIinvite}
        >
          {t('send')}
        </Button>
        <Button variant="contained" color="error" onClick={closeInviting}>
          {t('cancel')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
