import { useTranslation } from 'react-i18next';
import FileSaver from 'file-saver';
import { InputField, ActionButton } from '../../../../ui';

export const ReminderField = ({ field, form, mode }) => {
  const { t } = useTranslation();
  const hasReminderUrl = Boolean(field.value);

  return mode === 'view' ? (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <ActionButton
        style={{ width: '80%' }}
        disabled={!hasReminderUrl}
        onClick={(e) => {
          e.preventDefault();
          FileSaver.saveAs(field.value, 'Acceptance_reminder');
        }}
      >
        {hasReminderUrl
          ? t('openMemoAboutAcceptanceEstate')
          : t('receiptAcceptanceRealEstateisnotAttached')}
      </ActionButton>
    </div>
  ) : (
    <InputField
      label={t('MemoAcceptanceProperty')}
      placeholder={t('EnterLinkFile')}
      field={field}
      form={form}
    />
  );
};
