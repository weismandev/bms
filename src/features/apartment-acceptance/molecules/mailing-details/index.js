import DatePicker from 'react-datepicker';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Field } from 'formik';
import { Reply } from '@mui/icons-material';
import EmailInput from '@components/molecules/EmailInput';
import { Loader, InputField, ActionButton, SelectField } from '../../../../ui';
import '../../models/detail.init';
import { $mode, $opened } from '../../models/detail.model';
import '../../models/mailing-detail.init';
import { sendTestEmail, $isLoading } from '../../models/mailing-detail.model';

const $stores = combine({
  mode: $mode,
  opened: $opened,
  isLoading: $isLoading,
});

export const MailingDetails = (props) => {
  const { t } = useTranslation();
  const { mode, isLoading } = useStore($stores);

  return (
    <>
      <Loader isLoading={isLoading} />
      <Field
        name="emails"
        component={SelectField}
        kind="creatable"
        isMulti
        options={[]}
        components={{
          DropdownIndicator: null,
        }}
        formatCreateLabel={(inputValue) => `${t('add')} "${inputValue}"`}
        mode={mode}
        label={t('recipients')}
        placeholder={t('enterEmailAddresses')}
      />
      <Field
        name="send_time"
        render={({ field, form }) => {
          return (
            <DatePicker
              placeholderText={t('chooseTime')}
              readOnly={mode === 'view'}
              customInput={
                <InputField
                  field={field}
                  form={form}
                  label={t('sendNextDayScheduleTo')}
                  divider
                />
              }
              name={field.name}
              selected={field.value}
              onChange={(value) => form.setFieldValue(field.name, value)}
              locale={dateFnsLocale}
              showTimeSelect
              showTimeSelectOnly
              timeIntervals={15}
              timeCaption={t('Label.time')}
              dateFormat="HH:mm"
            />
          );
        }}
      />
      <Field
        name="test_email"
        label={t('testEmailAddress')}
        placeholder={t('enterYourEmailAddress')}
        mode={mode}
        component={EmailInput}
      />
      {props.values.test_email && mode === 'view' ? (
        <ActionButton
          style={{ margin: '24px 0 16px', alignSelf: 'center' }}
          kind="basic"
          onClick={sendTestEmail}
        >
          <Reply style={{ transform: 'scaleX(-1)', marginRight: '6px' }} />
          {t('sendTestEmail')}
        </ActionButton>
      ) : null}
    </>
  );
};
