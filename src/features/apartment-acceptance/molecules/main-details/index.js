import DatePicker from 'react-datepicker';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { subDays } from 'date-fns';
import { Field, FieldArray } from 'formik';
import { Button } from '@mui/material';
import { Add } from '@mui/icons-material';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import {
  InputField,
  BaseInputLabel,
  CloseButton,
  SelectField,
  Divider,
  PhoneMaskedInput,
} from '../../../../ui';
import { ManagersSelectField } from '../../../managers-select';
import { ReminderField } from '../../atoms';
import {
  $mode,
  typeOptions,
  durationOptions,
  $modeWithCheckAcceptenceStarted,
} from '../../models/detail.model';
import '../../models/page.init';
import { $buildings } from '../../models/page.model';

const $stores = combine({
  endsAtMode: $mode,
  buildings: $buildings,
  mode: $modeWithCheckAcceptenceStarted,
});

const durationRowStyle = {
  display: 'grid',
  gridTemplateRows: '1fr',
  gridTemplateColumns: '254px 178px 24px',
  gridGap: 12,
};

export const MainDetails = (props) => {
  const { t } = useTranslation();
  const { mode, endsAtMode, buildings } = useStore($stores);
  const { values } = props;

  const isNew = !values.id;

  return (
    <>
      <Field
        name="buildings"
        mode={mode}
        component={SelectField}
        options={buildings}
        label={t('objects')}
        placeholder={t('selectObjects')}
        required
        isMulti
      />
      <Field
        name="starts_at"
        render={({ field, form }) => {
          return (
            <DatePicker
              required
              placeholderText={t('selectDate')}
              readOnly={mode === 'view'}
              customInput={
                <InputField
                  field={field}
                  form={form}
                  label={t('acceptanceStartDate')}
                  divider
                />
              }
              name={field.name}
              selected={field.value}
              onChange={(value) => form.setFieldValue(field.name, value)}
              locale={dateFnsLocale}
              minDate={subDays(new Date(), 0)}
              maxDate={values.ends_at}
              dateFormat="dd.MM.yyyy"
            />
          );
        }}
      />
      <Field
        name="ends_at"
        render={({ field, form }) => {
          return (
            <DatePicker
              required
              placeholderText={t('selectDate')}
              readOnly={endsAtMode === 'view'}
              customInput={
                <InputField
                  field={field}
                  form={form}
                  label={t('acceptanceEndDate')}
                  divider
                />
              }
              name={field.name}
              selected={field.value}
              onChange={(value) => form.setFieldValue(field.name, value)}
              locale={dateFnsLocale}
              minDate={props.values.starts_at || new Date()}
              dateFormat="dd.MM.yyyy"
            />
          );
        }}
      />
      <Field
        name="default_duration"
        label={t('basicDurationOneAcceptance')}
        placeholder={t('selectDuration')}
        component={SelectField}
        mode={mode}
        options={durationOptions}
      />
      <Field
        name="tomorrow_appointment_closes_at"
        render={({ field, form }) => {
          return (
            <DatePicker
              placeholderText={t('chooseTime')}
              readOnly={endsAtMode === 'view'}
              required
              customInput={
                <InputField
                  field={field}
                  form={form}
                  label={
                    endsAtMode === 'edit'
                      ? t('CloseAppointmentTomorrowAfterSpecifiedTime')
                      : t('RegistrationTomorrowClosedAfter')
                  }
                  divider
                />
              }
              name={field.name}
              selected={field.value}
              onChange={(value) => form.setFieldValue(field.name, value)}
              locale={dateFnsLocale}
              showTimeSelect
              showTimeSelectOnly
              timeIntervals={15}
              timeCaption={t('Label.time')}
              dateFormat="HH:mm"
            />
          );
        }}
      />
      {(mode === 'view' && props.values.durations.length) || mode === 'edit' ? (
        <div>
          <BaseInputLabel>
            {mode === 'edit'
              ? t('SelectObjectsWithDifferentDuration')
              : t('ObjectsWithDifferentDuration')}
          </BaseInputLabel>
          <div>
            <FieldArray
              name="durations"
              render={({ push, remove }) => (
                <div>
                  {props.values.durations &&
                    props.values.durations.map((duration, index) => (
                      <div key={index} style={durationRowStyle}>
                        <Field
                          name={`durations.${index}.type`}
                          label={null}
                          placeholder={t('chooseType')}
                          component={SelectField}
                          mode={isNew ? mode : 'view'}
                          menuPlacement="top"
                          options={typeOptions}
                          divider={false}
                        />
                        <Field
                          name={`durations.${index}.duration`}
                          label={null}
                          placeholder={t('andDuration')}
                          component={SelectField}
                          mode={isNew ? mode : 'view'}
                          menuPlacement="top"
                          options={durationOptions}
                          divider={false}
                        />
                        {mode === 'view' || !isNew ? null : (
                          <CloseButton
                            style={{ margin: 'auto 0 4px' }}
                            onClick={() => remove(index)}
                          />
                        )}
                      </div>
                    ))}
                  {mode === 'view' || !isNew ? null : (
                    <Button
                      style={{ margin: '10px 0' }}
                      color="primary"
                      onClick={() => push({ type: '', duration: '' })}
                    >
                      <Add style={{ marginRight: 4 }} />
                      {t('AddObjectType')}
                    </Button>
                  )}
                </div>
              )}
            />
          </div>
          <Divider gap="0 0 15px" />
        </div>
      ) : null}
      <Field
        name="managers"
        render={(props) => (
          <ManagersSelectField
            {...props}
            isMulti
            mode={endsAtMode}
            label={t('managers')}
            placeholder={t('SelectManagers')}
            isClearable={props.field.value.some((v) => !v.isFixed)}
          />
        )}
      />
      <Field
        name="sales_phone"
        render={({ field, form }) => (
          <InputField
            field={field}
            form={form}
            inputComponent={PhoneMaskedInput}
            label={t('SalesDepartmentPhone')}
            placeholder={t('Label.phone')}
            readOnly={endsAtMode === 'view'}
          />
        )}
      />
      <Field
        name="legal_text"
        label={t('ExplanationPassportDataConfirmationScreenMP')}
        placeholder={t('Enterlegend')}
        mode={endsAtMode}
        component={InputField}
        multiline
        rowsMax={4}
      />
      <Field name="reminder_file_url" mode={mode} component={ReminderField} />
    </>
  );
};
