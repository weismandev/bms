import { useStore } from 'effector-react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@mui/material';
import {
  $isToIssueDialogOpen,
  $toIssueDialogContent,
  submitToIsseu,
  rejectToIsseu,
} from '../../models/acceptance-list.model';

export const ReturnToIssueDialog = () => {
  const isOpened = useStore($isToIssueDialogOpen);
  const { apartment_id, title, message, accept, reject } =
    useStore($toIssueDialogContent);
  const rejectButtonColor = accept ? 'error' : 'primary';
  return (
    <Dialog open={isOpened}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{message}</DialogContent>
      <DialogActions>
        <DialogActions>
          {accept && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => submitToIsseu(apartment_id)}
            >
              {accept}
            </Button>
          )}
          <Button variant="contained" color={rejectButtonColor} onClick={rejectToIsseu}>
            {reject}
          </Button>
        </DialogActions>
      </DialogActions>
    </Dialog>
  );
};
