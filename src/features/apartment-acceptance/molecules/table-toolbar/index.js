import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Toolbar, FoundInfo, AddButton, Greedy, Tabs } from '../../../../ui';
import '../../models/detail.init';
import { addClicked, $mode } from '../../models/detail.model';
import '../../models/page.init';
import {
  tableTabsList,
  changeTable,
  $currentTable,
  $managersCount,
  $approvalsCount,
} from '../../models/page.model';

const $stores = combine({
  mode: $mode,
  approvalsCount: $approvalsCount,
  managersCount: $managersCount,
  currentTable: $currentTable,
});

export const TableToolbar = (props) => {
  const { approvalsCount, managersCount, currentTable, mode } = useStore($stores);
  const isApprovals = currentTable === 'approvals';

  return (
    <Toolbar style={{ paddingBottom: 12 }}>
      <FoundInfo
        count={isApprovals ? approvalsCount : managersCount}
        style={{ paddingRight: 24 }}
      />

      <div style={{ padding: '0 24px' }}>
        <Tabs
          tabEl={<Tab style={{ minWidth: 20, minHeight: 32 }} />}
          currentTab={currentTable}
          options={tableTabsList}
          onChange={(e, value) => changeTable(value)}
          scrollButtons={false}
        />
      </div>
      <Greedy />
      <AddButton
        style={{ marginLeft: 24 }}
        disabled={mode === 'edit'}
        onClick={addClicked}
      />
    </Toolbar>
  );
};
