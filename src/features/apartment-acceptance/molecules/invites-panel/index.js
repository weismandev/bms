import { Toolbar } from '@mui/material';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { InvitesDialog, InvitesButton } from '../../atoms';

export const InvitesPanel = () => (
  <Toolbar dense sx={{ justifyContent: 'space-evenly' }}>
    <InvitesButton />
    <InvitesDialog />
    <ColoredTextIconNotification />
  </Toolbar>
);
