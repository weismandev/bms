import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Field, FieldArray } from 'formik';
import { Reply } from '@mui/icons-material';
import {
  InputField,
  BaseInputHelperText,
  Loader,
  Divider,
  ActionButton,
  PhoneMaskedInput,
} from '../../../../ui';
import { SmsProvidersSelectField } from '../../../sms-providers-select';
import '../../models/detail.init';
import { $mode, $opened } from '../../models/detail.model';
import '../../models/sms-detail.init';
import {
  $isLoading,
  sendTestSms,
  smsProviderChanged,
} from '../../models/sms-detail.model';

const $stores = combine({
  mode: $mode,
  opened: $opened,
  isLoading: $isLoading,
});

export const SmsDetails = (props) => {
  const { values } = props;
  const { t } = useTranslation();
  const { mode, isLoading } = useStore($stores);

  useEffect(() => {
    if (values.provider) smsProviderChanged(values.provider.scheme);
  }, [values]);

  return (
    <>
      <Loader isLoading={isLoading} />
      <Field
        name="provider"
        component={SmsProvidersSelectField}
        label={t('provider')}
        placeholder={t('ChooseProvider')}
        mode={mode}
        divider={null}
      />
      {!values.provider ? (
        <BaseInputHelperText>
          {t('ToCreateInvitationsSelectSMSProvider')}
        </BaseInputHelperText>
      ) : null}
      <Divider gap="10px 0 15px" />
      {values.provider && values.provider.id ? (
        <>
          <FieldArray
            name="credentials"
            render={() =>
              values.provider.scheme && values.provider.scheme.length
                ? values.provider.scheme.map(({ type, title, key }, index) => (
                    <Field
                      key={index}
                      name={`credentials.${key}`}
                      type={type}
                      label={title}
                      placeholder={`${t('Enters')} ${title.toLowerCase()}`}
                      component={InputField}
                      mode={mode}
                    />
                  ))
                : null
            }
          />
          <Field
            name="template.template_primary"
            label={t('InvitationText')}
            placeholder={t('EnterInvitationText')}
            multiline
            rowsMax={20}
            mode={mode}
            component={InputField}
            divider={false}
          />
          <BaseInputHelperText>
            {t('DispatchedTheFirstTimeUserPromptedGeneratesLoginPassword')}
          </BaseInputHelperText>
          <Divider gap="10px 0 15px" />
          <Field
            name="template.template_secondary"
            label={t('ReInvitationText')}
            placeholder={t('EnterInvitationText')}
            multiline
            rowsMax={20}
            mode={mode}
            component={InputField}
            divider={false}
          />
          <BaseInputHelperText>
            {t('DispatchedInvitationHasAlreadyBeenSentUser')}
          </BaseInputHelperText>
          <Divider gap="10px 0 15px" />
          <Field
            name="template.more_than_one_owner"
            label={t('ReminderTextAboutNeedPresenceOwners')}
            placeholder={t('EnterReminderText')}
            multiline
            rowsMax={20}
            mode={mode}
            component={InputField}
            divider={false}
          />
          <BaseInputHelperText>
            {t('SentThereSeveralOwnersApartment')}
          </BaseInputHelperText>
          <Divider gap="10px 0 15px" />
          <Field
            name="template.template_day_reminder"
            label={t('ReminderText')}
            placeholder={t('EnterReminderText')}
            multiline
            rowsMax={20}
            mode={mode}
            helpText={t('DispatchedOneDayPriorPickup')}
            component={InputField}
            divider={false}
          />
          <BaseInputHelperText>
            {t('Ifsetitisautomaticallysentoutthedaybeforeacceptance')}
            <br />
            {t('VariablesAvailable')}
            <br />
            %DATE% - {t('issettotheacceptancedateintheformat')}
            <br />
            %TIME% - {t('setatthestartofacceptanceintheformat')}
          </BaseInputHelperText>
          <Divider gap="10px 0 15px" />
          <Field
            name="template.template_appointment_set"
            label={t('AcceptanceAppointmentNotificationText')}
            placeholder={t('EnterAlertText')}
            multiline
            rowsMax={20}
            mode={mode}
            component={InputField}
            divider={false}
          />
          <BaseInputHelperText>
            {t('Ifsetitisautomaticallysentonsuccessfulbookingslot')}
            <br />
            {t('VariablesAvailable')}
            <br />
            %ADDRESS% - {t('ApartmentAddress')}
            <br />
            %DATE% - {t('AcceptanceDateInTheFormat')}
          </BaseInputHelperText>
          <Divider gap="10px 0 15px" />
          <Field
            name="template.test_phone"
            render={({ field, form }) => (
              <InputField
                field={field}
                form={form}
                inputComponent={PhoneMaskedInput}
                label={t('NumberForTestMessages')}
                placeholder={t('EnterNumber')}
                mode={mode}
                divider={false}
              />
            )}
          />
          {mode === 'view' ? (
            <ActionButton
              style={{ margin: '24px 0 16px', alignSelf: 'center' }}
              kind="basic"
              onClick={sendTestSms}
            >
              <Reply style={{ transform: 'scaleX(-1)', marginRight: '6px' }} />
              {t('SendTestMessage')}
            </ActionButton>
          ) : null}
        </>
      ) : null}
    </>
  );
};
