import { api } from '../../../api/api2';

const getAcceptanceList = (payload = {}) =>
  api.v1('get', 'apartment/approval/get-list', payload);

const getSettings = (payload = {}) =>
  api.v1('get', 'apartment/approval/timetable/get', payload);

const updateSettings = (payload = {}) =>
  api.v1('post', 'apartment/approval/timetable/update', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const getSlots = (payload = {}) =>
  api.v1('get', 'apartment/approval/timetable/get-intervals', payload);

const getCommonSlots = (payload = {}) =>
  api.v1('get', 'apartment/approval/get-all-intervals', payload);

const updateSlots = (payload = {}) =>
  api.v1('post', 'apartment/approval/timetable/update-intervals', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const getAppartmentsList = (payload = {}) =>
  api.v1('get', 'apartment/approval/get-apartments', payload);

const changeAppartmentState = (payload = {}) =>
  api.v1('post', 'apartment/approval/change-apartment-state', payload);

const createApproval = (payload = {}) =>
  api.v1('post', 'apartment/approval/create', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const updateApproval = (payload = {}) =>
  api.v1('post', 'apartment/approval/update', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const deleteApproval = (payload = {}) =>
  api.v1('post', 'apartment/approval/delete', payload);

const getManagersList = (payload = {}) =>
  api.v1('get', 'apartment/approval/manager/table-list', payload);

const createManager = (payload = {}) =>
  api.v1('post', 'apartment/approval/manager/create', payload);

const updateManager = (payload = {}) =>
  api.v1('post', 'apartment/approval/manager/update', payload);

const deleteManager = (payload = {}) =>
  api.v1('post', 'apartment/approval/manager/delete', payload);

const getSmsTemplate = (payload = {}) =>
  api.v1('get', 'apartment/approval/sms-template/get-for-approval', payload);

const getSmsProvider = (payload = {}) => api.v1('get', 'sms-provider/crm/get', payload);

const getMailing = (payload = {}) =>
  api.v1('get', 'apartment/approval/mailing/get-email-schedule', payload);

const saveMailing = (payload = {}) =>
  api.v1('post', 'apartment/approval/mailing/save', payload);

const sendTestEmail = (payload = {}) =>
  api.v1('post', 'apartment/approval/mailing/send-test-email', payload);

const createSmsTemplate = (payload = {}) =>
  api.v1('post', 'apartment/approval/sms-template/create', payload);

const updateSmsTemplate = (payload = {}) =>
  api.v1('post', 'apartment/approval/sms-template/update', payload);

const sendTestSms = (payload = {}) =>
  api.v1('post', 'apartment/approval/invite/send-testing', payload);

const inviteResident = (payload = {}) =>
  api.v1('post', 'apartment/approval/invite/resident', payload);

const inviteResidents = (payload = {}) =>
  api.v1('post', 'apartment/approval/invite/by-approval', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const setAppointment = (payload = {}) =>
  api.v1('post', 'apartment/approval/set-apartment-interval', payload);

const removeAppointment = (id) =>
  api.v1('post', 'apartment/approval/delete-apartment-interval', { id });

const getApartmentInfo = (payload) =>
  api.v1('post', 'apartment/approval/get-apartment-info', payload);

export const acceptanceApi = {
  getAcceptanceList,
  getSettings,
  updateSettings,
  getSlots,
  getCommonSlots,
  updateSlots,
  getAppartmentsList,
  changeAppartmentState,
  createApproval,
  updateApproval,
  deleteApproval,
  getManagersList,
  createManager,
  updateManager,
  deleteManager,
  getSmsTemplate,
  getSmsProvider,
  sendTestEmail,
  getMailing,
  saveMailing,
  createSmsTemplate,
  updateSmsTemplate,
  sendTestSms,
  inviteResident,
  inviteResidents,
  setAppointment,
  removeAppointment,
  getApartmentInfo,
};
