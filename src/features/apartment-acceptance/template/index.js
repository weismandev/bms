import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateRows: '100%',
    gridTemplateColumns: 'minmax(376px, 1fr) 520px',
    padding: 24,
    height: '100%',
    gridGap: 24,
  },
  left: {
    gridRow: '1 / 2',
    gridColumn: (props) => (props.right ? '1 / 2' : '1 / 3'),
    position: 'relative',
  },
  right: {
    gridRow: '1 / 2',
    gridColumn: '2 / 3',
    position: 'relative',
  },
}));

export const PageLayout = (props) => {
  const { left = null, right = null } = props;
  const style = useStyles({ right });

  return (
    <div className={style.container}>
      <section className={style.left}>{left}</section>
      {Boolean(right) && <section className={style.right}>{right}</section>}
    </div>
  );
};
