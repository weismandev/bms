import {
  createStore,
  createEvent,
  createEffect,
  forward,
  sample,
  split,
  guard,
} from 'effector';
import { CheckCircle, Announcement } from '@mui/icons-material';
import { theme } from '@configs/theme';
import {
  changeNotification,
  $notification,
} from '@features/colored-text-icon-notification';
import i18n from '@shared/config/i18n';
import { acceptanceApi } from '../api';
import { $acceptance, $listData } from './acceptance-list.model';

const { t } = i18n;

const openInviting = createEvent();
const closeInviting = createEvent();
const submitIinvite = createEvent();
const inviteToggle = createEvent();

const $canBeInvited = createStore([]);

const $isInvitingOpened = createStore(false);
const $isLoading = createStore(false);

const fxInviteSelectedResidents = createEffect(acceptanceApi.inviteResidents);

const partiallyIvited = createEvent();
const fullyIvited = createEvent();

export {
  fxInviteSelectedResidents,
  openInviting,
  submitIinvite,
  closeInviting,
  inviteToggle,
  partiallyIvited,
  $canBeInvited,
  $isInvitingOpened,
  $isLoading,
};

// INIT

// Выбор подходящих квартир для рассылки с учетом фильтра
// Выбираем только 2 статуса + должен быть собсбвенник [resident] обязательно

$canBeInvited
  .on($listData.updates, (_, apartments) =>
    apartments
      .filter(
        ({ state }) => state?.slug === 'not_approved' || state?.slug === 'issues_closed'
      )
      .filter(({ resident }) => Boolean(resident?.id))
      .map((apartment) => ({ checked: true, ...apartment }))
  )
  // Переключаем статусы квартир в списке
  .on(inviteToggle, (apartments, apartId) =>
    apartments.map(({ checked, ...apart }) =>
      apartId === apart.id ? { ...apart, checked: !checked } : { checked, ...apart }
    )
  );

$isInvitingOpened.on(openInviting, () => true).on(closeInviting, () => false);

// Подготовка данных и их отправка
sample({
  clock: submitIinvite,
  source: [$canBeInvited, $acceptance],
  fn: ([apartments, acceptance]) => ({
    residents: apartments
      .filter(({ checked }) => checked)
      .map(({ resident }) => resident?.id || undefined),
    approval_id: acceptance.id,
  }),
  target: fxInviteSelectedResidents,
});

// Показать лоадер на время работы запроса
$isLoading
  .on(fxInviteSelectedResidents.pending, (_, isLoading) => isLoading)
  .reset(fxInviteSelectedResidents.done);

// Получаем ответ на рассылку приглашений и отправляем его в обработчик
// Обработчик проверяет кейсы и дергает тот или иной эвент, нагрузкой которого
// будет вся нагрузка ответа
split({
  source: fxInviteSelectedResidents.done,
  match: {
    // все приглашения разосланы, ошибок нет
    fully: ({ result = {} }) => result.success && !result.partially,

    // приглашения разосланы частично
    partially: ({ result = {} }) => result.success && result.partially,

    // бек вернул ошибку
    // error: ({ result = {} }) => !result.success,
  },
  cases: {
    fully: fullyIvited,
    partially: partiallyIvited,
    // для этого кейса будет показано стандартное сообщение об ошибке
    // error: fullyIvited,
  },
});

// При удачном ответе fully (когда все выбранные инвайты отправлены)
forward({
  from: fullyIvited,
  to: [
    // закрыть диалог с выбором резидентов для рассылки инвайтов.
    closeInviting,

    // показать сообщение об удачной рассылке для fully
    changeNotification.prepend(() => ({
      isOpen: true,
      color: theme.palette.success.main,
      message: t('InTheNearFutureRecipientsWillReceiveInvitations'),
      text: t('invitationsHaveBeenSent'),
      Icon: CheckCircle,
    })),
  ],
});

// При удачном ответе partially (когда выбранные инвайты отправлены частично)
forward({
  from: partiallyIvited,
  to: [
    // закрыть диалог с выбором резидентов для рассылки инвайтов.
    closeInviting,

    // показать сообщение об частичной рассылке для partially
    changeNotification.prepend(({ result = {} }) => {
      const { errors = [] } = result;
      const errorsMessage = errors.filter((item) => typeof item === 'string').join('; ');
      return {
        isOpen: true,
        color: theme.palette.warning.main,
        message: errorsMessage,
        text: t('distributionInvitations'),
        Icon: Announcement,
      };
    }),
  ],
});

// Сборосить содержимое уведомения при старте каждого запроса на рассылку
$notification.reset(guard($isLoading, { filter: Boolean }));
