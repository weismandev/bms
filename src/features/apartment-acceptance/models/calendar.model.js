import {
  createEvent,
  createStore,
  restore,
  createEffect,
  createApi,
  split,
} from 'effector';
import format from 'date-fns/format';

const DEFAULT_CELL_DURATION = 30;
const DEFAULT_START_DAY_HOUR = '6:00';
const DEFAULT_END_DAY_HOUR = '24:00';

const defaultCalendarSettings = {
  cellDuration: DEFAULT_CELL_DURATION,
  startDayHour: DEFAULT_START_DAY_HOUR,
  endDayHour: DEFAULT_END_DAY_HOUR,
};

const changeWeek = createEvent();
const changeCell = createEvent();
const changeAllCellByDay = createEvent();
const changeCalendarMode = createEvent();
const cancelEdit = createEvent();
const submitCalendar = createEvent();
const changeAppointmentsCloseDialogVisibility = createEvent();
const changeAppointmentsAcceptanceDialogVisibility = createEvent();
const changeDeleteConfirmDialogVisibility = createEvent();
const approveAppointment = createEvent();
const cellOutdate = createEvent();
const deleteAppointmentConfirmed = createEvent();
const getApartmentInfo = createEvent();

const fxGetSlots = createEffect();
const fxGetCommonSlots = createEffect();
const fxUpdateSlots = createEffect();
const fxSetAppointment = createEffect();
const fxRemoveAppointment = createEffect();
const fxGetApartmentInfo = createEffect();

const $defaultDuration = createStore(DEFAULT_CELL_DURATION);
const $timeTableCells = createStore({});
const $events = createStore([]);
const $savedEvents = createStore([]);
const $calendarSettings = createStore(defaultCalendarSettings);
const $isLoading = createStore(false);
const $error = createStore(null);
const $isAppointmentsMode = createStore(false);
const $week = restore(changeWeek, '');
const $calendarMode = restore(changeCalendarMode, 'view');
const $apartmentInfo = createStore(null);

const calendarModeStatus = split($calendarMode.updates, {
  edit: (mode) => mode === 'edit',
  view: (mode) => mode === 'view',
});

const $isAppointmentsCloseDialogOpen = restore(
  changeAppointmentsCloseDialogVisibility,
  false
);

const $isAppointmentsAcceptanceDialogOpen = restore(
  changeAppointmentsAcceptanceDialogVisibility,
  {
    isOpen: false,
  }
);

const $isDeleteConfirmDialogOpen = restore(changeDeleteConfirmDialogVisibility, {
  isOpen: false,
});

const $calendarForm = $timeTableCells.map((data) => data);

const $daysHasSelectedCells = $calendarForm.map((cellsObject) =>
  Object.values(cellsObject).reduce((acc, cell) => {
    const day = format(new Date(cell.starts_at), 'dd.MM.yyyy');

    if (acc[day]) {
      return acc;
    }

    return { ...acc, [day]: cell.is_enabled };
  }, {})
);

const appointmentsModeApi = createApi($isAppointmentsMode, {
  open: () => true,
  close: () => false,
});

export {
  $calendarForm,
  $calendarMode,
  $calendarSettings,
  $error,
  $events,
  $isAppointmentsAcceptanceDialogOpen,
  $isAppointmentsCloseDialogOpen,
  $isAppointmentsMode,
  $isLoading,
  $timeTableCells,
  $week,
  appointmentsModeApi,
  approveAppointment,
  cancelEdit,
  changeAppointmentsAcceptanceDialogVisibility,
  changeAppointmentsCloseDialogVisibility,
  changeCalendarMode,
  changeCell,
  changeWeek,
  DEFAULT_CELL_DURATION,
  DEFAULT_START_DAY_HOUR,
  DEFAULT_END_DAY_HOUR,
  fxGetSlots,
  fxGetCommonSlots,
  fxSetAppointment,
  fxRemoveAppointment,
  fxUpdateSlots,
  submitCalendar,
  changeAllCellByDay,
  $daysHasSelectedCells,
  cellOutdate,
  $isDeleteConfirmDialogOpen,
  changeDeleteConfirmDialogVisibility,
  deleteAppointmentConfirmed,
  $defaultDuration,
  calendarModeStatus,
  $savedEvents,
  getApartmentInfo,
  fxGetApartmentInfo,
  $apartmentInfo,
};
