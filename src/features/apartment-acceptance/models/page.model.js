import { createStore, createEvent, restore, createEffect, merge, split } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum/pending';
import i18n from '@shared/config/i18n';
import { fxGetList as fxGetBuildings } from '../../house-select';
import { fxGetAcceptanceList } from './acceptance-list.model';
import { fxSendTestEmail, fxGetMailing, fxSaveMailing } from './mailing-detail.model';
import {
  fxGetSmsTemplate,
  fxGetSmsProvider,
  fxCreateSmsTemplate,
  fxUpdateSmsTemplate,
  fxSendTestSms,
} from './sms-detail.model';

const { t } = i18n;

const PageGate = createGate();

const changeSectionTab = createEvent();

const $currentSectionTab = restore(changeSectionTab, 'appointments');

const $tabsList = createStore([
  {
    value: 'appointments',
    label: t('registrationAacceptance'),
    disabled: false,
  },
  {
    value: 'schedule',
    label: t('schedules'),
    disabled: false,
  },
]);

const fxCreateApproval = createEffect();
const fxUpdateApproval = createEffect();
const fxDeleteApproval = createEffect();
const fxGetManagersList = createEffect();
const fxCreateManager = createEffect();
const fxUpdateManager = createEffect();
const fxDeleteManager = createEffect();

const deleteDialogVisibilityChange = createEvent();
const errorDialogVisibilityChange = createEvent();
const deleteConfirmed = createEvent();
const changeTable = createEvent();

const errorOccured = merge([
  fxGetBuildings.fail,
  fxGetAcceptanceList.fail,
  fxCreateApproval.fail,
  fxUpdateApproval.fail,
  fxDeleteApproval.fail,
  fxGetManagersList.fail,
  fxCreateManager.fail,
  fxUpdateManager.fail,
  fxDeleteManager.fail,
  fxGetSmsTemplate.fail,
  fxGetSmsProvider.fail,
  fxCreateSmsTemplate.fail,
  fxUpdateSmsTemplate.fail,
  fxSendTestSms.fail,
  fxSendTestEmail.fail,
  fxGetMailing.fail,
  fxSaveMailing.fail,
]);

const $isLoading = pending({
  effects: [
    fxGetBuildings,
    fxGetAcceptanceList,
    fxCreateApproval,
    fxUpdateApproval,
    fxDeleteApproval,
    fxGetManagersList,
    fxCreateManager,
    fxUpdateManager,
    fxDeleteManager,
  ],
});

const requestApi = split($isLoading.updates, {
  start: (isLoading) => isLoading,
  finish: (isLoading) => !isLoading,
});

const $buildings = createStore([]);
const $newRawApproval = createStore([]);
const $newRawManager = createStore({});
const $error = createStore(null);
const $isDeleteDialogOpen = restore(deleteDialogVisibilityChange, false);
const $isErrorDialogOpen = restore(errorDialogVisibilityChange, false);
const $currentTable = createStore('approvals');
const $managersRaw = createStore([]);
const $approvalsSource = restore(fxGetAcceptanceList.doneData, []);
const $approvalsRaw = createStore([]);

const $managersCount = $managersRaw.map((data) => data.length);
const $managersNorm = $managersRaw.map((data) =>
  data.reduce((acc, item) => ({ ...acc, [item.id]: item }), {})
);
const $approvalsCount = $approvalsRaw.map((data) => data.length);
const $approvalsNorm = $approvalsRaw.map((data) =>
  data.reduce((acc, item) => ({ ...acc, [item.id]: item }), {})
);

const tableTabsList = [
  {
    label: t('acceptance'),
    value: 'approvals',
  },
  {
    label: t('managers'),
    value: 'managers',
  },
];

export {
  deleteDialogVisibilityChange,
  errorDialogVisibilityChange,
  deleteConfirmed,
  errorOccured,
  requestApi,
  changeTable,
  fxGetBuildings,
  fxCreateApproval,
  fxUpdateApproval,
  fxDeleteApproval,
  fxGetManagersList,
  fxCreateManager,
  fxUpdateManager,
  fxDeleteManager,
  $isLoading,
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $currentTable,
  $buildings,
  $newRawApproval,
  $newRawManager,
  $managersRaw,
  $managersNorm,
  $managersCount,
  $approvalsSource,
  $approvalsRaw,
  $approvalsNorm,
  $approvalsCount,
  tableTabsList,
  changeSectionTab,
  $currentSectionTab,
  $tabsList,
  PageGate,
};
