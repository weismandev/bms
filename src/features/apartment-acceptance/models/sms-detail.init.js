import { attach, forward, sample, guard } from 'effector';
import { CheckCircle } from '@mui/icons-material';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { changeNotification } from '../../colored-text-icon-notification';
import { acceptanceApi } from '../api';
import { entityApi, $opened } from './detail.model';
import {
  sendTestSms,
  fxGetSmsTemplate,
  fxGetSmsProvider,
  fxCreateSmsTemplate,
  fxUpdateSmsTemplate,
  fxSendTestSms,
  smsRequest,
  $isLoading,
  $providerScheme,
} from './sms-detail.model';

const { t } = i18n;

const formatTemplate = ({
  template_primary,
  template_secondary,
  template_day_reminder,
  template_appointment_set,
  test_phone,
  id,
  more_than_one_owner,
}) => ({
  id,
  template_primary: template_primary || '',
  template_secondary: template_secondary || '',
  template_day_reminder: template_day_reminder || '',
  template_appointment_set: template_appointment_set || '',
  test_phone: test_phone ? formatPhone(test_phone) : '',
  more_than_one_owner: more_than_one_owner || '',
});

const formatCredentials = ({ credentials, scheme }) => {
  const res = {};

  if (Array.isArray(scheme)) {
    scheme.map(({ key }) =>
      Object.keys(credentials).forEach((item) => {
        if (item === key) {
          return (res[key] = credentials[item]);
        } else return null;
      })
    );
  }
  return res;
};

const formatPayload = ({ id = null, template, provider, credentials }) => ({
  id: template.id ? template.id : null,
  approval_id: id,
  provider_id: provider.id,
  credentials: credentials
    ? formatCredentials({ credentials, scheme: provider.scheme })
    : {},
  ...template,
});

fxGetSmsTemplate.use(acceptanceApi.getSmsTemplate);
fxGetSmsProvider.use(acceptanceApi.getSmsProvider);
fxCreateSmsTemplate.use(acceptanceApi.createSmsTemplate);
fxUpdateSmsTemplate.use(acceptanceApi.updateSmsTemplate);
fxSendTestSms.use(acceptanceApi.sendTestSms);

$isLoading.on(smsRequest, (_, pending) => pending);

$providerScheme.reset([$opened.updates, signout]);

$opened
  .on(fxGetSmsTemplate.doneData, (state, { template }) => {
    const { credentials } = template;

    return {
      ...state,
      credentials: Array.isArray(credentials) ? {} : credentials,
      template: formatTemplate(template),
    };
  })
  .on(fxGetSmsProvider.doneData, (state, { provider }) => ({
    ...state,
    provider,
  }));

guard({
  source: fxGetSmsTemplate.doneData,
  filter: ({ template }) => template && template.provider_id,
  target: fxGetSmsProvider.prepend(({ template }) => ({
    id: template.provider_id,
  })),
});

forward({
  from: entityApi.createSmsTemplate,
  to: fxCreateSmsTemplate.prepend((payload) => formatPayload(payload)),
});

forward({
  from: entityApi.updateSmsTemplate,
  to: fxUpdateSmsTemplate.prepend((payload) => formatPayload(payload)),
});

forward({
  from: [fxCreateSmsTemplate.done, fxUpdateSmsTemplate.done],
  to: attach({
    effect: fxGetSmsTemplate,
    source: $opened,
    mapParams: (_, opened) => ({ approval_id: opened.id }),
  }),
});

sample({
  source: $opened,
  clock: sendTestSms,
  fn: (opened) => ({
    approval_id: opened.id,
    phone: opened.template.test_phone,
  }),
  target: fxSendTestSms,
});

forward({
  from: fxSendTestSms.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('testMessageSentSuccessfully'),
    Icon: CheckCircle,
  })),
});
