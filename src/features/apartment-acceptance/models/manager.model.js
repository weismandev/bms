import { createEvent, createEffect, createStore, restore, guard, merge } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const defaultSettings = {
  timetable_id: null,
  days: [],
};

const changeManagerTab = createEvent();
const submitSettings = createEvent();
const changeSettingsMode = createEvent();

const movedToCommonTab = guard({
  source: changeManagerTab,
  filter: (tab) => tab === 'all',
});

const fxCreateManager = createEffect();
const fxGetSettings = createEffect();
const fxUpdateSettings = createEffect();

const $managers = createStore([]);
const $currentManagerSettings = createStore(defaultSettings);
const $isLoading = createStore(false);
const $error = createStore(null);

const $settingsMode = restore(changeSettingsMode, 'view');
const $currentManagerTab = restore(changeManagerTab, null);

const $managersTabs = $managers.map((managers) => {
  let tabs = managers.map(({ id, name, surname }) => ({
    value: id,
    label: `
      ${Boolean(surname) && typeof surname === 'string' ? surname : ''}
      ${Boolean(name) && typeof name === 'string' ? name : ''}
    `,
  }));

  if (tabs.length > 0) {
    tabs = tabs.concat({ value: 'all', label: t('all') });
  }

  return tabs;
});

const errorOccured = merge([fxGetSettings.failData, fxUpdateSettings.failData]);

export {
  $currentManagerSettings,
  $currentManagerTab,
  $managers,
  $managersTabs,
  $settingsMode,
  changeManagerTab,
  changeSettingsMode,
  fxCreateManager,
  fxGetSettings,
  fxUpdateSettings,
  movedToCommonTab,
  submitSettings,
  $isLoading,
  $error,
  errorOccured,
};
