import { forward, sample, guard, merge, createEffect } from 'effector';
import { format } from 'date-fns';
import { CheckCircle } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { changeNotification } from '../../colored-text-icon-notification';
import { signout } from '../../common';
import { acceptanceApi } from '../api';
import { $opened, $acceptance, $listData, $savedListData } from './acceptance-list.model';
import {
  fxGetSlots,
  fxGetCommonSlots,
  fxUpdateSlots,
  fxSetAppointment,
  fxRemoveAppointment,
  $timeTableCells,
  $week,
  changeWeek,
  changeCell,
  $calendarForm,
  cancelEdit,
  $calendarMode,
  submitCalendar,
  $isAppointmentsAcceptanceDialogOpen,
  approveAppointment,
  appointmentsModeApi,
  $events,
  $calendarSettings,
  $isLoading,
  $error,
  changeAllCellByDay,
  $daysHasSelectedCells,
  $isAppointmentsCloseDialogOpen,
  cellOutdate,
  deleteAppointmentConfirmed,
  $isDeleteConfirmDialogOpen,
  $defaultDuration,
  $isAppointmentsMode,
  calendarModeStatus,
  changeCalendarMode,
  DEFAULT_CELL_DURATION,
  DEFAULT_START_DAY_HOUR,
  DEFAULT_END_DAY_HOUR,
  $savedEvents,
  getApartmentInfo,
  fxGetApartmentInfo,
  changeDeleteConfirmDialogVisibility,
  $apartmentInfo,
} from './calendar.model';
import {
  $currentManagerSettings,
  fxGetSettings,
  fxUpdateSettings,
  movedToCommonTab,
  $currentManagerTab,
} from './manager.model';
import { $currentSectionTab, changeSectionTab } from './page.model';

const months = {
  Jan: '01',
  Feb: '02',
  Mar: '03',
  Apr: '04',
  May: '05',
  Jun: '06',
  Jul: '07',
  Aug: '08',
  Sep: '09',
  Oct: '10',
  Nov: '11',
  Dec: '12',
};

const { t } = i18n;

const requestStarted = guard({ source: $isLoading, filter: Boolean });

const errorOccured = merge([
  fxGetSlots.failData,
  fxGetCommonSlots.failData,
  fxUpdateSlots.failData,
  fxSetAppointment.failData,
  fxRemoveAppointment.failData,
  fxGetApartmentInfo.failData,
]);

fxGetSlots.use(acceptanceApi.getSlots);
fxGetCommonSlots.use(acceptanceApi.getCommonSlots);
fxUpdateSlots.use(acceptanceApi.updateSlots);
fxSetAppointment.use(acceptanceApi.setAppointment);
fxRemoveAppointment.use(acceptanceApi.removeAppointment);
fxGetApartmentInfo.use(acceptanceApi.getApartmentInfo);

$events
  .on(fxRemoveAppointment.done, (state, { params: removedId }) =>
    state.filter((event) => String(event.id) !== String(removedId))
  )
  .reset(signout);

$calendarSettings
  .on(calendarModeStatus.edit, (state, _) => ({
    ...state,
    startDayHour: DEFAULT_START_DAY_HOUR,
    endDayHour: DEFAULT_END_DAY_HOUR,
  }))
  .on($defaultDuration, (state, duration) => ({
    ...state,
    cellDuration: duration,
  }))
  .reset(signout);

$isLoading.reset(signout);
$week.reset(signout);
$calendarMode.on(changeSectionTab, () => 'view').reset(signout);
$isAppointmentsCloseDialogOpen.reset(signout);
$isDeleteConfirmDialogOpen.reset([signout, fxRemoveAppointment]);
$isAppointmentsMode.reset(signout);

$isAppointmentsAcceptanceDialogOpen
  .on(fxSetAppointment, () => ({ isOpen: false }))
  .reset(signout);

const calendarModeAndOutdatedDateSampled = sample(
  $calendarMode,
  cellOutdate,
  (mode, date) => ({
    mode,
    date,
  })
);

const cellOutdatedWhileUserDoesNotEditingCalendarSettings = guard({
  source: calendarModeAndOutdatedDateSampled,
  filter: ({ mode }) => mode === 'view',
});

const fxOneMinuteDelay = createEffect().use(
  (payload) => new Promise((res) => setTimeout(() => res(payload), 60000))
);

const cellOutdatedWhileUserEditingCalendarSettings = guard({
  source: calendarModeAndOutdatedDateSampled,
  filter: ({ mode }) => mode !== 'view',
});

forward({
  from: cellOutdatedWhileUserEditingCalendarSettings,
  to: fxOneMinuteDelay,
});

forward({
  from: fxOneMinuteDelay.doneData.map(({ date }) => date),
  to: cellOutdate,
});

$defaultDuration
  // .on($acceptance, (state, acceptance) => acceptance?.default_duration || state)
  .reset(signout);

$timeTableCells
  .on(
    [
      fxGetSlots.doneData,
      fxGetCommonSlots.doneData,
      fxUpdateSlots.done.map(({ params }) => params),
    ],
    (_, { events }) =>
      events.reduce((acc, i) => ({ ...acc, [`${i.starts_at}_${i.ends_at}`]: i }), {})
  )
  .on(cellOutdatedWhileUserDoesNotEditingCalendarSettings, (state, { date }) =>
    Object.entries(state)
      .map(([key, value]) => {
        if (new Date(value.starts_at) <= date) {
          return [key, { ...value, is_enabled: false }];
        }

        return [key, { ...value }];
      })
      .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
  )
  .reset(signout);

$error.on(errorOccured, (_, error) => error).reset([requestStarted, signout]);

sample({
  source: [
    fxGetSlots.pending,
    fxUpdateSlots.pending,
    fxSetAppointment.pending,
    fxGetCommonSlots.pending,
    fxRemoveAppointment.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

const formatEvent = (tab, manager, managerTab, data) => {
  if (tab === 'schedule') {
    return [];
  }

  let events = data
    .flatMap((i) => i.events.map((e) => ({ ...e, title: i.number })))

    .map(({ starts_at, ends_at, ...rest }) => ({
      ...rest,
      startDate: starts_at,
      endDate: ends_at,
    }));

  if (managerTab !== 'all') {
    events = events.filter((e) => e.timetable_id === manager.timetable_id);
  }

  return events;
};

sample({
  source: {
    tab: $currentSectionTab,
    manager: $currentManagerSettings,
    managerTab: $currentManagerTab,
    data: $listData,
  },
  fn: ({ tab, data, manager, managerTab }) => formatEvent(tab, manager, managerTab, data),
  target: $events,
});

sample({
  source: {
    tab: $currentSectionTab,
    manager: $currentManagerSettings,
    managerTab: $currentManagerTab,
    data: $savedListData,
  },
  fn: ({ tab, data, manager, managerTab }) => {
    const list = data?.result ? data.result : data;

    return formatEvent(tab, manager, managerTab, list);
  },
  target: $savedEvents,
});

$calendarForm
  .on(changeCell, (state, { name, checked, ends_at, starts_at }) => ({
    ...state,
    [name]: { ...state[name], is_enabled: checked, ends_at, starts_at },
  }))
  .on(sample($timeTableCells, cancelEdit), (state, stateBeforeEdit) => stateBeforeEdit)
  .on(
    sample($daysHasSelectedCells, changeAllCellByDay, (daysObject, day) => ({
      day,
      hasSelected: daysObject[day],
    })),
    (state, { day, hasSelected }) => {
      const entries = Object.entries(state).map((entry) => {
        const [key, value] = entry;
        const isDaysCell = format(new Date(value.starts_at), 'dd.MM.yyyy') === day;

        return isDaysCell ? [key, { ...value, is_enabled: !hasSelected }] : entry;
      });

      return Object.fromEntries(entries);
    }
  );

$calendarMode.on([cancelEdit, fxUpdateSlots.done], () => 'view').reset(signout);

guard({
  source: sample({
    source: { week: $week, settings: $currentManagerSettings },
    clock: [fxGetSettings.doneData, fxUpdateSettings.doneData, changeWeek],
    fn: ({ week, settings }) => ({
      timetable_id: settings.timetable_id,
      interval: {
        start: format(week.weekStart, 'yyyy-MM-dd'),
        end: format(week.weekEnd, 'yyyy-MM-dd'),
      },
    }),
  }),
  filter: ({ timetable_id }) => Boolean(timetable_id),
  target: fxGetSlots,
});

forward({
  from: sample({
    source: { calendar: $calendarForm, settings: $currentManagerSettings },
    clock: [submitCalendar, cancelEdit],
    fn: ({ calendar, settings }) => ({
      events: Object.values(calendar),
      timetable_id: settings.timetable_id,
    }),
  }),
  to: fxUpdateSlots,
});

const convertDateToUtc = (date) => {
  const utcString = new Date(date).toUTCString();

  if (utcString.length === 0) {
    return null;
  }

  const day = utcString.substring(5, 7);
  const month = utcString.substring(8, 11);
  const year = utcString.substring(12, 16);
  const time = utcString.substring(17, 22);

  return `${year}-${months[month]}-${day} ${time}`;
};

guard({
  source: sample({
    source: {
      week: $week,
      acceptance: $acceptance,
      managerTab: $currentManagerTab,
    },
    clock: [movedToCommonTab, changeWeek],
  }),
  filter: ({ acceptance, managerTab }) => Boolean(acceptance) && managerTab === 'all',
  target: fxGetCommonSlots.prepend(({ week, acceptance }) => {
    const starts_at = convertDateToUtc(week.weekStart) || '';
    const ends_at = convertDateToUtc(week.weekEnd) || '';

    return {
      starts_at,
      ends_at,
      approval_id: acceptance.id,
    };
  }),
});

sample({
  source: {
    acceptance: $acceptance,
    opened: $opened,
    currentManagerSettings: $currentManagerSettings,
    time: $isAppointmentsAcceptanceDialogOpen,
  },
  clock: approveAppointment,
  fn: ({ acceptance, opened, currentManagerSettings, time }) => {
    const payload = {
      approval_id: acceptance.id,
      apartment_id: opened.id,
      start: format(time.start, 'yyyy-MM-dd HH:mm'),
    };

    if (currentManagerSettings && currentManagerSettings.timetable_id) {
      payload.timetable_id = currentManagerSettings.timetable_id;
    }

    return payload;
  },
  target: fxSetAppointment,
});

forward({ from: fxSetAppointment.done, to: appointmentsModeApi.close });

forward({
  from: fxSetAppointment.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('receiptCreatedSuccessfully'),
    Icon: CheckCircle,
  })),
});

guard({
  clock: deleteAppointmentConfirmed,
  source: $apartmentInfo,
  filter: ({ cellId }) => Boolean(cellId) || cellId === 0,
  target: fxRemoveAppointment.prepend(({ cellId }) => cellId),
});

const oneMinute = 60000;

setInterval(() => {
  const defaultDuration = $defaultDuration.getState();
  const now = new Date();
  const minutesFromDateStart = now.getHours() * 60 + now.getMinutes();

  if (minutesFromDateStart % defaultDuration === 0) {
    cellOutdate(now);
  }
}, oneMinute);

function getMinMaxTimeFromIntervalsArray(array) {
  const [min = null] = array.sort();
  const [max = null] = array.reverse();

  return [min, max];
}

forward({
  from: getApartmentInfo,
  to: fxGetApartmentInfo.prepend(({ id, cellId }) => ({
    apartments: [id],
    cellId,
  })),
});

sample({
  clock: fxGetApartmentInfo.done,
  fn: () => ({ isOpen: true }),
  target: changeDeleteConfirmDialogVisibility,
});

const getFullName = (surname, name, patronymic) => {
  if (!(surname && name && patronymic)) {
    return null;
  }

  return `${surname} ${name} ${patronymic}`;
};

$apartmentInfo
  .on(fxGetApartmentInfo.done, (_, { params, result }) => {
    const data = result[0];

    const generalOwner = getFullName(
      data?.owner?.surname,
      data?.owner?.name,
      data?.owner?.patronymic
    );

    const otherOwners =
      Array.isArray(data?.owners) && data.owners.length > 1
        ? data.owners.slice(1).map((item) => ({
            name: getFullName(item.surname, item.name, item.patronymic),
            phone: item.phone,
          }))
        : null;

    const owner = otherOwners
      ? [generalOwner, ...otherOwners.map((item) => item.name)].join(', ')
      : generalOwner;

    const generalOwnerPhone = data?.owner?.phone || null;
    const phone =
      generalOwnerPhone && otherOwners
        ? [generalOwnerPhone, ...otherOwners.map((item) => item.phone)].join(', ')
        : generalOwnerPhone;

    const apartmentInfo = {
      cellId: params.cellId,
      flat: data?.number || null,
      owner,
      phone,
    };

    if (Array.isArray(data.owner.objects) && data.owner.objects.length === 0) {
      apartmentInfo.isExistObject = false;
    }

    if (Array.isArray(data.owner.objects) && data.owner.objects.length > 0) {
      const obj = data.owner.objects.reduce(
        (acc, object) => {
          if (object?.type?.slug === 'flat') {
            acc.flat.push(object.number);
          }

          if (object?.type?.slug === 'parking_slot') {
            acc.parking_slot.push(object.number);
          }

          if (object?.type?.slug === 'pantry') {
            acc.pantry.push(object.number);
          }

          return acc;
        },
        { flat: [], parking_slot: [], pantry: [] }
      );

      const isExistObject =
        obj.flat.length > 0 || obj.parking_slot.length > 0 || obj.pantry.length > 0;

      apartmentInfo.objects = obj;
      apartmentInfo.isExistObject = isExistObject;
    }

    return apartmentInfo;
  })
  .reset([fxGetApartmentInfo.fail, signout]);
