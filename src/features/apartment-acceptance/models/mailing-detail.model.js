import { createEffect, createStore, createEvent, merge } from 'effector';

const fxSendTestEmail = createEffect();
const fxGetMailing = createEffect();
const fxSaveMailing = createEffect();

const sendTestEmail = createEvent();

const $isLoading = createStore(false);

const emailRequest = merge([
  fxSendTestEmail.pending,
  fxGetMailing.pending,
  fxSaveMailing.pending,
]);

export {
  sendTestEmail,
  emailRequest,
  fxSendTestEmail,
  fxGetMailing,
  fxSaveMailing,
  $isLoading,
};
