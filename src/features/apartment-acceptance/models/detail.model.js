import { createStore, createEvent, split, sample, restore } from 'effector';
import { isAfter } from 'date-fns';
import i18n from '@shared/config/i18n';
import { deleteConfirmed } from './page.model';

const { t } = i18n;

const newManagerEntity = {
  type: 'manager',
  email: '',
  phone: '',
  name: '',
  surname: '',
  patronymic: '',
};

const newApprovalEntity = {
  type: 'approval',
  id: null,
  tomorrow_appointment_closes_at: '',
  buildings: [],
  starts_at: '',
  ends_at: '',
  default_duration: 60,
  durations: [{ duration: '', type: '' }],
  managers: [],
  template: {
    id: null,
    template_primary: '',
    template_secondary: '',
    template_day_reminder: '',
    template_appointment_set: '',
    test_phone: '',
  },
  provider: {},
  reminder_file_url: '',
  sales_phone: '',
  legal_text: '',
};

const typeOptions = [
  { value: 'other', title: t('other') },
  { value: 'flat_1', title: `1-${t('bedroomApartment')}` },
  { value: 'flat_2', title: `2-${t('bedroomApartment')}` },
  { value: 'flat_3', title: `3-${t('bedroomApartment')}` },
  { value: 'flat_4', title: `4-${t('bedroomApartment')}` },
  { value: 'pantry', title: t('pantry') },
  { value: 'parking_slot', title: t('parkingSpace') },
];

const durationOptions = [
  { title: `30 ${t('min')}`, value: 30 },
  { title: `60 ${t('min')}`, value: 60 },
  { title: `90 ${t('min')}`, value: 90 },
  { title: `120 ${t('min')}`, value: 120 },
];

const detailTabs = [
  {
    label: t('basicSettings'),
    value: 'main',
    onCreateIsDisabled: false,
  },
  {
    label: t('SMSInvitations'),
    value: 'sms',
    onCreateIsDisabled: true,
  },
  {
    label: t('scheduleDistribution'),
    value: 'mailing',
    onCreateIsDisabled: true,
  },
];

const changedDetailVisibility = createEvent();
const changeMode = createEvent();
const changeDetailsTab = createEvent();
const detailSubmitted = createEvent();
const addClicked = createEvent();
const open = createEvent();

const detailClosed = changedDetailVisibility.filterMap((visibility) => {
  if (!visibility) return visibility;

  return null;
});

const $detailData = createStore(null);
const $newEntity = createStore(newApprovalEntity);
const $opened = createStore(null);
const $detailTabs = createStore(detailTabs);
const $currentDetailsTab = createStore('main');
const $isDetailOpen = restore(changedDetailVisibility, false);
const $mode = restore(changeMode, 'view');
const $modeWithCheckAcceptenceStarted = createStore('view');
sample({
  source: [$mode, $opened],
  fn: ([mode, opened]) => {
    const alreadyStarted = isAfter(new Date(), opened.starts_at) && Boolean(opened.id);
    return alreadyStarted ? 'view' : mode;
  },
  target: $modeWithCheckAcceptenceStarted,
});

const $createdApproval = createStore(null);
const $createdManager = createStore(null);

const entitySubmittedOnTab = sample({
  source: { opened: $opened, tab: $currentDetailsTab },
  clock: detailSubmitted,
  fn: ({ opened, tab }, payload) => ({
    ...opened,
    ...payload,
    tab,
  }),
});

const entityApi = split(entitySubmittedOnTab, {
  createManager: ({ id, type }) => !id && type === 'manager',
  updateManager: ({ id, type }) => Boolean(id) && type === 'manager',

  createApproval: ({ id, type, tab }) => type === 'approval' && tab === 'main' && !id,

  updateApproval: ({ id, type, tab }) =>
    type === 'approval' && tab === 'main' && Boolean(id),

  createSmsTemplate: ({ template, type, tab }) =>
    type === 'approval' && tab === 'sms' && !template.id,

  updateSmsTemplate: ({ template, type, tab }) =>
    type === 'approval' && tab === 'sms' && Boolean(template.id),

  saveMailing: ({ tab }) => tab === 'mailing',
});

const deleteApi = split(
  sample($opened, deleteConfirmed, (opened, _) => opened),
  {
    manager: ({ type }) => type === 'manager',
    approval: ({ type }) => type === 'approval',
  }
);

export {
  typeOptions,
  durationOptions,
  newManagerEntity,
  newApprovalEntity,
  changedDetailVisibility,
  changeDetailsTab,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,
  deleteApi,
  $newEntity,
  $detailTabs,
  $isDetailOpen,
  $detailData,
  $currentDetailsTab,
  $mode,
  $modeWithCheckAcceptenceStarted,
  $opened,
  $createdApproval,
  $createdManager,
};
