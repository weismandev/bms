import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  scheduled_from: '',
  scheduled_to: '',
  status: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
