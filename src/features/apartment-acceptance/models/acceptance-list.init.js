import { guard, sample, forward, split, merge, restore } from 'effector';
import { format } from 'date-fns';
import { CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout, history } from '@features/common';
import { fxGetList as fxBuildings } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { acceptanceApi } from '../api';
import {
  $listData,
  fxGetAcceptanceList,
  $acceptanceId,
  fxGetAppartmentsList,
  fxChangeAppartmentState,
  $building,
  changeBuilding,
  $normalized,
  open,
  $opened,
  $search,
  changeAppartmentState,
  statuses,
  changeAcceptanceDialogVisibility,
  submitAcceptanceForm,
  $isAcceptanceDialogOpen,
  fxInviteResident,
  $acceptance,
  $isLoading,
  $error,
  $count,
  visitedSchedulePage,
  mount,
  unmount,
  $toIssueDialogContent,
  $isToIssueDialogOpen,
  $userRoleMatcher,
  toIssueClicked,
  openToIssueDialog,
  submitToIsseu,
  rejectToIsseu,
  $savedListData,
  throttledSearch,
} from './acceptance-list.model';
import { appointmentsModeApi, fxSetAppointment } from './calendar.model';
import { $filters } from './filters.model';
import { fxInviteSelectedResidents } from './invites.model';

const { t } = i18n;

const acceptance = split(submitAcceptanceForm, {
  success: ({ property }) => property.every((i) => i.state === 'approved'),
  fail: ({ property }) => property.some((i) => i.state !== 'approved'),
});

const changeAppartmentStateTo = split(changeAppartmentState, {
  invite_sent: ({ state }) => state === 'invite_sent',
  common: ({ state }) => state !== 'invite_sent',
});

const acceptanceDialogClosed = guard({
  source: changeAcceptanceDialogVisibility,
  filter: (visibility) => !visibility,
});

const errorOccured = merge([
  fxGetAcceptanceList.failData,
  fxGetAppartmentsList.failData,
  fxChangeAppartmentState.failData,
  fxInviteResident.failData,
]);

const requestStarted = guard({ source: $isLoading, filter: Boolean });

fxGetAcceptanceList.use(acceptanceApi.getAcceptanceList);
fxGetAppartmentsList.use(acceptanceApi.getAppartmentsList);
fxChangeAppartmentState.use(acceptanceApi.changeAppartmentState);
fxInviteResident.use(acceptanceApi.inviteResident);

$acceptanceId.on(visitedSchedulePage, (_, id) => id).reset([signout, unmount]);

const $acceptanceList = restore(fxGetAcceptanceList.doneData, []).reset([
  signout,
  unmount,
]);

$acceptance
  .on(
    sample({
      source: {
        data: $acceptanceList,
        id: $acceptanceId,
      },
      fn: ({ data, id }) =>
        (Array.isArray(data) && data.find((i) => String(i.id) === String(id))) || null,
    }),
    (_, acceptance) => acceptance
  )
  .reset([signout, unmount]);

guard({
  source: fxGetAppartmentsList.done,
  filter: ({ params: { filter } }) => {
    const filters = Object.values(filter);

    return filters.length > 0 ? filters.every((item) => item.length === 0) : false;
  },
  target: $savedListData,
});

$savedListData
  .on([fxChangeAppartmentState.done, fxInviteResident.done], updateStatus)
  .on(fxSetAppointment.doneData, updateApartment)
  .reset([signout, unmount]);

$listData
  .on(fxGetAppartmentsList.doneData, (_, data) =>
    Object.values(data).length === 0 ? [] : data
  )
  .on([fxChangeAppartmentState.done, fxInviteResident.done], updateStatus)
  .on(fxSetAppointment.doneData, updateApartment)
  .reset([signout, unmount]);

$count.on($listData.updates, (_, data) => data.length).reset([signout, unmount]);

$opened
  .on(
    sample($normalized, open, (data, id) => data[id]),
    (_, opened) => opened
  )
  .reset([
    acceptanceDialogClosed,
    fxChangeAppartmentState.done,
    appointmentsModeApi.close,
    signout,
    unmount,
  ]);

$isAcceptanceDialogOpen
  .on(fxChangeAppartmentState.done, () => false)
  .reset([signout, unmount]);

$error.on(errorOccured, (_, error) => error).reset([requestStarted, signout, unmount]);

$building.reset([signout, unmount]);
$search.reset([signout, unmount]);

sample({
  source: [
    fxBuildings.pending,
    fxGetAcceptanceList.pending,
    fxGetAppartmentsList.pending,
    fxChangeAppartmentState.pending,
    fxInviteResident.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

forward({
  from: mount,
  to: fxGetAcceptanceList,
});

guard({
  source: sample({
    source: {
      building: $building,
      search: $search,
      filters: $filters,
    },
    clock: [
      changeBuilding,
      throttledSearch,
      $filters,
      fxGetAcceptanceList.done,
      fxInviteSelectedResidents.done,
    ],
  }),
  filter: ({ building }) => building && building.id,
  target: fxGetAppartmentsList.prepend(({ building, search, filters }) => ({
    building_id: building.id,
    filter: {
      state: filters.status?.slug || '',
      search_query: search,
      scheduled_from:
        filters.scheduled_from.length > 0
          ? format(new Date(filters.scheduled_from), 'yyyy-MM-dd HH:mm:ss')
          : '',
      scheduled_to:
        filters.scheduled_to.length > 0
          ? format(new Date(filters.scheduled_to), 'yyyy-MM-dd HH:mm:ss')
          : '',
    },
  })),
});

forward({ from: changeAppartmentStateTo.common, to: fxChangeAppartmentState });

forward({
  from: sample(
    $acceptance,
    changeAppartmentStateTo.invite_sent,
    ({ id: approval_id }, payload) => ({ approval_id, ...payload })
  ),
  to: fxInviteResident,
});

forward({
  from: sample($opened, acceptance.success),
  to: fxChangeAppartmentState.prepend(({ id }) => ({
    apartment_id: id,
    state: 'approved',
  })),
});

forward({
  from: sample($opened, acceptance.fail),
  to: fxChangeAppartmentState.prepend(({ id }) => ({
    apartment_id: id,
    state: 'issues',
  })),
});

forward({
  from: appointmentsModeApi.open,
  to: open.prepend(({ apartment_id }) => apartment_id),
});

forward({
  from: fxInviteResident.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('inviteHasBeenSent'),
    Icon: CheckCircle,
  })),
});

guard({
  source: fxChangeAppartmentState.done,
  filter: ({ params }) => params.state === 'issues_closed',
  target: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('correctionMarkAccepted'),
    Icon: CheckCircle,
  })),
});

const handleHistoryStep = (location) => {
  const match =
    typeof location.pathname === 'string' &&
    location.pathname.match(/(?<page>apartment-acceptance)\/(?<id>.*)/);

  if (match) {
    visitedSchedulePage(match.groups.id);
  }
};

handleHistoryStep(history.location);
history.listen(handleHistoryStep);

function updateStatus(state, { params: { apartment_id, state: status } }) {
  const updatedItemIdx = state.findIndex((i) => String(i.id) === String(apartment_id));

  if (updatedItemIdx > -1) {
    const fullStatus = statuses.find((i) => i.slug === status);

    return state
      .slice(0, updatedItemIdx)
      .concat({
        ...state[updatedItemIdx],
        state: fullStatus,
        extra: state[updatedItemIdx].extra.map((i) => ({
          ...i,
          state: fullStatus,
        })),
      })
      .concat(state.slice(updatedItemIdx + 1));
  }

  return state;
}

function updateApartment(state, apartment) {
  const currentState = Array.isArray(state) ? state : state.result;

  const updatedItemIdx = currentState.findIndex(
    (i) => String(i.id) === String(apartment.id)
  );

  if (updatedItemIdx > -1) {
    return currentState
      .slice(0, updatedItemIdx)
      .concat(apartment)
      .concat(currentState.slice(updatedItemIdx + 1));
  }

  return currentState;
}

// Вернуть недвиж из статуса Принято в статус Замечания

// проверка на админа, выбор содержимого диалога
split({
  source: toIssueClicked,
  match: $userRoleMatcher,
  cases: {
    admin: openToIssueDialog.prepend((id) => ({
      ...id,
      title: t('attention'),
      message: t('afterConfirmationStatusAcceptedWillChanged'),
      accept: t('confirm').toUpperCase(),
      reject: t('cancel').toUpperCase(),
    })),
    other: openToIssueDialog.prepend((id) => ({
      ...id,
      title: t('accessDenied'),
      message: t('contactAdministratorChangeStatus'),
      accept: null,
      reject: t('ok').toUpperCase(),
    })),
  },
});

$toIssueDialogContent.on(openToIssueDialog, (_, content) => content).reset(unmount);

$isToIssueDialogOpen
  .on(openToIssueDialog, (state) => !state)
  .reset([submitToIsseu, rejectToIsseu, unmount]);

forward({
  from: submitToIsseu,
  to: fxChangeAppartmentState.prepend((id) => ({
    apartment_id: id,
    state: 'issues',
  })),
});
