import { createStore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const approvalsColumns = [
  { name: 'start_at', title: t('startDate') },
  { name: 'buildings', title: t('objects') },
  { name: 'statistics', title: t('ApartmentsAccepted_total') },
  { name: 'state', title: t('acceptanceStatus') },
  { name: 'actions', title: ' ' },
];

const approvalsColumnsWidths = [
  { columnName: 'start_at', width: 170 },
  { columnName: 'buildings', width: 240 },
  { columnName: 'statistics', width: 350 },
  { columnName: 'state', width: 250 },
  { columnName: 'actions', width: 190 },
];

const managersColumns = [
  { name: 'fullname', title: t('namesManager') },
  { name: 'city', title: t('Label.city') },
  { name: 'email', title: t('Label.email') },
  { name: 'phone', title: t('Label.phone') },
];

const managersColumnsWidths = [
  { columnName: 'fullname', width: 250 },
  { columnName: 'city', width: 250 },
  { columnName: 'email', width: 250 },
  { columnName: 'phone', width: 250 },
];

const $approvalsTableData = createStore([]);
const $managersTableData = createStore([]);

export {
  approvalsColumns,
  approvalsColumnsWidths,
  managersColumns,
  managersColumnsWidths,
  $approvalsTableData,
  $managersTableData,
};
