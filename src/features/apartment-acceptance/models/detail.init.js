import { forward, sample } from 'effector';
import { format } from 'date-fns';
import { signout } from '@features/common';
import { formatOpenedApproval, formatOpenedManager } from '../lib';
import {
  newManagerEntity,
  newApprovalEntity,
  changeDetailsTab,
  addClicked,
  open,
  detailClosed,
  entityApi,
  deleteApi,
  $newEntity,
  $currentDetailsTab,
  $isDetailOpen,
  $detailData,
  $mode,
  $opened,
  $detailTabs,
  $createdApproval,
  $createdManager,
} from './detail.model';
import { fxGetMailing, fxSaveMailing } from './mailing-detail.model';
import {
  $currentTable,
  $approvalsNorm,
  $managersNorm,
  $newRawApproval,
  $newRawManager,
  fxCreateManager,
  fxUpdateManager,
  fxDeleteManager,
  fxCreateApproval,
  fxUpdateApproval,
  fxDeleteApproval,
} from './page.model';
import {
  fxCreateSmsTemplate,
  fxUpdateSmsTemplate,
  fxGetSmsTemplate,
} from './sms-detail.model';

$isDetailOpen
  .on([addClicked, open], () => true)
  .on([fxDeleteManager.done, fxDeleteApproval.done, $currentTable.updates], () => false)
  .reset(signout);

$newEntity.reset(signout);
$detailTabs.reset(signout);
$createdApproval.reset(signout);
$createdManager.reset(signout);

$mode
  .on(addClicked, () => 'edit')
  .on(
    [
      detailClosed,
      open,
      $currentTable.updates,
      fxSaveMailing.done,
      fxCreateSmsTemplate.done,
      fxUpdateSmsTemplate.done,
      fxCreateApproval.done,
      fxUpdateApproval.done,
      fxDeleteApproval.done,
      fxCreateManager.done,
      fxUpdateManager.done,
    ],
    () => 'view'
  )
  .reset(signout);

$opened
  .on(
    sample({
      source: $newEntity,
      clock: [addClicked, detailClosed],
      fn: (entity, _) => entity,
    }),
    (_, entity) => entity
  )
  .on(
    sample($detailData, open, (data, id) => data[id]),
    (_, opened) => formatOpened(opened)
  )
  .reset([$currentTable, signout]);

$currentDetailsTab
  .on(changeDetailsTab, (_, tab) => tab)
  .reset([addClicked, detailClosed, signout]);

$detailData
  .on([$approvalsNorm.updates, $managersNorm.updates], (_, data) => data)
  .reset(signout);

forward({
  from: $newRawApproval.updates,
  to: open.prepend(([appr]) => appr.id),
});

forward({
  from: $newRawManager.updates,
  to: open.prepend((manager) => manager.id),
});

sample({
  source: $currentTable,
  fn: (currentTable) =>
    currentTable === 'managers' ? newManagerEntity : newApprovalEntity,
  target: $newEntity,
});

forward({
  from: entityApi.createManager,
  to: fxCreateManager,
});

forward({
  from: entityApi.updateManager,
  to: fxUpdateManager,
});

forward({
  from: entityApi.createApproval,
  to: fxCreateApproval.prepend(formatApprovalPayload),
});

forward({
  from: entityApi.updateApproval,
  to: fxUpdateApproval.prepend(formatApprovalPayload),
});

forward({
  from: deleteApi.approval,
  to: fxDeleteApproval.prepend((payload) => ({ id: payload.id })),
});

forward({
  from: deleteApi.manager,
  to: fxDeleteManager.prepend((payload) => ({ manager_id: payload.id })),
});

forward({
  from: open,
  to: [
    fxGetSmsTemplate.prepend((id) => ({ approval_id: id })),
    fxGetMailing.prepend((id) => ({ approval_id: id })),
  ],
});

function formatOpened(opened) {
  switch ($currentTable.getState()) {
    case 'approvals':
      return formatOpenedApproval(opened);

    case 'managers':
      return formatOpenedManager(opened);

    default:
      return {};
  }
}

function formatApprovalPayload({
  buildings,
  default_duration,
  starts_at,
  ends_at,
  durations,
  managers,
  tomorrow_appointment_closes_at,
  id = null,
  reminder_file_url,
  sales_phone,
  legal_text,
}) {
  const default_duration_result = default_duration.value
    ? Number(default_duration.value)
    : default_duration
    ? default_duration
    : 60;
  return {
    id,
    buildings: buildings.reduce((acc, { id }) => [...acc, id], []),
    default_duration: default_duration_result,
    starts_at: starts_at ? format(new Date(starts_at), 'yyyy-MM-dd') : null,
    ends_at: ends_at ? format(new Date(ends_at), 'yyyy-MM-dd') : null,
    durations: durations.reduce(
      (acc, { duration = {}, type = {} }) => [
        ...acc,
        { duration: duration.value, type: type.value },
      ],
      []
    ),
    managers: managers.reduce((acc, { id = null }) => [...acc, id], []),
    tomorrow_appointment_closes_at: tomorrow_appointment_closes_at
      ? format(tomorrow_appointment_closes_at, 'HH:mm')
      : null,
    reminder_file_url: reminder_file_url ? checkUrl(reminder_file_url) : null,
    sales_phone: sales_phone || '',
    legal_text,
  };
}

function checkUrl(url) {
  const urlRegexp = /^https?:\/\//;
  const hasPrefix = urlRegexp.test(url);

  return hasPrefix ? url : `http://${url}`;
}
