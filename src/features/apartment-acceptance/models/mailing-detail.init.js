import { forward, sample } from 'effector';
import { set, format } from 'date-fns';
import { CheckCircle } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { changeNotification } from '../../colored-text-icon-notification';
import { acceptanceApi } from '../api';
import { entityApi, $opened } from './detail.model';
import {
  sendTestEmail,
  emailRequest,
  fxSendTestEmail,
  fxGetMailing,
  fxSaveMailing,
  $isLoading,
} from './mailing-detail.model';

const { t } = i18n;

fxSendTestEmail.use(acceptanceApi.sendTestEmail);
fxSaveMailing.use(acceptanceApi.saveMailing);
fxGetMailing.use(acceptanceApi.getMailing);

$isLoading.on(emailRequest, (_, pending) => pending);

$opened.on([fxGetMailing.doneData, fxSaveMailing.doneData], (state, mailing) => ({
  ...state,
  ...formatMailing(mailing),
}));

forward({
  from: entityApi.saveMailing,
  to: fxSaveMailing.prepend(formatPayload),
});

sample({
  source: $opened,
  clock: sendTestEmail,
  fn: (opened) => ({
    approval_id: opened.id,
  }),
  target: fxSendTestEmail,
});

forward({
  from: fxSendTestEmail.done,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#1BB169',
    text: t('testEmailSentSuccessfully'),
    Icon: CheckCircle,
  })),
});

function formatMailing({ time, emails, test_email }) {
  return {
    emails: Array.isArray(emails)
      ? emails.map((email) => ({ label: email, value: email }))
      : [],
    send_time:
      typeof time === 'string'
        ? formatTime(time)
        : set(new Date(), { hours: 17, minutes: 0, seconds: 0 }),
    test_email: typeof test_email === 'string' ? test_email : '',
  };
}

function formatPayload({ emails, test_email, send_time, id }) {
  return {
    approval_id: id,
    emails: emails.map((email) => email.label),
    test_email,
    send_time: send_time instanceof Date ? format(send_time, 'HH:mm') : '17:00',
  };
}

function formatTime(time) {
  const [hours, minutes] = time.split(':');
  return set(new Date(), {
    hours,
    minutes,
    seconds: 0,
  });
}
