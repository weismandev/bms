import { format } from 'date-fns';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { formatName } from '../lib';
import { $approvalsRaw, $managersRaw } from './page.model';
import { $approvalsTableData, $managersTableData } from './table.model';

const { t } = i18n;

const formatApproval = ({
  id,
  buildings = [],
  starts_at,
  ends_at,
  state,
  statistics,
}) => ({
  id,
  start_at: starts_at
    ? format(new Date(starts_at), 'dd.MM.yyyy')
    : t('isNotSpecifiedshe'),
  ends_at: ends_at ? format(new Date(ends_at), 'dd.MM.yyyy') : t('isNotSpecifiedshe'),
  buildings: buildings || [],
  statistics: statistics || t('isNotDefinedit'),
  state: state || { title: t('isNotDefined'), slug: 'error' },
  actions: id,
});

const formatManager = ({ id, email, name, surname, patronymic, phone }) => ({
  email: email || t('isNotDefined'),
  fullname: formatName([surname, name, patronymic]),
  phone: phone ? formatPhone(phone) : t('isNotDefined'),
  id,
});

$approvalsTableData
  .on($approvalsRaw.updates, (_, raw) => raw.map((item) => formatApproval(item)))
  .reset(signout);

$managersTableData
  .on($managersRaw.updates, (_, raw) => raw.map((item) => formatManager(item)))
  .reset(signout);
