import { sample, forward, guard } from 'effector';
import format from 'date-fns/format';
import set from 'date-fns/set';
import { nanoid } from 'nanoid';
import { signout } from '../../common';
import { acceptanceApi } from '../api';
import { getHoursMinutesTupleFromColonSeparatedTime } from '../lib';
import { $acceptance } from './acceptance-list.model';
import {
  submitSettings,
  $currentManagerSettings,
  $currentManagerTab,
  fxGetSettings,
  fxUpdateSettings,
  $settingsMode,
  $managers,
  $isLoading,
  $error,
  changeManagerTab,
  errorOccured,
} from './manager.model';

const requestStarted = guard({ source: $isLoading, filter: Boolean });

$error.on(errorOccured, (_, error) => error).reset([requestStarted, signout]);

fxGetSettings.use(acceptanceApi.getSettings);
fxUpdateSettings.use(acceptanceApi.updateSettings);

$managers
  .on($acceptance.updates, (_, acceptance) => (acceptance && acceptance.managers) || [])
  .reset(signout);

$currentManagerTab
  .on($managers.updates, (_, [first]) => first && first.id)
  .reset(signout);

$currentManagerSettings
  .on([fxGetSettings.doneData, fxUpdateSettings.doneData], (_, data) => ({
    ...data,
    days: data.days
      .sort(sortByDaysOrderWithDayZeroAtEnd)
      .map(setIdForDayIntervals)
      .map(replaceIntervalTimeStringToDate),
  }))
  .reset([signout, changeManagerTab.filter({ fn: (tab) => tab === 'all' })]);

$settingsMode.on(fxUpdateSettings.done, () => 'view').reset(signout);

sample({
  source: [fxGetSettings.pending, fxUpdateSettings.pending],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

const preparedToGetSettings = sample({
  source: [$currentManagerTab, $acceptance],
  fn: ([manager_id, acceptance]) => ({
    manager_id,
    approval_id: acceptance && acceptance.id,
  }),
});

guard({
  source: preparedToGetSettings,
  filter: ({ manager_id, approval_id }) => approval_id && manager_id !== 'all',
  target: fxGetSettings,
});

forward({
  from: submitSettings,
  to: fxUpdateSettings.prepend(({ days, ...rest }) => ({
    ...rest,
    days: days.map(({ intervals, ...restDay }) => ({
      ...restDay,
      intervals: intervals.map(({ time_start, time_end }) => ({
        time_start: format(time_start, 'HH:mm'),
        time_end: format(time_end, 'HH:mm'),
      })),
    })),
  })),
});

function sortByDaysOrderWithDayZeroAtEnd(dayA, dayB) {
  return dayA.num !== 0 && dayB.num !== 0 ? 0 : -1;
}

function setIdForDayIntervals(day) {
  return {
    ...day,
    intervals: day.intervals.map((i) => ({ ...i, _id: nanoid(3) })),
  };
}

function replaceIntervalTimeStringToDate(day) {
  return {
    ...day,
    intervals: day.intervals.map((i) => {
      const [hoursStart, minutesStart] = getHoursMinutesTupleFromColonSeparatedTime(
        i.time_start
      );

      const [hoursEnd, minutesEnd] = getHoursMinutesTupleFromColonSeparatedTime(
        i.time_end
      );

      return {
        ...i,
        time_start: set(new Date(), {
          hours: hoursStart,
          minutes: minutesStart,
        }),
        time_end: set(new Date(), { hours: hoursEnd, minutes: minutesEnd }),
      };
    }),
  };
}
