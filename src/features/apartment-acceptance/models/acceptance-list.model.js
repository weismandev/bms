import { createStore, createEffect, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';
import { throttle } from 'patronum/throttle';
import { nanoid } from 'nanoid';
import { $userRole } from '@features/common';
import i18n from '@shared/config/i18n';
import { pick } from '@tools/pick';

const { t } = i18n;

const statuses = [
  { id: 1, title: t('isNotAccepted'), slug: 'not_approved' },
  { id: 2, title: t('record'), slug: 'scheduled' },
  { id: 3, title: t('comments'), slug: 'issues' },
  { id: 4, title: t('problemsHaveBeenEliminated'), slug: 'issues_closed' },
  { id: 5, title: t('adopted'), slug: 'approved' },
  { id: 6, title: t('invitationsHaveBeenSent'), slug: 'invite_sent' },
];

const AcceptanceListGate = createGate();
const { open: mount, close: unmount } = AcceptanceListGate;

const changeBuilding = createEvent();
const open = createEvent();
const openAcceptance = createEvent();
const visitedSchedulePage = createEvent();
const changeSearch = createEvent();
const changeAppartmentState = createEvent();
const changeAcceptanceDialogVisibility = createEvent();
const submitAcceptanceForm = createEvent();

const fxGetAcceptanceList = createEffect();
const fxGetAppartmentsList = createEffect();
const fxChangeAppartmentState = createEffect();
const fxInviteResident = createEffect();

const $savedListData = createStore([]);
const $listData = createStore([]);
const $count = createStore(0);
const $acceptanceId = restore(openAcceptance, null);
const $acceptance = createStore(null);
const $opened = createStore(null);
const $isLoading = createStore(false);
const $error = createStore(null);
const $building = restore(changeBuilding, '');
const $search = restore(changeSearch, '');
const $isAcceptanceDialogOpen = restore(changeAcceptanceDialogVisibility, false);
const $normalized = $listData.map((listData) =>
  listData.reduce((acc, item) => ({ ...acc, [item.id]: item }), {})
);
const $buildingsOptions = $acceptance.map(
  (acceptance) => (acceptance && acceptance.buildings) || []
);
const $acceptanceForm = $opened.map((opened) => {
  const pickTypeStateFrom = (object) => pick(['type', 'state', 'duration'], object);
  return opened
    ? {
        property: [pickTypeStateFrom(opened)].map((i) => ({
          ...i,
          _id: nanoid(3),
          state: i.state.slug === 'issues' ? 'issues' : 'approved',
        })),
      }
    : { property: [] };
});
const $intervalsCountForAppointment = $opened.map((opened) =>
  opened && opened.duration ? opened.duration.slots : 0
);

const $toIssueDialogContent = createStore({});
const $isToIssueDialogOpen = createStore(false);
const toIssueClicked = createEvent();
const openToIssueDialog = createEvent();
const submitToIsseu = createEvent();
const rejectToIsseu = createEvent();
const $userRoleMatcher = $userRole.map(({ is_update_locked }) =>
  is_update_locked ? 'admin' : 'other'
);

const throttledSearch = throttle({
  source: changeSearch,
  timeout: 1000,
});

export {
  $acceptance,
  $acceptanceId,
  $acceptanceForm,
  $buildingsOptions,
  $building,
  $error,
  $intervalsCountForAppointment,
  $isAcceptanceDialogOpen,
  $isLoading,
  $listData,
  $count,
  $normalized,
  $opened,
  $search,
  changeAcceptanceDialogVisibility,
  changeAppartmentState,
  changeBuilding,
  changeSearch,
  fxChangeAppartmentState,
  fxGetAcceptanceList,
  fxGetAppartmentsList,
  fxInviteResident,
  open,
  openAcceptance,
  statuses,
  submitAcceptanceForm,
  visitedSchedulePage,
  AcceptanceListGate,
  mount,
  unmount,
  $toIssueDialogContent,
  $isToIssueDialogOpen,
  toIssueClicked,
  openToIssueDialog,
  submitToIsseu,
  rejectToIsseu,
  $userRoleMatcher,
  $savedListData,
  throttledSearch,
};
