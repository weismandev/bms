import { sample, guard, forward, attach } from 'effector';
import { signout, history } from '../../common';
import { acceptanceApi } from '../api';
import { formatBuildings } from '../lib';
import { openAcceptance, fxGetAcceptanceList } from './acceptance-list.model';
import {
  errorOccured as managerErrorOccured,
  changeManagerTab,
  movedToCommonTab,
} from './manager.model';
import {
  deleteConfirmed,
  errorOccured,
  requestApi,
  changeTable,
  fxGetBuildings,
  fxCreateApproval,
  fxUpdateApproval,
  fxDeleteApproval,
  fxGetManagersList,
  fxCreateManager,
  fxUpdateManager,
  fxDeleteManager,
  $isLoading,
  $error,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $currentTable,
  $buildings,
  $managersRaw,
  $approvalsSource,
  $approvalsRaw,
  $currentSectionTab,
  $newRawApproval,
  $newRawManager,
  PageGate,
  $tabsList,
} from './page.model';

const prependApprovalsRaw = ({ approvals, buildings }) => {
  return approvals.map((item) => {
    const prependedBuildings = item.buildings.reduce((acc, id) => {
      const build = buildings.find((item) => item.id === id);

      return build ? [...acc, build] : acc;
    }, []);

    return { ...item, buildings: prependedBuildings };
  });
};

const updateRaw = ({ entities, entity }) => {
  const idx = entities.findIndex((i) => i.id === entity.id);

  if (idx === -1) {
    return [...entities, entity];
  }
  return [...entities.slice(0, idx), entity, ...entities.slice(idx + 1)];
};

fxCreateApproval.use(acceptanceApi.createApproval);
fxUpdateApproval.use(acceptanceApi.updateApproval);
fxDeleteApproval.use(acceptanceApi.deleteApproval);

fxGetManagersList.use(acceptanceApi.getManagersList);
fxCreateManager.use(acceptanceApi.createManager);
fxUpdateManager.use(acceptanceApi.updateManager);
fxDeleteManager.use(acceptanceApi.deleteManager);

$tabsList
  .on(changeManagerTab, (state) => state.map((i) => ({ ...i, disabled: false })))
  .on(movedToCommonTab, (state) =>
    state.map((i) => (i.value !== 'appointments' ? { ...i, disabled: true } : i))
  )
  .reset(signout);

$currentSectionTab.on(movedToCommonTab, (_, managerTab) => 'appointments').reset(signout);

$isDeleteDialogOpen.on(deleteConfirmed, () => false).reset(signout);

$isErrorDialogOpen
  .on(requestApi.start, () => false)
  .on([errorOccured, managerErrorOccured], () => true)
  .reset(signout);

$error
  .on([errorOccured, managerErrorOccured], (_, { error }) => error)
  .on(requestApi.start, () => null)
  .reset(signout);

$isLoading.reset(signout);
$newRawApproval.reset(signout);
$approvalsSource.reset(signout);

const $collectedSources = guard({
  source: { buildings: $buildings, approvals: $approvalsSource },
  filter: ({ buildings, approvals }) =>
    Boolean(buildings.length) && Array.isArray(approvals) && Boolean(approvals.length),
});

sample({
  source: $buildings,
  clock: [fxCreateApproval.doneData, fxUpdateApproval.doneData],
  fn: (buildings, { approval }) =>
    prependApprovalsRaw({ approvals: [approval], buildings }),
  target: $newRawApproval,
});

$approvalsRaw
  .on($collectedSources, (_, { buildings, approvals }) =>
    prependApprovalsRaw({ buildings, approvals })
  )
  .on($newRawApproval, (approvals, newApproval) => {
    const [approval] = newApproval;
    return updateRaw({ entities: approvals, entity: approval });
  })
  .reset(signout);

$newRawManager
  .on([fxCreateManager.doneData, fxUpdateManager.doneData], (_, { manager }) => manager)
  .reset(signout);

$managersRaw
  .on(fxGetManagersList.doneData, (_, { managers }) => managers)
  .on([fxCreateManager.doneData, fxUpdateManager.doneData], (managers, { manager }) =>
    updateRaw({ entities: managers, entity: manager })
  )
  .reset(signout);

$currentTable.on(changeTable, (_, table) => table).reset(signout);

$buildings
  .on(fxGetBuildings.doneData, (_, { buildings }) => formatBuildings(buildings))
  .reset(signout);

openAcceptance.watch((id) => {
  history.push(`/apartment-acceptance/${id}`);
});

guard({
  source: $currentTable.updates,
  filter: (table) => table === 'approvals',
  target: fxGetAcceptanceList,
});

guard({
  source: $currentTable.updates,
  filter: (table) => table === 'managers',
  target: attach({
    effect: fxGetManagersList,
    mapParams: () => ({ page: 1, per_page: 1000 }),
  }),
});

forward({
  from: PageGate.open,
  to: [fxGetBuildings, fxGetAcceptanceList],
});

forward({
  from: fxDeleteApproval.done,
  to: [fxGetBuildings, fxGetAcceptanceList],
});

forward({
  from: fxDeleteManager.done,
  to: fxGetManagersList,
});
