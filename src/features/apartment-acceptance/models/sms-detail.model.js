import { createEffect, createStore, createEvent, merge, restore } from 'effector';

const fxGetSmsTemplate = createEffect();
const fxGetSmsProvider = createEffect();
const fxCreateSmsTemplate = createEffect();
const fxUpdateSmsTemplate = createEffect();
const fxSendTestSms = createEffect();

const sendTestSms = createEvent();
const smsProviderChanged = createEvent();

const $isLoading = createStore(false);
const $providerScheme = restore(smsProviderChanged, null);

const smsRequest = merge([
  fxGetSmsTemplate.pending,
  fxGetSmsProvider.pending,
  fxCreateSmsTemplate.pending,
  fxUpdateSmsTemplate.pending,
  fxSendTestSms.pending,
]);

export {
  sendTestSms,
  smsProviderChanged,
  fxGetSmsTemplate,
  fxGetSmsProvider,
  fxCreateSmsTemplate,
  fxUpdateSmsTemplate,
  fxSendTestSms,
  smsRequest,
  $isLoading,
  $providerScheme,
};
