import { forward, merge, attach, split, sample } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';

import { signout } from '@features/common';

import { newsApi } from '../api';

import {
  INewsFormated,
  INewsData,
  INews,
  IDeleteNewsResponse,
  IEnterprise,
} from '../interfaces';

import {
  fxGetNews,
  $news,
  pageUnmounted,
  pageMounted,
  $isLoading,
  $isErrorDialogOpen,
  $error,
  changedErrorDialogVisibility,
  $countNews,
  $currentPage,
  changePerPage,
  changePage,
  $perPage,
  $enterprises,
  fxGetEnterprises,
} from './page.model';

import { fxGetNewsById, fxCreateNews, fxUpdateNews, fxDeleteNews } from './detail.model';

fxGetNews.use(newsApi.getNews);
fxGetEnterprises.use(newsApi.getEnterprises);

const errorOccured = merge([
  fxGetNews.fail,
  fxGetNewsById.fail,
  fxCreateNews.fail,
  fxUpdateNews.fail,
  fxDeleteNews.fail,
  fxGetEnterprises.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetNews,
        fxGetNewsById,
        fxCreateNews,
        fxUpdateNews,
        fxDeleteNews,
        fxGetEnterprises,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$news
  .on(fxGetNews.done, (_, { result }) =>
    Array.isArray(result?.items) ? result.items.map((item) => formatNews(item)) : []
  )
  .on(fxCreateNews.done, (state, { result }) => mergeDataAfterCreate(state, result))
  .on(fxUpdateNews.done, (state, { result }) =>
    mergeDataAfterUpdate(state, result.item ? result.item.id : -1, result.item)
  )
  .on(fxDeleteNews.done, (state, result) =>
    mergeDataAfterDelete(state, result.params.id, result.result)
  )
  .reset([signout, pageUnmounted]);

$countNews
  .on(fxGetNews.done, (_, { result }) => result?.meta?.total || 0)
  .on(fxCreateNews.done, (state, { result }) => (result?.item?.id ? state + 1 : state))
  .on(fxDeleteNews.done, (state, { result }) => (result?.deleted ? state - 1 : state))
  .reset([signout, pageUnmounted]);

forward({
  from: pageMounted,
  to: fxGetEnterprises,
});

$enterprises
  .on(fxGetEnterprises.done, (_, { result }) =>
    Array.isArray(result?.enterprises) ? result.enterprises.filter(isVerified) : []
  )
  .reset([signout, pageUnmounted]);

forward({
  from: [pageMounted, changePage, changePerPage],
  to: attach({
    effect: fxGetNews,
    source: [$currentPage, $perPage],
    mapParams: (_, [currentPage, perPage]) => ({
      page: currentPage,
      per_page: perPage,
    }),
  }),
});

const isVerified = ({ is_verified, employee_is_verified }: IEnterprise) =>
  is_verified && employee_is_verified;

const formatNews = ({ id, title, text, images, date }: INewsData): INewsFormated => {
  const maxTitleLength = 39;
  const maxTextLength = 139;
  const formattedText = text.replace(/<\/?[^>]+>/g, '').replace(/&nbsp;/g, '');

  return {
    id,
    title: title.length > maxTitleLength ? `${title.slice(0, maxTitleLength)}...` : title,
    text:
      formattedText.length > maxTextLength
        ? `${formattedText.slice(0, maxTextLength)}...`
        : formattedText,
    image: images.length > 0 ? images[0]?.url : null,
    date: format(new Date(date), 'dd.MM.yyyy'),
  };
};

const mergeDataAfterCreate = (state: INewsFormated[], result: INews) => {
  if (!result?.item) {
    return state;
  }

  const createdNews = formatNews(result.item);

  return [createdNews, ...state];
};

const mergeDataAfterUpdate = (
  state: INewsFormated[],
  updatedId: number,
  updated: INewsData
) => {
  const updatedNews = formatNews(updated);

  const updatedIdx = Array.isArray(state)
    ? state.findIndex((i) => String(i.id) === String(updatedId))
    : -1;

  return updatedIdx > -1
    ? state
        .slice(0, updatedIdx)
        .concat(updatedNews)
        .concat(state.slice(updatedIdx + 1))
    : state;
};

const mergeDataAfterDelete = (
  state: INewsFormated[],
  id: number,
  result: IDeleteNewsResponse
) => {
  if (!result?.deleted) {
    return state;
  }

  return state.filter((item) => item.id !== id);
};

$currentPage.reset(changePerPage);
