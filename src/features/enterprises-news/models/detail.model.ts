import { createEvent, createStore, createEffect } from 'effector';
import { format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';
import {
  ICreateUpdateNewsPayload,
  INews,
  IDeleteNewsResponse,
  INewsByIdPayload,
  IDeleteNewsPayload,
} from '../interfaces';

const { t } = i18n;

const newEntity = {
  id: null,
  images: [],
  text: '',
  title: '',
  date: format(new Date(), 'yyyy-MM-dd', { locale: dateFnsLocale }),
  status: { id: 1, title: t('Draft') },
  enterprises: [],
};

const changedDeleteVisibilityModal = createEvent<boolean>();
const deleteNews = createEvent<void>();

const fxGetNewsById = createEffect<INewsByIdPayload, INews, Error>();
const fxCreateNews = createEffect<ICreateUpdateNewsPayload, INews, Error>();
const fxUpdateNews = createEffect<ICreateUpdateNewsPayload, INews, Error>();
const fxDeleteNews = createEffect<IDeleteNewsPayload, IDeleteNewsResponse, Error>();

const $isOpenDeleteModal = createStore<boolean>(false);

export {
  fxGetNewsById,
  fxUpdateNews,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  deleteNews,
  fxDeleteNews,
  fxCreateNews,
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
