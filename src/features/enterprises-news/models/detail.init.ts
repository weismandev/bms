import { sample, forward, guard } from 'effector';
import { delay } from 'patronum';
import { format } from 'date-fns';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { signout, $pathname, history } from '@features/common';
import i18n from '@shared/config/i18n';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { newsApi } from '../api';
import { ICreateUpdateNewsPayload, INewsItem, INews, IEnterprise } from '../interfaces';
import {
  $opened,
  openViaUrl,
  $isDetailOpen,
  fxGetNewsById,
  $mode,
  entityApi,
  fxUpdateNews,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  fxDeleteNews,
  deleteNews,
  fxCreateNews,
  addClicked,
  changedDetailVisibility,
} from './detail.model';
import { pageUnmounted, fxGetNews, $entityId, $path } from './page.model';

const { t } = i18n;

fxGetNewsById.use(newsApi.getNewsById);
fxCreateNews.use(newsApi.createNews);
fxUpdateNews.use(newsApi.updateNews);
fxDeleteNews.use(newsApi.deleteNews);

guard({
  clock: fxGetNews.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetNewsById.prepend(({ entityId }: { entityId: number }) => ({
    id: entityId,
  })),
});

const delayedRedirect = delay({ source: fxGetNewsById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxDeleteNews.done,
  source: $path,
  fn: (path) => history.push(`../${path}`),
});

$isOpenDeleteModal.on(changedDeleteVisibilityModal, (_, visibility) => visibility);

$opened.reset([pageUnmounted, signout, addClicked]);

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetNewsById.done, () => true)
  .reset([pageUnmounted, fxDeleteNews.done, fxGetNewsById.fail, fxCreateNews.done]);

$mode.reset([fxCreateNews.done, fxUpdateNews.done, fxDeleteNews.done]);
$isOpenDeleteModal.reset([pageUnmounted, signout, fxDeleteNews.done]);

forward({
  from: openViaUrl,
  to: fxGetNewsById.prepend((id) => ({ id })),
});

sample({
  clock: deleteNews,
  source: $opened,
  fn: (opened) => ({
    id: Number(opened.id),
  }),
  target: fxDeleteNews,
});

sample({
  clock: [fxGetNewsById.done, fxUpdateNews.done],
  fn: ({ result }) => formatGetUpdateResponse(result),
  target: $opened,
});

forward({
  from: entityApi.create.map(prepareSubmittedPayload),
  to: fxCreateNews,
});

forward({
  from: entityApi.update.map(prepareSubmittedPayload),
  to: fxUpdateNews,
});

function formatGetUpdateResponse({ item }: INews) {
  if (!item) {
    return {};
  }
  return {
    id: item.id,
    images: item.images || [],
    text: Boolean(item) && Boolean(item.text) ? item.text : '',
    title: item.title || '',
    date: item.date,
    status:
      item?.status === 2
        ? { id: 2, title: t('ShePublished') }
        : { id: 1, title: t('Draft') },
    enterprises: (item as INewsItem)?.enterprises || null,
  };
}

function prepareSubmittedPayload({
  id,
  title,
  text,
  date,
  images,
  enterprises,
  status,
}: ICreateUpdateNewsPayload): ICreateUpdateNewsPayload {
  return {
    id,
    text,
    title,
    images: images.map((item) => (item.url ? { id: item.id } : { file: item.file })),
    status: typeof status === 'object' ? status.id : status,
    date: format(new Date(date), 'yyyy-MM-dd', { locale: dateFnsLocale }),
    enterprises: (enterprises as IEnterprise[]).map((i: { id: number }) => i.id),
  };
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
