import { createEffect, createStore, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';
import { $pathname } from '@features/common';
import {
  INewsFormated,
  INewsResponse,
  INewsPayload,
  IEnterprise,
  IEnterprisesResponse,
} from '../interfaces';

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const fxGetNews = createEffect<INewsPayload, INewsResponse, Error>();
const fxGetEnterprises = createEffect<undefined, IEnterprisesResponse, Error>();

const changedErrorDialogVisibility = createEvent<boolean>();
const changePage = createEvent<number>();
const changePerPage = createEvent<number>();

const $news = createStore<INewsFormated[]>([]);
const $enterprises = createStore<IEnterprise[]>([]);

const $isLoading = createStore<boolean>(false);
const $error = createStore<null | Error>(null);
const $isErrorDialogOpen = createStore<boolean>(false);
const $countNews = createStore<number>(0);
const $currentPage = restore<number>(changePage, 1);
const $perPage = restore<number>(changePerPage, 10);
const $path = $pathname.map((pathname) => pathname.split('/')[1]);
const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export {
  $error,
  $isLoading,
  PageGate,
  pageMounted,
  pageUnmounted,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  fxGetNews,
  $news,
  $countNews,
  changePage,
  changePerPage,
  $currentPage,
  $perPage,
  fxGetEnterprises,
  $enterprises,
  $path,
  $entityId,
};
