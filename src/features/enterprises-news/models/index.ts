import './detail.init';
import './page.init';

export {
  PageGate,
  $news,
  $countNews,
  $isErrorDialogOpen,
  $error,
  $isLoading,
  changedErrorDialogVisibility,
  $enterprises,
  changePage,
  changePerPage,
  $perPage,
  $path,
} from './page.model';

export {
  $isDetailOpen,
  openViaUrl,
  changedDetailVisibility,
  $opened,
  $mode,
  changeMode,
  detailSubmitted,
  $isOpenDeleteModal,
  changedDeleteVisibilityModal,
  deleteNews,
  addClicked,
} from './detail.model';
