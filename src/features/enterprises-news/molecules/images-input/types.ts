import { FormikValues } from 'formik';

export interface IFile {
  file: string;
  preview?: string;
  meta: object;
  name: string;
}

export interface IImage {
  mode: string;
  index: number;
  name?: string;
  url?: string;
  preview?: string;
  remove: (id: number) => void;
}

export interface IImageInput {
  mode: string;
  values: FormikValues;
  push: (image: IFile) => void;
  remove: (id: number) => void;
}
