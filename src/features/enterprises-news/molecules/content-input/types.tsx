import { FormikValues, FormikState } from 'formik';
import { WithStyles } from '@mui/styles';
import { contentStyles } from './styles';

export interface IContentInput {
  mode: string;
  form: FormikState<FormikValues>;
  field?: {
    value: string;
    name: string;
  };
  onChange: (e: string) => void;
}

export interface IStyledProps extends WithStyles<typeof contentStyles> {
  text: string;
  title: string;
  placeholder?: string;
}
