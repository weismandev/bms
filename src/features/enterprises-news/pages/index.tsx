import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import i18n from '@shared/config/i18n';
import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
  NoDataPage,
} from '@ui/index';
import {
  $error,
  $isDetailOpen,
  $isErrorDialogOpen,
  $isLoading,
  $isOpenDeleteModal,
  $news,
  $path,
  addClicked,
  changedDeleteVisibilityModal,
  changedErrorDialogVisibility,
  deleteNews,
  PageGate,
} from '../models';
import { NewsContent } from '../organisms/news-content';
import { NewsList } from '../organisms/news-list';

const { t } = i18n;

const NewsEnterprisesPage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isLoading = useStore($isLoading);
  const isOpenDeleteModal = useStore($isOpenDeleteModal);
  const news = useStore($news);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <DeleteConfirmDialog
        content={t('TheNewsWillBeDeletedFromTheRecipients')}
        header={t('Removal')}
        close={() => changedDeleteVisibilityModal(false)}
        isOpen={isOpenDeleteModal}
        confirm={() => deleteNews()}
      />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={null}
        main={
          !isLoading && news.length === 0 ? (
            <NoDataPage onAdd={addClicked} text={t('NoNewsYet')} />
          ) : (
            <NewsList />
          )
        }
        detail={isDetailOpen && <NewsContent />}
        params={{
          mainWidth: !isDetailOpen ? 'auto' : '150px',
          detailWidth: '1fr',
        }}
      />
    </>
  );
};

const RestrictedNewsPage: FC = () => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <NewsEnterprisesPage />
    </HaveSectionAccess>
  );
};

export { RestrictedNewsPage as NewsEnterprisesPage };
