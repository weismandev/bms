export interface INewsFormated {
  id: number;
  text: string;
  title: string;
  image: string | null;
  date: string;
}

export interface INewsData {
  id: number;
  text: string;
  title: string;
  images: IImages[];
  date: string;
}

export interface INewsResponse {
  items: INewsData[];
  meta: {
    total: number;
  };
}

export interface INewsPayload {
  page: number;
  per_page: number;
}

export interface INewsByIdPayload {
  id: number;
}

interface IImages {
  id: number;
  url: string;
}

export interface IEnterprisesResponse {
  enterprises: IEnterprise[];
}

export interface IEnterprise {
  id: number;
  title: string;
  is_verified: boolean;
  employee_is_verified: boolean;
  employee_permissions: string[];
}

interface IStatus {
  id: number;
}

export interface ICreateUpdateNewsPayload {
  id: number | null;
  text: string;
  title: string;
  date: string;
  status: number | IStatus;
  images: {
    id?: number;
    url?: string;
    file?: File;
  }[];
  enterprises: IEnterprise[] | number[];
}

export interface INewsItem {
  id: number;
  text: string;
  title: string;
  date: string;
  status: number;
  images: {
    id: number;
    url: string;
  }[];
  enterprises: IEnterprise[];
}

export interface INews {
  item: INewsItem;
}

export interface IDeleteNewsPayload {
  id: number;
}

export interface IDeleteNewsResponse {
  deleted: boolean;
}
