import { api } from '@api/api2';
import {
  INews,
  INewsPayload,
  INewsResponse,
  INewsByIdPayload,
  ICreateUpdateNewsPayload,
  IDeleteNewsPayload,
  IDeleteNewsResponse,
  IEnterprisesResponse,
} from '../interfaces';

const getNews = (payload: INewsPayload): INewsResponse =>
  api.v1('get', 'client-management/news/list', payload);

const getNewsById = ({ id }: INewsByIdPayload): INews =>
  api.v1('get', 'client-management/news/view', { id });

const createNews = (payload: ICreateUpdateNewsPayload): INews =>
  api.v1('post', 'client-management/news/create', payload);

const updateNews = (payload: ICreateUpdateNewsPayload): INews =>
  api.v1('post', 'client-management/news/update', payload);

const deleteNews = ({ id }: IDeleteNewsPayload): IDeleteNewsResponse =>
  api.v1('post', 'client-management/news/delete', { id });

const getEnterprises = (): IEnterprisesResponse =>
  api.v1('get', 'enterprises/my', { page: 1, per_page: 100 });

export const newsApi = {
  getNews,
  getNewsById,
  createNews,
  updateNews,
  deleteNews,
  getEnterprises,
};
