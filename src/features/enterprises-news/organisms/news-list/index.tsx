import { FC } from 'react';
import { useStore } from 'effector-react';
import {
  Wrapper,
  CustomScrollbar,
  Pagination,
  Toolbar,
  FoundInfo,
  Greedy,
  AddButton,
} from '@ui/index';
import {
  $news,
  $countNews,
  changePage,
  changePerPage,
  $perPage,
  addClicked,
} from '../../models';
import { $currentPage } from '../../models/page.model';
import { NewsCard } from '../news-card';
import { useStyles } from './styles';

const NewsList: FC = () => {
  const classes = useStyles();

  const news = useStore($news);
  const countNews = useStore($countNews);
  const currentPage = useStore($currentPage);
  const perPage = useStore($perPage);

  return (
    <Wrapper className={classes.container}>
      <Toolbar className={classes.toolbar}>
        <FoundInfo count={countNews} />
        <Greedy />
        <AddButton onClick={addClicked} />
      </Toolbar>
      <CustomScrollbar style={{ height: 'calc(100% - 105px)' }} autoHide>
        <ul className={classes.newsList}>
          {news.map((item) => (
            <NewsCard key={item.id} {...item} />
          ))}
        </ul>
      </CustomScrollbar>
      <Pagination
        currentPage={currentPage}
        changePage={changePage}
        rowCount={countNews}
        perPage={perPage}
        changePerPage={changePerPage}
      />
    </Wrapper>
  );
};

export { NewsList };
