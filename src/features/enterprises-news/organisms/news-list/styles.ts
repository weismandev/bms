import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  container: {
    height: '100%',
    padding: '24px 1px 24px 20px',
    overflow: 'hidden',
  },
  newsList: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(330px, 1fr))',
    gap: '15px calc(5px + 2%)',
    padding: '4px 24px 0px 4px',
  },
  scrollbar: {
    height: 'calc(100% - 105px)',
  },
  toolbar: {
    padding: '0 24px 24px 0',
    alignContent: 'center',
  },
});
