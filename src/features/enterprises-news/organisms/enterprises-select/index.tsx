import { useEffect, useState, FC } from 'react';
import classNames from 'classnames';
import { FormLabel, Checkbox, FormControlLabel } from '@mui/material';
import { Add, LocationOn, SaveOutlined, Close } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { declOfNum as declOfNumber } from '@tools/declOfNum';
import { CustomModal, ActionButton, CustomScrollbar, Divider } from '@ui/index';
import { IEnterprise } from '../../interfaces';
import { useStyles } from './styles';
import { IEnterpriseProps, IEnterprisesProps } from './types';

const { t } = i18n;

const formatOnOpen = (enterprises: IEnterprise[]) =>
  enterprises.reduce(
    (acc: Record<number, boolean>, { id: enterpriseId }) => ({
      ...acc,
      [enterpriseId]: true,
    }),
    {}
  );

const formatOnSave = (checkedIds: Record<number, boolean>) =>
  Object.entries(checkedIds)
    .filter(([_, value]) => value === true)
    .map(([key, _]) => ({ id: Number(key) }));

const Enterprise: FC<IEnterpriseProps> = ({
  enterpriseId,
  checkedIds,
  setCheckedIds,
  title,
}) => {
  const classes = useStyles();
  return (
    <div key={enterpriseId}>
      <FormControlLabel
        classes={{
          label: classes.checkboxLabel,
          root: classes.checkboxRoot,
        }}
        control={
          <Checkbox
            name={String(enterpriseId)}
            checked={Boolean(checkedIds[enterpriseId]) || false}
            onChange={({ target: { checked } }) => {
              setCheckedIds({ ...checkedIds, [enterpriseId]: checked });
            }}
          />
        }
        label={title}
      />
    </div>
  );
};

export const SelectEnterprises: FC<IEnterprisesProps> = (props) => {
  const { accountEnterprises, enterprises = [], setEnterprises, mode, error } = props;
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  const [checkedIds, setCheckedIds] = useState<Record<number, boolean>>({});

  const close = () => {
    setCheckedIds({});
    setIsOpen(false);
  };

  const save = () => {
    setEnterprises(formatOnSave(checkedIds));
    close();
  };

  useEffect(() => {
    if (accountEnterprises.length === 1) {
      setEnterprises([{ id: accountEnterprises[0].id }]);
    }
  }, []);

  useEffect(() => {
    if (isOpen) {
      setCheckedIds(formatOnOpen(enterprises));
    }
  }, [isOpen]);

  const count = enterprises.length;
  const isEdit = mode === 'edit' && accountEnterprises.length > 1;

  const content = (
    <div className={classes.modalContainer}>
      <div className={classNames(classes.block, classes.blockBorder)}>
        <div className={classes.scrollContent}>
          <CustomScrollbar>
            {accountEnterprises.map((enterprise: IEnterprise) => (
              <Enterprise
                enterpriseId={enterprise.id}
                key={enterprise.id}
                title={enterprise.title}
                checkedIds={checkedIds}
                setCheckedIds={setCheckedIds}
              />
            ))}
          </CustomScrollbar>
        </div>
      </div>
    </div>
  );

  const actions = (
    <div className={classes.buttons}>
      <ActionButton withMargin kind="positive" onClick={save}>
        <SaveOutlined />
        <div className={classes.iconLabel}>{t('Save')}</div>
      </ActionButton>
      <ActionButton kind="negative" onClick={close}>
        <Close />
        <div className={classes.iconLabel}>{t('Cancellation')}</div>
      </ActionButton>
    </div>
  );

  return (
    <div>
      <FormLabel classes={{ root: classes.label }}>
        {t('navMenu.market.marketCompanies')}
      </FormLabel>
      <div
        onClick={() => (isEdit ? setIsOpen(true) : null)}
        className={classNames(classes.defaultSelect, {
          [classes.select]: isEdit,
        })}
      >
        {count ? (
          <>
            <div className={classes.icon}>
              <LocationOn />
            </div>
            <span>
              {count}{' '}
              {declOfNumber(count, [
                t('company').toLowerCase(),
                t('Companies').toLowerCase(),
                t('companies'),
              ])}{' '}
              {declOfNumber(count, [t('selectedShe'), t('selectedIt'), t('selectedIt')])}
            </span>
          </>
        ) : (
          <>
            <div className={classes.icon}>
              <Add />
            </div>
            {t('SelectCompanies')}
          </>
        )}
      </div>
      {error && <span className={classes.errorText}>{error}</span>}
      <Divider />
      <CustomModal
        {...{ isOpen, content, actions }}
        header={t('ChoiceOfCompanies')}
        onClose={close}
        titleFontSize={24}
        minWidth="70vw"
      />
    </div>
  );
};
