import { FormikErrors } from 'formik';
import { IEnterprise } from '../../interfaces';

export interface IEnterprisesProps {
  accountEnterprises: Array<IEnterprise> | [];
  enterprises: IEnterprise[];
  setEnterprises: (e: Array<{ id: number }>) => void;
  mode: string;
  error?: FormikErrors<any> | string;
}

export interface IEnterpriseProps {
  enterpriseId: number;
  checkedIds: Record<number, boolean>;
  setCheckedIds: (e: Record<number, boolean>) => void;
  title: string;
}
