import { FC } from 'react';
import { useStore } from 'effector-react';
import { Card, CardContent, CardMedia, Typography, CardActionArea } from '@mui/material';
import { VisibilityOff } from '@mui/icons-material';
import { INewsFormated } from '../../interfaces';
import { openViaUrl, $opened } from '../../models';
import { useStyles } from './styles';

const NewsCard: FC<INewsFormated> = ({ id, title, text, image, date }) => {
  const opened = useStore($opened);
  const isActive: boolean = opened.id === id;

  const classes = useStyles({ isActive });

  return (
    <Card classes={{ root: classes.card }}>
      <CardActionArea
        classes={{ root: classes.actionArea }}
        onClick={() => openViaUrl(id)}
      >
        {image ? (
          <CardMedia component="img" height="250" image={image} />
        ) : (
          <div className={classes.visibilityOffIconWrapper}>
            <VisibilityOff className={classes.visibilityOffIcon} />
          </div>
        )}

        <CardContent classes={{ root: classes.cardContent }}>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            style={{ wordWrap: 'break-word' }}
          >
            {title}
          </Typography>

          <Typography variant="body2" color="textSecondary" gutterBottom>
            {text}
          </Typography>
        </CardContent>
        <Typography
          color="textSecondary"
          variant="body2"
          classes={{ root: classes.date }}
        >
          {date}
        </Typography>
      </CardActionArea>
    </Card>
  );
};

export { NewsCard };
