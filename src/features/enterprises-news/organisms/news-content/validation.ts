import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  text: Yup.string().required(t('EnterTheContentOfTheNews')),
  title: Yup.string().required(t('UntitledNewsNotNews')),
  enterprises: Yup.array().min(1, t('SpecifyACompany')).typeError(t('SpecifyACompany')),
});
