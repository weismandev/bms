import { FC } from 'react';
import { useStore } from 'effector-react';
import {
  Formik,
  Form,
  Field,
  FieldProps,
  FieldArray,
  FieldArrayRenderProps,
} from 'formik';
import i18n from '@shared/config/i18n';
import {
  InputField,
  SelectField,
  CustomScrollbar,
  DetailToolbar,
  Wrapper,
} from '@ui/index';
import {
  $opened,
  $mode,
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
  changedDeleteVisibilityModal,
  $enterprises,
  $isLoading,
} from '../../models';
import { ContentInput } from '../../molecules/content-input';
import { ImageInput } from '../../molecules/images-input';
import { SelectEnterprises } from '../enterprises-select';
import { useStyles } from './styles';
import { validationSchema } from './validation';

const { t } = i18n;

export const NewsContent: FC = () => {
  const classes = useStyles();

  const opened = useStore($opened);
  const mode = useStore($mode);
  const accountEnterprises = useStore($enterprises);
  const isLoading = useStore($isLoading);

  const isNewNews = !opened.id;

  if (isLoading) {
    return null;
  }

  return (
    <Wrapper className={classes.formWrapper}>
      <Formik
        initialValues={opened}
        validationSchema={validationSchema}
        onSubmit={detailSubmitted}
        enableReinitialize
        render={({ setFieldValue, values, errors, resetForm }) => (
          <Form className={classes.form}>
            <DetailToolbar
              mode={mode}
              className={classes.detailToolbar}
              hidden={{ send: true }}
              onClose={() => {
                resetForm();
                changedDetailVisibility(false);
              }}
              onDelete={() => changedDeleteVisibilityModal(true)}
              onEdit={() => changeMode('edit')}
              onCancel={() => {
                if (isNewNews) {
                  changedDetailVisibility(false);
                } else {
                  resetForm();
                  changeMode('view');
                }
              }}
            />
            <div className={classes.scrollWrapper}>
              <CustomScrollbar>
                <div className={classes.container}>
                  <div>
                    {mode === 'edit' && (
                      <Field
                        name="title"
                        component={InputField}
                        label={t('title')}
                        placeholder={t('EnterTheTitleOfTheNews')}
                        mode={mode}
                        divider={false}
                      />
                    )}
                    <Field
                      name="text"
                      onChange={(value: string) => setFieldValue('text', value)}
                      mode={mode}
                      component={ContentInput}
                    />
                  </div>
                  <div className={classes.detail}>
                    <SelectEnterprises
                      accountEnterprises={accountEnterprises}
                      enterprises={values.enterprises}
                      //@ts-ignore
                      error={errors.enterprises}
                      setEnterprises={(e: { id: number }[]) =>
                        setFieldValue('enterprises', e)
                      }
                      mode={mode}
                    />
                    <Field
                      name="date"
                      component={InputField}
                      type="date"
                      label={t('NewsDate')}
                      mode="view"
                    />
                    <Field
                      name="status"
                      render={({ field, form }: FieldProps) => (
                        <SelectField
                          field={field}
                          form={form}
                          label={t('Label.status')}
                          options={[
                            { id: 1, title: t('Draft') },
                            { id: 2, title: t('ShePublished') },
                          ]}
                          mode={!isNewNews && field?.value?.id === 2 ? 'view' : mode}
                        />
                      )}
                    />
                    <FieldArray
                      name="images"
                      render={({ push, remove }: FieldArrayRenderProps) => (
                        <ImageInput
                          values={values}
                          push={push}
                          remove={remove}
                          mode={mode}
                        />
                      )}
                    />
                  </div>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};
