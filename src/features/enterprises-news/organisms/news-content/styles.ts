import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateColumns: 'auto 260px',
    paddingBottom: 10,
    margin: '0 24px',
    '@media (max-width: 1024px)': {
      gridTemplateColumns: 'auto',
      paddingBottom: 10,
    },
  },
  detail: {
    paddingLeft: 25,
    '@media (max-width: 1024px)': {
      gridRowStart: 1,
      gridRowEnd: 2,
      paddingLeft: 0,
      paddingRight: 10,
    },
  },
  detailToolbar: {
    padding: 24,
  },
  form: {
    height: 'calc(100% - 52px)',
    position: 'relative',
    '& label': {
      fontSize: 14,
      margin: '10px 0',
      color: '#65657B',
    },
    '& .rdw-link-modal-label, & .rdw-link-modal-target-option': {
      margin: 0,
    },
    '& .rdw-link-modal-buttonsection': {
      margin: '15px auto 0px ',
    },
  },
  formWrapper: {
    height: '100%',
  },
  scrollWrapper: {
    height: 'calc(100% - 82px)',
    padding: '0 6px 24px 0',
  },
  title: {
    border: '1px solid',
  },
});
