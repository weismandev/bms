import { FC } from 'react';
import { useUnit } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $isFilterOpen,
  $isDetailOpen,
} from '../models';
import { Table, Filters, Detail, CopyModal } from '../organisms';

const PassesPage: FC = () => {
  const [isErrorDialogOpen, isLoading, isFilterOpen, isDetailOpen, error] = useUnit([
    $isErrorDialogOpen,
    $isLoading,
    $isFilterOpen,
    $isDetailOpen,
    $error,
  ]);

  const onClose = () => changedErrorDialogVisibility(false);

  return (
    <>
      <PageGate />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <CopyModal />
      <ColoredTextIconNotification />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filters />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

export const RestrictedPassesPage: FC = () => (
  <HaveSectionAccess>
    <PassesPage />
  </HaveSectionAccess>
);
