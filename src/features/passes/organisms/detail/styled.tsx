import styled from '@emotion/styled';

import { Wrapper } from '@ui/index';

interface StyledWrapperProps {
  className?: string;
  children: JSX.Element;
}

export const StyledWrapper = styled(({ className, children }: StyledWrapperProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
`;
