import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import {
  getStringRequiredSchema,
  getObjectRequiredSchema,
  getArrayRequiredSchema,
  getVehicleRequiredSchema,
  getTimePassSchema,
  getDatePassSchema,
  requiredGroupMember,
  getTmcRequiredSchema,
  getPhoneRequiredSchema,
} from '../../libs';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  building: Yup.mixed()
    .nullable()
    .test(
      'building-required',
      `${t('thisIsRequiredField')}`,
      (value, { parent: { id } }) => !id && value
    ),
  ...getStringRequiredSchema('surname'),
  ...getStringRequiredSchema('name'),
  ...getStringRequiredSchema('patronymic'),
  ...getPhoneRequiredSchema('phone'),
  ...getStringRequiredSchema('email'),
  ...getStringRequiredSchema('companyName'),
  ...getObjectRequiredSchema('guestType'),
  documents: Yup.array().of(
    Yup.object().shape({
      ...getStringRequiredSchema('birthday'),
      ...getObjectRequiredSchema('type'),
      ...getStringRequiredSchema('number'),
      ...getStringRequiredSchema('issued_date'),
    })
  ),
  ...getObjectRequiredSchema('enterprise'),
  ...getObjectRequiredSchema('complex'),
  ...getArrayRequiredSchema('properties'),
  ...getObjectRequiredSchema('passType'),
  ...getObjectRequiredSchema('author'),
  ...getStringRequiredSchema('comment'),
  ...getDatePassSchema('activeFrom', t('TheStartDateMustEqualOrEarlierThanTheEndDate')),
  ...getDatePassSchema('activeTo', t('TheEndDateMustEqualOrLaterThanTheStartDate')),
  ...getTimePassSchema('workTimeFrom', t('TheStartTimeMustBeBeforeTheEndTime')),
  ...getTimePassSchema('workTimeTo', t('TheEndTimeMustBeLaterThanTheStartTime')),
  ...getVehicleRequiredSchema(),
  groupCountMember: Yup.object().shape({
    value: Yup.string().test(
      'group-count-required',
      `${t('thisIsRequiredField')}`,
      requiredGroupMember
    ),
  }),
  // ...getTmcRequiredSchema(),
});
