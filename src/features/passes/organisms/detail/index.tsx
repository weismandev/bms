import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik } from 'formik';
import { Pass } from '../../interfaces';
import { $pass, detailSubmitted } from '../../models';
import { PassForm } from '../pass-form';
import { StyledWrapper } from './styled';
import { validationSchema } from './validation';

export const Detail: FC = () => {
  const pass = useUnit($pass);

  return (
    <StyledWrapper>
      <Formik
        initialValues={pass}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={detailSubmitted}
        render={({ values, setFieldValue, resetForm }) => (
          <PassForm
            values={values as Pass}
            setFieldValue={setFieldValue}
            resetForm={resetForm}
          />
        )}
      />
    </StyledWrapper>
  );
};
