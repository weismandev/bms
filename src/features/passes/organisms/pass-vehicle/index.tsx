import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, FastField } from 'formik';
import { InputField, SwitchField, Divider, SelectField } from '@ui/index';
import { Pass, Building, ParkingZone } from '../../interfaces';
import { $mode, $building, $isFilterOpen } from '../../models';
import { ParkingZoneSelectField } from '../parking-zone-select';
import { StyledFields, StyledSubTitle } from '../pass-form/styled';
import { VehicleCategoriesSelectField } from '../vehicle-categories-select';

interface Props {
  values: Pass;
  setFieldValue: (name: string, value: any) => void;
  isNew: boolean;
}

export const PassVehicle: FC<Props> = ({ values, setFieldValue, isNew }) => {
  const { t } = useTranslation();
  const [mode, building, isFilterOpen] = useUnit([$mode, $building, $isFilterOpen]);

  const buildingId = isNew ? (building as Building)?.id : values?.buildingId;
  const activeFrom = values?.activeFrom?.value || null;
  const activeTo = values?.activeTo?.value || values?.activeFrom?.value || null;

  const parkingPlaceOptions = values?.parkingZone?.value?.parkingPlaceList?.length
    ? values?.parkingZone?.value?.parkingPlaceList
    : [];

  const isShowParkingPlace =
    values?.parkingZone?.visible &&
    values?.parkingZone?.value &&
    values.parkingZone.value?.selectMode !== 'full_zone';

  const handleParkingZone = (value: ParkingZone) => {
    setFieldValue('parkingZone.value', value);
    setFieldValue('parkingPlace.value', []);
  };

  const isFieldsExist =
    values?.vehicleCategory?.visible ||
    values?.brand?.visible ||
    values?.govNumver?.visible ||
    values?.parkingZone?.visible ||
    isShowParkingPlace;

  const getVehicleCategoryField = () => {
    if (!values?.vehicleCategory?.visible) {
      return null;
    }

    const name = 'vehicleCategory.value';
    const label = t('Category');
    const placeholder = t('SelectCategory');

    if (mode === 'view') {
      return (
        <FastField
          name={name}
          component={VehicleCategoriesSelectField}
          label={label}
          placeholder={placeholder}
          mode="view"
          divider={false}
        />
      );
    }

    if (mode === 'edit' && values?.vehicleCategory?.required) {
      return (
        <FastField
          name={name}
          component={VehicleCategoriesSelectField}
          label={label}
          placeholder={placeholder}
          divider={false}
          required
          mode="edit"
        />
      );
    }

    if (mode === 'edit' && !values?.vehicleCategory?.required) {
      return (
        <FastField
          name={name}
          component={VehicleCategoriesSelectField}
          label={label}
          placeholder={placeholder}
          divider={false}
          mode="edit"
        />
      );
    }

    return null;
  };

  const getBrandField = () => {
    if (!values?.brand?.visible) {
      return null;
    }

    const name = 'brand.value';
    const label = t('Brand');
    const placeholder = t('EnterBrand');

    if (mode === 'view') {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          mode="view"
          divider={false}
        />
      );
    }

    if (mode === 'edit' && values?.brand?.required) {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          divider={false}
          required
          mode="edit"
        />
      );
    }

    if (mode === 'edit' && !values?.brand?.required) {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          divider={false}
          mode="edit"
        />
      );
    }

    return null;
  };

  const getGovNumberField = () => {
    if (!values?.govNumver?.visible) {
      return null;
    }

    const name = 'govNumver.value';
    const label = t('LicenseNumber');
    const placeholder = t('EnterLicensePlateNumber');

    if (mode === 'view') {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          mode="view"
          divider={false}
        />
      );
    }

    if (mode === 'edit' && values?.govNumver?.required) {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          divider={false}
          mode="edit"
          required
        />
      );
    }

    if (mode === 'edit' && !values?.govNumver?.required) {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          divider={false}
          mode="edit"
        />
      );
    }

    return null;
  };

  const fields = (
    <>
      {(values?.vehicleCategory?.visible ||
        values?.brand?.visible ||
        values?.govNumver?.visible) && (
        <StyledFields isFilterOpen={isFilterOpen}>
          {getVehicleCategoryField()}
          {getBrandField()}
          {getGovNumberField()}
        </StyledFields>
      )}
      {(values?.parkingZone?.visible || isShowParkingPlace) && (
        <StyledFields isFilterOpen={isFilterOpen}>
          {values?.parkingZone?.visible && (
            <Field
              name="parkingZone.value"
              component={ParkingZoneSelectField}
              label={t('ParkingAreaPasses')}
              placeholder={t('ChooseAZone')}
              mode={mode}
              required={values?.parkingZone?.required}
              divider={false}
              activeFrom={activeFrom}
              activeTo={activeTo}
              buildingId={buildingId}
              authorId={values?.author?.visible ? values?.author?.value?.id : null}
              onChange={handleParkingZone}
            />
          )}
          {isShowParkingPlace && (
            <Field
              name="parkingPlace.value"
              component={SelectField}
              label={t('parkingSpace')}
              placeholder={t('ChooseAPlace')}
              mode={mode}
              required={isShowParkingPlace}
              divider={false}
              options={parkingPlaceOptions}
              isMulti
            />
          )}
        </StyledFields>
      )}
      {isFieldsExist && <Divider />}
    </>
  );

  const editMode = (
    <>
      {isFieldsExist && (
        <FastField
          component={SwitchField}
          name="hasVehicle.value"
          label={t('VehicleAvailability')}
          labelPlacement="end"
          divider={false}
        />
      )}
      {values?.hasVehicle?.value && fields}
      {isFieldsExist && !values?.hasVehicle?.value && <Divider />}
    </>
  );

  const viewMode =
    values?.hasVehicle?.value && isFieldsExist ? (
      <>
        <StyledSubTitle>{t('vhcl')}</StyledSubTitle>
        {fields}
      </>
    ) : null;

  return mode === 'view' ? viewMode : editMode;
};
