import styled from '@emotion/styled';

import { Wrapper } from '@ui/index';

interface StyledWrapperProps {
  className?: string;
  children: JSX.Element;
}

export const StyledWrapper = styled(({ className, children }: StyledWrapperProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
  padding: 24px;
`;

export const StyledFormatterContainer = styled.div(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'start',
  marginTop: -1,
}));

export const StyledFirstValue = styled.div(() => ({
  marginTop: 5,
}));

export const StyledOtherValues = styled.div(() => ({
  display: 'grid',
  gap: 10,
  padding: 10,
  background: '#fcfcfc',
}));
