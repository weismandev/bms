import { memo, FC } from 'react';
import { useUnit } from 'effector-react';
import { CheckCircleRounded, Cancel } from '@mui/icons-material';

import { DataGrid } from '@features/data-grid';
import { Popover, IconButton } from '@ui/index';

import {
  $tableData,
  $isLoadingPasses,
  $selectRow,
  selectionRowChanged,
  sortChanged,
  $pageSize,
  $currentPage,
  $countPage,
  pageNumberChanged,
  pageSizeChanged,
  $sorting,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
  openViaUrl,
} from '../../models';
import { TableChip } from '../../atoms';
import { CustomToolbar } from '../../molecules';
import { useStyles } from './styles';
import {
  StyledWrapper,
  StyledFormatterContainer,
  StyledFirstValue,
  StyledOtherValues,
} from './styled';

export const Table: FC = memo(() => {
  const [
    tableData,
    isLoading,
    selectRow,
    sorting,
    visibilityColumns,
    columns,
    pageSize,
    currentPage,
    countPage,
  ] = useUnit([
    $tableData,
    $isLoadingPasses,
    $selectRow,
    $sorting,
    $visibilityColumns,
    $columns,
    $pageSize,
    $currentPage,
    $countPage,
  ]);
  const classes = useStyles();

  const propertiesIndex = columns.findIndex((column) => column.field === 'properties');
  // const hasStuffIndex = columns.findIndex((column) => column.field === 'hasStuff'); тмц пока скрыто
  const statusIndex = columns.findIndex((column) => column.field === 'status');
  // const stuffStatusIndex = columns.findIndex((column) => column.field === 'stuffStatus'); временно скрыто тмц

  columns[propertiesIndex].renderCell = ({ value }: { value: string[] }) => {
    if (!value || value[0]?.length === 0 || value.length === 0) {
      return '-';
    }

    return (
      <StyledFormatterContainer>
        <StyledFirstValue>{value[0]}</StyledFirstValue>
        {value[1] && (
          <Popover
            trigger={
              <IconButton size="large">
                <span>+{value.slice(1).length}</span>
              </IconButton>
            }
          >
            <StyledOtherValues>
              {value.slice(1).map((item: string, index: number) => (
                <div key={index}>{item}</div>
              ))}
            </StyledOtherValues>
          </Popover>
        )}
      </StyledFormatterContainer>
    );
  };

  // columns[hasStuffIndex].renderCell = ({ value }) =>
  //   !value ? <Cancel color="disabled" /> : <CheckCircleRounded color="primary" />; тмц пока скрыто

  columns[statusIndex].renderCell = ({
    value,
  }: {
    value: { style: { color?: string; backgroundColor?: string }; title: string };
  }) => <TableChip value={value} />;

  // columns[stuffStatusIndex].renderCell = ({
  //   value,
  // }: {
  //   value: { style: { color?: stri ng; backgroundColor?: string }; title: string };
  // }) => <TableChip value={value} />; временно скрыто тмц

  const params = {
    classes: { root: classes.tableRoot },
    tableData,
    columns,
    initialState: {
      sorting: {
        sortModel: sorting,
      },
    },
    visibilityColumns,
    visibilityColumnsChanged,
    open: openViaUrl,
    isLoading,
    selectionRowChanged,
    selectRow,
    sortChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    pageSize,
    currentPage,
    countPage,
    pageNumberChanged,
    pageSizeChanged,
  };

  const toolbar = <CustomToolbar />;

  return (
    <StyledWrapper>
      <DataGrid.Full params={params} toolbar={toolbar} />
    </StyledWrapper>
  );
});
