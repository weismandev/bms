import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field } from 'formik';
import { Divider } from '@ui/index';
import { Pass } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { PassSectionWrapper } from '../../molecules';
import { EnterprisesSelectField } from '../enterprises-select';
import { StyledAdaptiveField } from '../pass-form/styled';

interface Props {
  values: Pass;
  isNew: boolean;
}

export const PassCompany: FC<Props> = ({ values, isNew }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen] = useUnit([$mode, $isFilterOpen]);

  const section = {
    name: 'visit_company',
    title: t('DestinationCompany'),
  };

  const buildingId = isNew ? values?.building?.id : values?.buildingId;

  if (!values?.enterprise?.visible) {
    return null;
  }

  return (
    <PassSectionWrapper name={section.title}>
      <div>
        <StyledAdaptiveField isFilterOpen={isFilterOpen}>
          <Field
            name="enterprise.value"
            component={EnterprisesSelectField}
            label={t('company')}
            placeholder={t('ChooseCompany')}
            mode={mode}
            required={values?.enterprise?.required}
            divider={false}
            buildingId={buildingId}
          />
        </StyledAdaptiveField>
        <Divider />
      </div>
    </PassSectionWrapper>
  );
};
