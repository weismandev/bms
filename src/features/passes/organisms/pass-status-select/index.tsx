import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, fxGetList, $isLoading } from '../../models/pass-status-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const PassStatusSelect: FC<object> = (props) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const PassStatusSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PassStatusSelect />} onChange={onChange} {...props} />;
};
