import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FastField, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { Divider } from '@ui/index';
import { Pass, Building } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { PassSectionWrapper } from '../../molecules';
import { PropertySelectField } from '../property-select';
import { StyledTwoFields } from './styled';

interface Props {
  values: Pass;
  building: Building;
  setFieldValue: (name: string, value: any) => void;
}

export const PassPlace: FC<Props> = ({ values, building, setFieldValue }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen] = useUnit([$mode, $isFilterOpen]);

  if (!values.id && building?.complex && !values?.complex?.value) {
    setFieldValue('complex.value', building.complex);
  }

  const isShowForm = values?.complex?.visible || values?.properties?.visible;

  if (!isShowForm) {
    return null;
  }

  return (
    <PassSectionWrapper name={t('Destination')}>
      <div>
        {(values?.complex?.visible || values?.properties?.visible) && (
          <>
            <StyledTwoFields isFilterOpen={isFilterOpen}>
              {values?.properties?.visible && (
                <Field
                  name="properties.value"
                  component={PropertySelectField}
                  label={t('room')}
                  placeholder={t('ChooseRoom')}
                  mode={mode}
                  isMulti
                  complexId={values?.complex?.value?.id}
                  required={values?.properties?.required}
                  divider={false}
                />
              )}
              {values?.complex?.visible && (
                <FastField
                  name="complex.value"
                  component={ComplexSelectField}
                  label={t('AnObject')}
                  placeholder={t('selectObjects')}
                  mode="view"
                  required={values?.complex?.required}
                  divider={false}
                />
              )}
            </StyledTwoFields>
            <Divider />
          </>
        )}
      </div>
    </PassSectionWrapper>
  );
};
