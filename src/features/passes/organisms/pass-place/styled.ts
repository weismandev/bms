import styled from '@emotion/styled';

export const StyledTwoFields = styled.div(
  ({ isFilterOpen }: { isFilterOpen: boolean }) => ({
    marginTop: 20,
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    [`@media (max-width: ${isFilterOpen ? 1300 : 1100}px)`]: {
      gridTemplateColumns: 'repeat(1, 1fr)',
    },
    gap: 15,
  })
);
