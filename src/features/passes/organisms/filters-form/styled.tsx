import styled from '@emotion/styled';
import { Form } from 'formik';

interface StyledProps {
  className?: string;
  children: JSX.Element[];
}

export const StyledForm = styled(({ className, children }: StyledProps) => (
  <Form className={className}>{children}</Form>
))`
  padding: 0px 24px 24px;
`;
