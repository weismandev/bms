import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { EnterpriseSelectField } from '@features/enterprise-select';
import { PassTypesSelectField } from '@features/pass-types-select';
import { FilterFooter, SelectField, SwitchField } from '@ui/index';
import { PassStatusSelectField } from '../pass-status-select';
import { StyledForm } from './styled';

export const FiltersForm: FC = () => {
  const { t } = useTranslation();

  const vehicleOptions = [
    { id: 1, title: t('WithVehicle') },
    { id: 2, title: t('WithoutVehicle') },
  ];

  return (
    <StyledForm>
      <Field
        component={SwitchField}
        kind="switch"
        name="isArchive"
        label={t('ArchivePasses')}
        labelPlacement="start"
      />
      <Field
        name="enterprise"
        component={EnterpriseSelectField}
        label={t('company')}
        placeholder={t('ChooseCompany')}
      />
      <Field
        name="complexes"
        component={ComplexSelectField}
        label={t('AnObject')}
        placeholder={t('selectObjects')}
        isMulti
      />
      <Field
        name="status"
        component={PassStatusSelectField}
        label={t('Label.status')}
        placeholder={t('selectStatuse')}
      />
      <Field
        name="passTypes"
        component={PassTypesSelectField}
        label={t('PassesType')}
        placeholder={t('chooseType')}
        isMulti
      />
      <Field
        name="vehicle"
        component={SelectField}
        label={t('VehicleAvailability')}
        placeholder={t('SelectVehicleAvailability')}
        options={vehicleOptions}
        isMulti
      />
      <FilterFooter isResettable />
    </StyledForm>
  );
};
