import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import copyToClipboard from 'copy-to-clipboard';
import { Modal, ActionButton } from '@ui/index';
import { $copyModalData, closeCopyModalData } from '../../models';
import { useStyles, Header, Content, Actions, styles } from './styled';

export const CopyModal: FC = () => {
  const { t } = useTranslation();
  const copyModalData = useUnit($copyModalData);

  const classes = useStyles();

  if (!copyModalData?.isOpen || !copyModalData?.content) {
    return null;
  }

  const handleClose = () => closeCopyModalData();
  const handleCopy = () => {
    if (copyModalData?.content) {
      copyToClipboard(copyModalData.content);

      handleClose();
    }
  };

  const header = <Header>{t('PassData')}</Header>;
  const content = <Content>{copyModalData?.content}</Content>;

  const actions = (
    <Actions>
      <ActionButton onClick={handleCopy}>{t('CopyAndClose')}</ActionButton>
      <ActionButton style={styles.cancelButton} onClick={closeCopyModalData}>
        {t('Cancellation')}
      </ActionButton>
    </Actions>
  );

  return (
    <Modal
      isOpen={copyModalData.isOpen}
      onClose={handleClose}
      header={header}
      content={content}
      actions={actions}
      classes={{ paper: classes.modal }}
    />
  );
};
