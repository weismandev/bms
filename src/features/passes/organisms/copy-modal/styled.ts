import { styled } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  modal: {
    minWidth: 250,
  },
});

export const Header = styled('div')(() => ({
  fontWeight: 500,
  fontSize: 24,
  lineHeight: '32px',
  color: '#000000DE',
}));

export const Content = styled('div')(() => ({
  border: '1px solid #191C293B',
  borderRadius: 16,
  padding: '4px 12px',
  fontSize: 14,
  fontStyle: 'normal',
  fontWeight: 400,
  lineHeight: '20px',
  color: 'rgba(25, 28, 41, 0.87)',
  margin: '24px 0px',
}));

export const Actions = styled('div')(() => ({
  display: 'flex',
  gap: 8,
}));

export const styles = {
  cancelButton: {
    backgroundColor: '#FFF',
    color: '#1F87E5',
    border: '1px solid rgba(31, 135, 229, 0.50)',
    boxShadow: 'none',
  },
};
