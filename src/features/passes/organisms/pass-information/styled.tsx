import { Typography } from '@mui/material';
import styled from '@emotion/styled';

export const StyledLabelStatusField = styled('div')(() => ({
  marginTop: -1,
}));

export const StyledStatusField = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  color: rgba(0, 0, 0, 0.54);
  margin-bottom: 10px;
  line-height: 16px;
`;

export const StyledTwoFields = styled.div(({ isFilterOpen }: any) => ({
  marginTop: 20,
  display: 'grid',
  gridTemplateColumns: '1fr 2fr',
  [`@media (max-width: ${isFilterOpen ? 1300 : 1100}px)`]: {
    gridTemplateColumns: 'repeat(1, 1fr)',
  },
  gap: 15,
}));
