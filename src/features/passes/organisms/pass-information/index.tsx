import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FastField, Field } from 'formik';
import { Chip } from '@mui/material';
import { PassTypesSelectField } from '@features/pass-types-select';
import { PeopleSelectField } from '@features/people-select';
import { InputField, Divider } from '@ui/index';
import { Pass, Value, Building } from '../../interfaces';
import { $mode, $isFilterOpen, $building } from '../../models';
import { PassSectionWrapper } from '../../molecules';
import { StyledFields } from '../pass-form/styled';
import { StyledStatusField, StyledLabelStatusField, StyledTwoFields } from './styled';

interface Props {
  values: Pass;
  isNew: boolean;
  setFieldValue: (name: string, value: any) => void;
}

export const PassInformation: FC<Props> = ({ values, isNew, setFieldValue }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen, building] = useUnit([$mode, $isFilterOpen, $building]);

  const buildingId = isNew ? (building as Building)?.id : values?.buildingId;

  const isShowForm =
    mode === 'view'
      ? values?.passType?.visible ||
        values?.passNumber?.visible ||
        values?.author?.visible
      : values?.passType?.visible || values?.author?.visible;

  const object = buildingId ? [{ id: buildingId, type: 'building' }] : null;

  const viewMode = (
    <>
      {(values?.passType?.visible || values?.passNumber?.visible) && (
        <>
          <StyledFields isFilterOpen={isFilterOpen}>
            {values?.passType?.visible && (
              <Field
                name="passType.value"
                component={PassTypesSelectField}
                label={t('PassesType')}
                placeholder={t('chooseType')}
                mode="view"
                divider={false}
                buildingId={buildingId}
              />
            )}
            {!isNew && values?.passStatus?.visible && (
              <StyledLabelStatusField>
                <StyledStatusField>{t('Label.status')}</StyledStatusField>
                <Chip
                  label={values.passStatus?.value?.title || t('Unknown')}
                  size="small"
                  style={values.passStatus.value?.style}
                />
              </StyledLabelStatusField>
            )}
            {!isNew && values?.passNumber?.visible && (
              <FastField
                name="passNumber.value"
                component={InputField}
                mode="view"
                label={t('PassNumber')}
                placeholder=""
                divider={false}
              />
            )}
          </StyledFields>
          <Divider />
        </>
      )}
      {values?.author?.visible && (
        <>
          <StyledTwoFields isFilterOpen={isFilterOpen}>
            {!isNew && values?.passCreatedAt?.visible && (
              <FastField
                name="passCreatedAt.value"
                component={InputField}
                label={t('DateOfCreation')}
                mode="view"
                type="date"
                divider={false}
              />
            )}
            <FastField
              name="author.value"
              component={PeopleSelectField}
              label={t('AuthorApplicant')}
              placeholder={t('ChooseAnApplicant')}
              mode="view"
              divider={false}
              object={object}
            />
          </StyledTwoFields>
          <Divider />
        </>
      )}
    </>
  );

  const handlePassType = (value: Value) => {
    setFieldValue('passType.value', value);
    setFieldValue('activeFrom.value', '');
    setFieldValue('activeTo.value', '');
    setFieldValue('workTimeFrom.value', '');
    setFieldValue('workTimeTo.value', '');
  };

  const handleAuthor = (value: Value) => {
    setFieldValue('author.value', value);
    setFieldValue('parkingZone.value', '');
    setFieldValue('parkingPlace.value', []);
  };

  const editMode =
    values?.passType?.visible || values?.author?.visible ? (
      <>
        <StyledTwoFields isFilterOpen={isFilterOpen}>
          <>
            {values?.passType?.visible && (
              <Field
                name="passType.value"
                component={PassTypesSelectField}
                label={t('PassesType')}
                placeholder={t('chooseType')}
                mode={values?.passType?.disabled ? 'view' : mode}
                required={values?.passType?.required}
                divider={false}
                onChange={handlePassType}
                buildingId={buildingId}
              />
            )}
            {values?.author?.visible && (
              <Field
                name="author.value"
                component={PeopleSelectField}
                label={t('AuthorApplicant')}
                placeholder={t('ChooseAnApplicant')}
                required={values?.author?.required}
                divider={false}
                mode="edit"
                onChange={handleAuthor}
                object={object}
              />
            )}
          </>
        </StyledTwoFields>
        <Divider />
      </>
    ) : (
      <div />
    );

  if (!isShowForm) {
    return null;
  }

  return (
    <PassSectionWrapper name={t('InformationAboutThePass')}>
      {mode === 'view' ? viewMode : editMode}
    </PassSectionWrapper>
  );
};
