import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, fxGetList, $isLoading } from '../../models/enterprises-select';

type SelectFieldProps = {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
};

type Props = {
  buildingId?: number;
};

export const EnterprisesSelect: FC<Props> = ({ buildingId, ...props }) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    fxGetList({ filter: { building_id: buildingId } });
  }, [buildingId]);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const EnterprisesSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<EnterprisesSelect />} onChange={onChange} {...props} />;
};
