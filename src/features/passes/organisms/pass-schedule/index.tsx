import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { format } from 'date-fns';
import { FastField, Field } from 'formik';
import { InputField, Divider } from '@ui/index';
import { MaxDays } from '../../atoms';
import { Pass } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { StyledSubTitle, StyledFields } from '../pass-form/styled';
import {
  StyledFastDateSetter,
  StyledDateFields,
  StyledDataFieldSeparator,
  StyledField,
  StyledTwoField,
  StyledDateField,
  StyledTimeField,
} from './styled';

interface Props {
  values: Pass;
  isNew: boolean;
  setFieldValue: (name: string, value: any) => void;
}

export const PassSchedule: FC<Props> = ({ values, isNew, setFieldValue }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen] = useUnit([$mode, $isFilterOpen]);

  const isPpassTypeOneTime = values?.passType?.value?.id === 1;

  const handleDates = (value: string) => {
    const date = format(new Date(value), 'yyyy-MM-dd');

    setFieldValue('activeFrom.value', date);

    if (!isPpassTypeOneTime) {
      setFieldValue('activeTo.value', date);
    }

    if (values?.workTimeFrom?.visible && isPpassTypeOneTime) {
      setFieldValue('workTimeFrom.value', '00:00');
    }

    if (values?.workTimeTo?.visible && isPpassTypeOneTime) {
      setFieldValue('workTimeTo.value', '23:59');
    }

    setFieldValue('parkingZone.value', '');
    setFieldValue('parkingPlace.value', []);
  };

  const selectedDate = values?.activeFrom?.value ? values.activeFrom.value : new Date();

  const handleActiveFrom = ({ target: { value } }: { target: { value: string } }) => {
    setFieldValue('activeFrom.value', value);
    setFieldValue('parkingZone.value', '');
    setFieldValue('parkingPlace.value', []);
  };

  const handleToFrom = ({ target: { value } }: { target: { value: string } }) => {
    setFieldValue('activeTo.value', value);
    setFieldValue('parkingZone.value', '');
    setFieldValue('parkingPlace.value', []);
  };

  const isShowForm = values?.activeFrom?.visible || values?.activeTo?.visible;

  const editMode = (
    <>
      {(values?.activeFrom?.visible || values?.activeTo?.visible) && (
        <StyledTwoField
          isFilterOpen={isFilterOpen}
          isPpassTypeOneTime={isPpassTypeOneTime}
        >
          <StyledDateFields>
            {values?.activeFrom?.visible && (
              <StyledDateField>
                <Field
                  name="activeFrom.value"
                  component={InputField}
                  label={
                    isPpassTypeOneTime
                      ? t('DayValidityPass')
                      : t('ValidityPeriodOfThePass')
                  }
                  type="date"
                  required={values?.activeFrom?.required}
                  onChange={handleActiveFrom}
                  divider={false}
                  mode="edit"
                />
              </StyledDateField>
            )}
            {values?.activeFrom?.visible &&
              values?.activeTo?.visible &&
              !isPpassTypeOneTime && (
                <StyledDataFieldSeparator>—</StyledDataFieldSeparator>
              )}
            {values?.activeTo?.visible && !isPpassTypeOneTime && (
              <StyledField>
                <StyledDateField>
                  {values?.activeTo?.required ? (
                    <FastField
                      name="activeTo.value"
                      component={InputField}
                      label={null}
                      type="date"
                      required
                      onChange={handleToFrom}
                      divider={false}
                      mode="edit"
                    />
                  ) : (
                    <FastField
                      name="activeTo.value"
                      component={InputField}
                      label={null}
                      type="date"
                      onChange={handleToFrom}
                      divider={false}
                      mode="edit"
                    />
                  )}
                </StyledDateField>
              </StyledField>
            )}
          </StyledDateFields>
          {isNew && (
            <StyledFastDateSetter
              value={new Date(selectedDate)}
              onChange={handleDates}
              disablePadding
              size="small"
              today={t('Today')}
              tomorrow={t('Tomorrow')}
              plusDay={t('+1day')}
              isFilterOpen={isFilterOpen}
            />
          )}
        </StyledTwoField>
      )}
      {isNew && values?.activeFrom?.visible && values?.activeTo?.visible && <MaxDays />}
      <Divider />
      {isPpassTypeOneTime && (
        <>
          <StyledDateFields>
            <StyledTimeField>
              <FastField
                name="workTimeFrom.value"
                component={InputField}
                label={t('PassValidityPeriod')}
                type="time"
                required={isPpassTypeOneTime}
                divider={false}
                mode="edit"
              />
            </StyledTimeField>
            <StyledDataFieldSeparator>—</StyledDataFieldSeparator>
            <StyledField>
              <StyledTimeField>
                <FastField
                  name="workTimeTo.value"
                  component={InputField}
                  label={null}
                  type="time"
                  required={isPpassTypeOneTime}
                  divider={false}
                  mode="edit"
                />
              </StyledTimeField>
            </StyledField>
          </StyledDateFields>
          <Divider />
        </>
      )}
    </>
  );

  const getDataFieldValue = () => {
    const { activeFrom, activeTo } = values;

    if (activeFrom?.visible || activeTo?.visible) {
      if (activeFrom?.value && isPpassTypeOneTime) {
        return `${format(new Date(activeFrom.value), 'dd.MM.yyyy')}`;
      }

      if (activeFrom?.value && activeTo?.value) {
        return `${format(new Date(activeFrom.value), 'dd.MM.yyyy')} — ${format(
          new Date(activeTo.value),
          'dd.MM.yyyy'
        )}`;
      }

      if (activeFrom?.value && !activeTo?.value) {
        return `${format(new Date(activeFrom.value), 'dd.MM.yyyy')}`;
      }

      if (!activeFrom?.value && activeTo?.value) {
        return `${format(new Date(activeTo.value), 'dd.MM.yyyy')}`;
      }
    }

    return '';
  };

  const viewMode =
    values?.activeFrom?.visible ||
    values?.activeTo?.visible ||
    values?.workTimeFrom?.visible ||
    values?.workTimeTo?.visible ? (
      <>
        <StyledFields>
          {(values?.activeFrom?.visible || values?.activeTo?.visible) && (
            <FastField
              component={InputField}
              label={
                isPpassTypeOneTime ? t('DayValidityPass') : t('ValidityPeriodOfThePass')
              }
              mode="view"
              divider={false}
              value={getDataFieldValue()}
            />
          )}
          {isPpassTypeOneTime && (
            <FastField
              component={InputField}
              label={t('PassValidityPeriod')}
              mode="view"
              divider={false}
              value={
                values?.workTimeFrom?.value && values?.workTimeTo?.value
                  ? `${values.workTimeFrom.value} — ${values.workTimeTo.value}`
                  : ''
              }
            />
          )}
        </StyledFields>
        <Divider />
      </>
    ) : (
      <div />
    );

  if (!isShowForm) {
    return null;
  }

  return (
    <div>
      <StyledSubTitle>{t('PassSchedule')}</StyledSubTitle>
      {mode === 'view' ? viewMode : editMode}
    </div>
  );
};
