import styled from '@emotion/styled';
import { FastDateSetter } from '@ui/index';

interface StyledFastDateProps {
  className?: string;
  onChange: (value: string) => void;
  value: Date;
  disablePadding: boolean;
  size: string;
  today: string;
  tomorrow: string;
  plusDay: string;
  isFilterOpen: boolean;
}

export const StyledFastDateSetter = styled(
  ({
    className,
    onChange,
    value,
    disablePadding,
    size,
    today,
    tomorrow,
    plusDay,
  }: StyledFastDateProps) => (
    <FastDateSetter
      className={className}
      onChange={onChange}
      value={value}
      disablePadding={disablePadding}
      size={size}
      today={today}
      tomorrow={tomorrow}
      plusDay={plusDay}
    />
  )
)(({ isFilterOpen }) => ({
  border: '1px solid #1f87e5',
  borderRadius: 17,
  height: 32,
  marginTop: 26,
  [`@media (max-width: ${isFilterOpen ? 1700 : 1520}px)`]: {
    marginTop: 0,
  },
}));

export const StyledDateFields = styled('div')(() => ({
  display: 'flex',
  gap: 12,
  flexDirection: 'row',
}));

export const StyledDataFieldSeparator = styled('div')(() => ({
  marginTop: 30,
}));

export const StyledField = styled('div')(() => ({
  marginTop: 26,
}));

export const StyledTwoField = styled('div')(
  ({
    isFilterOpen,
    isPpassTypeOneTime,
  }: {
    isFilterOpen: boolean;
    isPpassTypeOneTime: boolean;
  }) => ({
    marginTop: 20,
    display: 'grid',
    gridTemplateColumns: `${isPpassTypeOneTime ? 1 : 2}fr 1fr`,
    [`@media (max-width: ${isFilterOpen ? 1700 : 1520}px)`]: {
      gridTemplateColumns: '1fr',
    },
    gap: 15,
  })
);

export const StyledDateField = styled('div')(() => ({
  width: 171,
}));

export const StyledTimeField = styled('div')(() => ({
  width: 120,
}));
