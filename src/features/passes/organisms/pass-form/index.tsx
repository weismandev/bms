import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FastField, Field } from 'formik';
import {
  HouseSelectField,
  $data as $buildings,
  $isLoading as $isLoadingBuildings,
} from '@features/house-select';
import { CustomScrollbar, SwitchControl, InputField, Divider } from '@ui/index';
import { Pass, Building } from '../../interfaces';
import {
  $pass,
  changeMode,
  changedDetailVisibility,
  revokePass,
  copyPass,
  dublicatePass,
  activatePass,
  $building,
  changeBuilding,
  $mode,
  sendSms,
  $onlyConfiguratorFields,
  changeOnlyConfiguratorFields,
} from '../../models';
import { DetailToolbar } from '../../molecules';
import { PassCompany } from '../pass-company';
import { PassGeneral } from '../pass-general';
import { PassGroup } from '../pass-group';
import { PassInformation } from '../pass-information';
import { PassPlace } from '../pass-place';
import { PassSchedule } from '../pass-schedule';
import { PassVehicle } from '../pass-vehicle';
import {
  StyledForm,
  StyledContent,
  StyledFormContent,
  StyledField,
  styles,
} from './styled';

interface Props {
  values: Pass;
  setFieldValue: (name: string, value: any) => void;
  resetForm: () => void;
}

export const PassForm: FC<Props> = ({ values, setFieldValue, resetForm }) => {
  const { t } = useTranslation();
  const [pass, building, mode, onlyConfiguratorFields, buildings, isLoadingBuildings] =
    useUnit([
      $pass,
      $building,
      $mode,
      $onlyConfiguratorFields,
      $buildings,
      $isLoadingBuildings,
    ]);

  const isNew = !pass.id;
  const title = isNew ? t('NewPass') : pass?.passNumber?.value;

  const revokeStatuses =
    pass?.passStatus?.value?.slug === 'active' ||
    pass?.passStatus?.value?.slug === 'waiting';

  const isShowRevokeButton = isNew ? false : revokeStatuses;
  const isShowCopyButton = isNew ? false : pass?.passStatus?.value?.slug === 'active';
  const isShowDublicateButton = !isNew;
  const isShowConfirmButton = isNew ? false : pass?.passStatus?.value?.slug === 'waiting';
  const isShowSmsButton = isNew ? false : pass?.passStatus?.value?.slug === 'active';
  const isShowFields = mode === 'view' ? true : Boolean(building);
  const isDisabledSmsButton = pass?.phone?.value?.length === 0;
  const isDisabledDublicateButton = !buildings.length || isLoadingBuildings;

  const handleConfiguratorFields = ({
    target: { checked },
  }: {
    target: { checked: boolean };
  }) => {
    changeOnlyConfiguratorFields(checked);
  };

  const handleBuilding = (value: Building) => {
    setFieldValue('building', value);
    changeBuilding(value);
  };

  const onCancel = () => {
    if (!isNew) {
      resetForm();
      changeMode('view');
    } else {
      changedDetailVisibility(false);
    }
  };

  const onClose = () => changedDetailVisibility(false);

  return (
    <StyledForm>
      <DetailToolbar
        isNew={isNew}
        onCancel={onCancel}
        onClose={onClose}
        show={{
          revoke: Boolean(isShowRevokeButton),
          copy: Boolean(isShowCopyButton),
          dublicate: Boolean(isShowDublicateButton),
          confirm: Boolean(isShowConfirmButton),
          sms: Boolean(isShowSmsButton),
        }}
        disabled={{
          sms: isDisabledSmsButton,
          dublicate: isDisabledDublicateButton,
        }}
        revokePass={revokePass}
        copyPass={copyPass}
        dublicatePass={dublicatePass}
        activatePass={activatePass}
        sendSms={sendSms}
        title={title}
      />
      <StyledContent>
        <CustomScrollbar>
          <StyledFormContent>
            {isNew && mode === 'edit' && (
              <FastField
                name="building"
                component={HouseSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                required
                value={building}
                onChange={handleBuilding}
              />
            )}
            {!isNew && (
              <>
                <SwitchControl
                  kind="switch"
                  labelPlacement="end"
                  divider={false}
                  label={t('ShowOnlyConfiguratorFields')}
                  value={onlyConfiguratorFields}
                  onChange={handleConfiguratorFields}
                />
                <Divider />
              </>
            )}
            {isShowFields && (
              <>
                <PassGeneral
                  values={values}
                  isNew={isNew}
                  setFieldValue={setFieldValue}
                />
                <PassCompany values={values} isNew={isNew} />
                <PassPlace
                  values={values}
                  building={building as Building}
                  setFieldValue={setFieldValue}
                />
                <PassInformation
                  values={values}
                  isNew={isNew}
                  setFieldValue={setFieldValue}
                />
                <PassSchedule
                  values={values}
                  isNew={isNew}
                  setFieldValue={setFieldValue}
                />
                <PassVehicle
                  values={values}
                  isNew={isNew}
                  setFieldValue={setFieldValue}
                />
                <PassGroup values={values} />
                {values?.comment?.visible && (
                  <StyledField style={styles.commentField}>
                    <Field
                      name="comment.value"
                      component={InputField}
                      label={t('Label.comment')}
                      placeholder={t('EnterComment')}
                      mode={mode}
                      required={values?.comment?.required}
                      multiline
                      rowsMax={10}
                      divider={false}
                    />
                  </StyledField>
                )}
              </>
            )}
            {/* <PassTmc values={values} /> временно отключено */}
          </StyledFormContent>
        </CustomScrollbar>
      </StyledContent>
    </StyledForm>
  );
};
