import { Form } from 'formik';
import { Typography } from '@mui/material';
import styled from '@emotion/styled';

interface StyledFormProps {
  className?: string;
  children: JSX.Element[];
}

export const StyledForm = styled(({ className, children }: StyledFormProps) => (
  <Form className={className}>{children}</Form>
))`
  height: calc(100% - 50px);
  position: relative;
`;

export const StyledContent = styled.div(() => ({
  height: 'calc(100% - 92px)',
  padding: '0px 0px 24px 24px',
}));

export const StyledFormContent = styled.div(() => ({
  paddingRight: 24,
}));

export const StyledFields = styled.div(({ isFilterOpen }: any) => ({
  marginTop: 20,
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  [`@media (max-width: ${isFilterOpen ? 1700 : 1520}px)`]: {
    gridTemplateColumns: 'repeat(2, 1fr)',
  },
  [`@media (max-width: ${isFilterOpen ? 1300 : 1100}px)`]: {
    gridTemplateColumns: 'repeat(1, 1fr)',
  },
  gap: 15,
}));

export const StyledField = styled.div(() => ({
  marginTop: 20,
}));

export const StyledSubTitle = styled(Typography)`
  font-weight: 500;
  font-size: 14px;
  color: #191c29;
`;

export const StyledAdaptiveField = styled.div(
  ({ isFilterOpen }: { isFilterOpen: boolean }) => ({
    marginTop: 20,
    width: '50%',
    [`@media (max-width: ${isFilterOpen ? 1300 : 1100}px)`]: {
      width: '100%',
    },
  })
);

export const styles = {
  commentField: {
    marginBottom: 5,
  },
};
