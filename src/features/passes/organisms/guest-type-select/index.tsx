import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, fxGetList, $isLoading } from '../../models/guest-type-select';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  buildingId?: string | null;
}

export const GuestTypeSelect: FC<Props> = ({ buildingId, ...props }) => {
  const [options, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    if (buildingId) {
      fxGetList({ building_id: buildingId });
    }
  }, [buildingId]);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const GuestTypeSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<GuestTypeSelect />} onChange={onChange} {...props} />;
};
