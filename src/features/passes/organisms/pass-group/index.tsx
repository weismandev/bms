import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FastField } from 'formik';
import { InputField, SwitchField, Divider } from '@ui/index';
import { Pass } from '../../interfaces';
import { $mode } from '../../models';
import { StyledFields, StyledSubTitle } from '../pass-form/styled';

interface Props {
  values: Pass;
}

export const PassGroup: FC<Props> = ({ values }) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);

  const getGroupCountEditModeField = () => {
    const name = 'groupCountMember.value';
    const label = t('NumberOfPersons');
    const placeholder = t('EnterNumberOfPersons');

    if (values?.groupCountMember?.required) {
      return (
        <FastField
          name={name}
          component={InputField}
          label={label}
          placeholder={placeholder}
          type="number"
          required
          divider={false}
          mode="edit"
        />
      );
    }

    return (
      <FastField
        name={name}
        component={InputField}
        label={label}
        placeholder={placeholder}
        type="number"
        divider={false}
        mode="edit"
      />
    );
  };

  const editMode = (
    <>
      {values?.groupCountMember?.visible && (
        <FastField
          component={SwitchField}
          name="hasGroup.value"
          label={t('GroupPresence')}
          labelPlacement="end"
          divider={false}
        />
      )}
      {values?.hasGroup?.value && values?.groupCountMember?.visible && (
        <>
          <StyledFields>{getGroupCountEditModeField()}</StyledFields>
          <Divider />
        </>
      )}
      {values?.groupCountMember?.visible && !values?.hasGroup?.value && <Divider />}
    </>
  );

  const viewMode =
    values?.hasGroup?.value && values?.groupCountMember?.visible ? (
      <>
        <StyledSubTitle>{t('DataGrid.treeDataGroupingHeaderName')}</StyledSubTitle>
        <StyledFields>
          <FastField
            name="groupCountMember.value"
            component={InputField}
            label={t('NumberOfPersons')}
            placeholder={t('EnterNumberOfPersons')}
            mode="view"
            type="number"
            divider={false}
          />
        </StyledFields>
        <Divider />
      </>
    ) : null;

  return mode === 'edit' ? editMode : viewMode;
};
