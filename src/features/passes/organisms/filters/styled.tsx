import styled from '@emotion/styled';

import { Wrapper, FilterToolbar } from '@ui/index';

interface StyledProps {
  className?: string;
  children: JSX.Element;
}

interface ToolbarProps {
  className?: string;
  closeFilter: () => boolean;
}

export const StyledWrapper = styled(({ className, children }: StyledProps) => (
  <Wrapper className={className}>{children}</Wrapper>
))`
  height: 100%;
`;

export const StyledFilterToolbar = styled(({ className, ...props }: ToolbarProps) => (
  <FilterToolbar className={className} {...props} />
))`
  padding: 24px;
`;
