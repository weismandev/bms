import { memo, FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik } from 'formik';

import { CustomScrollbar } from '@ui/index';

import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { StyledWrapper, StyledFilterToolbar } from './styled';
import { FiltersForm } from '../filters-form';

export const Filters: FC = memo(() => {
  const filters = useUnit($filters);

  const onClose = () => changedFilterVisibility(false);
  const onReset = () => filtersSubmitted('');

  return (
    <StyledWrapper>
      <CustomScrollbar autoHide>
        <StyledFilterToolbar closeFilter={onClose} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={onReset}
          enableReinitialize
          render={() => <FiltersForm />}
        />
      </CustomScrollbar>
    </StyledWrapper>
  );
});
