import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, fxGetList, $isLoading } from '../../models/property-select';
import { $building } from '../../models';
import { Building } from '../../interfaces';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  complexId?: number;
}

export const PropertySelect: FC<Props> = ({ complexId, ...props }) => {
  const [data, isLoading, building] = useUnit([$data, $isLoading, $building]);

  const buildingId = (building as Building)?.id;
  const complex = (building as Building)?.complex;

  useEffect(() => {
    if (complex?.id && buildingId) {
      fxGetList({ complex_id: complex.id, building_id: buildingId });
    }
  }, [buildingId, complex]);

  useEffect(() => {
    if (complexId && !buildingId) {
      fxGetList({ complex_id: complexId });
    }
  }, [complexId, buildingId]);

  const options = complexId || buildingId ? data : [];

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const PropertySelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PropertySelect />} onChange={onChange} {...props} />;
};
