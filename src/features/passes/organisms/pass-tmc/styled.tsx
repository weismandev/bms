import styled from '@emotion/styled';
import { Button } from '@mui/material';

export const StyledAddButton = styled(Button)`
  margin-top: 5px;
`;
