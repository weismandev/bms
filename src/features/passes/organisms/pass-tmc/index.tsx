import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Field, FieldArray } from 'formik';
import { nanoid } from 'nanoid';

import { InputField } from '@ui/index';

import { $mode, $optionList } from '../../models';
import { StyledField, StyledSubTitle } from '../pass-form/styled';
import { StyledAddButton } from './styled';
import { PassSectionWrapper, StuffRegistriesCard } from '../../molecules';
import { StuffActionTypesSelectField } from '../stuff-action-types-select';
import { StuffTypesSelectField } from '../stuff-types-select';
import { Pass, StuffRegistriesPass } from '../../interfaces';

interface Props {
  values: Pass;
}

export const PassTmc: FC<Props> = ({ values }) => {
  const { t } = useTranslation();
  const [mode, { sections, fields }] = useUnit([$mode, $optionList]);

  const subSection = {
    name: 'stuff_registry',
    title: t('ListOfGoodsAndMaterials'),
  };

  if (!values?.hasTmc?.value) {
    return null;
  }

  const newStaffRegistries = {
    _id: nanoid(3),
    name: {
      value: '',
      visible: fields?.stuff_title.visible || false,
      required: fields?.stuff_title.required || false,
    },
    amount: {
      value: '',
      visible: fields?.stuff_total_count.visible || false,
      required: fields?.stuff_total_count.required || false,
    },
  };

  return (
    <PassSectionWrapper name={t('Tmc')}>
      <>
        {values?.stuffActionType?.visible && (
          <StyledField>
            <Field
              name="stuffActionType.value"
              component={StuffActionTypesSelectField}
              label={t('InventoryRequestType')}
              placeholder={t('chooseType')}
              mode={mode}
              required={values?.stuffActionType?.required}
            />
          </StyledField>
        )}
        {values?.stuffType?.visible && (
          <StyledField>
            <Field
              name="stuffType.value"
              component={StuffTypesSelectField}
              label={t('TypeOfGoodsAndMaterials')}
              placeholder={t('SelectTheTypeOfGoodsAndMaterials')}
              isMulti
              mode={mode}
              required={values?.stuffType?.required}
            />
          </StyledField>
        )}
        {values?.stuffComment?.visible && (
          <StyledField>
            <Field
              name="stuffComment.value"
              component={InputField}
              label={t('Label.comment')}
              placeholder={t('EnterComment')}
              isMulti
              mode={mode}
              required={values?.stuffComment?.required}
            />
          </StyledField>
        )}
        {sections && sections[subSection.name] && (
          <>
            <StyledSubTitle>{subSection.title}</StyledSubTitle>
            <StyledField>
              <FieldArray
                name="stuffRegistries"
                render={({ push, remove }) => {
                  const stuffRegistriesCard = Array.isArray(values?.stuffRegistries)
                    ? values.stuffRegistries.map(
                        (stuff: StuffRegistriesPass, index: number) => {
                          const name = `stuffRegistries[${index}]`;

                          return (
                            <StuffRegistriesCard
                              key={stuff._id}
                              name={name}
                              index={index}
                              remove={remove}
                              stuff={stuff}
                            />
                          );
                        }
                      )
                    : null;

                  const handleClick = () => push(newStaffRegistries);

                  const addButton =
                    mode === 'edit' ? (
                      <StyledAddButton color="primary" onClick={handleClick}>
                        + {t('AddTmts')}
                      </StyledAddButton>
                    ) : null;

                  if (
                    !newStaffRegistries?.amount?.visible &&
                    !newStaffRegistries?.name?.visible
                  ) {
                    return null;
                  }

                  return (
                    <>
                      {stuffRegistriesCard}
                      {addButton}
                    </>
                  );
                }}
              />
            </StyledField>
          </>
        )}
      </>
    </PassSectionWrapper>
  );
};
