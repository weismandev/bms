import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $data, fxGetList, $isLoading } from '../../models/parking-zone-select';

type SelectFieldProps = {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
};

type Props = {
  activeFrom?: string | null;
  activeTo?: string | null;
  authorId?: number | null;
  buildingId?: number;
};

export const ParkingZoneSelect: FC<Props> = ({
  activeFrom,
  activeTo,
  buildingId,
  authorId,
  ...props
}) => {
  const [data, isLoading] = useUnit([$data, $isLoading]);

  useEffect(() => {
    if (activeFrom && activeTo && buildingId && !authorId) {
      fxGetList({
        active_from: activeFrom,
        active_to: activeTo,
        building_id: buildingId,
      });
    }

    if (activeFrom && activeTo && buildingId && authorId) {
      fxGetList({
        active_from: activeFrom,
        active_to: activeTo,
        building_id: buildingId,
        author_id: authorId,
      });
    }
  }, [activeFrom, activeTo, buildingId, authorId]);

  const options = activeFrom && activeTo && buildingId ? data : [];

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const ParkingZoneSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<ParkingZoneSelect />} onChange={onChange} {...props} />;
};
