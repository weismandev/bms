import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FormikProps, FieldArray, FieldProps, Field } from 'formik';
import { InputField, SomePhoneMaskedInput, EmailMaskedInput, Divider } from '@ui/index';
import { Pass, DocumentPass, GuestType } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { PassSectionWrapper, DocumentCard } from '../../molecules';
import { GuestTypeSelectField } from '../guest-type-select';
import { StyledFields, StyledSubTitle, StyledAdaptiveField } from '../pass-form/styled';

interface Props {
  values: Pass;
  isNew: boolean;
  setFieldValue: (name: string, value: any) => void;
}

export const PassGeneral: FC<Props> = ({ values, isNew, setFieldValue }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen] = useUnit([$mode, $isFilterOpen]);

  const subSection = {
    name: 'identity_document',
    title: t('Identification'),
  };

  const buildingId = isNew ? values?.building?.id : values?.buildingId;

  const atLeastOneFieldDocumentVisibility =
    Array.isArray(values?.documents) && values.documents.length > 0
      ? values.documents.some(
          (document) =>
            document?.birthday?.visible ||
            document?.issued_date?.visible ||
            document?.number?.visible ||
            document?.type?.visible
        )
      : false;

  const handleGuestType = (value: GuestType) => {
    setFieldValue('guestType.value', value);

    if (value?.is_predefined && value?.title) {
      setFieldValue('surname.value', value.title);
      setFieldValue('name.value', value.title);

      if (values?.patronymic?.visible && values?.patronymic?.required) {
        setFieldValue('patronymic.value', value.title);
      }
    }
  };

  const isShowForm =
    values?.surname?.visible ||
    values?.name?.visible ||
    values?.patronymic?.visible ||
    values?.phone?.visible ||
    values?.email?.visible ||
    values?.companyName?.visible ||
    atLeastOneFieldDocumentVisibility;

  if (!isShowForm) {
    return null;
  }

  return (
    <PassSectionWrapper name={t('BasicInformation')}>
      <>
        {values?.guestType?.visible && (
          <>
            <StyledAdaptiveField isFilterOpen={isFilterOpen}>
              <Field
                name="guestType.value"
                component={GuestTypeSelectField}
                label={t('GuestType')}
                placeholder={t('chooseType')}
                mode={mode}
                required={values?.guestType?.required}
                buildingId={buildingId}
                divider={false}
                onChange={handleGuestType}
              />
            </StyledAdaptiveField>
            <Divider />
          </>
        )}
        {(values?.surname?.visible ||
          values?.name?.visible ||
          values?.patronymic?.visible) && (
          <StyledFields isFilterOpen={isFilterOpen}>
            {values?.surname?.visible && (
              <Field
                name="surname.value"
                component={InputField}
                label={t('Label.lastname')}
                placeholder={t('EnterLastName')}
                required={values?.surname?.required}
                divider={false}
                mode={mode}
              />
            )}
            {values?.name?.visible && (
              <Field
                name="name.value"
                component={InputField}
                label={t('Label.name')}
                placeholder={t('EnterYourName')}
                mode={mode}
                required={values?.name?.required}
                divider={false}
              />
            )}
            {values?.patronymic?.visible && (
              <Field
                name="patronymic.value"
                component={InputField}
                label={t('Label.patronymic')}
                placeholder={t('EnterMiddleName')}
                mode={mode}
                required={values?.patronymic?.required}
                divider={false}
              />
            )}
          </StyledFields>
        )}
        {(values?.phone?.visible ||
          values?.email?.visible ||
          values?.companyName?.visible) && (
          <StyledFields isFilterOpen={isFilterOpen}>
            {values?.phone?.visible && (
              <Field
                name="phone.value"
                render={({ field, form }: FieldProps) => (
                  <InputField
                    field={field}
                    form={form}
                    inputComponent={SomePhoneMaskedInput}
                    label={t('Label.phone')}
                    placeholder={t('Label.phone')}
                    readOnly={mode === 'view'}
                    required={values?.phone?.required}
                    inputProps={{ form }}
                    divider={false}
                  />
                )}
              />
            )}
            {values?.email?.visible && (
              <Field
                name="email.value"
                render={({ field, form }: FieldProps) => (
                  <InputField
                    field={field}
                    form={form}
                    inputComponent={EmailMaskedInput}
                    label="Email"
                    placeholder={t('EnterEmail')}
                    mode={mode}
                    required={values?.email?.required}
                    divider={false}
                  />
                )}
              />
            )}
            {values?.companyName?.visible && (
              <Field
                name="companyName.value"
                component={InputField}
                label={t('PlaceOfWork')}
                placeholder={t('EnterCompanyName')}
                mode={mode}
                required={values?.companyName?.required}
                divider={false}
              />
            )}
          </StyledFields>
        )}
        {(values?.surname?.visible ||
          values?.name?.visible ||
          values?.patronymic?.visible ||
          values?.phone?.visible ||
          values?.email?.visible ||
          values?.companyName?.visible) && <Divider />}
        {atLeastOneFieldDocumentVisibility && (
          <>
            <StyledSubTitle>{subSection.title}</StyledSubTitle>
            <FieldArray
              name="documents"
              render={({ form }: { form: FormikProps<any> }) =>
                values.documents.map((document: DocumentPass, index: number) => {
                  const name = `documents[${index}]`;

                  return (
                    <DocumentCard
                      key={document._id}
                      name={name}
                      document={document}
                      setFieldValue={form.setFieldValue}
                    />
                  );
                })
              }
            />
          </>
        )}
      </>
    </PassSectionWrapper>
  );
};
