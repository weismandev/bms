import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { Arguments } from '../../interfaces';

const { t } = i18n;

function differenceTime(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  if (!pass) {
    return true;
  }

  const { workTimeFrom, workTimeTo } = pass;

  if (workTimeFrom?.value && workTimeTo?.value) {
    if (workTimeTo.value <= workTimeFrom.value) {
      return false;
    }

    return true;
  }

  return true;
}

export const getTimePassSchema = (name: string, text: string) => ({
  [name]: Yup.object().shape({
    reuqired: Yup.boolean(),
    visible: Yup.boolean(),
    value: Yup.string()
      .test('difference-date', text, differenceTime)
      .test('required', `${t('thisIsRequiredField')}`, (_, { from }) => {
        const val = from[0]?.value;
        const data = from[1]?.value;
        const passType = data?.passType?.value?.id;

        if (passType === 1) {
          return Boolean(val.value);
        }

        return true;
      }),
  }),
});

function differenceDate(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  if (!pass) {
    return true;
  }

  const { activeFrom, activeTo } = pass;

  const activeFromDate = new Date(activeFrom.value);
  const activeToDate = new Date(activeTo.value);

  if (activeFromDate && activeToDate) {
    const diffTime = Math.abs(Number(activeToDate) - Number(activeFromDate));
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    if (diffDays > 365) {
      return false;
    }
  }

  return true;
}

function startDateAfterFinish(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  if (!pass) {
    return true;
  }

  const { activeFrom, activeTo } = pass;

  const activeFromDate = new Date(activeFrom.value);
  const activeToDate = new Date(activeTo.value);

  if (activeFromDate && activeToDate) {
    if (Number(activeFromDate) > Number(activeToDate)) {
      return false;
    }
  }

  return true;
}

export const getDatePassSchema = (name: string, text: string) => ({
  [name]: Yup.object().shape({
    reuqired: Yup.boolean(),
    visible: Yup.boolean(),
    value: Yup.string()
      .test('difference-date', `${t('DateDifferenceExceeded')}`, differenceDate)
      .test('start-date-after-finish', text, startDateAfterFinish)
      .test('required', `${t('thisIsRequiredField')}`, (_, { from }) => {
        const val = from[0]?.value;
        const data = from[1]?.value;

        if (name === 'activeTo') {
          const passType = data?.passType?.value?.id;

          if (val.visible && val.required && passType !== 1) {
            return Boolean(val.value);
          }

          return true;
        }

        if (val.visible && val.required) {
          return Boolean(val.value);
        }

        return true;
      }),
  }),
});
