import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { Arguments, ArgumentsTmc } from '../../interfaces';

const { t } = i18n;

function requiredActionTypeStuff(this: Arguments) {
  const pass = this?.options?.from[2]?.value;

  const { hasTmc, stuffActionType } = pass;

  if (!hasTmc.value) {
    return true;
  }

  if (
    stuffActionType.visible &&
    stuffActionType.required &&
    (typeof stuffActionType.value !== 'object' || stuffActionType.value === null)
  ) {
    return false;
  }

  return true;
}

function requiredTypeStuff(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasTmc, stuffType } = pass;

  if (!hasTmc.value) {
    return true;
  }

  if (stuffType.visible && stuffType.required && stuffType.value.length === 0) {
    return false;
  }

  return true;
}

function requiredCommentStuff(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasTmc, stuffComment } = pass;

  if (!hasTmc.value) {
    return true;
  }

  if (stuffComment.visible && stuffComment.required && stuffComment.value.length === 0) {
    return false;
  }

  return true;
}

function requiredTmcStuffName(this: ArgumentsTmc) {
  const hasTmc = this?.options?.from[2]?.value?.hasTmc?.value;
  const name = this?.parent;

  if (!hasTmc) {
    return true;
  }

  if (name.visible && name.required && name.value.length === 0) {
    return false;
  }

  return true;
}

function requiredTmcStuffCount(this: ArgumentsTmc) {
  const hasTmc = this?.options?.from[2]?.value?.hasTmc?.value;
  const amount = this?.parent;

  if (!hasTmc) {
    return true;
  }

  if (amount.visible && amount.required && amount.value.length === 0) {
    return false;
  }

  return true;
}

export const getTmcRequiredSchema = () => ({
  stuffActionType: Yup.object().shape({
    value: Yup.object()
      .nullable()
      .test('action-type-required', t('thisIsRequiredField'), requiredActionTypeStuff),
  }),
  stuffType: Yup.object().shape({
    value: Yup.array()
      .nullable()
      .test('type-required', t('thisIsRequiredField'), requiredTypeStuff),
  }),
  stuffComment: Yup.object().shape({
    value: Yup.string().test(
      'comment-required',
      t('thisIsRequiredField'),
      requiredCommentStuff
    ),
  }),
  stuffRegistries: Yup.array().of(
    Yup.object().shape({
      name: Yup.object().shape({
        value: Yup.string().test(
          'tmc-name-required',
          t('thisIsRequiredField'),
          requiredTmcStuffName
        ),
      }),
      amount: Yup.object().shape({
        value: Yup.string().test(
          'tmc-count-required',
          t('thisIsRequiredField'),
          requiredTmcStuffCount
        ),
      }),
    })
  ),
});
