import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { Arguments } from '../../interfaces';

const { t } = i18n;

function requiredBrandVehicle(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasVehicle, brand } = pass;

  if (!hasVehicle.value) {
    return true;
  }

  if (brand.visible && brand.required && !brand.value) {
    return false;
  }

  return true;
}

function requiredGovNumberVehicle(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasVehicle, govNumver } = pass;

  if (!hasVehicle.value) {
    return true;
  }

  if (govNumver.visible && govNumver.required && !govNumver.value) {
    return false;
  }

  return true;
}

function requiredVehicleCategory(this: Arguments) {
  const { parent, from } = this;
  const {
    value: { hasVehicle },
  } = from[1];

  if (!hasVehicle.value) {
    return true;
  }

  if (parent.visible && parent.required) {
    return Boolean(parent.value);
  }

  return true;
}

function requiredParkingZone(this: Arguments) {
  const { parent, from } = this;
  const {
    value: { hasVehicle },
  } = from[1];

  if (!hasVehicle.value) {
    return true;
  }

  if (parent.visible && parent.required) {
    return Boolean(parent.value);
  }

  return true;
}

function requiredParkingPlace(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasVehicle, parkingZone, parkingPlace } = pass;

  if (!hasVehicle.value) {
    return true;
  }

  if (
    parkingZone?.visible &&
    parkingZone?.value &&
    parkingZone.value?.selectMode !== 'full_zone'
  ) {
    return Array.isArray(parkingPlace.value) && parkingPlace.value?.length;
  }

  return true;
}

export const getVehicleRequiredSchema = () => ({
  brand: Yup.object().shape({
    value: Yup.string().test(
      'required-brand',
      t('thisIsRequiredField'),
      requiredBrandVehicle
    ),
  }),
  govNumver: Yup.object().shape({
    value: Yup.string().test(
      'gov-number-required',
      t('thisIsRequiredField'),
      requiredGovNumberVehicle
    ),
  }),
  vehicleCategory: Yup.object().shape({
    value: Yup.mixed()
      .nullable()
      .test(
        'vehicle-сategory-required',
        t('thisIsRequiredField'),
        requiredVehicleCategory
      ),
  }),
  parkingZone: Yup.object().shape({
    value: Yup.mixed()
      .nullable()
      .test('required-parking-zone', t('thisIsRequiredField'), requiredParkingZone),
  }),
  parkingPlace: Yup.object().shape({
    value: Yup.mixed().test(
      'required-parking-place',
      `${t('thisIsRequiredField')}`,
      requiredParkingPlace
    ),
  }),
});
