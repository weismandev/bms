import { Arguments } from '../../interfaces';

export function requiredGroupMember(this: Arguments) {
  const pass = this?.options?.from[1]?.value;

  const { hasGroup, groupCountMember } = pass;

  if (!hasGroup.value) {
    return true;
  }

  if (
    groupCountMember.visible &&
    groupCountMember.required &&
    groupCountMember.value.toString().length === 0
  ) {
    return false;
  }

  return true;
}
