import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const getStringRequiredSchema = (name: string) => ({
  [name]: Yup.object().shape({
    value: Yup.string().test(
      `${name}-required`,
      `${t('thisIsRequiredField')}`,
      (_, { parent }) => {
        const { value, visible, required } = parent;

        if (visible && required) {
          return Boolean(value);
        }

        return true;
      }
    ),
  }),
});

export const getPhoneRequiredSchema = (name: string) => ({
  [name]: Yup.object().shape({
    value: Yup.string().test(
      `${name}-required`,
      `${t('thisIsRequiredField')}`,
      (_, { parent }) => {
        const { value, visible, required } = parent;

        if (visible && required) {
          if (!value) {
            return false;
          }

          return Boolean(value.slice(1).length);
        }

        return true;
      }
    ),
  }),
});

export const getObjectRequiredSchema = (name: string) => ({
  [name]: Yup.object().shape({
    value: Yup.mixed()
      .nullable()
      .test(`${name}-required`, `${t('thisIsRequiredField')}`, (_, { parent }) => {
        const { value, visible, required } = parent;

        if (visible && required) {
          return Boolean(value);
        }

        return true;
      }),
  }),
});

export const getArrayRequiredSchema = (name: string) => ({
  [name]: Yup.object().shape({
    value: Yup.array().test(
      `${name}-required`,
      `${t('thisIsRequiredField')}`,
      (_, { parent }) => {
        const { value, visible, required } = parent;

        if (visible && required) {
          return value.length;
        }

        return true;
      }
    ),
  }),
});
