import { createEffect, createStore } from 'effector';

import { DocumentTypesResponse, DocumentType } from '../../interfaces';

export const fxGetList = createEffect<void, DocumentTypesResponse, Error>();

export const $data = createStore<DocumentType[]>([]);
export const $isLoading = createStore<boolean>(false);
