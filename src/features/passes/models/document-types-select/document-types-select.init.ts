import { signout } from '@features/common';

import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './document-types-select.model';

fxGetList.use(passApi.getDocumentTypes);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.identityDocumentTypeList)) {
      return [];
    }

    return result.identityDocumentTypeList.map((item) => ({
      id: item.id,
      title: item?.title || '',
      slug: item.slug || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
