import { createEffect, createStore } from 'effector';

import { GuestTypePayload, GuestTypeResponse, GuestType } from '../../interfaces';

export const fxGetList = createEffect<GuestTypePayload, GuestTypeResponse, Error>();

export const $data = createStore<GuestType[]>([]);
export const $isLoading = createStore<boolean>(false);
