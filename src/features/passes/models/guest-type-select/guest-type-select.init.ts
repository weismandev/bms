import { signout } from '@features/common';
import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './guest-type-select.model';

fxGetList.use(passApi.getGuestType);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.guestTypeList)) {
      return [];
    }

    return result.guestTypeList;
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
