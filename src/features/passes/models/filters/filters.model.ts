import { createFilterBag } from '@tools/factories';

const defaultFilters = {
  isArchive: false,
  enterprise: '',
  complexes: [],
  status: '',
  passTypes: [],
  vehicle: [],
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
