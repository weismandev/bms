import { signout } from '@features/common';

import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './property-select.model';
import { addClicked } from '../detail/detail.model';

fxGetList.use(passApi.getPropertyList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.propertyList)) {
      return [];
    }

    return result.propertyList.map((item) => ({
      id: item.id,
      title: item?.title || '',
    }));
  })
  .reset([signout, addClicked]);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
