import { createEffect, createStore } from 'effector';

import { PropertyListResponse, Property, PropertyListPayload } from '../../interfaces';

export const fxGetList = createEffect<PropertyListPayload, PropertyListResponse, Error>();

export const $data = createStore<Property[]>([]);
export const $isLoading = createStore<boolean>(false);
