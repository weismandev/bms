import { createEffect, createStore, combine, createEvent } from 'effector';
import { nanoid } from 'nanoid';
import { $pathname } from '@features/common';
import { $data as $passTypes } from '@features/pass-types-select';
import { createDetailBag } from '@tools/factories';
import {
  GetPassPayload,
  GetPassResponse,
  SectionListResponse,
  OptionList,
  Opened,
  CreatePassPayload,
  SectionListPayload,
  PassFuncPayload,
  PassCopyResponse,
  Value,
  OptionListFields,
  Building,
} from '../../interfaces';
import { $data as $documentTypes } from '../document-types-select/document-types-select.model';
import { $data as $parkingZones } from '../parking-zone-select/parking-zone-select.model';

export const passEntity = {
  id: null,
  building: '',
  surname: '',
  name: '',
  patronymic: '',
  phone: '',
  email: '',
  companyName: '',
  guestType: '',
  documents: [
    {
      _id: nanoid(3),
      birthday: '',
      type: '',
      number: '',
      issued_date: '',
    },
  ],
  enterprise: '',
  complex: '',
  properties: [],
  passType: '',
  author: '',
  passCreatedAt: '',
  comment: '',
  passStatus: '',
  activeFrom: '',
  activeTo: '',
  workTimeFrom: '',
  workTimeTo: '',
  brand: '',
  govNumver: '',
  groupCountMember: '',
  hasGroup: false,
  hasVehicle: false,
  hasTmc: false,
  vehicleCategory: '',
  parkingZone: '',
  parkingPlace: [],
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(passEntity);

export const revokePass = createEvent<void>();
export const copyPass = createEvent<void>();
export const dublicatePass = createEvent<void>();
export const activatePass = createEvent<void>();
export const changeBuilding = createEvent<Value>();
export const sendSms = createEvent<void>();
export const changeOnlyConfiguratorFields = createEvent<boolean>();

export const fxGetPass = createEffect<GetPassPayload, GetPassResponse, Error>();
export const fxGetPassOptions = createEffect<
  SectionListPayload,
  SectionListResponse,
  Error
>();
export const fxCreatePass = createEffect<CreatePassPayload, GetPassResponse, Error>();
export const fxRevokePass = createEffect<PassFuncPayload, GetPassResponse, Error>();
export const fxCopyPass = createEffect<PassFuncPayload, PassCopyResponse, Error>();
export const fxActivatePass = createEffect<PassFuncPayload, GetPassResponse, Error>();
export const fxSendSms = createEffect<PassFuncPayload, object, Error>();

export const $onlyConfiguratorFields = createStore<boolean>(false);
export const $building = createStore<string | Building>('');
export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname: string) => pathname.split('/')[2]);
export const $optionList = createStore<OptionList>({});

const getVisibleFields = (
  fields: OptionListFields | undefined,
  id: number | null | undefined,
  visibleField: boolean | undefined
) => {
  if (!fields && !id && !visibleField) {
    return false;
  }

  if (!fields && id) {
    return true;
  }

  return visibleField;
};

export const $pass = combine(
  $opened,
  $building,
  $optionList,
  $documentTypes,
  $passTypes,
  $parkingZones,
  (opened: Opened, building, optionList, documentTypes, passTypes, parkingZones) => {
    const passTypeValue = Array.isArray(passTypes)
      ? passTypes.length === 1
        ? passTypes[0]
        : passTypes.find((type) => type?.id === 2)
      : '';

    return {
      id: opened.id,
      buildingId: opened.buildingId,
      building,
      surname: {
        value: opened.surname,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_surname?.visible
        ),
        required: optionList.fields?.user_surname?.required || false,
      },
      name: {
        value: opened.name,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_name?.visible
        ),
        required: optionList.fields?.user_name?.required || false,
      },
      patronymic: {
        value: opened.patronymic,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_patronymic?.visible
        ),
        required: optionList.fields?.user_patronymic?.required || false,
      },
      phone: {
        value: opened.phone,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_phone?.visible
        ),
        required: optionList.fields?.user_phone?.required || false,
      },
      email: {
        value: opened.email,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_email?.visible
        ),
        required: optionList.fields?.user_email?.required || false,
      },
      companyName: {
        value: opened.companyName,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.user_workplace?.visible
        ),
        required: optionList.fields?.user_workplace?.required || false,
      },
      guestType: {
        value: opened.guestType,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.guest_type?.visible
        ),
        required: optionList.fields?.guest_type?.required || false,
      },
      documents: opened.documents.map((document) => ({
        _id: document._id,
        birthday: {
          value: document.birthday,
          visible: getVisibleFields(
            optionList.fields,
            opened?.id,
            optionList.fields?.user_birthday?.visible
          ),
          required: optionList.fields?.user_birthday?.required || false,
        },
        type: {
          value:
            Array.isArray(documentTypes) && documentTypes.length > 0
              ? documentTypes.find((type) => type.slug === document.type)
              : document.type,
          visible: getVisibleFields(
            optionList.fields,
            opened?.id,
            optionList.fields?.user_passport?.visible
          ),
          required: optionList.fields?.user_passport?.required || false,
        },
        number: {
          value: document.number,
          visible: getVisibleFields(
            optionList.fields,
            opened?.id,
            optionList.fields?.user_passport_data?.visible
          ),
          required: optionList.fields?.user_passport_data?.required || false,
        },
        issued_date: {
          value: document.issued_date,
          visible: getVisibleFields(
            optionList.fields,
            opened?.id,
            optionList.fields?.user_passport_issued?.visible
          ),
          required: optionList.fields?.user_passport_issued?.required || false,
        },
      })),
      enterprise: {
        value: opened.enterprise,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.company?.visible
        ),
        required: optionList.fields?.company?.required || false,
      },
      complex: {
        value: opened.complex,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.target_object?.visible
        ),
        required: optionList.fields?.target_object?.required || false,
      },
      properties: {
        value: opened.properties,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.target_property?.visible
        ),
        required: optionList.fields?.target_property?.required || false,
      },
      passType: {
        value: opened?.id || opened?.passType ? opened.passType : passTypeValue,
        visible: true,
        required: optionList.fields?.strategy?.required || false,
        disabled: Array.isArray(passTypes)
          ? passTypes.length === 1
            ? true
            : false
          : false,
      },
      passNumber: {
        value: opened.passNumber,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.number?.visible
        ),
        required: optionList.fields?.number?.required || false,
      },
      hasGroup: {
        value: opened.hasGroup,
        visible: optionList.fields?.has_group?.visible || false,
        required: optionList.fields?.has_group?.required || false,
      },
      hasVehicle: {
        value: opened.hasVehicle,
        visible: optionList.fields?.has_vehicle?.visible || false,
        required: optionList.fields?.has_vehicle?.required || false,
      },
      hasTmc: {
        value: opened.hasTmc,
        // visible: optionList.fields.has_stuff.visible,
        visible: false, // тмц скрыто
        required: optionList.fields?.has_stuff?.required || false,
      },
      author: {
        value: opened.author,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.author?.visible
        ),
        required: optionList.fields?.author?.required || false,
      },
      passCreatedAt: {
        value: opened.passCreatedAt,
        visible: true,
        required: false,
      },
      comment: {
        value: opened.comment,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.comment?.visible
        ),
        required: optionList.fields?.comment?.required || false,
      },
      passStatus: {
        value: opened.passStatus,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.status?.visible
        ),
        required: optionList.fields?.status?.required || false,
      },
      activeFrom: {
        value: opened.activeFrom,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.active_from?.visible
        ),
        required: optionList.fields?.active_from?.required || false,
      },
      activeTo: {
        value: opened.activeTo,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.active_to?.visible
        ),
        required: optionList.fields?.active_to?.required || false,
      },
      workTimeFrom: {
        value: opened.workTimeFrom,
        visible: true,
        required: false,
      },
      workTimeTo: {
        value: opened.workTimeTo,
        visible: true,
        required: false,
      },
      brand: {
        value: opened.brand,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.vehicle_brand?.visible
        ),
        required: optionList.fields?.vehicle_brand?.required || false,
      },
      vehicleCategory: {
        value: opened.vehicleCategory,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.vehicle_category?.visible
        ),
        required: optionList.fields?.vehicle_category?.required || false,
      },
      govNumver: {
        value: opened.govNumver,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.vehicle_licence_plate?.visible
        ),
        required: optionList.fields?.vehicle_licence_plate?.required || false,
      },
      groupCountMember: {
        value: opened.groupCountMember,
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.group_members_number?.visible
        ),
        required: optionList.fields?.group_members_number?.required || false,
      },
      parkingZone: {
        value: parkingZones.find((item) => item.id === opened.parkingZone) ?? '',
        visible: getVisibleFields(
          optionList.fields,
          opened?.id,
          optionList.fields?.parking_place?.visible
        ),
        required: optionList.fields?.parking_place?.required || false,
      },
      parkingPlace: {
        value: opened.parkingPlace,
        visible: false,
        required: false,
      },
      // stuffActionType: {
      //   value: opened.stuffActionType,
      //   visible: optionList.fields?.stuff_pass_type?.visible || false,
      //   required: optionList.fields?.stuff_pass_type?.required || false,
      // },
      // stuffType: {
      //   value: opened.stuffType,
      //   visible: optionList.fields?.stuff_type?.visible || false,
      //   required: optionList.fields?.stuff_type?.required || false,
      // },
      // stuffComment: {
      //   value: opened.stuffComment,
      //   visible: optionList.fields?.stuff_comment?.visible || false,
      //   required: optionList.fields?.stuff_comment?.required || false,
      // },
      // stuffRegistries: opened.stuffRegistries.map((item) => ({
      //   _id: item._id,
      //   name: {
      //     value: item.name,
      //     visible: optionList.fields?.stuff_title?.visible || false,
      //     required: optionList.fields?.stuff_title?.required || false,
      //   },
      //   amount: {
      //     value: item.amount,
      //     visible: optionList.fields?.stuff_total_count?.visible || false,
      //     required: optionList.fields?.stuff_total_count?.required || false,
      //   },
      // })),
    };
  }
);
