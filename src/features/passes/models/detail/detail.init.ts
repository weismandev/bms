import { sample } from 'effector';
import { delay } from 'patronum';
import { format } from 'date-fns';
import { nanoid } from 'nanoid';
import { CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout, $pathname, historyPush } from '@features/common';
import { $data as $buildingList } from '@features/house-select';
import i18n from '@shared/config/i18n';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import {
  SectionList,
  CreatePassPayload,
  Pass,
  DocumentsPayload,
  VehiclePayload,
  Opened,
  Value,
} from '../../interfaces';
import { pageMounted, pageUnmounted } from '../page/page.model';
import { statusColor } from '../table/table.model';
import {
  $entityId,
  fxGetPass,
  $opened,
  passEntity,
  $isDetailOpen,
  openViaUrl,
  $path,
  fxGetPassOptions,
  addClicked,
  $optionList,
  detailClosed,
  entityApi,
  fxCreatePass,
  $mode,
  changedDetailVisibility,
  revokePass,
  fxRevokePass,
  copyPass,
  fxCopyPass,
  dublicatePass,
  activatePass,
  fxActivatePass,
  $building,
  changeBuilding,
  $pass,
  sendSms,
  fxSendSms,
  $onlyConfiguratorFields,
  changeOnlyConfiguratorFields,
} from './detail.model';

const { t } = i18n;

/* при дублировании пропуска запрашивается список опций,
  в котором будет информация отображаемых/обязательных полей */
sample({
  clock: dublicatePass,
  source: $pass,
  fn: ({ buildingId }) => ({ building_id: buildingId, mode: 'edit' }),
  target: fxGetPassOptions,
});

// при клике в таблице на строку отправляется запрос на получение данных о пропуске
sample({
  clock: openViaUrl,
  fn: (id: number) => ({ id, mode: 'detail', with_pass_config: 0 }),
  target: fxGetPass,
});

// при маунте проверяем есть ли в url id пропуска. Если есть, то берем его и запрашиваем данные
sample({
  clock: pageMounted,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }: { pathname: string }) => IsExistIdInUrl(pathname),
  fn: ({ entityId }: { entityId: string }) => ({
    id: Number.parseInt(entityId, 10),
    mode: 'detail',
    with_pass_config: 0,
  }),
  target: fxGetPass,
});

// детали открываются только, если выполнился запрос getById или нажат + в тулбаре таблицы
$isDetailOpen
  .off(openViaUrl)
  .on(fxGetPass.done, () => true)
  .on(detailClosed, () => false)
  .reset([pageUnmounted, signout]);

// если запрос getById упал, то произойдет редирект в реестр
sample({
  clock: delay({ source: fxGetPass.fail, timeout: 1000 }),
  source: $path,
  fn: (path: string) => `../${path}`,
  target: historyPush,
});

// при закрытии деталий из url убирается id
sample({
  clock: changedDetailVisibility,
  source: $path,
  filter: (_, visibility) => !visibility,
  fn: (path) => `../${path}`,
  target: historyPush,
});

$opened
  .on(fxGetPass.done, (_, { result: { pass } }) => {
    if (!pass) {
      return passEntity;
    }

    return {
      id: pass?.id,
      surname: pass?.user_surname || '',
      name: pass?.user_name || '',
      patronymic: pass?.user_patronymic || '',
      phone: pass?.user_phone ?? '',
      email: pass?.user_email || '',
      companyName: pass?.user_company_name || '',
      guestType: pass?.guest_type || '',
      documents:
        Array.isArray(pass?.identity_documents) && pass.identity_documents.length > 0
          ? pass.identity_documents.map((document) => ({
              _id: nanoid(3),
              birthday: document?.birthday || '',
              type: document?.type || '',
              number: document?.number || '',
              issued_date: document?.issued_date || '',
            }))
          : [],
      enterprise: pass?.enterprise || '',
      complex: pass?.complex || '',
      properties: pass?.properties || [],
      passType: pass?.strategy || '',
      passNumber: pass?.pass_number || '',
      author:
        typeof pass?.author === 'object'
          ? { id: pass.author.id, title: pass.author?.fullname || '' }
          : '',
      passCreatedAt: pass?.created_at?.date ? pass.created_at.date.substring(0, 10) : '',
      comment: pass?.comment || '',
      passStatus:
        pass?.state?.slug && pass?.state?.title
          ? {
              slug: pass?.state?.slug || '',
              title: pass?.state?.title || '',
              style: statusColor[pass.state.slug] || {},
            }
          : null,
      activeFrom: pass?.active_from?.date
        ? format(new Date(pass.active_from.date), 'yyyy-MM-dd')
        : '',
      activeTo: pass?.active_to?.date
        ? format(new Date(pass.active_to.date), 'yyyy-MM-dd')
        : '',
      workTimeFrom: pass?.work_time_from || '',
      workTimeTo: pass?.work_time_to || '',
      vehicleCategory:
        Array.isArray(pass?.vehicles) && pass.vehicles[0]?.category
          ? pass.vehicles[0].category
          : '',
      brand:
        Array.isArray(pass?.vehicles) && pass.vehicles[0]?.brand
          ? pass.vehicles[0].brand
          : '',
      govNumver:
        Array.isArray(pass?.vehicles) && pass.vehicles[0]?.id_number
          ? pass.vehicles[0].id_number
          : '',
      groupCountMember: pass?.group_members_number || '',
      stuffActionType: pass?.stuff_data?.action_type || '',
      stuffType: pass?.stuff_data?.stuff_types || [],
      stuffComment: pass?.stuff_data?.stuff_comment || '',
      stuffRegistries: Array.isArray(pass?.stuff_data?.stuff_registries)
        ? pass.stuff_data.stuff_registries.map((item) => ({
            _id: nanoid(3),
            name: item.name || '',
            amount: item.amount || '',
          }))
        : [],
      hasGroup: pass?.has_group || false,
      hasVehicle: pass?.has_vehicle || false,
      hasTmc: pass?.has_stuff || false,
      buildingId: pass?.building_id,
      parkingZone:
        pass?.parking_place?.parking_zone_id ?? pass?.parking_zone_group?.id ?? '',
      parkingPlace: Array.isArray(pass?.parking_places)
        ? pass.parking_places.map((place) => ({
            id: place?.id,
            title: place?.number ?? '',
          }))
        : [],
    };
  })
  .on(dublicatePass, (state) => {
    const newState: Opened = { ...state };

    newState.id = null;

    return newState;
  })
  .reset([pageUnmounted, signout, fxGetPass, changeBuilding]);

// после изменения здания запрашивается список опций
sample({
  clock: changeBuilding,
  source: $building,
  filter: (building) => Boolean(building),
  fn: (building) => ({ building_id: (building as Value).id, mode: 'edit' }),
  target: fxGetPassOptions,
});

$optionList
  .on(fxGetPassOptions.done, (_, { result }) => {
    if (!Array.isArray(result?.sectionList)) {
      return {
        sections: {},
      };
    }

    const sections = result.sectionList.reduce((acc, section) => {
      const subSectionList = section.sectionList.reduce(
        (subSectionacc: { [key: string]: boolean }, e: SectionList) => ({
          ...subSectionacc,
          ...{ [e.name]: e.visible },
        }),
        {}
      );

      return { ...acc, ...{ [section.name]: section.visible, ...subSectionList } };
    }, {});

    const fields = result.sectionList.map((item) => {
      const propertyList = item.propertyList.map((element) => ({
        name: element.name,
        required: element.required,
        visible: element.visible,
      }));

      const sectionPropertyList = item.sectionList.map((section: SectionList) =>
        section.propertyList.map((i) => ({
          name: i.name,
          required: i.required,
          visible: i.visible,
        }))
      );

      return [...propertyList, ...sectionPropertyList.flat()];
    });

    return {
      sections,
      fields: fields.flat().reduce(
        (
          acc: { [key: string]: { visible: boolean; required: boolean } },
          field: { name: string; required: boolean; visible: boolean }
        ) => ({
          ...acc,
          ...{ [field.name]: { required: field.required, visible: field.visible } },
        }),
        {}
      ),
    };
  })
  .reset([pageUnmounted, signout, fxGetPass, addClicked, changeBuilding]);

sample({
  clock: entityApi.create,
  fn: (pass: Pass) => {
    const payload: CreatePassPayload = {};

    if (pass?.building?.id) {
      payload.building_id = pass.building.id;
    }

    if (pass?.name?.visible && pass?.name?.value) {
      payload.user_name = pass.name.value;
    }

    if (pass?.surname?.visible && pass?.surname?.value) {
      payload.user_surname = pass.surname.value;
    }

    if (pass?.patronymic?.visible && pass?.patronymic?.value) {
      payload.user_patronymic = pass.patronymic.value;
    }

    if (pass?.phone?.visible && pass?.phone?.value) {
      payload.user_phone = pass.phone.value;
    }

    if (pass?.email?.visible && pass?.email?.value) {
      payload.user_email = pass.email.value;
    }

    if (pass?.companyName?.visible && pass?.companyName?.value) {
      payload.user_company_name = pass.companyName.value;
    }

    if (
      pass?.guestType?.visible &&
      typeof pass?.guestType?.value === 'object' &&
      pass?.guestType?.value?.id
    ) {
      payload.guest_type_id = pass.guestType.value.id;
    }

    if (Array.isArray(pass?.documents) && pass.documents.length > 0) {
      const formattedDocuments = pass.documents.map((document) => {
        const documentPayload: DocumentsPayload = {};

        if (
          document?.type?.visible &&
          typeof document?.type?.value === 'object' &&
          document?.type?.value?.slug
        ) {
          documentPayload.type = document.type.value.slug;
        }

        if (document?.birthday?.visible && document?.birthday?.value) {
          documentPayload.birthday = document.birthday.value;
        }

        if (document?.number?.visible && document?.number?.value) {
          documentPayload.number = document.number.value;
        }

        if (document?.issued_date?.visible && document?.issued_date?.value) {
          documentPayload.issued_date = document.issued_date.value;
        }

        return documentPayload;
      });

      if (formattedDocuments.length > 0) {
        payload.identity_documents = formattedDocuments;
      }
    }

    if (
      pass?.enterprise?.visible &&
      typeof pass?.enterprise?.value === 'object' &&
      pass?.enterprise?.value?.id
    ) {
      payload.enterprise_id = pass.enterprise.value.id;
    }

    if (
      pass?.complex?.visible &&
      typeof pass?.complex?.value === 'object' &&
      pass?.complex?.value?.id
    ) {
      payload.complex_id = pass.complex.value.id.toString();
    }

    if (
      pass?.properties?.visible &&
      Array.isArray(pass?.properties?.value) &&
      pass.properties.value.length > 0
    ) {
      payload.property_ids = pass.properties.value.map((property) =>
        property.id.toString()
      );
    }

    if (
      pass?.passType?.visible &&
      typeof pass?.passType?.value === 'object' &&
      pass?.passType?.value?.id
    ) {
      payload.strategy_id = pass.passType.value.id;
    }

    if (pass?.hasGroup?.visible) {
      payload.has_group = Number(pass.hasGroup.value);
    }

    if (pass?.hasVehicle?.visible) {
      payload.has_vehicle = Number(pass.hasVehicle.value);
    }

    if (pass?.hasTmc?.visible) {
      payload.has_stuff = Number(pass.hasTmc.value);
    }

    if (
      pass?.author?.visible &&
      typeof pass?.author?.value === 'object' &&
      pass?.author?.value?.id
    ) {
      payload.author_id = pass.author.value.id;
    }

    if (pass?.comment?.visible && pass?.comment?.value) {
      payload.comment = pass.comment.value;
    }

    if (pass?.activeFrom?.visible && pass?.activeFrom?.value) {
      payload.active_from = pass.activeFrom.value;
    }

    if (pass?.activeTo?.visible && pass?.activeTo?.value) {
      payload.active_to = pass.activeTo.value;
    }

    if (pass?.workTimeFrom?.visible && pass?.workTimeFrom?.value) {
      payload.work_time_from = pass.workTimeFrom.value;
    }

    if (pass?.workTimeTo?.visible && pass?.workTimeTo?.value) {
      payload.work_time_to = pass.workTimeTo.value;
    }

    const vehiclePayload: VehiclePayload = {};

    if (pass?.hasVehicle?.value && pass?.brand?.visible && pass?.brand?.value) {
      vehiclePayload.brand = pass.brand.value;
    }

    if (pass?.hasVehicle?.value && pass?.govNumver?.visible && pass?.govNumver?.value) {
      vehiclePayload.id_number = pass.govNumver.value;
    }

    if (
      pass?.hasVehicle?.value &&
      pass?.vehicleCategory?.visible &&
      typeof pass?.vehicleCategory?.value === 'object' &&
      pass?.vehicleCategory?.value?.id
    ) {
      vehiclePayload.category_id = pass.vehicleCategory.value.id;
    }

    if (Object.keys(vehiclePayload).length > 0) {
      payload.vehicles = [vehiclePayload];
    }

    if (
      pass?.hasGroup?.value &&
      pass?.groupCountMember?.visible &&
      pass?.groupCountMember?.value
    ) {
      payload.group_members_number = Number.parseInt(pass.groupCountMember.value, 10);
    }

    if (
      pass?.hasVehicle?.value &&
      pass?.parkingZone?.visible &&
      typeof pass?.parkingZone?.value === 'object' &&
      pass?.parkingZone?.value?.id
    ) {
      payload.parking_zone_id = pass.parkingZone.value.id;
    }

    if (
      pass?.hasVehicle?.value &&
      pass?.parkingZone?.visible &&
      pass?.parkingZone?.value &&
      pass?.parkingZone?.value?.selectMode !== 'full_zone'
    ) {
      payload.parking_place_ids = pass.parkingPlace.value.map((item) => item.id);
    }

    return payload;
  },
  target: fxCreatePass,
});

$mode.on(dublicatePass, () => 'edit').reset([pageUnmounted, signout, fxCreatePass.done]);

// после создания пропуска отправляется запрос на получение данных пропуска
sample({
  clock: fxCreatePass.done,
  fn: ({
    result: {
      pass: { id },
    },
  }) => ({ id, mode: 'detail', with_pass_config: 0 }),
  target: fxGetPass,
});

// после получения пропуска запрашивается список видимых и обязательных полей
sample({
  clock: fxGetPass.done,
  filter: ({ params: { with_pass_config } }) => Boolean(with_pass_config),
  fn: ({
    result: {
      pass: { building_id },
    },
  }) => ({ building_id, mode: 'detail' }),
  target: fxGetPassOptions,
});

// после создания пропуска добавляется в url id пропуска
sample({
  clock: fxCreatePass.done,
  source: $path,
  filter: (
    _,
    {
      result: {
        pass: { id },
      },
    }
  ) => Boolean(id),
  fn: (
    path,
    {
      result: {
        pass: { id },
      },
    }
  ) => `${path}/${id}`,
  target: historyPush,
});

// отправка запроса на отзыв пропуска
sample({
  clock: revokePass,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxRevokePass,
});

// после отзыва пропуска обновление данных
sample({
  clock: fxRevokePass.done,
  source: $onlyConfiguratorFields,
  fn: (
    onlyConfiguratorFields,
    {
      result: {
        pass: { id },
      },
    }
  ) => ({
    id,
    mode: 'detail',
    with_pass_config: Number(onlyConfiguratorFields),
  }),
  target: fxGetPass,
});

// отправка запроса копирования пропуска
sample({
  clock: copyPass,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxCopyPass,
});

// при дублировании пропуска убирается id из url
sample({
  clock: dublicatePass,
  source: $path,
  fn: (path) => `../${path}`,
  target: historyPush,
});

// отправка запроса на активацию пропуска
sample({
  clock: activatePass,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxActivatePass,
});

// обновление данных после активации пропуска
sample({
  clock: fxActivatePass.done,
  source: $onlyConfiguratorFields,
  fn: (
    onlyConfiguratorFields,
    {
      result: {
        pass: { id },
      },
    }
  ) => ({
    id,
    mode: 'detail',
    with_pass_config: Number(onlyConfiguratorFields),
  }),
  target: fxGetPass,
});

$building
  .on(changeBuilding, (_, building) => building)
  .reset([pageUnmounted, signout, addClicked]);

/** При дублировании обновляется стор building */
sample({
  clock: dublicatePass,
  source: { pass: $pass, buildingList: $buildingList },
  fn: ({ pass: { buildingId }, buildingList }) =>
    buildingList.find((item) => item.id === buildingId),
  target: $building,
});

// отправка смс
sample({
  clock: sendSms,
  source: $opened,
  fn: ({ id }) => ({ id }),
  target: fxSendSms,
});

// отображение сообщения об отправке смс
sample({
  clock: fxSendSms.done,
  fn: () => ({
    isOpen: true,
    color: '#1BB169',
    text: t('SMSScent'),
    Icon: CheckCircle,
  }),
  target: changeNotification,
});

$onlyConfiguratorFields
  .on(changeOnlyConfiguratorFields, (_, value) => value)
  .on(fxGetPass.done, (_, { params: { with_pass_config } }) => Boolean(with_pass_config))
  .reset([pageUnmounted, signout, fxGetPass]);

/* отправка запроса на получение пропуска с учетом параметра with_pass_config
при изменении свича в карточке пропуска */
sample({
  clock: changeOnlyConfiguratorFields,
  source: { opened: $opened, onlyConfiguratorFields: $onlyConfiguratorFields },
  fn: ({ opened: { id }, onlyConfiguratorFields }) => ({
    id,
    mode: 'detail',
    with_pass_config: Number(onlyConfiguratorFields),
  }),
  target: fxGetPass,
});
