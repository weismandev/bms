import { signout } from '@features/common';
import { fxCopyPass } from '../detail/detail.model';
import { pageUnmounted } from '../page/page.model';
import { $copyModalData, closeCopyModalData } from './copy-modal.model';

$copyModalData
  .on(fxCopyPass.done, (_, { result: { text } }) => ({ isOpen: true, content: text }))
  .reset([signout, pageUnmounted, closeCopyModalData]);
