import { createStore, createEvent } from 'effector';

export const $copyModalData = createStore<{
  isOpen: boolean;
  content: string;
}>({ isOpen: false, content: '' });

export const closeCopyModalData = createEvent<void>();
