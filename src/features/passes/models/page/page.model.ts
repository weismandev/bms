import { createEffect, createStore, createEvent } from 'effector';
import { createGate } from 'effector-react';

import { GetPassesResponse, GerPassesPayload } from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $raw = createStore<GetPassesResponse>({});
export const $isLoadingPasses = createStore<boolean>(false);

export const fxGetPasses = createEffect<GerPassesPayload, GetPassesResponse, Error>();
