import { sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { fxGetList as fxGetBuildings } from '@features/house-select';
import { passApi } from '../../api';
import { GerPassesPayload, Filters, Table } from '../../interfaces';
import {
  fxGetPass,
  fxGetPassOptions,
  fxCreatePass,
  fxRevokePass,
  fxCopyPass,
  fxActivatePass,
  fxSendSms,
} from '../detail/detail.model';
import { $filters, filtersSubmitted } from '../filters/filters.model';
import { $tableParams, $currentPage } from '../table/table.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';
import {
  fxGetPasses,
  pageUnmounted,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  pageMounted,
  $raw,
  $isLoadingPasses,
} from './page.model';

fxGetPasses.use(passApi.getPasses);
fxGetPass.use(passApi.getPass);
fxGetPassOptions.use(passApi.getPassOptions);
fxCreatePass.use(passApi.createPass);
fxRevokePass.use(passApi.revokePass);
fxCopyPass.use(passApi.getCopyTextPass);
fxActivatePass.use(passApi.activatePass);
fxSendSms.use(passApi.sendSms);

const errorOccured = merge([
  fxGetPasses.fail,
  fxGetPass.fail,
  fxGetPassOptions.fail,
  fxCreatePass.fail,
  fxRevokePass.fail,
  fxCopyPass.fail,
  fxActivatePass.fail,
  fxSendSms.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetPass,
        fxGetPassOptions,
        fxCreatePass,
        fxRevokePass,
        fxCopyPass,
        fxActivatePass,
        fxSendSms,
      ],
    }),
    (_, isPending) => isPending
  )
  .reset([pageUnmounted, signout]);

$isLoadingPasses
  .on(fxGetPasses.pending, (_, loading) => loading)
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

// запрос списка пропусков
sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxCreatePass.done,
    fxRevokePass.done,
    fxActivatePass.done,
    $settingsInitialized,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }: { table: Table; filters: Filters }) => {
    const payload: GerPassesPayload = {
      page: table.page,
      per_page: table.per_page,
      search: table.search,
      archived: Number(Boolean(filters.isArchive)),
      filters: {},
    };

    if (table.sorting.length > 0) {
      const sorting = table.sorting[0];

      if (sorting?.field === 'createDate') {
        payload.sort = 'created_at';
      }

      if (sorting?.field === 'activeFrom') {
        payload.sort = 'active_from';
      }

      if (sorting?.field === 'activeTo') {
        payload.sort = 'active_to';
      }

      if (sorting.sort) {
        payload.order = sorting.sort;
      }
    }

    if (typeof filters.enterprise === 'object' && filters.enterprise?.id) {
      payload.filters.enterprise_id = filters.enterprise.id;
    }

    if (filters.complexes.length > 0) {
      payload.filters.object_ids = filters.complexes.map((complex) => complex.id);
    }

    if (typeof filters.status === 'object' && filters.status?.id) {
      payload.filters.state_id = filters.status.id;
    }

    if (filters.passTypes.length > 0) {
      payload.filters.strategy_ids = filters.passTypes.map((type) => type.id);
    }

    if (filters.vehicle.length > 0) {
      const withCar = filters.vehicle.find((i) => i.id === 1);
      const withoutCar = filters.vehicle.find((i) => i.id === 2);

      if (withCar) {
        payload.filters.with_car = 1;
      }

      if (withoutCar) {
        payload.filters.without_car = 1;
      }
    }

    return payload;
  },
  target: fxGetPasses,
});

$raw.on(fxGetPasses.done, (_, { result }) => result).reset([pageUnmounted, signout]);

$currentPage.reset(filtersSubmitted);

// во время получения настроек крутится лоадер
sample({
  clock: $isGettingSettingsFromStorage,
  fn: (data) => data,
  target: $isLoading,
});

/** При маунте запрашивается список зданий */
sample({
  clock: pageMounted,
  target: fxGetBuildings,
});
