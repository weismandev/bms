import { createEffect, createStore } from 'effector';
import {
  GetParkingZonePayload,
  GetParkingZoneResponse,
  ParkingZone,
} from '../../interfaces';

export const fxGetList = createEffect<
  GetParkingZonePayload,
  GetParkingZoneResponse,
  Error
>();

export const $data = createStore<ParkingZone[]>([]);
export const $isLoading = createStore<boolean>(false);
