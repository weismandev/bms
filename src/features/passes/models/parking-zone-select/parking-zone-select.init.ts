import { signout } from '@features/common';
import { passApi } from '../../api';
import { ParkingPlaceList } from '../../interfaces';
import { fxGetList, $data, $isLoading } from './parking-zone-select.model';

fxGetList.use(passApi.getParkingZone);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.parkingZoneList)) {
      return [];
    }

    return result.parkingZoneList.map((item) => ({
      id: item?.id,
      title: item?.title ?? '',
      selectMode: item?.selectMode ?? null,
      parkingPlaceList:
        Array.isArray(item?.parkingPlaceList) && item.parkingPlaceList.length
          ? item.parkingPlaceList.map((place: ParkingPlaceList) => ({
              id: place?.id,
              title: place?.number ?? '',
            }))
          : [],
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
