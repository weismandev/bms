import { combine } from 'effector';
import { format } from 'date-fns';
import { createTableBag } from '@features/data-grid';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { PassResponse } from '../../interfaces';
import { $raw } from '../page/page.model';

const { t } = i18n;

export const statusColor: { [key: string]: { backgroundColor: string; color: string } } =
  {
    active: {
      backgroundColor: '#00A947',
      color: '#FFFFFF',
    },
    revoked: {
      backgroundColor: '#EEEEEE',
      color: 'rgba(25, 28, 41, 0.6)',
    },
    waiting: {
      backgroundColor: '#008CF7',
      color: '#FFFFFF',
    },
    expired: {
      backgroundColor: '#E13F3D',
      color: '#FFFFFF',
    },
  };

const stuffStatusColor: { [key: string]: { backgroundColor: string; color: string } } = {
  new: {
    backgroundColor: '#63B4F6',
    color: '#FFFFFF',
  },
  approved: {
    backgroundColor: '#00BC53',
    color: '#FFFFFF',
  },
  qualification: {
    backgroundColor: '#FFBE00',
    color: '#FFFFFF',
  },
  rejected: {
    backgroundColor: '#C22F30',
    color: '#FFFFFF',
  },
  issued: {
    backgroundColor: '#008831',
    color: '#FFFFFF',
  },
  on_territory: {
    backgroundColor: '#005BC3',
    color: '#FFFFFF',
  },
  overdue: {
    backgroundColor: '#F0483F',
    color: '#FFFFFF',
  },
  finished: {
    backgroundColor: '#E5F5EC',
    color: 'rgba(25, 28, 41, 0.6)',
  },
  expired: {
    backgroundColor: '#E3ECF1',
    color: 'rgba(25, 28, 41, 0.6)',
  },
  revoked: {
    backgroundColor: '#EEEEEE',
    color: 'rgba(25, 28, 41, 0.6)',
  },
  for_approval: {
    backgroundColor: '#008CF7',
    color: '#FFFFFF',
  },
  coordination_top: {
    backgroundColor: '#FF8A00',
    color: '#FFFFFF',
  },
};

const columns = [
  {
    field: 'passNumber',
    headerName: '№',
    width: 120,
    sortable: false,
  },
  {
    field: 'fullName',
    headerName: t('Label.FullName'),
    width: 270,
    sortable: false,
  },
  {
    field: 'guestType',
    headerName: t('GuestType'),
    width: 200,
    sortable: false,
  },
  {
    field: 'phone',
    headerName: t('Label.phone'),
    width: 140,
    sortable: false,
  },
  {
    field: 'enterprise',
    headerName: t('company'),
    width: 250,
    sortable: false,
  },
  {
    field: 'complex',
    headerName: t('AnObject'),
    width: 200,
    sortable: false,
  },
  {
    field: 'properties',
    headerName: t('room'),
    width: 300,
    sortable: false,
  },
  {
    field: 'activeFrom',
    headerName: t('ActualFrom'),
    width: 140,
    sortable: true,
  },
  {
    field: 'activeTo',
    headerName: t('ActualTo'),
    width: 140,
    sortable: true,
  },
  {
    field: 'startFrom',
    headerName: t('BeginningWith'),
    width: 100,
    sortable: false,
  },
  {
    field: 'startTo',
    headerName: t('EndingBy'),
    width: 120,
    sortable: false,
  },
  // {
  //   field: 'hasStuff',
  //   headerName: t('AvailabilityOfTmts'),
  //   width: 120,
  //   sortable: false,
  // }, тмц пока скрыто
  {
    field: 'vehicle',
    headerName: t('ShortVehicle'),
    width: 120,
    sortable: false,
  },
  {
    field: 'parkingPlace',
    headerName: t('PM'),
    width: 120,
    sortable: false,
  },
  {
    field: 'comment',
    headerName: t('Label.comment'),
    width: 300,
    sortable: false,
  },
  {
    field: 'status',
    headerName: t('Label.status'),
    width: 170,
    sortable: false,
  },
  // {
  //   field: 'stuffStatus',
  //   headerName: t('Label.status'),
  //   width: 250,
  //   sortable: false,
  // }, пока тмц не делать
  {
    field: 'passType',
    headerName: t('PassesType'),
    width: 140,
    sortable: false,
  },
  {
    field: 'author',
    headerName: t('Issued'),
    width: 270,
    sortable: false,
  },
  {
    field: 'createDate',
    headerName: t('PassCreationDate'),
    width: 190,
    sortable: true,
  },
];

export const {
  pageNumberChanged,
  pageSizeChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $search,
  searchChanged,
  $sorting,
  sortChanged,
  $selectRow,
  selectionRowChanged,
  $visibilityColumns,
  visibilityColumnsChanged,
  columnsWidthChanged,
  orderColumnsChanged,
  $columns,
} = createTableBag({
  columns,
  pageSize: 25,
  visibilityColumns: {
    phone: false,
    enterprise: false,
    // hasStuff: false,тмц пока скрыто
    vehicle: false,
    parkingPlace: false,
    comment: false,
    startFrom: false,
    startTo: false,
  },
});

export const $savedColumns = $columns.map((clmn) =>
  clmn.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);
export const $count = $raw.map(({ meta }) => meta?.total || 0);
export const $countPage = combine($count, $pageSize, (count, pageSize) =>
  count ? Math.ceil(count / pageSize) : 0
);

const formatData = (passList: PassResponse[]) =>
  passList.map((pass) => {
    const fullName =
      pass?.user_surname || pass?.user_name || pass?.user_patronymic
        ? `${pass?.user_surname || ''} ${pass?.user_name || ''} ${
            pass?.user_patronymic || ''
          }`
        : t('Unknown');

    return {
      id: pass.id,
      passNumber: pass?.pass_number || '-',
      fullName,
      guestType: pass?.guest_type?.title ?? '-',
      phone: Boolean(pass?.user_phone) ? pass?.user_phone : t('isNotDefined'),
      enterprise: pass?.enterprise?.title || '-',
      complex: pass?.complex?.title || '-',
      properties: Array.isArray(pass?.properties)
        ? pass.properties.map((property) => property.title)
        : [],
      activeFrom: pass?.active_from
        ? format(new Date(pass.active_from.date), 'dd.MM.yyyy')
        : '-',
      activeTo: pass?.active_to
        ? format(new Date(pass.active_to.date), 'dd.MM.yyyy')
        : '-',
      startFrom: pass?.active_from
        ? format(new Date(pass.active_from.date), 'HH:mm')
        : '-',
      startTo: pass?.active_to ? format(new Date(pass.active_to.date), 'HH:mm') : '-',
      hasStuff: Boolean(pass?.has_stuff),
      vehicle:
        (Array.isArray(pass?.vehicles) && pass?.vehicles[0]?.id_number) ||
        t('isNotDefinedit'),
      parkingPlace: pass?.parking_place?.number || t('isNotDefinedit'),
      comment: pass?.comment || '-',
      status:
        pass?.state?.slug && pass?.state?.title
          ? {
              title: pass.state.title,
              style: statusColor[pass.state.slug] || {},
            }
          : null,
      stuffStatus:
        pass?.stuff_data?.status?.slug && pass?.stuff_data?.status?.title
          ? {
              title: pass.stuff_data.status.title,
              style: stuffStatusColor[pass.stuff_data.status.slug] || {},
            }
          : null,
      passType: pass?.strategy?.title || '-',
      author: pass?.author?.fullname || t('Unknown'),
      createDate: pass?.created_at?.date
        ? format(new Date(pass.created_at.date), 'dd.MM.yyyy')
        : t('Unknown'),
    };
  });

export const $tableData = $raw.map(({ passList }) =>
  Array.isArray(passList) && passList.length > 0 ? formatData(passList) : []
);
