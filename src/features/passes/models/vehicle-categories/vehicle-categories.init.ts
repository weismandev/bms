import { signout } from '@features/common';
import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './vehicle-categories.model';

fxGetList.use(passApi.getVehicleCategories);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.vehicleCategoryList)) {
      return [];
    }

    return result.vehicleCategoryList.map((item) => ({
      id: item.id,
      title: item?.title || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
