import { createEffect, createStore } from 'effector';
import { Value } from '../../interfaces';

export const fxGetList = createEffect<void, { vehicleCategoryList: Value[] }, Error>();

export const $data = createStore<Value[]>([]);
export const $isLoading = createStore<boolean>(false);
