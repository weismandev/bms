import { signout } from '@features/common';
import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './enterprises-select.model';

fxGetList.use(passApi.getEnterprises);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.enterprises)) {
      return [];
    }

    return result.enterprises.map((item) => ({
      id: item?.id,
      title: item?.title ?? '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
