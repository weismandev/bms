import { createEffect, createStore } from 'effector';
import {
  GetEnterprisesPayload,
  GetEnterprisesResponse,
  Enterprise,
} from '../../interfaces';

export const fxGetList = createEffect<
  GetEnterprisesPayload,
  GetEnterprisesResponse,
  Error
>();

export const $data = createStore<Enterprise[]>([]);
export const $isLoading = createStore<boolean>(false);
