import { signout } from '@features/common';

import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './stuff-action-types-select.model';

fxGetList.use(passApi.getStuffActionTypes);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.stuffActionTypeList)) {
      return [];
    }

    return result.stuffActionTypeList.map((item) => ({
      id: item.id,
      title: item?.title || '',
      slug: item?.slug || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
