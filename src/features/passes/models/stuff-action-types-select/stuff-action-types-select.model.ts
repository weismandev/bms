import { createEffect, createStore } from 'effector';

import { StuffActionTypesResponse, StuffActionTypes } from '../../interfaces';

export const fxGetList = createEffect<void, StuffActionTypesResponse, Error>();

export const $data = createStore<StuffActionTypes[]>([]);
export const $isLoading = createStore<boolean>(false);
