import { createEffect, createStore } from 'effector';

import { PassStatusResponse, PassStatus } from '../../interfaces';

export const fxGetList = createEffect<void, PassStatusResponse, Error>();

export const $data = createStore<PassStatus[]>([]);
export const $isLoading = createStore<boolean>(false);
