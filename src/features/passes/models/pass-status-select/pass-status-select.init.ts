import { signout } from '@features/common';

import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './pass-status-select.model';

fxGetList.use(passApi.getPassStatus);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.stateList)) {
      return [];
    }

    return result.stateList.map((item) => ({
      id: item.id,
      title: item?.title || '',
      slug: item.slug || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
