import { signout } from '@features/common';

import { passApi } from '../../api';
import { fxGetList, $data, $isLoading } from './stuff-types-select.model';

fxGetList.use(passApi.getStuffTypes);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.stuffTypeList)) {
      return [];
    }

    return result.stuffTypeList.map((item) => ({
      id: item.id,
      title: item?.title || '',
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
