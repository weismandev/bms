import { createEffect, createStore } from 'effector';

import { StuffTypesResponse, StuffTypes } from '../../interfaces';

export const fxGetList = createEffect<void, StuffTypesResponse, Error>();

export const $data = createStore<StuffTypes[]>([]);
export const $isLoading = createStore<boolean>(false);
