import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FieldProps, Field } from 'formik';
import { InputField, Divider } from '@ui/index';
import { DocumentPass, DocumentType } from '../../interfaces';
import { $mode, $isFilterOpen } from '../../models';
import { DocumentTypesSelectField } from '../../organisms/document-types-select';
import { StyledFields } from '../../organisms/pass-form/styled';
import { PassportNumberMaskedField } from '../passport-number-masked-input';

interface Props {
  name: string;
  document: DocumentPass;
  setFieldValue: (field: string, value: any) => void;
}

export const DocumentCard: FC<Props> = ({ name, document, setFieldValue }) => {
  const { t } = useTranslation();
  const [mode, isFilterOpen] = useUnit([$mode, $isFilterOpen]);

  const handleDocumentType = (value: DocumentType) => {
    setFieldValue(`${name}.type.value`, value);
    setFieldValue(`${name}.number.value`, '');
  };

  return (
    <>
      {(document?.type?.visible ||
        document?.number?.visible ||
        document?.issued_date?.visible) && (
        <StyledFields isFilterOpen={isFilterOpen}>
          {document?.type?.visible && (
            <Field
              name={`${name}.type.value`}
              component={DocumentTypesSelectField}
              label={t('DocumentType')}
              placeholder={t('chooseType')}
              mode={mode}
              onChange={handleDocumentType}
              required={document?.type?.required}
              divider={false}
            />
          )}
          {document?.type?.visible &&
            document?.type?.value?.id === 1 &&
            document?.number?.visible && (
              <Field
                name={`${name}.number.value`}
                render={({ field, form }: FieldProps) => (
                  <PassportNumberMaskedField
                    field={field}
                    form={form}
                    mode={mode}
                    label={t('Serial/number')}
                    placeholder={t('EnterTheNormativeTerm')}
                    required={document?.number?.required}
                    divider={false}
                  />
                )}
              />
            )}
          {document?.type?.visible &&
            document?.type?.value?.id !== 1 &&
            document?.number?.visible && (
              <Field
                name={`${name}.number.value`}
                component={InputField}
                label={t('Serial/number')}
                placeholder={t('EnterSeriesAndNumber')}
                mode={mode}
                required={document?.number?.required}
                divider={false}
              />
            )}
          {document?.issued_date?.visible && (
            <Field
              name={`${name}.issued_date.value`}
              component={InputField}
              label={t('Issued')}
              mode={mode}
              type="date"
              required={document?.issued_date?.required}
              divider={false}
            />
          )}
        </StyledFields>
      )}
      {document?.birthday?.visible && (
        <StyledFields isFilterOpen={isFilterOpen}>
          {document?.birthday?.visible && (
            <Field
              name={`${name}.birthday.value`}
              component={InputField}
              label={t('DateOfBirth')}
              mode={mode}
              type="date"
              required={document?.birthday?.required}
              divider={false}
            />
          )}
        </StyledFields>
      )}
      {(document?.type?.visible ||
        document?.number?.visible ||
        document?.issued_date?.visible ||
        document?.birthday?.visible) && <Divider />}
    </>
  );
};
