import { FC } from 'react';
import InputMask from 'react-input-mask';
import { InputField } from '@ui/index';

const mask = '9999 999999';

interface Props {
  field: object;
  form: object;
  mode: string;
  label: string;
  placeholder: string;
  required: boolean;
  divider: boolean;
}

const DurationMaskedInput: FC<object> = (props) => (
  <InputMask mask={mask} alwaysShowMask {...props} />
);

export const PassportNumberMaskedField: FC<Props> = ({
  field,
  form,
  mode,
  label,
  placeholder,
  required,
  divider,
  ...rest
}) => (
  <InputField
    field={field}
    form={form}
    inputComponent={DurationMaskedInput}
    mode={mode}
    label={label}
    placeholder={placeholder}
    required={required}
    divider={divider}
    {...rest}
  />
);
