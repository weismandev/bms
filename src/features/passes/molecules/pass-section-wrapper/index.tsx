import { FC, useState } from 'react';
import { Collapse } from '@mui/material';

import { Toolbar } from '@ui/index';

import { StyledTypography, StyledExpandButton } from './styled';

interface Props {
  name: string;
  children: JSX.Element | JSX.Element[];
}

export const PassSectionWrapper: FC<Props> = ({ name, children }) => {
  const [expanded, setExpanded] = useState(true);

  const handleExpanded = () => setExpanded(!expanded);

  return (
    <>
      <Toolbar>
        <StyledTypography>{name}</StyledTypography>
        <StyledExpandButton expanded={expanded} handleExpanded={handleExpanded} />
      </Toolbar>
      <Collapse in={expanded} timeout="auto">
        {children}
      </Collapse>
    </>
  );
};
