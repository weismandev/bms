import { Typography } from '@mui/material';
import styled from '@emotion/styled';
import { ExpandButton } from '../../atoms';

export const StyledTypography = styled(Typography)`
  font-weight: 500;
  font-size: 18px;
  color: #191c29;
`;

interface StyledExpandButtonProps {
  expanded: boolean;
  handleExpanded: () => void;
  className?: string;
}

export const StyledExpandButton = styled((props: StyledExpandButtonProps) => (
  <ExpandButton {...props} />
))`
  height: 35px;
  width: 35px;
`;
