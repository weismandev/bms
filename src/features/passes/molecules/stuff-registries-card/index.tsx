import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';

import { InputField } from '@ui/index';

import { StyledFields, StyledDeleteButton } from './styled';
import { $mode } from '../../models';
import { StuffRegistriesPass } from '../../interfaces';

interface Props {
  name: string;
  index: number;
  remove: (index: number) => void;
  stuff: StuffRegistriesPass;
}

export const StuffRegistriesCard: FC<Props> = ({ name, index, remove, stuff }) => {
  const { t } = useTranslation();
  const mode = useUnit($mode);

  const handleDelete = () => remove(index);

  return (
    <StyledFields isFirstItem={index === 0}>
      <>
        {stuff?.name?.visible && (
          <Field
            name={`${name}.name.value`}
            component={InputField}
            label={index === 0 ? t('Name') : null}
            placeholder={t('EnterTheTitle')}
            mode={mode}
            divider={false}
            required={stuff?.name?.required}
          />
        )}
        {stuff?.amount?.visible && (
          <Field
            name={`${name}.amount.value`}
            component={InputField}
            label={index === 0 ? t('Amount') : null}
            placeholder={t('EnterQuantity')}
            mode={mode}
            type="number"
            divider={false}
            required={stuff?.amount?.required}
          />
        )}
      </>
      {mode === 'edit' && (
        <StyledDeleteButton isFirstItem={index === 0} onClick={handleDelete} />
      )}
    </StyledFields>
  );
};
