import styled from '@emotion/styled';

import { DeleteButton } from '@ui/index';

export const StyledFields = styled.div(
  ({ isFirstItem }: { isFirstItem: boolean }): any => ({
    display: 'grid',
    gridTemplateColumns: '2fr 0.5fr 0.2fr',
    gap: 15,
    marginTop: !isFirstItem && 10,
  })
);

interface DeleteButtonProps {
  isFirstItem: boolean;
  onClick: () => void;
}

export const StyledDeleteButton = styled((props: DeleteButtonProps) => (
  <DeleteButton {...props} />
))(({ isFirstItem }) => ({
  marginTop: isFirstItem ? 22 : 0,
}));
