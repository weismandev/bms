import styled from '@emotion/styled';

export const StyledToolbar = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 24,
  padding: 24,
}));

export const StyledFirstLevel = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
}));

export const StyledTitle = styled('div')(() => ({
  fontSize: 20,
  fontWeight: 500,
  fontStyle: 'normal',
  color: 'rgba(25, 28, 41, 0.87)',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  overflow: 'hidden',
}));

export const StyledSecondLevel = styled('div')(() => ({
  display: 'flex',
  gap: 8,
}));
