import { FC } from 'react';
import { Event } from 'effector';
import { CloseButton, SaveButton, CancelButton } from '@ui/index';
import {
  ConfirmButton,
  DublicateButton,
  RevokeButton,
  SmsButton,
  CopyButton,
} from '../../atoms';
import {
  StyledToolbar,
  StyledFirstLevel,
  StyledTitle,
  StyledSecondLevel,
} from './styled';

interface Props {
  isNew: boolean;
  onCancel?: () => void;
  onClose?: () => boolean;
  show: {
    [key: string]: boolean;
  };
  disabled: {
    [key: string]: boolean;
  };
  revokePass?: Event<void>;
  copyPass?: Event<void>;
  dublicatePass?: Event<void>;
  activatePass?: Event<void>;
  sendSms?: Event<void>;
  title: string;
}

export const DetailToolbar: FC<Props> = ({
  isNew,
  onCancel,
  onClose,
  show,
  disabled,
  revokePass,
  copyPass,
  dublicatePass,
  activatePass,
  sendSms,
  title,
}) => (
  <StyledToolbar>
    <StyledFirstLevel>
      <StyledTitle>{title}</StyledTitle>
      <CloseButton onClick={onClose} />
    </StyledFirstLevel>
    <StyledSecondLevel>
      {isNew ? (
        <>
          <SaveButton kind="positive" type="submit" />
          <CancelButton kind="negative" onClick={onCancel} />
        </>
      ) : (
        <>
          {show.confirm && <ConfirmButton onClick={activatePass} />}
          {show.revoke && <RevokeButton onClick={revokePass} />}
          {show.sms && <SmsButton onClick={sendSms} disabled={disabled.sms} />}
          {show.dublicate && (
            <DublicateButton onClick={dublicatePass} disabled={disabled.dublicate} />
          )}
          {show.copy && <CopyButton onClick={copyPass} />}
        </>
      )}
    </StyledSecondLevel>
  </StyledToolbar>
);
