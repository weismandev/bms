import styled from '@emotion/styled';

import { FilterButton, AddButton } from '@ui/index';

interface ButtonProps {
  disabled?: boolean;
  onClick?: () => void;
}

const buttonStyle = 'margin: 0px 10px 0px 0px;';

export const StyledFilterButton = styled((props: ButtonProps) => (
  <FilterButton {...props} />
))`
  ${buttonStyle}
`;

export const StyledAddButton = styled((props: ButtonProps) => <AddButton {...props} />)`
  ${buttonStyle}
`;
