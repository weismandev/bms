import { FC } from 'react';
import { useUnit } from 'effector-react';

import { Greedy, AdaptiveSearch } from '@ui/index';

import {
  $search,
  searchChanged,
  $isFilterOpen,
  changedFilterVisibility,
  $count,
  $isDetailOpen,
  addClicked,
  $mode,
  $opened,
} from '../../models';
import { Opened } from '../../interfaces';
import { StyledFilterButton, StyledAddButton } from './styled';

export const CustomToolbar: FC = () => {
  const [searchValue, isFilterOpen, totalCount, isDetailOpen, mode, opened] = useUnit([
    $search,
    $isFilterOpen,
    $count,
    $isDetailOpen,
    $mode,
    $opened,
  ]);

  const isDisabledAddClicked = mode === 'edit' && !(opened as Opened).id;
  const isAnySideOpen = isFilterOpen && isDetailOpen;
  const onClickFilter = () => changedFilterVisibility(true);

  return (
    <>
      <StyledFilterButton disabled={isFilterOpen} onClick={onClickFilter} />
      <AdaptiveSearch
        {...{
          totalCount,
          searchValue,
          searchChanged,
          isAnySideOpen,
        }}
      />
      <Greedy />
      <StyledAddButton disabled={isDisabledAddClicked} onClick={addClicked} />
    </>
  );
};
