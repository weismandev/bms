export type GetParkingZonePayload = {
  active_from: string;
  active_to: string;
  building_id: number;
  author_id?: number;
};

export type GetParkingZoneResponse = {
  parkingZoneList: {
    disabled: boolean;
    id: number;
    info: string;
    isCommonZone: boolean;
    parkingPlaceList: ParkingPlaceList[];
    selectMode: string;
    title: string;
  };
};

export type ParkingPlaceList = {
  id: number;
  number: string;
  is_common_zone: boolean;
};

export type ParkingZone = {
  id: number;
  title: string;
  selectMode: string;
  parkingPlaceList: { id: number; title: string }[];
};
