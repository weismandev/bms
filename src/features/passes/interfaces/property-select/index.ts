export interface PropertyListPayload {
  complex_id?: number;
  building_id?: number;
}

export interface PropertyListResponse {
  propertyList: Property[];
}

export interface Property {
  id: number;
  title: string;
}
