export interface PassStatusResponse {
  stateList: PassStatus[];
}

export interface PassStatus {
  id: number;
  slug: string;
  title: string;
}
