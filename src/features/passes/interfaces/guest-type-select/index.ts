export interface GuestTypePayload {
  building_id: string;
}

export interface GuestTypeResponse {
  guestTypeList: GuestType[];
}

export interface GuestType {
  id: number;
  title: string;
  is_predefined: boolean;
}
