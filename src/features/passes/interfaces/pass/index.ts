import { DocumentType } from '../document-types-select';
import { GuestType } from '../guest-type-select';
import { ParkingZone, ParkingPlaceList } from '../parking-zone';
import { PassStatus } from '../pass-status-select';
import { PassType } from '../pass-types-select';
import { StuffActionTypes } from '../stuff-action-types-select';
import { StuffTypes } from '../stuff-types-select';

export interface GetPassPayload {
  id: number;
  mode: string;
  with_pass_config: number;
}

export interface GetPassResponse {
  pass: PassResponse;
}

export interface PassParkingPlace {
  id: number;
  number: number;
  parking_lot_id: number;
  parking_lot_title: string;
  parking_zone_id: number;
  parking_zone_title: string;
}

export interface PassResponse {
  id: number;
  pass_number: string;
  user_surname: string;
  user_name: string;
  user_patronymic: string;
  user_phone: string;
  user_email: string;
  user_company_name: string;
  guest_type: Value;
  identity_documents: Documents[];
  enterprise: Value;
  complex: Value;
  properties: Value[];
  strategy: PassType;
  author: PassAuthor;
  created_at: {
    date: string;
  };
  comment: string;
  state: PassStatus;
  active_from: {
    date: string;
  };
  active_to: {
    date: string;
  };
  work_time_from: string;
  work_time_to: string;
  vehicles: {
    brand: string;
    id_number: string;
    category: Value;
  }[];
  group_members_number: number;
  stuff_data: {
    action_type: StuffActionTypes;
    status: PassStatus;
    stuff_types: [];
    stuff_comment: string;
    stuff_registries: { name: string; amount: string }[];
  };
  has_group: boolean;
  has_vehicle: boolean;
  has_stuff: boolean;
  building_id: number;
  parking_place: PassParkingPlace;
  parking_places: PassParkingPlace[];
  parking_zone_group: {
    id: number;
  };
}

export interface Stuff {
  _id: string;
  name: string;
  amount: string;
}

export interface Documents {
  birthday: string;
  type: string;
  number: number;
  issued_date: number;
}

export interface Value {
  id: number;
  title: string;
}

export interface Building {
  id: number;
  title: string;
  complex: Value;
}

export interface PassAuthor {
  id: number;
  fullname: string;
}

export interface SectionListResponse {
  sectionList: SectionList[];
}

export interface SectionList {
  name: string;
  propertyList: PropertyList[];
  title: string;
  visible: boolean;
}

export interface PropertyList {
  name: string;
  title: string;
  required: boolean;
  visible: boolean;
  sms: boolean;
}

export interface OptionList {
  sections?: {
    [name: string]: boolean;
  };
  fields?: OptionListFields;
}

export interface OptionListFields {
  [key: string]: { visible: boolean; required: boolean };
}

export interface ParkingPlace {
  id: number;
  number: string;
}

export interface OpenedPassStatus {
  title: string;
  slug: string;
  style: {
    backgroundColor?: string;
    color?: string;
  };
}

export interface Opened {
  id: number | null;
  buildingId: number;
  surname: string;
  name: string;
  patronymic: string;
  phone: string;
  email: string;
  companyName: string;
  guestType: GuestType;
  documents: {
    _id: string;
    birthday: string;
    type: string;
    number: string;
    issued_date: string;
  }[];
  enterprise: Value;
  complex: Value;
  properties: Value[];
  passType: PassType;
  passNumber: string;
  hasGroup: boolean;
  hasVehicle: boolean;
  hasTmc: boolean;
  author: Value;
  passCreatedAt: string;
  comment: string;
  passStatus: OpenedPassStatus | null;
  activeFrom: string;
  activeTo: string;
  workTimeFrom: string;
  workTimeTo: string;
  brand: string;
  govNumver: string;
  parkingPlace: ParkingPlaceList;
  groupCountMember: string;
  stuffActionType: StuffActionTypes;
  stuffType: StuffTypes;
  stuffComment: string;
  stuffRegistries: Stuff[];
  vehicleCategory: Value;
  parkingZone: number;
}

export interface DefaultField {
  value: string;
  visible: boolean;
  required: boolean;
}

interface ValueArrayField {
  value: Value[];
  visible: boolean;
  required: boolean;
}

interface ValueField {
  value: Value;
  visible: boolean;
  required: boolean;
}

interface ValueBooleanField {
  value: boolean;
  visible: boolean;
  required: boolean;
}

export interface Pass {
  id: number;
  buildingId: number;
  building: Value;
  surname: DefaultField;
  name: DefaultField;
  patronymic: DefaultField;
  phone: DefaultField;
  email: DefaultField;
  companyName: DefaultField;
  guestType: {
    value: GuestType;
    visible: boolean;
    required: boolean;
  };
  documents: DocumentPass[];
  enterprise: {
    value: Value;
    visible: boolean;
    required: boolean;
  };
  complex: ValueField;
  properties: ValueArrayField;
  passType: {
    value: PassType;
    visible: boolean;
    required: boolean;
    disabled: boolean;
  };
  passNumber: DefaultField;
  hasGroup: ValueBooleanField;
  hasVehicle: ValueBooleanField;
  hasTmc: ValueBooleanField;
  author: ValueField;
  passCreatedAt: DefaultField;
  comment: DefaultField;
  passStatus: {
    value: OpenedPassStatus | null;
    visible: boolean;
    required: boolean;
  };
  activeFrom: DefaultField;
  activeTo: DefaultField;
  workTimeFrom: DefaultField;
  workTimeTo: DefaultField;
  brand: DefaultField;
  govNumver: DefaultField;
  parkingPlace: {
    value: ParkingPlaceList[];
    visible: boolean;
    required: boolean;
  };
  groupCountMember: DefaultField;
  stuffActionType: {
    value: StuffActionTypes;
    visible: boolean;
    required: boolean;
  };
  stuffType: {
    value: StuffTypes[];
    visible: boolean;
    required: boolean;
  };
  stuffComment: DefaultField;
  stuffRegistries: StuffRegistriesPass[];
  vehicleCategory: {
    value: Value;
    visible: boolean;
    required: boolean;
  };
  parkingZone: {
    value: ParkingZone;
    visible: boolean;
    required: boolean;
  };
}

export interface DocumentPass {
  _id: string;
  birthday: DefaultField;
  type: {
    value: DocumentType;
    visible: boolean;
    required: boolean;
  };
  number: DefaultField;
  issued_date: DefaultField;
}

export interface StuffRegistriesPass {
  _id: string;
  name: DefaultField;
  amount: DefaultField;
}

export interface Arguments {
  options: {
    from: {
      value: Pass;
    }[];
  };
}

export interface ArgumentsTmc {
  options: {
    from: {
      value: Pass;
    }[];
  };
  parent: DefaultField;
}

export interface CreatePassPayload {
  building_id?: number;
  user_name?: string;
  user_surname?: string;
  user_patronymic?: string;
  user_phone?: string;
  user_email?: string;
  user_company_name?: string;
  guest_type_id?: number;
  identity_documents?: DocumentsPayload[];
  enterprise_id?: number;
  complex_id?: string;
  property_ids?: string[];
  strategy_id?: number;
  has_group?: number;
  has_vehicle?: number;
  has_stuff?: number;
  author_id?: number;
  comment?: string;
  active_from?: string;
  active_to?: string;
  work_time_from?: string;
  work_time_to?: string;
  vehicles?: VehiclePayload[];
  entry_point_id?: number;
  loading_zone_id?: number;
  parking_place_id?: number;
  group_members_number?: number;
  parking_zone_id?: number;
  parking_place_ids?: number[];
}

export interface DocumentsPayload {
  type?: string;
  birthday?: string;
  number?: string;
  issued_date?: string;
}
export interface VehiclePayload {
  brand?: string;
  id_number?: string;
  category_id?: number;
}

export interface SectionListPayload {
  mode: string;
  building_id: number;
}

export interface PassFuncPayload {
  id: number;
}

export interface PassCopyResponse {
  text: string;
}
