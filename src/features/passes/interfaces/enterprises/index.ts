export type GetEnterprisesPayload = {
  filter?: {
    building_id?: number;
  };
};

export type GetEnterprisesResponse = {
  enterprises: Enterprise[];
};

export type Enterprise = {
  id: number;
  title: string;
};
