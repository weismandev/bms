export interface DocumentTypesResponse {
  identityDocumentTypeList: DocumentType[];
}

export interface DocumentType {
  id: number;
  title: string;
  slug: string;
}
