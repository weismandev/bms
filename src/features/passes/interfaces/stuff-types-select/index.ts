export interface StuffTypesResponse {
  stuffTypeList: StuffTypes[];
}

export interface StuffTypes {
  id: number;
  title: string;
}
