export interface StuffActionTypesResponse {
  stuffActionTypeList: StuffActionTypes[];
}

export interface StuffActionTypes {
  id: number;
  slug: string;
  title: string;
}
