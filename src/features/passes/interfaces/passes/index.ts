import { GridSortDirection } from '@mui/x-data-grid-pro';

import { PassResponse, Value } from '../pass';
import { PassStatus } from '../pass-status-select';
import { PassType } from '../pass-types-select';

export interface GerPassesPayload {
  page: number;
  per_page: number;
  search: string;
  archived: number;
  sort?: string;
  order?: 'asc' | 'desc';
  filters: {
    enterprise_id?: number;
    object_ids?: number[];
    state_id?: number;
    strategy_ids?: number[];
    with_car?: number;
    without_car?: number;
  };
}

export interface GetPassesResponse {
  meta?: {
    total: number;
  };
  passList?: PassResponse[];
}

export interface Filters {
  isArchive: boolean;
  enterprise: Value;
  complexes: Value[];
  status: PassStatus;
  passTypes: PassType[];
  vehicle: Value[];
}

export interface Table {
  page: number;
  per_page: number;
  sorting: {
    field: string;
    sort: GridSortDirection;
  }[];
  search: string;
}
