import { api } from '@api/api2';
import {
  GetPassPayload,
  SectionListPayload,
  GuestTypePayload,
  CreatePassPayload,
  GerPassesPayload,
  PassFuncPayload,
  PropertyListPayload,
  GetParkingZonePayload,
  GetEnterprisesPayload,
} from '../interfaces';

const getPass = (payload: GetPassPayload) => api.v4('get', 'scud-pass/get', payload);
const getDocumentTypes = () => api.v4('get', 'scud-pass/identity-document-type/list');
const getPropertyList = (payload: PropertyListPayload) =>
  api.v4('get', 'scud-pass/property/list', payload);
const getStuffActionTypes = () => api.v4('get', 'scud-pass/stuff-action-type/list');
const getStuffTypes = () => api.v4('get', 'scud-pass/stuff-type/list');
const getPassOptions = (payload: SectionListPayload) =>
  api.v4('get', 'scud-pass/option/simple-list', {
    ...payload,
    show_all: 1,
  });
const getGuestType = (payload: GuestTypePayload) =>
  api.v4('get', 'scud-pass/guest-type/available-list', payload);
const createPass = (payload: CreatePassPayload) =>
  api.v4('post', 'scud-pass/create', payload);
const getPasses = (payload: GerPassesPayload) =>
  api.v4('get', 'scud-pass/list', { ...payload, with_pass_config: 0 });
const getPassStatus = () => api.v4('get', 'scud-pass/state/list');
const revokePass = (payload: PassFuncPayload) =>
  api.v4('post', 'mobile/scud-pass/revoke', payload);
const getCopyTextPass = (payload: PassFuncPayload) =>
  api.v4('get', 'scud-pass/share-text', payload);
const activatePass = (payload: PassFuncPayload) =>
  api.v4('post', 'mobile/scud-pass/activate', payload);
const sendSms = (payload: PassFuncPayload) =>
  api.v4('post', 'scud-pass/send-sms-invite', payload);
const getVehicleCategories = () => api.v4('get', 'vehicle/category-list');
const getParkingZone = (payload: GetParkingZonePayload) =>
  api.v4('get', 'scud-pass/get-grouped-free-slots', payload);
const getEnterprises = (payload: GetEnterprisesPayload) =>
  api.v4('get', 'enterprises/simple-list', payload);

export const passApi = {
  getPass,
  getDocumentTypes,
  getPropertyList,
  getStuffActionTypes,
  getStuffTypes,
  getPassOptions,
  getGuestType,
  createPass,
  getPasses,
  getPassStatus,
  revokePass,
  getCopyTextPass,
  activatePass,
  sendSms,
  getVehicleCategories,
  getParkingZone,
  getEnterprises,
};
