import { FC } from 'react';
import { Chip } from '@mui/material';

interface Props {
  value: {
    style: { color?: string; backgroundColor?: string };
    title: string;
  };
}

export const TableChip: FC<Props> = ({ value }) =>
  value ? <Chip label={value.title} size="small" style={value.style} /> : <div>-</div>;
