import { FC } from 'react';
import { Event } from 'effector';
import { Done } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { ActionIconButton } from '@ui/index';

interface Props {
  onClick?: Event<void>;
}

export const ConfirmButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionIconButton {...props} kind="positive" onClick={onClick}>
      <Done titleAccess={t('Confirm')} />
    </ActionIconButton>
  );
};
