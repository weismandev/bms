import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Event } from 'effector';
import { LibraryAddOutlined } from '@mui/icons-material';
import { ActionIconButton } from '@ui/index';

interface Props {
  onClick?: Event<void>;
  disabled: boolean;
}

export const DublicateButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionIconButton {...props} onClick={onClick}>
      <LibraryAddOutlined titleAccess={t('Dublicate')} />
    </ActionIconButton>
  );
};
