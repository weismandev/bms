import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { StyledAlert } from './styled';

export const MaxDays: FC = () => {
  const { t } = useTranslation();

  return <StyledAlert severity="info">{`${t('MaxNumberOfDays')} : 365`}</StyledAlert>;
};
