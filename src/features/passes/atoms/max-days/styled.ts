import { Alert } from '@mui/material';
import styled from '@emotion/styled';

export const StyledAlert = styled(Alert)(() => ({
  margin: '24px 0px',
}));
