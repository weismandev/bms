import { FC } from 'react';
import { Event } from 'effector';
import { Restore } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { ActionIconButton } from '@ui/index';

interface Props {
  onClick?: Event<void>;
}

export const RevokeButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionIconButton {...props} kind="negative" onClick={onClick}>
      <Restore titleAccess={t('Revoke')} />
    </ActionIconButton>
  );
};
