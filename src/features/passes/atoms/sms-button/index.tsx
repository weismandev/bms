import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Event } from 'effector';
import { SmsOutlined } from '@mui/icons-material';
import { ActionIconButton } from '@ui/index';

interface Props {
  onClick?: Event<void>;
  disabled: boolean;
}

export const SmsButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionIconButton {...props} onClick={onClick}>
      <SmsOutlined titleAccess={t('Sms')} />
    </ActionIconButton>
  );
};
