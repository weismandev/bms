import { FC } from 'react';
import { ExpandMore, ExpandLess } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import { useTranslation } from 'react-i18next';

interface Props {
  expanded: boolean;
  handleExpanded: () => void;
  className?: string;
}

export const ExpandButton: FC<Props> = ({ expanded, handleExpanded, ...props }) => {
  const { t } = useTranslation();

  return (
    <IconButton onClick={handleExpanded} size="large" {...props}>
      {expanded ? (
        <ExpandMore titleAccess={t('Collapse')} />
      ) : (
        <ExpandLess titleAccess={t('Expand')} />
      )}
    </IconButton>
  );
};
