import { FC } from 'react';
import { Event } from 'effector';
import { ContentCopyOutlined } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

import { ActionIconButton } from '@ui/index';

interface Props {
  onClick?: Event<void>;
}

export const CopyButton: FC<Props> = ({ onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <ActionIconButton {...props} onClick={onClick}>
      <ContentCopyOutlined titleAccess={t('Copy1')} />
    </ActionIconButton>
  );
};
