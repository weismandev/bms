import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { enterprisesCompaniesApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.enterprises) {
      return [];
    }

    return result.enterprises;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);

$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(enterprisesCompaniesApi.getList);

export { fxGetList, $data, $error, $isLoading };
