import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'stuff-pass/management/enterprises/list', payload);

export const enterprisesCompaniesApi = { getList };
