import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const StuffMyApprovedCompaniesSelect = (props) => {
  const { firstAsDefault = false, ...restProps } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  useEffect(() => {
    if (!props.value && firstAsDefault && options.length) {
      props.onChange(options[0]);
    }
  }, [options.length]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...restProps} />
  );
};

const StuffMyApprovedCompaniesSelectField = (props) => {
  const { onChangeEnterprise, ...rest } = props;

  const onChange = (value) => {
    if (onChangeEnterprise && typeof onChangeEnterprise === 'function') {
      onChangeEnterprise(props.form);
    }
    props.form.setFieldValue(props.field.name, value);
  };

  return (
    <SelectField
      component={<StuffMyApprovedCompaniesSelect />}
      onChange={onChange}
      {...rest}
    />
  );
};

export { StuffMyApprovedCompaniesSelect, StuffMyApprovedCompaniesSelectField };
