import { api } from '@api/api2';

const getWorksList = () => api.v4('get', 'time-tracker/works', { per_page: 9999999999 });

export const workApi = {
  getWorksList,
};
