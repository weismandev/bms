import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { Works } from '../../interfaces';
import { $data, fxGetList, $isLoading } from '../../models';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const WorkSelect: FC<object> = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

const WorkSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Works) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<WorkSelect />} onChange={onChange} {...props} />;
};

export { WorkSelect, WorkSelectField };
