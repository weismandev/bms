import { signout } from '@features/common';
import { workApi } from '../../api';
import { fxGetList, $data, $isLoading } from './works-select.model';

fxGetList.use(workApi.getWorksList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.works)) {
      return [];
    }

    return result.works.map((item) => ({
      id: item.id,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
