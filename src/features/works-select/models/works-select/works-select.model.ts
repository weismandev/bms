import { createEffect, createStore } from 'effector';
import { WorksListResponse, Works } from '../../interfaces';

const fxGetList = createEffect<void, WorksListResponse, Error>();

const $data = createStore<Works[]>([]);
const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
