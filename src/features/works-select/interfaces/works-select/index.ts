export interface WorksListResponse {
  works: Works[];
}

export interface Works {
  id: number;
  title: string;
}
