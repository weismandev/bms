import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('post', 'employee-groups/index', payload);

export const enterprisesGroupsApi = { getList };
