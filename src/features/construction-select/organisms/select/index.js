import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data } from '../../model';

const ConstructionSelect = (props) => {
  const options = useStore($data);

  return <SelectControl options={options} {...props} />;
};

const ConstructionSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<ConstructionSelect />} onChange={onChange} {...props} />
  );
};

export { ConstructionSelect, ConstructionSelectField };
