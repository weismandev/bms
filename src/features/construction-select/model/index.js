import { createStore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const $data = createStore([
  {
    title: t('ByFloor'),
    key: 'floor',
  },
  {
    title: t('ByEntrance'),
    state: 'entrance',
  },
  {
    title: t('ByTypeRoom'),
    state: 'placement',
  },
]);

export { $data };
