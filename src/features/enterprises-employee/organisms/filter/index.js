import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Wrapper, FilterToolbar, FilterFooter, SwitchField } from '@ui';
import { Formik, Form, Field } from 'formik';
import { EnterprisesCompaniesSelectField } from '../../../enterprises-companies-select';
import { EnterprisesGroupsSelectField } from '../../../enterprises-groups-select';
import {
  $filters,
  filtersSubmitted,
  changedFilterVisibility,
  defaultFilters,
} from '../../model/filter.model';

const Filter = memo(() => {
  const filters = useStore($filters);
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <FilterToolbar
        style={{ padding: 24 }}
        closeFilter={() => changedFilterVisibility(false)}
      />
      <Formik
        initialValues={filters}
        onSubmit={filtersSubmitted}
        onReset={() => filtersSubmitted(defaultFilters)}
        enableReinitialize
        render={() => (
          <Form style={{ padding: 24, paddingTop: 0 }}>
            <Field
              name="enterprises"
              component={EnterprisesCompaniesSelectField}
              label={t('Companies')}
              isMulti
              placeholder={t('SelectCompanies')}
            />
            <Field
              name="groups"
              component={EnterprisesGroupsSelectField}
              label={t('Groups')}
              isMulti
              placeholder={t('SelectGroups')}
            />
            <Field
              name="isset_vehicle"
              component={SwitchField}
              labelPlacement="start"
              label={t('HavingCar')}
            />
            <FilterFooter isResettable />
          </Form>
        )}
      />
    </Wrapper>
  );
});

export { Filter };
