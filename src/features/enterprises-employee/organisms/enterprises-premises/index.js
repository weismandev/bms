import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  LabeledContent,
  CloseButton,
  Loader,
  DeleteConfirmDialog,
  CustomScrollbar,
  DetailToolbar,
} from '../../../../ui';
import { EnterprisesPropertiesSelectField } from '../../../enterprises-properties-select';
import {
  $mode,
  changeMode,
  $isLoading,
  $error,
  changeDeleteData,
  confirm,
  $premises,
  PremisesGate,
  premiseSubmitted,
  $deleteData,
} from '../../model/premises.model';

const { t } = i18n;

const $stores = combine({
  mode: $mode,
  isLoading: $isLoading,
  error: $error,
  premises: $premises,
  deleteData: $deleteData,
});

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 12px 24px',
    height: 'calc( 100% - 85px)',
  },
  listInner: {
    paddingRight: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    position: 'relative',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
    '&:hover > svg': {
      opacity: 0.4,
    },
    '&:hover > svg:hover': {
      opacity: 1,
    },
  },
  part: {
    marginBottom: 20,
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  delete_btn: {
    position: 'absolute',
    opacity: 0,
    top: 6,
    right: 6,
    width: 20,
    height: 20,
  },

  formContainer: {
    padding: 24,
    paddingTop: 0,
    height: 'calc(100% - 82px)',
  },
  toolbar: { padding: 24, height: 82 },
  addBlock: { padding: 24, textAlign: 'center' },
});

const PremiseCard = (props) => {
  const { is_owner, title, id, building } = props;
  const { part, card, text, delete_btn } = useStyles();

  const isProperty = is_owner === t('Owner');

  return (
    <div className={card}>
      <CloseButton
        className={delete_btn}
        onClick={() => changeDeleteData({ isOpen: true, property_id: id })}
      />
      <LabeledContent className={part} label={t('Located')} labelPlacement="start">
        <span className={text}>{isProperty ? t('owned') : t('onLease')}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('Name')} labelPlacement="start">
        <span className={text}>{title}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('complex')} labelPlacement="start">
        <span className={text}>{building.complex_title}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('Label.address')} labelPlacement="start">
        <span className={text}>{building.building_title}</span>
      </LabeledContent>
    </div>
  );
};

const validationSchema = Yup.object().shape({
  premise: Yup.mixed().required(t('thisIsRequiredField')),
});

const EnterprisePremisesForm = (props) => {
  const { enterprise_id, premises } = props;
  const { formContainer, toolbar } = useStyles();

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{
        premise: '',
      }}
      onSubmit={premiseSubmitted}
      render={() => (
        <Form>
          <DetailToolbar
            onCancel={() => changeMode('view')}
            hidden={{ edit: true, delete: true, close: true }}
            className={toolbar}
            mode="edit"
          />
          <div className={formContainer}>
            <Field
              name="premise"
              component={EnterprisesPropertiesSelectField}
              {...{ enterprise_id, excludeIds: premises.map((e) => e.id) }}
              label={t('room')}
              placeholder={t('ChooseRoom')}
              mode="edit"
            />
          </div>
        </Form>
      )}
    />
  );
};

const EnterprisesPremises = ({ userdata_id, enterprise_id }) => {
  const { listOuter, listInner, addBlock } = useStyles();
  const { mode, isLoading, premises, deleteData } = useStore($stores);
  const form =
    mode === 'edit' ? <EnterprisePremisesForm {...{ enterprise_id, premises }} /> : null;

  const addNewButton =
    mode === 'view' ? (
      <div className={addBlock}>
        <Button onClick={() => changeMode('edit')} color="primary">
          {t('AddRoom')}
        </Button>
      </div>
    ) : null;

  const list =
    mode === 'view' ? (
      <div className={listOuter}>
        <CustomScrollbar autoHide>
          <div className={listInner}>
            {premises.map((item) => (
              <PremiseCard key={item.id} {...item} />
            ))}
          </div>
        </CustomScrollbar>
      </div>
    ) : null;
  return (
    <>
      <Loader isLoading={isLoading} />
      <DeleteConfirmDialog
        isOpen={deleteData.isOpen}
        confirm={confirm}
        close={() => changeDeleteData({ isOpen: false })}
        header={t('RemovingRoom')}
      />
      <PremisesGate {...{ userdata_id, enterprise_id }} />
      {addNewButton}
      {form}
      {list}
    </>
  );
};

export { EnterprisesPremises };
