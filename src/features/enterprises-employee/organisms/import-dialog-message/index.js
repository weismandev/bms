import i18n from '@shared/config/i18n';

const { t } = i18n;

function getImportDialogMessage(employees) {
  return (
    <div style={{ marginTop: 40, overflowY: 'auto', maxHeight: 500 }}>
      {employees.map((employee, index) => {
        return buildMessageLine(employee, index);
      })}
    </div>
  );
}

function buildMessageLine(employee, index) {
  const { surname, name, patronymic, error } = employee;

  const prefix = `${index + 1}) ${t('Employee')} `;
  const fullname = `${surname} ${name} ${patronymic} `;
  const status = error ? `${t('hasNotBeenImported')}. ` : `${t('successfullyImported')}.`;
  const statusColor = error ? '#eb5757' : '#1bb169';

  return (
    <p
      key={`${employee.personal_phone}${employee.vehicle_number}`}
      style={{ display: 'inline-block', fontSize: 14, marginBottom: 5 }}
    >
      <span>{prefix}</span>
      <span style={{ fontWeight: 'bold' }}>{fullname}</span>
      <span style={{ color: statusColor }}>{status}</span>
      {error ? <span>{error}</span> : null}
    </p>
  );
}

export { getImportDialogMessage };
