import { useState } from 'react';
import { useStore } from 'effector-react';
import {
  Wrapper,
  DetailToolbar,
  InputField,
  SelectField,
  SwitchField,
  Tabs,
  Divider,
  CustomScrollbar,
  FormSectionHeader,
  ConfirmDialog,
  CloseButton,
  PhoneMaskedInput,
  EmailMaskedInput,
} from '@ui';
import { Formik, Form, Field, FieldArray } from 'formik';
import { nanoid } from 'nanoid';
import * as Yup from 'yup';
import { Tab as MuiTab, Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { EnterprisesCompaniesSelectField } from '../../../enterprises-companies-select';
import { EmployeePass } from '../../../enterprises-pass';
import { ResetAndNotifyPassword } from '../../../reset-password';
import { VehiclesForm } from '../../../user-related-vehicles';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  changeVerifyState,
  $currentTab,
  selectTab,
  tabsConfig,
  selectEnterprise,
} from '../../model/detail.model';
import { deleteDialogVisibilityChanged } from '../../model/page.model';
import { ActionsToolbar } from '../../molecules';
import { EnterprisesParking } from '../enterprises-parking';
import { EnterprisesPremises } from '../enterprises-premises';

const useStyles = makeStyles({
  phone_card: {
    position: 'relative',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: 15,
  },
  delete_btn: {
    position: 'absolute',
    opacity: 0.4,
    top: -2,
    right: 6,
    width: 20,
    height: 20,
    '&:hover': {
      opacity: 0.8,
    },
  },
  key_container: {
    display: 'grid',
    gap: 10,
    marginBottom: 20,
  },
  key_card: {
    display: 'grid',
    gap: 10,
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    gridTemplateColumns: 'auto auto',
    background: '#edf6ff',
    padding: '10px 15px',
    borderRadius: 16,
  },
  key_title: {
    fontWeight: 'bold',
  },
});

const { t } = i18n;

const Detail = () => {
  const opened = useStore($opened);
  const currentTab = useStore($currentTab);
  const isNew = !opened.id;
  const [user_id, enterprise_id] = opened.id ? opened.id.split('-') : [];

  return (
    <Wrapper style={{ height: '100%', position: 'relative' }}>
      <Tabs
        options={tabsConfig}
        currentTab={currentTab}
        onChange={(e, value) => selectTab(value)}
        isNew={isNew}
        tabEl={<MuiTab />}
      />
      <div style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
        {currentTab === 'entity' && (
          <InfoForm enterprise_id={enterprise_id} opened={opened} />
        )}
        {currentTab === 'pass' && (
          <EmployeePass user_id={user_id} enterprise_id={enterprise_id} />
        )}
        {currentTab === 'parking' && (
          <EnterprisesParking employee_id={user_id} enterprise_id={enterprise_id} />
        )}
        {currentTab === 'premises' && (
          <EnterprisesPremises userdata_id={user_id} enterprise_id={enterprise_id} />
        )}

        {currentTab === 'vehicles' && (
          <VehiclesForm
            isCompany
            changeDetailVisibility={changedDetailVisibility}
            userdata_id={user_id}
          />
        )}
      </div>
    </Wrapper>
  );
};

const required = Yup.string().required(t('thisIsRequiredField'));
const validationSchema = Yup.object().shape({
  userdata: Yup.object().shape({
    phone: required.matches(/^\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/, {
      message: t('invalidNumber'),
      excludeEmptyString: true,
    }),
    surname: required,
    name: required,
    patronymic: Yup.string(),
  }),
  enterprise: Yup.mixed().required(t('thisIsRequiredField')),
  position: required,
  contacts: Yup.array().of(
    Yup.object().shape({
      sip: Yup.string().when('is_secretary', {
        is: true,
        then: Yup.string().required(t('TheSIPFieldIsRequiredForSecretary')),
      }),
      is_secretary: Yup.boolean(),
    })
  ),
});

const BasePermissionField = ({ name, label, ...props }) => {
  const mode = useStore($mode);

  return (
    <Field
      disabled={mode !== 'edit'}
      name={`${name}.value`}
      component={SwitchField}
      label={label}
      labelPlacement="start"
      {...props}
    />
  );
};

const PermissionsFieldArray = ({ values, mode }) => (
  <FieldArray
    name="permissions"
    render={() => {
      const issuePassGuestIx = values.permissions.findIndex(
        (permission) => permission.key === 'issue-a-pass-guest'
      );
      const issuePassWithVehicleIx = values.permissions.findIndex(
        (permission) => permission.key === 'issue-a-pass-with-vehicle'
      );

      return values.permissions.map((i, idx) => (
        <Field
          name={`permissions[${idx}]`}
          key={i.key}
          render={({ field, form }) => {
            const baseProps = {
              name: field.name,
              label: field.value.title,
            };

            return i.key === 'issue-a-pass-guest' ? (
              <BasePermissionField
                {...baseProps}
                onChange={(e) => {
                  form.setFieldValue(e.target.name, !field.value.value);
                  form.setFieldValue(
                    `permissions[${issuePassWithVehicleIx}].value`,
                    false
                  );
                }}
              />
            ) : i.key === 'issue-a-pass-with-vehicle' ? (
              <BasePermissionField
                {...baseProps}
                disabled={mode !== 'edit' || !values.permissions[issuePassGuestIx].value}
              />
            ) : (
              <BasePermissionField {...baseProps} />
            );
          }}
        />
      ));
    }}
  />
);

const InfoForm = ({ opened }) => {
  const mode = useStore($mode);
  const isNew = !opened.id;
  const showStateButton = !opened.is_verified && !isNew;
  const isEditMode = mode === 'edit';
  const [confirmDeleteContact, setConfirmDeleteContact] = useState(false);
  const classes = useStyles();

  const [user_id] = typeof opened.id === 'string' ? opened.id.split('-') : [null];

  const wasSecretary = (contact) => {
    const { id } = contact;
    if (!id) return false;

    const initialContact = opened.contacts.find((item) => item.id === id);

    return initialContact.is_secretary === true && contact.is_secretary === true;
  };

  return (
    <Formik
      initialValues={opened}
      enableReinitialize
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      validateOnBlur={false}
      render={({ resetForm, values }) => (
        <Form style={{ height: '100%' }}>
          <DetailToolbar
            onEdit={() => changeMode('edit')}
            onDelete={() => deleteDialogVisibilityChanged(true)}
            onCancel={() => {
              if (isNew) {
                changedDetailVisibility(false);
              } else {
                resetForm();
                changeMode('view');
              }
            }}
            onClose={() => changedDetailVisibility(false)}
            mode={mode}
            style={{ padding: 24 }}
          >
            {!isNew && user_id && (
              <ResetAndNotifyPassword context="enterprise" user_id={user_id} />
            )}
          </DetailToolbar>
          <div
            style={{
              padding: '0 12px 24px 24px',
              height: `calc(100% - 84px - ${showStateButton ? 58 : 0}px)`,
              overflow: 'auto',
            }}
          >
            <CustomScrollbar autoHide trackVerticalStyle={{ right: 0 }}>
              <div style={{ paddingRight: 12 }}>
                <FormSectionHeader
                  style={{ marginBottom: 15 }}
                  variant="h6"
                  header={t('ProfilePage.PersonalData')}
                />
                <Field
                  name="enterprise"
                  component={EnterprisesCompaniesSelectField}
                  label={t('company')}
                  required
                  placeholder={t('ChooseCompany')}
                  mode={mode}
                  onChange={(e) => {
                    selectEnterprise(e);
                  }}
                />
                <Field
                  name="userdata.surname"
                  component={InputField}
                  label={t('Label.lastname')}
                  placeholder={t('EnterLastName')}
                  mode={mode}
                  required
                />
                <Field
                  name="userdata.name"
                  component={InputField}
                  label={t('Label.name')}
                  placeholder={t('EnterYourName')}
                  mode={mode}
                  required
                />
                <Field
                  name="userdata.patronymic"
                  component={InputField}
                  label={t('Label.patronymic')}
                  placeholder={t('EnterMiddleName')}
                  mode={mode}
                />
                <Field
                  name="userdata.phone"
                  render={({ field, form }) => (
                    <InputField
                      field={field}
                      form={form}
                      required
                      inputComponent={PhoneMaskedInput}
                      label={t('Label.phone')}
                      placeholder={t('EnterPhone')}
                      mode={!isNew ? 'view' : mode}
                    />
                  )}
                />
                <Field
                  name="userdata.email"
                  render={({ field, form }) => (
                    <InputField
                      field={field}
                      form={form}
                      inputComponent={EmailMaskedInput}
                      label="Email"
                      placeholder={t('EnterEmail')}
                      mode={!isNew ? 'view' : mode}
                    />
                  )}
                />
                <Field
                  name="userdata.gender"
                  component={SelectField}
                  options={[
                    { id: 'nobody', title: t('isNotDefined') },
                    { id: 'male', title: t('Male') },
                    { id: 'female', title: t('Female') },
                  ]}
                  label={t('gender')}
                  placeholder={t('selectGender')}
                  mode={mode}
                />
                <Field
                  name="position"
                  component={InputField}
                  label={t('Label.Position')}
                  placeholder={t('EnterPosition')}
                  mode={mode}
                  required
                />

                {mode === 'view' && !values.contacts?.length ? null : (
                  <FormSectionHeader
                    style={{ marginBottom: 15 }}
                    variant="h6"
                    header={t('WorkingContacts')}
                  />
                )}
                <FieldArray
                  name="contacts"
                  render={(helpers) => {
                    const add = () =>
                      helpers.push({
                        _id: nanoid(3),
                        additional_phone: '',
                        sip: '',
                        cabinet: '',
                        comment: '',
                      });

                    const remove = (contact, idx) => {
                      if (wasSecretary(contact)) {
                        setConfirmDeleteContact(true);
                      } else {
                        helpers.remove(idx);
                      }
                    };

                    const handleDeleteContact = (idx) => {
                      setConfirmDeleteContact(false);
                      helpers.remove(idx);
                    };

                    const checkedIdSecretaryContact = values.contacts.find(
                      (item) => item.is_secretary === true
                    )?.id;

                    return (
                      <>
                        {values.contacts.map((contact, idx) => {
                          const isDisabledSecretatyField = checkedIdSecretaryContact
                            ? checkedIdSecretaryContact !== contact.id
                            : false;

                          return (
                            <div
                              key={contact.id || contact._id}
                              className={classes.phone_card}
                            >
                              <ConfirmDialog
                                isOpen={confirmDeleteContact}
                                close={() => setConfirmDeleteContact(false)}
                                content={`${t(
                                  'YouAreTryingDeleteOnlyContactWithAbility'
                                )} "${t('ReceiveSecretaryCalls')}". ${t(
                                  'AreYouSureYouWantDeleteContact'
                                )}`}
                                confirm={() => handleDeleteContact(idx)}
                              />

                              {isEditMode && (
                                <CloseButton
                                  className={classes.delete_btn}
                                  onClick={() => remove(contact, idx)}
                                />
                              )}

                              <Field
                                name={`contacts[${idx}].additional_phone`}
                                render={({ field, form }) => (
                                  <InputField
                                    field={field}
                                    form={form}
                                    inputComponent={PhoneMaskedInput}
                                    label={t('AdditionalPhone')}
                                    placeholder={t('EnterPhone')}
                                    mode={mode}
                                    divider={false}
                                  />
                                )}
                              />
                              <Field
                                name={`contacts[${idx}].sip`}
                                render={({ field, form }) => (
                                  <InputField
                                    field={field}
                                    form={form}
                                    inputComponent={PhoneMaskedInput}
                                    label={t('SIPNumber')}
                                    mode={mode}
                                    placeholder={t('EnterSIP')}
                                    divider={false}
                                  />
                                )}
                              />
                              <Field
                                component={InputField}
                                name={`contacts[${idx}].cabinet`}
                                label={t('Cabinet')}
                                mode={mode}
                                placeholder={t('EnterCabinetNumber')}
                                divider={false}
                              />
                              <Field
                                component={InputField}
                                name={`contacts[${idx}].comment`}
                                label={t('Label.comment')}
                                mode={mode}
                                placeholder={t('EnterComment')}
                                divider={false}
                              />
                              <Field
                                component={SwitchField}
                                name={`contacts[${idx}].is_secretary`}
                                label={t('ReceiveSecretaryCalls')}
                                disabled={!isEditMode || isDisabledSecretatyField}
                                labelPlacement="start"
                                divider
                              />
                            </div>
                          );
                        })}
                        {isEditMode && (
                          <>
                            <Button
                              style={{ marginBottom: 5 }}
                              variant="contained"
                              color="primary"
                              onClick={add}
                              fullWidth
                            >
                              {t('AddContact')}
                            </Button>
                            <Divider />
                          </>
                        )}
                      </>
                    );
                  }}
                />

                <FormSectionHeader
                  style={{ marginBottom: 15 }}
                  variant="h6"
                  header={t('AccessRights')}
                />
                {values.permissions.length === 0 ? (
                  <p style={{ fontSize: '13px', color: '#65657B' }}>
                    {t('ChooseCompany')}
                  </p>
                ) : (
                  <PermissionsFieldArray values={values} mode={mode} />
                )}
              </div>
            </CustomScrollbar>
          </div>
          {showStateButton && (
            <ActionsToolbar is_verified={opened.is_verified} action={changeVerifyState} />
          )}
        </Form>
      )}
    />
  );
};

export { Detail };
