import { useCallback, useMemo } from 'react';
import { useStore } from 'effector-react';
import {
  SortingLabel,
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  ItemStatus,
  TableToggle,
  VehiclePlate,
  TableColumnVisibility,
  PagingPanel,
} from '@ui';
import { Template } from '@devexpress/dx-react-core';
import {
  SortingState,
  PagingState,
  CustomPaging,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
  Toolbar,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import { $opened, open, changeVerifyState } from '../../model/detail.model';
import {
  $tableData,
  columns,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  $totalCount,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $unreadedEmployees,
  changeReadStatus,
  sortChanged,
  $sorting,
} from '../../model/table.model';
import { ActionsToolbar } from '../../molecules';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      minHeight: '36px',
      height: '60px',
      alignItems: 'flex-start',
      borderBottom: 'none',
      flexWrap: 'nowrap',
    }}
  />
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => open(id), []);

  const row = useMemo(
    () => <TableRow isSelected={isSelected} onRowClick={onRowClick} {...props} />,
    [isSelected, props.children]
  );
  return row;
};

const VerifiedFormatter = ({ row }) => (
  <ItemStatus is_verified={row.is_verified} verifiedText={t('Verified')} />
);

const VerifiedProvider = (props) => (
  <DataTypeProvider formatterComponent={VerifiedFormatter} {...props} />
);

const ActionsFormatter = ({ row }) => (
  <ActionsToolbar is_verified={row.is_verified} action={changeVerifyState} />
);

const ActionsProvider = (props) => (
  <DataTypeProvider formatterComponent={ActionsFormatter} {...props} />
);

const VerifiedStateFormatter = ({ value, row }) =>
  row.is_verified ? value : <b style={{ color: '#3B3B50' }}>{value}</b>;

const VerifiadStateProvider = (props) => (
  <DataTypeProvider formatterComponent={VerifiedStateFormatter} {...props} />
);

const VehicleFormatter = ({ value }) => {
  const vehiclesList = value.map(({ license_plate, brand }, idx) => {
    const licensePlate =
      Boolean(license_plate) && typeof license_plate === 'string' ? license_plate : ' - ';
    const brandName = Boolean(brand) && typeof brand === 'string' ? brand : '';

    return <VehiclePlate key={idx} number={licensePlate} brand={brandName} />;
  });

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'repeat(auto-fill, 1fr)',
        gridGap: 3,
        padding: 4,
      }}
    >
      {vehiclesList}
    </div>
  );
};

const VehicleProvider = (props) => (
  <DataTypeProvider formatterComponent={VehicleFormatter} {...props} />
);

const Table = () => {
  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($totalCount);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const unreadedEmployees = useStore($unreadedEmployees);
  const sortValue = useStore($sorting);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid rootComponent={Root} rows={data} getRowId={(row) => row.id} columns={columns}>
        <DragDropProvider />
        <SortingState sorting={sortValue} onSortingChange={sortChanged} />

        <VehicleProvider for={['vehicle']} />
        <VerifiedProvider for={['verified']} />
        <ActionsProvider for={['actions']} />
        <VerifiadStateProvider for={['fullname', 'phone', 'position', 'enterprise']} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />

        <DXTable
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableToggle active={unreadedEmployees} onToggle={changeReadStatus} />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        <TableHeaderRow
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel
              excludeSortColumns={[
                'phone',
                'position',
                'enterprise',
                'verified',
                'vehicle',
                'actions',
                'parking_slots',
                'entry_available',
              ]}
              {...props}
            />
          )}
        />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { Table };
