import { useTranslation } from 'react-i18next';
import { useList } from 'effector-react';
import makeStyles from '@mui/styles/makeStyles';
import { LabeledContent, CloseButton, CustomScrollbar } from '../../../../ui';
import { $slots, changeDeleteData } from '../../model/parking-slots.model';

const useStyles = makeStyles({
  listOuter: {
    padding: '0 12px 0 24px',
    height: '100%',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    position: 'relative',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
    '&:hover > svg': {
      opacity: 0.4,
    },
    '&:hover > svg:hover': {
      opacity: 1,
    },
  },
  part: {
    marginBottom: 20,
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  delete_btn: {
    position: 'absolute',
    opacity: 0,
    top: 6,
    right: 6,
    width: 20,
    height: 20,
  },
  cardTitleContainer: {
    display: 'flex',
    marginBottom: 30,
  },
  cardTitleText: {
    fontSize: 20,
    hyphens: 'manual',
  },
  cardTitleNumber: {
    display: 'flex',
    placeItems: 'center',
    backgroundColor: '#fff',
    padding: '2px 5px',
    borderRadius: 4,
    marginRight: 6,
  },
  cartTitleLocation: {
    display: 'flex',
    placeItems: 'center',
    backgroundColor: '#1bb169',
    color: '#fff',
    padding: '2px 5px',
    borderRadius: 4,
  },
});

const EnterprisesParkingList = () => {
  const { listOuter, listInner } = useStyles();
  const list = useList($slots, (pass) => <SlotCard {...pass} />);
  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
    </div>
  );
};

const SlotCard = (props) => {
  const { number, lot, zone, type, rent, rent_id } = props;
  const { t } = useTranslation();
  const { part, card, text, delete_btn } = useStyles();

  const isProperty = type === 'ownership';

  return (
    <div className={card}>
      <CloseButton
        className={delete_btn}
        onClick={() => changeDeleteData({ isOpen: true, rent_id })}
      />
      <SlotCardTitle number={number} lot={lot.title} zone={zone.title} />

      <LabeledContent className={part} label={t('Located')}>
        <span className={text}>{isProperty ? t('owned') : t('onLease')}</span>
      </LabeledContent>
      {!isProperty && rent && (
        <>
          <LabeledContent className={part} label={t('RentalStartDate')}>
            <span className={text}>{rent.starts_at}</span>
          </LabeledContent>
          <LabeledContent className={part} label={t('EndDateLease')}>
            <span className={text}>{rent.ends_at}</span>
          </LabeledContent>
        </>
      )}
    </div>
  );
};

const SlotCardTitle = ({ number = '-', lot, zone }) => {
  const { cardTitleContainer, cardTitleText, cardTitleNumber, cartTitleLocation } =
    useStyles();

  return (
    <div className={cardTitleContainer}>
      <div className={cardTitleNumber}>
        <span className={cardTitleText}>{number}</span>
      </div>
      {Boolean(lot) && (
        <div className={cartTitleLocation}>
          <span className={cardTitleText}>
            {lot}
            {zone ? ` : ${zone}` : ''}
          </span>
        </div>
      )}
    </div>
  );
};

export { EnterprisesParkingList };
