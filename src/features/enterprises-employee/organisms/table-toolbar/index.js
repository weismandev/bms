import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { AttachFile } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import {
  SearchInput,
  Toolbar,
  FoundInfo,
  Greedy,
  FilterButton,
  AddButton,
  ImportButton,
  SaveButton,
  CancelButton,
  FileControl,
  ActionIconButton,
  DownloadTemplateButton,
} from '../../../../ui';
import { EnterprisesCompaniesSelectField } from '../../../enterprises-companies-select';
import { addClicked, $mode } from '../../model/detail.model';
import { $isFilterOpen, changedFilterVisibility } from '../../model/filter.model';
import { importSubmitted } from '../../model/page.model';
import {
  $totalCount,
  searchChanged,
  $search,
  exportClicked,
} from '../../model/table.model';

const $stores = combine({
  totalCount: $totalCount,
  searchValue: $search,
  isFilterOpen: $isFilterOpen,
  mode: $mode,
});

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
});

function TableToolbar() {
  const { totalCount, searchValue, isFilterOpen, mode } = useStore($stores);

  const classes = useStyles();

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
        className={classes.margin}
      />
      <FoundInfo count={totalCount} className={classes.margin} />
      <SearchInput
        value={searchValue}
        onChange={(e) => searchChanged(e.target.value)}
        handleClear={() => searchChanged('')}
        className={classes.margin}
      />
      <Greedy />

      <DownloadTemplateButton onClick={exportClicked} className={classes.margin} />

      <ImportForm />

      <AddButton
        disabled={mode === 'edit'}
        onClick={addClicked}
        className={classes.margin}
      />
    </Toolbar>
  );
}

function ImportForm() {
  const { t } = useTranslation();
  const [isImportFormOpen, changeImportFormVisibility] = useState(false);

  return isImportFormOpen ? (
    <Formik
      initialValues={{ enterprise: '', import: '' }}
      onSubmit={(values) => {
        importSubmitted(values);
        changeImportFormVisibility(false);
      }}
      render={() => (
        <Form style={{ width: 500, display: 'flex', flexWrap: 'nowrap' }}>
          <div style={{ width: '80%', whiteSpace: 'nowrap' }}>
            <Field
              name="enterprise"
              label={null}
              labelPlacement="end"
              placeholder={t('ChooseCompany')}
              divider={false}
              component={EnterprisesCompaniesSelectField}
            />
          </div>
          <Field
            name="import"
            render={({ field, form }) => (
              <FileControl
                mode="edit"
                name={field.name}
                encoding="file"
                onChange={(value) => {
                  if (value) {
                    form.setFieldValue(field.name, value);
                  }
                }}
                renderPreview={() => null}
                renderButton={() => (
                  <ActionIconButton style={{ margin: '0 5px' }} component="span">
                    <AttachFile />
                  </ActionIconButton>
                )}
              />
            )}
          />
          <div style={{ marginLeft: 'auto', whiteSpace: 'nowrap' }}>
            <SaveButton type="submit" style={{ marginRight: 5 }} />
            <CancelButton onClick={() => changeImportFormVisibility(false)} />
          </div>
        </Form>
      )}
    />
  ) : (
    <ImportButton onClick={() => changeImportFormVisibility(true)} />
  );
}

export { TableToolbar };
