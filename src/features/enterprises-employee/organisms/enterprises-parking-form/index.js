import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { DetailToolbar, CustomScrollbar } from '../../../../ui';
import { RentedSlotForEnterpriseSelectField } from '../../../rented-slot-for-enterprise-select';
import { slotSubmitted, changeMode, $slots } from '../../model/parking-slots.model';

const { t } = i18n;

const requiredSelect = Yup.mixed().required(t('thisIsRequiredField'));

const validationSchema = Yup.object().shape({
  slot: requiredSelect,
});

const EnterprisesParkingForm = (props) => {
  const { enterprise_id } = props;
  const slots = useStore($slots);
  const assignedSlots = slots.map((i) => i.id);

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{
        slot: '',
      }}
      onSubmit={slotSubmitted}
      render={() => (
        <Form style={{ height: '100%' }}>
          <DetailToolbar
            onCancel={() => changeMode('view')}
            hidden={{ edit: true, delete: true, close: true }}
            style={{ padding: 24, height: 82 }}
            mode="edit"
          />
          <div
            style={{
              padding: 24,
              paddingRight: 12,
              paddingTop: 0,
              height: 'calc(100% - 82px)',
            }}
          >
            <CustomScrollbar>
              <div style={{ paddingRight: 12 }}>
                <Field
                  name="slot"
                  component={RentedSlotForEnterpriseSelectField}
                  enterprise_id={enterprise_id}
                  exclude={assignedSlots}
                  label={t('parkingSpace')}
                  placeholder={t('ChooseParkingSpace')}
                  required
                />
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};

export { EnterprisesParkingForm };
