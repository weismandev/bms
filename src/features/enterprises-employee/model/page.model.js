import { createEffect, createStore, createEvent, restore } from 'effector';
import { createGate } from 'effector-react';

const PageGate = createGate();

const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;

const fxGetList = createEffect();
const fxCreate = createEffect();
const fxUpdate = createEffect();
const fxDelete = createEffect();
const fxReject = createEffect();
const fxVerify = createEffect();
const fxImportEmployees = createEffect();
const fxMarkEmployeeAsRead = createEffect();
const fxGetKey = createEffect();

const deleteConfirmed = createEvent();
const deleteDialogVisibilityChanged = createEvent();
const errorDialogVisibilityChanged = createEvent();
const forceDialogVisibilityChanged = createEvent();
const importSubmitted = createEvent();
const forceConfirmed = createEvent();
const getKey = createEvent();

const $raw = createStore({ data: [], meta: { total: 0 } });
const $normalized = $raw.map(({ data }) =>
  data
    .filter((i) => Boolean(i.enterprise))
    .reduce((acc, i) => ({ ...acc, [`${i.id}-${i.enterprise.id}`]: { ...i } }), {})
);

const $isDeleteDialogOpen = restore(deleteDialogVisibilityChanged, false);
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = restore(errorDialogVisibilityChanged, false);
const $isForceDialogOpen = restore(forceDialogVisibilityChanged, false);
const $keys = createStore([]);

export {
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  fxReject,
  fxVerify,
  PageGate,
  pageMounted,
  pageUnmounted,
  $raw,
  $normalized,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
  fxImportEmployees,
  importSubmitted,
  fxMarkEmployeeAsRead,
  forceDialogVisibilityChanged,
  $isForceDialogOpen,
  forceConfirmed,
  fxGetKey,
  getKey,
  $keys,
};
