import { sample, forward, merge, split, guard } from 'effector';
import {
  signout,
  readNotifications,
  USER_VEHICLE_ADD,
  NEW_EMPLOYEE_ON_VERIFICATION,
} from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { enterprisesEmployeeApi } from '../api';
import {
  $isDetailOpen,
  addClicked,
  $mode,
  newEntity,
  $opened,
  open,
  detailClosed,
  entityApi,
  changeVerifyState,
  $currentTab,
  $tempData,
  selectEnterprise,
  $permissions,
  fxGetPermissions,
} from './detail.model';
import {
  $normalized,
  fxCreate,
  fxUpdate,
  fxDelete,
  deleteConfirmed,
  fxReject,
  fxVerify,
  forceConfirmed,
} from './page.model';

const verifyStateChanged = sample($opened, changeVerifyState, ({ id }, state) => {
  const [userdata_id, enterprise_id] = id.split('-');
  return { userdata_id, enterprise_id, state };
});

const verifyApi = split(verifyStateChanged, {
  accept: ({ state }) => state,
  decline: ({ state }) => !state,
});

$permissions
  .on(fxGetPermissions.doneData, (_, { permissions }) => permissions)
  .reset([signout, detailClosed]);

fxGetPermissions.use(enterprisesEmployeeApi.getPermissions);

$isDetailOpen
  .on([addClicked, open], () => true)
  .on(detailClosed, () => false)
  .reset(signout);

$mode
  .on(addClicked, () => 'edit')
  .on([open, detailClosed], () => 'view')
  .reset(signout);

$opened
  .on([addClicked, detailClosed], () => newEntity)
  .on(
    sample($normalized, open, (data, id) => data[id]),
    (state, opened) => formatOpened(opened)
  )
  .on(selectEnterprise, (state, data) => ({ ...state, enterprise: data }))
  .on($permissions.updates, (state, data) => ({ ...state, permissions: data }))
  .reset(signout);

$currentTab.reset([$opened.updates, signout]);

guard({
  source: selectEnterprise,
  filter: (data) => data?.id,
  target: fxGetPermissions.prepend(({ id }) => ({ enterprise_id: id })),
});

forward({ from: entityApi.create, to: fxCreate.prepend(formatPayload) });

forward({
  from: sample($opened, entityApi.update, (opened, payload) => ({
    ...payload,
  })),
  to: [fxUpdate.prepend(formatPayload), $tempData],
});

forward({
  from: sample($tempData, forceConfirmed),
  to: fxUpdate.prepend(addForceMode),
});

forward({
  from: sample($opened, deleteConfirmed, ({ id }) => {
    const [userdata_id, enterprise_id] = id.split('-');

    return { enterprise_id, userdata_id };
  }),
  to: fxDelete,
});

forward({
  from: sample({
    source: merge([fxCreate.done, fxUpdate.done, fxVerify.done]),
    fn: ({ result, params }) => {
      if (params.enterprise_id && params.userdata_id) {
        return params.userdata_id + '-' + params.enterprise_id;
      }

      return result.employee.userdata.id + '-' + params.enterprise_id;
    },
  }),
  to: open,
});

forward({
  from: [fxDelete.done, fxReject.done],
  to: detailClosed,
});

forward({
  from: verifyApi.accept.map(({ state, ...rest }) => rest),
  to: fxVerify,
});

forward({
  from: verifyApi.decline.map(({ state, ...rest }) => rest),
  to: fxReject,
});

function formatOpened(opened) {
  const {
    enterprise,
    surname,
    name,
    patronymic,
    gender,
    phone,
    email,
    position,
    permissions,
    is_verified,
    contacts,
    key,
  } = opened;

  const formatted = {
    enterprise: enterprise || '',
    userdata: {
      surname: surname || '',
      name: name || '',
      patronymic: patronymic || '',
      phone: phone ? formatPhone(String(phone)) : '',
      email: email || '',
      gender: gender || 'nobody',
    },
    position: position || '',
    permissions: Array.isArray(permissions) ? permissions : [],
    contacts: Array.isArray(contacts)
      ? contacts.map(({ additional_phone, cabinet, comment, id, sip, is_secretary }) => ({
          id,
          additional_phone: additional_phone ? formatPhone(additional_phone) : '',
          sip: sip ? formatPhone(sip) : '',
          cabinet: cabinet || '',
          comment: comment || '',
          is_secretary,
        }))
      : [],
    is_verified,
    key,
  };

  if (opened.id) {
    formatted.id = `${opened.id}-${opened.enterprise.id}`;
  }

  return formatted;
}

function formatPayload(data) {
  const { id, userdata, enterprise, position, permissions, contacts } = data;
  const { gender, ...restUserdata } = userdata;

  const payload = {
    userdata: {
      ...restUserdata,
      gender: gender ? (typeof gender === 'object' ? gender.id : gender) : 'nobody',
    },
    enterprise_id: enterprise
      ? typeof enterprise === 'object'
        ? enterprise.id
        : enterprise
      : '',
  };

  if (id) {
    payload.userdata_id = id.split('-')[0];
  }

  if (position) {
    payload.position = { title: position };
  }

  if (Array.isArray(permissions) && permissions.length > 0) {
    payload.permissions = permissions.filter((i) => i.value).map((i) => i.key);
  }

  if (Array.isArray(contacts) && contacts.length > 0) {
    payload.contacts = contacts.map(({ _id, id, ...contact }) => {
      const data = { ...contact };

      if (id) {
        data.contact_id = id;
      }

      if (payload.userdata_id) {
        data.userdata_id = payload.userdata_id;
      }

      data.is_secretary = data.is_secretary ? 1 : 0;

      return data;
    });
  }

  return payload;
}

function addForceMode(object) {
  const formattedObject = formatPayload(object);

  return { ...formattedObject, force_mode: 1 };
}

guard({
  source: $opened,
  filter: ({ id }) => Boolean(id),
  target: readNotifications.prepend(({ id }) => ({
    id,
    event: USER_VEHICLE_ADD,
  })),
});

guard({
  source: $opened,
  filter: ({ id }) => Boolean(id),
  target: readNotifications.prepend(({ id }) => ({
    id,
    event: NEW_EMPLOYEE_ON_VERIFICATION,
  })),
});
