import { guard, sample, merge } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';
import { signout } from '../../common';
import { enterprisesEmployeeApi } from '../api';
import {
  fxGetPremises,
  PremisesGate,
  fxBindPremise,
  fxUnbindPremise,
  premiseSubmitted,
  $premises,
  $isLoading,
  $error,
  $mode,
  $deleteData,
  confirm,
} from './premises.model';

fxGetPremises.use(enterprisesEmployeeApi.getPremises);
fxBindPremise.use(enterprisesEmployeeApi.bindPremise);
fxUnbindPremise.use(enterprisesEmployeeApi.unbindPremise);

sample({
  source: PremisesGate.state,
  clock: merge([PremisesGate.state, fxBindPremise.doneData, fxUnbindPremise.doneData]),
  target: fxGetPremises,
});

sample({
  source: PremisesGate.state,
  clock: premiseSubmitted,
  fn: (state, payload) => ({
    ...state,
    property_id: payload.premise.id,
  }),
  target: fxBindPremise,
});

sample({
  source: { state: PremisesGate.state, deleteData: $deleteData },
  clock: confirm,
  fn: ({ state, deleteData: { property_id } }) => ({
    ...state,
    property_id,
  }),
  target: fxUnbindPremise,
});

$premises
  .on(fxGetPremises.doneData, (_, { objects }) => {
    return Array.isArray(objects) ? objects : [];
  })
  .reset(PremisesGate.state);

$mode.on(fxBindPremise.done, () => 'view').reset(signout);

$isLoading
  .on(
    pending({
      effects: [fxGetPremises, fxBindPremise, fxUnbindPremise],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [fxGetPremises.failData, fxBindPremise.failData, fxUnbindPremise.failData],
    (state, error) => error
  )
  .reset([newRequestStarted, signout]);

$deleteData.reset([signout, fxUnbindPremise.done]);
