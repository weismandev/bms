import { forward, attach, sample, guard, merge } from 'effector';
import i18n from '@shared/config/i18n';
import parseError from '@tools/parseError';
import { pick } from '../../../tools/pick';
import { signout } from '../../common';
import { findById as findCompanyById } from '../../enterprises-companies-select';
import { fxGetEmployeePassesList, fxCreateEmployeePass } from '../../enterprises-pass';
import { enterprisesEmployeeApi } from '../api';
import { fxGetPermissions } from './detail.model';
import { $filters } from './filter.model';
import {
  fxGetList,
  fxCreate,
  fxUpdate,
  fxDelete,
  fxReject,
  fxVerify,
  pageMounted,
  $raw,
  deleteConfirmed,
  $isDeleteDialogOpen,
  $isLoading,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,
  fxImportEmployees,
  importSubmitted,
  fxMarkEmployeeAsRead,
  forceDialogVisibilityChanged,
  $isForceDialogOpen,
  forceConfirmed,
  fxGetKey,
  getKey,
  $keys,
} from './page.model';
import { $tableParams } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

fxGetList.use(enterprisesEmployeeApi.getList);
fxCreate.use(enterprisesEmployeeApi.create);
fxUpdate.use(enterprisesEmployeeApi.update);
fxDelete.use(enterprisesEmployeeApi.delete);
fxReject.use(enterprisesEmployeeApi.reject);
fxVerify.use(enterprisesEmployeeApi.verify);
fxImportEmployees.use(enterprisesEmployeeApi.importEmployees);
fxMarkEmployeeAsRead.use(enterprisesEmployeeApi.markEmployeeAsRead);
fxGetKey.use(enterprisesEmployeeApi.getKey);

const { t } = i18n;

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

const gotError = merge([
  fxGetList.fail,
  fxCreate.fail,
  fxDelete.fail,
  fxUpdate.fail,
  fxReject.fail,
  fxVerify.fail,
  fxImportEmployees.fail,
  fxGetEmployeePassesList.fail,
  fxCreateEmployeePass.fail,
  fxMarkEmployeeAsRead.fail,
  fxGetPermissions.fail,
  fxGetKey.fail,
]);

const closeError = guard({
  source: errorDialogVisibilityChanged,
  filter: (bool) => !bool,
});

const closeForce = guard({
  source: forceDialogVisibilityChanged,
  filter: (bool) => !bool,
});

$raw
  .on(
    fxGetList.doneData,
    (state, { employees = [], meta = { total: employees.length } }) => {
      employees?.map((item) => {
        // return getKey(item.id);
        return item;
      });

      return {
        meta,
        data: employees,
      };
    }
  )
  // .on(fxGetKey.done, (state, { params, result }) => {
  //   return {
  //     ...state,
  //     data: state.data.map((item) => {
  //       if (item.id === params.user_id) return { ...item, key: result.key };
  //       else return item;
  //     }),
  //   };
  // })
  .on(fxCreate.done, formatChange)
  .on(fxUpdate.done, formatChange)
  .on([fxDelete.done, fxReject.done], handleDelete)
  .on(fxVerify.done, handleVerify)
  .on(fxMarkEmployeeAsRead.done, handleRead)
  .reset(signout);

$isDeleteDialogOpen.on(deleteConfirmed, () => false).reset(signout);

$error.on(gotError, (state, { error }) => error).reset([signout, closeError, closeForce]);

$isErrorDialogOpen
  .on(
    gotError,
    (state, { error }) =>
      typeof parseError(error).content.data.force_mode_required == 'undefined'
  )
  .reset(signout);

$isForceDialogOpen
  .on(gotError, (state, { error }) => {
    const errorObject = parseError(error);
    return errorObject.content.data.force_mode_required;
  })
  .on(forceConfirmed, () => false)
  .reset(signout);

$isLoading.reset(signout);

sample({
  source: [
    fxCreate.pending,
    fxUpdate.pending,
    fxDelete.pending,
    fxGetList.pending,
    fxReject.pending,
    fxVerify.pending,
    fxImportEmployees.pending,
    fxGetEmployeePassesList.pending,
    fxCreateEmployeePass.pending,
    fxMarkEmployeeAsRead.pending,
    fxGetPermissions.pending,
    fxGetKey.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxImportEmployees.done,
    fxUpdate.done,
    $settingsInitialized,
  ],
  source: {
    table: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ table, filters }) => {
    const { search, sorting, ...restTableParams } = table;

    const payload = { ...restTableParams };

    if (search) {
      payload.search = search;
    }

    if (Array.isArray(filters.groups) && filters.groups.length) {
      payload.groups = filters.groups.map((i) => i.id);
    }

    if (Array.isArray(filters.enterprises) && filters.enterprises.length) {
      payload.enterprises = filters.enterprises.map((i) => i.id);
    }
    if (sorting) {
      const firstSort = sorting[0];
      payload.order = firstSort ? { key: firstSort.name, order: firstSort.order } : [];
    }
    if ('isset_vehicle' in filters) {
      payload.isset_vehicle = Number(filters.isset_vehicle);
    }

    return payload;
  },
  target: fxGetList,
});

// forward({
//   from: getKey,
//   to: fxGetKey.prepend((id) => {
//     return { type: 'universal', use_bms: 1, user_id: id };
//   }),
// });

forward({
  from: importSubmitted.map((payload) => ({
    enterprise_id: payload.enterprise ? payload.enterprise.id : null,
    import: payload.import ? payload.import.file : null,
  })),
  to: fxImportEmployees,
});

function formatChange({ data, meta }, { result, params }) {
  const { employee } = result;

  const op = params.userdata_id ? 'update' : 'create';

  const change = {
    ...pick(
      ['name', 'surname', 'patronymic', 'phone', 'id', 'fullname'],
      employee.userdata
    ),
    gender:
      employee.userdata.gender === t('male')
        ? 'male'
        : employee.userdata.gender === t('female')
        ? 'female'
        : 'nobody',
    position: employee.position ? employee.position.title : '',
    property_id: employee.property_id || '',
    is_verified: Boolean(employee.is_verified),
    enterprise: findCompanyById(params.enterprise_id) || '',
    permissions: Array.isArray(employee.permissions) ? employee.permissions : [],
    contacts: Array.isArray(employee.contacts) ? employee.contacts : [],
  };

  let idx = -1;

  const newState = {
    meta: { ...meta, total: op === 'update' ? meta.total : meta.total + 1 },
    data:
      op === 'create'
        ? [...data, change]
        : (idx = data.findIndex(
            (i) =>
              String(i.id) === String(params.userdata_id) &&
              String(i.enterprise.id) === String(params.enterprise_id)
          )) && idx > -1
        ? data
            .slice(0, idx)
            .concat(change)
            .concat(data.slice(idx + 1))
        : data,
  };

  return newState;
}

function handleDelete({ data, meta }, { params }) {
  return {
    meta: { ...meta, total: meta.total - 1 },
    data: data.filter((i) => String(i.id) !== String(params.userdata_id)),
  };
}

function handleVerify({ data, meta }, { params }) {
  const changedIdx = data.findIndex(
    (i) =>
      String(i.id) === String(params.userdata_id) &&
      String(i.enterprise.id) === String(params.enterprise_id)
  );

  return {
    meta: { ...meta },
    data:
      changedIdx > -1
        ? data
            .slice(0, changedIdx)
            .concat({ ...data[changedIdx], is_verified: true })
            .concat(data.slice(changedIdx + 1))
        : [...data],
  };
}

function handleRead({ data, meta }, { result, params }) {
  const changedIdx = data.findIndex((empl) => String(empl.id) === String(params.id));

  if (changedIdx > -1) {
    return {
      meta,
      data: [
        ...data.slice(0, changedIdx),
        result.employee,
        ...data.slice(changedIdx + 1),
      ],
    };
  }

  return { data, meta };
}
