import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';
import { $opened } from './detail.model';

const PremisesGate = createGate();

const changeMode = createEvent();
const changeDeleteData = createEvent();
const confirm = createEvent();
const premiseSubmitted = createEvent();
const fxGetPremises = createEffect();
const fxUnbindPremise = createEffect();
const fxBindPremise = createEffect();

const $premises = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);
const $mode = restore(changeMode, 'view');
const $deleteData = restore(changeDeleteData, { isOpen: false });

export {
  PremisesGate,
  changeMode,
  $premises,
  $isLoading,
  $error,
  $mode,
  changeDeleteData,
  $deleteData,
  confirm,
  fxGetPremises,
  fxUnbindPremise,
  fxBindPremise,
  premiseSubmitted,
};
