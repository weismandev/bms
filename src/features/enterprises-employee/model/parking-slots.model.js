import { createEvent, createEffect, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

const ParkingSlotsGate = createGate();

const changeMode = createEvent();
const slotSubmitted = createEvent();
const changeDeleteData = createEvent();
const confirm = createEvent();

const fxGetParkingSlots = createEffect();
const fxAddParkingSlot = createEffect();
const fxDeleteParkingSlot = createEffect();

const $slots = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);
const $mode = restore(changeMode, 'view');
const $deleteData = restore(changeDeleteData, { isOpen: false });

export {
  fxGetParkingSlots,
  fxAddParkingSlot,
  fxDeleteParkingSlot,
  ParkingSlotsGate,
  changeMode,
  slotSubmitted,
  $slots,
  $isLoading,
  $error,
  $mode,
  changeDeleteData,
  $deleteData,
  confirm,
};
