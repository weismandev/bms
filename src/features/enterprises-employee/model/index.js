import './detail.init';
import './filter.init';
import './import-dialog.init';
import './page.init';
import './parking-slots.init';
import './premises.init';
import './table.init';
