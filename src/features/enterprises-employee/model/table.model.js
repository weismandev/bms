import { createEvent } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { formatPhone } from '../../../tools/formatPhone';
import { $raw } from './page.model';

const { t } = i18n;

const columns = [
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'email', title: 'Email' },
  { name: 'position', title: t('Label.Position') },
  { name: 'enterprise', title: t('company') },
  { name: 'verified', title: t('Label.status') },
  { name: 'vehicle', title: t('Car') },
  { name: 'parking_slots', title: t('parkingLot') },
  { name: 'entry_available', title: t('RightToEnter') },
  { name: 'actions', title: t('Label.actions') },
];

const exportClicked = createEvent();
const changeReadStatus = createEvent();

const $totalCount = $raw.map(({ meta }) => meta.total);

const $tableData = $raw.map(({ data }) => formatTableData(data));

const $unreadedEmployees = $raw.map(({ data = [] }) =>
  data
    .filter((employee) => !employee.is_read)
    .map((employee) => `${employee.id}-${employee.enterprise.id}`)
);

function formatTableData(data = []) {
  return data
    .filter((i) => Boolean(i.enterprise))
    .map(
      ({
        id,
        enterprise,
        fullname,
        phone,
        email,
        position,
        is_verified,
        vehicle,
        parking_slots,
        entry_available,
      }) => {
        return {
          id: `${id}-${enterprise.id}`,
          fullname: fullname || t('isNotDefinedit'),
          phone: phone ? formatPhone(phone) : t('isNotDefined'),
          email: email || t('isNotDefined'),
          parking_slots:
            parking_slots && parking_slots.length
              ? parking_slots.map((e) => e.number).join(', ')
              : t('no'),
          entry_available: entry_available ? t('yes') : t('no'),
          position: position || t('isNotDefinedshe'),
          enterprise: (enterprise && enterprise.title) || t('isNotDefinedshe'),
          is_verified: Boolean(is_verified),
          vehicle: Array.isArray(vehicle) ? vehicle : [],
        };
      }
    );
}

const config = {
  widths: [
    { columnName: 'fullname', width: 300 },
    { columnName: 'phone', width: 200 },
    { columnName: 'email', width: 300 },
    { columnName: 'parking_slots', width: 150 },
    { columnName: 'entry_available', width: 150 },
    { columnName: 'position', width: 250 },
    { columnName: 'enterprise', width: 250 },
    { columnName: 'verified', width: 250 },
    { columnName: 'vehicle', width: 350 },
    { columnName: 'actions', width: 266 },
  ],
};

const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, config);

export {
  sortChanged,
  $sorting,
  columns,
  $tableData,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  pageNumChanged,
  pageSizeChanged,
  $currentPage,
  $pageSize,
  $totalCount,
  $tableParams,
  searchChanged,
  $search,
  hiddenColumnChanged,
  $hiddenColumnNames,
  exportClicked,
  changeReadStatus,
  $unreadedEmployees,
};
