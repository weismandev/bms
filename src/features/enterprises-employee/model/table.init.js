import { sample, guard, forward } from 'effector';
import FileSaver from 'file-saver';
import i18n from '@shared/config/i18n';
import { signout, readNotifications, NEW_EMPLOYEE_ON_VERIFICATION } from '../../common';
import { open } from './detail.model';
import { $filters } from './filter.model';
import { fxMarkEmployeeAsRead } from './page.model';
import {
  searchChanged,
  $currentPage,
  $columnWidths,
  $columnOrder,
  $pageSize,
  $search,
  $hiddenColumnNames,
  exportClicked,
  changeReadStatus,
  $unreadedEmployees,
} from './table.model';

const { t } = i18n;

$currentPage.on([searchChanged, $filters.updates], () => 0).reset(signout);

$columnWidths.reset(signout);
$columnOrder.reset(signout);
$pageSize.reset(signout);
$search.reset(signout);
$hiddenColumnNames.reset(signout);

const markedAsRead = guard({
  source: sample({
    source: $unreadedEmployees,
    clock: changeReadStatus,
    fn: (employees, readed_id) =>
      employees.includes(readed_id) && {
        id: readed_id.split('-')[0],
        enterprise_id: readed_id.split('-')[1],
      },
  }),
  filter: Boolean,
});

forward({
  from: markedAsRead,
  to: fxMarkEmployeeAsRead,
});

forward({
  from: markedAsRead,
  to: readNotifications.prepend(({ id }) => ({
    id,
    event: NEW_EMPLOYEE_ON_VERIFICATION,
  })),
});

forward({
  from: open,
  to: changeReadStatus,
});

exportClicked.watch(() => {
  FileSaver.saveAs(
    'https://files.mysmartflat.ru/crm/templates/employees_import_template.xlsx',
    t('ImportTemplate')
  );
});
