import { guard, sample } from 'effector';
import { pending } from 'patronum/pending';
import format from 'date-fns/format';
import { signout } from '../../common';
import { enterprisesEmployeeApi } from '../api';
import {
  fxGetParkingSlots,
  fxAddParkingSlot,
  fxDeleteParkingSlot,
  ParkingSlotsGate,
  slotSubmitted,
  $slots,
  $isLoading,
  $error,
  $mode,
  $deleteData,
  confirm,
} from './parking-slots.model';

const isEmployeeIdDefined = ({ employee_id }) =>
  Boolean(employee_id) || employee_id === 0;

const formatPayload = (payload) => {
  const { slot, employee_id, enterprise_id } = payload;

  return {
    rent_id: slot.rent_id,
    employee_id,
    enterprise_id,
  };
};

const getFormattedDateOrEmptyString = (date) => {
  if (date) {
    return format(new Date(date), 'dd.MM.yyyy');
  }

  return '';
};

const formatSlot = ({ rent, ...rest }) => {
  return {
    ...rest,
    rent: rent
      ? {
          starts_at: getFormattedDateOrEmptyString(rent.starts_at),
          ends_at: getFormattedDateOrEmptyString(rent.ends_at),
        }
      : null,
  };
};

fxGetParkingSlots.use(enterprisesEmployeeApi.getParkingSlots);
fxAddParkingSlot.use(enterprisesEmployeeApi.addSlotToEmployee);
fxDeleteParkingSlot.use(enterprisesEmployeeApi.removeEmployeeFromSlot);

guard({
  source: ParkingSlotsGate.state,
  filter: isEmployeeIdDefined,
  target: fxGetParkingSlots,
});

guard({
  source: sample(
    ParkingSlotsGate.state,
    slotSubmitted,
    ({ employee_id, enterprise_id }, payload) => ({
      employee_id,
      enterprise_id,
      ...payload,
    })
  ),
  filter: isEmployeeIdDefined,
  target: fxAddParkingSlot.prepend(formatPayload),
});

guard({
  source: sample({
    source: { gate: ParkingSlotsGate.state, deleteData: $deleteData },
    clock: confirm,
    fn: ({ gate, deleteData }) => ({
      employee_id: gate.employee_id,
      rent_id: deleteData.rent_id,
    }),
  }),
  filter: isEmployeeIdDefined,
  target: fxDeleteParkingSlot,
});

$slots
  .on(
    fxGetParkingSlots.doneData,
    (_, { slots }) => Array.isArray(slots) && slots.map(formatSlot)
  )
  .on(fxAddParkingSlot.doneData, (state, { slot }) =>
    [].concat(formatSlot(slot)).concat(state)
  )
  .on(fxDeleteParkingSlot.done, (state, { params }) =>
    state.filter((i) => i.rent_id !== params.rent_id)
  )
  .reset(signout);

$mode.on(fxAddParkingSlot.done, () => 'view').reset(signout);

$isLoading
  .on(
    pending({
      effects: [fxGetParkingSlots, fxAddParkingSlot, fxDeleteParkingSlot],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [fxGetParkingSlots.failData, fxAddParkingSlot.failData, fxDeleteParkingSlot.failData],
    (state, error) => error
  )
  .reset([newRequestStarted, signout]);

$deleteData.reset([signout, fxDeleteParkingSlot.done]);
