import { split, forward } from 'effector';
import { CheckCircle, Error, Warning } from '@mui/icons-material';
import i18n from '@shared/config/i18n';
import { changeNotification } from '../../colored-text-icon-notification';
import { getImportDialogMessage } from '../organisms';
import { fxImportEmployees } from './page.model';

forward({
  from: fxImportEmployees.fail,
  to: changeNotification.prepend(() => ({
    isOpen: false,
  })),
});

const { t } = i18n;

const { error, warning, successful } = split(fxImportEmployees.done, {
  error: ({ result }) => result.every((employee) => employee.error),
  warning: ({ result }) => result.some((employee) => employee.error),
  successful: ({ result }) => result.every((employee) => !employee.error),
});

forward({
  from: error,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#eb5757',
    text: t('ErrorWhileImportingEmployees'),
    message: getImportDialogMessage(result),
    Icon: Error,
  })),
});

forward({
  from: warning,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#ff9966',
    text: t('SomeEmployeeswereNotImported'),
    message: getImportDialogMessage(result),
    Icon: Warning,
  })),
});

forward({
  from: successful,
  to: changeNotification.prepend(({ result }) => ({
    isOpen: true,
    color: '#1bb169',
    text: t('EmployeesSuccessfullyImported'),
    message: getImportDialogMessage(result),
    Icon: CheckCircle,
  })),
});
