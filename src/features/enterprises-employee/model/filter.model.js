import { createEvent, restore } from 'effector';

const defaultFilters = {
  enterprises: '',
  groups: '',
  isset_vehicle: false,
};

const changedFilterVisibility = createEvent();
const filtersSubmitted = createEvent();

const $isFilterOpen = restore(changedFilterVisibility, false);
const $filters = restore(filtersSubmitted, defaultFilters);

export {
  changedFilterVisibility,
  filtersSubmitted,
  $isFilterOpen,
  $filters,
  defaultFilters,
};
