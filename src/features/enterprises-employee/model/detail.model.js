import { createEvent, createStore, restore, guard, split, createEffect } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const newEntity = {
  id: null,
  enterprise: '',
  userdata: {
    surname: '',
    name: '',
    patronymic: '',
    gender: '',
    email: '',
    phone: '',
  },
  position: '',
  permissions: [],
  contacts: [],
  is_verified: false,
  rent: '',
};

const tabsConfig = [
  {
    label: t('information'),
    value: 'entity',
    onCreateIsDisabled: false,
  },
  {
    label: t('Premises'),
    value: 'premises',
    onCreateIsDisabled: true,
  },
  {
    label: t('navMenu.security.passes'),
    value: 'pass',
    onCreateIsDisabled: true,
  },
  {
    label: t('vhcl'),
    value: 'vehicles',
    onCreateIsDisabled: true,
  },
  {
    label: t('navMenu.enterprises-parking'),
    value: 'parking',
    onCreateIsDisabled: true,
  },
];

const changedDetailVisibility = createEvent();
const addClicked = createEvent();
const changeMode = createEvent();
const open = createEvent();
const detailSubmitted = createEvent();
const changeVerifyState = createEvent();
const selectTab = createEvent();
const selectEnterprise = createEvent();

const fxGetPermissions = createEffect();

const detailClosed = guard({
  source: changedDetailVisibility,
  filter: (bool) => !bool,
});

const entityApi = split(detailSubmitted, {
  create: ({ id }) => !id,
  update: ({ id }) => Boolean(id),
});

const $isDetailOpen = restore(changedDetailVisibility, false);
const $mode = restore(changeMode, 'view');
const $opened = createStore(newEntity);
const $currentTab = restore(selectTab, 'entity');
const $tempData = createStore(newEntity);

const $permissions = createStore([], {
  updateFilter: (update, current) => {
    const currentKeys = current.map((item) => item.key);
    const updateKeys = update.map((item) => item.key);
    return JSON.stringify(currentKeys) !== JSON.stringify(updateKeys);
  },
});

export {
  changedDetailVisibility,
  $isDetailOpen,
  addClicked,
  changeMode,
  $mode,
  newEntity,
  $opened,
  open,
  detailClosed,
  detailSubmitted,
  entityApi,
  changeVerifyState,
  tabsConfig,
  selectTab,
  $currentTab,
  $tempData,
  $permissions,
  selectEnterprise,
  fxGetPermissions,
};
