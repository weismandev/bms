import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  DeleteConfirmDialog,
  ConfirmDialog,
  Loader,
  ErrorMessage,
} from '../../../ui';
import { ColoredTextIconNotification } from '../../colored-text-icon-notification';
import { HaveSectionAccess } from '../../common';
import { $isDetailOpen } from '../model/detail.model';
import { $isFilterOpen } from '../model/filter.model';
import {
  PageGate,
  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteConfirmed,
  $isLoading,
  errorDialogVisibilityChanged,
  $error,
  $isErrorDialogOpen,
  forceDialogVisibilityChanged,
  $isForceDialogOpen,
  forceConfirmed,
} from '../model/page.model';
import { Table, Detail, Filter } from '../organisms';

const EnterprisesEmployee = () => {
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isForceDialogOpen = useStore($isForceDialogOpen);
  const error = useStore($error);

  const { t } = useTranslation();

  return (
    <>
      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <ColoredTextIconNotification />

      <PageGate />
      <Loader isLoading={isLoading} />
      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />
      <ConfirmDialog
        isOpen={isForceDialogOpen}
        close={() => forceDialogVisibilityChanged(false)}
        content={t('ThereIsAlreadyEmployeeWithAbilityReceiveCallsFromSecretaryReplaceit')}
        confirm={forceConfirmed}
      />
      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedEnterprisesEmployee = () => (
  <HaveSectionAccess>
    <EnterprisesEmployee />
  </HaveSectionAccess>
);

export { RestrictedEnterprisesEmployee as EnterprisesEmployee };
