import { api } from '../../../api/api2';

const getList = (payload) =>
  api.v1('post', 'enterprises/crm/resident/employees/list', payload);

const getPermissions = ({ enterprise_id }) =>
  api.v1('get', 'enterprises/crm/resident/permissions', {
    enterprise_id,
  });

const setPermissions = (payload) =>
  api.v1('post', 'enterprises/employees/set-permissions', payload);

const create = async (payload) => {
  const { rent = {}, permissions = [], ...restPayload } = payload;

  const {
    employee: { userdata },
  } = await api.v1('post', 'enterprises/employees/create', restPayload);

  const permissionPayload = {
    enterprise_id: payload.enterprise_id,
    userdata_id: userdata.id,
    permissions,
  };

  let res = await setPermissions(permissionPayload);

  res = {
    employee: {
      ...res.employee,
      contacts: res.employee.enterprise_contacts,
    },
  };

  return res;
};

const update = async (payload) => {
  const { rent, remove_property_rent, userdata_id, enterprise_id, ...restPayload } =
    payload;

  const employeePromise = api.v1('post', 'enterprises/employees/update', {
    userdata_id,
    enterprise_id,
    ...restPayload,
  });

  return Promise.all([employeePromise]).then(([{ employee = {} }]) => ({
    employee: {
      ...employee,
      contacts: employee.enterprise_contacts,
      property_id: '',
    },
  }));
};

const deleteItem = (payload) => api.v1('post', 'enterprises/employees/remove', payload);

const reject = (payload) => api.v1('post', 'enterprises/employees/reject', payload);

const verify = (payload) => api.v1('post', 'enterprises/employees/verify', payload);

const importEmployees = (payload) =>
  api.v1('post', 'enterprises/employees/import', payload);

const getParkingSlots = (payload) =>
  api.v1('get', 'parking/enterprise_rent/slots-for-employee', payload);
const addSlotToEmployee = (payload) =>
  api.v1('post', 'parking/enterprise_rent/add-slot-to-employee', payload);
const removeEmployeeFromSlot = (payload) =>
  api.v1('post', 'parking/enterprise_rent/remove-employee-from-slot', payload);

const markEmployeeAsRead = (payload) =>
  api.v1('post', 'enterprises/crm/resident/employees/mark-read', payload);

const getPremises = (payload) =>
  api.v1('get', 'enterprises/get-user-related-properties', payload);
const unbindPremise = (payload) =>
  api.v1('post', 'enterprises/detach-user-from-property', payload);
const bindPremise = (payload) =>
  api.v1('post', 'enterprises/crm/attach-user-to-property', payload);
const getKey = (payload) => api.no_vers('post', 'acms/get-key', payload);

const enterprisesEmployeeApi = {
  getList,
  create,
  update,
  delete: deleteItem,
  reject,
  verify,
  importEmployees,
  getParkingSlots,
  addSlotToEmployee,
  removeEmployeeFromSlot,
  markEmployeeAsRead,
  getPremises,
  unbindPremise,
  bindPremise,
  getPermissions,
  getKey,
};

export { enterprisesEmployeeApi };
