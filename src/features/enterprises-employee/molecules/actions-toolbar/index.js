import { useTranslation } from 'react-i18next';
import { Check, Close } from '@mui/icons-material';
import { ActionButton } from '../../../../ui';

export const ActionsToolbar = ({ is_verified, action }) => {
  const { t } = useTranslation();
  const margin = { marginRight: 5 };
  const padding = { paddingRight: 16 };

  return is_verified ? null : (
    <div
      style={{
        width: 245,
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
        margin: '0 auto',
      }}
    >
      <ActionButton style={padding} kind="positive" onClick={() => action(true)}>
        <Check style={margin} />
        {t('Accept')}
      </ActionButton>
      <ActionButton style={padding} kind="negative" onClick={() => action(false)}>
        <Close style={margin} />
        {t('Reject')}
      </ActionButton>
    </div>
  );
};
