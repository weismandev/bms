import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const ParkingSlotEnterpriseSelect = (props) => {
  const { buildings = [], enterprise = {}, ...rest } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const [building_id] = buildings;

  useEffect(() => {
    fxGetList({ buildings, enterprise_id: enterprise.id });
  }, [enterprise, building_id, buildings.length]);

  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      error={error}
      getOptionLabel={(opt) => (opt ? opt.number : '')}
      {...rest}
    />
  );
};

const ParkingSlotEnterpriseSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ParkingSlotEnterpriseSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { ParkingSlotEnterpriseSelect, ParkingSlotEnterpriseSelectField };
