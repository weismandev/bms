import { createEffect, createStore } from 'effector';
import { signout } from '../../common';
import { changeEnterprise } from '../../stuff/models/detail.model';
import { parkingApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!Array.isArray(result.items) && !Array.isArray(result.slots)) {
      return [];
    }

    let res;

    if (result.slots) {
      res = result.slots;
    } else {
      res = result.items.reduce((acc, cur) => [...acc, ...cur.related_slots], []);
    }
    return res;
  })
  .reset([signout, changeEnterprise]);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use((params) => {
  return parkingApi.getCompanySlots(params);
});

export { fxGetList, $data, $error, $isLoading };
