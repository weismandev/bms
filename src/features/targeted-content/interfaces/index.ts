export type { GetTargetingListResponse, TargetingGroup } from './targeting-list';
export type { GetGendersResponse, Gender } from './genders-select';
