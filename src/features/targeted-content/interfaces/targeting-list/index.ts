export interface GetTargetingListResponse {
  targeting: {
    groups: TargetingGroup;
  };
}

export interface TargetingGroup {
  id: number;
  slug: string;
  title: string;
  options: {
    id: number;
    title: string;
  }[];
}
