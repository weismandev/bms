export interface GetGendersResponse {
  id: number;
  name: string;
  display_name: string;
}

export interface Gender {
  id: number;
  title: string;
}
