import { signout } from '@features/common';
import { targetedContentApi } from '../../api';
import { unmounted } from '../targeted-content/targeted-content.model';
import { fxGetGenders, $genders, $isLoadingGenders } from './genders-select.model';

fxGetGenders.use(targetedContentApi.getGenders);

$genders
  .on(fxGetGenders.done, (_, { result }) => {
    if (!Array.isArray(result)) {
      return [];
    }

    return result.map((item) => ({
      id: item.id,
      title: item?.display_name || '',
    }));
  })
  .reset([signout, unmounted]);

$isLoadingGenders
  .on(fxGetGenders.pending, (_, result) => result)
  .reset([signout, unmounted]);
