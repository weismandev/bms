import { createEffect, createStore } from 'effector';
import { GetGendersResponse, Gender } from '../../interfaces';

export const fxGetGenders = createEffect<void, GetGendersResponse[], Error>();

export const $genders = createStore<Gender[]>([]);
export const $isLoadingGenders = createStore<boolean>(false);
