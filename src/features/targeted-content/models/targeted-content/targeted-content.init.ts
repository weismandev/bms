import { sample } from 'effector';
import { signout } from '@features/common';
import { targetedContentApi } from '../../api';
import { TargetingGroup } from '../../interfaces';
import {
  mounted,
  unmounted,
  fxGetTargetingList,
  $targetingList,
} from './targeted-content.model';

fxGetTargetingList.use(targetedContentApi.getTargetingList);

// запрос target options
sample({
  clock: mounted,
  target: fxGetTargetingList,
});

$targetingList
  .on(
    fxGetTargetingList.done,
    (
      _,
      {
        result: {
          targeting: { groups },
        },
      }
    ) => {
      if (!Array.isArray(groups)) {
        return [];
      }

      return groups.map((group: TargetingGroup) => {
        const options =
          Array.isArray(group?.options) && group.options.length > 0
            ? group.options.map((option) => ({
                id: option.id,
                title: option?.title || '',
              }))
            : [];

        return {
          id: group.id,
          slug: group?.slug || '',
          title: group?.title || '',
          options,
        };
      });
    }
  )

  .reset([signout, unmounted]);
