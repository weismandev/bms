import { createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';
import { GetTargetingListResponse, TargetingGroup } from '../../interfaces';

export const Gate = createGate();

export const unmounted = Gate.close;
export const mounted = Gate.open;

export const fxGetTargetingList = createEffect<void, GetTargetingListResponse, Error>();

export const $targetingList = createStore<TargetingGroup[]>([]);
