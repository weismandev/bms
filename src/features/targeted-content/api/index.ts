import { api } from '@api/api2';

const getTargetingList = () => api.v1('get', 'bms/targeting/list');
const getGenders = () => api.v1('get', 'profile/genders');

export const targetedContentApi = {
  getTargetingList,
  getGenders,
};
