import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $genders, fxGetGenders, $isLoadingGenders } from '../../models';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const GendersSelect: FC<object> = (props) => {
  const [genders, isLoadingGenders] = useUnit([$genders, $isLoadingGenders]);

  useEffect(() => {
    fxGetGenders();
  }, []);

  return <SelectControl options={genders} isLoading={isLoadingGenders} {...props} />;
};

export const GendersSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<GendersSelect />} onChange={onChange} {...props} />;
};
