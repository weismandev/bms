import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledTypography = styled(Typography)(() => ({
  fontWeight: 500,
  fontSize: 16,
  color: 'rgba(25, 28, 41, 0.87)',
  margin: '20px 0',
}));
