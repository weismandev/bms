import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { FieldArray, Field } from 'formik';
import { SelectField } from '@ui/index';
import { Gate, $targetingList } from '../../models';
import { GendersSelectField } from '../genders-select';
import { SliderField } from '../slider-field';
import { StyledTypography } from './styled';

interface Props {
  mode: string;
}

export const TargetedContent: FC<Props> = ({ mode = 'view' }) => {
  const { t } = useTranslation();
  const targetingList = useUnit($targetingList);

  return (
    <>
      <Gate />

      <StyledTypography>{t('SettingUpAnAudience')}</StyledTypography>
      <FieldArray
        name="targetingOptions"
        render={() =>
          targetingList.map((item, idx) => (
            <Field
              key={item.id}
              component={SelectField}
              name={`targetingOptions.${idx}`}
              label={item.title}
              options={item.options}
              isMulti
              mode={mode}
              maxMenuHeight={200}
            />
          ))
        }
      />
      <Field
        component={GendersSelectField}
        name="genders"
        label={t('gender')}
        mode={mode}
        isMulti
      />
      <Field component={SliderField} name="age" label={t('Age')} mode={mode} />
    </>
  );
};
