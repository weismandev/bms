import { FC } from 'react';
import { StyledTypography, StyledSliderWrapper, StyledSlider } from './styled';

interface FieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
    value: number[];
  };
  label: string;
  mode: string;
}

export const SliderField: FC<FieldProps> = (props) => {
  const onChange = ({ target }: { target: { value: number[] } }) =>
    props.form.setFieldValue(props.field.name, target.value);

  return (
    <>
      {props?.label && <StyledTypography>{props.label}</StyledTypography>}
      <StyledSliderWrapper>
        <StyledSlider
          onChange={onChange}
          valueLabelDisplay="on"
          disabled={props.mode === 'view'}
          value={props.field.value.length === 0 ? [0, 100] : props.field.value}
          {...props}
        />
      </StyledSliderWrapper>
    </>
  );
};
