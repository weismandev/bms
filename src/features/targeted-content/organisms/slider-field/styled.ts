import { Typography, Slider } from '@mui/material';
import { styled } from '@mui/material/styles';

export const StyledTypography = styled(Typography)(() => ({
  color: '#65657B',
  margin: '10px 0px',
  fontSize: 15,
  whiteSpace: 'nowrap',
  cursor: 'pointer',
  fontWeight: 400,
}));

export const StyledSliderWrapper = styled('div')(() => ({
  padding: '0px 20px',
  margin: '40px 0px 10px',
}));

export const StyledSlider = styled(Slider)(() => ({
  '& .MuiSlider-valueLabel': {
    fontSize: '0.875rem',
    fontWeight: 400,
    backgroundColor: 'unset',
    color: 'rgba(0, 0, 0, 0.87)',
  },
}));
