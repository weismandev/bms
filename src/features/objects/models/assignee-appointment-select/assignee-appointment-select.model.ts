import { createEffect, createStore } from 'effector';
import {
  AssigneAppointment,
  AssigneAppointmentResponse,
} from '../../types/assignee-appointment';

export const fxGetListAppointment = createEffect<
  void,
  AssigneAppointmentResponse,
  Error
>();

export const $assigneeAppointmentList = createStore<AssigneAppointment[]>([]);
export const $isLoadingAppointmentList = createStore<boolean>(false);
