import { signout } from '@features/common';
import { objectsApi } from '../../api';
import {
  $assigneeAppointmentList,
  $isLoadingAppointmentList,
  fxGetListAppointment,
} from './assignee-appointment-select.model';

fxGetListAppointment.use(objectsApi.getAppointmentAssignees);

$assigneeAppointmentList
  .on(fxGetListAppointment.done, (_, { result }) =>
    !Array.isArray(result?.methods)
      ? []
      : result.methods.map((item) => ({
          id: item.id,
          title: item?.title || '',
        }))
  )
  .reset(signout);

$isLoadingAppointmentList
  .on(fxGetListAppointment.pending, (_, result) => result)
  .reset(signout);
