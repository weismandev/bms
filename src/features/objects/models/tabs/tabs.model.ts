import { createEvent, createStore } from 'effector';
import { Card } from '@features/objects/organisms/card';
import { Docs } from '@features/objects/organisms/docs';
import { Objects } from '@features/objects/organisms/objects-table';
import { Rent } from '@features/objects/organisms/rent';
import { Rules } from '@features/objects/organisms/rules';
import { Settings } from '@features/objects/organisms/settings';
import { TabCategory, TabType } from '@features/objects/types/tabs';
import i18n from '@shared/config/i18n';

export const $currentTab = createStore<TabCategory>('objects');

export const changeTab = createEvent<TabCategory>();
export const resetTab = createEvent();

const { t } = i18n;

export const tabs: TabType[] = [
  {
    value: 'objects',
    label: t('objects'),
    Component: Objects,
    access: [
      'building',
      'entrance',
      'passage',
      'floor',
      'apartment',
      'office',
      'coworking',
      'parking',
      'parking_zone',
      'parking_lot',
    ],
  },
  {
    value: 'card',
    label: t('card'),
    Component: Card,
    access: [
      'complex',
      'building',
      'entrance',
      'floor',
      'elevator',
      'apartment',
      'room',
      'passage',
      'office',
      'coworking',
      'parking',
      'parking_place',
      'parking_zone',
      'parking_lot',
      'pantry',
      'property_place',
      'property',
      'workplace',
      'meeting_room',
    ],
  },
  {
    value: 'settings',
    label: t('settings'),
    Component: Settings,
    access: ['complex', 'building'],
  },
  {
    value: 'rent',
    label: t('rental'),
    Component: Rent,
    access: ['meeting_room'],
  },
  {
    value: 'rules',
    label: t('rules'),
    Component: Rules,
    access: ['building'],
  },
  {
    value: 'docs',
    label: t('documents'),
    Component: Docs,
    access: [
      'building',
      'apartment',
      'pantry',
      'office',
      'coworking',
      'workplace',
      'event_area',
    ],
  },
];
