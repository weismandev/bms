import { sample } from 'effector';
import { signout } from '@features/common';
import { $currentObjectType, openObject } from '../objects/objects.model';
import { $currentTab, changeTab, resetTab, tabs } from './tabs.model';

/** Изменение текущей вкладки */
$currentTab.on(changeTab, (_, tab) => tab);

sample({
  source: $currentObjectType,
  clock: resetTab,
  fn: (currentObjectType) => {
    const objectAccess = tabs.find((item) => item.value === 'objects');
    if (
      objectAccess &&
      currentObjectType &&
      objectAccess.access?.indexOf(currentObjectType) > -1
    ) {
      return 'objects';
    }

    return 'card';
  },
  target: $currentTab,
});

/** Сброс */
sample({
  clock: [openObject, signout],
  target: resetTab,
});
