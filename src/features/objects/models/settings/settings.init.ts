import { sample } from 'effector';
import { reset } from 'patronum';
import lodashSet from 'lodash/set';
import { signout } from '@features/common';
import { ObjectInstance } from '@features/objects/types/object';
import { $currentObjectGuid } from '../objects/objects.model';
import { errorOccured, pageUnmounted } from '../root/root.model';
import {
  $isSettingsLoading,
  $settings,
  $settingsEditMode,
  fxGetSettings,
  fxUpdateSettings,
  getSettings,
  setSettingsEditMode,
  SettingsGate,
  settingsMounted,
  updateSettings,
  settingsUnmounted,
  reconciliationOptions,
} from './settings.model';

/** Триггер ивента на запрос настроек */
sample({
  clock: settingsMounted,
  target: getSettings,
});

sample({
  clock: $currentObjectGuid,
  source: SettingsGate.status,
  filter: (status, currentGuid) => Boolean(status && currentGuid),
  target: getSettings,
});

/** Запрос настроек зоны */
sample({
  clock: getSettings,
  source: [$currentObjectGuid, $settings],
  fn: ([currentObjectGuid]) => ({
    area_guid: (currentObjectGuid as ObjectInstance['guid']) || '',
  }),
  target: fxGetSettings,
});

/** Запись настроек в общий стор */
sample({
  clock: [fxGetSettings.done, fxUpdateSettings.done],
  source: $settings,
  fn: (settings, { params, result: { areaSettings } }) => ({
    ...lodashSet(settings, params.area_guid, {
      correct_counters_data_manually: areaSettings.correctCountersDataManually.value,
      correct_counters_data_from_date: areaSettings.correctCountersDataFromDate.value,
      correct_counters_data_until_date: areaSettings.correctCountersDataUntilDate.value,
      request_frequency: areaSettings.requestFrequency.value,
      reconciliation_period:
        areaSettings.reconciliationPeriod.value ??
        reconciliationOptions.find(({ value }) => value === null),
      cold_water_expense_norm: areaSettings.coldWaterExpenseNorm.value,
      cold_water_expense_minimum: areaSettings.coldWaterExpenseMinimum.value,
      cold_water_expense_maximum: areaSettings.coldWaterExpenseMaximum.value,
      hot_water_expense_norm: areaSettings.hotWaterExpenseNorm.value,
      hot_water_expense_minimum: areaSettings.hotWaterExpenseMinimum.value,
      hot_water_expense_maximum: areaSettings.hotWaterExpenseMaximum.value,
      electricity_expense_norm: areaSettings.electricityExpenseNorm.value,
      electricity_expense_minimum: areaSettings.electricityExpenseMinimum.value,
      electricity_expense_maximum: areaSettings.electricityExpenseMaximum.value,
      heat_energy_expense_norm: areaSettings.heatEnergyExpenseNorm.value,
      heat_energy_expense_minimum: areaSettings.heatEnergyExpenseMinimum.value,
      heat_energy_expense_maximum: areaSettings.heatEnergyExpenseMaximum.value,
      security_number: areaSettings.securityNumber.value,
      sell_enabled: areaSettings.sellEnabled.value,
      sell_emails: areaSettings.sellEmails.value,
      resident_request_resolve_mode: areaSettings.residentRequestResolveMode.value,
      buildings_properties_rent_available:
        areaSettings.buildingsPropertiesRentAvailable.value,
      start_work_time: areaSettings.startWorkTime.value,
      end_work_time: areaSettings.endWorkTime.value,
      guest_scud_pass_limit: areaSettings.guestScudPassLimit.value,
      paid_tickets_enabled: areaSettings.paidTicketsEnabled.value,
      is_matching_measures_complex_config:
        areaSettings.isMatchingMeasuresComplexConfig.value,
      updated_at: Date.now(),
    }),
  }),
  target: $settings,
});

/** Сохранение настроек зоны */
sample({
  clock: updateSettings,
  source: $currentObjectGuid,
  filter: (objectGuid, updatedSettings) => Boolean(objectGuid && updatedSettings),
  fn: (objectGuid, settings) => ({
    ...settings,
    area_guid: objectGuid || '',
    sell_emails: settings.sell_emails,
    reconciliation_period:
      typeof settings.reconciliation_period === 'object'
        ? (settings.reconciliation_period as any).value
        : settings.reconciliation_period,
  }),
  target: fxUpdateSettings,
});

/** Изменение режима редактирование */
sample({
  clock: setSettingsEditMode,
  target: $settingsEditMode,
});

/** Обработка ошибок */
sample({
  clock: [fxGetSettings.fail, fxUpdateSettings.fail],
  target: errorOccured,
});

/** Сброс */
reset({
  clock: [pageUnmounted, signout, settingsUnmounted],
  target: [$settings, $settingsEditMode, $isSettingsLoading],
});
