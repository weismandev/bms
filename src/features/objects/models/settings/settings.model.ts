import { combine, createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import { FormEditMode } from '@features/objects/types/form';
import { ObjectInstance } from '@features/objects/types/object';
import {
  GetSettingsRequest,
  GetSettingsResponse,
  Settings,
  UpdateSettingsResponse,
} from '@features/objects/types/settings';
import { $currentObjectGuid } from '../objects/objects.model';
import i18n from '@shared/config/i18n';

export const SettingsGate = createGate();
export const { open: settingsMounted, close: settingsUnmounted } = SettingsGate;

export const fxGetSettings = createEffect<
  GetSettingsRequest,
  GetSettingsResponse,
  Error
>().use(objectsApi.getSettings);

export const fxUpdateSettings = createEffect<
  Settings,
  UpdateSettingsResponse,
  Error
>().use(objectsApi.updateSettings);

export const $settings = createStore<Record<ObjectInstance['guid'], Settings>>({});
export const $currentSettings = combine(
  $currentObjectGuid,
  $settings,
  (currentObjectGuid, settings) =>
    (currentObjectGuid && settings[currentObjectGuid]) || null
);
export const $settingsEditMode = createStore<FormEditMode>('view');
export const $isSettingsLoading = pending({
  effects: [fxGetSettings, fxUpdateSettings],
});

export const getSettings = createEvent<void>();
export const updateSettings = createEvent<Settings>();
export const setSettingsEditMode = createEvent<FormEditMode>();

const { t } = i18n;

export const reconciliationOptions = [
  { title: t('DoNotMakeReconciliations'), value: null },
  { title: t('OnceEveryOneMonth'), value: 1 },
  { title: t('OnceEveryThreeMonths'), value: 3 },
  { title: t('OnceEverySixMonths'), value: 6 },
  { title: t('OnceEveryTwelveMonths'), value: 12 },
];
