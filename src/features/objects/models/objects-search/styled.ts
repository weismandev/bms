import { styled, Chip } from '@mui/material';

export const AttachedChip = styled(Chip)(() => ({
  color: '#FFFFFF',
  background: '#00A947',
}));

export const UnattachedChip = styled(Chip)(() => ({
  color: 'rgba(25, 28, 41, 0.6)',
  background: 'rgba(238, 238, 238, 1)',
}));
