import { sample } from 'effector';
import { signout } from '@features/common';
import { $currentObjectGuid, openObject } from '../objects/objects.model';
import { errorOccured, pageUnmounted } from '../root/root.model';
import {
  $search,
  searchChanged,
  throttledSearch,
  fxGetSearch,
  $isShowSearchResult,
  $searchCount,
  $searchTableData,
  $tableParamsSearch,
  defaultPaginationModel,
  $paginationSearch,
} from './objects-search.model';

/** Обработка ошибок запроса */
sample({
  clock: [fxGetSearch.fail],
  target: errorOccured,
});

/** Если есть значение в поиске, то открывается окно с результом */
sample({
  clock: $search,
  fn: (search) => search.trim().length > 0,
  target: $isShowSearchResult,
});

$search
  .on(searchChanged, (_, value) => value)
  .reset([signout, pageUnmounted, openObject]);

/** Поиск объектов */
sample({
  clock: [throttledSearch, $tableParamsSearch],
  source: { search: $search, table: $tableParamsSearch },
  filter: ({ search }) => search.trim().length > 0,
  fn: ({ search, table }) => ({
    search: search.trim(),
    page: table.page,
    per_page: table.per_page,
    fast_filters: {
      is_complex: 1,
      is_building: 1,
      is_entrance: 1,
      is_office: 1,
      is_floor: 1,
      is_apartment: 1,
      is_meeting_room: 1,
      is_coworking: 1,
      is_pantry: 1,
      is_premises: 1,
      is_parking_lot: 1,
      is_parking_zone: 1,
      is_parking_place: 1,
    },
  }),
  target: fxGetSearch,
});

$searchCount
  .on(
    fxGetSearch.done,
    (
      _,
      {
        result: {
          meta: { total },
        },
      }
    ) => total ?? 0
  )
  .reset([signout, pageUnmounted]);

/** При открытии окна с результатами поиска сбрасывается выбранный объект */
sample({
  clock: $isShowSearchResult,
  source: $currentObjectGuid,
  filter: (guid, isShow) => Boolean(guid && isShow),
  fn: () => null,
  target: $currentObjectGuid,
});

$searchTableData
  .on(fxGetSearch.done, (_, { result: { areaList } }) => {
    if (!Array.isArray(areaList)) {
      return [];
    }

    return areaList.map((item) => ({
      id: item.guid,
      title: item?.title ?? '-',
      complex: item?.complex?.title ?? '-',
      building: item?.building?.title ?? '-',
      entrance: item?.entrance_number ?? '-',
      floor: item?.floor_number ?? '-',
      number: item?.number ?? '-',
      area: item?.area ?? '-',
      accounts: Array.isArray(item?.area_accounts)
        ? item.area_accounts.map((account) => ({
            id: account.id,
            number: account?.account_number ?? '-',
          }))
        : [],
      status: item?.has_owner ?? false,
      account: item?.area_owner?.fullname ?? '-',
      passageTitle: item?.passage_title ?? '-',
    }));
  })
  .reset([signout, pageUnmounted]);

/** При закрытии окна с результатами сбрасывается количество найденных элементов */
sample({
  clock: $isShowSearchResult,
  filter: (isShowSearchResult) => !isShowSearchResult,
  fn: () => 0,
  target: $searchCount,
});

/** При закрытии окна с результатами очищаются табличные данные */
sample({
  clock: $isShowSearchResult,
  filter: (isShowSearchResult) => !isShowSearchResult,
  fn: () => [],
  target: $searchTableData,
});

/** При закрытии окна с результатами очищается пагинация */
sample({
  clock: $isShowSearchResult,
  filter: (isShowSearchResult) => !isShowSearchResult,
  fn: () => defaultPaginationModel,
  target: $paginationSearch,
});
