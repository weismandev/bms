import { createEffect, createEvent, createStore } from 'effector';
import { throttle, pending } from 'patronum';
import { GridColDef } from '@mui/x-data-grid-pro';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@shared/tools/table';
import { objectsApi } from '../../api';
import { TableHiddenArray } from '../../molecules/table-hidden-array';
import {
  SearchObjectsPayload,
  SearchObjectsResponse,
  SearchTableData,
  AccountType,
} from '../../types/objects-search';
import * as Styled from './styled';

const { t } = i18n;

export const searchTablecolumns: GridColDef[] = [
  {
    field: 'title',
    headerName: t('NameTitle') ?? '',
    width: 300,
    sortable: false,
    hideable: false,
  },
  {
    field: 'complex',
    headerName: t('complex') ?? '',
    width: 300,
    sortable: false,
    hideable: true,
  },
  {
    field: 'building',
    headerName: t('building') ?? '',
    width: 300,
    sortable: false,
    hideable: true,
  },
  {
    field: 'entrance',
    headerName: t('rizer') ?? '',
    width: 100,
    sortable: false,
    hideable: true,
  },
  {
    field: 'floor',
    headerName: t('Floor') ?? '',
    width: 100,
    sortable: false,
    hideable: true,
  },
  {
    field: 'number',
    headerName: t('Number') ?? '',
    width: 100,
    sortable: false,
    hideable: true,
  },
  {
    field: 'area',
    headerName: t('AreaInM') ?? '',
    width: 100,
    sortable: false,
    hideable: true,
  },
  {
    field: 'accounts',
    headerName: t('Label.personalAccount') ?? '',
    width: 200,
    sortable: false,
    hideable: true,
    renderCell: ({ value }: { value: AccountType[] }) =>
      value[0] ? (
        <TableHiddenArray>
          {value.map((item) => (
            <span key={item.id}>{item.number}</span>
          ))}
        </TableHiddenArray>
      ) : (
        <div>-</div>
      ),
  },
  {
    field: 'status',
    headerName: t('AnchorStatus') ?? '',
    width: 150,
    sortable: false,
    hideable: true,
    renderCell: ({ value }: { value: boolean }) =>
      value ? (
        <Styled.AttachedChip size="small" label={t('Attached')} />
      ) : (
        <Styled.UnattachedChip size="small" label={t('Unattached')} />
      ),
  },
  {
    field: 'account',
    headerName: t('MainAccount') ?? '',
    width: 300,
    sortable: false,
    hideable: true,
  },
  {
    field: 'passageTitle',
    headerName: t('Transition') ?? '',
    width: 300,
    sortable: false,
    hideable: true,
  },
];

export const defaultPaginationModel = {
  page: 0,
  pageSize: 25,
};

export const {
  $tableParams: $tableParamsSearch,
  $pagination: $paginationSearch,
  paginationModelChanged: paginationModelChangedSearch,
} = createTableBag({
  columns: searchTablecolumns,
  paginationModel: defaultPaginationModel,
});

export const fxGetSearch = createEffect<
  SearchObjectsPayload,
  SearchObjectsResponse,
  Error
>().use(objectsApi.searchObjects);

export const $search = createStore<string>('');
export const $searchCount = createStore<number>(0);
export const $isShowSearchResult = createStore<boolean>(false);
export const $isSearchLoading = pending({
  effects: [fxGetSearch],
});
export const $searchTableData = createStore<SearchTableData[]>([]);

export const searchChanged = createEvent<string>();

export const throttledSearch = throttle({
  source: searchChanged,
  timeout: 1000,
});
