import { reset } from 'patronum';
import { signout } from '@features/common';
import {
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  errorOccured,
  pageUnmounted,
  $error,
} from './root.model';

/** Логика модалки с ошибкой */
$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true);

$error.on(errorOccured, (_, { error }) => error);

/** Сброс */
reset({
  clock: [pageUnmounted, signout],
  target: [$isErrorDialogOpen, $error],
});
