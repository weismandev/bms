import { createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { some } from 'patronum';
import { $isCardLoading } from '../card/card.model';
import { $isCreationLoading } from '../creation/creation.model';
import { $isObjectDeleting } from '../delete/delete.model';
import { $isDocsLoading } from '../docs/docs.model';
import { $isObjectsLoading } from '../objects/objects.model';
import { $isObjectsTableLoading } from '../objectsTable/objectsTable.model';
import { $isRentLoading } from '../rent/rent.model';
import { $isRulesLoading } from '../rules/rules.model';
import { $isSettingsLoading } from '../settings/settings.model';

export const ObjectsPageGate = createGate();
export const { open: pageMounted, close: pageUnmounted } = ObjectsPageGate;

export const $error = createStore<any>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $isSidebarLoading = some({
  stores: [$isObjectsLoading],
  predicate: (value) => Boolean(value),
});

export const $isRootLoading = some({
  stores: [
    $isCardLoading,
    $isObjectsTableLoading,
    $isSettingsLoading,
    $isCreationLoading,
    $isObjectDeleting,
    $isRentLoading,
    $isRulesLoading,
    $isDocsLoading,
  ],
  predicate: (value) => Boolean(value),
});

export const changedErrorDialogVisibility = createEvent<boolean>();
export const errorOccured = createEvent<any>();
