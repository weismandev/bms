import { combine, createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import {
  Cards,
  GetCardRequest,
  GetCardResponse,
  UpdateCard,
  UpdateCardRequest,
  UpdateCardResponse,
} from '@features/objects/types/card';
import { FormEditMode } from '@features/objects/types/form';
import { ObjectInstance } from '@features/objects/types/object';
import { $currentObjectGuid } from '../objects/objects.model';

export const CardGate = createGate();
export const { open: cardMounted, close: cardUnmounted } = CardGate;

export const fxGetCard = createEffect<GetCardRequest, GetCardResponse, Error>().use(
  objectsApi.getCard
);
export const fxUpdateCard = createEffect<
  Partial<UpdateCardRequest>,
  UpdateCardResponse,
  Error
>().use(objectsApi.updateCard);

export const $cards = createStore<Cards>({});
export const $currentCard = combine(
  $currentObjectGuid,
  $cards,
  (currentObjectGuid, cards) => (currentObjectGuid && cards[currentObjectGuid]) || null
);
export const $isCardLoading = pending({ effects: [fxGetCard, fxUpdateCard] });
export const $cardEditMode = createStore<FormEditMode>('view');

export const getCard = createEvent<void>();
export const saveCard = createEvent<UpdateCard>();
export const setCardEditMode = createEvent<FormEditMode>();
export const cardUpdated = createEvent<ObjectInstance>();
