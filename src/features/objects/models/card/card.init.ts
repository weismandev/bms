import { sample } from 'effector';
import { reset } from 'patronum';
import lodashOmit from 'lodash/omit';
import lodashSet from 'lodash/set';
import { signout } from '@features/common';
import { UpdateCardRequest } from '@features/objects/types/card';
import { $currentObjectGuid } from '../objects/objects.model';
import { errorOccured, pageUnmounted } from '../root/root.model';
import {
  $cardEditMode,
  $cards,
  $currentCard,
  CardGate,
  cardMounted,
  cardUnmounted,
  cardUpdated,
  fxGetCard,
  fxUpdateCard,
  getCard,
  saveCard,
  setCardEditMode,
} from './card.model';

/** Триггер ивента на запрос карточки */
sample({
  clock: cardMounted,
  target: getCard,
});

sample({
  clock: $currentObjectGuid,
  source: CardGate.status,
  filter: (status, currentGuid) => Boolean(status && currentGuid),
  target: getCard,
});

/** Запрос карточки объекта при маунте */
sample({
  clock: getCard,
  source: $currentObjectGuid,
  fn: (currentObjectGuid) => ({
    area_guid: currentObjectGuid || '',
  }),
  target: fxGetCard,
});

/** Запись полученной карточки в общий стор */
sample({
  clock: [fxGetCard.doneData, fxUpdateCard.doneData],
  source: $cards,
  fn: (cards, { area }) => ({
    ...lodashSet(cards, area.guid, { ...area, updated_at: Date.now() }),
  }),
  target: $cards,
});

/** Триггер ивента об успешном обновлении карточки */
sample({
  clock: fxUpdateCard.doneData,
  fn: ({ area }) => area,
  target: cardUpdated,
});

/** Сохранение карточки */
sample({
  clock: saveCard,
  source: $currentCard,
  fn: (currentCard, card) => {
    const payload = {
      ...card,
      area_guid: currentCard?.guid,
      type_title: currentCard?.type_title,
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      image_id: card?.image?.id || null,
    } as UpdateCardRequest;

    const isOutdoor =
      // @ts-ignore
      typeof payload.is_outdoor === 'object' ? payload.is_outdoor.id : payload.is_outdoor;

    if (card?.assignee_appointment_method?.id) {
      payload.assignee_appointment_method_id = card.assignee_appointment_method.id;
    }

    if (isOutdoor) {
      // @ts-ignore
      payload.is_outdoor = isOutdoor === 1;

      // @ts-ignore
      payload.building_guids =
        // @ts-ignore
        isOutdoor === 1 ? [] : payload.building_guids.map((i) => i.id);
    }

    // @ts-ignore
    if (payload?.image_ids?.id) {
      // @ts-ignore
      payload.image_ids = [payload.image_ids.id];
    } else {
      // @ts-ignore
      payload.image_ids = [];
    }

    return lodashOmit(payload, ['updated_at', 'image']);
  },
  target: fxUpdateCard,
});

/** Изменение режима редактирования */
sample({
  clock: setCardEditMode,
  target: $cardEditMode,
});

/** Обработка ошибок */
sample({
  clock: [fxGetCard.fail, fxUpdateCard.fail],
  target: errorOccured,
});

/** Сброс */
reset({
  clock: [cardUnmounted, pageUnmounted, signout],
  target: $cardEditMode,
});
