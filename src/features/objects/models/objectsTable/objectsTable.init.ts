import { sample } from 'effector';
import { reset } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { $currentObjectGuid } from '../objects/objects.model';
import { errorOccured, pageUnmounted } from '../root/root.model';
import {
  $filters,
  $isObjectsTableLoading,
  $isQrModalOpen,
  $isDeleteModalOpen,
  $isConfirmationDeleteModalOpen,
  $isConfirmModalOpen,
  $isDeniedModalOpen,
  $objectsTable,
  $page,
  $pageSize,
  $rowsCount,
  $selectionModel,
  $tableRequestData,
  changeFiltersDebounced,
  changeQrModalVisibility,
  changeDeleteModalVisibility,
  changeConfirmDeleteModalVisibility,
  changeConfirmVisibility,
  changeDeniedVisibility,
  createQr,
  fxCreateQr,
  fxGetObjectsTable,
  fxMultipleDelete,
  getObjectsTable,
  deleteObjects,
  ObjectsTableGate,
  objectsTableMounted,
  objectsTableUnmounted,
  setPage,
  setPageSize,
  setSelectionModel,
} from './objectsTable.model';

/** Работа со страницами */
sample({
  clock: setPage,
  target: $page,
});

sample({
  clock: setPageSize,
  target: $pageSize,
});

/** Сброс страницы при изменении количества строк */
$page.reset(setPageSize);

/** Установка количества страниц */
sample({
  clock: fxGetObjectsTable.doneData,
  fn: ({ meta }) => meta.total,
  target: $rowsCount,
});

/** Удаление элементов из таблицы */
sample({
  source: $selectionModel,
  clock: deleteObjects,
  fn: (source) => ({ area_guids: source }),
  target: fxMultipleDelete,
});

/** Подтверждение удаления */
sample({
  clock: changeConfirmVisibility,
  target: $isConfirmModalOpen,
});

sample({
  clock: changeDeniedVisibility,
  target: $isDeniedModalOpen,
});

sample({
  source: fxMultipleDelete.doneData,
  fn: (data) => data.deleted ? true : false,
  target: changeConfirmVisibility,
});

/** Ошибка удаления */
sample({
  clock: fxMultipleDelete.doneData,
  fn: (data) => data.deleted ? false : true,
  target: changeDeniedVisibility,
});

/** Триггер ивента на запрос таблицы */
sample({
  clock: [$currentObjectGuid, $filters, objectsTableMounted, $page, $pageSize, fxMultipleDelete.doneData],
  source: ObjectsTableGate.status,
  filter: (status) => Boolean(status),
  target: getObjectsTable,
});

/** Запрос таблицы */
sample({
  clock: getObjectsTable,
  source: $tableRequestData,
  filter: ({ parent_guid }) => Boolean(parent_guid),
  target: fxGetObjectsTable,
});

/** Обработка объектов */
$objectsTable.on(fxGetObjectsTable.doneData, (_, { areaList }) =>
  areaList.map((object, index) => {
    const result = {
      ...object,
      has_owner: object.has_owner || false,
      id: object.guid || index,
    };

    return result;
  })
);

/** Обработка изменения фильтров */
sample({
  clock: changeFiltersDebounced,
  target: $filters,
});

/** Обработка выделения */
sample({
  clock: setSelectionModel,
  target: $selectionModel,
});

/** Обработка модалкм с QR кодом */
sample({
  clock: changeQrModalVisibility,
  target: $isQrModalOpen,
});

sample({
  clock: createQr,
  fn: () => false,
  target: $isQrModalOpen,
});

/** Обработка модалки с удалением */
sample({
  clock: changeDeleteModalVisibility,
  target: $isDeleteModalOpen,
});

/** Обработка модалки с подтверждением удаления */
sample({
  clock: changeConfirmDeleteModalVisibility,
  target: $isConfirmationDeleteModalOpen,
});

/** Скачивание QR */
sample({
  source: $selectionModel,
  clock: createQr,
  fn: (source, clock) => ({
    area_guids: source,
    ...clock,
  }),
  target: fxCreateQr,
});

sample({
  clock: fxCreateQr.doneData,
  fn: (data) => {
    const blob = new Blob([data], { type: 'application/pdf' });

    const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
    FileSaver.saveAs(URL.createObjectURL(blob), `qr-codes-${dateTimeNow}.pdf`);
  },
});

/** Обработка ошибок */
sample({
  clock: [fxGetObjectsTable.fail],
  target: errorOccured,
});

/** Сброс */
reset({
  clock: [objectsTableUnmounted, pageUnmounted, signout],
  target: [
    $objectsTable,
    $filters,
    $isObjectsTableLoading,
    $selectionModel,
    $isQrModalOpen,
  ],
});
