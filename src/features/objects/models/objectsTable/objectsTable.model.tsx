import { combine, createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { debounce, pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import { ExtendedObjectInstance, ObjectInstance } from '@features/objects/types/object';
import {
  GetObjectsTableRequest,
  GetObjectsTableResponse,
  ObjectsFilters,
  ObjectsTablePaginationModel,
} from '@features/objects/types/objectsTable';
import { DeleteObjectsRequest } from '@features/objects/types/delete';
import { HiddenArray } from '@ui/molecules';
import { $currentObjectGuid } from '../objects/objects.model';
import i18n from '@shared/config/i18n';

export const ObjectsTableGate = createGate();
export const { open: objectsTableMounted, close: objectsTableUnmounted } =
  ObjectsTableGate;

export const fxGetObjectsTable = createEffect<
  GetObjectsTableRequest,
  GetObjectsTableResponse,
  Error
>().use(objectsApi.getObjectsTable);
export const fxCreateQr = createEffect<IObjectsCreateQrRequest, any, Error>().use(
  objectsApi.createQr
);
export const fxMultipleDelete = createEffect<DeleteObjectsRequest, any, Error>().use(
  objectsApi.deleteMultipleObjects
)

export const $objectsTable = createStore<ExtendedObjectInstance[]>([]);
export const $page = createStore<number>(0);
export const $pageSize = createStore<number>(25);
export const $rowsCount = createStore<number>(0);
export const $filters = createStore<ObjectsFilters>({
  area_min: null,
  area_max: null,
  types: [],
});
export const $isObjectsTableLoading = pending({
  effects: [fxGetObjectsTable, fxCreateQr, fxMultipleDelete],
});
export const $selectionModel = createStore<any>([]);
export const $isQrModalOpen = createStore<boolean>(false);
export const $isDeleteModalOpen = createStore<boolean>(false);
export const $isConfirmationDeleteModalOpen = createStore<boolean>(false);
export const $isConfirmModalOpen = createStore<boolean>(false);
export const $isDeniedModalOpen = createStore<boolean>(false);
export const $tableRequestData = combine(
  $currentObjectGuid,
  $page,
  $pageSize,
  $filters,
  (currentObjectGuid, page, pageSize, filters) => ({
    parent_guid: currentObjectGuid || '',
    filters: {
      area_min: filters.area_min,
      area_max: filters.area_max,
    },
    fast_filters: filters.types.reduce(
      (fastFilters, type) => ({ ...fastFilters, [type.value]: 1 }),
      {}
    ),
    per_page: pageSize,
    page: page + 1,
    // show_all_area_types: 1,
  })
);

export const getObjectsTable = createEvent<void>();
export const deleteObjects = createEvent<void>();
export const changeFilters = createEvent<ObjectsFilters>();
export const changeFiltersDebounced = debounce({
  source: changeFilters,
  timeout: 500,
});
export const setPage = createEvent<number>();
export const setPageSize = createEvent<number>();
export const setSelectionModel = createEvent<any>();
export const changeQrModalVisibility = createEvent<boolean>();
export const changeDeleteModalVisibility = createEvent<boolean>();
export const changeConfirmDeleteModalVisibility = createEvent<boolean>();
export const changeConfirmVisibility = createEvent<boolean>();
export const changeDeniedVisibility = createEvent<boolean>();
export const createQr = createEvent<IObjectsCreateQr>();
export const setPaginationModel = createEvent<ObjectsTablePaginationModel>();
export const paginationModelChanged = createEvent<void>();

const { t } = i18n;

export const typesOptions = [
  { value: 'is_apartment', title: t('Apartments') },
  { value: 'is_office', title: t('Office') },
  { value: 'is_coworking', title: t('Coworking') },
  { value: 'is_premises', title: t('OtherPremises') },
];

export const objectsTableColumns = [
  {
    field: 'title',
    headerName: t('NameTitle'),
    width: 250,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'area_accounts',
    headerName: t('navMenu.directories.personalAccounts'),
    width: 250,
    headerClassName: 'super-app-theme--header',
    renderCell: (cell: { value: ObjectInstance['area_accounts'] }) =>
      cell.value ? (
        <HiddenArray>
          {cell.value.map((item, index) =>
            item.archived ? (
              <span key={index} style={{ opacity: 0.5 }}>
                {item.account_number}
              </span>
            ) : (
              <span key={index}>{item.account_number}</span>
            )
          )}
        </HiddenArray>
      ) : null,
  },
  {
    field: 'number',
    headerName: t('Number'),
    width: 100,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'area',
    headerName: t('area'),
    width: 100,
    headerClassName: 'super-app-theme--header',
  },
  {
    field: 'complex',
    headerName: t('complex'),
    width: 170,
    valueFormatter: (data: { value: { title: string } }) => data.value.title,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'building',
    headerName: t('building'),
    type: 'number',
    width: 170,
    valueFormatter: (data: { value: { title: string } }) => data.value.title,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'has_owner',
    headerName: t('AnchorStatus'),
    type: 'boolean',
    width: 200,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
    renderCell: (cell: { value: string }) =>
      cell.value ? (
        <span style={{ fontSize: 16, fontWeight: 500, color: '#1BB169' }}>{t('Attached')}</span>
      ) : (
        <span style={{ fontSize: 16, fontWeight: 500, color: '#7D7D8E' }}>
          {t('Unattached')}
        </span>
      ),
  },
  {
    field: 'area_owner',
    valueFormatter: (data: { value: { fullname: string } }) => data.value?.fullname || '',
    headerName: t('MainAccount'),
    type: 'string',
    width: 170,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'passage_title',
    headerName: t('Transition'),
    type: 'number',
    width: 170,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'entrance_number',
    headerName: t('rizer'),
    type: 'number',
    width: 100,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
  {
    field: 'floor_number',
    headerName: t('Floor'),
    type: 'number',
    width: 100,
    headerClassName: 'super-app-theme--header',
    align: 'left',
    headerAlign: 'left',
  },
];
