import { sample } from 'effector';
import { signout } from '@features/common';
import { UpdateRentPayload, RentValueItem, Rent } from '@features/objects/types/rent';
import { $currentCard } from '../card/card.model';
import { errorOccured } from '../root/root.model';
import {
  rentUnmounted,
  setRentEditMode,
  $rentEditMode,
  updateRent,
  rentMounted,
  $rent,
  fxUpdateRent,
  fxGetRent,
} from './rent.model';

/** Обработка ошибок */
sample({
  clock: [fxGetRent.fail, fxUpdateRent.fail],
  target: errorOccured,
});

/** Изменение режима редактирование */
sample({
  clock: setRentEditMode,
  target: $rentEditMode,
});

$rentEditMode.on(fxUpdateRent.fail, () => 'edit').reset([signout, rentUnmounted]);

/** Запрос аренды */
sample({
  clock: rentMounted,
  source: $currentCard,
  filter: (card) => card?.guid,
  fn: (card) => ({ area_id: card?.guid }),
  target: fxGetRent,
});

const formatPayload = (rent: Rent) => {
  const payload: UpdateRentPayload = {
    is_payment_required: Boolean(rent?.isPayment),
  };

  payload.area_description = rent.description;
  payload.is_need_booking_approval = Boolean(rent.isNeedBookingApproval);

  if (rent.isNeedBookingApproval) {
    payload.booking_approval_time =
      typeof rent.bookingApprovalTime === 'object'
        ? (rent.bookingApprovalTime as RentValueItem).id
        : (rent.bookingApprovalTime as number);
  }

  payload.is_need_proof_of_use = Boolean(rent.isNeedProofOfUse);

  if (rent.isNeedProofOfUse) {
    payload.proof_of_use_time =
      typeof rent.proofOfUseTime === 'object'
        ? (rent.proofOfUseTime as RentValueItem).id
        : (rent.proofOfUseTime as number);
  }

  payload.available_days = rent?.availableDays;

  if (rent.availableTimeFrom && rent.availableTimeTo) {
    payload.available_time = [rent.availableTimeFrom, rent.availableTimeTo];
  }

  payload.time_step =
    typeof rent.timeStep === 'object'
      ? (rent.timeStep as RentValueItem).id
      : rent.timeStep;

  payload.is_payment_required = Boolean(rent?.isPayment);

  if (rent?.isPayment) {
    payload.step_cost = Number.parseInt(rent.stepCost, 10);
    payload.paid_waiting_time = 60;

    // if (rent.paidWaitingTime) { пока нет функционала оплаты
    //   payload.paid_waiting_time =
    //     typeof rent.paidWaitingTime === 'object'
    //       ? (rent.paidWaitingTime as RentValueItem).id
    //       : rent.paidWaitingTime;
    // }
  }

  return payload;
};

/** Сохранение настроек, если доступно для аренды */
sample({
  clock: updateRent,
  source: $currentCard,
  filter: (_, { isRentAvailability }) => Boolean(isRentAvailability),
  fn: (currentCard, rent) => {
    const payload: UpdateRentPayload = {
      area_id: currentCard?.guid as string,
      is_rent_availability: rent.isRentAvailability,
    };

    const otherParams = formatPayload(rent);

    return { ...payload, ...otherParams };
  },
  target: fxUpdateRent,
});

/** Сохранение настроек, если аренда недоступна */
sample({
  clock: updateRent,
  source: {
    currentCard: $currentCard,
    rent: $rent,
  },
  filter: (_, { isRentAvailability }) => !isRentAvailability,
  fn: ({ currentCard, rent }) => {
    const payload = {
      area_id: currentCard?.guid as string,
      is_rent_availability: false,
    };

    const otherParams = formatPayload(rent as Rent);

    return { ...payload, ...otherParams };
  },
  target: fxUpdateRent,
});

$rent
  .on([fxGetRent.done, fxUpdateRent.done], (_, { result: { template } }) => ({
    isRentAvailability: Boolean(template?.is_rent_availability),
    description: template?.area_description || '',
    isNeedBookingApproval: Boolean(template?.is_need_booking_approval),
    bookingApprovalTime: template?.booking_approval_time || '',
    isNeedProofOfUse: Boolean(template?.is_need_proof_of_use),
    proofOfUseTime: template?.proof_of_use_time || '',
    availableDays: template?.available_days,
    availableTimeFrom: template?.available_time[0],
    availableTimeTo: template?.available_time[1],
    timeStep: template?.time_step,
    isPayment: Boolean(template?.is_payment_required),
    stepCost: template?.step_cost || '',
    // paidWaitingTime: template?.paid_waiting_time || null, пока нет функционала оплаты
  }))
  .reset([rentUnmounted, signout]);
