import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import { FormEditMode } from '@features/objects/types/form';
import {
  UpdateRentPayload,
  GetRentPayload,
  GetRentResponse,
  Rent,
} from '@features/objects/types/rent';

export const RentGate = createGate();
export const { open: rentMounted, close: rentUnmounted } = RentGate;

export const setRentEditMode = createEvent<FormEditMode>();
export const updateRent = createEvent<Rent>();

export const $rentEditMode = createStore<FormEditMode>('view');
export const $rent = createStore<null | Rent>(null);

export const fxUpdateRent = createEffect<UpdateRentPayload, GetRentResponse, Error>().use(
  objectsApi.updateRent
);
export const fxGetRent = createEffect<GetRentPayload, GetRentResponse, Error>().use(
  objectsApi.getRent
);

export const $isRentLoading = pending({
  effects: [fxUpdateRent, fxGetRent],
});
