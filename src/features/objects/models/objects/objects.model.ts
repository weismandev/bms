import { combine, createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum/pending';
import lodashSet from 'lodash/set';
import { objectsApi } from '@features/objects/api';
import {
  GetObjectsRequest,
  GetObjectsResponse,
  ObjectInstance,
  Objects,
  ObjectsTree,
  ObjectBuilding,
  GetExpandedTreePayload,
} from '@features/objects/types/object';

export const ObjectsTreeGate = createGate();
export const { open: objectsTreeMounted, close: objectsTreeUnmounted } = ObjectsTreeGate;

export const fxGetObjects = createEffect<
  GetObjectsRequest,
  GetObjectsResponse,
  Error
>().use(objectsApi.getObjects);
export const fxGetExpandedTree = createEffect<
  GetExpandedTreePayload,
  GetObjectsResponse,
  Error
>().use(objectsApi.getExpandedTree);

export const $objects = createStore<Objects>({});
export const $objectsTree = combine($objects, (objects) => {
  if (!Object.keys(objects).length) return [];

  const dataset = Object.values(objects);

  const hashTable: Record<any, any> = {};
  dataset.forEach((aData) => ({
    ...lodashSet(hashTable, aData.guid, { ...aData, children: [] }),
  }));
  const dataTree: ObjectsTree | [] = [];
  dataset.forEach((aData) => {
    if (aData.parent) hashTable[aData.parent]?.children.push(hashTable[aData.guid]);
    else {
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      dataTree.push(hashTable[aData.guid]);
    }
  });
  return dataTree;
});
export const $currentObjectGuid = createStore<ObjectInstance['guid'] | null>(null);
export const $currentObject = combine(
  $currentObjectGuid,
  $objects,
  (guid, objects) => (guid && objects[guid]) || null
);
export const $currentObjectType = $currentObject.map((object) => object?.type);
export const $isObjectsLoading = pending({ effects: [fxGetObjects, fxGetExpandedTree] });
export const $expandedObjects = createStore<ObjectInstance['guid'][]>([]);
export const $selectedObjects = createStore<ObjectInstance['guid'][]>([]);
export const $buildings = createStore<ObjectBuilding[]>([]);
export const $building = createStore<'' | ObjectBuilding>('');

export const getObjects = createEvent();
export const openObject = createEvent<ObjectInstance['guid']>();
export const refreshObjects = createEvent();
export const setExpandedObjects = createEvent<ObjectInstance['guid'][]>();
export const setSelectedObjects = createEvent<ObjectInstance['guid'][]>();
export const collapseAllObjects = createEvent<void>();
export const expandAllObjects = createEvent<void>();
export const getExpandedTree = createEvent<string>();
