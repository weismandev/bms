import { sample } from 'effector';
import { reset } from 'patronum';
import lodashOmit from 'lodash/omit';
import lodashSet from 'lodash/set';
import { signout } from '@features/common';
import { ObjectMeta, Objects } from '../../types/object';
import { cardUpdated } from '../card/card.model';
import { objectCreated } from '../creation/creation.model';
import { objectDeleted } from '../delete/delete.model';
import { fxMultipleDelete, $selectionModel } from '../objectsTable/objectsTable.model';
import { errorOccured, pageMounted, pageUnmounted } from '../root/root.model';
import {
  $currentObject,
  $currentObjectGuid,
  $expandedObjects,
  $objects,
  $selectedObjects,
  collapseAllObjects,
  expandAllObjects,
  fxGetObjects,
  openObject,
  refreshObjects,
  setExpandedObjects,
  setSelectedObjects,
  $buildings,
  getExpandedTree,
  fxGetExpandedTree,
  $building,
  $objectsTree,
} from './objects.model';

/** Запрос раскрытых объектов до указанного объекта */
sample({
  clock: getExpandedTree,
  fn: (id) => ({ guid: id }),
  target: fxGetExpandedTree,
});

/** Открытие объекта после клика в таблице */
sample({
  clock: fxGetExpandedTree.done,
  fn: ({ params: { guid } }) => guid,
  target: openObject,
});

const getExpandedObjects = (
  guid: string,
  areaTree: ObjectMeta[],
  expandedObjects: string[]
): string[] => {
  const currentObject = areaTree.find((area) => area.guid === guid);

  if (!currentObject?.parent && currentObject?.guid) {
    return [...expandedObjects, currentObject.guid];
  }

  if (currentObject?.parent && currentObject?.guid) {
    return getExpandedObjects(currentObject.parent, areaTree, [
      ...expandedObjects,
      currentObject.guid,
    ]);
  }
  return expandedObjects;
};

/** Формирование списка раскрытых объектов в дереве */
sample({
  clock: fxGetExpandedTree.done,
  fn: ({ params: { guid }, result: { areaTree } }) => {
    if (!areaTree.length) {
      return [];
    }

    return getExpandedObjects(guid, areaTree, []).reverse();
  },
  target: $expandedObjects,
});

/** Запрос на получение объектов при маунте или обновлении */
sample({
  clock: [pageMounted, refreshObjects],
  fn: () => ({}),
  target: fxGetObjects,
});

/** Обработка ошибок запроса */
sample({
  clock: [fxGetObjects.fail, fxGetExpandedTree.fail],
  target: errorOccured,
});

/** Записываем полученные объекты как meta информацию в общий стор с объектами */
sample({
  clock: [fxGetObjects.doneData, fxGetExpandedTree.doneData],
  source: $objects,
  fn: (objects, { areaTree }) => {
    const newObjects = objects;

    areaTree.forEach((object) => {
      lodashSet(newObjects, object.guid, object);
    });

    return { ...newObjects };
  },
  target: $objects,
});

/** Запрос на дочерние уровни при клике на элемент дерева */
sample({
  clock: $currentObject,
  source: $objects,
  filter: (objects, currentObject) =>
    !Object.values(objects).find(
      (item) => item.parent === (currentObject as ObjectMeta).guid
    ),
  fn: (_, currentObject) => ({ parent_guid: currentObject?.guid, level: 1 }),
  target: fxGetObjects,
});

/** Открытие объекта */
sample({
  clock: openObject,
  target: $currentObjectGuid,
});

/** Установка выделенного объекта при изменении текущего guid */
sample({
  clock: $currentObjectGuid,
  fn: (currentGuid) => (currentGuid ? [currentGuid] : []),
  target: setSelectedObjects,
});

/** Обновление объекта при обновлении карточки */
sample({
  clock: cardUpdated,
  source: $objects,
  fn: (objects, card) => ({
    ...lodashSet(objects, `${card.guid}.title`, card.name),
  }),
  target: $objects,
});

/** Обновление объектов при создании */
sample({
  clock: objectCreated,
  source: $objects,
  fn: (objects, object) => ({
    ...lodashSet(objects, object.guid, object),
  }),
  target: $objects,
});

/** Обновление объектов при удалении */
sample({
  clock: objectDeleted,
  source: $objects,
  fn: (objects, deletedGuid) => ({
    ...lodashOmit(objects, [deletedGuid]),
  }),
  target: $objects,
});

sample({
  clock: fxMultipleDelete.done,
  source: [$objects, $selectionModel],
  fn: ([objects, selectionModel]) => ({
    ...lodashOmit(objects, [...selectionModel]),
  }),
  target: $objects,
});

/** Сброс текущего объекта при удалении */
reset({
  clock: objectDeleted,
  target: $currentObjectGuid,
});

/** Установка раскрытых объектов */
sample({
  clock: setExpandedObjects,
  target: $expandedObjects,
});

/** Установка выделенных объектов */
sample({
  clock: setSelectedObjects,
  target: $selectedObjects,
});

sample({
  clock: collapseAllObjects,
  fn: () => [],
  target: setExpandedObjects,
});

sample({
  clock: expandAllObjects,
  source: $objects,
  fn: (objects) => Object.keys(objects),
  target: setExpandedObjects,
});

/** Автораскрытие ветки по выделению объекта */
sample({
  clock: $selectedObjects,
  source: $expandedObjects,
  fn: (expandedObjects, selectedObjects) => [
    ...new Set([...expandedObjects, ...selectedObjects]),
  ],
  target: $expandedObjects,
});

reset({
  clock: [refreshObjects, signout],
  target: [$currentObjectGuid, $objects, $expandedObjects, $selectedObjects],
});

sample({
  clock: [refreshObjects, signout],
  fn: () => ({}),
  target: $objects,
});

const getParentObject = (guid: string, objects: Objects): ObjectMeta => {
  const object = objects[guid];

  if (!object?.parent) {
    return object;
  }

  return getParentObject(object.parent, objects);
};

/** Формирование списка зданий у выбранного объекта */
sample({
  clock: [$objectsTree, $currentObjectGuid],
  source: {
    tree: $objectsTree,
    currentObjectGuid: $currentObjectGuid,
    objects: $objects,
  },
  fn: ({ tree, currentObjectGuid, objects }) => {
    if (!currentObjectGuid) {
      return [];
    }

    const parentObject = getParentObject(currentObjectGuid, objects);
    const object = tree.find((item) => (item as ObjectMeta).guid === parentObject.guid);

    if ((object as ObjectMeta | undefined)?.type === 'complex') {
      return (object as any).children
        .filter((item: ObjectMeta) => item.type === 'building')
        .map((item: ObjectMeta) => ({
          id: item.guid,
          title: item.title,
        }));
    }

    return [];
  },
  target: $buildings,
});

/** Сбоос */
reset({
  clock: [signout, pageUnmounted],
  target: [$buildings, $building],
});

const getRootBuildingGuid = (object: ObjectMeta, objects: Objects): string =>
  object.type === 'building'
    ? object.guid
    : getRootBuildingGuid(objects[(object as any)?.parent], objects);

/** Получение здания у выбранного этажа */
sample({
  clock: $currentObject,
  source: { currentObject: $currentObject, objects: $objects },
  filter: ({ currentObject }) => currentObject?.type === 'floor',
  fn: ({ objects, currentObject }) => {
    const { guid, title } =
      objects[getRootBuildingGuid(currentObject as ObjectMeta, objects)];

    return { id: guid, title };
  },
  target: $building,
});
