import { sample } from 'effector';
import { reset } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import { $currentObject } from '../objects/objects.model';
import {
  rulesUnmounted,
  $rulesEditMode,
  setRulesEditMode,
  saveRule,
  fxSaveRule,
  fxGetRule,
  rulesMounted,
  $rules,
  fxUpdateRule,
  exportRule,
  fxExportRule,
} from './rules.model';
import i18n from '@shared/config/i18n';

const { t } = i18n;

/** Изменение режима редактирование */
sample({
  clock: setRulesEditMode,
  target: $rulesEditMode,
});

/** Сброс */
reset({
  clock: [rulesUnmounted, signout],
  target: [$rulesEditMode, $rules],
});

/** Сброс */
reset({
  clock: [fxSaveRule.done, fxUpdateRule.done],
  target: $rulesEditMode,
});

/** Создание правил */
sample({
  clock: saveRule,
  source: $currentObject,
  filter: (_, { id }) => !id,
  fn: (currentObject, { text }) => ({
    area_guid: currentObject?.guid as string,
    content: text,
  }),
  target: fxSaveRule,
});

/** Редактирование правил */
sample({
  clock: saveRule,
  filter: ({ id }) => Boolean(id),
  fn: ({ id, text }) => ({
    id: id as number,
    content: text,
  }),
  target: fxUpdateRule,
});

/** Запрос правил */
sample({
  clock: rulesMounted,
  source: $currentObject,
  fn: (currentObject) => ({
    area_guid: currentObject?.guid as string,
  }),
  target: fxGetRule,
});

$rules.on(
  [fxGetRule.done, fxSaveRule.done, fxUpdateRule.done],
  (_, { result: { documents } }) => ({
    id: documents[0]?.id || null,
    text: documents[0]?.content || '',
    preview: documents[0]?.url || null,
  })
);

/** Экспорт правил */
sample({
  clock: exportRule,
  source: $rules,
  fn: (rules) => ({ id: rules?.id as number }),
  target: fxExportRule,
});

// eslint-disable-next-line effector/no-watch
fxExportRule.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/pdf',
  });

  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `rules-${dateTimeNow}.pdf`);
});

/** Вызов сообщения об успешном создании */
sample({
  clock: fxSaveRule.done,
  fn: () => ({
    isOpen: true,
    color: '#1BB169',
    text: t('RulesAdded'),
    Icon: CheckCircle,
  }),
  target: changeNotification,
});

/** Вызов сообщения об успешном редактировании */
sample({
  clock: fxUpdateRule.done,
  fn: () => ({
    isOpen: true,
    color: '#1BB169',
    text: t('RulesUpdated'),
    Icon: CheckCircle,
  }),
  target: changeNotification,
});
