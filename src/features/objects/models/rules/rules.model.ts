import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import { FormEditMode } from '@features/objects/types/form';
import {
  CreateRulePayload,
  GetRulePayload,
  GetRuleResponse,
  Rule,
  UpdateRulePayload,
  ExportRulePayload,
} from '@features/objects/types/rules';

export const RulesGate = createGate();
export const { open: rulesMounted, close: rulesUnmounted } = RulesGate;

export const setRulesEditMode = createEvent<FormEditMode>();
export const saveRule = createEvent<Rule>();
export const exportRule = createEvent<void>();

export const fxSaveRule = createEffect<CreateRulePayload, GetRuleResponse, Error>().use(
  objectsApi.saveRule
);
export const fxGetRule = createEffect<GetRulePayload, GetRuleResponse, Error>().use(
  objectsApi.getRule
);
export const fxUpdateRule = createEffect<UpdateRulePayload, GetRuleResponse, Error>().use(
  objectsApi.updateRule
);
export const fxExportRule = createEffect<ExportRulePayload, ArrayBuffer, Error>().use(
  objectsApi.exportRule
);

export const $rulesEditMode = createStore<FormEditMode>('view');
export const $rules = createStore<null | Rule>(null);
export const $isRulesLoading = pending({
  effects: [fxSaveRule, fxGetRule, fxUpdateRule, fxExportRule],
});
