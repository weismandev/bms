import { createEffect, createEvent, createStore } from 'effector';
import { pending } from 'patronum';
import { ObjectInstance } from '@features/objects/types/object';
import {
  DeleteObjectRequest,
  DeleteObjectResponse,
} from '@features/objects/types/delete';
import { objectsApi } from '@features/objects/api';

export const fxDeleteObject = createEffect<
  DeleteObjectRequest,
  DeleteObjectResponse,
  Error
>().use(objectsApi.deleteObject);

export const $isDeleteDialogVisible = createStore<boolean>(false);
export const $isObjectDeleting = pending({ effects: [fxDeleteObject] });

export const openDeleteDialog = createEvent();
export const cancelDelete = createEvent();
export const acceptDelete = createEvent();
export const objectDeleted = createEvent<ObjectInstance['guid']>();
