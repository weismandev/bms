import { sample } from 'effector';
import {
  $isDeleteDialogVisible,
  acceptDelete,
  cancelDelete,
  fxDeleteObject,
  objectDeleted,
  openDeleteDialog,
} from './delete.model';

import { $currentObjectGuid } from '../objects/objects.model';
import { errorOccured } from '../root/root.model';

/** Открытие модалки с удалением */
sample({
  clock: openDeleteDialog,
  fn: () => true,
  target: $isDeleteDialogVisible,
});

/** Закрытие модалки с удалением */
sample({
  clock: [cancelDelete, acceptDelete],
  fn: () => false,
  target: $isDeleteDialogVisible,
});

/** При удалении объекта берем guid текущего и прокидываем в метод удаления */
sample({
  clock: acceptDelete,
  source: $currentObjectGuid,
  filter: (guid) => Boolean(guid),
  fn: (guid) => ({ area_guid: guid || '' }),
  target: fxDeleteObject,
});

/** Вызов ивента об успешном удалении */
sample({
  clock: fxDeleteObject.done,
  filter: ({ result: { deleted } }) => Boolean(deleted),
  fn: ({ params }) => params.area_guid,
  target: objectDeleted,
});

/** Обработка ошибок */
sample({
  clock: fxDeleteObject.fail,
  target: errorOccured,
});
