import { createEffect, createStore, createEvent, attach } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import {
  PictureAsPdfOutlined,
  DescriptionOutlined,
  PermMediaOutlined,
} from '@mui/icons-material';
import { GRID_CHECKBOX_SELECTION_COL_DEF } from '@mui/x-data-grid-pro';
import {
  createSavedUserSettings,
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { createTableBag, Columns } from '@features/data-grid';
import { objectsApi } from '@features/objects/api';
import {
  GetDocsPayload,
  GetDocsResponse,
  Document,
  DocumentStatusesResponse,
  StatusDocument,
  UploadDocumentPayload,
  ExportDocPayload,
  DocumentsPayload,
  NotViewDocumentData,
} from '@features/objects/types/docs';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const DocsGate = createGate();
export const { open: docsMounted, close: docsUnmounted } = DocsGate;

export const setSelectedDocStatus = createEvent<StatusDocument>();
export const uploadDoc = createEvent<File>();
export const exportDocuments = createEvent<number[]>();
export const changedVisibilityDeleteDocsModal = createEvent<boolean>();
export const deleteDocs = createEvent<void>();
export const changedVisibilityArchiveDocsModal = createEvent<boolean>();
export const archiveDocs = createEvent<void>();
export const unarchiveDocs = createEvent<void>();
export const changedNotViewDocumentModalData = createEvent<NotViewDocumentData>();
export const exportDocument = createEvent<ExportDocPayload>();

export const fxGetDocs = createEffect<GetDocsPayload, GetDocsResponse, Error>().use(
  objectsApi.getDocs
);
export const fxGetDocsStatuses = createEffect<
  void,
  DocumentStatusesResponse,
  Error
>().use(objectsApi.getStatusesDocuments);
export const fxUploadFile = createEffect<UploadDocumentPayload, object, Error>().use(
  objectsApi.uploadDocument
);
export const fxExportDoc = createEffect<ExportDocPayload, ArrayBuffer, Error>().use(
  objectsApi.exportDocument
);
export const fxGetExportDocEach = attach({
  async effect(_: unknown, params: { id: number; title: string; type: string }[]) {
    await Promise.all(
      params.forEach(({ id, title, type }) => {
        fxExportDoc({ id, title, type, export: 1 });
      })
    );
  },
});
export const fxDeleteDocuments = createEffect<DocumentsPayload, object, Error>().use(
  objectsApi.deleteDocuments
);
export const fxArchiveDocuments = createEffect<DocumentsPayload, object, Error>().use(
  objectsApi.archiveDocuments
);
export const fxUnarchiveDocuments = createEffect<DocumentsPayload, object, Error>().use(
  objectsApi.unarchiveDocuments
);
export const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

export const $isDocsLoading = pending({
  effects: [
    fxGetDocs,
    fxUploadFile,
    fxExportDoc,
    fxDeleteDocuments,
    fxArchiveDocuments,
    fxUnarchiveDocuments,
  ],
});
export const $docs = createStore<Document[]>([]);
export const $rowsCountDocs = createStore<number>(0);
export const $docsStatuses = createStore<StatusDocument[]>([]);
export const $selectedDocStatus = createStore<StatusDocument | null>(null);
export const $isOpenDeleteDocsModal = createStore<boolean>(false);
export const $isOpenArchiveDocsModal = createStore<boolean>(false);
export const $notViewDocumentDataModal = createStore<NotViewDocumentData>({
  isOpen: false,
  id: null,
  title: '',
  type: '',
});

export const docsTableColumns = [
  {
    ...GRID_CHECKBOX_SELECTION_COL_DEF,
    hideable: false,
  },
  {
    field: 'title',
    headerName: t('NameTitle'),
    width: 350,
    headerClassName: 'super-app-theme--header',
    hideable: false,
    sortable: true,
    renderCell: ({ value, row: { type } }: { value: string; row: { type: string } }) => (
      <div>
        {type.toLowerCase() === 'pdf' && (
          <PictureAsPdfOutlined color="error" style={{ marginRight: 5 }} />
        )}
        {(type.toLowerCase() === 'doc' || type.toLowerCase() === 'docx') && (
          <DescriptionOutlined color="primary" style={{ marginRight: 5 }} />
        )}
        {(type.toLowerCase() === 'jpg' ||
          type.toLowerCase() === 'jpeg' ||
          type.toLowerCase() === 'png') && (
          <PermMediaOutlined color="success" style={{ marginRight: 5 }} />
        )}
        {value}
      </div>
    ),
  },
  {
    field: 'type',
    headerName: t('type'),
    width: 100,
    headerClassName: 'super-app-theme--header',
    hideable: true,
    sortable: true,
  },
  {
    field: 'date',
    headerName: t('dateOfDownload'),
    width: 150,
    headerClassName: 'super-app-theme--header',
    hideable: true,
    sortable: true,
  },
  {
    field: 'author',
    headerName: t('Author'),
    width: 250,
    headerClassName: 'super-app-theme--header',
    hideable: true,
    sortable: true,
  },
];

export const {
  $tableParams: $docsTableParams,
  pageNumberChanged: docsPageNumberChanged,
  pageSizeChanged: docsPageSizeChanged,
  $currentPage: $docsCurrentPage,
  $pageSize: $docsPageSize,
  $columns: $docsColumns,
  orderColumnsChanged: docsOrderColumnsChanged,
  columnsWidthChanged: docsColumnsWidthChanged,
  $visibilityColumns: $docsVisibilityColumns,
  visibilityColumnsChanged: docsVisibilityColumnsChanged,
  $selectRow: $docsSelectRow,
  selectionRowChanged: docsSelectionRowChanged,
  $pinnedColumns: $docsPinnedColumns,
  changePinnedColumns: docsChangePinnedColumns,
} = createTableBag({
  columns: docsTableColumns as Columns[],
  pageSize: 25,
});

export const $savedDocsColumns = $docsColumns.map((columns) =>
  columns.map((column) => ({
    field: column.field,
    width: column.width,
  }))
);

const docsSettings = {
  section: 'objects-docs',
  params: {
    table: {
      $pageSize: $docsPageSize,
      $hiddenColumnNames: $docsVisibilityColumns,
      $columns: $savedDocsColumns,
      $pinnedColumns: $docsPinnedColumns,
    },
  },
  pageMounted: docsMounted,
  storage: {
    fxSaveInStorage,
    fxGetFromStorage,
  },
};

export const [$settingsInitialized, $isGettingSettingsFromStorage] =
  createSavedUserSettings(docsSettings);
