import { sample } from 'effector';
import { reset } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { Error, CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import { Columns } from '@features/data-grid';
import { GetDocsPayload } from '@features/objects/types/docs';
import i18n from '@shared/config/i18n';
import { $currentObject } from '../objects/objects.model';
import { errorOccured } from '../root/root.model';
import {
  docsMounted,
  fxGetDocs,
  $docs,
  docsUnmounted,
  $rowsCountDocs,
  fxGetDocsStatuses,
  $docsStatuses,
  $selectedDocStatus,
  setSelectedDocStatus,
  uploadDoc,
  fxUploadFile,
  exportDocuments,
  fxGetExportDocEach,
  fxExportDoc,
  $isOpenDeleteDocsModal,
  changedVisibilityDeleteDocsModal,
  fxDeleteDocuments,
  deleteDocs,
  $isOpenArchiveDocsModal,
  changedVisibilityArchiveDocsModal,
  archiveDocs,
  fxArchiveDocuments,
  unarchiveDocs,
  fxUnarchiveDocuments,
  $notViewDocumentDataModal,
  changedNotViewDocumentModalData,
  exportDocument,
  $docsTableParams,
  $docsSelectRow,
  $settingsInitialized,
  fxGetFromStorage,
  $docsColumns,
  $savedDocsColumns,
} from './docs.model';

const { t } = i18n;

const coefficientKilobyte = 1024;
const maxFileSize = 1024;

/** Сброс */
reset({
  clock: [docsUnmounted, signout],
  target: [
    $docs,
    $rowsCountDocs,
    $docsStatuses,
    $selectedDocStatus,
    $isOpenDeleteDocsModal,
    $isOpenArchiveDocsModal,
    $notViewDocumentDataModal,
  ],
});

reset({
  clock: deleteDocs,
  target: $isOpenDeleteDocsModal,
});

reset({
  clock: [archiveDocs, unarchiveDocs],
  target: $isOpenArchiveDocsModal,
});

reset({
  clock: exportDocument,
  target: $notViewDocumentDataModal,
});

/** Запрос документов */
sample({
  clock: [
    docsMounted,
    $selectedDocStatus,
    fxUploadFile.done,
    fxDeleteDocuments.done,
    fxArchiveDocuments.done,
    fxUnarchiveDocuments.done,
    $docsTableParams,
    $settingsInitialized,
  ],
  source: {
    currentObject: $currentObject,
    tableParams: $docsTableParams,
    selectedDocStatus: $selectedDocStatus,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ selectedDocStatus, settingsInitialized }) =>
    settingsInitialized && Boolean(selectedDocStatus?.id),
  fn: ({ currentObject, tableParams, selectedDocStatus }) => {
    const payload: GetDocsPayload = {
      area_guid: currentObject?.guid as string,
      page: tableParams.page,
      per_page: tableParams.per_page,
    };

    if (selectedDocStatus?.id) {
      payload.status_id = selectedDocStatus.id;
    }

    return payload;
  },
  target: fxGetDocs,
});

$docs.on(fxGetDocs.done, (_, { result: { documents } }) => {
  if (!Array.isArray(documents)) {
    return [];
  }

  return documents.map((document) => ({
    id: document.id,
    title: document?.name || '',
    type: document?.extension?.toUpperCase() || '-',
    date: document?.created_at
      ? format(new Date(document.created_at), 'dd.MM.yyyy')
      : '-',
    author: document?.author?.name || '-',
    url: document?.url || '',
  }));
});

$rowsCountDocs.on(
  fxGetDocs.done,
  (
    _,
    {
      result: {
        meta: { total },
      },
    }
  ) => total || 0
);

/** Запрос статусов */
sample({
  clock: docsMounted,
  target: fxGetDocsStatuses,
});

$docsStatuses.on(fxGetDocsStatuses.done, (_, { result }) => {
  if (!Array.isArray(result?.statuses)) {
    return [];
  }

  return result.statuses.map((status) => ({
    id: status.id,
    title: status.name,
  }));
});

/** Обработка ошибок */
sample({
  clock: [
    fxGetDocsStatuses.fail,
    fxUploadFile.fail,
    fxExportDoc.fail,
    fxDeleteDocuments.fail,
    fxArchiveDocuments.fail,
    fxUnarchiveDocuments.fail,
  ],
  target: errorOccured,
});

$selectedDocStatus
  .on(fxGetDocsStatuses.done, (_, { result }) => {
    if (Array.isArray(result?.statuses) && result.statuses[0]) {
      return result.statuses[0];
    }

    return null;
  })
  .on(setSelectedDocStatus, (_, status) => status);

const checkingAllowedType = (type: string) =>
  type === 'application/pdf' ||
  type === 'application/msword' ||
  type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
  type === 'image/jpeg' ||
  type === 'image/png';

/** Если с таким типом файл нельзя загрузить, то открывается модалка */
sample({
  clock: uploadDoc,
  filter: ({ type }) => {
    const isAllowedType = checkingAllowedType(type);

    return !isAllowedType;
  },
  fn: () => ({
    isOpen: true,
    text: t('Error'),
    color: '#d32f2f',
    Icon: Error,
    message: `${t('docsFormatAllowedOnly')}: pdf, doc, docx, jpg, jpeg, png`,
  }),
  target: changeNotification,
});

/** Если размер файла больше лимита, то открывается модалка */
sample({
  clock: uploadDoc,
  filter: ({ size }) => {
    const fileSizeInKilobyte = Math.ceil(size / coefficientKilobyte);

    return fileSizeInKilobyte > maxFileSize;
  },
  fn: () => ({
    isOpen: true,
    text: t('Error'),
    color: '#d32f2f',
    Icon: Error,
    message: t('docFileSizeExceedVolume'),
  }),
  target: changeNotification,
});

/** Загрузка документа */
sample({
  clock: uploadDoc,
  source: $currentObject,
  filter: (_, { type, size }) => {
    const isAllowedType = checkingAllowedType(type);
    const fileSizeInKilobyte = Math.ceil(size / coefficientKilobyte);

    return isAllowedType && fileSizeInKilobyte <= maxFileSize;
  },
  fn: (currentObject, data: File) => ({
    files: [data],
    area_guid: currentObject?.guid as string,
  }),
  target: fxUploadFile,
});

/** Сообщение о загруженном документе */
sample({
  clock: fxUploadFile.done,
  fn: () => ({
    isOpen: true,
    color: '#1BB169',
    text: t('docUploaded'),
    Icon: CheckCircle,
    message: '',
  }),
  target: changeNotification,
});

$isOpenDeleteDocsModal.on(
  changedVisibilityDeleteDocsModal,
  (_, visibility) => visibility
);

/** Запрос на удаление документов */
sample({
  clock: deleteDocs,
  source: $docsSelectRow,
  fn: (docsSelectRow) => ({ ids: docsSelectRow }),
  target: fxDeleteDocuments,
});

$isOpenArchiveDocsModal.on(
  changedVisibilityArchiveDocsModal,
  (_, visibility) => visibility
);

/** Запрос на архивацию документов */
sample({
  clock: archiveDocs,
  source: $docsSelectRow,
  fn: (docsSelectRow) => ({ ids: docsSelectRow }),
  target: fxArchiveDocuments,
});

/** Запрос на разархивацию документов */
sample({
  clock: unarchiveDocs,
  source: $docsSelectRow,
  fn: (docsSelectRow) => ({ ids: docsSelectRow }),
  target: fxUnarchiveDocuments,
});

$notViewDocumentDataModal.on(changedNotViewDocumentModalData, (_, data) => data);

/** Экспорт документов */
sample({
  clock: exportDocuments,
  source: $docs,
  fn: (docs, ids) => ids.map((id) => docs.find((doc) => doc.id === id)),
  target: fxGetExportDocEach,
});

// /** Экспорт документа */
sample({
  clock: exportDocument,
  target: fxExportDoc,
});

const getFileType = (type: string) => {
  switch (type.toLowerCase()) {
    case 'pdf':
      return 'application/pdf';
    case 'jpg':
      return 'image/jpeg';
    case 'jpeg':
      return 'image/jpeg';
    case 'png':
      return 'image/png';
    case 'doc':
      return 'application/msword';
    case 'docx':
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    default:
      return null;
  }
};

// eslint-disable-next-line effector/no-watch
fxExportDoc.done.watch(({ params, result }) => {
  const type = getFileType(params.type);

  if (type) {
    const blob = new Blob([result], {
      type,
    });

    FileSaver.saveAs(URL.createObjectURL(blob), `${params.title}`);
  }
});

/** Обработка полученных данных */
sample({
  clock: fxGetFromStorage.done,
  source: { columns: $docsColumns, savedColumns: $savedDocsColumns },
  fn: ({ columns, savedColumns }) => {
    const formattedColumns = savedColumns.reduce((acc: Columns[], column) => {
      if (column.field === '__check__') {
        return acc;
      }

      const findedColumn = columns.find((item) => item.field === column.field);

      return findedColumn
        ? [
            ...acc,
            {
              field: column.field,
              headerName: findedColumn.headerName,
              width: column.width,
              sortable: findedColumn.sortable,
              hideable: findedColumn.hideable,
              renderCell: findedColumn.renderCell,
            },
          ]
        : acc;
    }, []);

    const newColumns = columns.filter(
      (item) => !formattedColumns.some((element: Columns) => element.field === item.field)
    );

    newColumns.forEach((item) => {
      const index = columns.map((column) => column.field).indexOf(item.field);

      formattedColumns.splice(index, 0, item);
    });

    return formattedColumns;
  },
  target: $docsColumns,
});
