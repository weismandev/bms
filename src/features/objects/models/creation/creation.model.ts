import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { pending } from 'patronum';
import { objectsApi } from '@features/objects/api';
import {
  CreateApartment,
  CreateBuilding,
  CreateEntrance,
  CreateFloor,
  CreateObject,
  CreateObjectRequest,
  CreateObjectResponse,
  CreateParkingLot,
  CreateParkingPlace,
  CreateParkingZone,
  CreateProperty,
  ObjectCreationMenu,
} from '@features/objects/types/creation';
import { ObjectMeta, ObjectType } from '@features/objects/types/object';
import i18n from '@shared/config/i18n';

export const CreationGate = createGate();
export const { open: creationMounted, close: creationUnmounted } = CreationGate;

export const fxCreateObject = createEffect<
  CreateObjectRequest,
  CreateObjectResponse,
  Error
>().use(objectsApi.createObject);

export const $isCreationMode = createStore<boolean>(false);
export const $creationType = createStore<ObjectType>('building');
export const $isCreationLoading = pending({ effects: [fxCreateObject] });

export const openCreation = createEvent<ObjectType>();
export const closeCreation = createEvent();
export const createObject = createEvent<CreateObject>();
export const objectCreated = createEvent<ObjectMeta>();

const { t } = i18n;

export const creationMenu: ObjectCreationMenu = {
  complex: [
    { type: 'building', label: t('building') },
    { type: 'parking_lot', label: t('parking') },
  ],
  parking_lot: [{ type: 'parking_zone', label: t('parkingZone') }],
  parking_zone: [{ type: 'parking_place', label: t('parkingSpace') }],
  building: [{ type: 'entrance', label: t('rizer') }],
  entrance: [{ type: 'floor', label: t('Floor') }],
  floor: [
    { type: 'parking_lot', label: t('parking') },
    { type: 'apartment', label: t('Apartments') },
    { type: 'property', label: t('room') },
  ],
};

export const propertyTypeOptions = [
  { id: 'pantry', title: t('pantry') },
  { id: 'office', title: t('Office') },
  { id: 'coworking', title: t('Coworking') },
  { id: 'workplace', title: t('Workspace') },
  { id: 'event_area', title: t('EventArea') },
  { id: 'meeting_room', title: t('meetingRoom') },
];

export const buildingCreationFields: CreateBuilding = {
  type_title: 'building',
  title: '',
  address: '',
  alias: '',
  fias: '',
};

export const entranceCreationFields: CreateEntrance = {
  type_title: 'entrance',
  number: '',
  elevator_count: 0,
};

export const floorCreationFields: CreateFloor = {
  type_title: 'floor',
  number: '',
};

export const apartmentCreationFields: CreateApartment = {
  type_title: 'apartment',
  title: '',
  number: '',
  area: 0,
  room_count: 0,
};

export const propertyCreationFields: CreateProperty = {
  type_title: 'pantry',
  title: '',
  number: '',
  area: 0,
  is_manufacture: false,
  truck_access: false,
  capacity: 0,
  owner_enterprise_id: undefined,
  price: 0,
  is_rent_available: false,
};

export const parkingLotCreationFields: CreateParkingLot = {
  type_title: 'parking_lot',
  area: 0,
  is_outdoor: 1,
  building_guids: [],
};

export const parkingZoneCreationFields: CreateParkingZone = {
  type_title: 'parking_zone',
  title: '',
  price: 0,
  is_common_zone: false,
  is_for_residents_zone: false,
  is_for_guests_zone: false,
  is_fullness_control_zone: false,
};

export const parkingPlaceCreationFields: CreateParkingPlace = {
  type_title: 'parking_place',
  number: '',
  area: 0,
  is_rent_available: false,
  is_private_rent_available: false,
};
