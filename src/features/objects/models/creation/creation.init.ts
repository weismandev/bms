import { sample } from 'effector';
import { reset } from 'patronum';
import { CheckCircle } from '@mui/icons-material';
import { changeNotification } from '@features/colored-text-icon-notification';
import { signout } from '@features/common';
import { CreateObjectRequest } from '@features/objects/types/creation';
import { ObjectInstance, ObjectMeta } from '@features/objects/types/object';
import i18n from '@shared/config/i18n';
import { $currentObjectGuid, openObject } from '../objects/objects.model';
import { errorOccured, pageUnmounted } from '../root/root.model';
import {
  $creationType,
  $isCreationLoading,
  $isCreationMode,
  closeCreation,
  createObject,
  creationUnmounted,
  fxCreateObject,
  objectCreated,
  openCreation,
} from './creation.model';

const { t } = i18n;

/** Обработка переключения режима создания объектов */
$isCreationMode.on(openCreation, () => true).on(closeCreation, () => false);
$creationType.on(openCreation, (state, type) => type);

/** Запрос на создание объекта */
sample({
  clock: createObject,
  source: $currentObjectGuid,
  fn: (currentObjectGuid, createdObject) => {
    const payload = {
      ...createdObject,
      parent_guid: currentObjectGuid,
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      image_id: createdObject?.image_id?.id || null,
    } as CreateObjectRequest;

    const isOutdoor =
      // @ts-ignore
      typeof payload.is_outdoor === 'object' ? payload.is_outdoor.id : payload.is_outdoor;

    if (payload.owner_enterprise_id) {
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      payload.owner_enterprise_id = payload.owner_enterprise_id.id;
    }

    if (isOutdoor) {
      // @ts-ignore
      payload.is_outdoor = isOutdoor === 1;

      // @ts-ignore
      payload.building_guids =
        // @ts-ignore
        isOutdoor === 1 ? [] : payload.building_guids.map((i) => i.id);
    }

    // @ts-ignore
    if (payload?.image_ids?.id) {
      // @ts-ignore
      payload.image_ids = [payload.image_ids.id];
    }

    return payload;
  },
  target: fxCreateObject,
});

/** Вызов ивента об успешном создании */
sample({
  clock: fxCreateObject.done,
  fn: ({ params, result: { area } }) =>
    ({
      guid: area.guid,
      parent: params.parent_guid,
      title: area.name,
      type: area.type_title,
      level: 1,
    } as ObjectMeta),
  target: objectCreated,
});

/** Вызов сообщения об успешном создании */
sample({
  clock: fxCreateObject.doneData,
  fn: ({ area }) => area,
  target: changeNotification.prepend((area: ObjectInstance) => ({
    isOpen: true,
    color: '#1BB169',
    text: t('ObjectCreated'),
    Icon: CheckCircle,
    onClose: () => {
      closeCreation();
    },
    buttons: [
      {
        title: t('CreateMore'),
        onClick: () => null,
      },
      {
        title: t('OpenObject'),
        onClick: () => {
          closeCreation();
          openObject(area.guid);
        },
      },
    ],
  })),
});

/** Обработка ошибок */
sample({
  clock: [fxCreateObject.fail],
  target: errorOccured,
});

/** Сброс */
reset({
  clock: [$currentObjectGuid, creationUnmounted, pageUnmounted, signout],
  target: [$isCreationMode, $creationType, $isCreationLoading],
});
