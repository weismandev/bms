import { ObjectInstance } from '@features/objects/types/object';

export type GetSettingsRequest = {
  area_guid: ObjectInstance['guid'];
};

export type GetSettingsResponse = {
  areaSettings: {
    correctCountersDataManually: SettingsItem;
    correctCountersDataFromDate: SettingsItem;
    correctCountersDataUntilDate: SettingsItem;
    requestFrequency: SettingsItem;
    reconciliationPeriod: SettingsItem;
    coldWaterExpenseNorm: SettingsItem;
    coldWaterExpenseMinimum: SettingsItem;
    coldWaterExpenseMaximum: SettingsItem;
    hotWaterExpenseNorm: SettingsItem;
    hotWaterExpenseMinimum: SettingsItem;
    hotWaterExpenseMaximum: SettingsItem;
    electricityExpenseNorm: SettingsItem;
    electricityExpenseMinimum: SettingsItem;
    electricityExpenseMaximum: SettingsItem;
    heatEnergyExpenseNorm: SettingsItem;
    heatEnergyExpenseMinimum: SettingsItem;
    heatEnergyExpenseMaximum: SettingsItem;
    securityNumber: SettingsItem;
    sellEnabled: SettingsItem;
    sellEmails: SettingsItem;
    residentRequestResolveMode: SettingsItem;
    buildingsPropertiesRentAvailable: SettingsItem;
    startWorkTime: SettingsItem;
    endWorkTime: SettingsItem;
    guestScudPassLimit: SettingsItem;
    paidTicketsEnabled: SettingsItem;
    isMatchingMeasuresComplexConfig: SettingsItem;
  };
};

export type SettingsName =
  | 'correct_counters_data_manually'
  | 'correct_counters_data_from_date'
  | 'correct_counters_data_until_date'
  | 'request_frequency'
  | 'reconciliation_period'
  | 'cold_water_expense_norm'
  | 'cold_water_expense_minimum'
  | 'cold_water_expense_maximum'
  | 'hot_water_expense_norm'
  | 'hot_water_expense_minimum'
  | 'hot_water_expense_maximum'
  | 'electricity_expense_norm'
  | 'electricity_expense_minimum'
  | 'electricity_expense_maximum'
  | 'heat_energy_expense_norm'
  | 'heat_energy_expense_minimum'
  | 'heat_energy_expense_maximum'
  | 'security_number'
  | 'sell_enabled'
  | 'sell_emails'
  | 'resident_request_resolve_mode'
  | 'buildings_properties_rent_available'
  | 'start_work_time'
  | 'end_work_time'
  | 'guest_scud_pass_limit'
  | 'paid_tickets_enabled'
  | 'is_matching_measures_complex_config';

export type SettingsItem = {
  name: SettingsName;
  title: string;
  type: string;
  value: any;
};

export type Settings = {
  area_guid: ObjectInstance['guid'];
  correct_counters_data_manually: boolean;
  correct_counters_data_from_date: string;
  correct_counters_data_until_date: string;
  request_frequency: string;
  reconciliation_period: string;
  cold_water_expense_norm: string;
  cold_water_expense_minimum: string;
  cold_water_expense_maximum: string;
  hot_water_expense_norm: string;
  hot_water_expense_minimum: string;
  hot_water_expense_maximum: string;
  electricity_expense_norm: string;
  electricity_expense_minimum: string;
  electricity_expense_maximum: string;
  heat_energy_expense_norm: string;
  heat_energy_expense_minimum: string;
  heat_energy_expense_maximum: string;
  security_number: string;
  sell_enabled: boolean;
  sell_emails: string[];
  resident_request_resolve_mode: boolean;
  buildings_properties_rent_available: boolean;
  start_work_time: string;
  end_work_time: string;
  guest_scud_pass_limit: string;
  paid_tickets_enabled: boolean;
  is_matching_measures_complex_config: boolean;
  updated_at: number;
};

export type UpdateSettingsResponse = GetSettingsResponse;
