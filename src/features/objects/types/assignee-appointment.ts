export type AssigneAppointment = {
  id: number;
  title: string;
};

export type AssigneAppointmentResponse = {
  methods: AssigneAppointment[];
};
