declare interface IObjectsCreateQrRequest {
  area_guids: ObjectsObject['guid'][];
  pdf_title: string;
  pdf_text: string;
}

declare interface IObjectsCreateQr {
  pdf_title: string;
  pdf_text: string;
}
