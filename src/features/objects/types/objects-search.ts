type SearchTypeObject =
  | 'is_complex'
  | 'is_building'
  | 'is_entrance'
  | 'is_office'
  | 'is_floor'
  | 'is_apartment'
  | 'is_meeting_room'
  | 'is_coworking'
  | 'is_pantry'
  | 'is_premises'
  | 'is_parking_lot'
  | 'is_parking_zone'
  | 'is_parking_place';

type ObjectType = {
  id: number;
  title: string;
};

export type SearchObjectsPayload = {
  search: string;
  page: number;
  per_page: number;
  fast_filters: { [key in SearchTypeObject]: number };
};

export type SearchObjectsResponse = {
  areaList: {
    area: number;
    area_accounts: {
      id: number;
      account_number: string;
      archived: boolean;
    }[];
    area_owner: { id: number; fullname: string };
    building: ObjectType;
    complex: ObjectType;
    entrance_number: number;
    floor_number: string;
    guid: string;
    has_owner: boolean;
    number: string;
    passage_title: null;
    title: string;
    type: string;
  }[];
  meta: { total: number };
};

export type AccountType = {
  id: number;
  number: number | string;
};

export type SearchTableData = {
  id: string;
  title: string;
  complex: string;
  building: string;
  entrance: number | string;
  floor: string;
  number: string;
  area: number | string;
  accounts: AccountType[];
  status: boolean;
  account: string;
  passageTitle: string;
};
