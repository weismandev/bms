import { ObjectInstance } from './object';

export type DeleteObjectRequest = {
  area_guid: ObjectInstance['guid'];
};

export type DeleteObjectsRequest = {
  area_guids: Array<ObjectInstance['guid']>
};

export type DeleteObjectResponse = {
  deleted: boolean;
};
