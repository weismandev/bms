import { ObjectInstance } from '@features/objects/types/object';

export type TabCategory =
  | 'objects'
  | 'card'
  | 'settings'
  | 'plans'
  | 'docs'
  | 'rules'
  | 'rent';

export type TabType = {
  value: TabCategory;
  label: string;
  Component: any;
  access: ObjectInstance['type_title'][];
};
