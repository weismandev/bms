export type FormType = {
  children: any;
  editMode: FormEditMode;
  setEditMode: (value: FormEditMode) => void;
  isValid: boolean;
  dirty: boolean;
  onSubmit: () => any;
  reset: () => any;
};

export type FormEditMode = 'edit' | 'view';
