export type GetDocsPayload = {
  area_guid: string;
  page: number;
  per_page: number;
  status_id?: number;
};

export type GetDocsResponse = {
  meta: {
    total: number;
  };
  documents: {
    author: { id: number; name: string };
    bytes: number;
    content: string;
    created_at: string;
    extension: string;
    id: number;
    name: string;
    url: string;
  }[];
};

export type Document = {
  id: number;
  title: string;
  type: string;
  date: string;
  author: string;
  url: string;
};

export type DocumentStatusesResponse = {
  statuses: {
    id: number;
    name: string;
  };
};

export type StatusDocument = {
  id: number;
  title: string;
};

export type UploadDocumentPayload = {
  files: File[];
  area_guid: string;
};

export type ExportDocPayload = {
  id: number;
  title: string;
  type: string;
  export: number;
};

export type DocumentsPayload = {
  ids: number[];
};

export type NotViewDocumentData = {
  isOpen: boolean;
  id: number | null;
  title: string;
  type: string;
};
