import { ObjectArea, ObjectInstance, ObjectType } from '@features/objects/types/object';

export type Cards = Record<ObjectInstance['guid'], ObjectInstance>;

export type GetCardRequest = {
  area_guid: ObjectInstance['guid'];
};

export type GetCardResponse = {
  area: ObjectInstance;
};

export type UpdateCardBase = {
  area_guid: ObjectInstance['guid'];
  type_title: ObjectType;
  image_id: ObjectInstance['image'];
};

export type UpdateCardComplex = {
  image?: ObjectInstance['image'];
  title: ObjectInstance['name'];
  locality_id: ObjectInstance['locality_id'];
  // utc: ObjectInstance['utc'];
  coordinates: ObjectInstance['coordinates'];
};

export type UpdateCardBuilding = {
  image?: ObjectInstance['image'];
  title: ObjectInstance['name'];
  address: ObjectInstance['address'];
  alias: ObjectInstance['alias'];
  fias: ObjectInstance['fias'];
  area?: ObjectArea['area'];
  coordinates: ObjectInstance['coordinates'];
};

export type UpdateCardApartment = {
  title: ObjectInstance['name'];
  number: ObjectInstance['number'];
  area?: ObjectArea['area'];
  is_rent_available: ObjectInstance['is_rent_available'];
  price: ObjectInstance['price'];
};

export type UpdateCardEntrance = {
  number: ObjectInstance['number'];
  elevator_count: ObjectInstance['elevator_count'];
};

export type UpdateCardFloor = {
  number: ObjectInstance['number'];
};

export type UpdateCardParkingLot = {
  title: ObjectInstance['name'];
  image?: ObjectInstance['image'];
  area: ObjectArea['area'];
  is_outdoor: ObjectInstance['relatedBuildings'];
  building_guids: ObjectInstance['buildings'];
};

export type UpdateCardParkingPlace = {
  number: ObjectInstance['number'];
  area?: ObjectArea['area'];
  is_rent_available: ObjectInstance['is_rent_available'];
  is_private_rent_available: ObjectInstance['is_private_rent_available'];
};

export type UpdateCardParkingZone = {
  title: ObjectInstance['name'];
  is_common_zone: ObjectInstance['is_common_zone'];
  is_for_residents_zone: ObjectInstance['is_for_residents_zone'];
  is_for_guests_zone: ObjectInstance['is_for_guests_zone'];
  is_fullness_control_zone: ObjectInstance['is_fullness_control_zone'];
};

export type UpdateCardPremise = {
  title: ObjectInstance['name'];
  number: ObjectInstance['number'];
  area?: ObjectArea['area'];
  is_manufacture: ObjectInstance['is_manufacture'];
  truck_access: ObjectInstance['truck_access'];
  capacity: ObjectInstance['capacity'];
  is_rent_available: ObjectInstance['is_rent_available'];
  price: ObjectInstance['price'];
  owner_enterprise_id: ObjectInstance['property_owner_enterprise'];
};

export type UpdateCard =
  | UpdateCardComplex
  | UpdateCardBuilding
  | UpdateCardApartment
  | UpdateCardEntrance
  | UpdateCardFloor
  | UpdateCardParkingLot
  | UpdateCardParkingPlace
  | UpdateCardParkingZone
  | UpdateCardPremise;

export type UpdateCardRequest = UpdateCardBase & UpdateCard;

export type UpdateCardResponse = {
  area: ObjectInstance;
};
