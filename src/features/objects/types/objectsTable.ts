import { ExtendedObjectInstance } from '@features/objects/types/object';

export type GetObjectsTableRequest = {
  parent_guid?: string;
  export?: boolean;
  filters?: {
    city_ids?: number[];
    building_ids?: number[];
    area_min?: ObjectsFilters['area_min'];
    area_max?: ObjectsFilters['area_max'];
  };
  fast_filters?: Partial<ObjectsFilters>;
};

export type GetObjectsTableResponse = {
  areaList: ExtendedObjectInstance[];
  meta: {
    current_page: number;
    from: number;
    last_page: number;
    path: string;
    per_page: string;
    to: number;
    total: number;
  };
};

export type ObjectsTablePaginationModel = { pageSize: number; page: number };

export type ObjectsFilters = {
  area_min: number | null;
  area_max: number | null;
  types: ObjectsFiltersTypeItem[];
};

export type ObjectsFiltersTypeItem = {
  value: ObjectsFiltersTypeValue;
  title: string;
};

export type ObjectsFiltersTypeValue = 'is_apartment' | 'is_premises' | 'is_coworking';
