export type ControlRow = {
  label: string | null;
  unit?: null | string;
  required?: boolean;
  children: any;
  hideDivider?: boolean;
};

export type ControlArray = {
  children: any;
};
