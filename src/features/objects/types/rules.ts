export type CreateRulePayload = {
  area_guid: string;
  content: string;
};

export type UpdateRulePayload = {
  id: number;
  content: string;
};

export type GetRulePayload = {
  area_guid: string;
};

export type GetRuleResponse = {
  documents: {
    author: { id: number; title: string };
    bytes: number;
    content: string;
    created_at: string;
    extension: string;
    id: number;
    meta: null;
    name: string;
    url: string;
  }[];
};

export type Rule = {
  id: number | null;
  text: string;
  preview?: string | null;
};

export type ExportRulePayload = {
  id: number;
};
