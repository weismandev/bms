export type UpdateRentPayload = {
  area_id?: string;
  area_description?: string;
  is_rent_availability?: boolean;
  booking_approval_time?: number;
  is_need_booking_approval?: boolean;
  proof_of_use_time?: number;
  is_need_proof_of_use?: boolean;
  available_days?: number[];
  available_time?: string[];
  time_step?: number;
  is_payment_required?: boolean;
  step_cost?: number;
  paid_waiting_time?: number;
};

export type GetRentPayload = {
  area_id: string;
};

export type GetRentResponse = {
  template: {
    area_id: string;
    area_title: string;
    available_days: number[];
    available_time: string;
    is_need_booking_approval: boolean;
    is_need_proof_of_use: boolean;
    is_payment_required: boolean;
    is_rent_availability: boolean;
    paid_waiting_time: number;
    step_cost: string;
    time_step: number;
    area_description: string | null;
    booking_approval_time: number;
    proof_of_use_time: number;
  };
};

export type Rent = {
  isRentAvailability: boolean;
  description: string;
  isNeedBookingApproval: boolean;
  bookingApprovalTime: number | string;
  isNeedProofOfUse: boolean;
  proofOfUseTime: number | string;
  availableDays: number[];
  availableTimeFrom: string;
  availableTimeTo: string;
  timeStep: number;
  isPayment: boolean;
  stepCost: string;
  // paidWaitingTime: number | null; пока нет функционала оплаты
};

export type RentValueItem = {
  id: number;
  title: string;
};
