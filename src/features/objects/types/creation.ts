import {
  ExtendedObjectInstance,
  ObjectArea,
  ObjectInstance,
  ObjectType,
} from '@features/objects/types/object';

export type CreateObjectBase = {
  parent_guid: ObjectInstance['guid'];
  owner_enterprise_id: ObjectInstance['property_owner_enterprise'];
};

export type CreateBuilding = {
  type_title: ObjectInstance['type_title'];
  title: ObjectInstance['name'];
  address: ObjectInstance['address'];
  alias: ObjectInstance['alias'];
  fias: ObjectInstance['fias'];
};

export type CreateEntrance = {
  type_title: ObjectInstance['type_title'];
  number: ObjectInstance['number'];
  elevator_count: ObjectInstance['elevator_count'];
};

export type CreateFloor = {
  type_title: ObjectInstance['type_title'];
  number: ObjectInstance['number'];
};

export type CreateApartment = {
  type_title: ObjectInstance['type_title'];
  title: ObjectInstance['name'];
  number: ObjectInstance['number'];
  area: ObjectArea['area'];
  room_count: ObjectInstance['room_count'];
};

export type CreateProperty = {
  type_title: ObjectInstance['type_title'];
  title: ObjectInstance['name'];
  number: ObjectInstance['number'];
  area: ObjectArea['area'];
  is_manufacture: ObjectInstance['is_manufacture'];
  truck_access: ObjectInstance['truck_access'];
  capacity: ObjectInstance['capacity'];
  owner_enterprise_id?: ObjectInstance['property_owner_enterprise'] | { id: number };
  price: ExtendedObjectInstance['price'];
  is_rent_available: ObjectInstance['is_rent_available'];
};

export type CreateParkingLot = {
  type_title: ObjectInstance['type_title'];
  area: ObjectArea['area'];
  is_outdoor: ObjectInstance['relatedBuildings'];
  building_guids: ObjectInstance['buildings'];
};

export type CreateParkingZone = {
  type_title: ObjectInstance['type_title'];
  title: ObjectInstance['name'];
  price: ObjectInstance['price'];
  is_common_zone: ObjectInstance['is_common_zone'];
  is_for_residents_zone: ObjectInstance['is_for_residents_zone'];
  is_for_guests_zone: ObjectInstance['is_for_guests_zone'];
  is_fullness_control_zone: ObjectInstance['is_fullness_control_zone'];
};

export type CreateParkingPlace = {
  type_title: ObjectInstance['type_title'];
  number: ObjectInstance['number'];
  area: ObjectArea['area'];
  is_rent_available: ObjectInstance['is_rent_available'];
  is_private_rent_available: ObjectInstance['is_private_rent_available'];
};

export type CreateObject =
  | CreateBuilding
  | CreateEntrance
  | CreateFloor
  | CreateApartment
  | CreateProperty
  | CreateParkingLot
  | CreateParkingZone
  | CreateParkingPlace;

export type CreateObjectRequest = CreateObjectBase & CreateObject;

export type CreateObjectResponse = {
  area: ObjectInstance;
};

export type ObjectCreationMenu = Record<
  'complex' | 'parking_lot' | 'parking_zone' | 'building' | 'entrance' | 'floor',
  ObjectCreationMenuItem[]
>;

export type ObjectCreationMenuItem = { type: ObjectType; label: string };
