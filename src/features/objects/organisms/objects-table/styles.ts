import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  objects: {
    width: '100%',
    height: '100%',
  },
});
