import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  objectsFilter: {
    display: 'grid',
    gap: 15,
    padding: '15px 15px 7px 15px',
    borderRadius: 15,
    background: '#EDF6FF',
  },
  objectsFilter__form: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    alignItems: 'start',
    gap: 15,
  },
  objectsFilter__area: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    alignItems: 'center',
    gap: 15,
    paddingBottom: 8,
  },
});
