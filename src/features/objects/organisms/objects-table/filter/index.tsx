import { useUnit } from 'effector-react';
import { FC } from 'react';
import { Field, Formik } from 'formik';
import { InputField, SelectField } from '@ui/index';
import { $filters, changeFiltersDebounced, typesOptions } from '@features/objects/models';
import { ObjectsFilters } from '@features/objects/types/objectsTable';
import { useStyles } from './styles';
import { t } from 'i18next';

export const ObjectsFilter: FC = () => {
  const [filters] = useUnit([$filters]);
  const classes = useStyles();
  const onSubmit = (data: ObjectsFilters) => {
    changeFiltersDebounced(data);
  };
  return (
    <div className={classes.objectsFilter}>
      <Formik
        onSubmit={onSubmit}
        initialValues={filters}
        enableReinitialize
        render={({ setFieldValue, handleChange, submitForm }) => {
          return (
            <form className={classes.objectsFilter__form}>
              <div className={classes.objectsFilter__area}>
                <Field
                  name="area_min"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder={t('MinArea')}
                      mode="edit"
                      divider={false}
                      onChange={(e: any) => {
                        handleChange(e);
                        setTimeout(submitForm, 0);
                      }}
                    />
                  )}
                />
                <Field
                  name="area_max"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder={t('MaxArea')}
                      mode="edit"
                      divider={false}
                      onChange={(e: any) => {
                        handleChange(e);
                        setTimeout(submitForm, 0);
                      }}
                    />
                  )}
                />
              </div>
              <Field
                name="types"
                label=""
                placeholder={t('TypeOfObject')}
                render={({ field, form }: any) => (
                  <SelectField
                    field={field}
                    form={form}
                    label=""
                    placeholder={t('TypeOfObject')}
                    divider={false}
                    options={typesOptions}
                    onChange={(e: any) => {
                      setFieldValue('types', e);
                      setTimeout(submitForm, 0);
                    }}
                    isMulti
                  />
                )}
              />
            </form>
          );
        }}
      />
    </div>
  );
};
