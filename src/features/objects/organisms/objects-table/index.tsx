import { FC, ChangeEvent } from 'react';
import { useGate, useUnit } from 'effector-react';
import { Box } from '@mui/material';
import { GridRowSelectionModel, GridEventListener } from '@mui/x-data-grid-pro';
import {
  $currentObjectType,
  $isQrModalOpen,
  $objectsTable,
  $page,
  $pageSize,
  $rowsCount,
  $selectionModel,
  changeQrModalVisibility,
  createQr,
  objectsTableColumns,
  ObjectsTableGate,
  setPage,
  setPageSize,
  setSelectionModel,
  getExpandedTree,
} from '@features/objects/models';
import { QrModal } from '@features/objects/molecules/controls/qr-modal';
import { ThemeAdapter } from '@shared/theme/adapter';
import { DataGridTable } from '@shared/ui/data-grid-table';
import { ObjectsFilter } from './filter';
import { useStyles } from './styles';
import { DeleteIcon } from '@features/objects/molecules/deleteIcon';
import { $roleData } from '@features/common';
import { DeleteModal } from '@features/objects/molecules/controls/delete-modal';
import { ConfirmationModal } from '@features/objects/molecules/controls/confirmation-modal';
import { ConfirmDeleteModal } from '@features/objects/molecules/controls/confirm-delete-modal';
import { DeniedDeleteModal } from '@features/objects/molecules/controls/denied-delete-modal';

export const Objects: FC = () => {
  useGate(ObjectsTableGate);

  const [
    objects,
    page,
    rowsPerPage,
    rowsCount,
    selectionModel,
    isQrModalOpen,
    currentObjectType,
    roleData
  ] = useUnit([
    $objectsTable,
    $page,
    $pageSize,
    $rowsCount,
    $selectionModel,
    $isQrModalOpen,
    $currentObjectType,
    $roleData
  ]);
  //@ts-ignore
  const isAdmin = roleData?.is_update_locked;
  const isSelectionEnabled = currentObjectType === 'building' || isAdmin;
  const classes = useStyles();

  const handlePageChange = (_: unknown, newPage: number) => setPage(newPage);

  const handleRowSelectionModel = (newSelectionModel: GridRowSelectionModel) =>
    setSelectionModel(newSelectionModel);

  const handlePageSizeChange = (event: ChangeEvent<HTMLInputElement>) =>
    setPageSize(Number.parseInt(event.target.value, 10));

  const handleClickRow = ({ id }: { id: string }) => getExpandedTree(id);

  //@ts-ignore
  const sxHeight = `calc(100vh - ${roleData?.is_update_locked ? '339' : '285'}px)`;
  console.log(selectionModel);

  return (
    <div className={classes.objects}>
      <ObjectsFilter />

      {isAdmin && currentObjectType !== 'apartment' && <DeleteIcon />}

      <Box
        sx={{
          width: 'calc(100vw - 600px)',
          margin: '0 auto 16px',
          height: sxHeight,
        }}
      >
        <ThemeAdapter>
          <DataGridTable
            rows={objects}
            rowCount={rowsCount}
            columns={objectsTableColumns}
            checkboxSelection={isSelectionEnabled}
            disableRowSelectionOnClick
            slotProps={{
              pagination: {
                page,
                rowsPerPage,
                onPageChange: handlePageChange,
                onRowsPerPageChange: handlePageSizeChange,
              },
            }}
            rowSelectionModel={selectionModel}
            onRowSelectionModelChange={handleRowSelectionModel}
            pageSizeOptions={[25, 50, 100]}
            onRowClick={handleClickRow as GridEventListener<'rowClick'>}
          />
        </ThemeAdapter>
      </Box>

      <QrModal
        isOpen={isQrModalOpen}
        changeVisibility={changeQrModalVisibility}
        createQRPDF={createQr}
      />
      <DeleteModal />
      <ConfirmationModal />
      <ConfirmDeleteModal />
      <DeniedDeleteModal />
    </div>
  );
};
