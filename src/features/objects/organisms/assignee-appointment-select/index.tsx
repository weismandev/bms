import { useEffect, FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import {
  $assigneeAppointmentList,
  $isLoadingAppointmentList,
  fxGetListAppointment,
} from '../../models';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const AssigneesMethodSelect: FC<object> = (props) => {
  const [options, isLoading] = useUnit([
    $assigneeAppointmentList,
    $isLoadingAppointmentList,
  ]);

  useEffect(() => {
    fxGetListAppointment();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const AssigneesMethodSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<AssigneesMethodSelect />} onChange={onChange} {...props} />
  );
};
