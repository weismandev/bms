import { LinearProgress, styled } from '@mui/material';
import { TabPanel as MuiTabPanel } from '@mui/lab';

export const Container = styled('div')(() => ({
  position: 'relative',
  display: 'grid',
  gridTemplateRows: 'auto 1fr',
  alignItems: 'start',
  padding: '0 24px',
  background: '#FFFFFF',
  boxShadow: '0px 4px 15px #E7E7EC',
  borderRadius: 15,
  overflow: 'hidden',
}));

export const Loader = styled(LinearProgress)(() => ({
  position: 'absolute',
  width: '100%',
  top: 0,
}));

export const TabPanel = styled(MuiTabPanel)(() => ({
  padding: 0,
  height: '91%',
  marginTop: 10,
}));
