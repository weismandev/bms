import { FC } from 'react';
import { useUnit } from 'effector-react';
import { TabContext } from '@mui/lab';
import {
  $currentObject,
  $currentTab,
  $isRootLoading,
  tabs,
} from '@features/objects/models';
import { $isShowSearchResult } from '../../models';
import { Header } from '../header';
import { SearchingResults } from '../searching-results';
import { StartScreen } from '../start-screen';
import * as Styled from './styled';

export const Root: FC = () => {
  const [currentObject, currentTab, isRootLoading, isShowSearchResult] = useUnit([
    $currentObject,
    $currentTab,
    $isRootLoading,
    $isShowSearchResult,
  ]);

  if (isShowSearchResult) {
    return <SearchingResults />;
  }

  if (!currentObject) {
    return <StartScreen />;
  }

  const renderedTabsPanel = tabs.map((tab) => {
    const { Component } = tab;
    return (
      <Styled.TabPanel key={tab.value} value={tab.value}>
        <Component />
      </Styled.TabPanel>
    );
  });

  return (
    <Styled.Container>
      {isRootLoading ? <Styled.Loader /> : null}
      <TabContext value={currentTab}>
        <Header />
        {renderedTabsPanel}
      </TabContext>
    </Styled.Container>
  );
};
