import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  width: '100%',
  height: '100%',
  padding: '0 24px',
  background: '#FFFFFF',
  boxShadow: '0px 4px 15px #E7E7EC',
  borderRadius: 15,
}));

export const Content = styled('div')(() => ({
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  padding: '12px 16px',
  background: '#ececec',
  borderRadius: 24,
}));

export const Title = styled('div')(() => ({
  fontSize: 16,
  fontWeght: 'bold',
  color: 'rgba(112,117,121, 0.7)',
  textAlign: 'center',
}));
