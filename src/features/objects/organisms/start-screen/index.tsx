import { FC } from 'react';
import * as Styled from './styled';
import { useTranslation } from 'react-i18next';

export const StartScreen: FC = () => {
  const { t } = useTranslation();

  return (
    <Styled.Container>
      <Styled.Content>
        <Styled.Title>
          {t('SelectAnObjectFromTheList')}
        </Styled.Title>
      </Styled.Content>
    </Styled.Container>
  );
};
