import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { AreaAccounts } from '@features/objects/molecules/areaAccounts/indedx';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { InputField } from '@ui/molecules';
import { useTranslation } from 'react-i18next';

export const ApartmentForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        name: currentCard.name,
        number: currentCard.number,
        area: currentCard.area?.area,
        room_count: currentCard.room_count,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required(`${t('RequiredField')}`),
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message:
              `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        room_count: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        price: Yup.mixed().when('is_rent_available', {
          is: (rent: boolean) => Boolean(rent),
          then: Yup.number()
            .typeError(`${t('Validation.numberOnly')}`)
            .required(`${t('RequiredField')}`)
            .integer(`${t('Validation.integerOnly')}`)
            .moreThan(-1, `${t('Validation.positiveNumberOnly')}`)
            .required(`${t('RequiredField')}`),
        }),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ values, setFieldValue, handleSubmit, handleReset, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('Name')} required>
            <Field
              component={InputField}
              name="name"
              label=""
              placeholder=""
              mode="view"
              divider={false}
            />
          </Control.Row>

          {currentCard?.area_accounts?.length ? (
            <Control.Row label={t('navMenu.directories.personalAccounts')}>
              <AreaAccounts areaAccounts={currentCard.area_accounts} />
            </Control.Row>
          ) : null}

          <Control.Row label={t('Number')} required>
            <Field
              component={InputField}
              name="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('area')} required>
            <Field
              component={InputField}
              name="area"
              label=""
              type="number"
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('NumberOfRooms')}>
            <Field
              component={InputField}
              name="room_count"
              label=""
              type="number"
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>
        </Form>
      )}
    />
  );
};
