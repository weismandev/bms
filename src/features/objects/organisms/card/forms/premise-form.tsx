import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { EnterpriseSelectField } from '@features/enterprise-select';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { ImageField } from '@features/objects/molecules/image-field';
import { ObjectInstance } from '@features/objects/types/object';
import { InputField, SwitchField } from '@ui/molecules';

export const PremiseForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        image_ids: (currentCard as any)?.images[0] ?? null,
        title: currentCard.name,
        number: currentCard.number,
        area: currentCard.area?.area,
        is_manufacture: currentCard.is_manufacture,
        truck_access: currentCard.truck_access,
        capacity: currentCard.capacity,
        is_rent_available: currentCard.is_rent_available,
        price: currentCard.price,
        owner_enterprise_id: currentCard.property_owner_enterprise?.id,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message: `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ values, handleSubmit, handleReset, setFieldValue, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          {currentCard?.type_title === 'meeting_room' ? (
            <Field
              component={ImageField}
              name="image_ids"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          ) : (
            <div />
          )}
          <Control.Row label={t('Name')} required>
            <Field
              component={InputField}
              name="title"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('Number')} required>
            <Field
              component={InputField}
              name="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('area')} required>
            <Field
              component={InputField}
              name="area"
              type="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('Owner')} required>
            <Field
              component={EnterpriseSelectField}
              name="owner_enterprise_id"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
              onChange={(value: ObjectInstance['property_owner_enterprise']) => {
                setFieldValue('owner_enterprise_id', value?.id);
              }}
            />
          </Control.Row>
          <Control.Row label={t('ProductionRoom')}>
            <Field
              component={SwitchField}
              disabled={cardEditMode === 'view'}
              name="is_manufacture"
              divider={false}
              label=""
              labelPlacement="start"
            />
          </Control.Row>
          <Control.Row label={t('TruckEntrance')}>
            <Field
              component={SwitchField}
              disabled={cardEditMode === 'view'}
              name="truck_access"
              divider={false}
              label=""
              labelPlacement="start"
            />
          </Control.Row>
          <Control.Row label={t('Capacity')}>
            <Field
              component={InputField}
              name="capacity"
              type="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('AvailableForRent')}>
            <Field
              component={SwitchField}
              disabled={cardEditMode === 'view'}
              name="is_rent_available"
              divider={false}
              label=""
              labelPlacement="start"
              onChange={(e: { target: { checked: boolean } }) => {
                setFieldValue('is_rent_available', e.target.checked);
                setFieldValue('price', 0);
              }}
            />
          </Control.Row>
          {values.is_rent_available ? (
            <Control.Row label={t('RentPrice')}>
              <Field
                component={InputField}
                name="price"
                type="number"
                label=""
                placeholder=""
                mode={cardEditMode}
                divider={false}
              />
            </Control.Row>
          ) : null}
        </Form>
      )}
    />
  );
};
