import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { Form } from '@features/objects/molecules/form';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { InputField, SwitchField } from '@ui/molecules';
import { Control } from '@features/objects/molecules/control';
import { useTranslation } from 'react-i18next';

export const ParkingPlaceForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        number: currentCard.number,
        area: currentCard.area?.area,
        is_rent_available: currentCard.is_rent_available,
        is_private_rent_available: currentCard.is_private_rent_available,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        number: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ values, handleSubmit, handleReset, setFieldValue, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('Number')} required>
            <Field
              component={InputField}
              name="number"
              type="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('area')} required>
            <Field
              component={InputField}
              name="area"
              type="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('PossibilityRenting')}>
            <Field
              component={SwitchField}
              name="is_rent_available"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
              onChange={(e: { target: { checked: boolean } }) => {
                setFieldValue('is_rent_available', e.target.checked);
                if (!values.is_private_rent_available) {
                  setFieldValue('price', 0);
                }
              }}
            />
          </Control.Row>

          <Control.Row label={t('PossibilityRentingIndividuals')}>
            <Field
              component={SwitchField}
              name="is_private_rent_available"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
              onChange={(e: { target: { checked: boolean } }) => {
                setFieldValue('is_private_rent_available', e.target.checked);
                if (!values.is_rent_available) {
                  setFieldValue('price', 0);
                }
              }}
            />
          </Control.Row>
        </Form>
      )}
    />
  );
};
