import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
  $objects,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { ImageField } from '@features/objects/molecules/image-field';
import { InputField } from '@ui/molecules';
import { ObjectBuilding } from '../../../types/object';
import { BuildingsSelectField } from '../../buildings-select';
import { ParkingRelatedBuildingsSelectField } from '../../parking-related-buildings-select';

export const ParkingLotForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode, objects] = useUnit([
    $currentCard,
    $cardEditMode,
    $objects,
  ]);

  if (!currentCard) return null;

  const building = currentCard?.building_title
    ? Object.values(objects).find(
        (object) => object?.title === currentCard.building_title
      )
    : null;

  const formattedBuilding = building
    ? [{ id: building?.guid, title: building?.title }]
    : [];

  return (
    <Formik
      initialValues={{
        image: currentCard.image,
        title: currentCard.name,
        area: currentCard?.area?.area || 0,
        is_outdoor: currentCard?.is_outdoor ? 1 : 2,
        building_guids: Array.isArray(currentCard?.buildings)
          ? currentCard.buildings.map((building) => ({
              id: (building as any).guid,
              title: building.title,
            }))
          : [],
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        is_outdoor: Yup.mixed().required(`${t('RequiredField')}`),
        building_guids: Yup.array().test(
          'buildings-required',
          t('RequiredField') ?? '',
          (value, { parent: { is_outdoor } }) => {
            const isOutdoorValue =
              typeof is_outdoor === 'object' ? is_outdoor.id : is_outdoor;

            if (isOutdoorValue === 2) {
              return Array.isArray(value) && value.length > 0;
            }

            return true;
          }
        ),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ handleSubmit, handleReset, isValid, dirty, setFieldValue, values }) => {
        const isOutdoorValue =
          typeof values.is_outdoor === 'object'
            ? values.is_outdoor.id
            : values.is_outdoor;

        const onChangeRelatedBuildings = (value: ObjectBuilding) => {
          setFieldValue('is_outdoor', value);
          setFieldValue('building_guids', []);

          if (building && value?.id === 2) {
            setFieldValue('building_guids', formattedBuilding);
          }
        };

        return (
          <Form
            editMode={cardEditMode}
            setEditMode={setCardEditMode}
            isValid={isValid}
            dirty={dirty}
            onSubmit={() => {
              handleSubmit();
            }}
            reset={() => {
              handleReset();
            }}
          >
            <Field
              component={ImageField}
              name="image"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
            <Control.Row label={t('Name')} required>
              <Field
                component={InputField}
                name="title"
                label=""
                placeholder=""
                mode={cardEditMode}
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('area')}>
              <Field
                component={InputField}
                name="area"
                type="number"
                label=""
                placeholder=""
                mode={cardEditMode}
                divider={false}
              />
            </Control.Row>
            <Control.Row
              label={t('ParkingRelatedBuildings')}
              hideDivider={isOutdoorValue === 2}
              required
            >
              <Field
                component={ParkingRelatedBuildingsSelectField}
                name="is_outdoor"
                label=""
                placeholder=""
                mode={cardEditMode}
                isClearable={false}
                divider={false}
                helpText={
                  isOutdoorValue === 2
                    ? t('TheParkingLotWellTheParkingZones')
                    : t('TheParkingLotAsWellAsTheParking')
                }
                onChange={onChangeRelatedBuildings}
              />
            </Control.Row>
            <div>
              {isOutdoorValue === 2 && (
                <Control.Row label={null}>
                  <Field
                    component={BuildingsSelectField}
                    name="building_guids"
                    isClearable={formattedBuilding.length === 0}
                    label=""
                    placeholder=""
                    mode={cardEditMode}
                    divider={false}
                    isMulti
                    notClearValues={
                      formattedBuilding.length > 0 ? formattedBuilding : null
                    }
                  />
                </Control.Row>
              )}
            </div>
          </Form>
        );
      }}
    />
  );
};
