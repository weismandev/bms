import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { MapField } from '@features/objects/molecules/controls/map-field';
import { Form } from '@features/objects/molecules/form';
// import { TimeZoneExtendedSelectField } from '@ui/atoms';
import { ImageField } from '@features/objects/molecules/image-field';
import { RegionSelectField } from '@features/region-select';
import { InputField } from '@ui/molecules';
import { AssigneesMethodSelectField } from '../../assignee-appointment-select';
import { useTranslation } from 'react-i18next';

export const ComplexForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        image: currentCard.image,
        title: currentCard.name,
        locality_id: currentCard.locality_id,
        // utc: currentCard.utc,
        coordinates: currentCard?.coordinates,
        updated_at: currentCard.updated_at,
        assignee_appointment_method: currentCard?.assignee_appointment_method_id,
      }}
      validationSchema={Yup.object().shape({
        title: Yup.mixed().required(`${t('RequiredField')}`),
        locality_id: Yup.mixed().required(`${t('RequiredField')}`),
        // utc: Yup.mixed().required('Обязательное поле'),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ handleSubmit, handleReset, setFieldValue, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Field
            component={ImageField}
            name="image"
            label=""
            placeholder=""
            mode={cardEditMode}
            divider={false}
          />

          <Control.Row label={t('Name')} required>
            <Field
              component={InputField}
              name="title"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('Label.city')} required>
            <Field
              component={RegionSelectField}
              onChange={(value: { id: number }) => {
                setFieldValue('locality_id', value?.id);
              }}
              name="locality_id"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('AppointmentExecutor')}>
            <Field
              component={AssigneesMethodSelectField}
              name="assignee_appointment_method"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          {/* <Control.Row label="Часовой пояс" required>
            <Field
              component={TimeZoneExtendedSelectField}
              name="utc"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row> */}

          <Field
            component={MapField}
            name="coordinates"
            label=""
            placeholder=""
            mode={cardEditMode}
            divider={false}
          />
        </Form>
      )}
    />
  );
};
