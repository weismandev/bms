import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { Form } from '@features/objects/molecules/form';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { InputField, SwitchField } from '@ui/molecules';
import { Control } from '@features/objects/molecules/control';
import { useTranslation } from 'react-i18next';

export const ParkingZoneForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        title: currentCard.name,
        price: currentCard.price,
        is_common_zone: currentCard.is_common_zone,
        is_for_residents_zone: currentCard.is_for_residents_zone,
        is_for_guests_zone: currentCard.is_for_guests_zone,
        is_fullness_control_zone: currentCard.is_fullness_control_zone,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        price: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`)
          .required(`${t('RequiredField')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ handleSubmit, handleReset, setFieldValue, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('Name')} required>
            <Field
              component={InputField}
              name="title"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('RentPrice')} required>
            <Field
              component={InputField}
              type="number"
              name="price"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('CommonArea')}>
            <Field
              component={SwitchField}
              name="is_common_zone"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('ResidentsArea')}>
            <Field
              component={SwitchField}
              name="is_for_residents_zone"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('GuestArea')}>
            <Field
              component={SwitchField}
              name="is_for_guests_zone"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('FullControlZone')}>
            <Field
              component={SwitchField}
              name="is_fullness_control_zone"
              label=""
              labelPlacement="start"
              disabled={cardEditMode === 'view'}
              divider={false}
            />
          </Control.Row>
        </Form>
      )}
    />
  );
};
