import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { InputField } from '@ui/molecules';
import { useTranslation } from 'react-i18next';

export const FloorForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        number: currentCard.number,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message:
              `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ handleSubmit, handleReset, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('Number')} required>
            <Field
              component={InputField}
              name="number"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('Apartments')}>{currentCard.apartment_count}</Control.Row>
          <Control.Row label={t('ApartmentsArea')} unit={currentCard.apartment_area?.units}>
            {currentCard.apartment_area?.area}
          </Control.Row>
          <Control.Row label={t('OfficeRooms')}>{currentCard.office_count}</Control.Row>
          <Control.Row
            label={t('OfficeArea')}
            unit={currentCard.office_area?.units}
          >
            {currentCard.office_area?.area}
          </Control.Row>
          <Control.Row label={t('OtherPremises')}>
            {currentCard.other_facility_count}
          </Control.Row>
          <Control.Row
            label={t('OtherPremisesArea')}
            unit={currentCard.other_facility_area?.units}
          >
            {currentCard.other_facility_area?.area}
          </Control.Row>
        </Form>
      )}
    />
  );
};
