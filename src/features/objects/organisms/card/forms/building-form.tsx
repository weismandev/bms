import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $cardEditMode,
  $currentCard,
  saveCard,
  setCardEditMode,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { MapField } from '@features/objects/molecules/controls/map-field';
import { Form } from '@features/objects/molecules/form';
import { ImageField } from '@features/objects/molecules/image-field';
import { InputField } from '@ui/molecules';
import { useTranslation } from 'react-i18next';

export const BuildingForm: FC = () => {
  const { t } = useTranslation();
  const [currentCard, cardEditMode] = useUnit([$currentCard, $cardEditMode]);

  if (!currentCard) return null;

  return (
    <Formik
      initialValues={{
        image: currentCard.image,
        title: currentCard.name,
        address: currentCard.address,
        alias: currentCard.alias,
        fias: currentCard.fias,
        area: currentCard.area?.area,
        coordinates: currentCard.coordinates,
        updated_at: currentCard.updated_at,
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        address: Yup.string().required(`${t('RequiredField')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        saveCard(values);
      }}
      render={({ handleSubmit, handleReset, isValid, dirty }) => (
        <Form
          editMode={cardEditMode}
          setEditMode={setCardEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Field
            component={ImageField}
            name="image"
            label=""
            placeholder=""
            mode={cardEditMode}
            divider={false}
          />

          <Control.Row label={t('Name')} required>
            <Field
              component={InputField}
              name="title"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('Label.address')} required>
            <Field
              component={InputField}
              name="address"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('Alias')}>
            <Field
              component={InputField}
              name="alias"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('FIAS')}>
            <Field
              component={InputField}
              name="fias"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('area')} unit={currentCard.area?.units}>
            <Field
              component={InputField}
              name="area"
              label=""
              placeholder=""
              mode={cardEditMode}
              divider={false}
              type="number"
            />
          </Control.Row>

          <Control.Row label={t('Entrances')}>{currentCard.entrance_count}</Control.Row>

          {currentCard.passage_count ? (
            <>
              <Control.Row label={t('Transitions')}>{currentCard.passage_count}</Control.Row>
              <Control.Row
                label={t('TransitionsArea')}
                unit={currentCard.passage_area?.units}
              >
                {currentCard.passage_area?.area}
              </Control.Row>
            </>
          ) : null}

          <Control.Row label={t('Floors')}>{currentCard.floor_count}</Control.Row>
          <Control.Row label={t('Elevators')}>{currentCard.elevator_count}</Control.Row>
          <Control.Row label={t('Apartments')}>{currentCard.apartment_count}</Control.Row>
          <Control.Row label={t('ApartmentsArea')} unit={currentCard.apartment_area?.units}>
            {currentCard.apartment_area?.area}
          </Control.Row>
          <Control.Row label={t('OfficeRooms')}>{currentCard.office_count}</Control.Row>
          <Control.Row
            label={t('OfficeArea')}
            unit={currentCard.office_area?.units}
          >
            {currentCard.office_area?.area}
          </Control.Row>
          <Control.Row label={t('OtherPremises')}>
            {currentCard.other_facility_count}
          </Control.Row>
          <Control.Row
            label={t('OtherPremisesArea')}
            unit={currentCard.other_facility_area?.units}
          >
            {currentCard.other_facility_area?.area}
          </Control.Row>
          <Control.Row label={t('ParkingSpaces')}>{currentCard.parking_place_count}</Control.Row>

          <Field
            component={MapField}
            name="coordinates"
            label=""
            placeholder=""
            mode={cardEditMode}
            divider={false}
          />
        </Form>
      )}
    />
  );
};
