import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { $currentCard, CardGate } from '@features/objects/models';
import { ApartmentForm } from './forms/apartment-form';
import { BuildingForm } from './forms/building-form';
import { ComplexForm } from './forms/complex-form';
import { EntranceForm } from './forms/entrance-form';
import { FloorForm } from './forms/floor-form';
import { ParkingLotForm } from './forms/parking-lot-form';
import { ParkingPlaceForm } from './forms/parking-place-form';
import { ParkingZoneForm } from './forms/parking-zone-form';
import { PremiseForm } from './forms/premise-form';
import * as Styled from './styled';

export const Card: FC = () => {
  useGate(CardGate);

  const [currentCard] = useUnit([$currentCard]);
  const currentType = currentCard?.type_title;

  const renderForm = () => {
    switch (currentType) {
      case 'complex':
        return <ComplexForm />;
      case 'building':
        return <BuildingForm />;
      case 'entrance':
        return <EntranceForm />;
      case 'passage':
      case 'floor':
        return <FloorForm />;
      case 'apartment':
        return <ApartmentForm />;
      case 'office':
      case 'coworking':
      case 'pantry':
      case 'property_place':
      case 'workplace':
      case 'meeting_room':
      case 'room':
        return <PremiseForm />;
      case 'parking_lot':
        return <ParkingLotForm />;
      case 'parking_zone':
        return <ParkingZoneForm />;
      case 'parking_place':
        return <ParkingPlaceForm />;
      default:
        return null;
    }
  };

  return <Styled.CardContainer>{renderForm()}</Styled.CardContainer>;
};
