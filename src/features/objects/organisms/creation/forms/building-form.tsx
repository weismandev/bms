import { FC } from 'react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { ActionButton, CustomScrollbar, InputField } from '@ui/index';
import { buildingCreationFields, createObject } from '@features/objects/models';
import { ImageField } from '@features/objects/molecules/image-field';
import { MapField } from '@features/objects/molecules/controls/map-field';
import { Control } from '@features/objects/molecules/control';
import * as Styled from '../styled';
import { useTranslation } from 'react-i18next';

export const BuildingForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={buildingCreationFields}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        address: Yup.string().required(`${t('RequiredField')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit }) => (
        <>
          <Styled.SaveButton />
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>

          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            <Field
              component={ImageField}
              name="image_id"
              label=""
              placeholder=""
              mode="edit"
              divider={false}
            />

            <Control.Row label={t('Name')} required>
              <Field
                component={InputField}
                name="title"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('Label.address')} required>
              <Field
                component={InputField}
                name="address"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('Alias')}>
              <Field
                component={InputField}
                name="alias"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('FIAS')}>
              <Field
                component={InputField}
                name="fias"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Field
              component={MapField}
              name="coordinates"
              label=""
              placeholder=""
              mode="edit"
              divider={false}
            />
          </CustomScrollbar>
        </>
      )}
    />
  )
};
