import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { EnterpriseSelectField } from '@features/enterprise-select';
import {
  createObject,
  propertyCreationFields,
  propertyTypeOptions,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { ImageField } from '@features/objects/molecules/image-field';
import {
  ActionButton,
  CustomScrollbar,
  InputField,
  SelectField,
  SwitchField,
} from '@ui/index';

export const PropertyForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={propertyCreationFields}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message: `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
        capacity: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        price: Yup.mixed().when('is_rent_available', {
          is: (rent: boolean) => Boolean(rent),
          then: Yup.number()
            .typeError(`${t('Validation.numberOnly')}`)
            .required(`${t('RequiredField')}`)
            .integer(`${t('Validation.integerOnly')}`)
            .moreThan(-1, `${t('Validation.positiveNumberOnly')}`)
            .required(`${t('RequiredField')}`),
        }),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ values, handleSubmit, setFieldValue }) => (
        <>
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>
          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            {values?.type_title === 'meeting_room' ? (
              <Field
                component={ImageField}
                name="image_ids"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            ) : (
              <div />
            )}
            <Control.Row label={t('RoomType')} required>
              <Field
                component={SelectField}
                options={propertyTypeOptions}
                onChange={(value: { id: number }) => {
                  setFieldValue('type_title', value.id);
                  setFieldValue('image_ids', null);
                }}
                name="type_title"
                type="select"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('Name')} required>
              <Field
                component={InputField}
                name="title"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('Owner')}>
              <Field
                component={EnterpriseSelectField}
                name="owner_enterprise_id"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('Number')} required>
              <Field
                component={InputField}
                name="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('area')} required>
              <Field
                component={InputField}
                name="area"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
            <Control.Row label={t('PossibilityRentingPremises')}>
              <Field
                component={SwitchField}
                name="is_rent_available"
                divider={false}
                label=""
                labelPlacement="start"
                onChange={(e: { target: { checked: boolean } }) => {
                  setFieldValue('is_rent_available', e.target.checked);
                  setFieldValue('price', 0);
                }}
              />
            </Control.Row>
            {values.is_rent_available ? (
              <Control.Row label={t('RentPrice')}>
                <Field
                  component={InputField}
                  name="price"
                  type="number"
                  label=""
                  placeholder=""
                  mode="edit"
                  divider={false}
                />
              </Control.Row>
            ) : (
              <div />
            )}
            <Control.Row label={t('TruckEntrance')}>
              <Field
                component={SwitchField}
                name="truck_access"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>
            <Control.Row label={t('Capacity')}>
              <Field
                component={InputField}
                name="capacity"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
          </CustomScrollbar>
        </>
      )}
    />
  );
};
