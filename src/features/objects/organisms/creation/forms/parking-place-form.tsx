import { FC } from 'react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { ActionButton, CustomScrollbar, InputField, SwitchField } from '@ui/index';
import { createObject, parkingPlaceCreationFields } from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { useTranslation } from 'react-i18next';

export const ParkingPlaceForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={parkingPlaceCreationFields}
      validationSchema={Yup.object().shape({
        number: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit }) => (
        <>
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>

          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            <Control.Row label={t('Number')} required>
              <Field
                component={InputField}
                name="number"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('area')} required>
              <Field
                component={InputField}
                name="area"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('AvailableForRent')}>
              <Field
                component={SwitchField}
                name="is_rent_available"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>

            <Control.Row label={t('AvailabilityForPrivateRenting')}>
              <Field
                component={SwitchField}
                name="is_private_rent_available"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>
          </CustomScrollbar>
        </>
      )}
    />
  );
};
