import { FC } from 'react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { ActionButton, CustomScrollbar, InputField, SwitchField } from '@ui/index';
import { createObject, parkingZoneCreationFields } from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { useTranslation } from 'react-i18next';

export const ParkingZoneForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={parkingZoneCreationFields}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        price: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`)
          .required(`${t('RequiredField')}`),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit }) => (
        <>
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>

          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            <Control.Row label={t('Name')} required>
              <Field
                component={InputField}
                name="title"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('RentPrice')} required>
              <Field
                component={InputField}
                name="price"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('CommonArea')}>
              <Field
                component={SwitchField}
                name="is_common_zone"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>

            <Control.Row label={t('ResidentsArea')}>
              <Field
                component={SwitchField}
                name="is_for_residents_zone"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>

            <Control.Row label={t('GuestArea')}>
              <Field
                component={SwitchField}
                name="is_for_guests_zone"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>

            <Control.Row label={t('FullControlZone')}>
              <Field
                component={SwitchField}
                name="is_fullness_control_zone"
                divider={false}
                label=""
                labelPlacement="start"
              />
            </Control.Row>
          </CustomScrollbar>
        </>
      )}
    />
  );
};
