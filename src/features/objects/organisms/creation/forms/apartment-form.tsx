import { FC } from 'react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { apartmentCreationFields, createObject } from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { ActionButton, CustomScrollbar, InputField } from '@ui/index';
import { useTranslation } from 'react-i18next';

export const ApartmentForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={apartmentCreationFields}
      validationSchema={Yup.object().shape({
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message:
              `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .required(`${t('RequiredField')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        room_count: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`)
          .integer(`${t('Validation.integerOnly')}`)
          .nullable(true),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit }) => (
        <>
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>

          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            <Control.Row label={t('Number')} required>
              <Field
                component={InputField}
                name="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('area')} required>
              <Field
                component={InputField}
                name="area"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('NumberOfRooms')}>
              <Field
                component={InputField}
                name="room_count"
                type="number"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>
          </CustomScrollbar>
        </>
      )}
    />
  );
};
