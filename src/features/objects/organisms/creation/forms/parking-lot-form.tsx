import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  createObject,
  parkingLotCreationFields,
  $building,
  $currentObject,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { ImageField } from '@features/objects/molecules/image-field';
import { ActionButton, CustomScrollbar, InputField } from '@ui/index';
import { ObjectBuilding } from '../../../types/object';
import { BuildingsSelectField } from '../../buildings-select';
import { ParkingRelatedBuildingsSelectField } from '../../parking-related-buildings-select';

export const ParkingLotForm: FC = () => {
  const { t } = useTranslation();
  const [building, currentObject] = useUnit([$building, $currentObject]);

  const isOutdoor = currentObject?.type === 'floor' ? 2 : 1;
  const buildingGuids = currentObject?.type === 'floor' ? [building] : [];

  const initialValue = {
    ...parkingLotCreationFields,
    is_outdoor: isOutdoor,
    building_guids: buildingGuids,
  };

  return (
    <Formik
      initialValues={initialValue}
      validationSchema={Yup.object().shape({
        title: Yup.string().required(`${t('RequiredField')}`),
        area: Yup.number()
          .typeError(`${t('Validation.numberOnly')}`)
          .moreThan(-1, `${t('Validation.positiveNumberOnly')}`),
        is_outdoor: Yup.mixed().required(`${t('RequiredField')}`),
        building_guids: Yup.array().test(
          'buildings-required',
          t('RequiredField') ?? '',
          (value, { parent: { is_outdoor } }) => {
            const isOutdoorValue =
              typeof is_outdoor === 'object' ? is_outdoor.id : is_outdoor;

            if (isOutdoorValue === 2) {
              return Array.isArray(value) && value.length > 0;
            }

            return true;
          }
        ),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit, values, setFieldValue }) => {
        const isOutdoorValue =
          typeof values.is_outdoor === 'object'
            ? values.is_outdoor.id
            : values.is_outdoor;

        const onChangeRelatedBuildings = (value: ObjectBuilding) => {
          setFieldValue('is_outdoor', value);
          setFieldValue('building_guids', []);

          if (currentObject?.type === 'floor' && value?.id === 2) {
            setFieldValue('building_guids', [building]);
          }
        };

        return (
          <>
            <ActionButton
              onClick={() => {
                handleSubmit();
              }}
              kind="positive"
              style={{
                position: 'absolute',
                top: 18,
                right: 24,
                padding: '0 16px',
                boxShadow: 'none',
              }}
            >
              <span>{t('Save')}</span>
            </ActionButton>
            <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
              <Field
                component={ImageField}
                name="image_id"
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
              <Control.Row label={t('Name')} required>
                <Field
                  component={InputField}
                  name="title"
                  label=""
                  placeholder=""
                  mode="edit"
                  divider={false}
                />
              </Control.Row>
              <Control.Row label={t('area')}>
                <Field
                  component={InputField}
                  name="area"
                  type="number"
                  label=""
                  placeholder=""
                  mode="edit"
                  divider={false}
                />
              </Control.Row>
              <Control.Row
                label={t('ParkingRelatedBuildings')}
                hideDivider={isOutdoorValue === 2}
                required
              >
                <Field
                  component={ParkingRelatedBuildingsSelectField}
                  name="is_outdoor"
                  label=""
                  placeholder=""
                  mode="edit"
                  isClearable={false}
                  divider={false}
                  helpText={
                    isOutdoorValue === 2
                      ? t('TheParkingLotWellTheParkingZones')
                      : t('TheParkingLotAsWellAsTheParking')
                  }
                  onChange={onChangeRelatedBuildings}
                />
              </Control.Row>
              <div>
                {isOutdoorValue === 2 && (
                  <Control.Row label={null}>
                    <Field
                      component={BuildingsSelectField}
                      name="building_guids"
                      isClearable={buildingGuids.length === 0}
                      label=""
                      placeholder=""
                      mode="edit"
                      divider={false}
                      isMulti
                      notClearValues={buildingGuids.length > 0 ? buildingGuids : null}
                    />
                  </Control.Row>
                )}
              </div>
            </CustomScrollbar>
          </>
        );
      }}
    />
  );
};
