import { FC } from 'react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { createObject, entranceCreationFields } from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { ActionButton, CustomScrollbar, InputField } from '@ui/index';
import { useTranslation } from 'react-i18next';

export const EntranceForm: FC = () => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={entranceCreationFields}
      validationSchema={Yup.object().shape({
        number: Yup.string()
          .matches(/^([a-zа-яё]+|\d+|[-/№.\s]+)+$/gi, {
            message:
              `${t('Validation.mayConsist')}`,
          })
          .max(10, `${t('Validation.mayConsistOnlyTenSigns')}`)
          .test('number-required', `${t('RequiredField')}`, (data) =>
            data ? Boolean(data.trim()) : false
          ),
      })}
      enableReinitialize
      onSubmit={(values) => {
        createObject(values);
      }}
      render={({ handleSubmit }) => (
        <>
          <ActionButton
            onClick={() => {
              handleSubmit();
            }}
            kind="positive"
            style={{
              position: 'absolute',
              top: 18,
              right: 24,
              padding: '0 16px',
              boxShadow: 'none',
            }}
          >
            <span>{t('Save')}</span>
          </ActionButton>

          <CustomScrollbar style={{ height: 'calc(100vh - 220px)' }}>
            <Control.Row label={t('Number')} required>
              <Field
                name="number"
                component={InputField}
                label=""
                placeholder=""
                mode="edit"
                divider={false}
              />
            </Control.Row>

            <Control.Row label={t('Elevators')}>
              <Field
                name="elevator_count"
                render={({ field, form }: any) => (
                  <InputField
                    field={field}
                    form={form}
                    type="number"
                    label=""
                    placeholder=""
                    mode={'edit'}
                    divider={false}
                  />
                )}
              />
            </Control.Row>
          </CustomScrollbar>
        </>
      )}
    />
  );
};
