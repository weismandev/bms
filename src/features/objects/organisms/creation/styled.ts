import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  position: 'relative',
  display: 'grid',
  gap: 30,
  alignContent: 'start',
  padding: '15px 24px 0 24px',
  background: '#FFFFFF',
  boxShadow: '0px 4px 15px #E7E7EC',
  borderRadius: 15,
}));

export const Header = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto 1fr auto',
  gap: 5,
  alignItems: 'center',
}));

export const Title = styled('span')(() => ({
  fontSize: 18,
  fontWeight: 700,
  width: '100%',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'elipsis',
}));

export const Content = styled('div')(() => ({
  display: 'grid',
  alignContent: 'start',
  alignItems: 'start',
  gap: 5,
  width: '100%',
  height: '100%',
  maxWidth: 888,
  margin: '0 auto',
}));

export const SaveButton = styled('div')(() => ({
  position: 'absolute',
  top: 18,
  right: 24,
  padding: '0 16px',
  boxShadow: 'none',
}));
