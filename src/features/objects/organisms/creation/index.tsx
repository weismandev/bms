import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { IconButton } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {
  $creationType,
  $isCreationLoading,
  closeCreation,
  CreationGate,
} from '@features/objects/models';
import { Loader } from '@ui/index';
import { BuildingForm } from './forms/building-form';
import { EntranceForm } from './forms/entrance-form';
import { FloorForm } from './forms/floor-form';
import { ApartmentForm } from './forms/apartment-form';
import { PropertyForm } from './forms/property-form';
import { ParkingLotForm } from './forms/parking-lot-form';
import { ParkingZoneForm } from './forms/parking-zone-form';
import { ParkingPlaceForm } from './forms/parking-place-form';
import * as Styled from './styled';
import { useTranslation } from 'react-i18next';

export const Creation: FC = () => {
  const { t } = useTranslation();
  useGate(CreationGate);

  const [creationType, isCreationLoading] = useUnit([$creationType, $isCreationLoading]);

  const handleBackClick = () => {
    closeCreation();
  };

  const renderForm = () => {
    switch (creationType) {
      case 'building':
        return <BuildingForm />;
      case 'entrance':
        return <EntranceForm />;
      case 'floor':
        return <FloorForm />;
      case 'apartment':
        return <ApartmentForm />;
      case 'property':
        return <PropertyForm />;
      case 'parking_lot':
        return <ParkingLotForm />;
      case 'parking_zone':
        return <ParkingZoneForm />;
      case 'parking_place':
        return <ParkingPlaceForm />;
      default:
        return null;
    }
  };

  const renderPageTitle = () => {
    switch (creationType) {
      case 'building':
        return t('BuildingCreate');
      case 'entrance':
        return t('EntranceCreate');
      case 'floor':
        return t('FloorCreate');
      case 'apartment':
        return t('ApartmentCreate');
      case 'property':
        return t('RoomCreate');
      case 'parking_lot':
        return t('ParkingCreate');
      case 'parking_zone':
        return t('ParkingZoneCreate');
      case 'parking_place':
        return t('ParkingSpaceCreate');

      default:
        return null;
    }
  };

  const renderLoader = () => (isCreationLoading ? <Loader isLoading /> : null);

  return (
    <Styled.Container>
      {renderLoader()}
      <Styled.Header>
        <IconButton aria-label="назад" onClick={handleBackClick}>
          <ArrowBackIcon />
        </IconButton>

        <Styled.Title>{renderPageTitle()}</Styled.Title>
      </Styled.Header>

      <Styled.Content>{renderForm()}</Styled.Content>
    </Styled.Container>
  );
};
