import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles({
  archiveButton: {
    backgroundColor: '#EC7F00',
    '&:disabled': {
      backgroundColor: '#E7E7E7',
    },
    '&:hover': {
      backgroundColor: '#EC7F00',
    },
  },
});
