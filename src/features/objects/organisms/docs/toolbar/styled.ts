import { styled } from '@mui/material';

export const Toolbar = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  padding: 10,
}));

export const SelectWrapper = styled('div')(() => ({
  width: 200,
}));

export const Actions = styled('div')(() => ({
  display: 'flex',
  gap: 10,
}));
