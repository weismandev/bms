import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import ReactFileReader from 'react-file-reader';
import { useUnit } from 'effector-react';
import {
  $docsSelectRow,
  $docsStatuses,
  $selectedDocStatus,
  setSelectedDocStatus,
  uploadDoc,
  $isDocsLoading,
  exportDocuments,
  changedVisibilityDeleteDocsModal,
  changedVisibilityArchiveDocsModal,
} from '@features/objects/models';
import { StatusDocument } from '@features/objects/types/docs';
import {
  AddButton,
  SelectControl,
  ExportButton,
  DeleteButton,
  ArchiveButton,
} from '@ui/index';
import * as Styled from './styled';
import { useStyles } from './styles';

export const Toolbar: FC = () => {
  const [selectionModel, docsStatuses, selectedDocStatus, isDocsLoading] = useUnit([
    $docsSelectRow,
    $docsStatuses,
    $selectedDocStatus,
    $isDocsLoading,
  ]);
  const { t } = useTranslation();

  const classes = useStyles();

  const isDisabledActions = selectionModel.length === 0 || isDocsLoading;

  const onChangeStatus = (value: StatusDocument) => setSelectedDocStatus(value);

  const handleFiles = (files: File[]) => {
    if (files[0]) {
      uploadDoc(files[0]);
    }
  };

  const onExport = () => exportDocuments(selectionModel);
  const onDeleteDocs = () => changedVisibilityDeleteDocsModal(true);
  const onArchive = () => changedVisibilityArchiveDocsModal(true);
  const onUnarchive = () => changedVisibilityArchiveDocsModal(true);

  return (
    <Styled.Toolbar>
      <Styled.Actions>
        {selectedDocStatus?.id === 1 && (
          <>
            <ReactFileReader
              fileTypes={['.pdf', '.doc', '.docx', '.jpg', '.jpeg', '.png']}
              handleFiles={handleFiles}
              disabled={isDocsLoading}
            >
              <AddButton
                disabled={isDocsLoading}
                titleAccess={t('docUpload')}
                style={{ order: 10 }}
              />
            </ReactFileReader>
            <ExportButton
              active
              disabled={isDisabledActions}
              onClick={onExport}
              style={{ order: 30 }}
            />
            <ArchiveButton
              disabled={isDisabledActions}
              onClick={onArchive}
              className={classes.archiveButton}
              style={{ order: 40 }}
            />
          </>
        )}
        {selectedDocStatus?.id === 2 && (
          <ArchiveButton
            disabled={isDisabledActions}
            onClick={onUnarchive}
            className={classes.archiveButton}
            isArchived
            style={{ order: 10 }}
          />
        )}
        <DeleteButton
          disabled={isDisabledActions}
          onClick={onDeleteDocs}
          style={{ order: 20 }}
        />
      </Styled.Actions>
      <Styled.SelectWrapper>
        <SelectControl
          placeholder={t('status')}
          label={null}
          divider={false}
          options={docsStatuses}
          value={selectedDocStatus}
          onChange={onChangeStatus}
          isClearable={false}
        />
      </Styled.SelectWrapper>
    </Styled.Toolbar>
  );
};
