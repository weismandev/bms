import { Box as BoxMui, styled } from '@mui/material';
import { DataGridPro } from '@mui/x-data-grid-pro';

export const Box = styled(BoxMui)(() => ({
  width: 'calc(100vw - 600px)',
  margin: '0 auto 16px',
  height: 'calc(100vh - 285px)',
}));

export const DataGrid = styled(DataGridPro)(() => ({
  cursor: 'pointer',
}));
