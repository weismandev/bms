import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Close, ArchiveOutlined, UnarchiveOutlined } from '@mui/icons-material';
import {
  $isOpenArchiveDocsModal,
  changedVisibilityArchiveDocsModal,
  archiveDocs,
  $selectedDocStatus,
  unarchiveDocs,
} from '@features/objects/models';
import { ActionButton, CloseButton, Modal } from '@ui/index';
import * as Styled from './styled';

export const ArchiveConfirmDialog: FC = () => {
  const [isOpen, selectedDocStatus] = useUnit([
    $isOpenArchiveDocsModal,
    $selectedDocStatus,
  ]);

  const { t } = useTranslation();

  const isArchive = selectedDocStatus?.id === 2 || false;

  const onClose = () => changedVisibilityArchiveDocsModal(false);

  const header = (
    <Styled.Header>
      <span>{isArchive ? t('docsUnarchive') : t('docsArchive')}</span>
      <CloseButton onClick={onClose} />
    </Styled.Header>
  );

  const actions = (
    <Styled.Toolbar disableGutters variant="dense">
      <ActionButton
        icon={isArchive ? <UnarchiveOutlined /> : <ArchiveOutlined />}
        onClick={isArchive ? unarchiveDocs : archiveDocs}
        style={{ backgroundColor: '#EC7F00' }}
      >
        {isArchive ? t('Unarchive') : t('Archive')}
      </ActionButton>
      <ActionButton icon={<Close />} onClick={onClose}>
        {t('Cancellation')}
      </ActionButton>
    </Styled.Toolbar>
  );

  if (!isOpen) {
    return null;
  }

  return (
    <Modal
      header={header}
      content={
        isArchive
          ? t('docsFromArchiveConfirm')
          : t('docsToArchiveConfirm')
      }
      actions={actions}
      isOpen={isOpen}
      onClose={onClose}
    />
  );
};
