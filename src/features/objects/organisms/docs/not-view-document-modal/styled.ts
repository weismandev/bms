import { styled, Toolbar as MuiToolbar } from '@mui/material';

export const Header = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'space-between',
}));

export const Toolbar = styled(MuiToolbar)(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  gap: 10,
}));
