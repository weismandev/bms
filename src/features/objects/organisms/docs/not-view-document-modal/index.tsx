import { FC } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Close, VerticalAlignBottom } from '@mui/icons-material';
import {
  $notViewDocumentDataModal,
  changedNotViewDocumentModalData,
  exportDocument,
} from '@features/objects/models';
import { ActionButton, CloseButton, Modal } from '@ui/index';
import * as Styled from './styled';

export const NotViewDocumentModal: FC = () => {
  const { isOpen, id, title, type } = useUnit($notViewDocumentDataModal);
  const { t } = useTranslation();

  const onClose = () =>
    changedNotViewDocumentModalData({ isOpen: false, id: null, title: '', type: '' });

  const onExport = () => exportDocument({ id: id as number, title, type, export: 1 });

  const header = (
    <Styled.Header>
      <span>{t('Error')}</span>
      <CloseButton onClick={onClose} />
    </Styled.Header>
  );

  const actions = (
    <Styled.Toolbar disableGutters variant="dense">
      <ActionButton icon={<VerticalAlignBottom />} onClick={onExport}>
        {t('Download')}
      </ActionButton>
      <ActionButton icon={<Close />} onClick={onClose}>
        {t('Cancellation')}
      </ActionButton>
    </Styled.Toolbar>
  );

  if (!isOpen) {
    return null;
  }

  return (
    <Modal
      header={header}
      content={t('fileForDownloadOnly')}
      actions={actions}
      isOpen={isOpen}
      onClose={onClose}
    />
  );
};
