import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { LicenseInfo, GridSelectionModel } from '@mui/x-data-grid-pro';
import { Column } from '@features/data-grid';
import {
  DocsGate,
  $docs,
  $docsCurrentPage,
  $docsPageSize,
  $rowsCountDocs,
  docsPageNumberChanged,
  docsPageSizeChanged,
  docsSelectionRowChanged,
  $docsSelectRow,
  changedVisibilityDeleteDocsModal,
  $isOpenDeleteDocsModal,
  deleteDocs,
  changedNotViewDocumentModalData,
  $docsColumns,
  $docsVisibilityColumns,
  docsVisibilityColumnsChanged,
  docsOrderColumnsChanged,
  docsColumnsWidthChanged,
  $docsPinnedColumns,
  docsChangePinnedColumns,
} from '@features/objects/models';
import { DeleteConfirmDialog } from '@ui/index';
import { ArchiveConfirmDialog } from './archive-confirm-dialog';
import { NotViewDocumentModal } from './not-view-document-modal';
import * as Styled from './styled';
import { Toolbar } from './toolbar';

export const Docs: FC = () => {
  useGate(DocsGate);
  const { t } = useTranslation();

  LicenseInfo.setLicenseKey(process.env.X_DATA_GRID_PRO_LICENSE as string);

  const [
    docs,
    page,
    pageSize,
    rowsCount,
    selectionModel,
    isOpenDeleteDocsModal,
    columns,
    visibilityColumns,
    docsPinnedColumns,
  ] = useUnit([
    $docs,
    $docsCurrentPage,
    $docsPageSize,
    $rowsCountDocs,
    $docsSelectRow,
    $isOpenDeleteDocsModal,
    $docsColumns,
    $docsVisibilityColumns,
    $docsPinnedColumns,
  ]);

  const onChangePage = (newPage: number) => docsPageNumberChanged(newPage);
  const onPageSizeChange = (newPageSize: number) => docsPageSizeChanged(newPageSize);

  const onSelectionModelChange = (newSelectionModel: GridSelectionModel) =>
    docsSelectionRowChanged(newSelectionModel as number[]);

  const onCloseDeleteModal = () => changedVisibilityDeleteDocsModal(false);
  const onRowClick = ({
    row: { id, title, type, url },
  }: {
    row: { id: number; title: string; type: string; url: string };
  }) => {
    if (
      (type.toLowerCase() === 'pdf' ||
        type.toLowerCase() === 'jpg' ||
        type.toLowerCase() === 'jpeg' ||
        type.toLowerCase() === 'png') &&
      url
    ) {
      return window.open(url);
    }

    return changedNotViewDocumentModalData({ isOpen: true, id, title, type });
  };

  const handleColumnWidth = (column: Column) => docsColumnsWidthChanged(column);
  const handleColumnOrder = (column: Column) => docsOrderColumnsChanged(column);

  const handleVisibility = (column: { [key: string]: boolean }) =>
    docsVisibilityColumnsChanged(column);

  const handlePinned = (column: { left: string[]; right: string[] }) =>
    docsChangePinnedColumns(column);

  return (
    <>
      <DeleteConfirmDialog
        header={t('docsRemove')}
        content={t('docsRemoveConfirm')}
        isOpen={isOpenDeleteDocsModal}
        close={onCloseDeleteModal}
        confirm={deleteDocs}
      />

      <ArchiveConfirmDialog />
      <NotViewDocumentModal />

      <Toolbar />
      <Styled.Box>
        <Styled.DataGrid
          rows={docs}
          rowCount={rowsCount}
          columns={columns}
          checkboxSelection
          disableRowSelectionOnClick
          rowSelectionModel={selectionModel}
          onRowSelectionModelChange={onSelectionModelChange}
          pagination
          paginationMode="server"
          page={page}
          pageSize={pageSize}
          onPageChange={onChangePage}
          onPageSizeChange={onPageSizeChange}
          rowsSelection
          onRowClick={onRowClick}
          columnVisibilityModel={visibilityColumns}
          onColumnWidthChange={handleColumnWidth}
          onColumnOrderChange={handleColumnOrder}
          onColumnVisibilityModelChange={handleVisibility}
          onPinnedColumnsChange={handlePinned}
          pinnedColumns={docsPinnedColumns}
        />
      </Styled.Box>
    </>
  );
};
