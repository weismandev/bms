import { FC } from 'react';
import i18n from '@shared/config/i18n';
import { SelectControl, SelectField } from '@ui/index';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const { t } = i18n;

const options = [
  { id: 1, title: t('TheWholeComplex') },
  { id: 2, title: t('SelectBuildings') },
];

export const ParkingRelatedBuildingsSelect: FC<object> = (props) => (
  <SelectControl options={options} {...props} />
);

export const ParkingRelatedBuildingsSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ParkingRelatedBuildingsSelect />}
      onChange={onChange}
      {...props}
    />
  );
};
