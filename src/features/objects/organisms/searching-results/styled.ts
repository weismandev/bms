import { styled, Typography, LinearProgress } from '@mui/material';

export const Wrapper = styled('div')(() => ({
  position: 'relative',
  padding: '24px 24px 48px',
  background: '#FFFFFF',
  boxShadow: '0px 4px 15px #E7E7EC',
  borderRadius: 15,
  overflow: 'hidden',
}));

export const Loader = styled(LinearProgress)(() => ({
  position: 'absolute',
  width: '100%',
  top: 0,
  left: 0,
}));

export const Header = styled(Typography)(() => ({
  fontWeight: 500,
  fontSize: 20,
  color: '#191C29DE',
  marginBottom: 24,
}));

export const EmptyObjectsTree = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100%',
  gap: 10,
}));

export const EmptyObjectsText = styled(Typography)(() => ({
  fontWeight: 500,
  fontSize: 14,
  color: '#191C2999',
}));

export const Table = styled('div')(() => ({
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
}));
