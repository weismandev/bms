import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { GridEventListener } from '@mui/x-data-grid-pro';
import SearchIcon from '@img/search.svg';
import { ThemeAdapter } from '@shared/theme/adapter';
import { DataGridTable } from '@shared/ui/data-grid-table';
import {
  $searchCount,
  $isSearchLoading,
  searchTablecolumns,
  $searchTableData,
  $paginationSearch,
  paginationModelChangedSearch,
  getExpandedTree,
} from '../../models';
import * as Styled from './styled';

export const SearchingResults: FC = () => {
  const { t } = useTranslation();
  const [searchCount, isSearchLoading, searchTableData, paginationSearch] = useUnit([
    $searchCount,
    $isSearchLoading,
    $searchTableData,
    $paginationSearch,
  ]);

  const handleClickRow = ({ id }: { id: string }) => getExpandedTree(id);

  return (
    <Styled.Wrapper>
      {isSearchLoading ? <Styled.Loader /> : null}
      <Styled.Header variant="h3">
        {`${t('SearchResults')} (${searchCount})`}
      </Styled.Header>
      {searchCount ? (
        <ThemeAdapter>
          <Styled.Table>
            <DataGridTable
              rows={searchTableData}
              columns={searchTablecolumns}
              rowCount={searchCount}
              density="compact"
              disableRowSelectionOnClick
              paginationModel={paginationSearch}
              onPaginationModelChange={paginationModelChangedSearch}
              pageSizeOptions={[25, 50, 100]}
              onRowClick={handleClickRow as GridEventListener<'rowClick'>}
            />
          </Styled.Table>
        </ThemeAdapter>
      ) : (
        <Styled.EmptyObjectsTree>
          <SearchIcon />
          <Styled.EmptyObjectsText variant="subtitle2">
            {t('NoObjectsFound')}
          </Styled.EmptyObjectsText>
        </Styled.EmptyObjectsTree>
      )}
    </Styled.Wrapper>
  );
};
