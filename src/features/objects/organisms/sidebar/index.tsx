import { FC, ChangeEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { IconButton, Tooltip } from '@mui/material';
// import RefreshRoundedIcon from '@mui/icons-material/RefreshRounded'; скрыто в задаче CRE-568
import UnfoldLessRoundedIcon from '@mui/icons-material/UnfoldLessRounded';
// import UnfoldMoreIcon from '@mui/icons-material/UnfoldMore'; скрыто в задаче CRE-568
import {
  $expandedObjects,
  $isCreationMode,
  $isSidebarLoading,
  collapseAllObjects, // expandAllObjects, скрыто в задаче CRE-568
  // refreshObjects, скрыто в задаче CRE-568
  $search,
  searchChanged,
} from '@features/objects/models';
import { ObjectsTree } from '@features/objects/organisms/objects-tree';
import { SearchInput } from '@ui/index';
import * as Styled from './styled';

export const Sidebar: FC = () => {
  const { t } = useTranslation();
  const [expandedObjects, isSidebarLoading, isCreationMode, search] = useUnit([
    $expandedObjects,
    $isSidebarLoading,
    $isCreationMode,
    $search,
  ]);

  // const handleRefreshClick = () => { скрыто в задаче CRE-568
  //   refreshObjects();
  // };

  const handleCollapseClick = () => {
    collapseAllObjects();
  };

  // const handleExpandClick = () => { скрыто в задаче CRE-568
  //   expandAllObjects();
  // };

  const handleSearch = (event: ChangeEvent<HTMLInputElement>) =>
    searchChanged(event.target.value);

  const handleSearchClear = () => searchChanged('');

  return (
    <Styled.Container>
      {isSidebarLoading ? <Styled.Loader /> : null}
      <Styled.Backdrop open={isCreationMode} />
      <Styled.Header>
        {/* скрыто в задаче CRE-568 <Styled.Title>{t('objects')}</Styled.Title> */}
        <SearchInput
          value={search}
          onChange={handleSearch}
          handleClear={handleSearchClear}
        />
        <Styled.Actions>
          {/* скрыто в задаче CRE-568 <Tooltip title={t('Refresh')}>
            <IconButton size="small" onClick={handleRefreshClick}>
              <RefreshRoundedIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title={t('ExpandAllLevels')} onClick={handleExpandClick}>
            <IconButton size="small">
              <UnfoldMoreIcon />
            </IconButton>
          </Tooltip> скрыто в задаче CRE-568 */}
          <Tooltip title={t('CollapseAllLevels')} onClick={handleCollapseClick}>
            <IconButton size="small" disabled={expandedObjects.length === 0}>
              <UnfoldLessRoundedIcon />
            </IconButton>
          </Tooltip>
        </Styled.Actions>
      </Styled.Header>
      <ObjectsTree />
    </Styled.Container>
  );
};
