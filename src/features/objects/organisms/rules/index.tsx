import { FC } from 'react';
import { useUnit, useGate } from 'effector-react';
import { Formik } from 'formik';
import { RulesGate, saveRule, $rules } from '@features/objects/models';
import { RulesForm } from './rules-form';
import { validationSchema } from './validation';

export const Rules: FC = () => {
  useGate(RulesGate);

  const rules = useUnit($rules);

  if (!rules) {
    return null;
  }

  return (
    <Formik
      initialValues={rules}
      onSubmit={(values) => saveRule(values)}
      enableReinitialize
      validationSchema={validationSchema}
      render={({ setFieldValue, resetForm, values }) => (
        <RulesForm setFieldValue={setFieldValue} resetForm={resetForm} values={values} />
      )}
    />
  );
};
