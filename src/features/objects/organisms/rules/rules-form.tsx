import { FC, useEffect } from 'react';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { Button } from '@mui/material';
import {
  $rulesEditMode,
  setRulesEditMode,
  $rules,
  exportRule,
} from '@features/objects/models';
import { RulesEditor } from '@features/objects/molecules/rules-editor';
import { RulesToolbar } from '@features/objects/molecules/rules-toolbar';
import { NotRules } from '@features/objects/organisms/not-rules';
import { Rule } from '@features/objects/types/rules';
import { CustomScrollbar } from '@ui/index';
import * as Styled from './styled';
import { styles } from './styles';

interface Props {
  setFieldValue: (field: string, value: any) => void;
  resetForm: (nextValues?: Rule) => void;
  values: Rule;
}

export const RulesForm: FC<Props> = ({ setFieldValue, resetForm, values }) => {
  const { t } = useTranslation();
  const [rulesEditMode, rules] = useUnit([$rulesEditMode, $rules]);

  const onChange = (value: string) => setFieldValue('text', value);
  const onEdit = () => setRulesEditMode('edit');

  const onCancel = () => {
    setRulesEditMode('view');
    resetForm();
  };

  const onClickPreview = () => {
    if (rules?.preview) {
      window.open(rules.preview);
    }
  };

  useEffect(() => {
    if (rules && values && rules?.text !== values?.text) {
      setFieldValue('text', rules.text);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rules]);

  return (
    <Styled.Form>
      <Styled.ToolbarWrapper>
        <RulesToolbar
          mode={rulesEditMode}
          onEdit={onEdit}
          onCancel={onCancel}
          isExportDisabled={!rules?.text}
          onExport={exportRule}
        />
      </Styled.ToolbarWrapper>
      {rulesEditMode === 'view' && !rules?.text ? (
        <NotRules />
      ) : (
        <Styled.Content>
          <Styled.WrapperPreviewButton>
            <Button
              disabled={rulesEditMode === 'edit' || !rules?.preview}
              onClick={onClickPreview}
            >
              <Styled.VisibilityOutlined />
              {t('Preview')}
            </Button>
          </Styled.WrapperPreviewButton>
          <CustomScrollbar>
            <Styled.Container>
              <Field
                name="text"
                placeholder={`${t('StartEnterText')}...`}
                onChange={onChange}
                mode={rulesEditMode}
                component={RulesEditor}
                editorStyle={styles.editor}
                toolbarStyle={styles.toolbar}
              />
            </Styled.Container>
          </CustomScrollbar>
        </Styled.Content>
      )}
    </Styled.Form>
  );
};
