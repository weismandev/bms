import { Form as FormFormik } from 'formik';
import { styled } from '@mui/material';
import { VisibilityOutlined as VisibilityOutlinedMui } from '@mui/icons-material';

export const Form = styled(FormFormik)(() => ({
  height: 'calc(100% - 50px)',
  position: 'relative',
}));

export const ToolbarWrapper = styled('div')(() => ({
  marginTop: 10,
  padding: '0px 24px',
}));

export const Content = styled('div')(() => ({
  height: 'calc(100% - 22px)',
  padding: '0px 0px 24px 24px',
}));

export const Container = styled('div')(() => ({
  paddingRight: 24,
}));

export const VisibilityOutlined = styled(VisibilityOutlinedMui)(() => ({
  marginRight: 10,
}));

export const WrapperPreviewButton = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'right',
  padding: '0px 24px 12px 0px',
}));
