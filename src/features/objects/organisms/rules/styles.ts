export const styles = {
  editor: {
    padding: '0px 1em',
    color: '#65657b',
    cursor: 'auto',
    border: '2px solid #E7E7EC',
  },
  toolbar: {
    position: 'sticky',
    width: 'fit-content',
    top: 0,
    zIndex: 10,
    border: '2px solid #E7E7EC',
  },
};
