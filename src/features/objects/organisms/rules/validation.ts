import * as Yup from 'yup';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const validationSchema = Yup.object().shape({
  text: Yup.string().test(
    'text-required',
    t('thisIsRequiredField') ?? '',
    (data) => {
      if (!data) {
        return false;
      }

      const formattedText = data.replace(/(<([^>]+)>)/gi, '');

      return formattedText.length > 1;
    }
  ),
});
