import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: '1fr auto',
  alignContent: 'start',
  alignItems: 'start',
  gap: 10,
  width: '100%',
  height: '100%',
  maxWidth: 888,
  margin: '0 auto',
}));
