import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import {
  $currentSettings,
  $settingsEditMode,
  reconciliationOptions,
  setSettingsEditMode,
  updateSettings,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { Settings } from '@features/objects/types/settings';
import { InputField, SelectField, SwitchField } from '@ui/index';

export const ComplexForm = () => {
  const { t } = useTranslation();
  const [currentSettings, editMode] = useUnit([$currentSettings, $settingsEditMode]);
  const isEditMode = editMode === 'edit';

  if (!currentSettings) return null;

  return (
    <Formik
      onSubmit={(data: Settings) => {
        updateSettings(data);
      }}
      initialValues={currentSettings}
      validationSchema={Yup.object().shape({
        correct_counters_data_from_date: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .min(1, t('Validation.minimumDayOfMonth1') as string)
          .max(31, t('Validation.maximumDayOfMonth31') as string),
        correct_counters_data_until_date: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .min(1, t('Validation.minimumDayOfMonth1') as string)
          .max(31, t('Validation.maximumDayOfMonth31') as string),
        request_frequency: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        cold_water_expense_norm: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        cold_water_expense_minimum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        cold_water_expense_maximum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_norm: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_minimum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_maximum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_norm: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_minimum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_maximum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_norm: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_minimum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_maximum: Yup.number()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
      })}
      enableReinitialize
      render={({ values, setFieldValue, handleReset, handleSubmit, isValid, dirty }) => (
        <Form
          editMode={editMode}
          setEditMode={setSettingsEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('ObjectsSettings.submitReadingsManually')}>
            <Field
              name="correct_counters_data_manually"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
          {values.correct_counters_data_manually ? (
            <>
              <Control.Row label={t('ObjectsSettings.submitOnDateOfMonth')}>
                <Field
                  component={InputField}
                  name="correct_counters_data_from_date"
                  type="number"
                  mode={editMode}
                  label=""
                  placeholder=""
                  divider={false}
                />
              </Control.Row>

              <Control.Row label={t('ObjectsSettings.submitByDateOfMonth')}>
                <Field
                  component={InputField}
                  name="correct_counters_data_until_date"
                  type="number"
                  mode={editMode}
                  label=""
                  placeholder=""
                  divider={false}
                />
              </Control.Row>
            </>
          ) : null}

          <Control.Row label={t('ObjectsSettings.pollingFrequencyNumberOfTimesPerMonth')}>
            <Field
              name="request_frequency"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          <Control.Row label={t('ObjectsSettings.reconciliationFrequency')}>
            <Field
              name="reconciliation_period"
              label=""
              placeholder=""
              component={SelectField}
              mode={editMode}
              options={reconciliationOptions}
              onChange={(value: { value: any }) => {
                if (value?.value === null) {
                  return setFieldValue('reconciliation_period', value);
                }

                return setFieldValue('reconciliation_period', value?.value);
              }}
              divider={false}
            />
          </Control.Row>

          <Control.Row label={t('ObjectsSettings.coldWaterReadingStandardCubicM')}>
            <Field
              name="cold_water_expense_norm"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row label={t('ObjectsSettings.criticalMinimumColdWaterFlowCubicM')}>
            <Field
              name="cold_water_expense_minimum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row label={t('ObjectsSettings.criticalMaximumColdWaterFlowCubicM')}>
            <Field
              name="cold_water_expense_maximum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          <Control.Row label={t('ObjectsSettings.hotWaterStandardCubicM')}>
            <Field
              name="hot_water_expense_norm"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('ObjectsSettings.criticalMinimumHotWaterConsumptionCubicM')}
          >
            <Field
              name="hot_water_expense_minimum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row label={t('ObjectsSettings.criticalMaximumHotWaterFlowCubicM')}>
            <Field
              name="hot_water_expense_maximum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          <Control.Row label={t('ObjectsSettings.electricityStandardKWh')}>
            <Field
              name="electricity_expense_norm"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('ObjectsSettings.criticalMinimumElectricityConsumptionKWh')}
          >
            <Field
              name="electricity_expense_minimum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('ObjectsSettings.criticalMaximumElectricityConsumptionKWh')}
          >
            <Field
              name="electricity_expense_maximum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          <Control.Row label={t('ObjectsSettings.thermalEnergyStandardGcal')}>
            <Field
              name="heat_energy_expense_norm"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('ObjectsSettings.criticalMinimumThermalEnergyConsumptionGcal')}
          >
            <Field
              name="heat_energy_expense_minimum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('ObjectsSettings.criticalMaximumThermalEnergyConsumptionGcal')}
          >
            <Field
              name="heat_energy_expense_maximum"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row label={t('ObjectsSettings.connectPaidApplications')}>
            <Field
              name="paid_tickets_enabled"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
        </Form>
      )}
    />
  );
};
