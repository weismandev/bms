import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Field, FieldArray, Formik } from 'formik';
import * as Yup from 'yup';
import { IconButton } from '@mui/material';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import {
  $currentSettings,
  $settingsEditMode,
  reconciliationOptions,
  setSettingsEditMode,
  updateSettings,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import { Settings } from '@features/objects/types/settings';
import {
  EmailMaskedInput,
  InputField,
  PhoneMaskedInput,
  SelectField,
  SwitchField,
} from '@ui/index';

export const BuildingForm = () => {
  const { t } = useTranslation();
  const [currentSettings, editMode] = useUnit([$currentSettings, $settingsEditMode]);
  const isEditMode = editMode === 'edit';

  if (!currentSettings) return null;

  return (
    <Formik
      onSubmit={(data: Settings) => {
        updateSettings(data);
      }}
      initialValues={currentSettings}
      validationSchema={Yup.object().shape({
        guest_scud_pass_limit: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        start_work_time: Yup.string()
          .nullable()
          .trim()
          .matches(
            /^(?:[01][0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?$/,
            t('Validation.rightTimeFormat') as string
          ),
        end_work_time: Yup.string()
          .nullable()
          .trim()
          .matches(
            /^(?:[01][0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?$/,
            t('Validation.rightTimeFormat') as string
          ),
        correct_counters_data_from_date: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .min(1, t('Validation.minimumDayOfMonth1') as string)
          .max(31, t('Validation.maximumDayOfMonth31') as string),
        correct_counters_data_until_date: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .min(1, t('Validation.minimumDayOfMonth1') as string)
          .max(31, t('Validation.maximumDayOfMonth31') as string),
        request_frequency: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .min(1, t('Validation.minimumDayOfMonth1') as string)
          .max(31, t('Validation.maximumDayOfMonth31') as string),
        cold_water_expense_norm: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        cold_water_expense_minimum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        cold_water_expense_maximum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_norm: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_minimum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        hot_water_expense_maximum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_norm: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_minimum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        electricity_expense_maximum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_norm: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_minimum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
        heat_energy_expense_maximum: Yup.number()
          .nullable()
          .typeError(t('Validation.numberOnly') as string)
          .integer(t('Validation.integerOnly') as string)
          .moreThan(-1, t('Validation.numberOnly') as string),
      })}
      enableReinitialize
      render={({ values, setFieldValue, handleReset, handleSubmit, isValid, dirty }) => (
        <Form
          editMode={editMode}
          setEditMode={setSettingsEditMode}
          isValid={isValid}
          dirty={dirty}
          onSubmit={() => {
            handleSubmit();
          }}
          reset={() => {
            handleReset();
          }}
        >
          <Control.Row label={t('BuildingsSettings.securityTelephone')}>
            <Field
              name="security_number"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  inputComponent={PhoneMaskedInput}
                  label=""
                  placeholder={t('Label.phone')}
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t(
              'BuildingsSettings.acceptingApplicationsForTheSaleOfApartmentsFromOwners'
            )}
          >
            <Field
              name="sell_enabled"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>

          {values.sell_enabled ? (
            <Control.Row label={t('BuildingsSettings.contactEmail')}>
              <FieldArray
                name="sell_emails"
                render={(arrayHelpers) => (
                  <>
                    {values.sell_emails
                      ? values.sell_emails.map((email, index) => (
                          <Control.Array key={index}>
                            <Field
                              name={`sell_emails.${index}`}
                              render={({ field, form }: any) => (
                                <InputField
                                  field={field}
                                  form={form}
                                  inputComponent={EmailMaskedInput}
                                  label=""
                                  placeholder=""
                                  mode={editMode}
                                  divider={false}
                                />
                              )}
                            />

                            {isEditMode ? (
                              <IconButton
                                size="small"
                                onClick={() => arrayHelpers.remove(index)}
                              >
                                <CloseRoundedIcon />
                              </IconButton>
                            ) : null}
                          </Control.Array>
                        ))
                      : null}

                    {isEditMode ? (
                      <IconButton size="small" onClick={() => arrayHelpers.push('')}>
                        <AddRoundedIcon />
                      </IconButton>
                    ) : null}
                  </>
                )}
              />
            </Control.Row>
          ) : null}

          <Control.Row
            label={t('BuildingsSettings.verificationForConnectionToApartments')}
          >
            <Field
              name="resident_request_resolve_mode"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('BuildingsSettings.possibilityOfRentingPremises')}>
            <Field
              name="buildings_properties_rent_available"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('BuildingsSettings.operatingTimeFrom')}>
            <Field
              name="start_work_time"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          <Control.Row label={t('BuildingsSettings.openingHoursUntil')}>
            <Field
              name="end_work_time"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>
          <Control.Row
            label={t('BuildingsSettings.individualValidityPeriodOfAGuestPassInDays')}
          >
            <Field
              component={InputField}
              name="guest_scud_pass_limit"
              type="number"
              mode={editMode}
              label=""
              placeholder=""
              divider={false}
            />
          </Control.Row>
          <Control.Row
            label={t('BuildingsSettings.theCounterSettingsCoincideWithTheLcdSettings')}
          >
            <Field
              name="is_matching_measures_complex_config"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
          <Control.Row label={t('BuildingsSettings.submitReadingsManually')}>
            <Field
              name="correct_counters_data_manually"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
          {values.correct_counters_data_manually ? (
            <>
              <Control.Row label={t('BuildingsSettings.serveOnDateOfMonth')}>
                <Field
                  component={InputField}
                  name="correct_counters_data_from_date"
                  type="number"
                  mode={editMode}
                  label=""
                  placeholder=""
                  divider={false}
                />
              </Control.Row>

              <Control.Row label={t('BuildingsSettings.serveByDateOfMonth')}>
                <Field
                  component={InputField}
                  name="correct_counters_data_until_date"
                  type="number"
                  mode={editMode}
                  label=""
                  placeholder=""
                  divider={false}
                />
              </Control.Row>
            </>
          ) : null}
          <Control.Row
            label={t('BuildingsSettings.pollingFrequencyNumberOfTimesPerMonth')}
          >
            <Field
              name="request_frequency"
              render={({ field, form }: any) => (
                <InputField
                  field={field}
                  form={form}
                  type="number"
                  label=""
                  placeholder=""
                  mode={editMode}
                  divider={false}
                />
              )}
            />
          </Control.Row>

          {!values.is_matching_measures_complex_config ? (
            <>
              <Control.Row label={t('BuildingsSettings.reconciliationFrequency')}>
                <Field
                  name="reconciliation_period"
                  label=""
                  placeholder=""
                  component={SelectField}
                  mode={values.is_matching_measures_complex_config ? 'view' : editMode}
                  options={reconciliationOptions}
                  onChange={(value: { value: any }) => {
                    if (value?.value === null) {
                      return setFieldValue('reconciliation_period', value);
                    }

                    return setFieldValue('reconciliation_period', value?.value);
                  }}
                  divider={false}
                />
              </Control.Row>

              <Control.Row label={t('BuildingsSettings.coldWaterReadingStandardCubicM')}>
                <Field
                  name="cold_water_expense_norm"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMinimumColdWaterFlowCubicM')}
              >
                <Field
                  name="cold_water_expense_minimum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMaximumColdWaterFlowCubicM')}
              >
                <Field
                  name="cold_water_expense_maximum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>

              <Control.Row label={t('BuildingsSettings.hotWaterStandardCubicM')}>
                <Field
                  name="hot_water_expense_norm"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMinimumHotWaterConsumptionCubicM')}
              >
                <Field
                  name="hot_water_expense_minimum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMaximumHotWaterFlowCubicM')}
              >
                <Field
                  name="hot_water_expense_maximum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>

              <Control.Row label={t('BuildingsSettings.electricityStandardKWh')}>
                <Field
                  name="electricity_expense_norm"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMinimumElectricityConsumptionKWh')}
              >
                <Field
                  name="electricity_expense_minimum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMaximumElectricityConsumptionKWh')}
              >
                <Field
                  name="electricity_expense_maximum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>

              <Control.Row label={t('BuildingsSettings.thermalEnergyStandardGcal')}>
                <Field
                  name="heat_energy_expense_norm"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMinimumThermalEnergyConsumptionGcal')}
              >
                <Field
                  name="heat_energy_expense_minimum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
              <Control.Row
                label={t('BuildingsSettings.criticalMaximumThermalEnergyConsumptionGcal')}
              >
                <Field
                  name="heat_energy_expense_maximum"
                  render={({ field, form }: any) => (
                    <InputField
                      field={field}
                      form={form}
                      type="number"
                      label=""
                      placeholder=""
                      mode={
                        values.is_matching_measures_complex_config ? 'view' : editMode
                      }
                      divider={false}
                    />
                  )}
                />
              </Control.Row>
            </>
          ) : null}
          <Control.Row label={t('BuildingsSettings.connectPaidApplications')}>
            <Field
              name="paid_tickets_enabled"
              component={SwitchField}
              label=""
              labelPlacement="start"
              disabled={!isEditMode}
              divider={false}
            />
          </Control.Row>
        </Form>
      )}
    />
  );
};
