import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { $currentObjectType, SettingsGate } from '@features/objects/models';
import { BuildingForm } from './forms/building-form';
import { ComplexForm } from './forms/complex-form';
import * as Styled from './styled';

export const Settings: FC = () => {
  useGate(SettingsGate);

  const [currentObjectType] = useUnit([$currentObjectType]);

  const renderForm = () => {
    switch (currentObjectType) {
      case 'complex':
        return <ComplexForm />;
      case 'building':
        return <BuildingForm />;
      default:
        return null;
    }
  };

  return <Styled.Container>{renderForm()}</Styled.Container>;
};
