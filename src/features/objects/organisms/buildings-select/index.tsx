import { FC } from 'react';
import { useUnit } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { $buildings, $isObjectsLoading } from '../../models/objects/objects.model';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const BuildingsSelect: FC<object> = (props) => {
  const [options, isLoading] = useUnit([$buildings, $isObjectsLoading]);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const BuildingsSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<BuildingsSelect />} onChange={onChange} {...props} />;
};
