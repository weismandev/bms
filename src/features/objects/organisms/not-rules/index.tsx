import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { setRulesEditMode } from '@features/objects/models';
import { Dumb } from '@ui/index';
import * as Styled from './styled';

export const NotRules: FC = () => {
  const { t } = useTranslation();
  const onClick = () => setRulesEditMode('edit');

  return (
    <Dumb>
      <Styled.InsertDriveFileOutlined />
      <Styled.Hint>{t('ToCreateRulesGoToEditMode')}</Styled.Hint>
      <Styled.EditModeText onClick={onClick}>{t('EditMode')}</Styled.EditModeText>
    </Dumb>
  );
};
