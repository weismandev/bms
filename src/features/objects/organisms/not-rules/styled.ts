import { styled, Typography } from '@mui/material';
import { InsertDriveFileOutlined as InsertDriveFileOutlinedMui } from '@mui/icons-material';

export const InsertDriveFileOutlined = styled(InsertDriveFileOutlinedMui)(() => ({
  width: 60,
  height: 60,
  color: 'rgba(25, 28, 41, 0.32)',
  marginBottom: 10,
}));

export const Hint = styled(Typography)(() => ({
  fontWeight: 400,
  fontSize: 16,
  color: 'rgba(25, 28, 41, 0.6)',
  marginBottom: 10,
}));

export const EditModeText = styled(Typography)(() => ({
  fontWeight: 500,
  fontSize: 15,
  color: 'rgba(31, 135, 229, 1)',
  cursor: 'pointer',
}));
