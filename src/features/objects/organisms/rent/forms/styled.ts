import { styled } from '@mui/material';

export const DayOfTheWeekControl = styled('div')(() => ({
  display: 'flex',
  alignItems: 'stretch',
  justifycontent: 'stretch',
  width: '100%',
  '> button:first-child': {
    borderRadius: '16px 0 0 16px',
  },
  '> button:last-child': {
    borderRadius: '0 16px 16px 0',
    borderRight: '1px solid #191c291f',
  },
}));

export const DayOfTheWeekItem = styled('button')(
  ({ selected, disabled }: { selected: boolean; disabled: boolean }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: '1 1 auto',
    border: '1px solid #191c291f',
    borderRight: 0,
    background: selected ? 'rgba(31, 135, 229, 0.08)' : '#fff',
    cursor: disabled ? 'default' : 'pointer',
    fontSize: 14,
    fontweight: 500,
    lineheight: 24,
    color: selected ? '#1F87E5' : '#191c2999',
  })
);

export const DualSelectTimeContainer = styled('div')(() => ({
  display: 'flex',
  gap: 10,
  justifyContent: 'end',
}));

export const TimeSelectField = styled('div')(() => ({
  width: '100%',
}));

export const TimeSelectDivider = styled('div')(() => ({
  marginTop: 7,
}));

export const ErrorMessage = styled('span')(() => ({
  color: '#d32f2f',
  fontSize: '0.75rem',
  fontWeight: 400,
}));

export const StepCostWrapper = styled('div')(() => ({
  display: 'flex',
}));

export const StepCostField = styled('div')(() => ({
  width: '100%',
}));

export const StepCostHint = styled('div')(() => ({
  margin: '5px 0px 0px 10px',
}));
