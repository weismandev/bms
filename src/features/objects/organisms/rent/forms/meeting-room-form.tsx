import { FC } from 'react';
import { useUnit } from 'effector-react';
import { Formik, Field, FieldProps } from 'formik';
import * as Yup from 'yup';
import {
  $rentEditMode,
  setRentEditMode,
  updateRent,
  $rent,
} from '@features/objects/models';
import { Control } from '@features/objects/molecules/control';
import { Form } from '@features/objects/molecules/form';
import i18n from '@shared/config/i18n';
import { SwitchField, SelectField, InputField } from '@ui/index';
import * as Styled from './styled';

const { t } = i18n;

const days = [
  { id: 1, value: t('WeekDays.MondayShort') },
  { id: 2, value: t('WeekDays.TuesdayShort') },
  { id: 3, value: t('WeekDays.WednesdayShort') },
  { id: 4, value: t('WeekDays.ThursdayShort') },
  { id: 5, value: t('WeekDays.FridayShort') },
  { id: 6, value: t('WeekDays.SaturdayShort') },
  { id: 7, value: t('WeekDays.SundayShort') },
];

const timeStepOptions = [
  {
    id: 15,
    title: `15 ${t('minutes')}`,
  },
  {
    id: 30,
    title: `30 ${t('minutes')}`,
  },
  {
    id: 60,
    title: `1 ${t('hour')}`,
  },
];

const proofOfUseTimeAllOptions = [
  {
    id: 10,
    title: `10 ${t('minutes')}`,
  },
  {
    id: 20,
    title: `20 ${t('minutes')}`,
  },
  {
    id: 30,
    title: `30 ${t('minutes')}`,
  },
  {
    id: 40,
    title: `40 ${t('minutes')}`,
  },
];

const getPoofOfUseTime = (timeStep: { id: number }) => {
  if (!timeStep) {
    return [];
  }

  const timeStepValue = typeof timeStep === 'object' ? timeStep?.id : timeStep;

  return proofOfUseTimeAllOptions.filter((option) => option.id < timeStepValue);
};

// const paidWaitingTimeOptions = [ пока нет функционала оплаты
//   {
//     id: 15,
//     title: `15 ${t('minutes')}`,
//   },
//   {
//     id: 30,
//     title: `30 ${t('minutes')}`,
//   },
// ];

const validationSchema = Yup.object().shape({
  isRentAvailability: Yup.boolean(),
  isNeedBookingApproval: Yup.boolean(),
  bookingApprovalTime: Yup.mixed().when(['isRentAvailability', 'isNeedBookingApproval'], {
    is: (isRentAvailability: boolean, isNeedBookingApproval: boolean) =>
      isRentAvailability && isNeedBookingApproval,
    then: Yup.mixed().required(t('thisIsRequiredField') ?? ''),
  }),
  isNeedProofOfUse: Yup.boolean(),
  proofOfUseTime: Yup.mixed().when(['isRentAvailability', 'isNeedProofOfUse'], {
    is: (isRentAvailability: boolean, isNeedProofOfUse: boolean) =>
      isRentAvailability && isNeedProofOfUse,
    then: Yup.mixed().required(t('thisIsRequiredField') ?? ''),
  }),
  availableDays: Yup.array().when('isRentAvailability', {
    is: true,
    then: Yup.array()
      .min(1, t('thisIsRequiredField') ?? '')
      .required(t('thisIsRequiredField') ?? ''),
  }),
  availableTimeFrom: Yup.string().when('isRentAvailability', {
    is: true,
    then: Yup.string()
      .required(t('thisIsRequiredField') ?? '')
      .test(
        'time',
        t('TheTimeFromMustBeLessThanTheTimeTo') ?? '',
        (_, { parent: { availableTimeFrom, availableTimeTo } }) => {
          if (availableTimeFrom && availableTimeTo && availableTimeTo !== '00:00') {
            return availableTimeFrom < availableTimeTo;
          }

          return true;
        }
      ),
  }),
  availableTimeTo: Yup.string().when('isRentAvailability', {
    is: true,
    then: Yup.string()
      .required(t('thisIsRequiredField') ?? '')
      .test(
        'time',
        t('TheTimeToMustBeGreaterThanTheTimeFrom') ?? '',
        (_, { parent: { availableTimeFrom, availableTimeTo } }) => {
          if (availableTimeFrom && availableTimeTo && availableTimeTo !== '00:00') {
            return availableTimeFrom < availableTimeTo;
          }

          return true;
        }
      ),
  }),
  timeStep: Yup.mixed().when('isRentAvailability', {
    is: true,
    then: Yup.mixed().required(t('thisIsRequiredField') ?? ''),
  }),
  isPayment: Yup.boolean(),
  stepCost: Yup.number().when(['isRentAvailability', 'isPayment'], {
    is: (isRentAvailability: boolean, isPayment: boolean) =>
      isRentAvailability && isPayment,
    then: Yup.number()
      .required(t('thisIsRequiredField') ?? '')
      .typeError(t('Validation.numberOnly') ?? '')
      .integer(t('Validation.integerOnly') ?? '')
      .moreThan(-1, t('Validation.positiveNumberOnly') ?? ''),
  }),
  // paidWaitingTime: Yup.mixed().when(['isRentAvailability', 'isPayment'], {
  //   is: (isRentAvailability: boolean, isPayment: boolean) =>
  //     isRentAvailability && isPayment,
  //   then: Yup.mixed().required(t('thisIsRequiredField') ?? ''),
  // }),
});

export const MeetingRoomForm: FC = () => {
  const [editMode, rent] = useUnit([$rentEditMode, $rent]);

  if (!rent) {
    return null;
  }

  return (
    <Formik
      onSubmit={(values) => updateRent(values)}
      initialValues={rent}
      enableReinitialize
      validationSchema={validationSchema}
      render={({ isValid, handleSubmit, handleReset, dirty, values, setFieldValue }) => {
        const poofOfUseTimeOptions = getPoofOfUseTime(values?.timeStep);

        const onSubmit = () => handleSubmit();
        const onReset = () => handleReset();

        const onChangeRentAvailability = ({
          target: { checked },
        }: {
          target: { checked: boolean };
        }) => {
          setFieldValue('isRentAvailability', checked);

          if (!checked) {
            setFieldValue('description', '');
            setFieldValue('isNeedBookingApproval', false);
            setFieldValue('bookingApprovalTime', '');
            setFieldValue('isNeedProofOfUse', false);
            setFieldValue('proofOfUseTime', '');
            setFieldValue('availableDays', []);
            setFieldValue('availableTimeFrom', '');
            setFieldValue('availableTimeTo', '');
            setFieldValue('timeStep', '');
            setFieldValue('isPayment', false);
            setFieldValue('stepCost', '');
            // setFieldValue('paidWaitingTime', '');
          }
        };

        const onChangeIsPayment = ({
          target: { checked },
        }: {
          target: { checked: boolean };
        }) => {
          setFieldValue('isPayment', checked);

          if (!checked) {
            setFieldValue('stepCost', '');
            // setFieldValue('paidWaitingTime', '');
          }
        };

        const onChangeBookingApproval = ({
          target: { checked },
        }: {
          target: { checked: boolean };
        }) => {
          setFieldValue('isNeedBookingApproval', checked);

          if (!checked) {
            setFieldValue('bookingApprovalTime', '');
          }
        };

        const onChangeProofOfUse = ({
          target: { checked },
        }: {
          target: { checked: boolean };
        }) => {
          setFieldValue('isNeedProofOfUse', checked);

          if (!checked) {
            setFieldValue('proofOfUseTime', '');
          }
        };

        const handleStepTime = (value: { id: number; title: string }) => {
          setFieldValue('timeStep', value);

          if (values?.isNeedProofOfUse) {
            setFieldValue('proofOfUseTime', '');
          }
        };

        return (
          <Form
            editMode={editMode}
            setEditMode={setRentEditMode}
            isValid={isValid}
            dirty={dirty}
            onSubmit={onSubmit}
            reset={onReset}
          >
            <Control.Row label={t('AvailableForRent')}>
              <Field
                name="isRentAvailability"
                component={SwitchField}
                label=""
                labelPlacement="start"
                disabled={editMode === 'view'}
                divider={false}
                onChange={onChangeRentAvailability}
              />
            </Control.Row>
            {values.isRentAvailability && (
              <>
                <Control.Row label={t('DescriptionPremisesMobileApplication')}>
                  <Field
                    name="description"
                    component={InputField}
                    label=""
                    placeholder=""
                    mode={editMode}
                    divider={false}
                    multiline
                    rowsMax={4}
                  />
                </Control.Row>
                <Control.Row label={t('BookingConfirmation')}>
                  <Field
                    name="isNeedBookingApproval"
                    component={SwitchField}
                    label=""
                    labelPlacement="start"
                    disabled={editMode === 'view'}
                    divider={false}
                    onChange={onChangeBookingApproval}
                  />
                </Control.Row>
                {values?.isNeedBookingApproval && (
                  <Control.Row label={t('AutomaticBookingConfirmationTime')} required>
                    <Field
                      name="bookingApprovalTime"
                      component={SelectField}
                      label=""
                      mode={editMode}
                      options={timeStepOptions}
                      divider={false}
                      helpText={t('CancellationTimeText')}
                    />
                  </Control.Row>
                )}
                <Control.Row label={t('ConfirmationUse')}>
                  <Field
                    name="isNeedProofOfUse"
                    component={SwitchField}
                    label=""
                    labelPlacement="start"
                    disabled={editMode === 'view'}
                    divider={false}
                    onChange={onChangeProofOfUse}
                  />
                </Control.Row>
                {values?.isNeedProofOfUse && (
                  <Control.Row label={t('UsageConfirmationTime')} required>
                    <Field
                      name="proofOfUseTime"
                      component={SelectField}
                      label=""
                      mode={editMode}
                      options={poofOfUseTimeOptions}
                      divider={false}
                      helpText={t('UsageConfirmationTimeText')}
                    />
                  </Control.Row>
                )}
                <Control.Row label={t('AvailableDays')} required>
                  <Field
                    name="availableDays"
                    label=""
                    divider={false}
                    render={({ field, form }: FieldProps) => (
                      <>
                        <Styled.DayOfTheWeekControl>
                          {days.map((item) => {
                            const selected = field.value.indexOf(item.id) >= 0;

                            const onClick = () => {
                              const value = [...field.value];

                              if (value.indexOf(item.id) >= 0) {
                                const newValue = value.filter(
                                  (arrId) => arrId !== item.id
                                );

                                return form.setFieldValue('availableDays', newValue);
                              }

                              return form.setFieldValue('availableDays', [
                                ...value,
                                item.id,
                              ]);
                            };

                            return (
                              <Styled.DayOfTheWeekItem
                                key={item.id}
                                selected={selected}
                                disabled={editMode === 'view'}
                                type="button"
                                onClick={onClick}
                              >
                                {item.value}
                              </Styled.DayOfTheWeekItem>
                            );
                          })}
                        </Styled.DayOfTheWeekControl>
                        {form.errors.availableDays && (
                          <Styled.ErrorMessage>
                            {form.errors.availableDays as string}
                          </Styled.ErrorMessage>
                        )}
                      </>
                    )}
                  />
                </Control.Row>
                <Control.Row label={t('AvailableTime')} required>
                  <Styled.DualSelectTimeContainer>
                    <Styled.TimeSelectField>
                      <Field
                        name="availableTimeFrom"
                        component={InputField}
                        label=""
                        mode={editMode}
                        type="time"
                        divider={false}
                      />
                    </Styled.TimeSelectField>
                    <Styled.TimeSelectDivider>—</Styled.TimeSelectDivider>
                    <Styled.TimeSelectField>
                      <Field
                        name="availableTimeTo"
                        component={InputField}
                        label=""
                        mode={editMode}
                        type="time"
                        divider={false}
                      />
                    </Styled.TimeSelectField>
                  </Styled.DualSelectTimeContainer>
                </Control.Row>
                <Control.Row label={t('BookingStep')} required>
                  <Field
                    name="timeStep"
                    component={SelectField}
                    label=""
                    mode={editMode}
                    divider={false}
                    options={timeStepOptions}
                    onChange={handleStepTime}
                  />
                </Control.Row>
                <Control.Row label={t('PaidRent')}>
                  <Field
                    name="isPayment"
                    component={SwitchField}
                    label=""
                    labelPlacement="start"
                    disabled={editMode === 'view'}
                    divider={false}
                    onChange={onChangeIsPayment}
                  />
                </Control.Row>
                {values.isPayment && (
                  <>
                    <Control.Row label={t('CostPerStep')} required>
                      <Styled.StepCostWrapper>
                        <Styled.StepCostField>
                          <Field
                            name="stepCost"
                            component={InputField}
                            label=""
                            mode={editMode}
                            type="number"
                            divider={false}
                          />
                        </Styled.StepCostField>
                        <Styled.StepCostHint>{t('rub')}</Styled.StepCostHint>
                      </Styled.StepCostWrapper>
                    </Control.Row>
                    {/* <Control.Row label={t('WaitingForPayment')} required> пока нет функционала оплаты
                      <Field
                        name="paidWaitingTime"
                        component={SelectField}
                        label=""
                        mode={editMode}
                        options={paidWaitingTimeOptions}
                        divider={false}
                      />
                    </Control.Row> */}
                  </>
                )}
              </>
            )}
          </Form>
        );
      }}
    />
  );
};
