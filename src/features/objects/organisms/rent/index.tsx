import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { $currentObjectType, RentGate } from '@features/objects/models';
import { MeetingRoomForm } from './forms/meeting-room-form';
import * as Styled from './styled';

export const Rent: FC = () => {
  useGate(RentGate);

  const [currentObjectType] = useUnit([$currentObjectType]);

  const renderForm = () => {
    switch (currentObjectType) {
      case 'meeting_room':
        return <MeetingRoomForm />;
      default:
        return null;
    }
  };

  return <Styled.Container>{renderForm()}</Styled.Container>;
};
