import { FC, SyntheticEvent } from 'react';
import { useUnit } from 'effector-react';
import { Tab } from '@mui/material';
import {
  $currentObject,
  $currentObjectType,
  $currentTab,
  $selectionModel,
  changeQrModalVisibility,
  changeTab,
  openDeleteDialog,
  tabs,
  $cardEditMode,
  $settingsEditMode,
  $rentEditMode,
  $rulesEditMode,
} from '@features/objects/models';
import { TabCategory } from '@features/objects/types/tabs';
import { DeleteButton, QrCodeButton } from '@ui/index';
import { CreationButton } from '../../molecules/creation-button';
import * as Styled from './styled';

export const Header: FC = () => {
  const [
    currentObject,
    currentObjectType,
    currentTab,
    selectionModel,
    cardEditMode,
    settingsEditMode,
    rentEditMode,
    rulesEditMode,
  ] = useUnit([
    $currentObject,
    $currentObjectType,
    $currentTab,
    $selectionModel,
    $cardEditMode,
    $settingsEditMode,
    $rentEditMode,
    $rulesEditMode,
  ]);

  if (!currentObject) return null;

  const onTabChange = (event: SyntheticEvent, tabValue: TabCategory) => {
    changeTab(tabValue);
  };

  const renderedTabs = tabs.map((tab) => {
    const isDisabled =
      tab.access && currentObjectType && tab.access?.indexOf(currentObjectType) < 0;
    return isDisabled ? null : (
      <Tab key={tab.value} label={tab.label} value={tab.value} />
    );
  });

  const isEditMode =
    cardEditMode === 'edit' ||
    settingsEditMode === 'edit' ||
    rulesEditMode === 'edit' ||
    rentEditMode === 'edit';

  const renderDeleteButton = () => {
    if (currentObjectType === 'complex') return null;
    return (
      <DeleteButton
        kind="negative"
        disabled={isEditMode || selectionModel?.length}
        onClick={() => openDeleteDialog()}
      />
    );
  };

  const renderQrButton = () => {
    if (currentTab !== 'objects' || currentObjectType !== 'building') return null;
    return (
      <QrCodeButton
        disabled={!selectionModel.length || isEditMode}
        clickHandler={() => {
          changeQrModalVisibility(true);
        }}
      />
    );
  };

  return (
    <div>
      <Styled.Container>
        <Styled.Title>{currentObject?.title}</Styled.Title>
        <Styled.Actions>
          {renderQrButton()}
          {renderDeleteButton()}
          <CreationButton disabled={isEditMode} />
        </Styled.Actions>
      </Styled.Container>
      <Styled.TabList onChange={onTabChange}>{renderedTabs}</Styled.TabList>
      <Styled.Divider />
    </div>
  );
};
