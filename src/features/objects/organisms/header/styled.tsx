import { styled } from '@mui/material';
import { TabList as TabListMui } from '@mui/lab';
import { Divider as DefaultDivider } from '@ui/index';

export const Container = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  alignItems: 'center',
  padding: '20px 0 0 0',
}));

export const Title = styled('span')(() => ({
  fontSize: 18,
  fontWeight: 700,
  width: '100%',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'elipsis',
}));

export const Actions = styled('div')(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'end',
  gap: 5,
}));

export const TabList = styled(TabListMui)(() => ({
  paddingTop: 10,
}));

export const Divider = styled(({ style }: { style?: object }) => (
  <DefaultDivider style={style} />
))`
  margin-top: 0px;
`;
