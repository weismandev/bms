import { styled } from '@mui/material/styles';

export const FormContainer = styled('div')(() => ({
  width: '100%',
  height: '100%',
  maxWidth: 888,
  margin: '0 auto',
}));

export const FormActions = styled('div')(() => ({
  display: 'flex',
  gap: 10,
  margin: '10px 0px',
}));
