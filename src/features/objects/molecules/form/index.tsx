import { FC } from 'react';
import { FormType } from '@features/objects/types/form';
import { CancelButton, CustomScrollbar, EditButton, SaveButton } from '@ui/index';
import * as Styled from './styled';

export const Form: FC<FormType> = ({
  children,
  editMode,
  setEditMode,
  isValid,
  dirty,
  onSubmit,
  reset,
}) => {
  const handleEdit = () => {
    setEditMode('edit');
  };
  const handleSave = () => {
    setEditMode('view');
    onSubmit();
  };
  const handleCancel = () => {
    setEditMode('view');
    reset();
  };

  const actionButtons =
    editMode === 'view' ? (
      <EditButton onClick={handleEdit} />
    ) : (
      <>
        <SaveButton onClick={handleSave} disabled={!isValid || !dirty} />
        <CancelButton onClick={handleCancel} />
      </>
    );

  return (
    <Styled.FormContainer>
      <Styled.FormActions>{actionButtons}</Styled.FormActions>
      <CustomScrollbar style={{ height: 'calc(100vh - 315px)' }}>
        <form style={{ paddingRight: 10 }}>{children}</form>
      </CustomScrollbar>
    </Styled.FormContainer>
  );
};
