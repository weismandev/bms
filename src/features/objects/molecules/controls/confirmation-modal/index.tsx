import { Modal } from '@ui/index';
import { Header, HeaderStyled, ContentStyled, ButtonContainter, ButtonAccept, ButtonTypo, ButtonDenied, ButtonTypoDenied } from './styles';
import { useTranslation } from 'react-i18next';
import { $isConfirmationDeleteModalOpen, changeConfirmDeleteModalVisibility, deleteObjects } from '@features/objects/models';
import { useStore } from 'effector-react';

export const ConfirmationModal = () => {
    const { t } = useTranslation();
    const isOpen = useStore($isConfirmationDeleteModalOpen);

    const header = 
        <Header>
            <HeaderStyled>Предупреждение</HeaderStyled>
        </Header>
    ;

    const content = <ContentStyled>Выбранные объекты и все вложенные в них объекты будут безвозвратно удалены. Продолжить удаление?</ContentStyled>

    const actions = 
        <ButtonContainter>
            <ButtonAccept onClick={() => { deleteObjects(); changeConfirmDeleteModalVisibility(false); }}>
                <ButtonTypo>Продолжить</ButtonTypo>
            </ButtonAccept>
            <ButtonDenied onClick={() => changeConfirmDeleteModalVisibility(false)}>
                <ButtonTypoDenied>Отмена</ButtonTypoDenied>
            </ButtonDenied>
        </ButtonContainter>
    return (
        <Modal
            isOpen={isOpen}
            header={header}
            content={content}
            actions={actions}
        />
    )
}