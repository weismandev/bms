import styled from '@emotion/styled';

export const Header = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export const HeaderStyled = styled.span`
    font-size: 24px;
    font-weight: 500;
    line-height: 32px;
    letter-spacing: 0px;
    color: rgba(37, 40, 52, 0.87);
`;

export const ContentStyled = styled.span`
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0px;
    text-align: left;
    color: rgba(37, 40, 52, 0.87);
`;

export const ButtonContainter = styled.div`
    display: flex;
    align-items: end;
    gap: 8px;
`;

export const ButtonAccept = styled.button`
    background: rgba(225, 63, 61, 1);
    border-radius: 16px;
    border: none;
    padding: 4px 16px 4px 16px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
`;

export const ButtonDenied = styled.button`
    background: #fff;
    border: 1px solid rgba(31, 135, 229, 0.5);
    border-radius: 16px;
    padding: 4px 16px 4px 16px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer; 
`;

export const ButtonTypo = styled.span`
    color: rgba(255, 255, 255, 1);
    font-size: 14px;
    font-weight: 500;
    line-height: 24px;
    letter-spacing: 0px;
`;

export const ButtonTypoDenied = styled.span`
    color: rgba(31, 135, 229, 1);
    font-size: 14px;
    font-weight: 500;
    line-height: 24px;
    letter-spacing: 0px;
`;