import { Modal } from '@ui/index';
import { Header, HeaderStyled, ContentStyled, ButtonContainter, ButtonAccept, ButtonTypo, ButtonDenied, ButtonTypoDenied } from './styles';
import { useTranslation } from 'react-i18next';
import { $isDeleteModalOpen, changeDeleteModalVisibility, changeConfirmDeleteModalVisibility } from '@features/objects/models';
import { useStore } from 'effector-react';

export const DeleteModal = () => {
    const { t } = useTranslation();
    const isOpen = useStore($isDeleteModalOpen);

    const header = 
        <Header>
            <HeaderStyled>Удалить объекты?</HeaderStyled>
        </Header>
    ;

    const content = <ContentStyled>Вы действительно хотите удалить выбранные объекты</ContentStyled>

    const actions = 
        <ButtonContainter>
            <ButtonAccept onClick={() => { changeDeleteModalVisibility(false); changeConfirmDeleteModalVisibility(true); }}>
                <ButtonTypo>Удалить</ButtonTypo>
            </ButtonAccept>
            <ButtonDenied onClick={() => changeDeleteModalVisibility(false)}>
                <ButtonTypoDenied>Отмена</ButtonTypoDenied>
            </ButtonDenied>
        </ButtonContainter>
    return (
        <Modal
            isOpen={isOpen}
            header={header}
            content={content}
            actions={actions}
        />
    )
}