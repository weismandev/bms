import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  position: 'relative',
  display: 'grid',
  gap: 10,
  width: '100%',
  margin: '10px 0px',
  borderRadius: 16,
  overflow: 'hidden',
  background: 'rgba(0, 0, 0, 0.05)',
}));

export const Empty = styled('div')(() => ({
  height: 300,
  margin: '10px 0px',
  display: 'grid',
  position: 'relative',
  background: 'rgba(0, 0, 0, 0.05)',
  placeItems: 'center',
  borderRadius: 16,
  color: 'rgba(0, 0, 0, 0.4)',
  fontWeight: 'bold',
}));

export const Overlay = styled('div')(() => ({
  position: 'absolute',
  width: '100%',
  height: '100%',
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  background: 'rgba(255, 255, 255, 0.8)',
  opacity: 0,
  zIndex: 100,
  transition: 'all 0.2s',

  '&:hover': {
    opacity: 1,
  },
}));

export const Title = styled('span')(() => ({
  fontSize: 18,
}));
