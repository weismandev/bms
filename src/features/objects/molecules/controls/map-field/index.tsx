import { FC, useState } from 'react';
import { Map, Placemark, YMaps } from 'react-yandex-maps';
import { MapFieldProps } from './types';
import * as Styled from './styled';
import { useTranslation } from 'react-i18next';

export const MapField: FC<MapFieldProps> = ({
  field: { value, name },
  form,
  mode = 'view',
}) => {
  const { t } = useTranslation();
  /** Стейст с координатами. Если их нет, задаем дефолтные в центре Москвы */
  const [coordsState, setCoordsState] = useState<any>(
    value ? [+value.latitude, +value.longitude] : [55.753312435866455, 37.621825185392794]
  );
  const isEditMode = mode === 'edit';

  /** Если координаты не переданы и не режим редактирования,
   * то показываем заглушку без карты */
  if (!value && !isEditMode) {
    return <Styled.Empty>{t('coordsNotExist')}</Styled.Empty>;
  }

  /** Хендлер изменения марки на карте */
  const handleMarkChange = (event: any) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const newCoords = event.get('coords');
    setCoordsState(newCoords);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    form.setFieldValue(name, {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      latitude: newCoords[0].toString(),
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      longitude: newCoords[1].toString(),
    });
  };

  const renderOverlay = () => {
    if (!isEditMode) {
      return (
        <Styled.Overlay>
          <Styled.Title>
            Чтобы изменить расположение объекта, включите режим редактированя
          </Styled.Title>
        </Styled.Overlay>
      );
    }
    return null;
  };

  return (
    <YMaps>
      <Styled.Container>
        {renderOverlay()}

        <Map
          defaultState={{ center: coordsState, zoom: 15 }}
          width="100%"
          height={400}
          onClick={handleMarkChange}
          disabled
        >
          <Placemark defaultGeometry={coordsState} geometry={coordsState} />
        </Map>
      </Styled.Container>
    </YMaps>
  );
};
