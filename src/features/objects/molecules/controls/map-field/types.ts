import { ObjectInstance } from '@features/objects/types/object';

export type MapFieldProps = {
  field: {
    name: string;
    value: ObjectInstance['coordinates'];
  };
  form: any;
  mode: 'view' | 'edit';
};
