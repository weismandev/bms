import { api } from '@api/api2';
import { ImageFieldFileUpload } from './types';

/** Загрузка файлов */
const uploadFile = (payload: ImageFieldFileUpload) =>
  api.v2('/system/files/save', payload, {}, false);

export const imageFieldApi = {
  uploadFile,
};
