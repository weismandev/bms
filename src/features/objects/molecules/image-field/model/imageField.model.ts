import { createEffect, createEvent, createStore } from 'effector';
import { pending } from 'patronum';
import { createGate } from 'effector-react';
import { ImageFieldFileUpload, ImageFieldImage } from '../types';

export const ImageFieldGate = createGate();
export const { open: imageFieldMounted, close: imageFieldUnmounted } = ImageFieldGate;

export const fxUploadImage = createEffect<ImageFieldFileUpload, any, Error>();

export const $metaData = createStore<any>(null);
export const $selectedImage = createStore<ImageFieldImage>(null);
export const $isImageUploading = pending({ effects: [fxUploadImage] });

export const selectImage = createEvent<ImageFieldImage>();
export const deleteImage = createEvent<void>();
