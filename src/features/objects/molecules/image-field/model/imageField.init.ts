// @ts-nocheck
import { sample } from 'effector';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { imageFieldApi } from '../api';
import {
  $metaData,
  $selectedImage,
  deleteImage,
  fxUploadImage,
  imageFieldMounted,
  imageFieldUnmounted,
  selectImage,
} from './imageField.model';

fxUploadImage.use(imageFieldApi.uploadFile);

$metaData.on(imageFieldMounted, (_, data) => data);

/** При выборе фото записываем его */
$selectedImage.on(selectImage, (_, image) => image);

/** При изменении выбранного фото загружаем его на бек */
sample({
  source: $selectedImage,
  filter: (image) => Boolean(image),
  fn: (image) => ({
    type: 'image',
    file: image,
  }),
  target: fxUploadImage,
});

/** При успешной загрузке фото устанавливаем значение в форму */
sample({
  source: $metaData,
  clock: fxUploadImage.doneData,
  fn: ({ form, field }, uploadedImage) => {
    form.setFieldValue(field.name, uploadedImage.file);
  },
});

/** При удалении фото устанавливаем значение в форму */
sample({
  source: $metaData,
  clock: deleteImage,
  fn: ({ form, field }) => {
    form.setFieldValue(field.name, null);
  },
});

/** Сброс */
reset({
  clock: [imageFieldUnmounted, signout, deleteImage],
  target: $selectedImage,
});
