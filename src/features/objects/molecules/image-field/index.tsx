import { FC, useState, useEffect } from 'react';
import ReactFileReader from 'react-file-reader';
import { useTranslation } from 'react-i18next';
import { useGate, useUnit } from 'effector-react';
import { Loader } from '@ui/index';
import {
  $isImageUploading,
  $selectedImage,
  deleteImage,
  ImageFieldGate,
  selectImage,
} from './model';
import * as Styled from './styled';
import { ImageFieldProps } from './types';

export const ImageField: FC<ImageFieldProps> = ({ field, form, mode = 'view' }) => {
  const { t } = useTranslation();
  const [isHovered, setIsHovered] = useState(false);
  const [selectedImage, isImageUploading] = useUnit([$selectedImage, $isImageUploading]);

  useGate(ImageFieldGate, { form, field });

  const isEditMode = mode === 'edit';
  const isExistImage = selectedImage || field.value?.url;

  useEffect(() => {
    if (!isEditMode && selectedImage) {
      selectImage(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEditMode]);

  const handleFiles = (files: File[]) =>
    files.length > 0 ? selectImage(files[0]) : null;

  const onMouseEnter = () => (isEditMode ? setIsHovered(true) : null);
  const onMouseLeave = () => (isEditMode ? setIsHovered(false) : null);

  const renderActions = () => {
    if (isHovered && isEditMode) {
      const handleDeleteClick = () => deleteImage();

      return (
        <>
          <Styled.WrapperFileReader>
            <ReactFileReader fileTypes="image/*" handleFiles={handleFiles}>
              <Styled.Action>
                <Styled.WrapperImage>
                  <Styled.AddPhotoAlternateOutlinedIcon />
                </Styled.WrapperImage>
                <Styled.Title variant="body1">{t('UploadPht')}</Styled.Title>
              </Styled.Action>
            </ReactFileReader>
          </Styled.WrapperFileReader>
          {isExistImage && (
            <Styled.Action onClick={handleDeleteClick}>
              <Styled.WrapperImage>
                <Styled.DeleteOutlineIcon />
              </Styled.WrapperImage>
              <Styled.Title variant="body1">{t('remove')}</Styled.Title>
            </Styled.Action>
          )}
        </>
      );
    }

    return null;
  };

  const renderEmptyImage = () =>
    !isHovered && !field.value ? <Styled.ImageOutlinedIcon /> : null;

  const renderImage = () => {
    if (selectedImage) {
      return (
        <Styled.Preview isHovered={isHovered} src={URL.createObjectURL(selectedImage)} />
      );
    }

    if (field?.value?.url) {
      return <Styled.Preview isHovered={isHovered} src={field.value.url} />;
    }

    return null;
  };

  return (
    <Styled.Container
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      isHovered={isHovered}
    >
      <Loader isLoading={isImageUploading} />
      {renderEmptyImage()}
      {renderActions()}
      {renderImage()}
    </Styled.Container>
  );
};
