import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import {
  ImageOutlined as ImageOutlinedMui,
  AddPhotoAlternateOutlined as AddPhotoAlternateOutlinedIconMui,
  DeleteOutline as DeleteOutlineMui,
} from '@mui/icons-material';

export const Container = styled('div')(({ isHovered }: { isHovered: boolean }) => ({
  position: 'relative',
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  height: 300,
  background: isHovered ? 'rgba(25, 28, 41, 0.87)' : '#EDF6FF',
  borderRadius: 16,
  margin: '10px 0',
}));

export const ImageOutlinedIcon = styled(ImageOutlinedMui)(() => ({
  height: 90,
  width: 90,
  color: 'rgba(210, 223, 232, 1)',
}));

export const AddPhotoAlternateOutlinedIcon = styled(AddPhotoAlternateOutlinedIconMui)(
  () => ({
    height: 35,
    width: 35,
    color: 'rgba(255, 255, 255, 1)',
  })
);

export const DeleteOutlineIcon = styled(DeleteOutlineMui)(() => ({
  height: 35,
  width: 35,
  color: 'rgba(255, 255, 255, 1)',
}));

export const Title = styled(Typography)(() => ({
  fontWeight: 400,
  fontSize: 16,
  color: 'rgba(255, 255, 255, 1)',
}));

export const Action = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 10,
  zIndex: 5,
  cursor: 'pointer',
}));

export const WrapperImage = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'center',
}));

export const Preview = styled('img')(({ isHovered }: { isHovered: boolean }) => ({
  position: 'absolute',
  width: '100%',
  height: 300,
  borderRadius: 16,
  objectFit: 'cover',
  filter: isHovered ? 'brightness(30%)' : 'brightness(1)',
}));

export const WrapperFileReader = styled('div')(() => ({
  zIndex: 5,
}));
