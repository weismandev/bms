import styled from '@emotion/styled';
import { Tooltip } from '@mui/material';
import { $selectionModel } from '@features/objects/models';
import { useGate, useUnit, useStore } from 'effector-react';
import { deleteObjects, changeDeleteModalVisibility } from '@features/objects/models';

const Container = styled.div`
    padding: 12px;
`;

const Icon = styled.div`
    width: 32px;
    height: 32px;
    border-radius: 48px;

    box-shadow: 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
    box-shadow: 0px 6px 10px 0px rgba(0, 0, 0, 0.14);
    box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2);
`;

// background: rgba(25, 28, 41, 0.54);


export const DeleteIcon = () => {
    const [selection] = useStore($selectionModel);

    return (
        <Container>
            <Tooltip title={'Удалить объекты'}>
                <Icon onClick={() => { if (selection?.length) changeDeleteModalVisibility(true) }}>
                    <svg style={{cursor: selection?.length ? 'pointer' : 'auto'}} width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="32" height="32" rx="16" fill={selection?.length ? '#E13F3D' : 'rgba(25, 28, 41, 0.54)'}/>
                        <path d="M20 13V23H12V13H20ZM18.5 7H13.5L12.5 8H9V10H23V8H19.5L18.5 7ZM22 11H10V23C10 24.1 10.9 25 12 25H20C21.1 25 22 24.1 22 23V11Z" fill="white"/>
                    </svg>
                </Icon>
            </Tooltip>
        </Container>
    )
}