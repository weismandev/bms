import { FC } from 'react';
import { Event } from 'effector';
import { useTranslation } from 'react-i18next';
import { VerticalAlignBottom } from '@mui/icons-material';
import { FormEditMode } from '@features/objects/types/form';
import { EditButton, SaveButton, CancelButton, ActionIconButton } from '@ui/index';
import * as Styled from './styled';

interface Props {
  mode?: FormEditMode;
  onEdit?: () => FormEditMode;
  onCancel?: () => void;
  isExportDisabled?: boolean;
  onExport?: Event<void>;
}

export const RulesToolbar: FC<Props> = ({
  mode,
  onEdit,
  onCancel,
  isExportDisabled = false,
  onExport,
}) => {
  const { t } = useTranslation();
  return (
    <Styled.Container>
      {mode === 'view' ? (
        <>
          <EditButton onClick={onEdit} />
          <ActionIconButton disabled={isExportDisabled} onClick={onExport}>
            <VerticalAlignBottom titleAccess={t('export') ?? ''} />
          </ActionIconButton>
        </>
      ) : (
        <>
          <SaveButton type="submit" />
          <CancelButton onClick={onCancel} />
        </>
      )}
    </Styled.Container>
  );
};
