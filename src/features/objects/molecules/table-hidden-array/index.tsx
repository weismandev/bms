import { FC, useState } from 'react';
import { Popover } from '@mui/material';
import * as Styled from './styled';

type Props = {
  children: JSX.Element[];
};

export const TableHiddenArray: FC<Props> = ({ children }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  if (!children.length) {
    return null;
  }

  if (children.length === 1) {
    return children;
  }

  const open = Boolean(anchorEl);

  const handlePopoverOpen = (event: React.MouseEvent<HTMLButtonElement>) =>
    setAnchorEl(event.currentTarget);

  const handlePopoverClose = () => setAnchorEl(null);

  return (
    <Styled.Container>
      <Styled.Button
        variant="text"
        size="small"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      >
        {children[0]}
      </Styled.Button>
      <Popover
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        sx={{
          pointerEvents: 'none',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Styled.List>{children.slice(1).map((child) => child)}</Styled.List>
      </Popover>
    </Styled.Container>
  );
};
