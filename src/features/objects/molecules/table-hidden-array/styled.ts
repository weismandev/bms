import { styled, Button as ButtonMui } from '@mui/material';

export const Container = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: 'auto auto',
  justifyContent: 'start',
  alignContent: 'center',
}));

export const Button = styled(ButtonMui)(() => ({
  minWidth: 'auto',
  margin: '0 10px',
}));

export const List = styled('div')(() => ({
  display: 'grid',
  gap: 10,
  padding: 10,
  background: '#fcfcfc',
}));
