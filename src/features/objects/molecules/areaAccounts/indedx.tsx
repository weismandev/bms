import { FC } from 'react';
import { ObjectInstance } from '@features/objects/types/object';
import * as Styled from './styled';

export const AreaAccounts: FC<{ areaAccounts: ObjectInstance['area_accounts'] }> = ({
  areaAccounts,
}) => {
  if (!areaAccounts?.length) return null;

  const renderAccounts = () =>
    areaAccounts
      .filter((account) => !account.archived)
      .map((account) => account.account_number)
      .join(', ');

  const renderArchivedAccounts = () =>
    areaAccounts
      .filter((account) => account.archived)
      .map((account) => account.account_number)
      .join(', ');

  return (
    <Styled.Container>
      <Styled.Active>{renderAccounts()}</Styled.Active>
      <Styled.Archived>{renderArchivedAccounts()}</Styled.Archived>
    </Styled.Container>
  );
};
