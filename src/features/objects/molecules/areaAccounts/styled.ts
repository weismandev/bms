import { styled } from '@mui/material/styles';

export const Container = styled('div')(() => ({
  display: 'grid',
  gap: 12,
}));

export const Active = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
}));

export const Archived = styled('div')(() => ({
  opacity: 0.7,
}));
