import { Row } from './row';
import { Array } from './array';

export const Control = {
  Row,
  Array,
};
