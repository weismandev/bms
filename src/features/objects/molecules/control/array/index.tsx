import { FC } from 'react';
import { ControlArray } from '@features/objects/types/control';
import * as Styled from './styled';

export const Array: FC<ControlArray> = ({ children }) => (
  <Styled.ArrayContainer>{children}</Styled.ArrayContainer>
);
