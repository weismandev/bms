import { styled } from '@mui/material/styles';

export const ArrayContainer = styled('div')(() => ({
  display: 'grid',
  gap: 5,
  gridTemplateColumns: '1fr auto',
  alignItems: 'center',
  alignContent: 'center',
}));
