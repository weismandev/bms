import { FC } from 'react';
import { ControlRow } from '@features/objects/types/control';
import * as Styled from './styled';

export const Row: FC<ControlRow> = ({
  label,
  unit = null,
  hideDivider = false,
  required,
  children,
}) => {
  const renderUnit = () => (unit ? ` (${unit})` : null);

  const renderLabel = () =>
    required ? (
      <Styled.ControlLabel>
        {label}
        {renderUnit()}
        <Styled.ControlDot>*</Styled.ControlDot>
      </Styled.ControlLabel>
    ) : (
      <Styled.ControlLabel>
        {label}
        {renderUnit()}
      </Styled.ControlLabel>
    );

  return (
    <Styled.ControlContainer hideDivider={hideDivider}>
      {renderLabel()}

      <Styled.ControlContent>{children}</Styled.ControlContent>
    </Styled.ControlContainer>
  );
};
