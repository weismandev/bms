import { styled } from '@mui/material/styles';

export const ControlContainer = styled('div')(
  ({ hideDivider }: { hideDivider?: boolean }) => ({
    width: '100%',
    minHeight: 45,
    display: 'grid',
    gridTemplateColumns: '1fr 0.5fr',
    alignItems: 'center',
    gap: 10,
    borderBottom: hideDivider ? 'none' : '1px solid #E7E7EC',
  })
);

export const ControlLabel = styled('div')(() => ({
  display: 'flex',
  color: '#7D7D8E',
}));

export const ControlDot = styled('div')(() => ({
  paddingLeft: 3,
  color: 'red',
}));

export const ControlContent = styled('div')(() => ({
  margin: '5px 0',
  fontWeight: '500',
  color: '#65657B',
  fontSize: 13,
}));
