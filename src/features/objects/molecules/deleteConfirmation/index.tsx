import { useUnit } from 'effector-react';
import { Button, Dialog, DialogActions, DialogTitle } from '@mui/material';
import {
  $isDeleteDialogVisible,
  acceptDelete,
  cancelDelete,
} from '@features/objects/models';
import { useTranslation } from 'react-i18next';

export const DeleteConfirmation = () => {
  const { t } = useTranslation();
  const [isDeleteDialogVisible] = useUnit([$isDeleteDialogVisible]);

  return (
    <Dialog open={isDeleteDialogVisible}>
      <DialogTitle>{t('DeleteTheObject')}</DialogTitle>
      <DialogActions>
        <Button autoFocus onClick={() => cancelDelete()}>
          {t('cancel')}
        </Button>
        <Button color="error" onClick={() => acceptDelete()}>
          {t('remove')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
