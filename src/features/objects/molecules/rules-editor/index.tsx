import { FC, useState, useEffect, useMemo } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { useTranslation } from 'react-i18next';
import {
  EditorState,
  ContentState,
  convertToRaw,
  DraftInlineStyleType,
  Modifier,
} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { FormEditMode } from '@features/objects/types/form';
import * as Styled from './styled';
import { useStyles } from './styles';

interface Props {
  onChange?: (value: string) => void;
  mode?: FormEditMode;
  editorStyle?: object;
  toolbarStyle?: object;
  placeholder?: string;
  form?: {
    errors?: { [key: string]: string };
  };
  field?: {
    name?: string;
    value?: string;
  };
}

export const RulesEditor: FC<Props> = ({
  onChange,
  mode = 'view',
  editorStyle,
  toolbarStyle,
  placeholder,
  form,
  field,
}) => {
  const { t } = useTranslation();
  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(ContentState.createFromText(''))
  );

  const classes = useStyles();

  const toolbar = {
    options: [
      'inline',
      'blockType',
      'fontSize',
      'fontFamily',
      'list',
      'textAlign',
      'colorPicker',
      'link',
      'image',
      'remove',
      'history',
    ],
    blockType: {
      inDropdown: true,
      options: ['Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'Blockquote', 'Code'],
      className: classes.blockType,
    },
  };

  const error = form?.errors && field?.name ? form.errors[field.name] : null;
  const fieldValue = field?.value || '';

  const onEditorStateChange = (value: EditorState) => setEditorState(value);

  const onChangeValue = () => {
    const rawContentState = convertToRaw(editorState.getCurrentContent());

    const rawEscapeQuptes = {
      ...rawContentState,
      blocks: rawContentState.blocks.map((block) => ({
        ...block,
        inlineStyleRanges: block.inlineStyleRanges.map((i) => ({
          ...i,
          style: i.style.replace(/"/g, '&quot;') as DraftInlineStyleType,
        })),
      })),
    };

    const html = draftToHtml(rawEscapeQuptes);

    if (onChange) {
      onChange(html);
    }
  };

  const handlePastedText = (val: string) => {
    const pastedContent = Modifier.insertText(
      editorState.getCurrentContent(),
      editorState.getSelection(),
      val.replace(/\s?style=".+"/g, '')
    );

    setEditorState(EditorState.push(editorState, pastedContent, 'insert-characters'));

    return true;
  };

  const setContent = (text: string) => {
    const contentBlock = htmlToDraft(text);
    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);

    setEditorState(EditorState.createWithContent(contentState));
  };

  const locale = useMemo(() => localStorage.getItem('lang') || 'ru', []);

  useEffect(() => {
    if (mode === 'edit') {
      setContent(fieldValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mode]);

  useEffect(() => {
    if (!fieldValue) {
      setContent(fieldValue);
    }
  }, [fieldValue]);

  if (mode === 'view') {
    return <Styled.ViewEditor dangerouslySetInnerHTML={{ __html: fieldValue }} />;
  }

  return (
    <>
      <Styled.EditorWrapper>
        <Editor
          toolbar={toolbar}
          placeholder={placeholder}
          localization={{
            locale,
            translations: {
              'components.controls.inline.monospace': t('Monospace'),
              'components.controls.remove.remove': t('RemoveFormatting'),
            },
          }}
          editorState={editorState}
          onEditorStateChange={onEditorStateChange}
          onChange={onChangeValue}
          handlePastedText={handlePastedText}
          editorStyle={editorStyle}
          toolbarStyle={toolbarStyle}
        />
      </Styled.EditorWrapper>
      {error && <Styled.ErrorMessage>{error}</Styled.ErrorMessage>}
    </>
  );
};
