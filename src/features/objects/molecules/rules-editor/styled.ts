import { styled, Typography } from '@mui/material';

export const ErrorMessage = styled(Typography)(() => ({
  color: '#d32f2f',
  fontSize: '0.75rem',
}));

export const ViewEditor = styled('div')(() => ({
  padding: '0px 24px',
  '& ul': {
    listStyleType: 'disc',
  },
}));

export const EditorWrapper = styled('div')(() => ({
  '.rdw-center-aligned-block *': {
    textAlign: 'center !important',
  },
  '.rdw-left-aligned-block *': {
    textAlign: 'left !important',
  },
  '.rdw-right-aligned-block *': {
    textAlign: 'right !important',
  },
  '.rdw-justify-aligned-block *': {
    textAlign: 'justify !important',
  },
}));
