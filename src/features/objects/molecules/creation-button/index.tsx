import { FC, MouseEvent, useState } from 'react';
import { useUnit } from 'effector-react';
import lodashGet from 'lodash/get';
import { MenuItem } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { $currentObject, creationMenu, openCreation } from '@features/objects/models';
import { ObjectCreationMenuItem } from '@features/objects/types/creation';
import { ObjectType } from '@features/objects/types/object';
import * as Styled from './styled';
import { useTranslation } from 'react-i18next';

interface Props {
  disabled?: boolean;
}

export const CreationButton: FC<Props> = ({ disabled = false }) => {
  const { t } = useTranslation();
  const [currentObject] = useUnit([$currentObject]);
  const currentObjectType = currentObject?.type;
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  if (!currentObjectType) return null;

  const menuItems = lodashGet(creationMenu, currentObjectType) || [];

  if (!menuItems.length) return null;

  const handleMenuClick = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleTypeClick = (type: ObjectType) => {
    openCreation(type);
    handleMenuClose();
  };

  const renderMenuItems = () =>
    menuItems.map((item: ObjectCreationMenuItem) => (
      <MenuItem
        onClick={() => {
          handleTypeClick(item.type);
        }}
        disableRipple
      >
        {item.label}
      </MenuItem>
    ));

  return (
    <>
      <Styled.CreationButton
        aria-haspopup="true"
        aria-expanded="true"
        variant="contained"
        disableElevation
        endIcon={<KeyboardArrowDownIcon />}
        onClick={handleMenuClick}
        disabled={disabled}
      >
        {t('create')}
      </Styled.CreationButton>
      <Styled.CreationMenu anchorEl={anchorEl} open={open} onClose={handleMenuClose}>
        {renderMenuItems()}
      </Styled.CreationMenu>
    </>
  );
};
