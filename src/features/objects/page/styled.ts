import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: '380px 1fr',
  gap: 24,
  padding: '12px 24px 24px 24px',
}));
