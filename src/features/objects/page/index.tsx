import { FC } from 'react';
import { useGate, useUnit } from 'effector-react';
import { ErrorMessage } from '@ui/index';
import { HaveSectionAccess } from '@features/common';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { DeleteConfirmation } from '@features/objects/molecules/deleteConfirmation';
import {
  $error,
  $isCreationMode,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  ObjectsPageGate,
} from '../models';
import { Sidebar } from '../organisms/sidebar';
import { Creation } from '../organisms/creation';
import { Root } from '../organisms/root';
import * as Styled from './styled';

const ObjectsPage: FC = () => {
  useGate(ObjectsPageGate);

  const [isCreationMode, isErrorDialogOpen, error] = useUnit([
    $isCreationMode,
    $isErrorDialogOpen,
    $error,
  ]);

  return (
    <>
      <ColoredTextIconNotification />

      <Styled.Container>
        <Sidebar />
        {isCreationMode ? <Creation /> : <Root />}
      </Styled.Container>

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <DeleteConfirmation />
    </>
  );
};

const RestrictedObjectsPage: FC = () => (
  <HaveSectionAccess>
    <ObjectsPage />
  </HaveSectionAccess>
);

export { RestrictedObjectsPage as ObjectsPage };
