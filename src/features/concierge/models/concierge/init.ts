import { attach, forward, merge, sample } from 'effector';
import { interval } from 'patronum/interval';
import { signout, $isAuthenticated } from '@features/common';
import { conciergeApi } from '@features/concierge/api';
import {
  $error,
  $isCallElevatorLoading,
  $isLoading,
  $liftData,
  TICK_INTERVAL,
  callElevator,
  fxCallElevator,
  fxGetLiftlist,
  fxGetTickLiftlist,
  pageMounted,
  pageUnmounted,
  $successCall,
  $liftsOrder,
  changeLiftOrder,
  setLiftOrder,
  fxSaveInStorageBase,
  fxGetFromStorageBase,
  LOCAL_STORAGE_KEY,
  $liftsOrderFromStore,
  $successPopup,
  showSuccessPopup,
  hideSuccessPopup,
} from './model';

$successPopup
  .on(showSuccessPopup, () => true)
  .on(hideSuccessPopup, () => false)
  .reset(pageUnmounted, signout);

const fxSaveInStorage = attach({ effect: fxSaveInStorageBase });
const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

fxGetLiftlist.use(conciergeApi.getConciergeElevators);
fxGetTickLiftlist.use(conciergeApi.getConciergeElevators);
fxCallElevator.use(conciergeApi.callElevator);

export const resolveNewLiftOrder = (order: {
  state: string[];
  sourceIndex: number;
  destinationIndex: number;
}) => {
  if (order.destinationIndex === order.sourceIndex) return order.state;

  if (order.sourceIndex < order.destinationIndex) {
    const changed = [...order.state];
    changed.splice(order.destinationIndex + 1, 0, order.state[order.sourceIndex]);
    changed.splice(order.sourceIndex, 1);
    return changed;
  }

  const changed = [...order.state];
  changed.splice(order.destinationIndex, 0, order.state[order.sourceIndex]);
  changed.splice(order.sourceIndex + 1, 1);
  return changed;
};

$liftsOrder
  .on(changeLiftOrder, (state, order) => resolveNewLiftOrder({ state, ...order }))
  .on(setLiftOrder, (_, data) => data);

$liftsOrderFromStore.on(fxGetFromStorage.done, (_, { result }) => {
  if (!result.liftsOrder || result.liftsOrder.length === 0) return;

  return result.liftsOrder;
});

$liftData
  .on(fxGetLiftlist.doneData, (_, data) => data)
  .on(fxGetTickLiftlist.doneData, (_, data) => data)
  .reset(pageUnmounted, signout);

$successCall
  .on(fxCallElevator.done, (state, _) => state + 1)
  .reset(pageUnmounted, signout);

/** Открываю popup при успешной отправке лифта */
sample({
  clock: fxCallElevator.doneData,
  target: showSuccessPopup,
});

/** Вызов лифта */
sample({
  clock: callElevator,
  target: fxCallElevator,
});

/** Запрос состояния лифтов после успешного вызова лифта */
sample({
  clock: fxCallElevator.done,
  target: fxGetTickLiftlist,
});

/** Интервал на опрос информации по лифтам */
const { tick } = interval({
  timeout: TICK_INTERVAL,
  start: pageMounted,
  stop: pageUnmounted,
});

/** При тике, запускаю запрос данных */
sample({
  clock: tick,
  target: fxGetTickLiftlist,
});

/** Делаю запрос лифтов при загрузке страницы */
sample({
  clock: pageMounted,
  target: fxGetLiftlist,
});

/** Проверка наличия запроса, запись в $isLoading */
const isRequest = merge([fxGetLiftlist.pending, fxGetFromStorage.pending]);
$isLoading.on(isRequest, (_state, isLoading) => isLoading).reset(pageUnmounted, signout);

/** Заполняю $liftsOrder на основе $liftData и $liftsOrderFromStore по окончании всех загрузок из Storage */
sample({
  clock: isRequest,
  source: {
    liftData: $liftData,
    liftsOrderFromStore: $liftsOrderFromStore,
  },
  filter: (_, isRequest) => !isRequest,
  fn: ({ liftData, liftsOrderFromStore }) => {
    const idFromLiftData = liftData.map((lift) => lift.id);

    if (!Array.isArray(liftsOrderFromStore)) return idFromLiftData;

    const result = [] as string[];

    liftsOrderFromStore.forEach((id) => {
      if (idFromLiftData.includes(id)) {
        result.push(id);

        const findIndex = idFromLiftData.indexOf(id);
        idFromLiftData.splice(findIndex, 1);
      }
    });

    return [...result, ...idFromLiftData];
  },
  target: setLiftOrder,
});

/** Проверка наличия запроса выова лифта, запись в $isLoading */
const isCallElevatorRequest = merge([fxCallElevator.pending]);
$isCallElevatorLoading
  .on(isCallElevatorRequest, (_state, isLoading) => isLoading)
  .reset(pageUnmounted, signout);

/** Отслеживание ошибки запросов, запись в $error */
const isError = merge<any>([fxGetLiftlist.failData, fxCallElevator.failData]);
$error
  .on(isError, (_, error) => error.message ?? 'Request error')
  .on(isRequest, (_, status) => {
    if (status) return null;
  })
  .reset(pageUnmounted, signout);

/** Сохраняю в Storage параметры liftsOrder */
sample({
  clock: changeLiftOrder,
  source: $liftsOrder,
  filter: (liftsOrder) => $isAuthenticated && liftsOrder.length > 0,
  target: attach({
    effect: fxSaveInStorage,
    mapParams: (liftsOrder) => ({
      data: { liftsOrder },
      storage_key: LOCAL_STORAGE_KEY,
    }),
  }),
});

/** Получаю параметры liftsOrder из Storage */
forward({
  from: pageMounted,
  to: fxGetFromStorage.prepend(() => ({ storage_key: LOCAL_STORAGE_KEY })),
});
