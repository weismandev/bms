import { createEffect, createEvent, createStore } from 'effector';
import { createGate } from 'effector-react';
import { CallElevator } from '@features/concierge/interfaces/callElevator';
import { Lift, LiftOrderDestination } from '@features/concierge/interfaces/lift';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '../../../common';

const TICK_INTERVAL = 5000;
const LOCAL_STORAGE_KEY = 'concierge';

const PageGate = createGate();
const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const clearError = createEvent();
const callElevator = createEvent<CallElevator>();
const changeLiftOrder = createEvent<LiftOrderDestination>();
const setLiftOrder = createEvent<string[]>();

const $liftData = createStore<Lift[]>([]);
const $liftsOrder = createStore<string[]>([]);
const $liftsOrderFromStore = createStore<string[]>([]);
const $isLoading = createStore(false);
const $isCallElevatorLoading = createStore(false);
const $error = createStore(null).on(clearError, () => null);
const $successCall = createStore(0);

const fxGetLiftlist = createEffect<void, Lift[], Error>();
const fxGetTickLiftlist = createEffect<void, Lift[], Error>();
const fxCallElevator = createEffect<CallElevator, void, Error>();

const $successPopup = createStore(false);
const showSuccessPopup = createEvent();
const hideSuccessPopup = createEvent();

export {
  PageGate,
  pageUnmounted,
  pageMounted,
  $liftData,
  $isLoading,
  $error,
  fxGetLiftlist,
  fxGetTickLiftlist,
  clearError,
  TICK_INTERVAL,
  callElevator,
  fxCallElevator,
  $isCallElevatorLoading,
  $successCall,
  $liftsOrder,
  changeLiftOrder,
  setLiftOrder,
  fxSaveInStorageBase,
  fxGetFromStorageBase,
  LOCAL_STORAGE_KEY,
  $liftsOrderFromStore,
  $successPopup,
  showSuccessPopup,
  hideSuccessPopup,
};
