import { useUnit } from 'effector-react';
import { Loader } from '@ui/atoms';
import { ErrorMessage } from '@ui/index';
import { FilterMainDetailLayout } from '@ui/templates';
import { $error, $isLoading, $successPopup, PageGate, clearError } from '../models';
import { Concierge } from '../organisms/concierge';
import { SuccessPopup } from '../organisms/successPopup';

const ConciergePage = () => {
  const [isLoading, error, successPopup] = useUnit([$isLoading, $error, $successPopup]);

  return (
    <div>
      <PageGate />
      <Loader isLoading={isLoading} />
      <ErrorMessage isOpen={error} onClose={clearError} error={error} />
      {successPopup && <SuccessPopup />}
      <FilterMainDetailLayout main={<Concierge />} />;
    </div>
  );
};

const RestrictedConciergePage = (props: Record<string, any>) => {
  return (
    // TODO добавить Access
    // <HaveSectionAccess>
    <ConciergePage {...props} />
    // </HaveSectionAccess>
  );
};

export { RestrictedConciergePage as ConciergePage };
