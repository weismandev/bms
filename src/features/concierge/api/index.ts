import { api } from '../../../api/api2';
import { CallElevator } from '../interfaces/callElevator';
import { DirectionNames, StateNames } from '../interfaces/lift';

const getConciergeElevators = () =>
  api.no_vers('get', 'elevator/get_concierge_elevators');

// const getConciergeElevators = async () => {
//   await api.no_vers('get', 'elevator/get_concierge_elevators');

//   return [
//     {
//       id: '11111',
//       title: '1111111111 1111111111 1111 1111111111 1111111111',
//       description: '',
//       state: { name: StateNames.norm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '22222',
//       title: '2',
//       description: '',
//       state: { name: StateNames.warn, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '3',
//       title: '3',
//       description: '',
//       state: { name: StateNames.norm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '4',
//       title: '444444444444444444444 4444444444 444444444444444 4444 444444444',
//       description: '',
//       state: { name: StateNames.alarm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '5',
//       title: '5',
//       description: '',
//       state: { name: StateNames.norm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '6',
//       title: '6',
//       description: '',
//       state: { name: StateNames.warn, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '7',
//       title: '7777777777777 777777777 777777777777777777777 77777777777777777 7777777777',
//       description: '',
//       state: { name: StateNames.alarm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '8',
//       title: '8',
//       description: '',
//       state: { name: StateNames.norm, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//     {
//       id: '9',
//       title: '9',
//       description: '',
//       state: { name: StateNames.warn, title: '' },
//       floors: { current: 1, destination: 1 },
//       direction: { name: DirectionNames.elevator_move_down, title: '' },
//     },
//   ];
// };

const callElevator = (params: CallElevator) =>
  api.no_vers('get', 'elevator/call', params);

export const conciergeApi = {
  getConciergeElevators,
  callElevator,
};
