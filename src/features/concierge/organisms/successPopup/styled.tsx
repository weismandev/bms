import styled from '@emotion/styled';

export const StyledPopupBody = styled.div`
  padding: 24px;
  width: 400px;
`;

export const StyledPopupButtons = styled.div`
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  gap: 16px;
`;
