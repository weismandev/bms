import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { Dialog } from '@mui/material';
import { Close } from '@mui/icons-material';
import { ActionButton } from '@ui/atoms';
import { $successPopup, hideSuccessPopup } from '../../models';
import { StyledPopupBody, StyledPopupButtons } from './styled';

export const SuccessPopup = () => {
  const { t } = useTranslation();
  const [successPopup] = useUnit([$successPopup]);

  return (
    <Dialog
      PaperProps={{
        style: { borderRadius: 16 },
      }}
      open={successPopup}
      onClose={() => hideSuccessPopup()}
    >
      <StyledPopupBody>
        <p>{t('concierge.ElevatorHasBeenSent')}</p>

        <StyledPopupButtons>
          <ActionButton kind="positive" onClick={hideSuccessPopup}>
            <Close />
            {t('close')}
          </ActionButton>
        </StyledPopupButtons>
      </StyledPopupBody>
    </Dialog>
  );
};
