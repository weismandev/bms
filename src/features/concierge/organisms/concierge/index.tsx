import i18n from '@shared/config/i18n';
import { Lifts } from './Lifts';
import { StyledConcierge, StyledConciergeToolbar, StyledCustomScrollbar } from './styled';
import { ActionButton } from '@ui/atoms';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

const { t } = i18n;

const ConciergeToolbar = () => {
  const history = useHistory();

  return (
    <StyledConciergeToolbar>
      <Link to="/passes" target="_blank">
        <ActionButton>{t('concierge.IssuingPass')}</ActionButton>
      </Link>

      <Link to="/tickets" target="_blank">
        <ActionButton>{t('concierge.ApplicationToDispatcher')}</ActionButton>
      </Link>
    </StyledConciergeToolbar>
  );
};

export const Concierge = () => (
  <StyledConcierge>
    <ConciergeToolbar />
    <StyledCustomScrollbar autoHide>
      <Lifts />
    </StyledCustomScrollbar>
  </StyledConcierge>
);
