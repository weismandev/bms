import { useEffect, useRef, useState } from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { useUnit } from 'effector-react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import EastIcon from '@mui/icons-material/East';
import { Direction, Floors, Lift } from '@features/concierge/interfaces/lift';
import {
  $isCallElevatorLoading,
  $liftData,
  $liftsOrder,
  $successCall,
  callElevator,
  changeLiftOrder,
} from '@features/concierge/models';
import { resolveNewLiftOrder } from '@features/concierge/models/concierge/init';
import i18n from '@shared/config/i18n';
import { InputField } from '@ui/molecules';
import {
  StyleToFloorButton,
  StyledFloor,
  StyledData,
  StyledFloorHeader,
  StyledFloors,
  StyledLift,
  StyledLiftData,
  StyledDirectionArrow,
  StyledLiftEntrance,
  StyledLiftHeader,
  StyledLiftStatus,
  StyledLifts,
  StyledDirectionHeader,
  StyleToFloorFormWrap,
  StyledFloorButton,
} from './styled';

interface SendFloor {
  floor: string;
}

const { t } = i18n;
const LIFT_ITEMS_IN_ROW = 4;

const ToNumberFloorButton = ({ serialnumber }: { serialnumber: string }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isCallElevatorLoading, successCall] = useUnit([
    $isCallElevatorLoading,
    $successCall,
  ]);

  useEffect(() => setIsEdit(false), [successCall]);

  const initialValues: SendFloor = {
    floor: '',
  };

  const inputRef = useRef<HTMLInputElement>();

  const onSubmit = ({ floor }: SendFloor) => {
    callElevator({ floor, serialnumber });
  };

  const openEnterFloor = () => {
    setIsEdit(true);

    setTimeout(() => {
      if (!inputRef.current) return;

      inputRef.current.focus();
    }, 0);
  };

  const validationSchema = Yup.object().shape({
    floor: Yup.string().required(t('thisIsRequiredField') as string),
  });

  return (
    <>
      {!isEdit && (
        <StyleToFloorButton onClick={openEnterFloor} disabled={isCallElevatorLoading}>
          {t('concierge.SendingToTheFloor')}
        </StyleToFloorButton>
      )}

      {isEdit && (
        <StyleToFloorFormWrap>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
          >
            <Form className="floorForm">
              <Field
                innerRef={inputRef}
                name="floor"
                component={InputField}
                label={false}
                placeholder={t('concierge.EnterFloor')}
                required
                disabled={isCallElevatorLoading}
                divider={false}
              />

              <StyledFloorButton type="submit">
                <EastIcon sx={{ color: '#12A25E' }} />
              </StyledFloorButton>
            </Form>
          </Formik>
        </StyleToFloorFormWrap>
      )}
    </>
  );
};

const LiftData = ({ floors, direction }: { floors: Floors; direction: Direction }) => {
  const { current, destination } = floors;
  const { name, title } = direction;

  return (
    <StyledLiftData>
      <StyledFloors>
        <StyledFloor>
          <StyledFloorHeader>{t('concierge.FloorInSteadyState')}</StyledFloorHeader>
          <StyledData>{current}</StyledData>
        </StyledFloor>
        <StyledFloor>
          <StyledFloorHeader>{t('concierge.FloorInMoveState')}</StyledFloorHeader>
          <StyledData>{destination}</StyledData>
        </StyledFloor>
      </StyledFloors>
      <StyledDirectionHeader>{t('concierge.DirectionOfTravel')}</StyledDirectionHeader>
      <StyledData>
        <StyledDirectionArrow direction={name} />
        {title}
      </StyledData>
    </StyledLiftData>
  );
};

export const Lifts = () => {
  const [liftData, liftsOrder, isCallElevatorLoading] = useUnit([
    $liftData,
    $liftsOrder,
    $isCallElevatorLoading,
  ]);

  const [liftDataOrdered, setLifttDataOrdered] = useState<Lift[]>([]);

  const createOrderedLiftsBy = (data: string[]) => {
    const orderedLifts: Lift[] = [];
    let liftDataSource = [...liftData];

    data.forEach((id) => {
      const lift = liftDataSource.find((lift) => lift.id === id);

      if (!lift) return;

      const index = liftDataSource.indexOf(lift);

      if (index >= 0) {
        orderedLifts.push(lift);
        liftDataSource.splice(index, 1);
      }
    });
    setLifttDataOrdered([...orderedLifts, ...liftDataSource]);
  };

  useEffect(() => createOrderedLiftsBy(liftsOrder), [liftsOrder, liftData]);

  const onDragEnd = (result: DropResult) => {
    const { destination, source } = result;

    if (destination?.index === undefined) return;

    const sourceIndex = source.index + Number(source.droppableId) * LIFT_ITEMS_IN_ROW;
    const destinationIndex =
      destination.index + Number(destination.droppableId) * LIFT_ITEMS_IN_ROW;

    const temporaryNewLiftOrder = resolveNewLiftOrder({
      state: liftsOrder,
      sourceIndex,
      destinationIndex,
    });

    createOrderedLiftsBy(temporaryNewLiftOrder);

    changeLiftOrder({
      sourceIndex,
      destinationIndex,
    });
  };

  const renderedLifts = (row: number) =>
    liftDataOrdered
      .slice(row * LIFT_ITEMS_IN_ROW, row * LIFT_ITEMS_IN_ROW + LIFT_ITEMS_IN_ROW)
      .map((liftData: Lift, index: number) => (
        <Draggable key={liftData.id} draggableId={liftData.id} index={index}>
          {(provided) => (
            <StyledLift
              state={liftData.state.name}
              key={liftData.id}
              ref={provided.innerRef}
              {...provided.dragHandleProps}
              {...provided.draggableProps}
            >
              <div>
                <StyledLiftHeader>{liftData.title}</StyledLiftHeader>
                <StyledLiftEntrance>{liftData.description}</StyledLiftEntrance>
                <StyledLiftStatus>{liftData.state.title}</StyledLiftStatus>
              </div>
              <div>
                <StyleToFloorButton
                  kind="positive"
                  disabled={isCallElevatorLoading}
                  onClick={() => callElevator({ floor: 1, serialnumber: liftData.id })}
                >
                  {t('concierge.SendingToTheFirstFloor')}
                </StyleToFloorButton>
                <ToNumberFloorButton serialnumber={liftData.id} />
                <LiftData floors={liftData.floors} direction={liftData.direction} />
              </div>
            </StyledLift>
          )}
        </Draggable>
      ));

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      {Array.from(
        Array(Math.ceil(liftDataOrdered.length / LIFT_ITEMS_IN_ROW)).keys()
      ).map((_, index) => (
        <Droppable
          droppableId={String(index)}
          direction="horizontal"
          type="column"
          key={index}
        >
          {(provided) => (
            <StyledLifts {...provided.droppableProps} ref={provided.innerRef}>
              {renderedLifts(index)}
            </StyledLifts>
          )}
        </Droppable>
      ))}
    </DragDropContext>
  );
};
