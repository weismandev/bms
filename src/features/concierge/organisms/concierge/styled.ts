import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import styled from '@emotion/styled';
import { DirectionNames, StateNames } from '@features/concierge/interfaces/lift';
import { ActionButton } from '@ui/atoms';
import { CustomScrollbar } from '@ui/molecules';

export const StyledCustomScrollbar = styled(CustomScrollbar as any)`
  margin-top: 10px;
  max-height: calc(100% - 30px);
`;

export const StyledConcierge = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px #e7e7ec;
  border-radius: 16px;
  padding: 24px;
  height: 100%;
`;

export const StyledConciergeToolbar = styled.div`
  display: flex;
  justify-content: flex-end;
  gap: 8px;
`;

export const StyledLifts = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 24px;
  gap: 24px;
  width: 100%;
`;

export const StyledLift = styled.div<{
  state: StateNames;
}>`
  flex: 0 0 calc(25% - 24px);
  background: #edf6ff;
  border-top: 8px solid
    ${({ state }) => {
      if (state === StateNames.alarm) return '#e13f3d';

      if (state === StateNames.warn) return '#FB8901';

      return '#00A947';
    }};
  border-radius: 8px;
  padding: 24px;
  font-style: normal;
  font-weight: 500;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const StyledLiftHeader = styled.h3`
  font-size: 20px;
  line-height: 32px;
  color: rgba(25, 28, 41, 0.87);
`;

export const StyledLiftEntrance = styled.p`
  font-size: 14px;
  line-height: 20px;
  color: rgba(25, 28, 41, 0.6);
`;

export const StyledLiftStatus = styled.p`
  font-size: 16px;
  line-height: 24px;
  color: rgba(25, 28, 41, 0.6);
  padding: 16px 0;
  border-bottom: 1px solid rgba(25, 28, 41, 0.23);
`;

export const StyleToFloorButton = styled(ActionButton as any)`
  margin: 6px 0;
  width: 100%;
`;

export const StyleToFloorFormWrap = styled.div`
  margin: 6px 0;
  padding-bottom: 8px;

  .floorForm {
    position: relative;
  }
`;

export const StyledLiftData = styled.div`
  margin-top: 38px;
`;

export const StyledFloors = styled.div`
  display: flex;
  justify-content: stretch;
  gap: 24px;
`;

export const StyledFloor = styled.div`
  flex: 0 1 50%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const StyledFloorHeader = styled.h4`
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  color: rgba(25, 28, 41, 0.6);
  margin-bottom: 8px;
`;

export const StyledData = styled.div`
  padding: 4px 12px;
  background: rgba(25, 28, 41, 0.06);
  border: 1px solid rgba(25, 28, 41, 0.23);
  border-radius: 16px;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: rgba(25, 28, 41, 0.6);
`;

export const StyledDirectionHeader = styled.h4`
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  color: rgba(25, 28, 41, 0.6);
  margin-bottom: 8px;
  margin-top: 24px;
`;

export const StyledFloorButton = styled.button`
  position: absolute;
  right: 0;
  top: 0;
  z-index: 10;
  height: 32px;
  border: 0;
  display: flex;
  align-items: center;
  box-shadow: 0;
  background: transparent;
  cursor: pointer;
`;

export const StyledDirectionArrow = styled(ArrowDownwardIcon as any)<{
  direction: DirectionNames;
}>`
  margin-right: 8px;
  transform: rotate(
    ${({ direction }) =>
      direction === DirectionNames.elevator_move_down ? '0deg' : '180deg'}
  );
  display: ${({ direction }) =>
    [DirectionNames.elevator_move_down, DirectionNames.elevator_move_up].includes(
      direction
    )
      ? 'inline-block'
      : 'none'};
  color: #12a25e;
`;
