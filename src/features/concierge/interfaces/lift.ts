export interface Lift {
  id: string;
  title: string;
  description: string;
  state: State;
  floors: Floors;
  direction: Direction;
}

export enum StateNames {
  norm = 'norm',
  warn = 'warn',
  alarm = 'alarm',
}

export enum DirectionNames {
  elevator_move_up = 'elevator_move_up',
  elevator_move_down = 'elevator_move_down',
  elevator_stay = 'elevator_stay',
  elevator_state_unknown = 'elevator_state_unknown',
  elevator_moving = 'elevator_moving',
}

export interface State {
  name: StateNames;
  title: string;
}

export interface Floors {
  current: number;
  destination: number;
}

export interface Direction {
  name: DirectionNames;
  title: string;
}

export interface LiftOrderDestination {
  destinationIndex: number;
  sourceIndex: number;
}
