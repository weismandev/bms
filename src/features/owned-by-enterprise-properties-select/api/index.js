import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'property/crm/resident/get-owned-by-enterprise', payload);

export const propertyApi = { getList };
