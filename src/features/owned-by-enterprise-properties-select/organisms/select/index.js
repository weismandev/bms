import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const OwnedByEnterprisePropertiesSelect = (props) => {
  const { enterprise_id } = props;

  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    if (enterprise_id) {
      props.onChange('');
      fxGetList({ enterprise_id });
    }
  }, [enterprise_id]);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const OwnedByEnterprisePropertiesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<OwnedByEnterprisePropertiesSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { OwnedByEnterprisePropertiesSelect, OwnedByEnterprisePropertiesSelectField };
