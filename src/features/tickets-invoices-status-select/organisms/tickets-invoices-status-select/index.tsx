import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';

import { SelectControl, SelectField } from '@ui/index';

import { $data, $isLoading, fxGetList } from '../../models';

interface SelectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

interface Props {
  disableFetchWithMount?: boolean;
}

export const InvoicesStatusTypeSelect: FC<Props> = ({
  disableFetchWithMount = false,
  ...props
}) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    if (!disableFetchWithMount) {
      fxGetList();
    }
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

export const InvoicesStatusTypeSelectField: FC<SelectFieldProps> = (props) => {
  const onChange = (value: object) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<InvoicesStatusTypeSelect />}
      onChange={onChange}
      {...props}
    />
  );
};
