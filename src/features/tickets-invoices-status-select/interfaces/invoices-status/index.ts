export interface GetInvoiceStatusResponse {
  invoice_statuses: InvoiceStatusItem[];
  prepayments: InvoiceStatusItem[];
  prices: InvoiceStatusItem[];
}

export interface InvoiceStatusItem {
  id: number | string;
  name: string;
  title: string;
}
