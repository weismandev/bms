import { api } from '@api/api2';

const getInvoicesStatus = () => api.v1('get', '/tck/payment-types/list');

export const ticketsInvoicesStatusApi = {
  getInvoicesStatus,
};
