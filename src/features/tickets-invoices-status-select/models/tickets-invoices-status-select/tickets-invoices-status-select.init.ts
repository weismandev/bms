import { signout } from '@features/common';

import { ticketsInvoicesStatusApi } from '../../api';
import { fxGetList, $data, $isLoading } from './tickets-invoices-status-select.model';

fxGetList.use(ticketsInvoicesStatusApi.getInvoicesStatus);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.invoice_statuses)) {
      return [];
    }

    return result.invoice_statuses.map((item) => ({
      id: item.name,
      name: item.name,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
