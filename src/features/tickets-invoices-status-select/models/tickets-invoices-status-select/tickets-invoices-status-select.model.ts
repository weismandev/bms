import { createEffect, createStore } from 'effector';

import { GetInvoiceStatusResponse, InvoiceStatusItem } from '../../interfaces';

export const fxGetList = createEffect<void, GetInvoiceStatusResponse, Error>();

export const $data = createStore<InvoiceStatusItem[]>([]);
export const $isLoading = createStore<boolean>(false);
