import { guard, merge, sample } from 'effector';
import format from 'date-fns/format';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { pending } from 'patronum/pending';

import { signout } from '@features/common';

import {
  getVehiclesList,
  saveVehiclesList,
  moveVehicleToArchive,
  getVehicleArchiveList,
} from '../api';

import {
  VehicleGate,
  detailSubmitted,
  fxGetVehiclesList,
  fxSaveVehiclesList,
  $mode,
  $vehicles,
  $isLoading,
  $error,
  loadSelectOptions,
  fxMoveVehicleToArchive,
  toArchiveClicked,
  fxGetVehiclesArchiveList,
  $vehiclesArchive,
} from './vehicles.model';

const isUserIdDefined = ({ userdata_id }) => Boolean(userdata_id) || userdata_id === 0;

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

fxGetVehiclesList.use(getVehiclesList);
fxSaveVehiclesList.use(saveVehiclesList);
fxMoveVehicleToArchive.use(moveVehicleToArchive);
fxGetVehiclesArchiveList.use(getVehicleArchiveList);

sample({
  clock: toArchiveClicked,
  fn: ({ userdata_id, vehicleId }) => ({
    vehicle: {
      id: vehicleId,
    },
    user: {
      id: userdata_id,
    },
  }),
  target: fxMoveVehicleToArchive,
});

$mode.on(fxSaveVehiclesList.done, () => 'view').reset([VehicleGate.close, signout]);

$vehicles
  .on([fxGetVehiclesList.doneData, fxSaveVehiclesList.doneData], (_, { vehicles }) =>
    Array.isArray(vehicles) ? vehicles : []
  )
  .on(fxMoveVehicleToArchive.done, (state, { result }) =>
    state.filter((item) => item.id !== result.vehicle.id)
  )
  .reset([VehicleGate.close, signout]);

$vehiclesArchive
  .on(fxGetVehiclesArchiveList.done, (_, { result }) => {
    if (!Array.isArray(result.vehicles)) {
      return [];
    }

    const { vehicles } = result;

    return vehicles.map((vehicle) => ({
      id: vehicle.id,
      brand: vehicle.brand,
      license_plate: vehicle.license_plate,
      created_at: format(new Date(vehicle.created_at), 'dd.MMMM.yyyy', {
        locale: dateFnsLocale,
      }),
      archived_at: format(new Date(vehicle.archived_at), 'dd.MMMM.yyyy', {
        locale: dateFnsLocale,
      }),
    }));
  })
  .on(fxMoveVehicleToArchive.done, (state, { result }) => {
    const { vehicle } = result;

    vehicle.created_at = format(new Date(vehicle.created_at), 'dd.MMMM.yyyy', {
      locale: dateFnsLocale,
    });

    vehicle.archived_at = format(new Date(vehicle.archived_at), 'dd.MMMM.yyyy', {
      locale: dateFnsLocale,
    });

    state.push(vehicle);

    return state;
  })
  .reset([VehicleGate.close, signout]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetVehiclesList,
        fxSaveVehiclesList,
        fxMoveVehicleToArchive,
        fxGetVehiclesArchiveList,
      ],
    }),
    (_, isLoading) => isLoading
  )
  .reset([VehicleGate.close, signout]);

$error
  .on(
    [
      fxGetVehiclesList.failData,
      fxSaveVehiclesList.failData,
      fxMoveVehicleToArchive.failData,
      fxGetVehiclesArchiveList.failData,
    ],
    (_, error) => error
  )
  .reset([newRequestStarted, VehicleGate.close, signout]);

guard({
  source: merge([VehicleGate.state, loadSelectOptions]),
  filter: isUserIdDefined,
  target: fxGetVehiclesList.prepend(({ userdata_id }) => userdata_id),
});

guard({
  source: VehicleGate.state,
  filter: ({ userdata_id, isUser }) => Boolean(userdata_id) && isUser,
  target: fxGetVehiclesArchiveList.prepend(({ userdata_id }) => userdata_id),
});

guard({
  source: sample(VehicleGate.state, detailSubmitted, formatVehiclesPayload),
  filter: isUserIdDefined,
  target: fxSaveVehiclesList,
});

function formatVehiclesPayload({ userdata_id }, payload) {
  return {
    userdata_id,
    vehicles: payload.vehicles.map((i) => {
      const { parking_slot, ...rest } = i;
      return { ...rest, parking_slot_id: parking_slot ? parking_slot.id : '' };
    }),
  };
}
