import { createEffect, createEvent, createStore, restore } from 'effector';
import { createGate } from 'effector-react';

export const newVehicle = {
  brand: '',
  license_plate: '',
  parking_slot: '',
};

const VehicleGate = createGate();

const changeMode = createEvent();
const detailSubmitted = createEvent();
const loadSelectOptions = createEvent();
const toArchiveClicked = createEvent();

const fxGetVehiclesList = createEffect();
const fxSaveVehiclesList = createEffect();
const fxMoveVehicleToArchive = createEffect();
const fxGetVehiclesArchiveList = createEffect();

const $mode = restore(changeMode, 'view');
const $vehicles = createStore([]);
const $vehiclesArchive = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);

export {
  VehicleGate,
  changeMode,
  detailSubmitted,
  fxGetVehiclesList,
  fxSaveVehiclesList,
  $mode,
  $vehicles,
  $error,
  $isLoading,
  loadSelectOptions,
  fxMoveVehicleToArchive,
  toArchiveClicked,
  fxGetVehiclesArchiveList,
  $vehiclesArchive,
};
