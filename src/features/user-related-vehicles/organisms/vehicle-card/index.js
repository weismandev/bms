import { useState } from 'react';
import cn from 'classnames';
import { Field } from 'formik';
import { Grid, IconButton, Collapse } from '@mui/material';
import { ArchiveOutlined, ExpandMore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { ParkingSlotsSelectField } from '@features/enterprises-employee-parking-select';
import { ParkingSlotForUserVehicleSelectField } from '@features/parking-slot-for-user-vehicle-select';
import i18n from '@shared/config/i18n';
import { InputField, FormSectionHeader, ActionButton } from '@ui/index';
import { toArchiveClicked } from '../../models/vehicles.model';
import { RemoveButtonOrStatusChip } from '../remove-button-status-chip';

const { t } = i18n;

const useStyles = makeStyles((theme) => ({
  cardContainer: {
    padding: '18px 24px 24px 24px',
    width: '100%',
    background: '#EDF6FF',
    borderRadius: 16,
    marginTop: 24,
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

function VehicleCard({
  remove,
  parentName,
  mode,
  label,
  isCompany,
  userdata_id,
  isUser,
  vehicleId,
}) {
  const classes = useStyles();

  const [expanded, setExpanded] = useState(true);
  const handleExpandClick = () => setExpanded(!expanded);

  const isEditMode = mode === 'edit';

  return (
    <div className={classes.cardContainer}>
      <div className={classes.cardHeader}>
        <FormSectionHeader header={t('vhcl')} />
        {!isUser ? (
          <RemoveButtonOrStatusChip
            remove={remove}
            isEditMode={isEditMode}
            label={label}
          />
        ) : (
          <IconButton
            className={cn(classes.expand, {
              [classes.expandOpen]: mode === 'edit' ? true : expanded,
            })}
            style={{ height: 35, width: 35 }}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
            disabled={mode === 'edit'}
            size="large"
          >
            <ExpandMore />
          </IconButton>
        )}
      </div>

      <Collapse in={mode === 'edit' ? true : expanded} timeout="auto" unmountOnExit>
        <Grid container spacing={2}>
          <Grid item sm={12} md={6}>
            <Field
              label={t('BrandModel')}
              placeholder={t('EnterTransportBrand')}
              divider={false}
              component={InputField}
              name={`${parentName}.brand`}
              mode={mode}
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <Field
              name={`${parentName}.license_plate`}
              label={t('GovNumber')}
              placeholder="A000AA000"
              helpText={t('FormatGovNumber')}
              mode={mode}
              component={InputField}
              divider={false}
              required
            />
          </Grid>
          {isEditMode ? (
            <>
              {isCompany ? (
                <Grid item xs={12}>
                  <Field
                    label={t('ParkingSpaceNumber')}
                    placeholder={t('ChooseParkingSpace')}
                    component={ParkingSlotsSelectField}
                    userdata_id={userdata_id}
                    divider={false}
                    name={`${parentName}.parking_slot`}
                    mode={mode}
                  />
                </Grid>
              ) : (
                <>
                  {!isUser && (
                    <Grid item xs={12}>
                      <Field
                        label={t('ParkingSpaceNumber')}
                        placeholder={t('ChooseParkingSpace')}
                        component={ParkingSlotForUserVehicleSelectField}
                        userdata_id={userdata_id}
                        divider={false}
                        name={`${parentName}.parking_slot`}
                        mode={mode}
                      />
                    </Grid>
                  )}
                </>
              )}
            </>
          ) : (
            isUser &&
            vehicleId && (
              <ActionButton
                onClick={() => toArchiveClicked({ userdata_id, vehicleId })}
                style={{
                  marginLeft: 10,
                  marginTop: 10,
                  fontSize: 14,
                  padding: '10px 15px',
                  backgroundColor: '#EC7F00',
                }}
              >
                <ArchiveOutlined style={{ marginRight: 7 }} />
                {t('ArchiveVehicle')}
              </ActionButton>
            )
          )}
        </Grid>
      </Collapse>
    </div>
  );
}

export { VehicleCard };
