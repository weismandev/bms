import { useState } from 'react';
import cn from 'classnames';
import { Grid, IconButton, Typography, Collapse } from '@mui/material';
import { ExpandMore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { FormSectionHeader } from '@ui/index';

const { t } = i18n;

const useStyles = makeStyles((theme) => ({
  cardContainer: {
    padding: '18px 24px 24px 24px',
    width: '100%',
    background: '#EDF6FF',
    borderRadius: 16,
    marginTop: 24,
  },
  fieldName: {
    fontSize: 14,
    color: '#65657B',
    opacity: 0.8,
  },
  fieldValue: {
    fontSize: 14,
    fontWeight: 500,
    color: '#65657B',
    paddingTop: 15,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const ArchiveList = ({ vehicle }) => {
  const classes = useStyles();

  const { brand, license_plate, created_at, archived_at } = vehicle;

  const [expanded, setExpanded] = useState(true);
  const handleExpandClick = () => setExpanded(!expanded);

  return (
    <div className={classes.cardContainer}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <FormSectionHeader header={t('vhcl')} />
        <IconButton
          className={cn(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          style={{ height: 35, width: 35 }}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
          size="large"
        >
          <ExpandMore />
        </IconButton>
      </div>

      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Grid container spacing={2}>
          <Grid item sm={12} md={6}>
            <Typography classes={{ root: classes.fieldName }}>
              {t('BrandModel')}
            </Typography>
            <Typography classes={{ root: classes.fieldValue }}>{brand}</Typography>
          </Grid>
          <Grid item sm={12} md={6}>
            <Typography classes={{ root: classes.fieldName }}>
              {t('GovNumber')}
            </Typography>
            <Typography classes={{ root: classes.fieldValue }}>
              {license_plate}
            </Typography>
          </Grid>

          <Grid item sm={12} md={6}>
            <Typography classes={{ root: classes.fieldName }}>
              {t('DateOfVehicleAddition')}
            </Typography>
            <Typography classes={{ root: classes.fieldValue }}>{created_at}</Typography>
          </Grid>
          <Grid item sm={12} md={6}>
            <Typography classes={{ root: classes.fieldName }}>
              {t('VehicleArchivingDate')}
            </Typography>
            <Typography classes={{ root: classes.fieldValue }}>{archived_at}</Typography>
          </Grid>
        </Grid>
      </Collapse>
    </div>
  );
};

export { ArchiveList };
