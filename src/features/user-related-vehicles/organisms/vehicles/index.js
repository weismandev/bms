import { useState } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import cn from 'classnames';
import { Formik, Form, FieldArray, Field } from 'formik';
import * as Yup from 'yup';
import { Button, IconButton, Typography, Collapse, Tooltip } from '@mui/material';
import { ExpandMore } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  Loader,
  ErrorMsg as ErrorMessage,
  CustomScrollbar,
  Divider,
  EditButton,
  SaveButton,
  CancelButton,
} from '@ui/index';
import {
  VehicleGate,
  changeMode,
  $mode,
  $vehicles,
  detailSubmitted,
  newVehicle,
  $isLoading,
  $error,
  $vehiclesArchive,
} from '../../models/vehicles.model';
import { ArchiveList } from '../archive-list';
import { VehicleCard } from '../vehicle-card';

const { t } = i18n;

const useStyles = makeStyles((theme) => ({
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  archiveTitle: {
    paddingTop: 3,
    fontWeight: 500,
    fontSize: 18,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  tooltip: {
    background: 'rgb(0, 0, 0, 0.8)',
    fontSize: 15,
    color: 'rgb(255, 255, 255)',
    fontWeight: 400,
  },
  margin: {
    margin: '0 5px',
  },
}));

const $stores = combine({
  mode: $mode,
  vehicles: $vehicles,
  isLoading: $isLoading,
  error: $error,
  vehiclesArchive: $vehiclesArchive,
});

const validationSchema = Yup.object().shape({
  vehicles: Yup.array().of(
    Yup.object().shape({
      license_plate: Yup.string().required(t('thisIsRequiredField')),
      brand: Yup.string().required(t('thisIsRequiredField')),
    })
  ),
});

function VehiclesForm({
  changeDetailVisibility,
  userdata_id,
  isCompany,
  isUser,
  isArchived,
  closeHandler,
  external,
}) {
  const { vehicles, mode, error, isLoading, vehiclesArchive } = useStore($stores);

  const classes = useStyles();

  const isEditMode = mode === 'edit';

  const [expanded, setExpanded] = useState(true);
  const handleExpandClick = () => setExpanded(!expanded);

  return (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} />
      <VehicleGate userdata_id={userdata_id} isUser={isUser} />
      <Formik
        initialValues={{ vehicles }}
        onSubmit={detailSubmitted}
        enableReinitialize
        validationSchema={validationSchema}
        render={({ resetForm, values }) => (
          <Form style={{ height: '100%', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              hidden={{ edit: true, delete: true, save: true, cancel: true }}
              onClose={external ? closeHandler : () => changeDetailVisibility(false)}
            >
              {!isArchived && !external && (
                <Tooltip title={t('edit')} classes={{ tooltip: classes.tooltip }}>
                  <div>
                    <EditButton
                      className={classes.margin}
                      onClick={() => changeMode('edit')}
                      disabled={mode === 'edit'}
                      style={{ order: 10 }}
                      isHideTitleAccess
                    />
                  </div>
                </Tooltip>
              )}
              {mode === 'edit' && (
                <>
                  <Tooltip title={t('Save')} classes={{ tooltip: classes.tooltip }}>
                    <div style={{ order: 60 }}>
                      <SaveButton
                        className={classes.margin}
                        type="submit"
                        isHideTitleAccess
                      />
                    </div>
                  </Tooltip>
                  <Tooltip title={t('cancel')} classes={{ tooltip: classes.tooltip }}>
                    <div style={{ order: 70 }}>
                      <CancelButton
                        className={classes.margin}
                        isHideTitleAccess
                        onClick={() => {
                          changeMode('view');
                          resetForm();
                        }}
                      />
                    </div>
                  </Tooltip>
                </>
              )}
            </DetailToolbar>
            <div
              style={{
                height: 'calc(100% - 84px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <FieldArray
                    name="vehicles"
                    render={(helpers) => {
                      const vehiclesCards = Array.isArray(values.vehicles)
                        ? values.vehicles.map((i, idx) => (
                            <Field
                              key={idx}
                              name={`vehicles[${idx}]`}
                              render={({ field }) => (
                                <VehicleCard
                                  isCompany={isCompany}
                                  userdata_id={userdata_id}
                                  remove={() => helpers.remove(idx)}
                                  label={
                                    field.value &&
                                    field.value.parking_slot &&
                                    `${t('p/m')} ${field.value.parking_slot.number}`
                                  }
                                  parentName={field.name}
                                  mode={mode}
                                  isUser={isUser}
                                  vehicleId={i.id}
                                />
                              )}
                            />
                          ))
                        : [];

                      const addButton = isEditMode ? (
                        <Button
                          style={{ marginTop: 16 }}
                          color="primary"
                          onClick={() => helpers.push(newVehicle)}
                        >
                          {`+ ${t('AddVehicle')}`}
                        </Button>
                      ) : null;

                      return (
                        <>
                          {vehiclesCards}
                          {addButton}
                        </>
                      );
                    }}
                  />
                  {isUser && (
                    <>
                      <Divider gap="30px 0px 15px" />
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}
                      >
                        <Typography classes={{ root: classes.archiveTitle }}>
                          {t('ArchiveVehicles')}
                        </Typography>
                        <IconButton
                          className={cn(classes.expand, {
                            [classes.expandOpen]: expanded,
                          })}
                          style={{ height: 35, width: 35 }}
                          onClick={handleExpandClick}
                          aria-expanded={expanded}
                          aria-label="show more"
                          size="large"
                        >
                          <ExpandMore />
                        </IconButton>
                      </div>
                      <Collapse in={expanded} timeout="auto">
                        {vehiclesArchive.map((vehicle) => (
                          <ArchiveList key={vehicle.id} vehicle={vehicle} />
                        ))}
                      </Collapse>
                    </>
                  )}
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </>
  );
}

export { VehiclesForm };
