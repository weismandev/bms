import { IconButton, Chip } from '@mui/material';
import { Close } from '@mui/icons-material';

function RemoveButtonOrStatusChip({ label, remove, isEditMode }) {
  return isEditMode ? (
    <IconButton size="small" onClick={remove}>
      <Close />
    </IconButton>
  ) : label ? (
    <Chip style={{ height: 24 }} color="primary" label={label} />
  ) : null;
}

export { RemoveButtonOrStatusChip };
