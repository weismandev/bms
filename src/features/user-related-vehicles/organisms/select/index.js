import { useEffect } from 'react';
import { useStore } from 'effector-react';
import i18n from '@shared/config/i18n';
import { SelectControl, SelectField } from '../../../../ui';
import {
  $vehicles,
  $isLoading,
  $error,
  loadSelectOptions,
} from '../../models/vehicles.model';

const { t } = i18n;

const RelatedVehicleSelect = ({ userdata_id, ...props }) => {
  const options = useStore($vehicles);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    loadSelectOptions({ userdata_id });
  }, [userdata_id]);

  return (
    <SelectControl
      options={options}
      isLoading={isLoading}
      getOptionLabel={(opt) =>
        opt ? `${opt.brand} ${opt.license_plate}` : t('UnknownVehicle')
      }
      error={error}
      {...props}
    />
  );
};

const RelatedVehicleSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<RelatedVehicleSelect />} onChange={onChange} {...props} />
  );
};

export { RelatedVehicleSelect, RelatedVehicleSelectField };
