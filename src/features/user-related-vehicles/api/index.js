import { api } from '../../../api/api2';

const getVehiclesList = (id) =>
  api.v1('get', 'users/related/vehicles/list', { userdata_id: id });

const saveVehiclesList = (payload) =>
  api.v1('post', 'users/related/vehicles/save', payload);

const getVehicleArchiveList = (id) =>
  api.v1('get', 'users/related/vehicles/list/archived', {
    user: { id },
  });

const moveVehicleToArchive = (payload) =>
  api.v1('post', 'users/related/vehicles/archive', payload);

export { getVehiclesList, saveVehiclesList, moveVehicleToArchive, getVehicleArchiveList };
