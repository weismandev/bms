import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'parking/lots/list', payload);
const create = (payload = {}) => api.v1('post', 'parking/lots/create', payload);
const update = (payload = {}) => api.v1('post', 'parking/lots/update', payload);
const deleteItem = (id) => api.v1('post', 'parking/lots/delete', { id });

export const parkingLotsApi = {
  getList,
  create,
  update,
  delete: deleteItem,
};
