import { useStore } from 'effector-react';
import { Toolbar, FoundInfo, FilterButton, Greedy, AddButton } from '@ui';
import i18n from '@shared/config/i18n';
import { featureFlags } from '@configs/feature-flags';
import { $mode, addClicked } from '../../models/detail.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import { $rowCount } from '../../models/page.model';

const isCRUDAllowed = featureFlags.oldParkingsCRUD;
const { t } = i18n;

const ListToolbar = () => {
  const count = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const mode = useStore($mode);

  const isEditModeWithCRUDAllowed = isCRUDAllowed ? mode === 'edit' : true;

  return (
    <Toolbar style={{ padding: 24, gap: 10 }}>
      <FilterButton
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <FoundInfo count={count} countTitle={`${t('TotalParks')}: `} />
      <Greedy />
      <AddButton onClick={addClicked} disabled={isEditModeWithCRUDAllowed} />
    </Toolbar>
  );
};

export { ListToolbar };
