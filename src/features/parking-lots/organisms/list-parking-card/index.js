import classNames from 'classnames';
import { Grid, ListItem, Typography, Chip } from '@mui/material';
import { DirectionsCarFilledOutlined } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { ActionButton, LabeledContent } from '../../../../ui';

const useStyles = makeStyles({
  itemRoot: {
    minWidth: 288,
    width: '25%',
  },
  itemCard: {
    height: '100%',
    width: '100%',
    borderRadius: 16,
    overflow: 'hidden',
  },
  itemPreview: {
    position: 'relative',
    width: '100%',
    height: 190,
    padding: 24,
  },
  itemHeader: {
    fontSize: 18,
    lineHeight: '21px',
    fontWeight: 900,
    color: '#fff',
  },
  itemCaption: {
    color: '#C0C0C0',
    fontSize: 14,
    lineHeight: '16px',
    fontWeight: 500,
    '& span': {
      fontWeight: 800,
    },
  },
  itemContent: {
    padding: 24,
    height: 'calc(100% - 190px)',
    background: '#F4F7FA',
  },
  itemContentSelected: {
    border: '2px solid #0394E3',
    borderTop: 'none',
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
    background: '#EDF6FF',
  },
  chipRoot: {
    padding: '24px 0',
    margin: '0 0px 5px 0',
  },
  chipLabelObject: {
    whiteSpace: 'break-spaces',
    color: '#65657B',
    fontWeight: 500,
    fontSize: 13,
    lineHeight: '15px',
    textAlign: 'left',
  },
  chipLabelZone: {
    whiteSpace: 'break-spaces',
    color: '#65657B',
    fontWeight: 500,
    fontSize: 13,
    lineHeight: '15px',
    textAlign: 'center',
  },
});

const { t } = i18n;

const ParkingZones = ({ zones, freeSlotsEnterpriseProperty }) => {
  const { chipRoot, chipLabelZone } = useStyles();

  return (
    <LabeledContent label={t('Zones')} style={{ marginTop: 20 }}>
      {zones.map(({ title, id, counts, slots_count, not_enterprises_slots_count }) => {
        const { owned_now, rented_now, total } = counts;

        const slotsEnterprise = not_enterprises_slots_count
          ? `${not_enterprises_slots_count}/${slots_count}`
          : '';

        const slotsResidents = `${total - (owned_now + rented_now)}/${total}`;

        const parkingSlots = freeSlotsEnterpriseProperty
          ? slotsEnterprise
          : slotsResidents;

        const label = `${title}\n${parkingSlots}`;

        return (
          <Chip
            classes={{ root: chipRoot, label: chipLabelZone }}
            style={{ marginRight: 5 }}
            key={id}
            label={label}
          />
        );
      })}
    </LabeledContent>
  );
};

const ParkingCard = (props) => {
  const { header, buildings, openCard, selected, slots_count, zones, image } = props;

  const {
    itemRoot,
    itemCard,
    itemPreview,
    itemHeader,
    itemCaption,
    itemContent,
    itemContentSelected,
    chipRoot,
    chipLabelObject,
  } = useStyles();

  const freeSlotsEnterpriseProperty = Array.isArray(zones)
    ? zones.reduce(
        (acc, { not_enterprises_slots_count: free_slots }) =>
          free_slots ? acc + Number(free_slots) : acc,
        0
      )
    : 0;

  const slotsResidentProperty = Array.isArray(zones)
    ? zones.reduce(
        (acc, { counts }) => {
          const { total, rented_now, owned_now } = counts;
          const free = total - (rented_now + owned_now);

          acc.free += free;
          acc.total += total;

          return acc;
        },
        { free: 0, total: 0 }
      )
    : { free: 0, total: 0 };

  const freeSlots = freeSlotsEnterpriseProperty || slotsResidentProperty.free;

  const totalSlots = freeSlotsEnterpriseProperty
    ? slots_count
    : slotsResidentProperty.total;

  const photoUrl = (image && image.preview) || '';

  return (
    <Grid item classes={{ root: itemRoot }} disableGutters component={ListItem}>
      <div className={itemCard}>
        <div
          className={itemPreview}
          style={{
            backgroundImage: `linear-gradient(to right, rgba(0,0,0,.5), rgba(0,0,0,.5)), url(${photoUrl})`,
            backgroundSize: 'cover',
          }}
        >
          <Typography classes={{ root: itemHeader }} variant="h3">
            {header}
          </Typography>
          <Typography classes={{ root: itemCaption }} variant="caption">
            {`${t('TotalParkingSpaces')}: `} <span>{totalSlots}</span>
          </Typography>
          <br />
          <Typography classes={{ root: itemCaption }} variant="caption">
            {`${t('AvailableParkingSpaces')}: `}
            <span>{freeSlots}</span>
          </Typography>
          <ActionButton
            kind="positive"
            style={{ position: 'absolute', bottom: 24, left: 24 }}
            onClick={openCard}
          >
            <DirectionsCarFilledOutlined style={{ margin: '0 5px' }} />
            {t('OpenCard')}
          </ActionButton>
        </div>
        <div
          className={classNames(itemContent, {
            [itemContentSelected]: selected,
          })}
        >
          <LabeledContent label={t('objects')}>
            {buildings.map(({ title, id }) => (
              <Chip
                classes={{ root: chipRoot, label: chipLabelObject }}
                key={id}
                label={title}
              />
            ))}
            <ParkingZones
              zones={zones}
              freeSlotsEnterpriseProperty={freeSlotsEnterpriseProperty}
            />
          </LabeledContent>
        </div>
      </div>
    </Grid>
  );
};

export { ParkingCard };
