import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import i18n from '@shared/config/i18n';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '../../../../ui';
import { ComplexSelectField } from '../../../complex-select';
import { HouseSelectField } from '../../../house-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';

const { t } = i18n;

const Filter = memo((props) => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', maxHeight: '89vh' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={(bag) => {
            return (
              <Form style={{ padding: 24, paddingTop: 0 }}>
                <Field
                  name="complex"
                  component={ComplexSelectField}
                  label={t('RC')}
                  placeholder={t('ChooseComplex')}
                />

                <Field
                  name="buildings"
                  component={HouseSelectField}
                  isMulti
                  label={t('objects')}
                  placeholder={t('selectObjects')}
                />

                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
