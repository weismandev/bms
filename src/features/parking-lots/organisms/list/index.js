import { useList, useStore } from 'effector-react';
import { Grid, List } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Wrapper, CustomScrollbar, Pagination } from '@ui/index';
import { open, $opened } from '../../models/detail.model';
import { $parkingLotsList } from '../../models/list.model';
import {
  $currentPage,
  changePage,
  $rowCount,
  $perPage,
  changePerPage,
} from '../../models/page.model';
import { ParkingCard } from '../list-parking-card';
import { ListToolbar } from '../list-toolbar';

const useStyles = makeStyles({
  list: {
    width: '100%',
    padding: '0 12px',
    height: 'calc(100% - 150px)',
    overflowY: 'auto',
  },
});

const ParkingCards = () => {
  const classes = useStyles();
  const opened = useStore($opened);

  const list = useList($parkingLotsList, {
    keys: [opened],
    fn: ({ title, buildings, id, slots_count, zones, image }) => (
      <ParkingCard
        image={image}
        slots_count={slots_count}
        selected={opened.id === id}
        header={title}
        buildings={buildings}
        openCard={() => open(id)}
        zones={zones}
      />
    ),
  });

  return (
    <div className={classes.list}>
      <CustomScrollbar
        renderTrackHorizontal={() => <div style={{ display: 'none' }} />}
        renderThumbHorizontal={() => <div style={{ display: 'none' }} />}
      >
        <Grid
          container
          spacing={3}
          style={{ width: 'calc(100% - 12px)' }}
          component={List}
        >
          {list}
        </Grid>
      </CustomScrollbar>
    </div>
  );
};

export const ParkingList = () => {
  const currentPage = useStore($currentPage);
  const rowCount = useStore($rowCount);
  const perPage = useStore($perPage);

  return (
    <Wrapper style={{ height: '100%', overflow: 'hidden' }}>
      <ListToolbar />
      <ParkingCards />
      <Pagination
        currentPage={currentPage}
        changePage={changePage}
        rowCount={rowCount}
        perPage={perPage}
        changePerPage={changePerPage}
      />
    </Wrapper>
  );
};
