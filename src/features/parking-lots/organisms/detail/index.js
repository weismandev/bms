import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Tab } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  DetailToolbar,
  CloseButton,
  FileControl,
  ActionButton,
  InputField,
  Tabs,
  CustomScrollbar,
} from '@ui';
import { featureFlags } from '@configs/feature-flags';
import { HouseSelectField } from '../../../house-select';
import { DetailTableForm } from '../../../parking-slots';
import {
  ZonesList,
  ZonesForm,
  changeZonesMode,
  $zonesMode,
  $isZonesDetailOpen,
} from '../../../parking-zones';
import {
  changedDetailVisibility,
  $mode,
  changeMode,
  detailSubmitted,
  $opened,
  $tabsList,
  changeTab,
  $currentTab,
} from '../../models/detail.model';
import { deleteDialogVisibilityChanged } from '../../models/page.model';

const isCRUDAllowed = featureFlags.oldParkingsCRUD;

const useStyles = makeStyles({
  photo: {
    height: 190,
    width: '100%',
    position: 'relative',
  },
  contentContainer: {
    height: 'calc(100% - 190px)',
    padding: '24px 12px 24px 24px',
  },
});

const { t } = i18n;

export const Detail = () => {
  const mode = useStore($mode);
  const tabsList = useStore($tabsList);
  const currentTab = useStore($currentTab);

  const tabs = (
    <Tabs
      options={tabsList}
      tabEl={<Tab style={{ minWidth: '33.3%' }} />}
      onChange={(e, tab) => changeTab(tab)}
      currentTab={currentTab}
    />
  );

  return (
    <Wrapper style={{ height: '100%' }}>
      {currentTab === 'info' && <InfoTab tabs={tabs} mode={mode} />}
      {currentTab === 'zones' && <ZonesTab tabs={tabs} />}
      {currentTab === 'slots' && <ParkingSlotsTab tabs={tabs} />}
    </Wrapper>
  );
};

function InfoTab(props) {
  const { mode, tabs } = props;
  const { photo, contentContainer } = useStyles();
  const opened = useStore($opened);

  const isViewMode = mode === 'view';

  return (
    <Formik
      initialValues={opened}
      enableReinitialize
      onSubmit={detailSubmitted}
      render={({ values, resetForm }) => {
        const isNew = !values.id;
        const photoUrl = (values.image && values.image.preview) || '';

        return (
          <Form style={{ height: 'inherit' }}>
            <div
              className={photo}
              style={{
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundImage: `linear-gradient(to right, rgba(0,0,0,.2), rgba(0,0,0,.2)), url(${photoUrl})`,
              }}
            >
              <DetailToolbar
                style={{ padding: 24 }}
                hidden={{ close: true, edit: !isCRUDAllowed, delete: !isCRUDAllowed }}
                mode={mode}
                onEdit={() => changeMode('edit')}
                onDelete={() => deleteDialogVisibilityChanged(true)}
                onCancel={() => {
                  if (isNew) {
                    changedDetailVisibility(false);
                  } else {
                    resetForm();
                    changeMode('view');
                  }
                }}
              >
                {isViewMode && (
                  <CloseButton
                    style={{ color: '#fff', margin: '0 5px', order: 100 }}
                    onClick={() => changedDetailVisibility(false)}
                  />
                )}
              </DetailToolbar>
              <Field
                name="image"
                render={({ field, form }) => (
                  <FileControl
                    inputProps={{ accept: 'image/*' }}
                    loadable
                    mode={mode}
                    value={field.value}
                    renderPreview={() => null}
                    renderButton={() => (
                      <ActionButton
                        component="span"
                        style={{
                          width: '75%',
                          position: 'absolute',
                          bottom: 24,
                          left: 0,
                          right: 0,
                          marginLeft: 'auto',
                          marginRight: 'auto',
                        }}
                      >
                        {values.image ? t('ProfilePage.ReplacePhoto') : t('UploadPhoto')}
                      </ActionButton>
                    )}
                    onChange={(value) => form.setFieldValue(field.name, value)}
                  />
                )}
              />
            </div>
            <div className={contentContainer}>
              <CustomScrollbar>
                <div style={{ paddingRight: 12 }}>
                  <div style={{ marginBottom: 24 }}>{tabs}</div>
                  <Field
                    name="title"
                    component={InputField}
                    label={t('Name')}
                    placeholder={t('EnterTheTitle')}
                    mode={mode}
                  />
                  <Field
                    name="buildings"
                    component={HouseSelectField}
                    label={t('objects')}
                    placeholder={t('selectObjects')}
                    isMulti
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
}

const ZonesToolbar = (props) => {
  const { onCancel, tabs } = props;
  const mode = useStore($zonesMode);

  return (
    <>
      <DetailToolbar
        hidden={{ delete: true, save: !isCRUDAllowed, edit: !isCRUDAllowed }}
        mode={mode}
        onClose={() => changedDetailVisibility(false)}
        onCancel={() =>
          typeof onCancel === 'function' ? onCancel() : changeZonesMode('view')
        }
        onEdit={() => changeZonesMode('edit')}
        style={{ height: 58, padding: '24px 24px 0 24px' }}
      />
      <div style={{ padding: '0 24px' }}>{tabs}</div>
    </>
  );
};

function ZonesTab(props) {
  const { tabs } = props;
  const isZonesDetailOpen = useStore($isZonesDetailOpen);
  const opened = useStore($opened);

  return isZonesDetailOpen ? (
    <ZonesForm
      lot_id={opened.id || ''}
      renderToolbar={({ onCancel }) => (
        <ZonesToolbar tabs={tabs} onCancel={onCancel} />
      )}
    />
  ) : (
    <>
      <ZonesToolbar tabs={tabs} />
      <ZonesList />
    </>
  );
}

function ParkingSlotsTab(props) {
  const { tabs } = props;
  const opened = useStore($opened);

  return (
    <DetailTableForm
      lot_id={opened.id}
      tabs={tabs}
      closeCard={() => changedDetailVisibility(false)}
    />
  );
}
