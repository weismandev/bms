import { useStore } from 'effector-react';
import {
  FilterMainDetailLayout,
  Loader,
  ErrorMessage,
  DeleteConfirmDialog,
} from '../../../ui';
import { HaveSectionAccess } from '../../common';
import '../models';
import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';
import {
  PageGate,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  deleteConfirmed,
} from '../models/page.model';
import { ParkingList, Filter, Detail } from '../organisms';

const ParkingLotsPage = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);

  return (
    <>
      <PageGate />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => errorDialogVisibilityChanged(false)}
        error={error}
      />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />

      <Loader isLoading={isLoading} />

      <FilterMainDetailLayout
        params={{ detailWidth: '850px' }}
        main={<ParkingList />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
      />
    </>
  );
};

const RestrictedParkingLotsPage = (props) => (
  <HaveSectionAccess>
    <ParkingLotsPage {...props} />
  </HaveSectionAccess>
);

export { RestrictedParkingLotsPage as ParkingLotsPage };
