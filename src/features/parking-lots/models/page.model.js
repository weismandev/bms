import { createStore, createEvent, restore } from 'effector';
import { createPageBag } from '@tools/factories';
import { parkingLotsApi } from '../api';

export const changePage = createEvent();
export const changePerPage = createEvent();

export const $currentPage = restore(changePage, 1);
export const $perPage = restore(changePerPage, 10);

export const {
  PageGate,
  pageUnmounted,
  pageMounted,
  deleteConfirmed,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $raw,
  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  $rowCount,
  deleteClicked,
} = createPageBag(
  parkingLotsApi,
  {},
  {
    idAttribute: 'id',
    itemsAttribute: 'lots',
    createdPath: 'lots',
    updatedPath: 'lots',
  }
);
