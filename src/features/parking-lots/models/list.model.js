import { createEvent, createStore } from 'effector';
import { formatFile } from '../../../tools/formatFile';
import { $raw } from './page.model';

export const searchChanged = createEvent();

export const $search = createStore('');
export const $parkingLotsList = $raw.map(({ data }) =>
  Array.isArray(data)
    ? data.map((i) => ({ ...i, image: i.image ? formatFile(i.image) : '' }))
    : []
);
