import { createStore, createEvent, restore } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

export const changeTab = createEvent();

export const newEntity = {
  image: '',
  title: '',
  buildings: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const $currentTab = restore(changeTab, 'info');

export const $tabsList = createStore([
  {
    value: 'info',
    label: t('information'),
  },
  {
    value: 'zones',
    label: t('Zones'),
  },
  {
    value: 'slots',
    label: t('navMenu.enterprises-parking'),
  },
]);
