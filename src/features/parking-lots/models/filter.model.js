import { createFilterBag } from '../../../tools/factories';

const defaultFilters = {
  complex: '',
  buildings: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
