import { sample, forward, attach, merge } from 'effector';
import { formatFile } from '../../../tools/formatFile';
import { signout } from '../../common';
import { fxGetZonesList, $zonesMode, resetZonesDetailOpen } from '../../parking-zones';
import {
  open,
  $opened,
  entityApi,
  $mode,
  newEntity,
  $isDetailOpen,
  $currentTab,
  $tabsList,
} from './detail.model';
import { $normalized, fxCreate, fxUpdate, fxDelete, deleteConfirmed } from './page.model';

$opened
  .on(
    sample($normalized, open, (data, id) => formatOpened(data[id])),
    (state, opened) => opened
  )
  .on([fxCreate.doneData, fxUpdate.doneData], (state, { lots }) => formatOpened(lots))
  .on(fxDelete.done, () => newEntity);

$mode.on([fxCreate.done, fxUpdate.done, fxDelete.done], () => 'view');

$isDetailOpen.on(fxDelete.done, () => false);

$currentTab.on($opened.updates, () => 'info').reset(signout);

const tabModeChanged = sample({
  source: $currentTab,
  clock: merge([$mode.updates, $zonesMode.updates]),
  fn: (currentTab, mode) => ({ currentTab, mode }),
});

$tabsList
  .on(tabModeChanged, (state, { mode, currentTab }) => {
    if (mode === 'edit') {
      return state.map((tab) =>
        tab.value === currentTab ? tab : { ...tab, disabled: true }
      );
    }
    if (mode === 'view') {
      return state.map(({ disabled, ...rest }) => ({ ...rest }));
    }

    return state;
  })
  .reset(signout);

forward({
  from: entityApi.create,
  to: attach({
    effect: fxCreate,
    mapParams: (params) => formatPayload(params),
  }),
});

forward({
  from: entityApi.update,
  to: attach({
    effect: fxUpdate,
    mapParams: (params) => formatPayload(params),
  }),
});

forward({
  from: sample($opened, deleteConfirmed, (opened) => opened.id),
  to: fxDelete,
});

forward({
  from: open,
  to: attach({ effect: fxGetZonesList, mapParams: (id) => ({ lot_id: id }) }),
});

forward({
  from: $opened,
  to: resetZonesDetailOpen,
});

function formatOpened(opened) {
  const { id, title, buildings, image } = opened;

  return {
    id,
    title,
    buildings: Array.isArray(buildings) ? buildings.map((item) => item.id) : [],
    image: image ? formatFile(image) : '',
  };
}

function formatPayload(payload) {
  const { buildings, image, ...rest } = payload;
  const data = { ...rest };

  if (Array.isArray(buildings)) {
    data.building_ids = buildings.map((i) => (typeof i === 'object' ? i.id : i));
  }

  if (image && image.id) {
    data.image_id = image.id;
  }

  return data;
}
