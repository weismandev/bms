import { forward, attach } from 'effector';
import { zonesChanged } from '../../parking-zones';
import { $filters } from './filter.model';
import { $search } from './list.model';
import {
  pageMounted,
  fxGetList,
  changePage,
  $currentPage,
  changePerPage,
  $perPage,
} from './page.model';

$currentPage.reset(changePerPage);

forward({
  from: [pageMounted, $filters, $search, zonesChanged, changePage, changePerPage],
  to: attach({
    effect: fxGetList,
    source: [$filters, $search, $currentPage, $perPage],
    mapParams: (_, [filters, search, currentPage, perPage]) => {
      const filter = {};
      const payload = {
        page: currentPage,
        per_page: perPage,
      };

      if (filters.complex) {
        filter.complex_id = filters.complex.id;
      }

      if (Array.isArray(filters.buildings)) {
        filter.building_ids = filters.buildings.map((i) => i.id);
      }

      payload.filter = filter;

      if (search) {
        payload.search = search;
      }

      return payload;
    },
  }),
});
