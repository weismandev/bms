import { signout } from '../../common';
import { searchChanged, $search } from './list.model';

$search.on(searchChanged, (state, value) => value).reset(signout);
