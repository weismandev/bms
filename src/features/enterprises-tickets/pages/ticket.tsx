import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { $pathname, HaveSectionAccess, history } from '@features/common';

import { ErrorMessage, FilterMainDetailLayout, Loader } from '@ui/index';
import i18n from '@shared/config/i18n';
import { currentPageChanged } from '@features/navbar/models';

import {
  $errorTicket,
  $isErrorDialogOpenTicket,
  $isLoadingTicket,
  $ticket,
  changedErrorDialogVisibilityTicket,
  path,
  TicketGate,
} from '../models';

import { Ticket } from '../organisms';
import { ChatPanel } from '@features/socket-chats-reworked/organisms/chat-panel';

const { t } = i18n;

const TicketPage: FC = () => {
  const pathname = useStore($pathname);
  const isErrorDialogOpen = useStore($isErrorDialogOpenTicket);
  const error = useStore($errorTicket);
  const isLoading = useStore($isLoadingTicket);
  const ticket = useStore($ticket);

  const enterpriseId = pathname.split('/')[2];
  const ticketId = pathname.split('/')[3];

  const socketsChat =
    ticket?.id && ticket?.channel && ticket?.related_userdata_token ? (
      <ChatPanel
        channelId={ticket?.channel}
        token={ticket.related_userdata_token}
        standalone
      />
    ) : null;

  useEffect(() => {
    (currentPageChanged as any)({
      title: ticket.number ? `${t('Request')} №${ticket.number}` : t('Request'),
      back: () => history.push(path),
    });
  }, [ticket]);

  return (
    <>
      <TicketGate enterpriseId={enterpriseId} ticketId={ticketId} />

      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibilityTicket(false)}
        error={error}
      />

      <FilterMainDetailLayout
        main={<Ticket />}
        detail={socketsChat}
        params={{
          mainWidth: 'auto',
          detailWidth: '0.6fr',
        }}
      />
    </>
  );
};

const RestrictedTicketPage: FC = () => (
  <HaveSectionAccess>
    <TicketPage />
  </HaveSectionAccess>
);

export default RestrictedTicketPage;
