import { FC } from 'react';
import { useStore } from 'effector-react';
import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  PageGate,
  changedErrorDialogVisibility,
  $isDetailOpen,
  $isFilterOpen,
} from '../models';
import { TicketsTable, Ticket, Filters } from '../organisms';

const TicketsPage: FC = () => {
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<TicketsTable />}
        detail={isDetailOpen && <Ticket />}
        params={{
          detailWidth: 'minmax(370px, 50%)',
        }}
      />
    </>
  );
};

const RestrictedTicketsPage: FC = () => (
  <HaveSectionAccess>
    <TicketsPage />
  </HaveSectionAccess>
);

export default RestrictedTicketsPage;
