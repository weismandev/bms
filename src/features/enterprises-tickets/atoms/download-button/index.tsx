import { FC } from 'react';
import { VerticalAlignBottom } from '@mui/icons-material';

import { ActionIconButton } from '@ui/index';

import { styles } from './styles';

interface Props {
  onClick: () => void;
}

export const DownloadButton: FC<Props> = ({ onClick }) => (
  <ActionIconButton style={styles.downloadButton} onClick={onClick}>
    <VerticalAlignBottom />
  </ActionIconButton>
);
