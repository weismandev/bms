import { format } from 'date-fns';

import { createTableBag } from '@tools/factories';
import { ClassesData } from '@features/tickets-class-select';
import { StatusData } from '@features/tickets-status-select';
import { InvoiceStatusItem } from '@features/tickets-invoices-status-select';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';
import i18n from '@shared/config/i18n';

import { $rawData, $tickets } from '../page/page.model';
import { Value, TicketsListResponseItem, InvoiceResponse, TypesData } from '../../interfaces';

const { t } = i18n;

interface Data {
  tickets: TicketsListResponseItem[];
  classData: ClassesData[];
  statusData: StatusData[];
  property: Value[];
  type: any;
  invoices: { [key: number]: InvoiceResponse[] };
  invoiceStatuses: InvoiceStatusItem[];
}

export const $rowCount = $rawData.map(({ total }) => total || 0);

export const columns = [
  { title: '№', name: 'number' },
  { title: t('RequestType'), name: 'type' },
  { title: t('Payment'), name: 'payment' },
  { title: t('Label.status'), name: 'status' },
  { title: t('DateOfCreation'), name: 'created_at' },
  { title: t('RequestClass'), name: 'class' },
  { title: t('room'), name: 'property' },
];

const columnsWidth = [
  { columnName: 'number', width: 120 },
  { columnName: 'type', width: 445 },
  { columnName: 'status', width: 160 },
  { columnName: 'created_at', width: 200 },
  { columnName: 'class', width: 200 },
  { columnName: 'property', width: 200 },
];

const formatDate = (date: string | null) => {
  if (!date || date?.length === 0) {
    return '-';
  }
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(new Date(localDate), 'dd.MM.yyy в HH:mm');
};

const getTicketInfo = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  return array.find((item: { id: string }) => item.id === id)?.title || '-';
};

const getProperty = (id: number, array: Value[]) => {
  if (!id) {
    return '-';
  }

  return array.find((item: { id: number }) => item.id === id)?.title || '-';
};

const getTicketStatus = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  const findedValue = array.find((item: { id: string }) => item.id === id);

  return [findedValue?.id, findedValue?.title];
};

const getTypeInfo = (type: number, types: TypesData[]) =>
  types.find((item: Value) => item.id === type);

const getTypesList = (currentType: number, types: TypesData[]) => {
  const iter = (type: number, acc: TypesData[]): TypesData[] => {
    const findedType: any = getTypeInfo(type, types);

    if (!findedType?.parent_id) {
      return [findedType, ...acc];
    }

    return iter(findedType.parent_id, [findedType, ...acc]);
  };

  return iter(currentType, []);
};

const getTicketType = (id: string, array: { id: string; title: string }[]) => {
  if (id?.toString().length === 0) {
    return '-';
  }

  return array.find((item: { id: string }) => item.id === id) || '-';
};

const formatData = ({
  tickets,
  classData,
  statusData,
  property,
  type,
  invoices,
  invoiceStatuses,
}: Data) =>
  tickets.map((item: TicketsListResponseItem) => {
    const typesList = item.types[0] ? getTypesList(item.types[0].id, type) : [];

    const ticketType = getTicketType(item.types[0]?.id, type);

    const invoice = invoices[item.id];
    const isWaitingPayment =
      Array.isArray(invoice) && invoice.length > 0
        ? invoice.some((i) => i.status === 'waiting-for-payment')
        : false;
    const isPaid =
      Array.isArray(invoice) && invoice.length > 0
        ? invoice.every((i) => i.status === 'paid')
        : false;
    const paymentStatus =
      Array.isArray(invoice) && invoice.length > 0
        ? isPaid
          ? 'paid'
          : isWaitingPayment
          ? 'waiting-for-payment'
          : null
        : null;
    const paymentStatusObject = invoiceStatuses.find(
      (invoiceStatus) => invoiceStatus?.id === paymentStatus
    );

    const data = {
      id: item.id,
      number: item?.number || '-',
      status: getTicketStatus(item?.status, statusData),
      created_at: formatDate(item?.created_at),
      class: getTicketInfo(item?.class, classData),
      property: getProperty(item?.objects[0]?.id, property),
      type: ticketType?.title || '-',
      payment: paymentStatusObject,
      formattedTypes: '',
      isTicketPaid: Boolean(ticketType?.paid),
    };

    if (typesList[0]) {
      const typesCount = typesList.length - 1;

      const types = typesList.map((item: Value, index: number) =>
        index !== typesCount ? `${item.title} -> ` : item.title
      );

      const formattedTypes = types.join('');

      data.formattedTypes = formattedTypes;
    }

    return data;
  });

export const $tableData = $tickets.map((result) => {
  if (!Array.isArray(result.tickets)) return [];

  return formatData(result);
});

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  columnOrderChanged,

  $search,
  $columnOrder,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
} = createTableBag(columns, {
  widths: columnsWidth,
  order: [],
  pageSize: 25,
});
