import { sample } from 'effector';
import { fxSaveInStorage, fxGetFromStorage } from '@features/common';
import {
  $hiddenColumnNames,
  hiddenColumnChanged,
  $columnOrder,
  columnOrderChanged,
} from './table.model';

sample({
  clock: [hiddenColumnChanged, columnOrderChanged],
  source: [$hiddenColumnNames, $columnOrder],
  fn: ([hiddenColumnNames, columnOrder]) => ({
    data: { hiddenClmn: hiddenColumnNames, orderClmn: columnOrder },
    storage_key: 'enterprises-tickets-new',
  }),
  target: fxSaveInStorage,
});

$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.hiddenClmn) ? result.hiddenClmn : state
);

$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.orderClmn) ? result.orderClmn : state
);
