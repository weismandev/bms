import { createStore, createEffect, createEvent } from 'effector';

import { $pathname } from '@features/common';

import {
  GetInvoicesPayload,
  GetInvoicesResponse,
  Invoice,
  FileInvoice,
} from '../../interfaces';

export const $invoices = createStore<Invoice[]>([]);
export const $companyId = $pathname.map((pathname: string) => {
  const splittedPathname = pathname.split('/');

  return splittedPathname[2];
});

export const downloadFile = createEvent<FileInvoice>();

export const fxGetInvoices = createEffect<
  GetInvoicesPayload,
  GetInvoicesResponse,
  Error
>();
