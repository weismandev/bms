import { guard } from 'effector';
import FileSaver from 'file-saver';

import { signout } from '@features/common';

import {
  fxGetInvoices,
  $invoices,
  $companyId,
  downloadFile,
} from './ticket-invoices.model';
import { pageUnmounted, $ticketData } from '../ticket/ticket.model';
import { Ticket } from '../../interfaces';

guard({
  source: { ticketData: $ticketData, companyId: $companyId },
  filter: ({ ticketData, companyId }) => Boolean(ticketData?.id && companyId),
  target: fxGetInvoices.prepend(
    ({ ticketData, companyId }: { ticketData: Ticket; companyId: string }) => ({
      ticket: {
        id: ticketData.id,
      },
      enterprise: {
        id: Number.parseInt(companyId, 10),
      },
    })
  ),
});

$invoices
  .on(fxGetInvoices.done, (_, { result }) => {
    if (!Array.isArray(result?.invoices)) {
      return [];
    }

    return result.invoices.map((invoice) => ({
      id: invoice.id,
      file: {
        url: invoice.file,
        name: invoice.name,
      },
      status: invoice.status,
      amount: invoice.total,
    }));
  })
  .reset([pageUnmounted, signout]);

downloadFile.watch((file) => FileSaver.saveAs(file.url, file.name));
