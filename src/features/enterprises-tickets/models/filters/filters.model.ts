import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  payment: '',
  invoiceStatus: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
