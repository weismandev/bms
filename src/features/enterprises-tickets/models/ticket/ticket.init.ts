import { merge, sample, forward } from 'effector';
import { pending, delay } from 'patronum';
import { format } from 'date-fns';

import { signout, history } from '@features/common';
import { fxGetList as fxGetStatus } from '@features/tickets-status-select';
import { convertUTCDateToLocalDate } from '@tools/formatDateWithZone';

import { ticketApi } from '../../api';
import { $isDetailOpen, path, $currentCompany } from '../page/page.model';
import { Value, TicketResponse } from '../../interfaces';
import {
  $mode,
  changedMode,
  pageMounted,
  changedErrorDialogVisibility,
  $isLoading,
  $error,
  fxCreateTicket,
  createTicket,
  pageUnmounted,
  $isErrorDialogOpen,
  fxGetTicketById,
  $ticketData,
  $formattedTypes,
  $formattedPrepaymentTypes,
} from './ticket.model';
import { fxGetInvoices } from '../ticket-invoices';

fxCreateTicket.use(ticketApi.createTicket);
fxGetTicketById.use(ticketApi.getTicketById);
fxGetInvoices.use(ticketApi.getInvoices);

const errorOccured = merge([fxGetTicketById.fail, fxGetInvoices.fail]);

$isLoading
  .on(
    pending({
      effects: [fxGetTicketById, fxGetInvoices],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$mode
  .on(changedMode, (_, mode) => mode)
  .on(pageMounted, () => 'view')
  .on($isDetailOpen, (_, isDetailOpen) => (isDetailOpen ? 'edit' : 'view'))
  .reset(signout);

sample({
  clock: createTicket,
  source: $currentCompany,
  fn: (company, ticket) => {
    const findedType = [...ticket.types].reverse().find((item: Value) => item);

    return {
      enterprise: {
        id: (company as Value)?.id,
      },
      description: ticket.description,
      objects: [{ id: (ticket.property as Value).id, type: 'property' }],
      types: findedType?.id ? [findedType.id] : [],
    };
  },
  target: fxCreateTicket,
});

sample({
  clock: pageMounted,
  fn: ({ ticketId, enterpriseId }) => ({
    enterprise: { id: enterpriseId },
    ticket: { id: ticketId },
  }),
  target: fxGetTicketById,
});

const delayedRedirect = delay({ source: fxGetTicketById.fail, timeout: 3000 });

delayedRedirect.watch(() => {
  history.push(path);
});

const formatDate = (date: string) => {
  const localDate = new Date(convertUTCDateToLocalDate(new Date(date)));

  return format(new Date(localDate), "yyyy-MM-dd'T'HH:mm");
};

const formatTicket = (result: TicketResponse) => {
  const { ticket } = result;

  return {
    id: ticket.id,
    description: ticket?.description || '-',
    class: ticket?.class,
    status: ticket?.status,
    number: ticket?.number || '-',
    property: (ticket?.objects as Value[])[0]?.id,
    channel: ticket?.channel?.id,
    created_at: ticket?.created_at ? formatDate(ticket.created_at) : '',
    types: [ticket.types[0]?.id],
    formattedTypes: '-',
    related_userdata_token: ticket?.related_userdata_token,
  };
};

$ticketData
  .on(fxGetTicketById.done, (_, { result }) => formatTicket(result))
  .reset([signout, pageUnmounted]);

forward({
  from: pageMounted,
  to: fxGetStatus,
});

$formattedTypes.reset([pageUnmounted, signout]);
$formattedPrepaymentTypes.reset([pageUnmounted, signout]);
