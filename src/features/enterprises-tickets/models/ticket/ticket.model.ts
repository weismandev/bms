import { createStore, createEvent, createEffect, combine } from 'effector';
import { createGate } from 'effector-react';

import { $data as $prepaymentTypes } from '@features/tickets-prepayment-type-select';

import { $invoices } from '../ticket-invoices/ticket-invoices.model';
import { $types } from '../page/page.model';
import {
  Ticket,
  CreateTicketPayload,
  TicketPayload,
  TicketResponse,
  Value,
  TypesData,
} from '../../interfaces';

interface Gate {
  ticketId: number;
  enterpriseId: number;
}

const TicketGate = createGate<Gate>();

const pageMounted = TicketGate.open;
const pageUnmounted = TicketGate.close;

const entityTicket = {
  id: null,
  description: '',
  property: '',
  types: [],
  formattedTypes: '-',
};

const changedErrorDialogVisibility = createEvent<boolean>();
const changedMode = createEvent<string>();
const createTicket = createEvent<Ticket>();

const fxCreateTicket = createEffect<CreateTicketPayload, TicketResponse, Error>();
const fxGetTicketById = createEffect<TicketPayload, TicketResponse, Error>();

const $ticketData = createStore<Ticket>(entityTicket);
const $mode = createStore<string>('view');
const $isLoading = createStore<boolean>(false);
const $error = createStore<null | Error>(null);
const $isErrorDialogOpen = createStore<boolean>(false);
export const $formattedTypes = $types.map((types) => {
  if (!Array.isArray(types)) {
    return {};
  }

  return types.reduce(
    (acc: { [key: number]: TypesData }, type: TypesData) => ({
      ...acc,
      ...{ [type.id]: type },
    }),
    {}
  );
});
export const $formattedPrepaymentTypes = $prepaymentTypes.map((types) => {
  if (!Array.isArray(types)) {
    return {};
  }

  return types.reduce((acc, type) => ({ ...acc, ...{ [type.name]: type.title } }), {});
});

const getTypeInfo = (type: number, types: TypesData[]) =>
  types.find((item: Value) => item.id === type);

const getTypesList = (currentType: number, types: TypesData[]) => {
  const iter = (type: number, acc: TypesData[]): TypesData[] => {
    const findedType: any = getTypeInfo(type, types);

    if (!findedType?.parent_id) {
      return [findedType, ...acc];
    }

    return iter(findedType.parent_id, [findedType, ...acc]);
  };

  return iter(currentType, []);
};

const $ticket = combine($ticketData, $types, $invoices, (ticketData, types, invoices) => {
  const currentType: any = ticketData.types[0];

  if (invoices.length > 0) {
    ticketData.invoices = invoices;
  } else {
    ticketData.invoices = [];
  }

  const typesList = currentType ? getTypesList(currentType, types) : [];

  if (typesList[0]) {
    const typesCount = typesList.length - 1;

    const types = typesList.map((item: Value, index: number) => {
      return index !== typesCount ? `${item.title} -> ` : item.title;
    });

    const formattedTypes = types.join('');

    ticketData.types = typesList;
    ticketData.formattedTypes = formattedTypes;
  }

  return ticketData;
});

export {
  $ticketData,
  $ticket,
  $mode,
  changedMode,
  pageMounted,
  pageUnmounted,
  createTicket,
  fxCreateTicket,
  changedErrorDialogVisibility,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  TicketGate,
  fxGetTicketById,
};
