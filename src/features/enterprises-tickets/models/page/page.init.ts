import { merge, guard, sample, forward } from 'effector';
import { pending } from 'patronum/pending';

import { signout, history, fxGetFromStorage, fxSaveInStorage } from '@features/common';
import { fxGetList as fxGetStatus } from '@features/tickets-status-select';
import { fxGetList as fxGetClass } from '@features/tickets-class-select';
import { fxGetList as fxGetProperty } from '@features/enterprises-properties-select';
import { fxGetList as fxGetPrepaymentType } from '@features/tickets-prepayment-type-select';
import { fxGetList as fxGetInvoiceStatus } from '@features/tickets-invoices-status-select';

import { ticketApi } from '../../api';
import { $tableParams } from '../table/table.model';
import { Value, TicketsListPayload } from '../../interfaces';
import { fxCreateTicket } from '../ticket/ticket.model';
import { $filters } from '../filters/filters.model';
import {
  pageUnmounted,
  changedErrorDialogVisibility,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  fxGetTicketsList,
  $rawData,
  pageMounted,
  closeDetail,
  addClicked,
  $isDetailOpen,
  open,
  path,
  $currentCompany,
  getTicketInvoices,
  fxGetTicketInvoices,
  $invoices,
  fxGetTypes,
  $types,
} from './page.model';

fxGetTicketsList.use(ticketApi.getTicketList);
fxGetTicketInvoices.use(ticketApi.getInvoices);
fxGetTypes.use(ticketApi.getTypesList);

const errorOccured = merge([
  fxGetTicketsList.fail,
  fxCreateTicket.fail,
  fxGetFromStorage.fail,
  fxSaveInStorage.fail,
  fxGetTypes.fail,
  fxGetTicketInvoices.fail,
]);

$types.on(fxGetTypes.done, (_, { result }) => {
  if (!Array.isArray(result?.types)) {
    return [];
  }

  return result.types;
}).reset([pageUnmounted, signout]);

$isLoading
  .on(
    pending({
      effects: [fxGetTicketsList, fxCreateTicket, fxGetTypes],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

$rawData
  .on(fxGetTicketsList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

forward({
  from: pageMounted,
  to: [
    fxGetStatus,
    fxGetClass,
    fxGetTypes,
    fxGetFromStorage.prepend(() => ({
      storage_key: 'enterprises-tickets-new',
    })),
  ],
});

guard({
  clock: [pageMounted, $currentCompany, $tableParams, $filters],
  source: { company: $currentCompany, table: $tableParams, filters: $filters },
  filter: ({ company }) => Boolean((company as Value)?.id),
  target: fxGetTicketsList.prepend(
    ({ company, table: { page, per_page }, filters }: any) => {
      const payload = {
        enterprise: {
          id: (company as Value)?.id,
        },
        page,
        per_page,
      };

      if (filters.payment && typeof filters.payment === 'object') {
        (payload as TicketsListPayload).paid = Boolean(filters.payment.id);
      }

      if (filters.invoiceStatus && typeof filters.invoiceStatus === 'object') {
        (payload as TicketsListPayload).invoice_status = filters.invoiceStatus.id;
      }

      return payload;
    }
  ),
});

guard({
  clock: [pageMounted, $currentCompany],
  source: $currentCompany,
  filter: (company) => Boolean((company as Value)?.id),
  target: fxGetProperty.prepend((company) => ({
    enterprise_id: (company as Value)?.id,
  })),
});

$isDetailOpen.on(addClicked, () => true).reset([closeDetail, signout, pageUnmounted]);

sample({
  clock: open,
  source: $currentCompany,
  fn: (company, id) => {
    const enterpriseId = (company as Value).id;

    history.push(`${path}/${enterpriseId}/${id}`);
  },
});

sample({
  clock: fxCreateTicket.done,
  source: $currentCompany,
  fn: (company, { result }) => {
    const enterpriseId = (company as Value).id;
    const ticketId = result.ticket.id;

    history.push(`${path}/${enterpriseId}/${ticketId}`);
  },
});

forward({
  from: pageMounted,
  to: [fxGetPrepaymentType, fxGetInvoiceStatus],
});

sample({
  clock: fxGetTicketsList.done,
  source: { type: $types, invoices: $invoices },
  fn: ({ type, invoices }, { result }) => {
    result.tickets.map((item) => {
      if (!Array.isArray(invoices[item.id])) {
        const findedType = type.find(
          (element: { id: number }) => element?.id === item?.types[0]?.id
        );

        if (findedType?.paid) {
          getTicketInvoices(item.id);
        }
      }
    });
  },
});

sample({
  clock: getTicketInvoices,
  source: $currentCompany,
  filter: (currentCompany) => Boolean((currentCompany as Value)?.id),
  fn: (currentCompany, id) => ({
    ticket: { id },
    enterprise: {
      id: (currentCompany as Value).id,
    },
  }),
  target: fxGetTicketInvoices,
});

$invoices
  .on(fxGetTicketInvoices.done, (state, { params, result }) => {
    if (!Array.isArray(result?.invoices)) {
      return {};
    }

    if (!params.ticket.id) {
      return state;
    }

    const invoices = { [params.ticket.id]: result.invoices };

    return { ...state, ...invoices };
  })
  .reset([pageUnmounted, signout]);
