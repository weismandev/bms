import { createStore, createEvent, createEffect, combine, restore } from 'effector';
import { createGate } from 'effector-react';

import { $data as $classData } from '@features/tickets-class-select';
import { $data as $statusData } from '@features/tickets-status-select';
import { $data as $property } from '@features/enterprises-properties-select';
import { $data as $invoiceStatuses } from '@features/tickets-invoices-status-select';

import {
  TicketsListPayload,
  TicketsListResponse,
  Value,
  GetInvoicesPayload,
  GetInvoicesResponse,
  InvoiceResponse,
  TypesResponse,
  TypesData,
} from '../../interfaces';

const path = '/enterprises-tickets';

const statusColor = {
  new: {
    backgroundColor: 'rgb(27, 177, 105)',
    color: 'rgb(255, 255, 255)',
  },
  work: {
    backgroundColor: 'rgb(28, 144, 251)',
    color: 'rgb(255, 255, 255)',
  },
  done: {
    backgroundColor: 'rgb(160, 167, 189)',
    color: 'rgb(255, 255, 255)',
  },
  closed: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.87)',
  },
  assigned: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  canceled: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  paused: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  confirmation: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  returned: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  denied: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  incorrect: {
    backgroundColor: 'rgb(224, 224, 224)',
    color: 'rgba(0, 0, 0, 0.87)',
  },
};

const PageGate = createGate();

const pageUnmounted = PageGate.close;
const pageMounted = PageGate.open;

const changedErrorDialogVisibility = createEvent<boolean>();
const open = createEvent<number>();
const closeDetail = createEvent<void>();
const addClicked = createEvent<void>();
const changeCompany = createEvent<string | Value>();
export const getTicketInvoices = createEvent<number | null>();

const fxGetTicketsList = createEffect<TicketsListPayload, TicketsListResponse, Error>();
export const fxGetTicketInvoices = createEffect<
  GetInvoicesPayload,
  GetInvoicesResponse,
  Error
>();
export const fxGetTypes = createEffect<void, TypesResponse, Error>();

export const $types = createStore<TypesData[]>([]);
export const $invoices = createStore<{ [key: number]: InvoiceResponse[] }>({});
const $currentCompany = restore(changeCompany, '');
const $isDetailOpen = createStore<boolean>(false);
const $isLoading = createStore<boolean>(false);
const $error = createStore<null | Error>(null);
const $isErrorDialogOpen = createStore<boolean>(false);
const $rawData = createStore<TicketsListResponse>({
  total: 0,
  tickets: [],
});
const $tickets = combine(
  $rawData,
  $classData,
  $statusData,
  $property,
  $types,
  $invoices,
  $invoiceStatuses,
  (rawData, classData, statusData, property, type, invoices, invoiceStatuses) => ({
    tickets: rawData?.tickets || [],
    classData,
    statusData,
    property,
    type,
    invoices,
    invoiceStatuses,
  })
);

export {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  fxGetTicketsList,
  PageGate,
  $rawData,
  $tickets,
  statusColor,
  closeDetail,
  addClicked,
  $isDetailOpen,
  path,
  open,
  changeCompany,
  $currentCompany,
};
