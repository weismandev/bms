export interface GetInvoicesPayload {
  ticket: {
    id: number | null;
  };
  enterprise: {
    id: number;
  };
}

export interface InvoiceResponse {
  id: number;
  file: string;
  total: number;
  status: string;
  name: string;
}

export interface GetInvoicesResponse {
  invoices: InvoiceResponse[];
}

export interface Invoice {
  id: number;
  file: FileInvoice;
  status: string;
  amount: number;
}

export interface FileInvoice {
  url: string;
  name: string;
}

export interface InvoiceStatusItem {
  id: number | string;
  name: string;
  title: string;
}
