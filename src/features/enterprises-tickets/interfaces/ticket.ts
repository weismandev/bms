import { Value, TypesData } from './tickets';

export interface Ticket {
  id: null | number;
  description: string;
  class?: string;
  status?: string;
  number?: string;
  property: number | string | Value;
  channel?: number;
  created_at?: string;
  types: [] | TypesData[];
  formattedTypes: string;
  related_userdata_token?: string;
}

export interface CreateTicketPayload {
  enterprise: {
    id: number;
  };
  description: string;
  objects?: { id: number; type: string }[];
  types: number[];
}

export interface TicketPayload {
  enterprise: {
    id: number;
  };
  ticket: {
    id: number;
  };
}

export interface TicketResponse {
  ticket: {
    id: null | number;
    description: string;
    class: string;
    status: string;
    number: string;
    objects: { id: number; type: string }[];
    channel: { id: number };
    created_at: string;
    types: any;
    related_userdata_token: string;
  };
}
