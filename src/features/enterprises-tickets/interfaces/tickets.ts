export interface TicketsListPayload {
  enterprise: {
    id: number;
  };
  paid?: boolean;
  invoice_status?: string;
}

export interface TicketsListResponse {
  total?: number;
  tickets: TicketsListResponseItem[];
}

export interface TicketsListResponseItem {
  id: number;
  number: string;
  status: string;
  class: string;
  created_at: string;
  objects: { id: number; type: string }[];
  types: { id: string }[];
}

export interface Value {
  id: number;
  title?: string;
}

export interface TypesResponse {
  types: TypesData[];
}

export interface TypesData {
  id: number;
  title: string;
  parent_id: null | number;
  children: number[] | [];
  planned_duration_minutes: null | number;
  archived: boolean;
  paid: null | {
    prepayment_type: string;
    price_lower: number;
    price_type: string;
    price_upper: number;
  };
}
