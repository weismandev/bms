import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  field: {
    paddingBottom: 10,
    paddingRight: 15,
    width: '50%',
    '@media (max-width: 1024px)': {
      width: '100%',
    },
  },
  text: {
    color: '#9494A3',
    fontWeight: 400,
    fontSize: 14,
    marginTop: -5,
    marginBottom: 10,
  },
  twoFields: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
  paymentText: {
    fontWeight: 400,
    fontSize: 13,
    color: '#65657B',
    marginTop: 5,
  },
  type: {
    display: 'flex',
  },
  typeFieldName: {
    fontStyle: 'normal',
    fontWeight: 400,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  typeField: {
    margin: '10px 0px 5px 0px',
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
  },
  container: {
    background: '#EDF6FF',
    borderRadius: 16,
    padding: '18px 24px 10px',
    marginBottom: 15,
    marginRight: 10,
  },
}));

export { useStyles };
