import { FC } from 'react';
import { Formik, Form, Field, FieldArray } from 'formik';
import { useStore } from 'effector-react';
import * as Yup from 'yup';
import { Chip, Typography } from '@mui/material';

import { Wrapper, DetailToolbar, CustomScrollbar, InputField, Divider } from '@ui/index';
import i18n from '@shared/config/i18n';
import { EnterprisesPropertiesSelectField } from '@features/enterprises-properties-select';
import { ClassSelectField } from '@features/tickets-class-select';
import { $data as $statuses } from '@features/tickets-status-select';
import { TypeSelectField } from '@features/tickets-new-type-select';
import { $pathname } from '@features/common';
import Paid from '@img/paid.svg';

import {
  $ticket,
  closeDetail,
  $mode,
  $currentCompany,
  createTicket,
  statusColor,
  $formattedTypes,
  $formattedPrepaymentTypes,
} from '../../models';
import { TicketInvoice } from '../ticket-invoice';
import { Value, TypesData } from '../../interfaces';
import { useStyles } from './classes';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  description: Yup.string().required(t('thisIsRequiredField')),
  property: Yup.mixed().required(t('thisIsRequiredField')),
});

const getCost = (priceType: string, priceLower: number, priceUpper: number) => {
  switch (priceType) {
    case 'range': {
      return (
        <span>
          <span>{`От ${priceLower} `}</span>
          <span>&#8381;</span>
          <span>{` до ${priceUpper} `}</span>
          <span>&#8381; </span>
        </span>
      );
    }
    case 'fixed': {
      return (
        <span>
          <span>Цена {priceLower} </span>
          <span>&#8381; </span>
        </span>
      );
    }
    case 'from':
      return (
        <span>
          <span>От {priceLower} </span>
          <span>&#8381; </span>
        </span>
      );
    case 'not-specified':
      return <div />;
    default: {
      return null;
    }
  }
};

const Ticket: FC = () => {
  const ticket = useStore($ticket);
  const mode = useStore($mode);
  const currentCompany = useStore($currentCompany);
  const pathname = useStore($pathname);
  const statuses = useStore($statuses);
  const formattedTypes = useStore($formattedTypes) as { [key: number]: TypesData };
  const formattedPrepaymentTypes = useStore($formattedPrepaymentTypes);

  const currentEnterpriseId = (currentCompany as Value)?.id;
  const enterpriseId = pathname.split('/')[2];

  const companyId = enterpriseId || currentEnterpriseId;

  const classes = useStyles();

  const isNew = !ticket.id;

  const status = statuses.find((item: { id: string }) => item.id === ticket.status);
  const statusId = status?.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Formik
        initialValues={ticket}
        //@ts-ignore
        onSubmit={(values) => createTicket(values)}
        enableReinitialize
        validationSchema={validationSchema}
        render={({ values, setFieldValue }) => {
          const types = [...values.types].filter((item) => item);
          const findedTypeId = types[0] ? types.reverse()[0]?.id : null;

          const paidData = findedTypeId
            ? formattedTypes[findedTypeId]?.paid || false
            : false;

          const paymentType = paidData
            ? formattedPrepaymentTypes[paidData.prepayment_type]
            : null;
          const cost = paidData
            ? getCost(paidData.price_type, paidData.price_lower, paidData.price_upper)
            : null;

          const paymentText =
            paymentType && cost ? (
              <div>
                {paymentType}. {cost}
              </div>
            ) : (
              ''
            );

          return (
            <Form style={{ height: 'calc(100% - 12px)', position: 'relative' }}>
              <DetailToolbar
                style={{ padding: 24 }}
                mode={mode}
                hidden={{ edit: true, close: true, delete: true }}
                onCancel={() => {
                  if (isNew) {
                    closeDetail();
                  }
                }}
              />
              <div
                style={{
                  height: isNew ? 'calc(100% - 82px)' : 'calc(100% - 40px)',
                  padding: '0 6px 24px 24px',
                }}
              >
                <CustomScrollbar>
                  <div style={{ paddingRight: 8, paddingBottom: 15 }}>
                    {!isNew && (
                      <div className={classes.field}>
                        <Field
                          label={t('RequestClass')}
                          placeholder={t('SelectRequestClass')}
                          component={ClassSelectField}
                          name="class"
                          mode="view"
                          excludeClassId="auto"
                          isNew={isNew}
                        />
                      </div>
                    )}
                    {values.id ? (
                      <div className={classes.field} style={{ width: '100%' }}>
                        <div className={classes.type}>
                          <div>
                            <Typography classes={{ root: classes.typeFieldName }}>
                              {t('RequestType')}
                            </Typography>
                            <div className={classes.typeField}>
                              {paidData && (
                                <Paid style={{ margin: '-2px 10px 0px 0px' }} />
                              )}
                              {values.formattedTypes}
                            </div>
                          </div>
                        </div>
                        {paymentText && (
                          <div className={classes.paymentText}>{paymentText}</div>
                        )}
                        <Divider />
                      </div>
                    ) : (
                      <>
                        <FieldArray
                          name="types"
                          render={() => (
                            <div className={classes.field}>
                              <Field
                                label={`${t('RequestType')} 1`}
                                placeholder={`${t('SelectRequestType')} 1`}
                                component={TypeSelectField}
                                name="types.0"
                                isFirstSelect
                                onChange={(value: TypesData) => {
                                  setFieldValue('types.0', value);
                                  setFieldValue('types.1', '');
                                  setFieldValue('types.2', '');
                                }}
                                mode={isNew ? 'edit' : 'view'}
                                withoutArchived={isNew}
                                ticketId={values.id}
                                divider={
                                  paymentText
                                    ? values?.types[0]?.children &&
                                      values.types[0].children.length > 0
                                    : true
                                }
                              />
                              {Array.isArray(values?.types) &&
                                values.types.map((type: TypesData, index: number) => {
                                  const formattedValuesTypes = [type].reduce(
                                    (
                                      acc: {
                                        [key: number]: {
                                          [key: string]: number[];
                                        };
                                      },
                                      value
                                    ) => {
                                      if (Boolean(value?.id) && Boolean(value?.title)) {
                                        return {
                                          ...acc,
                                          ...{
                                            [value.id]: {
                                              [value.title]: value?.children || [],
                                            },
                                          },
                                        };
                                      }

                                      return acc;
                                    },
                                    {}
                                  );

                                  const childrenTypes =
                                    Object.values(formattedValuesTypes)
                                      .map((item) => Object.values(item))
                                      .flat(2) || [];

                                  if (childrenTypes.length === 0) {
                                    return null;
                                  }

                                  const isExist = Boolean(
                                    values.types[index + 1] &&
                                      values.types[index + 1]?.children.length > 0
                                  );
                                  const divider = paymentText
                                    ? isExist
                                      ? index + 1 !== 2
                                      : false
                                    : true;

                                  return (
                                    <Field
                                      key={type.id}
                                      label={`${t('RequestType')} ${index + 2}`}
                                      placeholder={`${t('SelectRequestType')} ${
                                        index + 2
                                      }`}
                                      component={TypeSelectField}
                                      name={`types.${index + 1}`}
                                      mode={isNew ? 'edit' : 'view'}
                                      formattedTypes={formattedValuesTypes}
                                      childrenTypes={childrenTypes}
                                      withoutArchived={isNew}
                                      onChange={(value: TypesData) => {
                                        setFieldValue(`types.${index + 1}`, value);
                                        setFieldValue(`types.${index + 2}`, '');
                                      }}
                                      topLevelValue={[type]}
                                      divider={divider}
                                    />
                                  );
                                })}
                            </div>
                          )}
                        />
                        {paymentText && (
                          <div className={classes.field}>
                            <div className={classes.paymentText}>{paymentText}</div>
                            <Divider />
                          </div>
                        )}
                      </>
                    )}
                    <div className={classes.field} style={{ width: '100%' }}>
                      <Field
                        name="description"
                        label={t('RequestDescription')}
                        placeholder={t('EnterTheTextOfTheDescriptionRequest')}
                        component={InputField}
                        rowsMax={10}
                        multiline
                        mode={!isNew ? 'view' : mode}
                        required
                      />
                    </div>
                    {!isNew && (
                      <div className={classes.field}>
                        <Typography classes={{ root: classes.text }}>
                          {t('Label.status')}
                        </Typography>
                        <Chip
                          label={status?.title}
                          size="medium"
                          style={{
                            backgroundColor:
                              statusId && (statusColor as any)[statusId]?.backgroundColor,
                            color: statusId && (statusColor as any)[statusId]?.color,
                          }}
                        />
                        <Divider />
                      </div>
                    )}
                    <div className={classes.field}>
                      <Field
                        name="property"
                        label={t('room')}
                        placeholder={t('ChooseRoom')}
                        component={EnterprisesPropertiesSelectField}
                        enterprise_id={companyId}
                        mode={!isNew ? 'view' : mode}
                        required
                      />
                    </div>
                    {!isNew && (
                      <div className={classes.twoFields}>
                        <div className={classes.field}>
                          <Field
                            label={t('DateAndTimeTheRequestWasCreated')}
                            placeholder=""
                            component={InputField}
                            name="created_at"
                            type="datetime-local"
                            mode="view"
                          />
                        </div>
                        <div className={classes.field}>
                          <Field
                            label={t('RequestNumber')}
                            placeholder=""
                            component={InputField}
                            name="number"
                            mode="view"
                          />
                        </div>
                      </div>
                    )}
                    <TicketInvoice values={values} isNew={isNew} />
                  </div>
                </CustomScrollbar>
              </div>
            </Form>
          );
        }}
      />
    </Wrapper>
  );
};

export { Ticket };
