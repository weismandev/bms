import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { useMediaQuery, Typography } from '@mui/material';

import { InputField } from '@ui/index';
import { InvoicesStatusTypeSelectField } from '@features/tickets-invoices-status-select';

import { DownloadButton } from '../../atoms';
import { downloadFile } from '../../models';
import { Ticket, FileInvoice } from '../../interfaces';
import { useStyles } from './styles';

interface Props {
  values: Ticket;
}

interface InvoiceItem {
  id?: number;
  file: { id: number; name: string } | FileInvoice;
  amount: number;
  status: { id: string; title: string };
}

export const TicketInvoiceItem: FC<Props> = ({ values }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const isScreenLess = useMediaQuery('(max-width:1495px)');

  return Array.isArray(values?.invoices)
    ? values.invoices.map((item: InvoiceItem, index: number) => {
        const handleDownload = () => downloadFile(item.file as FileInvoice);

        const name = `invoices[${index}]`;

        const fields = (
          <div className={classes.fields}>
            <div className={classes.amountField}>
              <Field
                name={`${name}.amount`}
                label={null}
                placeholder={t('AmountPayable')}
                mode="view"
                component={InputField}
                divider={false}
                type="number"
              />
            </div>
            {item?.id && (
              <div className={classes.statusField}>
                <Field
                  name={`${name}.status`}
                  label={null}
                  placeholder={t('selectStatuse')}
                  mode="view"
                  component={InvoicesStatusTypeSelectField}
                  divider={false}
                />
              </div>
            )}
          </div>
        );

        const file = item?.file && (
          <div className={isScreenLess ? classes.fileScreenLess : classes.file}>
            <DownloadButton onClick={handleDownload} />
            <Typography
              noWrap
              title={item.file.name}
              classes={{ root: classes.fileName }}
            >
              {item.file.name}
            </Typography>
          </div>
        );

        return (
          <Field
            key={index}
            name={name}
            render={() =>
              isScreenLess ? (
                <div>
                  {file}
                  {fields}
                </div>
              ) : (
                <div className={classes.invoice}>
                  {file}
                  {fields}
                </div>
              )
            }
          />
        );
      })
    : null;
};
