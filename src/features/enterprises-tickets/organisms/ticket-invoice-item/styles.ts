import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  invoice: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  fields: {
    display: 'flex',
    '@media (max-width: 1495px)': {
      marginBottom: 20,
    },
    '@media (max-width: 1320px)': {
      flexDirection: 'column',
    },
  },
  amountField: {
    marginRight: 10,
    minWidth: 50,
    '@media (max-width: 1495px)': {
      minWidth: 'auto',
      width: '50%',
      marginRight: 20,
    },
    '@media (max-width: 1320px)': {
      minWidth: 'auto',
      width: '100%',
      margin: '0px 0px 10px 0px',
    },
  },
  statusField: {
    minWidth: 50,
    '@media (max-width: 1495px)': {
      minWidth: 'auto',
      width: '50%',
    },
    '@media (max-width: 1320px)': {
      minWidth: 'auto',
      width: '100%',
    },
  },
  fileName: {
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: 13,
    color: '#65657B',
    margin: '7px 30px 0px 0px',
  },
  file: {
    display: 'flex',
    width: '60%',
  },
  fileScreenLess: {
    display: 'flex',
    width: '100%',
    marginBottom: 20,
  },
}));
