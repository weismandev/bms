import { FC } from 'react';
import { useStore } from 'effector-react';

import { Toolbar, Greedy, AddButton, FoundInfo, FilterButton } from '@ui/index';
import i18n from '@shared/config/i18n';
import { EnterprisesCompaniesSelect } from '@features/enterprises-companies-select';

import {
  changeCompany,
  $currentCompany,
  $rowCount,
  addClicked,
  $isFilterOpen,
  changedFilterVisibility,
} from '../../models';
import { useStyles } from './styles';

const { t } = i18n;

const TableToolbar: FC = () => {
  const currentCompany = useStore($currentCompany);
  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);

  const classes = useStyles();

  const handleFilter = () => changedFilterVisibility(true);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        style={{ marginRight: 10 }}
        onClick={handleFilter}
      />
      <FoundInfo count={totalCount} />
      <div className={classes.select}>
        <EnterprisesCompaniesSelect
          onChange={changeCompany}
          value={currentCompany}
          label={null}
          divider={false}
          placeholder={t('ChooseCompany')}
          firstAsDefault
        />
      </div>
      <Greedy />
      <AddButton className={classes.margin} onClick={addClicked} />
    </Toolbar>
  );
};

export { TableToolbar };
