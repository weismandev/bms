import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  select: {
    marginLeft: 10,
    maxWidth: 300,
    width: '100%',
  },
});

export { useStyles };
