import { useMemo, useCallback, FC, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { Chip, Tooltip } from '@mui/material';
import { ErrorOutlineOutlined } from '@mui/icons-material';
import { Template } from '@devexpress/dx-react-core';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  DataTypeProviderProps,
} from '@devexpress/dx-react-grid';

import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  PagingPanel,
} from '@ui/index';
import i18n from '@shared/config/i18n';
import Paid from '@img/paid.svg';

import {
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  columnOrderChanged,
  $columnOrder,
  statusColor,
  open,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { InvoiceStatusItem } from '../../interfaces';

const { t } = i18n;

interface rowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: rowProps) => {
  const onRowClick = useCallback((_, id) => open(id), []);

  const row = useMemo(
    () => <TableRow {...props} onRowClick={onRowClick} />,
    [props.children]
  );
  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: ReactNode) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const StatusFormatter = ({ value }: { value: string[] }) => {
  const id = value[0];
  const title = value[1];

  return (
    <Chip
      label={title}
      size="medium"
      style={{
        backgroundColor: id && (statusColor as any)[id]?.backgroundColor,
        color: id && (statusColor as any)[id]?.color,
      }}
    />
  );
};

const TypeFormatter = ({ value, row: { formattedTypes, isTicketPaid } }: any) => (
  <div style={{ display: 'flex' }}>
    {formattedTypes.length > 0 ? (
      <Tooltip title={formattedTypes}>
        <div>
          {isTicketPaid ? (
            <Paid style={{ marginRight: 5 }} />
          ) : (
            <ErrorOutlineOutlined style={{ marginRight: 5, color: '#9F9F9F' }} />
          )}
        </div>
      </Tooltip>
    ) : null}
    {value}
  </div>
);

const PaymentFormatter = ({ value }: { value: InvoiceStatusItem }) => {
  if (!value) {
    return null;
  }

  const color = value?.id === 'paid' ? '#1BB169' : '#EB5757';

  return <span style={value?.id && { color }}>{value?.title}</span>;
};

const StatusTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={StatusFormatter} {...props} />
);

const TypesTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={TypeFormatter} {...props} />
);

const PaymentTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={PaymentFormatter} {...props} />
);

const TicketsTable: FC = () => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const columnOrder = useStore($columnOrder);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <DragDropProvider />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TypesTypeProvider for={['type']} />
        <StatusTypeProvider for={['status']} />
        <PaymentTypeProvider for={['payment']} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { TicketsTable };
