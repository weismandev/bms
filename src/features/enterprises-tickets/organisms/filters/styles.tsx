import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  form: {
    padding: 24,
    paddingTop: 0,
  },
  invoiceField: {
    marginTop: 15,
  },
}));
