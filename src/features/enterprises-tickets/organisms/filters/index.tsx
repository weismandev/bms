import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';

import i18n from '@shared/config/i18n';
import { InvoicesStatusTypeSelectField } from '@features/tickets-invoices-status-select';
import { TicketsPaymentTypeSelectField } from '@features/tickets-payment-type-select';
import { Wrapper, FilterToolbar, FilterFooter, CustomScrollbar } from '@ui/index';

import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { Value } from '../../interfaces';
import { useStyles } from './styles';

const { t } = i18n;

export const Filters = memo(() => {
  const filters = useStore($filters);
  const classes = useStyles();

  const onCloseFilter = () => changedFilterVisibility(false);
  const resetForm = () => filtersSubmitted('');

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar style={{ padding: 24 }} closeFilter={onCloseFilter} />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={resetForm}
          enableReinitialize
          render={({ values, setFieldValue }) => {
            const handlePayment = (value: Value) => {
              setFieldValue('payment', value);
              setFieldValue('invoiceStatus', '');
            };

            return (
              <Form className={classes.form}>
                <Field
                  name="payment"
                  label={t('Payment')}
                  placeholder={t('SelectPayment')}
                  component={TicketsPaymentTypeSelectField}
                  divider={values?.payment?.id !== 1}
                  onChange={handlePayment}
                />
                {typeof values?.payment === 'object' && values.payment?.id === 1 && (
                  <div className={classes.invoiceField}>
                    <Field
                      name="invoiceStatus"
                      label={null}
                      placeholder={t('selectStatuse')}
                      component={InvoicesStatusTypeSelectField}
                    />
                  </div>
                )}
                <FilterFooter isResettable />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
