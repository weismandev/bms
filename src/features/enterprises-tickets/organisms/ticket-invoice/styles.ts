import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  notInvoicesText: {
    fontStyle: 'italic',
    fontWeight: 500,
    fontSize: 13,
    color: '#767676',
    marginBottom: 20,
  },
}));
