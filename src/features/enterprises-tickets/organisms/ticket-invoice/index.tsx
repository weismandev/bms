import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { FieldArray } from 'formik';
import { Typography } from '@mui/material';

import { FormSectionHeader } from '@ui/index';

import { TicketInvoiceItem } from '../ticket-invoice-item';
import { Ticket } from '../../interfaces';
import { useStyles } from '../ticket/classes';
import { useStyles as useStylesInvoice } from './styles';

interface Props {
  values: Ticket;
  isNew: boolean;
}

export const TicketInvoice: FC<Props> = ({ values, isNew }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const classesInvoice = useStylesInvoice();

  const isPaidtype =
    values.types.length > 0 ? Boolean([...values.types].reverse()[0]?.paid) : false;

  if (!isPaidtype || isNew) {
    return null;
  }

  return (
    <div className={classes.container}>
      <FormSectionHeader header={t('Invoices')} />
      {values.invoices.length > 0 && (
        <FieldArray
          name="invoices"
          render={() => <TicketInvoiceItem values={values} />}
        />
      )}
      {values.invoices.length === 0 && (
        <Typography classes={{ root: classesInvoice.notInvoicesText }}>
          Тут будут отображаться счета
        </Typography>
      )}
    </div>
  );
};
