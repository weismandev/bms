import { api } from '@api/api2';

import {
  TicketsListPayload,
  CreateTicketPayload,
  TicketPayload,
  GetInvoicesPayload,
} from '../interfaces';

const getTicketList = (payload: TicketsListPayload) =>
  api.v1('post', 'tck/enterprise/tickets/list', payload, {
    headers: { 'Content-Type': 'application/json' },
  });
const createTicket = (payload: CreateTicketPayload) =>
  api.v1('post', 'tck/enterprise/tickets/create', payload);
const getTicketById = (payload: TicketPayload) =>
  api.v1('post', 'tck/enterprise/tickets/item', payload);
const getInvoices = (payload: GetInvoicesPayload) =>
  api.v1('post', 'tck/enterprise/tickets/item/list-invoices', payload);
const getTypesList = () => api.v4('get', 'ticket/types/list');

export const ticketApi = {
  getTicketList,
  createTicket,
  getTicketById,
  getInvoices,
  getTypesList,
};
