export const formatArray = <T extends { id: number }>(array: T[]) =>
  array.reduce(
    (acc, item) => ({
      ...acc,
      ...{ [item.id]: item },
    }),
    {}
  );
