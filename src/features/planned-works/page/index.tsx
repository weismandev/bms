import { FC, useEffect } from 'react';
import { useStore } from 'effector-react';
import { parseUrlParams } from '@tools/parseUrlParams';

import { HaveSectionAccess } from '@features/common';
import { FilterMainDetailLayout, ErrorMessage, Loader } from '@ui/index';

import {
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  open,
  PageGate,
  $isLoading,
  $isDetailOpen,
  $isFilterOpen,
  $path,
} from '../models';
import { PlannedWorkTable, Detail, Filters, ArchiveConfirmDialog } from '../organisms';

const PlannedWorksPage = () => {
  const error = useStore($error);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  const onClose = () => changedErrorDialogVisibility(false);

  useEffect(() => {
    const id = Number(parseUrlParams('id'));
    if (id && !isNaN(id)) {
      open(id);
    }
  }, []);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />

      <ArchiveConfirmDialog />

      <ErrorMessage isOpen={isErrorDialogOpen} onClose={onClose} error={error} />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filters />}
        main={<PlannedWorkTable />}
        detail={isDetailOpen && <Detail />}
        params={{
          detailWidth: !isFilterOpen ? 'minmax(370px, 50%)' : '35%',
        }}
      />
    </>
  );
};

const RestrictedPlannedWorksPage: FC = () => {
  const path = useStore($path);

  return (
    // <HaveSectionAccess>
      <PlannedWorksPage />
    // </HaveSectionAccess>
  );
};

export { RestrictedPlannedWorksPage as PlannedWorksPage };
