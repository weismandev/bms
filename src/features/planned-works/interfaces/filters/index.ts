import { Value } from '../planned-work';

export interface Filters {
  work_types: Value[];
  complexes: Value[];
  buildings: Value[];
  devices: Value[];
  service_next_at: string;
  repeat_types: {
    id: number;
    title: string;
    type: string;
  }[];
  is_archived: string | boolean;
}
