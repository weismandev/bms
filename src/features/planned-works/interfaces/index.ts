export * from './planned-work';
export * from './buildings';
export * from './filters';

export interface TableParams {
  page: number;
  per_page: number;
  search: string;
  sorting: {
    name: string;
    order: string;
  }[];
}
