export interface BuildingsResponse {
  buildings: BuildingsItem[];
}

export interface BuildingsItem {
  building: {
    id: number;
    title: string;
  };
}
