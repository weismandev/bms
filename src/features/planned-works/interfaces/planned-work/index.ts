import { Word } from '@tools/word-declension/word-declension.types';

export interface Value {
  id: number;
  slug: Word;
  title: string;
  fullname?: string;
  type?: string;
}

export type RepeatTypes = Omit<Value, 'slug' | 'fullname'>;

export interface Data {
  [key: number]: Value | RepeatTypes;
}

export interface PlannedWorkPayload {
  page: number;
  per_page: number;
  work_types?: number[];
  complexes?: number[];
  buildings?: number[];
  devices?: number[];
  service_next_at?: string;
  repeat_types?: number[];
  is_archived?: number;
  order_column?: string;
  order_direction?: string;
  search: string;
  export?: number;
}

export interface PlannedWorkResponse {
  meta: {
    total: number;
  };
  planned_works: PlannedWork[];
}

export interface PlannedWork {
  building_id: number;
  complex_id: number;
  created_at: string;
  description: string;
  devices: Device[];
  executor_id: number;
  id: number;
  repeat_frequency: number[];
  repeat_type: Value;
  repeat_until_at: string;
  repeat_until_days: number;
  service_first_at: string;
  service_last_at: string;
  service_next_at: string;
  title: string;
  updated_at: string;
  work_types: WorkType[];
  is_calculate_from_service_last_at: boolean;
}

export interface Device {
  id: number;
  model_title: string;
  serial_number: string;
  title: string;
}

export interface WorkType {
  id: number;
  title: string;
}

export interface CreateUpdatePlannedWorkPayload {
  repeat_type_id?: number;
  work_types?: number[];
  title?: string;
  complex_id?: number;
  building_id?: number;
  service_first_at?: string;
  repeat_until_at?: string;
  repeat_frequency?: string;
  repeat_until_days?: number;
  executor_id?: number;
  devices?: number[];
  is_calculate_from_service_last_at: boolean;
}

export interface WorkResponse {
  planned_work: PlannedWork;
}

export interface Opened {
  works: Value[];
  repeat_type: Value;
  title: string;
  object: Value;
  building: Value;
  service_first_at: string;
  repeat_until_at: string;
  repeat_frequency: number | string;
  repeat_until_days: number;
  executor: Value;
  devices: Value[];
  isCalculateServiceLastAt: boolean;
}

export interface ArchivePayload {
  planned_work_id: number;
}

export interface GetPlannedWorkPayload {
  planned_work_id: number;
}
