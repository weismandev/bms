import { format } from 'date-fns';
import { convertNumberToHours } from '@tools/convertNumberToHours';
import { formatAtom, FORMAT_REVERSE_DATE } from '@tools/formatAtom';
import { formatArray } from '../../libs';
import {
  Data,
  Value,
  CreateUpdatePlannedWorkPayload,
  PlannedWork,
  Opened,
  RepeatTypes,
} from '../../interfaces';

export const formatOpened = (
  opened: PlannedWork,
  complexes: Value[],
  repeatTypeData: RepeatTypes[],
  employees: Value[]
) => {
  const formattedComplexses: Data = formatArray<Value>(complexes);
  const formattedRepeatTypeData: Data = formatArray<RepeatTypes>(repeatTypeData);
  const formattedEmployees: Data = formatArray<Value>(employees);

  const devices =
    opened?.devices.length > 0
      ? opened?.devices.map((item) => ({
        id: item.serial_number,
        title: item.title,
      }))
      : [];

  return {
    id: opened.id,
    title: opened.title,
    object: formattedComplexses[opened.complex_id] || '',
    building: opened.building_id || '',
    devices,
    works: opened.work_types,
    service_last_at: formatAtom(opened.service_last_at, FORMAT_REVERSE_DATE),
    service_first_at: formatAtom(opened.service_first_at, FORMAT_REVERSE_DATE),
    repeat_type: formattedRepeatTypeData[opened.repeat_type.id] || '',
    repeat_until_at: formatAtom(opened.repeat_until_at, FORMAT_REVERSE_DATE),
    repeat_frequency:
      opened.repeat_frequency && opened.repeat_frequency.length > 0
        ? opened.repeat_frequency[0]
        : '',
    service_next_at: formatAtom(opened.service_next_at, FORMAT_REVERSE_DATE),
    repeat_until_days: opened.repeat_until_days,
    executor: formattedEmployees[opened.executor_id] || '',
    isCalculateServiceLastAt: opened.is_calculate_from_service_last_at ?? false,
  };
};

export const formatPayload = (data: Opened) => {
  const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  const payload: CreateUpdatePlannedWorkPayload = {
    is_calculate_from_service_last_at: data.isCalculateServiceLastAt
  };

  if (data.devices.length > 0) {
    const formattedDevices = data.devices.map((item: Value) => item.id);

    payload.devices = formattedDevices;
  }

  if (data.works.length > 0) {
    const formattedWorks = data.works.map((item: Value) => item.id);

    payload.work_types = formattedWorks;
  }

  if (data.repeat_type && typeof data.repeat_type === 'object') {
    payload.repeat_type_id = data.repeat_type.id;
  }

  if (data.title.length > 0) {
    payload.title = data.title;
  }

  if (data.object && typeof data.object === 'object') {
    payload.complex_id = data.object.id;
  }

  if (data.building) {
    payload.building_id =
      typeof data.building === 'object' ? data.building.id : data.building;
  }

  if (data.service_first_at && data.service_first_at.length > 0) {
    const date = new Date(data.service_first_at);
    const formattedDate = `${format(date, FORMAT)}${convertNumberToHours(
      date.getTimezoneOffset(),
      false
    )}`;
    payload.service_first_at = formattedDate;
  }

  if (data.repeat_until_at && data.repeat_until_at.length > 0) {
    const date = new Date(data.repeat_until_at);
    const formattedDate = `${format(date, FORMAT)}${convertNumberToHours(
      date.getTimezoneOffset(),
      false
    )}`;
    payload.repeat_until_at = formattedDate;
  }

  if (typeof data.repeat_frequency === 'number') {
    payload.repeat_frequency = data.repeat_frequency.toString();
  }

  if (typeof data.repeat_until_days === 'number') {
    payload.repeat_until_days = data.repeat_until_days;
  }

  if (data.executor && typeof data.executor === 'object') {
    payload.executor_id = data.executor.id;
  }

  return payload;
};
