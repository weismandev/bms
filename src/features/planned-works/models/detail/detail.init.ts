import { sample, guard, forward, merge } from 'effector';
import { delay } from 'patronum';

import { signout, $pathname, history, historyPush } from '@features/common';
import { $data as $complexes } from '@features/complex-select';
import { $employees } from '@features/employee-select';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';

import { pageMounted, pageUnmounted, $path, $entityId } from '../page/page.model';
import { filtersSubmitted } from '../filters/filters.model';
import { data as repeatTypeData } from '../repeat-type-select/repeat-type-select.model';
import {
  $opened,
  fxCreatePlannedWork,
  entityApi,
  $mode,
  fxUpdatePlannedWork,
  fxArchive,
  archive,
  $isDetailOpen,
  fxUnArchive,
  unArchive,
  fxGetPlannedWork,
  openViaUrl,
  $plannedWork,
  changedDetailVisibility,
  detailClosed,
} from './detail.model';
import { formatPayload } from './detail.formatter';

import { GetPlannedWorkPayload } from '../../interfaces';

$plannedWork
  .on(fxGetPlannedWork.done, (_, { result: { planned_work } }) => planned_work)
  .reset([signout, detailClosed]);

guard({
  clock: pageMounted,
  source: { pathname: $pathname, planned_work_id: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetPlannedWork.prepend<GetPlannedWorkPayload>(
    ({ planned_work_id }) => ({ planned_work_id })),
});

forward<number>({
  from: openViaUrl,
  to: fxGetPlannedWork.prepend((planned_work_id) => ({ planned_work_id }))
});

const delayedRedirect = delay({ source: fxGetPlannedWork.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxCreatePlannedWork.done,
  fn: ({
    result: {
      planned_work: { id },
    },
  }) => ({
    planned_work_id: id,
  }),
  target: fxGetPlannedWork,
});

sample({
  clock: fxCreatePlannedWork.done,
  source: $path,
  fn: (path, { result: { id } }) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxUpdatePlannedWork.done,
  fn: ({
    result: {
      planned_work: { id },
    },
  }) => ({
    planned_work_id: id,
  }),
  target: fxGetPlannedWork,
});

sample({
  clock: entityApi.create,
  fn: formatPayload,
  target: fxCreatePlannedWork,
});

sample({
  clock: changedDetailVisibility,
  source: $path,
  fn: (path) => `../${path}`,
  filter: (payload) => Boolean(payload),
  target: [historyPush, detailClosed],
});

$mode
  .on(unArchive, () => 'edit')
  .reset([fxCreatePlannedWork.done, signout, pageUnmounted, fxUpdatePlannedWork.done]);

$opened.reset([
  pageUnmounted,
  signout,
  fxArchive.done,
  fxUnArchive.done,
  filtersSubmitted,
  fxGetPlannedWork.pending,
]);

sample({
  clock: entityApi.update,
  fn: ({ id, ...restData }) => ({
    planned_work_id: id,
    ...formatPayload(restData),
  }),
  target: fxUpdatePlannedWork,
});

sample({
  clock: archive,
  source: $opened,
  fn: ({ id }) => ({ planned_work_id: id }),
  target: fxArchive,
});

$isDetailOpen
  .on(fxGetPlannedWork.done, () => true)
  .reset([fxArchive.done, fxUnArchive.done, filtersSubmitted]);

sample({
  clock: unArchive,
  source: $opened,
  fn: ({ id }) => ({ planned_work_id: id }),
  target: fxUnArchive,
});
