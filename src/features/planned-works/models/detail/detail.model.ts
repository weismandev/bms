import { createEffect, createEvent, createStore, combine } from 'effector';

import { createDetailBag } from '@tools/factories';

import { $data as $complexes } from '@features/complex-select';
import { $employees } from '@features/employee-select';
import { data as repeatTypeData } from '../repeat-type-select/repeat-type-select.model';

import { formatOpened } from './detail.formatter';

import {
  CreateUpdatePlannedWorkPayload,
  ArchivePayload,
  GetPlannedWorkPayload,
  WorkResponse,
  PlannedWork,
} from '../../interfaces';

export const newEntity = {
  title: '',
  object: '',
  building: '',
  devices: [],
  works: [],
  service_first_at: '',
  repeat_type: '',
  repeat_until_at: '',
  repeat_frequency: '',
  repeat_until_days: 7,
  executor: '',
  isCalculateServiceLastAt: false,
};

export const fxCreatePlannedWork = createEffect<
  CreateUpdatePlannedWorkPayload,
  WorkResponse,
  Error
>();
export const fxUpdatePlannedWork = createEffect<
  CreateUpdatePlannedWorkPayload,
  WorkResponse,
  Error
>();
export const fxArchive = createEffect<ArchivePayload, Object, Error>();
export const fxUnArchive = createEffect<ArchivePayload, Object, Error>();
export const fxGetPlannedWork = createEffect<
  GetPlannedWorkPayload,
  WorkResponse,
  Error
>();

export const archive = createEvent<void>();
export const unArchive = createEvent<void>();

export const $plannedWork = createStore<PlannedWork>(newEntity);

export const $opened = combine(
  $plannedWork, $complexes, $employees, (plannedWork, complexes, employees) =>
  formatOpened(plannedWork, complexes, repeatTypeData, employees)
);

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  entityApi,
  openViaUrl,
  detailClosed,

  $isDetailOpen,
  $mode,
  // $opened,
} = createDetailBag(newEntity);
