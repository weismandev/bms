import i18n from '@shared/config/i18n';
import { RepeatTypes } from '../../interfaces';

const { t } = i18n;

export const data: RepeatTypes[] = [
  { id: 1, title: t('Daily'), type: 'day' },
  { id: 2, title: t('Weekly'), type: 'week' },
  { id: 3, title: t('Monthly'), type: 'month' },
  { id: 4, title: t('Annually'), type: 'year' },
];
