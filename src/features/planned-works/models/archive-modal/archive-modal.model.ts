import { createEvent, createStore } from 'effector';

export const changedModalVisibility = createEvent<boolean>();

export const $isOpen = createStore<boolean>(false);
