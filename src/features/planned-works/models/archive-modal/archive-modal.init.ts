import { signout } from '@features/common';

import { pageUnmounted } from '../page/page.model';
import { archive } from '../detail/detail.model';
import { $isOpen, changedModalVisibility } from './archive-modal.model';

$isOpen
  .on(changedModalVisibility, (_, visibility) => visibility)
  .reset([signout, pageUnmounted, archive]);
