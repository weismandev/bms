import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  work_types: [],
  complexes: [],
  buildings: [],
  devices: [],
  service_next_at: '',
  repeat_types: [],
  is_archived: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
