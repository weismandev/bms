import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { $pathname } from '@features/common';

import {
  PlannedWorkPayload,
  PlannedWorkResponse,
  BuildingsResponse,
  Value,
} from '../../interfaces';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const changedErrorDialogVisibility = createEvent<boolean>();
export const exportPlannedWorks = createEvent<void>();

export const fxGetPlannedWorkList = createEffect<
  PlannedWorkPayload,
  PlannedWorkResponse,
  Error
>();
export const fxExportPlannedWorkList = createEffect<
  PlannedWorkPayload,
  ArrayBuffer,
  Error
>();
export const fxGetBuildings = createEffect<void, BuildingsResponse, Error>();

export const $isLoading = createStore<boolean>(false);
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
export const $raw = createStore<PlannedWorkResponse>({
  meta: { total: 0 },
  planned_works: [],
});
export const $buildings = createStore<Value[]>([]);
export const $path = $pathname.map((pathname: string) => pathname.split('/')[1]);
export const $entityId = $pathname.map<number>((pathname: string) => Number(pathname.split('/')[2]));
