import { format } from 'date-fns';
import { convertNumberToHours } from '@tools/convertNumberToHours';
import { PlannedWorkPayload, TableParams, Filters, Value, RepeatTypes } from '../../interfaces';

const FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

export const formatPayload = (tableParams: TableParams, filters: Filters) => {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { page, per_page, sorting, search } = tableParams;

  const payload: PlannedWorkPayload = {
    page,
    per_page,
    search,
  };

  if (sorting.length > 0) {
    payload.order_column = sorting[0].name;
    payload.order_direction = sorting[0].order;
  }

  if (filters.work_types.length > 0) {
    const formattedWorks = filters.work_types.map((item: Value) => item.id);

    payload.work_types = formattedWorks;
  }

  if (filters.complexes.length > 0) {
    const formattedComplexes = filters.complexes.map((item: Value) => item.id);

    payload.complexes = formattedComplexes;
  }

  if (filters.buildings.length > 0) {
    const formattedBuildings = filters.buildings.map((item: Value) => item.id);

    payload.buildings = formattedBuildings;
  }

  if (filters.devices.length > 0) {
    const formattedDevices = filters.devices.map((item: Value) => item.id);

    payload.devices = formattedDevices;
  }

  if (filters.service_next_at.length > 0) {
    const date = new Date(filters.service_next_at);
    date.setHours(0);
    payload.service_next_at = `${format(date, FORMAT)}${convertNumberToHours(
      date.getTimezoneOffset(),
      false
    )}`;
  }

  if (filters.repeat_types.length > 0) {
    const formattedRepeatTypes = filters.repeat_types.map((item: RepeatTypes) => item.id);

    payload.repeat_types = formattedRepeatTypes;
  }

  if (typeof filters.is_archived === 'boolean') {
    payload.is_archived = Number(Boolean(filters.is_archived));
  }

  return payload;
};
