import { forward, merge, sample, guard } from 'effector';
import { pending } from 'patronum';
import { format } from 'date-fns';
import FileSaver from 'file-saver';

import { signout } from '@features/common';
import { fxGetList as fxGetComplexes } from '@features/complex-select';
import { fxGetList as fxGetEmployee } from '@features/employee-select';

import { plannedWorkApi } from '../../api';
import { $tableParams } from '../table/table.model';
import {
  fxCreatePlannedWork,
  fxUpdatePlannedWork,
  fxArchive,
  fxUnArchive,
  fxGetPlannedWork,
} from '../detail/detail.model';
import { $filters } from '../filters/filters.model';
import {
  pageUnmounted,
  pageMounted,
  changedErrorDialogVisibility,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  fxGetPlannedWorkList,
  $raw,
  fxGetBuildings,
  $buildings,
  exportPlannedWorks,
  fxExportPlannedWorkList,
} from './page.model';

import { formatPayload } from './page.formatter';

fxGetPlannedWorkList.use(plannedWorkApi.getPlannedWorkList);
fxCreatePlannedWork.use(plannedWorkApi.createPlannedWork);
fxUpdatePlannedWork.use(plannedWorkApi.updatePlannedWork);
fxArchive.use(plannedWorkApi.archivePlannedWork);
fxUnArchive.use(plannedWorkApi.unArchivePlannedWork);
fxGetPlannedWork.use(plannedWorkApi.getPlannedWork);
fxGetBuildings.use(plannedWorkApi.getBuildings);
fxExportPlannedWorkList.use(plannedWorkApi.exportPlannedWorks);

const errorOccured = merge([
  fxGetPlannedWorkList.fail,
  fxCreatePlannedWork.fail,
  fxUpdatePlannedWork.fail,
  fxArchive.fail,
  fxUnArchive.fail,
  fxGetPlannedWork.fail,
  fxGetBuildings.fail,
  fxExportPlannedWorkList.fail,
]);

$isLoading
  .on(
    pending({
      effects: [
        fxGetPlannedWorkList,
        fxCreatePlannedWork,
        fxUpdatePlannedWork,
        fxArchive,
        fxUnArchive,
        fxGetPlannedWork,
        fxGetBuildings,
        fxExportPlannedWorkList,
      ],
    }),
    (_, pending) => pending
  )
  .reset([pageUnmounted, signout]);

$error.on(errorOccured, (_, { error }) => error).reset([pageUnmounted, signout]);

$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true)
  .reset([pageUnmounted, signout]);

guard({
  clock: pageMounted,
  source: $buildings,
  filter: (buildings) => buildings.length === 0,
  target: fxGetBuildings,
});

forward({
  from: pageMounted,
  to: [fxGetComplexes, fxGetEmployee],
});

sample({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxCreatePlannedWork.done,
    fxArchive.done,
    fxUpdatePlannedWork.done,
    fxUnArchive.done,
  ],
  source: { tableParams: $tableParams, filters: $filters },
  fn: ({ tableParams, filters }) => formatPayload(tableParams, filters),
  target: fxGetPlannedWorkList,
});

sample({
  clock: exportPlannedWorks,
  source: { tableParams: $tableParams, filters: $filters },
  fn: ({ tableParams, filters }) => ({
    ...formatPayload(tableParams, filters),
    ...{ export: 1 },
  }),
  target: fxExportPlannedWorkList,
});

fxExportPlannedWorkList.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `planned-works-${dateTimeNow}.xlsx`);
});

$raw
  .on(fxGetPlannedWorkList.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

$buildings
  .on(fxGetBuildings.done, (_, { result }) => {
    if (!Array.isArray(result?.buildings)) {
      return [];
    }

    return result.buildings.map((build) => ({
      id: build.building.id,
      title: build.building.title,
    }));
  })
  .reset(signout);
