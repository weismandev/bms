import { formatAtom, FORMAT_DATE } from '@tools/formatAtom';
import { PlannedWork, Data } from '../../interfaces';

export const formatData = (
  works: PlannedWork[],
  complexses: Data,
  buildings: Data,
  formattedRepeatTypeData: Data,
  formattedEmployees: Data
) =>
  works.map((work) => {
    const devices =
      work.devices.length > 0 ? work.devices.map((item) => item.model_title) : '-';

    const work_type =
      work.work_types.length > 0 ? work.work_types.map((item) => item.title) : '-';

    return {
      id: work.id,
      object: complexses[work.complex_id]?.title || '',
      building: buildings[work.building_id]?.title || '',
      title: work.title,
      work_type: work_type,
      devices: devices,
      repeat_type: formattedRepeatTypeData[work.repeat_type_id]?.title || '',
      service_first_at: formatAtom(work.service_first_at || '', FORMAT_DATE),
      service_next_at: formatAtom(work.service_next_at || '', FORMAT_DATE),
      executor: formattedEmployees[work.executor_id]?.fullname || '',
    };
  });
