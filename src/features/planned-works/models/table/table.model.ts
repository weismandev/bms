import { combine } from 'effector';

import { createTableBag, IColumns, IWidths } from '@tools/factories/table';
import { $data as $complexes } from '@features/complex-select';
import { $employees } from '@features/employee-select';
import i18n from '@shared/config/i18n';

import { $raw, $buildings } from '../page/page.model';
import { data as repeatTypeData } from '../repeat-type-select/repeat-type-select.model';
import { formatArray } from '../../libs';
import { formatData } from './table.formatter';

const { t } = i18n;

export const columns: IColumns[] = [
  { name: 'object', title: t('AnObject') },
  { name: 'building', title: t('building') },
  { name: 'title', title: t('MaintenanceTitle') },
  { name: 'work_type', title: t('TypeOfWork') },
  { name: 'devices', title: t('Equipment') },
  { name: 'repeat_type', title: t('ServiceSchedule') },
  { name: 'service_first_at', title: t('DateOfFirstService') },
  { name: 'service_next_at', title: t('NextServiceDate') },
  { name: 'executor', title: t('Performer') },
];

export const columnsWidth: IWidths[] = [
  { columnName: 'object', width: 200 },
  { columnName: 'building', width: 350 },
  { columnName: 'title', width: 220 },
  { columnName: 'work_type', width: 400 },
  { columnName: 'devices', width: 400 },
  { columnName: 'repeat_type', width: 180 },
  { columnName: 'service_first_at', width: 180 },
  { columnName: 'service_next_at', width: 180 },
  { columnName: 'executor', width: 250 },
];

export const excludedColumnsFromSort = [
  'object',
  'building',
  'work_type',
  'devices',
  'repeat_type',
  'service_first_at',
  'service_next_at',
  'executor',
];

export const $rowCount = $raw.map(({ meta }) => meta?.total || 0);
export const $tableData = combine(
  $raw,
  $complexes,
  $buildings,
  $employees,
  ({ planned_works }, complexses, buildings, employees) => {
    if (planned_works && planned_works.length > 0) {
      const formattedComplexses = formatArray(complexses);
      const formattedBuildings = formatArray(buildings);
      const formattedRepeatTypeData = formatArray(repeatTypeData);
      const formattedEmployees = formatArray(employees);

      return formatData(
        planned_works,
        formattedComplexses,
        formattedBuildings,
        formattedRepeatTypeData,
        formattedEmployees
      );
    }

    return [];
  }
);

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  $tableParams,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $sorting,
  sortChanged,
  $search,
  searchChanged,
} = createTableBag(columns, {
  widths: columnsWidth,
  pageSize: 25,
  order: [],
});
