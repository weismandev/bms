import { filtersSubmitted } from '../filters/filters.model';
import { $currentPage } from './table.model';

$currentPage.reset(filtersSubmitted);
