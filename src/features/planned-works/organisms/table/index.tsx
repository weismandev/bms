import { useMemo, useCallback, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  SortingState,
} from '@devexpress/dx-react-grid';

import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  PagingPanel,
  Popover,
  IconButton,
  SortingLabel,
} from '@ui/index';

import {
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $opened,
  $sorting,
  sortChanged,
  excludedColumnsFromSort,
  openViaUrl,
} from '../../models';
import { TableToolbar } from '../table-toolbar';
import { useStyles } from './styles';

const { t } = i18n;

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);

  const onRowClick = useCallback((_, id: number) => openViaUrl(id), []);
  const isSelected = opened && opened.id === props.row.id;

  const row = useMemo(
    () => <TableRow {...props} onRowClick={onRowClick} isSelected={isSelected} />,
    [isSelected, props.children]
  );
  return row;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: ReactNode) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const FormatterComponent = ({ value }: { value: string[] }) => {
  const classes = useStyles();

  if (!value || value[0]?.length === 0) {
    return '';
  }

  return (
    <div className={classes.formatterContainer}>
      <div className={classes.firstValue}>{value[0]}</div>
      {value[1] && (
        <Popover
          trigger={
            <IconButton size="large">
              <span>{Number(value.slice(1).length)}</span>
            </IconButton>
          }
        >
          <div className={classes.otherValues}>
            {value.slice(1).map((item: string) => (
              <div key={item}>{item}</div>
            ))}
          </div>
        </Popover>
      )}
    </div>
  );
};

const WorkTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

const DeviceTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={FormatterComponent} {...props} />
);

export const PlannedWorkTable: React.FC = () => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const sorting = useStore($sorting);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <Table
          messages={{ noData: t('ScheduledMaintenanceNotFound') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludedColumnsFromSort} {...props} />
          )}
        />

        <WorkTypeProvider for={['work_type']} />
        <DeviceTypeProvider for={['devices']} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};
