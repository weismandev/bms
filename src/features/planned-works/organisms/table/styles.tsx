import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    padding: 24,
    '& .MuiTableCell-head': {
      whiteSpace: 'normal',
    },
  },
  formatterContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'start',
  },
  firstValue: {
    marginTop: 3,
  },
  otherValues: {
    display: 'grid',
    gap: 10,
    padding: 10,
    background: '#fcfcfc',
  },
}));
