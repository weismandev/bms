import { FC } from 'react';

import { SelectControl, SelectField } from '@ui/index';

import { data } from '../../models/repeat-type-select';
import { Value } from '../../interfaces';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

export const RepeatTypeSelect: FC<object> = (props) => (
  <SelectControl options={data} {...props} />
);

export const RepeatTypeSelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: Value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<RepeatTypeSelect />} onChange={onChange} {...props} />;
};
