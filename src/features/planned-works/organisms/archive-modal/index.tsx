import { useStore } from 'effector-react';
import { Toolbar } from '@mui/material';
import { ArchiveOutlined, Close } from '@mui/icons-material';

import { ActionButton, CloseButton, Modal } from '@ui/index';
import i18n from '@shared/config/i18n';

import { changedModalVisibility, archive, $isOpen } from '../../models';
import { useStyles } from './styles';

const { t } = i18n;

export const CustomToolbar = () => {
  const classes = useStyles();

  const onClose = () => changedModalVisibility(false);
  const onArchive = () => archive();

  return (
    <Toolbar disableGutters variant="dense" classes={{ root: classes.root }}>
      <ActionButton
        icon={<Close />}
        onClick={onClose}
        classes={{ root: classes.closeButton }}
      >
        {t('Cancel')}
      </ActionButton>
      <ActionButton
        icon={<ArchiveOutlined />}
        onClick={onArchive}
        classes={{ root: classes.archiveButton }}
      >
        {t('Archive')}
      </ActionButton>
    </Toolbar>
  );
};

export const ArchiveConfirmDialog = () => {
  const isOpen = useStore($isOpen);

  const classes = useStyles();

  const onClose = () => changedModalVisibility(false);

  if (!isOpen) {
    return null;
  }

  return (
    <Modal
      header={
        <span className={classes.header}>
          <span>{`${t('Archive')}?`}</span>
          <CloseButton onClick={onClose} />
        </span>
      }
      content={t('AreYouSureYouWantToArchive')}
      actions={<CustomToolbar />}
      isOpen={isOpen}
      onClose={onClose}
    />
  );
};
