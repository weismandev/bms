import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    minHeight: 'auto',
    justifyContent: 'flex-end',
    width: '100%',
  },
  margin: {
    margin: '0 10px',
  },
  archiveButton: {
    backgroundColor: '#EC7F00',
    margin: '0 0 0 10px',
    '&:hover': {
      background: '#EC7F00',
    },
  },
  closeButton: {
    backgroundColor: '#AAAAAA',
    margin: '0 0 0 10px',
    '&:hover': {
      background: '#AAAAAA',
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));
