import makeStyles from '@mui/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
  },
  form: {
    height: 'calc(100% - 12px)',
    position: 'relative',
  },
  formContent: {
    height: 'calc(100% - 82px)',
    padding: '0 0 24px 24px',
  },
  content: {
    paddingRight: 18,
  },
  twoFields: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1165px)': {
      flexDirection: 'column',
    },
  },
  field: {
    paddingBottom: 10,
    paddingRight: 15,
    width: '50%',
    '@media (max-width: 1165px)': {
      width: '100%',
    },
  },
  fieldControlLabel: {
    paddingRight: 15,
    width: '100%',
    marginBottom: 24,

    '& span': {
      fontSize: 14,
    },
  },
  fieldContainer: {
    background: '#E1EBFF',
    borderRadius: 15,
    padding: '18px 24px 10px',
    marginBottom: 15,
  },
  repeatFrequency: {
    display: 'flex',
    marginTop: 25,
    '@media (max-width: 1165px)': {
      marginTop: 0,
    },
  },
  text: {
    color: '#9494A3',
    fontWeight: 400,
    fontSize: 14,
    marginTop: 5,
    marginLeft: 10,
  },
}));
