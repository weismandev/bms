import { memo, useState, useEffect } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { Typography, FormControlLabel } from '@mui/material';
import { InfoOutlined } from '@mui/icons-material';
import Tooltip from '@mui/material/Tooltip';
import { wordDeclension, wordGender } from '@tools/word-declension';
import { Cases } from '@tools/word-declension/word-declension.types';

import { Wrapper, DetailToolbar, CustomScrollbar, InputField, Divider, Checkbox } from '@ui/index';
import i18n from '@shared/config/i18n';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { EquipmentSelectField } from '@features/equipment-select';
import { WorkSelectField } from '@features/works-select';
import { EmployeeSelectField } from '@features/employee-select';

import {
  $mode,
  changedDetailVisibility,
  $opened,
  detailSubmitted,
  changeMode,
  unArchive,
  $filters,
  changedModalVisibility,
} from '../../models';
import { RepeatTypeSelectField } from '../repeat-type-select';
import { Opened, Value } from '../../interfaces';
import { useStyles } from './styles';
import { validationSchema, validationSchemaArchived } from './validation';

const { t } = i18n;

export const Detail = memo(() => {
  const mode = useStore($mode);
  const { isCalculateServiceLastAt, ...opened } = useStore($opened);
  const { is_archived } = useStore($filters);

  const [formControl, setFormControl] = useState<{
    isCalculateServiceLastAt: boolean
  }>({
    isCalculateServiceLastAt
  });

  const isArchived = Boolean(is_archived);
  const isNew = !opened.id;

  const classes = useStyles();

  const translateRepeat = (value: Value, frequency: number) =>
    wordDeclension(value.type, frequency);

  const getRepeatLabel = (value: Value, frequency: number) =>
    <span style={{ textTransform: 'capitalize' }}>{wordDeclension('each', frequency, Cases.NOMINATIVE, wordGender[value.type])}</span>;

  const onEdit = () => changeMode('edit');
  const onClose = () => changedDetailVisibility(false);
  const onArchive = () => {
    if (!isArchived) {
      return changedModalVisibility(true);
    }
    return changeMode('edit');
  };
  const onSubmit = (values: Opened) => {
    if (!isArchived) {
      return detailSubmitted({ ...values, ...formControl });
    }

    return unArchive();
  };
  const handleFormControl = (type: 'isCalculateServiceLastAt') => () => {
    setFormControl({
      ...formControl,
      [type]: !formControl[type],
    });
  };

  const formMode = isArchived ? 'view' : mode;
  const validation = isArchived ? validationSchemaArchived : validationSchema(isNew);

  const renderHint = !isNew ? (
    <Tooltip title={t<string>('CannotEditFirstMaintenanceDateAfterMaintenanceHasBeenPerformed')}>
      <InfoOutlined style={{ fontSize: 15, marginLeft: 2 }} />
    </Tooltip>
  ) : null;

  useEffect(() => {
    setFormControl({
      ...formControl,
      isCalculateServiceLastAt
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isCalculateServiceLastAt]);

  return (
    <Wrapper className={classes.wrapper}>
      <Formik
        initialValues={opened}
        onSubmit={onSubmit}
        validationSchema={validation}
        enableReinitialize
        render={({ resetForm, values, setFieldValue }) => (
          <Form className={classes.form}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={onEdit}
              onClose={onClose}
              onCancel={() => {
                if (isNew) {
                  changedDetailVisibility(false);
                } else {
                  changeMode('view');
                  resetForm();
                }
              }}
              isArchived={isArchived}
              onArchive={onArchive}
              hidden={{ delete: true, edit: isArchived, archive: isNew }}
            />
            <div className={classes.formContent}>
              <CustomScrollbar>
                <div className={classes.content}>
                  <div className={classes.field} style={{ width: '100%' }}>
                    <Field
                      label={t('MaintenanceTitle')}
                      placeholder={t('EnterTheTitle')}
                      component={InputField}
                      name="title"
                      mode={formMode}
                      required
                    />
                  </div>
                  <div className={classes.twoFields}>
                    <div className={classes.field}>
                      <Field
                        label={t('AnObject')}
                        placeholder={t('selectObject')}
                        component={ComplexSelectField}
                        name="object"
                        mode={mode}
                        required
                        onChange={(value: object) => {
                          setFieldValue('object', value);
                          setFieldValue('building', '');
                          setFieldValue('devices', '');
                        }}
                      />
                    </div>
                    <div className={classes.field}>
                      <Field
                        label={t('building')}
                        placeholder={t('selectBbuilding')}
                        component={HouseSelectField}
                        complex={values?.object ? [values.object] : []}
                        name="building"
                        mode={mode}
                        required
                        onChange={(value: object) => {
                          setFieldValue('building', value);
                          setFieldValue('devices', '');
                        }}
                      />
                    </div>
                  </div>
                  <div className={classes.field}>
                    <Field
                      label={t('equipment')}
                      placeholder={t('ChooseEquipment')}
                      component={EquipmentSelectField}
                      object={values.object}
                      house={values.building}
                      name="devices"
                      mode={mode}
                      helpText={t('ForListEquipmentAppearStartTypingName')}
                      isMulti
                      required
                      menuPlacement="auto"
                    />
                  </div>
                  <div className={classes.field}>
                    <Field
                      label={t('NameOfWorks')}
                      placeholder={t('ChooseAWork')}
                      component={WorkSelectField}
                      name="works"
                      mode={mode}
                      isMulti
                      required
                    />
                  </div>
                  {!isNew && (
                    <div className={classes.field}>
                      <Field
                        label={t('DateOfLastService')}
                        placeholder={t('SelectADate')}
                        component={InputField}
                        name="service_last_at"
                        mode="view"
                        type="date"
                      />
                    </div>
                  )}
                  <div>
                    <FormControlLabel
                      label={t('CalculateLastMaintenanceSchedule')}
                      className={classes.fieldControlLabel}
                      control={(
                        <Checkbox
                          name={t('isCalculateServiceLastAt')}
                          size="small"
                          checked={formControl.isCalculateServiceLastAt}
                          onChange={handleFormControl('isCalculateServiceLastAt')}
                          disabled={mode === 'view'}
                        />
                      )}
                    />
                  </div>
                  <div className={classes.fieldContainer}>
                    <div className={classes.twoFields}>
                      <div className={classes.field}>
                        <Field
                          label={<span>{t('DateOfFirstService')}{renderHint}</span>}
                          placeholder={t('SelectADate')}
                          component={InputField}
                          name="service_first_at"
                          mode={formMode}
                          disabled={!isNew}
                          type="date"
                          required
                        />
                      </div>
                      <div className={classes.field}>
                        <Field
                          label={t('ServiceSchedule')}
                          placeholder={t('ChooseASchedule')}
                          component={RepeatTypeSelectField}
                          name="repeat_type"
                          mode={formMode}
                          required
                          onChange={(value: object) => {
                            setFieldValue('repeat_type', value);
                            setFieldValue('repeat_until_at', '');
                            setFieldValue('repeat_frequency', '');
                          }}
                        />
                      </div>
                    </div>
                    {values.repeat_type && typeof values.repeat_type === 'object' && (
                      <div className={classes.twoFields}>
                        <div className={classes.field}>
                          <Field
                            label={t('RepeatUntil')}
                            placeholder={t('SelectADate')}
                            component={InputField}
                            name="repeat_until_at"
                            mode={formMode}
                            type="date"
                          />
                        </div>
                        <div className={classes.field}>
                          <div className={classes.repeatFrequency}>
                            <div>
                              <Field
                                label={getRepeatLabel(values.repeat_type, values.repeat_frequency)}
                                placeholder=""
                                component={InputField}
                                mode={formMode}
                                labelPlacement="start"
                                divider={false}
                                name="repeat_frequency"
                                type="number"
                              />
                            </div>
                            <Typography
                              classes={{
                                root: classes.text,
                              }}
                            >
                              {translateRepeat(values.repeat_type, values.repeat_frequency)}
                            </Typography>
                          </div>
                        </div>
                        <Divider />
                      </div>
                    )}
                  </div>
                  <div className={classes.twoFields}>
                    {!isNew && (
                      <div className={classes.field}>
                        <Field
                          label={t('NextServiceDate')}
                          placeholder={t('SelectADate')}
                          component={InputField}
                          name="service_next_at"
                          mode="view"
                          type="date"
                        />
                      </div>
                    )}
                    <div className={classes.field}>
                      <Field
                        label={t('TheNumberOfDaysForWhichTheRequestIsFormed')}
                        placeholder={t('SpecifyTheNumberOfDays')}
                        component={InputField}
                        name="repeat_until_days"
                        mode={formMode}
                        type="number"
                      />
                    </div>
                  </div>
                  <div className={classes.field}>
                    <Field
                      label={t('Performer')}
                      placeholder={t('SelectPerformer')}
                      component={EmployeeSelectField}
                      name="executor"
                      mode={formMode}
                      divider
                    />
                  </div>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
});
