import * as Yup from 'yup';

import i18n from '@shared/config/i18n';

import { PlannedWork } from '../../interfaces';

const { t } = i18n;

interface Params {
  parent: PlannedWork;
}

export const validationSchema = (isNew: boolean) => Yup.object().shape({
  title: Yup.mixed().required(t('thisIsRequiredField')),
  object: Yup.mixed().required(t('thisIsRequiredField')),
  building: Yup.mixed().required(t('thisIsRequiredField')),
  devices: Yup.array()
    .min(1, t('thisIsRequiredField'))
    .required(t('thisIsRequiredField')),
  works: Yup.array().min(1, t('thisIsRequiredField')).required(t('thisIsRequiredField')),
  repeat_type: Yup.mixed().required(t('thisIsRequiredField')),
  service_first_at: Yup.mixed().test(
    'service-data-after-current-date',
    t('FirstServiceDateCannotBeEarlierThanCurrent'),
    serviceDataAfterCurrentDate(isNew)
  ),
  repeat_until_at: Yup.mixed().test(
    'repeat-data-after-service-date',
    t('DateMustBeAfterFirstServiceDate'),
    repeatDataAfterServiceDate
  ),
});

export const validationSchemaArchived = Yup.object().shape({
  object: Yup.mixed().required(t('thisIsRequiredField')),
  building: Yup.mixed().required(t('thisIsRequiredField')),
  devices: Yup.array()
    .min(1, t('thisIsRequiredField'))
    .required(t('thisIsRequiredField')),
  works: Yup.array().min(1, t('thisIsRequiredField')).required(t('thisIsRequiredField')),
});

function serviceDataAfterCurrentDate(isNew) {
  return function (this: Params) {
    const { service_first_at } = this.parent;
    if (!isNew) return true;

    if (!service_first_at) {
      return false;
    }
  
    const date = new Date(service_first_at);
    date.setHours(0, 0, 0);
  
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0);
  
    return Math.trunc(Number(date) / 1000) >= Math.trunc(Number(currentDate) / 1000);
  }
}

function repeatDataAfterServiceDate(this: Params) {
  const { service_first_at, repeat_until_at } = this.parent;

  if (!service_first_at || !repeat_until_at) {
    return true;
  }

  const serviceDate = new Date(service_first_at);
  serviceDate.setHours(0, 0, 0);

  const repeatDate = new Date(repeat_until_at);
  repeatDate.setHours(0, 0, 0);

  return Math.trunc(Number(repeatDate) / 1000) > Math.trunc(Number(serviceDate) / 1000);
}
