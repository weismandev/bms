import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';

import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
  SwitchField,
} from '@ui/index';
import { WorkSelectField } from '@features/works-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { EquipmentSelectField } from '@features/equipment-select';

import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models';
import { RepeatTypeSelectField } from '../repeat-type-select';
import { useStyles } from './styles';

const { t } = i18n;

export const Filters = memo(() => {
  const filters = useStore($filters);

  const classes = useStyles();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          render={({ values }) => (
            <Form className={classes.form}>
              <Field
                label={t('NameOfWorks')}
                placeholder={t('ChooseAWork')}
                component={WorkSelectField}
                name="work_types"
                isMulti
              />
              <Field
                label={t('AnObject')}
                placeholder={t('selectObject')}
                component={ComplexSelectField}
                name="complexes"
                isMulti
              />
              <Field
                label={t('building')}
                placeholder={t('selectBbuilding')}
                component={HouseSelectField}
                name="buildings"
                complex={values.complexes.length > 0 ? values.complexes : []}
                isMulti
              />
              <Field
                label={t('equipment')}
                placeholder={t('ChooseEquipment')}
                component={EquipmentSelectField}
                name="devices"
                helpText={t('ForListEquipmentAppearStartTypingName')}
                isMulti
              />
              <Field
                label={t('NextServiceDate')}
                placeholder={t('SelectADate')}
                component={InputField}
                name="service_next_at"
                type="date"
              />
              <Field
                label={t('ServiceSchedule')}
                placeholder={t('ChooseASchedule')}
                component={RepeatTypeSelectField}
                name="repeat_types"
                isMulti
              />
              <Field
                name="is_archived"
                component={SwitchField}
                label={t('ShowArchive')}
                labelPlacement="start"
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});
