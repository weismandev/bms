import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';

import {
  Toolbar,
  FilterButton,
  Greedy,
  AddButton,
  AdaptiveSearch,
  ExportButton,
} from '@ui/index';
import i18n from '@shared/config/i18n';

import {
  $rowCount,
  $hiddenColumnNames,
  $tableData,
  columns,
  addClicked,
  $isDetailOpen,
  $isFilterOpen,
  changedFilterVisibility,
  $search,
  searchChanged,
  $filters,
  $opened,
  exportPlannedWorks,
  $isLoading,
} from '../../models';

const { t } = i18n;

export const TableToolbar: FC = () => {
  const totalCount = useStore($rowCount);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const tableData = useStore($tableData);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);
  const searchValue = useStore($search);
  const { is_archived } = useStore($filters);
  const { id } = useStore($opened);
  const isLoading = useStore($isLoading);

  const isArchived = Boolean(is_archived);

  const isAnySideOpen = isFilterOpen && isDetailOpen;
  const isDisabled = (isDetailOpen && !id) || isArchived;

  const onClickFilter = () => changedFilterVisibility(true);

  return (
    <Toolbar>
      <Tooltip title={t('Filters') as string}>
        <div>
          <FilterButton
            disabled={isFilterOpen}
            style={{ marginRight: 10 }}
            onClick={onClickFilter}
          />
        </div>
      </Tooltip>
      <AdaptiveSearch
        {...{
          totalCount,
          searchValue,
          searchChanged,
          isAnySideOpen,
        }}
      />
      <Greedy />
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        style={{ marginRight: 10 }}
        disabled={isLoading || tableData.length === 0}
        onClick={exportPlannedWorks}
      />
      <Tooltip title={t('Add') as string}>
        <div>
          <AddButton disabled={isDisabled} onClick={addClicked} />
        </div>
      </Tooltip>
    </Toolbar>
  );
};
