import { api } from '@api/api2';

import {
  PlannedWorkPayload,
  CreateUpdatePlannedWorkPayload,
  ArchivePayload,
  GetPlannedWorkPayload,
} from '../interfaces';

const getPlannedWorkList = (payload: PlannedWorkPayload) =>
  api.v4('get', 'planned-work/list', payload);
const createPlannedWork = (payload: CreateUpdatePlannedWorkPayload) =>
  api.v4('post', 'planned-work/create', payload);
const updatePlannedWork = (payload: CreateUpdatePlannedWorkPayload) =>
  api.v4('post', 'planned-work/update', payload);
const archivePlannedWork = (payload: ArchivePayload) =>
  api.v4('post', 'planned-work/archive', payload);
const unArchivePlannedWork = (payload: ArchivePayload) =>
  api.v4('post', 'planned-work/unarchive', payload);
const getPlannedWork = (payload: GetPlannedWorkPayload) =>
  api.v4('get', 'planned-work/view', payload);
const getBuildings = () => api.v1('get', 'buildings/get-list-crm', { per_page: 1000 });
const exportPlannedWorks = (payload: PlannedWorkPayload) =>
  api.v4('get', 'planned-work/list', {
    ...payload,
    ...{ responseType: 'arraybuffer' },
  });

export const plannedWorkApi = {
  getPlannedWorkList,
  createPlannedWork,
  updatePlannedWork,
  archivePlannedWork,
  unArchivePlannedWork,
  getPlannedWork,
  getBuildings,
  exportPlannedWorks,
};
