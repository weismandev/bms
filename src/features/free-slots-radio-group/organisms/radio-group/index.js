import { useList, useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { RadioGroup, FormControlLabel, Radio, Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

import {
  FieldComponent,
  LabeledContent,
  Divider,
  BaseInputErrorText,
  BaseInputLabel,
  FormControl,
} from '@ui/index';

import { $data, GroupGate } from '../../models';

const useStyles = makeStyles(() => {
  const labelStyle = {
    fontSize: 14,
    color: '#9494A3 !important',
    lineHeight: '16px',
    marginBottom: 10,
  };

  const inputContainer = { display: 'flex', alignItems: 'center' };
  const fullWidth = {
    width: '100%',
  };

  return {
    formLabel: labelStyle,
    focusedFormLabel: labelStyle,
    inputContainer,
    fullWidth,
  };
});

const SlotViewMode = (props) => {
  const { value, label } = props;

  const { t } = useTranslation();

  return (
    <>
      <LabeledContent label={label}>
        {Boolean(value) && typeof value === 'object' ? value.number : t('isNotDefinedit')}
      </LabeledContent>
      <Divider />
    </>
  );
};

const FreeSlotsRadioGroup = (props) => {
  const {
    value,
    onChange,
    name,
    mode,
    label,
    enterprise_id,
    active_from,
    active_to,
    time_from,
    time_to,
    errors,
  } = props;

  const data = useStore($data);
  const { inputContainer, fullWidth } = useStyles();

  const valueId = Boolean(value) && typeof value === 'object' ? value.id : '';

  const options = useList($data, {
    keys: [value, mode],
    fn: ({ id, number }) => (
      <FormControlLabel
        checked={String(id) === String(valueId)}
        value={id}
        control={<Radio />}
        label={number}
      />
    ),
  });

  if (mode === 'view') {
    return <SlotViewMode label={label} value={value} />;
  }

  return (
    <>
      <GroupGate
        enterprise_id={enterprise_id}
        active_from={active_from}
        active_to={active_to}
        time_from={time_from}
        time_to={time_to}
      />

      {/*
       TODO: Gonna hide it for now. Might return it later.

       {options.length === 0 ? (
        <ColorPaper accentColor="#EB5757" bcgColor="rgb(235,87,87,.1)">
          <Typography
            style={{
              fontSize: 14,
              fontWeight: 500,
              color: '#65657B',
              marginBottom: 10,
            }}
          >
            Парковочные места в заданный период не найдены
          </Typography>
          <Typography style={{ fontSize: 14, color: '#65657B' }}>
            Скорректируйте дату и время действия пропуска
          </Typography>
        </ColorPaper>
      ) : null} */}

      <FormControl {...props}>
        <Grid container alignItems="baseline" justifyContent="space-between">
          <Grid item style={{ width: 'auto' }} classes={{ item: fullWidth }}>
            <BaseInputLabel>{label}</BaseInputLabel>
          </Grid>

          <Grid item classes={{ item: fullWidth }}>
            <Grid container direction="column">
              <Grid item className={inputContainer}>
                <RadioGroup
                  name={name}
                  value={valueId}
                  onChange={(e) =>
                    onChange(
                      data.find((i) => String(i.id) === String(e.target.value)) || ''
                    )
                  }
                >
                  {options}
                </RadioGroup>
              </Grid>
              <Grid item>
                <BaseInputErrorText errors={errors} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
      </FormControl>
    </>
  );
};

const FreeSlotsRadioGroupField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <FieldComponent component={<FreeSlotsRadioGroup />} onChange={onChange} {...props} />
  );
};

export { FreeSlotsRadioGroup, FreeSlotsRadioGroupField };
