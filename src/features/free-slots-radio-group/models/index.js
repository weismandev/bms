import { createEffect, createStore, guard, sample } from 'effector';
import { createGate } from 'effector-react';
import { debounce } from 'patronum/debounce';
import format from 'date-fns/format';
import { signout } from '../../common';
import { getList } from '../api';

const hasEnoughDataToRequest = ({
  enterprise_id,
  active_from,
  active_to,
  time_from,
  time_to,
}) =>
  Boolean(enterprise_id) &&
  Boolean(active_from) &&
  Boolean(active_to) &&
  Boolean(time_from) &&
  Boolean(time_to);

const formatPayload = ({
  enterprise_id,
  active_from,
  active_to,
  time_from,
  time_to,
}) => ({
  enterprise_id,
  active_from: format(active_from, 'yyyy-MM-dd'),
  active_to: format(active_to, 'yyyy-MM-dd'),
  time_from: format(time_from, 'HH:mm'),
  time_to: format(time_to, 'HH:mm'),
});

const GroupGate = createGate();

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

fxGetList.use(getList);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (!result.slots) {
      return [];
    }

    return result.slots;
  })
  .reset([GroupGate.close, signout]);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

guard({
  source: debounce({ source: GroupGate.state.updates, timeout: 500 }),
  filter: hasEnoughDataToRequest,
  target: fxGetList.prepend(formatPayload),
});

sample({
  source: GroupGate.state.updates,
  filter: ({ enterprise_id, active_from, active_to, time_from, time_to }) => {
    const isExistAllParams =
      Boolean(enterprise_id) &&
      Boolean(active_from) &&
      Boolean(active_to) &&
      Boolean(time_from) &&
      Boolean(time_to);

    return !isExistAllParams;
  },
  fn: () => [],
  target: $data,
});

export { fxGetList, $data, $error, $isLoading, GroupGate };
