import { api } from '../../../api/api2';

export const getList = (payload = {}) =>
  api.v1('get', 'scud/client/management/pass/get-free-slots', payload);
