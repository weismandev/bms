import { createEffect, createStore } from 'effector';
import { PriorityResponse, PriorityData } from '../../interfaces';

const fxGetList = createEffect<void, PriorityResponse, Error>();

const $data = createStore<PriorityData[] | []>([]);
const $isLoading = createStore<boolean>(false);

export { fxGetList, $data, $isLoading };
