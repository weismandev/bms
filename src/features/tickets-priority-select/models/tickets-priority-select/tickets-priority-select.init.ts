import { signout } from '@features/common';
import { priorityApi } from '../../api';
import { fxGetList, $data, $isLoading } from './tickets-priority-select.model';

fxGetList.use(priorityApi.getPrioritiesList);

$data
  .on(fxGetList.done, (_, { result }) => {
    if (!Array.isArray(result?.priorities)) {
      return [];
    }

    return result.priorities.map((item) => ({
      id: item.name,
      title: item.title,
    }));
  })
  .reset(signout);

$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
