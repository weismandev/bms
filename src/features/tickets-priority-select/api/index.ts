import { api } from '@api/api2';

const getPrioritiesList = () => api.v1('get', 'tck/priorities/list');

export const priorityApi = {
  getPrioritiesList,
};
