import { useEffect, FC } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import { PriorityData } from '../../interfaces';
import { $data, fxGetList, $isLoading } from '../../models';

interface selectFieldProps {
  form: {
    setFieldValue: (name: string, value: object) => void;
  };
  field: {
    name: string;
  };
}

const PrioritySelect: FC<any> = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={options} isLoading={isLoading} {...props} />;
};

const PrioritySelectField: FC<selectFieldProps> = (props) => {
  const onChange = (value: PriorityData) =>
    props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PrioritySelect />} onChange={onChange} {...props} />;
};

export { PrioritySelect, PrioritySelectField };
