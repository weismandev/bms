export interface PriorityResponse {
  priorities: {
    name: string;
    title: string;
  }[];
}

export interface PriorityData {
  id: string;
  title: string;
}
