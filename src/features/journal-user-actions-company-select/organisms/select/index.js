import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $options, $isLoading, $error, PageGate } from '../../model/select';

const JournalUserActionsCompanySelect = (props) => {
  const options = useStore($options);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <PageGate />
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
    </>
  );
};

const JournalUserActionsCompanySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<JournalUserActionsCompanySelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { JournalUserActionsCompanySelect, JournalUserActionsCompanySelectField };
