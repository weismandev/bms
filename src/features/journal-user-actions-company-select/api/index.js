import { api } from '../../../api/api2';

const getCompanies = () => {
  return api.v1('get', 'journal/filter/get-company-list');
};

export const journalUserActionsSelectApi = {
  getCompanies,
};
