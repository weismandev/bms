import { createEffect, createStore } from 'effector';
import { createGate } from 'effector-react';

const $options = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

const fxGetSelectOptions = createEffect();

const PageGate = createGate();
const pageMounted = PageGate.open;

export { $options, $error, $isLoading, fxGetSelectOptions, PageGate, pageMounted };
