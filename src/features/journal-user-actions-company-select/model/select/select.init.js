import { forward } from 'effector';
import { signout } from '../../../common';
import { journalUserActionsSelectApi } from '../../api';
import {
  $options,
  $isLoading,
  $error,
  fxGetSelectOptions,
  pageMounted,
} from './select.model';

fxGetSelectOptions.use(journalUserActionsSelectApi.getCompanies);

$options
  .on(fxGetSelectOptions.doneData, (state, { company_list }) => {
    if (!company_list.length) {
      return [];
    }

    return company_list;
  })
  .reset(signout);

$error.on(fxGetSelectOptions.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetSelectOptions.pending, (state, result) => result).reset(signout);

forward({
  from: pageMounted,
  to: fxGetSelectOptions,
});
