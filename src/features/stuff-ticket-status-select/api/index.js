import { api } from '../../../api/api2';

const getList = (payload = {}) => api.v1('get', 'stuff-pass/crm/statuses/list', payload);

export { getList };
