import { useStore } from 'effector-react';
import { $pathname, HaveSectionAccess } from '@features/common';
import { ErrorMessage, FilterMainDetailLayout, Loader } from '@ui/index';

import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  $path,
  changedErrorDialogVisibility,
  PageGate,
} from '../models/page.model';
import { $isDetailOpen } from '../models/detail.model';
import { $isFilterOpen } from '../models/filter.model';

import { Detail, Filter, Table } from '../organisms';
import { ImportModal } from '../organisms/import-modal';

const UsersPage = () => {
  const pathname = useStore($pathname);
  const isDetailOpen = useStore($isDetailOpen);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const isLoading = useStore($isLoading);
  const error = useStore($error);
  const isFilterOpen = useStore($isFilterOpen);

  const userId = pathname.split('/')[2];

  return (
    <>
      <PageGate userId={userId} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <Loader isLoading={isLoading} />

      <ImportModal />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<Table />}
        detail={isDetailOpen && <Detail />}
        params={{ filterWidth: '400px', detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedUsersPage = (props) => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <UsersPage {...props} />
    </HaveSectionAccess>
  );
};

export { RestrictedUsersPage as UsersPage };
