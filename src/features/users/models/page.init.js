import { forward, sample, merge, attach, guard } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { fxGetUser, signout } from '@features/common';
import { usersApi } from '../api';
import { $opened, changedDetailVisibility, $currentTab } from './detail.model';
import { $filters } from './filter.model';
import {
  pageMounted,
  pageUnmounted,
  fxGetList,
  fxUserCreate,
  fxUserUpdate,
  fxArchiveUser,
  fxGetRoles,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  $rawUsersData,
  $userRole,
  fxGetContractingCompanies,
  $contractingCompanies,
  fxGetComplexList,
  $complexList,
  fxExportList,
  exportList,
  fxDownloadTemplate,
  downloadTemplate,
  validateFile,
  fxValidateFile,
  fxGetUserById,
  fxGetTimeTable,
  $timeData,
  fxSetTimeTable,
  archiveUser,
  fxImportData,
  changedGroupVisibility,
  $isGroupOpen,
  fxGetKeys,
  getKeys,
  fxGetEvents,
  getEvents,
  eventsFiltersSubmitted,
  $eventsFilters,
  $userIdFromRoute,
  needUser,
} from './page.model';
import { $tableParams, $pageSize } from './table.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

const gotError = merge([
  fxGetList.fail,
  fxUserCreate.fail,
  fxArchiveUser.fail,
  fxUserUpdate.fail,
  fxGetUser.fail,
  fxGetContractingCompanies.fail,
  fxGetComplexList.fail,
  fxExportList.fail,
  fxDownloadTemplate.fail,
  fxValidateFile.fail,
  fxGetUserById.fail,
  fxGetTimeTable.fail,
  fxSetTimeTable.fail,
  fxImportData.fail,
  fxGetKeys.fail,
  fxGetEvents.fail,
]);

fxGetRoles.use(usersApi.getRoles);
fxGetList.use(usersApi.getList);
fxUserCreate.use(usersApi.userCreate);
fxUserUpdate.use(usersApi.userUpdate);
fxArchiveUser.use(usersApi.archiveUser);
fxGetContractingCompanies.use(usersApi.getContractingCompanies);
fxGetComplexList.use(usersApi.getComplexList);
fxExportList.use(usersApi.exportList);
fxDownloadTemplate.use(usersApi.downloadTemplate);
fxValidateFile.use(usersApi.validateFile);
fxGetUserById.use(usersApi.getUserById);
fxGetTimeTable.use(usersApi.getTimeTable);
fxSetTimeTable.use(usersApi.setTimeTable);
fxImportData.use(usersApi.importData);
fxGetKeys.use(usersApi.getKey);
fxGetEvents.use(usersApi.getEvents);

$isGroupOpen.reset([changedGroupVisibility, signout]);

$timeData
  .on(fxGetTimeTable.done, (_, { result }) => {
    const { timetable } = result;

    if (!timetable) {
      return {
        days_on: '',
        days_off: '',
        first_work_day: '',
        starts_at: '',
        ends_at: '',
        break_starts_at: '',
        break_ends_at: '',
        vacation_first_day: '',
        vacation_last_day: '',
        timezone: '',
      };
    }

    const date = new Date();
    const breakStart = timetable.break.starts_at.split(':');
    const breakEnd = timetable.break.ends_at.split(':');

    const break_starts_at = new Date(date.setHours(breakStart[0], breakStart[1], 0));
    const break_ends_at = new Date(date.setHours(breakEnd[0], breakEnd[1], 0));

    return {
      days_on: timetable.days_on,
      days_off: timetable.days_off,
      first_work_day: timetable.first_work_day,
      starts_at: timetable.starts_at,
      ends_at: timetable.ends_at,
      break_starts_at,
      break_ends_at,
      vacation_first_day: timetable.vacation.first_day,
      vacation_last_day: timetable.vacation.last_day,
      timezone: timetable.timezone,
    };
  })
  .reset(signout);

$eventsFilters.on(eventsFiltersSubmitted, (_, data) => data).reset($opened);

$userIdFromRoute.on([pageMounted, needUser], (_, { userId }) => +userId);

sample({
  clock: archiveUser,
  source: $opened,
  fn: ({ id }) => id,
  target: fxArchiveUser,
});

forward({
  from: exportList,
  to: attach({
    effect: fxExportList,
    source: { table: $tableParams, pageSize: $pageSize, filters: $filters },
    mapParams: (_, { table, pageSize, filters }) =>
      formatPayload(table, pageSize, filters),
  }),
});

forward({
  from: pageMounted,
  to: [fxGetRoles, fxGetContractingCompanies, fxGetComplexList],
});

guard({
  clock: [
    pageMounted,
    $tableParams,
    $filters,
    fxImportData.done,
    fxUserUpdate.done,
    $settingsInitialized,
    fxUserCreate.done,
    fxUserUpdate.done,
  ],
  filter: $settingsInitialized,
  target: attach({
    effect: fxGetList,
    source: { table: $tableParams, pageSize: $pageSize, filters: $filters },
    mapParams: (_, { table, pageSize, filters }) =>
      formatPayload(table, pageSize, filters),
  }),
});

const formatPayload = (table, pageSize, filters) => {
  const { page, search } = table;

  const complexes = filters.complexes.map((complex) => complex.id);
  const buildings = filters.buildings.map((building) => building.id);
  const contracting_companies = filters.contracting_companies.map(
    (company) => company.id
  );
  const roles = filters.roles.map((role) => role.id);
  const archived = Number(filters.archived);
  const show_management_company_employees = Number(
    filters.show_management_company_employees
  );
  const show_contracting_company_employees = Number(
    filters.show_contracting_company_employees
  );

  return {
    page,
    per_page: pageSize,
    search,
    complexes,
    buildings,
    contracting_companies,
    roles,
    show_management_company_employees,
    show_contracting_company_employees,
    archived,
  };
};

$rawUsersData
  .on(fxGetList.done, (_, { result }) => ({
    ...result,
    bms_employees: result.bms_employees.map((item) => ({
      ...item,
      keys: null,
      events: null,
    })),
  }))
  .on(fxGetKeys.done, (state, { params, result }) => ({
    ...state,
    bms_employees: state.bms_employees.map((item) => {
      if (item.user.id === params.user_id) return { ...item, keys: result.key };
      return item;
    }),
  }))
  .on(fxGetEvents.done, (state, { params, result }) => ({
    ...state,
    bms_employees: state.bms_employees.map((item) => {
      if (item.user.id === params.userdata_id) {
        return {
          ...item,
          events: result.events ? result.events : [],
        };
      }

      return item;
    }),
  }))
  .reset(signout);

$complexList
  .on(fxGetComplexList.done, (_, { result }) => {
    const { items } = result;

    if (!items) {
      return [];
    }

    return items.reduce((acc, value) => {
      const newValue = { [value.id]: value.title };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset([pageUnmounted, signout]);

$userRole
  .on(fxGetRoles.done, (_, { result }) => {
    if (result?.length <= 0) {
      return [];
    }

    return result.reduce((acc, value) => {
      const newValue = { [value.id]: value.title };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset(signout);

$contractingCompanies
  .on(fxGetContractingCompanies.done, (_, { result }) => {
    const { enterprises } = result;

    if (enterprises?.length <= 0) {
      return [];
    }

    return enterprises.reduce((acc, value) => {
      const newValue = { [value.id]: value.title };

      return { ...acc, ...newValue };
    }, {});
  })
  .reset(signout);

$isLoading.reset(pageUnmounted);
$error.on(gotError, (_, { error }) => error).reset([pageUnmounted, signout]);
$isErrorDialogOpen.on(gotError, () => true).reset([pageUnmounted, signout]);

forward({
  from: $isGettingSettingsFromStorage,
  to: $isLoading,
});

sample({
  source: [
    fxUserCreate.pending,
    fxUserUpdate.pending,
    fxArchiveUser.pending,
    fxGetList.pending,
    fxGetUser.pending,
    fxGetContractingCompanies.pending,
    fxGetComplexList.pending,
    fxExportList.pending,
    fxDownloadTemplate.pending,
    fxGetUserById.pending,
    fxGetTimeTable.pending,
    fxSetTimeTable.pending,
    fxGetKeys.pending,
    fxGetEvents.pending,
    fxValidateFile.pending,
    fxImportData.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

forward({ from: validateFile, to: fxValidateFile });

sample({
  source: fxExportList.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `users-list-${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});

forward({ from: downloadTemplate, to: fxDownloadTemplate });

guard({
  clock: fxUserUpdate.done,
  source: $filters,
  filter: ({ archived }) => Boolean(archived),
  target: changedDetailVisibility.prepend(() => false),
});

sample({
  source: fxDownloadTemplate.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(URL.createObjectURL(blob), 'template.xlsx');
  },
});

guard({
  clock: $opened,
  filter: ({ keys }) => keys == null,
  target: getKeys,
});

sample({
  source: { opened: $opened, tab: $currentTab },
  clock: getKeys,
  filter: ({ opened: { id }, tab }) => id && tab === 'info',
  fn: ({ opened: { id } }) => id,
  target: fxGetKeys.prepend((id) => ({
    type: 'universal',
    use_bms: 1,
    user_id: id,
  })),
});

guard({
  clock: $opened,
  filter: ({ events }) => events === null,
  target: getEvents,
});

sample({
  source: [$opened, $eventsFilters, $currentTab],
  clock: [getEvents, eventsFiltersSubmitted, $currentTab],
  filter: (data) => data[0]?.id && data[2] === 'eventLog',
  fn: (data) => ({
    opened: data[0],
    eventsFilters: data[1],
  }),
  target: fxGetEvents.prepend(({ opened, eventsFilters }) => ({
    userdata_id: opened.id,
    ...eventsFilters,
  })),
});
