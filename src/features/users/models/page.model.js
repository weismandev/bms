import { createGate } from 'effector-react';
import { createEffect, createEvent, createStore, restore } from 'effector';

import { $pathname } from '../../common';

const PageGate = createGate();

const pageMounted = PageGate.open;
const pageUnmounted = PageGate.close;

const changedErrorDialogVisibility = createEvent();
const exportList = createEvent();
const downloadTemplate = createEvent();
const validateFile = createEvent();
const archiveUser = createEvent();
const importData = createEvent();
const changedGroupVisibility = createEvent();
const getKeys = createEvent();
const getEvents = createEvent();
const eventsFiltersSubmitted = createEvent();
// Для вызова с других страниц
const needUser = createEvent();

const fxGetRoles = createEffect();
const fxGetList = createEffect();
const fxGetUserById = createEffect();
const fxGetComplexList = createEffect();
const fxGetContractingCompanies = createEffect();
const fxUserCreate = createEffect();
const fxUserUpdate = createEffect();
const fxGetTimeTable = createEffect();
const fxSetTimeTable = createEffect();
const fxArchiveUser = createEffect();
const fxExportList = createEffect();
const fxDownloadTemplate = createEffect();
const fxValidateFile = createEffect();
const fxImportData = createEffect();
const fxGetKeys = createEffect();
const fxGetEvents = createEffect();

const $userIdFromRoute = createStore(null);
const $rawUsersData = createStore({});
const $normalized = $rawUsersData.map(({ bms_employees }) => {
  if (!bms_employees) {
    return {};
  }

  return bms_employees.reduce(
    (acc, value) => ({
      ...acc,
      [value.user.id]: value,
    }),
    {}
  );
});
const $eventsFilters = createStore({
  date_start: new Date().toISOString().split('T')[0],
  date_end: new Date().toISOString().split('T')[0],
});

const $complexList = createStore(null);
const $userRole = createStore(null);
const $contractingCompanies = createStore(null);
const $timeData = createStore(null);
const $isLoading = createStore(false);
const $error = createStore(null);
const $isErrorDialogOpen = restore(changedErrorDialogVisibility, false);
const $isGroupOpen = restore(changedGroupVisibility, false);
const $path = $pathname.map((pathname) => pathname.split('/')[1]);
const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export {
  PageGate,
  pageMounted,
  pageUnmounted,
  fxGetRoles,
  fxGetList,
  fxUserCreate,
  fxUserUpdate,
  fxArchiveUser,
  $normalized,
  $isLoading,
  $error,
  $isErrorDialogOpen,
  changedErrorDialogVisibility,
  $userIdFromRoute,
  $rawUsersData,
  $userRole,
  fxGetContractingCompanies,
  $contractingCompanies,
  fxGetComplexList,
  $complexList,
  fxExportList,
  exportList,
  fxDownloadTemplate,
  downloadTemplate,
  validateFile,
  fxValidateFile,
  fxGetUserById,
  fxGetTimeTable,
  $timeData,
  fxSetTimeTable,
  archiveUser,
  importData,
  fxImportData,
  changedGroupVisibility,
  $isGroupOpen,
  fxGetKeys,
  getKeys,
  getEvents,
  fxGetEvents,
  eventsFiltersSubmitted,
  $eventsFilters,
  needUser,
  $path,
  $entityId,
};
