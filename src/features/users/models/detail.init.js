import { sample, forward, guard, merge } from 'effector';
import { delay } from 'patronum';
import { signout, $pathname, history } from '@features/common';
import i18n from '@shared/config/i18n';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import {
  openViaUrl,
  $opened,
  entityApi,
  $currentTab,
  addClicked,
  $mode,
  changeTab,
  detailSubmitted,
  changedDetailVisibility,
  $isDetailOpen,
} from './detail.model';
import { filtersSubmitted } from './filter.model';
import {
  $normalized,
  fxUserCreate,
  fxUserUpdate,
  fxGetUserById,
  fxGetTimeTable,
  fxSetTimeTable,
  $timeData,
  $userIdFromRoute,
  pageUnmounted,
  $entityId,
  $path,
  fxArchiveUser,
  fxGetList,
} from './page.model';

const { t } = i18n;

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetUserById.prepend(({ entityId }) => entityId),
});

const delayedRedirect = delay({ source: fxGetUserById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetUserById.done, () => true)
  .on([fxGetUserById.fail, fxArchiveUser.done], () => false)
  .reset(pageUnmounted);

sample({
  clock: fxUserCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        bms_employee: {
          user: { id },
        },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

sample({
  clock: fxArchiveUser.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        bms_employee: {
          user: { id },
        },
      },
    }
  ) => {
    if (id) {
      history.push(`../${path}`);
    }
  },
});

guard({
  source: $userIdFromRoute,
  // clock: $normalized,
  filter: (source) => Boolean(source),
  target: openViaUrl,
});

forward({ from: openViaUrl, to: fxGetUserById });

forward({
  from: filtersSubmitted,
  to: changedDetailVisibility.prepend(() => false),
});

$mode.on([fxUserUpdate.done, fxUserCreate.done, fxSetTimeTable.done], () => 'view');

sample({
  clock: fxUserCreate.done,
  fn: ({ result }) => result.bms_employee.user.id,
  target: fxGetUserById,
});

sample({
  clock: fxUserUpdate.done,
  fn: ({ result }) => result.bms_employee.user.id,
  target: fxGetUserById,
});

$opened
  .on(fxGetUserById.doneData, (_, userData) => formatOpenedFromRoute(userData))
  .reset(pageUnmounted);

sample({
  source: { normalized: $normalized, opened: $opened },
  filter: ({ normalized, opened }) => {
    if (Object.values(normalized).length === 0) {
      return false;
    }

    const newOpened = { ...opened };

    const { keys, events } = normalized[newOpened?.id];

    if (keys) {
      newOpened.keys = keys;
    }

    if (events) {
      newOpened.events = events;
    }

    return JSON.stringify(newOpened) !== JSON.stringify(opened);
  },
  fn: ({ normalized, opened }) => {
    const newOpened = { ...opened };

    const { keys, events } = normalized[newOpened?.id];

    if (keys) {
      newOpened.keys = keys;
    }

    if (events) {
      newOpened.events = events;
    }

    return newOpened;
  },
  target: $opened,
});

const formatOpenedFromRoute = ({
  bms_employee,
  is_allowed_change_automatic_ticket_assignment,
  is_exists_works_with_routing,
}) => ({
  id: bms_employee.user.id,
  avatar: bms_employee.user.avatar,
  surname: bms_employee.user?.surname,
  name: bms_employee.user?.name,
  patronymic: bms_employee.user?.patronymic,
  position: bms_employee?.position || '',
  complexes: bms_employee.complexes,
  buildings: bms_employee.buildings,
  role: bms_employee?.role?.id,
  competences: bms_employee.competences,
  inn: bms_employee.inn,
  phone: String(bms_employee.user.phone),
  email: bms_employee.user.email,
  ticket_notify: bms_employee.ticket_notify,
  contracting_company: bms_employee.contracting_company?.id,
  is_user_contracting_company: Boolean(bms_employee.contracting_company?.id),
  keys: bms_employee.keys,
  events: bms_employee.events,
  qualifications: bms_employee?.qualifications || [],
  is_dispatcher_role: bms_employee.is_dispatcher_role,
  is_allowed_change_automatic_ticket_assignment,
  is_exists_works_with_routing,
  is_allowed_automatic_ticket_assignment:
    bms_employee.is_allowed_automatic_ticket_assignment,
});

guard({
  source: sample({
    source: { tab: $currentTab, opened: $opened, timeData: $timeData },
    clock: detailSubmitted,
    fn: ({ tab, opened }, payload) => ({ tab, opened, payload }),
  }),
  filter: ({ tab, opened }) => tab === 'schedule' && Boolean(opened.id),
  target: fxSetTimeTable.prepend(formatTimeTablePayload),
});

$currentTab.reset([signout, addClicked]);

guard({
  source: merge([
    sample($opened, changeTab, (opened, tab) => ({
      id: opened.id,
      tab,
    })),
    sample($currentTab, openViaUrl, (tab, id) => ({ tab, id })),
  ]),
  filter: ({ id, tab }) => Boolean(id) && tab === 'schedule',
  target: fxGetTimeTable.prepend(({ id }) => id),
});

sample({
  clock: entityApi.update,
  source: $currentTab,
  filter: (tab) => tab === 'info',
  fn: (_, data) => {
    const payload = formatPayload({ payload: data });

    payload.is_allowed_automatic_ticket_assignment = Number(
      Boolean(data.is_allowed_automatic_ticket_assignment)
    );

    return payload;
  },
  target: fxUserUpdate,
});

guard({
  source: sample($currentTab, entityApi.create, (tab, payload) => ({
    tab,
    payload,
  })),
  filter: ({ tab }) => tab === 'info',
  target: fxUserCreate.prepend(formatPayload),
});

function formatTimeTablePayload(data) {
  const user = { id: data.opened.id };

  const {
    timezone,
    days_on,
    days_off,
    first_work_day,
    starts_at,
    ends_at,
    break_starts_at,
    break_ends_at,
    vacation_first_day,
    vacation_last_day,
  } = data.payload;

  const breakTimes = {};

  if (break_starts_at) {
    const hours = break_starts_at.getHours();
    const minutes = break_starts_at.getMinutes();

    breakTimes.starts_at = `${hours.toString().length === 1 ? `0${hours}` : hours}:${
      minutes.toString().length === 1 ? `0${minutes}` : minutes
    }`;
  }

  if (break_ends_at) {
    const hours = break_ends_at.getHours();
    const minutes = break_ends_at.getMinutes();

    breakTimes.ends_at = `${hours.toString().length === 1 ? `0${hours}` : hours}:${
      minutes.toString().length === 1 ? `0${minutes}` : minutes
    }`;
  }

  const vacation = {};

  if (vacation_first_day) vacation.first_day = vacation_first_day;
  if (vacation_last_day) vacation.last_day = vacation_last_day;

  const timetable = {
    timezone: timezone?.id || timezone,
    days_on,
    days_off,
    first_work_day,
    starts_at: typeof ends_at === 'string' && starts_at.slice(0, 5),
    ends_at: typeof ends_at === 'string' && ends_at.slice(0, 5),
    break: breakTimes,
    vacation,
  };

  return { user, timetable };
}

function formatPayload(data) {
  const {
    id,
    avatar,
    buildings,
    competences,
    email,
    surname,
    name,
    patronymic,
    inn,
    phone,
    position,
    role,
    ticket_notify,
    contracting_company,
    qualifications,
  } = data.payload;

  const formatBuildings =
    buildings && buildings.map((item) => (typeof item === 'object' ? item.id : item));

  const formatСompetences =
    competences &&
    competences?.map((item) => (typeof item === 'object' ? item.id : item));

  const formattedPayload = {};

  const user = {};

  if (id) user.id = id;
  if (surname) user.surname = surname;
  if (name) user.name = name;
  if (patronymic) user.patronymic = patronymic;
  if (email) user.email = email;
  if (phone) user.phone = String(phone.replace(/[ \+]/g, ''));
  if (avatar?.file) user.avatar = avatar;

  const roleId = {};

  if (role) roleId.id = role?.id || role;

  const contractingCompany = {};

  if (contracting_company) {
    contractingCompany.id = contracting_company?.id || contracting_company;
  }

  if (position) formattedPayload.position = position;
  if (inn) formattedPayload.inn = inn;

  if (qualifications.length > 0) {
    formattedPayload.qualifications = qualifications.map((item) =>
      typeof item === 'object' ? item.id : item
    );
  }

  formattedPayload.user = user;
  formattedPayload.role = roleId;
  formattedPayload.buildings = formatBuildings;
  formattedPayload.competences = formatСompetences;
  formattedPayload.contracting_company = contractingCompany;
  formattedPayload.ticket_notify = Number(ticket_notify);
  formattedPayload.is_dispatcher_role = Number(data.payload.is_dispatcher_role);

  return formattedPayload;
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
