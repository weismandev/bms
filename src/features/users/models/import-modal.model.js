import { createEvent, createStore } from 'effector';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const changeVisibilityModal = createEvent();

export const $isOpenModal = createStore(false);
export const $validationResult = createStore(null);

export const $errors = $validationResult.map((item) =>
  item?.validated && Array.isArray(item?.validated?.errors)
    ? formatErorrs(item.validated.errors)
    : []
);

export const tableColumnExtensions = [
  { columnName: 'cell', width: 100, align: 'center' },
];

export const columns = [
  { name: 'cell', title: t('Cell') },
  { name: 'value', title: t('Value') },
  { name: 'message', title: t('ErrorText') },
];

const formatErorrs = (errors) =>
  errors.map((error) => ({
    cell: error.cell || t('Unknown'),
    value: error.value || t('Unknown'),
    message: error.message || t('Unknown'),
  }));
