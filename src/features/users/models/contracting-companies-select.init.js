import { signout } from '@features/common';
import { usersApi } from '../api';
import {
  fxGetList,
  $data,
  $error,
  $isLoading,
} from './contracting-companies-select.model';

fxGetList.use(usersApi.getContractingCompanies);

$data
  .on(fxGetList.done, (_, { result }) => {
    const { enterprises } = result;

    if (!Array.isArray(enterprises)) {
      return [];
    }

    return enterprises;
  })
  .reset(signout);

$error.on(fxGetList.fail, (_, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (_, result) => result).reset(signout);
