import { restore, combine } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';
import { formatPhone } from '@tools/formatPhone';
import {
  $rawUsersData,
  $userRole,
  $contractingCompanies,
  $complexList,
} from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
  { name: 'position', title: t('Label.Position') },
  { name: 'buildings', title: t('AnObject') },
  { name: 'contracting_company', title: t('EnterpriseContractor') },
  { name: 'role', title: t('Role') },
];

const columnsWidth = [
  { columnName: 'fullname', width: 300 },
  { columnName: 'contracting_company', width: 280 },
];

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, {
  widths: columnsWidth,
});

export const $pageSize = restore(pageSizeChanged, 25);
export const $rowCount = $rawUsersData.map(({ meta }) => meta?.total || 0);

export const $tableData = combine(
  $rawUsersData,
  $userRole,
  $contractingCompanies,
  $complexList,
  ({ bms_employees }, userRole, contractingCompanies, complexList) => {
    if (!bms_employees) {
      return [];
    }

    return bms_employees.map((employee) => ({
      id: employee.user.id,
      fullname: employee.user.fullname || t('isNotDefinedit'),
      phone: employee.user.phone ? `+${employee.user.phone}` : t('isNotDefined'),
      position: employee.position || t('isNotDefinedshe'),
      buildings: formatBuildings(employee.complexes, complexList),
      contracting_company:
        employee?.contracting_company && contractingCompanies
          ? contractingCompanies[employee.contracting_company?.id]
          : '',
      role: userRole[employee?.role?.id] || t('isNotDefinedshe'),
    }));
  }
);

const formatBuildings = (buildings, complexList) => {
  const formatedBuildings = buildings.reduce((acc, value) => {
    const complexTitle = complexList[value];

    if (!complexTitle) {
      return acc;
    }

    return [...acc, complexTitle];
  }, []);

  if (formatedBuildings.length > 0) {
    return formatedBuildings;
  }

  return t('isNotDefined');
};
