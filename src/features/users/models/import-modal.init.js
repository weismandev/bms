import { sample } from 'effector';
import { signout } from '@features/common';
import {
  $validationResult,
  changeVisibilityModal,
  $isOpenModal,
} from './import-modal.model';
import { fxValidateFile, fxImportData, importData } from './page.model';

$validationResult.on(fxValidateFile.done, (_, { result }) => result).reset(signout);

$isOpenModal
  .on(changeVisibilityModal, (_, visibility) => visibility)
  .on(fxValidateFile.done, () => true)
  .reset([signout, fxImportData.done, fxImportData.fail]);

sample({
  clock: importData,
  source: $validationResult,
  fn: ({ task }) => task.id,
  target: fxImportData,
});
