import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  buildings: [],
  complexes: [],
  contracting_companies: [],
  roles: [],
  show_management_company_employees: true,
  show_contracting_company_employees: true,
  archived: false,
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
