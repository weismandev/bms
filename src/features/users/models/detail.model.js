import { createEvent, restore } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '@tools/factories';

const { t } = i18n;

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'vehicles',
    onCreateIsDisabled: true,
    label: t('transport'),
  },
  {
    value: 'schedule',
    onCreateIsDisabled: true,
    label: t('Schedule'),
  },
  {
    value: 'eventLog',
    onCreateIsDisabled: true,
    label: t('Events'),
  },
];

export const newEntity = {
  id: null,
  avatar: '',
  buildings: '',
  competences: '',
  complexes: '',
  email: '',
  surname: '',
  name: '',
  patronymic: '',
  inn: '',
  phone: '',
  position: '',
  role: '',
  ticket_notify: true,
  contracting_company: '',
  keys: null,
  events: null,
  qualifications: [],
  is_dispatcher_role: false
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  detailClosed,
  entityApi,
  openViaUrl,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const changeTab = createEvent();
export const $currentTab = restore(changeTab, 'info');
