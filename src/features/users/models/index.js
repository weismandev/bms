import './page.init';
import './detail.init';
import './contracting-companies-select.init';
import './import-modal.init';

export * from './page.model';
