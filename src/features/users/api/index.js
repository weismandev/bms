import { api } from '@api/api2';

const getRoles = () => api.v1('post', 'role/list', {});
const getList = (payload) => api.v1('get', 'bms/employees/list', payload);
const getUserById = (payload) =>
  api.v1('get', 'bms/employees/item', { user: { id: payload } });
const getContractingCompanies = () =>
  api.v1('get', 'enterprises/get-list', { per_page: 500, type: 'contractor' });
const getComplexList = () => api.v1('get', 'complex/list');
const userCreate = (payload = {}) => api.v1('post', 'bms/employees/create', payload);
const userUpdate = (payload = {}) => api.v1('post', 'bms/employees/update', payload);
const getTimeTable = (payload = {}) =>
  api.v1('get', 'bms/employees/timetable', { user: { id: payload } });
const setTimeTable = (payload = {}) =>
  api.v1('post', 'bms/employees/timetable/set', payload);
const archiveUser = (id) => api.v1('post', 'bms/employees/archive', { user: { id } });
const exportList = (payload) =>
  api.v1(
    'request',
    'bms/employees/list',
    { ...payload, limit: 1000000, export: 1 },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );
const downloadTemplate = () =>
  api.v1(
    'request',
    'bms/employees/import/template',
    {},
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'post',
    }
  );
const validateFile = (file) =>
  api.v1('post', 'bms/employees/import/validate', { import: file });
const importData = (id) => api.v1('post', 'bms/employees/import', { task: { id } });
const getKey = (payload) => api.no_vers('post', 'acms/get-key', payload);
const getEvents = (payload) =>
  api.no_vers('get', 'information/events-v2', { ...payload, limit: 100 });

export const usersApi = {
  getRoles,
  getList,
  getComplexList,
  userCreate,
  userUpdate,
  archiveUser,
  getContractingCompanies,
  exportList,
  downloadTemplate,
  validateFile,
  getUserById,
  getTimeTable,
  setTimeTable,
  importData,
  getKey,
  getEvents,
};
