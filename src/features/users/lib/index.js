import { isBefore } from 'date-fns';

export function startTimeAfterFinishTimeWork() {
  if (!this.parent.starts_at && !this.parent.ends_at) {
    return this.createError();
  }

  if (this.parent.starts_at >= this.parent.ends_at) {
    return false;
  }

  return true;
}

export function startTimeAfterFinishTimeBreak() {
  if (!this.parent.break_starts_at && !this.parent.break_ends_at) {
    return true;
  }

  const startTime = new Date(this.parent.break_starts_at) / 1000;
  const endTime = new Date(this.parent.break_ends_at) / 1000;

  if (startTime > endTime) {
    return this.createError();
  }

  return true;
}

export function startBreakBetweenWorkTime() {
  if (!this.parent.break_starts_at) {
    return true;
  }

  if (!this.parent.starts_at && !this.parent.ends_at) {
    return true;
  }

  const date = new Date();

  const start = this.parent.starts_at.split(':');
  const end = this.parent.ends_at.split(':');

  const starts_at = new Date(date.setHours(start[0], start[1], 0));
  const ends_at = new Date(date.setHours(end[0], end[1], 0));

  const startTime = new Date(starts_at) / 1000;
  const endTime = new Date(ends_at) / 1000;
  const startTimeBreak = new Date(this.parent.break_starts_at) / 1000;

  if (startTimeBreak >= startTime && startTimeBreak <= endTime) {
    return true;
  }

  return this.createError();
}

export function endBreakBetweenWorkTime() {
  if (!this.parent.break_ends_at) {
    return true;
  }

  if (!this.parent.starts_at && !this.parent.ends_at) {
    return true;
  }

  const date = new Date();

  const start = this.parent.starts_at.split(':');
  const end = this.parent.ends_at.split(':');

  const starts_at = new Date(date.setHours(start[0], start[1], 0));
  const ends_at = new Date(date.setHours(end[0], end[1], 0));

  const startTime = new Date(starts_at) / 1000;
  const endTime = new Date(ends_at) / 1000;
  const endTimeBreak = new Date(this.parent.break_ends_at) / 1000;

  if (endTimeBreak >= startTime && endTimeBreak <= endTime) {
    return true;
  }

  return this.createError();
}

export function firstDateAfterLastVacation() {
  if (this.parent.vacation_first_day && this.parent.vacation_last_day) {
    const firstDay = new Date(this.parent.vacation_first_day);
    const lastDay = new Date(this.parent.vacation_last_day);

    if (!isBefore(firstDay, lastDay)) {
      return false;
    }

    return true;
  }

  return true;
}

export function firstVacationDate() {
  if (this.parent.vacation_first_day) {
    const firstWorkDay = new Date(this.parent.first_work_day);
    const firstVacationDay = new Date(this.parent.vacation_first_day);

    if (!isBefore(firstWorkDay, firstVacationDay)) {
      return this.createError();
    }

    return true;
  }

  return true;
}
