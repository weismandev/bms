import { useEffect } from 'react';
import { Avatar } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { useFileReader, EditButton } from '@ui/index';

const useStyles = makeStyles(() => ({
  input: {
    display: 'none',
  },
  avatar: {
    width: 'clamp(50px, 17vw, 190px)',
    height: 'clamp(50px, 17vw, 190px)',
    background: '#f7f7fc',
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    top: -50,
    left: 160,
    '@media (max-width: 1085px)': {
      left: 145,
    },
    '@media (max-width: 940px)': {
      left: 135,
    },
    '@media (max-width: 900px)': {
      left: 125,
    },
    '@media (max-width: 830px)': {
      left: 115,
    },
  },
  avatarContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

const AvatarControl = ({
  name = 'avatar',
  value,
  mode,
  onChange,
  encoding = 'base64',
  resetLoadedFileOnOpen,
}) => {
  const classes = useStyles();

  const { read, readResult, reset } = useFileReader({
    encoding,
    isMultiple: false,
  });

  const isEditMode = mode === 'edit';

  useEffect(() => {
    if (readResult) {
      onChange(readResult);
    }
  }, [readResult]);

  useEffect(() => {
    reset();
  }, [resetLoadedFileOnOpen]);

  const id = Math.random().toString(36).slice(2, 11);

  const innerValue =
    (Boolean(readResult) && Boolean(readResult.preview) && readResult.preview) || value;

  return (
    <>
      <div className={classes.avatarContainer}>
        <div style={{ position: 'relative' }}>
          <Avatar alt={name} src={innerValue} className={classes.avatar} />
          {isEditMode && (
            <label className={classes.label} htmlFor={id}>
              <EditButton component="span" />
            </label>
          )}
        </div>
      </div>
      <input
        accept="image/jpeg,image/png"
        type="file"
        className={classes.input}
        id={id}
        onChange={read}
        name={name}
      />
    </>
  );
};

export { AvatarControl };
