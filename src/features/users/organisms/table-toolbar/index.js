import { useState } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { useMediaQuery, ClickAwayListener, Tooltip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import {
  AddButton,
  Toolbar,
  AdaptiveSearch,
  Greedy,
  DownloadTemplateButton,
  ImportButton,
  ExportButton,
  FilterButton,
  FoundInfo,
  FileControl,
  MoreButton,
  GroupButton,
} from '@ui/index';
import { addClicked, $mode } from '../../models/detail.model';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter.model';
import {
  exportList,
  downloadTemplate,
  validateFile,
  $isGroupOpen,
  changedGroupVisibility,
} from '../../models/page.model';
import {
  searchChanged,
  $search,
  $rowCount,
  columns,
  $tableData,
  $hiddenColumnNames,
} from '../../models/table.model';

const useStyles = makeStyles({
  margin: {
    margin: '0 10px',
  },
  tooltipContainer: {
    display: 'flex',
  },
});

const useTooltipClasses = makeStyles(() => ({
  tooltip: {
    backgroundColor: '#fff',
    boxShadow: '0px 5px 15px #DCDCDC',
    borderRadius: 25,
  },
}));

const $stores = combine({
  searchValue: $search,
  mode: $mode,
  rowCount: $rowCount,
  isFilterOpen: $isFilterOpen,
  tableData: $tableData,
  hiddenColumnNames: $hiddenColumnNames,
  isGroupOpen: $isGroupOpen,
});

export const TableToolbar = () => {
  const classes = useStyles();
  const tooltipClasses = useTooltipClasses();

  const [tooltipOpen, setTooltip] = useState(false);
  const handleTooltipClose = () => setTooltip(false);
  const handleTooltipOpen = () => setTooltip(true);

  const {
    searchValue,
    mode,
    rowCount,
    isFilterOpen,
    tableData,
    hiddenColumnNames,
    isGroupOpen,
  } = useStore($stores);

  const isEditMode = mode === 'edit';

  const isAnySideOpen = isFilterOpen || isGroupOpen || isEditMode;
  const isScreenLess1440 = useMediaQuery('(max-width:1440px)');

  const rightToolbar = (
    <>
      <ExportButton {...{ columns, tableData, hiddenColumnNames }} onClick={exportList} />
      <FileControl
        mode="edit"
        name="file"
        encoding="file"
        onChange={(value) => {
          if (value && value.file) {
            validateFile(value.file);
          }
        }}
        renderPreview={() => null}
        renderButton={() => <ImportButton component="span" className={classes.margin} />}
      />
      <DownloadTemplateButton onClick={downloadTemplate} />
    </>
  );

  const rightTooltip = (
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <div>
        <Tooltip
          placement="bottom"
          classes={tooltipClasses}
          className={classes.margin}
          title={<div className={classes.tooltipContainer}>{rightToolbar}</div>}
          PopperProps={{
            disablePortal: true,
          }}
          onClose={handleTooltipClose}
          open={tooltipOpen}
          disableFocusListener
          disableHoverListener
          disableTouchListener
        >
          <MoreButton onClick={handleTooltipOpen} disabled={tooltipOpen} />
        </Tooltip>
      </div>
    </ClickAwayListener>
  );

  const getResponsiveOrFullElement = (fullElement, responsiveElement) => {
    if (isScreenLess1440 || isAnySideOpen) {
      return responsiveElement;
    }
    return fullElement;
  };

  return (
    <Toolbar>
      <FilterButton
        className={classes.margin}
        disabled={isFilterOpen}
        onClick={() => changedFilterVisibility(true)}
      />
      <GroupButton
        style={{ display: 'none' }}
        disabled={isGroupOpen}
        onClick={() => changedGroupVisibility(true)}
        className={classes.margin}
      />
      <AdaptiveSearch
        {...{ totalCount: rowCount, searchValue, searchChanged, isAnySideOpen }}
      />
      <Greedy />

      {getResponsiveOrFullElement(rightToolbar, rightTooltip)}
      <AddButton
        onClick={addClicked}
        disabled={mode === 'edit'}
        className={classes.margin}
      />
    </Toolbar>
  );
};
