import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { RolesSelectField } from '@features/roles-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  SwitchField,
} from '@ui/index';
import {
  defaultFilters,
  changedFilterVisibility,
  filtersSubmitted,
  $filters,
} from '../../models/filter.model';
import { ContractingCompaniesSelectField } from '../contracting-companies-select';

const { t } = i18n;

const Filter = () => {
  const filters = useStore($filters);

  return (
    <Wrapper style={{ height: '100%', paddingTop: 10 }}>
      <CustomScrollbar autoHide style={{ height: 'calc(100% - 15px)' }}>
        <FilterToolbar
          style={{ padding: '14px 24px 24px' }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted(defaultFilters)}
          enableReinitialize
          render={({ values }) => (
            <Form style={{ padding: 24, paddingTop: 0 }}>
              <Field
                name="complexes"
                component={ComplexSelectField}
                label={t('AnObject')}
                placeholder={t('selectObject')}
                isMulti
              />
              <Field
                name="buildings"
                component={HouseSelectField}
                complex={Array.isArray(values.complexes) ? values.complexes : []}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />
              <Field
                name="contracting_companies"
                component={ContractingCompaniesSelectField}
                label={t('EnterpriseContractor')}
                placeholder={t('ChooseAnOrganization')}
                isMulti
              />
              <Field
                name="roles"
                component={RolesSelectField}
                label={t('Role')}
                placeholder={t('SelectRole')}
                isMulti
              />
              <Field
                name="show_management_company_employees"
                component={SwitchField}
                label={t('ShowMCEmployees')}
                labelPlacement="start"
              />
              <Field
                name="show_contracting_company_employees"
                component={SwitchField}
                label={t('ShowContractorEmployees')}
                labelPlacement="start"
              />
              <Field
                name="archived"
                component={SwitchField}
                label={t('ShowArchive')}
                labelPlacement="start"
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};

export { Filter };
