import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { VehiclesForm } from '@features/user-related-vehicles';
import { Wrapper, Tabs } from '@ui/index';
import {
  $mode,
  changedDetailVisibility,
  $opened,
  changeTab,
  $currentTab,
  tabs,
} from '../../models/detail.model';
import { $filters } from '../../models/filter.model';
import { EventsLog } from '../events-log';
import { InfoTabForm } from '../info-tab-form';
import { ScheduleForm } from '../schedule-form';

const $stores = combine({
  mode: $mode,
  opened: $opened,
  currentTab: $currentTab,
  filters: $filters,
});

const Detail = ({ closeHandler = null, external = false }) => {
  const { mode, opened, currentTab, filters } = useStore($stores);

  const isNew = !opened.id;

  return (
    <Wrapper style={{ height: '100%' }}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(_, tab) => changeTab(tab)}
        isNew={isNew}
        tabEl={<Tab />}
      />
      <div style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
        {currentTab === 'info' && (
          <InfoTabForm
            opened={opened}
            mode={mode}
            isNew={isNew}
            isArchived={filters.archived}
            closeHandler={closeHandler}
            external={external}
          />
        )}
        {currentTab === 'vehicles' && (
          <VehiclesForm
            userdata_id={opened.id}
            changeDetailVisibility={changedDetailVisibility}
            isUser
            isArchived={filters.archived}
            closeHandler={closeHandler}
            external={external}
          />
        )}
        {currentTab === 'schedule' && (
          <ScheduleForm
            isArchived={filters.archived}
            closeHandler={closeHandler}
            external={external}
          />
        )}
        {currentTab === 'eventLog' && (
          <EventsLog
            isArchived={filters.archived}
            closeHandler={closeHandler}
            external={external}
          />
        )}
      </div>
    </Wrapper>
  );
};

export { Detail };
