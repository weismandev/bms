import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Alert } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { CompetencesSelectField } from '@features/competences-select';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField, $data as $houses } from '@features/house-select';
import { QualificationsSelectField } from '@features/qualifications-select';
import { ResetAndNotifyPassword } from '@features/reset-password';
import { RolesSelectField } from '@features/roles-select';
import i18n from '@shared/config/i18n';
import { createEmailOrPhoneValidateFn } from '@tools/validations';
import { validateSomePhone } from '@ui/atoms/some-phone-masked-input';
import {
  DetailToolbar,
  InputField,
  SwitchField,
  CustomScrollbar,
  ArchiveButton,
  Divider,
  FormSectionHeader,
  SomePhoneMaskedInput,
  EmailMaskedInput,
} from '@ui/index';
import { $data as $companies } from '../../models/contracting-companies-select.model';
import {
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
} from '../../models/detail.model';
import { $filters } from '../../models/filter.model';
import { archiveUser } from '../../models/page.model';
import { AvatarControl } from '../avatar-control';
import { ContractingCompaniesSelectField } from '../contracting-companies-select';

const { t } = i18n;

const testEmailOrPhoneProvided = createEmailOrPhoneValidateFn({
  emailKey: 'email',
  phoneKey: 'phone',
});

const useStyles = makeStyles({
  twoItem: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1085px)': {
      flexDirection: 'column',
    },
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    width: '50%',
    paddingRight: 15,
    '@media (max-width: 1085px)': {
      width: '100%',
    },
    marginBottom: 5,
  },
  key_container: {
    display: 'grid',
    gap: 10,
    marginBottom: 20,
  },
  key_card: {
    display: 'grid',
    gap: 10,
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    gridTemplateColumns: 'auto auto',
    background: '#edf6ff',
    padding: '10px 15px',
    borderRadius: 16,
  },
  key_title: {
    fontWeight: 'bold',
  },
});

const InfoTabForm = (props) => {
  const { opened, mode, isNew, isArchived, closeHandler, external } = props;
  const filters = useStore($filters);
  const houses = useStore($houses);
  const companies = useStore($companies);

  function testActualBuildings() {
    const { id, buildings } = this.parent;

    if (!filters.archived || !id) {
      return true;
    }

    const findedBuildings = buildings
      .map((item) => houses.find((house) => house.id === item))
      .filter((item) => item);

    if (findedBuildings.length === buildings.length) {
      return true;
    }

    return false;
  }

  function testActualСontractingСompany() {
    const { id, contracting_company } = this.parent;

    if (!contracting_company) {
      return true;
    }

    if (!filters.archived || !id) {
      return true;
    }

    const findedCompany = companies.find((item) => item.id === contracting_company);

    if (findedCompany) {
      return true;
    }

    return false;
  }

  const validationSchema = Yup.object().shape({
    surname: Yup.string().required(t('thisIsRequiredField')),
    name: Yup.string().required(t('thisIsRequiredField')),
    patronymic: Yup.string(),
    email: Yup.string()
      .email(t('incorrectEmail'))
      .test(
        'email-or-phone-mandatory',
        t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
        testEmailOrPhoneProvided
      ),
    phone: Yup.string()
      .test(
        'email-or-phone-mandatory',
        t('AtLeastOneOfTheFieldsMailPhoneMustBeFilled'),
        testEmailOrPhoneProvided
      )
      .test('phone-length', t('invalidNumber'), validateSomePhone),
    inn: Yup.string()
      .nullable()
      .min(10, t('AtLeast10Characters'))
      .max(12, t('AtLeast12Characters'))
      .matches(/^\d{10,12}$/, {
        message: t('UseNumbersFrom0to9'),
      }),
    position: Yup.string().required(t('thisIsRequiredField')),
    role: Yup.mixed().required(t('thisIsRequiredField')),
    buildings: Yup.mixed().test(
      'is-actual-buildings',
      t('ChooseActualValues'),
      testActualBuildings
    ),
    contracting_company: Yup.mixed().test(
      'is-actual-contracting_company',
      t('ChooseActualValues'),
      testActualСontractingСompany
    ),
  });

  const classes = useStyles();

  const calcHidden = () => {
    if (external) return { edit: true, send: true, delete: true };
    if (isArchived) return { edit: true, send: true, delete: true };
    return { delete: true };
  };

  return (
    <Formik
      initialValues={opened}
      enableReinitialize
      onSubmit={detailSubmitted}
      validationSchema={validationSchema}
      render={({ values, resetForm }) => (
        <Form style={{ height: '100%', position: 'relative' }}>
          <DetailToolbar
            style={{ padding: 24 }}
            mode={mode}
            onEdit={() => changeMode('edit')}
            onClose={external ? closeHandler : () => changedDetailVisibility(false)}
            onCancel={() => {
              if (isNew) {
                changedDetailVisibility(false);
              } else {
                changeMode('view');
                resetForm();
              }
            }}
            hidden={calcHidden()}
          >
            {mode === 'view' && (
              <ArchiveButton
                onClick={isArchived ? () => changeMode('edit') : archiveUser}
                isArchived={isArchived}
                titleAccess={!isArchived && t('MoveToArchive')}
                disabled={mode === 'edit'}
                style={{
                  order: 10,
                  margin: '0 5px',
                  backgroundColor: '#EC7F00',
                }}
              />
            )}

            {!isNew && (
              <ResetAndNotifyPassword
                user_id={opened.id}
                disabled={isArchived}
                context="employee"
              />
            )}
          </DetailToolbar>
          <div
            style={{
              height: 'calc(100% - 82px)',
              padding: '0 6px 24px 24px',
            }}
          >
            <CustomScrollbar>
              <div style={{ paddingRight: 18 }}>
                <div className={classes.item}>
                  <Field
                    name="avatar"
                    render={({ field, form }) => (
                      <AvatarControl
                        resetLoadedFileOnOpen={opened}
                        encoding="base64"
                        mode={mode}
                        onChange={(value) => form.setFieldValue(field.name, value)}
                        value={field.value}
                      />
                    )}
                  />
                </div>
                <div className={classes.item}>
                  <Field
                    name="surname"
                    component={InputField}
                    label={t('Label.lastname')}
                    required
                    mode={mode}
                    divider={false}
                  />
                  <Divider />
                </div>
                <div className={classes.item}>
                  <Field
                    name="name"
                    component={InputField}
                    label={t('Label.name')}
                    required
                    mode={mode}
                    divider={false}
                  />
                  <Divider />
                </div>
                <div className={classes.item}>
                  <Field
                    name="patronymic"
                    component={InputField}
                    label={t('Label.patronymic')}
                    mode={mode}
                    divider={false}
                  />
                  <Divider />
                </div>
                <div className={classes.item}>
                  <Field
                    name="is_user_contracting_company"
                    component={SwitchField}
                    label={t('ContractorEmployee')}
                    labelPlacement="start"
                    disabled={mode !== 'edit'}
                    divider={false}
                  />
                  <Divider />
                </div>
                <div className={classes.item}>
                  <Field
                    name="is_dispatcher_role"
                    component={SwitchField}
                    label={t('DispatcherRightsInApplications')}
                    labelPlacement="start"
                    disabled={mode !== 'edit'}
                    divider={false}
                  />
                  <Divider />
                </div>
                <div className={classes.twoItem}>
                  <div className={classes.item}>
                    <Field
                      name="position"
                      component={InputField}
                      label={t('Label.Position')}
                      mode={mode}
                      required
                      divider={false}
                    />
                    <Divider />
                  </div>
                  <div className={classes.item}>
                    <Field
                      name="complexes"
                      component={ComplexSelectField}
                      label={t('objects')}
                      mode="view"
                      isMulti
                      placeholder={t('selectObjects')}
                      divider={false}
                    />
                    <Divider />
                  </div>
                </div>
                <div className={classes.twoItem}>
                  {values.is_user_contracting_company && (
                    <div className={classes.item}>
                      <Field
                        name="contracting_company"
                        component={ContractingCompaniesSelectField}
                        label={t('EnterpriseContractor')}
                        mode={mode}
                        placeholder={t('ChooseAnOrganization')}
                        divider={false}
                      />
                      <Divider />
                    </div>
                  )}
                  <div className={classes.item}>
                    <Field
                      name="buildings"
                      component={HouseSelectField}
                      label={t('Buildings')}
                      mode={mode}
                      isMulti
                      placeholder={t('selectBbuilding')}
                      divider={false}
                      params={{ all: true }}
                    />
                    <Divider />
                  </div>
                </div>
                <div className={classes.twoItem}>
                  <div className={classes.item}>
                    <Field
                      name="role"
                      component={RolesSelectField}
                      label={t('Role')}
                      placeholder={t('SelectRole')}
                      mode={mode}
                      divider={false}
                      required
                    />
                    <div
                      style={{
                        alignItems: 'flex-end',
                      }}
                    >
                      <Divider />
                    </div>
                  </div>
                  <div className={classes.item}>
                    <Field
                      name="competences"
                      component={CompetencesSelectField}
                      label={t('Competencies')}
                      mode={mode}
                      placeholder={t('SelectСompetencies')}
                      isMulti
                      divider={false}
                    />
                    <Divider />
                  </div>
                </div>
                <div className={classes.twoItem}>
                  <div className={classes.item}>
                    <Field
                      name="qualifications"
                      label={t('Qualifications')}
                      placeholder={t('SelectQualification')}
                      mode={mode}
                      component={QualificationsSelectField}
                      divider
                      isMulti
                      path="qualifications"
                    />
                  </div>
                </div>
                {values.id && (
                  <div className={classes.twoItem}>
                    <div className={classes.item}>
                      <Field
                        name="is_allowed_automatic_ticket_assignment"
                        label={t('AutomaticAssignmentOfRequests')}
                        mode={mode}
                        labelPlacement="start"
                        disabled={
                          values.is_allowed_change_automatic_ticket_assignment
                            ? mode !== 'edit'
                            : true
                        }
                        component={SwitchField}
                        divider={false}
                      />
                      {!values.is_exists_works_with_routing &&
                        values.is_allowed_automatic_ticket_assignment &&
                        mode === 'edit' && (
                          <Alert severity="info">
                            {t('UsersNotExistWorksWithRouting')}
                          </Alert>
                        )}
                      <Divider />
                    </div>
                  </div>
                )}
                <div className={classes.twoItem}>
                  <div className={classes.item}>
                    <Field
                      name="inn"
                      label={t('TIN')}
                      placeholder={t('PleaseEnterYourTIN')}
                      mode={mode}
                      component={InputField}
                      divider={false}
                    />
                    <Divider />
                  </div>
                  <div className={classes.item}>
                    <Field
                      name="phone"
                      render={({ field, form }) => (
                        <InputField
                          field={field}
                          form={form}
                          required={!form.values.email}
                          inputComponent={SomePhoneMaskedInput}
                          label={t('Label.phone')}
                          placeholder={t('Label.phone')}
                          mode={mode}
                          divider={false}
                          inputProps={{ form }}
                        />
                      )}
                    />
                    <Divider />
                  </div>
                </div>
                <div className={classes.item}>
                  <Field
                    name="email"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        required={!form.values.phone}
                        inputComponent={EmailMaskedInput}
                        label="Email"
                        mode={mode}
                        divider={false}
                      />
                    )}
                  />
                  <Divider />
                </div>
                <div className={classes.item}>
                  <Field
                    component={SwitchField}
                    name="ticket_notify"
                    label={t('ReceiveNotificationsAboutActionsInTickets')}
                    labelPlacement="start"
                    disabled={mode === 'view'}
                    divider={false}
                  />
                  <Divider />
                </div>
                <FormSectionHeader
                  style={{ marginBottom: 15 }}
                  variant="h6"
                  header={t('navMenu.security.accessKeys')}
                />
                {opened.keys?.length ? (
                  <div className={classes.key_container}>
                    {opened.keys.map((item, index) => (
                      <div key={index} className={classes.key_card}>
                        <span className={classes.key_title}>{item.key_title}</span>
                        <span>{item.key_value ? item.key_value : t('NoValue')}</span>
                      </div>
                    ))}
                  </div>
                ) : (
                  <span>{t('NoKeys')}</span>
                )}
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};

export { InfoTabForm };
