import { SelectControl, SelectField } from '@ui/index';
import { timeZones } from '../../models/timezone-select.model';

const TimeZoneSelect = (props) => <SelectControl options={timeZones} {...props} />;

const TimeZoneSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<TimeZoneSelect />} onChange={onChange} {...props} />;
};

export { TimeZoneSelect, TimeZoneSelectField };
