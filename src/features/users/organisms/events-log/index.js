import { useState } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { groupBy } from 'lodash';
import { Accordion, AccordionSummary, AccordionDetails, styled } from '@mui/material';
import { ExpandMore, AccessTime, Check, Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { FilterDatepicker, IconButton, CustomScrollbar, Loader, Empty } from '@ui/index';
import { $opened, changedDetailVisibility } from '../../models/detail.model';
import {
  eventsFiltersSubmitted,
  $eventsFilters,
  $isLoading,
} from '../../models/page.model';

const { t } = i18n;

const useEventsStyles = makeStyles({
  events__toolbar: {
    marginBottom: 24,
  },
  events__container: {
    height: '100%',
    padding: '24px',
  },
  events__form: {
    display: 'grid',
    gridTemplateColumns: 'repeat(5, auto) 1fr',
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    gap: 10,
  },
  events__formText: {
    fontWeight: 500,
    color: '#7D7D8E',
  },
  events__formButtons: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
    justifyItems: 'start',
    justifyContent: 'start',
    color: '#68687e',
  },
  events__close: {
    justifySelf: 'end',
    cursor: 'pointer',
    marginRight: 5,
  },
});

const useEventsItemStyles = makeStyles({
  eventsItem: {
    display: 'grid',
    gridTemplateColumns: '24px 1fr',
    gap: 24,
    alignItems: 'start',
    padding: '20px 16px 20px 24px',
    backgroundColor: '#e5f3fa',
  },
  eventsItem__iconWrap: {
    display: 'grid',
    alignItems: 'center',
    justifyItems: 'center',
    backgroundColor: '#35A9E9',
    borderRadius: '100%',
    width: 24,
    height: 24,
  },
  eventsItem__icon: {
    fontSize: 16,
    color: 'white',
  },
  eventsItem__content: {
    display: 'grid',
    alignItems: 'start',
    gap: 8,
  },
  eventsItem__time: {
    color: '#65657B',
    fontWeight: 500,
    opacity: 0.7,
  },
  eventsItem__text: {
    color: '#65657B',
    fontWeight: 500,
    fontSize: 13,
  },
});

const StyledAccordion = styled((props) => <Accordion elevation={0} square {...props} />)(
  ({ theme }) => ({
    border: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
  })
);

const StyledAccordionSummary = styled((props) => (
  <AccordionSummary expandIcon={<ExpandMore sx={{ fontSize: '0.9rem' }} />} {...props} />
))(({ theme }) => ({
  minHeight: 'auto',
  padding: '22px 0',
  borderBottom: '1px solid #E7E7EC',
  backgroundColor: 'transparent',
  '&.Mui-expanded': {
    border: 'none',
    padding: '22px 0',
  },
  '& .MuiAccordionSummary-expandIcon': {
    paddingTop: 0,
    paddingBottom: 0,
  },
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
    color: '#3B3B50',
    fontWeight: '500',
    fontSize: 18,
    margin: 0,
  },
}));

const StyledAccordionDetails = styled(AccordionDetails)(({ theme }) => ({
  display: 'grid',
  gap: 30,
  padding: 0,
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

const EventsItem = ({ data }) => {
  const classes = useEventsItemStyles();

  return (
    <div className={classes.eventsItem}>
      <div className={classes.eventsItem__iconWrap}>
        <AccessTime className={classes.eventsItem__icon} />
      </div>

      <div className={classes.eventsItem__content}>
        <span className={classes.eventsItem__time}>{data.dt.time}</span>
        <span className={classes.eventsItem__text}>
          {data.type_title} {t('ByTheAddress')} {data.object?.title}
        </span>
      </div>
    </div>
  );
};

const EventsLog = ({ isArchived, closeHandler, external }) => {
  const { events } = useStore($opened);
  const eventsFilters = useStore($eventsFilters);
  const isLoading = useStore($isLoading);
  const [expanded, setExpanded] = useState('panel0');
  const classes = useEventsStyles();

  const groupedEvents = groupBy(events, (item) => item.dt.date);

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const renderEvents = () => {
    if (isLoading) return <Loader isLoading />;
    if (!events?.length) return <Empty title={t('NoEventsFoundForTheSelectedPeriod')} />;

    return Object.keys(groupedEvents).map((group, index) => (
      <StyledAccordion
        expanded={expanded == `panel${index}`}
        onChange={handleChange(`panel${index}`)}
        key={index}
      >
        <StyledAccordionSummary aria-controls="panel1d-content" id="panel1d-header">
          {group.split('-').reverse().join('.')}
        </StyledAccordionSummary>
        <StyledAccordionDetails>
          {groupedEvents[group].map((event, key) => (
            <EventsItem key={key} type="green" data={event} />
          ))}
        </StyledAccordionDetails>
      </StyledAccordion>
    ));
  };

  return (
    <div className={classes.events__container}>
      <CustomScrollbar style={{ width: '100%', heigth: '100%' }} autoHide>
        <Formik
          initialValues={eventsFilters}
          onSubmit={eventsFiltersSubmitted}
          onReset={() => eventsFiltersSubmitted('')}
          render={({
            values,
            setFieldValue,
            handleChange,
            handleSubmit,
            handleReset,
          }) => (
            <Form className={classes.events__form}>
              <span className={classes.events__formText}>c</span>
              <Field
                name="date_start"
                component={FilterDatepicker}
                value={values.date_start}
                onChange={(value) => {
                  setFieldValue('date_start', value);
                }}
              />
              <span className={classes.events__formText}>по</span>
              <Field
                name="date_end"
                component={FilterDatepicker}
                value={values.date_end}
                onChange={(value) => {
                  setFieldValue('date_end', value);
                }}
              />
              <div className={classes.events__formButtons}>
                <IconButton title="Применить" onClick={handleSubmit} size="large">
                  <Check />
                </IconButton>
              </div>

              <Close
                className={classes.events__close}
                onClick={external ? closeHandler : () => changedDetailVisibility(false)}
              />
            </Form>
          )}
        />

        {renderEvents()}
      </CustomScrollbar>
    </div>
  );
};

export { EventsLog };
