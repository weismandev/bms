import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { DetailToolbar, InputField, CustomScrollbar, Divider } from '@ui/index';
import {
  startTimeAfterFinishTimeWork,
  startTimeAfterFinishTimeBreak,
  startBreakBetweenWorkTime,
  endBreakBetweenWorkTime,
  firstDateAfterLastVacation,
  firstVacationDate,
} from '../../lib';
import {
  $mode,
  changeMode,
  changedDetailVisibility,
  detailSubmitted,
} from '../../models/detail.model';
import { $timeData } from '../../models/page.model';
import { TimeZoneSelectField } from '../timezone-select';

const { t } = i18n;

const useStyles = makeStyles({
  form: {
    height: '100%',
    position: 'relative',
  },
  toolbar: {
    padding: 24,
  },
  contentForm: {
    height: 'calc(100% - 82px)',
    padding: '0 6px 24px 24px',
  },
  content: {
    paddingRight: 18,
    paddingBottom: 20,
  },
  field: {
    display: 'flex',
  },
  fieldTitle: {
    color: '#8F91A3',
    marginBottom: 12,
    fontSize: 14,
  },
  fieldWidth: {
    width: ({ mode }) => (mode === 'view' ? 10 : '50%'),
  },
  scheduleDivider: {
    margin: '6px 4px 0',
  },
  fieldWorkWrap: {
    display: 'flex',
    flexDirection: 'column',
  },
  fieldWorkTime: {
    marginBottom: 10,
    width: '40%',
    '& label': {
      width: 100,
    },
  },
  time: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
  item: {
    width: '50%',
    paddingRight: 15,
    '@media (max-width: 1024px)': {
      width: '100%',
    },
  },
});

const validationSchema = Yup.object().shape({
  days_on: Yup.string()
    .required(t('thisIsRequiredField'))
    .matches(/^\d+$/, {
      message: t('You can only enter whole numbers!'),
    }),
  days_off: Yup.string()
    .required(t('thisIsRequiredField'))
    .matches(/^\d+$/, {
      message: t('You can only enter whole numbers!'),
    }),
  first_work_day: Yup.string().required(t('thisIsRequiredField')),
  starts_at: Yup.string()
    .required(t('thisIsRequiredField'))
    .test('ends_at', t('StartWorkDayBeforeEndWorkDay'), startTimeAfterFinishTimeWork),
  ends_at: Yup.string()
    .required(t('thisIsRequiredField'))
    .test('starts_at', t('EndWorkDayAfterStartWorkDay'), startTimeAfterFinishTimeWork),
  break_starts_at: Yup.mixed()
    .test('break_ends_at', t('StartBreakBeforeEndBreak'), startTimeAfterFinishTimeBreak)
    .test(
      'startBreakBetweenWorkTime',
      t('TheBreakMustBeIncludedInTheWorkingHours'),
      startBreakBetweenWorkTime
    ),
  break_ends_at: Yup.mixed()
    .test('break_starts_at', t('EndBreakAfterStartBreak'), startTimeAfterFinishTimeBreak)
    .test(
      'endBreakBetweenWorkTime',
      t('TheBreakMustBeIncludedInTheWorkingHours'),
      endBreakBetweenWorkTime
    ),
  vacation_first_day: Yup.string()
    .test(
      'vacation_last_day',
      t('StartVacationBeforeEndVacation'),
      firstDateAfterLastVacation
    )
    .test(
      'first_day',
      t('VacationMustNotBeEarlierThanTheFirstWorkingDay'),
      firstVacationDate
    ),
  vacation_last_day: Yup.string().test(
    'vacation_first_day',
    t('EndVacationAfterStartVacation'),
    firstDateAfterLastVacation
  ),
  timezone: Yup.mixed().required(t('thisIsRequiredField')),
});

const ScheduleForm = ({ isArchived, closeHandler, external }) => {
  const mode = useStore($mode);
  const timeData = useStore($timeData);

  const classes = useStyles({ mode });

  return (
    <Formik
      initialValues={timeData}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={detailSubmitted}
      render={({ resetForm }) => {
        return (
          <Form className={classes.form}>
            <DetailToolbar
              className={classes.toolbar}
              hidden={{ edit: isArchived || external, delete: true }}
              mode={mode}
              onEdit={() => changeMode('edit')}
              onClose={external ? closeHandler : () => changedDetailVisibility(false)}
              onCancel={() => {
                changeMode('view');
                resetForm();
              }}
            />
            <div className={classes.contentForm}>
              <CustomScrollbar>
                <div className={classes.content}>
                  <div className={classes.item}>
                    <p className={classes.fieldTitle}>{t('Chart')}</p>
                    <div className={classes.field}>
                      <div className={classes.fieldWidth}>
                        <Field
                          name="days_on"
                          label={null}
                          divider={false}
                          component={InputField}
                          mode={mode}
                          placeholder=""
                          required
                        />
                      </div>
                      <p className={classes.scheduleDivider}>/</p>
                      <div className={classes.fieldWidth}>
                        <Field
                          name="days_off"
                          label={null}
                          divider={false}
                          component={InputField}
                          mode={mode}
                          placeholder=""
                          required
                        />
                      </div>
                    </div>
                    <Divider isVisible />
                  </div>

                  <div className={classes.item}>
                    <Field
                      name="first_work_day"
                      label={t('FirstWorkDay')}
                      component={InputField}
                      mode={mode}
                      type="date"
                      required
                    />
                  </div>

                  <div className={classes.time}>
                    <div className={classes.item}>
                      <p className={classes.fieldTitle}>{t('WorkingTime')}</p>
                      <div className={classes.fieldWorkWrap}>
                        <div className={classes.fieldWorkTime}>
                          <Field
                            name="starts_at"
                            label={t('Start')}
                            divider={false}
                            component={InputField}
                            mode={mode}
                            placeholder=""
                            labelPlacement="start"
                            type="time"
                            required
                          />
                        </div>
                        <div className={classes.fieldWorkTime}>
                          <Field
                            name="ends_at"
                            label={t('End')}
                            divider={false}
                            component={InputField}
                            mode={mode}
                            labelPlacement="start"
                            placeholder=""
                            type="time"
                            required
                          />
                        </div>
                      </div>
                      <Divider isVisible />
                    </div>
                    <div className={classes.item}>
                      <p className={classes.fieldTitle}>{t('Break')}</p>
                      <div className={classes.fieldWorkWrap}>
                        <div className={classes.fieldWorkTime}>
                          <Field
                            name="break_starts_at"
                            render={({ field, form }) => (
                              <DatePicker
                                customInput={
                                  <InputField
                                    field={field}
                                    form={form}
                                    label={t('Start')}
                                    placeholder={t('chooseTime')}
                                    labelPlacement="start"
                                    divider={false}
                                    style={{ width: 130 }}
                                  />
                                }
                                placeholderText={t('chooseTime')}
                                name={field.name}
                                readOnly={mode === 'view'}
                                selected={field.value}
                                onChange={(value) =>
                                  form.setFieldValue(field.name, value)
                                }
                                timeFormat="HH:mm"
                                timeIntervals={15}
                                locale={dateFnsLocale}
                                timeCaption={t('Label.time').toLowerCase()}
                                dateFormat="HH:mm"
                                showTimeSelect
                                showTimeSelectOnly
                              />
                            )}
                          />
                        </div>
                        <div className={classes.fieldWorkTime}>
                          <Field
                            name="break_ends_at"
                            render={({ field, form }) => (
                              <DatePicker
                                customInput={
                                  <InputField
                                    field={field}
                                    form={form}
                                    label={t('End')}
                                    placeholder={t('chooseTime')}
                                    labelPlacement="start"
                                    divider={false}
                                    style={{ width: 130 }}
                                  />
                                }
                                placeholderText={t('chooseTime')}
                                name={field.name}
                                readOnly={mode === 'view'}
                                selected={field.value}
                                onChange={(value) =>
                                  form.setFieldValue(field.name, value)
                                }
                                timeFormat="HH:mm"
                                timeIntervals={15}
                                locale={dateFnsLocale}
                                timeCaption={t('Label.time').toLowerCase()}
                                dateFormat="HH:mm"
                                showTimeSelect
                                showTimeSelectOnly
                              />
                            )}
                          />
                        </div>
                      </div>
                      <Divider isVisible />
                    </div>
                  </div>

                  <div className={classes.item}>
                    <p className={classes.fieldTitle}>{t('Vacation')}</p>
                    <div className={classes.fieldWorkWrap}>
                      <div className={classes.fieldWorkTime}>
                        <Field
                          name="vacation_first_day"
                          label={t('Start')}
                          divider={false}
                          component={InputField}
                          mode={mode}
                          placeholder=""
                          labelPlacement="start"
                          type="date"
                        />
                      </div>
                      <div className={classes.fieldWorkTime}>
                        <Field
                          name="vacation_last_day"
                          label={t('End')}
                          divider={false}
                          component={InputField}
                          mode={mode}
                          labelPlacement="start"
                          placeholder=""
                          type="date"
                        />
                      </div>
                    </div>
                    <Divider isVisible />
                  </div>

                  <div className={classes.item}>
                    <Field
                      name="timezone"
                      label={t('Timezone')}
                      divider={false}
                      component={TimeZoneSelectField}
                      mode={mode}
                      placeholder={t('SpecifyTheTimezone')}
                      required
                    />
                  </div>
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        );
      }}
    />
  );
};

export { ScheduleForm };
