import { useMemo, useCallback } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  SortingState,
  IntegratedSorting,
  PagingState,
  CustomPaging,
  DataTypeProvider,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  TableColumnResizing,
  TableColumnReordering,
  Toolbar,
  TableColumnVisibility,
  ColumnChooser,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  SortingLabel,
  TableContainer,
  TableRow,
  TableCell,
  Wrapper,
  PagingPanel,
} from '@ui/index';
import { $opened, openViaUrl } from '../../models/detail.model';
import {
  $tableData,
  columns,
  sortChanged,
  $sorting,
  columnWidthsChanged,
  $columnWidths,
  columnOrderChanged,
  $columnOrder,
  hiddenColumnChanged,
  $hiddenColumnNames,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const { t } = i18n;

const $stores = combine({
  tableData: $tableData,
  sortValue: $sorting,
  columnWidths: $columnWidths,
  columnOrder: $columnOrder,
  hiddenColumnNames: $hiddenColumnNames,
  currentPage: $currentPage,
  pageSize: $pageSize,
  rowCount: $rowCount,
});

const Root = ({ children, props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const BuildingsFormatter = ({ value }) => {
  if (!Array.isArray(value)) {
    return <div>{value}</div>;
  }

  const buildingsList = value.map((build, index) => <div key={index}>{build}</div>);

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'repeat(auto-fill, 1fr)',
        gridGap: 3,
        padding: 4,
      }}
    >
      {buildingsList}
    </div>
  );
};

const BuildingsProvider = (props) => (
  <DataTypeProvider formatterComponent={BuildingsFormatter} {...props} />
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => openViaUrl(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [isSelected, props.children]
  );
  return row;
};

export function Table() {
  const {
    tableData,
    sortValue,
    columnWidths,
    columnOrder,
    hiddenColumnNames,
    currentPage,
    pageSize,
    rowCount,
  } = useStore($stores);

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid
        rootComponent={Root}
        rows={tableData}
        getRowId={(row) => row.id}
        columns={columns}
      >
        <SortingState sorting={sortValue} onSortingChange={sortChanged} />

        <DragDropProvider />

        <IntegratedSorting />

        <BuildingsProvider for={['buildings']} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={rowCount} />

        <DXTable
          containerComponent={TableContainer}
          rowComponent={Row}
          cellComponent={TableCell}
          messages={{ noData: t('noData') }}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <TableHeaderRow showSortingControls sortLabelComponent={SortingLabel} />

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames, pageSizes: [25, 50, 100] }} />
      </Grid>
    </Wrapper>
  );
}
