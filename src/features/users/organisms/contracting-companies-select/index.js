import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '@ui/index';
import {
  $data,
  $isLoading,
  $error,
  fxGetList,
} from '../../models/contracting-companies-select.model';

const ContractingCompaniesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const ContractingCompaniesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<ContractingCompaniesSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { ContractingCompaniesSelect, ContractingCompaniesSelectField };
