import { useStore } from 'effector-react';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Grid, Table, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';
import i18n from '@shared/config/i18n';
import {
  ActionButton,
  Modal,
  TableContainer,
  TableHeaderCell,
  TableRow,
  TableCell,
} from '@ui/index';
import {
  $isOpenModal,
  changeVisibilityModal,
  $validationResult,
  $errors,
  columns,
  tableColumnExtensions,
} from '../../models/import-modal.model';
import { importData } from '../../models/page.model';

const { t } = i18n;

const useStyles = makeStyles({
  fullwidth: {
    width: '100%',
    minWidth: '80%',
  },
  title: {
    padding: 15,
  },
});

const Root = (props) => (
  <Grid.Root {...props} style={{ height: '50vh' }}>
    {props.children}
  </Grid.Root>
);

const Row = (props) => <TableRow {...props} addStyles={{ cursor: 'auto' }} />;

const Content = () => {
  const data = useStore($errors);
  const classes = useStyles();

  if (data.length > 0) {
    return (
      <>
        <Typography classes={{ root: classes.title }}>
          {`${t('TheFileHasErrors')} :`}
        </Typography>
        <Grid rootComponent={Root} rows={data} columns={columns}>
          <Table
            messages={{ noData: t('noData') }}
            rowComponent={Row}
            cellComponent={TableCell}
            containerComponent={TableContainer}
            columnExtensions={tableColumnExtensions}
          />

          <TableHeaderRow cellComponent={TableHeaderCell} />
        </Grid>
      </>
    );
  }

  return <div>{t('SuccessfulValidation')}</div>;
};

const Actions = ({ taskId }) => (
  <>
    <ActionButton
      kind="positive"
      onClick={importData}
      disabled={!taskId}
      style={{ marginRight: '10px' }}
    >
      {t('Downloads')}
    </ActionButton>
    <ActionButton kind="negative" onClick={() => changeVisibilityModal(false)}>
      {t('cancel')}
    </ActionButton>
  </>
);

const ImportModal = () => {
  const classes = useStyles();
  const isOpen = useStore($isOpenModal);
  const validationResult = useStore($validationResult);

  return (
    <Modal
      header={t('LoadingOfEmployeesOfTheMC')}
      content={<Content />}
      isOpen={isOpen}
      onClose={() => changeVisibilityModal(false)}
      classes={validationResult?.validated?.has_errors && { paper: classes.fullwidth }}
      actions={<Actions taskId={validationResult?.task?.id} />}
    />
  );
};

export { ImportModal };
