import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const PropertySelect = (props) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const { property_types = [], enterprise, houseList } = props;

  useEffect(() => {
    const filter = enterprise
      ? { filter: { property_types, enterprise_id: enterprise.id } }
      : { filter: { property_types } };

    fxGetList(filter);
  }, [enterprise]);

  const options =
    houseList?.length > 0
      ? data.filter((item) => {
          const findedBuild = item.buildings.filter((build) =>
            houseList.find((house) => house.title === build.building)
          );

          return findedBuild.length > 0;
        })
      : data;

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const PropertySelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return <SelectField component={<PropertySelect />} onChange={onChange} {...props} />;
};

export { PropertySelect, PropertySelectField };
