import { api } from '@api/api2';
import { getCookieNameWithBuildType, cookieMap } from '@tools/cookie';

const getList = (payload = {}) => api.no_vers('get', 'interface/instructions', payload);

const getInstructions = ({ page, with_data }) =>
  api.no_vers('get', 'interface/instructions', {
    page,
    with_data,
  });

export const getTableData = (payload = {}) =>
  api.no_vers('get', 'interface/get-table', payload);

const getListDevices = (payload = {}) =>
  api.no_vers('get', 'bmsdevice/get-devices', payload);

const getDevices = (payload = {}, config = {}) =>
  api.no_vers('get', 'bmsdevice/get-devices', payload, config);

const exportList = (payload) =>
  api.no_vers(
    'request',
    'bmsdevice/get-devices',
    { ...payload, limit: 1000000, export: 1 },
    {
      config: {
        responseType: 'arraybuffer',
      },
    }
  );

const getDeviceTypes = (payload = {}, config = {}) =>
  api.no_vers('get', 'devices/get-device-type', payload, config);

const addDevice = (payload = {}, config = {}) =>
  api.no_vers('get', 'devices/add-device', payload, config);

const getRooms = (payload = {}, config = {}) =>
  api.no_vers('get', 'bmsdevice/get-rooms/', {}, config);

const nextStep = (url) => api.no_vers('get', url);

export const equipmentApi = {
  getList,
  getListDevices,
  getDevices,
  exportList,
  getDeviceTypes,
  addDevice,
  getRooms,
  nextStep,
  getInstructions,
  getTableData,
};
