const getURLParams = (sourceURL) => {
  const url = new URL(sourceURL);
  const urlSearchParams = new URLSearchParams(url.search);
  return Object.fromEntries(urlSearchParams.entries());
};

export default getURLParams;
