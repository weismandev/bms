import { signout } from '@features/common';
import { fxRemoveDevice } from '@features/fabrics/devices/detail/model/controls/controls.model';
import {
  $isArchiveOpen,
  changedArchiveVisibility,
  openedLinkChanged,
  $openedLink,
  $openedSerialnumber,
  openedSerialnumberChanged,
  addClicked,
} from './detail.model';

$openedLink
  .on(fxRemoveDevice.done, () => null)
  .on(openedLinkChanged, (_, url) => url)
  .reset([signout, addClicked, fxRemoveDevice.done]);

// $openedLink
//   .on(openedLinkChanged, (_, serialNumber) => serialNumber)
//   .reset([signout, addClicked, fxRemoveDevice.done]);

$isArchiveOpen.on(changedArchiveVisibility, (_, visibility) => visibility).reset(signout);

$openedSerialnumber.on(openedSerialnumberChanged, (_, serialnumber) => serialnumber);
