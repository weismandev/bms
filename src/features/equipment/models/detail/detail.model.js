import { createEvent, createStore } from 'effector';
import { createDetailBag } from '../../../../tools/factories';

export const newEntity = {
  id: '1',
  avatar: '',
  fullname: '',
  building: '',
  apartment: '',
  statuses: '',
  type: '',
  rights: '2',
  phone: '',
  email: '',
  size: '',
  comment: '',
};

export const newObject = {
  type: { slug: 'flat', title: 'Квартира' },
  building: '',
  entity: '',
  ownership_type: {
    title: 'Собственник',
    id: '1',
  },
  share: '',
};

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: 'Информация',
  },
  {
    value: 'inspection',
    onCreateIsDisabled: false,
    label: 'ТОиР и проверка',
  },
  {
    value: 'signals',
    onCreateIsDisabled: true,
    label: 'Сигналы и связанные устройства',
  },
  {
    value: 'eventLog',
    onCreateIsDisabled: true,
    label: 'Журнал событий',
  },
];

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  open,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const changeTab = createEvent();

export const $currentTab = createStore('info');
export const $userObjects = createStore([]);

export const $isArchiveOpen = createStore(false);

export const changedArchiveVisibility = createEvent();

export const $openedLink = createStore(null);
export const openedLinkChanged = createEvent();

export const openedSerialnumberChanged = createEvent();
export const $openedSerialnumber = createStore(null);
