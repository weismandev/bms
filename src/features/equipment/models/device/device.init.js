import { guard } from 'effector';
import { signout } from '@features/common';
import { fxGetList } from '../../../house-select';
import { equipmentApi } from '../../api';
import getURLParams from '../../lib/getURLParams';
import {
  $currentStepAddDevice,
  changedDeviceVisibility,
  fxDeviceTypes,
  fxDeviceAdd,
  fxDeviceNextStep,
  fxDeviceRooms,
  $deviceTypes,
  $devices,
  $deviceForm,
  $deviceError,
  $isLoadingDevice,
  $isDeviceOpen,
  $payloadByNextStep,
  nextStep,
  updateDeviceFormStore,
  defaultQrPayload,
  $qrPayload,
  setQrPayload,
  defaultDeviceForm,
} from './device.model';

fxDeviceTypes.use(equipmentApi.getDeviceTypes);
fxDeviceAdd.use(equipmentApi.addDevice);
fxDeviceNextStep.use(equipmentApi.nextStep);

$currentStepAddDevice
  .on(nextStep, (_, step) => (typeof step === 'string' ? step : step.params.step))
  .on(fxDeviceAdd.fail, (step) => (step === 'deviceList' ? 'typeDevice' : step))
  .on(fxDeviceAdd.done, (step) => {
    if (step === 'qr') return 'deviceForm';
    return step === 'deviceForm' ? 'success' : step;
  })
  .on(changedDeviceVisibility, () => 'typeDevice')
  .reset(signout);

$isDeviceOpen.on(changedDeviceVisibility, (_, visibility) => visibility).reset(signout);

$qrPayload
  .on(setQrPayload, (_, payload) => ({ ...defaultQrPayload, ...payload }))
  .on(changedDeviceVisibility, (state, isOpen) => (!isOpen ? defaultQrPayload : state))
  .reset(signout);

$deviceForm
  .on(updateDeviceFormStore, (_, payload) => payload)
  .on(fxDeviceTypes.done, (state) => ({ ...state, step: 2 }))
  .on(fxDeviceAdd.done, (state, { result: { devices } }) => {
    if (devices && Object.keys(devices[0]).length > 0) {
      return { ...state, ...devices[0], serialnumber: devices[0].serial };
    }
    return state;
  })
  .on(nextStep, (state, step) => (step === 'typeDevice' ? defaultDeviceForm : state))
  .reset(signout);

$isLoadingDevice.on(
  [fxDeviceTypes.pending, fxDeviceAdd.pending, fxDeviceAdd.fail],
  (_, isLoading) => isLoading
);

$deviceTypes.on(fxDeviceTypes.done, (_, { result }) => result.types).reset(signout);

$deviceError
  .on(fxDeviceAdd.fail, (_, { error }) => error.message)
  .on(changedDeviceVisibility, () => '')
  .on(nextStep, () => '')
  .reset(signout);

$devices
  .on(fxDeviceAdd.pending, (state, isLoading) => {
    return isLoading ? [] : state;
  })
  .on(fxDeviceAdd.done, (_, { result }) => result.devices)
  .reset(signout);

$payloadByNextStep
  .on(
    fxDeviceAdd.done,
    (_, { result }) => result.next_step && getURLParams(result.next_step)
  )
  .reset(signout);

guard({
  clock: changedDeviceVisibility,
  source: $isDeviceOpen,
  filter: (isDeviceOpen) => isDeviceOpen,
  target: [fxDeviceTypes, fxDeviceRooms, fxGetList],
});

guard({
  clock: updateDeviceFormStore,
  source: $deviceForm,
  filter: ({ isSubmittedForm }) => isSubmittedForm,
  target: fxDeviceAdd.prepend(({ isSubmittedForm, ...rest }) => rest),
});
