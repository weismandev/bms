import { sample, guard, merge, forward } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { fxRemoveDevice } from '@features/fabrics/devices/detail/model/controls/controls.model';
import {
  $filters,
  $filterControls,
} from '@features/fabrics/devices/filter/model/filter.model';
import { $instructions, needUpdateTable } from '@features/fabrics/table/model';
import { $tableParams } from '@features/fabrics/table/model/';
import { equipmentApi } from '../../api';
import {
  PageGate,
  pageMounted,
  pageUnmounted,
  fxGetList,
  fxGetInstructions,
  fxExportList,
  fxDelete,
  $equipmentInstructions,
  $isLoading,
  $raw,
  $error,
  $isErrorDialogOpen,
  defaultDeleteErrorOccurred,
} from '../equipment';
import {
  fxCreateExploitations,
  fxGetExploitations,
  fxEditExploitations,
} from '../exploitation';

// import { exportListClicked, $selection } from './table.model';

fxExportList.use(equipmentApi.exportList);
fxGetInstructions.use(equipmentApi.getInstructions);

$isLoading.on(
  merge([
    fxExportList.pending,
    fxRemoveDevice.pending,
    fxCreateExploitations.pending,
    fxGetExploitations.pending,
    fxEditExploitations.pending,
  ]),
  (_, isLoading) => isLoading
);

const errorOccured = merge([
  fxExportList.fail,
  fxRemoveDevice.fail,
  fxCreateExploitations.fail,
  fxEditExploitations.fail,
]);

$error
  .on(errorOccured, (_, { error }) => error)
  .on(defaultDeleteErrorOccurred, (_, { error }) => error.message)
  .off(fxDelete.fail);

$isErrorDialogOpen
  .on([errorOccured, defaultDeleteErrorOccurred], () => true)
  .off(fxDelete.fail);

// $filterControls
//   .on(fxGetList.done, (_, { result: { filter } }) => filter.main)
//   .reset(signout);

$filterControls
  .on(fxGetInstructions.done, (_, { result: { filter } }) => filter.main)
  .reset([pageUnmounted, signout]);

// sample({
//   clock: [pageMounted, fxRemoveDevice.done],
//   fn: ({ page = 'catalogdevice', with_data = 1, filter }) => ({
//     ...filter,
//     page,
//     with_data,
//     limit: 25,
//   }),
//   target: fxGetList,
// });

// sample({
//   clock: exportListClicked,
//   source: [$tableParams, $filters, $selection],
//   fn: collectPayload,
//   target: fxExportList,
// });

fxExportList.done.watch(({ result }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `equipment-${dateTimeNow}.xls`);
});

$equipmentInstructions
  .on(fxGetInstructions.done, (_, { result }) => result)
  .reset([pageUnmounted, signout]);

sample({
  clock: PageGate.state,
  fn: ({ page }) => ({ page }),
  target: fxGetInstructions,
});

// Прокидываем инструкции в table factory
forward({
  from: $equipmentInstructions,
  to: $instructions,
});
// Пробрасываем сигнал о необходимости обновления таблицы в table factory
forward({
  from: [fxRemoveDevice.done],
  to: needUpdateTable,
});

function collectPayload(params, [tableParams, filters, selection]) {
  const payload = {};

  if (tableParams.sorting.length > 0) {
    payload.sort = tableParams.sorting[0].order;
    payload.column = tableParams.sorting[0].name;
  }

  if (tableParams.search) {
    payload.search = tableParams.search;
  }

  if (tableParams.per_page) {
    payload.limit = tableParams.per_page;
  }

  if (tableParams.page) {
    payload.offset = tableParams.per_page * (tableParams.page - 1);
  }

  return payload;
}
