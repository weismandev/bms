import { createEffect, createStore, attach, createEvent } from 'effector';
import {
  fxSaveInStorage as fxSaveInStorageBase,
  fxGetFromStorage as fxGetFromStorageBase,
} from '@features/common';
import { createPageBag, createDialogBag } from '../../../../tools/factories';
import { equipmentApi } from '../../api';

export const fxExportList = createEffect();
export const fxAssignQuarantine = createEffect();
export const fxDownloadTemplate = createEffect();
export const fxGetInstructions = createEffect();

export const defaultDeleteErrorOccurred = createEvent();

export const $equipmentInstructions = createStore(null);

export const fxGetFromStorage = attach({ effect: fxGetFromStorageBase });

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  $raw,

  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  request,
  errorOccured,
  requestApi,

  $normalized,

  $isDeleteDialogOpen,
  deleteDialogVisibilityChanged,
  $isErrorDialogOpen,
  errorDialogVisibilityChanged,
  deleteClicked,
  deleteConfirmed,
} = createPageBag(
  equipmentApi,
  {
    handleGetList: (_, result) => result,
  },
  { idAttribute: 'serialnumber', itemsAttribute: 'devices' }
);
