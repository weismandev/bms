import { Loader, ActionButton, BaseInputErrorText } from '@ui';
import { List, ListItem, ListItemText } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import i18n from '@shared/config/i18n';
import useStyles from '../styles';

const { t } = i18n;

const DeviceListByType = ({ isLoading, error, devicesList, onSubmit, onCancel }) => {
  const classes = useStyles();

  const handleSelectDevice = (name) => () => onSubmit(name);

  return (
    <div className={classes.wrap}>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Loader isLoading={isLoading} />
      <List component="nav" className={classes.content}>
        {devicesList.map(({ name, title }) => (
          <ListItem button onClick={handleSelectDevice(name)} key={name}>
            <ListItemText primary={title} />
          </ListItem>
        ))}
      </List>
      <div className={classes.btnGroup}>
        <ActionButton onClick={onCancel}>
          <CloseIcon />
          {t('Cancellation')}
        </ActionButton>
      </div>
    </div>
  );
};

export default DeviceListByType;
