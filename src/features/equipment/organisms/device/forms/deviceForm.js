import { ActionButton, BaseInputErrorText, InputField } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { HouseSelectField } from '../../../../house-select';
import { RoomsSelectField, changeBuilding } from '../../../../rooms-select';
import useStyles from '../styles';

const { t } = i18n;

const DeviceFormSchema = (isSerialNumber) =>
  Yup.object().shape({
    building_id: Yup.object().nullable().required(t('thisIsRequiredField')),
    title: Yup.string().required(t('thisIsRequiredField')),
    ...(!isSerialNumber
      ? { serialnumber: Yup.string().required(t('thisIsRequiredField')) }
      : {}),
  });

const DeviceForm = (props) => {
  const classes = useStyles();
  const { initialValues, isSerialNumber, onBack, error, onSubmit } = props;

  const handleSubmit = (values) =>
    onSubmit({
      ...values,
      step: 3,
      building_id: values?.building_id?.id,
      room_id: values.room_id.id,
    });

  const handleChangeBuilding = (setFieldValue) => (value) => {
    changeBuilding(value ? value.id : '');
    setFieldValue('building_id', value, false);
  };

  return (
    <div className={classes.wrap}>
      {error && <BaseInputErrorText style={{ color: 'red' }} errors={[error]} />}
      <Formik
        initialValues={{ ...initialValues, factory_number: 73263509 }}
        onSubmit={handleSubmit}
        validationSchema={DeviceFormSchema(isSerialNumber)}
        enableReinitialize
        render={({ values, errors, touched, setFieldValue }) => (
          <Form style={{ paddingTop: 24 }}>
            <Field
              name="title"
              label={t('NameTitle')}
              placeholder={t('EnterDeviceName')}
              component={InputField}
            />
            {errors.title && touched.title && <div>{errors.title}</div>}
            <Field
              name="building_id"
              label={t('Label.address')}
              placeholder={t('ChooseAddress')}
              component={HouseSelectField}
              onChange={handleChangeBuilding(setFieldValue)}
            />
            {errors.id && touched.id && <div>{errors.id}</div>}
            <Field
              name="room_id"
              label={t('DevicePremises')}
              placeholder={t('ChooseRoom')}
              component={RoomsSelectField}
              isDisabled={!values.building_id}
            />
            {errors.room_id && touched.room_id && <div>{errors.room_id}</div>}
            <Field
              name="factory_number"
              label={t('FactoryNumber')}
              placeholder={t('EnterFactoryNumber')}
              component={InputField}
            />
            <div className={classes.btnGroup}>
              <ActionButton kind="basic" type="submit">
                {t('AddDevice')}
              </ActionButton>
              <ActionButton kind="positive" onClick={onBack} style={{ marginLeft: 10 }}>
                {t('Cancellation')}
              </ActionButton>
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default DeviceForm;
