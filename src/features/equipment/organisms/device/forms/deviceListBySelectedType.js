import { List, ListItem, ListItemText } from '@mui/material';
import i18n from '@shared/config/i18n';
import { Loader, ActionButton } from '../../../../../ui';
import useStyles from '../styles';

const { t } = i18n;

const DeviceListBySelectedType = ({
  isLoading,
  devicesList,
  onSubmit,
  onCancel,
  onBack,
}) => {
  const classes = useStyles();

  const handleSelectDevice = (serialnumber) => () => onSubmit({ serialnumber });

  return (
    <div className={classes.wrap}>
      <Loader isLoading={isLoading} />
      <List component="nav" className={classes.content}>
        {devicesList.map(({ serial, title }) => (
          <ListItem button onClick={handleSelectDevice(serial)} key={serial}>
            <ListItemText primary={title} />
          </ListItem>
        ))}
      </List>
      <div className={classes.btnGroup}>
        <ActionButton kind="basic" onClick={onCancel}>
          {t('Cancellation')}
        </ActionButton>
        <ActionButton
          kind="negative-outlined"
          onClick={onBack}
          style={{ marginLeft: 10 }}
        >
          {t('Back')}
        </ActionButton>
      </div>
    </div>
  );
};

export default DeviceListBySelectedType;
