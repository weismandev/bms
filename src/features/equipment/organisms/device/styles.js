import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  wrap: {
    height: 591,
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    height: '92%',
    overflow: 'auto',
  },
  centerContent: {
    height: '90%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnGroup: {
    position: 'absolute',
    bottom: 20,
    width: '90%',
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  direction: {
    flexDirection: 'column',
  },
});

export default useStyles;
