import { TextareaAutosize } from '@mui/material';
import ArchiveIcon from '@mui/icons-material/Archive';
import CloseIcon from '@mui/icons-material/Close';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { ActionButton } from '../../../../ui';

const { t } = i18n;

const useStyles = makeStyles(() => ({
  title: {
    fontSize: 14,
    fontWeight: 500,
    padding: '24px 0 12px',
    margin: 0,
    color: '#65657B',
  },
  wrapTextarea: {
    width: '100%',
    borderRadius: 16,
    border: '1px solid #E7E7EC',
    paddingLeft: 12,
    fontSize: 13,
    fontWeight: 500,
    color: '#65657B',
  },
  wrapBtn: {
    marginTop: 32,
    marginBottom: 12,
  },
  btnArchive: {
    backgroundColor: '#EC7F00',
    '&:hover': {
      backgroundColor: '#EC7F00',
    },
  },
  btnClose: {
    marginLeft: 16,
  },
}));

const ArchiveModal = ({ onClose }) => {
  const { wrapTextarea, wrapBtn, title, btnArchive, btnClose } = useStyles();
  return (
    <div>
      <p className={title}>{t('ReasonForArchiving')}</p>
      <TextareaAutosize
        className={wrapTextarea}
        aria-label="minimum height"
        minRows={5}
        placeholder={t('ArchivingReasonText')}
      />
      <div className={wrapBtn}>
        <ActionButton className={btnArchive} onClick={onClose}>
          <ArchiveIcon />
          <span className="btnArchiveTitle">{t('ArchiveDevice')}</span>
        </ActionButton>
        <ActionButton onClick={onClose} className={btnClose}>
          <CloseIcon />
          {t('Cancellation')}
        </ActionButton>
      </div>
    </div>
  );
};

export { ArchiveModal };
