import { useEffect, useState } from 'react';
import { useStore } from 'effector-react';
import { ColoredTextIconNotification } from '@features/colored-text-icon-notification';
import { HaveSectionAccess } from '@features/common';
import PageContext from '@features/fabrics/app/pageContext';
import { Detail } from '@features/fabrics/devices/detail';
import {
  $isOpenDetail,
  openedLinkChanged,
  setURLSearchParamsSerialNumber,
} from '@features/fabrics/devices/detail/model/detail';
import { Filter } from '@features/fabrics/filter';
import { InstructionGate } from '@features/fabrics/processes/instructions/models/instructions';
import { Table } from '@features/fabrics/table';
import { parseUrlParams } from '@tools/parseUrlParams';
import { ErrorMessage, Modal, FilterMainDetailLayout } from '@ui/index';
import { $isArchiveOpen, changedArchiveVisibility } from '../models/detail';
import {
  $isDeviceOpen,
  changedDeviceVisibility,
  $currentStepAddDevice,
  $stepsAddDevice,
} from '../models/device';
import {
  $isErrorDialogOpen,
  $error,
  errorDialogVisibilityChanged,
} from '../models/equipment';
import { Device, ArchiveModal } from '../organisms';

const PageRoot = () => {
  const pageName = 'catalogdevice';

  const [isFilterOpen, visibleFilter] = useState(false);

  const isOpenDetail = useStore($isOpenDetail);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isDeviceOpen = useStore($isDeviceOpen);
  const currentStepAddDevice = useStore($currentStepAddDevice);
  const stepsAddDevice = useStore($stepsAddDevice);
  const isArchiveOpen = useStore($isArchiveOpen);

  const deviceUrl = parseUrlParams('url');
  const deviceSerialnumberUrl = parseUrlParams('serialnumber');

  useEffect(() => {
    if (deviceUrl) openedLinkChanged(deviceUrl);
    if (deviceSerialnumberUrl) {
      setURLSearchParamsSerialNumber(deviceSerialnumberUrl);
    }
  }, []);

  const handleDeviceVisibility = () => changedDeviceVisibility(false);
  const handleDialogErrorVisibility = () => errorDialogVisibilityChanged(false);
  const handleArchiveVisibility = () => changedArchiveVisibility(false);

  const providerValues = {
    page: pageName,
    filter: {
      isFilterOpen,
      visibleFilter,
    },
  };

  return (
    <PageContext.Provider value={providerValues}>
      <InstructionGate page={pageName} with_data={1} />

      <ColoredTextIconNotification />

      <Modal
        isOpen={isDeviceOpen}
        onClose={handleDeviceVisibility}
        header={stepsAddDevice[currentStepAddDevice].title}
        content={<Device type={currentStepAddDevice} />}
      />

      <Modal
        isOpen={isArchiveOpen}
        onClose={handleArchiveVisibility}
        header="Архивация устройства"
        content={<ArchiveModal onClose={handleArchiveVisibility} />}
      />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        error={error}
        onClose={handleDialogErrorVisibility}
      />

      <FilterMainDetailLayout
        params={{ detailWidth: 'minmax(370px, 50%)' }}
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isOpenDetail && <Detail />}
      />
    </PageContext.Provider>
  );
};

const RestrictedPeoplePage = (props) => (
  <HaveSectionAccess>
    <PageRoot {...props} />
  </HaveSectionAccess>
);

export { RestrictedPeoplePage as EquipmentPage };
