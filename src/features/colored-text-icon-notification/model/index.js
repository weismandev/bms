import { createEvent, createStore } from 'effector';
import { signout } from '../../common';

export const changeNotification = createEvent();

export const $notification = createStore({ isOpen: false })
  .on(changeNotification, (state, payload) => ({ ...state, ...payload }))
  .reset(signout);
