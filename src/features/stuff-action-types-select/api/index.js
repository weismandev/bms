import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'stuff-pass/crm/action-types/list');

export { getList };
