import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, fxGetList, $isLoading, $error } from '../../model';

const StuffActionTypesSelect = (props) => {
  const options = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return (
    <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
  );
};

const StuffActionTypesSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField component={<StuffActionTypesSelect />} onChange={onChange} {...props} />
  );
};

export { StuffActionTypesSelect, StuffActionTypesSelectField };
