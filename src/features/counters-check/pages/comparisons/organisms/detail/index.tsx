import { useStore } from 'effector-react';
import { Loader } from '@ui/index';
import { $isDetailLoading, $isLoading, $detailType } from '../../models';
import { AddForm } from '../forms/add-form/index';
import { EntityForm } from '../forms/entity-form/index';

const Detail = () => {
  const detailType = useStore($detailType);
  const isDetailLoading = useStore($isDetailLoading);
  const isLoading = useStore($isLoading);

  const renderForms = () => {
    switch (detailType) {
      case 'add-form':
        return <AddForm />;
      case 'entity-form':
        return <EntityForm />;
      default:
        return null;
    }
  };

  return (
    <>
      <Loader isLoading={isDetailLoading || isLoading} />
      {renderForms()}
    </>
  );
};

export { Detail };
