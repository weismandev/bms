import { FC } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Tooltip } from '@mui/material';

import {
  Loader,
  DetailToolbar,
  Wrapper,
  CustomScrollbar,
  InputField,
  SelectField,
  ActionButton,
  EditButton,
  SaveButton,
} from '@ui/index';

import {
  $mode,
  changeMode,
  changedDetailVisibility,
  $opened,
  $isDetailLoading,
  deleteEntity,
  saveSubmitted,
} from '@features/counters-check/pages/comparisons/models/detail';
import { HouseSelectField } from '@features/house-select';
import {
  startDateAfterFinishDate,
  checkTodayIsBefore,
} from '@features/counters-check/lib';
import { history } from '@features/common';
import { comparisonTypes } from '@features/counters-check/vars';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  registry_number: Yup.string().required(t('thisIsRequiredField')),
  object: Yup.mixed().required(t('thisIsRequiredField')),
  buildings: Yup.mixed().required(t('thisIsRequiredField')),
  types: Yup.mixed().required(t('thisIsRequiredField')),
  starts_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .nullable()
    .required(t('thisIsRequiredField')),
  ends_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    )
    .nullable()
    .required(t('thisIsRequiredField')),
  comment: Yup.mixed().required(t('thisIsRequiredField')),
});

const EntityForm: FC = () => {
  const mode = useStore($mode);
  const opened = useStore($opened);
  const isLoading = useStore($isDetailLoading);

  const isDeleteDisabled = !checkTodayIsBefore(opened.starts_at);

  const renderDeleteButton = () => {
    if (isDeleteDisabled)
      return (
        <Tooltip title={t('UnableToCancelAReconciliationThatHasStarted') as string}>
          <div
            style={{
              order: 50,
              marginRight: 15,
            }}
          >
            <ActionButton
              style={{
                padding: '0 20px',
              }}
              kind="basic"
              disabled={true}
            >
              {t('cancel')}
            </ActionButton>
          </div>
        </Tooltip>
      );
    else
      return (
        <ActionButton
          style={{
            padding: '0 20px',
            order: 50,
            marginRight: 15,
          }}
          kind="basic"
          onClick={() => deleteEntity(opened.id)}
          disabled={false}
        >
          {t('cancel')}
        </ActionButton>
      );
  };

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />

      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={opened}
        render={({ resetForm, values, setFieldValue, submitForm }) => (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              title={`${opened.registry_number}`}
              onCancel={() => changedDetailVisibility(false)}
              onClose={() => changedDetailVisibility(false)}
              hidden={{ delete: true, edit: true }}
              mode="view"
            >
              <ActionButton
                style={{
                  padding: '0 15px',
                  fontWeight: 400,
                  marginRight: 15,
                  order: 50,
                }}
                kind="basic"
                onClick={() => history.push(`/counters-check/${opened.id}`, '_blank')}
                disabled={false}
              >
                {t('Results')}
              </ActionButton>
              {mode === 'view' ? (
                <EditButton
                  style={{
                    order: 60,
                  }}
                  onClick={() => changeMode('edit')}
                />
              ) : (
                <SaveButton
                  style={{
                    order: 60,
                  }}
                  onClick={() => {
                    saveSubmitted(values);
                    changeMode('view');
                  }}
                />
              )}
              {renderDeleteButton()}
            </DetailToolbar>

            <div
              style={{
                width: 'calc(100% + 19px)',
                height: 'calc(100% - 50px)',
              }}
            >
              <CustomScrollbar style={{ height: '100%' }} autoHide>
                <div
                  style={{
                    width: 'calc(100% - 19px)',
                    paddingTop: 14,
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <Field
                    name="registry_number"
                    label={t('ReconciliationID')}
                    component={InputField}
                    mode="view"
                  />
                  <Field
                    name="buildings"
                    complex={values.object ? [values.object] : []}
                    component={HouseSelectField}
                    label={t('building')}
                    isMulti
                    mode="view"
                  />
                  <Field
                    name="types"
                    component={SelectField}
                    label={t('MeterType')}
                    options={comparisonTypes}
                    isMulti
                    mode="view"
                  />
                  <Field
                    name="starts_at"
                    label={t('ReconciliationStartDate')}
                    component={InputField}
                    type="date"
                    mode="view"
                  />
                  <Field
                    name="ends_at"
                    label={t('ReconciliationEndDate')}
                    component={InputField}
                    type="date"
                    mode="view"
                  />
                  <Field
                    name="comment"
                    label={t('Label.comment')}
                    component={InputField}
                    divider={false}
                    multiline
                    rowsMax={5}
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

export { EntityForm };
