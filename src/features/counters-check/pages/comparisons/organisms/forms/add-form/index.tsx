import { FC } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import makeStyles from '@mui/styles/makeStyles';
import {
  CustomScrollbar,
  DetailToolbar,
  InputField,
  Loader,
  SelectField,
  Wrapper,
} from '@ui/index';
import {
  $isDetailLoading,
  $mode,
  $opened,
  addSubmitted,
  changedDetailVisibility,
  newEntity,
} from '@features/counters-check/pages/comparisons/models';
import { ComplexSelectField } from '@features/complex-select';
import { HouseSelectField } from '@features/house-select';
import { startDateAfterFinishDate } from '@features/counters-check/lib';
import { comparisonTypes } from '@features/counters-check/vars';
import { useTranslation } from 'react-i18next';
import i18n from '@shared/config/i18n';

const { t } = i18n;

const $stores = combine({
  mode: $mode,
  isLoading: $isDetailLoading,
  opened: $opened,
});

interface AddFormProps {
  closeHandler: () => void;
}

const validationSchema = Yup.object().shape({
  registry_number: Yup.string().required(t('thisIsRequiredField')),
  object: Yup.mixed().required(t('thisIsRequiredField')),
  buildings: Yup.mixed().required(t('thisIsRequiredField')),
  types: Yup.mixed().required(t('thisIsRequiredField')),
  starts_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .nullable()
    .required(t('thisIsRequiredField')),
  ends_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    )
    .nullable()
    .required(t('thisIsRequiredField')),
  comment: Yup.string(),
});

const useStyles = makeStyles((theme) => ({
  headerRoot: {
    padding: '16px 0 17px',
    minHeight: 62,
    overflowWrap: 'anywhere',
    // marginTop: -48,
  },
  headerText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '28px',
  },
}));

const AddForm: FC = () => {
  const { mode, opened, isLoading } = useStore($stores);
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />

      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={newEntity}
        onSubmit={addSubmitted}
        render={({ resetForm, values, setFieldValue, submitForm }) => (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              title={t('NewReconciliation')}
              onSave={submitForm}
              onCancel={() => changedDetailVisibility(false)}
              onClose={() => changedDetailVisibility(false)}
              // onDelete={() => deleteDialogVisibilityChange(true)}
              hidden={{ delete: true, edit: true }}
              mode="edit"
            />
            <div
              style={{
                width: 'calc(100% + 19px)',
                height: 'calc(100% - 50px)',
              }}
            >
              <CustomScrollbar style={{ height: '100%' }} autoHide>
                <div
                  style={{
                    width: 'calc(100% - 19px)',
                    paddingTop: 14,
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <Field
                    name="registry_number"
                    label={t('ReconciliationID')}
                    component={InputField}
                    placeholder=""
                  />
                  <Field
                    name="object"
                    component={ComplexSelectField}
                    label={t('AnObject')}
                    placeholder={t('selectObject')}
                    onChange={(value: any) => {
                      setFieldValue('object', value);
                      setFieldValue('buildings', '');
                    }}
                  />
                  {values.object ? (
                    <Field
                      name="buildings"
                      complex={values.object ? [values.object] : []}
                      component={HouseSelectField}
                      label={t('building')}
                      placeholder={t('selectBbuilding')}
                      isMulti
                    />
                  ) : null}

                  <Field
                    name="types"
                    component={SelectField}
                    label={t('MeterType')}
                    placeholder={t('ChooseAValue')}
                    options={comparisonTypes}
                    isMulti
                  />
                  <Field
                    name="starts_at"
                    label={t('ReconciliationStartDate')}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                  <Field
                    name="ends_at"
                    label={t('ReconciliationEndDate')}
                    component={InputField}
                    placeholder=""
                    type="date"
                  />
                  <Field
                    name="comment"
                    label={t('Label.comment')}
                    component={InputField}
                    divider={false}
                    placeholder=""
                    multiline
                    rowsMax={5}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

export { AddForm };
