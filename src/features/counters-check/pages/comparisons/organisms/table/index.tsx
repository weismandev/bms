import { useCallback, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { Button } from '@mui/material';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  DataTypeProvider,
  DataTypeProviderProps,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
} from '@devexpress/dx-react-grid-material-ui';
import { history } from '@features/common';
import {
  openEntity,
  $opened,
} from '@features/counters-check/pages/comparisons/models/detail';
import {
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $columnOrder,
  columnOrderChanged,
} from '@features/counters-check/pages/comparisons/models/table';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  PagingPanel,
  HiddenArray,
} from '@ui/index';
import { TableToolbar } from '../table-toolbar';
import { useStyles } from './styles';

const { t } = i18n;

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((_, id) => openEntity(id), []);

  return <TableRow {...props} onRowClick={onRowClick} isSelected={isSelected} />;
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: ReactNode) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const IdFormatter = ({ value }: { value: string[] }) => (
  <Button
    color="primary"
    onClick={() => history.push(`/counters-check/${value}`, '_blank')}
  >
    {t('Results')}
  </Button>
);

const IdTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={IdFormatter} {...props} />
);

const BuildingsFormatter = ({ value }: { value: any[] }) => (
  <HiddenArray>
    {value.map((item, index) => (
      <span key={index}>{item.title}</span>
    ))}
  </HiddenArray>
);

const BuildingsTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={BuildingsFormatter} {...props} />
);

const TypesFormatter = ({ value }: { value: any[] }) => (
  <HiddenArray>
    {value.map((item, index) => (
      <span key={index}>{item}</span>
    ))}
  </HiddenArray>
);

const TypesTypeProvider = (props: DataTypeProviderProps) => (
  <DataTypeProvider formatterComponent={TypesFormatter} {...props} />
);

const ComparisonsTable: React.FC = () => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const columnOrder = useStore($columnOrder);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <DragDropProvider />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <IdTypeProvider for={['id']} />

        <BuildingsTypeProvider for={['buildings']} />

        <TypesTypeProvider for={['types']} />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { ComparisonsTable };
