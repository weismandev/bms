import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import i18n from '@shared/config/i18n';
import { Toolbar, FilterButton, Greedy, AdaptiveSearch, ActionButton } from '@ui/index';
import { addEntity, $isDetailOpen } from '../../models/detail';
import { changedFilterVisibility, $isFilterOpen } from '../../models/filter';
import { $comparisons } from '../../models/page';
import { $search, searchChanged } from '../../models/table';

const { t } = i18n;

const TableToolbar: FC = () => {
  const comparisons = useStore($comparisons);
  const totalCount = comparisons.meta.total;
  const searchValue = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);

  const isAnySideOpen = isFilterOpen && isDetailOpen;

  const startButton = (
    <ActionButton
      kind="positive"
      onClick={() => addEntity()}
      style={{
        width: 'max-content',
        minWidth: 'unset',
        padding: '0 20px 0 20px',
      }}
    >
      {t('RunReconciliation')}
    </ActionButton>
  );

  return (
    <Toolbar>
      <Tooltip title={t('Filters' as string)}>
        <div>
          <FilterButton
            disabled={isFilterOpen}
            style={{ marginRight: 10 }}
            onClick={() => changedFilterVisibility(true)}
          />
        </div>
      </Tooltip>
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <Greedy />
      {startButton}
    </Toolbar>
  );
};

export { TableToolbar };
