import { memo } from 'react';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { ComplexSelectField } from '@features/complex-select';
import { startDateAfterFinishDate } from '@features/counters-check/lib';
import { comparisonTypes } from '@features/counters-check/vars';
import { HouseSelectField } from '@features/house-select';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  InputField,
  SelectField,
} from '@ui/index';
import { changedFilterVisibility, filtersSubmitted, $filters } from '../../models/filter';
import { useStyles } from './styles';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  starts_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('StartDateBeforeEndDate'),
      startDateAfterFinishDate
    )
    .nullable(),
  ends_at: Yup.string()
    .test(
      'start_date-after-finish_date',
      t('EndDateAfterStartDate'),
      startDateAfterFinishDate
    )
    .nullable(),
});

const statuses = [
  { title: t('New'), id: 'new' },
  { title: t('InWork'), id: 'in_progress' },
  { title: t('Сompleted'), id: 'finished' },
];

const Filter = memo(() => {
  const filters = useStore($filters);

  const classes = useStyles();

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filters}
          onSubmit={filtersSubmitted}
          onReset={() => filtersSubmitted('')}
          enableReinitialize
          validationSchema={validationSchema}
          render={({ values, setFieldValue }) => (
            <Form className={classes.form}>
              <Field
                name="object"
                component={ComplexSelectField}
                label={t('AnObject')}
                placeholder={t('selectObject')}
                onChange={(value: any) => {
                  setFieldValue('object', value);
                  setFieldValue('building_id', '');
                }}
              />
              <Field
                name="building_id"
                complex={values.object ? [values.object] : []}
                component={HouseSelectField}
                label={t('building')}
                placeholder={t('selectBbuilding')}
                isMulti
              />
              <Field
                name="status"
                component={SelectField}
                options={statuses}
                label={t('Label.status')}
                placeholder={t('selectStatuse')}
              />
              <Field
                name="type"
                component={SelectField}
                placeholder={t('chooseTypes')}
                options={comparisonTypes}
                label={t('MeterType')}
                isMulti
              />
              <Field
                name="starts_at"
                label={t('ReconciliationStartDate')}
                component={InputField}
                placeholder=""
                type="date"
              />
              <Field
                name="ends_at"
                label={t('ReconciliationEndDate')}
                component={InputField}
                placeholder=""
                type="date"
              />
              <FilterFooter isResettable />
            </Form>
          )}
        />
      </CustomScrollbar>
    </Wrapper>
  );
});

export { Filter };
