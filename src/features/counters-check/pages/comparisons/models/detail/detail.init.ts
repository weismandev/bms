import { sample } from 'effector';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { comparisonsApi } from '@features/counters-check/api';
import { formatComparisonEntity } from '@features/counters-check/lib';
import { fxGetComparisonById, pageUnmounted, getComparisons } from '../page';
import {
  $opened,
  $isDetailOpen,
  addEntity,
  openEntity,
  $detailType,
  fxCreateComparison,
  addSubmitted,
  fxDeleteComparison,
  $isDetailLoading,
  deleteEntity,
  $currentId,
  $mode,
  fxUpdateComparison,
  saveSubmitted,
} from './detail.model';

fxCreateComparison.use(comparisonsApi.createComparison);
fxDeleteComparison.use(comparisonsApi.deleteComparison);
fxUpdateComparison.use(comparisonsApi.setComparisonComment);

/** Обработка открытия деталей */
$isDetailOpen
  .on([addEntity, openEntity], () => true)
  .on([fxDeleteComparison.done], () => false);

/** Вычисление типа клика для отрисовки форм */
$detailType.on(addEntity, () => 'add-form').on(openEntity, () => 'entity-form');

/** Запрос на создание сверки */
sample({
  clock: addSubmitted,
  fn: (data) => ({
    registry_number: data.registry_number,
    building_id: data.buildings.map((item: any) =>
      typeof item === 'object' ? item.id : item
    ),
    type: data.types && data.types.map((item: any) => item.value),
    starts_at: data.starts_at,
    ends_at: data.ends_at,
    comment: data.comment,
  }),
  target: fxCreateComparison,
});

/** Изменение текущего id сверки после создания */
$currentId.on(fxCreateComparison.doneData, (_, data) => data.comparison_id);

/** Открытие деталей сверки после создания */
sample({
  clock: fxCreateComparison.done,
  fn: (data) => data.result.comparison_id,
  target: [openEntity, getComparisons],
});

/** Открытие сверки по клику на строку таблицы */
sample({
  clock: openEntity,
  fn: (id) => ({ ids: [id] }),
  target: fxGetComparisonById,
});

/** Форматирование деталей после получения информации о сверке */
$opened.on(fxGetComparisonById.doneData, (_, { list }) =>
  formatComparisonEntity(list[0])
);

/** Запрос на удаление сверки */
sample({
  clock: deleteEntity,
  fn: (id) => ({ ids: [id] }),
  target: fxDeleteComparison,
});

/** Запрос на сверки после удаления */
sample({
  clock: fxDeleteComparison.doneData,
  target: getComparisons,
});

/** Сохранение комментария */
sample({
  clock: saveSubmitted,
  fn: (data) => ({
    id: data.id,
    comment: data.comment,
  }),
  target: fxUpdateComparison,
});

reset({
  clock: openEntity,
  target: [$mode],
});

reset({
  clock: [pageUnmounted, signout],
  target: [$isDetailOpen, $mode, $opened, $isDetailLoading, $detailType, $currentId],
});
