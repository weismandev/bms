import { createEffect, createEvent, createStore } from 'effector';
import { inFlight } from 'patronum';
import { createDetailBag } from '@tools/factories';
import {
  ISetComparisonCommentPayload,
  ISetComparisonCommentResult,
} from '@features/counters-check/types/comparisons';

export const fxCreateComparison = createEffect<
  IComparisonCreateRequest,
  IComparisonCreateResponse,
  Error
>();
export const fxDeleteComparison = createEffect<
  IComparisonDeleteRequest,
  IComparisonDeleteResponse,
  Error
>();
export const fxUpdateComparison = createEffect<
  ISetComparisonCommentPayload,
  ISetComparisonCommentResult,
  Error
>();

export const $isDetailLoading = inFlight({
  effects: [fxCreateComparison, fxDeleteComparison],
});
export const $detailType = createStore('');
export const $currentId = createStore<number | null>(null);

export const addEntity = createEvent();
export const openEntity = createEvent<Comparison['id']>();
export const addSubmitted = createEvent<any>();
export const saveSubmitted = createEvent();
export const deleteEntity = createEvent<Comparison['id']>();

export const newEntity = {
  registry_number: '',
  object: '',
  buildings: '',
  types: '',
  starts_at: '',
  ends_at: '',
  comment: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  open,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
