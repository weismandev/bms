import { merge, sample } from 'effector';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { comparisonsApi } from '@features/counters-check/api';
import { fxCreateComparison, fxDeleteComparison } from '../detail/detail.model';
import { $filters } from '../filter/filter.model';
import { $tableParams, $rowCount } from '../table';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  pageMounted,
  pageUnmounted,
  $comparisons,
  fxGetComparisonsList,
  fxGetComparisonById,
  getComparisons,
} from './page.model';
import {
  $settingsInitialized,
  $isGettingSettingsFromStorage,
} from '../user-settings/user-settings.model';

fxGetComparisonsList.use(comparisonsApi.getComparisonsList);
fxGetComparisonById.use(comparisonsApi.getComparisonById);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

/** Технические сторы */
const errorOccured = merge([
  fxGetComparisonsList.fail,
  fxCreateComparison.fail,
  fxDeleteComparison.fail,
]);
$error.on(errorOccured, (_, { error }) => error);
$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true);

$rowCount.on(fxGetComparisonsList.doneData, (_, data) => data?.meta.total || 0);

/** Запрос на сверки */
sample({
  clock: [pageMounted, getComparisons, $settingsInitialized],
  source: {
    tableParams: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ tableParams, filters }) => {
    const filtersStore: ComparisonsFilters = filters;

    return {
      page: tableParams.page,
      per_page: tableParams.per_page,
      search: tableParams.search,
      building_id:
        filtersStore.building_id && filtersStore.building_id.map((item: any) => item.id),
      types: filtersStore.type && filtersStore.type.map((item: any) => item.value),
      starts_at: filtersStore.starts_at,
      ends_at: filtersStore.ends_at,
      status:
        filters.status && typeof filters.status === 'object' ? filters.status.id : '',
    };
  },
  target: fxGetComparisonsList,
});

/** Обработка сверок */
$comparisons.on(fxGetComparisonsList.doneData, (_, data) => data);

reset({
  clock: [pageUnmounted, signout],
  target: [$comparisons, $isLoading, $error, $isErrorDialogOpen],
});
