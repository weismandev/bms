import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { inFlight } from 'patronum';

export const path = '/comparisons';

export const PageGate = createGate();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const openComparison = createEvent<number>();
export const changedErrorDialogVisibility = createEvent<boolean>();
export const openDeatail = createEvent<number>();
export const closeDetail = createEvent<void>();
export const getComparisons = createEvent();

export const fxGetComparisonsList = createEffect<
  IComparisonsListRequest,
  IComparisonsListResponse,
  Error
>();
export const fxGetComparisonById = createEffect<
  IComparisonGetRequest,
  IComparisonGetResponse,
  Error
>();

export const $comparisons = createStore<IComparisonsListResponse>({
  links: {},
  list: [],
  meta: {},
});
export const $isLoading = inFlight({
  effects: [fxGetComparisonsList, fxGetComparisonById],
});
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
