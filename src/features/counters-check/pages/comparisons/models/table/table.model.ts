import { createStore, createEvent } from 'effector';
import i18n from '@shared/config/i18n';
import { createTableBag } from '@tools/factories';

const { t } = i18n;

export const columns = [
  { title: t('Results'), name: 'id' },
  { title: t('startDate'), name: 'starts_at' },
  { title: t('expirationDate'), name: 'ends_at' },
  { title: t('AnObject'), name: 'buildings' },
  { title: t('TotalApartments'), name: 'apartments_in_registry' },
  { title: t('ReconciliationID'), name: 'registry_number' },
  { title: t('MeterType'), name: 'types' },
  { title: t('Label.status'), name: 'status' },
];

export const columnsWidth = [
  { columnName: 'id', width: 140 },
  { columnName: 'starts_at', width: 140 },
  { columnName: 'ends_at', width: 180 },
  { columnName: 'priority', width: 200 },
  { columnName: 'apartments_in_registry', width: 160 },
  { columnName: 'class', width: 200 },
  { columnName: 'type', width: 200 },
  { columnName: 'buildings', width: 320 },
  { columnName: 'status', width: 200 },
];

export const hiddenColumns = ['priority'];

export const $tableData = createStore<any>([]);
export const $rowCount = createStore<number>(0);
export const $selection = createStore([]);

export const selectionChanged = createEvent<any>();

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  columnOrderChanged,
  $search,
  $columnOrder,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
} = createTableBag(columns, {
  widths: columnsWidth,
  order: [],
  pageSize: 25,
  hiddenColumns,
});
