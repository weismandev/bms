import { guard, sample } from 'effector';
import { reset } from 'patronum';
import { fxGetFromStorage, signout } from '@features/common';
import { formatComparisons } from '@features/counters-check/lib';
import { filtersSubmitted } from '../filter/filter.model';
import { $comparisons, getComparisons, pageUnmounted } from '../page/page.model';
import {
  $hiddenColumnNames,
  $columnOrder,
  $currentPage,
  $tableData,
  $selection,
  selectionChanged,
  $rowCount,
  $tableParams,
} from './table.model';

$currentPage.reset(filtersSubmitted);

/** Форматирование сверок и проброс в таблицу */
sample({
  source: $comparisons,
  fn: (data) => formatComparisons(data.list),
  target: $tableData,
});

/** Обработка выделенных колонок */
$selection.on(selectionChanged, (_, selection) => selection);

/** Обработка скрытых колонок */
$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.hiddenClmn) ? result.hiddenClmn : state
);

/** Изменение порядка колонок */
$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.orderClmn) ? result.orderClmn : state
);

/** Запрос на сверки при изменении таблицы */
guard({
  source: $tableParams,
  filter: Boolean,
  target: getComparisons,
});

reset({
  clock: [pageUnmounted, signout],
  target: [$tableData, $rowCount, $selection],
});
