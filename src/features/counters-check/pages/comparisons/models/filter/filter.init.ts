import { guard } from 'effector';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { getComparisons, pageUnmounted } from '../page';
import { $filters, $isFilterOpen } from './filter.model';

guard({
  source: $filters,
  filter: Boolean,
  target: getComparisons,
});

reset({
  clock: [pageUnmounted, signout],
  target: [$isFilterOpen, $filters],
});
