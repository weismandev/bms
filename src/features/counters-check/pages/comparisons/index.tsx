import { FC } from 'react';
import { useGate, useStore } from 'effector-react';
import { FilterMainDetailLayout, Loader, ErrorMessage } from '@ui/index';
import { HaveSectionAccess } from '@features/common';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  PageGate,
  changedErrorDialogVisibility,
} from './models/page';
import { $isDetailOpen } from './models/detail';
import { $isFilterOpen } from './models/filter';
import { ComparisonsTable } from './organisms/table';
import { Filter } from './organisms/filter';
import { Detail } from './organisms/detail';

const ComparisonsPage: FC = () => {
  useGate(PageGate);

  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);

  return (
    <>
      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<ComparisonsTable />}
        detail={isDetailOpen && <Detail />}
        params={{
          filterWidth: '400px',
          detailWidth: !isFilterOpen ? 'minmax(370px, 35%)' : '35%',
        }}
      />
    </>
  );
};

const RestrictedComparisonsPage: FC = () => (
  <HaveSectionAccess>
    <ComparisonsPage />
  </HaveSectionAccess>
);

export default RestrictedComparisonsPage;
