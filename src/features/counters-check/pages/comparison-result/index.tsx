//@ts-nocheck
import { FC, useEffect } from 'react';
import { useGate, useStore } from 'effector-react';
import { ErrorMessage, FilterMainDetailLayout, Loader } from '@ui/index';
import { $pathname, HaveSectionAccess, history } from '@features/common';
import i18n from '@shared/config/i18n';
import { currentPageChanged } from '@features/navbar/models';
import {
  $comparison,
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  PageGate,
} from './models/page';
import { $isDetailOpen } from './models/detail';
import { $isFilterOpen } from './models/filter';
import { ComparisonResultTable } from './organisms/table';
import { Filter } from './organisms/filter';
import { Detail } from './organisms/detail';

const { t } = i18n;

const ComparisonPage: FC = () => {
  const pathname = useStore($pathname);
  const comparisonId = pathname.split('/')[2];

  useGate(PageGate, { comparisonId: comparisonId });

  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isLoading = useStore($isLoading);
  const isDetailOpen = useStore($isDetailOpen);
  const isFilterOpen = useStore($isFilterOpen);
  const comparison = useStore($comparison);

  useEffect(() => {
    currentPageChanged({
      title:
        comparison && comparison.comparison?.registry_number
          ? `${t('ReconciliationResults')}: ${comparison.comparison?.registry_number}`
          : t('ReconciliationResults'),
      back: () => history.push('/counters-check'),
    });
  }, [comparison]);

  return (
    <>
      <Loader isLoading={isLoading} />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        onClose={() => changedErrorDialogVisibility(false)}
        error={error}
      />

      <FilterMainDetailLayout
        filter={isFilterOpen && <Filter />}
        main={<ComparisonResultTable />}
        detail={isDetailOpen && <Detail />}
        params={{
          filterWidth: '400px',
          detailWidth: !isFilterOpen ? 'minmax(370px, 35%)' : '35%',
        }}
      />
    </>
  );
};

const RestrictedComparisonPage: FC = () => (
  <HaveSectionAccess>
    <ComparisonPage />
  </HaveSectionAccess>
);

export default RestrictedComparisonPage;
