import { guard, sample } from 'effector';
import { reset } from 'patronum';
import { fxGetFromStorage, signout } from '@features/common';
import { comparisonsApi } from '@features/counters-check/api';
import { formatComparison } from '@features/counters-check/lib';
import { changedDetailVisibility } from '@features/counters-check/pages/comparison-result/models/detail/detail.model';
import { filtersSubmitted } from '../filter/filter.model';
import { $comparison, getComparison, pageUnmounted } from '../page/page.model';
import {
  $hiddenColumnNames,
  $columnOrder,
  $currentPage,
  $tableData,
  $selection,
  selectionChanged,
  verifyMeters,
  fxVerifyMeters,
  markVerified,
  $rowCount,
  $tableParams,
} from './table.model';

fxVerifyMeters.use(comparisonsApi.verifyMeters);

$currentPage.reset([filtersSubmitted, pageUnmounted, signout]);

/** Форматирование сверок и проброс в таблицу */
sample({
  source: $comparison,
  fn: (data) => formatComparison(data.list),
  target: $tableData,
});

/** Обработка скрытых колонок */
$hiddenColumnNames.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.hiddenClmn) ? result.hiddenClmn : state
);

/** Изменение порядка колонок */
$columnOrder.on(fxGetFromStorage.done, (state, { result }) =>
  Array.isArray(result?.orderClmn) ? result.orderClmn : state
);

/** Обработка выделенных колонок */
$selection
  .on(selectionChanged, (_, selection) => selection)
  .on(fxVerifyMeters.doneData, () => []);

/** Верификация счетчиков */
sample({
  source: $selection,
  clock: verifyMeters,
  fn: (source) => source,
  target: fxVerifyMeters.prepend((data) => ({ ids: data })),
});

/** После верификации парсим на верифицированные и ошибочные */
sample({
  clock: fxVerifyMeters.doneData,
  fn: (data) => {
    const verifiedList = data.items.filter((item: Comparison) => item.is_verified);

    if (verifiedList.length) markVerified(verifiedList);
  },
});

/** При верификации отмечаем их флагами в сторе */
$comparison.on(markVerified, (state, data) => ({
  ...state,
  list: state.list.map((item) => {
    return {
      ...item,
      ...data.find((dataItem: Comparison) => dataItem.id == item.id),
    };
  }),
}));

/** Закрытие деталей после верификации */
sample({
  clock: fxVerifyMeters.done,
  fn: () => {
    changedDetailVisibility(false);
  },
});

/** Запрос на сверки при изменении таблицы */
guard({
  source: $tableParams,
  filter: Boolean,
  target: getComparison,
});

reset({
  clock: [pageUnmounted, signout],
  target: [$tableData, $rowCount, $selection],
});
