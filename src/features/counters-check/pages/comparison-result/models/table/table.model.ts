import { createEffect, createEvent, createStore } from 'effector';
import { createTableBag } from '@tools/factories';
import i18n from '@shared/config/i18n';

const { t } = i18n;

export const columns = [
  { title: t('Label.flat'), name: 'apartment_title' },
  { title: t('NamePU'), name: 'device_name' },
  { title: t('TypeOfIndication'), name: 'dict_name_rus' },
  { title: t('serialNumber'), name: 'serial_number' },
  { title: t('ExternalUID'), name: 'ext_guid' },
  { title: t('startDate'), name: 'starts_at' },
  { title: t('expirationDate'), name: 'ends_at' },
  { title: t('CurrentReadings'), name: 'signal_value' },
  { title: t('Label.status'), name: 'status' },
  { title: t('Label.comment'), name: 'comment' },
];

export const columnsWidth = [
  { columnName: 'apartment_title', width: 250 },
  { columnName: 'device_name', width: 220 },
  { columnName: 'dict_name_rus', width: 200 },
  { columnName: 'serial_number', width: 200 },
  { columnName: 'ext_guid', width: 200 },
  { columnName: 'starts_at', width: 200 },
  { columnName: 'ends_at', width: 200 },
  { columnName: 'signal_value', width: 250 },
  { columnName: 'status', width: 150 },
  { columnName: 'comment', width: 200 },
];

export const hiddenColumns = ['ext_guid', 'starts_at', 'ends_at'];

export const $tableData = createStore<any>([]);
export const $rowCount = createStore<number>(0);
export const $selection = createStore([]);

export const selectionChanged = createEvent<any>();
export const verifyMeters = createEvent<any>();
export const markVerified = createEvent<any>();
export const alertNotVerified = createEvent();

export const fxVerifyMeters = createEffect<any, any>();

export const {
  pageNumChanged,
  pageSizeChanged,
  columnWidthsChanged,
  hiddenColumnChanged,
  searchChanged,
  columnOrderChanged,
  $search,
  $columnOrder,
  $currentPage,
  $pageSize,
  $columnWidths,
  $hiddenColumnNames,
  $tableParams,
} = createTableBag(columns, {
  widths: columnsWidth,
  order: [],
  pageSize: 25,
  hiddenColumns,
});
