import { createStore, createEvent, createEffect } from 'effector';
import { createGate } from 'effector-react';
import { inFlight } from 'patronum';

export const path = '/counters-check';

export const PageGate = createGate<ComparisonPageGate>();

export const pageUnmounted = PageGate.close;
export const pageMounted = PageGate.open;

export const openComparison = createEvent<number>();
export const changedErrorDialogVisibility = createEvent<boolean>();
export const openDeatail = createEvent<number>();
export const closeDetail = createEvent<void>();
export const exportComparisons = createEvent<any>();
export const getComparison = createEvent();

export const fxGetComparisonResult = createEffect<
  IComparisonGetResultRequest,
  IComparisonGetResultResponse,
  Error
>();
export const fxGetComparisonById = createEffect<
  IComparisonGetRequest,
  IComparisonGetResponse,
  Error
>();
export const fxExportComparisons = createEffect<IComparisonExportRequest, null, Error>();

export const $comparison = createStore<IComparisonsListResponse>({
  links: {},
  list: [],
  meta: {},
});
export const $externalComparison = createStore<Comparison | null>(null);
export const $comparisonId = createStore<number | null>(null);
export const $isLoading = inFlight({
  effects: [fxGetComparisonResult, fxExportComparisons],
});
export const $error = createStore<null | Error>(null);
export const $isErrorDialogOpen = createStore<boolean>(false);
