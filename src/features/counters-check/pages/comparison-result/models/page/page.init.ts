import { merge, sample } from 'effector';
import FileSaver from 'file-saver';
import { format } from 'date-fns';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { comparisonsApi } from '@features/counters-check/api';
import { $tableParams, $rowCount } from '../table';
import { $filters } from '../filter/filter.model';
import {
  fxSetComparisonValue,
  fxSetComparisonComment,
  $isDetailOpen,
} from '../detail/detail.model';
import {
  $error,
  $isErrorDialogOpen,
  $isLoading,
  changedErrorDialogVisibility,
  pageMounted,
  pageUnmounted,
  $comparison,
  fxGetComparisonResult,
  $comparisonId,
  fxGetComparisonById,
  $externalComparison,
  exportComparisons,
  fxExportComparisons,
  getComparison,
} from './page.model';

fxGetComparisonResult.use(comparisonsApi.getComparisonResult);
fxGetComparisonById.use(comparisonsApi.getComparisonById);
fxExportComparisons.use(comparisonsApi.exportComparisons);

const errorOccured = merge([
  fxGetComparisonResult.fail,
  fxExportComparisons.fail,
  fxSetComparisonValue.fail,
  fxSetComparisonComment.fail,
]);

/** Технические сторы */
$error.on(errorOccured, (_, { error }) => error);
$isErrorDialogOpen
  .on(changedErrorDialogVisibility, (_, visibility) => visibility)
  .on(errorOccured, () => true);

/** Запрос на сверки */
sample({
  source: [$tableParams, $filters, $comparisonId],
  clock: [pageMounted, getComparison],
  fn: ([tableParams, filters, comparisonId], clock: any) => {
    const filtersStore: ComparisonsFilters = filters;
    const id = comparisonId || +clock.comparisonId;

    return {
      page: tableParams.page,
      per_page: tableParams.per_page,
      search: tableParams.search,
      building_id:
        filtersStore.building_id && filtersStore.building_id.map((item: any) => item.id),
      types: filtersStore.type && filtersStore.type.map((item: any) => item.value),
      starts_at: filtersStore.starts_at,
      ends_at: filtersStore.ends_at,
      comparison_id: id,
    };
  },
  target: fxGetComparisonResult,
});

/** Получение результатов сверки */
$comparison.on(fxGetComparisonResult.doneData, (_, data) => data);

/** Изменение количества строк после получение списка результатов */
$rowCount.on(fxGetComparisonResult.doneData, (_, data) => data?.meta.total || 0);

$comparisonId.on(pageMounted, (_, data: any) => +data.comparisonId);

/** Запрос общей информации о сверке при маунте */
sample({
  clock: pageMounted,
  fn: (data) => ({
    ids: [data.comparisonId],
  }),
  target: fxGetComparisonById,
});

//log[fxGetComparisonById]

/** Получение общей информации о сверке */
$externalComparison.on(fxGetComparisonById.doneData, (_, { list }) => ({
  ...list[0],
  editable: list[0].is_verification_available,
}));

/** Запрос на экспорт */
sample({
  source: [$tableParams, $filters, $comparisonId],
  clock: exportComparisons,
  fn: ([tableParams, filters, comparisonId]) => {
    const filtersStore: ComparisonsFilters = filters;

    return {
      search: tableParams.search,
      building_id:
        filtersStore.building_id && filtersStore.building_id.map((item: any) => item.id),
      type: filtersStore.type && filtersStore.type.map((item: any) => item.value),
      starts_at: filtersStore.starts_at,
      ends_at: filtersStore.ends_at,
      comparison_id: comparisonId || 0,
    };
  },
  target: fxExportComparisons,
});

/** Получение экспорта и преобразование в файл */
fxExportComparisons.done.watch(({ result }: { result: any }) => {
  const blob = new Blob([result], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const dateTimeNow = format(new Date(), 'dd.MM.yyyy-HH.mm.ss');
  FileSaver.saveAs(URL.createObjectURL(blob), `counters-check-${dateTimeNow}.xlsx`);
});

reset({
  clock: [pageUnmounted, signout],
  target: [
    $comparison,
    $externalComparison,
    $comparisonId,
    $isLoading,
    $error,
    $isErrorDialogOpen,
    $isDetailOpen,
  ],
});
