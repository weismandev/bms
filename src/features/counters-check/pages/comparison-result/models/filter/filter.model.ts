import { createFilterBag } from '@tools/factories';

export const defaultFilters = {
  object: [],
  building_id: [],
  type: [],
  // starts_at: '',
  // ends_at: '',
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);
