import { createEffect, createEvent, createStore } from 'effector';
import { inFlight } from 'patronum';
import { createDetailBag } from '@tools/factories';

export const fxSetComparisonValue = createEffect<ComparisonValue, any>();
export const fxSetComparisonComment = createEffect<ComparisonComment, any>();

export const $isDetailLoading = inFlight({
  effects: [fxSetComparisonValue, fxSetComparisonComment],
});
export const $detailType = createStore<string>('');
export const $currentId = createStore<number | null>(null);

export const openEntity = createEvent();
export const saveSubmitted = createEvent<any>();

export const newEntity = {
  value: '',
  comment: '',
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  open,
  detailClosed,
  entityApi,
  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);
