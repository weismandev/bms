import { sample, guard } from 'effector';
import { reset } from 'patronum';
import { signout } from '@features/common';
import { comparisonsApi } from '@features/counters-check/api';
import { formatComparisonResultEntity } from '@features/counters-check/lib';
import {
  $comparison,
  fxGetComparisonResult,
  getComparison,
  pageUnmounted,
} from '../page';
import {
  $opened,
  $mode,
  $isDetailOpen,
  openEntity,
  $detailType,
  $isDetailLoading,
  $currentId,
  saveSubmitted,
  fxSetComparisonValue,
  fxSetComparisonComment,
  changeMode,
} from './detail.model';

fxSetComparisonValue.use(comparisonsApi.setComparisonResultValue);
fxSetComparisonComment.use(comparisonsApi.setComparisonResultComment);

$isDetailOpen.on([openEntity], () => true);

// Вычисляем тип клика для отрисовки форм
$detailType.on(openEntity, () => 'entity-form');

$mode.on($opened, () => 'view');

guard({
  source: $currentId,
  clock: fxGetComparisonResult.done,
  filter: (source) => Boolean(source),
  target: openEntity.prepend((data) => data),
});

// При клике на строку ищем сверку и прокидываем в opened
sample({
  source: $comparison,
  clock: openEntity,
  fn: (comparisons: IComparisonsListResponse, id: any) => {
    const entity = comparisons.list.find((item) => item.id === id);

    if (entity) {
      return formatComparisonResultEntity(entity);
    }
  },
  target: $opened,
});

// Сохранение показаний
sample({
  clock: saveSubmitted,
  fn: (data) => {
    fxSetComparisonValue({
      id: data.id,
      value: data.signal_value,
    });
    fxSetComparisonComment({
      id: data.id,
      comment: data.comment,
    });

    changeMode('view');

    getComparison();
  },
});

reset({
  clock: [pageUnmounted, signout],
  target: [$isDetailOpen, $mode, $opened, $isDetailLoading, $detailType, $currentId],
});
