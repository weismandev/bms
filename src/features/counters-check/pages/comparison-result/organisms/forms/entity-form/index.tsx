import { FC } from 'react';
import { combine } from 'effector';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Alert } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import {
  $mode,
  changedDetailVisibility,
  $opened,
  changeMode,
  saveSubmitted,
} from '@features/counters-check/pages/comparison-result/models/detail';
import {
  $externalComparison,
  $isLoading,
} from '@features/counters-check/pages/comparison-result/models/page';
import i18n from '@shared/config/i18n';
import { Loader, DetailToolbar, Wrapper, CustomScrollbar, InputField } from '@ui/index';

const { t } = i18n;

const $stores = combine({
  mode: $mode,
  isLoading: $isLoading,
  opened: $opened,
});

const validationSchema = Yup.object().shape({
  signal_value: Yup.string().nullable().required(t('thisIsRequiredField')),
  comment: Yup.string().nullable(),
});

const useStyles = makeStyles(() => ({
  headerRoot: {
    padding: '16px 0 17px',
    minHeight: 62,
    overflowWrap: 'anywhere',
  },
  headerText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '28px',
  },
}));

const EntityForm: FC = () => {
  const { mode, opened, isLoading } = useStore($stores);
  const externalComparison = useStore($externalComparison);
  const classes = useStyles();

  const initialValues: Comparison = {
    id: opened.id,
    signal_value: opened.signal_value,
    comment: opened.comment,
  };

  // Редактируемая если прошла/не началась/значение подтверждено
  const isVerified = opened.is_verified;

  const isEditable = externalComparison?.editable && !isVerified;

  const renderInfoAlert = () => {
    const verifiedText = t('ValueAlreadyConfirmed');
    const rangeDateText = t('ReconciliationHasPassedOrHasNotYetBegun');

    if (!externalComparison?.editable)
      return (
        <Alert severity="info" style={{ marginBottom: 24 }}>
          {rangeDateText}
        </Alert>
      );

    if (isVerified)
      return (
        <Alert severity="info" style={{ marginBottom: 24 }}>
          {verifiedText}
        </Alert>
      );
  };

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Loader isLoading={isLoading} />

      <Formik
        enableReinitialize
        validationSchema={validationSchema}
        //@ts-ignore
        onSubmit={saveSubmitted}
        //@ts-ignore
        initialValues={initialValues}
        render={({ submitForm }) => (
          <Form style={{ height: '100%' }}>
            <DetailToolbar
              title={opened?.ext_guid || t('Info')}
              onSave={submitForm}
              onCancel={() => changedDetailVisibility(false)}
              onClose={() => changedDetailVisibility(false)}
              onEdit={() => changeMode('edit')}
              // onDelete={() => deleteDialogVisibilityChange(true)}
              hidden={{ delete: true, edit: !isEditable }}
              mode={mode}
            />

            <div
              style={{
                width: 'calc(100% + 19px)',
                height: 'calc(100% - 50px)',
              }}
            >
              <CustomScrollbar style={{ height: '100%' }} autoHide>
                <div
                  style={{
                    width: 'calc(100% - 19px)',
                    paddingTop: 24,
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  {!isEditable && renderInfoAlert()}

                  <Field
                    name="signal_value"
                    label={`${t('CurrentReadings')} (${opened.measure})`}
                    component={InputField}
                    mode={mode}
                  />
                  <Field
                    name="comment"
                    label={t('Label.comment')}
                    component={InputField}
                    divider={false}
                    multiline
                    rowsMax={5}
                    mode={mode}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </Wrapper>
  );
};

export { EntityForm };
