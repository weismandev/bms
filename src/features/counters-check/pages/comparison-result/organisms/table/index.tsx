import { useCallback, ReactNode } from 'react';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  PagingState,
  CustomPaging,
  SelectionState,
  IntegratedSelection,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  TableColumnResizing,
  ColumnChooser,
  TableColumnReordering,
  DragDropProvider,
  TableSelection,
} from '@devexpress/dx-react-grid-material-ui';
import {
  openEntity,
  $opened,
} from '@features/counters-check/pages/comparison-result/models/detail';
import {
  $selection,
  selectionChanged,
  $tableData,
  columns,
  $currentPage,
  pageNumChanged as pageNumberChanged,
  $pageSize,
  pageSizeChanged,
  $rowCount,
  $columnWidths,
  columnWidthsChanged,
  $hiddenColumnNames,
  hiddenColumnChanged,
  $columnOrder,
  columnOrderChanged,
} from '@features/counters-check/pages/comparison-result/models/table';
import i18n from '@shared/config/i18n';
import {
  Wrapper,
  TableCell,
  TableRow,
  TableHeaderCell,
  TableContainer,
  TableColumnVisibility,
  PagingPanel,
} from '@ui/index';
import { TableToolbar } from '../table-toolbar';
import { useStyles } from './styles';

const { t } = i18n;

interface RowProps {
  children: ReactNode;
  row: {
    id: number;
  };
}

const Row = (props: RowProps) => {
  const opened = useStore($opened);
  const row: Comparison = props.row;
  const isSelected = opened && opened.id === row.id;
  const onRowClick = useCallback((_, id) => openEntity(id), []);

  const colors = {
    green: 'rgba(27, 177, 105, 0.15)',
    orange: 'rgba(236, 127, 0, 0.15)',
    white: 'white',
  };

  const calcColor = () => {
    if (!row.signal_value && !row.status) return colors.white;
    else if (row.signal_value && !row.status) return colors.orange;
    else if (row.signal_value && row.status) return colors.green;
  };

  return (
    <TableRow
      {...props}
      onRowClick={onRowClick}
      isSelected={isSelected}
      style={{
        background: `${calcColor()}`,
      }}
    />
  );
};

const Root = ({ children, ...props }: { children?: ReactNode }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const ToolbarRoot = (props: ReactNode) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      marginBottom: '12px',
      minHeight: '36px',
      height: '36px',
      flexWrap: 'nowrap',
      borderBottom: 'none',
    }}
  />
);

const ComparisonResultTable: React.FC = () => {
  const data = useStore($tableData);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const rowCount = useStore($rowCount);
  const columnWidths = useStore($columnWidths);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const columnOrder = useStore($columnOrder);
  const selection = useStore($selection);

  const classes = useStyles();

  return (
    <Wrapper className={classes.wrapper}>
      <Grid
        rootComponent={Root}
        rows={data}
        getRowId={(row) => row && row.id}
        columns={columns}
      >
        <SelectionState selection={selection} onSelectionChange={selectionChanged} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />

        <CustomPaging totalCount={rowCount} />

        <IntegratedSelection />

        <DragDropProvider />

        <Table
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />

        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />

        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />

        <TableHeaderRow cellComponent={TableHeaderCell} />

        <TableSelection showSelectAll />

        <TableColumnVisibility
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />

        <Toolbar rootComponent={ToolbarRoot} />

        <Template name="toolbarContent">
          <TableToolbar />
        </Template>

        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />

        <PagingPanel {...{ columns, hiddenColumnNames }} />
      </Grid>
    </Wrapper>
  );
};

export { ComparisonResultTable };
