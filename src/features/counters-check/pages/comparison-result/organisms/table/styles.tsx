import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    padding: 24,

    '& .MuiTableCell-head': {
      whiteSpace: 'normal',
    },
  },
  smile: {
    marginLeft: 10,
    width: 30,
    height: 30,
  },
}));

export { useStyles };
