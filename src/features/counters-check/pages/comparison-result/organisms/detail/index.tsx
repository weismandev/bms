import { useStore } from 'effector-react';
import { Loader } from '@ui/index';
import { $isLoading, $detailType } from '../../models';
import { EntityForm } from '../forms/entity-form/index';

const Detail = () => {
  const detailType = useStore($detailType);
  const isLoading = useStore($isLoading);

  const renderForms = () => {
    switch (detailType) {
      case 'entity-form':
        return <EntityForm />;
      default:
        return null;
    }
  };

  return (
    <>
      <Loader isLoading={isLoading} />
      {renderForms()}
    </>
  );
};

export { Detail };
