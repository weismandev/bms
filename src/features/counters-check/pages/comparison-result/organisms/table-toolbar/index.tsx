import { FC } from 'react';
import { useStore } from 'effector-react';
import { Tooltip } from '@mui/material';
import i18n from '@shared/config/i18n';
import {
  Toolbar,
  FilterButton,
  Greedy,
  AdaptiveSearch,
  ExportButton,
  ActionButton,
} from '@ui/index';
import {
  changedFilterVisibility,
  $isFilterOpen,
  $comparison,
  $isDetailOpen,
  $externalComparison,
  exportComparisons,
} from '../../models';
import {
  $search,
  searchChanged,
  $hiddenColumnNames,
  $tableData,
  columns,
  verifyMeters,
  $selection,
} from '../../models/table';

const { t } = i18n;

const TableToolbar: FC = () => {
  const tableData = useStore($tableData);
  const comparisons = useStore($comparison);
  const totalCount = comparisons.meta.total;
  const searchValue = useStore($search);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const selection = useStore($selection);
  const externalComparison = useStore($externalComparison);

  const isAnySideOpen = isFilterOpen && isDetailOpen;

  const renderVerifyButton = () => {
    if (!externalComparison || !externalComparison.editable)
      return (
        <Tooltip title={t('ConfirmationNotAvailable') as string}>
          <div>
            <ActionButton
              kind="positive"
              onClick={() => verifyMeters()}
              disabled={true}
              style={{
                width: 'max-content',
                minWidth: 'unset',
                padding: '0 20px 0 20px',
              }}
            >
              {t('Confirm')}
            </ActionButton>
          </div>
        </Tooltip>
      );
    else
      return (
        <ActionButton
          kind="positive"
          onClick={() => verifyMeters()}
          disabled={!selection.length}
          style={{
            width: 'max-content',
            minWidth: 'unset',
            padding: '0 20px 0 20px',
          }}
        >
          {t('Confirm')}
        </ActionButton>
      );
  };
  return (
    <Toolbar>
      <Tooltip title={t('Filters') as string}>
        <div>
          <FilterButton
            disabled={isFilterOpen}
            style={{ marginRight: 10 }}
            onClick={() => changedFilterVisibility(true)}
          />
        </div>
      </Tooltip>
      <AdaptiveSearch {...{ totalCount, searchValue, searchChanged, isAnySideOpen }} />
      <Greedy />
      {renderVerifyButton()}
      <ExportButton
        {...{ columns, tableData, hiddenColumnNames }}
        onClick={exportComparisons}
        disabled={!totalCount}
        title={t('ExportNoMoreThan50000Records')}
        style={{ marginRight: 10, marginLeft: 10 }}
      />
    </Toolbar>
  );
};

export { TableToolbar };
