declare type ComparisonTypes = 'heat' | 'water' | 'energy' | 'gas';

declare type ComparisonPageGate = {
  comparisonId: Comparison['id'];
};

declare type Comparison = {
  id: number;
  resource_type?: string;
  type?: string;
  value?: any;
  device_name?: string;
  signal_value?: any;
  is_verified?: boolean;
  starts_at?: string;
  ends_at?: string;
  serial_number?: string;
  ext_guid?: string;
  apartment_title?: string;
  dict_name?: string;
  dict_name_rus?: string;
  measure?: string;
  comment?: string;
  //
  registry_number?: string;
  types?: ComparisonTypes[] | { value: ComparisonTypes; title: string }[];
  buildings?: any;
  apartments_in_registry?: number;
  status?: boolean;
  message?: string;
  //
  is_verification_available?: boolean;
  editable?: boolean;
};

declare type ComparisonValue = {
  id: number;
  value: string;
};

declare type ComparisonComment = {
  id: number;
  comment: string;
};

// Get result

declare interface IComparisonGetResultRequest {
  page: number;
  per_page: number;
  search?: string;
  building_id?: number[];
  type?: ComparisonTypes[];
  starts_at?: string;
  ends_at?: string;
  comparison_id?: number;
}

declare interface IComparisonGetResultResponse extends IComparisonsDefaultResponse {}

// Get

declare interface IComparisonGetRequest {
  ids: Comparison['id'][];
}

declare interface IComparisonGetResponse extends IComparisonsDefaultResponse {}

// Export

declare interface IComparisonExportRequest {
  search?: string;
  building_id?: number[];
  type?: ComparisonTypes[];
  starts_at?: string;
  ends_at?: string;
  comparison_id?: Comparison['id'];
}

// Create
declare interface IComparisonCreateRequest {
  registry_number: string;
  building_id: number[];
  type: ComparisonTypes[];
  starts_at: string;
  ends_at: string;
  comment: string;
}

declare interface IComparisonCreateResponse {
  success: boolean;
  comparison_id: number;
}

// Delete

declare interface IComparisonDeleteRequest {
  ids: Comparison['id'][];
}

declare interface IComparisonDeleteResponse {
  success: boolean;
  result: {
    id: Comparison['id'];
    deleted: boolean;
    message: string;
  };
}
