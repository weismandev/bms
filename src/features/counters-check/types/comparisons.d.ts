import { bool } from 'yup';

declare type ComparisonsFilters = {
  object: number[];
  building_id: number[];
  type: ComparisonTypes[];
  starts_at: string;
  ends_at: string;
};

declare interface IComparisonsDefaultResponse {
  links: {
    first?: string | null;
    last?: string | null;
    next?: string | null;
    prev?: string | null;
  };
  list: Comparison[];
  meta: {
    current_page?: number;
    from?: number;
    last_page?: number;
    path?: string;
    per_page?: number;
    to?: number;
    total?: number;
  };
}

declare interface IComparisonsListRequest {
  page: number;
  per_page: number;
  search?: string;
  building_id?: number[];
  type?: ComparisonTypes[];
  starts_at?: string;
  ends_at?: string;
  ids?: number[];
}

declare interface IComparisonsListResponse extends IComparisonsDefaultResponse {}

declare interface ISetComparisonCommentPayload {
  id: Comparison['id'];
  comment: string;
}

declare interface ISetComparisonCommentResult {
  success: boolean;
  registry_id: number;
}
