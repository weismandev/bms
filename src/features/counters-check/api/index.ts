import { api } from '@api/api2';
import {
  IComparisonsListRequest,
  ISetComparisonCommentPayload,
} from '@features/counters-check/types/comparisons';

// Comparisons
const getComparisonsList = (payload: IComparisonsListRequest) =>
  api.v1('get', 'comparison-of-meters/comparison-list', payload);
const getComparisonById = (payload: IComparisonGetRequest) =>
  api.v1('get', 'comparison-of-meters/comparison-list', payload);
const createComparison = (payload: IComparisonCreateRequest) =>
  api.v1('post', 'comparison-of-meters/create', payload);
const deleteComparison = (payload: IComparisonDeleteRequest) =>
  api.v1('post', 'comparison-of-meters/delete', payload);
const setComparisonComment = (payload: ISetComparisonCommentPayload) =>
  api.v1('post', 'comparison-of-meters/update-comparison-comment', payload);

// Comparison (Registry)
const getComparisonResult = (payload: IComparisonsListRequest) =>
  api.v1('get', 'comparison-of-meters/registry-list', payload);
const setComparisonResultValue = (payload: Comparison) => {
  api.v1('post', 'comparison-of-meters/set-value', payload);
};
const setComparisonResultComment = (payload: Comparison) =>
  api.v1('post', 'comparison-of-meters/update-comment', payload);
const verifyMeters = (payload: { ids: number[] }) =>
  api.v1('post', 'comparison-of-meters/verify', payload);

//
const exportComparisons = (payload: any) =>
  api.v1(
    'request',
    'comparison-of-meters/export-registry-list',
    { ...payload },
    {
      config: {
        responseType: 'arraybuffer',
      },
      method: 'get',
    }
  );

export const comparisonsApi = {
  getComparisonsList,
  getComparisonById,
  createComparison,
  deleteComparison,
  getComparisonResult,
  setComparisonComment,
  setComparisonResultValue,
  setComparisonResultComment,
  verifyMeters,
  exportComparisons,
};
