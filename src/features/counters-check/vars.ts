import i18n from '@shared/config/i18n';

const { t } = i18n;

export const comparisonTypes = [
  { value: 'heat', title: t('Heat') },
  { value: 'water', title: t('Water') },
  { value: 'energy', title: t('Electricity') },
  { value: 'gas', title: t('gas') },
];
