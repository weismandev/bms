import i18n from '@shared/config/i18n';
import { comparisonTypes } from '../vars';

const { t } = i18n;

const formatDate = (date: any) => date.split('-').reverse().join('.');

export const formatComparisons = (comparisons: Comparison[]) =>
  comparisons.map((item) => ({
    id: item.id,
    registry_number: item.registry_number,
    starts_at: formatDate(item.starts_at),
    ends_at: formatDate(item.ends_at),
    comment: item.comment,
    types:
      item.types &&
      item.types.map((item) => {
        const type = comparisonTypes.find((type) => type.value == item);

        if (type) return type.title;

        return null;
      }),
    buildings: item.buildings,
    apartments_in_registry: item.apartments_in_registry,
    status: item.status,
  }));

export const formatComparisonEntity = (comparison: Comparison) => ({
  id: comparison.id,
  registry_number: comparison.registry_number,
  starts_at: comparison.starts_at,
  ends_at: comparison.ends_at,
  comment: comparison.comment,
  types: comparison.types,
  buildings: Array.isArray(comparison.buildings)
    ? comparison.buildings.map((item) => item?.id)
    : [],
  apartments_in_registry: comparison.apartments_in_registry,
});

export const formatComparison = (comparisons: Comparison[]) =>
  comparisons.map((item) => ({
    id: item.id,
    apartment_title: item.apartment_title,
    dict_name_rus: item.dict_name_rus,
    serial_number: item.serial_number,
    ext_guid: item.ext_guid,
    starts_at: formatDate(item.starts_at),
    ends_at: formatDate(item.ends_at),
    signal_value: item.value
      ? `${item.value} ${item.measure}`
      : item.signal_value
      ? `${item.signal_value} ${item.measure}`
      : '',
    status: item.is_verified ? t('ConfirmedBy') : '',
    comment: item.comment,
    device_name: item?.device_name || '',
  }));

export const formatComparisonResultEntity = (comparison: Comparison) => ({
  ...comparison,
  signal_value: comparison.value
    ? `${comparison.value}`
    : comparison.signal_value
    ? `${comparison.signal_value}`
    : '',
  measure: comparison.measure,
  comment: comparison.comment,
});

export function startDateAfterFinishDate(this: {
  createError: any;
  parent: {
    starts_at: string;
    ends_at: string;
  };
}) {
  const isDatesDefined = Boolean(this.parent.starts_at) && Boolean(this.parent.ends_at);

  if (
    isDatesDefined &&
    Number(new Date(this.parent.starts_at)) >= Number(new Date(this.parent.ends_at))
  ) {
    return this.createError();
  }

  return true;
}

export const checkTodayIsBefore = (date: string) => {
  const today = new Date();
  const compareDate = new Date(date);

  return today < compareDate;
};

export const checkTodayIsNotInRange = (startDate: string, endDate: string) =>
  !checkTodayIsBefore(startDate) && !checkTodayIsBefore(endDate);
