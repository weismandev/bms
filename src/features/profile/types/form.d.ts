declare interface IProfileUpdateUserPayload {
  user_mail: string;
  user_phone: number;
  user_fullname: string;
}

declare interface IProfileUpdateUserResponse {
  activated_at: string;
  birthday: any;
  created_at: string;
  gender_id: string;
  goto_phone: any;
  goto_phone_only_out: any;
  need_restart: any;
  provider_id: string;
  userdata_access: string;
  userdata_acms: string;
  userdata_avatar: string;
  userdata_debug: any;
  userdata_email: string;
  userdata_enabled: string;
  userdata_enabled_date: any;
  userdata_ext_guid: string;
  userdata_ext_id: string;
  userdata_ext_token: any;
  userdata_fullname: string;
  userdata_id: string;
  userdata_jsondata: string;
  userdata_md5: string;
  userdata_name: string;
  userdata_need_demo_dt: string;
  userdata_parent_id: string;
  userdata_patronymic: string;
  userdata_phone: string;
  userdata_salt: string;
  userdata_search_disable: string;
  userdata_sip_info: any;
  userdata_surname: string;
  userdata_type_id: any;
  userdata_use_last: string;
  userdata_verifyemail: string;
  userdata_verifyemailtext: string;
  userdata_verifysms: string;
  userdata_verifysmsnumber: string;
}

declare interface IProfileUpdateAuthPayload {
  oldpass: string;
  newpass: string;
}

declare interface IProfileUpdateAvatarPayload {
  user_avatar: any;
}
