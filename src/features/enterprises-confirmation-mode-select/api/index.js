import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.v1('get', 'enterprises/admin/confirmation-modes', payload);

export const enterprisesModeApi = { getList };
