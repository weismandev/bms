import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $options, $isLoading, $error, PageGate } from '../../model/select';

const JournalUserActionsBuildingSelect = (props) => {
  const options = useStore($options);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  return (
    <>
      <PageGate />
      <SelectControl options={options} isLoading={isLoading} error={error} {...props} />
    </>
  );
};

const JournalUserActionsBuildingSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<JournalUserActionsBuildingSelect />}
      onChange={onChange}
      {...props}
    />
  );
};

export { JournalUserActionsBuildingSelect, JournalUserActionsBuildingSelectField };
