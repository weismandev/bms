import { api } from '../../../api/api2';

const getBuilding = () => {
  return api.v1('get', 'journal/filter/get-building-list');
};

export const journalUserActionsSelectApi = {
  getBuilding,
};
