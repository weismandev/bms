import { createStore, createEffect } from 'effector';
import { signout } from '../../common';
import { buildingsApi } from '../api';

const fxGetList = createEffect();

const $data = createStore([]);
const $error = createStore(null);
const $isLoading = createStore(false);

$data
  .on(fxGetList.done, (state, { result }) => {
    if (Array.isArray(result.buildings) && result.buildings.length) {
      return result.buildings;
    }

    return state;
  })
  .reset(signout);

$error.on(fxGetList.fail, (state, { error }) => error).reset(signout);
$isLoading.on(fxGetList.pending, (state, result) => result).reset(signout);

fxGetList.use(buildingsApi.getList);

export { fxGetList, $data, $error, $isLoading };
