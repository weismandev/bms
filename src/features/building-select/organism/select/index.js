import { useEffect } from 'react';
import { useStore } from 'effector-react';
import { SelectControl, SelectField } from '../../../../ui';
import { $data, $isLoading, $error, fxGetList } from '../../model';

const BuildingSelect = (props) => {
  const data = useStore($data);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  useEffect(() => {
    fxGetList();
  }, []);

  return <SelectControl options={data} isLoading={isLoading} error={error} {...props} />;
};

const BuildingSelectField = (props) => {
  const onChange = (value) => props.form.setFieldValue(props.field.name, value);

  return (
    <SelectField
      component={<BuildingSelect />}
      getOptionLabel={(option) =>
        option && `${option.complex_title}, ${option.building_title}`
      }
      onChange={onChange}
      {...props}
    />
  );
};

export { BuildingSelect, BuildingSelectField };
