import { api } from '../../../api/api2';

const getList = () => api.v1('get', 'business-center/buildings/related');

export const buildingsApi = { getList };

// Селектор предназначен для использования в ЛК Компаний
