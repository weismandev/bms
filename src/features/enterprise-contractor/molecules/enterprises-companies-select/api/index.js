import { api } from '../../../../../api/api2';

const getList = (payload = {}) =>
  api.no_vers('get', '/v1/enterprises/get-list', {
    ...payload,
    type: payload.type,
  });

export const enterprisesCompaniesApi = { getList };
