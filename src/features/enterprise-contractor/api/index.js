import { api } from '../../../api/api2';

const getList = (payload = {}) =>
  api.no_vers('get', '/v1/enterprises/get-list', {
    ...payload,
    type: payload.type || 'contractor',
  });

const getById = (id) => api.no_vers('get', '/v1/enterprises/get', { id });

const create = (payload = {}) =>
  api.no_vers('post', '/v1/enterprises/admin/contractor/create', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const update = (payload = {}) =>
  api.no_vers('post', '/v1/enterprises/admin/contractor/update', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const deleteById = (id) => api.no_vers('post', '/v1/enterprises/delete', { id });

const getParkingSlots = (payload) =>
  api.v1('get', 'parking/enterprise_rent/enterprise-slots', {
    ...payload,
    type: 'all',
  });

const addParkingSlot = (payload) =>
  api.v1('post', 'parking/enterprise_rent/add-slot-to-enterprise', payload);

const getVehicle = (payload = {}) => api.no_vers('get', '/v1/vehicle/search', payload);

const createVehicle = (payload = {}) => api.no_vers('get', '/v1/vehicle/create', payload);

const deleteParkingSlot = (payload) =>
  api.v1('post', 'parking/enterprise_rent/delete-slot-rent-from-enterprise', payload);

const extendParkingSlotRent = (payload) =>
  api.v1('post', 'parking/enterprise_rent/extend-rent', payload, {
    headers: { 'Content-Type': 'application/json' },
  });

const updateVehicle = (payload = {}) => api.no_vers('get', '/v1/vehicle/update', payload);

/* START attachments */

const createFolder = (payload) =>
  api.no_vers('post', '/v1/enterprises/attachments/folders/create', payload);

const updateFolder = (payload) =>
  api.no_vers('post', '/v1/enterprises/attachments/folders/update', payload);

const addAttachment = (payload) =>
  api.v1('post', '/enterprises/admin/attachment/add', payload);

const deleteFolder = (payload) =>
  api.no_vers('post', '/v1/enterprises/attachments/folders/delete', payload);

const deleteFile = (payload) =>
  api.no_vers('post', '/v1/enterprises/admin/attachment/delete', payload);

const downloadAttachment = (payload) => fetch(payload.url).then((response) => {});

/* END attachments */

const exportList = (payload = {}) =>
  api.v1('request', 'enterprises/get-list', payload, {
    config: {
      responseType: 'arraybuffer',
    },
    method: 'get',
  });

const preloadFile = (file) => api.v1('post', 'enterprises/admin/contractor-import', { import: file });
const getVehicleList = (payload) => api.v4('get', 'enterprises/vehicles/list', payload);
const saveVehicles = (payload) => api.v4('post', 'enterprises/vehicles/save', payload);

export const contractingApi = {
  getList,
  getById,
  create,
  update,
  delete: deleteById,
  exportList,
  preloadFile,
  getParkingSlots,
  addParkingSlot,
  deleteParkingSlot,
  extendParkingSlotRent,
  getVehicle,
  createVehicle,
  updateVehicle,
  createFolder,
  updateFolder,
  addAttachment,
  deleteFolder,
  deleteFile,
  downloadAttachment,
  getVehicleList,
  saveVehicles,
};
