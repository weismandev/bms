import { useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Formik, Form, Field } from 'formik';
import { FormControlLabel } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import {
  Wrapper,
  FilterToolbar,
  FilterFooter,
  CustomScrollbar,
  Checkbox,
  InputField,
} from '../../../../ui';
import { ServicesSelectField, ServiceItemsSelectField } from '../../../services-select';
import {
  changedFilterVisibility,
  filtersSubmitted,
  defaultFilters,
  $filters,
} from '../../models/filter.model';

const useStyles = makeStyles({
  formWrapper: {
    padding: 24,
    paddingTop: 0,
    '& .MuiFormControlLabel-root': {
      marginBottom: 20,
    },
  },
});

const Filter = () => {
  const { t } = useTranslation();
  const { formWrapper } = useStyles();
  const filters = useStore($filters);
  const [filterValues, setFilters] = useState(filters);

  const handleChange = (name) => (e) => {
    setFilters({
      ...filterValues,
      [name]: e.target.checked,
    });
  };

  const handleFiltersSubmitted = (values) => {
    const services = values.service_id.reduce(
      (acc, current) => ({ ...acc, [current.id]: {} }),
      {}
    );
    const servicesItems = values.service_item_id.reduce((acc, current) => {
      const { items = [] } = acc[current.parentId] || {};
      return {
        ...acc,
        [current.parentId]: {
          items: items.concat(current.id),
        },
      };
    }, {});
    const description = values.service_other_id
      ? { 16: { description: values.service_other_id } }
      : {};

    filtersSubmitted({
      ...values,
      services: {
        ...services,
        ...servicesItems,
        ...description,
      },
    });
  };

  const handleReset = () => {
    filtersSubmitted(defaultFilters);
    setFilters(defaultFilters);
  };

  const computeServices = (services) => {
    return (
      services?.length > 0 &&
      services.filter((item) => item.item_group && item.item_group.items).length > 0
    );
  };
  const computeOtherServices = (services) => {
    return services?.length > 0 && services.find(({ id }) => id === 16);
  };

  return (
    <Wrapper style={{ height: '100%' }}>
      <CustomScrollbar autoHide>
        <FilterToolbar
          style={{ padding: 24 }}
          closeFilter={() => changedFilterVisibility(false)}
        />
        <Formik
          initialValues={filterValues}
          onSubmit={handleFiltersSubmitted}
          onReset={handleReset}
          enableReinitialize
          render={({ values }) => {
            // const isAdditionalServices = computeServices(values.service_id);
            // const isOtherWorksServices = computeOtherServices(
            //   values.service_id
            // );
            return (
              <Form className={formWrapper}>
                {/* <Field
                  name="service_id"
                  component={ServicesSelectField}
                  label="Виду работ"
                  placeholder="Выберите вид работ"
                  isMulti
                />
                {isAdditionalServices && (
                  <Field
                    name="service_item_id"
                    component={ServiceItemsSelectField}
                    label="Вид оборудования"
                    placeholder="Выберите вид оборудования"
                    isMulti
                  />
                )}
                {isOtherWorksServices && (
                  <Field
                    name="service_other_id"
                    component={InputField}
                    label="Уточните работы"
                    placeholder="Введите иные работы"
                  />
                )} */}
                <FormControlLabel
                  label={t('HavingCar')}
                  control={
                    <Checkbox
                      name="has_vehicle"
                      size="small"
                      onChange={handleChange('has_vehicle')}
                      checked={filterValues.has_vehicle}
                    />
                  }
                />
                <FormControlLabel
                  label={t('AreResidents')}
                  control={
                    <Checkbox
                      name="is_resident"
                      size="small"
                      onChange={handleChange('is_resident')}
                      checked={filterValues.is_resident}
                    />
                  }
                />
                <FormControlLabel
                  label={t('AvailabilityOfParkingSpace')}
                  control={
                    <Checkbox
                      name="has_parking"
                      size="small"
                      onChange={handleChange('has_parking')}
                      checked={filterValues.has_parking}
                    />
                  }
                />
                <FormControlLabel
                  label={t('ShowArchive')}
                  control={
                    <Checkbox
                      name="is_archived"
                      size="small"
                      onChange={handleChange('is_archived')}
                      checked={filterValues.is_archived}
                    />
                  }
                />
                <FormControlLabel
                  label={t('ShowBlacklist')}
                  control={
                    <Checkbox
                      name="is_banned"
                      size="small"
                      onChange={handleChange('is_banned')}
                      checked={filterValues.is_banned}
                    />
                  }
                />
                <FilterFooter isResettable={true} />
              </Form>
            );
          }}
        />
      </CustomScrollbar>
    </Wrapper>
  );
};

export { Filter };
