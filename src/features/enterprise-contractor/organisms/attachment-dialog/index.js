import { useRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import cn from 'classnames';
import { List, ListItem } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import DescriptionIcon from '@mui/icons-material/Description';
import VerticalAlignBottomIcon from '@mui/icons-material/VerticalAlignBottom';
import VerticalAlignTopIcon from '@mui/icons-material/VerticalAlignTop';
import makeStyles from '@mui/styles/makeStyles';
import FolderIcon from '../../../../img/folder';
import {
  ActionButton,
  CloseButton,
  CustomModal,
  Checkbox,
  Loader,
  ErrorMsg as ErrorMessage,
} from '../../../../ui';
import formatBytes from '../../lib/formatBytes';
import {
  $attachments,
  createFolder,
  updateFolder,
  deleteFolder,
  deleteFile,
  uploadAttachment,
  downloadAttachment,
  $isLoading,
  $error,
  resetErrorMessage,
} from '../../models/attachment.modal';

const useStyles = makeStyles({
  content: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px 24px',
    '& p': {
      margin: 0,
      fontWeight: 500,
      fontSize: 13,
    },
    '& > p:first-child': {
      flex: '0 0 50%',
    },
    '& hr': {
      display: 'none',
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 24,
    '& > span': {
      color: '#3B3B50',
      fontSize: 24,
    },
  },
  contentHeader: {
    padding: '0px 24px 24px',
  },
  contentTitle: {
    color: '#7D7D8E',
    fontSize: '18px !important',
    fontWeight: 900,
  },
  contentBody: {
    color: '#65657B',
    fontSize: 13,
    // marginBottom: 240,
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-start',
    '& button': {
      marginRight: 16,
    },
  },
  wrapCustomModal: {
    '& .MuiDialogActions-root': {
      justifyContent: 'flex-start',
      padding: 24,
    },
  },
  wrapCreateFolder: {
    display: 'flex',
    padding: '10px 24px',
    '& > div': {
      width: '45%',
      marginLeft: 10,
      '& hr': {
        display: 'none',
      },
    },
  },
  indentTitle: {
    marginLeft: 6,
  },
});

const initSelectItem = {
  folder: null,
  file: null,
};

function download(fileUrl, fileName) {
  const a = document.createElement('a');
  a.href = fileUrl;
  a.setAttribute('download', fileName);
  a.click();
}

const AttachmentDialog = ({ isOpen, enterprise_id, onClose }) => {
  const {
    header,
    content,
    contentTitle,
    contentHeader,
    contentBody,
    actions,
    wrapCustomModal,
    wrapCreateFolder,
    indentTitle,
  } = useStyles();
  const { t } = useTranslation();
  const attachments = useStore($attachments);
  const isLoading = useStore($isLoading);
  const error = useStore($error);

  const inputFileElement = useRef(null);
  const inputFolderCreate = useRef(null);
  const inputFolderEdit = useRef(null);

  const [selectedItem, setItem] = useState(
    attachments.reduce((acc, current) => ({ ...acc, [current.id]: false }), {})
  );
  const [selectedFile, setSelectedFile] = useState();
  const [isCreateFolder, visibilityCreateFolder] = useState(false);
  const [editorFolder, setEditFolder] = useState(null);

  const saveFolder = (title) => {
    if (isCreateFolder) {
      createFolder({
        title,
        enterprise_id,
        type: 'work_report',
      });
    } else {
      updateFolder({
        title,
        id: selectedItem.folder,
        type: 'work_report',
      });
    }
  };

  const handleBlur = (e) => {
    visibilityCreateFolder(false);
    setEditFolder(null);
  };

  const handleConfirFolderName = (e) => {
    if (e.keyCode === 13) {
      saveFolder(e.target.value);
      visibilityCreateFolder(false);
      setEditFolder(null);
    }
  };

  const handleClickItem = (id) => (e) => {
    setItem({
      ...selectedItem,
      [id]: !selectedItem[id],
    });
  };

  const handleClickFile = (file) => (e) => {
    setSelectedFile(file);
    handleClickItem(file.file_id)(e);
  };

  const handleDelete = () => {
    Object.keys(selectedItem)
      .filter((item) => selectedItem[item])
      .forEach((file_id) => {
        const { id } = attachments.find(({ id }) => Number(id) === Number(file_id)) || {};
        deleteFile({ enterprise_id, id });
      });
    setItem(
      Object.keys(selectedItem).reduce(
        (acc, current) => ({ ...acc, [current]: false }),
        {}
      )
    );
  };

  const handleDownload = () => {
    try {
      Object.keys(selectedItem)
        .filter((item) => selectedItem[item])
        .forEach((id) => {
          const { url = '', title } =
            attachments.find((item) => Number(item.id) === Number(id)) || {};
          downloadAttachment({ url, title });
        });
    } catch (error_) {
      console.log(error_);
    }
  };

  const handleChangeFile = (event) => {
    uploadAttachment({
      file: event.target.files[0],
      enterprise_id,
      type: 'work_contract',
      folder_id: selectedItem.folder,
    });
    inputFileElement.current.value = null;
  };

  const handleAddFile = () => inputFileElement.current.click();

  const handleCloseErrorMessage = () => {
    resetErrorMessage();
  };

  useEffect(() => {
    if (isCreateFolder && inputFolderCreate.current !== null) {
      inputFolderCreate.current.focus();
      inputFolderCreate.current.addEventListener('blur', handleBlur);
    }
  }, [isCreateFolder]);

  useEffect(() => {
    if (editorFolder && inputFolderEdit.current !== null) {
      inputFolderEdit.current.focus();
      inputFolderEdit.current.addEventListener('blur', handleBlur);
    }
  }, [editorFolder]);

  const renderDialogContent = (
    <>
      <Loader isLoading={isLoading} />
      <ErrorMessage error={error} onClose={handleCloseErrorMessage} />
      <div className={cn(content, contentHeader)}>
        <p className={contentTitle} style={{ paddingLeft: 40 }}>
          {t('Label.name')}
        </p>
        <p className={contentTitle}>{t('dateOfDownload')}</p>
        <p className={contentTitle}>{t('type')}</p>
        <p className={contentTitle}>{t('Size')}</p>
      </div>
      <List className={contentBody} disablePadding>
        {attachments
          .filter(({ id }) => id)
          .map((attachment) => (
            <div key={attachment.id}>
              <ListItem
                key={attachment.id}
                button
                component="li"
                // selected={selectedItem[attachment.file_id]}
                onClick={handleClickItem(attachment.id)}
                className={content}
              >
                <p>
                  <Checkbox
                    name={attachment.id}
                    checked={Boolean(selectedItem[attachment.id])}
                    onChange={handleClickItem(attachment.id)}
                  />
                  <DescriptionIcon />
                  <span className={indentTitle}>{attachment.title}</span>
                </p>
                <p>{attachment.updated_at}</p>
                <p>{t('File')}</p>
                <p style={{ width: 70, textAlign: 'right' }}>
                  {formatBytes(attachment.size)}
                </p>
              </ListItem>
            </div>
          ))}
      </List>
    </>
  );

  const isDisabled =
    Object.keys(selectedItem).filter((item) => selectedItem[item]).length === 0;
  const renderDialogActions = (
    <div className={actions}>
      <ActionButton onClick={handleDownload} disabled={isDisabled}>
        <VerticalAlignBottomIcon />
        {t('Download')}
      </ActionButton>
      <ActionButton onClick={handleAddFile}>
        <VerticalAlignTopIcon />
        {t('Downloads')}
      </ActionButton>
      <ActionButton onClick={handleDelete} disabled={isDisabled} kind="negative">
        <DeleteOutlineIcon />
        {t('remove')}
      </ActionButton>
    </div>
  );

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        header={
          <span className={header}>
            <span>{t('documents')}</span>
            <CloseButton onClick={onClose} />
          </span>
        }
        content={renderDialogContent}
        actions={renderDialogActions}
        className={wrapCustomModal}
        minWidth={740}
        padding="0"
      />
      <input
        type="file"
        id="file"
        name="file"
        ref={inputFileElement}
        onChange={handleChangeFile}
        style={{ display: 'none' }}
      />
    </>
  );
};

export default AttachmentDialog;
