import { DetailToolbar, InputField, SelectField, CustomScrollbar } from '@ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import { ParkingAvailableSlotSelectField } from '../../../parking-available-slot-select';
import { slotSubmitted, changeMode } from '../../models/parking-slots.model';

const { t } = i18n;

const purposes = [
  { id: 'ownership', title: t('property') },
  { id: 'rent', title: t('rental') },
];

const validationErrorMessage = t('thisIsRequiredField');

const requiredSelect = Yup.mixed().required(validationErrorMessage);

const requiredSelectWhenRent = Yup.string().when('type', (value) => {
  if (value?.id === 'rent') return Yup.string().required(validationErrorMessage);
  return Yup.string();
});

const validationSchema = Yup.object().shape({
  type: requiredSelect,
  slot: requiredSelect,
  starts_at: requiredSelectWhenRent,
  ends_at: requiredSelectWhenRent,
});

const EnterprisesParkingForm = () => (
  <Formik
    validationSchema={validationSchema}
    initialValues={{
      type: { id: 'ownership', title: t('property') },
      slot: '',
      starts_at: '',
      ends_at: '',
    }}
    onSubmit={slotSubmitted}
    render={({ values }) => (
      <Form style={{ height: '100%' }}>
        <DetailToolbar
          onCancel={() => changeMode('view')}
          hidden={{ edit: true, delete: true, close: true }}
          style={{ padding: 24, height: 82 }}
          mode="edit"
        />
        <div
          style={{
            padding: 24,
            paddingRight: 12,
            paddingTop: 0,
            height: 'calc(100% - 82px)',
          }}
        >
          <CustomScrollbar>
            <div style={{ paddingRight: 12 }}>
              <Field
                name="type"
                component={SelectField}
                label={t('Purpose')}
                placeholder={t('ChooseDestination')}
                options={purposes}
                required
              />
              {values.type && values.type.id === 'rent' && (
                <>
                  <Field
                    name="starts_at"
                    component={InputField}
                    label={t('RentalStartDate')}
                    type="date"
                    required
                  />

                  <Field
                    name="ends_at"
                    component={InputField}
                    label={t('EndDateLease')}
                    type="date"
                    required
                  />
                </>
              )}
              <Field
                name="slot"
                component={ParkingAvailableSlotSelectField}
                label={t('parkingSpace')}
                starts_at={values.starts_at}
                ends_at={values.ends_at}
                type={values.type && values.type.id}
                placeholder={t('ChooseParkingSpace')}
                required
              />
            </div>
          </CustomScrollbar>
        </div>
      </Form>
    )}
  />
);

export { EnterprisesParkingForm };
