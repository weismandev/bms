import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import {
  Toolbar,
  FilterButton,
  FoundInfo,
  SearchInput,
  Greedy,
  ExportButton,
  DownloadTemplateButton,
  FileControl,
  ImportButton,
  AddButton,
} from '@ui';
import makeStyles from '@mui/styles/makeStyles';
import { addClicked } from '../../models/detail.model';
import { $isFilterOpen, changedFilterVisibility } from '../../models/filter.model';
import { $rowCount, exportEventsClicked, $isLoading } from '../../models/page.model';
import {
  $search,
  searchChanged,
  $tableData,
  $hiddenColumnNames,
  columns,
  fileUploaded,
  downloadTemplateClicked,
} from '../../models/table.model';

const useStyles = makeStyles({
  margin: {
    margin: '0 5px',
  },
  tooltipContainer: {
    display: 'flex',
  },
});

function TableToolbar() {
  const { t } = useTranslation();

  const totalCount = useStore($rowCount);
  const isFilterOpen = useStore($isFilterOpen);
  const search = useStore($search);

  const isLoading = useStore($isLoading);
  const tableData = useStore($tableData);
  const hiddenColumnNames = useStore($hiddenColumnNames);

  const classes = useStyles();

  const handleChangeSearch = (e) => searchChanged(e.target.value);
  const handleFilterVisibility = () => changedFilterVisibility(true);

  const handleUploadFile = (value) => value && value.file && fileUploaded(value.file);

  return (
    <Toolbar>
      <FilterButton
        disabled={isFilterOpen}
        onClick={handleFilterVisibility}
        className={classes.margin}
      />
      <FoundInfo countTitle={t('Found')} count={totalCount} className={classes.margin} />
      <SearchInput
        className={classes.margin}
        value={search}
        onChange={handleChangeSearch}
      />
      <Greedy />
      <DownloadTemplateButton
        className={classes.margin}
        onClick={downloadTemplateClicked}
      />
      <FileControl
        mode="edit"
        name="file"
        encoding="file"
        onChange={handleUploadFile}
        renderPreview={() => null}
        renderButton={() => <ImportButton component="span" />}
      />
      {/*<ExportButton*/}
      {/*  {...{ columns, tableData, hiddenColumnNames }}*/}
      {/*  onClick={exportEventsClicked}*/}
      {/*  className={classes.margin}*/}
      {/*  disabled={isLoading || !tableData.length}*/}
      {/*/>*/}
      <AddButton onClick={addClicked} className={classes.margin} disabled={false} />
    </Toolbar>
  );
}

export { TableToolbar };
