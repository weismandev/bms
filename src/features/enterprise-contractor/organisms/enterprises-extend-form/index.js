import DatePicker from 'react-datepicker';
import { useStore } from 'effector-react';
import { DetailToolbar, InputField, CustomScrollbar } from '@ui';
import { dateFnsLocale } from '@tools/dateFnsLocale';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import i18n from '@shared/config/i18n';
import {
  changeMode,
  $slotForExtendRent,
  submitExtendSlotRent,
} from '../../models/parking-slots.model';
import { SlotCard } from '../enterprises-parking-list';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  extend_date: Yup.date().nullable().required(t('ChooseNewRentalEndDate')),
});

const EnterprisesExtendForm = () => {
  const slotForExtendRent = useStore($slotForExtendRent);

  const getEndRentDateFromString = (dateString) => {
    const dateMatch = dateString.match(/^(?<date>\d{2}).(?<month>\d{2}).(?<year>\d{4})$/);

    if (!dateMatch) return new Date();

    const { date, month, year } = dateMatch.groups;
    return new Date(Number(year), Number(month) - 1, Number(date) + 1, 0, 0, 0);
  };

  const afterDate = getEndRentDateFromString(slotForExtendRent.rent.ends_at);

  return (
    <Formik
      validateOnBlur
      validationSchema={validationSchema}
      initialValues={{ extend_date: null }}
      onSubmit={submitExtendSlotRent}
      render={() => (
        <Form style={{ height: '100%' }}>
          <DetailToolbar
            onCancel={() => changeMode('view')}
            hidden={{ edit: true, delete: true, close: true }}
            style={{ padding: 24, height: 82 }}
            mode="edit"
          />
          <div
            style={{
              padding: 24,
              paddingRight: 12,
              paddingTop: 0,
              height: 'calc(100% - 82px)',
            }}
          >
            <CustomScrollbar>
              <div style={{ paddingRight: 12 }}>
                <SlotCard {...slotForExtendRent} />
                <Field
                  name="extend_date"
                  render={({ field, form }) => (
                    <DatePicker
                      required
                      placeholderText={t('selectDate')}
                      customInput={
                        <InputField
                          field={field}
                          form={form}
                          label={t('ExtendYourLeaseUntil')}
                          divider
                        />
                      }
                      name={field.name}
                      selected={field.value}
                      onChange={(value) => form.setFieldValue(field.name, value)}
                      locale={dateFnsLocale}
                      minDate={afterDate}
                      dateFormat="dd.MM.yyyy"
                    />
                  )}
                />
              </div>
            </CustomScrollbar>
          </div>
        </Form>
      )}
    />
  );
};

export { EnterprisesExtendForm };
