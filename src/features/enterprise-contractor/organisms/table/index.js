import { useMemo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useStore } from 'effector-react';
import { Template } from '@devexpress/dx-react-core';
import {
  SortingState,
  SelectionState,
  PagingState,
  CustomPaging,
  IntegratedSelection,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table as DXTable,
  TableHeaderRow,
  ColumnChooser,
  TableColumnVisibility,
  Toolbar,
  TableSelection,
  PagingPanel,
  TableColumnResizing,
  DragDropProvider,
  TableColumnReordering,
} from '@devexpress/dx-react-grid-material-ui';
import {
  Wrapper,
  TableContainer,
  TableRow,
  TableCell,
  TableHeaderCell,
  SortingLabel,
} from '../../../../ui';
import { $opened, openViaUrl } from '../../models/detail.model';
import { $rowCount } from '../../models/page.model';
import {
  $tableData,
  columns,
  pageNumChanged as pageNumberChanged,
  pageSizeChanged,
  hiddenColumnChanged,
  columnOrderChanged,
  columnWidthsChanged,
  $currentPage,
  $pageSize,
  $hiddenColumnNames,
  $columnOrder,
  $columnWidths,
  $selection,
  selectionChanged,
  $sorting,
  sortChanged,
  excludeSortColumns,
} from '../../models/table.model';
import { TableToolbar } from '../table-toolbar';

const Root = ({ children, ...props }) => (
  <Grid.Root {...props} style={{ height: '100%' }}>
    {children}
  </Grid.Root>
);

const Row = (props) => {
  const opened = useStore($opened);
  const isSelected = opened && opened.id === props.row.id;
  const onRowClick = useCallback((data, id) => openViaUrl(id), []);

  const row = useMemo(
    () => <TableRow {...props} isSelected={isSelected} onRowClick={onRowClick} />,
    [props.children]
  );

  return row;
};

const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      padding: '0',
      margin: '0 0 14px 0',
      minHeight: '36px',
      height: '36px',
      borderBottom: 'none',
      flexWrap: 'nowrap',
    }}
  />
);

export const Table = () => {
  const { t } = useTranslation();
  const data = useStore($tableData);
  const columnWidths = useStore($columnWidths);
  const columnOrder = useStore($columnOrder);
  const hiddenColumnNames = useStore($hiddenColumnNames);
  const currentPage = useStore($currentPage);
  const pageSize = useStore($pageSize);
  const totalCount = useStore($rowCount);
  const selection = useStore($selection);
  const sorting = useStore($sorting);

  const hasLeastOneVisibleColumn = columns.length !== hiddenColumnNames.length;

  return (
    <Wrapper style={{ height: '100%', padding: 24 }}>
      <Grid rootComponent={Root} rows={data} getRowId={(row) => row.id} columns={columns}>
        <SelectionState selection={selection} onSelectionChange={selectionChanged} />

        <SortingState sorting={sorting} onSortingChange={sortChanged} />

        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={pageNumberChanged}
          pageSize={pageSize}
          onPageSizeChange={pageSizeChanged}
        />
        <CustomPaging totalCount={totalCount} />

        <DragDropProvider />
        <IntegratedSelection />
        <DXTable
          columnExtensions={[]}
          messages={{ noData: t('noData') }}
          rowComponent={Row}
          cellComponent={TableCell}
          containerComponent={TableContainer}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={columnOrderChanged} />
        <TableColumnResizing
          columnWidths={columnWidths}
          onColumnWidthsChange={columnWidthsChanged}
        />
        <TableHeaderRow
          cellComponent={TableHeaderCell}
          showSortingControls
          sortLabelComponent={(props) => (
            <SortingLabel excludeSortColumns={excludeSortColumns} {...props} />
          )}
        />
        <TableSelection showSelectAll />
        <TableColumnVisibility
          data-test-id="hide-columns-btn"
          hiddenColumnNames={hiddenColumnNames}
          onHiddenColumnNamesChange={hiddenColumnChanged}
        />
        <Toolbar rootComponent={ToolbarRoot} />
        <Template name="toolbarContent">
          <TableToolbar />
        </Template>
        <ColumnChooser messages={{ showColumnChooser: t('showColumnSelection') }} />
        {hasLeastOneVisibleColumn && (
          <PagingPanel
            pageSizes={[10, 25, 50, 100]}
            messages={{ rowsPerPage: t('EntriesPerPage'), info: '' }}
          />
        )}
      </Grid>
    </Wrapper>
  );
};
