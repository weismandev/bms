import { useTranslation } from 'react-i18next';
import { useList, useStore } from 'effector-react';
import { LabeledContent, CustomScrollbar, ActionButton } from '@ui';
import { IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import { PaginationItem, Pagination } from '@mui/lab';
import makeStyles from '@mui/styles/makeStyles';
import {
  $slots,
  $slotsPageParams,
  changeDeleteData,
  changeSlotsPage,
  clickExtendSlotRent,
  $mode,
} from '../../models/parking-slots.model';

const useStyles = makeStyles({
  listOuter: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 12px 0 24px',
    height: '100%',
    overflow: 'hidden',
  },
  listInner: {
    paddingRight: 12,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  card: {
    position: 'relative',
    padding: 24,
    background: '#EDF6FF',
    borderRadius: 16,
    marginBottom: 20,
    '& > *:last-child': {
      marginBottom: 0,
    },
  },
  part: {
    marginBottom: 20,
  },
  text: {
    color: '#65657B',
    fontSize: '13px',
    fontWeight: 500,
  },
  delete_btn: {
    position: 'absolute',
    top: 12,
    right: 12,
  },
});

const EnterprisesParkingList = () => {
  const { listOuter, listInner } = useStyles();
  const list = useList($slots, (pass) => <SlotCard {...pass} />);
  const slotsPageParams = useStore($slotsPageParams);

  return (
    <div className={listOuter}>
      <CustomScrollbar>
        <div className={listInner}>{list}</div>
      </CustomScrollbar>
      {list.length ? (
        <Pagination
          style={{
            padding: 12,
            display: 'flex',
            justifyContent: 'center',
            flexWrap: 'nowrap',
          }}
          onChange={(_, page) => changeSlotsPage(page)}
          page={slotsPageParams.current_page}
          count={slotsPageParams.last_page}
          size="small"
          shape="rounded"
          renderItem={(item) => (
            <PaginationItem
              {...item}
              style={{
                width: 20,
                height: 36,
                fontWeight: 600,
              }}
            />
          )}
        />
      ) : null}
    </div>
  );
};

const SlotCard = (props) => {
  const { title, type, rent, id, rent_id } = props;
  const { part, card, text, delete_btn } = useStyles();
  const mode = useStore($mode);
  const { t } = useTranslation();

  const isProperty = type === 'ownership';

  return (
    <div className={card}>
      {mode !== 'extend' && (
        <IconButton
          classes={{ root: delete_btn }}
          onClick={() => changeDeleteData({ isOpen: true, slot_id: id, rent_id })}
          size="large"
        >
          <Close />
        </IconButton>
      )}
      <LabeledContent className={part} label={t('Name')}>
        <span className={text}>{title}</span>
      </LabeledContent>
      <LabeledContent className={part} label={t('Located')}>
        <span className={text}>{isProperty ? t('owned') : t('onLease')}</span>
      </LabeledContent>
      {!isProperty && rent && (
        <>
          <LabeledContent className={part} label={t('RentalStartDate')}>
            <span className={text}>{rent.starts_at}</span>
          </LabeledContent>
          <LabeledContent className={part} label={t('EndDateLease')}>
            <span className={text}>{rent.ends_at}</span>
          </LabeledContent>
          {mode !== 'extend' && (
            <ActionButton
              style={{ width: '100%' }}
              onClick={() => clickExtendSlotRent(id)}
            >
              {t('ExtendRental')}
            </ActionButton>
          )}
        </>
      )}
    </div>
  );
};

export { EnterprisesParkingList, SlotCard };
