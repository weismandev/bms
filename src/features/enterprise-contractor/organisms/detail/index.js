import { memo } from 'react';
import { useStore } from 'effector-react';
import { Tab } from '@mui/material';
import { Wrapper, Tabs } from '../../../../ui';
import {
  $isAttachmentOpen,
  changedAttachmentVisibility,
} from '../../models/attachment.modal';
import {
  $mode,
  changedDetailVisibility,
  $opened,
  $currentTab,
  changeTab,
  tabs,
  changeMode,
  detailSubmitted,
  createVehicle,
  createEnterprise,
  updateEnterprise,
} from '../../models/detail.model';
import { deleteDialogVisibilityChanged } from '../../models/page.model';
import AttachmentDialog from '../attachment-dialog';
import EnterprisesParking from './enterprises-parking';
import InfoTabForm from './info-tab-form';
import VehicleTabForm from './vehicle-tab-form';

const Detail = memo(() => {
  const mode = useStore($mode);
  const opened = useStore($opened);
  const currentTab = useStore($currentTab);
  const isAttachmentOpen = useStore($isAttachmentOpen);

  const isNew = !opened.id;

  const handleChangeMode = (newMode) => () => changeMode(newMode);
  const handleDetailVisibility = () => changedDetailVisibility(false);
  const handleCancel = () => {
    if (isNew) {
      changedDetailVisibility(false);
    } else {
      changeMode('view');
    }
  };
  const handleDelete = () => deleteDialogVisibilityChanged(true);
  const handleDetailSubmitted = (values) => {
    detailSubmitted(values);
    if (isNew) {
      createEnterprise(values);
    } else {
      updateEnterprise(values);
    }
  };

  const renderFormByCurrentTab = (tab) => {
    switch (tab) {
      case 'info':
        return (
          <InfoTabForm
            mode={mode}
            initialValues={opened}
            onSubmit={handleDetailSubmitted}
            onEdit={handleChangeMode('edit')}
            onClose={handleDetailVisibility}
            onCancel={handleCancel}
            onDelete={handleDelete}
            onChangeAttachmentDialog={changedAttachmentVisibility}
          />
        );
      case 'transport':
        return (
          <VehicleTabForm
            mode={mode}
            onEdit={handleChangeMode('edit')}
            onClose={handleDetailVisibility}
            onCancel={handleCancel}
          />
        );
      case 'parking':
        return (
          <EnterprisesParking
            mode={mode}
            enterprise_id={opened.id}
            onEdit={handleChangeMode('edit')}
            onClose={handleDetailVisibility}
            onCancel={handleCancel}
          />
        );
      default:
        return <div />;
    }
  };

  return (
    <Wrapper style={{ height: '100%', overflow: 'hidden' }}>
      <Tabs
        options={tabs}
        currentTab={currentTab}
        onChange={(e, tab) => changeTab(tab)}
        isNew={isNew}
        tabEl={<Tab />}
      />
      <AttachmentDialog
        isOpen={isAttachmentOpen}
        enterprise_id={opened.id}
        onClose={() => changedAttachmentVisibility(false)}
      />
      {renderFormByCurrentTab(currentTab)}
    </Wrapper>
  );
});

export { Detail };
