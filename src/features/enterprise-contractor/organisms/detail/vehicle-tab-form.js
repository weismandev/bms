import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { Button } from '@mui/material';
import i18n from '@shared/config/i18n';
import { DetailToolbar, Loader, CustomScrollbar } from '@ui/index';
import {
  $isLoading,
  $vehicles,
  newVehicle,
  saveVehicles,
} from '../../models/vehicles.model';
import { VehicleCard } from './vehicle-card';

const { t } = i18n;

const validationSchema = Yup.object().shape({
  vehicles: Yup.array().of(
    Yup.object().shape({
      brand: Yup.string().required(t('thisIsRequiredField')),
      license_plate: Yup.string().required(t('thisIsRequiredField')),
    })
  ),
});

const VehicleTabForm = ({ mode, onEdit, onClose, onCancel }) => {
  const isLoading = useStore($isLoading);
  const vehicles = useStore($vehicles);

  return (
    <>
      <Loader isLoading={isLoading} />

      <Formik
        initialValues={{ vehicles }}
        onSubmit={(values) => saveVehicles(values)}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values, resetForm }) => (
          <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
            <DetailToolbar
              style={{ padding: 24 }}
              mode={mode}
              onEdit={onEdit}
              onClose={onClose}
              onCancel={() => {
                onCancel();
                resetForm();
              }}
              hidden={{ delete: true }}
            />
            <div
              style={{
                height: 'calc(100% - 82px)',
                padding: '0 6px 24px 24px',
              }}
            >
              <CustomScrollbar>
                <div style={{ paddingRight: 18 }}>
                  <FieldArray
                    name="vehicles"
                    render={({ push, remove }) => {
                      const vehicleCard = values.vehicles.map((_, index) => (
                        <Field
                          key={index}
                          name={`vehicles[${index}]`}
                          render={({ field }) => (
                            <VehicleCard
                              parentName={field.name}
                              remove={() => remove(index)}
                              mode={mode}
                            />
                          )}
                        />
                      ));
                      const addButton =
                        mode === 'edit' ? (
                          <Button
                            style={{ marginTop: 16 }}
                            color="primary"
                            onClick={() => push(newVehicle)}
                          >
                            + {t('AddTransport')}
                          </Button>
                        ) : null;

                      return (
                        <>
                          {vehicleCard}
                          {addButton}
                        </>
                      );
                    }}
                  />
                </div>
              </CustomScrollbar>
            </div>
          </Form>
        )}
      />
    </>
  );
};

export default VehicleTabForm;
