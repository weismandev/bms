import { useStore } from 'effector-react';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import AddIcon from '@mui/icons-material/Add';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import {
  DetailToolbar,
  ActionButton,
  InputField,
  SelectField,
  CustomScrollbar,
  Loader,
} from '../../../../ui';
import { ParkingAvailableSlotSelectField } from '../../../parking-available-slot-select';
import {
  ParkingSlotsGate,
  $slots,
  $isLoading,
  slotSubmitted,
  submitExtendSlotRent,
} from '../../models/parking-slots.model';

const useStyles = makeStyles({
  wrapperForm: {
    backgroundColor: '#EDF6FF',
    borderRadius: 16,
    padding: 24,
    margin: 24,
    '&>div': {
      marginBottom: 24,
      '& hr': {
        display: 'none',
      },
    },
  },
  wrapperFormDate: {
    display: 'flex',
    justifyContent: 'space-between',
    '&>div': {
      width: '48%',
    },
  },
  ml15: {
    marginLeft: 15,
  },
});

const { t } = i18n;

const purposes = [
  { id: 'ownership', title: t('property') },
  { id: 'rent', title: t('rental') },
];

const validationErrorMessage = t('thisIsRequiredField');

const requiredSelect = Yup.mixed().required(validationErrorMessage);

const requiredSelectWhenRent = Yup.string().when('type', (value) => {
  if (value.id === 'rent') {
    return Yup.string().required(validationErrorMessage);
  }
});

const validationSchema = Yup.object().shape({
  type: requiredSelect,
  slot: requiredSelect,
  starts_at: requiredSelectWhenRent,
  ends_at: requiredSelectWhenRent,
});

const ParkingTabForm = ({ mode, enterprise_id, onEdit, onClose, onCancel }) => {
  const { wrapperForm, wrapperFormDate, ml15 } = useStyles();

  const slots = useStore($slots);
  const isLoading = useStore($isLoading);

  const handleAddPlace = (arrayHelpers) => () => {
    const index = arrayHelpers.form.values.parking.length - 1;
    arrayHelpers.insert(index, {
      type: '',
      slot: '',
      starts_at: '',
      ends_at: '',
    });
  };

  const handleSubmit = (values) => {
    values.parking.forEach((element) => {
      if (element.rent_id) {
        submitExtendSlotRent(element);
      } else {
        slotSubmitted(element);
      }
    });
  };

  return (
    <>
      <ParkingSlotsGate enterprise_id={enterprise_id} />
      <Loader isLoading={isLoading} />
      <Formik
        initialValues={{
          parking: slots,
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        render={({ values }) => {
          return (
            <Form style={{ height: 'calc(100% - 50px)', position: 'relative' }}>
              <DetailToolbar
                style={{ padding: 24 }}
                mode={mode}
                onEdit={onEdit}
                onClose={onClose}
                onCancel={onCancel}
                // onDelete={() => deleteDialogVisibilityChanged(true)}
              />
              <CustomScrollbar>
                <FieldArray
                  name="parking"
                  render={(arrayHelpers) => (
                    <>
                      {values.parking &&
                        values.parking.length > 0 &&
                        values.parking.map((parking, index) => (
                          <div key={index} className={wrapperForm}>
                            <Field
                              name={`parking.${index}.type`}
                              component={SelectField}
                              label={t('Purpose')}
                              placeholder={t('ChooseDestination')}
                              options={purposes}
                              mode={mode}
                              required
                            />
                            <div className={wrapperFormDate}>
                              <Field
                                name={`parking.${index}.starts_at`}
                                component={InputField}
                                label={t('RentalStartDate')}
                                type="date"
                                mode={mode}
                              />
                              <Field
                                name={`parking.${index}.ends_at`}
                                component={InputField}
                                label={t('EndDateLease')}
                                type="date"
                                mode={mode}
                              />
                            </div>
                            <Field
                              name={`parking.${index}.slot`}
                              component={ParkingAvailableSlotSelectField}
                              label={t('parkingSpace')}
                              starts_at={`parking.${index}.starts_at`}
                              ends_at={`parking.${index}.ends_at`}
                              type={`parking.${index}.type`}
                              placeholder={t('ChooseParkingSpace')}
                              mode={mode}
                              required
                            />
                          </div>
                        ))}
                      {mode === 'edit' && (
                        <ActionButton
                          onClick={handleAddPlace(arrayHelpers)}
                          className={ml15}
                        >
                          <AddIcon /> {t('AddParkingSpace')}
                        </ActionButton>
                      )}
                    </>
                  )}
                />
              </CustomScrollbar>
            </Form>
          );
        }}
      />
    </>
  );
};

export default ParkingTabForm;
