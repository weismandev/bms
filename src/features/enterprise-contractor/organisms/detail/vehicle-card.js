import { useTranslation } from 'react-i18next';
import { Field } from 'formik';
import { IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import makeStyles from '@mui/styles/makeStyles';
import { InputField } from '@ui/index';

const useStyles = makeStyles({
  wrapperForm: {
    backgroundColor: '#EDF6FF',
    borderRadius: 16,
    padding: '18px 24px 13px 24px',
    marginTop: 24,
  },
  wrapperFields: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media (max-width: 1024px)': {
      flexDirection: 'column',
    },
  },
  brandField: {
    width: '48%',
    '@media (max-width: 1024px)': {
      width: '100%',
      paddingBottom: 15,
    },
  },
  field: {
    width: '48%',
    '@media (max-width: 1024px)': {
      width: '100%',
    },
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'end',
  },
});

const VehicleCard = ({ parentName, mode, remove }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.wrapperForm}>
      {mode === 'edit' && (
        <div className={classes.cardHeader}>
          <IconButton size="small" onClick={remove}>
            <Close />
          </IconButton>
        </div>
      )}
      <div className={classes.wrapperFields}>
        <div className={classes.brandField}>
          <Field
            name={`${parentName}.brand`}
            component={InputField}
            label={t('BrandModel')}
            placeholder={t('EnterTheBrandOrModelVehicle')}
            mode={mode}
            divider={false}
            required
          />
        </div>
        <div className={classes.field}>
          <Field
            name={`${parentName}.license_plate`}
            component={InputField}
            label={t('GovNumber')}
            placeholder={t('EnterGovNumber')}
            mode={mode}
            divider={false}
            required
          />
        </div>
      </div>
    </div>
  );
};

export { VehicleCard };
