import { useState, useEffect } from 'react';
import {
  DetailToolbar,
  Checkbox,
  InputField,
  ActionButton,
  CustomScrollbar,
  PhoneMaskedInput,
  SomePhoneMaskedInput,
} from '@ui';
import { Formik, Form, Field, FieldArray } from 'formik';
import * as Yup from 'yup';
import { FormControlLabel } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import AssignmentReturnedIcon from '@mui/icons-material/AssignmentReturned';
import makeStyles from '@mui/styles/makeStyles';
import i18n from '@shared/config/i18n';
import { formatPhone } from '@tools/formatPhone';
import { validateSomePhone } from '@ui/atoms/some-phone-masked-input';
import { ServicesSelectField, ServiceItemsSelectField } from '../../../services-select';
import { getEnterprisesByResident } from '../../models/page.model';
import { EnterprisesCompaniesSelectField } from '../../molecules';

const useStyles = makeStyles({
  wrapperForm: {
    height: 'calc(100% - 50px)',
    position: 'relative',
    padding: 24,
  },
  residentFieldWrap: {
    width: '100%',
    paddingTop: 24,
  },
  mainForm: {
    paddingTop: 24,
    display: 'flex',
    justifyContent: 'space-between',
    '&>div': {
      width: '48%',
    },
  },
  m24: {
    display: 'flex',
    margin: '24px 0px',
  },
  btnArchive: {
    marginTop: 24,
    backgroundColor: '#EC7F00',
  },
  archiveTitle: {
    color: '#EC7F00',
    paddingTop: 24,
    margin: 0,
  },
});

const { t } = i18n;

const validationSchema = (isShape) =>
  Yup.object().shape({
    title: Yup.lazy((value) => {
      switch (typeof value) {
        case 'number':
          return Yup.number();
        case 'string':
          return Yup.string().required(t('thisIsRequiredField')).nullable();
        case 'object':
          return Yup.object().required(t('thisIsRequiredField')).nullable();
        default:
          return Yup.string().required(t('thisIsRequiredField'));
      }
    }),
    name: Yup.string().required(t('thisIsRequiredField')),
    phone: Yup.string()
      .required(t('thisIsRequiredField'))
      .test('phone-length', t('invalidNumber'), validateSomePhone),
    inn: Yup.string()
      .required(`${t('PleaseEnterYourTIN')}.`)
      .min(10, `${t('AtLeast10Characters')}.`)
      .max(12, `${t('AtLeast12Characters')}.`)
      .matches(/^\d{10,12}$/, {
        message: `${t('UseNumbersFrom0to9')}.`,
      }),
  });

const InfoTabForm = ({
  mode,
  initialValues,
  onSubmit,
  onEdit,
  onClose,
  onCancel,
  onDelete,
  onChangeAttachmentDialog,
}) => {
  const { wrapperForm, residentFieldWrap, mainForm, m24, btnArchive, archiveTitle } =
    useStyles();

  const [formControl, setFormControl] = useState({
    is_resident: initialValues.is_resident,
    is_banned: initialValues.is_banned,
  });

  useEffect(() => {
    setFormControl({
      is_resident: initialValues.is_resident,
      is_banned: initialValues.is_banned,
    });
  }, [initialValues]);

  useEffect(() => {
    if (formControl.is_resident) {
      getEnterprisesByResident();
    }
  }, [formControl.is_resident]);

  const handleFormControl = (type, setFieldValue) => () => {
    setFormControl({
      ...formControl,
      [type]: !formControl[type],
    });

    if (type === 'is_resident') {
      setFieldValue('title', '');
    }
  };

  const handleAddContract = (arrayHelpers) => () => {
    const index = arrayHelpers.form.values.contracts.length - 1;
    arrayHelpers.insert(index, { number: '' });
  };

  const handleAddPhone = (arrayHelpers) => () => {
    const index = arrayHelpers.form.values.additional_phones.length - 1;
    arrayHelpers.insert(index, '');
  };

  const handleSubmit = ({ service_item_id = [], ...values }, { setFieldValue }) => {
    setFieldValue(
      'additional_phones',
      values.additional_phones.filter((item) => item)
    );
    setFieldValue(
      'contracts',
      values.contracts.filter((item) => item.number)
    );

    const serviceItems = service_item_id.reduce(
      (acc, current) => ({
        ...acc,
        [current.parentId]: acc[current.parentId]
          ? acc[current.parentId].concat(current.id)
          : [current.id],
      }),
      {}
    );
    const residentDirtyForm = formControl.is_resident
      ? {
          id: values.title.id || values.id,
          building_id: values.title.building_id || values.building_id,
        }
      : {};
    onSubmit({
      ...values,
      ...formControl,
      ...residentDirtyForm,
      services: [],
      // services: values.services.reduce(
      //   (acc, current) => ({
      //     ...acc,
      //     [current.id]: { items: serviceItems[current.id] } || {},
      //   }),
      //   {}
      // ),
    });
  };

  const computeServices = (services) => {
    return (
      services?.length > 0 &&
      services.filter((item) => item.item_group && item.item_group.items).length > 0
    );
  };

  const computeOtherServices = (services) => {
    return services?.length > 0 && services.find(({ id }) => id === 16);
  };

  const handleCompanies =
    (setFieldValue) =>
    (selectedCompany = {}) => {
      const values = {
        id: null,
        inn: '',
        responsible: { name: '', phone: '' },
        ...selectedCompany,
      };
      setFieldValue('id', values.id || '');
      setFieldValue('title', values.id ? values : '');
      setFieldValue('inn', values.inn);
      setFieldValue('name', values.responsible.name || '');
      setFieldValue('phone', values.responsible.phone || '');
    };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema(formControl.is_resident)}
      enableReinitialize
      render={({ values, setFieldValue }) => {
        // const isAdditionalServices = computeServices(values.services);
        // const isOtherWorksServices = computeOtherServices(values.services);
        return (
          <Form className={wrapperForm}>
            <DetailToolbar
              isArchived={initialValues.is_archived}
              mode={mode}
              onEdit={onEdit}
              onClose={onClose}
              onCancel={onCancel}
              onDelete={onDelete}
              onArchive={() =>
                handleSubmit(
                  { ...values, is_archived: !initialValues.is_archived },
                  { setFieldValue }
                )
              }
              hidden={{ delete: true, edit: initialValues.is_archived }}
            />
            <CustomScrollbar>
              {values.is_archived && (
                <p className={archiveTitle}>{t('ContractorMovedToArchive')}</p>
              )}
              {mode === 'edit' && (
                <FormControlLabel
                  label={t('AreResidents')}
                  control={
                    <Checkbox
                      name="is_resident"
                      size="small"
                      checked={formControl.is_resident}
                      onChange={handleFormControl('is_resident', setFieldValue)}
                      disabled={initialValues.id !== null}
                    />
                  }
                  className={residentFieldWrap}
                />
              )}
              <div className={mainForm}>
                <div>
                  {formControl.is_resident && !initialValues.is_resident ? (
                    <Field
                      name="id"
                      component={EnterprisesCompaniesSelectField}
                      label={t('NameOfCompany')}
                      placeholder={t('EnterTheNameOfTheOrganization')}
                      mode={mode}
                      onChange={handleCompanies(setFieldValue)}
                    />
                  ) : (
                    <Field
                      name="title"
                      component={InputField}
                      label={t('NameOfCompany')}
                      placeholder={t('EnterTheNameOfTheOrganization')}
                      mode={mode}
                    />
                  )}
                  <Field
                    name="name"
                    component={InputField}
                    label={t('ResponsiblePerson')}
                    placeholder={t('EnterResponsiblePerson')}
                    disabled={formControl.is_resident}
                    mode={mode}
                  />
                </div>
                <div>
                  <Field
                    name="inn"
                    component={InputField}
                    label={t('TIN')}
                    placeholder={t('EnterYourTIN')}
                    disabled={formControl.is_resident}
                    mode={mode}
                  />
                  <Field
                    name="phone"
                    render={({ field, form }) => (
                      <InputField
                        field={field}
                        form={form}
                        inputComponent={SomePhoneMaskedInput}
                        label={t('Label.phone')}
                        placeholder={t('EnterPhoneNumber')}
                        disabled={formControl.is_resident}
                        mode={mode}
                        required
                        inputProps={{ form }}
                      />
                    )}
                  />
                </div>
              </div>
              <FieldArray
                name="additional_phones"
                render={(arrayHelpers) => (
                  <>
                    {mode === 'edit' && (
                      <ActionButton
                        className={m24}
                        onClick={handleAddPhone(arrayHelpers)}
                      >
                        <AddIcon />
                        {t('AddPhoneNumber')}
                      </ActionButton>
                    )}
                    {values.additional_phones &&
                      values.additional_phones.length > 0 &&
                      values.additional_phones.map((contract, index) => (
                        <Field
                          key={index}
                          name={`additional_phones.${index}`}
                          render={({ field, form }) => (
                            <InputField
                              field={field}
                              form={form}
                              inputComponent={PhoneMaskedInput}
                              label={t('AdditionalPhone')}
                              placeholder={t('EnterAdditionalPhoneNumber')}
                              mode={mode}
                            />
                          )}
                        />
                      ))}
                  </>
                )}
              />
              {/* <Field
                name="services"
                component={ServicesSelectField}
                label="Вид работ"
                placeholder="Выберите вид работ"
                mode={mode}
                isMulti
              />
              {isAdditionalServices && (
                <Field
                  name="service_item_id"
                  component={ServiceItemsSelectField}
                  label="Вид оборудования"
                  placeholder="Выберите вид оборудования"
                  mode={mode}
                  isMulti
                />
              )}
              {isOtherWorksServices && (
                <Field
                  name="service_other_id"
                  component={InputField}
                  label="Уточните работы"
                  placeholder="Введите иные работы"
                />
              )} */}
              <FieldArray
                name="contracts"
                render={(arrayHelpers) => (
                  <>
                    {mode === 'edit' && (
                      <ActionButton
                        className={m24}
                        onClick={handleAddContract(arrayHelpers)}
                      >
                        <AddIcon />
                        {t('AddContract')}
                      </ActionButton>
                    )}
                    {values.contracts &&
                      values.contracts.length > 0 &&
                      values.contracts.map((contract, index) => (
                        <Field
                          key={index}
                          name={`contracts.${index}.number`}
                          component={InputField}
                          label={t('contractNumber')}
                          placeholder={t('EnterContractNumber')}
                          mode={mode}
                        />
                      ))}
                  </>
                )}
              />
              {mode === 'edit' && initialValues.id && (
                <ActionButton
                  className={m24}
                  onClick={() => onChangeAttachmentDialog(true)}
                >
                  <AssignmentReturnedIcon />
                  {t('documents')}
                </ActionButton>
              )}
              <Field
                label={t('Label.comment')}
                placeholder={t('EnterComment')}
                component={InputField}
                name="comment"
                type="textarea"
                mode={mode}
              />
              {mode === 'edit' && (
                <FormControlLabel
                  label={t('MoveBlacklist')}
                  control={
                    <Checkbox
                      name="is_banned"
                      size="small"
                      checked={formControl.is_banned}
                      onChange={handleFormControl('is_banned')}
                    />
                  }
                  className={residentFieldWrap}
                />
              )}
            </CustomScrollbar>
          </Form>
        );
      }}
    />
  );
};

export default InfoTabForm;
