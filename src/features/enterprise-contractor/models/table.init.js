import { forward } from 'effector';
import FileSaver from 'file-saver';
import { signout } from '@features/common';
import { fxPreloadFile } from './page.model';
import {
  $selection,
  selectionChanged,
  fileUploaded,
  downloadTemplateClicked,
} from './table.model';

$selection.on(selectionChanged, (state, selection) => selection).reset(signout);

forward({ from: fileUploaded, to: fxPreloadFile });

downloadTemplateClicked.watch(() => {
  FileSaver.saveAs(
    'https://files.mysmartflat.ru/crm/templates/enterprise_import_template.xlsx',
    'ujin-шаблон-импорта-подрядных-организаций.xlsx'
  );
});
