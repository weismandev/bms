import { sample, merge } from 'effector';
import FileSaver from 'file-saver';
import i18n from '@shared/config/i18n';
import { signout } from '../../common';
import { contractingApi } from '../api';
import convertTimestamp from '../lib/convertTimestamp';
import {
  fxCreateFolder,
  fxUpdateFolder,
  fxDeleteFolder,
  fxDeleteFile,
  fxAddAttachment,
  fxDownloadAttachment,
  $attachments,
  $isAttachmentOpen,
  createFolder,
  updateFolder,
  uploadAttachment,
  changedAttachmentVisibility,
  deleteFolder,
  deleteFile,
  downloadAttachment,
  $isLoading,
  $error,
  resetErrorMessage,
} from './attachment.modal';
import { $opened } from './detail.model';

const { t } = i18n;

$isAttachmentOpen
  .on(changedAttachmentVisibility, (_, visibility) => visibility)
  .reset(signout);

$attachments
  .on($opened, (_, { attachments }) =>
    attachments.map((item) => ({
      ...item,
      updated_at: convertTimestamp(item.updated_at),
    }))
  )
  .on(fxCreateFolder.done, (state, { result }) =>
    state.concat({
      ...result.folder,
      updated_at: convertTimestamp(Date.now() / 1000),
    })
  )
  .on(fxAddAttachment.done, (state, { result: { attachment } }) =>
    state.concat({
      enterprise_id: attachment.enterprise_id,
      id: attachment.file_id,
      title: attachment.name,
      type: t('File'),
      size: attachment.size,
      updated_at: convertTimestamp(Date.now() / 1000),
    })
  )
  .on(fxDeleteFile.done, (state, { result: { file_id } }) =>
    state.filter((item) => item.id !== file_id)
  )
  .on(fxDeleteFolder.done, (state, { result: { folder_id } }) =>
    state.filter(({ id }) => id !== folder_id)
  )
  .reset(signout);

fxCreateFolder.use(contractingApi.createFolder);
fxUpdateFolder.use(contractingApi.updateFolder);
fxAddAttachment.use(contractingApi.addAttachment);
fxDeleteFolder.use(contractingApi.deleteFolder);
fxDeleteFile.use(contractingApi.deleteFile);
fxDownloadAttachment.use(contractingApi.downloadAttachment);

const errorOccured = merge([
  fxCreateFolder.failData,
  fxUpdateFolder.failData,
  fxAddAttachment.failData,
  fxDeleteFolder.failData,
  fxDeleteFile.failData,
  fxDownloadAttachment.failData,
]);

$error.on(errorOccured, (_, error) => error).reset([resetErrorMessage, signout]);

sample({
  source: [
    fxCreateFolder.pending,
    fxUpdateFolder.pending,
    fxAddAttachment.pending,
    fxDeleteFolder.pending,
    fxDeleteFile.pending,
    fxDownloadAttachment.pending,
  ],
  fn: (array) => array.some(Boolean),
  target: $isLoading,
});

sample({
  clock: createFolder,
  target: fxCreateFolder,
});

sample({
  clock: updateFolder,
  target: fxUpdateFolder,
});

sample({
  clock: uploadAttachment,
  target: fxAddAttachment,
});

sample({
  clock: deleteFolder,
  target: fxDeleteFolder,
});

sample({
  clock: deleteFile,
  target: fxDeleteFile,
});

downloadAttachment.watch(({ url, title }) => {
  FileSaver.saveAs(url, title);
});
