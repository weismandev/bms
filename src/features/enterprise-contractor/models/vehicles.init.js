import { guard, sample } from 'effector';
import { pending } from 'patronum/pending';
import { signout } from '@features/common';
import { contractingApi } from '../api';
import { $currentTab, $opened } from './detail.model';
import {
  fxGetVehicles,
  $isLoading,
  $vehicles,
  fxSaveVehicles,
  saveVehicles,
} from './vehicles.model';

fxGetVehicles.use(contractingApi.getVehicleList);
fxSaveVehicles.use(contractingApi.saveVehicles);

$isLoading
  .on(
    pending({
      effects: [fxGetVehicles, fxSaveVehicles],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

guard({
  source: {
    opened: $opened,
    currentTab: $currentTab,
  },
  filter: ({ opened, currentTab }) => Boolean(opened?.id) && currentTab === 'transport',
  target: fxGetVehicles.prepend(({ opened }) => ({
    enterprise_id: opened.id,
    page: 1,
    per_page: 1000,
  })),
});

$vehicles
  .on(fxGetVehicles.done, (_, { result }) => {
    if (!Array.isArray(result?.vehicles)) {
      return [];
    }

    return result.vehicles.map((vehicle) => ({
      id: vehicle.id,
      brand: vehicle.brand,
      license_plate: vehicle.license_plate,
    }));
  })
  .reset(signout);

sample({
  source: $opened,
  clock: saveVehicles,
  fn: ({ id }, { vehicles }) => ({ enterprise_id: id, vehicles }),
  target: fxSaveVehicles,
});

sample({
  source: $opened,
  clock: fxSaveVehicles.done,
  fn: ({ id }) => ({
    enterprise_id: id,
    page: 1,
    per_page: 1000,
  }),
  target: fxGetVehicles,
});
