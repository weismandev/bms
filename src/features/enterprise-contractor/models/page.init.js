import { forward, sample, split, merge } from 'effector';
import { format } from 'date-fns';
import FileSaver from 'file-saver';
import ErrorIcon from '@mui/icons-material/Error';
import { changeNotification } from '@features/colored-text-icon-notification';
import i18n from '@shared/config/i18n';
import { signout } from '../../common';
import { contractingApi } from '../api';
import { $filters } from './filter.model';
import {
  pageMounted,
  fxGetList,
  fxUpdate,
  fxExportEventsList,
  fxPreloadFile,
  fxSaveEnterpriseList,
  fxGetParkingSlots,
  fxGetVehicle,
  fxCreateVehicle,
  fxGetListResident,
  $raw,
  $rowCount,
  $isLoading,
  EXPORT_LIMIT,
  exportEventsClicked,
  $error,
  $residentData,
  $isErrorDialogOpen,
  getEnterprisesByResident,
} from './page.model';
import { $tableParams, $search } from './table.model';
import { fxGetVehicles, fxSaveVehicles } from './vehicles.model';
import { $settingsInitialized, $isGettingSettingsFromStorage } from './user-settings';

const { t } = i18n;

fxGetList.use(contractingApi.getList);
fxExportEventsList.use(contractingApi.exportList);
fxPreloadFile.use(contractingApi.preloadFile);
fxSaveEnterpriseList.use(contractingApi.update);
fxGetParkingSlots.use(contractingApi.getParkingSlots);
fxGetVehicle.use(contractingApi.getVehicle);
fxCreateVehicle.use(contractingApi.createVehicle);
fxGetListResident.use(contractingApi.getList);

sample({
  source: $isGettingSettingsFromStorage,
  fn: (loading) => loading,
  target: $isLoading,
});

$isLoading.on(fxGetList.pending, (_, isLoading) => isLoading).reset(signout);

$raw
  .on(fxGetList.done, (_, { result: { enterprises: data, meta } }) => ({
    data,
    meta,
  }))
  .on(fxUpdate.done, (state, { result: { enterprise } }) => ({
    ...state,
    data: state.data.map((item) => (item.id === enterprise.id ? enterprise : item)),
  }))
  .reset(signout);

$isLoading.on(
  merge([
    fxExportEventsList.pending,
    fxPreloadFile.pending,
    fxGetParkingSlots.pending,
  ]),
  (_, isLoading) => isLoading
);

const errorOccured = merge([
  fxExportEventsList.fail,
  fxPreloadFile.fail,
  fxGetParkingSlots.fail,
  fxGetVehicles.fail,
  fxSaveVehicles.fail,
]);

$error.on(errorOccured, (_, { error }) => error);
$isErrorDialogOpen.on(errorOccured, () => true);

$residentData
  .on(fxGetListResident.done, (_, { result: { enterprises } }) => enterprises)
  .reset(signout);

forward({
  from: fxExportEventsList.pending,
  to: $isLoading,
});

sample({
  clock: [pageMounted, $tableParams, $filters, fxUpdate.done, $settingsInitialized],
  source: {
    params: $tableParams,
    filters: $filters,
    settingsInitialized: $settingsInitialized,
  },
  filter: ({ settingsInitialized }) => settingsInitialized,
  fn: ({ params, filters }) => paylodParams({ params, filters }),
  target: fxGetList,
});

sample({
  source: {
    type: 'resident',
    filter: {
      is_verified: 'all',
      is_owner: 'all',
      is_renter: 'all',
    },
  },
  clock: getEnterprisesByResident,
  target: fxGetListResident,
});

const checkExportLimit = sample({
  source: $rowCount,
  clock: exportEventsClicked,
  fn: (count) => count < EXPORT_LIMIT,
});

const exportEvents = split(checkExportLimit, {
  accepted: (bool) => bool,
  rejected: (bool) => !bool,
});

sample({
  source: { filters: $filters, search: $search },
  clock: exportEvents.accepted,
  fn: ({ filters, search }) => ({ search, per_page: EXPORT_LIMIT, ...filters }),
  target: fxExportEventsList,
});

forward({
  from: exportEvents.rejected,
  to: changeNotification.prepend(() => ({
    isOpen: true,
    color: '#FFA500',
    text: 'Внимание!',
    Icon: ErrorIcon,
    message: `${t('YouRequestedExport')} ${$rowCount.getState()} ${t('ofEvents')}.
      ${t('MaximumAmount')} - ${EXPORT_LIMIT} ${t('eventsPerExport')}.
      ${t('PleaseLimitTheNumberUploadedPostsUsingFiltersandorDearch')}.`,
  })),
});

sample({
  source: fxExportEventsList.done,
  fn: ({ result }) => {
    const blob = new Blob([result], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    FileSaver.saveAs(
      URL.createObjectURL(blob),
      `Events_${format(new Date(), 'dd-MM-yyyy_HH-mm')}.xls`
    );
  },
});

function paylodParams({ params, filters }) {
  const { services, has_vehicle, is_resident, has_parking, is_archived, is_banned } =
    filters;
  const order = {
    key: params.sorting[0]?.name || '',
    order: params.sorting[0]?.order || ' ',
  };
  const availability = {
    has_vehicle,
    is_resident,
    has_parking,
    is_archived,
    is_banned,
  };
  const filtersValues = Object.keys(availability)
    .filter((item) => availability[item])
    .reduce((acc, current) => ({ ...acc, [current]: 'yes' }), {});

  return {
    ...params,
    ...(params.sorting.length > 0 ? order : {}),
    services,
    filter: filtersValues,
  };
}
