import { createEvent, restore } from 'effector';
import { createFilterBag } from '../../../tools/factories';
import { signout } from '../../common';

export const defaultFilters = {
  service_id: [],
  service_item_id: [],
  service_other_id: '',
  services: {},
  has_vehicle: false,
  is_resident: false,
  has_parking: false,
  is_archived: false,
  is_banned: false,
};

export const { changedFilterVisibility, filtersSubmitted, $isFilterOpen, $filters } =
  createFilterBag(defaultFilters);

export const changedGroupVisibility = createEvent();

export const $isGroupOpen = restore(changedGroupVisibility, false).reset([
  signout,
  changedFilterVisibility,
]);
