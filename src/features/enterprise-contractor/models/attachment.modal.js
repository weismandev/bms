import { createEvent, createStore, createEffect } from 'effector';

const fxCreateFolder = createEffect();
const fxUpdateFolder = createEffect();
const fxDeleteFolder = createEffect();
const fxDeleteFile = createEffect();
const fxAddAttachment = createEffect();
const fxDownloadAttachment = createEffect();

const $isAttachmentOpen = createStore(false);
const $attachments = createStore([]);
const $isLoading = createStore(false);
const $error = createStore(null);

const changedAttachmentVisibility = createEvent();
const createFolder = createEvent();
const updateFolder = createEvent();
const uploadAttachment = createEvent();
const setAttachments = createEvent();
const deleteFolder = createEvent();
const deleteFile = createEvent();
const downloadAttachment = createEvent();
const resetErrorMessage = createEvent();

export {
  fxCreateFolder,
  fxUpdateFolder,
  fxDeleteFolder,
  fxDeleteFile,
  fxAddAttachment,
  fxDownloadAttachment,
  $isAttachmentOpen,
  $attachments,
  setAttachments,
  createFolder,
  uploadAttachment,
  updateFolder,
  changedAttachmentVisibility,
  deleteFolder,
  deleteFile,
  downloadAttachment,
  $isLoading,
  $error,
  resetErrorMessage,
};
