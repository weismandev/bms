import { forward, sample, guard } from 'effector';
import { delay } from 'patronum';
import { signout, $pathname, history } from '@features/common';
import { formatPhone } from '@tools/formatPhone';
import { IsExistIdInUrl } from '@tools/is-exist-id-in-url';
import { contractingApi } from '../api';
import convertTimestamp from '../lib/convertTimestamp';
import {
  $mode,
  $currentTab,
  changeTab,
  addClicked,
  $opened,
  detailSubmitted,
  newEntity,
  openViaUrl,
  entityApi,
  $isDetailOpen,
  createVehicle,
  createEnterprise,
  updateEnterprise,
  changedDetailVisibility,
} from './detail.model';
import {
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,
  fxGetVehicle,
  deleteConfirmed,
  fxCreateVehicle,
  pageUnmounted,
  $entityId,
  $path,
  fxGetList,
} from './page.model';
import { fxSaveVehicles } from './vehicles.model';

$currentTab
  .on(changeTab, (_, tab) => tab)
  .on(addClicked, () => 'info')
  .reset(signout);

guard({
  clock: fxGetList.done,
  source: { pathname: $pathname, entityId: $entityId },
  filter: ({ pathname }) => IsExistIdInUrl(pathname),
  target: fxGetById.prepend(({ entityId }) => entityId),
});

const delayedRedirect = delay({ source: fxGetById.fail, timeout: 1000 });

delayedRedirect.watch(() => {
  history.push('.');
});

sample({
  clock: fxCreate.done,
  source: $path,
  fn: (
    path,
    {
      result: {
        enterprise: { id },
      },
    }
  ) => {
    if (id) {
      history.push(`${path}/${id}`);
    }
  },
});

$isDetailOpen
  .off(openViaUrl)
  .on(fxGetById.done, () => true)
  .on([fxDelete.done, fxGetById.fail], () => false)
  .reset([signout, pageUnmounted]);

$mode
  .on([fxCreate.done, fxUpdate.done, fxSaveVehicles.done], () => 'view')
  .reset(signout);

forward({ from: openViaUrl, to: fxGetById });

forward({ from: createVehicle, to: fxCreateVehicle });

$opened
  .on([fxGetById.done, fxCreate.done, fxUpdate.done], (_, { result: { enterprise } }) =>
    handleResponseDataById(enterprise)
  )
  .on([fxGetById.fail, fxDelete.done], () => newEntity)
  .reset([signout, pageUnmounted]);

sample({
  clock: updateEnterprise,
  target: fxUpdate.prepend(handlePayload),
});

sample({
  clock: createEnterprise,
  target: fxCreate.prepend(handlePayload),
});

sample({
  source: $opened,
  clock: deleteConfirmed,
  fn: ({ id }) => id,
  target: fxDelete,
});

function handlePayload(params) {
  const contracts = params.contracts.filter(({ number }) => number);
  return {
    type: 'contractor',
    enterprise: {
      ...params,
      id: params.id || (params.is_resident ? params.title : null),
      title: params.title.title || params.title,
      is_resident: Number(params.is_resident),
      is_archived: Number(params.is_archived),
      is_banned: Number(params.is_banned),
      contracts: contracts.length === 0 ? null : contracts,
      additional_phones: params.additional_phones.filter((phone) => phone),
    },
  };
}

function handleResponseDataById(data) {
  const isResident = Boolean(data.is_resident);
  return {
    id: data.id,
    is_resident: isResident,
    title: data.title, // isResident ? data.id :
    name: (data.responsible && data.responsible.fullname) || '',
    inn: data.inn,
    phone: data.phone,
    comment: data.comment || '',
    ogrn: data.ogrn,
    kpp: data.kpp,
    director: (data.director && data.director.id) || '',
    okved: data.okved,
    attachments: data.attachments || [],
    folders: [],
    // folders: data.folders.map((folder) => ({
    //   ...folder,
    //   updated_at: convertTimestamp(folder.updated_at),
    //   items: folder?.items.map((item) => ({
    //     ...item,
    //     updated_at: convertTimestamp(item.updated_at),
    //   })),
    // })),
    is_verified: Boolean(data.is_verified),
    is_archived: Boolean(data.is_archived),
    is_banned: Boolean(data.is_banned),
    building_id: data.building_id,
    pass_limit: data.pass_limit,
    services: data.services,
    contracts:
      data.contracts && data.contracts.length > 0 ? data.contracts : [{ number: '' }],
    service_item_id: [],
    service_other_id: '',
    additional_phones:
      data.additional_phones instanceof Array
        ? data.additional_phones.map(formatPhone)
        : [],
  };
}

sample({
  clock: guard({
    clock: changedDetailVisibility,
    filter: (visibility) => !visibility,
  }),
  source: $path,
  fn: (path) => history.push(`../${path}`),
});
