import './attachment.init';
import './detail.init';
import './filter.init';
import './page.init';
import './parking-slots.init';
import './table.init';
import './vehicles.init';
