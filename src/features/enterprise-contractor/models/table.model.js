import { combine, createEvent, createStore } from 'effector';
import { signout } from '@features/common';
import i18n from '@shared/config/i18n';
import { createTableBag } from '../../../tools/factories';
import { $raw } from './page.model';

const { t } = i18n;

export const columns = [
  { name: 'title', title: t('NameTitle') },
  { name: 'fullname', title: t('Label.FullName') },
  { name: 'phone', title: t('Label.phone') },
];

export const excludeSortColumns = ['fullname', 'phone'];

const widths = [];

export const fileUploaded = createEvent();
export const downloadTemplateClicked = createEvent();

export const $selection = createStore([]);
export const selectionChanged = createEvent();

export const {
  pageNumChanged,
  pageSizeChanged,
  columnOrderChanged,
  columnWidthsChanged,
  sortChanged,
  hiddenColumnChanged,
  searchChanged,

  $currentPage,
  $pageSize,
  $columnOrder,
  $columnWidths,
  $sorting,
  $hiddenColumnNames,
  $search,
  $tableParams,
} = createTableBag(columns, { widths });

export const $tableData = combine($raw, ({ data }) => {
  return data.map((item) => {
    const phone = item.phone.replace(/[ ]/g, '');

    return {
      ...item,
      fullname: (item.responsible && item.responsible.fullname) || '',
      phone: phone.startsWith('+') ? phone : `+${phone}`,
    };
  });
}).reset(signout);
