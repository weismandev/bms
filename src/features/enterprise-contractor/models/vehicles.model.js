import { createEvent, createStore, createEffect } from 'effector';

export const saveVehicles = createEvent();

export const fxGetVehicles = createEffect();
export const fxSaveVehicles = createEffect();

export const $isLoading = createStore(false);
export const $vehicles = createStore([]);

export const newVehicle = {
  brand: '',
  license_plate: '',
};
