import { createEvent, createStore } from 'effector';
import i18n from '@shared/config/i18n';
import { createDetailBag } from '../../../tools/factories';

const { t } = i18n;

export const newEntity = {
  id: null,
  title: '',
  name: '',
  phone: '',
  ogrn: '',
  inn: '',
  kpp: '',
  director: null,
  okved: '',
  folders: [],
  is_verified: null,
  building_id: null,
  pass_limit: 0,
  is_resident: false,
  is_banned: false,
  is_archived: false,
  contracts: [],
  services: [],
  service_item_id: [],
  service_other_id: '',
  additional_phones: [],
};

export const {
  changedDetailVisibility,
  changeMode,
  detailSubmitted,
  addClicked,
  openViaUrl,
  detailClosed,
  entityApi,

  $isDetailOpen,
  $mode,
  $opened,
} = createDetailBag(newEntity);

export const tabs = [
  {
    value: 'info',
    onCreateIsDisabled: false,
    label: t('information'),
  },
  {
    value: 'transport',
    onCreateIsDisabled: false,
    label: t('transport'),
  },
  {
    value: 'parking',
    onCreateIsDisabled: false,
    label: t('navMenu.enterprises-parking'),
  },
];

export const changeTab = createEvent();
export const createVehicle = createEvent();

export const createEnterprise = createEvent();
export const updateEnterprise = createEvent();

export const $currentTab = createStore('info');
