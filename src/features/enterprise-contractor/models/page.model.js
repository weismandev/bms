import { createEffect, createEvent, createStore } from 'effector';
import { $pathname } from '@features/common';
import { createPageBag, createDialogBag } from '../../../tools/factories';
import { contractingApi } from '../api';

export const exportEventsClicked = createEvent();
export const getEnterprisesByResident = createEvent();

export const fxExportEventsList = createEffect();
export const fxPreloadFile = createEffect();
export const fxSaveEnterpriseList = createEffect();
export const fxForceDeleteItem = createEffect();
export const fxGetListResident = createEffect();

export const fxGetParkingSlots = createEffect();

export const fxGetVehicle = createEffect();
export const fxCreateVehicle = createEffect();

export const parkingSlots = createStore([]);
export const $residentData = createStore([]);

export const EXPORT_LIMIT = 10000;

export const {
  PageGate,
  pageMounted,
  pageUnmounted,

  fxGetList,
  fxGetById,
  fxCreate,
  fxUpdate,
  fxDelete,

  $isLoading,
  $error,
  errorDialogVisibilityChanged,
  $isErrorDialogOpen,

  deleteDialogVisibilityChanged,
  $isDeleteDialogOpen,
  deleteConfirmed,

  $raw,
  $normalized,
  $rowCount,
} = createPageBag(contractingApi, {
  handleCreate,
  handleUpdate,
});

export const $acceptanceError = createStore(null);
export const forceDeleteClicked = createEvent();
export const $path = $pathname.map((pathname) => pathname.split('/')[1]);
export const $entityId = $pathname.map((pathname) => pathname.split('/')[2]);

export const {
  $isDialogOpen: $isAcceptanceErrorDialogOpen,
  dialogVisibilityChanged: acceptanceErrorDialogVisibilityChanged,
} = createDialogBag();

function handleCreate(state, { enterprise }) {
  return {
    ...state,
    data: state.data.concat(enterprise),
    meta: { ...state.meta, total: state.meta.total + 1 },
  };
}

function handleUpdate(state, { enterprise }) {
  const foundIndex = state.data.findIndex((item) => item.id === enterprise.id);
  if (foundIndex) {
    state.data[foundIndex] = enterprise;
  }
  return { ...state };
}
