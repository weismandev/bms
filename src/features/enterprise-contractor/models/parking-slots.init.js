import { guard, sample, forward, attach } from 'effector';
import { pending } from 'patronum/pending';
import { format } from 'date-fns';
import i18n from '@shared/config/i18n';
import { signout } from '../../common';
import { contractingApi } from '../api';
import {
  fxGetParkingSlots,
  $currentPage,
  fxAddParkingSlot,
  fxDeleteParkingSlot,
  ParkingSlotsGate,
  slotSubmitted,
  $slots,
  $slotsPageParams,
  $isLoading,
  $error,
  $mode,
  $deleteData,
  confirm,
  $slotForExtendRent,
  clickExtendSlotRent,
  submitExtendSlotRent,
  fxExtendSlotRent,
} from './parking-slots.model';

const { t } = i18n;

$currentPage.reset(signout);

forward({
  from: $currentPage,
  to: attach({
    effect: fxGetParkingSlots,
    source: [$currentPage, ParkingSlotsGate.state],
    mapParams: (params, [currentPage, gate]) => ({
      ...params,
      page: currentPage,
      enterprise_id: gate.enterprise_id,
    }),
  }),
});

const isEnterpriseIdDefine = ({ enterprise_id }) =>
  Boolean(enterprise_id) || enterprise_id === 0;

const formatPayload = (payload) => {
  const { type, starts_at, ends_at, slot, enterprise_id } = payload;

  const data = {
    type: type.id,
    slot_id: slot.id,
    enterprise_id,
  };

  if (data.type === 'rent') {
    data.rent = {
      starts_at,
      ends_at,
    };
  }

  return data;
};

const getFormattedDateOrEmptyString = (date) => {
  if (date) {
    return format(new Date(date), 'dd.MM.yyyy');
  }

  return '';
};

const formatSlot = ({ rent, ...rest }) => {
  return {
    ...rest,
    rent: rent
      ? {
          starts_at: getFormattedDateOrEmptyString(rent.starts_at),
          ends_at: getFormattedDateOrEmptyString(rent.ends_at),
        }
      : null,
    title: formatParkSlotTitle(rest),
  };
};

fxGetParkingSlots.use(contractingApi.getParkingSlots);
fxAddParkingSlot.use(contractingApi.addParkingSlot);
fxDeleteParkingSlot.use(contractingApi.deleteParkingSlot);
fxExtendSlotRent.use(contractingApi.extendParkingSlotRent);

guard({
  source: ParkingSlotsGate.state,
  filter: isEnterpriseIdDefine,
  target: fxGetParkingSlots,
});

guard({
  source: sample(ParkingSlotsGate.state, slotSubmitted, ({ enterprise_id }, payload) => ({
    enterprise_id,
    ...payload,
  })),
  filter: isEnterpriseIdDefine,
  target: fxAddParkingSlot.prepend(formatPayload),
});

guard({
  source: sample({
    source: { gate: ParkingSlotsGate.state, deleteData: $deleteData },
    clock: confirm,
    fn: ({ gate, deleteData }) => ({
      enterprise_id: gate.enterprise_id,
      slot_id: deleteData.slot_id,
      rent_id: deleteData.rent_id,
    }),
  }),
  filter: isEnterpriseIdDefine,
  target: fxDeleteParkingSlot,
});

sample({
  source: $slots,
  clock: clickExtendSlotRent,
  fn: (slots, idx) => slots.find(({ id }) => id === idx),
  target: $slotForExtendRent,
});

sample({
  source: $slotForExtendRent,
  clock: submitExtendSlotRent,
  fn: (slot, { extend_date }) => ({
    slot_id: slot.id,
    rent_id: slot.rent_id,
    ends_at: format(extend_date, 'yyyy-MM-dd'),
  }),
  target: fxExtendSlotRent,
});

$slots
  .on(fxGetParkingSlots.doneData, (_, { slots }) =>
    Array.isArray(slots) ? slots.map(formatSlot) : []
  )
  .on(fxAddParkingSlot.doneData, (state, { slot }) =>
    [].concat(formatSlot(slot)).concat(state)
  )
  .on(fxDeleteParkingSlot.done, (state, { params }) =>
    state.filter((i) => i.id !== params.slot_id)
  )
  .on(fxExtendSlotRent.doneData, (state, { slot }) =>
    state.map((item) => (item.id === slot.id ? formatSlot(slot) : item))
  )
  .reset(signout);

$slotsPageParams.on(fxGetParkingSlots.doneData, (_, { meta }) => meta).reset(signout);

$mode
  .on([fxAddParkingSlot.done, fxExtendSlotRent.done], () => 'view')
  .on(clickExtendSlotRent, () => 'extend')
  .reset(signout);

$isLoading
  .on(
    pending({
      effects: [
        fxGetParkingSlots,
        fxAddParkingSlot,
        fxDeleteParkingSlot,
        fxExtendSlotRent,
      ],
    }),
    (_, isLoading) => isLoading
  )
  .reset(signout);

const newRequestStarted = guard({ source: $isLoading, filter: Boolean });

$error
  .on(
    [
      fxGetParkingSlots.failData,
      fxAddParkingSlot.failData,
      fxDeleteParkingSlot.failData,
      fxExtendSlotRent.failData,
    ],
    (state, error) => error
  )
  .reset([newRequestStarted, signout]);

$deleteData.reset([signout, fxDeleteParkingSlot.done]);

function formatParkSlotTitle({ number, lot, zone }) {
  const number_ = number || '[ - ]';
  const lotTitle = (Boolean(lot) && lot.title) || t('parking');
  const zoneTitle = Boolean(zone) && zone.title ? `> ${zone.title}` : '';

  return `${number_} ${lotTitle} ${zoneTitle}`;
}
