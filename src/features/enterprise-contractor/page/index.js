import { useStore } from 'effector-react';
import { useTranslation } from 'react-i18next';
import { HaveSectionAccess } from '@features/common';

import {
  DeleteConfirmDialog,
  ErrorMessage,
  FilterMainDetailLayout,
  Loader,
} from '../../../ui';

import { ColoredTextIconNotification } from '../../colored-text-icon-notification';

import {
  $acceptanceError,
  $error,
  $isAcceptanceErrorDialogOpen,
  $isDeleteDialogOpen,
  $isErrorDialogOpen,
  $isLoading,
  $path,
  acceptanceErrorDialogVisibilityChanged,
  deleteConfirmed,
  deleteDialogVisibilityChanged,
  errorDialogVisibilityChanged,
  forceDeleteClicked,
  PageGate,
} from '../models/page.model';

import { $isFilterOpen } from '../models/filter.model';
import { $isDetailOpen } from '../models/detail.model';

import { Detail, Filter, Table } from '../organisms';

const ContractingPage = () => {
  const { t } = useTranslation();
  const isLoading = useStore($isLoading);
  const isErrorDialogOpen = useStore($isErrorDialogOpen);
  const error = useStore($error);
  const isFilterOpen = useStore($isFilterOpen);
  const isDetailOpen = useStore($isDetailOpen);
  const isDeleteDialogOpen = useStore($isDeleteDialogOpen);
  const acceptanceError = useStore($acceptanceError);
  const isAcceptanceErrorDialogOpen = useStore($isAcceptanceErrorDialogOpen);

  return (
    <>
      <PageGate />

      <Loader isLoading={isLoading} />
      <ColoredTextIconNotification />

      <ErrorMessage
        isOpen={isErrorDialogOpen}
        error={error}
        onClose={() => errorDialogVisibilityChanged(false)}
      />

      <DeleteConfirmDialog
        isOpen={isDeleteDialogOpen}
        close={() => deleteDialogVisibilityChanged(false)}
        confirm={deleteConfirmed}
      />

      <DeleteConfirmDialog
        header={t('attention')}
        content={acceptanceError}
        isOpen={isAcceptanceErrorDialogOpen}
        close={() => acceptanceErrorDialogVisibilityChanged(false)}
        confirm={() => forceDeleteClicked()}
      />

      <FilterMainDetailLayout
        main={<Table />}
        filter={isFilterOpen && <Filter />}
        detail={isDetailOpen && <Detail />}
        params={{ detailWidth: 'minmax(370px, 50%)' }}
      />
    </>
  );
};

const RestrictedContractingPage = () => {
  const path = useStore($path);

  return (
    <HaveSectionAccess>
      <ContractingPage />
    </HaveSectionAccess>
  );
};

export { RestrictedContractingPage as ContractingPage };
