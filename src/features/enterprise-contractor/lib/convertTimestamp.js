function convertTimestamp(updated_at) {
  const date = new Date(updated_at * 1000);
  const day = `0${date.getDate()}`;
  const month = `0${date.getMonth()}`;
  const hourse = `0${date.getHours()}`;
  const minutes = `0${date.getMinutes()}`;

  return `${day.slice(-2)}.${month.slice(-2)}.${date.getFullYear()} в ${hourse.slice(
    -2
  )}:${minutes.slice(-2)}`;
}

export default convertTimestamp;
