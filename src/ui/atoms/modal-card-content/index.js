import DialogContent from '@mui/material/DialogContent';
import withStyles from '@mui/styles/withStyles';

const ModalCardContent = withStyles({
  root: {
    color: '#65657B',
    fontSize: '.9em',
    padding: 0,
    lineHeight: '21px',
    overflow: 'visible',
  },
})(DialogContent);

export { ModalCardContent };
