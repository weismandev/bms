import MaskedInput from 'react-text-mask';
import emailMask from 'text-mask-addons/dist/emailMask';

function EmailMaskedInput(props) {
  return <MaskedInput {...props} mask={emailMask} showMask />;
}

export { EmailMaskedInput };
