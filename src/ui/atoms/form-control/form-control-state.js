export function formControlState({ props, states, ctxProps }) {
  return states.reduce((acc, state) => {
    acc[state] = props[state];

    if (ctxProps) {
      if (typeof props[state] === 'undefined') {
        acc[state] = ctxProps[state];
      }
    }

    return acc;
  }, {});
}
