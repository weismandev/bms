import { useContext } from 'react';
import { FormControlContext } from './form-control-context';

export function useFormControl() {
  return useContext(FormControlContext);
}
