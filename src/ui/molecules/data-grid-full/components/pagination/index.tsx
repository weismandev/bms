import { FC, useState } from 'react';
import { MenuItem, Pagination, Select } from '@mui/material';
import { useStyles } from './styles';

export const CustomPagination: FC<IPagination> = ({ rowCount, onPaginationChange }) => {
  const [page, setPage] = useState<number>(1);
  const [pageSize, setPageSize] = useState<DataGridPageSize>(25);
  const pageCount = rowCount / pageSize;
  const classes = useStyles();

  const handlePageChange = (event: any, value: any) => {
    onPaginationChange({
      pageSize: 10,
      page: value,
    });

    setPage(value);
  };

  const handlePageSizeChange = (e: any) => {
    const size = e.target.value;

    onPaginationChange({
      pageSize: size,
      page: 1,
    });

    setPageSize(size);
    setPage(1);
  };

  return (
    <div className={classes.pagination__container}>
      <div className={classes.pagination__pageContainer}>
        <span className={classes.pagination__pageTitle}>Записей на странице</span>

        <Select
          value={pageSize}
          onChange={handlePageSizeChange}
          size="small"
          disableUnderline
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={25}>25</MenuItem>
          <MenuItem value={50}>50</MenuItem>
          <MenuItem value={100}>100</MenuItem>
        </Select>
      </div>

      <Pagination
        color="primary"
        count={pageCount}
        page={page}
        onChange={handlePageChange}
      />
    </div>
  );
};
