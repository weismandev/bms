import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  pagination__container: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    justifyContent: 'end',
    alignItems: 'center',
    gap: 10,
    paddingBottom: 10,

    '& *': {
      borderWidth: '0 !important',
    },
  },
  pagination__pageContainer: {
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    alignItems: 'center',
  },
  pagination__pageTitle: {},
});
