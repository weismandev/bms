import { FC } from 'react';
import { DataGridPro as DataGridMui, LicenseInfo, ruRU } from '@mui/x-data-grid-pro';
import { Loader } from '@ui/index';
import { CustomPagination } from './components/pagination';
import { useStyles } from './style';

LicenseInfo.setLicenseKey(process.env.X_DATA_GRID_PRO_LICENSE);

export const DataGridFull: FC<IDataGrid> = ({
  rows,
  columns,
  rowCount,
  onPaginationChange,
  loading,
  pagination,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Loader isLoading={loading} />

      <DataGridMui
        localeText={ruRU.components.MuiDataGrid.defaultProps.localeText}
        classes={{ root: classes.root }}
        rows={rows}
        columns={columns}
        disableSelectionOnClick
        density="standard"
        pagination={false}
        headerHeight={45}
        rowHeight={45}
        rowCount={rowCount}
        hideFooter
        {...rest}
      />

      {pagination && onPaginationChange ? (
        <CustomPagination rowCount={rowCount} onPaginationChange={onPaginationChange} />
      ) : null}
    </div>
  );
};
