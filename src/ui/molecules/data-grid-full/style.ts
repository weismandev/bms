import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  container: {
    position: 'relative',
    height: '100%',
    display: 'grid',
    gridTemplateRows: '1fr auto',
    gap: 10,
  },
  root: {
    border: 0,

    '& .super-app-theme--header': {
      color: '#7D7D8E',
      fontSize: 16,
      padding: 5,
    },

    '& *::-webkit-scrollbar': {
      width: 10,
      height: 10,
    },

    '& *::-webkit-scrollbar-track': {
      backgroundColor: 'transparent',
    },

    '& *::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      borderRadius: 6,
    },
  },
});
