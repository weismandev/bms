declare interface IDataGrid {
  rows: any[];
  columns: any[];
  rowCount: number;
  onPaginationChange?: (data: any) => any;
  loading?: boolean;
  pagination?: boolean;
}

declare type DataGridPageSize = 10 | 25 | 50 | 100;

declare type DataGridPagination = {
  pageSize: DataGridPageSize;
  page: number;
};

declare interface IPagination {
  rowCount: IDataGrid['rowCount'];
  onPaginationChange: (data: any) => DataGridPagination;
}
