import { api } from '../../../../api/api2';

const fileSave = (payload) => api.no_vers('post', 'v2/system/files/save', payload);

export { fileSave };
