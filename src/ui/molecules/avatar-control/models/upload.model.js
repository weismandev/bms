import { createEffect, createEvent, createStore } from 'effector';

export const fxUploadImage = createEffect();

export const uploadImage = createEvent();
export const unMount = createEvent();

export const $file = createStore({});
