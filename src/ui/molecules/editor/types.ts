export interface IProps {
  name: string;
  error: boolean;
  inputProps: {
    type: string;
  };
  text: string;
  mode: string;
  style?: object;
  className?: string;
  onChange: (e: string) => void;
  placeholder?: string;
}
