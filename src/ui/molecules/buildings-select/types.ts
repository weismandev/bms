import { FormikErrors, FormikValues } from 'formik';

export interface IComplex {
  id: number;
  title: string;
}

export interface IBuildings {
  id: number;
  building: {
    id: number;
  };
  complex: IComplex;
}

export interface IProps {
  houses: Array<IBuildings> | [];
  setBuildings: (e: Array<{ id: number }>) => void;
  buildings: IBuildings[];
  mode: string;
  error?: string | FormikErrors<FormikValues>;
  hideSelect?: boolean;
  isOpenSelect?: boolean;
  changeVisibilityBuildingsSelect?: (data: boolean) => void;
  changeComplex?: (data: { [key: number]: boolean }) => void;
  onSave?: () => void;
  buttonSaveText?: string;
  hideIcon?: boolean;
  saveKind?: any;
  cancelKind?: any;
}

export interface ISelectedBuildings {
  building: {
    title: string;
    id: number | string;
  };
  complex: {
    id: number | string;
  };
}
