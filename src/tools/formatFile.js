export const formatFile = ({ id, name, meta, mime_type, url, bytes }) => ({
  id,
  preview: url,
  meta: { ...meta, name, size: bytes, type: mime_type },
});
