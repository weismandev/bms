import { describe, expect, test } from '@jest/globals';

// eslint-disable-next-line import/no-unresolved
import { wordDeclension } from './word-declension';

describe('sum module', () => {
  test('adds 1 + 2 to equal 3', () => {
    expect(wordDeclension('day', 2)).toBe('дня');
  });
});
