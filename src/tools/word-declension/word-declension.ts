import i18n from '@shared/config/i18n';
import { Word, Cases, Gender } from './word-declension.types';

const { t } = i18n;

/*
@@TODO Сделать проверку на массив, для родительного падежа [годо́в // лет]
*/

export const wordGender = {
  day: Gender.male,
  week: Gender.female,
  month: Gender.male,
  quarter: Gender.male,
  year: Gender.male,
  each: Gender.male,
};

const wordDeclension = (
  word: Word,
  count: number,
  cases: Cases = Cases.NOMINATIVE,
  gender: Gender = wordGender[word]
) => {
  let pluralCase = '';
  let number = '';
  if (word === 'each') {
    pluralCase = count >= 1 && count <= 4 ? Cases.NOMINATIVE : Cases.GENITIVE;
    number = count >= 2 ? 'PLURAL' : 'SINGULAR';
  } else {
    pluralCase = count >= 2 && cases === Cases.NOMINATIVE ? Cases.GENITIVE : cases;
    number = (pluralCase === Cases.GENITIVE && count >= 2 && count <= 4) || count === 1 ? 'SINGULAR' : 'PLURAL';
  }
  const path = `DECLENSION.${pluralCase}.${number}`;

  return t([`${path}.GENUS.${word}.${gender}`, `${path}.${word}`]);
};

export { wordDeclension };
