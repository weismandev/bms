export type Word = 'day' | 'each' | 'week' | 'month' | 'quarter' | 'year';

export enum Cases {
  NOMINATIVE = 'NOMINATIVE',
  GENITIVE = 'GENITIVE',
  DATIVE = 'DATIVE',
  ACCUSATIVE = 'ACCUSATIVE',
  INSTRUMENTAL = 'INSTRUMENTAL',
  PREPOSITIONAL = 'PREPOSITIONAL'
}

export enum Gender {
  male = 0,
  androgynous = 1,
  female = 2,
}
