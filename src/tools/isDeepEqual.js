import { deepStrictEqual } from 'assert';

export function isDeepEqual(first, second) {
  let isEqual;

  try {
    if (deepStrictEqual(first, second) === undefined) {
      isEqual = true;
    }
  } catch (error) {
    isEqual = false;
  }

  return isEqual;
}
