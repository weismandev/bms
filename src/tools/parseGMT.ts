/**
 * Парсинг гринвича к локальному времени
 *
 * @param {string} meanTime Ожидаю что формат даты будет иметь вид yyyy-mm-dd hh:mm:ss
 * @return {Date} Возвращаю дату, для последующего форматирования
* */

export const parseGMT = (meanTime: string): Date => {
  const [date, time] = meanTime.split(' ');
  const [year, month, day] = date.split('-').map(Number);
  const [hour, minutes, seconds] = time.split(':').map(Number);

  return new Date(Date.UTC(year, month, day, hour, minutes, seconds));
};
