import * as Yup from 'yup';

export function createEmailOrPhoneValidateFn(config = {}) {
  const { emailKey = 'email', phoneKey = 'phone' } = config;

  return function testEmailOrPhoneProvided() {
    if (
      !this.parent[emailKey] &&
      (!this.parent[phoneKey] || this.parent[phoneKey] === '+')
    ) {
      return this.createError();
    }

    return true;
  };
}

export function createVehicleNumberValidateFn(numberKey) {
  return function testVehicleNumber() {
    const value = this.parent[numberKey];
    if (
      typeof value === 'string' &&
      /^[АВЕКМНОРСТУХ]\d{3}[АВЕКМНОРСТУХ]{2}\d{2,3}$/gi.test(value)
    ) {
      return true;
    }
    return this.createError();
  };
}

export const vehicleNumberValidation = (config = {}) => {
  const string =
    config.string || Yup.string().required('Это поле обязательно для заполнения.');
  const numberKey = config.numberKey || 'id_number';

  return string.test(
    'correct-vehicle-number',
    'Неверный формат номера!',
    createVehicleNumberValidateFn(numberKey)
  );
};
