import { isArray, isBlob, isNull, isNumber, isObject, isString, type } from './type';

test('Type function return "array" string if passed array arg', () => {
  expect(type([])).toBe('array');
  expect(type([1, 2, 3])).toBe('array');
  expect(type(new Array())).toBe('array');
});

test('Type function return "object" string if passed object arg', () => {
  expect(type({})).toBe('object');
  expect(type(new Object())).toBe('object');
});

test('Type function return "number" string if passed number arg', () => {
  expect(type(12)).toBe('number');
  expect(type(1e3)).toBe('number');
  expect(type(0xfff)).toBe('number');
  expect(type(0b101)).toBe('number');
  expect(type(0o3737)).toBe('number');
});

test('Type function return "function" string if passed function arg', () => {
  expect(type(() => {})).toBe('function');
  expect(type(() => {})).toBe('function');
  expect(type(new Function())).toBe('function');
});

test('Type function return "boolean" string if passed boolean arg', () => {
  expect(type(true)).toBe('boolean');
  expect(type(false)).toBe('boolean');
});

test('Type function return "undefined" string if passed undefined arg', () => {
  expect(type(undefined)).toBe('undefined');
  expect(type([][0])).toBe('undefined');
});

test('Type function return "NaN" string if passed NaN arg', () => {
  expect(type(12 - 'two')).toBe('NaN');
  expect(type(Number.NaN)).toBe('NaN');
});

test('Type function return "blob" string if passed blob arg', () => {
  expect(type(new Blob())).toBe('blob');
});

test('Type function return "set" string if passed set arg', () => {
  expect(type(new Set())).toBe('set');
});

test('Type function return "string" string if passed string arg', () => {
  expect(type('')).toBe('string');
  expect(type('abc')).toBe('string');
  expect(type(String())).toBe('string');
});

test('Type function return "null" string if passed null arg', () => {
  expect(type(null)).toBe('null');
});

test('isNumber function return true if passed a number arg and false otherwise', () => {
  expect(isNumber(12)).toBeTruthy();
  expect(isNumber(null)).toBeFalsy();
  expect(isNumber('string')).toBeFalsy();
  expect(isNumber(new Object())).toBeFalsy();
  expect(isNumber(Number(12))).toBeTruthy();
  expect(isNumber(new Blob())).toBeFalsy();
  expect(isNumber([])).toBeFalsy();
});

test('isArray function return true if passed a array arg and false otherwise', () => {
  expect(isArray([])).toBeTruthy();
  expect(isArray(null)).toBeFalsy();
  expect(isArray('string')).toBeFalsy();
  expect(isArray(new Object())).toBeFalsy();
  expect(isArray(new Array())).toBeTruthy();
  expect(isArray(new Blob())).toBeFalsy();
  expect(isArray(12)).toBeFalsy();
});

test('isString function return true if passed a string arg and false otherwise', () => {
  expect(isString('')).toBeTruthy();
  expect(isString(null)).toBeFalsy();
  expect(isString(undefined)).toBeFalsy();
  expect(isString(new Object())).toBeFalsy();
  expect(isString(String())).toBeTruthy();
  expect(isString(new Blob())).toBeFalsy();
  expect(isString(12)).toBeFalsy();
});

test('isNull function return true if passed a null arg and false otherwise', () => {
  expect(isNull(null)).toBeTruthy();
  expect(isNull(new Set())).toBeFalsy();
  expect(isNull(undefined)).toBeFalsy();
  expect(isNull(new Object())).toBeFalsy();
  expect(isNull([])).toBeFalsy();
  expect(isNull(new Blob())).toBeFalsy();
  expect(isNull(12)).toBeFalsy();
  expect(isNull({})).toBeFalsy();
});

test('isObject function return true if passed a object arg and false otherwise', () => {
  expect(isObject(null)).toBeFalsy();
  expect(isObject(new Set())).toBeFalsy();
  expect(isObject(undefined)).toBeFalsy();
  expect(isObject(new Object())).toBeTruthy();
  expect(isObject([])).toBeFalsy();
  expect(isObject(new Blob())).toBeFalsy();
  expect(isObject(12)).toBeFalsy();
  expect(isObject({})).toBeTruthy();
});

test('isBlob function return true if passed a blob arg and false otherwise', () => {
  expect(isBlob(null)).toBeFalsy();
  expect(isBlob(new Set())).toBeFalsy();
  expect(isBlob(undefined)).toBeFalsy();
  expect(isBlob(new Object())).toBeFalsy();
  expect(isBlob([])).toBeFalsy();
  expect(isBlob(new Blob())).toBeTruthy();
  expect(isBlob(12)).toBeFalsy();
  expect(isBlob({})).toBeFalsy();
});
