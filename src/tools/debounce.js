export const debounce = (func, delay) => {
  let id;
  return function (...args) {
    clearTimeout(id);

    id = setTimeout(() => func.apply(this, args), delay);
  };
};
