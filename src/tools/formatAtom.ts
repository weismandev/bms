/**
 * Форматтер представления атом, формата
 * @param {string} atom исходная строка формата YYYY-MM-DDTHH:mm:ss+HH:mm
 * @param {string} format формат в котором вернуть дату
 * @param {boolean} isLocalTimezone приводить ли локально к таймозоне пользователя
 *                                  для этого использовать тулзу parseGMT
 *
 * @return {string} отморматированная дата
*/

import { format } from 'date-fns';

type FormatDate = 'dd.MM.yyyy';
type FormatDateTime = 'dd.MM.yyyy HH:mm';
type FormatReverse = 'yyyy-MM-dd';

export const FORMAT_DATE: FormatDate = 'dd.MM.yyyy';
export const FORMAT_DATETIME: FormatDateTime = 'dd.MM.yyyy HH:mm';
export const FORMAT_REVERSE_DATE: FormatReverse = 'yyyy-MM-dd';

type FormatOptions = FormatDate | FormatDateTime | FormatReverse;

export const formatAtom = (
  atom: string,
  formatter: FormatOptions = FORMAT_DATE,
  isLocalTimezone: boolean = false
): string => {
  if (!atom?.length) return '';
  const splitDate = atom.split('T');
  if (splitDate.length < 2) {
    console.error('formatAtom: некорректный формат атома');
    return '';
  }
  const [date, time] = splitDate;

  return format(new Date(date), formatter);
};
