const convertNumberToHours = (timezone: number, screenSymbol = true): string => {
  const isNegative = timezone < 0;
  let hours = String(Math.abs(Math.floor(timezone / 60)));
  let minutes = String(timezone % 60);

  if (minutes.length < 2) {
    minutes = `0${minutes}`;
  }
  if (hours.length < 2) {
    hours = `0${hours}`;
  }
  const symbolPlus = screenSymbol ? '%2b' : '+';
  return `${isNegative ? symbolPlus : '-'}${hours}:${minutes}`;
};

export { convertNumberToHours };
