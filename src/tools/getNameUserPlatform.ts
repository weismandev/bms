const getNameUserPlatform = (): string => {
  return navigator?.userAgentData?.platform || navigator.platform;
};

export { getNameUserPlatform };
