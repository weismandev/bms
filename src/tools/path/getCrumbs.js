export function getCrumbs(path) {
  if (Array.isArray(path)) {
    return path;
  }
  const matches = path.matchAll(/[a-zA-Z0-9\-_]+|\?[.*?]/g);
  return Array.from(matches).reduce((acc, [crumb]) => acc.concat(crumb), []);
}
