import { hasPath } from './hasPath';

test('hasPath function returns false if no arguments are passed', () => {
  expect(hasPath()).toBeFalsy();
  expect(hasPath('some.path.name')).toBeFalsy();
  expect(hasPath('', {})).toBeFalsy();
});

const object = {
  a: {
    b: {
      c: 10,
      c1: false,
    },
    b1: null,
    b2: [0, false, null, { b22: undefined, b23: [0, [1, { b231: 231 }]] }],
  },
  a1: '123',
};

const array = [0, [0, null, { a: 1, b: [10] }]];

test('hasPath function return true when path arg passed as "string" and exists in object arg', () => {
  expect(hasPath('a', object)).toBeTruthy();
  expect(hasPath('a1', object)).toBeTruthy();
  expect(hasPath('a.b1', object)).toBeTruthy();
  expect(hasPath('a.b.c1', object)).toBeTruthy();
  expect(hasPath('a.b2[1]', object)).toBeTruthy();
  expect(hasPath('a.b2[3].b22', object)).toBeTruthy();
  expect(hasPath('a.b2[3].b23[1][1].b231', object)).toBeTruthy();
});

test('hasPath function return true when path arg passed as "array" and exists in object arg', () => {
  expect(hasPath(['a'], object)).toBeTruthy();
  expect(hasPath(['a1'], object)).toBeTruthy();
  expect(hasPath(['a', 'b1'], object)).toBeTruthy();
  expect(hasPath(['a', 'b', 'c1'], object)).toBeTruthy();
  expect(hasPath(['a', 'b2', 1], object)).toBeTruthy();
  expect(hasPath(['a', 'b2', '3', 'b22'], object)).toBeTruthy();
  expect(hasPath(['a', 'b2', 3, 'b23', '1', 1, 'b231'], object)).toBeTruthy();
});

test('hasPath function return true when path arg passed as "string" and exists in array arg', () => {
  expect(hasPath('0', array)).toBeTruthy();
  expect(hasPath('1', array)).toBeTruthy();
  expect(hasPath('1.2.b.0', array)).toBeTruthy();
});

test('hasPath function return true when path arg passed as "array" and exists in array arg', () => {
  expect(hasPath(['0'], array)).toBeTruthy();
  expect(hasPath(['1'], array)).toBeTruthy();
  expect(hasPath(['1', '2', 'b', '0'], array)).toBeTruthy();
});
