import { getPathOr } from './getPathOr';

test('getPathOr function returns false if no arguments are passed', () => {
  expect(getPathOr()).toEqual({});
  expect(getPathOr('some.path.name')).toEqual({});
  expect(getPathOr('', {})).toEqual({});
});

const object = {
  a: {
    b: {
      c: 10,
      c1: false,
    },
    b1: null,
    b2: [0, false, null, { b22: 123, b23: [0, [1, { b231: 231 }]] }],
  },
  a1: '123',
};

const array = [0, [0, null, { a: 1, b: [10] }]];

test('getPathOr function return value when path arg passed as "string" and exists in object arg', () => {
  expect(getPathOr('a', object)).toEqual({
    b: {
      c: 10,
      c1: false,
    },
    b1: null,
    b2: [0, false, null, { b22: 123, b23: [0, [1, { b231: 231 }]] }],
  });
  expect(getPathOr('a1', object)).toEqual('123');
  expect(getPathOr('a.b1', object)).toEqual(null);
  expect(getPathOr('a.b.c1', object)).toEqual(false);
  expect(getPathOr('a.b2[1]', object)).toEqual(false);
  expect(getPathOr('a.b2[3].b22', object)).toEqual(123);
  expect(getPathOr('a.b2[3].b23[1][1].b231', object)).toEqual(231);
});

test('getPathOr function return value when path arg passed as "array" and exists in object arg', () => {
  expect(getPathOr(['a'], object)).toEqual({
    b: {
      c: 10,
      c1: false,
    },
    b1: null,
    b2: [0, false, null, { b22: 123, b23: [0, [1, { b231: 231 }]] }],
  });
  expect(getPathOr(['a1'], object)).toEqual('123');
  expect(getPathOr(['a', 'b1'], object)).toEqual(null);
  expect(getPathOr(['a', 'b', 'c1'], object)).toEqual(false);
  expect(getPathOr(['a', 'b2', 1], object)).toEqual(false);
  expect(getPathOr(['a', 'b2', '3', 'b22'], object)).toEqual(123);
  expect(getPathOr(['a', 'b2', 3, 'b23', '1', 1, 'b231'], object)).toEqual(231);
});

test('getPathOr function return value when path arg passed as "string" and exists in array arg', () => {
  expect(getPathOr('0', array)).toEqual(0);
  expect(getPathOr('1', array)).toEqual([0, null, { a: 1, b: [10] }]);
  expect(getPathOr('1.2.b.0', array)).toEqual(10);
});

test('getPathOr function return value when path arg passed as "array" and exists in array arg', () => {
  expect(getPathOr(['0'], array)).toEqual(0);
  expect(getPathOr(['1'], array)).toEqual([0, null, { a: 1, b: [10] }]);
  expect(getPathOr(['1', '2', 'b', '0'], array)).toEqual(10);
});

test("getPathOr function return fallback when path don't exists in array or obj arg", () => {
  expect(getPathOr('', object, 'not found')).toEqual('not found');
  expect(getPathOr('10', array, { one: 1 })).toEqual({ one: 1 });
  expect(getPathOr('a.b2[3].b23[1][1].vasya', object, ['some'])).toEqual(['some']);
  expect(getPathOr(['1', '2', 'b', '0', 'lol'], array, false)).toEqual(false);
});
