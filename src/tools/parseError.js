const parseError = (error) => {
  if (!error) return { isParse: false, error };
  let content;
  try {
    const content = JSON.parse(error.message);
    return { isParse: true, content };
  } catch {
    if (Array.isArray(error)) {
      content = error.map((item, index) => (
        <span key={`err-${index}`}>
          {' '}
          {typeof item === 'object' ? item.message : item}{' '}
        </span>
      ));
    } else if (
      error !== null &&
      typeof error === 'object' &&
      Object.prototype.hasOwnProperty.call(error, 'message')
    ) {
      content = <span> {error.message} </span>;
    } else if (typeof error === 'string') {
      content = <span> {error} </span>;
    } else {
      content = 'Фатальная ошибка';
    }
    return { isParse: false, content };
  }
};

export default parseError;
