import { mergeColumnWidths } from './index';

const defaultColumnWidths = [
  { columnName: 'column-1', value: 100 },
  { columnName: 'column-2', value: 200 },
  { columnName: 'column-3', value: 300 },
  { columnName: 'column-4', value: 400 },
  { columnName: 'column-5', value: 500 },
];

const receivedColumnWidths = [
  { columnName: 'column-9', value: 300 },
  { columnName: 'column-2', value: 500 },
  { columnName: 'column-3', value: 50 },
  { columnName: 'column-8', value: 70 },
  { columnName: 'column-7', value: 90 },
];

const expected = [
  { columnName: 'column-1', value: 100 },
  { columnName: 'column-2', value: 500 },
  { columnName: 'column-3', value: 50 },
  { columnName: 'column-4', value: 400 },
  { columnName: 'column-5', value: 500 },
];

describe('Merge columns result should', () => {
  test("be equal default column widths if received column width isn't array", () => {
    expect(mergeColumnWidths(defaultColumnWidths, null)).toEqual(defaultColumnWidths);
  });

  test('includes widths for column names described in default config', () => {
    expect(mergeColumnWidths(defaultColumnWidths, receivedColumnWidths)).toEqual(
      expected
    );
  });
});
