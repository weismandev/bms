import {
  red,
  pink,
  purple,
  deepPurple,
  indigo,
  lightBlue,
  cyan,
  teal,
  green,
  lightGreen,
  lime,
  yellow,
  amber,
  orange,
  deepOrange,
  brown,
} from '@consts/colors';

const getRandomColor = () => {
  const colors = [
    red,
    pink,
    purple,
    deepPurple,
    indigo,
    lightBlue,
    cyan,
    teal,
    green,
    lightGreen,
    lime,
    yellow,
    amber,
    orange,
    deepOrange,
    brown,
  ];

  const getRandomNumber = () => Math.floor(Math.random() * colors.length);

  return colors[getRandomNumber()];
};

const getRandomColors = (keys = [], definedColorsSchema = {}) => {
  const definedKeys = Object.keys(definedColorsSchema);
  const notDefinedKeys = keys.filter((key) => !definedKeys.includes(String(key)));

  const newColorsSchema = notDefinedKeys.reduce(
    (acc, key) => ({
      ...acc,
      [key]: getRandomColor(),
    }),
    definedColorsSchema
  );

  return newColorsSchema;
};

export { getRandomColors, getRandomColor };
