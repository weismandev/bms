export const IsExistIdInUrl = (pathname) => {
  const path = pathname.split('/')[2];

  return Boolean(path && path.length > 0);
};
