import { createEffect } from 'effector';
import { storageApi } from '../../api/storage/storage';

import type {
  GetStoragePayload,
  GetStorageResponse,
  UpdateStoragePayload,
  UpdateStorageResponse
} from './storage.types';

export const fxGetStorage = createEffect<GetStoragePayload, GetStorageResponse, Error>();
export const fxUpdateStorage = createEffect<UpdateStoragePayload, UpdateStorageResponse, Error>();

fxGetStorage.use(storageApi.getStorage);
fxUpdateStorage.use(storageApi.updateStorage);
