import type {
  GridPinnedColumns,
  GridColumnVisibilityModel,
} from '@mui/x-data-grid-pro';

export type GridColumn = {
  field: string;
  width: string;
};

export type StorageSettingsScheme = {
  columns: GridColumn[],
  pageSize: number
  pinnedColumns: GridPinnedColumns,
  visibilityColumns: GridColumnVisibilityModel;
};

export type GetStoragePayload = {
  storage_key: string;
};

export type GetStorageResponse = StorageSettingsScheme | null;

export type UpdateStoragePayload = {
  storage_key: string;
  data: StorageSettingsScheme;
};

export type UpdateStorageResponse = {
  params: UpdateStoragePayload;
  result: null
};
