import { Components } from '@mui/material';

export const Dialog: Components['MuiDialog'] = {
  defaultProps: {
    fullWidth: true,
  },
  styleOverrides: {},
};
