import { Components } from '@mui/material';

export const CssBaseline: Components['MuiCssBaseline'] = {
  defaultProps: {
    enableColorScheme: true,
  },
};
