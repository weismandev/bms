import { Components } from '@mui/material';

export const List: Components['MuiList'] = {
  styleOverrides: {
    root: {
      padding: 0,
    },
  },
};
