import { Components } from '@mui/material';

export const Tooltip: Components['MuiTooltip'] = {
  styleOverrides: {
    tooltip: {
      padding: '5px 9px',
    },
  },
};
