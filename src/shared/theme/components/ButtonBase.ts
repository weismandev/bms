import { Components } from '@mui/material';

export const ButtonBase: Components['MuiButtonBase'] = {
  defaultProps: {
    disableTouchRipple: true,
  },
};
