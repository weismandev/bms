import { Components } from '@mui/material';

export const ToggleButton: Components['MuiToggleButton'] = {
  styleOverrides: {
    root: {
      textTransform: 'none',
    },
  },
};
