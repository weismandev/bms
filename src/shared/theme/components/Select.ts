import { Components } from '@mui/material';

export const Select: Components['MuiSelect'] = {
  styleOverrides: {
    iconFilled: {
      top: 'calc(50% - .25em)',
    },
  },
};
