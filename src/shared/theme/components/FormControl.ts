import { Components } from '@mui/material';

export const FormControl: Components['MuiFormControl'] = {
  styleOverrides: {
    root: {
      width: '100%',
    },
  },
};
