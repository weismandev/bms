import { Components } from '@mui/material';

export const Popover: Components['MuiPopover'] = {
  styleOverrides: {
    paper: {
      padding: 12,
    },
  },
};
