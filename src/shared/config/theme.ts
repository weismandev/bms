import { createTheme } from '@mui/material';

const { palette } = createTheme();
const { augmentColor } = palette;
const createColor = (mainColor: any) => augmentColor({ color: { main: mainColor } });

declare module '@mui/material/styles' {
  interface Palette {
    custom: Palette['primary'];
  }
  interface PaletteOptions {
    custom: PaletteOptions['primary'];
  }

  interface PaletteColor {
    grey?: string;
  }
  interface SimplePaletteColorOptions {
    grey?: string;
  }
}

const theme = createTheme({
  //https://mui.com/material-ui/customization/default-theme/#main-content
  palette: {
    primary: augmentColor({
      color: {
        main: '#0394E3',
        light: '#35A9E9',
        dark: '#0276B6',
        contrastText: 'white',
      },
    }),
    success: augmentColor({
      color: {
        main: '#1BB169',
        light: '#49C187',
        dark: '#168E54',
        contrastText: 'white',
      },
    }),
    error: createColor('#EB5757'),
    custom: {
      main: '',
      grey: '#F0F2F5',
    },
  },

  // button: {
  //   basic: {
  //     default: '#0394E3',
  //     hover: '#35A9E9',
  //     pressed: '#0276B6',
  //     disabled: '#E7E7E7',
  //   },
  //   positive: {
  //     default: '#1BB169',
  //     hover: '#49C187',
  //     pressed: '#168E54',
  //     disabled: '#E7E7E7',
  //   },
  //   negative: {
  //     default: '#EB5757',
  //     hover: '#EF7979',
  //     pressed: '#BC4646',
  //     disabled: '#E7E7E7',
  //   },
  //   active: {
  //     default: '#0394E3',
  //     hover: '#35A9E9',
  //     pressed: '#0276B6 ',
  //     disabled: '#E7E7E7',
  //   },
  //   'no-active': {
  //     default: '#f2f2f2',
  //     hover: '#f2f2f2',
  //     pressed: '#f2f2f2 ',
  //     disabled: '#f2f2f2',
  //   },
  //   dark: {
  //     default: '#3B3B50',
  //     hover: '#3B3B50',
  //     pressed: '#3B3B50',
  //     disabled: '#3B3B50',
  //   },
  // },
  // label: {
  //   active: '#aaa',
  // },
  // input: {
  //   text: '#65657B',
  //   default: '#e7e7ec',
  //   hover: '#35A9E9',
  //   active: '#0276B6',
  // },
  typography: {
    button: {
      textTransform: 'none',
    },
    fontFamily: [
      'Roboto',
      '-apple-system',
      'BlinkMacSystemFont',
      'Segoe UI',
      'Arial',
      'sans-serif',
    ].join(','),
  },
  // variant: 'outlined',
  components: {
    MuiOutlinedInput: {
      defaultProps: {
        notched: true,
      },
    },
    MuiLink: {
      defaultProps: {
        underline: 'hover',
      },
    },
  },
});

export { theme };
