import { FC } from 'react';
import { CssBaseline, PaletteOptions, StyledEngineProvider } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';

// project import

const palette: PaletteOptions = {
  mode: 'light',
  common: {
    black: '#000',
    white: '#fff',
  },
  primary: {
    '100': '#bae7ff',
    '200': '#91d5ff',
    '400': '#40a9ff',
    '700': '#0050b3',
    '900': '#002766',
    lighter: '#e6f7ff',
    light: '#69c0ff',
    main: '#1890ff',
    dark: '#096dd9',
    darker: '#003a8c',
    contrastText: '#fff',
  },
  secondary: {
    '100': '#f5f5f5',
    '200': '#f0f0f0',
    '400': '#bfbfbf',
    '600': '#595959',
    '800': '#141414',
    lighter: '#f5f5f5',
    light: '#d9d9d9',
    main: '#8c8c8c',
    dark: '#262626',
    darker: '#000000',
    A100: '#ffffff',
    A200: '#434343',
    A300: '#1f1f1f',
    contrastText: '#ffffff',
  },
  error: {
    lighter: '#fff1f0',
    light: '#ffa39e',
    main: '#ff4d4f',
    dark: '#a8071a',
    darker: '#5c0011',
    contrastText: '#fff',
  },
  warning: {
    lighter: '#fffbe6',
    light: '#ffd666',
    main: '#faad14',
    dark: '#ad6800',
    darker: '#613400',
    contrastText: '#f5f5f5',
  },
  info: {
    lighter: '#e6fffb',
    light: '#5cdbd3',
    main: '#13c2c2',
    dark: '#006d75',
    darker: '#002329',
    contrastText: '#fff',
  },
  success: {
    lighter: '#f6ffed',
    light: '#95de64',
    main: '#52c41a',
    dark: '#237804',
    darker: '#092b00',
    contrastText: '#fff',
  },
  grey: {
    '0': '#ffffff',
    '50': '#fafafa',
    '100': '#f5f5f5',
    '200': '#f0f0f0',
    '300': '#d9d9d9',
    '400': '#bfbfbf',
    '500': '#8c8c8c',
    '600': '#595959',
    '700': '#262626',
    '800': '#141414',
    '900': '#000000',
    A50: '#fafafb',
    A100: '#fafafa',
    A200: '#bfbfbf',
    A400: '#434343',
    A700: '#1f1f1f',
    A800: '#e6ebf1',
  },
  text: {
    primary: '#262626',
    secondary: '#8c8c8c',
    disabled: '#bfbfbf',
  },
  action: {
    disabled: '#d9d9d9',
    active: 'rgba(0, 0, 0, 0.54)',
    hover: 'rgba(0, 0, 0, 0.04)',
    hoverOpacity: 0.04,
    selected: 'rgba(0, 0, 0, 0.08)',
    selectedOpacity: 0.08,
    disabledBackground: 'rgba(0, 0, 0, 0.12)',
    disabledOpacity: 0.38,
    focus: 'rgba(0, 0, 0, 0.12)',
    focusOpacity: 0.12,
    activatedOpacity: 0.12,
  },
  divider: '#f0f0f0',
  background: {
    paper: '#ffffff',
    default: '#fafafb',
  },
  contrastThreshold: 3,
  tonalOffset: 0.2,
};

const theme = createTheme({
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xs: 0,
      sm: 768,
      md: 1024,
      lg: 1266,
      xl: 1536,
    },
    unit: 'px',
  },
  direction: 'ltr',
  components: {
    MuiButton: {
      defaultProps: {
        disableElevation: true,
      },
      styleOverrides: {
        root: {
          fontWeight: 400,
        },
        contained: {
          '&.Mui-disabled': {
            backgroundColor: '#f0f0f0',
          },
        },
        outlined: {
          '&.Mui-disabled': {
            backgroundColor: '#f0f0f0',
          },
        },
      },
    },
    MuiBadge: {
      styleOverrides: {
        standard: {
          minWidth: '16px',
          height: '16px',
          padding: '4px',
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: 20,
          '&:last-child': {
            paddingBottom: 20,
          },
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {},
      },
    },
    MuiChip: {
      styleOverrides: {
        root: {
          borderRadius: 4,
          '&:active': {
            boxShadow: 'none',
          },
        },
        sizeLarge: {
          fontSize: '1rem',
          height: 40,
        },
        light: {
          color: '#1890ff',
          backgroundColor: '#e6f7ff',
          borderColor: '#69c0ff',
          '&.MuiChip-lightError': {
            color: '#ff4d4f',
            backgroundColor: '#fff1f0',
            borderColor: '#ffa39e',
          },
          '&.MuiChip-lightSuccess': {
            color: '#52c41a',
            backgroundColor: '#f6ffed',
            borderColor: '#95de64',
          },
          '&.MuiChip-lightWarning': {
            color: '#faad14',
            backgroundColor: '#fffbe6',
            borderColor: '#ffd666',
          },
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          borderRadius: 4,
        },
        sizeLarge: {
          width: '44px',
          height: '44px',
          fontSize: '1.25rem',
        },
        sizeMedium: {
          width: '36px',
          height: '36px',
          fontSize: '1rem',
        },
        sizeSmall: {
          width: '30px',
          height: '30px',
          fontSize: '0.75rem',
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: '#595959',
        },
        outlined: {
          lineHeight: '0.8em',
          '&.MuiInputLabel-sizeSmall': {
            lineHeight: '1em',
          },
          '&.MuiInputLabel-shrink': {
            background: '#ffffff',
            padding: '0 8px',
            marginLeft: -6,
            lineHeight: '1.4375em',
          },
        },
        asterisk: {
          color: 'red',
        },
      },
    },
    MuiLinearProgress: {
      styleOverrides: {
        root: {
          height: 6,
          borderRadius: 100,
        },
        bar: {
          borderRadius: 100,
        },
      },
    },
    MuiLink: {
      defaultProps: {
        underline: 'hover',
      },
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          minWidth: 24,
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        input: {
          padding: '10.5px 14px 10.5px 12px',
        },
        notchedOutline: {
          borderColor: '#d9d9d9',
        },
        root: {
          '&:hover .MuiOutlinedInput-notchedOutline': {
            borderColor: '#69c0ff',
          },
          '&.Mui-focused': {
            boxShadow: '0 0 0 2px rgba(24, 144, 255, 0.2)',
            '& .MuiOutlinedInput-notchedOutline': {
              border: '1px solid #69c0ff',
            },
          },
          '&.Mui-error': {
            '&:hover .MuiOutlinedInput-notchedOutline': {
              borderColor: '#ffa39e',
            },
            '&.Mui-focused': {
              boxShadow: '0 0 0 2px rgba(255, 77, 79, 0.2)',
              '& .MuiOutlinedInput-notchedOutline': {
                border: '1px solid #ffa39e',
              },
            },
          },
        },
        inputSizeSmall: {
          padding: '7.5px 8px 7.5px 12px',
        },
        inputMultiline: {
          padding: 0,
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        inputRoot: {
          padding: '3px 65px 3px 3px',
        },
        tag: {
          backgroundColor: palette.primary.lighter,
          border: '1px solid',
          borderColor: palette.primary.light,
          '& .MuiSvgIcon-root': {
            color: palette.primary.main,
            '&:hover': {
              color: palette.primary.dark,
            },
          },
        },
        tagSizeMedium: {
          borderRadius: 4,
          padding: '2px 0',
          textAlign: 'center',
          verticalAlign: 'middle',
        },
        endAdornment: {
          top: 'unset',
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          minHeight: 46,
          color: '#262626',
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          fontSize: '0.875rem',
          padding: 12,
          borderColor: '#f0f0f0',
        },
        head: {
          fontWeight: 600,
          paddingTop: 20,
          paddingBottom: 20,
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        vertical: {
          overflow: 'visible',
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        gutterBottom: {
          marginBottom: 12,
        },
      },
    },
  },
  palette: palette,
  shape: {
    borderRadius: 4,
  },
  mixins: {
    toolbar: {
      minHeight: 60,
      paddingTop: 8,
      paddingBottom: 8,
    },
  },
  customShadows: {
    button: '0 2px #0000000b',
    text: '0 -1px 0 rgb(0 0 0 / 12%)',
    z1: '0px 2px 8px rgba(0, 0, 0, 0.15)',
  },
  typography: {
    htmlFontSize: 16,
    fontFamily: "'Roboto', sans-serif",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 600,
    h1: {
      fontWeight: 600,
      fontSize: '2.375rem',
      lineHeight: 1.21,
      fontFamily: "'Roboto', sans-serif",
    },
    h2: {
      fontWeight: 600,
      fontSize: '1.875rem',
      lineHeight: 1.27,
      fontFamily: "'Roboto', sans-serif",
    },
    h3: {
      fontWeight: 600,
      fontSize: '1.5rem',
      lineHeight: 1.33,
      fontFamily: "'Roboto', sans-serif",
    },
    h4: {
      fontWeight: 600,
      fontSize: '1.25rem',
      lineHeight: 1.4,
      fontFamily: "'Roboto', sans-serif",
    },
    h5: {
      fontWeight: 600,
      fontSize: '1rem',
      lineHeight: 1.5,
      fontFamily: "'Roboto', sans-serif",
    },
    h6: {
      fontWeight: 400,
      fontSize: '0.875rem',
      lineHeight: 1.57,
      fontFamily: "'Roboto', sans-serif",
    },
    caption: {
      fontWeight: 400,
      fontSize: '0.75rem',
      lineHeight: 1.66,
      fontFamily: "'Roboto', sans-serif",
    },
    body1: {
      fontSize: '0.875rem',
      lineHeight: 1.57,
      fontFamily: "'Roboto', sans-serif",
      fontWeight: 400,
    },
    body2: {
      fontSize: '0.75rem',
      lineHeight: 1.66,
      fontFamily: "'Roboto', sans-serif",
      fontWeight: 400,
    },
    subtitle1: {
      fontSize: '0.875rem',
      fontWeight: 600,
      lineHeight: 1.57,
      fontFamily: "'Roboto', sans-serif",
    },
    subtitle2: {
      fontSize: '0.75rem',
      fontWeight: 500,
      lineHeight: 1.66,
      fontFamily: "'Roboto', sans-serif",
    },
    overline: {
      lineHeight: 1.66,
      fontFamily: "'Roboto', sans-serif",
      fontWeight: 400,
      fontSize: '0.75rem',
      textTransform: 'uppercase',
    },
    button: {
      textTransform: 'capitalize',
      fontFamily: "'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '0.875rem',
      lineHeight: 1.75,
    },
    fontSize: 14,
  },
  shadows: [
    'none',
    '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
    '0px 3px 1px -2px rgba(0,0,0,0.2),0px 2px 2px 0px rgba(0,0,0,0.14),0px 1px 5px 0px rgba(0,0,0,0.12)',
    '0px 3px 3px -2px rgba(0,0,0,0.2),0px 3px 4px 0px rgba(0,0,0,0.14),0px 1px 8px 0px rgba(0,0,0,0.12)',
    '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 6px 10px 0px rgba(0,0,0,0.14),0px 1px 18px 0px rgba(0,0,0,0.12)',
    '0px 4px 5px -2px rgba(0,0,0,0.2),0px 7px 10px 1px rgba(0,0,0,0.14),0px 2px 16px 1px rgba(0,0,0,0.12)',
    '0px 5px 5px -3px rgba(0,0,0,0.2),0px 8px 10px 1px rgba(0,0,0,0.14),0px 3px 14px 2px rgba(0,0,0,0.12)',
    '0px 5px 6px -3px rgba(0,0,0,0.2),0px 9px 12px 1px rgba(0,0,0,0.14),0px 3px 16px 2px rgba(0,0,0,0.12)',
    '0px 6px 6px -3px rgba(0,0,0,0.2),0px 10px 14px 1px rgba(0,0,0,0.14),0px 4px 18px 3px rgba(0,0,0,0.12)',
    '0px 6px 7px -4px rgba(0,0,0,0.2),0px 11px 15px 1px rgba(0,0,0,0.14),0px 4px 20px 3px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 12px 17px 2px rgba(0,0,0,0.14),0px 5px 22px 4px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 13px 19px 2px rgba(0,0,0,0.14),0px 5px 24px 4px rgba(0,0,0,0.12)',
    '0px 7px 9px -4px rgba(0,0,0,0.2),0px 14px 21px 2px rgba(0,0,0,0.14),0px 5px 26px 4px rgba(0,0,0,0.12)',
    '0px 8px 9px -5px rgba(0,0,0,0.2),0px 15px 22px 2px rgba(0,0,0,0.14),0px 6px 28px 5px rgba(0,0,0,0.12)',
    '0px 8px 10px -5px rgba(0,0,0,0.2),0px 16px 24px 2px rgba(0,0,0,0.14),0px 6px 30px 5px rgba(0,0,0,0.12)',
    '0px 8px 11px -5px rgba(0,0,0,0.2),0px 17px 26px 2px rgba(0,0,0,0.14),0px 6px 32px 5px rgba(0,0,0,0.12)',
    '0px 9px 11px -5px rgba(0,0,0,0.2),0px 18px 28px 2px rgba(0,0,0,0.14),0px 7px 34px 6px rgba(0,0,0,0.12)',
    '0px 9px 12px -6px rgba(0,0,0,0.2),0px 19px 29px 2px rgba(0,0,0,0.14),0px 7px 36px 6px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 20px 31px 3px rgba(0,0,0,0.14),0px 8px 38px 7px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 21px 33px 3px rgba(0,0,0,0.14),0px 8px 40px 7px rgba(0,0,0,0.12)',
    '0px 10px 14px -6px rgba(0,0,0,0.2),0px 22px 35px 3px rgba(0,0,0,0.14),0px 8px 42px 7px rgba(0,0,0,0.12)',
    '0px 11px 14px -7px rgba(0,0,0,0.2),0px 23px 36px 3px rgba(0,0,0,0.14),0px 9px 44px 8px rgba(0,0,0,0.12)',
    '0px 11px 15px -7px rgba(0,0,0,0.2),0px 24px 38px 3px rgba(0,0,0,0.14),0px 9px 46px 8px rgba(0,0,0,0.12)',
  ],
  transitions: {
    easing: {
      easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
      easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
      easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
      sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
    },
    duration: {
      shortest: 150,
      shorter: 200,
      short: 250,
      standard: 300,
      complex: 375,
      enteringScreen: 225,
      leavingScreen: 195,
    },
  },
  zIndex: {
    mobileStepper: 1000,
    fab: 1050,
    speedDial: 1050,
    appBar: 1100,
    drawer: 1200,
    modal: 1300,
    snackbar: 1400,
    tooltip: 1500,
  },
});

export const ThemeMantis: FC<{ children: any }> = ({ children }) => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  </StyledEngineProvider>
);
