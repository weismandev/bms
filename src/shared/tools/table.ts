import { createStore, createEvent, combine, restore } from 'effector';
import { throttle } from 'patronum/throttle';
import type {
  GridSortModel,
  GridColDef,
  GridRowId,
  GridPinnedColumns,
  GridColumnVisibilityModel,
  GridColumnOrderChangeParams,
  GridColumnResizeParams,
  GridPaginationModel,
  GridRowSelectionModel,
  GridInputRowSelectionModel
} from '@mui/x-data-grid-pro';
import { signout } from '@features/common';
import type { GridColumn } from '@shared/models/storage';
import { createForm } from '@unicorn/effector-form';
import type { Config, SearchForm } from './table.types';

/*
  Можно сохранять количество строк в таблице ($pageSize), ширину и порядок столбцов ($columns),
  видимость столбцов ($visibilityColumns).
  Пример реализации сохранения можно посмотреть в фиче polls. Файлы:
    1) модель сохранения /features/polls/models/user-settings/user-settings.model,
    2) обработка полученных значений с бэка/features/polls/models/table/table.init,
    3) запрос сохраненных данных /features/polls/models/page/page.init
*/

const getConfig = (config: Config) => ({
  search: '',
  sorting: [],
  visibilityColumns: {},
  selectRow: [],
  pinnedColumns: { left: [], right: [] },
  paginationModel: { page: 0, pageSize: 10 },
  ...config,
});

export const createTableBag = (customConfig: Config) => {
  const config = getConfig(customConfig);

  const searchForm = createForm<SearchForm>({
    initialValues: {
      search: config.search,
    },
    editable: true,
  });

  const paginationModelChanged = createEvent<GridPaginationModel>();
  const sortChanged = createEvent<GridSortModel>();
  const searchChanged = createEvent<string>();
  const selectionRowChanged = createEvent<GridRowSelectionModel>();
  const visibilityColumnsChanged = createEvent<GridColumnVisibilityModel>();
  const columnsWidthChanged = createEvent<GridColumnResizeParams>();
  const orderColumnsChanged = createEvent<GridColumnOrderChangeParams>();
  const openRowChanged = createEvent<GridInputRowSelectionModel | null>();
  const changePinnedColumns = createEvent<GridPinnedColumns>();
  const mergeColumnsByPattern = createEvent<GridColDef[]>();

  const throttledSearch = throttle({
    source: searchChanged,
    timeout: 1000,
  });

  const $pagination = createStore<GridPaginationModel>({
    page: config.paginationModel.page,
    pageSize: config.paginationModel.pageSize,
  });

  const $sorting = createStore<GridSortModel>(config.sorting);
  const $search = createStore<string>(config.search);
  const $columns = createStore<GridColDef[]>(config.columns);
  const $selectRow = createStore<GridRowSelectionModel>(config.selectRow);

  const $columnWidths = $columns.map((columns) =>
    columns.map(({ field, width }) => ({ field, width }))
  );

  const $columnOrders = $columns.map((columns) => columns.map(({ field }) => field));

  const $openRow = createStore<GridRowId | null>(config?.openRow ?? null);
  const $pinnedColumns = createStore<GridPinnedColumns>(config.pinnedColumns);

  /*
    $selectRow хранит какая строка в таблице выбрана.
    В каждой фиче, где используется DataGrid:
    1) в модели дополнительно сбрасывать
    $selectRow при срабатывании ивентов detailClosed, addClicked,
    если используется фабрика деталей.
    2) после успешного выполнения запроса на создание сущности (fxCreate.done),
      из result взять id и передать в стор $selectRow в модели фичи
      (нужно чтобы после создания сущности в таблице отображалась выбранная строка)

    Пример можно посмотреть в /features/polls/models/table/table.init
  */
  const $visibilityColumns = createStore<GridColumnVisibilityModel>(
    config.visibilityColumns
  );
  /*
    $visibilityColumns представляет из себя объект.
    Структура объекта: название колонки и значение видимости.
    Если нет необходимости сохранять скрытые столбцы при переходе между разделами или
    сохранять на бэке, то можно не использовать

    Пример можно посмотреть в /features/polls/organisms/table/index.tsx
  */
  const $tableParams = combine(
    $pagination,
    $sorting,
    restore(throttledSearch, ''),
    ({ page, pageSize }, sorting, search) => ({
      page: Number(page) + 1,
      per_page: pageSize,
      sorting: sorting.map((item) => ({ field: item.field, sort: item.sort })),
      search,
    })
  );

  $pagination
    .on(paginationModelChanged, (paginationModel, newPaginationModel) => {
      if (paginationModel.pageSize === newPaginationModel.pageSize) {
        if (paginationModel.page !== newPaginationModel.page) {
          return newPaginationModel;
        }

        return paginationModel;
      }

      return {
        ...newPaginationModel,
        page: 0,
      };
    })
    .on(searchChanged, (paginationModel) => ({
      ...paginationModel,
      page: 0,
    }))
    .reset([signout]);

  $sorting.on(sortChanged, (_, sort) => sort).reset(signout);
  $search.on(searchChanged, (_, search) => search).reset(signout);
  $openRow.on(openRowChanged, (_, row) => row).reset(signout);
  $selectRow.on(selectionRowChanged, (_, row) => row).reset(signout);
  $visibilityColumns.on(visibilityColumnsChanged, (_, column) => column).reset(signout);

  $columnWidths
    .on(columnsWidthChanged, (state, { colDef }) => ({
      ...state,
      [colDef.field]: colDef.width,
    }))
    .reset(signout);

  $columnOrders
    .on(orderColumnsChanged, (columns, { oldIndex, targetIndex }) => {
      const orderedColumn = columns[oldIndex];
      return columns.toSpliced(oldIndex, 1).toSpliced(targetIndex, 0, orderedColumn);
    })
    .reset(signout);

  $columns
    .on(columnsWidthChanged, (state, { colDef }) =>
      state.map((item) =>
        item.field === colDef.field
          ? { ...item, width: colDef.width || item.width }
          : item
      )
    )
    .on(orderColumnsChanged, (columns, { oldIndex, targetIndex }) => {
      const orderedColumn = columns[oldIndex];
      return columns.toSpliced(oldIndex, 1).toSpliced(targetIndex, 0, orderedColumn);
    })
    .on(
      mergeColumnsByPattern,
      (columns: GridColDef[], storageColumns: GridColumn[]): GridColDef[] => {
        /* Фильтрую полученные колонки по признаку существовании их в конфиге */
        const relevantStorageColumns: GridColDef[] | [] = storageColumns.filter(
          ({ field }) => columns.some((column) => column.field === field)
        );

        /* новые колонки, которых еще нет в сторадже */
        const newColumns: GridColDef[] = columns.filter(
          ({ field }) => !relevantStorageColumns.some((column) => column.field === field)
        );

        return relevantStorageColumns
          .map(({ field, width }) => {
            const column = columns.find((colDef) => colDef.field === field);
            return {
              ...column,
              field,
              width,
            };
          })
          .concat(newColumns);
      }
    )
    .reset(signout);

  $pinnedColumns.on(changePinnedColumns, (_, columns) => columns).reset(signout);

  /** При изменении в инпуте поиска обновляется стор $search */
  throttle({
    source: searchForm.values,
    timeout: 1000,
    target: throttledSearch.prepend(({ search }: { search: string }) => search),
  });

  return {
    $pagination,
    sortChanged,
    searchChanged,
    $sorting,
    $search,
    $tableParams,
    $selectRow,
    selectionRowChanged,
    $visibilityColumns,
    visibilityColumnsChanged,
    columnsWidthChanged,
    orderColumnsChanged,
    $columns,
    $openRow,
    openRowChanged,
    $pinnedColumns,
    changePinnedColumns,
    mergeColumnsByPattern,
    paginationModelChanged,
    searchForm,
  };
};
