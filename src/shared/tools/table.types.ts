import type {
  GridSortModel,
  GridColDef,
  GridPinnedColumns,
  GridPaginationModel,
  GridColumnVisibilityModel,
  GridRowId,
} from '@mui/x-data-grid-pro';

export type Config = {
  columns: GridColDef[];
  search?: string;
  sorting?: GridSortModel;
  visibilityColumns?: GridColumnVisibilityModel;
  selectRow?: number[];
  paginationModel?: GridPaginationModel;
  columnsWidth?: { [key: string]: number };
  pinnedColumns?: GridPinnedColumns;
  openRow?: GridRowId | null;
};

export type SearchForm = {
  search: string;
};
