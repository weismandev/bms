import isEqual from 'react-fast-compare';
import { createStore, sample, combine, createEffect, Store, Event } from 'effector';
import { Gate } from 'effector-react';
import type {
  GridColDef,
  GridPinnedColumns,
  GridColumnVisibilityModel,
} from '@mui/x-data-grid-pro';
import {
  fxGetStorage,
  fxUpdateStorage,
  StorageSettingsScheme,
} from '@shared/models/storage';

export type StorageScheme = {
  columns: [Store<GridColDef[]>, Event<GridColDef[]>];
  pageSize: [Store<number>, Event<number>];
  pinnedColumns: [Store<GridPinnedColumns>, Event<GridPinnedColumns>];
  visibilityColumns: [Store<GridColumnVisibilityModel>, Event<GridColumnVisibilityModel>];
};

type Props<G> = {
  storageKey: string;
  storageGate: Gate<G>;
  storageScheme: StorageScheme;
};

export const connectStorage = <G>({
  storageKey,
  storageGate,
  storageScheme,
}: Props<G>): {
  $isGettingSettingsFromStorage: Store<boolean>;
  $storage: Store<StorageSettingsScheme | null>;
} => {
  const {
    columns: [$columns, mergeColumnsByPattern],
    pageSize: [$pageSize, pageSizeChanged],
    pinnedColumns: [$pinnedColumns, changePinnedColumns],
    visibilityColumns: [$visibilityColumns, visibilityColumnsChanged],
  } = storageScheme;

  const $settings = combine(
    $columns,
    $pageSize,
    $pinnedColumns,
    $visibilityColumns,
    (columns, pageSize, pinnedColumns, visibilityColumns) => ({
      columns: columns.map(({ field, width }) => ({ field, width })),
      pageSize,
      pinnedColumns,
      visibilityColumns,
    })
  );

  /* Статус получения значений из хранилища */
  const $isGettingSettingsFromStorage = createStore<boolean>(false)
    .on(fxGetStorage.done, () => true)
    .reset(storageGate.close);

  /* Флаг запроса на получение стораджа, нужен что бы не отправлять
      значения на сохранения сторов до момента пока не получили их из базы
  */
  const $isDirtyStorage = createStore<boolean>(false)
    .on(fxGetStorage.done, () => true)
    .reset(storageGate.close);

  /* Стор со значениями */
  const $storage = createStore<StorageSettingsScheme | null>(null)
    .on(fxGetStorage.done, (_, { result }) => result)
    // .on(fxUpdateStorage.done, (_, { params }) => params.data)
    .reset(storageGate.close);

  /* Запрос на получение из хранилища */
  sample({
    clock: storageGate.open,
    fn: () => ({ storage_key: storageKey }),
    target: fxGetStorage,
  });

  /* Эффект, срабатывает после получения стораджа, для вызова событий на обнавление сторов */
  const useEvents = createEffect(
    ({ columns, pageSize, pinnedColumns, visibilityColumns }: StorageSettingsScheme) => {
      pageSizeChanged(pageSize);
      changePinnedColumns(pinnedColumns);
      visibilityColumnsChanged(visibilityColumns);
      mergeColumnsByPattern(columns);
    }
  );

  /* Запрос на сохранение в хранилище,
    при условии если мы получили изначальный стор
  */
  /* TODO isEqual для каждого свойства */
  sample({
    clock: $settings,
    source: [$settings, $storage, $isGettingSettingsFromStorage, $isDirtyStorage],
    filter: ([settings, storage, isGettingSettingsFromStorage, isDirtyStorage]) =>
      Boolean(
        isGettingSettingsFromStorage && isDirtyStorage && !isEqual(settings, storage)
      ),
    fn: ([data]) => ({ data, storage_key: storageKey }),
    target: fxUpdateStorage,
  });

  /* Вызов эвентов для обновления сторов */
  sample({
    clock: fxGetStorage.done,
    source: storageGate.status,
    filter: (status, { result }) =>
      status && result !== null && Boolean(Object.keys(result).length > 0),
    fn: (_, { result }) => result,
    target: useEvents,
  });

  return {
    $isGettingSettingsFromStorage,
    $storage,
  };
};
