import { FC, forwardRef } from 'react';
import { ButtonDefaultProps } from './global';
import { Styled } from './styles';

export const Button: FC<ButtonDefaultProps> = forwardRef(
  (
    { title, variant = 'contained', size = 'small', children, loading = false, ...props },
    ref: any
  ) => {
    return loading ? (
      <Styled.LoadingButton
        ref={ref}
        variant={variant}
        size={size}
        loading={loading}
        {...props}
      >
        {title}
        {children}
      </Styled.LoadingButton>
    ) : (
      <Styled.Button ref={ref} variant={variant} size={size} {...props}>
        {children}
        {title}
      </Styled.Button>
    );
  }
);
