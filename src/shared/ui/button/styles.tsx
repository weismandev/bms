import { Button as MuiButton } from '@mui/material';
import { LoadingButton as MuiLoadingButton } from '@mui/lab';
import styled from '@emotion/styled';

const baseButtonStyles = {
  zIndex: 'auto',
  paddingLeft: 15,
  paddingRight: 15,
  borderRadius: 17,
  fontSize: 14,
  fontWeight: 500,
};

const Button = styled(MuiButton)(() => ({
  ...baseButtonStyles,
}));

const LoadingButton = styled(MuiLoadingButton)(() => ({
  ...baseButtonStyles,
}));

export const Styled = {
  Button,
  LoadingButton,
};
