//@ts-nocheck
import { Check } from '@mui/icons-material';
import { Button } from './';

export default {
  title: 'Button',
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const Text = Template.bind({});
Text.args = {
  title: 'Text Button',
  color: 'primary',
};

export const Icon = Template.bind({});
Icon.args = {
  title: 'Icon Button',
  color: 'primary',
  startIcon: <Check />,
};
