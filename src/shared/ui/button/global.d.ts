import { ButtonProps } from '@mui/material';
import { LoadingButtonProps } from '@mui/lab';

declare interface ButtonDefaultProps extends ButtonProps, LoadingButtonProps {
  loading: boolean;
}
