import { Meta } from '@storybook/addon-docs';
import { Palette } from './palette';

export default {
  title: 'Palette',
  component: Palette,
};

const Template = (args: any) => (
  <Palette {...args} />
);

export const Default = Template.bind({});

// @ts-ignore
Default.args = {};
