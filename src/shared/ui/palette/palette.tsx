import { PaletteOptions, Grid, Typography } from '@mui/material';

import { theme } from '../../theme';
import { StyledPaper, StyledTypography, StyledColor } from './styled';

const ColorItem = (paletteOption: PaletteOptions) => (
  <StyledPaper>
    {Object
    .keys(paletteOption)
    .map((palette) => (
      <StyledColor key={palette} backgroundColor={paletteOption[palette]}>
        <StyledTypography variant="overline">
          {palette}
        </StyledTypography>
      </StyledColor>
    ))}
  </StyledPaper>
);

export const Palette = () => {
  const { palette } = theme;
  const { primary, secondary, error, warning, info, success } = palette;
  const colors = { primary, secondary, error, warning, info, success };

  return (
    <Grid container spacing={2}>
      {Object.keys(colors).map((color) => (
        <>
          <Grid item xs={2}>
            <Typography variant="h6">
              {`${color.charAt(0).toUpperCase()}${color.slice(1)}`}
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <ColorItem {...colors[color]} />
          </Grid>
        </>
      ))}
    </Grid>
  );
};
