import styled from '@emotion/styled';
import { Paper, Typography } from '@mui/material';

export const StyledPaper = styled(Paper)(() => ({
  display: 'flex',
  height: 40,
  marginBottom: 24
}));

export const StyledTypography = styled(Typography)(() => ({
  display: 'block',
  marginTop: 40
}));

export const StyledColor = styled.div(({ backgroundColor }) => ({
  textAlign: 'center',
  width: '100%',
  backgroundColor
}));
