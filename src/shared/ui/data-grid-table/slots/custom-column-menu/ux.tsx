import { GridColumnMenu, GridColumnMenuPinningItem, GridColumnMenuProps } from '@mui/x-data-grid-pro';

export const CustomColumnMenu = (props: GridColumnMenuProps) => {
  const { hideMenu, colDef, ...rest } = props;
  return (
    <GridColumnMenu
      hideMenu={hideMenu}
      colDef={colDef}
      slots={{
        // columnMenuColumnsItem: null,
        columnMenuFilterItem: null
      }}
      {...rest}
    >
      {/* <GridColumnMenuPinningItem colDef={colDef} /> */}
    </GridColumnMenu>
  );
};
