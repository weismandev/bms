import { useMemo, MouseEvent, ChangeEvent } from 'react';
import { GridPagination } from '@mui/x-data-grid-pro';
import MuiPagination from '@mui/material/Pagination';

import type { TablePaginationProps } from '@mui/material/TablePagination';

type SlotPropsCustomPagination = {
  page: number;
  rowsPerPage: number;
  onPageChange: (event: ChangeEvent<unknown>, newPage: number) => void;
};

const Pagination = ({
  count,
  page,
  className,
  rowsPerPage,
  onPageChange,
}: TablePaginationProps) => {
  const handleChange = (event: ChangeEvent<unknown>, newPage: number) => {
    onPageChange(event as MouseEvent<HTMLButtonElement>, newPage - 1);
  };

  const countPage = useMemo(
    () => Math.ceil(count / rowsPerPage),
    [count, rowsPerPage]
  );

  return (
    <MuiPagination
      shape="rounded"
      count={countPage}
      page={page + 1}
      onChange={handleChange}
      className={className}
    />
  );
};

export const CustomPagination = (props: SlotPropsCustomPagination) => (
  <GridPagination ActionsComponent={Pagination} {...props} />
);
