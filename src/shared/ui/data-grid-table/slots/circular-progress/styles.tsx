import CircularProgress from '@mui/material/CircularProgress';
import styled from '@emotion/styled';

const Overlay = styled.div(() => ({
  backgroundColor: 'rgba(255, 255, 255, 0.8)',
  display: 'grid',
  height: '100%',
  width: '100%',
}));

const StyledSpinner = styled(CircularProgress)`
  place-self: center;

  .MuiCircularProgress-circle {
    stroke: #1C90FB;
  }
`;

export {
  Overlay,
  StyledSpinner,
};
