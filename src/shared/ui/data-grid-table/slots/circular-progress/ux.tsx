import { Overlay, StyledSpinner } from './styles';

export const CircularProgress = () => (
  <Overlay>
    <StyledSpinner />
  </Overlay>
);
