import { useRef } from 'react';

import {
  DataGridPro,
  DataGridProProps,
  LicenseInfo,
  GridColumnOrderChangeParams,
  MuiEvent,
  GridCallbackDetails
} from '@mui/x-data-grid-pro';

import {
  CircularProgress,
  CustomColumnMenu,
  CustomPagination,
} from './slots';
import { pageSizeOptions } from './constants';

LicenseInfo.setLicenseKey(process.env.X_DATA_GRID_PRO_LICENSE as string);

export const DataGridTable = (props: DataGridProProps) => {
  const ref = useRef(null);

  const {
    rows,
    columns,
    paginationMode,
    rowSelectionModel,
    onColumnOrderChange,
    sx = {},
    ...rest
  } = props;

  // TODO: replace any
  const handleColumnOrderChange = (
    params: GridColumnOrderChangeParams,
    event: MuiEvent<any>,
    details: GridCallbackDetails & { api?: any }
  ) => {
      if (onColumnOrderChange instanceof Function) {
        const [column] = details.api.getAllColumns();
        if (column.type === 'checkboxSelection') {
          onColumnOrderChange(
            { ...params, oldIndex: params.oldIndex - 1, targetIndex: params.targetIndex - 1 },
            { defaultMuiPrevented: false },
            details
          );
        } else {
          onColumnOrderChange(params, { defaultMuiPrevented: false }, details);
        }
      }
    };

  return (
    <DataGridPro
      ref={ref}
      rows={rows}
      columns={columns}
      pagination
      slots={{
        columnMenu: CustomColumnMenu,
        loadingOverlay: CircularProgress,
        pagination: CustomPagination,
      }}
      rowSelectionModel={rowSelectionModel ?? undefined} // на случай если в rowSelectionModel будет null
      pageSizeOptions={pageSizeOptions}
      onColumnOrderChange={handleColumnOrderChange}
      paginationMode={paginationMode ?? 'server'}
      sx={{ ...sx, flex: '1 1 0px' }}
      {...rest}
    />
  );
};
