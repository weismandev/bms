import { Box } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid-pro';

import { DataGridTable } from './ux';
import { createTableBag } from '../../tools/table';

export default {
  title: 'DataGridTable',
  component: DataGridTable,
};

const Template = (args: any) => (
  <DataGridTable {...args} />
);

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 90 },
  {
    field: 'firstName',
    headerName: 'First name',
    width: 150,
    editable: false,
  },
  {
    field: 'lastName',
    headerName: 'Last name',
    width: 150,
  },
  {
    field: 'age',
    headerName: 'Age',
    type: 'number',
    width: 110,
  },
];

const rows = new Array(5).fill(undefined).map((_, index) => ({
  id: index,
  lastName: `row-${index}`,
  firstName: `row-${index}`,
  age: `row-${index}`,
}));

export const Default = Template.bind({});
export const WithoutPagination = Template.bind({});
export const FixedColumn = Template.bind({});

// @ts-ignore
Default.args = {
  columns,
  rows,
  // rowCount: 100,
  // pageSizeOptions: [5],
  // paginationModel: {
  //   page: 0,
  //   pageSize: 5,
  // },
  // paginationMode: 'server',
  initialState: {
    pagination: { paginationModel: { pageSize: 5 } },
  }
};

// @ts-ignore
WithoutPagination.args = {
  columns,
  rows,
  hideFooter: true,
  hideFooterPagination: true
};

// @ts-ignore
FixedColumn.args = {
  columns,
  rows,
  pinnedColumns: { left: ['firstName'] }
};
